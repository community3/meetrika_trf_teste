/*
               File: REL_RelatorioComparacaoDemandas
        Description: Relat�rio de Compara��o entre Demandas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/19/2020 1:8:40.97
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class rel_relatoriocomparacaodemandas : GXProcedure
   {
      public rel_relatoriocomparacaodemandas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public rel_relatoriocomparacaodemandas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_AreaTrabalhoCod ,
                           int aP1_ContagemResultado_ContratadaCod ,
                           int aP2_ContagemResultado_ServicoGrupo ,
                           int aP3_ContagemResultado_Servico ,
                           int aP4_ContagemResultado_CntadaOsVinc ,
                           int aP5_ContagemResultado_SerGrupoVinc ,
                           int aP6_ContagemResultado_CodSrvVnc ,
                           String aP7_GridStateXML ,
                           out String aP8_Filename ,
                           out String aP9_ErrorMessage )
      {
         this.AV16Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV17ContagemResultado_ContratadaCod = aP1_ContagemResultado_ContratadaCod;
         this.AV18ContagemResultado_ServicoGrupo = aP2_ContagemResultado_ServicoGrupo;
         this.AV19ContagemResultado_Servico = aP3_ContagemResultado_Servico;
         this.AV20ContagemResultado_CntadaOsVinc = aP4_ContagemResultado_CntadaOsVinc;
         this.AV21ContagemResultado_SerGrupoVinc = aP5_ContagemResultado_SerGrupoVinc;
         this.AV22ContagemResultado_CodSrvVnc = aP6_ContagemResultado_CodSrvVnc;
         this.AV62GridStateXML = aP7_GridStateXML;
         this.AV11Filename = "" ;
         this.AV12ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP8_Filename=this.AV11Filename;
         aP9_ErrorMessage=this.AV12ErrorMessage;
      }

      public String executeUdp( int aP0_Contratada_AreaTrabalhoCod ,
                                int aP1_ContagemResultado_ContratadaCod ,
                                int aP2_ContagemResultado_ServicoGrupo ,
                                int aP3_ContagemResultado_Servico ,
                                int aP4_ContagemResultado_CntadaOsVinc ,
                                int aP5_ContagemResultado_SerGrupoVinc ,
                                int aP6_ContagemResultado_CodSrvVnc ,
                                String aP7_GridStateXML ,
                                out String aP8_Filename )
      {
         this.AV16Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV17ContagemResultado_ContratadaCod = aP1_ContagemResultado_ContratadaCod;
         this.AV18ContagemResultado_ServicoGrupo = aP2_ContagemResultado_ServicoGrupo;
         this.AV19ContagemResultado_Servico = aP3_ContagemResultado_Servico;
         this.AV20ContagemResultado_CntadaOsVinc = aP4_ContagemResultado_CntadaOsVinc;
         this.AV21ContagemResultado_SerGrupoVinc = aP5_ContagemResultado_SerGrupoVinc;
         this.AV22ContagemResultado_CodSrvVnc = aP6_ContagemResultado_CodSrvVnc;
         this.AV62GridStateXML = aP7_GridStateXML;
         this.AV11Filename = "" ;
         this.AV12ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP8_Filename=this.AV11Filename;
         aP9_ErrorMessage=this.AV12ErrorMessage;
         return AV12ErrorMessage ;
      }

      public void executeSubmit( int aP0_Contratada_AreaTrabalhoCod ,
                                 int aP1_ContagemResultado_ContratadaCod ,
                                 int aP2_ContagemResultado_ServicoGrupo ,
                                 int aP3_ContagemResultado_Servico ,
                                 int aP4_ContagemResultado_CntadaOsVinc ,
                                 int aP5_ContagemResultado_SerGrupoVinc ,
                                 int aP6_ContagemResultado_CodSrvVnc ,
                                 String aP7_GridStateXML ,
                                 out String aP8_Filename ,
                                 out String aP9_ErrorMessage )
      {
         rel_relatoriocomparacaodemandas objrel_relatoriocomparacaodemandas;
         objrel_relatoriocomparacaodemandas = new rel_relatoriocomparacaodemandas();
         objrel_relatoriocomparacaodemandas.AV16Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         objrel_relatoriocomparacaodemandas.AV17ContagemResultado_ContratadaCod = aP1_ContagemResultado_ContratadaCod;
         objrel_relatoriocomparacaodemandas.AV18ContagemResultado_ServicoGrupo = aP2_ContagemResultado_ServicoGrupo;
         objrel_relatoriocomparacaodemandas.AV19ContagemResultado_Servico = aP3_ContagemResultado_Servico;
         objrel_relatoriocomparacaodemandas.AV20ContagemResultado_CntadaOsVinc = aP4_ContagemResultado_CntadaOsVinc;
         objrel_relatoriocomparacaodemandas.AV21ContagemResultado_SerGrupoVinc = aP5_ContagemResultado_SerGrupoVinc;
         objrel_relatoriocomparacaodemandas.AV22ContagemResultado_CodSrvVnc = aP6_ContagemResultado_CodSrvVnc;
         objrel_relatoriocomparacaodemandas.AV62GridStateXML = aP7_GridStateXML;
         objrel_relatoriocomparacaodemandas.AV11Filename = "" ;
         objrel_relatoriocomparacaodemandas.AV12ErrorMessage = "" ;
         objrel_relatoriocomparacaodemandas.context.SetSubmitInitialConfig(context);
         objrel_relatoriocomparacaodemandas.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objrel_relatoriocomparacaodemandas);
         aP8_Filename=this.AV11Filename;
         aP9_ErrorMessage=this.AV12ErrorMessage;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((rel_relatoriocomparacaodemandas)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'OPENDOCUMENT' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV13CellRow = 1;
         AV14FirstColumn = 1;
         /* Execute user subroutine: 'WRITEMAINTITLE' */
         S131 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'WRITEFILTERS' */
         S141 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'WRITECOLUMNTITLES' */
         S151 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'WRITEDATA' */
         S161 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'CLOSEDOCUMENT' */
         S191 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'OPENDOCUMENT' Routine */
         AV15Random = (int)(NumberUtil.Random( )*10000);
         AV11Filename = "RelatorioComparacaoDemandas-" + StringUtil.Trim( StringUtil.Str( (decimal)(AV15Random), 8, 0)) + ".xlsx";
         AV10ExcelDocument.Open(AV11Filename);
         /* Execute user subroutine: 'CHECKSTATUS' */
         S121 ();
         if (returnInSub) return;
         AV10ExcelDocument.Clear();
      }

      protected void S131( )
      {
         /* 'WRITEMAINTITLE' Routine */
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Relat�rio de Compara��o entre Demandas";
         AV13CellRow = (int)(AV13CellRow+2);
      }

      protected void S141( )
      {
         /* 'WRITEFILTERS' Routine */
         if ( ! ( (0==AV16Contratada_AreaTrabalhoCod) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = "�rea de Trabalho";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV16Contratada_AreaTrabalhoCod;
         }
         if ( ! ( (0==AV17ContagemResultado_ContratadaCod) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = "Contratada";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV17ContagemResultado_ContratadaCod;
         }
         if ( ! ( (0==AV18ContagemResultado_ServicoGrupo) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = "Grupo de Servi�os";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV18ContagemResultado_ServicoGrupo;
         }
         if ( ! ( (0==AV19ContagemResultado_Servico) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = "Servi�o";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV19ContagemResultado_Servico;
         }
         if ( ! ( (0==AV20ContagemResultado_CntadaOsVinc) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = "Contratada";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV20ContagemResultado_CntadaOsVinc;
         }
         if ( ! ( (0==AV21ContagemResultado_SerGrupoVinc) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = "Grupo de Servi�os";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV21ContagemResultado_SerGrupoVinc;
         }
         if ( ! ( (0==AV22ContagemResultado_CodSrvVnc) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = "Servi�o";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV22ContagemResultado_CodSrvVnc;
         }
         AV63GridState.gxTpr_Dynamicfilters.FromXml(AV62GridStateXML, "");
         if ( AV63GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV64GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV63GridState.gxTpr_Dynamicfilters.Item(1));
            AV23DynamicFiltersSelector1 = AV64GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV23DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
            {
               AV24DynamicFiltersOperator1 = AV64GridStateDynamicFilter.gxTpr_Operator;
               AV25ContagemResultado_DemandaFM1 = AV64GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContagemResultado_DemandaFM1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  if ( AV24DynamicFiltersOperator1 == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Igual)";
                  }
                  else if ( AV24DynamicFiltersOperator1 == 1 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Come�a com)";
                  }
                  else if ( AV24DynamicFiltersOperator1 == 2 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Cont�m)";
                  }
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV25ContagemResultado_DemandaFM1;
               }
            }
            else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 )
            {
               AV24DynamicFiltersOperator1 = AV64GridStateDynamicFilter.gxTpr_Operator;
               AV26ContagemResultado_DataDmn1 = context.localUtil.CToD( AV64GridStateDynamicFilter.gxTpr_Value, 2);
               AV27ContagemResultado_DataDmn_To1 = context.localUtil.CToD( AV64GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV26ContagemResultado_DataDmn1) || ! (DateTime.MinValue==AV27ContagemResultado_DataDmn_To1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  if ( AV24DynamicFiltersOperator1 == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data da Demanda (Per�odo)";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "�";
                  }
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV26ContagemResultado_DataDmn1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV27ContagemResultado_DataDmn_To1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
               }
            }
            else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
            {
               AV24DynamicFiltersOperator1 = AV64GridStateDynamicFilter.gxTpr_Operator;
               AV29ContagemResultado_DataEntregaReal1 = context.localUtil.CToT( AV64GridStateDynamicFilter.gxTpr_Value, 2);
               AV30ContagemResultado_DataEntregaReal_To1 = context.localUtil.CToT( AV64GridStateDynamicFilter.gxTpr_Valueto, 2);
               AV28PrintFilterValue = false;
               if ( ! (DateTime.MinValue==AV29ContagemResultado_DataEntregaReal1) || ! (DateTime.MinValue==AV30ContagemResultado_DataEntregaReal_To1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  if ( AV24DynamicFiltersOperator1 == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data de Entrega";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Passado";
                  }
                  else if ( AV24DynamicFiltersOperator1 == 1 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data de Entrega";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Hoje";
                  }
                  else if ( AV24DynamicFiltersOperator1 == 2 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data de Entrega";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "No futuro";
                  }
                  else if ( AV24DynamicFiltersOperator1 == 3 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data de Entrega (Per�odo)";
                     AV28PrintFilterValue = true;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "�";
                  }
                  if ( AV28PrintFilterValue )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = AV29ContagemResultado_DataEntregaReal1;
                     if ( AV28PrintFilterValue )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = AV30ContagemResultado_DataEntregaReal_To1;
                     }
                  }
               }
            }
            else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
            {
               AV31ContagemResultado_StatusDmn1 = AV64GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31ContagemResultado_StatusDmn1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status da Demanda";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31ContagemResultado_StatusDmn1)) )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatusdemanda.getDescription(context,AV31ContagemResultado_StatusDmn1);
                  }
               }
            }
            else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSCNT") == 0 )
            {
               AV32ContagemResultado_StatusCnt1 = (short)(NumberUtil.Val( AV64GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV32ContagemResultado_StatusCnt1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status do Servi�o";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  if ( ! (0==AV32ContagemResultado_StatusCnt1) )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatuscontagem.getDescription(context,AV32ContagemResultado_StatusCnt1);
                  }
               }
            }
            else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
            {
               AV24DynamicFiltersOperator1 = AV64GridStateDynamicFilter.gxTpr_Operator;
               AV33ContagemResultado_Descricao1 = AV64GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33ContagemResultado_Descricao1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  if ( AV24DynamicFiltersOperator1 == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "T�tulo (Come�a com)";
                  }
                  else if ( AV24DynamicFiltersOperator1 == 1 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "T�tulo (Cont�m)";
                  }
                  else if ( AV24DynamicFiltersOperator1 == 2 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "T�tulo (Igual)";
                  }
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV33ContagemResultado_Descricao1;
               }
            }
            else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
            {
               AV24DynamicFiltersOperator1 = AV64GridStateDynamicFilter.gxTpr_Operator;
               AV34ContagemResultado_SistemaCod1 = (int)(NumberUtil.Val( AV64GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV34ContagemResultado_SistemaCod1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  if ( AV24DynamicFiltersOperator1 == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema (<)";
                  }
                  else if ( AV24DynamicFiltersOperator1 == 1 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema (=)";
                  }
                  else if ( AV24DynamicFiltersOperator1 == 2 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema (>)";
                  }
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV34ContagemResultado_SistemaCod1;
               }
            }
            else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
            {
               AV24DynamicFiltersOperator1 = AV64GridStateDynamicFilter.gxTpr_Operator;
               AV35ContagemResultado_Agrupador1 = AV64GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35ContagemResultado_Agrupador1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  if ( AV24DynamicFiltersOperator1 == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Agrupador (Igual)";
                  }
                  else if ( AV24DynamicFiltersOperator1 == 1 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Agrupador (Come�a com)";
                  }
                  else if ( AV24DynamicFiltersOperator1 == 2 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Agrupador (Cont�m)";
                  }
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV35ContagemResultado_Agrupador1;
               }
            }
            if ( AV63GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV36DynamicFiltersEnabled2 = true;
               AV64GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV63GridState.gxTpr_Dynamicfilters.Item(2));
               AV37DynamicFiltersSelector2 = AV64GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
               {
                  AV38DynamicFiltersOperator2 = AV64GridStateDynamicFilter.gxTpr_Operator;
                  AV39ContagemResultado_DemandaFM2 = AV64GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39ContagemResultado_DemandaFM2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     if ( AV38DynamicFiltersOperator2 == 0 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Igual)";
                     }
                     else if ( AV38DynamicFiltersOperator2 == 1 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Come�a com)";
                     }
                     else if ( AV38DynamicFiltersOperator2 == 2 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Cont�m)";
                     }
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV39ContagemResultado_DemandaFM2;
                  }
               }
               else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 )
               {
                  AV38DynamicFiltersOperator2 = AV64GridStateDynamicFilter.gxTpr_Operator;
                  AV40ContagemResultado_DataDmn2 = context.localUtil.CToD( AV64GridStateDynamicFilter.gxTpr_Value, 2);
                  AV41ContagemResultado_DataDmn_To2 = context.localUtil.CToD( AV64GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV40ContagemResultado_DataDmn2) || ! (DateTime.MinValue==AV41ContagemResultado_DataDmn_To2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     if ( AV38DynamicFiltersOperator2 == 0 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data da Demanda (Per�odo)";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "to";
                     }
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV40ContagemResultado_DataDmn2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV41ContagemResultado_DataDmn_To2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                  }
               }
               else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
               {
                  AV38DynamicFiltersOperator2 = AV64GridStateDynamicFilter.gxTpr_Operator;
                  AV42ContagemResultado_DataEntregaReal2 = context.localUtil.CToT( AV64GridStateDynamicFilter.gxTpr_Value, 2);
                  AV43ContagemResultado_DataEntregaReal_To2 = context.localUtil.CToT( AV64GridStateDynamicFilter.gxTpr_Valueto, 2);
                  AV28PrintFilterValue = false;
                  if ( ! (DateTime.MinValue==AV42ContagemResultado_DataEntregaReal2) || ! (DateTime.MinValue==AV43ContagemResultado_DataEntregaReal_To2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     if ( AV38DynamicFiltersOperator2 == 0 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data de Entrega";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Passado";
                     }
                     else if ( AV38DynamicFiltersOperator2 == 1 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data de Entrega";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Hoje";
                     }
                     else if ( AV38DynamicFiltersOperator2 == 2 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data de Entrega";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "No futuro";
                     }
                     else if ( AV38DynamicFiltersOperator2 == 3 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data de Entrega (Per�odo)";
                        AV28PrintFilterValue = true;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "to";
                     }
                     if ( AV28PrintFilterValue )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = AV42ContagemResultado_DataEntregaReal2;
                        if ( AV28PrintFilterValue )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                           AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = AV43ContagemResultado_DataEntregaReal_To2;
                        }
                     }
                  }
               }
               else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
               {
                  AV44ContagemResultado_StatusDmn2 = AV64GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ContagemResultado_StatusDmn2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status da Demanda";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ContagemResultado_StatusDmn2)) )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatusdemanda.getDescription(context,AV44ContagemResultado_StatusDmn2);
                     }
                  }
               }
               else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSCNT") == 0 )
               {
                  AV45ContagemResultado_StatusCnt2 = (short)(NumberUtil.Val( AV64GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV45ContagemResultado_StatusCnt2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status do Servi�o";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     if ( ! (0==AV45ContagemResultado_StatusCnt2) )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatuscontagem.getDescription(context,AV45ContagemResultado_StatusCnt2);
                     }
                  }
               }
               else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
               {
                  AV38DynamicFiltersOperator2 = AV64GridStateDynamicFilter.gxTpr_Operator;
                  AV46ContagemResultado_Descricao2 = AV64GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46ContagemResultado_Descricao2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     if ( AV38DynamicFiltersOperator2 == 0 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "T�tulo (Come�a com)";
                     }
                     else if ( AV38DynamicFiltersOperator2 == 1 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "T�tulo (Cont�m)";
                     }
                     else if ( AV38DynamicFiltersOperator2 == 2 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "T�tulo (Igual)";
                     }
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV46ContagemResultado_Descricao2;
                  }
               }
               else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
               {
                  AV38DynamicFiltersOperator2 = AV64GridStateDynamicFilter.gxTpr_Operator;
                  AV47ContagemResultado_SistemaCod2 = (int)(NumberUtil.Val( AV64GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV47ContagemResultado_SistemaCod2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     if ( AV38DynamicFiltersOperator2 == 0 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema (<)";
                     }
                     else if ( AV38DynamicFiltersOperator2 == 1 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema (=)";
                     }
                     else if ( AV38DynamicFiltersOperator2 == 2 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema (>)";
                     }
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV47ContagemResultado_SistemaCod2;
                  }
               }
               else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
               {
                  AV38DynamicFiltersOperator2 = AV64GridStateDynamicFilter.gxTpr_Operator;
                  AV48ContagemResultado_Agrupador2 = AV64GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ContagemResultado_Agrupador2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     if ( AV38DynamicFiltersOperator2 == 0 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Agrupador (Igual)";
                     }
                     else if ( AV38DynamicFiltersOperator2 == 1 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Agrupador (Come�a com)";
                     }
                     else if ( AV38DynamicFiltersOperator2 == 2 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Agrupador (Cont�m)";
                     }
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV48ContagemResultado_Agrupador2;
                  }
               }
               if ( AV63GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV49DynamicFiltersEnabled3 = true;
                  AV64GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV63GridState.gxTpr_Dynamicfilters.Item(3));
                  AV50DynamicFiltersSelector3 = AV64GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
                  {
                     AV51DynamicFiltersOperator3 = AV64GridStateDynamicFilter.gxTpr_Operator;
                     AV52ContagemResultado_DemandaFM3 = AV64GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52ContagemResultado_DemandaFM3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        if ( AV51DynamicFiltersOperator3 == 0 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Igual)";
                        }
                        else if ( AV51DynamicFiltersOperator3 == 1 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Come�a com)";
                        }
                        else if ( AV51DynamicFiltersOperator3 == 2 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Cont�m)";
                        }
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV52ContagemResultado_DemandaFM3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 )
                  {
                     AV51DynamicFiltersOperator3 = AV64GridStateDynamicFilter.gxTpr_Operator;
                     AV53ContagemResultado_DataDmn3 = context.localUtil.CToD( AV64GridStateDynamicFilter.gxTpr_Value, 2);
                     AV54ContagemResultado_DataDmn_To3 = context.localUtil.CToD( AV64GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV53ContagemResultado_DataDmn3) || ! (DateTime.MinValue==AV54ContagemResultado_DataDmn_To3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        if ( AV51DynamicFiltersOperator3 == 0 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data da Demanda (Per�odo)";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "to";
                        }
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV53ContagemResultado_DataDmn3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV54ContagemResultado_DataDmn_To3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
                  {
                     AV51DynamicFiltersOperator3 = AV64GridStateDynamicFilter.gxTpr_Operator;
                     AV55ContagemResultado_DataEntregaReal3 = context.localUtil.CToT( AV64GridStateDynamicFilter.gxTpr_Value, 2);
                     AV56ContagemResultado_DataEntregaReal_To3 = context.localUtil.CToT( AV64GridStateDynamicFilter.gxTpr_Valueto, 2);
                     AV28PrintFilterValue = false;
                     if ( ! (DateTime.MinValue==AV55ContagemResultado_DataEntregaReal3) || ! (DateTime.MinValue==AV56ContagemResultado_DataEntregaReal_To3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        if ( AV51DynamicFiltersOperator3 == 0 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data de Entrega";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Passado";
                        }
                        else if ( AV51DynamicFiltersOperator3 == 1 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data de Entrega";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Hoje";
                        }
                        else if ( AV51DynamicFiltersOperator3 == 2 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data de Entrega";
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "No futuro";
                        }
                        else if ( AV51DynamicFiltersOperator3 == 3 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data de Entrega (Per�odo)";
                           AV28PrintFilterValue = true;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "to";
                        }
                        if ( AV28PrintFilterValue )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                           AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = AV55ContagemResultado_DataEntregaReal3;
                           if ( AV28PrintFilterValue )
                           {
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                              AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                              AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = AV56ContagemResultado_DataEntregaReal_To3;
                           }
                        }
                     }
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                  {
                     AV57ContagemResultado_StatusDmn3 = AV64GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57ContagemResultado_StatusDmn3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status da Demanda";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57ContagemResultado_StatusDmn3)) )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatusdemanda.getDescription(context,AV57ContagemResultado_StatusDmn3);
                        }
                     }
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSCNT") == 0 )
                  {
                     AV58ContagemResultado_StatusCnt3 = (short)(NumberUtil.Val( AV64GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV58ContagemResultado_StatusCnt3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status do Servi�o";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        if ( ! (0==AV58ContagemResultado_StatusCnt3) )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatuscontagem.getDescription(context,AV58ContagemResultado_StatusCnt3);
                        }
                     }
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                  {
                     AV51DynamicFiltersOperator3 = AV64GridStateDynamicFilter.gxTpr_Operator;
                     AV59ContagemResultado_Descricao3 = AV64GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59ContagemResultado_Descricao3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        if ( AV51DynamicFiltersOperator3 == 0 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "T�tulo (Come�a com)";
                        }
                        else if ( AV51DynamicFiltersOperator3 == 1 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "T�tulo (Cont�m)";
                        }
                        else if ( AV51DynamicFiltersOperator3 == 2 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "T�tulo (Igual)";
                        }
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV59ContagemResultado_Descricao3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                  {
                     AV51DynamicFiltersOperator3 = AV64GridStateDynamicFilter.gxTpr_Operator;
                     AV60ContagemResultado_SistemaCod3 = (int)(NumberUtil.Val( AV64GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV60ContagemResultado_SistemaCod3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        if ( AV51DynamicFiltersOperator3 == 0 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema (<)";
                        }
                        else if ( AV51DynamicFiltersOperator3 == 1 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema (=)";
                        }
                        else if ( AV51DynamicFiltersOperator3 == 2 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema (>)";
                        }
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV60ContagemResultado_SistemaCod3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                  {
                     AV51DynamicFiltersOperator3 = AV64GridStateDynamicFilter.gxTpr_Operator;
                     AV61ContagemResultado_Agrupador3 = AV64GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61ContagemResultado_Agrupador3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        if ( AV51DynamicFiltersOperator3 == 0 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Agrupador (Igual)";
                        }
                        else if ( AV51DynamicFiltersOperator3 == 1 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Agrupador (Come�a com)";
                        }
                        else if ( AV51DynamicFiltersOperator3 == 2 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Agrupador (Cont�m)";
                        }
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV61ContagemResultado_Agrupador3;
                     }
                  }
               }
            }
         }
         AV13CellRow = (int)(AV13CellRow+2);
      }

      protected void S151( )
      {
         /* 'WRITECOLUMNTITLES' Routine */
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = "Resultado_Codigo";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "OS";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "OS Ref.";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Text = "Data Demanda";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+4, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+4, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+4, 1, 1).Text = "Data de Entrega";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+5, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+5, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+5, 1, 1).Text = "T�tulo";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+6, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+6, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+6, 1, 1).Text = "Sigla do Sistema";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+7, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+7, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+7, 1, 1).Text = "C�digo OS Vinculada";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Text = "Tipo de F�brica";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Text = "PFBFM";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+10, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+10, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+10, 1, 1).Text = "PFBFS";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Text = "Esfor�o";
      }

      protected void S161( )
      {
         /* 'WRITEDATA' Routine */
         AV69WP_RelatorioComparacaoDemandasModeloDS_1_Contratada_areatrabalhocod = AV16Contratada_AreaTrabalhoCod;
         AV70WP_RelatorioComparacaoDemandasModeloDS_2_Contagemresultado_contratadacod = AV17ContagemResultado_ContratadaCod;
         AV71WP_RelatorioComparacaoDemandasModeloDS_3_Contagemresultado_servicogrupo = AV18ContagemResultado_ServicoGrupo;
         AV72WP_RelatorioComparacaoDemandasModeloDS_4_Contagemresultado_servico = AV19ContagemResultado_Servico;
         AV73WP_RelatorioComparacaoDemandasModeloDS_5_Contagemresultado_cntadaosvinc = AV20ContagemResultado_CntadaOsVinc;
         AV74WP_RelatorioComparacaoDemandasModeloDS_6_Contagemresultado_sergrupovinc = AV21ContagemResultado_SerGrupoVinc;
         AV75WP_RelatorioComparacaoDemandasModeloDS_7_Contagemresultado_codsrvvnc = AV22ContagemResultado_CodSrvVnc;
         AV76WP_RelatorioComparacaoDemandasModeloDS_8_Dynamicfiltersselector1 = AV23DynamicFiltersSelector1;
         AV77WP_RelatorioComparacaoDemandasModeloDS_9_Dynamicfiltersoperator1 = AV24DynamicFiltersOperator1;
         AV78WP_RelatorioComparacaoDemandasModeloDS_10_Contagemresultado_demandafm1 = AV25ContagemResultado_DemandaFM1;
         AV79WP_RelatorioComparacaoDemandasModeloDS_11_Contagemresultado_datadmn1 = AV26ContagemResultado_DataDmn1;
         AV80WP_RelatorioComparacaoDemandasModeloDS_12_Contagemresultado_datadmn_to1 = AV27ContagemResultado_DataDmn_To1;
         AV81WP_RelatorioComparacaoDemandasModeloDS_13_Contagemresultado_dataentregareal1 = AV29ContagemResultado_DataEntregaReal1;
         AV82WP_RelatorioComparacaoDemandasModeloDS_14_Contagemresultado_dataentregareal_to1 = AV30ContagemResultado_DataEntregaReal_To1;
         AV83WP_RelatorioComparacaoDemandasModeloDS_15_Contagemresultado_statusdmn1 = AV31ContagemResultado_StatusDmn1;
         AV84WP_RelatorioComparacaoDemandasModeloDS_16_Contagemresultado_statuscnt1 = AV32ContagemResultado_StatusCnt1;
         AV85WP_RelatorioComparacaoDemandasModeloDS_17_Contagemresultado_descricao1 = AV33ContagemResultado_Descricao1;
         AV86WP_RelatorioComparacaoDemandasModeloDS_18_Contagemresultado_sistemacod1 = AV34ContagemResultado_SistemaCod1;
         AV87WP_RelatorioComparacaoDemandasModeloDS_19_Contagemresultado_agrupador1 = AV35ContagemResultado_Agrupador1;
         AV88WP_RelatorioComparacaoDemandasModeloDS_20_Dynamicfiltersenabled2 = AV36DynamicFiltersEnabled2;
         AV89WP_RelatorioComparacaoDemandasModeloDS_21_Dynamicfiltersselector2 = AV37DynamicFiltersSelector2;
         AV90WP_RelatorioComparacaoDemandasModeloDS_22_Dynamicfiltersoperator2 = AV38DynamicFiltersOperator2;
         AV91WP_RelatorioComparacaoDemandasModeloDS_23_Contagemresultado_demandafm2 = AV39ContagemResultado_DemandaFM2;
         AV92WP_RelatorioComparacaoDemandasModeloDS_24_Contagemresultado_datadmn2 = AV40ContagemResultado_DataDmn2;
         AV93WP_RelatorioComparacaoDemandasModeloDS_25_Contagemresultado_datadmn_to2 = AV41ContagemResultado_DataDmn_To2;
         AV94WP_RelatorioComparacaoDemandasModeloDS_26_Contagemresultado_dataentregareal2 = AV42ContagemResultado_DataEntregaReal2;
         AV95WP_RelatorioComparacaoDemandasModeloDS_27_Contagemresultado_dataentregareal_to2 = AV43ContagemResultado_DataEntregaReal_To2;
         AV96WP_RelatorioComparacaoDemandasModeloDS_28_Contagemresultado_statusdmn2 = AV44ContagemResultado_StatusDmn2;
         AV97WP_RelatorioComparacaoDemandasModeloDS_29_Contagemresultado_statuscnt2 = AV45ContagemResultado_StatusCnt2;
         AV98WP_RelatorioComparacaoDemandasModeloDS_30_Contagemresultado_descricao2 = AV46ContagemResultado_Descricao2;
         AV99WP_RelatorioComparacaoDemandasModeloDS_31_Contagemresultado_sistemacod2 = AV47ContagemResultado_SistemaCod2;
         AV100WP_RelatorioComparacaoDemandasModeloDS_32_Contagemresultado_agrupador2 = AV48ContagemResultado_Agrupador2;
         AV101WP_RelatorioComparacaoDemandasModeloDS_33_Dynamicfiltersenabled3 = AV49DynamicFiltersEnabled3;
         AV102WP_RelatorioComparacaoDemandasModeloDS_34_Dynamicfiltersselector3 = AV50DynamicFiltersSelector3;
         AV103WP_RelatorioComparacaoDemandasModeloDS_35_Dynamicfiltersoperator3 = AV51DynamicFiltersOperator3;
         AV104WP_RelatorioComparacaoDemandasModeloDS_36_Contagemresultado_demandafm3 = AV52ContagemResultado_DemandaFM3;
         AV105WP_RelatorioComparacaoDemandasModeloDS_37_Contagemresultado_datadmn3 = AV53ContagemResultado_DataDmn3;
         AV106WP_RelatorioComparacaoDemandasModeloDS_38_Contagemresultado_datadmn_to3 = AV54ContagemResultado_DataDmn_To3;
         AV107WP_RelatorioComparacaoDemandasModeloDS_39_Contagemresultado_dataentregareal3 = AV55ContagemResultado_DataEntregaReal3;
         AV108WP_RelatorioComparacaoDemandasModeloDS_40_Contagemresultado_dataentregareal_to3 = AV56ContagemResultado_DataEntregaReal_To3;
         AV109WP_RelatorioComparacaoDemandasModeloDS_41_Contagemresultado_statusdmn3 = AV57ContagemResultado_StatusDmn3;
         AV110WP_RelatorioComparacaoDemandasModeloDS_42_Contagemresultado_statuscnt3 = AV58ContagemResultado_StatusCnt3;
         AV111WP_RelatorioComparacaoDemandasModeloDS_43_Contagemresultado_descricao3 = AV59ContagemResultado_Descricao3;
         AV112WP_RelatorioComparacaoDemandasModeloDS_44_Contagemresultado_sistemacod3 = AV60ContagemResultado_SistemaCod3;
         AV113WP_RelatorioComparacaoDemandasModeloDS_45_Contagemresultado_agrupador3 = AV61ContagemResultado_Agrupador3;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV70WP_RelatorioComparacaoDemandasModeloDS_2_Contagemresultado_contratadacod ,
                                              AV71WP_RelatorioComparacaoDemandasModeloDS_3_Contagemresultado_servicogrupo ,
                                              AV72WP_RelatorioComparacaoDemandasModeloDS_4_Contagemresultado_servico ,
                                              AV73WP_RelatorioComparacaoDemandasModeloDS_5_Contagemresultado_cntadaosvinc ,
                                              AV74WP_RelatorioComparacaoDemandasModeloDS_6_Contagemresultado_sergrupovinc ,
                                              AV75WP_RelatorioComparacaoDemandasModeloDS_7_Contagemresultado_codsrvvnc ,
                                              AV76WP_RelatorioComparacaoDemandasModeloDS_8_Dynamicfiltersselector1 ,
                                              AV77WP_RelatorioComparacaoDemandasModeloDS_9_Dynamicfiltersoperator1 ,
                                              AV78WP_RelatorioComparacaoDemandasModeloDS_10_Contagemresultado_demandafm1 ,
                                              AV79WP_RelatorioComparacaoDemandasModeloDS_11_Contagemresultado_datadmn1 ,
                                              AV80WP_RelatorioComparacaoDemandasModeloDS_12_Contagemresultado_datadmn_to1 ,
                                              AV81WP_RelatorioComparacaoDemandasModeloDS_13_Contagemresultado_dataentregareal1 ,
                                              AV82WP_RelatorioComparacaoDemandasModeloDS_14_Contagemresultado_dataentregareal_to1 ,
                                              AV83WP_RelatorioComparacaoDemandasModeloDS_15_Contagemresultado_statusdmn1 ,
                                              AV84WP_RelatorioComparacaoDemandasModeloDS_16_Contagemresultado_statuscnt1 ,
                                              AV85WP_RelatorioComparacaoDemandasModeloDS_17_Contagemresultado_descricao1 ,
                                              AV86WP_RelatorioComparacaoDemandasModeloDS_18_Contagemresultado_sistemacod1 ,
                                              AV87WP_RelatorioComparacaoDemandasModeloDS_19_Contagemresultado_agrupador1 ,
                                              AV88WP_RelatorioComparacaoDemandasModeloDS_20_Dynamicfiltersenabled2 ,
                                              AV89WP_RelatorioComparacaoDemandasModeloDS_21_Dynamicfiltersselector2 ,
                                              AV90WP_RelatorioComparacaoDemandasModeloDS_22_Dynamicfiltersoperator2 ,
                                              AV91WP_RelatorioComparacaoDemandasModeloDS_23_Contagemresultado_demandafm2 ,
                                              AV92WP_RelatorioComparacaoDemandasModeloDS_24_Contagemresultado_datadmn2 ,
                                              AV93WP_RelatorioComparacaoDemandasModeloDS_25_Contagemresultado_datadmn_to2 ,
                                              AV94WP_RelatorioComparacaoDemandasModeloDS_26_Contagemresultado_dataentregareal2 ,
                                              AV95WP_RelatorioComparacaoDemandasModeloDS_27_Contagemresultado_dataentregareal_to2 ,
                                              AV96WP_RelatorioComparacaoDemandasModeloDS_28_Contagemresultado_statusdmn2 ,
                                              AV97WP_RelatorioComparacaoDemandasModeloDS_29_Contagemresultado_statuscnt2 ,
                                              AV98WP_RelatorioComparacaoDemandasModeloDS_30_Contagemresultado_descricao2 ,
                                              AV99WP_RelatorioComparacaoDemandasModeloDS_31_Contagemresultado_sistemacod2 ,
                                              AV100WP_RelatorioComparacaoDemandasModeloDS_32_Contagemresultado_agrupador2 ,
                                              AV101WP_RelatorioComparacaoDemandasModeloDS_33_Dynamicfiltersenabled3 ,
                                              AV102WP_RelatorioComparacaoDemandasModeloDS_34_Dynamicfiltersselector3 ,
                                              AV103WP_RelatorioComparacaoDemandasModeloDS_35_Dynamicfiltersoperator3 ,
                                              AV104WP_RelatorioComparacaoDemandasModeloDS_36_Contagemresultado_demandafm3 ,
                                              AV105WP_RelatorioComparacaoDemandasModeloDS_37_Contagemresultado_datadmn3 ,
                                              AV106WP_RelatorioComparacaoDemandasModeloDS_38_Contagemresultado_datadmn_to3 ,
                                              AV107WP_RelatorioComparacaoDemandasModeloDS_39_Contagemresultado_dataentregareal3 ,
                                              AV108WP_RelatorioComparacaoDemandasModeloDS_40_Contagemresultado_dataentregareal_to3 ,
                                              AV109WP_RelatorioComparacaoDemandasModeloDS_41_Contagemresultado_statusdmn3 ,
                                              AV110WP_RelatorioComparacaoDemandasModeloDS_42_Contagemresultado_statuscnt3 ,
                                              AV111WP_RelatorioComparacaoDemandasModeloDS_43_Contagemresultado_descricao3 ,
                                              AV112WP_RelatorioComparacaoDemandasModeloDS_44_Contagemresultado_sistemacod3 ,
                                              AV113WP_RelatorioComparacaoDemandasModeloDS_45_Contagemresultado_agrupador3 ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A764ContagemResultado_ServicoGrupo ,
                                              A601ContagemResultado_Servico ,
                                              A2145ContagemResultado_CntadaOsVinc ,
                                              A2146ContagemResultado_SerGrupoVinc ,
                                              A1591ContagemResultado_CodSrvVnc ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A2017ContagemResultado_DataEntregaReal ,
                                              A484ContagemResultado_StatusDmn ,
                                              A483ContagemResultado_StatusCnt ,
                                              A494ContagemResultado_Descricao ,
                                              A489ContagemResultado_SistemaCod ,
                                              A1046ContagemResultado_Agrupador ,
                                              A602ContagemResultado_OSVinculada ,
                                              A456ContagemResultado_Codigo ,
                                              AV17ContagemResultado_ContratadaCod ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV78WP_RelatorioComparacaoDemandasModeloDS_10_Contagemresultado_demandafm1 = StringUtil.Concat( StringUtil.RTrim( AV78WP_RelatorioComparacaoDemandasModeloDS_10_Contagemresultado_demandafm1), "%", "");
         lV78WP_RelatorioComparacaoDemandasModeloDS_10_Contagemresultado_demandafm1 = StringUtil.Concat( StringUtil.RTrim( AV78WP_RelatorioComparacaoDemandasModeloDS_10_Contagemresultado_demandafm1), "%", "");
         lV85WP_RelatorioComparacaoDemandasModeloDS_17_Contagemresultado_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV85WP_RelatorioComparacaoDemandasModeloDS_17_Contagemresultado_descricao1), "%", "");
         lV85WP_RelatorioComparacaoDemandasModeloDS_17_Contagemresultado_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV85WP_RelatorioComparacaoDemandasModeloDS_17_Contagemresultado_descricao1), "%", "");
         lV87WP_RelatorioComparacaoDemandasModeloDS_19_Contagemresultado_agrupador1 = StringUtil.PadR( StringUtil.RTrim( AV87WP_RelatorioComparacaoDemandasModeloDS_19_Contagemresultado_agrupador1), 15, "%");
         lV87WP_RelatorioComparacaoDemandasModeloDS_19_Contagemresultado_agrupador1 = StringUtil.PadR( StringUtil.RTrim( AV87WP_RelatorioComparacaoDemandasModeloDS_19_Contagemresultado_agrupador1), 15, "%");
         lV91WP_RelatorioComparacaoDemandasModeloDS_23_Contagemresultado_demandafm2 = StringUtil.Concat( StringUtil.RTrim( AV91WP_RelatorioComparacaoDemandasModeloDS_23_Contagemresultado_demandafm2), "%", "");
         lV91WP_RelatorioComparacaoDemandasModeloDS_23_Contagemresultado_demandafm2 = StringUtil.Concat( StringUtil.RTrim( AV91WP_RelatorioComparacaoDemandasModeloDS_23_Contagemresultado_demandafm2), "%", "");
         lV98WP_RelatorioComparacaoDemandasModeloDS_30_Contagemresultado_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV98WP_RelatorioComparacaoDemandasModeloDS_30_Contagemresultado_descricao2), "%", "");
         lV98WP_RelatorioComparacaoDemandasModeloDS_30_Contagemresultado_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV98WP_RelatorioComparacaoDemandasModeloDS_30_Contagemresultado_descricao2), "%", "");
         lV100WP_RelatorioComparacaoDemandasModeloDS_32_Contagemresultado_agrupador2 = StringUtil.PadR( StringUtil.RTrim( AV100WP_RelatorioComparacaoDemandasModeloDS_32_Contagemresultado_agrupador2), 15, "%");
         lV100WP_RelatorioComparacaoDemandasModeloDS_32_Contagemresultado_agrupador2 = StringUtil.PadR( StringUtil.RTrim( AV100WP_RelatorioComparacaoDemandasModeloDS_32_Contagemresultado_agrupador2), 15, "%");
         lV104WP_RelatorioComparacaoDemandasModeloDS_36_Contagemresultado_demandafm3 = StringUtil.Concat( StringUtil.RTrim( AV104WP_RelatorioComparacaoDemandasModeloDS_36_Contagemresultado_demandafm3), "%", "");
         lV104WP_RelatorioComparacaoDemandasModeloDS_36_Contagemresultado_demandafm3 = StringUtil.Concat( StringUtil.RTrim( AV104WP_RelatorioComparacaoDemandasModeloDS_36_Contagemresultado_demandafm3), "%", "");
         lV111WP_RelatorioComparacaoDemandasModeloDS_43_Contagemresultado_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV111WP_RelatorioComparacaoDemandasModeloDS_43_Contagemresultado_descricao3), "%", "");
         lV111WP_RelatorioComparacaoDemandasModeloDS_43_Contagemresultado_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV111WP_RelatorioComparacaoDemandasModeloDS_43_Contagemresultado_descricao3), "%", "");
         lV113WP_RelatorioComparacaoDemandasModeloDS_45_Contagemresultado_agrupador3 = StringUtil.PadR( StringUtil.RTrim( AV113WP_RelatorioComparacaoDemandasModeloDS_45_Contagemresultado_agrupador3), 15, "%");
         lV113WP_RelatorioComparacaoDemandasModeloDS_45_Contagemresultado_agrupador3 = StringUtil.PadR( StringUtil.RTrim( AV113WP_RelatorioComparacaoDemandasModeloDS_45_Contagemresultado_agrupador3), 15, "%");
         /* Using cursor P00YO3 */
         pr_default.execute(0, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, AV70WP_RelatorioComparacaoDemandasModeloDS_2_Contagemresultado_contratadacod, AV71WP_RelatorioComparacaoDemandasModeloDS_3_Contagemresultado_servicogrupo, AV72WP_RelatorioComparacaoDemandasModeloDS_4_Contagemresultado_servico, AV73WP_RelatorioComparacaoDemandasModeloDS_5_Contagemresultado_cntadaosvinc, AV74WP_RelatorioComparacaoDemandasModeloDS_6_Contagemresultado_sergrupovinc, AV75WP_RelatorioComparacaoDemandasModeloDS_7_Contagemresultado_codsrvvnc, AV78WP_RelatorioComparacaoDemandasModeloDS_10_Contagemresultado_demandafm1, lV78WP_RelatorioComparacaoDemandasModeloDS_10_Contagemresultado_demandafm1, lV78WP_RelatorioComparacaoDemandasModeloDS_10_Contagemresultado_demandafm1, AV79WP_RelatorioComparacaoDemandasModeloDS_11_Contagemresultado_datadmn1, AV80WP_RelatorioComparacaoDemandasModeloDS_12_Contagemresultado_datadmn_to1, AV81WP_RelatorioComparacaoDemandasModeloDS_13_Contagemresultado_dataentregareal1, AV82WP_RelatorioComparacaoDemandasModeloDS_14_Contagemresultado_dataentregareal_to1, AV83WP_RelatorioComparacaoDemandasModeloDS_15_Contagemresultado_statusdmn1, AV84WP_RelatorioComparacaoDemandasModeloDS_16_Contagemresultado_statuscnt1, lV85WP_RelatorioComparacaoDemandasModeloDS_17_Contagemresultado_descricao1, lV85WP_RelatorioComparacaoDemandasModeloDS_17_Contagemresultado_descricao1, AV85WP_RelatorioComparacaoDemandasModeloDS_17_Contagemresultado_descricao1, AV86WP_RelatorioComparacaoDemandasModeloDS_18_Contagemresultado_sistemacod1, AV87WP_RelatorioComparacaoDemandasModeloDS_19_Contagemresultado_agrupador1, lV87WP_RelatorioComparacaoDemandasModeloDS_19_Contagemresultado_agrupador1, lV87WP_RelatorioComparacaoDemandasModeloDS_19_Contagemresultado_agrupador1, AV91WP_RelatorioComparacaoDemandasModeloDS_23_Contagemresultado_demandafm2, lV91WP_RelatorioComparacaoDemandasModeloDS_23_Contagemresultado_demandafm2, lV91WP_RelatorioComparacaoDemandasModeloDS_23_Contagemresultado_demandafm2, AV92WP_RelatorioComparacaoDemandasModeloDS_24_Contagemresultado_datadmn2, AV93WP_RelatorioComparacaoDemandasModeloDS_25_Contagemresultado_datadmn_to2, AV94WP_RelatorioComparacaoDemandasModeloDS_26_Contagemresultado_dataentregareal2, AV95WP_RelatorioComparacaoDemandasModeloDS_27_Contagemresultado_dataentregareal_to2, AV96WP_RelatorioComparacaoDemandasModeloDS_28_Contagemresultado_statusdmn2, AV97WP_RelatorioComparacaoDemandasModeloDS_29_Contagemresultado_statuscnt2, lV98WP_RelatorioComparacaoDemandasModeloDS_30_Contagemresultado_descricao2, lV98WP_RelatorioComparacaoDemandasModeloDS_30_Contagemresultado_descricao2, AV98WP_RelatorioComparacaoDemandasModeloDS_30_Contagemresultado_descricao2, AV99WP_RelatorioComparacaoDemandasModeloDS_31_Contagemresultado_sistemacod2, AV100WP_RelatorioComparacaoDemandasModeloDS_32_Contagemresultado_agrupador2, lV100WP_RelatorioComparacaoDemandasModeloDS_32_Contagemresultado_agrupador2, lV100WP_RelatorioComparacaoDemandasModeloDS_32_Contagemresultado_agrupador2, AV104WP_RelatorioComparacaoDemandasModeloDS_36_Contagemresultado_demandafm3, lV104WP_RelatorioComparacaoDemandasModeloDS_36_Contagemresultado_demandafm3, lV104WP_RelatorioComparacaoDemandasModeloDS_36_Contagemresultado_demandafm3, AV105WP_RelatorioComparacaoDemandasModeloDS_37_Contagemresultado_datadmn3, AV106WP_RelatorioComparacaoDemandasModeloDS_38_Contagemresultado_datadmn_to3, AV107WP_RelatorioComparacaoDemandasModeloDS_39_Contagemresultado_dataentregareal3, AV108WP_RelatorioComparacaoDemandasModeloDS_40_Contagemresultado_dataentregareal_to3, AV109WP_RelatorioComparacaoDemandasModeloDS_41_Contagemresultado_statusdmn3, AV110WP_RelatorioComparacaoDemandasModeloDS_42_Contagemresultado_statuscnt3, lV111WP_RelatorioComparacaoDemandasModeloDS_43_Contagemresultado_descricao3, lV111WP_RelatorioComparacaoDemandasModeloDS_43_Contagemresultado_descricao3, AV111WP_RelatorioComparacaoDemandasModeloDS_43_Contagemresultado_descricao3, AV112WP_RelatorioComparacaoDemandasModeloDS_44_Contagemresultado_sistemacod3, AV113WP_RelatorioComparacaoDemandasModeloDS_45_Contagemresultado_agrupador3, lV113WP_RelatorioComparacaoDemandasModeloDS_45_Contagemresultado_agrupador3, lV113WP_RelatorioComparacaoDemandasModeloDS_45_Contagemresultado_agrupador3});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P00YO3_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00YO3_n1553ContagemResultado_CntSrvCod[0];
            A1627ContagemResultado_CntSrvVncCod = P00YO3_A1627ContagemResultado_CntSrvVncCod[0];
            n1627ContagemResultado_CntSrvVncCod = P00YO3_n1627ContagemResultado_CntSrvVncCod[0];
            A52Contratada_AreaTrabalhoCod = P00YO3_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00YO3_n52Contratada_AreaTrabalhoCod[0];
            A456ContagemResultado_Codigo = P00YO3_A456ContagemResultado_Codigo[0];
            A602ContagemResultado_OSVinculada = P00YO3_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00YO3_n602ContagemResultado_OSVinculada[0];
            A1046ContagemResultado_Agrupador = P00YO3_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P00YO3_n1046ContagemResultado_Agrupador[0];
            A489ContagemResultado_SistemaCod = P00YO3_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00YO3_n489ContagemResultado_SistemaCod[0];
            A494ContagemResultado_Descricao = P00YO3_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00YO3_n494ContagemResultado_Descricao[0];
            A483ContagemResultado_StatusCnt = P00YO3_A483ContagemResultado_StatusCnt[0];
            A484ContagemResultado_StatusDmn = P00YO3_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00YO3_n484ContagemResultado_StatusDmn[0];
            A2017ContagemResultado_DataEntregaReal = P00YO3_A2017ContagemResultado_DataEntregaReal[0];
            n2017ContagemResultado_DataEntregaReal = P00YO3_n2017ContagemResultado_DataEntregaReal[0];
            A471ContagemResultado_DataDmn = P00YO3_A471ContagemResultado_DataDmn[0];
            A493ContagemResultado_DemandaFM = P00YO3_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00YO3_n493ContagemResultado_DemandaFM[0];
            A1591ContagemResultado_CodSrvVnc = P00YO3_A1591ContagemResultado_CodSrvVnc[0];
            n1591ContagemResultado_CodSrvVnc = P00YO3_n1591ContagemResultado_CodSrvVnc[0];
            A2146ContagemResultado_SerGrupoVinc = P00YO3_A2146ContagemResultado_SerGrupoVinc[0];
            n2146ContagemResultado_SerGrupoVinc = P00YO3_n2146ContagemResultado_SerGrupoVinc[0];
            A2145ContagemResultado_CntadaOsVinc = P00YO3_A2145ContagemResultado_CntadaOsVinc[0];
            n2145ContagemResultado_CntadaOsVinc = P00YO3_n2145ContagemResultado_CntadaOsVinc[0];
            A601ContagemResultado_Servico = P00YO3_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00YO3_n601ContagemResultado_Servico[0];
            A764ContagemResultado_ServicoGrupo = P00YO3_A764ContagemResultado_ServicoGrupo[0];
            n764ContagemResultado_ServicoGrupo = P00YO3_n764ContagemResultado_ServicoGrupo[0];
            A490ContagemResultado_ContratadaCod = P00YO3_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00YO3_n490ContagemResultado_ContratadaCod[0];
            A509ContagemrResultado_SistemaSigla = P00YO3_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00YO3_n509ContagemrResultado_SistemaSigla[0];
            A1326ContagemResultado_ContratadaTipoFab = P00YO3_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = P00YO3_n1326ContagemResultado_ContratadaTipoFab[0];
            A457ContagemResultado_Demanda = P00YO3_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00YO3_n457ContagemResultado_Demanda[0];
            A682ContagemResultado_PFBFMUltima = P00YO3_A682ContagemResultado_PFBFMUltima[0];
            A684ContagemResultado_PFBFSUltima = P00YO3_A684ContagemResultado_PFBFSUltima[0];
            A473ContagemResultado_DataCnt = P00YO3_A473ContagemResultado_DataCnt[0];
            A511ContagemResultado_HoraCnt = P00YO3_A511ContagemResultado_HoraCnt[0];
            A1553ContagemResultado_CntSrvCod = P00YO3_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00YO3_n1553ContagemResultado_CntSrvCod[0];
            A602ContagemResultado_OSVinculada = P00YO3_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P00YO3_n602ContagemResultado_OSVinculada[0];
            A1046ContagemResultado_Agrupador = P00YO3_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P00YO3_n1046ContagemResultado_Agrupador[0];
            A489ContagemResultado_SistemaCod = P00YO3_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00YO3_n489ContagemResultado_SistemaCod[0];
            A494ContagemResultado_Descricao = P00YO3_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00YO3_n494ContagemResultado_Descricao[0];
            A484ContagemResultado_StatusDmn = P00YO3_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00YO3_n484ContagemResultado_StatusDmn[0];
            A2017ContagemResultado_DataEntregaReal = P00YO3_A2017ContagemResultado_DataEntregaReal[0];
            n2017ContagemResultado_DataEntregaReal = P00YO3_n2017ContagemResultado_DataEntregaReal[0];
            A471ContagemResultado_DataDmn = P00YO3_A471ContagemResultado_DataDmn[0];
            A493ContagemResultado_DemandaFM = P00YO3_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00YO3_n493ContagemResultado_DemandaFM[0];
            A490ContagemResultado_ContratadaCod = P00YO3_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00YO3_n490ContagemResultado_ContratadaCod[0];
            A457ContagemResultado_Demanda = P00YO3_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00YO3_n457ContagemResultado_Demanda[0];
            A601ContagemResultado_Servico = P00YO3_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00YO3_n601ContagemResultado_Servico[0];
            A764ContagemResultado_ServicoGrupo = P00YO3_A764ContagemResultado_ServicoGrupo[0];
            n764ContagemResultado_ServicoGrupo = P00YO3_n764ContagemResultado_ServicoGrupo[0];
            A1627ContagemResultado_CntSrvVncCod = P00YO3_A1627ContagemResultado_CntSrvVncCod[0];
            n1627ContagemResultado_CntSrvVncCod = P00YO3_n1627ContagemResultado_CntSrvVncCod[0];
            A2145ContagemResultado_CntadaOsVinc = P00YO3_A2145ContagemResultado_CntadaOsVinc[0];
            n2145ContagemResultado_CntadaOsVinc = P00YO3_n2145ContagemResultado_CntadaOsVinc[0];
            A1591ContagemResultado_CodSrvVnc = P00YO3_A1591ContagemResultado_CodSrvVnc[0];
            n1591ContagemResultado_CodSrvVnc = P00YO3_n1591ContagemResultado_CodSrvVnc[0];
            A2146ContagemResultado_SerGrupoVinc = P00YO3_A2146ContagemResultado_SerGrupoVinc[0];
            n2146ContagemResultado_SerGrupoVinc = P00YO3_n2146ContagemResultado_SerGrupoVinc[0];
            A509ContagemrResultado_SistemaSigla = P00YO3_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00YO3_n509ContagemrResultado_SistemaSigla[0];
            A52Contratada_AreaTrabalhoCod = P00YO3_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00YO3_n52Contratada_AreaTrabalhoCod[0];
            A1326ContagemResultado_ContratadaTipoFab = P00YO3_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = P00YO3_n1326ContagemResultado_ContratadaTipoFab[0];
            A682ContagemResultado_PFBFMUltima = P00YO3_A682ContagemResultado_PFBFMUltima[0];
            A684ContagemResultado_PFBFSUltima = P00YO3_A684ContagemResultado_PFBFSUltima[0];
            if ( ( A602ContagemResultado_OSVinculada > 0 ) || new prc_verificademandaviculada(context).executeUdp(  A456ContagemResultado_Codigo,  AV17ContagemResultado_ContratadaCod) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               /* Execute user subroutine: 'BEFOREWRITELINE' */
               S172 ();
               if ( returnInSub )
               {
                  pr_default.close(0);
                  returnInSub = true;
                  if (true) return;
               }
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Number = A456ContagemResultado_Codigo;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = A493ContagemResultado_DemandaFM;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = A457ContagemResultado_Demanda;
               GXt_dtime1 = DateTimeUtil.ResetTime( A471ContagemResultado_DataDmn ) ;
               AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
               AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+4, 1, 1).Date = A2017ContagemResultado_DataEntregaReal;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+5, 1, 1).Text = A494ContagemResultado_Descricao;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+6, 1, 1).Text = A509ContagemrResultado_SistemaSigla;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+7, 1, 1).Number = A602ContagemResultado_OSVinculada;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1326ContagemResultado_ContratadaTipoFab)) )
               {
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Text = gxdomaintipofabrica.getDescription(context,A1326ContagemResultado_ContratadaTipoFab);
               }
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Number = (double)(A682ContagemResultado_PFBFMUltima);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+10, 1, 1).Number = (double)(A684ContagemResultado_PFBFSUltima);
               AV65Esforco = ((StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "M")==0) ? A682ContagemResultado_PFBFMUltima : ((StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "S")==0) ? A684ContagemResultado_PFBFSUltima : (decimal)(0)));
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Number = (double)(AV65Esforco);
               /* Execute user subroutine: 'AFTERWRITELINE' */
               S182 ();
               if ( returnInSub )
               {
                  pr_default.close(0);
                  returnInSub = true;
                  if (true) return;
               }
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void S191( )
      {
         /* 'CLOSEDOCUMENT' Routine */
         AV10ExcelDocument.Save();
         /* Execute user subroutine: 'CHECKSTATUS' */
         S121 ();
         if (returnInSub) return;
         AV10ExcelDocument.Close();
      }

      protected void S121( )
      {
         /* 'CHECKSTATUS' Routine */
         if ( AV10ExcelDocument.ErrCode != 0 )
         {
            AV11Filename = "";
            AV12ErrorMessage = AV10ExcelDocument.ErrDescription;
            AV10ExcelDocument.Close();
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S172( )
      {
         /* 'BEFOREWRITELINE' Routine */
      }

      protected void S182( )
      {
         /* 'AFTERWRITELINE' Routine */
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10ExcelDocument = new ExcelDocumentI();
         AV63GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV64GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV23DynamicFiltersSelector1 = "";
         AV25ContagemResultado_DemandaFM1 = "";
         AV26ContagemResultado_DataDmn1 = DateTime.MinValue;
         AV27ContagemResultado_DataDmn_To1 = DateTime.MinValue;
         AV29ContagemResultado_DataEntregaReal1 = (DateTime)(DateTime.MinValue);
         AV30ContagemResultado_DataEntregaReal_To1 = (DateTime)(DateTime.MinValue);
         AV31ContagemResultado_StatusDmn1 = "";
         AV33ContagemResultado_Descricao1 = "";
         AV35ContagemResultado_Agrupador1 = "";
         AV37DynamicFiltersSelector2 = "";
         AV39ContagemResultado_DemandaFM2 = "";
         AV40ContagemResultado_DataDmn2 = DateTime.MinValue;
         AV41ContagemResultado_DataDmn_To2 = DateTime.MinValue;
         AV42ContagemResultado_DataEntregaReal2 = (DateTime)(DateTime.MinValue);
         AV43ContagemResultado_DataEntregaReal_To2 = (DateTime)(DateTime.MinValue);
         AV44ContagemResultado_StatusDmn2 = "";
         AV46ContagemResultado_Descricao2 = "";
         AV48ContagemResultado_Agrupador2 = "";
         AV50DynamicFiltersSelector3 = "";
         AV52ContagemResultado_DemandaFM3 = "";
         AV53ContagemResultado_DataDmn3 = DateTime.MinValue;
         AV54ContagemResultado_DataDmn_To3 = DateTime.MinValue;
         AV55ContagemResultado_DataEntregaReal3 = (DateTime)(DateTime.MinValue);
         AV56ContagemResultado_DataEntregaReal_To3 = (DateTime)(DateTime.MinValue);
         AV57ContagemResultado_StatusDmn3 = "";
         AV59ContagemResultado_Descricao3 = "";
         AV61ContagemResultado_Agrupador3 = "";
         AV76WP_RelatorioComparacaoDemandasModeloDS_8_Dynamicfiltersselector1 = "";
         AV78WP_RelatorioComparacaoDemandasModeloDS_10_Contagemresultado_demandafm1 = "";
         AV79WP_RelatorioComparacaoDemandasModeloDS_11_Contagemresultado_datadmn1 = DateTime.MinValue;
         AV80WP_RelatorioComparacaoDemandasModeloDS_12_Contagemresultado_datadmn_to1 = DateTime.MinValue;
         AV81WP_RelatorioComparacaoDemandasModeloDS_13_Contagemresultado_dataentregareal1 = (DateTime)(DateTime.MinValue);
         AV82WP_RelatorioComparacaoDemandasModeloDS_14_Contagemresultado_dataentregareal_to1 = (DateTime)(DateTime.MinValue);
         AV83WP_RelatorioComparacaoDemandasModeloDS_15_Contagemresultado_statusdmn1 = "";
         AV85WP_RelatorioComparacaoDemandasModeloDS_17_Contagemresultado_descricao1 = "";
         AV87WP_RelatorioComparacaoDemandasModeloDS_19_Contagemresultado_agrupador1 = "";
         AV89WP_RelatorioComparacaoDemandasModeloDS_21_Dynamicfiltersselector2 = "";
         AV91WP_RelatorioComparacaoDemandasModeloDS_23_Contagemresultado_demandafm2 = "";
         AV92WP_RelatorioComparacaoDemandasModeloDS_24_Contagemresultado_datadmn2 = DateTime.MinValue;
         AV93WP_RelatorioComparacaoDemandasModeloDS_25_Contagemresultado_datadmn_to2 = DateTime.MinValue;
         AV94WP_RelatorioComparacaoDemandasModeloDS_26_Contagemresultado_dataentregareal2 = (DateTime)(DateTime.MinValue);
         AV95WP_RelatorioComparacaoDemandasModeloDS_27_Contagemresultado_dataentregareal_to2 = (DateTime)(DateTime.MinValue);
         AV96WP_RelatorioComparacaoDemandasModeloDS_28_Contagemresultado_statusdmn2 = "";
         AV98WP_RelatorioComparacaoDemandasModeloDS_30_Contagemresultado_descricao2 = "";
         AV100WP_RelatorioComparacaoDemandasModeloDS_32_Contagemresultado_agrupador2 = "";
         AV102WP_RelatorioComparacaoDemandasModeloDS_34_Dynamicfiltersselector3 = "";
         AV104WP_RelatorioComparacaoDemandasModeloDS_36_Contagemresultado_demandafm3 = "";
         AV105WP_RelatorioComparacaoDemandasModeloDS_37_Contagemresultado_datadmn3 = DateTime.MinValue;
         AV106WP_RelatorioComparacaoDemandasModeloDS_38_Contagemresultado_datadmn_to3 = DateTime.MinValue;
         AV107WP_RelatorioComparacaoDemandasModeloDS_39_Contagemresultado_dataentregareal3 = (DateTime)(DateTime.MinValue);
         AV108WP_RelatorioComparacaoDemandasModeloDS_40_Contagemresultado_dataentregareal_to3 = (DateTime)(DateTime.MinValue);
         AV109WP_RelatorioComparacaoDemandasModeloDS_41_Contagemresultado_statusdmn3 = "";
         AV111WP_RelatorioComparacaoDemandasModeloDS_43_Contagemresultado_descricao3 = "";
         AV113WP_RelatorioComparacaoDemandasModeloDS_45_Contagemresultado_agrupador3 = "";
         scmdbuf = "";
         lV78WP_RelatorioComparacaoDemandasModeloDS_10_Contagemresultado_demandafm1 = "";
         lV85WP_RelatorioComparacaoDemandasModeloDS_17_Contagemresultado_descricao1 = "";
         lV87WP_RelatorioComparacaoDemandasModeloDS_19_Contagemresultado_agrupador1 = "";
         lV91WP_RelatorioComparacaoDemandasModeloDS_23_Contagemresultado_demandafm2 = "";
         lV98WP_RelatorioComparacaoDemandasModeloDS_30_Contagemresultado_descricao2 = "";
         lV100WP_RelatorioComparacaoDemandasModeloDS_32_Contagemresultado_agrupador2 = "";
         lV104WP_RelatorioComparacaoDemandasModeloDS_36_Contagemresultado_demandafm3 = "";
         lV111WP_RelatorioComparacaoDemandasModeloDS_43_Contagemresultado_descricao3 = "";
         lV113WP_RelatorioComparacaoDemandasModeloDS_45_Contagemresultado_agrupador3 = "";
         A493ContagemResultado_DemandaFM = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A2017ContagemResultado_DataEntregaReal = (DateTime)(DateTime.MinValue);
         A484ContagemResultado_StatusDmn = "";
         A494ContagemResultado_Descricao = "";
         A1046ContagemResultado_Agrupador = "";
         P00YO3_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00YO3_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00YO3_A1627ContagemResultado_CntSrvVncCod = new int[1] ;
         P00YO3_n1627ContagemResultado_CntSrvVncCod = new bool[] {false} ;
         P00YO3_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00YO3_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00YO3_A456ContagemResultado_Codigo = new int[1] ;
         P00YO3_A602ContagemResultado_OSVinculada = new int[1] ;
         P00YO3_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P00YO3_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P00YO3_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P00YO3_A489ContagemResultado_SistemaCod = new int[1] ;
         P00YO3_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00YO3_A494ContagemResultado_Descricao = new String[] {""} ;
         P00YO3_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00YO3_A483ContagemResultado_StatusCnt = new short[1] ;
         P00YO3_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00YO3_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00YO3_A2017ContagemResultado_DataEntregaReal = new DateTime[] {DateTime.MinValue} ;
         P00YO3_n2017ContagemResultado_DataEntregaReal = new bool[] {false} ;
         P00YO3_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00YO3_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00YO3_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00YO3_A1591ContagemResultado_CodSrvVnc = new int[1] ;
         P00YO3_n1591ContagemResultado_CodSrvVnc = new bool[] {false} ;
         P00YO3_A2146ContagemResultado_SerGrupoVinc = new int[1] ;
         P00YO3_n2146ContagemResultado_SerGrupoVinc = new bool[] {false} ;
         P00YO3_A2145ContagemResultado_CntadaOsVinc = new int[1] ;
         P00YO3_n2145ContagemResultado_CntadaOsVinc = new bool[] {false} ;
         P00YO3_A601ContagemResultado_Servico = new int[1] ;
         P00YO3_n601ContagemResultado_Servico = new bool[] {false} ;
         P00YO3_A764ContagemResultado_ServicoGrupo = new int[1] ;
         P00YO3_n764ContagemResultado_ServicoGrupo = new bool[] {false} ;
         P00YO3_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00YO3_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00YO3_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00YO3_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00YO3_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         P00YO3_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         P00YO3_A457ContagemResultado_Demanda = new String[] {""} ;
         P00YO3_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00YO3_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         P00YO3_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         P00YO3_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P00YO3_A511ContagemResultado_HoraCnt = new String[] {""} ;
         A509ContagemrResultado_SistemaSigla = "";
         A1326ContagemResultado_ContratadaTipoFab = "";
         A457ContagemResultado_Demanda = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.rel_relatoriocomparacaodemandas__default(),
            new Object[][] {
                new Object[] {
               P00YO3_A1553ContagemResultado_CntSrvCod, P00YO3_n1553ContagemResultado_CntSrvCod, P00YO3_A1627ContagemResultado_CntSrvVncCod, P00YO3_n1627ContagemResultado_CntSrvVncCod, P00YO3_A52Contratada_AreaTrabalhoCod, P00YO3_n52Contratada_AreaTrabalhoCod, P00YO3_A456ContagemResultado_Codigo, P00YO3_A602ContagemResultado_OSVinculada, P00YO3_n602ContagemResultado_OSVinculada, P00YO3_A1046ContagemResultado_Agrupador,
               P00YO3_n1046ContagemResultado_Agrupador, P00YO3_A489ContagemResultado_SistemaCod, P00YO3_n489ContagemResultado_SistemaCod, P00YO3_A494ContagemResultado_Descricao, P00YO3_n494ContagemResultado_Descricao, P00YO3_A483ContagemResultado_StatusCnt, P00YO3_A484ContagemResultado_StatusDmn, P00YO3_n484ContagemResultado_StatusDmn, P00YO3_A2017ContagemResultado_DataEntregaReal, P00YO3_n2017ContagemResultado_DataEntregaReal,
               P00YO3_A471ContagemResultado_DataDmn, P00YO3_A493ContagemResultado_DemandaFM, P00YO3_n493ContagemResultado_DemandaFM, P00YO3_A1591ContagemResultado_CodSrvVnc, P00YO3_n1591ContagemResultado_CodSrvVnc, P00YO3_A2146ContagemResultado_SerGrupoVinc, P00YO3_n2146ContagemResultado_SerGrupoVinc, P00YO3_A2145ContagemResultado_CntadaOsVinc, P00YO3_n2145ContagemResultado_CntadaOsVinc, P00YO3_A601ContagemResultado_Servico,
               P00YO3_n601ContagemResultado_Servico, P00YO3_A764ContagemResultado_ServicoGrupo, P00YO3_n764ContagemResultado_ServicoGrupo, P00YO3_A490ContagemResultado_ContratadaCod, P00YO3_n490ContagemResultado_ContratadaCod, P00YO3_A509ContagemrResultado_SistemaSigla, P00YO3_n509ContagemrResultado_SistemaSigla, P00YO3_A1326ContagemResultado_ContratadaTipoFab, P00YO3_n1326ContagemResultado_ContratadaTipoFab, P00YO3_A457ContagemResultado_Demanda,
               P00YO3_n457ContagemResultado_Demanda, P00YO3_A682ContagemResultado_PFBFMUltima, P00YO3_A684ContagemResultado_PFBFSUltima, P00YO3_A473ContagemResultado_DataCnt, P00YO3_A511ContagemResultado_HoraCnt
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV24DynamicFiltersOperator1 ;
      private short AV32ContagemResultado_StatusCnt1 ;
      private short AV38DynamicFiltersOperator2 ;
      private short AV45ContagemResultado_StatusCnt2 ;
      private short AV51DynamicFiltersOperator3 ;
      private short AV58ContagemResultado_StatusCnt3 ;
      private short AV77WP_RelatorioComparacaoDemandasModeloDS_9_Dynamicfiltersoperator1 ;
      private short AV84WP_RelatorioComparacaoDemandasModeloDS_16_Contagemresultado_statuscnt1 ;
      private short AV90WP_RelatorioComparacaoDemandasModeloDS_22_Dynamicfiltersoperator2 ;
      private short AV97WP_RelatorioComparacaoDemandasModeloDS_29_Contagemresultado_statuscnt2 ;
      private short AV103WP_RelatorioComparacaoDemandasModeloDS_35_Dynamicfiltersoperator3 ;
      private short AV110WP_RelatorioComparacaoDemandasModeloDS_42_Contagemresultado_statuscnt3 ;
      private short A483ContagemResultado_StatusCnt ;
      private int AV16Contratada_AreaTrabalhoCod ;
      private int AV17ContagemResultado_ContratadaCod ;
      private int AV18ContagemResultado_ServicoGrupo ;
      private int AV19ContagemResultado_Servico ;
      private int AV20ContagemResultado_CntadaOsVinc ;
      private int AV21ContagemResultado_SerGrupoVinc ;
      private int AV22ContagemResultado_CodSrvVnc ;
      private int AV13CellRow ;
      private int AV14FirstColumn ;
      private int AV15Random ;
      private int AV34ContagemResultado_SistemaCod1 ;
      private int AV47ContagemResultado_SistemaCod2 ;
      private int AV60ContagemResultado_SistemaCod3 ;
      private int AV69WP_RelatorioComparacaoDemandasModeloDS_1_Contratada_areatrabalhocod ;
      private int AV70WP_RelatorioComparacaoDemandasModeloDS_2_Contagemresultado_contratadacod ;
      private int AV71WP_RelatorioComparacaoDemandasModeloDS_3_Contagemresultado_servicogrupo ;
      private int AV72WP_RelatorioComparacaoDemandasModeloDS_4_Contagemresultado_servico ;
      private int AV73WP_RelatorioComparacaoDemandasModeloDS_5_Contagemresultado_cntadaosvinc ;
      private int AV74WP_RelatorioComparacaoDemandasModeloDS_6_Contagemresultado_sergrupovinc ;
      private int AV75WP_RelatorioComparacaoDemandasModeloDS_7_Contagemresultado_codsrvvnc ;
      private int AV86WP_RelatorioComparacaoDemandasModeloDS_18_Contagemresultado_sistemacod1 ;
      private int AV99WP_RelatorioComparacaoDemandasModeloDS_31_Contagemresultado_sistemacod2 ;
      private int AV112WP_RelatorioComparacaoDemandasModeloDS_44_Contagemresultado_sistemacod3 ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A764ContagemResultado_ServicoGrupo ;
      private int A601ContagemResultado_Servico ;
      private int A2145ContagemResultado_CntadaOsVinc ;
      private int A2146ContagemResultado_SerGrupoVinc ;
      private int A1591ContagemResultado_CodSrvVnc ;
      private int A489ContagemResultado_SistemaCod ;
      private int A602ContagemResultado_OSVinculada ;
      private int A456ContagemResultado_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1627ContagemResultado_CntSrvVncCod ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal AV65Esforco ;
      private String AV31ContagemResultado_StatusDmn1 ;
      private String AV35ContagemResultado_Agrupador1 ;
      private String AV44ContagemResultado_StatusDmn2 ;
      private String AV48ContagemResultado_Agrupador2 ;
      private String AV57ContagemResultado_StatusDmn3 ;
      private String AV61ContagemResultado_Agrupador3 ;
      private String AV83WP_RelatorioComparacaoDemandasModeloDS_15_Contagemresultado_statusdmn1 ;
      private String AV87WP_RelatorioComparacaoDemandasModeloDS_19_Contagemresultado_agrupador1 ;
      private String AV96WP_RelatorioComparacaoDemandasModeloDS_28_Contagemresultado_statusdmn2 ;
      private String AV100WP_RelatorioComparacaoDemandasModeloDS_32_Contagemresultado_agrupador2 ;
      private String AV109WP_RelatorioComparacaoDemandasModeloDS_41_Contagemresultado_statusdmn3 ;
      private String AV113WP_RelatorioComparacaoDemandasModeloDS_45_Contagemresultado_agrupador3 ;
      private String scmdbuf ;
      private String lV87WP_RelatorioComparacaoDemandasModeloDS_19_Contagemresultado_agrupador1 ;
      private String lV100WP_RelatorioComparacaoDemandasModeloDS_32_Contagemresultado_agrupador2 ;
      private String lV113WP_RelatorioComparacaoDemandasModeloDS_45_Contagemresultado_agrupador3 ;
      private String A484ContagemResultado_StatusDmn ;
      private String A1046ContagemResultado_Agrupador ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String A1326ContagemResultado_ContratadaTipoFab ;
      private String A511ContagemResultado_HoraCnt ;
      private DateTime AV29ContagemResultado_DataEntregaReal1 ;
      private DateTime AV30ContagemResultado_DataEntregaReal_To1 ;
      private DateTime AV42ContagemResultado_DataEntregaReal2 ;
      private DateTime AV43ContagemResultado_DataEntregaReal_To2 ;
      private DateTime AV55ContagemResultado_DataEntregaReal3 ;
      private DateTime AV56ContagemResultado_DataEntregaReal_To3 ;
      private DateTime AV81WP_RelatorioComparacaoDemandasModeloDS_13_Contagemresultado_dataentregareal1 ;
      private DateTime AV82WP_RelatorioComparacaoDemandasModeloDS_14_Contagemresultado_dataentregareal_to1 ;
      private DateTime AV94WP_RelatorioComparacaoDemandasModeloDS_26_Contagemresultado_dataentregareal2 ;
      private DateTime AV95WP_RelatorioComparacaoDemandasModeloDS_27_Contagemresultado_dataentregareal_to2 ;
      private DateTime AV107WP_RelatorioComparacaoDemandasModeloDS_39_Contagemresultado_dataentregareal3 ;
      private DateTime AV108WP_RelatorioComparacaoDemandasModeloDS_40_Contagemresultado_dataentregareal_to3 ;
      private DateTime A2017ContagemResultado_DataEntregaReal ;
      private DateTime GXt_dtime1 ;
      private DateTime AV26ContagemResultado_DataDmn1 ;
      private DateTime AV27ContagemResultado_DataDmn_To1 ;
      private DateTime AV40ContagemResultado_DataDmn2 ;
      private DateTime AV41ContagemResultado_DataDmn_To2 ;
      private DateTime AV53ContagemResultado_DataDmn3 ;
      private DateTime AV54ContagemResultado_DataDmn_To3 ;
      private DateTime AV79WP_RelatorioComparacaoDemandasModeloDS_11_Contagemresultado_datadmn1 ;
      private DateTime AV80WP_RelatorioComparacaoDemandasModeloDS_12_Contagemresultado_datadmn_to1 ;
      private DateTime AV92WP_RelatorioComparacaoDemandasModeloDS_24_Contagemresultado_datadmn2 ;
      private DateTime AV93WP_RelatorioComparacaoDemandasModeloDS_25_Contagemresultado_datadmn_to2 ;
      private DateTime AV105WP_RelatorioComparacaoDemandasModeloDS_37_Contagemresultado_datadmn3 ;
      private DateTime AV106WP_RelatorioComparacaoDemandasModeloDS_38_Contagemresultado_datadmn_to3 ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool returnInSub ;
      private bool AV28PrintFilterValue ;
      private bool AV36DynamicFiltersEnabled2 ;
      private bool AV49DynamicFiltersEnabled3 ;
      private bool AV88WP_RelatorioComparacaoDemandasModeloDS_20_Dynamicfiltersenabled2 ;
      private bool AV101WP_RelatorioComparacaoDemandasModeloDS_33_Dynamicfiltersenabled3 ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1627ContagemResultado_CntSrvVncCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n1046ContagemResultado_Agrupador ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n494ContagemResultado_Descricao ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n2017ContagemResultado_DataEntregaReal ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n1591ContagemResultado_CodSrvVnc ;
      private bool n2146ContagemResultado_SerGrupoVinc ;
      private bool n2145ContagemResultado_CntadaOsVinc ;
      private bool n601ContagemResultado_Servico ;
      private bool n764ContagemResultado_ServicoGrupo ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n1326ContagemResultado_ContratadaTipoFab ;
      private bool n457ContagemResultado_Demanda ;
      private String AV62GridStateXML ;
      private String AV12ErrorMessage ;
      private String AV11Filename ;
      private String AV23DynamicFiltersSelector1 ;
      private String AV25ContagemResultado_DemandaFM1 ;
      private String AV33ContagemResultado_Descricao1 ;
      private String AV37DynamicFiltersSelector2 ;
      private String AV39ContagemResultado_DemandaFM2 ;
      private String AV46ContagemResultado_Descricao2 ;
      private String AV50DynamicFiltersSelector3 ;
      private String AV52ContagemResultado_DemandaFM3 ;
      private String AV59ContagemResultado_Descricao3 ;
      private String AV76WP_RelatorioComparacaoDemandasModeloDS_8_Dynamicfiltersselector1 ;
      private String AV78WP_RelatorioComparacaoDemandasModeloDS_10_Contagemresultado_demandafm1 ;
      private String AV85WP_RelatorioComparacaoDemandasModeloDS_17_Contagemresultado_descricao1 ;
      private String AV89WP_RelatorioComparacaoDemandasModeloDS_21_Dynamicfiltersselector2 ;
      private String AV91WP_RelatorioComparacaoDemandasModeloDS_23_Contagemresultado_demandafm2 ;
      private String AV98WP_RelatorioComparacaoDemandasModeloDS_30_Contagemresultado_descricao2 ;
      private String AV102WP_RelatorioComparacaoDemandasModeloDS_34_Dynamicfiltersselector3 ;
      private String AV104WP_RelatorioComparacaoDemandasModeloDS_36_Contagemresultado_demandafm3 ;
      private String AV111WP_RelatorioComparacaoDemandasModeloDS_43_Contagemresultado_descricao3 ;
      private String lV78WP_RelatorioComparacaoDemandasModeloDS_10_Contagemresultado_demandafm1 ;
      private String lV85WP_RelatorioComparacaoDemandasModeloDS_17_Contagemresultado_descricao1 ;
      private String lV91WP_RelatorioComparacaoDemandasModeloDS_23_Contagemresultado_demandafm2 ;
      private String lV98WP_RelatorioComparacaoDemandasModeloDS_30_Contagemresultado_descricao2 ;
      private String lV104WP_RelatorioComparacaoDemandasModeloDS_36_Contagemresultado_demandafm3 ;
      private String lV111WP_RelatorioComparacaoDemandasModeloDS_43_Contagemresultado_descricao3 ;
      private String A493ContagemResultado_DemandaFM ;
      private String A494ContagemResultado_Descricao ;
      private String A457ContagemResultado_Demanda ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00YO3_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00YO3_n1553ContagemResultado_CntSrvCod ;
      private int[] P00YO3_A1627ContagemResultado_CntSrvVncCod ;
      private bool[] P00YO3_n1627ContagemResultado_CntSrvVncCod ;
      private int[] P00YO3_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00YO3_n52Contratada_AreaTrabalhoCod ;
      private int[] P00YO3_A456ContagemResultado_Codigo ;
      private int[] P00YO3_A602ContagemResultado_OSVinculada ;
      private bool[] P00YO3_n602ContagemResultado_OSVinculada ;
      private String[] P00YO3_A1046ContagemResultado_Agrupador ;
      private bool[] P00YO3_n1046ContagemResultado_Agrupador ;
      private int[] P00YO3_A489ContagemResultado_SistemaCod ;
      private bool[] P00YO3_n489ContagemResultado_SistemaCod ;
      private String[] P00YO3_A494ContagemResultado_Descricao ;
      private bool[] P00YO3_n494ContagemResultado_Descricao ;
      private short[] P00YO3_A483ContagemResultado_StatusCnt ;
      private String[] P00YO3_A484ContagemResultado_StatusDmn ;
      private bool[] P00YO3_n484ContagemResultado_StatusDmn ;
      private DateTime[] P00YO3_A2017ContagemResultado_DataEntregaReal ;
      private bool[] P00YO3_n2017ContagemResultado_DataEntregaReal ;
      private DateTime[] P00YO3_A471ContagemResultado_DataDmn ;
      private String[] P00YO3_A493ContagemResultado_DemandaFM ;
      private bool[] P00YO3_n493ContagemResultado_DemandaFM ;
      private int[] P00YO3_A1591ContagemResultado_CodSrvVnc ;
      private bool[] P00YO3_n1591ContagemResultado_CodSrvVnc ;
      private int[] P00YO3_A2146ContagemResultado_SerGrupoVinc ;
      private bool[] P00YO3_n2146ContagemResultado_SerGrupoVinc ;
      private int[] P00YO3_A2145ContagemResultado_CntadaOsVinc ;
      private bool[] P00YO3_n2145ContagemResultado_CntadaOsVinc ;
      private int[] P00YO3_A601ContagemResultado_Servico ;
      private bool[] P00YO3_n601ContagemResultado_Servico ;
      private int[] P00YO3_A764ContagemResultado_ServicoGrupo ;
      private bool[] P00YO3_n764ContagemResultado_ServicoGrupo ;
      private int[] P00YO3_A490ContagemResultado_ContratadaCod ;
      private bool[] P00YO3_n490ContagemResultado_ContratadaCod ;
      private String[] P00YO3_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00YO3_n509ContagemrResultado_SistemaSigla ;
      private String[] P00YO3_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] P00YO3_n1326ContagemResultado_ContratadaTipoFab ;
      private String[] P00YO3_A457ContagemResultado_Demanda ;
      private bool[] P00YO3_n457ContagemResultado_Demanda ;
      private decimal[] P00YO3_A682ContagemResultado_PFBFMUltima ;
      private decimal[] P00YO3_A684ContagemResultado_PFBFSUltima ;
      private DateTime[] P00YO3_A473ContagemResultado_DataCnt ;
      private String[] P00YO3_A511ContagemResultado_HoraCnt ;
      private String aP8_Filename ;
      private String aP9_ErrorMessage ;
      private ExcelDocumentI AV10ExcelDocument ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV63GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV64GridStateDynamicFilter ;
   }

   public class rel_relatoriocomparacaodemandas__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00YO3( IGxContext context ,
                                             int AV70WP_RelatorioComparacaoDemandasModeloDS_2_Contagemresultado_contratadacod ,
                                             int AV71WP_RelatorioComparacaoDemandasModeloDS_3_Contagemresultado_servicogrupo ,
                                             int AV72WP_RelatorioComparacaoDemandasModeloDS_4_Contagemresultado_servico ,
                                             int AV73WP_RelatorioComparacaoDemandasModeloDS_5_Contagemresultado_cntadaosvinc ,
                                             int AV74WP_RelatorioComparacaoDemandasModeloDS_6_Contagemresultado_sergrupovinc ,
                                             int AV75WP_RelatorioComparacaoDemandasModeloDS_7_Contagemresultado_codsrvvnc ,
                                             String AV76WP_RelatorioComparacaoDemandasModeloDS_8_Dynamicfiltersselector1 ,
                                             short AV77WP_RelatorioComparacaoDemandasModeloDS_9_Dynamicfiltersoperator1 ,
                                             String AV78WP_RelatorioComparacaoDemandasModeloDS_10_Contagemresultado_demandafm1 ,
                                             DateTime AV79WP_RelatorioComparacaoDemandasModeloDS_11_Contagemresultado_datadmn1 ,
                                             DateTime AV80WP_RelatorioComparacaoDemandasModeloDS_12_Contagemresultado_datadmn_to1 ,
                                             DateTime AV81WP_RelatorioComparacaoDemandasModeloDS_13_Contagemresultado_dataentregareal1 ,
                                             DateTime AV82WP_RelatorioComparacaoDemandasModeloDS_14_Contagemresultado_dataentregareal_to1 ,
                                             String AV83WP_RelatorioComparacaoDemandasModeloDS_15_Contagemresultado_statusdmn1 ,
                                             short AV84WP_RelatorioComparacaoDemandasModeloDS_16_Contagemresultado_statuscnt1 ,
                                             String AV85WP_RelatorioComparacaoDemandasModeloDS_17_Contagemresultado_descricao1 ,
                                             int AV86WP_RelatorioComparacaoDemandasModeloDS_18_Contagemresultado_sistemacod1 ,
                                             String AV87WP_RelatorioComparacaoDemandasModeloDS_19_Contagemresultado_agrupador1 ,
                                             bool AV88WP_RelatorioComparacaoDemandasModeloDS_20_Dynamicfiltersenabled2 ,
                                             String AV89WP_RelatorioComparacaoDemandasModeloDS_21_Dynamicfiltersselector2 ,
                                             short AV90WP_RelatorioComparacaoDemandasModeloDS_22_Dynamicfiltersoperator2 ,
                                             String AV91WP_RelatorioComparacaoDemandasModeloDS_23_Contagemresultado_demandafm2 ,
                                             DateTime AV92WP_RelatorioComparacaoDemandasModeloDS_24_Contagemresultado_datadmn2 ,
                                             DateTime AV93WP_RelatorioComparacaoDemandasModeloDS_25_Contagemresultado_datadmn_to2 ,
                                             DateTime AV94WP_RelatorioComparacaoDemandasModeloDS_26_Contagemresultado_dataentregareal2 ,
                                             DateTime AV95WP_RelatorioComparacaoDemandasModeloDS_27_Contagemresultado_dataentregareal_to2 ,
                                             String AV96WP_RelatorioComparacaoDemandasModeloDS_28_Contagemresultado_statusdmn2 ,
                                             short AV97WP_RelatorioComparacaoDemandasModeloDS_29_Contagemresultado_statuscnt2 ,
                                             String AV98WP_RelatorioComparacaoDemandasModeloDS_30_Contagemresultado_descricao2 ,
                                             int AV99WP_RelatorioComparacaoDemandasModeloDS_31_Contagemresultado_sistemacod2 ,
                                             String AV100WP_RelatorioComparacaoDemandasModeloDS_32_Contagemresultado_agrupador2 ,
                                             bool AV101WP_RelatorioComparacaoDemandasModeloDS_33_Dynamicfiltersenabled3 ,
                                             String AV102WP_RelatorioComparacaoDemandasModeloDS_34_Dynamicfiltersselector3 ,
                                             short AV103WP_RelatorioComparacaoDemandasModeloDS_35_Dynamicfiltersoperator3 ,
                                             String AV104WP_RelatorioComparacaoDemandasModeloDS_36_Contagemresultado_demandafm3 ,
                                             DateTime AV105WP_RelatorioComparacaoDemandasModeloDS_37_Contagemresultado_datadmn3 ,
                                             DateTime AV106WP_RelatorioComparacaoDemandasModeloDS_38_Contagemresultado_datadmn_to3 ,
                                             DateTime AV107WP_RelatorioComparacaoDemandasModeloDS_39_Contagemresultado_dataentregareal3 ,
                                             DateTime AV108WP_RelatorioComparacaoDemandasModeloDS_40_Contagemresultado_dataentregareal_to3 ,
                                             String AV109WP_RelatorioComparacaoDemandasModeloDS_41_Contagemresultado_statusdmn3 ,
                                             short AV110WP_RelatorioComparacaoDemandasModeloDS_42_Contagemresultado_statuscnt3 ,
                                             String AV111WP_RelatorioComparacaoDemandasModeloDS_43_Contagemresultado_descricao3 ,
                                             int AV112WP_RelatorioComparacaoDemandasModeloDS_44_Contagemresultado_sistemacod3 ,
                                             String AV113WP_RelatorioComparacaoDemandasModeloDS_45_Contagemresultado_agrupador3 ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             int A764ContagemResultado_ServicoGrupo ,
                                             int A601ContagemResultado_Servico ,
                                             int A2145ContagemResultado_CntadaOsVinc ,
                                             int A2146ContagemResultado_SerGrupoVinc ,
                                             int A1591ContagemResultado_CodSrvVnc ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             DateTime A2017ContagemResultado_DataEntregaReal ,
                                             String A484ContagemResultado_StatusDmn ,
                                             short A483ContagemResultado_StatusCnt ,
                                             String A494ContagemResultado_Descricao ,
                                             int A489ContagemResultado_SistemaCod ,
                                             String A1046ContagemResultado_Agrupador ,
                                             int A602ContagemResultado_OSVinculada ,
                                             int A456ContagemResultado_Codigo ,
                                             int AV17ContagemResultado_ContratadaCod ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [55] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T5.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvVncCod, T9.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_Codigo], T2.[ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada, T2.[ContagemResultado_Agrupador], T2.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T2.[ContagemResultado_Descricao], T1.[ContagemResultado_StatusCnt], T2.[ContagemResultado_StatusDmn], T2.[ContagemResultado_DataEntregaReal], T2.[ContagemResultado_DataDmn], T2.[ContagemResultado_DemandaFM], T6.[Servico_Codigo] AS ContagemResultado_CodSrvVnc, T7.[ServicoGrupo_Codigo] AS ContagemResultado_SerGrupoVinc, T5.[ContagemResultado_ContratadaCod] AS ContagemResultado_CntadaOsVinc, T3.[Servico_Codigo] AS ContagemResultado_Servico, T4.[ServicoGrupo_Codigo] AS ContagemResultado_ServicoGrupo, T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T8.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T9.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, T2.[ContagemResultado_Demanda], COALESCE( T10.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T10.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt] FROM ((((((((([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T3.[Servico_Codigo]) LEFT JOIN [ContagemResultado] T5 WITH (NOLOCK) ON T5.[ContagemResultado_Codigo] = T2.[ContagemResultado_OSVinculada])";
         scmdbuf = scmdbuf + " LEFT JOIN [ContratoServicos] T6 WITH (NOLOCK) ON T6.[ContratoServicos_Codigo] = T5.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T7 WITH (NOLOCK) ON T7.[Servico_Codigo] = T6.[Servico_Codigo]) LEFT JOIN [Sistema] T8 WITH (NOLOCK) ON T8.[Sistema_Codigo] = T2.[ContagemResultado_SistemaCod]) LEFT JOIN [Contratada] T9 WITH (NOLOCK) ON T9.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T10 ON T10.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T9.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ! (0==AV70WP_RelatorioComparacaoDemandasModeloDS_2_Contagemresultado_contratadacod) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV70WP_RelatorioComparacaoDemandasModeloDS_2_Contagemresultado_contratadacod)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! (0==AV71WP_RelatorioComparacaoDemandasModeloDS_3_Contagemresultado_servicogrupo) )
         {
            sWhereString = sWhereString + " and (T4.[ServicoGrupo_Codigo] = @AV71WP_RelatorioComparacaoDemandasModeloDS_3_Contagemresultado_servicogrupo)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! (0==AV72WP_RelatorioComparacaoDemandasModeloDS_4_Contagemresultado_servico) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Codigo] = @AV72WP_RelatorioComparacaoDemandasModeloDS_4_Contagemresultado_servico)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! (0==AV73WP_RelatorioComparacaoDemandasModeloDS_5_Contagemresultado_cntadaosvinc) )
         {
            sWhereString = sWhereString + " and (T5.[ContagemResultado_ContratadaCod] = @AV73WP_RelatorioComparacaoDemandasModeloDS_5_Contagemresultado_cntadaosvinc)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! (0==AV74WP_RelatorioComparacaoDemandasModeloDS_6_Contagemresultado_sergrupovinc) )
         {
            sWhereString = sWhereString + " and (T7.[ServicoGrupo_Codigo] = @AV74WP_RelatorioComparacaoDemandasModeloDS_6_Contagemresultado_sergrupovinc)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (0==AV75WP_RelatorioComparacaoDemandasModeloDS_7_Contagemresultado_codsrvvnc) )
         {
            sWhereString = sWhereString + " and (T6.[Servico_Codigo] = @AV75WP_RelatorioComparacaoDemandasModeloDS_7_Contagemresultado_codsrvvnc)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WP_RelatorioComparacaoDemandasModeloDS_8_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV77WP_RelatorioComparacaoDemandasModeloDS_9_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WP_RelatorioComparacaoDemandasModeloDS_10_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV78WP_RelatorioComparacaoDemandasModeloDS_10_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WP_RelatorioComparacaoDemandasModeloDS_8_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV77WP_RelatorioComparacaoDemandasModeloDS_9_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WP_RelatorioComparacaoDemandasModeloDS_10_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV78WP_RelatorioComparacaoDemandasModeloDS_10_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WP_RelatorioComparacaoDemandasModeloDS_8_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV77WP_RelatorioComparacaoDemandasModeloDS_9_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WP_RelatorioComparacaoDemandasModeloDS_10_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like '%' + @lV78WP_RelatorioComparacaoDemandasModeloDS_10_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WP_RelatorioComparacaoDemandasModeloDS_8_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV77WP_RelatorioComparacaoDemandasModeloDS_9_Dynamicfiltersoperator1 == 0 ) && ( ! (DateTime.MinValue==AV79WP_RelatorioComparacaoDemandasModeloDS_11_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV79WP_RelatorioComparacaoDemandasModeloDS_11_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WP_RelatorioComparacaoDemandasModeloDS_8_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV77WP_RelatorioComparacaoDemandasModeloDS_9_Dynamicfiltersoperator1 == 0 ) && ( ! (DateTime.MinValue==AV80WP_RelatorioComparacaoDemandasModeloDS_12_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV80WP_RelatorioComparacaoDemandasModeloDS_12_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WP_RelatorioComparacaoDemandasModeloDS_8_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV77WP_RelatorioComparacaoDemandasModeloDS_9_Dynamicfiltersoperator1 == 0 ) && ( ! (DateTime.MinValue==AV81WP_RelatorioComparacaoDemandasModeloDS_13_Contagemresultado_dataentregareal1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] >= @AV81WP_RelatorioComparacaoDemandasModeloDS_13_Contagemresultado_dataentregareal1)";
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WP_RelatorioComparacaoDemandasModeloDS_8_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV77WP_RelatorioComparacaoDemandasModeloDS_9_Dynamicfiltersoperator1 == 0 ) && ( ! (DateTime.MinValue==AV82WP_RelatorioComparacaoDemandasModeloDS_14_Contagemresultado_dataentregareal_to1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] <= @AV82WP_RelatorioComparacaoDemandasModeloDS_14_Contagemresultado_dataentregareal_to1)";
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WP_RelatorioComparacaoDemandasModeloDS_8_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WP_RelatorioComparacaoDemandasModeloDS_15_Contagemresultado_statusdmn1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_StatusDmn] = @AV83WP_RelatorioComparacaoDemandasModeloDS_15_Contagemresultado_statusdmn1)";
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WP_RelatorioComparacaoDemandasModeloDS_8_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSCNT") == 0 ) && ( ! (0==AV84WP_RelatorioComparacaoDemandasModeloDS_16_Contagemresultado_statuscnt1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusCnt] = @AV84WP_RelatorioComparacaoDemandasModeloDS_16_Contagemresultado_statuscnt1)";
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WP_RelatorioComparacaoDemandasModeloDS_8_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV77WP_RelatorioComparacaoDemandasModeloDS_9_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WP_RelatorioComparacaoDemandasModeloDS_17_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like @lV85WP_RelatorioComparacaoDemandasModeloDS_17_Contagemresultado_descricao1)";
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WP_RelatorioComparacaoDemandasModeloDS_8_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV77WP_RelatorioComparacaoDemandasModeloDS_9_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WP_RelatorioComparacaoDemandasModeloDS_17_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV85WP_RelatorioComparacaoDemandasModeloDS_17_Contagemresultado_descricao1)";
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WP_RelatorioComparacaoDemandasModeloDS_8_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV77WP_RelatorioComparacaoDemandasModeloDS_9_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WP_RelatorioComparacaoDemandasModeloDS_17_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] = @AV85WP_RelatorioComparacaoDemandasModeloDS_17_Contagemresultado_descricao1)";
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WP_RelatorioComparacaoDemandasModeloDS_8_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV86WP_RelatorioComparacaoDemandasModeloDS_18_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV86WP_RelatorioComparacaoDemandasModeloDS_18_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WP_RelatorioComparacaoDemandasModeloDS_8_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV77WP_RelatorioComparacaoDemandasModeloDS_9_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WP_RelatorioComparacaoDemandasModeloDS_19_Contagemresultado_agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV87WP_RelatorioComparacaoDemandasModeloDS_19_Contagemresultado_agrupador1)";
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WP_RelatorioComparacaoDemandasModeloDS_8_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV77WP_RelatorioComparacaoDemandasModeloDS_9_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WP_RelatorioComparacaoDemandasModeloDS_19_Contagemresultado_agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] like @lV87WP_RelatorioComparacaoDemandasModeloDS_19_Contagemresultado_agrupador1)";
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76WP_RelatorioComparacaoDemandasModeloDS_8_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV77WP_RelatorioComparacaoDemandasModeloDS_9_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WP_RelatorioComparacaoDemandasModeloDS_19_Contagemresultado_agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] like '%' + @lV87WP_RelatorioComparacaoDemandasModeloDS_19_Contagemresultado_agrupador1)";
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( AV88WP_RelatorioComparacaoDemandasModeloDS_20_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WP_RelatorioComparacaoDemandasModeloDS_21_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV90WP_RelatorioComparacaoDemandasModeloDS_22_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WP_RelatorioComparacaoDemandasModeloDS_23_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV91WP_RelatorioComparacaoDemandasModeloDS_23_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( AV88WP_RelatorioComparacaoDemandasModeloDS_20_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WP_RelatorioComparacaoDemandasModeloDS_21_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV90WP_RelatorioComparacaoDemandasModeloDS_22_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WP_RelatorioComparacaoDemandasModeloDS_23_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV91WP_RelatorioComparacaoDemandasModeloDS_23_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int2[24] = 1;
         }
         if ( AV88WP_RelatorioComparacaoDemandasModeloDS_20_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WP_RelatorioComparacaoDemandasModeloDS_21_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV90WP_RelatorioComparacaoDemandasModeloDS_22_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WP_RelatorioComparacaoDemandasModeloDS_23_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like '%' + @lV91WP_RelatorioComparacaoDemandasModeloDS_23_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int2[25] = 1;
         }
         if ( AV88WP_RelatorioComparacaoDemandasModeloDS_20_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WP_RelatorioComparacaoDemandasModeloDS_21_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV90WP_RelatorioComparacaoDemandasModeloDS_22_Dynamicfiltersoperator2 == 0 ) && ( ! (DateTime.MinValue==AV92WP_RelatorioComparacaoDemandasModeloDS_24_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV92WP_RelatorioComparacaoDemandasModeloDS_24_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int2[26] = 1;
         }
         if ( AV88WP_RelatorioComparacaoDemandasModeloDS_20_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WP_RelatorioComparacaoDemandasModeloDS_21_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV90WP_RelatorioComparacaoDemandasModeloDS_22_Dynamicfiltersoperator2 == 0 ) && ( ! (DateTime.MinValue==AV93WP_RelatorioComparacaoDemandasModeloDS_25_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV93WP_RelatorioComparacaoDemandasModeloDS_25_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int2[27] = 1;
         }
         if ( AV88WP_RelatorioComparacaoDemandasModeloDS_20_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WP_RelatorioComparacaoDemandasModeloDS_21_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV90WP_RelatorioComparacaoDemandasModeloDS_22_Dynamicfiltersoperator2 == 0 ) && ( ! (DateTime.MinValue==AV94WP_RelatorioComparacaoDemandasModeloDS_26_Contagemresultado_dataentregareal2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] >= @AV94WP_RelatorioComparacaoDemandasModeloDS_26_Contagemresultado_dataentregareal2)";
         }
         else
         {
            GXv_int2[28] = 1;
         }
         if ( AV88WP_RelatorioComparacaoDemandasModeloDS_20_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WP_RelatorioComparacaoDemandasModeloDS_21_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV90WP_RelatorioComparacaoDemandasModeloDS_22_Dynamicfiltersoperator2 == 0 ) && ( ! (DateTime.MinValue==AV95WP_RelatorioComparacaoDemandasModeloDS_27_Contagemresultado_dataentregareal_to2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] <= @AV95WP_RelatorioComparacaoDemandasModeloDS_27_Contagemresultado_dataentregareal_to2)";
         }
         else
         {
            GXv_int2[29] = 1;
         }
         if ( AV88WP_RelatorioComparacaoDemandasModeloDS_20_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WP_RelatorioComparacaoDemandasModeloDS_21_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WP_RelatorioComparacaoDemandasModeloDS_28_Contagemresultado_statusdmn2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_StatusDmn] = @AV96WP_RelatorioComparacaoDemandasModeloDS_28_Contagemresultado_statusdmn2)";
         }
         else
         {
            GXv_int2[30] = 1;
         }
         if ( AV88WP_RelatorioComparacaoDemandasModeloDS_20_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WP_RelatorioComparacaoDemandasModeloDS_21_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSCNT") == 0 ) && ( ! (0==AV97WP_RelatorioComparacaoDemandasModeloDS_29_Contagemresultado_statuscnt2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusCnt] = @AV97WP_RelatorioComparacaoDemandasModeloDS_29_Contagemresultado_statuscnt2)";
         }
         else
         {
            GXv_int2[31] = 1;
         }
         if ( AV88WP_RelatorioComparacaoDemandasModeloDS_20_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WP_RelatorioComparacaoDemandasModeloDS_21_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV90WP_RelatorioComparacaoDemandasModeloDS_22_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WP_RelatorioComparacaoDemandasModeloDS_30_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like @lV98WP_RelatorioComparacaoDemandasModeloDS_30_Contagemresultado_descricao2)";
         }
         else
         {
            GXv_int2[32] = 1;
         }
         if ( AV88WP_RelatorioComparacaoDemandasModeloDS_20_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WP_RelatorioComparacaoDemandasModeloDS_21_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV90WP_RelatorioComparacaoDemandasModeloDS_22_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WP_RelatorioComparacaoDemandasModeloDS_30_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV98WP_RelatorioComparacaoDemandasModeloDS_30_Contagemresultado_descricao2)";
         }
         else
         {
            GXv_int2[33] = 1;
         }
         if ( AV88WP_RelatorioComparacaoDemandasModeloDS_20_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WP_RelatorioComparacaoDemandasModeloDS_21_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV90WP_RelatorioComparacaoDemandasModeloDS_22_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WP_RelatorioComparacaoDemandasModeloDS_30_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] = @AV98WP_RelatorioComparacaoDemandasModeloDS_30_Contagemresultado_descricao2)";
         }
         else
         {
            GXv_int2[34] = 1;
         }
         if ( AV88WP_RelatorioComparacaoDemandasModeloDS_20_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WP_RelatorioComparacaoDemandasModeloDS_21_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV99WP_RelatorioComparacaoDemandasModeloDS_31_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV99WP_RelatorioComparacaoDemandasModeloDS_31_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int2[35] = 1;
         }
         if ( AV88WP_RelatorioComparacaoDemandasModeloDS_20_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WP_RelatorioComparacaoDemandasModeloDS_21_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV90WP_RelatorioComparacaoDemandasModeloDS_22_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WP_RelatorioComparacaoDemandasModeloDS_32_Contagemresultado_agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV100WP_RelatorioComparacaoDemandasModeloDS_32_Contagemresultado_agrupador2)";
         }
         else
         {
            GXv_int2[36] = 1;
         }
         if ( AV88WP_RelatorioComparacaoDemandasModeloDS_20_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WP_RelatorioComparacaoDemandasModeloDS_21_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV90WP_RelatorioComparacaoDemandasModeloDS_22_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WP_RelatorioComparacaoDemandasModeloDS_32_Contagemresultado_agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] like @lV100WP_RelatorioComparacaoDemandasModeloDS_32_Contagemresultado_agrupador2)";
         }
         else
         {
            GXv_int2[37] = 1;
         }
         if ( AV88WP_RelatorioComparacaoDemandasModeloDS_20_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WP_RelatorioComparacaoDemandasModeloDS_21_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV90WP_RelatorioComparacaoDemandasModeloDS_22_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WP_RelatorioComparacaoDemandasModeloDS_32_Contagemresultado_agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] like '%' + @lV100WP_RelatorioComparacaoDemandasModeloDS_32_Contagemresultado_agrupador2)";
         }
         else
         {
            GXv_int2[38] = 1;
         }
         if ( AV101WP_RelatorioComparacaoDemandasModeloDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102WP_RelatorioComparacaoDemandasModeloDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV103WP_RelatorioComparacaoDemandasModeloDS_35_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WP_RelatorioComparacaoDemandasModeloDS_36_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV104WP_RelatorioComparacaoDemandasModeloDS_36_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int2[39] = 1;
         }
         if ( AV101WP_RelatorioComparacaoDemandasModeloDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102WP_RelatorioComparacaoDemandasModeloDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV103WP_RelatorioComparacaoDemandasModeloDS_35_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WP_RelatorioComparacaoDemandasModeloDS_36_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV104WP_RelatorioComparacaoDemandasModeloDS_36_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int2[40] = 1;
         }
         if ( AV101WP_RelatorioComparacaoDemandasModeloDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102WP_RelatorioComparacaoDemandasModeloDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV103WP_RelatorioComparacaoDemandasModeloDS_35_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WP_RelatorioComparacaoDemandasModeloDS_36_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like '%' + @lV104WP_RelatorioComparacaoDemandasModeloDS_36_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int2[41] = 1;
         }
         if ( AV101WP_RelatorioComparacaoDemandasModeloDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102WP_RelatorioComparacaoDemandasModeloDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV103WP_RelatorioComparacaoDemandasModeloDS_35_Dynamicfiltersoperator3 == 0 ) && ( ! (DateTime.MinValue==AV105WP_RelatorioComparacaoDemandasModeloDS_37_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV105WP_RelatorioComparacaoDemandasModeloDS_37_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int2[42] = 1;
         }
         if ( AV101WP_RelatorioComparacaoDemandasModeloDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102WP_RelatorioComparacaoDemandasModeloDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV103WP_RelatorioComparacaoDemandasModeloDS_35_Dynamicfiltersoperator3 == 0 ) && ( ! (DateTime.MinValue==AV106WP_RelatorioComparacaoDemandasModeloDS_38_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV106WP_RelatorioComparacaoDemandasModeloDS_38_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int2[43] = 1;
         }
         if ( AV101WP_RelatorioComparacaoDemandasModeloDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102WP_RelatorioComparacaoDemandasModeloDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV103WP_RelatorioComparacaoDemandasModeloDS_35_Dynamicfiltersoperator3 == 0 ) && ( ! (DateTime.MinValue==AV107WP_RelatorioComparacaoDemandasModeloDS_39_Contagemresultado_dataentregareal3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] >= @AV107WP_RelatorioComparacaoDemandasModeloDS_39_Contagemresultado_dataentregareal3)";
         }
         else
         {
            GXv_int2[44] = 1;
         }
         if ( AV101WP_RelatorioComparacaoDemandasModeloDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102WP_RelatorioComparacaoDemandasModeloDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV103WP_RelatorioComparacaoDemandasModeloDS_35_Dynamicfiltersoperator3 == 0 ) && ( ! (DateTime.MinValue==AV108WP_RelatorioComparacaoDemandasModeloDS_40_Contagemresultado_dataentregareal_to3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] <= @AV108WP_RelatorioComparacaoDemandasModeloDS_40_Contagemresultado_dataentregareal_to3)";
         }
         else
         {
            GXv_int2[45] = 1;
         }
         if ( AV101WP_RelatorioComparacaoDemandasModeloDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102WP_RelatorioComparacaoDemandasModeloDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109WP_RelatorioComparacaoDemandasModeloDS_41_Contagemresultado_statusdmn3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_StatusDmn] = @AV109WP_RelatorioComparacaoDemandasModeloDS_41_Contagemresultado_statusdmn3)";
         }
         else
         {
            GXv_int2[46] = 1;
         }
         if ( AV101WP_RelatorioComparacaoDemandasModeloDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102WP_RelatorioComparacaoDemandasModeloDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSCNT") == 0 ) && ( ! (0==AV110WP_RelatorioComparacaoDemandasModeloDS_42_Contagemresultado_statuscnt3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusCnt] = @AV110WP_RelatorioComparacaoDemandasModeloDS_42_Contagemresultado_statuscnt3)";
         }
         else
         {
            GXv_int2[47] = 1;
         }
         if ( AV101WP_RelatorioComparacaoDemandasModeloDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102WP_RelatorioComparacaoDemandasModeloDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV103WP_RelatorioComparacaoDemandasModeloDS_35_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV111WP_RelatorioComparacaoDemandasModeloDS_43_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like @lV111WP_RelatorioComparacaoDemandasModeloDS_43_Contagemresultado_descricao3)";
         }
         else
         {
            GXv_int2[48] = 1;
         }
         if ( AV101WP_RelatorioComparacaoDemandasModeloDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102WP_RelatorioComparacaoDemandasModeloDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV103WP_RelatorioComparacaoDemandasModeloDS_35_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV111WP_RelatorioComparacaoDemandasModeloDS_43_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV111WP_RelatorioComparacaoDemandasModeloDS_43_Contagemresultado_descricao3)";
         }
         else
         {
            GXv_int2[49] = 1;
         }
         if ( AV101WP_RelatorioComparacaoDemandasModeloDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102WP_RelatorioComparacaoDemandasModeloDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV103WP_RelatorioComparacaoDemandasModeloDS_35_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV111WP_RelatorioComparacaoDemandasModeloDS_43_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] = @AV111WP_RelatorioComparacaoDemandasModeloDS_43_Contagemresultado_descricao3)";
         }
         else
         {
            GXv_int2[50] = 1;
         }
         if ( AV101WP_RelatorioComparacaoDemandasModeloDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102WP_RelatorioComparacaoDemandasModeloDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV112WP_RelatorioComparacaoDemandasModeloDS_44_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV112WP_RelatorioComparacaoDemandasModeloDS_44_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int2[51] = 1;
         }
         if ( AV101WP_RelatorioComparacaoDemandasModeloDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102WP_RelatorioComparacaoDemandasModeloDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV103WP_RelatorioComparacaoDemandasModeloDS_35_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WP_RelatorioComparacaoDemandasModeloDS_45_Contagemresultado_agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV113WP_RelatorioComparacaoDemandasModeloDS_45_Contagemresultado_agrupador3)";
         }
         else
         {
            GXv_int2[52] = 1;
         }
         if ( AV101WP_RelatorioComparacaoDemandasModeloDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102WP_RelatorioComparacaoDemandasModeloDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV103WP_RelatorioComparacaoDemandasModeloDS_35_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WP_RelatorioComparacaoDemandasModeloDS_45_Contagemresultado_agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] like @lV113WP_RelatorioComparacaoDemandasModeloDS_45_Contagemresultado_agrupador3)";
         }
         else
         {
            GXv_int2[53] = 1;
         }
         if ( AV101WP_RelatorioComparacaoDemandasModeloDS_33_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102WP_RelatorioComparacaoDemandasModeloDS_34_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV103WP_RelatorioComparacaoDemandasModeloDS_35_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WP_RelatorioComparacaoDemandasModeloDS_45_Contagemresultado_agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] like '%' + @lV113WP_RelatorioComparacaoDemandasModeloDS_45_Contagemresultado_agrupador3)";
         }
         else
         {
            GXv_int2[54] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[ContagemResultado_Demanda]";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00YO3(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] , (String)dynConstraints[13] , (short)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (String)dynConstraints[17] , (bool)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (String)dynConstraints[21] , (DateTime)dynConstraints[22] , (DateTime)dynConstraints[23] , (DateTime)dynConstraints[24] , (DateTime)dynConstraints[25] , (String)dynConstraints[26] , (short)dynConstraints[27] , (String)dynConstraints[28] , (int)dynConstraints[29] , (String)dynConstraints[30] , (bool)dynConstraints[31] , (String)dynConstraints[32] , (short)dynConstraints[33] , (String)dynConstraints[34] , (DateTime)dynConstraints[35] , (DateTime)dynConstraints[36] , (DateTime)dynConstraints[37] , (DateTime)dynConstraints[38] , (String)dynConstraints[39] , (short)dynConstraints[40] , (String)dynConstraints[41] , (int)dynConstraints[42] , (String)dynConstraints[43] , (int)dynConstraints[44] , (int)dynConstraints[45] , (int)dynConstraints[46] , (int)dynConstraints[47] , (int)dynConstraints[48] , (int)dynConstraints[49] , (String)dynConstraints[50] , (DateTime)dynConstraints[51] , (DateTime)dynConstraints[52] , (String)dynConstraints[53] , (short)dynConstraints[54] , (String)dynConstraints[55] , (int)dynConstraints[56] , (String)dynConstraints[57] , (int)dynConstraints[58] , (int)dynConstraints[59] , (int)dynConstraints[60] , (int)dynConstraints[61] , (int)dynConstraints[62] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00YO3 ;
          prmP00YO3 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV70WP_RelatorioComparacaoDemandasModeloDS_2_Contagemresultado_contratadacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV71WP_RelatorioComparacaoDemandasModeloDS_3_Contagemresultado_servicogrupo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV72WP_RelatorioComparacaoDemandasModeloDS_4_Contagemresultado_servico",SqlDbType.Int,6,0} ,
          new Object[] {"@AV73WP_RelatorioComparacaoDemandasModeloDS_5_Contagemresultado_cntadaosvinc",SqlDbType.Int,6,0} ,
          new Object[] {"@AV74WP_RelatorioComparacaoDemandasModeloDS_6_Contagemresultado_sergrupovinc",SqlDbType.Int,6,0} ,
          new Object[] {"@AV75WP_RelatorioComparacaoDemandasModeloDS_7_Contagemresultado_codsrvvnc",SqlDbType.Int,6,0} ,
          new Object[] {"@AV78WP_RelatorioComparacaoDemandasModeloDS_10_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV78WP_RelatorioComparacaoDemandasModeloDS_10_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV78WP_RelatorioComparacaoDemandasModeloDS_10_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV79WP_RelatorioComparacaoDemandasModeloDS_11_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV80WP_RelatorioComparacaoDemandasModeloDS_12_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV81WP_RelatorioComparacaoDemandasModeloDS_13_Contagemresultado_dataentregareal1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV82WP_RelatorioComparacaoDemandasModeloDS_14_Contagemresultado_dataentregareal_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV83WP_RelatorioComparacaoDemandasModeloDS_15_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV84WP_RelatorioComparacaoDemandasModeloDS_16_Contagemresultado_statuscnt1",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV85WP_RelatorioComparacaoDemandasModeloDS_17_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV85WP_RelatorioComparacaoDemandasModeloDS_17_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV85WP_RelatorioComparacaoDemandasModeloDS_17_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV86WP_RelatorioComparacaoDemandasModeloDS_18_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV87WP_RelatorioComparacaoDemandasModeloDS_19_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV87WP_RelatorioComparacaoDemandasModeloDS_19_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV87WP_RelatorioComparacaoDemandasModeloDS_19_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@AV91WP_RelatorioComparacaoDemandasModeloDS_23_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV91WP_RelatorioComparacaoDemandasModeloDS_23_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV91WP_RelatorioComparacaoDemandasModeloDS_23_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV92WP_RelatorioComparacaoDemandasModeloDS_24_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV93WP_RelatorioComparacaoDemandasModeloDS_25_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV94WP_RelatorioComparacaoDemandasModeloDS_26_Contagemresultado_dataentregareal2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV95WP_RelatorioComparacaoDemandasModeloDS_27_Contagemresultado_dataentregareal_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV96WP_RelatorioComparacaoDemandasModeloDS_28_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV97WP_RelatorioComparacaoDemandasModeloDS_29_Contagemresultado_statuscnt2",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV98WP_RelatorioComparacaoDemandasModeloDS_30_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV98WP_RelatorioComparacaoDemandasModeloDS_30_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV98WP_RelatorioComparacaoDemandasModeloDS_30_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV99WP_RelatorioComparacaoDemandasModeloDS_31_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV100WP_RelatorioComparacaoDemandasModeloDS_32_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV100WP_RelatorioComparacaoDemandasModeloDS_32_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV100WP_RelatorioComparacaoDemandasModeloDS_32_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@AV104WP_RelatorioComparacaoDemandasModeloDS_36_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV104WP_RelatorioComparacaoDemandasModeloDS_36_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV104WP_RelatorioComparacaoDemandasModeloDS_36_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV105WP_RelatorioComparacaoDemandasModeloDS_37_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV106WP_RelatorioComparacaoDemandasModeloDS_38_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV107WP_RelatorioComparacaoDemandasModeloDS_39_Contagemresultado_dataentregareal3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV108WP_RelatorioComparacaoDemandasModeloDS_40_Contagemresultado_dataentregareal_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV109WP_RelatorioComparacaoDemandasModeloDS_41_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV110WP_RelatorioComparacaoDemandasModeloDS_42_Contagemresultado_statuscnt3",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV111WP_RelatorioComparacaoDemandasModeloDS_43_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV111WP_RelatorioComparacaoDemandasModeloDS_43_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV111WP_RelatorioComparacaoDemandasModeloDS_43_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV112WP_RelatorioComparacaoDemandasModeloDS_44_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV113WP_RelatorioComparacaoDemandasModeloDS_45_Contagemresultado_agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV113WP_RelatorioComparacaoDemandasModeloDS_45_Contagemresultado_agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV113WP_RelatorioComparacaoDemandasModeloDS_45_Contagemresultado_agrupador3",SqlDbType.Char,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00YO3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00YO3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((short[]) buf[15])[0] = rslt.getShort(9) ;
                ((String[]) buf[16])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[18])[0] = rslt.getGXDateTime(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[20])[0] = rslt.getGXDate(12) ;
                ((String[]) buf[21])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((int[]) buf[23])[0] = rslt.getInt(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((int[]) buf[27])[0] = rslt.getInt(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((int[]) buf[29])[0] = rslt.getInt(17) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(17);
                ((int[]) buf[31])[0] = rslt.getInt(18) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(18);
                ((int[]) buf[33])[0] = rslt.getInt(19) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(19);
                ((String[]) buf[35])[0] = rslt.getString(20, 25) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(20);
                ((String[]) buf[37])[0] = rslt.getString(21, 1) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(21);
                ((String[]) buf[39])[0] = rslt.getVarchar(22) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(22);
                ((decimal[]) buf[41])[0] = rslt.getDecimal(23) ;
                ((decimal[]) buf[42])[0] = rslt.getDecimal(24) ;
                ((DateTime[]) buf[43])[0] = rslt.getGXDate(25) ;
                ((String[]) buf[44])[0] = rslt.getString(26, 5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[56]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[59]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[60]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[61]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[65]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[66]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[67]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[68]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[70]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[71]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[72]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[73]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[74]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[75]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[78]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[79]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[80]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[81]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[82]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[83]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[84]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[85]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[86]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[87]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[88]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[89]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[90]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[91]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[92]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[93]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[94]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[95]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[96]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[97]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[98]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[99]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[100]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[101]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[102]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[103]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[104]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[105]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[106]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[107]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[108]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[109]);
                }
                return;
       }
    }

 }

}
