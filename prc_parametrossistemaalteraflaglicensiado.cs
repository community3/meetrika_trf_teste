/*
               File: PRC_ParametrosSistemaAlteraFlagLicensiado
        Description: ParametrosSistemaAlteraFlagLicensiado
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:50.1
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_parametrossistemaalteraflaglicensiado : GXProcedure
   {
      public prc_parametrossistemaalteraflaglicensiado( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_parametrossistemaalteraflaglicensiado( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ParametrosSistema_Codigo ,
                           bool aP1_ParametrosSistema_LicensiadoCadastrado )
      {
         this.A330ParametrosSistema_Codigo = aP0_ParametrosSistema_Codigo;
         this.AV8ParametrosSistema_LicensiadoCadastrado = aP1_ParametrosSistema_LicensiadoCadastrado;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_ParametrosSistema_Codigo ,
                                 bool aP1_ParametrosSistema_LicensiadoCadastrado )
      {
         prc_parametrossistemaalteraflaglicensiado objprc_parametrossistemaalteraflaglicensiado;
         objprc_parametrossistemaalteraflaglicensiado = new prc_parametrossistemaalteraflaglicensiado();
         objprc_parametrossistemaalteraflaglicensiado.A330ParametrosSistema_Codigo = aP0_ParametrosSistema_Codigo;
         objprc_parametrossistemaalteraflaglicensiado.AV8ParametrosSistema_LicensiadoCadastrado = aP1_ParametrosSistema_LicensiadoCadastrado;
         objprc_parametrossistemaalteraflaglicensiado.context.SetSubmitInitialConfig(context);
         objprc_parametrossistemaalteraflaglicensiado.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_parametrossistemaalteraflaglicensiado);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_parametrossistemaalteraflaglicensiado)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Optimized UPDATE. */
         /* Using cursor P00262 */
         pr_default.execute(0, new Object[] {n399ParametrosSistema_LicensiadoCadastrado, AV8ParametrosSistema_LicensiadoCadastrado, A330ParametrosSistema_Codigo});
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("ParametrosSistema") ;
         dsDefault.SmartCacheProvider.SetUpdated("ParametrosSistema") ;
         /* End optimized UPDATE. */
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_ParametrosSistemaAlteraFlagLicensiado");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_parametrossistemaalteraflaglicensiado__default(),
            new Object[][] {
                new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A330ParametrosSistema_Codigo ;
      private bool AV8ParametrosSistema_LicensiadoCadastrado ;
      private bool n399ParametrosSistema_LicensiadoCadastrado ;
      private bool A399ParametrosSistema_LicensiadoCadastrado ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
   }

   public class prc_parametrossistemaalteraflaglicensiado__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new UpdateCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00262 ;
          prmP00262 = new Object[] {
          new Object[] {"@ParametrosSistema_LicensiadoCadastrado",SqlDbType.Bit,4,0} ,
          new Object[] {"@ParametrosSistema_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00262", "UPDATE [ParametrosSistema] SET [ParametrosSistema_LicensiadoCadastrado]=@ParametrosSistema_LicensiadoCadastrado  WHERE [ParametrosSistema_Codigo] = @ParametrosSistema_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00262)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
