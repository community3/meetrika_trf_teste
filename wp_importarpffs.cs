/*
               File: WP_ImportarPFFS
        Description: Importar PF FS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:47:45.37
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_importarpffs : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_importarpffs( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_importarpffs( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavSelcontratada_codigo = new GXCombobox();
         dynavContratoservicos_codigo = new GXCombobox();
         dynavSelcontadorfs_codigo = new GXCombobox();
         cmbavRegradivergencia = new GXCombobox();
         chkavFinal = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSELCONTRATADA_CODIGO") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV69WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvSELCONTRATADA_CODIGOTP2( AV69WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATOSERVICOS_CODIGO") == 0 )
            {
               AV64SelContratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64SelContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64SelContratada_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTRATOSERVICOS_CODIGOTP2( AV64SelContratada_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSELCONTADORFS_CODIGO") == 0 )
            {
               AV64SelContratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64SelContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64SelContratada_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvSELCONTADORFS_CODIGOTP2( AV64SelContratada_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PATP2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTTP2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299474546");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_importarpffs.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vARQUIVO", StringUtil.RTrim( AV7Arquivo));
         GxWebStd.gx_hidden_field( context, "vCOLDMNN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19ColDmnn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLPFBFSN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV27ColPFBFSn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLPFLFSN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31ColPFLFSn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLPFBFMN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV25ColPFBFMn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLPFLFMN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV29ColPFLFMn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLDATACNTN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12ColDataCntn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLSISTEMAN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV71ColSistemaN), 1, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV69WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV69WWPContext);
         }
         GXCCtlgxBlob = "vBLOB" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, AV9Blob);
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL1_Title", StringUtil.RTrim( Confirmpanel1_Title));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL1_Confirmationtext", StringUtil.RTrim( Confirmpanel1_Confirmationtext));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL1_Yesbuttoncaption", StringUtil.RTrim( Confirmpanel1_Yesbuttoncaption));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL1_Nobuttoncaption", StringUtil.RTrim( Confirmpanel1_Nobuttoncaption));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL1_Cancelbuttoncaption", StringUtil.RTrim( Confirmpanel1_Cancelbuttoncaption));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL1_Yesbuttonposition", StringUtil.RTrim( Confirmpanel1_Yesbuttonposition));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL1_Confirmtype", StringUtil.RTrim( Confirmpanel1_Confirmtype));
         GxWebStd.gx_hidden_field( context, "INNEWWINDOW1_Target", StringUtil.RTrim( Innewwindow1_Target));
         GxWebStd.gx_hidden_field( context, "CONFIRMPANEL1_Result", StringUtil.RTrim( Confirmpanel1_Result));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WETP2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTTP2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_importarpffs.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_ImportarPFFS" ;
      }

      public override String GetPgmdesc( )
      {
         return "Importar PF FS" ;
      }

      protected void WBTP0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_TP2( true) ;
         }
         else
         {
            wb_table1_2_TP2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_TP2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "<p>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"INNEWWINDOW1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContadorfs_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33ContadorFS_Codigo), 6, 0, ",", "")), ((edtavContadorfs_codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV33ContadorFS_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV33ContadorFS_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,119);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContadorfs_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavContadorfs_codigo_Visible, edtavContadorfs_codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_WP_ImportarPFFS.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34Contratada_Codigo), 6, 0, ",", "")), ((edtavContratada_codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV34Contratada_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV34Contratada_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,120);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavContratada_codigo_Visible, edtavContratada_codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_WP_ImportarPFFS.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFilename_Internalname, StringUtil.RTrim( AV47FileName), StringUtil.RTrim( context.localUtil.Format( AV47FileName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,121);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFilename_Jsonclick, 0, "Attribute", "", "", "", edtavFilename_Visible, edtavFilename_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</p>") ;
            context.WriteHtmlText( "<p></p>") ;
         }
         wbLoad = true;
      }

      protected void STARTTP2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Importar PF FS", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPTP0( ) ;
      }

      protected void WSTP2( )
      {
         STARTTP2( ) ;
         EVTTP2( ) ;
      }

      protected void EVTTP2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11TP2 */
                              E11TP2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12TP2 */
                              E12TP2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E13TP2 */
                                    E13TP2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14TP2 */
                              E14TP2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WETP2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PATP2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavSelcontratada_codigo.Name = "vSELCONTRATADA_CODIGO";
            dynavSelcontratada_codigo.WebTags = "";
            dynavContratoservicos_codigo.Name = "vCONTRATOSERVICOS_CODIGO";
            dynavContratoservicos_codigo.WebTags = "";
            dynavSelcontadorfs_codigo.Name = "vSELCONTADORFS_CODIGO";
            dynavSelcontadorfs_codigo.WebTags = "";
            cmbavRegradivergencia.Name = "vREGRADIVERGENCIA";
            cmbavRegradivergencia.WebTags = "";
            cmbavRegradivergencia.addItem("C", "Indice de aceita��o de diverg�ncia no contrato", 0);
            cmbavRegradivergencia.addItem("M", "Aceitar menor valor de PF da FS", 0);
            if ( cmbavRegradivergencia.ItemCount > 0 )
            {
               AV59RegraDivergencia = cmbavRegradivergencia.getValidValue(AV59RegraDivergencia);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59RegraDivergencia", AV59RegraDivergencia);
            }
            chkavFinal.Name = "vFINAL";
            chkavFinal.WebTags = "";
            chkavFinal.Caption = "Contagem final para demandas com Diverg�ncia";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavFinal_Internalname, "TitleCaption", chkavFinal.Caption);
            chkavFinal.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavSelcontratada_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         GXVvCONTRATOSERVICOS_CODIGO_htmlTP2( AV64SelContratada_Codigo) ;
         GXVvSELCONTADORFS_CODIGO_htmlTP2( AV64SelContratada_Codigo) ;
         /* End function dynload_actions */
      }

      protected void GXDLVvSELCONTRATADA_CODIGOTP2( wwpbaseobjects.SdtWWPContext AV69WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSELCONTRATADA_CODIGO_dataTP2( AV69WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSELCONTRATADA_CODIGO_htmlTP2( wwpbaseobjects.SdtWWPContext AV69WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvSELCONTRATADA_CODIGO_dataTP2( AV69WWPContext) ;
         gxdynajaxindex = 1;
         dynavSelcontratada_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavSelcontratada_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavSelcontratada_codigo.ItemCount > 0 )
         {
            AV64SelContratada_Codigo = (int)(NumberUtil.Val( dynavSelcontratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV64SelContratada_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64SelContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64SelContratada_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvSELCONTRATADA_CODIGO_dataTP2( wwpbaseobjects.SdtWWPContext AV69WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor H00TP2 */
         pr_default.execute(0, new Object[] {AV69WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00TP2_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00TP2_A41Contratada_PessoaNom[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvCONTRATOSERVICOS_CODIGOTP2( int AV64SelContratada_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATOSERVICOS_CODIGO_dataTP2( AV64SelContratada_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATOSERVICOS_CODIGO_htmlTP2( int AV64SelContratada_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATOSERVICOS_CODIGO_dataTP2( AV64SelContratada_Codigo) ;
         gxdynajaxindex = 1;
         dynavContratoservicos_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratoservicos_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratoservicos_codigo.ItemCount > 0 )
         {
            AV35Contratoservicos_Codigo = (int)(NumberUtil.Val( dynavContratoservicos_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV35Contratoservicos_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Contratoservicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35Contratoservicos_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATOSERVICOS_CODIGO_dataTP2( int AV64SelContratada_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00TP3 */
         pr_default.execute(1, new Object[] {AV64SelContratada_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00TP3_A160ContratoServicos_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00TP3_A826ContratoServicos_ServicoSigla[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXDLVvSELCONTADORFS_CODIGOTP2( int AV64SelContratada_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSELCONTADORFS_CODIGO_dataTP2( AV64SelContratada_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSELCONTADORFS_CODIGO_htmlTP2( int AV64SelContratada_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvSELCONTADORFS_CODIGO_dataTP2( AV64SelContratada_Codigo) ;
         gxdynajaxindex = 1;
         dynavSelcontadorfs_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavSelcontadorfs_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavSelcontadorfs_codigo.ItemCount > 0 )
         {
            AV63SelContadorFS_Codigo = (int)(NumberUtil.Val( dynavSelcontadorfs_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV63SelContadorFS_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63SelContadorFS_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63SelContadorFS_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvSELCONTADORFS_CODIGO_dataTP2( int AV64SelContratada_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00TP4 */
         pr_default.execute(2, new Object[] {AV64SelContratada_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00TP4_A69ContratadaUsuario_UsuarioCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00TP4_A71ContratadaUsuario_UsuarioPessoaNom[0]));
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavSelcontratada_codigo.ItemCount > 0 )
         {
            AV64SelContratada_Codigo = (int)(NumberUtil.Val( dynavSelcontratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV64SelContratada_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64SelContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64SelContratada_Codigo), 6, 0)));
         }
         if ( dynavContratoservicos_codigo.ItemCount > 0 )
         {
            AV35Contratoservicos_Codigo = (int)(NumberUtil.Val( dynavContratoservicos_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV35Contratoservicos_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Contratoservicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35Contratoservicos_Codigo), 6, 0)));
         }
         if ( dynavSelcontadorfs_codigo.ItemCount > 0 )
         {
            AV63SelContadorFS_Codigo = (int)(NumberUtil.Val( dynavSelcontadorfs_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV63SelContadorFS_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63SelContadorFS_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63SelContadorFS_Codigo), 6, 0)));
         }
         if ( cmbavRegradivergencia.ItemCount > 0 )
         {
            AV59RegraDivergencia = cmbavRegradivergencia.getValidValue(AV59RegraDivergencia);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59RegraDivergencia", AV59RegraDivergencia);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFTP2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavContadorfs_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContadorfs_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContadorfs_codigo_Enabled), 5, 0)));
         edtavContratada_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_codigo_Enabled), 5, 0)));
      }

      protected void RFTP2( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E12TP2 */
         E12TP2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E14TP2 */
            E14TP2 ();
            WBTP0( ) ;
         }
      }

      protected void STRUPTP0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavContadorfs_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContadorfs_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContadorfs_codigo_Enabled), 5, 0)));
         edtavContratada_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_codigo_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11TP2 */
         E11TP2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvSELCONTRATADA_CODIGO_htmlTP2( AV69WWPContext) ;
         GXVvCONTRATOSERVICOS_CODIGO_htmlTP2( AV64SelContratada_Codigo) ;
         GXVvSELCONTADORFS_CODIGO_htmlTP2( AV64SelContratada_Codigo) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            dynavSelcontratada_codigo.CurrentValue = cgiGet( dynavSelcontratada_codigo_Internalname);
            AV64SelContratada_Codigo = (int)(NumberUtil.Val( cgiGet( dynavSelcontratada_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64SelContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64SelContratada_Codigo), 6, 0)));
            dynavContratoservicos_codigo.CurrentValue = cgiGet( dynavContratoservicos_codigo_Internalname);
            AV35Contratoservicos_Codigo = (int)(NumberUtil.Val( cgiGet( dynavContratoservicos_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Contratoservicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35Contratoservicos_Codigo), 6, 0)));
            dynavSelcontadorfs_codigo.CurrentValue = cgiGet( dynavSelcontadorfs_codigo_Internalname);
            AV63SelContadorFS_Codigo = (int)(NumberUtil.Val( cgiGet( dynavSelcontadorfs_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63SelContadorFS_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63SelContadorFS_Codigo), 6, 0)));
            cmbavRegradivergencia.CurrentValue = cgiGet( cmbavRegradivergencia_Internalname);
            AV59RegraDivergencia = cgiGet( cmbavRegradivergencia_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59RegraDivergencia", AV59RegraDivergencia);
            if ( context.localUtil.VCDateTime( cgiGet( edtavDatadmn_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data da Demanda"}), 1, "vDATADMN");
               GX_FocusControl = edtavDatadmn_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37DataDmn = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37DataDmn", context.localUtil.Format(AV37DataDmn, "99/99/99"));
            }
            else
            {
               AV37DataDmn = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDatadmn_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37DataDmn", context.localUtil.Format(AV37DataDmn, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDataentrega_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data de Entrega"}), 1, "vDATAENTREGA");
               GX_FocusControl = edtavDataentrega_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38DataEntrega = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DataEntrega", context.localUtil.Format(AV38DataEntrega, "99/99/99"));
            }
            else
            {
               AV38DataEntrega = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDataentrega_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DataEntrega", context.localUtil.Format(AV38DataEntrega, "99/99/99"));
            }
            AV41DemandaFM = cgiGet( edtavDemandafm_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41DemandaFM", AV41DemandaFM);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavPralinha_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPralinha_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPRALINHA");
               GX_FocusControl = edtavPralinha_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV57PraLinha = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57PraLinha", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57PraLinha), 4, 0)));
            }
            else
            {
               AV57PraLinha = (short)(context.localUtil.CToN( cgiGet( edtavPralinha_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57PraLinha", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57PraLinha), 4, 0)));
            }
            AV18ColDmn = StringUtil.Upper( cgiGet( edtavColdmn_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ColDmn", AV18ColDmn);
            AV26ColPFBFS = StringUtil.Upper( cgiGet( edtavColpfbfs_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26ColPFBFS", AV26ColPFBFS);
            AV30ColPFLFS = StringUtil.Upper( cgiGet( edtavColpflfs_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ColPFLFS", AV30ColPFLFS);
            AV70ColSistema = cgiGet( edtavColsistema_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ColSistema", AV70ColSistema);
            AV11ColDataCnt = StringUtil.Upper( cgiGet( edtavColdatacnt_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ColDataCnt", AV11ColDataCnt);
            if ( context.localUtil.VCDateTime( cgiGet( edtavDatacnt_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data Cnt"}), 1, "vDATACNT");
               GX_FocusControl = edtavDatacnt_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36DataCnt = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36DataCnt", context.localUtil.Format(AV36DataCnt, "99/99/99"));
            }
            else
            {
               AV36DataCnt = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDatacnt_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36DataCnt", context.localUtil.Format(AV36DataCnt, "99/99/99"));
            }
            AV50Final = StringUtil.StrToBool( cgiGet( chkavFinal_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50Final", AV50Final);
            AV24ColPFBFM = StringUtil.Upper( cgiGet( edtavColpfbfm_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ColPFBFM", AV24ColPFBFM);
            AV28ColPFLFM = StringUtil.Upper( cgiGet( edtavColpflfm_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ColPFLFM", AV28ColPFLFM);
            AV9Blob = cgiGet( edtavBlob_Internalname);
            AV5Aba = cgiGet( edtavAba_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Aba", AV5Aba);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContadorfs_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContadorfs_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTADORFS_CODIGO");
               GX_FocusControl = edtavContadorfs_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV33ContadorFS_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContadorFS_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33ContadorFS_Codigo), 6, 0)));
            }
            else
            {
               AV33ContadorFS_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavContadorfs_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContadorFS_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33ContadorFS_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratada_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratada_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATADA_CODIGO");
               GX_FocusControl = edtavContratada_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV34Contratada_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Contratada_Codigo), 6, 0)));
            }
            else
            {
               AV34Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavContratada_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Contratada_Codigo), 6, 0)));
            }
            AV47FileName = cgiGet( edtavFilename_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47FileName", AV47FileName);
            /* Read saved values. */
            AV7Arquivo = cgiGet( "vARQUIVO");
            AV19ColDmnn = (short)(context.localUtil.CToN( cgiGet( "vCOLDMNN"), ",", "."));
            AV27ColPFBFSn = (short)(context.localUtil.CToN( cgiGet( "vCOLPFBFSN"), ",", "."));
            AV31ColPFLFSn = (short)(context.localUtil.CToN( cgiGet( "vCOLPFLFSN"), ",", "."));
            AV25ColPFBFMn = (short)(context.localUtil.CToN( cgiGet( "vCOLPFBFMN"), ",", "."));
            AV29ColPFLFMn = (short)(context.localUtil.CToN( cgiGet( "vCOLPFLFMN"), ",", "."));
            AV12ColDataCntn = (short)(context.localUtil.CToN( cgiGet( "vCOLDATACNTN"), ",", "."));
            AV71ColSistemaN = (short)(context.localUtil.CToN( cgiGet( "vCOLSISTEMAN"), ",", "."));
            Confirmpanel1_Title = cgiGet( "CONFIRMPANEL1_Title");
            Confirmpanel1_Confirmationtext = cgiGet( "CONFIRMPANEL1_Confirmationtext");
            Confirmpanel1_Yesbuttoncaption = cgiGet( "CONFIRMPANEL1_Yesbuttoncaption");
            Confirmpanel1_Nobuttoncaption = cgiGet( "CONFIRMPANEL1_Nobuttoncaption");
            Confirmpanel1_Cancelbuttoncaption = cgiGet( "CONFIRMPANEL1_Cancelbuttoncaption");
            Confirmpanel1_Yesbuttonposition = cgiGet( "CONFIRMPANEL1_Yesbuttonposition");
            Confirmpanel1_Confirmtype = cgiGet( "CONFIRMPANEL1_Confirmtype");
            Innewwindow1_Target = cgiGet( "INNEWWINDOW1_Target");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV9Blob)) )
            {
               GXCCtlgxBlob = "vBLOB" + "_gxBlob";
               AV9Blob = cgiGet( GXCCtlgxBlob);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXVvSELCONTRATADA_CODIGO_htmlTP2( AV69WWPContext) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11TP2 */
         E11TP2 ();
         if (returnInSub) return;
      }

      protected void E11TP2( )
      {
         /* Start Routine */
         Form.Meta.addItem("Versao", "1.3 - Data: 21/05/2020 03:23", 0) ;
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV69WWPContext) ;
         AV14ColDem = 0;
         AV57PraLinha = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57PraLinha", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57PraLinha), 4, 0)));
         edtavFilename_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFilename_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFilename_Visible), 5, 0)));
         tblTblpffm_Visible = (AV50Final ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblpffm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblpffm_Visible), 5, 0)));
         edtavContadorfs_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContadorfs_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContadorfs_codigo_Visible), 5, 0)));
         edtavContratada_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_codigo_Visible), 5, 0)));
         if ( AV69WWPContext.gxTpr_Userehadministradorgam )
         {
            cmbavRegradivergencia.addItem("N", "N�o calcular diverg�ncia nem criar OS da FS", 0);
         }
      }

      protected void E12TP2( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV69WWPContext) ;
         bttCarregar_Visible = (AV69WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttCarregar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttCarregar_Visible), 5, 0)));
         AV14ColDem = 0;
         AV63SelContadorFS_Codigo = AV33ContadorFS_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63SelContadorFS_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63SelContadorFS_Codigo), 6, 0)));
         AV64SelContratada_Codigo = AV34Contratada_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64SelContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64SelContratada_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV69WWPContext", AV69WWPContext);
         dynavSelcontadorfs_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV63SelContadorFS_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelcontadorfs_codigo_Internalname, "Values", dynavSelcontadorfs_codigo.ToJavascriptSource());
         dynavSelcontratada_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV64SelContratada_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelcontratada_codigo_Internalname, "Values", dynavSelcontratada_codigo.ToJavascriptSource());
      }

      public void GXEnter( )
      {
         /* Execute user event: E13TP2 */
         E13TP2 ();
         if (returnInSub) return;
      }

      protected void E13TP2( )
      {
         /* Enter Routine */
         AV7Arquivo = AV9Blob;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Arquivo", AV7Arquivo);
         AV54Ok = true;
         if ( (0==AV34Contratada_Codigo) )
         {
            GX_msglist.addItem("Informe a F�brica de Software!");
            AV54Ok = false;
            GX_FocusControl = dynavSelcontratada_codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( (0==AV34Contratada_Codigo) )
         {
            GX_msglist.addItem("Informe o Servi�o da F�brica de Software!");
            AV54Ok = false;
            GX_FocusControl = dynavContratoservicos_codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( (DateTime.MinValue==AV36DataCnt) && String.IsNullOrEmpty(StringUtil.RTrim( AV11ColDataCnt)) )
         {
            GX_msglist.addItem("Informe coluna ou a data da Contagem!");
            AV54Ok = false;
            GX_FocusControl = edtavDatacnt_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( ! (DateTime.MinValue==AV36DataCnt) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV11ColDataCnt)) )
         {
            GX_msglist.addItem("Informe apenas a coluna ou a data da Contagem!");
            AV54Ok = false;
            GX_FocusControl = edtavDatacnt_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( (0==AV57PraLinha) )
         {
            GX_msglist.addItem("Informe a Primeira linha de dados!");
            AV54Ok = false;
            GX_FocusControl = edtavPralinha_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV18ColDmn)) )
         {
            GX_msglist.addItem("Informe a coluna das OS");
            AV54Ok = false;
            GX_FocusControl = edtavColdmn_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV26ColPFBFS)) || String.IsNullOrEmpty(StringUtil.RTrim( AV30ColPFLFS)) )
         {
            GX_msglist.addItem("Informe ambas colunas dos PF da FS!");
            AV54Ok = false;
            GX_FocusControl = edtavColpfbfs_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV70ColSistema)) )
         {
            GX_msglist.addItem("Informe a coluna do Sistema!");
            AV54Ok = false;
         }
         else if ( StringUtil.StringSearch( AV7Arquivo, ".xlsx", 1) == 0 )
         {
            GX_msglist.addItem("Arquivo esperado para processar Excel com extens�o .xlsx!");
            AV54Ok = false;
         }
         if ( AV54Ok )
         {
            AV27ColPFBFSn = (short)(StringUtil.Asc( AV26ColPFBFS)-64);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ColPFBFSn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27ColPFBFSn), 4, 0)));
            AV31ColPFLFSn = (short)(StringUtil.Asc( AV30ColPFLFS)-64);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ColPFLFSn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31ColPFLFSn), 4, 0)));
            AV19ColDmnn = (short)(StringUtil.Asc( AV18ColDmn)-64);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ColDmnn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19ColDmnn), 4, 0)));
            AV25ColPFBFMn = (short)(StringUtil.Asc( AV24ColPFBFM)-64);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ColPFBFMn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25ColPFBFMn), 4, 0)));
            AV29ColPFLFMn = (short)(StringUtil.Asc( AV28ColPFLFM)-64);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ColPFLFMn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29ColPFLFMn), 4, 0)));
            AV12ColDataCntn = (short)(StringUtil.Asc( AV11ColDataCnt)-64);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ColDataCntn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12ColDataCntn), 4, 0)));
            AV71ColSistemaN = (short)(StringUtil.Asc( AV70ColSistema)-64);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ColSistemaN", StringUtil.Str( (decimal)(AV71ColSistemaN), 1, 0));
            if ( AV69WWPContext.gxTpr_Userehadministradorgam && ! ( StringUtil.StrCmp(AV59RegraDivergencia, "N") == 0 ) )
            {
               Confirmpanel1_Title = "Administrador";
               context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Confirmpanel1_Internalname, "Title", Confirmpanel1_Title);
               Confirmpanel1_Confirmationtext = "Confirma usar esse c�lculo de diverg�ncia?";
               context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Confirmpanel1_Internalname, "ConfirmationText", Confirmpanel1_Confirmationtext);
               this.executeUsercontrolMethod("", false, "CONFIRMPANEL1Container", "Confirm", "", new Object[] {});
            }
            else
            {
               /* Execute user subroutine: 'IMPORTARCONTAGENS' */
               S112 ();
               if (returnInSub) return;
            }
         }
      }

      protected void S112( )
      {
         /* 'IMPORTARCONTAGENS' Routine */
         Innewwindow1_Target = formatLink("aprc_importarpffs.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV7Arquivo)) + "," + UrlEncode(StringUtil.RTrim(AV5Aba)) + "," + UrlEncode("" +AV19ColDmnn) + "," + UrlEncode("" +AV57PraLinha) + "," + UrlEncode("" +AV27ColPFBFSn) + "," + UrlEncode("" +AV31ColPFLFSn) + "," + UrlEncode("" +AV25ColPFBFMn) + "," + UrlEncode("" +AV29ColPFLFMn) + "," + UrlEncode(StringUtil.BoolToStr(AV50Final)) + "," + UrlEncode("" +AV34Contratada_Codigo) + "," + UrlEncode("" +AV33ContadorFS_Codigo) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV36DataCnt)) + "," + UrlEncode("" +AV12ColDataCntn) + "," + UrlEncode(StringUtil.RTrim(AV59RegraDivergencia)) + "," + UrlEncode(StringUtil.RTrim(AV47FileName)) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV37DataDmn)) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV38DataEntrega)) + "," + UrlEncode(StringUtil.RTrim(AV41DemandaFM)) + "," + UrlEncode("" +AV71ColSistemaN) + "," + UrlEncode("" +AV35Contratoservicos_Codigo);
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Innewwindow1_Internalname, "Target", Innewwindow1_Target);
         this.executeUsercontrolMethod("", false, "INNEWWINDOW1Container", "OpenWindow", "", new Object[] {});
      }

      protected void nextLoad( )
      {
      }

      protected void E14TP2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_TP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 5, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='Table'>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicostitle_Internalname, "Importa��o dos Pontos de Fun��o da FS:", "", "", lblContratoservicostitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  style=\""+CSSHelper.Prettify( "height:46px")+"\">") ;
            wb_table2_13_TP2( true) ;
         }
         else
         {
            wb_table2_13_TP2( false) ;
         }
         return  ;
      }

      protected void wb_table2_13_TP2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  style=\""+CSSHelper.Prettify( "height:26px")+"\" class='DataContentCell'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            wb_table3_99_TP2( true) ;
         }
         else
         {
            wb_table3_99_TP2( false) ;
         }
         return  ;
      }

      protected void wb_table3_99_TP2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  style=\""+CSSHelper.Prettify( "height:14px")+"\" class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  style=\""+CSSHelper.Prettify( "height:14px")+"\" class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttCarregar_Internalname, "", "Importar", bttCarregar_Jsonclick, 5, "Importar", "", StyleString, ClassString, bttCarregar_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"CONFIRMPANEL1Container"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"CONFIRMPANEL1Container"+"Body"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_TP2e( true) ;
         }
         else
         {
            wb_table1_2_TP2e( false) ;
         }
      }

      protected void wb_table3_99_TP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable3_Internalname, tblTable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            ClassString = "Image";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'',0)\"";
            edtavBlob_Filetype = "tmp";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "Filetype", edtavBlob_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV9Blob)) )
            {
               gxblobfileaux.Source = AV9Blob;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtavBlob_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtavBlob_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  AV9Blob = gxblobfileaux.GetAbsoluteName();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "URL", context.PathToRelativeUrl( AV9Blob));
                  edtavBlob_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "Filetype", edtavBlob_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "URL", context.PathToRelativeUrl( AV9Blob));
            }
            GxWebStd.gx_blob_field( context, edtavBlob_Internalname, StringUtil.RTrim( AV9Blob), context.PathToRelativeUrl( AV9Blob), (String.IsNullOrEmpty(StringUtil.RTrim( edtavBlob_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtavBlob_Filetype)) ? AV9Blob : edtavBlob_Filetype)) : edtavBlob_Contenttype), false, "", edtavBlob_Parameters, 0, 1, 1, "", "", 0, 0, 250, "px", 18, "px", 0, 0, 0, edtavBlob_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,102);\"", "", "", "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock27_Internalname, "Nome da Aba:", "", "", lblTextblock27_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAba_Internalname, StringUtil.RTrim( AV5Aba), StringUtil.RTrim( context.localUtil.Format( AV5Aba, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAba_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_99_TP2e( true) ;
         }
         else
         {
            wb_table3_99_TP2e( false) ;
         }
      }

      protected void wb_table2_13_TP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTblcast_Internalname, tblTblcast_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock29_Internalname, "F�brica de Software:", "", "", lblTextblock29_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"14\"  class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavSelcontratada_codigo, dynavSelcontratada_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV64SelContratada_Codigo), 6, 0)), 1, dynavSelcontratada_codigo_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e15tp1_client"+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_WP_ImportarPFFS.htm");
            dynavSelcontratada_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV64SelContratada_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelcontratada_codigo_Internalname, "Values", (String)(dynavSelcontratada_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock39_Internalname, "Servi�o:", "", "", lblTextblock39_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"14\"  class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratoservicos_codigo, dynavContratoservicos_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV35Contratoservicos_Codigo), 6, 0)), 1, dynavContratoservicos_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", "", true, "HLP_WP_ImportarPFFS.htm");
            dynavContratoservicos_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV35Contratoservicos_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratoservicos_codigo_Internalname, "Values", (String)(dynavContratoservicos_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:36px")+"\" class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock35_Internalname, "Contador FS:", "", "", lblTextblock35_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"14\"  class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavSelcontadorfs_codigo, dynavSelcontadorfs_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV63SelContadorFS_Codigo), 6, 0)), 1, dynavSelcontadorfs_codigo_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e16tp1_client"+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_WP_ImportarPFFS.htm");
            dynavSelcontadorfs_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV63SelContadorFS_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSelcontadorfs_codigo_Internalname, "Values", (String)(dynavSelcontadorfs_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:66px")+"\" class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock38_Internalname, "C�lculo da Diverg�ncia", "", "", lblTextblock38_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"14\"  style=\""+CSSHelper.Prettify( "width:100%")+"\" class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavRegradivergencia, cmbavRegradivergencia_Internalname, StringUtil.RTrim( AV59RegraDivergencia), 1, cmbavRegradivergencia_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WP_ImportarPFFS.htm");
            cmbavRegradivergencia.CurrentValue = StringUtil.RTrim( AV59RegraDivergencia);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRegradivergencia_Internalname, "Values", (String)(cmbavRegradivergencia.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Data da Demanda:", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDatadmn_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDatadmn_Internalname, context.localUtil.Format(AV37DataDmn, "99/99/99"), context.localUtil.Format( AV37DataDmn, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDatadmn_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ImportarPFFS.htm");
            GxWebStd.gx_bitmap( context, edtavDatadmn_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "Data de Entrega:", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDataentrega_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDataentrega_Internalname, context.localUtil.Format(AV38DataEntrega, "99/99/99"), context.localUtil.Format( AV38DataEntrega, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDataentrega_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ImportarPFFS.htm");
            GxWebStd.gx_bitmap( context, edtavDataentrega_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "N�  OS Interna:", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavDemandafm_Internalname, AV41DemandaFM, StringUtil.RTrim( context.localUtil.Format( AV41DemandaFM, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDemandafm_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"20\"  class='DataContentCell'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock28_Internalname, "1� Linha �", "", "", lblTextblock28_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPralinha_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV57PraLinha), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV57PraLinha), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPralinha_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock34_Internalname, "Colunas:  Demandas �", "", "", lblTextblock34_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColdmn_Internalname, StringUtil.RTrim( AV18ColDmn), StringUtil.RTrim( context.localUtil.Format( AV18ColDmn, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,56);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColdmn_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock30_Internalname, "PFB FS �", "", "", lblTextblock30_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColpfbfs_Internalname, StringUtil.RTrim( AV26ColPFBFS), StringUtil.RTrim( context.localUtil.Format( AV26ColPFBFS, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,60);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColpfbfs_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock31_Internalname, "PFL FS �", "", "", lblTextblock31_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColpflfs_Internalname, StringUtil.RTrim( AV30ColPFLFS), StringUtil.RTrim( context.localUtil.Format( AV30ColPFLFS, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColpflfs_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock40_Internalname, "Sistema �", "", "", lblTextblock40_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColsistema_Internalname, StringUtil.RTrim( AV70ColSistema), StringUtil.RTrim( context.localUtil.Format( AV70ColSistema, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,68);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColsistema_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock36_Internalname, "Data da Contagem �", "", "", lblTextblock36_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColdatacnt_Internalname, StringUtil.RTrim( AV11ColDataCnt), StringUtil.RTrim( context.localUtil.Format( AV11ColDataCnt, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,72);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColdatacnt_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock37_Internalname, "Ou:", "", "", lblTextblock37_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDatacnt_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDatacnt_Internalname, context.localUtil.Format(AV36DataCnt, "99/99/99"), context.localUtil.Format( AV36DataCnt, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,76);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDatacnt_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ImportarPFFS.htm");
            GxWebStd.gx_bitmap( context, edtavDatacnt_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"left\" colspan=\"8\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-left;text-align:-moz-left;text-align:-webkit-left")+"\">") ;
            wb_table4_79_TP2( true) ;
         }
         else
         {
            wb_table4_79_TP2( false) ;
         }
         return  ;
      }

      protected void wb_table4_79_TP2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_13_TP2e( true) ;
         }
         else
         {
            wb_table2_13_TP2e( false) ;
         }
      }

      protected void wb_table4_79_TP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:middle")+"\" class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'',0)\"";
            ClassString = "AttSemBorda";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavFinal_Internalname, StringUtil.BoolToStr( AV50Final), "", "", 1, 1, "true", "Contagem final para demandas com Diverg�ncia", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(82, this, 'true', 'false');gx.ajax.executeCliEvent('e17tp1_client',this, event);gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,82);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:middle")+"\" class='DataContentCell'>") ;
            wb_table5_84_TP2( true) ;
         }
         else
         {
            wb_table5_84_TP2( false) ;
         }
         return  ;
      }

      protected void wb_table5_84_TP2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_79_TP2e( true) ;
         }
         else
         {
            wb_table4_79_TP2e( false) ;
         }
      }

      protected void wb_table5_84_TP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblpffm_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblpffm_Internalname, tblTblpffm_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:59px")+"\" class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock33_Internalname, "PFB FM �", "", "", lblTextblock33_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColpfbfm_Internalname, StringUtil.RTrim( AV24ColPFBFM), StringUtil.RTrim( context.localUtil.Format( AV24ColPFBFM, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,89);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColpfbfm_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock32_Internalname, "PFL FM �", "", "", lblTextblock32_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColpflfm_Internalname, StringUtil.RTrim( AV28ColPFLFM), StringUtil.RTrim( context.localUtil.Format( AV28ColPFLFM, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,93);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColpflfm_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ImportarPFFS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_84_TP2e( true) ;
         }
         else
         {
            wb_table5_84_TP2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PATP2( ) ;
         WSTP2( ) ;
         WETP2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299474661");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_importarpffs.js", "?20205299474662");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblContratoservicostitle_Internalname = "CONTRATOSERVICOSTITLE";
         lblTextblock29_Internalname = "TEXTBLOCK29";
         dynavSelcontratada_codigo_Internalname = "vSELCONTRATADA_CODIGO";
         lblTextblock39_Internalname = "TEXTBLOCK39";
         dynavContratoservicos_codigo_Internalname = "vCONTRATOSERVICOS_CODIGO";
         lblTextblock35_Internalname = "TEXTBLOCK35";
         dynavSelcontadorfs_codigo_Internalname = "vSELCONTADORFS_CODIGO";
         lblTextblock38_Internalname = "TEXTBLOCK38";
         cmbavRegradivergencia_Internalname = "vREGRADIVERGENCIA";
         lblTextblock1_Internalname = "TEXTBLOCK1";
         edtavDatadmn_Internalname = "vDATADMN";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         edtavDataentrega_Internalname = "vDATAENTREGA";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         edtavDemandafm_Internalname = "vDEMANDAFM";
         lblTextblock28_Internalname = "TEXTBLOCK28";
         edtavPralinha_Internalname = "vPRALINHA";
         lblTextblock34_Internalname = "TEXTBLOCK34";
         edtavColdmn_Internalname = "vCOLDMN";
         lblTextblock30_Internalname = "TEXTBLOCK30";
         edtavColpfbfs_Internalname = "vCOLPFBFS";
         lblTextblock31_Internalname = "TEXTBLOCK31";
         edtavColpflfs_Internalname = "vCOLPFLFS";
         lblTextblock40_Internalname = "TEXTBLOCK40";
         edtavColsistema_Internalname = "vCOLSISTEMA";
         lblTextblock36_Internalname = "TEXTBLOCK36";
         edtavColdatacnt_Internalname = "vCOLDATACNT";
         lblTextblock37_Internalname = "TEXTBLOCK37";
         edtavDatacnt_Internalname = "vDATACNT";
         chkavFinal_Internalname = "vFINAL";
         lblTextblock33_Internalname = "TEXTBLOCK33";
         edtavColpfbfm_Internalname = "vCOLPFBFM";
         lblTextblock32_Internalname = "TEXTBLOCK32";
         edtavColpflfm_Internalname = "vCOLPFLFM";
         tblTblpffm_Internalname = "TBLPFFM";
         tblTable2_Internalname = "TABLE2";
         tblTblcast_Internalname = "TBLCAST";
         edtavBlob_Internalname = "vBLOB";
         lblTextblock27_Internalname = "TEXTBLOCK27";
         edtavAba_Internalname = "vABA";
         tblTable3_Internalname = "TABLE3";
         bttCarregar_Internalname = "CARREGAR";
         Confirmpanel1_Internalname = "CONFIRMPANEL1";
         tblTable1_Internalname = "TABLE1";
         Innewwindow1_Internalname = "INNEWWINDOW1";
         edtavContadorfs_codigo_Internalname = "vCONTADORFS_CODIGO";
         edtavContratada_codigo_Internalname = "vCONTRATADA_CODIGO";
         edtavFilename_Internalname = "vFILENAME";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavColpflfm_Jsonclick = "";
         edtavColpfbfm_Jsonclick = "";
         edtavDatacnt_Jsonclick = "";
         edtavColdatacnt_Jsonclick = "";
         edtavColsistema_Jsonclick = "";
         edtavColpflfs_Jsonclick = "";
         edtavColpfbfs_Jsonclick = "";
         edtavColdmn_Jsonclick = "";
         edtavPralinha_Jsonclick = "";
         edtavDemandafm_Jsonclick = "";
         edtavDataentrega_Jsonclick = "";
         edtavDatadmn_Jsonclick = "";
         cmbavRegradivergencia_Jsonclick = "";
         dynavSelcontadorfs_codigo_Jsonclick = "";
         dynavContratoservicos_codigo_Jsonclick = "";
         dynavSelcontratada_codigo_Jsonclick = "";
         edtavAba_Jsonclick = "";
         edtavBlob_Jsonclick = "";
         edtavBlob_Parameters = "";
         edtavBlob_Contenttype = "";
         edtavBlob_Filetype = "";
         bttCarregar_Visible = 1;
         tblTblpffm_Visible = 1;
         chkavFinal.Caption = "";
         edtavFilename_Jsonclick = "";
         edtavFilename_Enabled = 1;
         edtavFilename_Visible = 1;
         edtavContratada_codigo_Jsonclick = "";
         edtavContratada_codigo_Enabled = 1;
         edtavContratada_codigo_Visible = 1;
         edtavContadorfs_codigo_Jsonclick = "";
         edtavContadorfs_codigo_Enabled = 1;
         edtavContadorfs_codigo_Visible = 1;
         Innewwindow1_Target = "";
         Confirmpanel1_Confirmtype = "1";
         Confirmpanel1_Yesbuttonposition = "left";
         Confirmpanel1_Cancelbuttoncaption = "Cancelar";
         Confirmpanel1_Nobuttoncaption = "Cancelar";
         Confirmpanel1_Yesbuttoncaption = "Confirmar";
         Confirmpanel1_Confirmationtext = "Confirma usar esse c�lculo de diverg�ncia?";
         Confirmpanel1_Title = "Administrador";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Importar PF FS";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public void Validv_Selcontratada_codigo( GXCombobox dynGX_Parm1 ,
                                               GXCombobox dynGX_Parm2 ,
                                               GXCombobox dynGX_Parm3 )
      {
         dynavSelcontratada_codigo = dynGX_Parm1;
         AV64SelContratada_Codigo = (int)(NumberUtil.Val( dynavSelcontratada_codigo.CurrentValue, "."));
         dynavContratoservicos_codigo = dynGX_Parm2;
         AV35Contratoservicos_Codigo = (int)(NumberUtil.Val( dynavContratoservicos_codigo.CurrentValue, "."));
         dynavSelcontadorfs_codigo = dynGX_Parm3;
         AV63SelContadorFS_Codigo = (int)(NumberUtil.Val( dynavSelcontadorfs_codigo.CurrentValue, "."));
         GXVvCONTRATOSERVICOS_CODIGO_htmlTP2( AV64SelContratada_Codigo) ;
         GXVvSELCONTADORFS_CODIGO_htmlTP2( AV64SelContratada_Codigo) ;
         dynload_actions( ) ;
         if ( dynavContratoservicos_codigo.ItemCount > 0 )
         {
            AV35Contratoservicos_Codigo = (int)(NumberUtil.Val( dynavContratoservicos_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV35Contratoservicos_Codigo), 6, 0))), "."));
         }
         dynavContratoservicos_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV35Contratoservicos_Codigo), 6, 0));
         isValidOutput.Add(dynavContratoservicos_codigo);
         if ( dynavSelcontadorfs_codigo.ItemCount > 0 )
         {
            AV63SelContadorFS_Codigo = (int)(NumberUtil.Val( dynavSelcontadorfs_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV63SelContadorFS_Codigo), 6, 0))), "."));
         }
         dynavSelcontadorfs_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV63SelContadorFS_Codigo), 6, 0));
         isValidOutput.Add(dynavSelcontadorfs_codigo);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'AV33ContadorFS_Codigo',fld:'vCONTADORFS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV34Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV69WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{ctrl:'CARREGAR',prop:'Visible'},{av:'AV63SelContadorFS_Codigo',fld:'vSELCONTADORFS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV64SelContratada_Codigo',fld:'vSELCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VSELCONTRATADA_CODIGO.CLICK","{handler:'E15TP1',iparms:[{av:'AV64SelContratada_Codigo',fld:'vSELCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV34Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VSELCONTADORFS_CODIGO.CLICK","{handler:'E16TP1',iparms:[{av:'AV63SelContadorFS_Codigo',fld:'vSELCONTADORFS_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV33ContadorFS_Codigo',fld:'vCONTADORFS_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("ENTER","{handler:'E13TP2',iparms:[{av:'AV9Blob',fld:'vBLOB',pic:'',nv:''},{av:'AV64SelContratada_Codigo',fld:'vSELCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV35Contratoservicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV34Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11ColDataCnt',fld:'vCOLDATACNT',pic:'@!',nv:''},{av:'AV36DataCnt',fld:'vDATACNT',pic:'',nv:''},{av:'AV57PraLinha',fld:'vPRALINHA',pic:'ZZZ9',nv:0},{av:'AV18ColDmn',fld:'vCOLDMN',pic:'@!',nv:''},{av:'AV30ColPFLFS',fld:'vCOLPFLFS',pic:'@!',nv:''},{av:'AV26ColPFBFS',fld:'vCOLPFBFS',pic:'@!',nv:''},{av:'AV70ColSistema',fld:'vCOLSISTEMA',pic:'',nv:''},{av:'AV24ColPFBFM',fld:'vCOLPFBFM',pic:'@!',nv:''},{av:'AV28ColPFLFM',fld:'vCOLPFLFM',pic:'@!',nv:''},{av:'AV69WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV59RegraDivergencia',fld:'vREGRADIVERGENCIA',pic:'',nv:''},{av:'AV7Arquivo',fld:'vARQUIVO',pic:'',nv:''},{av:'AV5Aba',fld:'vABA',pic:'',nv:''},{av:'AV19ColDmnn',fld:'vCOLDMNN',pic:'ZZZ9',nv:0},{av:'AV27ColPFBFSn',fld:'vCOLPFBFSN',pic:'ZZZ9',nv:0},{av:'AV31ColPFLFSn',fld:'vCOLPFLFSN',pic:'ZZZ9',nv:0},{av:'AV25ColPFBFMn',fld:'vCOLPFBFMN',pic:'ZZZ9',nv:0},{av:'AV29ColPFLFMn',fld:'vCOLPFLFMN',pic:'ZZZ9',nv:0},{av:'AV50Final',fld:'vFINAL',pic:'',nv:false},{av:'AV33ContadorFS_Codigo',fld:'vCONTADORFS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12ColDataCntn',fld:'vCOLDATACNTN',pic:'ZZZ9',nv:0},{av:'AV47FileName',fld:'vFILENAME',pic:'',nv:''},{av:'AV37DataDmn',fld:'vDATADMN',pic:'',nv:''},{av:'AV38DataEntrega',fld:'vDATAENTREGA',pic:'',nv:''},{av:'AV41DemandaFM',fld:'vDEMANDAFM',pic:'',nv:''},{av:'AV71ColSistemaN',fld:'vCOLSISTEMAN',pic:'9',nv:0}],oparms:[{av:'AV7Arquivo',fld:'vARQUIVO',pic:'',nv:''},{av:'AV27ColPFBFSn',fld:'vCOLPFBFSN',pic:'ZZZ9',nv:0},{av:'AV31ColPFLFSn',fld:'vCOLPFLFSN',pic:'ZZZ9',nv:0},{av:'AV19ColDmnn',fld:'vCOLDMNN',pic:'ZZZ9',nv:0},{av:'AV25ColPFBFMn',fld:'vCOLPFBFMN',pic:'ZZZ9',nv:0},{av:'AV29ColPFLFMn',fld:'vCOLPFLFMN',pic:'ZZZ9',nv:0},{av:'AV12ColDataCntn',fld:'vCOLDATACNTN',pic:'ZZZ9',nv:0},{av:'AV71ColSistemaN',fld:'vCOLSISTEMAN',pic:'9',nv:0},{av:'Confirmpanel1_Title',ctrl:'CONFIRMPANEL1',prop:'Title'},{av:'Confirmpanel1_Confirmationtext',ctrl:'CONFIRMPANEL1',prop:'ConfirmationText'},{av:'Innewwindow1_Target',ctrl:'INNEWWINDOW1',prop:'Target'}]}");
         setEventMetadata("VFINAL.CLICK","{handler:'E17TP1',iparms:[{av:'AV50Final',fld:'vFINAL',pic:'',nv:false}],oparms:[{av:'tblTblpffm_Visible',ctrl:'TBLPFFM',prop:'Visible'},{av:'AV24ColPFBFM',fld:'vCOLPFBFM',pic:'@!',nv:''},{av:'AV28ColPFLFM',fld:'vCOLPFLFM',pic:'@!',nv:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Confirmpanel1_Result = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV69WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV7Arquivo = "";
         GXCCtlgxBlob = "";
         AV9Blob = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         AV47FileName = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV59RegraDivergencia = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00TP2_A40Contratada_PessoaCod = new int[1] ;
         H00TP2_A39Contratada_Codigo = new int[1] ;
         H00TP2_A41Contratada_PessoaNom = new String[] {""} ;
         H00TP2_n41Contratada_PessoaNom = new bool[] {false} ;
         H00TP2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00TP2_A516Contratada_TipoFabrica = new String[] {""} ;
         H00TP2_A43Contratada_Ativo = new bool[] {false} ;
         H00TP3_A74Contrato_Codigo = new int[1] ;
         H00TP3_A155Servico_Codigo = new int[1] ;
         H00TP3_A160ContratoServicos_Codigo = new int[1] ;
         H00TP3_A39Contratada_Codigo = new int[1] ;
         H00TP3_A92Contrato_Ativo = new bool[] {false} ;
         H00TP3_A826ContratoServicos_ServicoSigla = new String[] {""} ;
         H00TP4_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00TP4_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00TP4_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00TP4_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00TP4_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H00TP4_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         AV37DataDmn = DateTime.MinValue;
         AV38DataEntrega = DateTime.MinValue;
         AV41DemandaFM = "";
         AV18ColDmn = "";
         AV26ColPFBFS = "";
         AV30ColPFLFS = "";
         AV70ColSistema = "";
         AV11ColDataCnt = "";
         AV36DataCnt = DateTime.MinValue;
         AV24ColPFBFM = "";
         AV28ColPFLFM = "";
         AV5Aba = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblContratoservicostitle_Jsonclick = "";
         bttCarregar_Jsonclick = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         lblTextblock27_Jsonclick = "";
         lblTextblock29_Jsonclick = "";
         lblTextblock39_Jsonclick = "";
         lblTextblock35_Jsonclick = "";
         lblTextblock38_Jsonclick = "";
         lblTextblock1_Jsonclick = "";
         lblTextblock2_Jsonclick = "";
         lblTextblock3_Jsonclick = "";
         lblTextblock28_Jsonclick = "";
         lblTextblock34_Jsonclick = "";
         lblTextblock30_Jsonclick = "";
         lblTextblock31_Jsonclick = "";
         lblTextblock40_Jsonclick = "";
         lblTextblock36_Jsonclick = "";
         lblTextblock37_Jsonclick = "";
         lblTextblock33_Jsonclick = "";
         lblTextblock32_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_importarpffs__default(),
            new Object[][] {
                new Object[] {
               H00TP2_A40Contratada_PessoaCod, H00TP2_A39Contratada_Codigo, H00TP2_A41Contratada_PessoaNom, H00TP2_n41Contratada_PessoaNom, H00TP2_A52Contratada_AreaTrabalhoCod, H00TP2_A516Contratada_TipoFabrica, H00TP2_A43Contratada_Ativo
               }
               , new Object[] {
               H00TP3_A74Contrato_Codigo, H00TP3_A155Servico_Codigo, H00TP3_A160ContratoServicos_Codigo, H00TP3_A39Contratada_Codigo, H00TP3_A92Contrato_Ativo, H00TP3_A826ContratoServicos_ServicoSigla
               }
               , new Object[] {
               H00TP4_A70ContratadaUsuario_UsuarioPessoaCod, H00TP4_n70ContratadaUsuario_UsuarioPessoaCod, H00TP4_A69ContratadaUsuario_UsuarioCod, H00TP4_A71ContratadaUsuario_UsuarioPessoaNom, H00TP4_n71ContratadaUsuario_UsuarioPessoaNom, H00TP4_A66ContratadaUsuario_ContratadaCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavContadorfs_codigo_Enabled = 0;
         edtavContratada_codigo_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short AV19ColDmnn ;
      private short AV27ColPFBFSn ;
      private short AV31ColPFLFSn ;
      private short AV25ColPFBFMn ;
      private short AV29ColPFLFMn ;
      private short AV12ColDataCntn ;
      private short AV71ColSistemaN ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV57PraLinha ;
      private short AV14ColDem ;
      private short nGXWrapped ;
      private short wbTemp ;
      private int AV64SelContratada_Codigo ;
      private int AV33ContadorFS_Codigo ;
      private int edtavContadorfs_codigo_Enabled ;
      private int edtavContadorfs_codigo_Visible ;
      private int AV34Contratada_Codigo ;
      private int edtavContratada_codigo_Enabled ;
      private int edtavContratada_codigo_Visible ;
      private int edtavFilename_Visible ;
      private int edtavFilename_Enabled ;
      private int gxdynajaxindex ;
      private int AV35Contratoservicos_Codigo ;
      private int AV63SelContadorFS_Codigo ;
      private int tblTblpffm_Visible ;
      private int bttCarregar_Visible ;
      private int idxLst ;
      private String Confirmpanel1_Result ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV7Arquivo ;
      private String GXCCtlgxBlob ;
      private String Confirmpanel1_Title ;
      private String Confirmpanel1_Confirmationtext ;
      private String Confirmpanel1_Yesbuttoncaption ;
      private String Confirmpanel1_Nobuttoncaption ;
      private String Confirmpanel1_Cancelbuttoncaption ;
      private String Confirmpanel1_Yesbuttonposition ;
      private String Confirmpanel1_Confirmtype ;
      private String Innewwindow1_Target ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavContadorfs_codigo_Internalname ;
      private String edtavContadorfs_codigo_Jsonclick ;
      private String edtavContratada_codigo_Internalname ;
      private String edtavContratada_codigo_Jsonclick ;
      private String edtavFilename_Internalname ;
      private String AV47FileName ;
      private String edtavFilename_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV59RegraDivergencia ;
      private String chkavFinal_Internalname ;
      private String dynavSelcontratada_codigo_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String dynavContratoservicos_codigo_Internalname ;
      private String dynavSelcontadorfs_codigo_Internalname ;
      private String cmbavRegradivergencia_Internalname ;
      private String edtavDatadmn_Internalname ;
      private String edtavDataentrega_Internalname ;
      private String edtavDemandafm_Internalname ;
      private String edtavPralinha_Internalname ;
      private String AV18ColDmn ;
      private String edtavColdmn_Internalname ;
      private String AV26ColPFBFS ;
      private String edtavColpfbfs_Internalname ;
      private String AV30ColPFLFS ;
      private String edtavColpflfs_Internalname ;
      private String AV70ColSistema ;
      private String edtavColsistema_Internalname ;
      private String AV11ColDataCnt ;
      private String edtavColdatacnt_Internalname ;
      private String edtavDatacnt_Internalname ;
      private String AV24ColPFBFM ;
      private String edtavColpfbfm_Internalname ;
      private String AV28ColPFLFM ;
      private String edtavColpflfm_Internalname ;
      private String edtavBlob_Internalname ;
      private String AV5Aba ;
      private String edtavAba_Internalname ;
      private String tblTblpffm_Internalname ;
      private String bttCarregar_Internalname ;
      private String Confirmpanel1_Internalname ;
      private String Innewwindow1_Internalname ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblContratoservicostitle_Internalname ;
      private String lblContratoservicostitle_Jsonclick ;
      private String bttCarregar_Jsonclick ;
      private String tblTable3_Internalname ;
      private String edtavBlob_Filetype ;
      private String edtavBlob_Contenttype ;
      private String edtavBlob_Parameters ;
      private String edtavBlob_Jsonclick ;
      private String lblTextblock27_Internalname ;
      private String lblTextblock27_Jsonclick ;
      private String edtavAba_Jsonclick ;
      private String tblTblcast_Internalname ;
      private String lblTextblock29_Internalname ;
      private String lblTextblock29_Jsonclick ;
      private String dynavSelcontratada_codigo_Jsonclick ;
      private String lblTextblock39_Internalname ;
      private String lblTextblock39_Jsonclick ;
      private String dynavContratoservicos_codigo_Jsonclick ;
      private String lblTextblock35_Internalname ;
      private String lblTextblock35_Jsonclick ;
      private String dynavSelcontadorfs_codigo_Jsonclick ;
      private String lblTextblock38_Internalname ;
      private String lblTextblock38_Jsonclick ;
      private String cmbavRegradivergencia_Jsonclick ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String edtavDatadmn_Jsonclick ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String edtavDataentrega_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String edtavDemandafm_Jsonclick ;
      private String lblTextblock28_Internalname ;
      private String lblTextblock28_Jsonclick ;
      private String edtavPralinha_Jsonclick ;
      private String lblTextblock34_Internalname ;
      private String lblTextblock34_Jsonclick ;
      private String edtavColdmn_Jsonclick ;
      private String lblTextblock30_Internalname ;
      private String lblTextblock30_Jsonclick ;
      private String edtavColpfbfs_Jsonclick ;
      private String lblTextblock31_Internalname ;
      private String lblTextblock31_Jsonclick ;
      private String edtavColpflfs_Jsonclick ;
      private String lblTextblock40_Internalname ;
      private String lblTextblock40_Jsonclick ;
      private String edtavColsistema_Jsonclick ;
      private String lblTextblock36_Internalname ;
      private String lblTextblock36_Jsonclick ;
      private String edtavColdatacnt_Jsonclick ;
      private String lblTextblock37_Internalname ;
      private String lblTextblock37_Jsonclick ;
      private String edtavDatacnt_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblock33_Internalname ;
      private String lblTextblock33_Jsonclick ;
      private String edtavColpfbfm_Jsonclick ;
      private String lblTextblock32_Internalname ;
      private String lblTextblock32_Jsonclick ;
      private String edtavColpflfm_Jsonclick ;
      private DateTime AV37DataDmn ;
      private DateTime AV38DataEntrega ;
      private DateTime AV36DataCnt ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV50Final ;
      private bool returnInSub ;
      private bool AV54Ok ;
      private String AV41DemandaFM ;
      private String AV9Blob ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private GxFile gxblobfileaux ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavSelcontratada_codigo ;
      private GXCombobox dynavContratoservicos_codigo ;
      private GXCombobox dynavSelcontadorfs_codigo ;
      private GXCombobox cmbavRegradivergencia ;
      private GXCheckbox chkavFinal ;
      private IDataStoreProvider pr_default ;
      private int[] H00TP2_A40Contratada_PessoaCod ;
      private int[] H00TP2_A39Contratada_Codigo ;
      private String[] H00TP2_A41Contratada_PessoaNom ;
      private bool[] H00TP2_n41Contratada_PessoaNom ;
      private int[] H00TP2_A52Contratada_AreaTrabalhoCod ;
      private String[] H00TP2_A516Contratada_TipoFabrica ;
      private bool[] H00TP2_A43Contratada_Ativo ;
      private int[] H00TP3_A74Contrato_Codigo ;
      private int[] H00TP3_A155Servico_Codigo ;
      private int[] H00TP3_A160ContratoServicos_Codigo ;
      private int[] H00TP3_A39Contratada_Codigo ;
      private bool[] H00TP3_A92Contrato_Ativo ;
      private String[] H00TP3_A826ContratoServicos_ServicoSigla ;
      private int[] H00TP4_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00TP4_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H00TP4_A69ContratadaUsuario_UsuarioCod ;
      private String[] H00TP4_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00TP4_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] H00TP4_A66ContratadaUsuario_ContratadaCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV69WWPContext ;
   }

   public class wp_importarpffs__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00TP2 ;
          prmH00TP2 = new Object[] {
          new Object[] {"@AV69WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00TP3 ;
          prmH00TP3 = new Object[] {
          new Object[] {"@AV64SelContratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00TP4 ;
          prmH00TP4 = new Object[] {
          new Object[] {"@AV64SelContratada_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00TP2", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod], T1.[Contratada_TipoFabrica], T1.[Contratada_Ativo] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE (T1.[Contratada_Ativo] = 1) AND (T1.[Contratada_AreaTrabalhoCod] = @AV69WWPC_1Areatrabalho_codigo) AND (T1.[Contratada_TipoFabrica] = 'S') ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TP2,0,0,true,false )
             ,new CursorDef("H00TP3", "SELECT T3.[Contrato_Codigo], T2.[Servico_Codigo], T1.[ContratoServicos_Codigo], T3.[Contratada_Codigo], T3.[Contrato_Ativo], T2.[Servico_Sigla] AS ContratoServicos_ServicoSigla FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T3.[Contrato_Ativo] = 1) AND (T3.[Contratada_Codigo] = @AV64SelContratada_Codigo) ORDER BY [ContratoServicos_ServicoSigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TP3,0,0,true,false )
             ,new CursorDef("H00TP4", "SELECT T3.[Pessoa_Codigo] AS ContratadaUsuario_UsuarioPessoaCod, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPesso, T1.[ContratadaUsuario_ContratadaCod] FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE T1.[ContratadaUsuario_ContratadaCod] = @AV64SelContratada_Codigo ORDER BY T3.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TP4,0,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[6])[0] = rslt.getBool(6) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.getBool(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 15) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
