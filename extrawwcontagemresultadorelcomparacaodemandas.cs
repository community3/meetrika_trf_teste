/*
               File: ExtraWWContagemResultadoRelComparacaoDemandas
        Description: Relat�rio de Compara��o entre Demandas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/14/2020 11:59:52.46
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class extrawwcontagemresultadorelcomparacaodemandas : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public extrawwcontagemresultadorelcomparacaodemandas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public extrawwcontagemresultadorelcomparacaodemandas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavContagemresultado_contratadacod = new GXCombobox();
         dynavContagemresultado_servicogrupo = new GXCombobox();
         dynavContagemresultado_servico = new GXCombobox();
         dynavContagemresultado_cntadaosvinc = new GXCombobox();
         dynavContagemresultado_sergrupovinc = new GXCombobox();
         dynavContagemresultado_codsrvvnc = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavContagemresultado_statusdmn1 = new GXCombobox();
         cmbavContagemresultado_statuscnt1 = new GXCombobox();
         dynavContagemresultado_sistemacod1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavContagemresultado_statusdmn2 = new GXCombobox();
         cmbavContagemresultado_statuscnt2 = new GXCombobox();
         dynavContagemresultado_sistemacod2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         cmbavContagemresultado_statusdmn3 = new GXCombobox();
         cmbavContagemresultado_statuscnt3 = new GXCombobox();
         dynavContagemresultado_sistemacod3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_CONTRATADACOD") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_CONTRATADACODTB2( AV6WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
            {
               AV66ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66ContagemResultado_ContratadaCod), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_SERVICOGRUPOTB2( AV66ContagemResultado_ContratadaCod) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_SERVICO") == 0 )
            {
               AV66ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66ContagemResultado_ContratadaCod), 6, 0)));
               AV67ContagemResultado_ServicoGrupo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ContagemResultado_ServicoGrupo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67ContagemResultado_ServicoGrupo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_SERVICOTB2( AV66ContagemResultado_ContratadaCod, AV67ContagemResultado_ServicoGrupo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_CNTADAOSVINC") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_CNTADAOSVINCTB2( AV6WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_SERGRUPOVINC") == 0 )
            {
               AV69ContagemResultado_CntadaOsVinc = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ContagemResultado_CntadaOsVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69ContagemResultado_CntadaOsVinc), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_SERGRUPOVINCTB2( AV69ContagemResultado_CntadaOsVinc) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_CODSRVVNC") == 0 )
            {
               AV69ContagemResultado_CntadaOsVinc = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ContagemResultado_CntadaOsVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69ContagemResultado_CntadaOsVinc), 6, 0)));
               AV71ContagemResultado_SerGrupoVinc = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ContagemResultado_SerGrupoVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71ContagemResultado_SerGrupoVinc), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_CODSRVVNCTB2( AV69ContagemResultado_CntadaOsVinc, AV71ContagemResultado_SerGrupoVinc) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_SISTEMACOD1") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_SISTEMACOD1TB2( AV6WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_SISTEMACOD2") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_SISTEMACOD2TB2( AV6WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_SISTEMACOD3") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_SISTEMACOD3TB2( AV6WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_206 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_206_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_206_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV67ContagemResultado_ServicoGrupo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ContagemResultado_ServicoGrupo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67ContagemResultado_ServicoGrupo), 6, 0)));
               AV68ContagemResultado_Servico = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68ContagemResultado_Servico), 6, 0)));
               AV69ContagemResultado_CntadaOsVinc = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ContagemResultado_CntadaOsVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69ContagemResultado_CntadaOsVinc), 6, 0)));
               AV71ContagemResultado_SerGrupoVinc = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ContagemResultado_SerGrupoVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71ContagemResultado_SerGrupoVinc), 6, 0)));
               AV70ContagemResultado_CodSrvVnc = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ContagemResultado_CodSrvVnc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV70ContagemResultado_CodSrvVnc), 6, 0)));
               AV14DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
               AV15DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0)));
               AV16ContagemResultado_DemandaFM1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultado_DemandaFM1", AV16ContagemResultado_DemandaFM1);
               AV17ContagemResultado_DataDmn1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultado_DataDmn1", context.localUtil.Format(AV17ContagemResultado_DataDmn1, "99/99/99"));
               AV18ContagemResultado_DataDmn_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultado_DataDmn_To1", context.localUtil.Format(AV18ContagemResultado_DataDmn_To1, "99/99/99"));
               AV19ContagemResultado_DataEntregaReal1 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultado_DataEntregaReal1", context.localUtil.TToC( AV19ContagemResultado_DataEntregaReal1, 8, 5, 0, 3, "/", ":", " "));
               AV20ContagemResultado_DataEntregaReal_To1 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultado_DataEntregaReal_To1", context.localUtil.TToC( AV20ContagemResultado_DataEntregaReal_To1, 8, 5, 0, 3, "/", ":", " "));
               AV21ContagemResultado_StatusDmn1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultado_StatusDmn1", AV21ContagemResultado_StatusDmn1);
               AV22ContagemResultado_StatusCnt1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemResultado_StatusCnt1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22ContagemResultado_StatusCnt1), 2, 0)));
               AV23ContagemResultado_Descricao1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultado_Descricao1", AV23ContagemResultado_Descricao1);
               AV24ContagemResultado_SistemaCod1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultado_SistemaCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24ContagemResultado_SistemaCod1), 6, 0)));
               AV25ContagemResultado_Agrupador1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContagemResultado_Agrupador1", AV25ContagemResultado_Agrupador1);
               AV27DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector2", AV27DynamicFiltersSelector2);
               AV28DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator2), 4, 0)));
               AV29ContagemResultado_DemandaFM2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultado_DemandaFM2", AV29ContagemResultado_DemandaFM2);
               AV30ContagemResultado_DataDmn2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultado_DataDmn2", context.localUtil.Format(AV30ContagemResultado_DataDmn2, "99/99/99"));
               AV31ContagemResultado_DataDmn_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContagemResultado_DataDmn_To2", context.localUtil.Format(AV31ContagemResultado_DataDmn_To2, "99/99/99"));
               AV32ContagemResultado_DataEntregaReal2 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultado_DataEntregaReal2", context.localUtil.TToC( AV32ContagemResultado_DataEntregaReal2, 8, 5, 0, 3, "/", ":", " "));
               AV33ContagemResultado_DataEntregaReal_To2 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultado_DataEntregaReal_To2", context.localUtil.TToC( AV33ContagemResultado_DataEntregaReal_To2, 8, 5, 0, 3, "/", ":", " "));
               AV34ContagemResultado_StatusDmn2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultado_StatusDmn2", AV34ContagemResultado_StatusDmn2);
               AV35ContagemResultado_StatusCnt2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ContagemResultado_StatusCnt2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35ContagemResultado_StatusCnt2), 2, 0)));
               AV36ContagemResultado_Descricao2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ContagemResultado_Descricao2", AV36ContagemResultado_Descricao2);
               AV37ContagemResultado_SistemaCod2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContagemResultado_SistemaCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37ContagemResultado_SistemaCod2), 6, 0)));
               AV38ContagemResultado_Agrupador2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContagemResultado_Agrupador2", AV38ContagemResultado_Agrupador2);
               AV40DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40DynamicFiltersSelector3", AV40DynamicFiltersSelector3);
               AV41DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41DynamicFiltersOperator3), 4, 0)));
               AV42ContagemResultado_DemandaFM3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ContagemResultado_DemandaFM3", AV42ContagemResultado_DemandaFM3);
               AV43ContagemResultado_DataDmn3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ContagemResultado_DataDmn3", context.localUtil.Format(AV43ContagemResultado_DataDmn3, "99/99/99"));
               AV44ContagemResultado_DataDmn_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ContagemResultado_DataDmn_To3", context.localUtil.Format(AV44ContagemResultado_DataDmn_To3, "99/99/99"));
               AV45ContagemResultado_DataEntregaReal3 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ContagemResultado_DataEntregaReal3", context.localUtil.TToC( AV45ContagemResultado_DataEntregaReal3, 8, 5, 0, 3, "/", ":", " "));
               AV46ContagemResultado_DataEntregaReal_To3 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ContagemResultado_DataEntregaReal_To3", context.localUtil.TToC( AV46ContagemResultado_DataEntregaReal_To3, 8, 5, 0, 3, "/", ":", " "));
               AV47ContagemResultado_StatusDmn3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ContagemResultado_StatusDmn3", AV47ContagemResultado_StatusDmn3);
               AV48ContagemResultado_StatusCnt3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ContagemResultado_StatusCnt3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48ContagemResultado_StatusCnt3), 2, 0)));
               AV49ContagemResultado_Descricao3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ContagemResultado_Descricao3", AV49ContagemResultado_Descricao3);
               AV50ContagemResultado_SistemaCod3 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ContagemResultado_SistemaCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50ContagemResultado_SistemaCod3), 6, 0)));
               AV51ContagemResultado_Agrupador3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ContagemResultado_Agrupador3", AV51ContagemResultado_Agrupador3);
               AV26DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled2", AV26DynamicFiltersEnabled2);
               AV39DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39DynamicFiltersEnabled3", AV39DynamicFiltersEnabled3);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               AV13Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Contratada_AreaTrabalhoCod), 6, 0)));
               AV65ContagemResultado_OSVinculada = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65ContagemResultado_OSVinculada), 6, 0)));
               AV66ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66ContagemResultado_ContratadaCod), 6, 0)));
               AV122Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV53DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53DynamicFiltersIgnoreFirst", AV53DynamicFiltersIgnoreFirst);
               AV52DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52DynamicFiltersRemoving", AV52DynamicFiltersRemoving);
               A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV67ContagemResultado_ServicoGrupo, AV68ContagemResultado_Servico, AV69ContagemResultado_CntadaOsVinc, AV71ContagemResultado_SerGrupoVinc, AV70ContagemResultado_CodSrvVnc, AV14DynamicFiltersSelector1, AV15DynamicFiltersOperator1, AV16ContagemResultado_DemandaFM1, AV17ContagemResultado_DataDmn1, AV18ContagemResultado_DataDmn_To1, AV19ContagemResultado_DataEntregaReal1, AV20ContagemResultado_DataEntregaReal_To1, AV21ContagemResultado_StatusDmn1, AV22ContagemResultado_StatusCnt1, AV23ContagemResultado_Descricao1, AV24ContagemResultado_SistemaCod1, AV25ContagemResultado_Agrupador1, AV27DynamicFiltersSelector2, AV28DynamicFiltersOperator2, AV29ContagemResultado_DemandaFM2, AV30ContagemResultado_DataDmn2, AV31ContagemResultado_DataDmn_To2, AV32ContagemResultado_DataEntregaReal2, AV33ContagemResultado_DataEntregaReal_To2, AV34ContagemResultado_StatusDmn2, AV35ContagemResultado_StatusCnt2, AV36ContagemResultado_Descricao2, AV37ContagemResultado_SistemaCod2, AV38ContagemResultado_Agrupador2, AV40DynamicFiltersSelector3, AV41DynamicFiltersOperator3, AV42ContagemResultado_DemandaFM3, AV43ContagemResultado_DataDmn3, AV44ContagemResultado_DataDmn_To3, AV45ContagemResultado_DataEntregaReal3, AV46ContagemResultado_DataEntregaReal_To3, AV47ContagemResultado_StatusDmn3, AV48ContagemResultado_StatusCnt3, AV49ContagemResultado_Descricao3, AV50ContagemResultado_SistemaCod3, AV51ContagemResultado_Agrupador3, AV26DynamicFiltersEnabled2, AV39DynamicFiltersEnabled3, AV6WWPContext, AV13Contratada_AreaTrabalhoCod, AV65ContagemResultado_OSVinculada, AV66ContagemResultado_ContratadaCod, AV122Pgmname, AV10GridState, AV53DynamicFiltersIgnoreFirst, AV52DynamicFiltersRemoving, A456ContagemResultado_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PATB2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTTB2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202041411595299");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("extrawwcontagemresultadorelcomparacaodemandas.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_SERVICOGRUPO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV67ContagemResultado_ServicoGrupo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_SERVICO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV68ContagemResultado_Servico), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_CNTADAOSVINC", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV69ContagemResultado_CntadaOsVinc), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_SERGRUPOVINC", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV71ContagemResultado_SerGrupoVinc), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_CODSRVVNC", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV70ContagemResultado_CodSrvVnc), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV14DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DEMANDAFM1", AV16ContagemResultado_DemandaFM1);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DATADMN1", context.localUtil.Format(AV17ContagemResultado_DataDmn1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DATADMN_TO1", context.localUtil.Format(AV18ContagemResultado_DataDmn_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DATAENTREGAREAL1", context.localUtil.TToC( AV19ContagemResultado_DataEntregaReal1, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1", context.localUtil.TToC( AV20ContagemResultado_DataEntregaReal_To1, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_STATUSDMN1", StringUtil.RTrim( AV21ContagemResultado_StatusDmn1));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_STATUSCNT1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22ContagemResultado_StatusCnt1), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DESCRICAO1", AV23ContagemResultado_Descricao1);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_SISTEMACOD1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24ContagemResultado_SistemaCod1), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_AGRUPADOR1", StringUtil.RTrim( AV25ContagemResultado_Agrupador1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV27DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV28DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DEMANDAFM2", AV29ContagemResultado_DemandaFM2);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DATADMN2", context.localUtil.Format(AV30ContagemResultado_DataDmn2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DATADMN_TO2", context.localUtil.Format(AV31ContagemResultado_DataDmn_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DATAENTREGAREAL2", context.localUtil.TToC( AV32ContagemResultado_DataEntregaReal2, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2", context.localUtil.TToC( AV33ContagemResultado_DataEntregaReal_To2, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_STATUSDMN2", StringUtil.RTrim( AV34ContagemResultado_StatusDmn2));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_STATUSCNT2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35ContagemResultado_StatusCnt2), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DESCRICAO2", AV36ContagemResultado_Descricao2);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_SISTEMACOD2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37ContagemResultado_SistemaCod2), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_AGRUPADOR2", StringUtil.RTrim( AV38ContagemResultado_Agrupador2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV40DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV41DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DEMANDAFM3", AV42ContagemResultado_DemandaFM3);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DATADMN3", context.localUtil.Format(AV43ContagemResultado_DataDmn3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DATADMN_TO3", context.localUtil.Format(AV44ContagemResultado_DataDmn_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DATAENTREGAREAL3", context.localUtil.TToC( AV45ContagemResultado_DataEntregaReal3, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3", context.localUtil.TToC( AV46ContagemResultado_DataEntregaReal_To3, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_STATUSDMN3", StringUtil.RTrim( AV47ContagemResultado_StatusDmn3));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_STATUSCNT3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV48ContagemResultado_StatusCnt3), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DESCRICAO3", AV49ContagemResultado_Descricao3);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_SISTEMACOD3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV50ContagemResultado_SistemaCod3), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_AGRUPADOR3", StringUtil.RTrim( AV51ContagemResultado_Agrupador3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV26DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV39DynamicFiltersEnabled3));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_206", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_206), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV62GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV63GridPageCount), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV122Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV53DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV52DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vTODAY", context.localUtil.DToC( Gx_date, 0, "/"));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WETB2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTTB2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("extrawwcontagemresultadorelcomparacaodemandas.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "ExtraWWContagemResultadoRelComparacaoDemandas" ;
      }

      public override String GetPgmdesc( )
      {
         return "Relat�rio de Compara��o entre Demandas" ;
      }

      protected void WBTB0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_TB2( true) ;
         }
         else
         {
            wb_table1_2_TB2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_TB2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 219,'',false,'" + sGXsfl_206_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV26DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(219, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,219);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 220,'',false,'" + sGXsfl_206_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV39DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(220, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,220);\"");
         }
         wbLoad = true;
      }

      protected void STARTTB2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Relat�rio de Compara��o entre Demandas", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPTB0( ) ;
      }

      protected void WSTB2( )
      {
         STARTTB2( ) ;
         EVTTB2( ) ;
      }

      protected void EVTTB2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11TB2 */
                              E11TB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12TB2 */
                              E12TB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13TB2 */
                              E13TB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14TB2 */
                              E14TB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15TB2 */
                              E15TB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16TB2 */
                              E16TB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSOPERATOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17TB2 */
                              E17TB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18TB2 */
                              E18TB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19TB2 */
                              E19TB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSOPERATOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20TB2 */
                              E20TB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21TB2 */
                              E21TB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSOPERATOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22TB2 */
                              E22TB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_206_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_206_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_206_idx), 4, 0)), 4, "0");
                              SubsflControlProps_2062( ) ;
                              AV64Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV64Display)) ? AV120Display_GXI : context.convertURL( context.PathToRelativeUrl( AV64Display))));
                              A456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", "."));
                              A493ContagemResultado_DemandaFM = cgiGet( edtContagemResultado_DemandaFM_Internalname);
                              n493ContagemResultado_DemandaFM = false;
                              A457ContagemResultado_Demanda = StringUtil.Upper( cgiGet( edtContagemResultado_Demanda_Internalname));
                              n457ContagemResultado_Demanda = false;
                              A471ContagemResultado_DataDmn = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContagemResultado_DataDmn_Internalname), 0));
                              A2017ContagemResultado_DataEntregaReal = context.localUtil.CToT( cgiGet( edtContagemResultado_DataEntregaReal_Internalname), 0);
                              n2017ContagemResultado_DataEntregaReal = false;
                              A494ContagemResultado_Descricao = cgiGet( edtContagemResultado_Descricao_Internalname);
                              n494ContagemResultado_Descricao = false;
                              A509ContagemrResultado_SistemaSigla = StringUtil.Upper( cgiGet( edtContagemrResultado_SistemaSigla_Internalname));
                              n509ContagemrResultado_SistemaSigla = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E23TB2 */
                                    E23TB2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E24TB2 */
                                    E24TB2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E25TB2 */
                                    E25TB2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Contagemresultado_servicogrupo Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_SERVICOGRUPO"), ",", ".") != Convert.ToDecimal( AV67ContagemResultado_ServicoGrupo )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_servico Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_SERVICO"), ",", ".") != Convert.ToDecimal( AV68ContagemResultado_Servico )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_cntadaosvinc Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_CNTADAOSVINC"), ",", ".") != Convert.ToDecimal( AV69ContagemResultado_CntadaOsVinc )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_sergrupovinc Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_SERGRUPOVINC"), ",", ".") != Convert.ToDecimal( AV71ContagemResultado_SerGrupoVinc )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_codsrvvnc Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_CODSRVVNC"), ",", ".") != Convert.ToDecimal( AV70ContagemResultado_CodSrvVnc )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV14DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV15DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_demandafm1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDAFM1"), AV16ContagemResultado_DemandaFM1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_datadmn1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATADMN1"), 0) != AV17ContagemResultado_DataDmn1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_datadmn_to1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATADMN_TO1"), 0) != AV18ContagemResultado_DataDmn_To1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_dataentregareal1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATAENTREGAREAL1"), 0) != AV19ContagemResultado_DataEntregaReal1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_dataentregareal_to1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1"), 0) != AV20ContagemResultado_DataEntregaReal_To1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_statusdmn1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_STATUSDMN1"), AV21ContagemResultado_StatusDmn1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_statuscnt1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_STATUSCNT1"), ",", ".") != Convert.ToDecimal( AV22ContagemResultado_StatusCnt1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_descricao1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DESCRICAO1"), AV23ContagemResultado_Descricao1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_sistemacod1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_SISTEMACOD1"), ",", ".") != Convert.ToDecimal( AV24ContagemResultado_SistemaCod1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_agrupador1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_AGRUPADOR1"), AV25ContagemResultado_Agrupador1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV27DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV28DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_demandafm2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDAFM2"), AV29ContagemResultado_DemandaFM2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_datadmn2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATADMN2"), 0) != AV30ContagemResultado_DataDmn2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_datadmn_to2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATADMN_TO2"), 0) != AV31ContagemResultado_DataDmn_To2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_dataentregareal2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATAENTREGAREAL2"), 0) != AV32ContagemResultado_DataEntregaReal2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_dataentregareal_to2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2"), 0) != AV33ContagemResultado_DataEntregaReal_To2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_statusdmn2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_STATUSDMN2"), AV34ContagemResultado_StatusDmn2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_statuscnt2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_STATUSCNT2"), ",", ".") != Convert.ToDecimal( AV35ContagemResultado_StatusCnt2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_descricao2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DESCRICAO2"), AV36ContagemResultado_Descricao2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_sistemacod2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_SISTEMACOD2"), ",", ".") != Convert.ToDecimal( AV37ContagemResultado_SistemaCod2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_agrupador2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_AGRUPADOR2"), AV38ContagemResultado_Agrupador2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV40DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV41DynamicFiltersOperator3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_demandafm3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDAFM3"), AV42ContagemResultado_DemandaFM3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_datadmn3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATADMN3"), 0) != AV43ContagemResultado_DataDmn3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_datadmn_to3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATADMN_TO3"), 0) != AV44ContagemResultado_DataDmn_To3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_dataentregareal3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATAENTREGAREAL3"), 0) != AV45ContagemResultado_DataEntregaReal3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_dataentregareal_to3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3"), 0) != AV46ContagemResultado_DataEntregaReal_To3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_statusdmn3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_STATUSDMN3"), AV47ContagemResultado_StatusDmn3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_statuscnt3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_STATUSCNT3"), ",", ".") != Convert.ToDecimal( AV48ContagemResultado_StatusCnt3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_descricao3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DESCRICAO3"), AV49ContagemResultado_Descricao3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_sistemacod3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_SISTEMACOD3"), ",", ".") != Convert.ToDecimal( AV50ContagemResultado_SistemaCod3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contagemresultado_agrupador3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_AGRUPADOR3"), AV51ContagemResultado_Agrupador3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV26DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV39DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WETB2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PATB2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavContagemresultado_contratadacod.Name = "vCONTAGEMRESULTADO_CONTRATADACOD";
            dynavContagemresultado_contratadacod.WebTags = "";
            dynavContagemresultado_servicogrupo.Name = "vCONTAGEMRESULTADO_SERVICOGRUPO";
            dynavContagemresultado_servicogrupo.WebTags = "";
            dynavContagemresultado_servico.Name = "vCONTAGEMRESULTADO_SERVICO";
            dynavContagemresultado_servico.WebTags = "";
            dynavContagemresultado_cntadaosvinc.Name = "vCONTAGEMRESULTADO_CNTADAOSVINC";
            dynavContagemresultado_cntadaosvinc.WebTags = "";
            dynavContagemresultado_sergrupovinc.Name = "vCONTAGEMRESULTADO_SERGRUPOVINC";
            dynavContagemresultado_sergrupovinc.WebTags = "";
            dynavContagemresultado_codsrvvnc.Name = "vCONTAGEMRESULTADO_CODSRVVNC";
            dynavContagemresultado_codsrvvnc.WebTags = "";
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADO_DEMANDAFM", "OS / OS Refer�ncia", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADO_DATADMN", "Data da Demanda", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADO_DATAENTREGAREAL", "Data de Entrega", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADO_STATUSDMN", "Status da Demanda", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADO_STATUSCNT", "Status do Servi�o", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADO_DESCRICAO", "T�tulo", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADO_SISTEMACOD", "Sistema", 0);
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADO_AGRUPADOR", "Agrupador", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV14DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV14DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Igual", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV15DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0)));
            }
            cmbavContagemresultado_statusdmn1.Name = "vCONTAGEMRESULTADO_STATUSDMN1";
            cmbavContagemresultado_statusdmn1.WebTags = "";
            cmbavContagemresultado_statusdmn1.addItem("", "Todos", 0);
            cmbavContagemresultado_statusdmn1.addItem("B", "Stand by", 0);
            cmbavContagemresultado_statusdmn1.addItem("S", "Solicitada", 0);
            cmbavContagemresultado_statusdmn1.addItem("E", "Em An�lise", 0);
            cmbavContagemresultado_statusdmn1.addItem("A", "Em execu��o", 0);
            cmbavContagemresultado_statusdmn1.addItem("R", "Resolvida", 0);
            cmbavContagemresultado_statusdmn1.addItem("C", "Conferida", 0);
            cmbavContagemresultado_statusdmn1.addItem("D", "Rejeitada", 0);
            cmbavContagemresultado_statusdmn1.addItem("H", "Homologada", 0);
            cmbavContagemresultado_statusdmn1.addItem("O", "Aceite", 0);
            cmbavContagemresultado_statusdmn1.addItem("P", "A Pagar", 0);
            cmbavContagemresultado_statusdmn1.addItem("L", "Liquidada", 0);
            cmbavContagemresultado_statusdmn1.addItem("X", "Cancelada", 0);
            cmbavContagemresultado_statusdmn1.addItem("N", "N�o Faturada", 0);
            cmbavContagemresultado_statusdmn1.addItem("J", "Planejamento", 0);
            cmbavContagemresultado_statusdmn1.addItem("I", "An�lise Planejamento", 0);
            cmbavContagemresultado_statusdmn1.addItem("T", "Validacao T�cnica", 0);
            cmbavContagemresultado_statusdmn1.addItem("Q", "Validacao Qualidade", 0);
            cmbavContagemresultado_statusdmn1.addItem("G", "Em Homologa��o", 0);
            cmbavContagemresultado_statusdmn1.addItem("M", "Valida��o Mensura��o", 0);
            cmbavContagemresultado_statusdmn1.addItem("U", "Rascunho", 0);
            if ( cmbavContagemresultado_statusdmn1.ItemCount > 0 )
            {
               AV21ContagemResultado_StatusDmn1 = cmbavContagemresultado_statusdmn1.getValidValue(AV21ContagemResultado_StatusDmn1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultado_StatusDmn1", AV21ContagemResultado_StatusDmn1);
            }
            cmbavContagemresultado_statuscnt1.Name = "vCONTAGEMRESULTADO_STATUSCNT1";
            cmbavContagemresultado_statuscnt1.WebTags = "";
            cmbavContagemresultado_statuscnt1.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 2, 0)), "Todos", 0);
            cmbavContagemresultado_statuscnt1.addItem("1", "Inicial", 0);
            cmbavContagemresultado_statuscnt1.addItem("2", "Auditoria", 0);
            cmbavContagemresultado_statuscnt1.addItem("3", "Negociac�o", 0);
            cmbavContagemresultado_statuscnt1.addItem("4", "N�o Aprovada", 0);
            cmbavContagemresultado_statuscnt1.addItem("5", "Aprovada", 0);
            cmbavContagemresultado_statuscnt1.addItem("6", "Pend�ncias", 0);
            cmbavContagemresultado_statuscnt1.addItem("7", "Diverg�ncia", 0);
            if ( cmbavContagemresultado_statuscnt1.ItemCount > 0 )
            {
               AV22ContagemResultado_StatusCnt1 = (short)(NumberUtil.Val( cmbavContagemresultado_statuscnt1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22ContagemResultado_StatusCnt1), 2, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemResultado_StatusCnt1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22ContagemResultado_StatusCnt1), 2, 0)));
            }
            dynavContagemresultado_sistemacod1.Name = "vCONTAGEMRESULTADO_SISTEMACOD1";
            dynavContagemresultado_sistemacod1.WebTags = "";
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADO_DEMANDAFM", "OS / OS Refer�ncia", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADO_DATADMN", "Data da Demanda", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADO_DATAENTREGAREAL", "Data de Entrega", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADO_STATUSDMN", "Status da Demanda", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADO_STATUSCNT", "Status do Servi�o", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADO_DESCRICAO", "T�tulo", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADO_SISTEMACOD", "Sistema", 0);
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADO_AGRUPADOR", "Agrupador", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV27DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV27DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector2", AV27DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Igual", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("2", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV28DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator2), 4, 0)));
            }
            cmbavContagemresultado_statusdmn2.Name = "vCONTAGEMRESULTADO_STATUSDMN2";
            cmbavContagemresultado_statusdmn2.WebTags = "";
            cmbavContagemresultado_statusdmn2.addItem("", "Todos", 0);
            cmbavContagemresultado_statusdmn2.addItem("B", "Stand by", 0);
            cmbavContagemresultado_statusdmn2.addItem("S", "Solicitada", 0);
            cmbavContagemresultado_statusdmn2.addItem("E", "Em An�lise", 0);
            cmbavContagemresultado_statusdmn2.addItem("A", "Em execu��o", 0);
            cmbavContagemresultado_statusdmn2.addItem("R", "Resolvida", 0);
            cmbavContagemresultado_statusdmn2.addItem("C", "Conferida", 0);
            cmbavContagemresultado_statusdmn2.addItem("D", "Rejeitada", 0);
            cmbavContagemresultado_statusdmn2.addItem("H", "Homologada", 0);
            cmbavContagemresultado_statusdmn2.addItem("O", "Aceite", 0);
            cmbavContagemresultado_statusdmn2.addItem("P", "A Pagar", 0);
            cmbavContagemresultado_statusdmn2.addItem("L", "Liquidada", 0);
            cmbavContagemresultado_statusdmn2.addItem("X", "Cancelada", 0);
            cmbavContagemresultado_statusdmn2.addItem("N", "N�o Faturada", 0);
            cmbavContagemresultado_statusdmn2.addItem("J", "Planejamento", 0);
            cmbavContagemresultado_statusdmn2.addItem("I", "An�lise Planejamento", 0);
            cmbavContagemresultado_statusdmn2.addItem("T", "Validacao T�cnica", 0);
            cmbavContagemresultado_statusdmn2.addItem("Q", "Validacao Qualidade", 0);
            cmbavContagemresultado_statusdmn2.addItem("G", "Em Homologa��o", 0);
            cmbavContagemresultado_statusdmn2.addItem("M", "Valida��o Mensura��o", 0);
            cmbavContagemresultado_statusdmn2.addItem("U", "Rascunho", 0);
            if ( cmbavContagemresultado_statusdmn2.ItemCount > 0 )
            {
               AV34ContagemResultado_StatusDmn2 = cmbavContagemresultado_statusdmn2.getValidValue(AV34ContagemResultado_StatusDmn2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultado_StatusDmn2", AV34ContagemResultado_StatusDmn2);
            }
            cmbavContagemresultado_statuscnt2.Name = "vCONTAGEMRESULTADO_STATUSCNT2";
            cmbavContagemresultado_statuscnt2.WebTags = "";
            cmbavContagemresultado_statuscnt2.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 2, 0)), "Todos", 0);
            cmbavContagemresultado_statuscnt2.addItem("1", "Inicial", 0);
            cmbavContagemresultado_statuscnt2.addItem("2", "Auditoria", 0);
            cmbavContagemresultado_statuscnt2.addItem("3", "Negociac�o", 0);
            cmbavContagemresultado_statuscnt2.addItem("4", "N�o Aprovada", 0);
            cmbavContagemresultado_statuscnt2.addItem("5", "Aprovada", 0);
            cmbavContagemresultado_statuscnt2.addItem("6", "Pend�ncias", 0);
            cmbavContagemresultado_statuscnt2.addItem("7", "Diverg�ncia", 0);
            if ( cmbavContagemresultado_statuscnt2.ItemCount > 0 )
            {
               AV35ContagemResultado_StatusCnt2 = (short)(NumberUtil.Val( cmbavContagemresultado_statuscnt2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV35ContagemResultado_StatusCnt2), 2, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ContagemResultado_StatusCnt2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35ContagemResultado_StatusCnt2), 2, 0)));
            }
            dynavContagemresultado_sistemacod2.Name = "vCONTAGEMRESULTADO_SISTEMACOD2";
            dynavContagemresultado_sistemacod2.WebTags = "";
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADO_DEMANDAFM", "OS / OS Refer�ncia", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADO_DATADMN", "Data da Demanda", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADO_DATAENTREGAREAL", "Data de Entrega", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADO_STATUSDMN", "Status da Demanda", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADO_STATUSCNT", "Status do Servi�o", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADO_DESCRICAO", "T�tulo", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADO_SISTEMACOD", "Sistema", 0);
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADO_AGRUPADOR", "Agrupador", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV40DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV40DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40DynamicFiltersSelector3", AV40DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Igual", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("2", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV41DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV41DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41DynamicFiltersOperator3), 4, 0)));
            }
            cmbavContagemresultado_statusdmn3.Name = "vCONTAGEMRESULTADO_STATUSDMN3";
            cmbavContagemresultado_statusdmn3.WebTags = "";
            cmbavContagemresultado_statusdmn3.addItem("", "Todos", 0);
            cmbavContagemresultado_statusdmn3.addItem("B", "Stand by", 0);
            cmbavContagemresultado_statusdmn3.addItem("S", "Solicitada", 0);
            cmbavContagemresultado_statusdmn3.addItem("E", "Em An�lise", 0);
            cmbavContagemresultado_statusdmn3.addItem("A", "Em execu��o", 0);
            cmbavContagemresultado_statusdmn3.addItem("R", "Resolvida", 0);
            cmbavContagemresultado_statusdmn3.addItem("C", "Conferida", 0);
            cmbavContagemresultado_statusdmn3.addItem("D", "Rejeitada", 0);
            cmbavContagemresultado_statusdmn3.addItem("H", "Homologada", 0);
            cmbavContagemresultado_statusdmn3.addItem("O", "Aceite", 0);
            cmbavContagemresultado_statusdmn3.addItem("P", "A Pagar", 0);
            cmbavContagemresultado_statusdmn3.addItem("L", "Liquidada", 0);
            cmbavContagemresultado_statusdmn3.addItem("X", "Cancelada", 0);
            cmbavContagemresultado_statusdmn3.addItem("N", "N�o Faturada", 0);
            cmbavContagemresultado_statusdmn3.addItem("J", "Planejamento", 0);
            cmbavContagemresultado_statusdmn3.addItem("I", "An�lise Planejamento", 0);
            cmbavContagemresultado_statusdmn3.addItem("T", "Validacao T�cnica", 0);
            cmbavContagemresultado_statusdmn3.addItem("Q", "Validacao Qualidade", 0);
            cmbavContagemresultado_statusdmn3.addItem("G", "Em Homologa��o", 0);
            cmbavContagemresultado_statusdmn3.addItem("M", "Valida��o Mensura��o", 0);
            cmbavContagemresultado_statusdmn3.addItem("U", "Rascunho", 0);
            if ( cmbavContagemresultado_statusdmn3.ItemCount > 0 )
            {
               AV47ContagemResultado_StatusDmn3 = cmbavContagemresultado_statusdmn3.getValidValue(AV47ContagemResultado_StatusDmn3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ContagemResultado_StatusDmn3", AV47ContagemResultado_StatusDmn3);
            }
            cmbavContagemresultado_statuscnt3.Name = "vCONTAGEMRESULTADO_STATUSCNT3";
            cmbavContagemresultado_statuscnt3.WebTags = "";
            cmbavContagemresultado_statuscnt3.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 2, 0)), "Todos", 0);
            cmbavContagemresultado_statuscnt3.addItem("1", "Inicial", 0);
            cmbavContagemresultado_statuscnt3.addItem("2", "Auditoria", 0);
            cmbavContagemresultado_statuscnt3.addItem("3", "Negociac�o", 0);
            cmbavContagemresultado_statuscnt3.addItem("4", "N�o Aprovada", 0);
            cmbavContagemresultado_statuscnt3.addItem("5", "Aprovada", 0);
            cmbavContagemresultado_statuscnt3.addItem("6", "Pend�ncias", 0);
            cmbavContagemresultado_statuscnt3.addItem("7", "Diverg�ncia", 0);
            if ( cmbavContagemresultado_statuscnt3.ItemCount > 0 )
            {
               AV48ContagemResultado_StatusCnt3 = (short)(NumberUtil.Val( cmbavContagemresultado_statuscnt3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV48ContagemResultado_StatusCnt3), 2, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ContagemResultado_StatusCnt3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48ContagemResultado_StatusCnt3), 2, 0)));
            }
            dynavContagemresultado_sistemacod3.Name = "vCONTAGEMRESULTADO_SISTEMACOD3";
            dynavContagemresultado_sistemacod3.WebTags = "";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavContratada_areatrabalhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         GXVvCONTAGEMRESULTADO_SERVICO_htmlTB2( AV66ContagemResultado_ContratadaCod, AV67ContagemResultado_ServicoGrupo) ;
         GXVvCONTAGEMRESULTADO_SERVICO_htmlTB2( AV66ContagemResultado_ContratadaCod, AV67ContagemResultado_ServicoGrupo) ;
         GXVvCONTAGEMRESULTADO_CODSRVVNC_htmlTB2( AV69ContagemResultado_CntadaOsVinc, AV71ContagemResultado_SerGrupoVinc) ;
         GXVvCONTAGEMRESULTADO_CODSRVVNC_htmlTB2( AV69ContagemResultado_CntadaOsVinc, AV71ContagemResultado_SerGrupoVinc) ;
         /* End function dynload_actions */
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTRATADACODTB2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_CONTRATADACOD_dataTB2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_CONTRATADACOD_htmlTB2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_CONTRATADACOD_dataTB2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_contratadacod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_contratadacod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_contratadacod.ItemCount > 0 )
         {
            AV66ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( dynavContagemresultado_contratadacod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV66ContagemResultado_ContratadaCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66ContagemResultado_ContratadaCod), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_CONTRATADACOD_dataTB2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00TB2 */
         pr_default.execute(0, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00TB2_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00TB2_A438Contratada_Sigla[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvCONTAGEMRESULTADO_SERVICOGRUPOTB2( int AV66ContagemResultado_ContratadaCod )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_SERVICOGRUPO_dataTB2( AV66ContagemResultado_ContratadaCod) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_SERVICOGRUPO_htmlTB2( int AV66ContagemResultado_ContratadaCod )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_SERVICOGRUPO_dataTB2( AV66ContagemResultado_ContratadaCod) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_servicogrupo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_servicogrupo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_servicogrupo.ItemCount > 0 )
         {
            AV67ContagemResultado_ServicoGrupo = (int)(NumberUtil.Val( dynavContagemresultado_servicogrupo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV67ContagemResultado_ServicoGrupo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ContagemResultado_ServicoGrupo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67ContagemResultado_ServicoGrupo), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_SERVICOGRUPO_dataTB2( int AV66ContagemResultado_ContratadaCod )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00TB3 */
         pr_default.execute(1, new Object[] {AV66ContagemResultado_ContratadaCod});
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00TB3_A157ServicoGrupo_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00TB3_A158ServicoGrupo_Descricao[0]);
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXDLVvCONTAGEMRESULTADO_SERVICOTB2( int AV66ContagemResultado_ContratadaCod ,
                                                         int AV67ContagemResultado_ServicoGrupo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_SERVICO_dataTB2( AV66ContagemResultado_ContratadaCod, AV67ContagemResultado_ServicoGrupo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_SERVICO_htmlTB2( int AV66ContagemResultado_ContratadaCod ,
                                                            int AV67ContagemResultado_ServicoGrupo )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_SERVICO_dataTB2( AV66ContagemResultado_ContratadaCod, AV67ContagemResultado_ServicoGrupo) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_servico.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_servico.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_servico.ItemCount > 0 )
         {
            AV68ContagemResultado_Servico = (int)(NumberUtil.Val( dynavContagemresultado_servico.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV68ContagemResultado_Servico), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68ContagemResultado_Servico), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_SERVICO_dataTB2( int AV66ContagemResultado_ContratadaCod ,
                                                              int AV67ContagemResultado_ServicoGrupo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00TB4 */
         pr_default.execute(2, new Object[] {AV66ContagemResultado_ContratadaCod, AV67ContagemResultado_ServicoGrupo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00TB4_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00TB4_A605Servico_Sigla[0]));
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void GXDLVvCONTAGEMRESULTADO_CNTADAOSVINCTB2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_CNTADAOSVINC_dataTB2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_CNTADAOSVINC_htmlTB2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_CNTADAOSVINC_dataTB2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_cntadaosvinc.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_cntadaosvinc.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_cntadaosvinc.ItemCount > 0 )
         {
            AV69ContagemResultado_CntadaOsVinc = (int)(NumberUtil.Val( dynavContagemresultado_cntadaosvinc.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV69ContagemResultado_CntadaOsVinc), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ContagemResultado_CntadaOsVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69ContagemResultado_CntadaOsVinc), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_CNTADAOSVINC_dataTB2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00TB5 */
         pr_default.execute(3, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00TB5_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00TB5_A438Contratada_Sigla[0]));
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void GXDLVvCONTAGEMRESULTADO_SERGRUPOVINCTB2( int AV69ContagemResultado_CntadaOsVinc )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_SERGRUPOVINC_dataTB2( AV69ContagemResultado_CntadaOsVinc) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_SERGRUPOVINC_htmlTB2( int AV69ContagemResultado_CntadaOsVinc )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_SERGRUPOVINC_dataTB2( AV69ContagemResultado_CntadaOsVinc) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_sergrupovinc.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_sergrupovinc.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_sergrupovinc.ItemCount > 0 )
         {
            AV71ContagemResultado_SerGrupoVinc = (int)(NumberUtil.Val( dynavContagemresultado_sergrupovinc.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV71ContagemResultado_SerGrupoVinc), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ContagemResultado_SerGrupoVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71ContagemResultado_SerGrupoVinc), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_SERGRUPOVINC_dataTB2( int AV69ContagemResultado_CntadaOsVinc )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00TB6 */
         pr_default.execute(4, new Object[] {AV69ContagemResultado_CntadaOsVinc});
         while ( (pr_default.getStatus(4) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00TB6_A157ServicoGrupo_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00TB6_A158ServicoGrupo_Descricao[0]);
            pr_default.readNext(4);
         }
         pr_default.close(4);
      }

      protected void GXDLVvCONTAGEMRESULTADO_CODSRVVNCTB2( int AV69ContagemResultado_CntadaOsVinc ,
                                                           int AV71ContagemResultado_SerGrupoVinc )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_CODSRVVNC_dataTB2( AV69ContagemResultado_CntadaOsVinc, AV71ContagemResultado_SerGrupoVinc) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_CODSRVVNC_htmlTB2( int AV69ContagemResultado_CntadaOsVinc ,
                                                              int AV71ContagemResultado_SerGrupoVinc )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_CODSRVVNC_dataTB2( AV69ContagemResultado_CntadaOsVinc, AV71ContagemResultado_SerGrupoVinc) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_codsrvvnc.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_codsrvvnc.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_codsrvvnc.ItemCount > 0 )
         {
            AV70ContagemResultado_CodSrvVnc = (int)(NumberUtil.Val( dynavContagemresultado_codsrvvnc.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV70ContagemResultado_CodSrvVnc), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ContagemResultado_CodSrvVnc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV70ContagemResultado_CodSrvVnc), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_CODSRVVNC_dataTB2( int AV69ContagemResultado_CntadaOsVinc ,
                                                                int AV71ContagemResultado_SerGrupoVinc )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00TB7 */
         pr_default.execute(5, new Object[] {AV69ContagemResultado_CntadaOsVinc, AV71ContagemResultado_SerGrupoVinc});
         while ( (pr_default.getStatus(5) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00TB7_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00TB7_A605Servico_Sigla[0]));
            pr_default.readNext(5);
         }
         pr_default.close(5);
      }

      protected void GXDLVvCONTAGEMRESULTADO_SISTEMACOD1TB2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_SISTEMACOD1_dataTB2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_SISTEMACOD1_htmlTB2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_SISTEMACOD1_dataTB2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_sistemacod1.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_sistemacod1.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_sistemacod1.ItemCount > 0 )
         {
            AV24ContagemResultado_SistemaCod1 = (int)(NumberUtil.Val( dynavContagemresultado_sistemacod1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24ContagemResultado_SistemaCod1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultado_SistemaCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24ContagemResultado_SistemaCod1), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_SISTEMACOD1_dataTB2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhum");
         /* Using cursor H00TB8 */
         pr_default.execute(6, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(6) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00TB8_A127Sistema_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00TB8_A129Sistema_Sigla[0]));
            pr_default.readNext(6);
         }
         pr_default.close(6);
      }

      protected void GXDLVvCONTAGEMRESULTADO_SISTEMACOD2TB2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_SISTEMACOD2_dataTB2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_SISTEMACOD2_htmlTB2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_SISTEMACOD2_dataTB2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_sistemacod2.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_sistemacod2.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_sistemacod2.ItemCount > 0 )
         {
            AV37ContagemResultado_SistemaCod2 = (int)(NumberUtil.Val( dynavContagemresultado_sistemacod2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV37ContagemResultado_SistemaCod2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContagemResultado_SistemaCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37ContagemResultado_SistemaCod2), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_SISTEMACOD2_dataTB2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhum");
         /* Using cursor H00TB9 */
         pr_default.execute(7, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(7) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00TB9_A127Sistema_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00TB9_A129Sistema_Sigla[0]));
            pr_default.readNext(7);
         }
         pr_default.close(7);
      }

      protected void GXDLVvCONTAGEMRESULTADO_SISTEMACOD3TB2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_SISTEMACOD3_dataTB2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_SISTEMACOD3_htmlTB2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_SISTEMACOD3_dataTB2( AV6WWPContext) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_sistemacod3.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_sistemacod3.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_sistemacod3.ItemCount > 0 )
         {
            AV50ContagemResultado_SistemaCod3 = (int)(NumberUtil.Val( dynavContagemresultado_sistemacod3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV50ContagemResultado_SistemaCod3), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ContagemResultado_SistemaCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50ContagemResultado_SistemaCod3), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_SISTEMACOD3_dataTB2( wwpbaseobjects.SdtWWPContext AV6WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhum");
         /* Using cursor H00TB10 */
         pr_default.execute(8, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(8) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00TB10_A127Sistema_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00TB10_A129Sistema_Sigla[0]));
            pr_default.readNext(8);
         }
         pr_default.close(8);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_2062( ) ;
         while ( nGXsfl_206_idx <= nRC_GXsfl_206 )
         {
            sendrow_2062( ) ;
            nGXsfl_206_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_206_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_206_idx+1));
            sGXsfl_206_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_206_idx), 4, 0)), 4, "0");
            SubsflControlProps_2062( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       int AV67ContagemResultado_ServicoGrupo ,
                                       int AV68ContagemResultado_Servico ,
                                       int AV69ContagemResultado_CntadaOsVinc ,
                                       int AV71ContagemResultado_SerGrupoVinc ,
                                       int AV70ContagemResultado_CodSrvVnc ,
                                       String AV14DynamicFiltersSelector1 ,
                                       short AV15DynamicFiltersOperator1 ,
                                       String AV16ContagemResultado_DemandaFM1 ,
                                       DateTime AV17ContagemResultado_DataDmn1 ,
                                       DateTime AV18ContagemResultado_DataDmn_To1 ,
                                       DateTime AV19ContagemResultado_DataEntregaReal1 ,
                                       DateTime AV20ContagemResultado_DataEntregaReal_To1 ,
                                       String AV21ContagemResultado_StatusDmn1 ,
                                       short AV22ContagemResultado_StatusCnt1 ,
                                       String AV23ContagemResultado_Descricao1 ,
                                       int AV24ContagemResultado_SistemaCod1 ,
                                       String AV25ContagemResultado_Agrupador1 ,
                                       String AV27DynamicFiltersSelector2 ,
                                       short AV28DynamicFiltersOperator2 ,
                                       String AV29ContagemResultado_DemandaFM2 ,
                                       DateTime AV30ContagemResultado_DataDmn2 ,
                                       DateTime AV31ContagemResultado_DataDmn_To2 ,
                                       DateTime AV32ContagemResultado_DataEntregaReal2 ,
                                       DateTime AV33ContagemResultado_DataEntregaReal_To2 ,
                                       String AV34ContagemResultado_StatusDmn2 ,
                                       short AV35ContagemResultado_StatusCnt2 ,
                                       String AV36ContagemResultado_Descricao2 ,
                                       int AV37ContagemResultado_SistemaCod2 ,
                                       String AV38ContagemResultado_Agrupador2 ,
                                       String AV40DynamicFiltersSelector3 ,
                                       short AV41DynamicFiltersOperator3 ,
                                       String AV42ContagemResultado_DemandaFM3 ,
                                       DateTime AV43ContagemResultado_DataDmn3 ,
                                       DateTime AV44ContagemResultado_DataDmn_To3 ,
                                       DateTime AV45ContagemResultado_DataEntregaReal3 ,
                                       DateTime AV46ContagemResultado_DataEntregaReal_To3 ,
                                       String AV47ContagemResultado_StatusDmn3 ,
                                       short AV48ContagemResultado_StatusCnt3 ,
                                       String AV49ContagemResultado_Descricao3 ,
                                       int AV50ContagemResultado_SistemaCod3 ,
                                       String AV51ContagemResultado_Agrupador3 ,
                                       bool AV26DynamicFiltersEnabled2 ,
                                       bool AV39DynamicFiltersEnabled3 ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       int AV13Contratada_AreaTrabalhoCod ,
                                       int AV65ContagemResultado_OSVinculada ,
                                       int AV66ContagemResultado_ContratadaCod ,
                                       String AV122Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV53DynamicFiltersIgnoreFirst ,
                                       bool AV52DynamicFiltersRemoving ,
                                       int A456ContagemResultado_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFTB2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavContagemresultado_contratadacod.ItemCount > 0 )
         {
            AV66ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( dynavContagemresultado_contratadacod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV66ContagemResultado_ContratadaCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66ContagemResultado_ContratadaCod), 6, 0)));
         }
         if ( dynavContagemresultado_servicogrupo.ItemCount > 0 )
         {
            AV67ContagemResultado_ServicoGrupo = (int)(NumberUtil.Val( dynavContagemresultado_servicogrupo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV67ContagemResultado_ServicoGrupo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ContagemResultado_ServicoGrupo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67ContagemResultado_ServicoGrupo), 6, 0)));
         }
         if ( dynavContagemresultado_servico.ItemCount > 0 )
         {
            AV68ContagemResultado_Servico = (int)(NumberUtil.Val( dynavContagemresultado_servico.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV68ContagemResultado_Servico), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68ContagemResultado_Servico), 6, 0)));
         }
         if ( dynavContagemresultado_cntadaosvinc.ItemCount > 0 )
         {
            AV69ContagemResultado_CntadaOsVinc = (int)(NumberUtil.Val( dynavContagemresultado_cntadaosvinc.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV69ContagemResultado_CntadaOsVinc), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ContagemResultado_CntadaOsVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69ContagemResultado_CntadaOsVinc), 6, 0)));
         }
         if ( dynavContagemresultado_sergrupovinc.ItemCount > 0 )
         {
            AV71ContagemResultado_SerGrupoVinc = (int)(NumberUtil.Val( dynavContagemresultado_sergrupovinc.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV71ContagemResultado_SerGrupoVinc), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ContagemResultado_SerGrupoVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71ContagemResultado_SerGrupoVinc), 6, 0)));
         }
         if ( dynavContagemresultado_codsrvvnc.ItemCount > 0 )
         {
            AV70ContagemResultado_CodSrvVnc = (int)(NumberUtil.Val( dynavContagemresultado_codsrvvnc.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV70ContagemResultado_CodSrvVnc), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ContagemResultado_CodSrvVnc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV70ContagemResultado_CodSrvVnc), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV14DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV14DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV15DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavContagemresultado_statusdmn1.ItemCount > 0 )
         {
            AV21ContagemResultado_StatusDmn1 = cmbavContagemresultado_statusdmn1.getValidValue(AV21ContagemResultado_StatusDmn1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultado_StatusDmn1", AV21ContagemResultado_StatusDmn1);
         }
         if ( cmbavContagemresultado_statuscnt1.ItemCount > 0 )
         {
            AV22ContagemResultado_StatusCnt1 = (short)(NumberUtil.Val( cmbavContagemresultado_statuscnt1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22ContagemResultado_StatusCnt1), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemResultado_StatusCnt1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22ContagemResultado_StatusCnt1), 2, 0)));
         }
         if ( dynavContagemresultado_sistemacod1.ItemCount > 0 )
         {
            AV24ContagemResultado_SistemaCod1 = (int)(NumberUtil.Val( dynavContagemresultado_sistemacod1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24ContagemResultado_SistemaCod1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultado_SistemaCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24ContagemResultado_SistemaCod1), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV27DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV27DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector2", AV27DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV28DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavContagemresultado_statusdmn2.ItemCount > 0 )
         {
            AV34ContagemResultado_StatusDmn2 = cmbavContagemresultado_statusdmn2.getValidValue(AV34ContagemResultado_StatusDmn2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultado_StatusDmn2", AV34ContagemResultado_StatusDmn2);
         }
         if ( cmbavContagemresultado_statuscnt2.ItemCount > 0 )
         {
            AV35ContagemResultado_StatusCnt2 = (short)(NumberUtil.Val( cmbavContagemresultado_statuscnt2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV35ContagemResultado_StatusCnt2), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ContagemResultado_StatusCnt2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35ContagemResultado_StatusCnt2), 2, 0)));
         }
         if ( dynavContagemresultado_sistemacod2.ItemCount > 0 )
         {
            AV37ContagemResultado_SistemaCod2 = (int)(NumberUtil.Val( dynavContagemresultado_sistemacod2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV37ContagemResultado_SistemaCod2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContagemResultado_SistemaCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37ContagemResultado_SistemaCod2), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV40DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV40DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40DynamicFiltersSelector3", AV40DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV41DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV41DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41DynamicFiltersOperator3), 4, 0)));
         }
         if ( cmbavContagemresultado_statusdmn3.ItemCount > 0 )
         {
            AV47ContagemResultado_StatusDmn3 = cmbavContagemresultado_statusdmn3.getValidValue(AV47ContagemResultado_StatusDmn3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ContagemResultado_StatusDmn3", AV47ContagemResultado_StatusDmn3);
         }
         if ( cmbavContagemresultado_statuscnt3.ItemCount > 0 )
         {
            AV48ContagemResultado_StatusCnt3 = (short)(NumberUtil.Val( cmbavContagemresultado_statuscnt3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV48ContagemResultado_StatusCnt3), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ContagemResultado_StatusCnt3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48ContagemResultado_StatusCnt3), 2, 0)));
         }
         if ( dynavContagemresultado_sistemacod3.ItemCount > 0 )
         {
            AV50ContagemResultado_SistemaCod3 = (int)(NumberUtil.Val( dynavContagemresultado_sistemacod3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV50ContagemResultado_SistemaCod3), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ContagemResultado_SistemaCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50ContagemResultado_SistemaCod3), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFTB2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV122Pgmname = "ExtraWWContagemResultadoRelComparacaoDemandas";
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      protected void RFTB2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 206;
         /* Execute user event: E24TB2 */
         E24TB2 ();
         nGXsfl_206_idx = 1;
         sGXsfl_206_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_206_idx), 4, 0)), 4, "0");
         SubsflControlProps_2062( ) ;
         nGXsfl_206_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_2062( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(9, new Object[]{ new Object[]{
                                                 AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1 ,
                                                 AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 ,
                                                 AV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1 ,
                                                 AV79ExtraWWContagemResultadoRelComparacaoDemandasDS_6_Contagemresultado_datadmn1 ,
                                                 AV80ExtraWWContagemResultadoRelComparacaoDemandasDS_7_Contagemresultado_datadmn_to1 ,
                                                 AV81ExtraWWContagemResultadoRelComparacaoDemandasDS_8_Contagemresultado_dataentregareal1 ,
                                                 AV82ExtraWWContagemResultadoRelComparacaoDemandasDS_9_Contagemresultado_dataentregareal_to1 ,
                                                 AV83ExtraWWContagemResultadoRelComparacaoDemandasDS_10_Contagemresultado_statusdmn1 ,
                                                 AV84ExtraWWContagemResultadoRelComparacaoDemandasDS_11_Contagemresultado_statuscnt1 ,
                                                 AV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1 ,
                                                 AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1 ,
                                                 AV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1 ,
                                                 AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 ,
                                                 AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2 ,
                                                 AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 ,
                                                 AV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2 ,
                                                 AV92ExtraWWContagemResultadoRelComparacaoDemandasDS_19_Contagemresultado_datadmn2 ,
                                                 AV93ExtraWWContagemResultadoRelComparacaoDemandasDS_20_Contagemresultado_datadmn_to2 ,
                                                 AV94ExtraWWContagemResultadoRelComparacaoDemandasDS_21_Contagemresultado_dataentregareal2 ,
                                                 AV95ExtraWWContagemResultadoRelComparacaoDemandasDS_22_Contagemresultado_dataentregareal_to2 ,
                                                 AV96ExtraWWContagemResultadoRelComparacaoDemandasDS_23_Contagemresultado_statusdmn2 ,
                                                 AV97ExtraWWContagemResultadoRelComparacaoDemandasDS_24_Contagemresultado_statuscnt2 ,
                                                 AV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2 ,
                                                 AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2 ,
                                                 AV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2 ,
                                                 AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 ,
                                                 AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3 ,
                                                 AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 ,
                                                 AV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3 ,
                                                 AV105ExtraWWContagemResultadoRelComparacaoDemandasDS_32_Contagemresultado_datadmn3 ,
                                                 AV106ExtraWWContagemResultadoRelComparacaoDemandasDS_33_Contagemresultado_datadmn_to3 ,
                                                 AV107ExtraWWContagemResultadoRelComparacaoDemandasDS_34_Contagemresultado_dataentregareal3 ,
                                                 AV108ExtraWWContagemResultadoRelComparacaoDemandasDS_35_Contagemresultado_dataentregareal_to3 ,
                                                 AV109ExtraWWContagemResultadoRelComparacaoDemandasDS_36_Contagemresultado_statusdmn3 ,
                                                 AV110ExtraWWContagemResultadoRelComparacaoDemandasDS_37_Contagemresultado_statuscnt3 ,
                                                 AV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3 ,
                                                 AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3 ,
                                                 AV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3 ,
                                                 AV115ExtraWWContagemResultadoRelComparacaoDemandasDS_42_Contagemresultado_servicogrupo ,
                                                 AV116ExtraWWContagemResultadoRelComparacaoDemandasDS_43_Contagemresultado_servico ,
                                                 AV117ExtraWWContagemResultadoRelComparacaoDemandasDS_44_Contagemresultado_cntadaosvinc ,
                                                 AV118ExtraWWContagemResultadoRelComparacaoDemandasDS_45_Contagemresultado_sergrupovinc ,
                                                 AV119ExtraWWContagemResultadoRelComparacaoDemandasDS_46_Contagemresultado_codsrvvnc ,
                                                 A493ContagemResultado_DemandaFM ,
                                                 A471ContagemResultado_DataDmn ,
                                                 A2017ContagemResultado_DataEntregaReal ,
                                                 A484ContagemResultado_StatusDmn ,
                                                 A483ContagemResultado_StatusCnt ,
                                                 A494ContagemResultado_Descricao ,
                                                 A489ContagemResultado_SistemaCod ,
                                                 A1046ContagemResultado_Agrupador ,
                                                 A764ContagemResultado_ServicoGrupo ,
                                                 A601ContagemResultado_Servico ,
                                                 A2145ContagemResultado_CntadaOsVinc ,
                                                 A2146ContagemResultado_SerGrupoVinc ,
                                                 A1591ContagemResultado_CodSrvVnc ,
                                                 A602ContagemResultado_OSVinculada ,
                                                 A52Contratada_AreaTrabalhoCod ,
                                                 AV6WWPContext.gxTpr_Areatrabalho_codigo ,
                                                 A490ContagemResultado_ContratadaCod ,
                                                 AV114ExtraWWContagemResultadoRelComparacaoDemandasDS_41_Contagemresultado_contratadacod },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                                 TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                                 TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                                 }
            });
            lV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1 = StringUtil.Concat( StringUtil.RTrim( AV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1), "%", "");
            lV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1 = StringUtil.Concat( StringUtil.RTrim( AV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1), "%", "");
            lV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1), "%", "");
            lV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1), "%", "");
            lV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1 = StringUtil.PadR( StringUtil.RTrim( AV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1), 15, "%");
            lV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1 = StringUtil.PadR( StringUtil.RTrim( AV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1), 15, "%");
            lV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2 = StringUtil.Concat( StringUtil.RTrim( AV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2), "%", "");
            lV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2 = StringUtil.Concat( StringUtil.RTrim( AV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2), "%", "");
            lV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2), "%", "");
            lV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2), "%", "");
            lV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2 = StringUtil.PadR( StringUtil.RTrim( AV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2), 15, "%");
            lV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2 = StringUtil.PadR( StringUtil.RTrim( AV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2), 15, "%");
            lV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3 = StringUtil.Concat( StringUtil.RTrim( AV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3), "%", "");
            lV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3 = StringUtil.Concat( StringUtil.RTrim( AV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3), "%", "");
            lV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3), "%", "");
            lV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3), "%", "");
            lV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3 = StringUtil.PadR( StringUtil.RTrim( AV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3), 15, "%");
            lV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3 = StringUtil.PadR( StringUtil.RTrim( AV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3), 15, "%");
            /* Using cursor H00TB11 */
            pr_default.execute(9, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, AV114ExtraWWContagemResultadoRelComparacaoDemandasDS_41_Contagemresultado_contratadacod, AV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1, lV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1, lV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1, AV79ExtraWWContagemResultadoRelComparacaoDemandasDS_6_Contagemresultado_datadmn1, AV80ExtraWWContagemResultadoRelComparacaoDemandasDS_7_Contagemresultado_datadmn_to1, AV81ExtraWWContagemResultadoRelComparacaoDemandasDS_8_Contagemresultado_dataentregareal1, AV81ExtraWWContagemResultadoRelComparacaoDemandasDS_8_Contagemresultado_dataentregareal1, AV82ExtraWWContagemResultadoRelComparacaoDemandasDS_9_Contagemresultado_dataentregareal_to1, AV82ExtraWWContagemResultadoRelComparacaoDemandasDS_9_Contagemresultado_dataentregareal_to1, AV83ExtraWWContagemResultadoRelComparacaoDemandasDS_10_Contagemresultado_statusdmn1, AV84ExtraWWContagemResultadoRelComparacaoDemandasDS_11_Contagemresultado_statuscnt1, lV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1, lV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1, AV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1, AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1, AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1, AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1, AV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1, lV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1, lV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1, AV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2, lV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2, lV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2, AV92ExtraWWContagemResultadoRelComparacaoDemandasDS_19_Contagemresultado_datadmn2, AV93ExtraWWContagemResultadoRelComparacaoDemandasDS_20_Contagemresultado_datadmn_to2, AV94ExtraWWContagemResultadoRelComparacaoDemandasDS_21_Contagemresultado_dataentregareal2, AV94ExtraWWContagemResultadoRelComparacaoDemandasDS_21_Contagemresultado_dataentregareal2, AV95ExtraWWContagemResultadoRelComparacaoDemandasDS_22_Contagemresultado_dataentregareal_to2, AV95ExtraWWContagemResultadoRelComparacaoDemandasDS_22_Contagemresultado_dataentregareal_to2, AV96ExtraWWContagemResultadoRelComparacaoDemandasDS_23_Contagemresultado_statusdmn2, AV97ExtraWWContagemResultadoRelComparacaoDemandasDS_24_Contagemresultado_statuscnt2, lV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2, lV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2, AV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2, AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2, AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2, AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2, AV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2, lV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2, lV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2, AV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3, lV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3, lV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3, AV105ExtraWWContagemResultadoRelComparacaoDemandasDS_32_Contagemresultado_datadmn3, AV106ExtraWWContagemResultadoRelComparacaoDemandasDS_33_Contagemresultado_datadmn_to3, AV107ExtraWWContagemResultadoRelComparacaoDemandasDS_34_Contagemresultado_dataentregareal3, AV107ExtraWWContagemResultadoRelComparacaoDemandasDS_34_Contagemresultado_dataentregareal3, AV108ExtraWWContagemResultadoRelComparacaoDemandasDS_35_Contagemresultado_dataentregareal_to3, AV108ExtraWWContagemResultadoRelComparacaoDemandasDS_35_Contagemresultado_dataentregareal_to3, AV109ExtraWWContagemResultadoRelComparacaoDemandasDS_36_Contagemresultado_statusdmn3, AV110ExtraWWContagemResultadoRelComparacaoDemandasDS_37_Contagemresultado_statuscnt3, lV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3, lV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3, AV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3, AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3, AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3, AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3, AV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3, lV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3, lV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3, AV115ExtraWWContagemResultadoRelComparacaoDemandasDS_42_Contagemresultado_servicogrupo, AV116ExtraWWContagemResultadoRelComparacaoDemandasDS_43_Contagemresultado_servico, AV117ExtraWWContagemResultadoRelComparacaoDemandasDS_44_Contagemresultado_cntadaosvinc, AV118ExtraWWContagemResultadoRelComparacaoDemandasDS_45_Contagemresultado_sergrupovinc, AV119ExtraWWContagemResultadoRelComparacaoDemandasDS_46_Contagemresultado_codsrvvnc, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_206_idx = 1;
            while ( ( (pr_default.getStatus(9) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1553ContagemResultado_CntSrvCod = H00TB11_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00TB11_n1553ContagemResultado_CntSrvCod[0];
               A1627ContagemResultado_CntSrvVncCod = H00TB11_A1627ContagemResultado_CntSrvVncCod[0];
               n1627ContagemResultado_CntSrvVncCod = H00TB11_n1627ContagemResultado_CntSrvVncCod[0];
               A52Contratada_AreaTrabalhoCod = H00TB11_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = H00TB11_n52Contratada_AreaTrabalhoCod[0];
               A490ContagemResultado_ContratadaCod = H00TB11_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00TB11_n490ContagemResultado_ContratadaCod[0];
               A1591ContagemResultado_CodSrvVnc = H00TB11_A1591ContagemResultado_CodSrvVnc[0];
               n1591ContagemResultado_CodSrvVnc = H00TB11_n1591ContagemResultado_CodSrvVnc[0];
               A2146ContagemResultado_SerGrupoVinc = H00TB11_A2146ContagemResultado_SerGrupoVinc[0];
               n2146ContagemResultado_SerGrupoVinc = H00TB11_n2146ContagemResultado_SerGrupoVinc[0];
               A2145ContagemResultado_CntadaOsVinc = H00TB11_A2145ContagemResultado_CntadaOsVinc[0];
               n2145ContagemResultado_CntadaOsVinc = H00TB11_n2145ContagemResultado_CntadaOsVinc[0];
               A601ContagemResultado_Servico = H00TB11_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = H00TB11_n601ContagemResultado_Servico[0];
               A764ContagemResultado_ServicoGrupo = H00TB11_A764ContagemResultado_ServicoGrupo[0];
               n764ContagemResultado_ServicoGrupo = H00TB11_n764ContagemResultado_ServicoGrupo[0];
               A1046ContagemResultado_Agrupador = H00TB11_A1046ContagemResultado_Agrupador[0];
               n1046ContagemResultado_Agrupador = H00TB11_n1046ContagemResultado_Agrupador[0];
               A489ContagemResultado_SistemaCod = H00TB11_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = H00TB11_n489ContagemResultado_SistemaCod[0];
               A483ContagemResultado_StatusCnt = H00TB11_A483ContagemResultado_StatusCnt[0];
               A484ContagemResultado_StatusDmn = H00TB11_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = H00TB11_n484ContagemResultado_StatusDmn[0];
               A602ContagemResultado_OSVinculada = H00TB11_A602ContagemResultado_OSVinculada[0];
               n602ContagemResultado_OSVinculada = H00TB11_n602ContagemResultado_OSVinculada[0];
               A509ContagemrResultado_SistemaSigla = H00TB11_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = H00TB11_n509ContagemrResultado_SistemaSigla[0];
               A494ContagemResultado_Descricao = H00TB11_A494ContagemResultado_Descricao[0];
               n494ContagemResultado_Descricao = H00TB11_n494ContagemResultado_Descricao[0];
               A2017ContagemResultado_DataEntregaReal = H00TB11_A2017ContagemResultado_DataEntregaReal[0];
               n2017ContagemResultado_DataEntregaReal = H00TB11_n2017ContagemResultado_DataEntregaReal[0];
               A471ContagemResultado_DataDmn = H00TB11_A471ContagemResultado_DataDmn[0];
               A457ContagemResultado_Demanda = H00TB11_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = H00TB11_n457ContagemResultado_Demanda[0];
               A493ContagemResultado_DemandaFM = H00TB11_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00TB11_n493ContagemResultado_DemandaFM[0];
               A456ContagemResultado_Codigo = H00TB11_A456ContagemResultado_Codigo[0];
               A1553ContagemResultado_CntSrvCod = H00TB11_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00TB11_n1553ContagemResultado_CntSrvCod[0];
               A490ContagemResultado_ContratadaCod = H00TB11_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00TB11_n490ContagemResultado_ContratadaCod[0];
               A1046ContagemResultado_Agrupador = H00TB11_A1046ContagemResultado_Agrupador[0];
               n1046ContagemResultado_Agrupador = H00TB11_n1046ContagemResultado_Agrupador[0];
               A489ContagemResultado_SistemaCod = H00TB11_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = H00TB11_n489ContagemResultado_SistemaCod[0];
               A484ContagemResultado_StatusDmn = H00TB11_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = H00TB11_n484ContagemResultado_StatusDmn[0];
               A602ContagemResultado_OSVinculada = H00TB11_A602ContagemResultado_OSVinculada[0];
               n602ContagemResultado_OSVinculada = H00TB11_n602ContagemResultado_OSVinculada[0];
               A494ContagemResultado_Descricao = H00TB11_A494ContagemResultado_Descricao[0];
               n494ContagemResultado_Descricao = H00TB11_n494ContagemResultado_Descricao[0];
               A2017ContagemResultado_DataEntregaReal = H00TB11_A2017ContagemResultado_DataEntregaReal[0];
               n2017ContagemResultado_DataEntregaReal = H00TB11_n2017ContagemResultado_DataEntregaReal[0];
               A471ContagemResultado_DataDmn = H00TB11_A471ContagemResultado_DataDmn[0];
               A457ContagemResultado_Demanda = H00TB11_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = H00TB11_n457ContagemResultado_Demanda[0];
               A493ContagemResultado_DemandaFM = H00TB11_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00TB11_n493ContagemResultado_DemandaFM[0];
               A601ContagemResultado_Servico = H00TB11_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = H00TB11_n601ContagemResultado_Servico[0];
               A764ContagemResultado_ServicoGrupo = H00TB11_A764ContagemResultado_ServicoGrupo[0];
               n764ContagemResultado_ServicoGrupo = H00TB11_n764ContagemResultado_ServicoGrupo[0];
               A52Contratada_AreaTrabalhoCod = H00TB11_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = H00TB11_n52Contratada_AreaTrabalhoCod[0];
               A509ContagemrResultado_SistemaSigla = H00TB11_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = H00TB11_n509ContagemrResultado_SistemaSigla[0];
               A1627ContagemResultado_CntSrvVncCod = H00TB11_A1627ContagemResultado_CntSrvVncCod[0];
               n1627ContagemResultado_CntSrvVncCod = H00TB11_n1627ContagemResultado_CntSrvVncCod[0];
               A2145ContagemResultado_CntadaOsVinc = H00TB11_A2145ContagemResultado_CntadaOsVinc[0];
               n2145ContagemResultado_CntadaOsVinc = H00TB11_n2145ContagemResultado_CntadaOsVinc[0];
               A1591ContagemResultado_CodSrvVnc = H00TB11_A1591ContagemResultado_CodSrvVnc[0];
               n1591ContagemResultado_CodSrvVnc = H00TB11_n1591ContagemResultado_CodSrvVnc[0];
               A2146ContagemResultado_SerGrupoVinc = H00TB11_A2146ContagemResultado_SerGrupoVinc[0];
               n2146ContagemResultado_SerGrupoVinc = H00TB11_n2146ContagemResultado_SerGrupoVinc[0];
               /* Execute user event: E25TB2 */
               E25TB2 ();
               pr_default.readNext(9);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(9) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(9);
            wbEnd = 206;
            WBTB0( ) ;
         }
         nGXsfl_206_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV74ExtraWWContagemResultadoRelComparacaoDemandasDS_1_Contratada_areatrabalhocod = AV13Contratada_AreaTrabalhoCod;
         AV75ExtraWWContagemResultadoRelComparacaoDemandasDS_2_Contagemresultado_osvinculada = AV65ContagemResultado_OSVinculada;
         AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1 = AV14DynamicFiltersSelector1;
         AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 = AV15DynamicFiltersOperator1;
         AV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1 = AV16ContagemResultado_DemandaFM1;
         AV79ExtraWWContagemResultadoRelComparacaoDemandasDS_6_Contagemresultado_datadmn1 = AV17ContagemResultado_DataDmn1;
         AV80ExtraWWContagemResultadoRelComparacaoDemandasDS_7_Contagemresultado_datadmn_to1 = AV18ContagemResultado_DataDmn_To1;
         AV81ExtraWWContagemResultadoRelComparacaoDemandasDS_8_Contagemresultado_dataentregareal1 = AV19ContagemResultado_DataEntregaReal1;
         AV82ExtraWWContagemResultadoRelComparacaoDemandasDS_9_Contagemresultado_dataentregareal_to1 = AV20ContagemResultado_DataEntregaReal_To1;
         AV83ExtraWWContagemResultadoRelComparacaoDemandasDS_10_Contagemresultado_statusdmn1 = AV21ContagemResultado_StatusDmn1;
         AV84ExtraWWContagemResultadoRelComparacaoDemandasDS_11_Contagemresultado_statuscnt1 = AV22ContagemResultado_StatusCnt1;
         AV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1 = AV23ContagemResultado_Descricao1;
         AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1 = AV24ContagemResultado_SistemaCod1;
         AV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1 = AV25ContagemResultado_Agrupador1;
         AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 = AV26DynamicFiltersEnabled2;
         AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2 = AV27DynamicFiltersSelector2;
         AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 = AV28DynamicFiltersOperator2;
         AV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2 = AV29ContagemResultado_DemandaFM2;
         AV92ExtraWWContagemResultadoRelComparacaoDemandasDS_19_Contagemresultado_datadmn2 = AV30ContagemResultado_DataDmn2;
         AV93ExtraWWContagemResultadoRelComparacaoDemandasDS_20_Contagemresultado_datadmn_to2 = AV31ContagemResultado_DataDmn_To2;
         AV94ExtraWWContagemResultadoRelComparacaoDemandasDS_21_Contagemresultado_dataentregareal2 = AV32ContagemResultado_DataEntregaReal2;
         AV95ExtraWWContagemResultadoRelComparacaoDemandasDS_22_Contagemresultado_dataentregareal_to2 = AV33ContagemResultado_DataEntregaReal_To2;
         AV96ExtraWWContagemResultadoRelComparacaoDemandasDS_23_Contagemresultado_statusdmn2 = AV34ContagemResultado_StatusDmn2;
         AV97ExtraWWContagemResultadoRelComparacaoDemandasDS_24_Contagemresultado_statuscnt2 = AV35ContagemResultado_StatusCnt2;
         AV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2 = AV36ContagemResultado_Descricao2;
         AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2 = AV37ContagemResultado_SistemaCod2;
         AV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2 = AV38ContagemResultado_Agrupador2;
         AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 = AV39DynamicFiltersEnabled3;
         AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3 = AV40DynamicFiltersSelector3;
         AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 = AV41DynamicFiltersOperator3;
         AV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3 = AV42ContagemResultado_DemandaFM3;
         AV105ExtraWWContagemResultadoRelComparacaoDemandasDS_32_Contagemresultado_datadmn3 = AV43ContagemResultado_DataDmn3;
         AV106ExtraWWContagemResultadoRelComparacaoDemandasDS_33_Contagemresultado_datadmn_to3 = AV44ContagemResultado_DataDmn_To3;
         AV107ExtraWWContagemResultadoRelComparacaoDemandasDS_34_Contagemresultado_dataentregareal3 = AV45ContagemResultado_DataEntregaReal3;
         AV108ExtraWWContagemResultadoRelComparacaoDemandasDS_35_Contagemresultado_dataentregareal_to3 = AV46ContagemResultado_DataEntregaReal_To3;
         AV109ExtraWWContagemResultadoRelComparacaoDemandasDS_36_Contagemresultado_statusdmn3 = AV47ContagemResultado_StatusDmn3;
         AV110ExtraWWContagemResultadoRelComparacaoDemandasDS_37_Contagemresultado_statuscnt3 = AV48ContagemResultado_StatusCnt3;
         AV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3 = AV49ContagemResultado_Descricao3;
         AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3 = AV50ContagemResultado_SistemaCod3;
         AV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3 = AV51ContagemResultado_Agrupador3;
         AV114ExtraWWContagemResultadoRelComparacaoDemandasDS_41_Contagemresultado_contratadacod = AV66ContagemResultado_ContratadaCod;
         AV115ExtraWWContagemResultadoRelComparacaoDemandasDS_42_Contagemresultado_servicogrupo = AV67ContagemResultado_ServicoGrupo;
         AV116ExtraWWContagemResultadoRelComparacaoDemandasDS_43_Contagemresultado_servico = AV68ContagemResultado_Servico;
         AV117ExtraWWContagemResultadoRelComparacaoDemandasDS_44_Contagemresultado_cntadaosvinc = AV69ContagemResultado_CntadaOsVinc;
         AV118ExtraWWContagemResultadoRelComparacaoDemandasDS_45_Contagemresultado_sergrupovinc = AV71ContagemResultado_SerGrupoVinc;
         AV119ExtraWWContagemResultadoRelComparacaoDemandasDS_46_Contagemresultado_codsrvvnc = AV70ContagemResultado_CodSrvVnc;
         pr_default.dynParam(10, new Object[]{ new Object[]{
                                              AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1 ,
                                              AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 ,
                                              AV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1 ,
                                              AV79ExtraWWContagemResultadoRelComparacaoDemandasDS_6_Contagemresultado_datadmn1 ,
                                              AV80ExtraWWContagemResultadoRelComparacaoDemandasDS_7_Contagemresultado_datadmn_to1 ,
                                              AV81ExtraWWContagemResultadoRelComparacaoDemandasDS_8_Contagemresultado_dataentregareal1 ,
                                              AV82ExtraWWContagemResultadoRelComparacaoDemandasDS_9_Contagemresultado_dataentregareal_to1 ,
                                              AV83ExtraWWContagemResultadoRelComparacaoDemandasDS_10_Contagemresultado_statusdmn1 ,
                                              AV84ExtraWWContagemResultadoRelComparacaoDemandasDS_11_Contagemresultado_statuscnt1 ,
                                              AV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1 ,
                                              AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1 ,
                                              AV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1 ,
                                              AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 ,
                                              AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2 ,
                                              AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 ,
                                              AV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2 ,
                                              AV92ExtraWWContagemResultadoRelComparacaoDemandasDS_19_Contagemresultado_datadmn2 ,
                                              AV93ExtraWWContagemResultadoRelComparacaoDemandasDS_20_Contagemresultado_datadmn_to2 ,
                                              AV94ExtraWWContagemResultadoRelComparacaoDemandasDS_21_Contagemresultado_dataentregareal2 ,
                                              AV95ExtraWWContagemResultadoRelComparacaoDemandasDS_22_Contagemresultado_dataentregareal_to2 ,
                                              AV96ExtraWWContagemResultadoRelComparacaoDemandasDS_23_Contagemresultado_statusdmn2 ,
                                              AV97ExtraWWContagemResultadoRelComparacaoDemandasDS_24_Contagemresultado_statuscnt2 ,
                                              AV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2 ,
                                              AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2 ,
                                              AV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2 ,
                                              AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 ,
                                              AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3 ,
                                              AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 ,
                                              AV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3 ,
                                              AV105ExtraWWContagemResultadoRelComparacaoDemandasDS_32_Contagemresultado_datadmn3 ,
                                              AV106ExtraWWContagemResultadoRelComparacaoDemandasDS_33_Contagemresultado_datadmn_to3 ,
                                              AV107ExtraWWContagemResultadoRelComparacaoDemandasDS_34_Contagemresultado_dataentregareal3 ,
                                              AV108ExtraWWContagemResultadoRelComparacaoDemandasDS_35_Contagemresultado_dataentregareal_to3 ,
                                              AV109ExtraWWContagemResultadoRelComparacaoDemandasDS_36_Contagemresultado_statusdmn3 ,
                                              AV110ExtraWWContagemResultadoRelComparacaoDemandasDS_37_Contagemresultado_statuscnt3 ,
                                              AV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3 ,
                                              AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3 ,
                                              AV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3 ,
                                              AV115ExtraWWContagemResultadoRelComparacaoDemandasDS_42_Contagemresultado_servicogrupo ,
                                              AV116ExtraWWContagemResultadoRelComparacaoDemandasDS_43_Contagemresultado_servico ,
                                              AV117ExtraWWContagemResultadoRelComparacaoDemandasDS_44_Contagemresultado_cntadaosvinc ,
                                              AV118ExtraWWContagemResultadoRelComparacaoDemandasDS_45_Contagemresultado_sergrupovinc ,
                                              AV119ExtraWWContagemResultadoRelComparacaoDemandasDS_46_Contagemresultado_codsrvvnc ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A2017ContagemResultado_DataEntregaReal ,
                                              A484ContagemResultado_StatusDmn ,
                                              A483ContagemResultado_StatusCnt ,
                                              A494ContagemResultado_Descricao ,
                                              A489ContagemResultado_SistemaCod ,
                                              A1046ContagemResultado_Agrupador ,
                                              A764ContagemResultado_ServicoGrupo ,
                                              A601ContagemResultado_Servico ,
                                              A2145ContagemResultado_CntadaOsVinc ,
                                              A2146ContagemResultado_SerGrupoVinc ,
                                              A1591ContagemResultado_CodSrvVnc ,
                                              A602ContagemResultado_OSVinculada ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV6WWPContext.gxTpr_Areatrabalho_codigo ,
                                              A490ContagemResultado_ContratadaCod ,
                                              AV114ExtraWWContagemResultadoRelComparacaoDemandasDS_41_Contagemresultado_contratadacod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1 = StringUtil.Concat( StringUtil.RTrim( AV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1), "%", "");
         lV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1 = StringUtil.Concat( StringUtil.RTrim( AV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1), "%", "");
         lV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1), "%", "");
         lV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1), "%", "");
         lV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1 = StringUtil.PadR( StringUtil.RTrim( AV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1), 15, "%");
         lV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1 = StringUtil.PadR( StringUtil.RTrim( AV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1), 15, "%");
         lV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2 = StringUtil.Concat( StringUtil.RTrim( AV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2), "%", "");
         lV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2 = StringUtil.Concat( StringUtil.RTrim( AV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2), "%", "");
         lV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2), "%", "");
         lV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2), "%", "");
         lV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2 = StringUtil.PadR( StringUtil.RTrim( AV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2), 15, "%");
         lV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2 = StringUtil.PadR( StringUtil.RTrim( AV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2), 15, "%");
         lV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3 = StringUtil.Concat( StringUtil.RTrim( AV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3), "%", "");
         lV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3 = StringUtil.Concat( StringUtil.RTrim( AV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3), "%", "");
         lV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3), "%", "");
         lV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3), "%", "");
         lV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3 = StringUtil.PadR( StringUtil.RTrim( AV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3), 15, "%");
         lV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3 = StringUtil.PadR( StringUtil.RTrim( AV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3), 15, "%");
         /* Using cursor H00TB12 */
         pr_default.execute(10, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, AV114ExtraWWContagemResultadoRelComparacaoDemandasDS_41_Contagemresultado_contratadacod, AV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1, lV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1, lV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1, AV79ExtraWWContagemResultadoRelComparacaoDemandasDS_6_Contagemresultado_datadmn1, AV80ExtraWWContagemResultadoRelComparacaoDemandasDS_7_Contagemresultado_datadmn_to1, AV81ExtraWWContagemResultadoRelComparacaoDemandasDS_8_Contagemresultado_dataentregareal1, AV81ExtraWWContagemResultadoRelComparacaoDemandasDS_8_Contagemresultado_dataentregareal1, AV82ExtraWWContagemResultadoRelComparacaoDemandasDS_9_Contagemresultado_dataentregareal_to1, AV82ExtraWWContagemResultadoRelComparacaoDemandasDS_9_Contagemresultado_dataentregareal_to1, AV83ExtraWWContagemResultadoRelComparacaoDemandasDS_10_Contagemresultado_statusdmn1, AV84ExtraWWContagemResultadoRelComparacaoDemandasDS_11_Contagemresultado_statuscnt1, lV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1, lV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1, AV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1, AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1, AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1, AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1, AV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1, lV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1, lV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1, AV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2, lV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2, lV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2, AV92ExtraWWContagemResultadoRelComparacaoDemandasDS_19_Contagemresultado_datadmn2, AV93ExtraWWContagemResultadoRelComparacaoDemandasDS_20_Contagemresultado_datadmn_to2, AV94ExtraWWContagemResultadoRelComparacaoDemandasDS_21_Contagemresultado_dataentregareal2, AV94ExtraWWContagemResultadoRelComparacaoDemandasDS_21_Contagemresultado_dataentregareal2, AV95ExtraWWContagemResultadoRelComparacaoDemandasDS_22_Contagemresultado_dataentregareal_to2, AV95ExtraWWContagemResultadoRelComparacaoDemandasDS_22_Contagemresultado_dataentregareal_to2, AV96ExtraWWContagemResultadoRelComparacaoDemandasDS_23_Contagemresultado_statusdmn2, AV97ExtraWWContagemResultadoRelComparacaoDemandasDS_24_Contagemresultado_statuscnt2, lV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2, lV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2, AV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2, AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2, AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2, AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2, AV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2, lV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2, lV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2, AV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3, lV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3, lV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3, AV105ExtraWWContagemResultadoRelComparacaoDemandasDS_32_Contagemresultado_datadmn3, AV106ExtraWWContagemResultadoRelComparacaoDemandasDS_33_Contagemresultado_datadmn_to3, AV107ExtraWWContagemResultadoRelComparacaoDemandasDS_34_Contagemresultado_dataentregareal3, AV107ExtraWWContagemResultadoRelComparacaoDemandasDS_34_Contagemresultado_dataentregareal3, AV108ExtraWWContagemResultadoRelComparacaoDemandasDS_35_Contagemresultado_dataentregareal_to3, AV108ExtraWWContagemResultadoRelComparacaoDemandasDS_35_Contagemresultado_dataentregareal_to3, AV109ExtraWWContagemResultadoRelComparacaoDemandasDS_36_Contagemresultado_statusdmn3, AV110ExtraWWContagemResultadoRelComparacaoDemandasDS_37_Contagemresultado_statuscnt3, lV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3, lV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3, AV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3, AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3, AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3, AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3, AV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3, lV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3, lV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3, AV115ExtraWWContagemResultadoRelComparacaoDemandasDS_42_Contagemresultado_servicogrupo, AV116ExtraWWContagemResultadoRelComparacaoDemandasDS_43_Contagemresultado_servico, AV117ExtraWWContagemResultadoRelComparacaoDemandasDS_44_Contagemresultado_cntadaosvinc, AV118ExtraWWContagemResultadoRelComparacaoDemandasDS_45_Contagemresultado_sergrupovinc, AV119ExtraWWContagemResultadoRelComparacaoDemandasDS_46_Contagemresultado_codsrvvnc});
         GRID_nRecordCount = H00TB12_AGRID_nRecordCount[0];
         pr_default.close(10);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV74ExtraWWContagemResultadoRelComparacaoDemandasDS_1_Contratada_areatrabalhocod = AV13Contratada_AreaTrabalhoCod;
         AV75ExtraWWContagemResultadoRelComparacaoDemandasDS_2_Contagemresultado_osvinculada = AV65ContagemResultado_OSVinculada;
         AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1 = AV14DynamicFiltersSelector1;
         AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 = AV15DynamicFiltersOperator1;
         AV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1 = AV16ContagemResultado_DemandaFM1;
         AV79ExtraWWContagemResultadoRelComparacaoDemandasDS_6_Contagemresultado_datadmn1 = AV17ContagemResultado_DataDmn1;
         AV80ExtraWWContagemResultadoRelComparacaoDemandasDS_7_Contagemresultado_datadmn_to1 = AV18ContagemResultado_DataDmn_To1;
         AV81ExtraWWContagemResultadoRelComparacaoDemandasDS_8_Contagemresultado_dataentregareal1 = AV19ContagemResultado_DataEntregaReal1;
         AV82ExtraWWContagemResultadoRelComparacaoDemandasDS_9_Contagemresultado_dataentregareal_to1 = AV20ContagemResultado_DataEntregaReal_To1;
         AV83ExtraWWContagemResultadoRelComparacaoDemandasDS_10_Contagemresultado_statusdmn1 = AV21ContagemResultado_StatusDmn1;
         AV84ExtraWWContagemResultadoRelComparacaoDemandasDS_11_Contagemresultado_statuscnt1 = AV22ContagemResultado_StatusCnt1;
         AV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1 = AV23ContagemResultado_Descricao1;
         AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1 = AV24ContagemResultado_SistemaCod1;
         AV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1 = AV25ContagemResultado_Agrupador1;
         AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 = AV26DynamicFiltersEnabled2;
         AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2 = AV27DynamicFiltersSelector2;
         AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 = AV28DynamicFiltersOperator2;
         AV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2 = AV29ContagemResultado_DemandaFM2;
         AV92ExtraWWContagemResultadoRelComparacaoDemandasDS_19_Contagemresultado_datadmn2 = AV30ContagemResultado_DataDmn2;
         AV93ExtraWWContagemResultadoRelComparacaoDemandasDS_20_Contagemresultado_datadmn_to2 = AV31ContagemResultado_DataDmn_To2;
         AV94ExtraWWContagemResultadoRelComparacaoDemandasDS_21_Contagemresultado_dataentregareal2 = AV32ContagemResultado_DataEntregaReal2;
         AV95ExtraWWContagemResultadoRelComparacaoDemandasDS_22_Contagemresultado_dataentregareal_to2 = AV33ContagemResultado_DataEntregaReal_To2;
         AV96ExtraWWContagemResultadoRelComparacaoDemandasDS_23_Contagemresultado_statusdmn2 = AV34ContagemResultado_StatusDmn2;
         AV97ExtraWWContagemResultadoRelComparacaoDemandasDS_24_Contagemresultado_statuscnt2 = AV35ContagemResultado_StatusCnt2;
         AV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2 = AV36ContagemResultado_Descricao2;
         AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2 = AV37ContagemResultado_SistemaCod2;
         AV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2 = AV38ContagemResultado_Agrupador2;
         AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 = AV39DynamicFiltersEnabled3;
         AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3 = AV40DynamicFiltersSelector3;
         AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 = AV41DynamicFiltersOperator3;
         AV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3 = AV42ContagemResultado_DemandaFM3;
         AV105ExtraWWContagemResultadoRelComparacaoDemandasDS_32_Contagemresultado_datadmn3 = AV43ContagemResultado_DataDmn3;
         AV106ExtraWWContagemResultadoRelComparacaoDemandasDS_33_Contagemresultado_datadmn_to3 = AV44ContagemResultado_DataDmn_To3;
         AV107ExtraWWContagemResultadoRelComparacaoDemandasDS_34_Contagemresultado_dataentregareal3 = AV45ContagemResultado_DataEntregaReal3;
         AV108ExtraWWContagemResultadoRelComparacaoDemandasDS_35_Contagemresultado_dataentregareal_to3 = AV46ContagemResultado_DataEntregaReal_To3;
         AV109ExtraWWContagemResultadoRelComparacaoDemandasDS_36_Contagemresultado_statusdmn3 = AV47ContagemResultado_StatusDmn3;
         AV110ExtraWWContagemResultadoRelComparacaoDemandasDS_37_Contagemresultado_statuscnt3 = AV48ContagemResultado_StatusCnt3;
         AV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3 = AV49ContagemResultado_Descricao3;
         AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3 = AV50ContagemResultado_SistemaCod3;
         AV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3 = AV51ContagemResultado_Agrupador3;
         AV114ExtraWWContagemResultadoRelComparacaoDemandasDS_41_Contagemresultado_contratadacod = AV66ContagemResultado_ContratadaCod;
         AV115ExtraWWContagemResultadoRelComparacaoDemandasDS_42_Contagemresultado_servicogrupo = AV67ContagemResultado_ServicoGrupo;
         AV116ExtraWWContagemResultadoRelComparacaoDemandasDS_43_Contagemresultado_servico = AV68ContagemResultado_Servico;
         AV117ExtraWWContagemResultadoRelComparacaoDemandasDS_44_Contagemresultado_cntadaosvinc = AV69ContagemResultado_CntadaOsVinc;
         AV118ExtraWWContagemResultadoRelComparacaoDemandasDS_45_Contagemresultado_sergrupovinc = AV71ContagemResultado_SerGrupoVinc;
         AV119ExtraWWContagemResultadoRelComparacaoDemandasDS_46_Contagemresultado_codsrvvnc = AV70ContagemResultado_CodSrvVnc;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV67ContagemResultado_ServicoGrupo, AV68ContagemResultado_Servico, AV69ContagemResultado_CntadaOsVinc, AV71ContagemResultado_SerGrupoVinc, AV70ContagemResultado_CodSrvVnc, AV14DynamicFiltersSelector1, AV15DynamicFiltersOperator1, AV16ContagemResultado_DemandaFM1, AV17ContagemResultado_DataDmn1, AV18ContagemResultado_DataDmn_To1, AV19ContagemResultado_DataEntregaReal1, AV20ContagemResultado_DataEntregaReal_To1, AV21ContagemResultado_StatusDmn1, AV22ContagemResultado_StatusCnt1, AV23ContagemResultado_Descricao1, AV24ContagemResultado_SistemaCod1, AV25ContagemResultado_Agrupador1, AV27DynamicFiltersSelector2, AV28DynamicFiltersOperator2, AV29ContagemResultado_DemandaFM2, AV30ContagemResultado_DataDmn2, AV31ContagemResultado_DataDmn_To2, AV32ContagemResultado_DataEntregaReal2, AV33ContagemResultado_DataEntregaReal_To2, AV34ContagemResultado_StatusDmn2, AV35ContagemResultado_StatusCnt2, AV36ContagemResultado_Descricao2, AV37ContagemResultado_SistemaCod2, AV38ContagemResultado_Agrupador2, AV40DynamicFiltersSelector3, AV41DynamicFiltersOperator3, AV42ContagemResultado_DemandaFM3, AV43ContagemResultado_DataDmn3, AV44ContagemResultado_DataDmn_To3, AV45ContagemResultado_DataEntregaReal3, AV46ContagemResultado_DataEntregaReal_To3, AV47ContagemResultado_StatusDmn3, AV48ContagemResultado_StatusCnt3, AV49ContagemResultado_Descricao3, AV50ContagemResultado_SistemaCod3, AV51ContagemResultado_Agrupador3, AV26DynamicFiltersEnabled2, AV39DynamicFiltersEnabled3, AV6WWPContext, AV13Contratada_AreaTrabalhoCod, AV65ContagemResultado_OSVinculada, AV66ContagemResultado_ContratadaCod, AV122Pgmname, AV10GridState, AV53DynamicFiltersIgnoreFirst, AV52DynamicFiltersRemoving, A456ContagemResultado_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV74ExtraWWContagemResultadoRelComparacaoDemandasDS_1_Contratada_areatrabalhocod = AV13Contratada_AreaTrabalhoCod;
         AV75ExtraWWContagemResultadoRelComparacaoDemandasDS_2_Contagemresultado_osvinculada = AV65ContagemResultado_OSVinculada;
         AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1 = AV14DynamicFiltersSelector1;
         AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 = AV15DynamicFiltersOperator1;
         AV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1 = AV16ContagemResultado_DemandaFM1;
         AV79ExtraWWContagemResultadoRelComparacaoDemandasDS_6_Contagemresultado_datadmn1 = AV17ContagemResultado_DataDmn1;
         AV80ExtraWWContagemResultadoRelComparacaoDemandasDS_7_Contagemresultado_datadmn_to1 = AV18ContagemResultado_DataDmn_To1;
         AV81ExtraWWContagemResultadoRelComparacaoDemandasDS_8_Contagemresultado_dataentregareal1 = AV19ContagemResultado_DataEntregaReal1;
         AV82ExtraWWContagemResultadoRelComparacaoDemandasDS_9_Contagemresultado_dataentregareal_to1 = AV20ContagemResultado_DataEntregaReal_To1;
         AV83ExtraWWContagemResultadoRelComparacaoDemandasDS_10_Contagemresultado_statusdmn1 = AV21ContagemResultado_StatusDmn1;
         AV84ExtraWWContagemResultadoRelComparacaoDemandasDS_11_Contagemresultado_statuscnt1 = AV22ContagemResultado_StatusCnt1;
         AV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1 = AV23ContagemResultado_Descricao1;
         AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1 = AV24ContagemResultado_SistemaCod1;
         AV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1 = AV25ContagemResultado_Agrupador1;
         AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 = AV26DynamicFiltersEnabled2;
         AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2 = AV27DynamicFiltersSelector2;
         AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 = AV28DynamicFiltersOperator2;
         AV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2 = AV29ContagemResultado_DemandaFM2;
         AV92ExtraWWContagemResultadoRelComparacaoDemandasDS_19_Contagemresultado_datadmn2 = AV30ContagemResultado_DataDmn2;
         AV93ExtraWWContagemResultadoRelComparacaoDemandasDS_20_Contagemresultado_datadmn_to2 = AV31ContagemResultado_DataDmn_To2;
         AV94ExtraWWContagemResultadoRelComparacaoDemandasDS_21_Contagemresultado_dataentregareal2 = AV32ContagemResultado_DataEntregaReal2;
         AV95ExtraWWContagemResultadoRelComparacaoDemandasDS_22_Contagemresultado_dataentregareal_to2 = AV33ContagemResultado_DataEntregaReal_To2;
         AV96ExtraWWContagemResultadoRelComparacaoDemandasDS_23_Contagemresultado_statusdmn2 = AV34ContagemResultado_StatusDmn2;
         AV97ExtraWWContagemResultadoRelComparacaoDemandasDS_24_Contagemresultado_statuscnt2 = AV35ContagemResultado_StatusCnt2;
         AV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2 = AV36ContagemResultado_Descricao2;
         AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2 = AV37ContagemResultado_SistemaCod2;
         AV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2 = AV38ContagemResultado_Agrupador2;
         AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 = AV39DynamicFiltersEnabled3;
         AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3 = AV40DynamicFiltersSelector3;
         AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 = AV41DynamicFiltersOperator3;
         AV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3 = AV42ContagemResultado_DemandaFM3;
         AV105ExtraWWContagemResultadoRelComparacaoDemandasDS_32_Contagemresultado_datadmn3 = AV43ContagemResultado_DataDmn3;
         AV106ExtraWWContagemResultadoRelComparacaoDemandasDS_33_Contagemresultado_datadmn_to3 = AV44ContagemResultado_DataDmn_To3;
         AV107ExtraWWContagemResultadoRelComparacaoDemandasDS_34_Contagemresultado_dataentregareal3 = AV45ContagemResultado_DataEntregaReal3;
         AV108ExtraWWContagemResultadoRelComparacaoDemandasDS_35_Contagemresultado_dataentregareal_to3 = AV46ContagemResultado_DataEntregaReal_To3;
         AV109ExtraWWContagemResultadoRelComparacaoDemandasDS_36_Contagemresultado_statusdmn3 = AV47ContagemResultado_StatusDmn3;
         AV110ExtraWWContagemResultadoRelComparacaoDemandasDS_37_Contagemresultado_statuscnt3 = AV48ContagemResultado_StatusCnt3;
         AV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3 = AV49ContagemResultado_Descricao3;
         AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3 = AV50ContagemResultado_SistemaCod3;
         AV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3 = AV51ContagemResultado_Agrupador3;
         AV114ExtraWWContagemResultadoRelComparacaoDemandasDS_41_Contagemresultado_contratadacod = AV66ContagemResultado_ContratadaCod;
         AV115ExtraWWContagemResultadoRelComparacaoDemandasDS_42_Contagemresultado_servicogrupo = AV67ContagemResultado_ServicoGrupo;
         AV116ExtraWWContagemResultadoRelComparacaoDemandasDS_43_Contagemresultado_servico = AV68ContagemResultado_Servico;
         AV117ExtraWWContagemResultadoRelComparacaoDemandasDS_44_Contagemresultado_cntadaosvinc = AV69ContagemResultado_CntadaOsVinc;
         AV118ExtraWWContagemResultadoRelComparacaoDemandasDS_45_Contagemresultado_sergrupovinc = AV71ContagemResultado_SerGrupoVinc;
         AV119ExtraWWContagemResultadoRelComparacaoDemandasDS_46_Contagemresultado_codsrvvnc = AV70ContagemResultado_CodSrvVnc;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV67ContagemResultado_ServicoGrupo, AV68ContagemResultado_Servico, AV69ContagemResultado_CntadaOsVinc, AV71ContagemResultado_SerGrupoVinc, AV70ContagemResultado_CodSrvVnc, AV14DynamicFiltersSelector1, AV15DynamicFiltersOperator1, AV16ContagemResultado_DemandaFM1, AV17ContagemResultado_DataDmn1, AV18ContagemResultado_DataDmn_To1, AV19ContagemResultado_DataEntregaReal1, AV20ContagemResultado_DataEntregaReal_To1, AV21ContagemResultado_StatusDmn1, AV22ContagemResultado_StatusCnt1, AV23ContagemResultado_Descricao1, AV24ContagemResultado_SistemaCod1, AV25ContagemResultado_Agrupador1, AV27DynamicFiltersSelector2, AV28DynamicFiltersOperator2, AV29ContagemResultado_DemandaFM2, AV30ContagemResultado_DataDmn2, AV31ContagemResultado_DataDmn_To2, AV32ContagemResultado_DataEntregaReal2, AV33ContagemResultado_DataEntregaReal_To2, AV34ContagemResultado_StatusDmn2, AV35ContagemResultado_StatusCnt2, AV36ContagemResultado_Descricao2, AV37ContagemResultado_SistemaCod2, AV38ContagemResultado_Agrupador2, AV40DynamicFiltersSelector3, AV41DynamicFiltersOperator3, AV42ContagemResultado_DemandaFM3, AV43ContagemResultado_DataDmn3, AV44ContagemResultado_DataDmn_To3, AV45ContagemResultado_DataEntregaReal3, AV46ContagemResultado_DataEntregaReal_To3, AV47ContagemResultado_StatusDmn3, AV48ContagemResultado_StatusCnt3, AV49ContagemResultado_Descricao3, AV50ContagemResultado_SistemaCod3, AV51ContagemResultado_Agrupador3, AV26DynamicFiltersEnabled2, AV39DynamicFiltersEnabled3, AV6WWPContext, AV13Contratada_AreaTrabalhoCod, AV65ContagemResultado_OSVinculada, AV66ContagemResultado_ContratadaCod, AV122Pgmname, AV10GridState, AV53DynamicFiltersIgnoreFirst, AV52DynamicFiltersRemoving, A456ContagemResultado_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV74ExtraWWContagemResultadoRelComparacaoDemandasDS_1_Contratada_areatrabalhocod = AV13Contratada_AreaTrabalhoCod;
         AV75ExtraWWContagemResultadoRelComparacaoDemandasDS_2_Contagemresultado_osvinculada = AV65ContagemResultado_OSVinculada;
         AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1 = AV14DynamicFiltersSelector1;
         AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 = AV15DynamicFiltersOperator1;
         AV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1 = AV16ContagemResultado_DemandaFM1;
         AV79ExtraWWContagemResultadoRelComparacaoDemandasDS_6_Contagemresultado_datadmn1 = AV17ContagemResultado_DataDmn1;
         AV80ExtraWWContagemResultadoRelComparacaoDemandasDS_7_Contagemresultado_datadmn_to1 = AV18ContagemResultado_DataDmn_To1;
         AV81ExtraWWContagemResultadoRelComparacaoDemandasDS_8_Contagemresultado_dataentregareal1 = AV19ContagemResultado_DataEntregaReal1;
         AV82ExtraWWContagemResultadoRelComparacaoDemandasDS_9_Contagemresultado_dataentregareal_to1 = AV20ContagemResultado_DataEntregaReal_To1;
         AV83ExtraWWContagemResultadoRelComparacaoDemandasDS_10_Contagemresultado_statusdmn1 = AV21ContagemResultado_StatusDmn1;
         AV84ExtraWWContagemResultadoRelComparacaoDemandasDS_11_Contagemresultado_statuscnt1 = AV22ContagemResultado_StatusCnt1;
         AV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1 = AV23ContagemResultado_Descricao1;
         AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1 = AV24ContagemResultado_SistemaCod1;
         AV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1 = AV25ContagemResultado_Agrupador1;
         AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 = AV26DynamicFiltersEnabled2;
         AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2 = AV27DynamicFiltersSelector2;
         AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 = AV28DynamicFiltersOperator2;
         AV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2 = AV29ContagemResultado_DemandaFM2;
         AV92ExtraWWContagemResultadoRelComparacaoDemandasDS_19_Contagemresultado_datadmn2 = AV30ContagemResultado_DataDmn2;
         AV93ExtraWWContagemResultadoRelComparacaoDemandasDS_20_Contagemresultado_datadmn_to2 = AV31ContagemResultado_DataDmn_To2;
         AV94ExtraWWContagemResultadoRelComparacaoDemandasDS_21_Contagemresultado_dataentregareal2 = AV32ContagemResultado_DataEntregaReal2;
         AV95ExtraWWContagemResultadoRelComparacaoDemandasDS_22_Contagemresultado_dataentregareal_to2 = AV33ContagemResultado_DataEntregaReal_To2;
         AV96ExtraWWContagemResultadoRelComparacaoDemandasDS_23_Contagemresultado_statusdmn2 = AV34ContagemResultado_StatusDmn2;
         AV97ExtraWWContagemResultadoRelComparacaoDemandasDS_24_Contagemresultado_statuscnt2 = AV35ContagemResultado_StatusCnt2;
         AV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2 = AV36ContagemResultado_Descricao2;
         AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2 = AV37ContagemResultado_SistemaCod2;
         AV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2 = AV38ContagemResultado_Agrupador2;
         AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 = AV39DynamicFiltersEnabled3;
         AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3 = AV40DynamicFiltersSelector3;
         AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 = AV41DynamicFiltersOperator3;
         AV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3 = AV42ContagemResultado_DemandaFM3;
         AV105ExtraWWContagemResultadoRelComparacaoDemandasDS_32_Contagemresultado_datadmn3 = AV43ContagemResultado_DataDmn3;
         AV106ExtraWWContagemResultadoRelComparacaoDemandasDS_33_Contagemresultado_datadmn_to3 = AV44ContagemResultado_DataDmn_To3;
         AV107ExtraWWContagemResultadoRelComparacaoDemandasDS_34_Contagemresultado_dataentregareal3 = AV45ContagemResultado_DataEntregaReal3;
         AV108ExtraWWContagemResultadoRelComparacaoDemandasDS_35_Contagemresultado_dataentregareal_to3 = AV46ContagemResultado_DataEntregaReal_To3;
         AV109ExtraWWContagemResultadoRelComparacaoDemandasDS_36_Contagemresultado_statusdmn3 = AV47ContagemResultado_StatusDmn3;
         AV110ExtraWWContagemResultadoRelComparacaoDemandasDS_37_Contagemresultado_statuscnt3 = AV48ContagemResultado_StatusCnt3;
         AV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3 = AV49ContagemResultado_Descricao3;
         AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3 = AV50ContagemResultado_SistemaCod3;
         AV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3 = AV51ContagemResultado_Agrupador3;
         AV114ExtraWWContagemResultadoRelComparacaoDemandasDS_41_Contagemresultado_contratadacod = AV66ContagemResultado_ContratadaCod;
         AV115ExtraWWContagemResultadoRelComparacaoDemandasDS_42_Contagemresultado_servicogrupo = AV67ContagemResultado_ServicoGrupo;
         AV116ExtraWWContagemResultadoRelComparacaoDemandasDS_43_Contagemresultado_servico = AV68ContagemResultado_Servico;
         AV117ExtraWWContagemResultadoRelComparacaoDemandasDS_44_Contagemresultado_cntadaosvinc = AV69ContagemResultado_CntadaOsVinc;
         AV118ExtraWWContagemResultadoRelComparacaoDemandasDS_45_Contagemresultado_sergrupovinc = AV71ContagemResultado_SerGrupoVinc;
         AV119ExtraWWContagemResultadoRelComparacaoDemandasDS_46_Contagemresultado_codsrvvnc = AV70ContagemResultado_CodSrvVnc;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV67ContagemResultado_ServicoGrupo, AV68ContagemResultado_Servico, AV69ContagemResultado_CntadaOsVinc, AV71ContagemResultado_SerGrupoVinc, AV70ContagemResultado_CodSrvVnc, AV14DynamicFiltersSelector1, AV15DynamicFiltersOperator1, AV16ContagemResultado_DemandaFM1, AV17ContagemResultado_DataDmn1, AV18ContagemResultado_DataDmn_To1, AV19ContagemResultado_DataEntregaReal1, AV20ContagemResultado_DataEntregaReal_To1, AV21ContagemResultado_StatusDmn1, AV22ContagemResultado_StatusCnt1, AV23ContagemResultado_Descricao1, AV24ContagemResultado_SistemaCod1, AV25ContagemResultado_Agrupador1, AV27DynamicFiltersSelector2, AV28DynamicFiltersOperator2, AV29ContagemResultado_DemandaFM2, AV30ContagemResultado_DataDmn2, AV31ContagemResultado_DataDmn_To2, AV32ContagemResultado_DataEntregaReal2, AV33ContagemResultado_DataEntregaReal_To2, AV34ContagemResultado_StatusDmn2, AV35ContagemResultado_StatusCnt2, AV36ContagemResultado_Descricao2, AV37ContagemResultado_SistemaCod2, AV38ContagemResultado_Agrupador2, AV40DynamicFiltersSelector3, AV41DynamicFiltersOperator3, AV42ContagemResultado_DemandaFM3, AV43ContagemResultado_DataDmn3, AV44ContagemResultado_DataDmn_To3, AV45ContagemResultado_DataEntregaReal3, AV46ContagemResultado_DataEntregaReal_To3, AV47ContagemResultado_StatusDmn3, AV48ContagemResultado_StatusCnt3, AV49ContagemResultado_Descricao3, AV50ContagemResultado_SistemaCod3, AV51ContagemResultado_Agrupador3, AV26DynamicFiltersEnabled2, AV39DynamicFiltersEnabled3, AV6WWPContext, AV13Contratada_AreaTrabalhoCod, AV65ContagemResultado_OSVinculada, AV66ContagemResultado_ContratadaCod, AV122Pgmname, AV10GridState, AV53DynamicFiltersIgnoreFirst, AV52DynamicFiltersRemoving, A456ContagemResultado_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV74ExtraWWContagemResultadoRelComparacaoDemandasDS_1_Contratada_areatrabalhocod = AV13Contratada_AreaTrabalhoCod;
         AV75ExtraWWContagemResultadoRelComparacaoDemandasDS_2_Contagemresultado_osvinculada = AV65ContagemResultado_OSVinculada;
         AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1 = AV14DynamicFiltersSelector1;
         AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 = AV15DynamicFiltersOperator1;
         AV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1 = AV16ContagemResultado_DemandaFM1;
         AV79ExtraWWContagemResultadoRelComparacaoDemandasDS_6_Contagemresultado_datadmn1 = AV17ContagemResultado_DataDmn1;
         AV80ExtraWWContagemResultadoRelComparacaoDemandasDS_7_Contagemresultado_datadmn_to1 = AV18ContagemResultado_DataDmn_To1;
         AV81ExtraWWContagemResultadoRelComparacaoDemandasDS_8_Contagemresultado_dataentregareal1 = AV19ContagemResultado_DataEntregaReal1;
         AV82ExtraWWContagemResultadoRelComparacaoDemandasDS_9_Contagemresultado_dataentregareal_to1 = AV20ContagemResultado_DataEntregaReal_To1;
         AV83ExtraWWContagemResultadoRelComparacaoDemandasDS_10_Contagemresultado_statusdmn1 = AV21ContagemResultado_StatusDmn1;
         AV84ExtraWWContagemResultadoRelComparacaoDemandasDS_11_Contagemresultado_statuscnt1 = AV22ContagemResultado_StatusCnt1;
         AV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1 = AV23ContagemResultado_Descricao1;
         AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1 = AV24ContagemResultado_SistemaCod1;
         AV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1 = AV25ContagemResultado_Agrupador1;
         AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 = AV26DynamicFiltersEnabled2;
         AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2 = AV27DynamicFiltersSelector2;
         AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 = AV28DynamicFiltersOperator2;
         AV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2 = AV29ContagemResultado_DemandaFM2;
         AV92ExtraWWContagemResultadoRelComparacaoDemandasDS_19_Contagemresultado_datadmn2 = AV30ContagemResultado_DataDmn2;
         AV93ExtraWWContagemResultadoRelComparacaoDemandasDS_20_Contagemresultado_datadmn_to2 = AV31ContagemResultado_DataDmn_To2;
         AV94ExtraWWContagemResultadoRelComparacaoDemandasDS_21_Contagemresultado_dataentregareal2 = AV32ContagemResultado_DataEntregaReal2;
         AV95ExtraWWContagemResultadoRelComparacaoDemandasDS_22_Contagemresultado_dataentregareal_to2 = AV33ContagemResultado_DataEntregaReal_To2;
         AV96ExtraWWContagemResultadoRelComparacaoDemandasDS_23_Contagemresultado_statusdmn2 = AV34ContagemResultado_StatusDmn2;
         AV97ExtraWWContagemResultadoRelComparacaoDemandasDS_24_Contagemresultado_statuscnt2 = AV35ContagemResultado_StatusCnt2;
         AV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2 = AV36ContagemResultado_Descricao2;
         AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2 = AV37ContagemResultado_SistemaCod2;
         AV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2 = AV38ContagemResultado_Agrupador2;
         AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 = AV39DynamicFiltersEnabled3;
         AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3 = AV40DynamicFiltersSelector3;
         AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 = AV41DynamicFiltersOperator3;
         AV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3 = AV42ContagemResultado_DemandaFM3;
         AV105ExtraWWContagemResultadoRelComparacaoDemandasDS_32_Contagemresultado_datadmn3 = AV43ContagemResultado_DataDmn3;
         AV106ExtraWWContagemResultadoRelComparacaoDemandasDS_33_Contagemresultado_datadmn_to3 = AV44ContagemResultado_DataDmn_To3;
         AV107ExtraWWContagemResultadoRelComparacaoDemandasDS_34_Contagemresultado_dataentregareal3 = AV45ContagemResultado_DataEntregaReal3;
         AV108ExtraWWContagemResultadoRelComparacaoDemandasDS_35_Contagemresultado_dataentregareal_to3 = AV46ContagemResultado_DataEntregaReal_To3;
         AV109ExtraWWContagemResultadoRelComparacaoDemandasDS_36_Contagemresultado_statusdmn3 = AV47ContagemResultado_StatusDmn3;
         AV110ExtraWWContagemResultadoRelComparacaoDemandasDS_37_Contagemresultado_statuscnt3 = AV48ContagemResultado_StatusCnt3;
         AV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3 = AV49ContagemResultado_Descricao3;
         AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3 = AV50ContagemResultado_SistemaCod3;
         AV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3 = AV51ContagemResultado_Agrupador3;
         AV114ExtraWWContagemResultadoRelComparacaoDemandasDS_41_Contagemresultado_contratadacod = AV66ContagemResultado_ContratadaCod;
         AV115ExtraWWContagemResultadoRelComparacaoDemandasDS_42_Contagemresultado_servicogrupo = AV67ContagemResultado_ServicoGrupo;
         AV116ExtraWWContagemResultadoRelComparacaoDemandasDS_43_Contagemresultado_servico = AV68ContagemResultado_Servico;
         AV117ExtraWWContagemResultadoRelComparacaoDemandasDS_44_Contagemresultado_cntadaosvinc = AV69ContagemResultado_CntadaOsVinc;
         AV118ExtraWWContagemResultadoRelComparacaoDemandasDS_45_Contagemresultado_sergrupovinc = AV71ContagemResultado_SerGrupoVinc;
         AV119ExtraWWContagemResultadoRelComparacaoDemandasDS_46_Contagemresultado_codsrvvnc = AV70ContagemResultado_CodSrvVnc;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV67ContagemResultado_ServicoGrupo, AV68ContagemResultado_Servico, AV69ContagemResultado_CntadaOsVinc, AV71ContagemResultado_SerGrupoVinc, AV70ContagemResultado_CodSrvVnc, AV14DynamicFiltersSelector1, AV15DynamicFiltersOperator1, AV16ContagemResultado_DemandaFM1, AV17ContagemResultado_DataDmn1, AV18ContagemResultado_DataDmn_To1, AV19ContagemResultado_DataEntregaReal1, AV20ContagemResultado_DataEntregaReal_To1, AV21ContagemResultado_StatusDmn1, AV22ContagemResultado_StatusCnt1, AV23ContagemResultado_Descricao1, AV24ContagemResultado_SistemaCod1, AV25ContagemResultado_Agrupador1, AV27DynamicFiltersSelector2, AV28DynamicFiltersOperator2, AV29ContagemResultado_DemandaFM2, AV30ContagemResultado_DataDmn2, AV31ContagemResultado_DataDmn_To2, AV32ContagemResultado_DataEntregaReal2, AV33ContagemResultado_DataEntregaReal_To2, AV34ContagemResultado_StatusDmn2, AV35ContagemResultado_StatusCnt2, AV36ContagemResultado_Descricao2, AV37ContagemResultado_SistemaCod2, AV38ContagemResultado_Agrupador2, AV40DynamicFiltersSelector3, AV41DynamicFiltersOperator3, AV42ContagemResultado_DemandaFM3, AV43ContagemResultado_DataDmn3, AV44ContagemResultado_DataDmn_To3, AV45ContagemResultado_DataEntregaReal3, AV46ContagemResultado_DataEntregaReal_To3, AV47ContagemResultado_StatusDmn3, AV48ContagemResultado_StatusCnt3, AV49ContagemResultado_Descricao3, AV50ContagemResultado_SistemaCod3, AV51ContagemResultado_Agrupador3, AV26DynamicFiltersEnabled2, AV39DynamicFiltersEnabled3, AV6WWPContext, AV13Contratada_AreaTrabalhoCod, AV65ContagemResultado_OSVinculada, AV66ContagemResultado_ContratadaCod, AV122Pgmname, AV10GridState, AV53DynamicFiltersIgnoreFirst, AV52DynamicFiltersRemoving, A456ContagemResultado_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV74ExtraWWContagemResultadoRelComparacaoDemandasDS_1_Contratada_areatrabalhocod = AV13Contratada_AreaTrabalhoCod;
         AV75ExtraWWContagemResultadoRelComparacaoDemandasDS_2_Contagemresultado_osvinculada = AV65ContagemResultado_OSVinculada;
         AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1 = AV14DynamicFiltersSelector1;
         AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 = AV15DynamicFiltersOperator1;
         AV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1 = AV16ContagemResultado_DemandaFM1;
         AV79ExtraWWContagemResultadoRelComparacaoDemandasDS_6_Contagemresultado_datadmn1 = AV17ContagemResultado_DataDmn1;
         AV80ExtraWWContagemResultadoRelComparacaoDemandasDS_7_Contagemresultado_datadmn_to1 = AV18ContagemResultado_DataDmn_To1;
         AV81ExtraWWContagemResultadoRelComparacaoDemandasDS_8_Contagemresultado_dataentregareal1 = AV19ContagemResultado_DataEntregaReal1;
         AV82ExtraWWContagemResultadoRelComparacaoDemandasDS_9_Contagemresultado_dataentregareal_to1 = AV20ContagemResultado_DataEntregaReal_To1;
         AV83ExtraWWContagemResultadoRelComparacaoDemandasDS_10_Contagemresultado_statusdmn1 = AV21ContagemResultado_StatusDmn1;
         AV84ExtraWWContagemResultadoRelComparacaoDemandasDS_11_Contagemresultado_statuscnt1 = AV22ContagemResultado_StatusCnt1;
         AV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1 = AV23ContagemResultado_Descricao1;
         AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1 = AV24ContagemResultado_SistemaCod1;
         AV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1 = AV25ContagemResultado_Agrupador1;
         AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 = AV26DynamicFiltersEnabled2;
         AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2 = AV27DynamicFiltersSelector2;
         AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 = AV28DynamicFiltersOperator2;
         AV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2 = AV29ContagemResultado_DemandaFM2;
         AV92ExtraWWContagemResultadoRelComparacaoDemandasDS_19_Contagemresultado_datadmn2 = AV30ContagemResultado_DataDmn2;
         AV93ExtraWWContagemResultadoRelComparacaoDemandasDS_20_Contagemresultado_datadmn_to2 = AV31ContagemResultado_DataDmn_To2;
         AV94ExtraWWContagemResultadoRelComparacaoDemandasDS_21_Contagemresultado_dataentregareal2 = AV32ContagemResultado_DataEntregaReal2;
         AV95ExtraWWContagemResultadoRelComparacaoDemandasDS_22_Contagemresultado_dataentregareal_to2 = AV33ContagemResultado_DataEntregaReal_To2;
         AV96ExtraWWContagemResultadoRelComparacaoDemandasDS_23_Contagemresultado_statusdmn2 = AV34ContagemResultado_StatusDmn2;
         AV97ExtraWWContagemResultadoRelComparacaoDemandasDS_24_Contagemresultado_statuscnt2 = AV35ContagemResultado_StatusCnt2;
         AV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2 = AV36ContagemResultado_Descricao2;
         AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2 = AV37ContagemResultado_SistemaCod2;
         AV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2 = AV38ContagemResultado_Agrupador2;
         AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 = AV39DynamicFiltersEnabled3;
         AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3 = AV40DynamicFiltersSelector3;
         AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 = AV41DynamicFiltersOperator3;
         AV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3 = AV42ContagemResultado_DemandaFM3;
         AV105ExtraWWContagemResultadoRelComparacaoDemandasDS_32_Contagemresultado_datadmn3 = AV43ContagemResultado_DataDmn3;
         AV106ExtraWWContagemResultadoRelComparacaoDemandasDS_33_Contagemresultado_datadmn_to3 = AV44ContagemResultado_DataDmn_To3;
         AV107ExtraWWContagemResultadoRelComparacaoDemandasDS_34_Contagemresultado_dataentregareal3 = AV45ContagemResultado_DataEntregaReal3;
         AV108ExtraWWContagemResultadoRelComparacaoDemandasDS_35_Contagemresultado_dataentregareal_to3 = AV46ContagemResultado_DataEntregaReal_To3;
         AV109ExtraWWContagemResultadoRelComparacaoDemandasDS_36_Contagemresultado_statusdmn3 = AV47ContagemResultado_StatusDmn3;
         AV110ExtraWWContagemResultadoRelComparacaoDemandasDS_37_Contagemresultado_statuscnt3 = AV48ContagemResultado_StatusCnt3;
         AV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3 = AV49ContagemResultado_Descricao3;
         AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3 = AV50ContagemResultado_SistemaCod3;
         AV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3 = AV51ContagemResultado_Agrupador3;
         AV114ExtraWWContagemResultadoRelComparacaoDemandasDS_41_Contagemresultado_contratadacod = AV66ContagemResultado_ContratadaCod;
         AV115ExtraWWContagemResultadoRelComparacaoDemandasDS_42_Contagemresultado_servicogrupo = AV67ContagemResultado_ServicoGrupo;
         AV116ExtraWWContagemResultadoRelComparacaoDemandasDS_43_Contagemresultado_servico = AV68ContagemResultado_Servico;
         AV117ExtraWWContagemResultadoRelComparacaoDemandasDS_44_Contagemresultado_cntadaosvinc = AV69ContagemResultado_CntadaOsVinc;
         AV118ExtraWWContagemResultadoRelComparacaoDemandasDS_45_Contagemresultado_sergrupovinc = AV71ContagemResultado_SerGrupoVinc;
         AV119ExtraWWContagemResultadoRelComparacaoDemandasDS_46_Contagemresultado_codsrvvnc = AV70ContagemResultado_CodSrvVnc;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV67ContagemResultado_ServicoGrupo, AV68ContagemResultado_Servico, AV69ContagemResultado_CntadaOsVinc, AV71ContagemResultado_SerGrupoVinc, AV70ContagemResultado_CodSrvVnc, AV14DynamicFiltersSelector1, AV15DynamicFiltersOperator1, AV16ContagemResultado_DemandaFM1, AV17ContagemResultado_DataDmn1, AV18ContagemResultado_DataDmn_To1, AV19ContagemResultado_DataEntregaReal1, AV20ContagemResultado_DataEntregaReal_To1, AV21ContagemResultado_StatusDmn1, AV22ContagemResultado_StatusCnt1, AV23ContagemResultado_Descricao1, AV24ContagemResultado_SistemaCod1, AV25ContagemResultado_Agrupador1, AV27DynamicFiltersSelector2, AV28DynamicFiltersOperator2, AV29ContagemResultado_DemandaFM2, AV30ContagemResultado_DataDmn2, AV31ContagemResultado_DataDmn_To2, AV32ContagemResultado_DataEntregaReal2, AV33ContagemResultado_DataEntregaReal_To2, AV34ContagemResultado_StatusDmn2, AV35ContagemResultado_StatusCnt2, AV36ContagemResultado_Descricao2, AV37ContagemResultado_SistemaCod2, AV38ContagemResultado_Agrupador2, AV40DynamicFiltersSelector3, AV41DynamicFiltersOperator3, AV42ContagemResultado_DemandaFM3, AV43ContagemResultado_DataDmn3, AV44ContagemResultado_DataDmn_To3, AV45ContagemResultado_DataEntregaReal3, AV46ContagemResultado_DataEntregaReal_To3, AV47ContagemResultado_StatusDmn3, AV48ContagemResultado_StatusCnt3, AV49ContagemResultado_Descricao3, AV50ContagemResultado_SistemaCod3, AV51ContagemResultado_Agrupador3, AV26DynamicFiltersEnabled2, AV39DynamicFiltersEnabled3, AV6WWPContext, AV13Contratada_AreaTrabalhoCod, AV65ContagemResultado_OSVinculada, AV66ContagemResultado_ContratadaCod, AV122Pgmname, AV10GridState, AV53DynamicFiltersIgnoreFirst, AV52DynamicFiltersRemoving, A456ContagemResultado_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPTB0( )
      {
         /* Before Start, stand alone formulas. */
         AV122Pgmname = "ExtraWWContagemResultadoRelComparacaoDemandas";
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E23TB2 */
         E23TB2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvCONTAGEMRESULTADO_CONTRATADACOD_htmlTB2( AV6WWPContext) ;
         GXVvCONTAGEMRESULTADO_CNTADAOSVINC_htmlTB2( AV6WWPContext) ;
         GXVvCONTAGEMRESULTADO_SISTEMACOD1_htmlTB2( AV6WWPContext) ;
         GXVvCONTAGEMRESULTADO_SISTEMACOD2_htmlTB2( AV6WWPContext) ;
         GXVvCONTAGEMRESULTADO_SISTEMACOD3_htmlTB2( AV6WWPContext) ;
         GXVvCONTAGEMRESULTADO_SERVICOGRUPO_htmlTB2( AV66ContagemResultado_ContratadaCod) ;
         GXVvCONTAGEMRESULTADO_SERVICO_htmlTB2( AV66ContagemResultado_ContratadaCod, AV67ContagemResultado_ServicoGrupo) ;
         GXVvCONTAGEMRESULTADO_SERGRUPOVINC_htmlTB2( AV69ContagemResultado_CntadaOsVinc) ;
         GXVvCONTAGEMRESULTADO_CODSRVVNC_htmlTB2( AV69ContagemResultado_CntadaOsVinc, AV71ContagemResultado_SerGrupoVinc) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratada_areatrabalhocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratada_areatrabalhocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATADA_AREATRABALHOCOD");
               GX_FocusControl = edtavContratada_areatrabalhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13Contratada_AreaTrabalhoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Contratada_AreaTrabalhoCod), 6, 0)));
            }
            else
            {
               AV13Contratada_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtavContratada_areatrabalhocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Contratada_AreaTrabalhoCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_osvinculada_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_osvinculada_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_OSVINCULADA");
               GX_FocusControl = edtavContagemresultado_osvinculada_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV65ContagemResultado_OSVinculada = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65ContagemResultado_OSVinculada), 6, 0)));
            }
            else
            {
               AV65ContagemResultado_OSVinculada = (int)(context.localUtil.CToN( cgiGet( edtavContagemresultado_osvinculada_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65ContagemResultado_OSVinculada), 6, 0)));
            }
            dynavContagemresultado_contratadacod.Name = dynavContagemresultado_contratadacod_Internalname;
            dynavContagemresultado_contratadacod.CurrentValue = cgiGet( dynavContagemresultado_contratadacod_Internalname);
            AV66ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_contratadacod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66ContagemResultado_ContratadaCod), 6, 0)));
            dynavContagemresultado_servicogrupo.Name = dynavContagemresultado_servicogrupo_Internalname;
            dynavContagemresultado_servicogrupo.CurrentValue = cgiGet( dynavContagemresultado_servicogrupo_Internalname);
            AV67ContagemResultado_ServicoGrupo = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_servicogrupo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ContagemResultado_ServicoGrupo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67ContagemResultado_ServicoGrupo), 6, 0)));
            dynavContagemresultado_servico.Name = dynavContagemresultado_servico_Internalname;
            dynavContagemresultado_servico.CurrentValue = cgiGet( dynavContagemresultado_servico_Internalname);
            AV68ContagemResultado_Servico = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_servico_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68ContagemResultado_Servico), 6, 0)));
            dynavContagemresultado_cntadaosvinc.Name = dynavContagemresultado_cntadaosvinc_Internalname;
            dynavContagemresultado_cntadaosvinc.CurrentValue = cgiGet( dynavContagemresultado_cntadaosvinc_Internalname);
            AV69ContagemResultado_CntadaOsVinc = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_cntadaosvinc_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ContagemResultado_CntadaOsVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69ContagemResultado_CntadaOsVinc), 6, 0)));
            dynavContagemresultado_sergrupovinc.Name = dynavContagemresultado_sergrupovinc_Internalname;
            dynavContagemresultado_sergrupovinc.CurrentValue = cgiGet( dynavContagemresultado_sergrupovinc_Internalname);
            AV71ContagemResultado_SerGrupoVinc = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_sergrupovinc_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ContagemResultado_SerGrupoVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71ContagemResultado_SerGrupoVinc), 6, 0)));
            dynavContagemresultado_codsrvvnc.Name = dynavContagemresultado_codsrvvnc_Internalname;
            dynavContagemresultado_codsrvvnc.CurrentValue = cgiGet( dynavContagemresultado_codsrvvnc_Internalname);
            AV70ContagemResultado_CodSrvVnc = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_codsrvvnc_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ContagemResultado_CodSrvVnc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV70ContagemResultado_CodSrvVnc), 6, 0)));
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV14DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV15DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0)));
            AV16ContagemResultado_DemandaFM1 = cgiGet( edtavContagemresultado_demandafm1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultado_DemandaFM1", AV16ContagemResultado_DemandaFM1);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datadmn1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Dmn1"}), 1, "vCONTAGEMRESULTADO_DATADMN1");
               GX_FocusControl = edtavContagemresultado_datadmn1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17ContagemResultado_DataDmn1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultado_DataDmn1", context.localUtil.Format(AV17ContagemResultado_DataDmn1, "99/99/99"));
            }
            else
            {
               AV17ContagemResultado_DataDmn1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datadmn1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultado_DataDmn1", context.localUtil.Format(AV17ContagemResultado_DataDmn1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datadmn_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Dmn_To1"}), 1, "vCONTAGEMRESULTADO_DATADMN_TO1");
               GX_FocusControl = edtavContagemresultado_datadmn_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV18ContagemResultado_DataDmn_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultado_DataDmn_To1", context.localUtil.Format(AV18ContagemResultado_DataDmn_To1, "99/99/99"));
            }
            else
            {
               AV18ContagemResultado_DataDmn_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datadmn_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultado_DataDmn_To1", context.localUtil.Format(AV18ContagemResultado_DataDmn_To1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_dataentregareal1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado_Data Entrega Real1"}), 1, "vCONTAGEMRESULTADO_DATAENTREGAREAL1");
               GX_FocusControl = edtavContagemresultado_dataentregareal1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV19ContagemResultado_DataEntregaReal1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultado_DataEntregaReal1", context.localUtil.TToC( AV19ContagemResultado_DataEntregaReal1, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV19ContagemResultado_DataEntregaReal1 = context.localUtil.CToT( cgiGet( edtavContagemresultado_dataentregareal1_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultado_DataEntregaReal1", context.localUtil.TToC( AV19ContagemResultado_DataEntregaReal1, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_dataentregareal_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado_Data Entrega Real_To1"}), 1, "vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1");
               GX_FocusControl = edtavContagemresultado_dataentregareal_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20ContagemResultado_DataEntregaReal_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultado_DataEntregaReal_To1", context.localUtil.TToC( AV20ContagemResultado_DataEntregaReal_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV20ContagemResultado_DataEntregaReal_To1 = context.localUtil.CToT( cgiGet( edtavContagemresultado_dataentregareal_to1_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultado_DataEntregaReal_To1", context.localUtil.TToC( AV20ContagemResultado_DataEntregaReal_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            cmbavContagemresultado_statusdmn1.Name = cmbavContagemresultado_statusdmn1_Internalname;
            cmbavContagemresultado_statusdmn1.CurrentValue = cgiGet( cmbavContagemresultado_statusdmn1_Internalname);
            AV21ContagemResultado_StatusDmn1 = cgiGet( cmbavContagemresultado_statusdmn1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultado_StatusDmn1", AV21ContagemResultado_StatusDmn1);
            cmbavContagemresultado_statuscnt1.Name = cmbavContagemresultado_statuscnt1_Internalname;
            cmbavContagemresultado_statuscnt1.CurrentValue = cgiGet( cmbavContagemresultado_statuscnt1_Internalname);
            AV22ContagemResultado_StatusCnt1 = (short)(NumberUtil.Val( cgiGet( cmbavContagemresultado_statuscnt1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemResultado_StatusCnt1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22ContagemResultado_StatusCnt1), 2, 0)));
            AV23ContagemResultado_Descricao1 = cgiGet( edtavContagemresultado_descricao1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultado_Descricao1", AV23ContagemResultado_Descricao1);
            dynavContagemresultado_sistemacod1.Name = dynavContagemresultado_sistemacod1_Internalname;
            dynavContagemresultado_sistemacod1.CurrentValue = cgiGet( dynavContagemresultado_sistemacod1_Internalname);
            AV24ContagemResultado_SistemaCod1 = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_sistemacod1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultado_SistemaCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24ContagemResultado_SistemaCod1), 6, 0)));
            AV25ContagemResultado_Agrupador1 = StringUtil.Upper( cgiGet( edtavContagemresultado_agrupador1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContagemResultado_Agrupador1", AV25ContagemResultado_Agrupador1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV27DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector2", AV27DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV28DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator2), 4, 0)));
            AV29ContagemResultado_DemandaFM2 = cgiGet( edtavContagemresultado_demandafm2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultado_DemandaFM2", AV29ContagemResultado_DemandaFM2);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datadmn2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Dmn2"}), 1, "vCONTAGEMRESULTADO_DATADMN2");
               GX_FocusControl = edtavContagemresultado_datadmn2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV30ContagemResultado_DataDmn2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultado_DataDmn2", context.localUtil.Format(AV30ContagemResultado_DataDmn2, "99/99/99"));
            }
            else
            {
               AV30ContagemResultado_DataDmn2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datadmn2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultado_DataDmn2", context.localUtil.Format(AV30ContagemResultado_DataDmn2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datadmn_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Dmn_To2"}), 1, "vCONTAGEMRESULTADO_DATADMN_TO2");
               GX_FocusControl = edtavContagemresultado_datadmn_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV31ContagemResultado_DataDmn_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContagemResultado_DataDmn_To2", context.localUtil.Format(AV31ContagemResultado_DataDmn_To2, "99/99/99"));
            }
            else
            {
               AV31ContagemResultado_DataDmn_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datadmn_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContagemResultado_DataDmn_To2", context.localUtil.Format(AV31ContagemResultado_DataDmn_To2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_dataentregareal2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado_Data Entrega Real2"}), 1, "vCONTAGEMRESULTADO_DATAENTREGAREAL2");
               GX_FocusControl = edtavContagemresultado_dataentregareal2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32ContagemResultado_DataEntregaReal2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultado_DataEntregaReal2", context.localUtil.TToC( AV32ContagemResultado_DataEntregaReal2, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV32ContagemResultado_DataEntregaReal2 = context.localUtil.CToT( cgiGet( edtavContagemresultado_dataentregareal2_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultado_DataEntregaReal2", context.localUtil.TToC( AV32ContagemResultado_DataEntregaReal2, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_dataentregareal_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado_Data Entrega Real_To2"}), 1, "vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2");
               GX_FocusControl = edtavContagemresultado_dataentregareal_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV33ContagemResultado_DataEntregaReal_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultado_DataEntregaReal_To2", context.localUtil.TToC( AV33ContagemResultado_DataEntregaReal_To2, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV33ContagemResultado_DataEntregaReal_To2 = context.localUtil.CToT( cgiGet( edtavContagemresultado_dataentregareal_to2_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultado_DataEntregaReal_To2", context.localUtil.TToC( AV33ContagemResultado_DataEntregaReal_To2, 8, 5, 0, 3, "/", ":", " "));
            }
            cmbavContagemresultado_statusdmn2.Name = cmbavContagemresultado_statusdmn2_Internalname;
            cmbavContagemresultado_statusdmn2.CurrentValue = cgiGet( cmbavContagemresultado_statusdmn2_Internalname);
            AV34ContagemResultado_StatusDmn2 = cgiGet( cmbavContagemresultado_statusdmn2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultado_StatusDmn2", AV34ContagemResultado_StatusDmn2);
            cmbavContagemresultado_statuscnt2.Name = cmbavContagemresultado_statuscnt2_Internalname;
            cmbavContagemresultado_statuscnt2.CurrentValue = cgiGet( cmbavContagemresultado_statuscnt2_Internalname);
            AV35ContagemResultado_StatusCnt2 = (short)(NumberUtil.Val( cgiGet( cmbavContagemresultado_statuscnt2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ContagemResultado_StatusCnt2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35ContagemResultado_StatusCnt2), 2, 0)));
            AV36ContagemResultado_Descricao2 = cgiGet( edtavContagemresultado_descricao2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ContagemResultado_Descricao2", AV36ContagemResultado_Descricao2);
            dynavContagemresultado_sistemacod2.Name = dynavContagemresultado_sistemacod2_Internalname;
            dynavContagemresultado_sistemacod2.CurrentValue = cgiGet( dynavContagemresultado_sistemacod2_Internalname);
            AV37ContagemResultado_SistemaCod2 = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_sistemacod2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContagemResultado_SistemaCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37ContagemResultado_SistemaCod2), 6, 0)));
            AV38ContagemResultado_Agrupador2 = StringUtil.Upper( cgiGet( edtavContagemresultado_agrupador2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContagemResultado_Agrupador2", AV38ContagemResultado_Agrupador2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV40DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40DynamicFiltersSelector3", AV40DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV41DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41DynamicFiltersOperator3), 4, 0)));
            AV42ContagemResultado_DemandaFM3 = cgiGet( edtavContagemresultado_demandafm3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ContagemResultado_DemandaFM3", AV42ContagemResultado_DemandaFM3);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datadmn3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Dmn3"}), 1, "vCONTAGEMRESULTADO_DATADMN3");
               GX_FocusControl = edtavContagemresultado_datadmn3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV43ContagemResultado_DataDmn3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ContagemResultado_DataDmn3", context.localUtil.Format(AV43ContagemResultado_DataDmn3, "99/99/99"));
            }
            else
            {
               AV43ContagemResultado_DataDmn3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datadmn3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ContagemResultado_DataDmn3", context.localUtil.Format(AV43ContagemResultado_DataDmn3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datadmn_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contagem Resultado_Data Dmn_To3"}), 1, "vCONTAGEMRESULTADO_DATADMN_TO3");
               GX_FocusControl = edtavContagemresultado_datadmn_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44ContagemResultado_DataDmn_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ContagemResultado_DataDmn_To3", context.localUtil.Format(AV44ContagemResultado_DataDmn_To3, "99/99/99"));
            }
            else
            {
               AV44ContagemResultado_DataDmn_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datadmn_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ContagemResultado_DataDmn_To3", context.localUtil.Format(AV44ContagemResultado_DataDmn_To3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_dataentregareal3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado_Data Entrega Real3"}), 1, "vCONTAGEMRESULTADO_DATAENTREGAREAL3");
               GX_FocusControl = edtavContagemresultado_dataentregareal3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV45ContagemResultado_DataEntregaReal3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ContagemResultado_DataEntregaReal3", context.localUtil.TToC( AV45ContagemResultado_DataEntregaReal3, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV45ContagemResultado_DataEntregaReal3 = context.localUtil.CToT( cgiGet( edtavContagemresultado_dataentregareal3_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ContagemResultado_DataEntregaReal3", context.localUtil.TToC( AV45ContagemResultado_DataEntregaReal3, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_dataentregareal_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado_Data Entrega Real_To3"}), 1, "vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3");
               GX_FocusControl = edtavContagemresultado_dataentregareal_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV46ContagemResultado_DataEntregaReal_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ContagemResultado_DataEntregaReal_To3", context.localUtil.TToC( AV46ContagemResultado_DataEntregaReal_To3, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV46ContagemResultado_DataEntregaReal_To3 = context.localUtil.CToT( cgiGet( edtavContagemresultado_dataentregareal_to3_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ContagemResultado_DataEntregaReal_To3", context.localUtil.TToC( AV46ContagemResultado_DataEntregaReal_To3, 8, 5, 0, 3, "/", ":", " "));
            }
            cmbavContagemresultado_statusdmn3.Name = cmbavContagemresultado_statusdmn3_Internalname;
            cmbavContagemresultado_statusdmn3.CurrentValue = cgiGet( cmbavContagemresultado_statusdmn3_Internalname);
            AV47ContagemResultado_StatusDmn3 = cgiGet( cmbavContagemresultado_statusdmn3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ContagemResultado_StatusDmn3", AV47ContagemResultado_StatusDmn3);
            cmbavContagemresultado_statuscnt3.Name = cmbavContagemresultado_statuscnt3_Internalname;
            cmbavContagemresultado_statuscnt3.CurrentValue = cgiGet( cmbavContagemresultado_statuscnt3_Internalname);
            AV48ContagemResultado_StatusCnt3 = (short)(NumberUtil.Val( cgiGet( cmbavContagemresultado_statuscnt3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ContagemResultado_StatusCnt3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48ContagemResultado_StatusCnt3), 2, 0)));
            AV49ContagemResultado_Descricao3 = cgiGet( edtavContagemresultado_descricao3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ContagemResultado_Descricao3", AV49ContagemResultado_Descricao3);
            dynavContagemresultado_sistemacod3.Name = dynavContagemresultado_sistemacod3_Internalname;
            dynavContagemresultado_sistemacod3.CurrentValue = cgiGet( dynavContagemresultado_sistemacod3_Internalname);
            AV50ContagemResultado_SistemaCod3 = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_sistemacod3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ContagemResultado_SistemaCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50ContagemResultado_SistemaCod3), 6, 0)));
            AV51ContagemResultado_Agrupador3 = StringUtil.Upper( cgiGet( edtavContagemresultado_agrupador3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ContagemResultado_Agrupador3", AV51ContagemResultado_Agrupador3);
            AV26DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled2", AV26DynamicFiltersEnabled2);
            AV39DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39DynamicFiltersEnabled3", AV39DynamicFiltersEnabled3);
            /* Read saved values. */
            nRC_GXsfl_206 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_206"), ",", "."));
            AV62GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV63GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_SERVICOGRUPO"), ",", ".") != Convert.ToDecimal( AV67ContagemResultado_ServicoGrupo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_SERVICO"), ",", ".") != Convert.ToDecimal( AV68ContagemResultado_Servico )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_CNTADAOSVINC"), ",", ".") != Convert.ToDecimal( AV69ContagemResultado_CntadaOsVinc )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_SERGRUPOVINC"), ",", ".") != Convert.ToDecimal( AV71ContagemResultado_SerGrupoVinc )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_CODSRVVNC"), ",", ".") != Convert.ToDecimal( AV70ContagemResultado_CodSrvVnc )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV14DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV15DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDAFM1"), AV16ContagemResultado_DemandaFM1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATADMN1"), 0) != AV17ContagemResultado_DataDmn1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATADMN_TO1"), 0) != AV18ContagemResultado_DataDmn_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATAENTREGAREAL1"), 0) != AV19ContagemResultado_DataEntregaReal1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1"), 0) != AV20ContagemResultado_DataEntregaReal_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_STATUSDMN1"), AV21ContagemResultado_StatusDmn1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_STATUSCNT1"), ",", ".") != Convert.ToDecimal( AV22ContagemResultado_StatusCnt1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DESCRICAO1"), AV23ContagemResultado_Descricao1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_SISTEMACOD1"), ",", ".") != Convert.ToDecimal( AV24ContagemResultado_SistemaCod1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_AGRUPADOR1"), AV25ContagemResultado_Agrupador1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV27DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV28DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDAFM2"), AV29ContagemResultado_DemandaFM2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATADMN2"), 0) != AV30ContagemResultado_DataDmn2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATADMN_TO2"), 0) != AV31ContagemResultado_DataDmn_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATAENTREGAREAL2"), 0) != AV32ContagemResultado_DataEntregaReal2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2"), 0) != AV33ContagemResultado_DataEntregaReal_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_STATUSDMN2"), AV34ContagemResultado_StatusDmn2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_STATUSCNT2"), ",", ".") != Convert.ToDecimal( AV35ContagemResultado_StatusCnt2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DESCRICAO2"), AV36ContagemResultado_Descricao2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_SISTEMACOD2"), ",", ".") != Convert.ToDecimal( AV37ContagemResultado_SistemaCod2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_AGRUPADOR2"), AV38ContagemResultado_Agrupador2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV40DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV41DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDAFM3"), AV42ContagemResultado_DemandaFM3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATADMN3"), 0) != AV43ContagemResultado_DataDmn3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATADMN_TO3"), 0) != AV44ContagemResultado_DataDmn_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATAENTREGAREAL3"), 0) != AV45ContagemResultado_DataEntregaReal3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3"), 0) != AV46ContagemResultado_DataEntregaReal_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_STATUSDMN3"), AV47ContagemResultado_StatusDmn3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_STATUSCNT3"), ",", ".") != Convert.ToDecimal( AV48ContagemResultado_StatusCnt3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_DESCRICAO3"), AV49ContagemResultado_Descricao3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_SISTEMACOD3"), ",", ".") != Convert.ToDecimal( AV50ContagemResultado_SistemaCod3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTAGEMRESULTADO_AGRUPADOR3"), AV51ContagemResultado_Agrupador3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV26DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV39DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E23TB2 */
         E23TB2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E23TB2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV21ContagemResultado_StatusDmn1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultado_StatusDmn1", AV21ContagemResultado_StatusDmn1);
         AV22ContagemResultado_StatusCnt1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemResultado_StatusCnt1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22ContagemResultado_StatusCnt1), 2, 0)));
         AV14DynamicFiltersSelector1 = "CONTAGEMRESULTADO_DEMANDAFM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV34ContagemResultado_StatusDmn2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultado_StatusDmn2", AV34ContagemResultado_StatusDmn2);
         AV35ContagemResultado_StatusCnt2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ContagemResultado_StatusCnt2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35ContagemResultado_StatusCnt2), 2, 0)));
         AV27DynamicFiltersSelector2 = "CONTAGEMRESULTADO_DEMANDAFM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector2", AV27DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV47ContagemResultado_StatusDmn3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ContagemResultado_StatusDmn3", AV47ContagemResultado_StatusDmn3);
         AV48ContagemResultado_StatusCnt3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ContagemResultado_StatusCnt3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48ContagemResultado_StatusCnt3), 2, 0)));
         AV40DynamicFiltersSelector3 = "CONTAGEMRESULTADO_DEMANDAFM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40DynamicFiltersSelector3", AV40DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         Form.Caption = "Relat�rio de Compara��o entre Demandas";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( ! (0==AV6WWPContext.gxTpr_Contratada_codigo) )
         {
            AV66ContagemResultado_ContratadaCod = AV6WWPContext.gxTpr_Contratada_codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66ContagemResultado_ContratadaCod), 6, 0)));
            dynavContagemresultado_contratadacod.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_contratadacod.Enabled), 5, 0)));
            AV69ContagemResultado_CntadaOsVinc = AV6WWPContext.gxTpr_Contratada_codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ContagemResultado_CntadaOsVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69ContagemResultado_CntadaOsVinc), 6, 0)));
            dynavContagemresultado_cntadaosvinc.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_cntadaosvinc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_cntadaosvinc.Enabled), 5, 0)));
         }
      }

      protected void E24TB2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Igual", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Per�odo", 0);
         }
         else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Passado", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Hoje", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "No futuro", 0);
            cmbavDynamicfiltersoperator1.addItem("3", "Per�odo", 0);
         }
         else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Igual", 0);
         }
         else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator1.addItem("2", ">", 0);
         }
         else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Igual", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("2", "Cont�m", 0);
         }
         if ( AV26DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Igual", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("2", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Per�odo", 0);
            }
            else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Passado", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Hoje", 0);
               cmbavDynamicfiltersoperator2.addItem("2", "No futuro", 0);
               cmbavDynamicfiltersoperator2.addItem("3", "Per�odo", 0);
            }
            else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
               cmbavDynamicfiltersoperator2.addItem("2", "Igual", 0);
            }
            else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "<", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "=", 0);
               cmbavDynamicfiltersoperator2.addItem("2", ">", 0);
            }
            else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Igual", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("2", "Cont�m", 0);
            }
            if ( AV39DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Igual", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Per�odo", 0);
               }
               else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Passado", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Hoje", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", "No futuro", 0);
                  cmbavDynamicfiltersoperator3.addItem("3", "Per�odo", 0);
               }
               else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", "Igual", 0);
               }
               else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "<", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "=", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", ">", 0);
               }
               else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Igual", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("2", "Cont�m", 0);
               }
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV62GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62GridCurrentPage), 10, 0)));
         AV63GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63GridPageCount), 10, 0)));
         AV74ExtraWWContagemResultadoRelComparacaoDemandasDS_1_Contratada_areatrabalhocod = AV13Contratada_AreaTrabalhoCod;
         AV75ExtraWWContagemResultadoRelComparacaoDemandasDS_2_Contagemresultado_osvinculada = AV65ContagemResultado_OSVinculada;
         AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1 = AV14DynamicFiltersSelector1;
         AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 = AV15DynamicFiltersOperator1;
         AV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1 = AV16ContagemResultado_DemandaFM1;
         AV79ExtraWWContagemResultadoRelComparacaoDemandasDS_6_Contagemresultado_datadmn1 = AV17ContagemResultado_DataDmn1;
         AV80ExtraWWContagemResultadoRelComparacaoDemandasDS_7_Contagemresultado_datadmn_to1 = AV18ContagemResultado_DataDmn_To1;
         AV81ExtraWWContagemResultadoRelComparacaoDemandasDS_8_Contagemresultado_dataentregareal1 = AV19ContagemResultado_DataEntregaReal1;
         AV82ExtraWWContagemResultadoRelComparacaoDemandasDS_9_Contagemresultado_dataentregareal_to1 = AV20ContagemResultado_DataEntregaReal_To1;
         AV83ExtraWWContagemResultadoRelComparacaoDemandasDS_10_Contagemresultado_statusdmn1 = AV21ContagemResultado_StatusDmn1;
         AV84ExtraWWContagemResultadoRelComparacaoDemandasDS_11_Contagemresultado_statuscnt1 = AV22ContagemResultado_StatusCnt1;
         AV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1 = AV23ContagemResultado_Descricao1;
         AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1 = AV24ContagemResultado_SistemaCod1;
         AV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1 = AV25ContagemResultado_Agrupador1;
         AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 = AV26DynamicFiltersEnabled2;
         AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2 = AV27DynamicFiltersSelector2;
         AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 = AV28DynamicFiltersOperator2;
         AV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2 = AV29ContagemResultado_DemandaFM2;
         AV92ExtraWWContagemResultadoRelComparacaoDemandasDS_19_Contagemresultado_datadmn2 = AV30ContagemResultado_DataDmn2;
         AV93ExtraWWContagemResultadoRelComparacaoDemandasDS_20_Contagemresultado_datadmn_to2 = AV31ContagemResultado_DataDmn_To2;
         AV94ExtraWWContagemResultadoRelComparacaoDemandasDS_21_Contagemresultado_dataentregareal2 = AV32ContagemResultado_DataEntregaReal2;
         AV95ExtraWWContagemResultadoRelComparacaoDemandasDS_22_Contagemresultado_dataentregareal_to2 = AV33ContagemResultado_DataEntregaReal_To2;
         AV96ExtraWWContagemResultadoRelComparacaoDemandasDS_23_Contagemresultado_statusdmn2 = AV34ContagemResultado_StatusDmn2;
         AV97ExtraWWContagemResultadoRelComparacaoDemandasDS_24_Contagemresultado_statuscnt2 = AV35ContagemResultado_StatusCnt2;
         AV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2 = AV36ContagemResultado_Descricao2;
         AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2 = AV37ContagemResultado_SistemaCod2;
         AV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2 = AV38ContagemResultado_Agrupador2;
         AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 = AV39DynamicFiltersEnabled3;
         AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3 = AV40DynamicFiltersSelector3;
         AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 = AV41DynamicFiltersOperator3;
         AV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3 = AV42ContagemResultado_DemandaFM3;
         AV105ExtraWWContagemResultadoRelComparacaoDemandasDS_32_Contagemresultado_datadmn3 = AV43ContagemResultado_DataDmn3;
         AV106ExtraWWContagemResultadoRelComparacaoDemandasDS_33_Contagemresultado_datadmn_to3 = AV44ContagemResultado_DataDmn_To3;
         AV107ExtraWWContagemResultadoRelComparacaoDemandasDS_34_Contagemresultado_dataentregareal3 = AV45ContagemResultado_DataEntregaReal3;
         AV108ExtraWWContagemResultadoRelComparacaoDemandasDS_35_Contagemresultado_dataentregareal_to3 = AV46ContagemResultado_DataEntregaReal_To3;
         AV109ExtraWWContagemResultadoRelComparacaoDemandasDS_36_Contagemresultado_statusdmn3 = AV47ContagemResultado_StatusDmn3;
         AV110ExtraWWContagemResultadoRelComparacaoDemandasDS_37_Contagemresultado_statuscnt3 = AV48ContagemResultado_StatusCnt3;
         AV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3 = AV49ContagemResultado_Descricao3;
         AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3 = AV50ContagemResultado_SistemaCod3;
         AV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3 = AV51ContagemResultado_Agrupador3;
         AV114ExtraWWContagemResultadoRelComparacaoDemandasDS_41_Contagemresultado_contratadacod = AV66ContagemResultado_ContratadaCod;
         AV115ExtraWWContagemResultadoRelComparacaoDemandasDS_42_Contagemresultado_servicogrupo = AV67ContagemResultado_ServicoGrupo;
         AV116ExtraWWContagemResultadoRelComparacaoDemandasDS_43_Contagemresultado_servico = AV68ContagemResultado_Servico;
         AV117ExtraWWContagemResultadoRelComparacaoDemandasDS_44_Contagemresultado_cntadaosvinc = AV69ContagemResultado_CntadaOsVinc;
         AV118ExtraWWContagemResultadoRelComparacaoDemandasDS_45_Contagemresultado_sergrupovinc = AV71ContagemResultado_SerGrupoVinc;
         AV119ExtraWWContagemResultadoRelComparacaoDemandasDS_46_Contagemresultado_codsrvvnc = AV70ContagemResultado_CodSrvVnc;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV41DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11TB2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV61PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV61PageToGo) ;
         }
      }

      private void E25TB2( )
      {
         /* Grid_Load Routine */
         edtavDisplay_Tooltiptext = "Mostrar";
         if ( AV6WWPContext.gxTpr_Display )
         {
            edtavDisplay_Link = formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +A456ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
            AV64Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV64Display);
            AV120Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
            edtavDisplay_Enabled = 1;
         }
         else
         {
            edtavDisplay_Link = "";
            AV64Display = context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV64Display);
            AV120Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( )));
            edtavDisplay_Enabled = 0;
         }
         edtContagemResultado_DataDmn_Link = formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +A456ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 206;
         }
         sendrow_2062( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_206_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(206, GridRow);
         }
      }

      protected void E15TB2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV26DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled2", AV26DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV67ContagemResultado_ServicoGrupo, AV68ContagemResultado_Servico, AV69ContagemResultado_CntadaOsVinc, AV71ContagemResultado_SerGrupoVinc, AV70ContagemResultado_CodSrvVnc, AV14DynamicFiltersSelector1, AV15DynamicFiltersOperator1, AV16ContagemResultado_DemandaFM1, AV17ContagemResultado_DataDmn1, AV18ContagemResultado_DataDmn_To1, AV19ContagemResultado_DataEntregaReal1, AV20ContagemResultado_DataEntregaReal_To1, AV21ContagemResultado_StatusDmn1, AV22ContagemResultado_StatusCnt1, AV23ContagemResultado_Descricao1, AV24ContagemResultado_SistemaCod1, AV25ContagemResultado_Agrupador1, AV27DynamicFiltersSelector2, AV28DynamicFiltersOperator2, AV29ContagemResultado_DemandaFM2, AV30ContagemResultado_DataDmn2, AV31ContagemResultado_DataDmn_To2, AV32ContagemResultado_DataEntregaReal2, AV33ContagemResultado_DataEntregaReal_To2, AV34ContagemResultado_StatusDmn2, AV35ContagemResultado_StatusCnt2, AV36ContagemResultado_Descricao2, AV37ContagemResultado_SistemaCod2, AV38ContagemResultado_Agrupador2, AV40DynamicFiltersSelector3, AV41DynamicFiltersOperator3, AV42ContagemResultado_DemandaFM3, AV43ContagemResultado_DataDmn3, AV44ContagemResultado_DataDmn_To3, AV45ContagemResultado_DataEntregaReal3, AV46ContagemResultado_DataEntregaReal_To3, AV47ContagemResultado_StatusDmn3, AV48ContagemResultado_StatusCnt3, AV49ContagemResultado_Descricao3, AV50ContagemResultado_SistemaCod3, AV51ContagemResultado_Agrupador3, AV26DynamicFiltersEnabled2, AV39DynamicFiltersEnabled3, AV6WWPContext, AV13Contratada_AreaTrabalhoCod, AV65ContagemResultado_OSVinculada, AV66ContagemResultado_ContratadaCod, AV122Pgmname, AV10GridState, AV53DynamicFiltersIgnoreFirst, AV52DynamicFiltersRemoving, A456ContagemResultado_Codigo) ;
      }

      protected void E12TB2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV52DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52DynamicFiltersRemoving", AV52DynamicFiltersRemoving);
         AV53DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53DynamicFiltersIgnoreFirst", AV53DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV52DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52DynamicFiltersRemoving", AV52DynamicFiltersRemoving);
         AV53DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53DynamicFiltersIgnoreFirst", AV53DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV67ContagemResultado_ServicoGrupo, AV68ContagemResultado_Servico, AV69ContagemResultado_CntadaOsVinc, AV71ContagemResultado_SerGrupoVinc, AV70ContagemResultado_CodSrvVnc, AV14DynamicFiltersSelector1, AV15DynamicFiltersOperator1, AV16ContagemResultado_DemandaFM1, AV17ContagemResultado_DataDmn1, AV18ContagemResultado_DataDmn_To1, AV19ContagemResultado_DataEntregaReal1, AV20ContagemResultado_DataEntregaReal_To1, AV21ContagemResultado_StatusDmn1, AV22ContagemResultado_StatusCnt1, AV23ContagemResultado_Descricao1, AV24ContagemResultado_SistemaCod1, AV25ContagemResultado_Agrupador1, AV27DynamicFiltersSelector2, AV28DynamicFiltersOperator2, AV29ContagemResultado_DemandaFM2, AV30ContagemResultado_DataDmn2, AV31ContagemResultado_DataDmn_To2, AV32ContagemResultado_DataEntregaReal2, AV33ContagemResultado_DataEntregaReal_To2, AV34ContagemResultado_StatusDmn2, AV35ContagemResultado_StatusCnt2, AV36ContagemResultado_Descricao2, AV37ContagemResultado_SistemaCod2, AV38ContagemResultado_Agrupador2, AV40DynamicFiltersSelector3, AV41DynamicFiltersOperator3, AV42ContagemResultado_DemandaFM3, AV43ContagemResultado_DataDmn3, AV44ContagemResultado_DataDmn_To3, AV45ContagemResultado_DataEntregaReal3, AV46ContagemResultado_DataEntregaReal_To3, AV47ContagemResultado_StatusDmn3, AV48ContagemResultado_StatusCnt3, AV49ContagemResultado_Descricao3, AV50ContagemResultado_SistemaCod3, AV51ContagemResultado_Agrupador3, AV26DynamicFiltersEnabled2, AV39DynamicFiltersEnabled3, AV6WWPContext, AV13Contratada_AreaTrabalhoCod, AV65ContagemResultado_OSVinculada, AV66ContagemResultado_ContratadaCod, AV122Pgmname, AV10GridState, AV53DynamicFiltersIgnoreFirst, AV52DynamicFiltersRemoving, A456ContagemResultado_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV40DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV41DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV14DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         dynavContagemresultado_sistemacod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24ContagemResultado_SistemaCod1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod1_Internalname, "Values", dynavContagemresultado_sistemacod1.ToJavascriptSource());
         cmbavContagemresultado_statuscnt1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22ContagemResultado_StatusCnt1), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt1_Internalname, "Values", cmbavContagemresultado_statuscnt1.ToJavascriptSource());
         cmbavContagemresultado_statusdmn1.CurrentValue = StringUtil.RTrim( AV21ContagemResultado_StatusDmn1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn1_Internalname, "Values", cmbavContagemresultado_statusdmn1.ToJavascriptSource());
         dynavContagemresultado_sistemacod2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV37ContagemResultado_SistemaCod2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod2_Internalname, "Values", dynavContagemresultado_sistemacod2.ToJavascriptSource());
         cmbavContagemresultado_statuscnt2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV35ContagemResultado_StatusCnt2), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt2_Internalname, "Values", cmbavContagemresultado_statuscnt2.ToJavascriptSource());
         cmbavContagemresultado_statusdmn2.CurrentValue = StringUtil.RTrim( AV34ContagemResultado_StatusDmn2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn2_Internalname, "Values", cmbavContagemresultado_statusdmn2.ToJavascriptSource());
         dynavContagemresultado_sistemacod3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV50ContagemResultado_SistemaCod3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod3_Internalname, "Values", dynavContagemresultado_sistemacod3.ToJavascriptSource());
         cmbavContagemresultado_statuscnt3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV48ContagemResultado_StatusCnt3), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt3_Internalname, "Values", cmbavContagemresultado_statuscnt3.ToJavascriptSource());
         cmbavContagemresultado_statusdmn3.CurrentValue = StringUtil.RTrim( AV47ContagemResultado_StatusDmn3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn3_Internalname, "Values", cmbavContagemresultado_statusdmn3.ToJavascriptSource());
      }

      protected void E16TB2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV15DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E17TB2( )
      {
         /* Dynamicfiltersoperator1_Click Routine */
         if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
         {
            AV19ContagemResultado_DataEntregaReal1 = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultado_DataEntregaReal1", context.localUtil.TToC( AV19ContagemResultado_DataEntregaReal1, 8, 5, 0, 3, "/", ":", " "));
            AV20ContagemResultado_DataEntregaReal_To1 = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultado_DataEntregaReal_To1", context.localUtil.TToC( AV20ContagemResultado_DataEntregaReal_To1, 8, 5, 0, 3, "/", ":", " "));
            /* Execute user subroutine: 'UPDATECONTAGEMRESULTADO_DATAENTREGAREAL1OPERATORVALUES' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            gxgrGrid_refresh( subGrid_Rows, AV67ContagemResultado_ServicoGrupo, AV68ContagemResultado_Servico, AV69ContagemResultado_CntadaOsVinc, AV71ContagemResultado_SerGrupoVinc, AV70ContagemResultado_CodSrvVnc, AV14DynamicFiltersSelector1, AV15DynamicFiltersOperator1, AV16ContagemResultado_DemandaFM1, AV17ContagemResultado_DataDmn1, AV18ContagemResultado_DataDmn_To1, AV19ContagemResultado_DataEntregaReal1, AV20ContagemResultado_DataEntregaReal_To1, AV21ContagemResultado_StatusDmn1, AV22ContagemResultado_StatusCnt1, AV23ContagemResultado_Descricao1, AV24ContagemResultado_SistemaCod1, AV25ContagemResultado_Agrupador1, AV27DynamicFiltersSelector2, AV28DynamicFiltersOperator2, AV29ContagemResultado_DemandaFM2, AV30ContagemResultado_DataDmn2, AV31ContagemResultado_DataDmn_To2, AV32ContagemResultado_DataEntregaReal2, AV33ContagemResultado_DataEntregaReal_To2, AV34ContagemResultado_StatusDmn2, AV35ContagemResultado_StatusCnt2, AV36ContagemResultado_Descricao2, AV37ContagemResultado_SistemaCod2, AV38ContagemResultado_Agrupador2, AV40DynamicFiltersSelector3, AV41DynamicFiltersOperator3, AV42ContagemResultado_DemandaFM3, AV43ContagemResultado_DataDmn3, AV44ContagemResultado_DataDmn_To3, AV45ContagemResultado_DataEntregaReal3, AV46ContagemResultado_DataEntregaReal_To3, AV47ContagemResultado_StatusDmn3, AV48ContagemResultado_StatusCnt3, AV49ContagemResultado_Descricao3, AV50ContagemResultado_SistemaCod3, AV51ContagemResultado_Agrupador3, AV26DynamicFiltersEnabled2, AV39DynamicFiltersEnabled3, AV6WWPContext, AV13Contratada_AreaTrabalhoCod, AV65ContagemResultado_OSVinculada, AV66ContagemResultado_ContratadaCod, AV122Pgmname, AV10GridState, AV53DynamicFiltersIgnoreFirst, AV52DynamicFiltersRemoving, A456ContagemResultado_Codigo) ;
         }
      }

      protected void E18TB2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV39DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39DynamicFiltersEnabled3", AV39DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV67ContagemResultado_ServicoGrupo, AV68ContagemResultado_Servico, AV69ContagemResultado_CntadaOsVinc, AV71ContagemResultado_SerGrupoVinc, AV70ContagemResultado_CodSrvVnc, AV14DynamicFiltersSelector1, AV15DynamicFiltersOperator1, AV16ContagemResultado_DemandaFM1, AV17ContagemResultado_DataDmn1, AV18ContagemResultado_DataDmn_To1, AV19ContagemResultado_DataEntregaReal1, AV20ContagemResultado_DataEntregaReal_To1, AV21ContagemResultado_StatusDmn1, AV22ContagemResultado_StatusCnt1, AV23ContagemResultado_Descricao1, AV24ContagemResultado_SistemaCod1, AV25ContagemResultado_Agrupador1, AV27DynamicFiltersSelector2, AV28DynamicFiltersOperator2, AV29ContagemResultado_DemandaFM2, AV30ContagemResultado_DataDmn2, AV31ContagemResultado_DataDmn_To2, AV32ContagemResultado_DataEntregaReal2, AV33ContagemResultado_DataEntregaReal_To2, AV34ContagemResultado_StatusDmn2, AV35ContagemResultado_StatusCnt2, AV36ContagemResultado_Descricao2, AV37ContagemResultado_SistemaCod2, AV38ContagemResultado_Agrupador2, AV40DynamicFiltersSelector3, AV41DynamicFiltersOperator3, AV42ContagemResultado_DemandaFM3, AV43ContagemResultado_DataDmn3, AV44ContagemResultado_DataDmn_To3, AV45ContagemResultado_DataEntregaReal3, AV46ContagemResultado_DataEntregaReal_To3, AV47ContagemResultado_StatusDmn3, AV48ContagemResultado_StatusCnt3, AV49ContagemResultado_Descricao3, AV50ContagemResultado_SistemaCod3, AV51ContagemResultado_Agrupador3, AV26DynamicFiltersEnabled2, AV39DynamicFiltersEnabled3, AV6WWPContext, AV13Contratada_AreaTrabalhoCod, AV65ContagemResultado_OSVinculada, AV66ContagemResultado_ContratadaCod, AV122Pgmname, AV10GridState, AV53DynamicFiltersIgnoreFirst, AV52DynamicFiltersRemoving, A456ContagemResultado_Codigo) ;
      }

      protected void E13TB2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV52DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52DynamicFiltersRemoving", AV52DynamicFiltersRemoving);
         AV26DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled2", AV26DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV52DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52DynamicFiltersRemoving", AV52DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV67ContagemResultado_ServicoGrupo, AV68ContagemResultado_Servico, AV69ContagemResultado_CntadaOsVinc, AV71ContagemResultado_SerGrupoVinc, AV70ContagemResultado_CodSrvVnc, AV14DynamicFiltersSelector1, AV15DynamicFiltersOperator1, AV16ContagemResultado_DemandaFM1, AV17ContagemResultado_DataDmn1, AV18ContagemResultado_DataDmn_To1, AV19ContagemResultado_DataEntregaReal1, AV20ContagemResultado_DataEntregaReal_To1, AV21ContagemResultado_StatusDmn1, AV22ContagemResultado_StatusCnt1, AV23ContagemResultado_Descricao1, AV24ContagemResultado_SistemaCod1, AV25ContagemResultado_Agrupador1, AV27DynamicFiltersSelector2, AV28DynamicFiltersOperator2, AV29ContagemResultado_DemandaFM2, AV30ContagemResultado_DataDmn2, AV31ContagemResultado_DataDmn_To2, AV32ContagemResultado_DataEntregaReal2, AV33ContagemResultado_DataEntregaReal_To2, AV34ContagemResultado_StatusDmn2, AV35ContagemResultado_StatusCnt2, AV36ContagemResultado_Descricao2, AV37ContagemResultado_SistemaCod2, AV38ContagemResultado_Agrupador2, AV40DynamicFiltersSelector3, AV41DynamicFiltersOperator3, AV42ContagemResultado_DemandaFM3, AV43ContagemResultado_DataDmn3, AV44ContagemResultado_DataDmn_To3, AV45ContagemResultado_DataEntregaReal3, AV46ContagemResultado_DataEntregaReal_To3, AV47ContagemResultado_StatusDmn3, AV48ContagemResultado_StatusCnt3, AV49ContagemResultado_Descricao3, AV50ContagemResultado_SistemaCod3, AV51ContagemResultado_Agrupador3, AV26DynamicFiltersEnabled2, AV39DynamicFiltersEnabled3, AV6WWPContext, AV13Contratada_AreaTrabalhoCod, AV65ContagemResultado_OSVinculada, AV66ContagemResultado_ContratadaCod, AV122Pgmname, AV10GridState, AV53DynamicFiltersIgnoreFirst, AV52DynamicFiltersRemoving, A456ContagemResultado_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV40DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV41DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV14DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         dynavContagemresultado_sistemacod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24ContagemResultado_SistemaCod1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod1_Internalname, "Values", dynavContagemresultado_sistemacod1.ToJavascriptSource());
         cmbavContagemresultado_statuscnt1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22ContagemResultado_StatusCnt1), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt1_Internalname, "Values", cmbavContagemresultado_statuscnt1.ToJavascriptSource());
         cmbavContagemresultado_statusdmn1.CurrentValue = StringUtil.RTrim( AV21ContagemResultado_StatusDmn1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn1_Internalname, "Values", cmbavContagemresultado_statusdmn1.ToJavascriptSource());
         dynavContagemresultado_sistemacod2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV37ContagemResultado_SistemaCod2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod2_Internalname, "Values", dynavContagemresultado_sistemacod2.ToJavascriptSource());
         cmbavContagemresultado_statuscnt2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV35ContagemResultado_StatusCnt2), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt2_Internalname, "Values", cmbavContagemresultado_statuscnt2.ToJavascriptSource());
         cmbavContagemresultado_statusdmn2.CurrentValue = StringUtil.RTrim( AV34ContagemResultado_StatusDmn2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn2_Internalname, "Values", cmbavContagemresultado_statusdmn2.ToJavascriptSource());
         dynavContagemresultado_sistemacod3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV50ContagemResultado_SistemaCod3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod3_Internalname, "Values", dynavContagemresultado_sistemacod3.ToJavascriptSource());
         cmbavContagemresultado_statuscnt3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV48ContagemResultado_StatusCnt3), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt3_Internalname, "Values", cmbavContagemresultado_statuscnt3.ToJavascriptSource());
         cmbavContagemresultado_statusdmn3.CurrentValue = StringUtil.RTrim( AV47ContagemResultado_StatusDmn3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn3_Internalname, "Values", cmbavContagemresultado_statusdmn3.ToJavascriptSource());
      }

      protected void E19TB2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV28DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E20TB2( )
      {
         /* Dynamicfiltersoperator2_Click Routine */
         if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
         {
            AV32ContagemResultado_DataEntregaReal2 = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultado_DataEntregaReal2", context.localUtil.TToC( AV32ContagemResultado_DataEntregaReal2, 8, 5, 0, 3, "/", ":", " "));
            AV33ContagemResultado_DataEntregaReal_To2 = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultado_DataEntregaReal_To2", context.localUtil.TToC( AV33ContagemResultado_DataEntregaReal_To2, 8, 5, 0, 3, "/", ":", " "));
            /* Execute user subroutine: 'UPDATECONTAGEMRESULTADO_DATAENTREGAREAL2OPERATORVALUES' */
            S212 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            gxgrGrid_refresh( subGrid_Rows, AV67ContagemResultado_ServicoGrupo, AV68ContagemResultado_Servico, AV69ContagemResultado_CntadaOsVinc, AV71ContagemResultado_SerGrupoVinc, AV70ContagemResultado_CodSrvVnc, AV14DynamicFiltersSelector1, AV15DynamicFiltersOperator1, AV16ContagemResultado_DemandaFM1, AV17ContagemResultado_DataDmn1, AV18ContagemResultado_DataDmn_To1, AV19ContagemResultado_DataEntregaReal1, AV20ContagemResultado_DataEntregaReal_To1, AV21ContagemResultado_StatusDmn1, AV22ContagemResultado_StatusCnt1, AV23ContagemResultado_Descricao1, AV24ContagemResultado_SistemaCod1, AV25ContagemResultado_Agrupador1, AV27DynamicFiltersSelector2, AV28DynamicFiltersOperator2, AV29ContagemResultado_DemandaFM2, AV30ContagemResultado_DataDmn2, AV31ContagemResultado_DataDmn_To2, AV32ContagemResultado_DataEntregaReal2, AV33ContagemResultado_DataEntregaReal_To2, AV34ContagemResultado_StatusDmn2, AV35ContagemResultado_StatusCnt2, AV36ContagemResultado_Descricao2, AV37ContagemResultado_SistemaCod2, AV38ContagemResultado_Agrupador2, AV40DynamicFiltersSelector3, AV41DynamicFiltersOperator3, AV42ContagemResultado_DemandaFM3, AV43ContagemResultado_DataDmn3, AV44ContagemResultado_DataDmn_To3, AV45ContagemResultado_DataEntregaReal3, AV46ContagemResultado_DataEntregaReal_To3, AV47ContagemResultado_StatusDmn3, AV48ContagemResultado_StatusCnt3, AV49ContagemResultado_Descricao3, AV50ContagemResultado_SistemaCod3, AV51ContagemResultado_Agrupador3, AV26DynamicFiltersEnabled2, AV39DynamicFiltersEnabled3, AV6WWPContext, AV13Contratada_AreaTrabalhoCod, AV65ContagemResultado_OSVinculada, AV66ContagemResultado_ContratadaCod, AV122Pgmname, AV10GridState, AV53DynamicFiltersIgnoreFirst, AV52DynamicFiltersRemoving, A456ContagemResultado_Codigo) ;
         }
      }

      protected void E14TB2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV52DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52DynamicFiltersRemoving", AV52DynamicFiltersRemoving);
         AV39DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39DynamicFiltersEnabled3", AV39DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV52DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52DynamicFiltersRemoving", AV52DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV67ContagemResultado_ServicoGrupo, AV68ContagemResultado_Servico, AV69ContagemResultado_CntadaOsVinc, AV71ContagemResultado_SerGrupoVinc, AV70ContagemResultado_CodSrvVnc, AV14DynamicFiltersSelector1, AV15DynamicFiltersOperator1, AV16ContagemResultado_DemandaFM1, AV17ContagemResultado_DataDmn1, AV18ContagemResultado_DataDmn_To1, AV19ContagemResultado_DataEntregaReal1, AV20ContagemResultado_DataEntregaReal_To1, AV21ContagemResultado_StatusDmn1, AV22ContagemResultado_StatusCnt1, AV23ContagemResultado_Descricao1, AV24ContagemResultado_SistemaCod1, AV25ContagemResultado_Agrupador1, AV27DynamicFiltersSelector2, AV28DynamicFiltersOperator2, AV29ContagemResultado_DemandaFM2, AV30ContagemResultado_DataDmn2, AV31ContagemResultado_DataDmn_To2, AV32ContagemResultado_DataEntregaReal2, AV33ContagemResultado_DataEntregaReal_To2, AV34ContagemResultado_StatusDmn2, AV35ContagemResultado_StatusCnt2, AV36ContagemResultado_Descricao2, AV37ContagemResultado_SistemaCod2, AV38ContagemResultado_Agrupador2, AV40DynamicFiltersSelector3, AV41DynamicFiltersOperator3, AV42ContagemResultado_DemandaFM3, AV43ContagemResultado_DataDmn3, AV44ContagemResultado_DataDmn_To3, AV45ContagemResultado_DataEntregaReal3, AV46ContagemResultado_DataEntregaReal_To3, AV47ContagemResultado_StatusDmn3, AV48ContagemResultado_StatusCnt3, AV49ContagemResultado_Descricao3, AV50ContagemResultado_SistemaCod3, AV51ContagemResultado_Agrupador3, AV26DynamicFiltersEnabled2, AV39DynamicFiltersEnabled3, AV6WWPContext, AV13Contratada_AreaTrabalhoCod, AV65ContagemResultado_OSVinculada, AV66ContagemResultado_ContratadaCod, AV122Pgmname, AV10GridState, AV53DynamicFiltersIgnoreFirst, AV52DynamicFiltersRemoving, A456ContagemResultado_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV40DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV41DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV14DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         dynavContagemresultado_sistemacod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24ContagemResultado_SistemaCod1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod1_Internalname, "Values", dynavContagemresultado_sistemacod1.ToJavascriptSource());
         cmbavContagemresultado_statuscnt1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22ContagemResultado_StatusCnt1), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt1_Internalname, "Values", cmbavContagemresultado_statuscnt1.ToJavascriptSource());
         cmbavContagemresultado_statusdmn1.CurrentValue = StringUtil.RTrim( AV21ContagemResultado_StatusDmn1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn1_Internalname, "Values", cmbavContagemresultado_statusdmn1.ToJavascriptSource());
         dynavContagemresultado_sistemacod2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV37ContagemResultado_SistemaCod2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod2_Internalname, "Values", dynavContagemresultado_sistemacod2.ToJavascriptSource());
         cmbavContagemresultado_statuscnt2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV35ContagemResultado_StatusCnt2), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt2_Internalname, "Values", cmbavContagemresultado_statuscnt2.ToJavascriptSource());
         cmbavContagemresultado_statusdmn2.CurrentValue = StringUtil.RTrim( AV34ContagemResultado_StatusDmn2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn2_Internalname, "Values", cmbavContagemresultado_statusdmn2.ToJavascriptSource());
         dynavContagemresultado_sistemacod3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV50ContagemResultado_SistemaCod3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod3_Internalname, "Values", dynavContagemresultado_sistemacod3.ToJavascriptSource());
         cmbavContagemresultado_statuscnt3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV48ContagemResultado_StatusCnt3), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt3_Internalname, "Values", cmbavContagemresultado_statuscnt3.ToJavascriptSource());
         cmbavContagemresultado_statusdmn3.CurrentValue = StringUtil.RTrim( AV47ContagemResultado_StatusDmn3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn3_Internalname, "Values", cmbavContagemresultado_statusdmn3.ToJavascriptSource());
      }

      protected void E21TB2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV41DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV41DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E22TB2( )
      {
         /* Dynamicfiltersoperator3_Click Routine */
         if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
         {
            AV45ContagemResultado_DataEntregaReal3 = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ContagemResultado_DataEntregaReal3", context.localUtil.TToC( AV45ContagemResultado_DataEntregaReal3, 8, 5, 0, 3, "/", ":", " "));
            AV46ContagemResultado_DataEntregaReal_To3 = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ContagemResultado_DataEntregaReal_To3", context.localUtil.TToC( AV46ContagemResultado_DataEntregaReal_To3, 8, 5, 0, 3, "/", ":", " "));
            /* Execute user subroutine: 'UPDATECONTAGEMRESULTADO_DATAENTREGAREAL3OPERATORVALUES' */
            S222 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            gxgrGrid_refresh( subGrid_Rows, AV67ContagemResultado_ServicoGrupo, AV68ContagemResultado_Servico, AV69ContagemResultado_CntadaOsVinc, AV71ContagemResultado_SerGrupoVinc, AV70ContagemResultado_CodSrvVnc, AV14DynamicFiltersSelector1, AV15DynamicFiltersOperator1, AV16ContagemResultado_DemandaFM1, AV17ContagemResultado_DataDmn1, AV18ContagemResultado_DataDmn_To1, AV19ContagemResultado_DataEntregaReal1, AV20ContagemResultado_DataEntregaReal_To1, AV21ContagemResultado_StatusDmn1, AV22ContagemResultado_StatusCnt1, AV23ContagemResultado_Descricao1, AV24ContagemResultado_SistemaCod1, AV25ContagemResultado_Agrupador1, AV27DynamicFiltersSelector2, AV28DynamicFiltersOperator2, AV29ContagemResultado_DemandaFM2, AV30ContagemResultado_DataDmn2, AV31ContagemResultado_DataDmn_To2, AV32ContagemResultado_DataEntregaReal2, AV33ContagemResultado_DataEntregaReal_To2, AV34ContagemResultado_StatusDmn2, AV35ContagemResultado_StatusCnt2, AV36ContagemResultado_Descricao2, AV37ContagemResultado_SistemaCod2, AV38ContagemResultado_Agrupador2, AV40DynamicFiltersSelector3, AV41DynamicFiltersOperator3, AV42ContagemResultado_DemandaFM3, AV43ContagemResultado_DataDmn3, AV44ContagemResultado_DataDmn_To3, AV45ContagemResultado_DataEntregaReal3, AV46ContagemResultado_DataEntregaReal_To3, AV47ContagemResultado_StatusDmn3, AV48ContagemResultado_StatusCnt3, AV49ContagemResultado_Descricao3, AV50ContagemResultado_SistemaCod3, AV51ContagemResultado_Agrupador3, AV26DynamicFiltersEnabled2, AV39DynamicFiltersEnabled3, AV6WWPContext, AV13Contratada_AreaTrabalhoCod, AV65ContagemResultado_OSVinculada, AV66ContagemResultado_ContratadaCod, AV122Pgmname, AV10GridState, AV53DynamicFiltersIgnoreFirst, AV52DynamicFiltersRemoving, A456ContagemResultado_Codigo) ;
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavContagemresultado_demandafm1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demandafm1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demandafm1_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_datadmn1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Visible), 5, 0)));
         cmbavContagemresultado_statusdmn1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statusdmn1.Visible), 5, 0)));
         cmbavContagemresultado_statuscnt1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statuscnt1.Visible), 5, 0)));
         edtavContagemresultado_descricao1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_descricao1_Visible), 5, 0)));
         dynavContagemresultado_sistemacod1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_sistemacod1.Visible), 5, 0)));
         edtavContagemresultado_agrupador1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_agrupador1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_agrupador1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
         {
            edtavContagemresultado_demandafm1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demandafm1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demandafm1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_datadmn1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
            /* Execute user subroutine: 'UPDATECONTAGEMRESULTADO_DATAENTREGAREAL1OPERATORVALUES' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
         {
            cmbavContagemresultado_statusdmn1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statusdmn1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSCNT") == 0 )
         {
            cmbavContagemresultado_statuscnt1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statuscnt1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
         {
            edtavContagemresultado_descricao1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_descricao1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
         {
            dynavContagemresultado_sistemacod1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_sistemacod1.Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
         {
            edtavContagemresultado_agrupador1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_agrupador1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_agrupador1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'UPDATECONTAGEMRESULTADO_DATAENTREGAREAL1OPERATORVALUES' Routine */
         cellContagemresultado_dataentregareal_cell1_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_dataentregareal_cell1_Internalname, "Class", cellContagemresultado_dataentregareal_cell1_Class);
         cellContagemresultado_dataentregareal_to_cell1_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_dataentregareal_to_cell1_Internalname, "Class", cellContagemresultado_dataentregareal_to_cell1_Class);
         lblContagemresultado_dataentregareal_rangemiddletext_11_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContagemresultado_dataentregareal_rangemiddletext_11_Internalname, "Class", lblContagemresultado_dataentregareal_rangemiddletext_11_Class);
         if ( AV15DynamicFiltersOperator1 == 0 )
         {
            AV19ContagemResultado_DataEntregaReal1 = DateTimeUtil.ResetTime( Gx_date ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultado_DataEntregaReal1", context.localUtil.TToC( AV19ContagemResultado_DataEntregaReal1, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV15DynamicFiltersOperator1 == 1 )
         {
            AV19ContagemResultado_DataEntregaReal1 = DateTimeUtil.ResetTime( Gx_date ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultado_DataEntregaReal1", context.localUtil.TToC( AV19ContagemResultado_DataEntregaReal1, 8, 5, 0, 3, "/", ":", " "));
            AV20ContagemResultado_DataEntregaReal_To1 = DateTimeUtil.ResetTime( DateTimeUtil.DAdd( Gx_date , + ( (int)(1) )) ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultado_DataEntregaReal_To1", context.localUtil.TToC( AV20ContagemResultado_DataEntregaReal_To1, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV15DynamicFiltersOperator1 == 2 )
         {
            AV19ContagemResultado_DataEntregaReal1 = DateTimeUtil.ResetTime( DateTimeUtil.DAdd( Gx_date , + ( (int)(1) )) ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultado_DataEntregaReal1", context.localUtil.TToC( AV19ContagemResultado_DataEntregaReal1, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV15DynamicFiltersOperator1 == 3 )
         {
            cellContagemresultado_dataentregareal_cell1_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_dataentregareal_cell1_Internalname, "Class", cellContagemresultado_dataentregareal_cell1_Class);
            cellContagemresultado_dataentregareal_to_cell1_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_dataentregareal_to_cell1_Internalname, "Class", cellContagemresultado_dataentregareal_to_cell1_Class);
            lblContagemresultado_dataentregareal_rangemiddletext_11_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContagemresultado_dataentregareal_rangemiddletext_11_Internalname, "Class", lblContagemresultado_dataentregareal_rangemiddletext_11_Class);
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavContagemresultado_demandafm2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demandafm2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demandafm2_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_datadmn2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Visible), 5, 0)));
         cmbavContagemresultado_statusdmn2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statusdmn2.Visible), 5, 0)));
         cmbavContagemresultado_statuscnt2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statuscnt2.Visible), 5, 0)));
         edtavContagemresultado_descricao2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_descricao2_Visible), 5, 0)));
         dynavContagemresultado_sistemacod2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_sistemacod2.Visible), 5, 0)));
         edtavContagemresultado_agrupador2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_agrupador2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_agrupador2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
         {
            edtavContagemresultado_demandafm2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demandafm2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demandafm2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_datadmn2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
            /* Execute user subroutine: 'UPDATECONTAGEMRESULTADO_DATAENTREGAREAL2OPERATORVALUES' */
            S212 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
         {
            cmbavContagemresultado_statusdmn2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statusdmn2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSCNT") == 0 )
         {
            cmbavContagemresultado_statuscnt2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statuscnt2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
         {
            edtavContagemresultado_descricao2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_descricao2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
         {
            dynavContagemresultado_sistemacod2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_sistemacod2.Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
         {
            edtavContagemresultado_agrupador2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_agrupador2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_agrupador2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S212( )
      {
         /* 'UPDATECONTAGEMRESULTADO_DATAENTREGAREAL2OPERATORVALUES' Routine */
         cellContagemresultado_dataentregareal_cell2_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_dataentregareal_cell2_Internalname, "Class", cellContagemresultado_dataentregareal_cell2_Class);
         cellContagemresultado_dataentregareal_to_cell2_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_dataentregareal_to_cell2_Internalname, "Class", cellContagemresultado_dataentregareal_to_cell2_Class);
         lblContagemresultado_dataentregareal_rangemiddletext_12_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContagemresultado_dataentregareal_rangemiddletext_12_Internalname, "Class", lblContagemresultado_dataentregareal_rangemiddletext_12_Class);
         if ( AV28DynamicFiltersOperator2 == 0 )
         {
            AV32ContagemResultado_DataEntregaReal2 = DateTimeUtil.ResetTime( Gx_date ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultado_DataEntregaReal2", context.localUtil.TToC( AV32ContagemResultado_DataEntregaReal2, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV28DynamicFiltersOperator2 == 1 )
         {
            AV32ContagemResultado_DataEntregaReal2 = DateTimeUtil.ResetTime( Gx_date ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultado_DataEntregaReal2", context.localUtil.TToC( AV32ContagemResultado_DataEntregaReal2, 8, 5, 0, 3, "/", ":", " "));
            AV33ContagemResultado_DataEntregaReal_To2 = DateTimeUtil.ResetTime( DateTimeUtil.DAdd( Gx_date , + ( (int)(1) )) ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultado_DataEntregaReal_To2", context.localUtil.TToC( AV33ContagemResultado_DataEntregaReal_To2, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV28DynamicFiltersOperator2 == 2 )
         {
            AV32ContagemResultado_DataEntregaReal2 = DateTimeUtil.ResetTime( DateTimeUtil.DAdd( Gx_date , + ( (int)(1) )) ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultado_DataEntregaReal2", context.localUtil.TToC( AV32ContagemResultado_DataEntregaReal2, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV28DynamicFiltersOperator2 == 3 )
         {
            cellContagemresultado_dataentregareal_cell2_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_dataentregareal_cell2_Internalname, "Class", cellContagemresultado_dataentregareal_cell2_Class);
            cellContagemresultado_dataentregareal_to_cell2_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_dataentregareal_to_cell2_Internalname, "Class", cellContagemresultado_dataentregareal_to_cell2_Class);
            lblContagemresultado_dataentregareal_rangemiddletext_12_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContagemresultado_dataentregareal_rangemiddletext_12_Internalname, "Class", lblContagemresultado_dataentregareal_rangemiddletext_12_Class);
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavContagemresultado_demandafm3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demandafm3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demandafm3_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_datadmn3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Visible), 5, 0)));
         cmbavContagemresultado_statusdmn3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statusdmn3.Visible), 5, 0)));
         cmbavContagemresultado_statuscnt3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statuscnt3.Visible), 5, 0)));
         edtavContagemresultado_descricao3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_descricao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_descricao3_Visible), 5, 0)));
         dynavContagemresultado_sistemacod3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_sistemacod3.Visible), 5, 0)));
         edtavContagemresultado_agrupador3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_agrupador3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_agrupador3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
         {
            edtavContagemresultado_demandafm3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demandafm3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demandafm3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_datadmn3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
            /* Execute user subroutine: 'UPDATECONTAGEMRESULTADO_DATAENTREGAREAL3OPERATORVALUES' */
            S222 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
         {
            cmbavContagemresultado_statusdmn3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statusdmn3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSCNT") == 0 )
         {
            cmbavContagemresultado_statuscnt3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavContagemresultado_statuscnt3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
         {
            edtavContagemresultado_descricao3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_descricao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_descricao3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
         {
            dynavContagemresultado_sistemacod3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavContagemresultado_sistemacod3.Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
         {
            edtavContagemresultado_agrupador3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_agrupador3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_agrupador3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S222( )
      {
         /* 'UPDATECONTAGEMRESULTADO_DATAENTREGAREAL3OPERATORVALUES' Routine */
         cellContagemresultado_dataentregareal_cell3_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_dataentregareal_cell3_Internalname, "Class", cellContagemresultado_dataentregareal_cell3_Class);
         cellContagemresultado_dataentregareal_to_cell3_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_dataentregareal_to_cell3_Internalname, "Class", cellContagemresultado_dataentregareal_to_cell3_Class);
         lblContagemresultado_dataentregareal_rangemiddletext_13_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContagemresultado_dataentregareal_rangemiddletext_13_Internalname, "Class", lblContagemresultado_dataentregareal_rangemiddletext_13_Class);
         if ( AV41DynamicFiltersOperator3 == 0 )
         {
            AV45ContagemResultado_DataEntregaReal3 = DateTimeUtil.ResetTime( Gx_date ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ContagemResultado_DataEntregaReal3", context.localUtil.TToC( AV45ContagemResultado_DataEntregaReal3, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV41DynamicFiltersOperator3 == 1 )
         {
            AV45ContagemResultado_DataEntregaReal3 = DateTimeUtil.ResetTime( Gx_date ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ContagemResultado_DataEntregaReal3", context.localUtil.TToC( AV45ContagemResultado_DataEntregaReal3, 8, 5, 0, 3, "/", ":", " "));
            AV46ContagemResultado_DataEntregaReal_To3 = DateTimeUtil.ResetTime( DateTimeUtil.DAdd( Gx_date , + ( (int)(1) )) ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ContagemResultado_DataEntregaReal_To3", context.localUtil.TToC( AV46ContagemResultado_DataEntregaReal_To3, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV41DynamicFiltersOperator3 == 2 )
         {
            AV45ContagemResultado_DataEntregaReal3 = DateTimeUtil.ResetTime( DateTimeUtil.DAdd( Gx_date , + ( (int)(1) )) ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ContagemResultado_DataEntregaReal3", context.localUtil.TToC( AV45ContagemResultado_DataEntregaReal3, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( AV41DynamicFiltersOperator3 == 3 )
         {
            cellContagemresultado_dataentregareal_cell3_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_dataentregareal_cell3_Internalname, "Class", cellContagemresultado_dataentregareal_cell3_Class);
            cellContagemresultado_dataentregareal_to_cell3_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellContagemresultado_dataentregareal_to_cell3_Internalname, "Class", cellContagemresultado_dataentregareal_to_cell3_Class);
            lblContagemresultado_dataentregareal_rangemiddletext_13_Class = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblContagemresultado_dataentregareal_rangemiddletext_13_Internalname, "Class", lblContagemresultado_dataentregareal_rangemiddletext_13_Class);
         }
      }

      protected void S182( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV26DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled2", AV26DynamicFiltersEnabled2);
         AV27DynamicFiltersSelector2 = "CONTAGEMRESULTADO_DEMANDAFM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector2", AV27DynamicFiltersSelector2);
         AV28DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator2), 4, 0)));
         AV29ContagemResultado_DemandaFM2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultado_DemandaFM2", AV29ContagemResultado_DemandaFM2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV39DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39DynamicFiltersEnabled3", AV39DynamicFiltersEnabled3);
         AV40DynamicFiltersSelector3 = "CONTAGEMRESULTADO_DEMANDAFM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40DynamicFiltersSelector3", AV40DynamicFiltersSelector3);
         AV41DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41DynamicFiltersOperator3), 4, 0)));
         AV42ContagemResultado_DemandaFM3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ContagemResultado_DemandaFM3", AV42ContagemResultado_DemandaFM3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV54Session.Get(AV122Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV122Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV54Session.Get(AV122Pgmname+"GridState"), "");
         }
         AV123GXV1 = 1;
         while ( AV123GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV123GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "CONTRATADA_AREATRABALHOCOD") == 0 )
            {
               AV13Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Contratada_AreaTrabalhoCod), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "CONTAGEMRESULTADO_OSVINCULADA") == 0 )
            {
               AV65ContagemResultado_OSVinculada = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65ContagemResultado_OSVinculada), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
            {
               AV66ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66ContagemResultado_ContratadaCod), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "CONTAGEMRESULTADO_SERVICOGRUPO") == 0 )
            {
               AV67ContagemResultado_ServicoGrupo = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ContagemResultado_ServicoGrupo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67ContagemResultado_ServicoGrupo), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "CONTAGEMRESULTADO_SERVICO") == 0 )
            {
               AV68ContagemResultado_Servico = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68ContagemResultado_Servico), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "CONTAGEMRESULTADO_CNTADAOSVINC") == 0 )
            {
               AV69ContagemResultado_CntadaOsVinc = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ContagemResultado_CntadaOsVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69ContagemResultado_CntadaOsVinc), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "CONTAGEMRESULTADO_SERGRUPOVINC") == 0 )
            {
               AV71ContagemResultado_SerGrupoVinc = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ContagemResultado_SerGrupoVinc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71ContagemResultado_SerGrupoVinc), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "CONTAGEMRESULTADO_CODSRVVNC") == 0 )
            {
               AV70ContagemResultado_CodSrvVnc = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70ContagemResultado_CodSrvVnc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV70ContagemResultado_CodSrvVnc), 6, 0)));
            }
            AV123GXV1 = (int)(AV123GXV1+1);
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S192( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV14DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14DynamicFiltersSelector1", AV14DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
            {
               AV15DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0)));
               AV16ContagemResultado_DemandaFM1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultado_DemandaFM1", AV16ContagemResultado_DemandaFM1);
            }
            else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 )
            {
               AV15DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0)));
               AV17ContagemResultado_DataDmn1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultado_DataDmn1", context.localUtil.Format(AV17ContagemResultado_DataDmn1, "99/99/99"));
               AV18ContagemResultado_DataDmn_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultado_DataDmn_To1", context.localUtil.Format(AV18ContagemResultado_DataDmn_To1, "99/99/99"));
            }
            else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
            {
               AV15DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0)));
               AV19ContagemResultado_DataEntregaReal1 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultado_DataEntregaReal1", context.localUtil.TToC( AV19ContagemResultado_DataEntregaReal1, 8, 5, 0, 3, "/", ":", " "));
               AV20ContagemResultado_DataEntregaReal_To1 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultado_DataEntregaReal_To1", context.localUtil.TToC( AV20ContagemResultado_DataEntregaReal_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
            {
               AV21ContagemResultado_StatusDmn1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultado_StatusDmn1", AV21ContagemResultado_StatusDmn1);
            }
            else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSCNT") == 0 )
            {
               AV22ContagemResultado_StatusCnt1 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemResultado_StatusCnt1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22ContagemResultado_StatusCnt1), 2, 0)));
            }
            else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
            {
               AV15DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0)));
               AV23ContagemResultado_Descricao1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContagemResultado_Descricao1", AV23ContagemResultado_Descricao1);
            }
            else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
            {
               AV15DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0)));
               AV24ContagemResultado_SistemaCod1 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultado_SistemaCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24ContagemResultado_SistemaCod1), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
            {
               AV15DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0)));
               AV25ContagemResultado_Agrupador1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContagemResultado_Agrupador1", AV25ContagemResultado_Agrupador1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV26DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersEnabled2", AV26DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV27DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersSelector2", AV27DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
               {
                  AV28DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator2), 4, 0)));
                  AV29ContagemResultado_DemandaFM2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29ContagemResultado_DemandaFM2", AV29ContagemResultado_DemandaFM2);
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 )
               {
                  AV28DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator2), 4, 0)));
                  AV30ContagemResultado_DataDmn2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultado_DataDmn2", context.localUtil.Format(AV30ContagemResultado_DataDmn2, "99/99/99"));
                  AV31ContagemResultado_DataDmn_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContagemResultado_DataDmn_To2", context.localUtil.Format(AV31ContagemResultado_DataDmn_To2, "99/99/99"));
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
               {
                  AV28DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator2), 4, 0)));
                  AV32ContagemResultado_DataEntregaReal2 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ContagemResultado_DataEntregaReal2", context.localUtil.TToC( AV32ContagemResultado_DataEntregaReal2, 8, 5, 0, 3, "/", ":", " "));
                  AV33ContagemResultado_DataEntregaReal_To2 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ContagemResultado_DataEntregaReal_To2", context.localUtil.TToC( AV33ContagemResultado_DataEntregaReal_To2, 8, 5, 0, 3, "/", ":", " "));
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
               {
                  AV34ContagemResultado_StatusDmn2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ContagemResultado_StatusDmn2", AV34ContagemResultado_StatusDmn2);
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSCNT") == 0 )
               {
                  AV35ContagemResultado_StatusCnt2 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ContagemResultado_StatusCnt2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35ContagemResultado_StatusCnt2), 2, 0)));
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
               {
                  AV28DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator2), 4, 0)));
                  AV36ContagemResultado_Descricao2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ContagemResultado_Descricao2", AV36ContagemResultado_Descricao2);
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
               {
                  AV28DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator2), 4, 0)));
                  AV37ContagemResultado_SistemaCod2 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContagemResultado_SistemaCod2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37ContagemResultado_SistemaCod2), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
               {
                  AV28DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator2), 4, 0)));
                  AV38ContagemResultado_Agrupador2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContagemResultado_Agrupador2", AV38ContagemResultado_Agrupador2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV39DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39DynamicFiltersEnabled3", AV39DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV40DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40DynamicFiltersSelector3", AV40DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 )
                  {
                     AV41DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41DynamicFiltersOperator3), 4, 0)));
                     AV42ContagemResultado_DemandaFM3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ContagemResultado_DemandaFM3", AV42ContagemResultado_DemandaFM3);
                  }
                  else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 )
                  {
                     AV41DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41DynamicFiltersOperator3), 4, 0)));
                     AV43ContagemResultado_DataDmn3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ContagemResultado_DataDmn3", context.localUtil.Format(AV43ContagemResultado_DataDmn3, "99/99/99"));
                     AV44ContagemResultado_DataDmn_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ContagemResultado_DataDmn_To3", context.localUtil.Format(AV44ContagemResultado_DataDmn_To3, "99/99/99"));
                  }
                  else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 )
                  {
                     AV41DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41DynamicFiltersOperator3), 4, 0)));
                     AV45ContagemResultado_DataEntregaReal3 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ContagemResultado_DataEntregaReal3", context.localUtil.TToC( AV45ContagemResultado_DataEntregaReal3, 8, 5, 0, 3, "/", ":", " "));
                     AV46ContagemResultado_DataEntregaReal_To3 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ContagemResultado_DataEntregaReal_To3", context.localUtil.TToC( AV46ContagemResultado_DataEntregaReal_To3, 8, 5, 0, 3, "/", ":", " "));
                  }
                  else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                  {
                     AV47ContagemResultado_StatusDmn3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ContagemResultado_StatusDmn3", AV47ContagemResultado_StatusDmn3);
                  }
                  else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSCNT") == 0 )
                  {
                     AV48ContagemResultado_StatusCnt3 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ContagemResultado_StatusCnt3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48ContagemResultado_StatusCnt3), 2, 0)));
                  }
                  else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                  {
                     AV41DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41DynamicFiltersOperator3), 4, 0)));
                     AV49ContagemResultado_Descricao3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ContagemResultado_Descricao3", AV49ContagemResultado_Descricao3);
                  }
                  else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                  {
                     AV41DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41DynamicFiltersOperator3), 4, 0)));
                     AV50ContagemResultado_SistemaCod3 = (int)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ContagemResultado_SistemaCod3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50ContagemResultado_SistemaCod3), 6, 0)));
                  }
                  else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                  {
                     AV41DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41DynamicFiltersOperator3), 4, 0)));
                     AV51ContagemResultado_Agrupador3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ContagemResultado_Agrupador3", AV51ContagemResultado_Agrupador3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV52DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV54Session.Get(AV122Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV13Contratada_AreaTrabalhoCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "CONTRATADA_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV13Contratada_AreaTrabalhoCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV65ContagemResultado_OSVinculada) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "CONTAGEMRESULTADO_OSVINCULADA";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV65ContagemResultado_OSVinculada), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV66ContagemResultado_ContratadaCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "CONTAGEMRESULTADO_CONTRATADACOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV66ContagemResultado_ContratadaCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV67ContagemResultado_ServicoGrupo) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "CONTAGEMRESULTADO_SERVICOGRUPO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV67ContagemResultado_ServicoGrupo), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV68ContagemResultado_Servico) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "CONTAGEMRESULTADO_SERVICO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV68ContagemResultado_Servico), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV69ContagemResultado_CntadaOsVinc) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "CONTAGEMRESULTADO_CNTADAOSVINC";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV69ContagemResultado_CntadaOsVinc), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV71ContagemResultado_SerGrupoVinc) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "CONTAGEMRESULTADO_SERGRUPOVINC";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV71ContagemResultado_SerGrupoVinc), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV70ContagemResultado_CodSrvVnc) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "CONTAGEMRESULTADO_CODSRVVNC";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV70ContagemResultado_CodSrvVnc), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV122Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S172( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV53DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV14DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV16ContagemResultado_DemandaFM1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV16ContagemResultado_DemandaFM1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV15DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ! ( (DateTime.MinValue==AV17ContagemResultado_DataDmn1) && (DateTime.MinValue==AV18ContagemResultado_DataDmn_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV17ContagemResultado_DataDmn1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Operator = AV15DynamicFiltersOperator1;
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV18ContagemResultado_DataDmn_To1, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ! ( (DateTime.MinValue==AV19ContagemResultado_DataEntregaReal1) && (DateTime.MinValue==AV20ContagemResultado_DataEntregaReal_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV19ContagemResultado_DataEntregaReal1, 8, 5, 0, 3, "/", ":", " ");
               AV12GridStateDynamicFilter.gxTpr_Operator = AV15DynamicFiltersOperator1;
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV20ContagemResultado_DataEntregaReal_To1, 8, 5, 0, 3, "/", ":", " ");
            }
            else if ( ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21ContagemResultado_StatusDmn1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21ContagemResultado_StatusDmn1;
            }
            else if ( ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSCNT") == 0 ) && ! (0==AV22ContagemResultado_StatusCnt1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV22ContagemResultado_StatusCnt1), 2, 0);
            }
            else if ( ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23ContagemResultado_Descricao1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV23ContagemResultado_Descricao1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV15DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ! (0==AV24ContagemResultado_SistemaCod1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV24ContagemResultado_SistemaCod1), 6, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV15DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV14DynamicFiltersSelector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContagemResultado_Agrupador1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25ContagemResultado_Agrupador1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV15DynamicFiltersOperator1;
            }
            if ( AV52DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV26DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV27DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV29ContagemResultado_DemandaFM2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV29ContagemResultado_DemandaFM2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV28DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ! ( (DateTime.MinValue==AV30ContagemResultado_DataDmn2) && (DateTime.MinValue==AV31ContagemResultado_DataDmn_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV30ContagemResultado_DataDmn2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Operator = AV28DynamicFiltersOperator2;
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV31ContagemResultado_DataDmn_To2, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ! ( (DateTime.MinValue==AV32ContagemResultado_DataEntregaReal2) && (DateTime.MinValue==AV33ContagemResultado_DataEntregaReal_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV32ContagemResultado_DataEntregaReal2, 8, 5, 0, 3, "/", ":", " ");
               AV12GridStateDynamicFilter.gxTpr_Operator = AV28DynamicFiltersOperator2;
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV33ContagemResultado_DataEntregaReal_To2, 8, 5, 0, 3, "/", ":", " ");
            }
            else if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV34ContagemResultado_StatusDmn2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV34ContagemResultado_StatusDmn2;
            }
            else if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSCNT") == 0 ) && ! (0==AV35ContagemResultado_StatusCnt2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV35ContagemResultado_StatusCnt2), 2, 0);
            }
            else if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV36ContagemResultado_Descricao2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV36ContagemResultado_Descricao2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV28DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ! (0==AV37ContagemResultado_SistemaCod2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV37ContagemResultado_SistemaCod2), 6, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV28DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV38ContagemResultado_Agrupador2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV38ContagemResultado_Agrupador2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV28DynamicFiltersOperator2;
            }
            if ( AV52DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV39DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV40DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV42ContagemResultado_DemandaFM3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV42ContagemResultado_DemandaFM3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV41DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ! ( (DateTime.MinValue==AV43ContagemResultado_DataDmn3) && (DateTime.MinValue==AV44ContagemResultado_DataDmn_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV43ContagemResultado_DataDmn3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Operator = AV41DynamicFiltersOperator3;
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV44ContagemResultado_DataDmn_To3, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ! ( (DateTime.MinValue==AV45ContagemResultado_DataEntregaReal3) && (DateTime.MinValue==AV46ContagemResultado_DataEntregaReal_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV45ContagemResultado_DataEntregaReal3, 8, 5, 0, 3, "/", ":", " ");
               AV12GridStateDynamicFilter.gxTpr_Operator = AV41DynamicFiltersOperator3;
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV46ContagemResultado_DataEntregaReal_To3, 8, 5, 0, 3, "/", ":", " ");
            }
            else if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV47ContagemResultado_StatusDmn3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV47ContagemResultado_StatusDmn3;
            }
            else if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSCNT") == 0 ) && ! (0==AV48ContagemResultado_StatusCnt3) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV48ContagemResultado_StatusCnt3), 2, 0);
            }
            else if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ContagemResultado_Descricao3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV49ContagemResultado_Descricao3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV41DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ! (0==AV50ContagemResultado_SistemaCod3) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV50ContagemResultado_SistemaCod3), 6, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV41DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV51ContagemResultado_Agrupador3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV51ContagemResultado_Agrupador3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV41DynamicFiltersOperator3;
            }
            if ( AV52DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV122Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContagemResultado";
         AV54Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_TB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_TB2( true) ;
         }
         else
         {
            wb_table2_8_TB2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_TB2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_200_TB2( true) ;
         }
         else
         {
            wb_table3_200_TB2( false) ;
         }
         return  ;
      }

      protected void wb_table3_200_TB2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_TB2e( true) ;
         }
         else
         {
            wb_table1_2_TB2e( false) ;
         }
      }

      protected void wb_table3_200_TB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_203_TB2( true) ;
         }
         else
         {
            wb_table4_203_TB2( false) ;
         }
         return  ;
      }

      protected void wb_table4_203_TB2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_200_TB2e( true) ;
         }
         else
         {
            wb_table3_200_TB2e( false) ;
         }
      }

      protected void wb_table4_203_TB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"206\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Resultado_Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(80), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "OS") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "OS Ref.") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(90), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Data Demanda") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Data de Entrega") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "T�tulo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Sigla do Sistema") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV64Display));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A493ContagemResultado_DemandaFM);
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A457ContagemResultado_Demanda);
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContagemResultado_DataDmn_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A2017ContagemResultado_DataEntregaReal, 10, 8, 0, 3, "/", ":", " "));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A494ContagemResultado_Descricao);
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A509ContagemrResultado_SistemaSigla));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 206 )
         {
            wbEnd = 0;
            nRC_GXsfl_206 = (short)(nGXsfl_206_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_203_TB2e( true) ;
         }
         else
         {
            wb_table4_203_TB2e( false) ;
         }
      }

      protected void wb_table2_8_TB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_11_TB2( true) ;
         }
         else
         {
            wb_table5_11_TB2( false) ;
         }
         return  ;
      }

      protected void wb_table5_11_TB2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_16_TB2( true) ;
         }
         else
         {
            wb_table6_16_TB2( false) ;
         }
         return  ;
      }

      protected void wb_table6_16_TB2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_TB2e( true) ;
         }
         else
         {
            wb_table2_8_TB2e( false) ;
         }
      }

      protected void wb_table6_16_TB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextcontratada_areatrabalhocod_Internalname, "�rea de Trabalho", "", "", lblFiltertextcontratada_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_206_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_areatrabalhocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Contratada_AreaTrabalhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13Contratada_AreaTrabalhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_areatrabalhocod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextcontagemresultado_osvinculada_Internalname, "ContagemResultado_OSVinculada.Title", "", "", lblFiltertextcontagemresultado_osvinculada_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'" + sGXsfl_206_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_osvinculada_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV65ContagemResultado_OSVinculada), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV65ContagemResultado_OSVinculada), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,27);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_osvinculada_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_29_TB2( true) ;
         }
         else
         {
            wb_table7_29_TB2( false) ;
         }
         return  ;
      }

      protected void wb_table7_29_TB2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_16_TB2e( true) ;
         }
         else
         {
            wb_table6_16_TB2e( false) ;
         }
      }

      protected void wb_table7_29_TB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefixcontagemresultado_contratadacod_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefixcontagemresultado_contratadacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersfixedfiltercaptioncontagemresultado_contratadacod_Internalname, "Contratada", "", "", lblDynamicfiltersfixedfiltercaptioncontagemresultado_contratadacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFixedFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddlecontagemresultado_contratadacod_Internalname, "valor", "", "", lblDynamicfiltersmiddlecontagemresultado_contratadacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_206_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_contratadacod, dynavContagemresultado_contratadacod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV66ContagemResultado_ContratadaCod), 6, 0)), 1, dynavContagemresultado_contratadacod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynavContagemresultado_contratadacod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            dynavContagemresultado_contratadacod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV66ContagemResultado_ContratadaCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_contratadacod_Internalname, "Values", (String)(dynavContagemresultado_contratadacod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefixcontagemresultado_servicogrupo_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefixcontagemresultado_servicogrupo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersfixedfiltercaptioncontagemresultado_servicogrupo_Internalname, "Grupo de Servi�o", "", "", lblDynamicfiltersfixedfiltercaptioncontagemresultado_servicogrupo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFixedFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddlecontagemresultado_servicogrupo_Internalname, "valor", "", "", lblDynamicfiltersmiddlecontagemresultado_servicogrupo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'" + sGXsfl_206_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_servicogrupo, dynavContagemresultado_servicogrupo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV67ContagemResultado_ServicoGrupo), 6, 0)), 1, dynavContagemresultado_servicogrupo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", "", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            dynavContagemresultado_servicogrupo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV67ContagemResultado_ServicoGrupo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_servicogrupo_Internalname, "Values", (String)(dynavContagemresultado_servicogrupo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefixcontagemresultado_servico_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefixcontagemresultado_servico_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersfixedfiltercaptioncontagemresultado_servico_Internalname, "Servi�o", "", "", lblDynamicfiltersfixedfiltercaptioncontagemresultado_servico_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFixedFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddlecontagemresultado_servico_Internalname, "valor", "", "", lblDynamicfiltersmiddlecontagemresultado_servico_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_206_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_servico, dynavContagemresultado_servico_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV68ContagemResultado_Servico), 6, 0)), 1, dynavContagemresultado_servico_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", "", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            dynavContagemresultado_servico.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV68ContagemResultado_Servico), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_servico_Internalname, "Values", (String)(dynavContagemresultado_servico.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefixcontagemresultado_cntadaosvinc_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefixcontagemresultado_cntadaosvinc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersfixedfiltercaptioncontagemresultado_cntadaosvinc_Internalname, "Contratada", "", "", lblDynamicfiltersfixedfiltercaptioncontagemresultado_cntadaosvinc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFixedFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddlecontagemresultado_cntadaosvinc_Internalname, "valor", "", "", lblDynamicfiltersmiddlecontagemresultado_cntadaosvinc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_206_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_cntadaosvinc, dynavContagemresultado_cntadaosvinc_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV69ContagemResultado_CntadaOsVinc), 6, 0)), 1, dynavContagemresultado_cntadaosvinc_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynavContagemresultado_cntadaosvinc.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", "", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            dynavContagemresultado_cntadaosvinc.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV69ContagemResultado_CntadaOsVinc), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_cntadaosvinc_Internalname, "Values", (String)(dynavContagemresultado_cntadaosvinc.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefixcontagemresultado_sergrupovinc_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefixcontagemresultado_sergrupovinc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersfixedfiltercaptioncontagemresultado_sergrupovinc_Internalname, "Grupo de Servi�os ", "", "", lblDynamicfiltersfixedfiltercaptioncontagemresultado_sergrupovinc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFixedFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddlecontagemresultado_sergrupovinc_Internalname, "valor", "", "", lblDynamicfiltersmiddlecontagemresultado_sergrupovinc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_206_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_sergrupovinc, dynavContagemresultado_sergrupovinc_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV71ContagemResultado_SerGrupoVinc), 6, 0)), 1, dynavContagemresultado_sergrupovinc_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            dynavContagemresultado_sergrupovinc.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV71ContagemResultado_SerGrupoVinc), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sergrupovinc_Internalname, "Values", (String)(dynavContagemresultado_sergrupovinc.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefixcontagemresultado_codsrvvnc_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefixcontagemresultado_codsrvvnc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersfixedfiltercaptioncontagemresultado_codsrvvnc_Internalname, "Servi�o", "", "", lblDynamicfiltersfixedfiltercaptioncontagemresultado_codsrvvnc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFixedFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddlecontagemresultado_codsrvvnc_Internalname, "valor", "", "", lblDynamicfiltersmiddlecontagemresultado_codsrvvnc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'" + sGXsfl_206_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_codsrvvnc, dynavContagemresultado_codsrvvnc_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV70ContagemResultado_CodSrvVnc), 6, 0)), 1, dynavContagemresultado_codsrvvnc_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,83);\"", "", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            dynavContagemresultado_codsrvvnc.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV70ContagemResultado_CodSrvVnc), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_codsrvvnc_Internalname, "Values", (String)(dynavContagemresultado_codsrvvnc.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'" + sGXsfl_206_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV14DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,88);\"", "", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV14DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_92_TB2( true) ;
         }
         else
         {
            wb_table8_92_TB2( false) ;
         }
         return  ;
      }

      protected void wb_table8_92_TB2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'" + sGXsfl_206_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV27DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,126);\"", "", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_130_TB2( true) ;
         }
         else
         {
            wb_table9_130_TB2( false) ;
         }
         return  ;
      }

      protected void wb_table9_130_TB2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 158,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 159,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 164,'',false,'" + sGXsfl_206_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV40DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,164);\"", "", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV40DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_168_TB2( true) ;
         }
         else
         {
            wb_table10_168_TB2( false) ;
         }
         return  ;
      }

      protected void wb_table10_168_TB2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 196,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_29_TB2e( true) ;
         }
         else
         {
            wb_table7_29_TB2e( false) ;
         }
      }

      protected void wb_table10_168_TB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 171,'',false,'" + sGXsfl_206_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV41DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSOPERATOR3.CLICK."+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,171);\"", "", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV41DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 173,'',false,'" + sGXsfl_206_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_demandafm3_Internalname, AV42ContagemResultado_DemandaFM3, StringUtil.RTrim( context.localUtil.Format( AV42ContagemResultado_DemandaFM3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,173);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_demandafm3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_demandafm3_Visible, 1, 0, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            wb_table11_174_TB2( true) ;
         }
         else
         {
            wb_table11_174_TB2( false) ;
         }
         return  ;
      }

      protected void wb_table11_174_TB2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table12_182_TB2( true) ;
         }
         else
         {
            wb_table12_182_TB2( false) ;
         }
         return  ;
      }

      protected void wb_table12_182_TB2e( bool wbgen )
      {
         if ( wbgen )
         {
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 190,'',false,'" + sGXsfl_206_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContagemresultado_statusdmn3, cmbavContagemresultado_statusdmn3_Internalname, StringUtil.RTrim( AV47ContagemResultado_StatusDmn3), 1, cmbavContagemresultado_statusdmn3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavContagemresultado_statusdmn3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,190);\"", "", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            cmbavContagemresultado_statusdmn3.CurrentValue = StringUtil.RTrim( AV47ContagemResultado_StatusDmn3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn3_Internalname, "Values", (String)(cmbavContagemresultado_statusdmn3.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 191,'',false,'" + sGXsfl_206_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContagemresultado_statuscnt3, cmbavContagemresultado_statuscnt3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV48ContagemResultado_StatusCnt3), 2, 0)), 1, cmbavContagemresultado_statuscnt3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavContagemresultado_statuscnt3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,191);\"", "", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            cmbavContagemresultado_statuscnt3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV48ContagemResultado_StatusCnt3), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt3_Internalname, "Values", (String)(cmbavContagemresultado_statuscnt3.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 192,'',false,'" + sGXsfl_206_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_descricao3_Internalname, AV49ContagemResultado_Descricao3, StringUtil.RTrim( context.localUtil.Format( AV49ContagemResultado_Descricao3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,192);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_descricao3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_descricao3_Visible, 1, 0, "text", "", 100, "%", 1, "row", 500, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 193,'',false,'" + sGXsfl_206_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_sistemacod3, dynavContagemresultado_sistemacod3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV50ContagemResultado_SistemaCod3), 6, 0)), 1, dynavContagemresultado_sistemacod3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavContagemresultado_sistemacod3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,193);\"", "", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            dynavContagemresultado_sistemacod3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV50ContagemResultado_SistemaCod3), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod3_Internalname, "Values", (String)(dynavContagemresultado_sistemacod3.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 194,'',false,'" + sGXsfl_206_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_agrupador3_Internalname, StringUtil.RTrim( AV51ContagemResultado_Agrupador3), StringUtil.RTrim( context.localUtil.Format( AV51ContagemResultado_Agrupador3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,194);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_agrupador3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_agrupador3_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_168_TB2e( true) ;
         }
         else
         {
            wb_table10_168_TB2e( false) ;
         }
      }

      protected void wb_table12_182_TB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Internalname, tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_dataentregareal_cell3_Internalname+"\"  class='"+cellContagemresultado_dataentregareal_cell3_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 185,'',false,'" + sGXsfl_206_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_dataentregareal3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_dataentregareal3_Internalname, context.localUtil.TToC( AV45ContagemResultado_DataEntregaReal3, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV45ContagemResultado_DataEntregaReal3, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,185);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_dataentregareal3_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_dataentregareal3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_dataentregareal_rangemiddletext_1_cell3_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultado_dataentregareal_rangemiddletext_13_Internalname, "to", "", "", lblContagemresultado_dataentregareal_rangemiddletext_13_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", lblContagemresultado_dataentregareal_rangemiddletext_13_Class, 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_dataentregareal_to_cell3_Internalname+"\"  class='"+cellContagemresultado_dataentregareal_to_cell3_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 189,'',false,'" + sGXsfl_206_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_dataentregareal_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_dataentregareal_to3_Internalname, context.localUtil.TToC( AV46ContagemResultado_DataEntregaReal_To3, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV46ContagemResultado_DataEntregaReal_To3, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,189);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_dataentregareal_to3_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_dataentregareal_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_182_TB2e( true) ;
         }
         else
         {
            wb_table12_182_TB2e( false) ;
         }
      }

      protected void wb_table11_174_TB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultado_datadmn3_Internalname, tblTablemergeddynamicfilterscontagemresultado_datadmn3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 177,'',false,'" + sGXsfl_206_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datadmn3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datadmn3_Internalname, context.localUtil.Format(AV43ContagemResultado_DataDmn3, "99/99/99"), context.localUtil.Format( AV43ContagemResultado_DataDmn3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,177);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datadmn3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datadmn3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_datadmn_rangemiddletext_1_cell3_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultado_datadmn_rangemiddletext_13_Internalname, "to", "", "", lblContagemresultado_datadmn_rangemiddletext_13_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_datadmn_to_cell3_Internalname+"\"  class=''>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 181,'',false,'" + sGXsfl_206_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datadmn_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datadmn_to3_Internalname, context.localUtil.Format(AV44ContagemResultado_DataDmn_To3, "99/99/99"), context.localUtil.Format( AV44ContagemResultado_DataDmn_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,181);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datadmn_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datadmn_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_174_TB2e( true) ;
         }
         else
         {
            wb_table11_174_TB2e( false) ;
         }
      }

      protected void wb_table9_130_TB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'" + sGXsfl_206_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSOPERATOR2.CLICK."+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,133);\"", "", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'" + sGXsfl_206_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_demandafm2_Internalname, AV29ContagemResultado_DemandaFM2, StringUtil.RTrim( context.localUtil.Format( AV29ContagemResultado_DemandaFM2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,135);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_demandafm2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_demandafm2_Visible, 1, 0, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            wb_table13_136_TB2( true) ;
         }
         else
         {
            wb_table13_136_TB2( false) ;
         }
         return  ;
      }

      protected void wb_table13_136_TB2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table14_144_TB2( true) ;
         }
         else
         {
            wb_table14_144_TB2( false) ;
         }
         return  ;
      }

      protected void wb_table14_144_TB2e( bool wbgen )
      {
         if ( wbgen )
         {
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 152,'',false,'" + sGXsfl_206_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContagemresultado_statusdmn2, cmbavContagemresultado_statusdmn2_Internalname, StringUtil.RTrim( AV34ContagemResultado_StatusDmn2), 1, cmbavContagemresultado_statusdmn2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavContagemresultado_statusdmn2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,152);\"", "", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            cmbavContagemresultado_statusdmn2.CurrentValue = StringUtil.RTrim( AV34ContagemResultado_StatusDmn2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn2_Internalname, "Values", (String)(cmbavContagemresultado_statusdmn2.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 153,'',false,'" + sGXsfl_206_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContagemresultado_statuscnt2, cmbavContagemresultado_statuscnt2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV35ContagemResultado_StatusCnt2), 2, 0)), 1, cmbavContagemresultado_statuscnt2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavContagemresultado_statuscnt2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,153);\"", "", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            cmbavContagemresultado_statuscnt2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV35ContagemResultado_StatusCnt2), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt2_Internalname, "Values", (String)(cmbavContagemresultado_statuscnt2.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 154,'',false,'" + sGXsfl_206_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_descricao2_Internalname, AV36ContagemResultado_Descricao2, StringUtil.RTrim( context.localUtil.Format( AV36ContagemResultado_Descricao2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,154);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_descricao2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_descricao2_Visible, 1, 0, "text", "", 100, "%", 1, "row", 500, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 155,'',false,'" + sGXsfl_206_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_sistemacod2, dynavContagemresultado_sistemacod2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV37ContagemResultado_SistemaCod2), 6, 0)), 1, dynavContagemresultado_sistemacod2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavContagemresultado_sistemacod2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,155);\"", "", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            dynavContagemresultado_sistemacod2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV37ContagemResultado_SistemaCod2), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod2_Internalname, "Values", (String)(dynavContagemresultado_sistemacod2.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 156,'',false,'" + sGXsfl_206_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_agrupador2_Internalname, StringUtil.RTrim( AV38ContagemResultado_Agrupador2), StringUtil.RTrim( context.localUtil.Format( AV38ContagemResultado_Agrupador2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,156);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_agrupador2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_agrupador2_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_130_TB2e( true) ;
         }
         else
         {
            wb_table9_130_TB2e( false) ;
         }
      }

      protected void wb_table14_144_TB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Internalname, tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_dataentregareal_cell2_Internalname+"\"  class='"+cellContagemresultado_dataentregareal_cell2_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 147,'',false,'" + sGXsfl_206_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_dataentregareal2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_dataentregareal2_Internalname, context.localUtil.TToC( AV32ContagemResultado_DataEntregaReal2, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV32ContagemResultado_DataEntregaReal2, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,147);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_dataentregareal2_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_dataentregareal2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_dataentregareal_rangemiddletext_1_cell2_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultado_dataentregareal_rangemiddletext_12_Internalname, "to", "", "", lblContagemresultado_dataentregareal_rangemiddletext_12_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", lblContagemresultado_dataentregareal_rangemiddletext_12_Class, 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_dataentregareal_to_cell2_Internalname+"\"  class='"+cellContagemresultado_dataentregareal_to_cell2_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 151,'',false,'" + sGXsfl_206_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_dataentregareal_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_dataentregareal_to2_Internalname, context.localUtil.TToC( AV33ContagemResultado_DataEntregaReal_To2, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV33ContagemResultado_DataEntregaReal_To2, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,151);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_dataentregareal_to2_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_dataentregareal_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table14_144_TB2e( true) ;
         }
         else
         {
            wb_table14_144_TB2e( false) ;
         }
      }

      protected void wb_table13_136_TB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultado_datadmn2_Internalname, tblTablemergeddynamicfilterscontagemresultado_datadmn2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 139,'',false,'" + sGXsfl_206_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datadmn2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datadmn2_Internalname, context.localUtil.Format(AV30ContagemResultado_DataDmn2, "99/99/99"), context.localUtil.Format( AV30ContagemResultado_DataDmn2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,139);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datadmn2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datadmn2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_datadmn_rangemiddletext_1_cell2_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultado_datadmn_rangemiddletext_12_Internalname, "to", "", "", lblContagemresultado_datadmn_rangemiddletext_12_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_datadmn_to_cell2_Internalname+"\"  class=''>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 143,'',false,'" + sGXsfl_206_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datadmn_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datadmn_to2_Internalname, context.localUtil.Format(AV31ContagemResultado_DataDmn_To2, "99/99/99"), context.localUtil.Format( AV31ContagemResultado_DataDmn_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,143);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datadmn_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datadmn_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table13_136_TB2e( true) ;
         }
         else
         {
            wb_table13_136_TB2e( false) ;
         }
      }

      protected void wb_table8_92_TB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_206_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSOPERATOR1.CLICK."+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,95);\"", "", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_206_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_demandafm1_Internalname, AV16ContagemResultado_DemandaFM1, StringUtil.RTrim( context.localUtil.Format( AV16ContagemResultado_DemandaFM1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_demandafm1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_demandafm1_Visible, 1, 0, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            wb_table15_98_TB2( true) ;
         }
         else
         {
            wb_table15_98_TB2( false) ;
         }
         return  ;
      }

      protected void wb_table15_98_TB2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table16_106_TB2( true) ;
         }
         else
         {
            wb_table16_106_TB2( false) ;
         }
         return  ;
      }

      protected void wb_table16_106_TB2e( bool wbgen )
      {
         if ( wbgen )
         {
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_206_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContagemresultado_statusdmn1, cmbavContagemresultado_statusdmn1_Internalname, StringUtil.RTrim( AV21ContagemResultado_StatusDmn1), 1, cmbavContagemresultado_statusdmn1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavContagemresultado_statusdmn1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,114);\"", "", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            cmbavContagemresultado_statusdmn1.CurrentValue = StringUtil.RTrim( AV21ContagemResultado_StatusDmn1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statusdmn1_Internalname, "Values", (String)(cmbavContagemresultado_statusdmn1.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_206_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContagemresultado_statuscnt1, cmbavContagemresultado_statuscnt1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22ContagemResultado_StatusCnt1), 2, 0)), 1, cmbavContagemresultado_statuscnt1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavContagemresultado_statuscnt1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,115);\"", "", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            cmbavContagemresultado_statuscnt1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22ContagemResultado_StatusCnt1), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContagemresultado_statuscnt1_Internalname, "Values", (String)(cmbavContagemresultado_statuscnt1.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_206_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_descricao1_Internalname, AV23ContagemResultado_Descricao1, StringUtil.RTrim( context.localUtil.Format( AV23ContagemResultado_Descricao1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_descricao1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_descricao1_Visible, 1, 0, "text", "", 100, "%", 1, "row", 500, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_206_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_sistemacod1, dynavContagemresultado_sistemacod1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24ContagemResultado_SistemaCod1), 6, 0)), 1, dynavContagemresultado_sistemacod1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavContagemresultado_sistemacod1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,117);\"", "", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            dynavContagemresultado_sistemacod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24ContagemResultado_SistemaCod1), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_sistemacod1_Internalname, "Values", (String)(dynavContagemresultado_sistemacod1.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_206_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_agrupador1_Internalname, StringUtil.RTrim( AV25ContagemResultado_Agrupador1), StringUtil.RTrim( context.localUtil.Format( AV25ContagemResultado_Agrupador1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,118);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_agrupador1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContagemresultado_agrupador1_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_92_TB2e( true) ;
         }
         else
         {
            wb_table8_92_TB2e( false) ;
         }
      }

      protected void wb_table16_106_TB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Internalname, tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_dataentregareal_cell1_Internalname+"\"  class='"+cellContagemresultado_dataentregareal_cell1_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_206_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_dataentregareal1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_dataentregareal1_Internalname, context.localUtil.TToC( AV19ContagemResultado_DataEntregaReal1, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV19ContagemResultado_DataEntregaReal1, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_dataentregareal1_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_dataentregareal1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_dataentregareal_rangemiddletext_1_cell1_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultado_dataentregareal_rangemiddletext_11_Internalname, "to", "", "", lblContagemresultado_dataentregareal_rangemiddletext_11_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", lblContagemresultado_dataentregareal_rangemiddletext_11_Class, 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_dataentregareal_to_cell1_Internalname+"\"  class='"+cellContagemresultado_dataentregareal_to_cell1_Class+"'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_206_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_dataentregareal_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_dataentregareal_to1_Internalname, context.localUtil.TToC( AV20ContagemResultado_DataEntregaReal_To1, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV20ContagemResultado_DataEntregaReal_To1, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_dataentregareal_to1_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_dataentregareal_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table16_106_TB2e( true) ;
         }
         else
         {
            wb_table16_106_TB2e( false) ;
         }
      }

      protected void wb_table15_98_TB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultado_datadmn1_Internalname, tblTablemergeddynamicfilterscontagemresultado_datadmn1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_206_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datadmn1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datadmn1_Internalname, context.localUtil.Format(AV17ContagemResultado_DataDmn1, "99/99/99"), context.localUtil.Format( AV17ContagemResultado_DataDmn1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datadmn1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datadmn1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_datadmn_rangemiddletext_1_cell1_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultado_datadmn_rangemiddletext_11_Internalname, "to", "", "", lblContagemresultado_datadmn_rangemiddletext_11_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemresultado_datadmn_to_cell1_Internalname+"\"  class=''>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_206_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datadmn_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datadmn_to1_Internalname, context.localUtil.Format(AV18ContagemResultado_DataDmn_To1, "99/99/99"), context.localUtil.Format( AV18ContagemResultado_DataDmn_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datadmn_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datadmn_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table15_98_TB2e( true) ;
         }
         else
         {
            wb_table15_98_TB2e( false) ;
         }
      }

      protected void wb_table5_11_TB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultadotitle_Internalname, "Relat�rio de Compara��o entre Demandas", "", "", lblContagemresultadotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_ExtraWWContagemResultadoRelComparacaoDemandas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_11_TB2e( true) ;
         }
         else
         {
            wb_table5_11_TB2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PATB2( ) ;
         WSTB2( ) ;
         WETB2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?5113033");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020414120047");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("extrawwcontagemresultadorelcomparacaodemandas.js", "?2020414120047");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_2062( )
      {
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_206_idx;
         edtContagemResultado_Codigo_Internalname = "CONTAGEMRESULTADO_CODIGO_"+sGXsfl_206_idx;
         edtContagemResultado_DemandaFM_Internalname = "CONTAGEMRESULTADO_DEMANDAFM_"+sGXsfl_206_idx;
         edtContagemResultado_Demanda_Internalname = "CONTAGEMRESULTADO_DEMANDA_"+sGXsfl_206_idx;
         edtContagemResultado_DataDmn_Internalname = "CONTAGEMRESULTADO_DATADMN_"+sGXsfl_206_idx;
         edtContagemResultado_DataEntregaReal_Internalname = "CONTAGEMRESULTADO_DATAENTREGAREAL_"+sGXsfl_206_idx;
         edtContagemResultado_Descricao_Internalname = "CONTAGEMRESULTADO_DESCRICAO_"+sGXsfl_206_idx;
         edtContagemrResultado_SistemaSigla_Internalname = "CONTAGEMRRESULTADO_SISTEMASIGLA_"+sGXsfl_206_idx;
      }

      protected void SubsflControlProps_fel_2062( )
      {
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_206_fel_idx;
         edtContagemResultado_Codigo_Internalname = "CONTAGEMRESULTADO_CODIGO_"+sGXsfl_206_fel_idx;
         edtContagemResultado_DemandaFM_Internalname = "CONTAGEMRESULTADO_DEMANDAFM_"+sGXsfl_206_fel_idx;
         edtContagemResultado_Demanda_Internalname = "CONTAGEMRESULTADO_DEMANDA_"+sGXsfl_206_fel_idx;
         edtContagemResultado_DataDmn_Internalname = "CONTAGEMRESULTADO_DATADMN_"+sGXsfl_206_fel_idx;
         edtContagemResultado_DataEntregaReal_Internalname = "CONTAGEMRESULTADO_DATAENTREGAREAL_"+sGXsfl_206_fel_idx;
         edtContagemResultado_Descricao_Internalname = "CONTAGEMRESULTADO_DESCRICAO_"+sGXsfl_206_fel_idx;
         edtContagemrResultado_SistemaSigla_Internalname = "CONTAGEMRRESULTADO_SISTEMASIGLA_"+sGXsfl_206_fel_idx;
      }

      protected void sendrow_2062( )
      {
         SubsflControlProps_2062( ) ;
         WBTB0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_206_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_206_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_206_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV64Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV64Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV120Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV64Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV64Display)) ? AV120Display_GXI : context.PathToRelativeUrl( AV64Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDisplay_Enabled,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV64Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)206,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_DemandaFM_Internalname,(String)A493ContagemResultado_DemandaFM,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_DemandaFM_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)80,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)206,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_Demanda_Internalname,(String)A457ContagemResultado_Demanda,StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_Demanda_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)30,(short)0,(short)0,(short)206,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_DataDmn_Internalname,context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"),context.localUtil.Format( A471ContagemResultado_DataDmn, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtContagemResultado_DataDmn_Link,(String)"",(String)"",(String)"",(String)edtContagemResultado_DataDmn_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)90,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)206,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_DataEntregaReal_Internalname,context.localUtil.TToC( A2017ContagemResultado_DataEntregaReal, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A2017ContagemResultado_DataEntregaReal, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_DataEntregaReal_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)206,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_Descricao_Internalname,(String)A494ContagemResultado_Descricao,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)206,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemrResultado_SistemaSigla_Internalname,StringUtil.RTrim( A509ContagemrResultado_SistemaSigla),StringUtil.RTrim( context.localUtil.Format( A509ContagemrResultado_SistemaSigla, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemrResultado_SistemaSigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)25,(short)0,(short)0,(short)206,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_CODIGO"+"_"+sGXsfl_206_idx, GetSecureSignedToken( sGXsfl_206_idx, context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_206_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_206_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_206_idx+1));
            sGXsfl_206_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_206_idx), 4, 0)), 4, "0");
            SubsflControlProps_2062( ) ;
         }
         /* End function sendrow_2062 */
      }

      protected void init_default_properties( )
      {
         lblContagemresultadotitle_Internalname = "CONTAGEMRESULTADOTITLE";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblFiltertextcontratada_areatrabalhocod_Internalname = "FILTERTEXTCONTRATADA_AREATRABALHOCOD";
         edtavContratada_areatrabalhocod_Internalname = "vCONTRATADA_AREATRABALHOCOD";
         lblFiltertextcontagemresultado_osvinculada_Internalname = "FILTERTEXTCONTAGEMRESULTADO_OSVINCULADA";
         edtavContagemresultado_osvinculada_Internalname = "vCONTAGEMRESULTADO_OSVINCULADA";
         lblDynamicfiltersprefixcontagemresultado_contratadacod_Internalname = "DYNAMICFILTERSPREFIXCONTAGEMRESULTADO_CONTRATADACOD";
         lblDynamicfiltersfixedfiltercaptioncontagemresultado_contratadacod_Internalname = "DYNAMICFILTERSFIXEDFILTERCAPTIONCONTAGEMRESULTADO_CONTRATADACOD";
         lblDynamicfiltersmiddlecontagemresultado_contratadacod_Internalname = "DYNAMICFILTERSMIDDLECONTAGEMRESULTADO_CONTRATADACOD";
         dynavContagemresultado_contratadacod_Internalname = "vCONTAGEMRESULTADO_CONTRATADACOD";
         lblDynamicfiltersprefixcontagemresultado_servicogrupo_Internalname = "DYNAMICFILTERSPREFIXCONTAGEMRESULTADO_SERVICOGRUPO";
         lblDynamicfiltersfixedfiltercaptioncontagemresultado_servicogrupo_Internalname = "DYNAMICFILTERSFIXEDFILTERCAPTIONCONTAGEMRESULTADO_SERVICOGRUPO";
         lblDynamicfiltersmiddlecontagemresultado_servicogrupo_Internalname = "DYNAMICFILTERSMIDDLECONTAGEMRESULTADO_SERVICOGRUPO";
         dynavContagemresultado_servicogrupo_Internalname = "vCONTAGEMRESULTADO_SERVICOGRUPO";
         lblDynamicfiltersprefixcontagemresultado_servico_Internalname = "DYNAMICFILTERSPREFIXCONTAGEMRESULTADO_SERVICO";
         lblDynamicfiltersfixedfiltercaptioncontagemresultado_servico_Internalname = "DYNAMICFILTERSFIXEDFILTERCAPTIONCONTAGEMRESULTADO_SERVICO";
         lblDynamicfiltersmiddlecontagemresultado_servico_Internalname = "DYNAMICFILTERSMIDDLECONTAGEMRESULTADO_SERVICO";
         dynavContagemresultado_servico_Internalname = "vCONTAGEMRESULTADO_SERVICO";
         lblDynamicfiltersprefixcontagemresultado_cntadaosvinc_Internalname = "DYNAMICFILTERSPREFIXCONTAGEMRESULTADO_CNTADAOSVINC";
         lblDynamicfiltersfixedfiltercaptioncontagemresultado_cntadaosvinc_Internalname = "DYNAMICFILTERSFIXEDFILTERCAPTIONCONTAGEMRESULTADO_CNTADAOSVINC";
         lblDynamicfiltersmiddlecontagemresultado_cntadaosvinc_Internalname = "DYNAMICFILTERSMIDDLECONTAGEMRESULTADO_CNTADAOSVINC";
         dynavContagemresultado_cntadaosvinc_Internalname = "vCONTAGEMRESULTADO_CNTADAOSVINC";
         lblDynamicfiltersprefixcontagemresultado_sergrupovinc_Internalname = "DYNAMICFILTERSPREFIXCONTAGEMRESULTADO_SERGRUPOVINC";
         lblDynamicfiltersfixedfiltercaptioncontagemresultado_sergrupovinc_Internalname = "DYNAMICFILTERSFIXEDFILTERCAPTIONCONTAGEMRESULTADO_SERGRUPOVINC";
         lblDynamicfiltersmiddlecontagemresultado_sergrupovinc_Internalname = "DYNAMICFILTERSMIDDLECONTAGEMRESULTADO_SERGRUPOVINC";
         dynavContagemresultado_sergrupovinc_Internalname = "vCONTAGEMRESULTADO_SERGRUPOVINC";
         lblDynamicfiltersprefixcontagemresultado_codsrvvnc_Internalname = "DYNAMICFILTERSPREFIXCONTAGEMRESULTADO_CODSRVVNC";
         lblDynamicfiltersfixedfiltercaptioncontagemresultado_codsrvvnc_Internalname = "DYNAMICFILTERSFIXEDFILTERCAPTIONCONTAGEMRESULTADO_CODSRVVNC";
         lblDynamicfiltersmiddlecontagemresultado_codsrvvnc_Internalname = "DYNAMICFILTERSMIDDLECONTAGEMRESULTADO_CODSRVVNC";
         dynavContagemresultado_codsrvvnc_Internalname = "vCONTAGEMRESULTADO_CODSRVVNC";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavContagemresultado_demandafm1_Internalname = "vCONTAGEMRESULTADO_DEMANDAFM1";
         edtavContagemresultado_datadmn1_Internalname = "vCONTAGEMRESULTADO_DATADMN1";
         lblContagemresultado_datadmn_rangemiddletext_11_Internalname = "CONTAGEMRESULTADO_DATADMN_RANGEMIDDLETEXT_11";
         cellContagemresultado_datadmn_rangemiddletext_1_cell1_Internalname = "CONTAGEMRESULTADO_DATADMN_RANGEMIDDLETEXT_1_CELL1";
         edtavContagemresultado_datadmn_to1_Internalname = "vCONTAGEMRESULTADO_DATADMN_TO1";
         cellContagemresultado_datadmn_to_cell1_Internalname = "CONTAGEMRESULTADO_DATADMN_TO_CELL1";
         tblTablemergeddynamicfilterscontagemresultado_datadmn1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN1";
         edtavContagemresultado_dataentregareal1_Internalname = "vCONTAGEMRESULTADO_DATAENTREGAREAL1";
         cellContagemresultado_dataentregareal_cell1_Internalname = "CONTAGEMRESULTADO_DATAENTREGAREAL_CELL1";
         lblContagemresultado_dataentregareal_rangemiddletext_11_Internalname = "CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_11";
         cellContagemresultado_dataentregareal_rangemiddletext_1_cell1_Internalname = "CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_1_CELL1";
         edtavContagemresultado_dataentregareal_to1_Internalname = "vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1";
         cellContagemresultado_dataentregareal_to_cell1_Internalname = "CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL1";
         tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL1";
         cmbavContagemresultado_statusdmn1_Internalname = "vCONTAGEMRESULTADO_STATUSDMN1";
         cmbavContagemresultado_statuscnt1_Internalname = "vCONTAGEMRESULTADO_STATUSCNT1";
         edtavContagemresultado_descricao1_Internalname = "vCONTAGEMRESULTADO_DESCRICAO1";
         dynavContagemresultado_sistemacod1_Internalname = "vCONTAGEMRESULTADO_SISTEMACOD1";
         edtavContagemresultado_agrupador1_Internalname = "vCONTAGEMRESULTADO_AGRUPADOR1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavContagemresultado_demandafm2_Internalname = "vCONTAGEMRESULTADO_DEMANDAFM2";
         edtavContagemresultado_datadmn2_Internalname = "vCONTAGEMRESULTADO_DATADMN2";
         lblContagemresultado_datadmn_rangemiddletext_12_Internalname = "CONTAGEMRESULTADO_DATADMN_RANGEMIDDLETEXT_12";
         cellContagemresultado_datadmn_rangemiddletext_1_cell2_Internalname = "CONTAGEMRESULTADO_DATADMN_RANGEMIDDLETEXT_1_CELL2";
         edtavContagemresultado_datadmn_to2_Internalname = "vCONTAGEMRESULTADO_DATADMN_TO2";
         cellContagemresultado_datadmn_to_cell2_Internalname = "CONTAGEMRESULTADO_DATADMN_TO_CELL2";
         tblTablemergeddynamicfilterscontagemresultado_datadmn2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN2";
         edtavContagemresultado_dataentregareal2_Internalname = "vCONTAGEMRESULTADO_DATAENTREGAREAL2";
         cellContagemresultado_dataentregareal_cell2_Internalname = "CONTAGEMRESULTADO_DATAENTREGAREAL_CELL2";
         lblContagemresultado_dataentregareal_rangemiddletext_12_Internalname = "CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_12";
         cellContagemresultado_dataentregareal_rangemiddletext_1_cell2_Internalname = "CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_1_CELL2";
         edtavContagemresultado_dataentregareal_to2_Internalname = "vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2";
         cellContagemresultado_dataentregareal_to_cell2_Internalname = "CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL2";
         tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL2";
         cmbavContagemresultado_statusdmn2_Internalname = "vCONTAGEMRESULTADO_STATUSDMN2";
         cmbavContagemresultado_statuscnt2_Internalname = "vCONTAGEMRESULTADO_STATUSCNT2";
         edtavContagemresultado_descricao2_Internalname = "vCONTAGEMRESULTADO_DESCRICAO2";
         dynavContagemresultado_sistemacod2_Internalname = "vCONTAGEMRESULTADO_SISTEMACOD2";
         edtavContagemresultado_agrupador2_Internalname = "vCONTAGEMRESULTADO_AGRUPADOR2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavContagemresultado_demandafm3_Internalname = "vCONTAGEMRESULTADO_DEMANDAFM3";
         edtavContagemresultado_datadmn3_Internalname = "vCONTAGEMRESULTADO_DATADMN3";
         lblContagemresultado_datadmn_rangemiddletext_13_Internalname = "CONTAGEMRESULTADO_DATADMN_RANGEMIDDLETEXT_13";
         cellContagemresultado_datadmn_rangemiddletext_1_cell3_Internalname = "CONTAGEMRESULTADO_DATADMN_RANGEMIDDLETEXT_1_CELL3";
         edtavContagemresultado_datadmn_to3_Internalname = "vCONTAGEMRESULTADO_DATADMN_TO3";
         cellContagemresultado_datadmn_to_cell3_Internalname = "CONTAGEMRESULTADO_DATADMN_TO_CELL3";
         tblTablemergeddynamicfilterscontagemresultado_datadmn3_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN3";
         edtavContagemresultado_dataentregareal3_Internalname = "vCONTAGEMRESULTADO_DATAENTREGAREAL3";
         cellContagemresultado_dataentregareal_cell3_Internalname = "CONTAGEMRESULTADO_DATAENTREGAREAL_CELL3";
         lblContagemresultado_dataentregareal_rangemiddletext_13_Internalname = "CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_13";
         cellContagemresultado_dataentregareal_rangemiddletext_1_cell3_Internalname = "CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_1_CELL3";
         edtavContagemresultado_dataentregareal_to3_Internalname = "vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3";
         cellContagemresultado_dataentregareal_to_cell3_Internalname = "CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL3";
         tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL3";
         cmbavContagemresultado_statusdmn3_Internalname = "vCONTAGEMRESULTADO_STATUSDMN3";
         cmbavContagemresultado_statuscnt3_Internalname = "vCONTAGEMRESULTADO_STATUSCNT3";
         edtavContagemresultado_descricao3_Internalname = "vCONTAGEMRESULTADO_DESCRICAO3";
         dynavContagemresultado_sistemacod3_Internalname = "vCONTAGEMRESULTADO_SISTEMACOD3";
         edtavContagemresultado_agrupador3_Internalname = "vCONTAGEMRESULTADO_AGRUPADOR3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavDisplay_Internalname = "vDISPLAY";
         edtContagemResultado_Codigo_Internalname = "CONTAGEMRESULTADO_CODIGO";
         edtContagemResultado_DemandaFM_Internalname = "CONTAGEMRESULTADO_DEMANDAFM";
         edtContagemResultado_Demanda_Internalname = "CONTAGEMRESULTADO_DEMANDA";
         edtContagemResultado_DataDmn_Internalname = "CONTAGEMRESULTADO_DATADMN";
         edtContagemResultado_DataEntregaReal_Internalname = "CONTAGEMRESULTADO_DATAENTREGAREAL";
         edtContagemResultado_Descricao_Internalname = "CONTAGEMRESULTADO_DESCRICAO";
         edtContagemrResultado_SistemaSigla_Internalname = "CONTAGEMRRESULTADO_SISTEMASIGLA";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContagemrResultado_SistemaSigla_Jsonclick = "";
         edtContagemResultado_Descricao_Jsonclick = "";
         edtContagemResultado_DataEntregaReal_Jsonclick = "";
         edtContagemResultado_DataDmn_Jsonclick = "";
         edtContagemResultado_Demanda_Jsonclick = "";
         edtContagemResultado_DemandaFM_Jsonclick = "";
         edtContagemResultado_Codigo_Jsonclick = "";
         edtavContagemresultado_datadmn_to1_Jsonclick = "";
         edtavContagemresultado_datadmn1_Jsonclick = "";
         edtavContagemresultado_dataentregareal_to1_Jsonclick = "";
         cellContagemresultado_dataentregareal_to_cell1_Class = "";
         edtavContagemresultado_dataentregareal1_Jsonclick = "";
         cellContagemresultado_dataentregareal_cell1_Class = "";
         edtavContagemresultado_agrupador1_Jsonclick = "";
         dynavContagemresultado_sistemacod1_Jsonclick = "";
         edtavContagemresultado_descricao1_Jsonclick = "";
         cmbavContagemresultado_statuscnt1_Jsonclick = "";
         cmbavContagemresultado_statusdmn1_Jsonclick = "";
         edtavContagemresultado_demandafm1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavContagemresultado_datadmn_to2_Jsonclick = "";
         edtavContagemresultado_datadmn2_Jsonclick = "";
         edtavContagemresultado_dataentregareal_to2_Jsonclick = "";
         cellContagemresultado_dataentregareal_to_cell2_Class = "";
         edtavContagemresultado_dataentregareal2_Jsonclick = "";
         cellContagemresultado_dataentregareal_cell2_Class = "";
         edtavContagemresultado_agrupador2_Jsonclick = "";
         dynavContagemresultado_sistemacod2_Jsonclick = "";
         edtavContagemresultado_descricao2_Jsonclick = "";
         cmbavContagemresultado_statuscnt2_Jsonclick = "";
         cmbavContagemresultado_statusdmn2_Jsonclick = "";
         edtavContagemresultado_demandafm2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavContagemresultado_datadmn_to3_Jsonclick = "";
         edtavContagemresultado_datadmn3_Jsonclick = "";
         edtavContagemresultado_dataentregareal_to3_Jsonclick = "";
         cellContagemresultado_dataentregareal_to_cell3_Class = "";
         edtavContagemresultado_dataentregareal3_Jsonclick = "";
         cellContagemresultado_dataentregareal_cell3_Class = "";
         edtavContagemresultado_agrupador3_Jsonclick = "";
         dynavContagemresultado_sistemacod3_Jsonclick = "";
         edtavContagemresultado_descricao3_Jsonclick = "";
         cmbavContagemresultado_statuscnt3_Jsonclick = "";
         cmbavContagemresultado_statusdmn3_Jsonclick = "";
         edtavContagemresultado_demandafm3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         dynavContagemresultado_codsrvvnc_Jsonclick = "";
         dynavContagemresultado_sergrupovinc_Jsonclick = "";
         dynavContagemresultado_cntadaosvinc_Jsonclick = "";
         dynavContagemresultado_servico_Jsonclick = "";
         dynavContagemresultado_servicogrupo_Jsonclick = "";
         dynavContagemresultado_contratadacod_Jsonclick = "";
         edtavContagemresultado_osvinculada_Jsonclick = "";
         edtavContratada_areatrabalhocod_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtContagemResultado_DataDmn_Link = "";
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDisplay_Enabled = 1;
         subGrid_Class = "WorkWithBorder WorkWith";
         lblContagemresultado_dataentregareal_rangemiddletext_13_Class = "DataFilterDescription";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavContagemresultado_agrupador3_Visible = 1;
         dynavContagemresultado_sistemacod3.Visible = 1;
         edtavContagemresultado_descricao3_Visible = 1;
         cmbavContagemresultado_statuscnt3.Visible = 1;
         cmbavContagemresultado_statusdmn3.Visible = 1;
         tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible = 1;
         edtavContagemresultado_demandafm3_Visible = 1;
         lblContagemresultado_dataentregareal_rangemiddletext_12_Class = "DataFilterDescription";
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavContagemresultado_agrupador2_Visible = 1;
         dynavContagemresultado_sistemacod2.Visible = 1;
         edtavContagemresultado_descricao2_Visible = 1;
         cmbavContagemresultado_statuscnt2.Visible = 1;
         cmbavContagemresultado_statusdmn2.Visible = 1;
         tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible = 1;
         edtavContagemresultado_demandafm2_Visible = 1;
         lblContagemresultado_dataentregareal_rangemiddletext_11_Class = "DataFilterDescription";
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavContagemresultado_agrupador1_Visible = 1;
         dynavContagemresultado_sistemacod1.Visible = 1;
         edtavContagemresultado_descricao1_Visible = 1;
         cmbavContagemresultado_statuscnt1.Visible = 1;
         cmbavContagemresultado_statusdmn1.Visible = 1;
         tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible = 1;
         edtavContagemresultado_demandafm1_Visible = 1;
         dynavContagemresultado_cntadaosvinc.Enabled = 1;
         dynavContagemresultado_contratadacod.Enabled = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Relat�rio de Compara��o entre Demandas";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public void Validv_Contagemresultado_contratadacod( GXCombobox dynGX_Parm1 ,
                                                          GXCombobox dynGX_Parm2 ,
                                                          GXCombobox dynGX_Parm3 )
      {
         dynavContagemresultado_contratadacod = dynGX_Parm1;
         AV66ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( dynavContagemresultado_contratadacod.CurrentValue, "."));
         dynavContagemresultado_servicogrupo = dynGX_Parm2;
         AV67ContagemResultado_ServicoGrupo = (int)(NumberUtil.Val( dynavContagemresultado_servicogrupo.CurrentValue, "."));
         dynavContagemresultado_servico = dynGX_Parm3;
         AV68ContagemResultado_Servico = (int)(NumberUtil.Val( dynavContagemresultado_servico.CurrentValue, "."));
         GXVvCONTAGEMRESULTADO_SERVICOGRUPO_htmlTB2( AV66ContagemResultado_ContratadaCod) ;
         GXVvCONTAGEMRESULTADO_SERVICO_htmlTB2( AV66ContagemResultado_ContratadaCod, AV67ContagemResultado_ServicoGrupo) ;
         dynload_actions( ) ;
         if ( dynavContagemresultado_servicogrupo.ItemCount > 0 )
         {
            AV67ContagemResultado_ServicoGrupo = (int)(NumberUtil.Val( dynavContagemresultado_servicogrupo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV67ContagemResultado_ServicoGrupo), 6, 0))), "."));
         }
         dynavContagemresultado_servicogrupo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV67ContagemResultado_ServicoGrupo), 6, 0));
         isValidOutput.Add(dynavContagemresultado_servicogrupo);
         if ( dynavContagemresultado_servico.ItemCount > 0 )
         {
            AV68ContagemResultado_Servico = (int)(NumberUtil.Val( dynavContagemresultado_servico.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV68ContagemResultado_Servico), 6, 0))), "."));
         }
         dynavContagemresultado_servico.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV68ContagemResultado_Servico), 6, 0));
         isValidOutput.Add(dynavContagemresultado_servico);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Validv_Contagemresultado_servicogrupo( GXCombobox dynGX_Parm1 ,
                                                         GXCombobox dynGX_Parm2 ,
                                                         GXCombobox dynGX_Parm3 )
      {
         dynavContagemresultado_contratadacod = dynGX_Parm1;
         AV66ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( dynavContagemresultado_contratadacod.CurrentValue, "."));
         dynavContagemresultado_servicogrupo = dynGX_Parm2;
         AV67ContagemResultado_ServicoGrupo = (int)(NumberUtil.Val( dynavContagemresultado_servicogrupo.CurrentValue, "."));
         dynavContagemresultado_servico = dynGX_Parm3;
         AV68ContagemResultado_Servico = (int)(NumberUtil.Val( dynavContagemresultado_servico.CurrentValue, "."));
         GXVvCONTAGEMRESULTADO_SERVICO_htmlTB2( AV66ContagemResultado_ContratadaCod, AV67ContagemResultado_ServicoGrupo) ;
         dynload_actions( ) ;
         if ( dynavContagemresultado_servico.ItemCount > 0 )
         {
            AV68ContagemResultado_Servico = (int)(NumberUtil.Val( dynavContagemresultado_servico.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV68ContagemResultado_Servico), 6, 0))), "."));
         }
         dynavContagemresultado_servico.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV68ContagemResultado_Servico), 6, 0));
         isValidOutput.Add(dynavContagemresultado_servico);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Validv_Contagemresultado_cntadaosvinc( GXCombobox dynGX_Parm1 ,
                                                         GXCombobox dynGX_Parm2 ,
                                                         GXCombobox dynGX_Parm3 )
      {
         dynavContagemresultado_cntadaosvinc = dynGX_Parm1;
         AV69ContagemResultado_CntadaOsVinc = (int)(NumberUtil.Val( dynavContagemresultado_cntadaosvinc.CurrentValue, "."));
         dynavContagemresultado_sergrupovinc = dynGX_Parm2;
         AV71ContagemResultado_SerGrupoVinc = (int)(NumberUtil.Val( dynavContagemresultado_sergrupovinc.CurrentValue, "."));
         dynavContagemresultado_codsrvvnc = dynGX_Parm3;
         AV70ContagemResultado_CodSrvVnc = (int)(NumberUtil.Val( dynavContagemresultado_codsrvvnc.CurrentValue, "."));
         GXVvCONTAGEMRESULTADO_SERGRUPOVINC_htmlTB2( AV69ContagemResultado_CntadaOsVinc) ;
         GXVvCONTAGEMRESULTADO_CODSRVVNC_htmlTB2( AV69ContagemResultado_CntadaOsVinc, AV71ContagemResultado_SerGrupoVinc) ;
         dynload_actions( ) ;
         if ( dynavContagemresultado_sergrupovinc.ItemCount > 0 )
         {
            AV71ContagemResultado_SerGrupoVinc = (int)(NumberUtil.Val( dynavContagemresultado_sergrupovinc.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV71ContagemResultado_SerGrupoVinc), 6, 0))), "."));
         }
         dynavContagemresultado_sergrupovinc.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV71ContagemResultado_SerGrupoVinc), 6, 0));
         isValidOutput.Add(dynavContagemresultado_sergrupovinc);
         if ( dynavContagemresultado_codsrvvnc.ItemCount > 0 )
         {
            AV70ContagemResultado_CodSrvVnc = (int)(NumberUtil.Val( dynavContagemresultado_codsrvvnc.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV70ContagemResultado_CodSrvVnc), 6, 0))), "."));
         }
         dynavContagemresultado_codsrvvnc.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV70ContagemResultado_CodSrvVnc), 6, 0));
         isValidOutput.Add(dynavContagemresultado_codsrvvnc);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Validv_Contagemresultado_sergrupovinc( GXCombobox dynGX_Parm1 ,
                                                         GXCombobox dynGX_Parm2 ,
                                                         GXCombobox dynGX_Parm3 )
      {
         dynavContagemresultado_cntadaosvinc = dynGX_Parm1;
         AV69ContagemResultado_CntadaOsVinc = (int)(NumberUtil.Val( dynavContagemresultado_cntadaosvinc.CurrentValue, "."));
         dynavContagemresultado_sergrupovinc = dynGX_Parm2;
         AV71ContagemResultado_SerGrupoVinc = (int)(NumberUtil.Val( dynavContagemresultado_sergrupovinc.CurrentValue, "."));
         dynavContagemresultado_codsrvvnc = dynGX_Parm3;
         AV70ContagemResultado_CodSrvVnc = (int)(NumberUtil.Val( dynavContagemresultado_codsrvvnc.CurrentValue, "."));
         GXVvCONTAGEMRESULTADO_CODSRVVNC_htmlTB2( AV69ContagemResultado_CntadaOsVinc, AV71ContagemResultado_SerGrupoVinc) ;
         dynload_actions( ) ;
         if ( dynavContagemresultado_codsrvvnc.ItemCount > 0 )
         {
            AV70ContagemResultado_CodSrvVnc = (int)(NumberUtil.Val( dynavContagemresultado_codsrvvnc.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV70ContagemResultado_CodSrvVnc), 6, 0))), "."));
         }
         dynavContagemresultado_codsrvvnc.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV70ContagemResultado_CodSrvVnc), 6, 0));
         isValidOutput.Add(dynavContagemresultado_codsrvvnc);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV26DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV39DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV13Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV65ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV17ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV18ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV19ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV22ContagemResultado_StatusCnt1',fld:'vCONTAGEMRESULTADO_STATUSCNT1',pic:'Z9',nv:0},{av:'AV23ContagemResultado_Descricao1',fld:'vCONTAGEMRESULTADO_DESCRICAO1',pic:'',nv:''},{av:'AV24ContagemResultado_SistemaCod1',fld:'vCONTAGEMRESULTADO_SISTEMACOD1',pic:'ZZZZZ9',nv:0},{av:'AV25ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV28DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV30ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV31ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV32ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV35ContagemResultado_StatusCnt2',fld:'vCONTAGEMRESULTADO_STATUSCNT2',pic:'Z9',nv:0},{av:'AV36ContagemResultado_Descricao2',fld:'vCONTAGEMRESULTADO_DESCRICAO2',pic:'',nv:''},{av:'AV37ContagemResultado_SistemaCod2',fld:'vCONTAGEMRESULTADO_SISTEMACOD2',pic:'ZZZZZ9',nv:0},{av:'AV38ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV41DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV43ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV44ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV45ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV46ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV47ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV48ContagemResultado_StatusCnt3',fld:'vCONTAGEMRESULTADO_STATUSCNT3',pic:'Z9',nv:0},{av:'AV49ContagemResultado_Descricao3',fld:'vCONTAGEMRESULTADO_DESCRICAO3',pic:'',nv:''},{av:'AV50ContagemResultado_SistemaCod3',fld:'vCONTAGEMRESULTADO_SISTEMACOD3',pic:'ZZZZZ9',nv:0},{av:'AV51ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV66ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV67ContagemResultado_ServicoGrupo',fld:'vCONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'AV68ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV69ContagemResultado_CntadaOsVinc',fld:'vCONTAGEMRESULTADO_CNTADAOSVINC',pic:'ZZZZZ9',nv:0},{av:'AV71ContagemResultado_SerGrupoVinc',fld:'vCONTAGEMRESULTADO_SERGRUPOVINC',pic:'ZZZZZ9',nv:0},{av:'AV70ContagemResultado_CodSrvVnc',fld:'vCONTAGEMRESULTADO_CODSRVVNC',pic:'ZZZZZ9',nv:0},{av:'AV122Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV53DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV52DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV28DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV62GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV63GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11TB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV67ContagemResultado_ServicoGrupo',fld:'vCONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'AV68ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV69ContagemResultado_CntadaOsVinc',fld:'vCONTAGEMRESULTADO_CNTADAOSVINC',pic:'ZZZZZ9',nv:0},{av:'AV71ContagemResultado_SerGrupoVinc',fld:'vCONTAGEMRESULTADO_SERGRUPOVINC',pic:'ZZZZZ9',nv:0},{av:'AV70ContagemResultado_CodSrvVnc',fld:'vCONTAGEMRESULTADO_CODSRVVNC',pic:'ZZZZZ9',nv:0},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV17ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV18ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV19ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV22ContagemResultado_StatusCnt1',fld:'vCONTAGEMRESULTADO_STATUSCNT1',pic:'Z9',nv:0},{av:'AV23ContagemResultado_Descricao1',fld:'vCONTAGEMRESULTADO_DESCRICAO1',pic:'',nv:''},{av:'AV24ContagemResultado_SistemaCod1',fld:'vCONTAGEMRESULTADO_SISTEMACOD1',pic:'ZZZZZ9',nv:0},{av:'AV25ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV28DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV30ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV31ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV32ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV35ContagemResultado_StatusCnt2',fld:'vCONTAGEMRESULTADO_STATUSCNT2',pic:'Z9',nv:0},{av:'AV36ContagemResultado_Descricao2',fld:'vCONTAGEMRESULTADO_DESCRICAO2',pic:'',nv:''},{av:'AV37ContagemResultado_SistemaCod2',fld:'vCONTAGEMRESULTADO_SISTEMACOD2',pic:'ZZZZZ9',nv:0},{av:'AV38ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV40DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV43ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV44ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV45ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV46ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV47ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV48ContagemResultado_StatusCnt3',fld:'vCONTAGEMRESULTADO_STATUSCNT3',pic:'Z9',nv:0},{av:'AV49ContagemResultado_Descricao3',fld:'vCONTAGEMRESULTADO_DESCRICAO3',pic:'',nv:''},{av:'AV50ContagemResultado_SistemaCod3',fld:'vCONTAGEMRESULTADO_SISTEMACOD3',pic:'ZZZZZ9',nv:0},{av:'AV51ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV26DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV39DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV13Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV65ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV66ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV122Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV53DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV52DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E25TB2',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV64Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Enabled',ctrl:'vDISPLAY',prop:'Enabled'},{av:'edtContagemResultado_DataDmn_Link',ctrl:'CONTAGEMRESULTADO_DATADMN',prop:'Link'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E15TB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV67ContagemResultado_ServicoGrupo',fld:'vCONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'AV68ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV69ContagemResultado_CntadaOsVinc',fld:'vCONTAGEMRESULTADO_CNTADAOSVINC',pic:'ZZZZZ9',nv:0},{av:'AV71ContagemResultado_SerGrupoVinc',fld:'vCONTAGEMRESULTADO_SERGRUPOVINC',pic:'ZZZZZ9',nv:0},{av:'AV70ContagemResultado_CodSrvVnc',fld:'vCONTAGEMRESULTADO_CODSRVVNC',pic:'ZZZZZ9',nv:0},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV17ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV18ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV19ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV22ContagemResultado_StatusCnt1',fld:'vCONTAGEMRESULTADO_STATUSCNT1',pic:'Z9',nv:0},{av:'AV23ContagemResultado_Descricao1',fld:'vCONTAGEMRESULTADO_DESCRICAO1',pic:'',nv:''},{av:'AV24ContagemResultado_SistemaCod1',fld:'vCONTAGEMRESULTADO_SISTEMACOD1',pic:'ZZZZZ9',nv:0},{av:'AV25ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV28DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV30ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV31ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV32ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV35ContagemResultado_StatusCnt2',fld:'vCONTAGEMRESULTADO_STATUSCNT2',pic:'Z9',nv:0},{av:'AV36ContagemResultado_Descricao2',fld:'vCONTAGEMRESULTADO_DESCRICAO2',pic:'',nv:''},{av:'AV37ContagemResultado_SistemaCod2',fld:'vCONTAGEMRESULTADO_SISTEMACOD2',pic:'ZZZZZ9',nv:0},{av:'AV38ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV40DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV43ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV44ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV45ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV46ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV47ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV48ContagemResultado_StatusCnt3',fld:'vCONTAGEMRESULTADO_STATUSCNT3',pic:'Z9',nv:0},{av:'AV49ContagemResultado_Descricao3',fld:'vCONTAGEMRESULTADO_DESCRICAO3',pic:'',nv:''},{av:'AV50ContagemResultado_SistemaCod3',fld:'vCONTAGEMRESULTADO_SISTEMACOD3',pic:'ZZZZZ9',nv:0},{av:'AV51ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV26DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV39DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV13Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV65ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV66ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV122Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV53DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV52DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E12TB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV67ContagemResultado_ServicoGrupo',fld:'vCONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'AV68ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV69ContagemResultado_CntadaOsVinc',fld:'vCONTAGEMRESULTADO_CNTADAOSVINC',pic:'ZZZZZ9',nv:0},{av:'AV71ContagemResultado_SerGrupoVinc',fld:'vCONTAGEMRESULTADO_SERGRUPOVINC',pic:'ZZZZZ9',nv:0},{av:'AV70ContagemResultado_CodSrvVnc',fld:'vCONTAGEMRESULTADO_CODSRVVNC',pic:'ZZZZZ9',nv:0},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV17ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV18ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV19ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV22ContagemResultado_StatusCnt1',fld:'vCONTAGEMRESULTADO_STATUSCNT1',pic:'Z9',nv:0},{av:'AV23ContagemResultado_Descricao1',fld:'vCONTAGEMRESULTADO_DESCRICAO1',pic:'',nv:''},{av:'AV24ContagemResultado_SistemaCod1',fld:'vCONTAGEMRESULTADO_SISTEMACOD1',pic:'ZZZZZ9',nv:0},{av:'AV25ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV28DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV30ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV31ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV32ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV35ContagemResultado_StatusCnt2',fld:'vCONTAGEMRESULTADO_STATUSCNT2',pic:'Z9',nv:0},{av:'AV36ContagemResultado_Descricao2',fld:'vCONTAGEMRESULTADO_DESCRICAO2',pic:'',nv:''},{av:'AV37ContagemResultado_SistemaCod2',fld:'vCONTAGEMRESULTADO_SISTEMACOD2',pic:'ZZZZZ9',nv:0},{av:'AV38ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV40DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV43ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV44ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV45ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV46ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV47ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV48ContagemResultado_StatusCnt3',fld:'vCONTAGEMRESULTADO_STATUSCNT3',pic:'Z9',nv:0},{av:'AV49ContagemResultado_Descricao3',fld:'vCONTAGEMRESULTADO_DESCRICAO3',pic:'',nv:''},{av:'AV50ContagemResultado_SistemaCod3',fld:'vCONTAGEMRESULTADO_SISTEMACOD3',pic:'ZZZZZ9',nv:0},{av:'AV51ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV26DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV39DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV13Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV65ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV66ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV122Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV53DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV52DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV52DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV53DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV28DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV39DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV25ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV24ContagemResultado_SistemaCod1',fld:'vCONTAGEMRESULTADO_SISTEMACOD1',pic:'ZZZZZ9',nv:0},{av:'AV23ContagemResultado_Descricao1',fld:'vCONTAGEMRESULTADO_DESCRICAO1',pic:'',nv:''},{av:'AV22ContagemResultado_StatusCnt1',fld:'vCONTAGEMRESULTADO_STATUSCNT1',pic:'Z9',nv:0},{av:'AV21ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV19ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV18ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV16ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV38ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV37ContagemResultado_SistemaCod2',fld:'vCONTAGEMRESULTADO_SISTEMACOD2',pic:'ZZZZZ9',nv:0},{av:'AV36ContagemResultado_Descricao2',fld:'vCONTAGEMRESULTADO_DESCRICAO2',pic:'',nv:''},{av:'AV35ContagemResultado_StatusCnt2',fld:'vCONTAGEMRESULTADO_STATUSCNT2',pic:'Z9',nv:0},{av:'AV34ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV32ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV30ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV31ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV51ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV50ContagemResultado_SistemaCod3',fld:'vCONTAGEMRESULTADO_SISTEMACOD3',pic:'ZZZZZ9',nv:0},{av:'AV49ContagemResultado_Descricao3',fld:'vCONTAGEMRESULTADO_DESCRICAO3',pic:'',nv:''},{av:'AV48ContagemResultado_StatusCnt3',fld:'vCONTAGEMRESULTADO_STATUSCNT3',pic:'Z9',nv:0},{av:'AV47ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV45ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV46ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV44ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'edtavContagemresultado_demandafm2_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL2',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn2'},{av:'cmbavContagemresultado_statuscnt2'},{av:'edtavContagemresultado_descricao2_Visible',ctrl:'vCONTAGEMRESULTADO_DESCRICAO2',prop:'Visible'},{av:'dynavContagemresultado_sistemacod2'},{av:'edtavContagemresultado_agrupador2_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagemresultado_demandafm3_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL3',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn3'},{av:'cmbavContagemresultado_statuscnt3'},{av:'edtavContagemresultado_descricao3_Visible',ctrl:'vCONTAGEMRESULTADO_DESCRICAO3',prop:'Visible'},{av:'dynavContagemresultado_sistemacod3'},{av:'edtavContagemresultado_agrupador3_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContagemresultado_demandafm1_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL1',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn1'},{av:'cmbavContagemresultado_statuscnt1'},{av:'edtavContagemresultado_descricao1_Visible',ctrl:'vCONTAGEMRESULTADO_DESCRICAO1',prop:'Visible'},{av:'dynavContagemresultado_sistemacod1'},{av:'edtavContagemresultado_agrupador1_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'cellContagemresultado_dataentregareal_cell2_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL2',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell2_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL2',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_12_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_12',prop:'Class'},{av:'cellContagemresultado_dataentregareal_cell3_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL3',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell3_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL3',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_13_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_13',prop:'Class'},{av:'cellContagemresultado_dataentregareal_cell1_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL1',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell1_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL1',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_11_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_11',prop:'Class'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E16TB2',iparms:[{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavContagemresultado_demandafm1_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL1',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn1'},{av:'cmbavContagemresultado_statuscnt1'},{av:'edtavContagemresultado_descricao1_Visible',ctrl:'vCONTAGEMRESULTADO_DESCRICAO1',prop:'Visible'},{av:'dynavContagemresultado_sistemacod1'},{av:'edtavContagemresultado_agrupador1_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'cellContagemresultado_dataentregareal_cell1_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL1',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell1_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL1',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_11_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_11',prop:'Class'},{av:'AV19ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''}]}");
         setEventMetadata("VDYNAMICFILTERSOPERATOR1.CLICK","{handler:'E17TB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV67ContagemResultado_ServicoGrupo',fld:'vCONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'AV68ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV69ContagemResultado_CntadaOsVinc',fld:'vCONTAGEMRESULTADO_CNTADAOSVINC',pic:'ZZZZZ9',nv:0},{av:'AV71ContagemResultado_SerGrupoVinc',fld:'vCONTAGEMRESULTADO_SERGRUPOVINC',pic:'ZZZZZ9',nv:0},{av:'AV70ContagemResultado_CodSrvVnc',fld:'vCONTAGEMRESULTADO_CODSRVVNC',pic:'ZZZZZ9',nv:0},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV17ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV18ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV19ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV22ContagemResultado_StatusCnt1',fld:'vCONTAGEMRESULTADO_STATUSCNT1',pic:'Z9',nv:0},{av:'AV23ContagemResultado_Descricao1',fld:'vCONTAGEMRESULTADO_DESCRICAO1',pic:'',nv:''},{av:'AV24ContagemResultado_SistemaCod1',fld:'vCONTAGEMRESULTADO_SISTEMACOD1',pic:'ZZZZZ9',nv:0},{av:'AV25ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV28DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV30ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV31ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV32ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV35ContagemResultado_StatusCnt2',fld:'vCONTAGEMRESULTADO_STATUSCNT2',pic:'Z9',nv:0},{av:'AV36ContagemResultado_Descricao2',fld:'vCONTAGEMRESULTADO_DESCRICAO2',pic:'',nv:''},{av:'AV37ContagemResultado_SistemaCod2',fld:'vCONTAGEMRESULTADO_SISTEMACOD2',pic:'ZZZZZ9',nv:0},{av:'AV38ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV40DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV43ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV44ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV45ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV46ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV47ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV48ContagemResultado_StatusCnt3',fld:'vCONTAGEMRESULTADO_STATUSCNT3',pic:'Z9',nv:0},{av:'AV49ContagemResultado_Descricao3',fld:'vCONTAGEMRESULTADO_DESCRICAO3',pic:'',nv:''},{av:'AV50ContagemResultado_SistemaCod3',fld:'vCONTAGEMRESULTADO_SISTEMACOD3',pic:'ZZZZZ9',nv:0},{av:'AV51ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV26DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV39DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV13Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV65ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV66ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV122Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV53DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV52DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV19ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'cellContagemresultado_dataentregareal_cell1_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL1',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell1_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL1',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_11_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_11',prop:'Class'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E18TB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV67ContagemResultado_ServicoGrupo',fld:'vCONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'AV68ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV69ContagemResultado_CntadaOsVinc',fld:'vCONTAGEMRESULTADO_CNTADAOSVINC',pic:'ZZZZZ9',nv:0},{av:'AV71ContagemResultado_SerGrupoVinc',fld:'vCONTAGEMRESULTADO_SERGRUPOVINC',pic:'ZZZZZ9',nv:0},{av:'AV70ContagemResultado_CodSrvVnc',fld:'vCONTAGEMRESULTADO_CODSRVVNC',pic:'ZZZZZ9',nv:0},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV17ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV18ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV19ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV22ContagemResultado_StatusCnt1',fld:'vCONTAGEMRESULTADO_STATUSCNT1',pic:'Z9',nv:0},{av:'AV23ContagemResultado_Descricao1',fld:'vCONTAGEMRESULTADO_DESCRICAO1',pic:'',nv:''},{av:'AV24ContagemResultado_SistemaCod1',fld:'vCONTAGEMRESULTADO_SISTEMACOD1',pic:'ZZZZZ9',nv:0},{av:'AV25ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV28DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV30ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV31ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV32ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV35ContagemResultado_StatusCnt2',fld:'vCONTAGEMRESULTADO_STATUSCNT2',pic:'Z9',nv:0},{av:'AV36ContagemResultado_Descricao2',fld:'vCONTAGEMRESULTADO_DESCRICAO2',pic:'',nv:''},{av:'AV37ContagemResultado_SistemaCod2',fld:'vCONTAGEMRESULTADO_SISTEMACOD2',pic:'ZZZZZ9',nv:0},{av:'AV38ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV40DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV43ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV44ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV45ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV46ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV47ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV48ContagemResultado_StatusCnt3',fld:'vCONTAGEMRESULTADO_STATUSCNT3',pic:'Z9',nv:0},{av:'AV49ContagemResultado_Descricao3',fld:'vCONTAGEMRESULTADO_DESCRICAO3',pic:'',nv:''},{av:'AV50ContagemResultado_SistemaCod3',fld:'vCONTAGEMRESULTADO_SISTEMACOD3',pic:'ZZZZZ9',nv:0},{av:'AV51ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV26DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV39DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV13Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV65ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV66ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV122Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV53DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV52DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV39DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E13TB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV67ContagemResultado_ServicoGrupo',fld:'vCONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'AV68ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV69ContagemResultado_CntadaOsVinc',fld:'vCONTAGEMRESULTADO_CNTADAOSVINC',pic:'ZZZZZ9',nv:0},{av:'AV71ContagemResultado_SerGrupoVinc',fld:'vCONTAGEMRESULTADO_SERGRUPOVINC',pic:'ZZZZZ9',nv:0},{av:'AV70ContagemResultado_CodSrvVnc',fld:'vCONTAGEMRESULTADO_CODSRVVNC',pic:'ZZZZZ9',nv:0},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV17ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV18ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV19ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV22ContagemResultado_StatusCnt1',fld:'vCONTAGEMRESULTADO_STATUSCNT1',pic:'Z9',nv:0},{av:'AV23ContagemResultado_Descricao1',fld:'vCONTAGEMRESULTADO_DESCRICAO1',pic:'',nv:''},{av:'AV24ContagemResultado_SistemaCod1',fld:'vCONTAGEMRESULTADO_SISTEMACOD1',pic:'ZZZZZ9',nv:0},{av:'AV25ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV28DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV30ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV31ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV32ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV35ContagemResultado_StatusCnt2',fld:'vCONTAGEMRESULTADO_STATUSCNT2',pic:'Z9',nv:0},{av:'AV36ContagemResultado_Descricao2',fld:'vCONTAGEMRESULTADO_DESCRICAO2',pic:'',nv:''},{av:'AV37ContagemResultado_SistemaCod2',fld:'vCONTAGEMRESULTADO_SISTEMACOD2',pic:'ZZZZZ9',nv:0},{av:'AV38ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV40DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV43ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV44ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV45ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV46ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV47ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV48ContagemResultado_StatusCnt3',fld:'vCONTAGEMRESULTADO_STATUSCNT3',pic:'Z9',nv:0},{av:'AV49ContagemResultado_Descricao3',fld:'vCONTAGEMRESULTADO_DESCRICAO3',pic:'',nv:''},{av:'AV50ContagemResultado_SistemaCod3',fld:'vCONTAGEMRESULTADO_SISTEMACOD3',pic:'ZZZZZ9',nv:0},{av:'AV51ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV26DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV39DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV13Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV65ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV66ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV122Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV53DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV52DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV52DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV28DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV39DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV25ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV24ContagemResultado_SistemaCod1',fld:'vCONTAGEMRESULTADO_SISTEMACOD1',pic:'ZZZZZ9',nv:0},{av:'AV23ContagemResultado_Descricao1',fld:'vCONTAGEMRESULTADO_DESCRICAO1',pic:'',nv:''},{av:'AV22ContagemResultado_StatusCnt1',fld:'vCONTAGEMRESULTADO_STATUSCNT1',pic:'Z9',nv:0},{av:'AV21ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV19ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV18ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV16ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV38ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV37ContagemResultado_SistemaCod2',fld:'vCONTAGEMRESULTADO_SISTEMACOD2',pic:'ZZZZZ9',nv:0},{av:'AV36ContagemResultado_Descricao2',fld:'vCONTAGEMRESULTADO_DESCRICAO2',pic:'',nv:''},{av:'AV35ContagemResultado_StatusCnt2',fld:'vCONTAGEMRESULTADO_STATUSCNT2',pic:'Z9',nv:0},{av:'AV34ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV32ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV30ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV31ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV51ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV50ContagemResultado_SistemaCod3',fld:'vCONTAGEMRESULTADO_SISTEMACOD3',pic:'ZZZZZ9',nv:0},{av:'AV49ContagemResultado_Descricao3',fld:'vCONTAGEMRESULTADO_DESCRICAO3',pic:'',nv:''},{av:'AV48ContagemResultado_StatusCnt3',fld:'vCONTAGEMRESULTADO_STATUSCNT3',pic:'Z9',nv:0},{av:'AV47ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV45ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV46ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV44ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'edtavContagemresultado_demandafm2_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL2',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn2'},{av:'cmbavContagemresultado_statuscnt2'},{av:'edtavContagemresultado_descricao2_Visible',ctrl:'vCONTAGEMRESULTADO_DESCRICAO2',prop:'Visible'},{av:'dynavContagemresultado_sistemacod2'},{av:'edtavContagemresultado_agrupador2_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagemresultado_demandafm3_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL3',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn3'},{av:'cmbavContagemresultado_statuscnt3'},{av:'edtavContagemresultado_descricao3_Visible',ctrl:'vCONTAGEMRESULTADO_DESCRICAO3',prop:'Visible'},{av:'dynavContagemresultado_sistemacod3'},{av:'edtavContagemresultado_agrupador3_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContagemresultado_demandafm1_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL1',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn1'},{av:'cmbavContagemresultado_statuscnt1'},{av:'edtavContagemresultado_descricao1_Visible',ctrl:'vCONTAGEMRESULTADO_DESCRICAO1',prop:'Visible'},{av:'dynavContagemresultado_sistemacod1'},{av:'edtavContagemresultado_agrupador1_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'cellContagemresultado_dataentregareal_cell2_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL2',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell2_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL2',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_12_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_12',prop:'Class'},{av:'cellContagemresultado_dataentregareal_cell3_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL3',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell3_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL3',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_13_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_13',prop:'Class'},{av:'cellContagemresultado_dataentregareal_cell1_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL1',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell1_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL1',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_11_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_11',prop:'Class'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E19TB2',iparms:[{av:'AV27DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV28DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV28DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavContagemresultado_demandafm2_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL2',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn2'},{av:'cmbavContagemresultado_statuscnt2'},{av:'edtavContagemresultado_descricao2_Visible',ctrl:'vCONTAGEMRESULTADO_DESCRICAO2',prop:'Visible'},{av:'dynavContagemresultado_sistemacod2'},{av:'edtavContagemresultado_agrupador2_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cellContagemresultado_dataentregareal_cell2_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL2',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell2_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL2',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_12_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_12',prop:'Class'},{av:'AV32ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''}]}");
         setEventMetadata("VDYNAMICFILTERSOPERATOR2.CLICK","{handler:'E20TB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV67ContagemResultado_ServicoGrupo',fld:'vCONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'AV68ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV69ContagemResultado_CntadaOsVinc',fld:'vCONTAGEMRESULTADO_CNTADAOSVINC',pic:'ZZZZZ9',nv:0},{av:'AV71ContagemResultado_SerGrupoVinc',fld:'vCONTAGEMRESULTADO_SERGRUPOVINC',pic:'ZZZZZ9',nv:0},{av:'AV70ContagemResultado_CodSrvVnc',fld:'vCONTAGEMRESULTADO_CODSRVVNC',pic:'ZZZZZ9',nv:0},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV17ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV18ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV19ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV22ContagemResultado_StatusCnt1',fld:'vCONTAGEMRESULTADO_STATUSCNT1',pic:'Z9',nv:0},{av:'AV23ContagemResultado_Descricao1',fld:'vCONTAGEMRESULTADO_DESCRICAO1',pic:'',nv:''},{av:'AV24ContagemResultado_SistemaCod1',fld:'vCONTAGEMRESULTADO_SISTEMACOD1',pic:'ZZZZZ9',nv:0},{av:'AV25ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV28DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV30ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV31ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV32ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV35ContagemResultado_StatusCnt2',fld:'vCONTAGEMRESULTADO_STATUSCNT2',pic:'Z9',nv:0},{av:'AV36ContagemResultado_Descricao2',fld:'vCONTAGEMRESULTADO_DESCRICAO2',pic:'',nv:''},{av:'AV37ContagemResultado_SistemaCod2',fld:'vCONTAGEMRESULTADO_SISTEMACOD2',pic:'ZZZZZ9',nv:0},{av:'AV38ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV40DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV43ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV44ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV45ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV46ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV47ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV48ContagemResultado_StatusCnt3',fld:'vCONTAGEMRESULTADO_STATUSCNT3',pic:'Z9',nv:0},{av:'AV49ContagemResultado_Descricao3',fld:'vCONTAGEMRESULTADO_DESCRICAO3',pic:'',nv:''},{av:'AV50ContagemResultado_SistemaCod3',fld:'vCONTAGEMRESULTADO_SISTEMACOD3',pic:'ZZZZZ9',nv:0},{av:'AV51ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV26DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV39DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV13Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV65ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV66ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV122Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV53DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV52DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV32ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'cellContagemresultado_dataentregareal_cell2_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL2',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell2_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL2',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_12_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_12',prop:'Class'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E14TB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV67ContagemResultado_ServicoGrupo',fld:'vCONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'AV68ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV69ContagemResultado_CntadaOsVinc',fld:'vCONTAGEMRESULTADO_CNTADAOSVINC',pic:'ZZZZZ9',nv:0},{av:'AV71ContagemResultado_SerGrupoVinc',fld:'vCONTAGEMRESULTADO_SERGRUPOVINC',pic:'ZZZZZ9',nv:0},{av:'AV70ContagemResultado_CodSrvVnc',fld:'vCONTAGEMRESULTADO_CODSRVVNC',pic:'ZZZZZ9',nv:0},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV17ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV18ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV19ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV22ContagemResultado_StatusCnt1',fld:'vCONTAGEMRESULTADO_STATUSCNT1',pic:'Z9',nv:0},{av:'AV23ContagemResultado_Descricao1',fld:'vCONTAGEMRESULTADO_DESCRICAO1',pic:'',nv:''},{av:'AV24ContagemResultado_SistemaCod1',fld:'vCONTAGEMRESULTADO_SISTEMACOD1',pic:'ZZZZZ9',nv:0},{av:'AV25ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV28DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV30ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV31ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV32ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV35ContagemResultado_StatusCnt2',fld:'vCONTAGEMRESULTADO_STATUSCNT2',pic:'Z9',nv:0},{av:'AV36ContagemResultado_Descricao2',fld:'vCONTAGEMRESULTADO_DESCRICAO2',pic:'',nv:''},{av:'AV37ContagemResultado_SistemaCod2',fld:'vCONTAGEMRESULTADO_SISTEMACOD2',pic:'ZZZZZ9',nv:0},{av:'AV38ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV40DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV43ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV44ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV45ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV46ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV47ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV48ContagemResultado_StatusCnt3',fld:'vCONTAGEMRESULTADO_STATUSCNT3',pic:'Z9',nv:0},{av:'AV49ContagemResultado_Descricao3',fld:'vCONTAGEMRESULTADO_DESCRICAO3',pic:'',nv:''},{av:'AV50ContagemResultado_SistemaCod3',fld:'vCONTAGEMRESULTADO_SISTEMACOD3',pic:'ZZZZZ9',nv:0},{av:'AV51ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV26DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV39DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV13Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV65ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV66ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV122Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV53DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV52DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV52DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV39DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV27DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV28DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV40DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV25ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV24ContagemResultado_SistemaCod1',fld:'vCONTAGEMRESULTADO_SISTEMACOD1',pic:'ZZZZZ9',nv:0},{av:'AV23ContagemResultado_Descricao1',fld:'vCONTAGEMRESULTADO_DESCRICAO1',pic:'',nv:''},{av:'AV22ContagemResultado_StatusCnt1',fld:'vCONTAGEMRESULTADO_STATUSCNT1',pic:'Z9',nv:0},{av:'AV21ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV19ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV18ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV16ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV38ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV37ContagemResultado_SistemaCod2',fld:'vCONTAGEMRESULTADO_SISTEMACOD2',pic:'ZZZZZ9',nv:0},{av:'AV36ContagemResultado_Descricao2',fld:'vCONTAGEMRESULTADO_DESCRICAO2',pic:'',nv:''},{av:'AV35ContagemResultado_StatusCnt2',fld:'vCONTAGEMRESULTADO_STATUSCNT2',pic:'Z9',nv:0},{av:'AV34ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV32ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV30ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV31ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV51ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV50ContagemResultado_SistemaCod3',fld:'vCONTAGEMRESULTADO_SISTEMACOD3',pic:'ZZZZZ9',nv:0},{av:'AV49ContagemResultado_Descricao3',fld:'vCONTAGEMRESULTADO_DESCRICAO3',pic:'',nv:''},{av:'AV48ContagemResultado_StatusCnt3',fld:'vCONTAGEMRESULTADO_STATUSCNT3',pic:'Z9',nv:0},{av:'AV47ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV45ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV46ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV43ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV44ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'edtavContagemresultado_demandafm2_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL2',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn2'},{av:'cmbavContagemresultado_statuscnt2'},{av:'edtavContagemresultado_descricao2_Visible',ctrl:'vCONTAGEMRESULTADO_DESCRICAO2',prop:'Visible'},{av:'dynavContagemresultado_sistemacod2'},{av:'edtavContagemresultado_agrupador2_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContagemresultado_demandafm3_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL3',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn3'},{av:'cmbavContagemresultado_statuscnt3'},{av:'edtavContagemresultado_descricao3_Visible',ctrl:'vCONTAGEMRESULTADO_DESCRICAO3',prop:'Visible'},{av:'dynavContagemresultado_sistemacod3'},{av:'edtavContagemresultado_agrupador3_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContagemresultado_demandafm1_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL1',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn1'},{av:'cmbavContagemresultado_statuscnt1'},{av:'edtavContagemresultado_descricao1_Visible',ctrl:'vCONTAGEMRESULTADO_DESCRICAO1',prop:'Visible'},{av:'dynavContagemresultado_sistemacod1'},{av:'edtavContagemresultado_agrupador1_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'cellContagemresultado_dataentregareal_cell2_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL2',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell2_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL2',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_12_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_12',prop:'Class'},{av:'cellContagemresultado_dataentregareal_cell3_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL3',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell3_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL3',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_13_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_13',prop:'Class'},{av:'cellContagemresultado_dataentregareal_cell1_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL1',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell1_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL1',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_11_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_11',prop:'Class'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E21TB2',iparms:[{av:'AV40DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV41DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtavContagemresultado_demandafm3_Visible',ctrl:'vCONTAGEMRESULTADO_DEMANDAFM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATADMN3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADO_DATAENTREGAREAL3',prop:'Visible'},{av:'cmbavContagemresultado_statusdmn3'},{av:'cmbavContagemresultado_statuscnt3'},{av:'edtavContagemresultado_descricao3_Visible',ctrl:'vCONTAGEMRESULTADO_DESCRICAO3',prop:'Visible'},{av:'dynavContagemresultado_sistemacod3'},{av:'edtavContagemresultado_agrupador3_Visible',ctrl:'vCONTAGEMRESULTADO_AGRUPADOR3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'cellContagemresultado_dataentregareal_cell3_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL3',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell3_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL3',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_13_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_13',prop:'Class'},{av:'AV45ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV46ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''}]}");
         setEventMetadata("VDYNAMICFILTERSOPERATOR3.CLICK","{handler:'E22TB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV67ContagemResultado_ServicoGrupo',fld:'vCONTAGEMRESULTADO_SERVICOGRUPO',pic:'ZZZZZ9',nv:0},{av:'AV68ContagemResultado_Servico',fld:'vCONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV69ContagemResultado_CntadaOsVinc',fld:'vCONTAGEMRESULTADO_CNTADAOSVINC',pic:'ZZZZZ9',nv:0},{av:'AV71ContagemResultado_SerGrupoVinc',fld:'vCONTAGEMRESULTADO_SERGRUPOVINC',pic:'ZZZZZ9',nv:0},{av:'AV70ContagemResultado_CodSrvVnc',fld:'vCONTAGEMRESULTADO_CODSRVVNC',pic:'ZZZZZ9',nv:0},{av:'AV14DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV15DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV16ContagemResultado_DemandaFM1',fld:'vCONTAGEMRESULTADO_DEMANDAFM1',pic:'',nv:''},{av:'AV17ContagemResultado_DataDmn1',fld:'vCONTAGEMRESULTADO_DATADMN1',pic:'',nv:''},{av:'AV18ContagemResultado_DataDmn_To1',fld:'vCONTAGEMRESULTADO_DATADMN_TO1',pic:'',nv:''},{av:'AV19ContagemResultado_DataEntregaReal1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL1',pic:'99/99/99 99:99',nv:''},{av:'AV20ContagemResultado_DataEntregaReal_To1',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultado_StatusDmn1',fld:'vCONTAGEMRESULTADO_STATUSDMN1',pic:'',nv:''},{av:'AV22ContagemResultado_StatusCnt1',fld:'vCONTAGEMRESULTADO_STATUSCNT1',pic:'Z9',nv:0},{av:'AV23ContagemResultado_Descricao1',fld:'vCONTAGEMRESULTADO_DESCRICAO1',pic:'',nv:''},{av:'AV24ContagemResultado_SistemaCod1',fld:'vCONTAGEMRESULTADO_SISTEMACOD1',pic:'ZZZZZ9',nv:0},{av:'AV25ContagemResultado_Agrupador1',fld:'vCONTAGEMRESULTADO_AGRUPADOR1',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV28DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV29ContagemResultado_DemandaFM2',fld:'vCONTAGEMRESULTADO_DEMANDAFM2',pic:'',nv:''},{av:'AV30ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV31ContagemResultado_DataDmn_To2',fld:'vCONTAGEMRESULTADO_DATADMN_TO2',pic:'',nv:''},{av:'AV32ContagemResultado_DataEntregaReal2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL2',pic:'99/99/99 99:99',nv:''},{av:'AV33ContagemResultado_DataEntregaReal_To2',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV34ContagemResultado_StatusDmn2',fld:'vCONTAGEMRESULTADO_STATUSDMN2',pic:'',nv:''},{av:'AV35ContagemResultado_StatusCnt2',fld:'vCONTAGEMRESULTADO_STATUSCNT2',pic:'Z9',nv:0},{av:'AV36ContagemResultado_Descricao2',fld:'vCONTAGEMRESULTADO_DESCRICAO2',pic:'',nv:''},{av:'AV37ContagemResultado_SistemaCod2',fld:'vCONTAGEMRESULTADO_SISTEMACOD2',pic:'ZZZZZ9',nv:0},{av:'AV38ContagemResultado_Agrupador2',fld:'vCONTAGEMRESULTADO_AGRUPADOR2',pic:'@!',nv:''},{av:'AV40DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42ContagemResultado_DemandaFM3',fld:'vCONTAGEMRESULTADO_DEMANDAFM3',pic:'',nv:''},{av:'AV43ContagemResultado_DataDmn3',fld:'vCONTAGEMRESULTADO_DATADMN3',pic:'',nv:''},{av:'AV44ContagemResultado_DataDmn_To3',fld:'vCONTAGEMRESULTADO_DATADMN_TO3',pic:'',nv:''},{av:'AV45ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV46ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV47ContagemResultado_StatusDmn3',fld:'vCONTAGEMRESULTADO_STATUSDMN3',pic:'',nv:''},{av:'AV48ContagemResultado_StatusCnt3',fld:'vCONTAGEMRESULTADO_STATUSCNT3',pic:'Z9',nv:0},{av:'AV49ContagemResultado_Descricao3',fld:'vCONTAGEMRESULTADO_DESCRICAO3',pic:'',nv:''},{av:'AV50ContagemResultado_SistemaCod3',fld:'vCONTAGEMRESULTADO_SISTEMACOD3',pic:'ZZZZZ9',nv:0},{av:'AV51ContagemResultado_Agrupador3',fld:'vCONTAGEMRESULTADO_AGRUPADOR3',pic:'@!',nv:''},{av:'AV26DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV39DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV13Contratada_AreaTrabalhoCod',fld:'vCONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV65ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'AV66ContagemResultado_ContratadaCod',fld:'vCONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'AV122Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV53DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV52DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''}],oparms:[{av:'AV45ContagemResultado_DataEntregaReal3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL3',pic:'99/99/99 99:99',nv:''},{av:'AV46ContagemResultado_DataEntregaReal_To3',fld:'vCONTAGEMRESULTADO_DATAENTREGAREAL_TO3',pic:'99/99/99 99:99',nv:''},{av:'cellContagemresultado_dataentregareal_cell3_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_CELL3',prop:'Class'},{av:'cellContagemresultado_dataentregareal_to_cell3_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_TO_CELL3',prop:'Class'},{av:'lblContagemresultado_dataentregareal_rangemiddletext_13_Class',ctrl:'CONTAGEMRESULTADO_DATAENTREGAREAL_RANGEMIDDLETEXT_13',prop:'Class'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV14DynamicFiltersSelector1 = "";
         AV16ContagemResultado_DemandaFM1 = "";
         AV17ContagemResultado_DataDmn1 = DateTime.MinValue;
         AV18ContagemResultado_DataDmn_To1 = DateTime.MinValue;
         AV19ContagemResultado_DataEntregaReal1 = (DateTime)(DateTime.MinValue);
         AV20ContagemResultado_DataEntregaReal_To1 = (DateTime)(DateTime.MinValue);
         AV21ContagemResultado_StatusDmn1 = "";
         AV23ContagemResultado_Descricao1 = "";
         AV25ContagemResultado_Agrupador1 = "";
         AV27DynamicFiltersSelector2 = "";
         AV29ContagemResultado_DemandaFM2 = "";
         AV30ContagemResultado_DataDmn2 = DateTime.MinValue;
         AV31ContagemResultado_DataDmn_To2 = DateTime.MinValue;
         AV32ContagemResultado_DataEntregaReal2 = (DateTime)(DateTime.MinValue);
         AV33ContagemResultado_DataEntregaReal_To2 = (DateTime)(DateTime.MinValue);
         AV34ContagemResultado_StatusDmn2 = "";
         AV36ContagemResultado_Descricao2 = "";
         AV38ContagemResultado_Agrupador2 = "";
         AV40DynamicFiltersSelector3 = "";
         AV42ContagemResultado_DemandaFM3 = "";
         AV43ContagemResultado_DataDmn3 = DateTime.MinValue;
         AV44ContagemResultado_DataDmn_To3 = DateTime.MinValue;
         AV45ContagemResultado_DataEntregaReal3 = (DateTime)(DateTime.MinValue);
         AV46ContagemResultado_DataEntregaReal_To3 = (DateTime)(DateTime.MinValue);
         AV47ContagemResultado_StatusDmn3 = "";
         AV49ContagemResultado_Descricao3 = "";
         AV51ContagemResultado_Agrupador3 = "";
         AV122Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         Gx_date = DateTime.MinValue;
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV64Display = "";
         AV120Display_GXI = "";
         A493ContagemResultado_DemandaFM = "";
         A457ContagemResultado_Demanda = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A2017ContagemResultado_DataEntregaReal = (DateTime)(DateTime.MinValue);
         A494ContagemResultado_Descricao = "";
         A509ContagemrResultado_SistemaSigla = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00TB2_A39Contratada_Codigo = new int[1] ;
         H00TB2_A438Contratada_Sigla = new String[] {""} ;
         H00TB2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00TB2_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00TB3_A74Contrato_Codigo = new int[1] ;
         H00TB3_A155Servico_Codigo = new int[1] ;
         H00TB3_A160ContratoServicos_Codigo = new int[1] ;
         H00TB3_A157ServicoGrupo_Codigo = new int[1] ;
         H00TB3_A158ServicoGrupo_Descricao = new String[] {""} ;
         H00TB3_A39Contratada_Codigo = new int[1] ;
         H00TB3_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00TB4_A74Contrato_Codigo = new int[1] ;
         H00TB4_A160ContratoServicos_Codigo = new int[1] ;
         H00TB4_A155Servico_Codigo = new int[1] ;
         H00TB4_A605Servico_Sigla = new String[] {""} ;
         H00TB4_A39Contratada_Codigo = new int[1] ;
         H00TB4_A157ServicoGrupo_Codigo = new int[1] ;
         H00TB4_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00TB5_A39Contratada_Codigo = new int[1] ;
         H00TB5_A438Contratada_Sigla = new String[] {""} ;
         H00TB5_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00TB5_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00TB6_A74Contrato_Codigo = new int[1] ;
         H00TB6_A155Servico_Codigo = new int[1] ;
         H00TB6_A160ContratoServicos_Codigo = new int[1] ;
         H00TB6_A157ServicoGrupo_Codigo = new int[1] ;
         H00TB6_A158ServicoGrupo_Descricao = new String[] {""} ;
         H00TB6_A39Contratada_Codigo = new int[1] ;
         H00TB6_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00TB7_A74Contrato_Codigo = new int[1] ;
         H00TB7_A160ContratoServicos_Codigo = new int[1] ;
         H00TB7_A155Servico_Codigo = new int[1] ;
         H00TB7_A605Servico_Sigla = new String[] {""} ;
         H00TB7_A39Contratada_Codigo = new int[1] ;
         H00TB7_A157ServicoGrupo_Codigo = new int[1] ;
         H00TB7_A638ContratoServicos_Ativo = new bool[] {false} ;
         H00TB8_A127Sistema_Codigo = new int[1] ;
         H00TB8_A129Sistema_Sigla = new String[] {""} ;
         H00TB8_A135Sistema_AreaTrabalhoCod = new int[1] ;
         H00TB8_A130Sistema_Ativo = new bool[] {false} ;
         H00TB9_A127Sistema_Codigo = new int[1] ;
         H00TB9_A129Sistema_Sigla = new String[] {""} ;
         H00TB9_A135Sistema_AreaTrabalhoCod = new int[1] ;
         H00TB9_A130Sistema_Ativo = new bool[] {false} ;
         H00TB10_A127Sistema_Codigo = new int[1] ;
         H00TB10_A129Sistema_Sigla = new String[] {""} ;
         H00TB10_A135Sistema_AreaTrabalhoCod = new int[1] ;
         H00TB10_A130Sistema_Ativo = new bool[] {false} ;
         GridContainer = new GXWebGrid( context);
         lV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1 = "";
         lV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1 = "";
         lV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1 = "";
         lV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2 = "";
         lV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2 = "";
         lV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2 = "";
         lV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3 = "";
         lV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3 = "";
         lV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3 = "";
         AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1 = "";
         AV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1 = "";
         AV79ExtraWWContagemResultadoRelComparacaoDemandasDS_6_Contagemresultado_datadmn1 = DateTime.MinValue;
         AV80ExtraWWContagemResultadoRelComparacaoDemandasDS_7_Contagemresultado_datadmn_to1 = DateTime.MinValue;
         AV81ExtraWWContagemResultadoRelComparacaoDemandasDS_8_Contagemresultado_dataentregareal1 = (DateTime)(DateTime.MinValue);
         AV82ExtraWWContagemResultadoRelComparacaoDemandasDS_9_Contagemresultado_dataentregareal_to1 = (DateTime)(DateTime.MinValue);
         AV83ExtraWWContagemResultadoRelComparacaoDemandasDS_10_Contagemresultado_statusdmn1 = "";
         AV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1 = "";
         AV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1 = "";
         AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2 = "";
         AV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2 = "";
         AV92ExtraWWContagemResultadoRelComparacaoDemandasDS_19_Contagemresultado_datadmn2 = DateTime.MinValue;
         AV93ExtraWWContagemResultadoRelComparacaoDemandasDS_20_Contagemresultado_datadmn_to2 = DateTime.MinValue;
         AV94ExtraWWContagemResultadoRelComparacaoDemandasDS_21_Contagemresultado_dataentregareal2 = (DateTime)(DateTime.MinValue);
         AV95ExtraWWContagemResultadoRelComparacaoDemandasDS_22_Contagemresultado_dataentregareal_to2 = (DateTime)(DateTime.MinValue);
         AV96ExtraWWContagemResultadoRelComparacaoDemandasDS_23_Contagemresultado_statusdmn2 = "";
         AV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2 = "";
         AV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2 = "";
         AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3 = "";
         AV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3 = "";
         AV105ExtraWWContagemResultadoRelComparacaoDemandasDS_32_Contagemresultado_datadmn3 = DateTime.MinValue;
         AV106ExtraWWContagemResultadoRelComparacaoDemandasDS_33_Contagemresultado_datadmn_to3 = DateTime.MinValue;
         AV107ExtraWWContagemResultadoRelComparacaoDemandasDS_34_Contagemresultado_dataentregareal3 = (DateTime)(DateTime.MinValue);
         AV108ExtraWWContagemResultadoRelComparacaoDemandasDS_35_Contagemresultado_dataentregareal_to3 = (DateTime)(DateTime.MinValue);
         AV109ExtraWWContagemResultadoRelComparacaoDemandasDS_36_Contagemresultado_statusdmn3 = "";
         AV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3 = "";
         AV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3 = "";
         A484ContagemResultado_StatusDmn = "";
         A1046ContagemResultado_Agrupador = "";
         H00TB11_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         H00TB11_A511ContagemResultado_HoraCnt = new String[] {""} ;
         H00TB11_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00TB11_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00TB11_A1627ContagemResultado_CntSrvVncCod = new int[1] ;
         H00TB11_n1627ContagemResultado_CntSrvVncCod = new bool[] {false} ;
         H00TB11_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00TB11_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00TB11_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00TB11_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00TB11_A1591ContagemResultado_CodSrvVnc = new int[1] ;
         H00TB11_n1591ContagemResultado_CodSrvVnc = new bool[] {false} ;
         H00TB11_A2146ContagemResultado_SerGrupoVinc = new int[1] ;
         H00TB11_n2146ContagemResultado_SerGrupoVinc = new bool[] {false} ;
         H00TB11_A2145ContagemResultado_CntadaOsVinc = new int[1] ;
         H00TB11_n2145ContagemResultado_CntadaOsVinc = new bool[] {false} ;
         H00TB11_A601ContagemResultado_Servico = new int[1] ;
         H00TB11_n601ContagemResultado_Servico = new bool[] {false} ;
         H00TB11_A764ContagemResultado_ServicoGrupo = new int[1] ;
         H00TB11_n764ContagemResultado_ServicoGrupo = new bool[] {false} ;
         H00TB11_A1046ContagemResultado_Agrupador = new String[] {""} ;
         H00TB11_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         H00TB11_A489ContagemResultado_SistemaCod = new int[1] ;
         H00TB11_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00TB11_A483ContagemResultado_StatusCnt = new short[1] ;
         H00TB11_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00TB11_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00TB11_A602ContagemResultado_OSVinculada = new int[1] ;
         H00TB11_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         H00TB11_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         H00TB11_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         H00TB11_A494ContagemResultado_Descricao = new String[] {""} ;
         H00TB11_n494ContagemResultado_Descricao = new bool[] {false} ;
         H00TB11_A2017ContagemResultado_DataEntregaReal = new DateTime[] {DateTime.MinValue} ;
         H00TB11_n2017ContagemResultado_DataEntregaReal = new bool[] {false} ;
         H00TB11_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00TB11_A457ContagemResultado_Demanda = new String[] {""} ;
         H00TB11_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00TB11_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00TB11_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00TB11_A456ContagemResultado_Codigo = new int[1] ;
         H00TB12_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         GridRow = new GXWebRow();
         AV54Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblFiltertextcontratada_areatrabalhocod_Jsonclick = "";
         lblFiltertextcontagemresultado_osvinculada_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefixcontagemresultado_contratadacod_Jsonclick = "";
         lblDynamicfiltersfixedfiltercaptioncontagemresultado_contratadacod_Jsonclick = "";
         lblDynamicfiltersmiddlecontagemresultado_contratadacod_Jsonclick = "";
         lblDynamicfiltersprefixcontagemresultado_servicogrupo_Jsonclick = "";
         lblDynamicfiltersfixedfiltercaptioncontagemresultado_servicogrupo_Jsonclick = "";
         lblDynamicfiltersmiddlecontagemresultado_servicogrupo_Jsonclick = "";
         lblDynamicfiltersprefixcontagemresultado_servico_Jsonclick = "";
         lblDynamicfiltersfixedfiltercaptioncontagemresultado_servico_Jsonclick = "";
         lblDynamicfiltersmiddlecontagemresultado_servico_Jsonclick = "";
         lblDynamicfiltersprefixcontagemresultado_cntadaosvinc_Jsonclick = "";
         lblDynamicfiltersfixedfiltercaptioncontagemresultado_cntadaosvinc_Jsonclick = "";
         lblDynamicfiltersmiddlecontagemresultado_cntadaosvinc_Jsonclick = "";
         lblDynamicfiltersprefixcontagemresultado_sergrupovinc_Jsonclick = "";
         lblDynamicfiltersfixedfiltercaptioncontagemresultado_sergrupovinc_Jsonclick = "";
         lblDynamicfiltersmiddlecontagemresultado_sergrupovinc_Jsonclick = "";
         lblDynamicfiltersprefixcontagemresultado_codsrvvnc_Jsonclick = "";
         lblDynamicfiltersfixedfiltercaptioncontagemresultado_codsrvvnc_Jsonclick = "";
         lblDynamicfiltersmiddlecontagemresultado_codsrvvnc_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblContagemresultado_dataentregareal_rangemiddletext_13_Jsonclick = "";
         lblContagemresultado_datadmn_rangemiddletext_13_Jsonclick = "";
         lblContagemresultado_dataentregareal_rangemiddletext_12_Jsonclick = "";
         lblContagemresultado_datadmn_rangemiddletext_12_Jsonclick = "";
         lblContagemresultado_dataentregareal_rangemiddletext_11_Jsonclick = "";
         lblContagemresultado_datadmn_rangemiddletext_11_Jsonclick = "";
         lblContagemresultadotitle_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.extrawwcontagemresultadorelcomparacaodemandas__default(),
            new Object[][] {
                new Object[] {
               H00TB2_A39Contratada_Codigo, H00TB2_A438Contratada_Sigla, H00TB2_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               H00TB3_A74Contrato_Codigo, H00TB3_A155Servico_Codigo, H00TB3_A160ContratoServicos_Codigo, H00TB3_A157ServicoGrupo_Codigo, H00TB3_A158ServicoGrupo_Descricao, H00TB3_A39Contratada_Codigo, H00TB3_A638ContratoServicos_Ativo
               }
               , new Object[] {
               H00TB4_A74Contrato_Codigo, H00TB4_A160ContratoServicos_Codigo, H00TB4_A155Servico_Codigo, H00TB4_A605Servico_Sigla, H00TB4_A39Contratada_Codigo, H00TB4_A157ServicoGrupo_Codigo, H00TB4_A638ContratoServicos_Ativo
               }
               , new Object[] {
               H00TB5_A39Contratada_Codigo, H00TB5_A438Contratada_Sigla, H00TB5_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               H00TB6_A74Contrato_Codigo, H00TB6_A155Servico_Codigo, H00TB6_A160ContratoServicos_Codigo, H00TB6_A157ServicoGrupo_Codigo, H00TB6_A158ServicoGrupo_Descricao, H00TB6_A39Contratada_Codigo, H00TB6_A638ContratoServicos_Ativo
               }
               , new Object[] {
               H00TB7_A74Contrato_Codigo, H00TB7_A160ContratoServicos_Codigo, H00TB7_A155Servico_Codigo, H00TB7_A605Servico_Sigla, H00TB7_A39Contratada_Codigo, H00TB7_A157ServicoGrupo_Codigo, H00TB7_A638ContratoServicos_Ativo
               }
               , new Object[] {
               H00TB8_A127Sistema_Codigo, H00TB8_A129Sistema_Sigla, H00TB8_A135Sistema_AreaTrabalhoCod, H00TB8_A130Sistema_Ativo
               }
               , new Object[] {
               H00TB9_A127Sistema_Codigo, H00TB9_A129Sistema_Sigla, H00TB9_A135Sistema_AreaTrabalhoCod, H00TB9_A130Sistema_Ativo
               }
               , new Object[] {
               H00TB10_A127Sistema_Codigo, H00TB10_A129Sistema_Sigla, H00TB10_A135Sistema_AreaTrabalhoCod, H00TB10_A130Sistema_Ativo
               }
               , new Object[] {
               H00TB11_A473ContagemResultado_DataCnt, H00TB11_A511ContagemResultado_HoraCnt, H00TB11_A1553ContagemResultado_CntSrvCod, H00TB11_n1553ContagemResultado_CntSrvCod, H00TB11_A1627ContagemResultado_CntSrvVncCod, H00TB11_n1627ContagemResultado_CntSrvVncCod, H00TB11_A52Contratada_AreaTrabalhoCod, H00TB11_n52Contratada_AreaTrabalhoCod, H00TB11_A490ContagemResultado_ContratadaCod, H00TB11_n490ContagemResultado_ContratadaCod,
               H00TB11_A1591ContagemResultado_CodSrvVnc, H00TB11_n1591ContagemResultado_CodSrvVnc, H00TB11_A2146ContagemResultado_SerGrupoVinc, H00TB11_n2146ContagemResultado_SerGrupoVinc, H00TB11_A2145ContagemResultado_CntadaOsVinc, H00TB11_n2145ContagemResultado_CntadaOsVinc, H00TB11_A601ContagemResultado_Servico, H00TB11_n601ContagemResultado_Servico, H00TB11_A764ContagemResultado_ServicoGrupo, H00TB11_n764ContagemResultado_ServicoGrupo,
               H00TB11_A1046ContagemResultado_Agrupador, H00TB11_n1046ContagemResultado_Agrupador, H00TB11_A489ContagemResultado_SistemaCod, H00TB11_n489ContagemResultado_SistemaCod, H00TB11_A483ContagemResultado_StatusCnt, H00TB11_A484ContagemResultado_StatusDmn, H00TB11_n484ContagemResultado_StatusDmn, H00TB11_A602ContagemResultado_OSVinculada, H00TB11_n602ContagemResultado_OSVinculada, H00TB11_A509ContagemrResultado_SistemaSigla,
               H00TB11_n509ContagemrResultado_SistemaSigla, H00TB11_A494ContagemResultado_Descricao, H00TB11_n494ContagemResultado_Descricao, H00TB11_A2017ContagemResultado_DataEntregaReal, H00TB11_n2017ContagemResultado_DataEntregaReal, H00TB11_A471ContagemResultado_DataDmn, H00TB11_A457ContagemResultado_Demanda, H00TB11_n457ContagemResultado_Demanda, H00TB11_A493ContagemResultado_DemandaFM, H00TB11_n493ContagemResultado_DemandaFM,
               H00TB11_A456ContagemResultado_Codigo
               }
               , new Object[] {
               H00TB12_AGRID_nRecordCount
               }
            }
         );
         AV122Pgmname = "ExtraWWContagemResultadoRelComparacaoDemandas";
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         AV122Pgmname = "ExtraWWContagemResultadoRelComparacaoDemandas";
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_206 ;
      private short nGXsfl_206_idx=1 ;
      private short AV15DynamicFiltersOperator1 ;
      private short AV22ContagemResultado_StatusCnt1 ;
      private short AV28DynamicFiltersOperator2 ;
      private short AV35ContagemResultado_StatusCnt2 ;
      private short AV41DynamicFiltersOperator3 ;
      private short AV48ContagemResultado_StatusCnt3 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_206_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 ;
      private short AV84ExtraWWContagemResultadoRelComparacaoDemandasDS_11_Contagemresultado_statuscnt1 ;
      private short AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 ;
      private short AV97ExtraWWContagemResultadoRelComparacaoDemandasDS_24_Contagemresultado_statuscnt2 ;
      private short AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 ;
      private short AV110ExtraWWContagemResultadoRelComparacaoDemandasDS_37_Contagemresultado_statuscnt3 ;
      private short A483ContagemResultado_StatusCnt ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private short wbTemp ;
      private int AV66ContagemResultado_ContratadaCod ;
      private int AV67ContagemResultado_ServicoGrupo ;
      private int AV69ContagemResultado_CntadaOsVinc ;
      private int AV71ContagemResultado_SerGrupoVinc ;
      private int subGrid_Rows ;
      private int AV68ContagemResultado_Servico ;
      private int AV70ContagemResultado_CodSrvVnc ;
      private int AV24ContagemResultado_SistemaCod1 ;
      private int AV37ContagemResultado_SistemaCod2 ;
      private int AV50ContagemResultado_SistemaCod3 ;
      private int AV13Contratada_AreaTrabalhoCod ;
      private int AV65ContagemResultado_OSVinculada ;
      private int A456ContagemResultado_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV6WWPContext_gxTpr_Areatrabalho_codigo ;
      private int AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1 ;
      private int AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2 ;
      private int AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3 ;
      private int AV115ExtraWWContagemResultadoRelComparacaoDemandasDS_42_Contagemresultado_servicogrupo ;
      private int AV116ExtraWWContagemResultadoRelComparacaoDemandasDS_43_Contagemresultado_servico ;
      private int AV117ExtraWWContagemResultadoRelComparacaoDemandasDS_44_Contagemresultado_cntadaosvinc ;
      private int AV118ExtraWWContagemResultadoRelComparacaoDemandasDS_45_Contagemresultado_sergrupovinc ;
      private int AV119ExtraWWContagemResultadoRelComparacaoDemandasDS_46_Contagemresultado_codsrvvnc ;
      private int A489ContagemResultado_SistemaCod ;
      private int A764ContagemResultado_ServicoGrupo ;
      private int A601ContagemResultado_Servico ;
      private int A2145ContagemResultado_CntadaOsVinc ;
      private int A2146ContagemResultado_SerGrupoVinc ;
      private int A1591ContagemResultado_CodSrvVnc ;
      private int A602ContagemResultado_OSVinculada ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int AV114ExtraWWContagemResultadoRelComparacaoDemandasDS_41_Contagemresultado_contratadacod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1627ContagemResultado_CntSrvVncCod ;
      private int AV74ExtraWWContagemResultadoRelComparacaoDemandasDS_1_Contratada_areatrabalhocod ;
      private int AV75ExtraWWContagemResultadoRelComparacaoDemandasDS_2_Contagemresultado_osvinculada ;
      private int AV61PageToGo ;
      private int edtavDisplay_Enabled ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavContagemresultado_demandafm1_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultado_datadmn1_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Visible ;
      private int edtavContagemresultado_descricao1_Visible ;
      private int edtavContagemresultado_agrupador1_Visible ;
      private int edtavContagemresultado_demandafm2_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultado_datadmn2_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Visible ;
      private int edtavContagemresultado_descricao2_Visible ;
      private int edtavContagemresultado_agrupador2_Visible ;
      private int edtavContagemresultado_demandafm3_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultado_datadmn3_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Visible ;
      private int edtavContagemresultado_descricao3_Visible ;
      private int edtavContagemresultado_agrupador3_Visible ;
      private int AV123GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV62GridCurrentPage ;
      private long AV63GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_206_idx="0001" ;
      private String AV21ContagemResultado_StatusDmn1 ;
      private String AV25ContagemResultado_Agrupador1 ;
      private String AV34ContagemResultado_StatusDmn2 ;
      private String AV38ContagemResultado_Agrupador2 ;
      private String AV47ContagemResultado_StatusDmn3 ;
      private String AV51ContagemResultado_Agrupador3 ;
      private String AV122Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavDisplay_Internalname ;
      private String edtContagemResultado_Codigo_Internalname ;
      private String edtContagemResultado_DemandaFM_Internalname ;
      private String edtContagemResultado_Demanda_Internalname ;
      private String edtContagemResultado_DataDmn_Internalname ;
      private String edtContagemResultado_DataEntregaReal_Internalname ;
      private String edtContagemResultado_Descricao_Internalname ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String edtContagemrResultado_SistemaSigla_Internalname ;
      private String edtavContratada_areatrabalhocod_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String lV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1 ;
      private String lV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2 ;
      private String lV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3 ;
      private String AV83ExtraWWContagemResultadoRelComparacaoDemandasDS_10_Contagemresultado_statusdmn1 ;
      private String AV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1 ;
      private String AV96ExtraWWContagemResultadoRelComparacaoDemandasDS_23_Contagemresultado_statusdmn2 ;
      private String AV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2 ;
      private String AV109ExtraWWContagemResultadoRelComparacaoDemandasDS_36_Contagemresultado_statusdmn3 ;
      private String AV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3 ;
      private String A484ContagemResultado_StatusDmn ;
      private String A1046ContagemResultado_Agrupador ;
      private String edtavContagemresultado_osvinculada_Internalname ;
      private String dynavContagemresultado_contratadacod_Internalname ;
      private String dynavContagemresultado_servicogrupo_Internalname ;
      private String dynavContagemresultado_servico_Internalname ;
      private String dynavContagemresultado_cntadaosvinc_Internalname ;
      private String dynavContagemresultado_sergrupovinc_Internalname ;
      private String dynavContagemresultado_codsrvvnc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavContagemresultado_demandafm1_Internalname ;
      private String edtavContagemresultado_datadmn1_Internalname ;
      private String edtavContagemresultado_datadmn_to1_Internalname ;
      private String edtavContagemresultado_dataentregareal1_Internalname ;
      private String edtavContagemresultado_dataentregareal_to1_Internalname ;
      private String cmbavContagemresultado_statusdmn1_Internalname ;
      private String cmbavContagemresultado_statuscnt1_Internalname ;
      private String edtavContagemresultado_descricao1_Internalname ;
      private String dynavContagemresultado_sistemacod1_Internalname ;
      private String edtavContagemresultado_agrupador1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavContagemresultado_demandafm2_Internalname ;
      private String edtavContagemresultado_datadmn2_Internalname ;
      private String edtavContagemresultado_datadmn_to2_Internalname ;
      private String edtavContagemresultado_dataentregareal2_Internalname ;
      private String edtavContagemresultado_dataentregareal_to2_Internalname ;
      private String cmbavContagemresultado_statusdmn2_Internalname ;
      private String cmbavContagemresultado_statuscnt2_Internalname ;
      private String edtavContagemresultado_descricao2_Internalname ;
      private String dynavContagemresultado_sistemacod2_Internalname ;
      private String edtavContagemresultado_agrupador2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavContagemresultado_demandafm3_Internalname ;
      private String edtavContagemresultado_datadmn3_Internalname ;
      private String edtavContagemresultado_datadmn_to3_Internalname ;
      private String edtavContagemresultado_dataentregareal3_Internalname ;
      private String edtavContagemresultado_dataentregareal_to3_Internalname ;
      private String cmbavContagemresultado_statusdmn3_Internalname ;
      private String cmbavContagemresultado_statuscnt3_Internalname ;
      private String edtavContagemresultado_descricao3_Internalname ;
      private String dynavContagemresultado_sistemacod3_Internalname ;
      private String edtavContagemresultado_agrupador3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtContagemResultado_DataDmn_Link ;
      private String tblTablemergeddynamicfilterscontagemresultado_datadmn1_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultado_dataentregareal1_Internalname ;
      private String cellContagemresultado_dataentregareal_cell1_Class ;
      private String cellContagemresultado_dataentregareal_cell1_Internalname ;
      private String cellContagemresultado_dataentregareal_to_cell1_Class ;
      private String cellContagemresultado_dataentregareal_to_cell1_Internalname ;
      private String lblContagemresultado_dataentregareal_rangemiddletext_11_Class ;
      private String lblContagemresultado_dataentregareal_rangemiddletext_11_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultado_datadmn2_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultado_dataentregareal2_Internalname ;
      private String cellContagemresultado_dataentregareal_cell2_Class ;
      private String cellContagemresultado_dataentregareal_cell2_Internalname ;
      private String cellContagemresultado_dataentregareal_to_cell2_Class ;
      private String cellContagemresultado_dataentregareal_to_cell2_Internalname ;
      private String lblContagemresultado_dataentregareal_rangemiddletext_12_Class ;
      private String lblContagemresultado_dataentregareal_rangemiddletext_12_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultado_datadmn3_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultado_dataentregareal3_Internalname ;
      private String cellContagemresultado_dataentregareal_cell3_Class ;
      private String cellContagemresultado_dataentregareal_cell3_Internalname ;
      private String cellContagemresultado_dataentregareal_to_cell3_Class ;
      private String cellContagemresultado_dataentregareal_to_cell3_Internalname ;
      private String lblContagemresultado_dataentregareal_rangemiddletext_13_Class ;
      private String lblContagemresultado_dataentregareal_rangemiddletext_13_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextcontratada_areatrabalhocod_Internalname ;
      private String lblFiltertextcontratada_areatrabalhocod_Jsonclick ;
      private String edtavContratada_areatrabalhocod_Jsonclick ;
      private String lblFiltertextcontagemresultado_osvinculada_Internalname ;
      private String lblFiltertextcontagemresultado_osvinculada_Jsonclick ;
      private String edtavContagemresultado_osvinculada_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefixcontagemresultado_contratadacod_Internalname ;
      private String lblDynamicfiltersprefixcontagemresultado_contratadacod_Jsonclick ;
      private String lblDynamicfiltersfixedfiltercaptioncontagemresultado_contratadacod_Internalname ;
      private String lblDynamicfiltersfixedfiltercaptioncontagemresultado_contratadacod_Jsonclick ;
      private String lblDynamicfiltersmiddlecontagemresultado_contratadacod_Internalname ;
      private String lblDynamicfiltersmiddlecontagemresultado_contratadacod_Jsonclick ;
      private String dynavContagemresultado_contratadacod_Jsonclick ;
      private String lblDynamicfiltersprefixcontagemresultado_servicogrupo_Internalname ;
      private String lblDynamicfiltersprefixcontagemresultado_servicogrupo_Jsonclick ;
      private String lblDynamicfiltersfixedfiltercaptioncontagemresultado_servicogrupo_Internalname ;
      private String lblDynamicfiltersfixedfiltercaptioncontagemresultado_servicogrupo_Jsonclick ;
      private String lblDynamicfiltersmiddlecontagemresultado_servicogrupo_Internalname ;
      private String lblDynamicfiltersmiddlecontagemresultado_servicogrupo_Jsonclick ;
      private String dynavContagemresultado_servicogrupo_Jsonclick ;
      private String lblDynamicfiltersprefixcontagemresultado_servico_Internalname ;
      private String lblDynamicfiltersprefixcontagemresultado_servico_Jsonclick ;
      private String lblDynamicfiltersfixedfiltercaptioncontagemresultado_servico_Internalname ;
      private String lblDynamicfiltersfixedfiltercaptioncontagemresultado_servico_Jsonclick ;
      private String lblDynamicfiltersmiddlecontagemresultado_servico_Internalname ;
      private String lblDynamicfiltersmiddlecontagemresultado_servico_Jsonclick ;
      private String dynavContagemresultado_servico_Jsonclick ;
      private String lblDynamicfiltersprefixcontagemresultado_cntadaosvinc_Internalname ;
      private String lblDynamicfiltersprefixcontagemresultado_cntadaosvinc_Jsonclick ;
      private String lblDynamicfiltersfixedfiltercaptioncontagemresultado_cntadaosvinc_Internalname ;
      private String lblDynamicfiltersfixedfiltercaptioncontagemresultado_cntadaosvinc_Jsonclick ;
      private String lblDynamicfiltersmiddlecontagemresultado_cntadaosvinc_Internalname ;
      private String lblDynamicfiltersmiddlecontagemresultado_cntadaosvinc_Jsonclick ;
      private String dynavContagemresultado_cntadaosvinc_Jsonclick ;
      private String lblDynamicfiltersprefixcontagemresultado_sergrupovinc_Internalname ;
      private String lblDynamicfiltersprefixcontagemresultado_sergrupovinc_Jsonclick ;
      private String lblDynamicfiltersfixedfiltercaptioncontagemresultado_sergrupovinc_Internalname ;
      private String lblDynamicfiltersfixedfiltercaptioncontagemresultado_sergrupovinc_Jsonclick ;
      private String lblDynamicfiltersmiddlecontagemresultado_sergrupovinc_Internalname ;
      private String lblDynamicfiltersmiddlecontagemresultado_sergrupovinc_Jsonclick ;
      private String dynavContagemresultado_sergrupovinc_Jsonclick ;
      private String lblDynamicfiltersprefixcontagemresultado_codsrvvnc_Internalname ;
      private String lblDynamicfiltersprefixcontagemresultado_codsrvvnc_Jsonclick ;
      private String lblDynamicfiltersfixedfiltercaptioncontagemresultado_codsrvvnc_Internalname ;
      private String lblDynamicfiltersfixedfiltercaptioncontagemresultado_codsrvvnc_Jsonclick ;
      private String lblDynamicfiltersmiddlecontagemresultado_codsrvvnc_Internalname ;
      private String lblDynamicfiltersmiddlecontagemresultado_codsrvvnc_Jsonclick ;
      private String dynavContagemresultado_codsrvvnc_Jsonclick ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavContagemresultado_demandafm3_Jsonclick ;
      private String cmbavContagemresultado_statusdmn3_Jsonclick ;
      private String cmbavContagemresultado_statuscnt3_Jsonclick ;
      private String edtavContagemresultado_descricao3_Jsonclick ;
      private String dynavContagemresultado_sistemacod3_Jsonclick ;
      private String edtavContagemresultado_agrupador3_Jsonclick ;
      private String edtavContagemresultado_dataentregareal3_Jsonclick ;
      private String cellContagemresultado_dataentregareal_rangemiddletext_1_cell3_Internalname ;
      private String lblContagemresultado_dataentregareal_rangemiddletext_13_Jsonclick ;
      private String edtavContagemresultado_dataentregareal_to3_Jsonclick ;
      private String edtavContagemresultado_datadmn3_Jsonclick ;
      private String cellContagemresultado_datadmn_rangemiddletext_1_cell3_Internalname ;
      private String lblContagemresultado_datadmn_rangemiddletext_13_Internalname ;
      private String lblContagemresultado_datadmn_rangemiddletext_13_Jsonclick ;
      private String cellContagemresultado_datadmn_to_cell3_Internalname ;
      private String edtavContagemresultado_datadmn_to3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavContagemresultado_demandafm2_Jsonclick ;
      private String cmbavContagemresultado_statusdmn2_Jsonclick ;
      private String cmbavContagemresultado_statuscnt2_Jsonclick ;
      private String edtavContagemresultado_descricao2_Jsonclick ;
      private String dynavContagemresultado_sistemacod2_Jsonclick ;
      private String edtavContagemresultado_agrupador2_Jsonclick ;
      private String edtavContagemresultado_dataentregareal2_Jsonclick ;
      private String cellContagemresultado_dataentregareal_rangemiddletext_1_cell2_Internalname ;
      private String lblContagemresultado_dataentregareal_rangemiddletext_12_Jsonclick ;
      private String edtavContagemresultado_dataentregareal_to2_Jsonclick ;
      private String edtavContagemresultado_datadmn2_Jsonclick ;
      private String cellContagemresultado_datadmn_rangemiddletext_1_cell2_Internalname ;
      private String lblContagemresultado_datadmn_rangemiddletext_12_Internalname ;
      private String lblContagemresultado_datadmn_rangemiddletext_12_Jsonclick ;
      private String cellContagemresultado_datadmn_to_cell2_Internalname ;
      private String edtavContagemresultado_datadmn_to2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavContagemresultado_demandafm1_Jsonclick ;
      private String cmbavContagemresultado_statusdmn1_Jsonclick ;
      private String cmbavContagemresultado_statuscnt1_Jsonclick ;
      private String edtavContagemresultado_descricao1_Jsonclick ;
      private String dynavContagemresultado_sistemacod1_Jsonclick ;
      private String edtavContagemresultado_agrupador1_Jsonclick ;
      private String edtavContagemresultado_dataentregareal1_Jsonclick ;
      private String cellContagemresultado_dataentregareal_rangemiddletext_1_cell1_Internalname ;
      private String lblContagemresultado_dataentregareal_rangemiddletext_11_Jsonclick ;
      private String edtavContagemresultado_dataentregareal_to1_Jsonclick ;
      private String edtavContagemresultado_datadmn1_Jsonclick ;
      private String cellContagemresultado_datadmn_rangemiddletext_1_cell1_Internalname ;
      private String lblContagemresultado_datadmn_rangemiddletext_11_Internalname ;
      private String lblContagemresultado_datadmn_rangemiddletext_11_Jsonclick ;
      private String cellContagemresultado_datadmn_to_cell1_Internalname ;
      private String edtavContagemresultado_datadmn_to1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String lblContagemresultadotitle_Internalname ;
      private String lblContagemresultadotitle_Jsonclick ;
      private String sGXsfl_206_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContagemResultado_Codigo_Jsonclick ;
      private String edtContagemResultado_DemandaFM_Jsonclick ;
      private String edtContagemResultado_Demanda_Jsonclick ;
      private String edtContagemResultado_DataDmn_Jsonclick ;
      private String edtContagemResultado_DataEntregaReal_Jsonclick ;
      private String edtContagemResultado_Descricao_Jsonclick ;
      private String edtContagemrResultado_SistemaSigla_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV19ContagemResultado_DataEntregaReal1 ;
      private DateTime AV20ContagemResultado_DataEntregaReal_To1 ;
      private DateTime AV32ContagemResultado_DataEntregaReal2 ;
      private DateTime AV33ContagemResultado_DataEntregaReal_To2 ;
      private DateTime AV45ContagemResultado_DataEntregaReal3 ;
      private DateTime AV46ContagemResultado_DataEntregaReal_To3 ;
      private DateTime A2017ContagemResultado_DataEntregaReal ;
      private DateTime AV81ExtraWWContagemResultadoRelComparacaoDemandasDS_8_Contagemresultado_dataentregareal1 ;
      private DateTime AV82ExtraWWContagemResultadoRelComparacaoDemandasDS_9_Contagemresultado_dataentregareal_to1 ;
      private DateTime AV94ExtraWWContagemResultadoRelComparacaoDemandasDS_21_Contagemresultado_dataentregareal2 ;
      private DateTime AV95ExtraWWContagemResultadoRelComparacaoDemandasDS_22_Contagemresultado_dataentregareal_to2 ;
      private DateTime AV107ExtraWWContagemResultadoRelComparacaoDemandasDS_34_Contagemresultado_dataentregareal3 ;
      private DateTime AV108ExtraWWContagemResultadoRelComparacaoDemandasDS_35_Contagemresultado_dataentregareal_to3 ;
      private DateTime AV17ContagemResultado_DataDmn1 ;
      private DateTime AV18ContagemResultado_DataDmn_To1 ;
      private DateTime AV30ContagemResultado_DataDmn2 ;
      private DateTime AV31ContagemResultado_DataDmn_To2 ;
      private DateTime AV43ContagemResultado_DataDmn3 ;
      private DateTime AV44ContagemResultado_DataDmn_To3 ;
      private DateTime Gx_date ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime AV79ExtraWWContagemResultadoRelComparacaoDemandasDS_6_Contagemresultado_datadmn1 ;
      private DateTime AV80ExtraWWContagemResultadoRelComparacaoDemandasDS_7_Contagemresultado_datadmn_to1 ;
      private DateTime AV92ExtraWWContagemResultadoRelComparacaoDemandasDS_19_Contagemresultado_datadmn2 ;
      private DateTime AV93ExtraWWContagemResultadoRelComparacaoDemandasDS_20_Contagemresultado_datadmn_to2 ;
      private DateTime AV105ExtraWWContagemResultadoRelComparacaoDemandasDS_32_Contagemresultado_datadmn3 ;
      private DateTime AV106ExtraWWContagemResultadoRelComparacaoDemandasDS_33_Contagemresultado_datadmn_to3 ;
      private bool entryPointCalled ;
      private bool AV26DynamicFiltersEnabled2 ;
      private bool AV39DynamicFiltersEnabled3 ;
      private bool AV53DynamicFiltersIgnoreFirst ;
      private bool AV52DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n457ContagemResultado_Demanda ;
      private bool n2017ContagemResultado_DataEntregaReal ;
      private bool n494ContagemResultado_Descricao ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 ;
      private bool AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1627ContagemResultado_CntSrvVncCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n1591ContagemResultado_CodSrvVnc ;
      private bool n2146ContagemResultado_SerGrupoVinc ;
      private bool n2145ContagemResultado_CntadaOsVinc ;
      private bool n601ContagemResultado_Servico ;
      private bool n764ContagemResultado_ServicoGrupo ;
      private bool n1046ContagemResultado_Agrupador ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV64Display_IsBlob ;
      private String AV14DynamicFiltersSelector1 ;
      private String AV16ContagemResultado_DemandaFM1 ;
      private String AV23ContagemResultado_Descricao1 ;
      private String AV27DynamicFiltersSelector2 ;
      private String AV29ContagemResultado_DemandaFM2 ;
      private String AV36ContagemResultado_Descricao2 ;
      private String AV40DynamicFiltersSelector3 ;
      private String AV42ContagemResultado_DemandaFM3 ;
      private String AV49ContagemResultado_Descricao3 ;
      private String AV120Display_GXI ;
      private String A493ContagemResultado_DemandaFM ;
      private String A457ContagemResultado_Demanda ;
      private String A494ContagemResultado_Descricao ;
      private String lV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1 ;
      private String lV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1 ;
      private String lV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2 ;
      private String lV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2 ;
      private String lV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3 ;
      private String lV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3 ;
      private String AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1 ;
      private String AV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1 ;
      private String AV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1 ;
      private String AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2 ;
      private String AV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2 ;
      private String AV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2 ;
      private String AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3 ;
      private String AV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3 ;
      private String AV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3 ;
      private String AV64Display ;
      private IGxSession AV54Session ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavContagemresultado_contratadacod ;
      private GXCombobox dynavContagemresultado_servicogrupo ;
      private GXCombobox dynavContagemresultado_servico ;
      private GXCombobox dynavContagemresultado_cntadaosvinc ;
      private GXCombobox dynavContagemresultado_sergrupovinc ;
      private GXCombobox dynavContagemresultado_codsrvvnc ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavContagemresultado_statusdmn1 ;
      private GXCombobox cmbavContagemresultado_statuscnt1 ;
      private GXCombobox dynavContagemresultado_sistemacod1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavContagemresultado_statusdmn2 ;
      private GXCombobox cmbavContagemresultado_statuscnt2 ;
      private GXCombobox dynavContagemresultado_sistemacod2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCombobox cmbavContagemresultado_statusdmn3 ;
      private GXCombobox cmbavContagemresultado_statuscnt3 ;
      private GXCombobox dynavContagemresultado_sistemacod3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00TB2_A39Contratada_Codigo ;
      private String[] H00TB2_A438Contratada_Sigla ;
      private int[] H00TB2_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00TB2_n52Contratada_AreaTrabalhoCod ;
      private int[] H00TB3_A74Contrato_Codigo ;
      private int[] H00TB3_A155Servico_Codigo ;
      private int[] H00TB3_A160ContratoServicos_Codigo ;
      private int[] H00TB3_A157ServicoGrupo_Codigo ;
      private String[] H00TB3_A158ServicoGrupo_Descricao ;
      private int[] H00TB3_A39Contratada_Codigo ;
      private bool[] H00TB3_A638ContratoServicos_Ativo ;
      private int[] H00TB4_A74Contrato_Codigo ;
      private int[] H00TB4_A160ContratoServicos_Codigo ;
      private int[] H00TB4_A155Servico_Codigo ;
      private String[] H00TB4_A605Servico_Sigla ;
      private int[] H00TB4_A39Contratada_Codigo ;
      private int[] H00TB4_A157ServicoGrupo_Codigo ;
      private bool[] H00TB4_A638ContratoServicos_Ativo ;
      private int[] H00TB5_A39Contratada_Codigo ;
      private String[] H00TB5_A438Contratada_Sigla ;
      private int[] H00TB5_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00TB5_n52Contratada_AreaTrabalhoCod ;
      private int[] H00TB6_A74Contrato_Codigo ;
      private int[] H00TB6_A155Servico_Codigo ;
      private int[] H00TB6_A160ContratoServicos_Codigo ;
      private int[] H00TB6_A157ServicoGrupo_Codigo ;
      private String[] H00TB6_A158ServicoGrupo_Descricao ;
      private int[] H00TB6_A39Contratada_Codigo ;
      private bool[] H00TB6_A638ContratoServicos_Ativo ;
      private int[] H00TB7_A74Contrato_Codigo ;
      private int[] H00TB7_A160ContratoServicos_Codigo ;
      private int[] H00TB7_A155Servico_Codigo ;
      private String[] H00TB7_A605Servico_Sigla ;
      private int[] H00TB7_A39Contratada_Codigo ;
      private int[] H00TB7_A157ServicoGrupo_Codigo ;
      private bool[] H00TB7_A638ContratoServicos_Ativo ;
      private int[] H00TB8_A127Sistema_Codigo ;
      private String[] H00TB8_A129Sistema_Sigla ;
      private int[] H00TB8_A135Sistema_AreaTrabalhoCod ;
      private bool[] H00TB8_A130Sistema_Ativo ;
      private int[] H00TB9_A127Sistema_Codigo ;
      private String[] H00TB9_A129Sistema_Sigla ;
      private int[] H00TB9_A135Sistema_AreaTrabalhoCod ;
      private bool[] H00TB9_A130Sistema_Ativo ;
      private int[] H00TB10_A127Sistema_Codigo ;
      private String[] H00TB10_A129Sistema_Sigla ;
      private int[] H00TB10_A135Sistema_AreaTrabalhoCod ;
      private bool[] H00TB10_A130Sistema_Ativo ;
      private DateTime[] H00TB11_A473ContagemResultado_DataCnt ;
      private String[] H00TB11_A511ContagemResultado_HoraCnt ;
      private int[] H00TB11_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00TB11_n1553ContagemResultado_CntSrvCod ;
      private int[] H00TB11_A1627ContagemResultado_CntSrvVncCod ;
      private bool[] H00TB11_n1627ContagemResultado_CntSrvVncCod ;
      private int[] H00TB11_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00TB11_n52Contratada_AreaTrabalhoCod ;
      private int[] H00TB11_A490ContagemResultado_ContratadaCod ;
      private bool[] H00TB11_n490ContagemResultado_ContratadaCod ;
      private int[] H00TB11_A1591ContagemResultado_CodSrvVnc ;
      private bool[] H00TB11_n1591ContagemResultado_CodSrvVnc ;
      private int[] H00TB11_A2146ContagemResultado_SerGrupoVinc ;
      private bool[] H00TB11_n2146ContagemResultado_SerGrupoVinc ;
      private int[] H00TB11_A2145ContagemResultado_CntadaOsVinc ;
      private bool[] H00TB11_n2145ContagemResultado_CntadaOsVinc ;
      private int[] H00TB11_A601ContagemResultado_Servico ;
      private bool[] H00TB11_n601ContagemResultado_Servico ;
      private int[] H00TB11_A764ContagemResultado_ServicoGrupo ;
      private bool[] H00TB11_n764ContagemResultado_ServicoGrupo ;
      private String[] H00TB11_A1046ContagemResultado_Agrupador ;
      private bool[] H00TB11_n1046ContagemResultado_Agrupador ;
      private int[] H00TB11_A489ContagemResultado_SistemaCod ;
      private bool[] H00TB11_n489ContagemResultado_SistemaCod ;
      private short[] H00TB11_A483ContagemResultado_StatusCnt ;
      private String[] H00TB11_A484ContagemResultado_StatusDmn ;
      private bool[] H00TB11_n484ContagemResultado_StatusDmn ;
      private int[] H00TB11_A602ContagemResultado_OSVinculada ;
      private bool[] H00TB11_n602ContagemResultado_OSVinculada ;
      private String[] H00TB11_A509ContagemrResultado_SistemaSigla ;
      private bool[] H00TB11_n509ContagemrResultado_SistemaSigla ;
      private String[] H00TB11_A494ContagemResultado_Descricao ;
      private bool[] H00TB11_n494ContagemResultado_Descricao ;
      private DateTime[] H00TB11_A2017ContagemResultado_DataEntregaReal ;
      private bool[] H00TB11_n2017ContagemResultado_DataEntregaReal ;
      private DateTime[] H00TB11_A471ContagemResultado_DataDmn ;
      private String[] H00TB11_A457ContagemResultado_Demanda ;
      private bool[] H00TB11_n457ContagemResultado_Demanda ;
      private String[] H00TB11_A493ContagemResultado_DemandaFM ;
      private bool[] H00TB11_n493ContagemResultado_DemandaFM ;
      private int[] H00TB11_A456ContagemResultado_Codigo ;
      private long[] H00TB12_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
   }

   public class extrawwcontagemresultadorelcomparacaodemandas__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00TB11( IGxContext context ,
                                              String AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1 ,
                                              short AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 ,
                                              String AV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1 ,
                                              DateTime AV79ExtraWWContagemResultadoRelComparacaoDemandasDS_6_Contagemresultado_datadmn1 ,
                                              DateTime AV80ExtraWWContagemResultadoRelComparacaoDemandasDS_7_Contagemresultado_datadmn_to1 ,
                                              DateTime AV81ExtraWWContagemResultadoRelComparacaoDemandasDS_8_Contagemresultado_dataentregareal1 ,
                                              DateTime AV82ExtraWWContagemResultadoRelComparacaoDemandasDS_9_Contagemresultado_dataentregareal_to1 ,
                                              String AV83ExtraWWContagemResultadoRelComparacaoDemandasDS_10_Contagemresultado_statusdmn1 ,
                                              short AV84ExtraWWContagemResultadoRelComparacaoDemandasDS_11_Contagemresultado_statuscnt1 ,
                                              String AV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1 ,
                                              int AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1 ,
                                              String AV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1 ,
                                              bool AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 ,
                                              String AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2 ,
                                              short AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 ,
                                              String AV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2 ,
                                              DateTime AV92ExtraWWContagemResultadoRelComparacaoDemandasDS_19_Contagemresultado_datadmn2 ,
                                              DateTime AV93ExtraWWContagemResultadoRelComparacaoDemandasDS_20_Contagemresultado_datadmn_to2 ,
                                              DateTime AV94ExtraWWContagemResultadoRelComparacaoDemandasDS_21_Contagemresultado_dataentregareal2 ,
                                              DateTime AV95ExtraWWContagemResultadoRelComparacaoDemandasDS_22_Contagemresultado_dataentregareal_to2 ,
                                              String AV96ExtraWWContagemResultadoRelComparacaoDemandasDS_23_Contagemresultado_statusdmn2 ,
                                              short AV97ExtraWWContagemResultadoRelComparacaoDemandasDS_24_Contagemresultado_statuscnt2 ,
                                              String AV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2 ,
                                              int AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2 ,
                                              String AV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2 ,
                                              bool AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 ,
                                              String AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3 ,
                                              short AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 ,
                                              String AV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3 ,
                                              DateTime AV105ExtraWWContagemResultadoRelComparacaoDemandasDS_32_Contagemresultado_datadmn3 ,
                                              DateTime AV106ExtraWWContagemResultadoRelComparacaoDemandasDS_33_Contagemresultado_datadmn_to3 ,
                                              DateTime AV107ExtraWWContagemResultadoRelComparacaoDemandasDS_34_Contagemresultado_dataentregareal3 ,
                                              DateTime AV108ExtraWWContagemResultadoRelComparacaoDemandasDS_35_Contagemresultado_dataentregareal_to3 ,
                                              String AV109ExtraWWContagemResultadoRelComparacaoDemandasDS_36_Contagemresultado_statusdmn3 ,
                                              short AV110ExtraWWContagemResultadoRelComparacaoDemandasDS_37_Contagemresultado_statuscnt3 ,
                                              String AV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3 ,
                                              int AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3 ,
                                              String AV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3 ,
                                              int AV115ExtraWWContagemResultadoRelComparacaoDemandasDS_42_Contagemresultado_servicogrupo ,
                                              int AV116ExtraWWContagemResultadoRelComparacaoDemandasDS_43_Contagemresultado_servico ,
                                              int AV117ExtraWWContagemResultadoRelComparacaoDemandasDS_44_Contagemresultado_cntadaosvinc ,
                                              int AV118ExtraWWContagemResultadoRelComparacaoDemandasDS_45_Contagemresultado_sergrupovinc ,
                                              int AV119ExtraWWContagemResultadoRelComparacaoDemandasDS_46_Contagemresultado_codsrvvnc ,
                                              String A493ContagemResultado_DemandaFM ,
                                              DateTime A471ContagemResultado_DataDmn ,
                                              DateTime A2017ContagemResultado_DataEntregaReal ,
                                              String A484ContagemResultado_StatusDmn ,
                                              short A483ContagemResultado_StatusCnt ,
                                              String A494ContagemResultado_Descricao ,
                                              int A489ContagemResultado_SistemaCod ,
                                              String A1046ContagemResultado_Agrupador ,
                                              int A764ContagemResultado_ServicoGrupo ,
                                              int A601ContagemResultado_Servico ,
                                              int A2145ContagemResultado_CntadaOsVinc ,
                                              int A2146ContagemResultado_SerGrupoVinc ,
                                              int A1591ContagemResultado_CodSrvVnc ,
                                              int A602ContagemResultado_OSVinculada ,
                                              int A52Contratada_AreaTrabalhoCod ,
                                              int AV6WWPContext_gxTpr_Areatrabalho_codigo ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              int AV114ExtraWWContagemResultadoRelComparacaoDemandasDS_41_Contagemresultado_contratadacod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [72] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt], T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T7.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvVncCod, T5.[Contratada_AreaTrabalhoCod], T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T8.[Servico_Codigo] AS ContagemResultado_CodSrvVnc, T9.[ServicoGrupo_Codigo] AS ContagemResultado_SerGrupoVinc, T7.[ContagemResultado_ContratadaCod] AS ContagemResultado_CntadaOsVinc, T3.[Servico_Codigo] AS ContagemResultado_Servico, T4.[ServicoGrupo_Codigo] AS ContagemResultado_ServicoGrupo, T2.[ContagemResultado_Agrupador], T2.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_StatusCnt], T2.[ContagemResultado_StatusDmn], T2.[ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada, T6.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T2.[ContagemResultado_Descricao], T2.[ContagemResultado_DataEntregaReal], T2.[ContagemResultado_DataDmn], T2.[ContagemResultado_Demanda], T2.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Codigo]";
         sFromString = " FROM (((((((([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T3.[Servico_Codigo]) LEFT JOIN [Contratada] T5 WITH (NOLOCK) ON T5.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) LEFT JOIN [Sistema] T6 WITH (NOLOCK) ON T6.[Sistema_Codigo] = T2.[ContagemResultado_SistemaCod]) LEFT JOIN [ContagemResultado] T7 WITH (NOLOCK) ON T7.[ContagemResultado_Codigo] = T2.[ContagemResultado_OSVinculada]) LEFT JOIN [ContratoServicos] T8 WITH (NOLOCK) ON T8.[ContratoServicos_Codigo] = T7.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T9 WITH (NOLOCK) ON T9.[Servico_Codigo] = T8.[Servico_Codigo])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T2.[ContagemResultado_OSVinculada] > 0)";
         sWhereString = sWhereString + " and (T5.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV114ExtraWWContagemResultadoRelComparacaoDemandasDS_41_Contagemresultado_contratadacod)";
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like '%' + @lV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! (DateTime.MinValue==AV79ExtraWWContagemResultadoRelComparacaoDemandasDS_6_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV79ExtraWWContagemResultadoRelComparacaoDemandasDS_6_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! (DateTime.MinValue==AV80ExtraWWContagemResultadoRelComparacaoDemandasDS_7_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV80ExtraWWContagemResultadoRelComparacaoDemandasDS_7_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! (DateTime.MinValue==AV81ExtraWWContagemResultadoRelComparacaoDemandasDS_8_Contagemresultado_dataentregareal1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] < @AV81ExtraWWContagemResultadoRelComparacaoDemandasDS_8_Contagemresultado_dataentregareal1)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 1 ) || ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 2 ) || ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 3 ) ) && ( ! (DateTime.MinValue==AV81ExtraWWContagemResultadoRelComparacaoDemandasDS_8_Contagemresultado_dataentregareal1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] >= @AV81ExtraWWContagemResultadoRelComparacaoDemandasDS_8_Contagemresultado_dataentregareal1)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! (DateTime.MinValue==AV82ExtraWWContagemResultadoRelComparacaoDemandasDS_9_Contagemresultado_dataentregareal_to1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] < @AV82ExtraWWContagemResultadoRelComparacaoDemandasDS_9_Contagemresultado_dataentregareal_to1)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 3 ) && ( ! (DateTime.MinValue==AV82ExtraWWContagemResultadoRelComparacaoDemandasDS_9_Contagemresultado_dataentregareal_to1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] <= @AV82ExtraWWContagemResultadoRelComparacaoDemandasDS_9_Contagemresultado_dataentregareal_to1)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83ExtraWWContagemResultadoRelComparacaoDemandasDS_10_Contagemresultado_statusdmn1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_StatusDmn] = @AV83ExtraWWContagemResultadoRelComparacaoDemandasDS_10_Contagemresultado_statusdmn1)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSCNT") == 0 ) && ( ! (0==AV84ExtraWWContagemResultadoRelComparacaoDemandasDS_11_Contagemresultado_statuscnt1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusCnt] = @AV84ExtraWWContagemResultadoRelComparacaoDemandasDS_11_Contagemresultado_statuscnt1)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like @lV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1)";
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1)";
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] = @AV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1)";
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! (0==AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] < @AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! (0==AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 2 ) && ( ! (0==AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] > @AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1)";
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] like @lV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1)";
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] like '%' + @lV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1)";
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like '%' + @lV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 0 ) && ( ! (DateTime.MinValue==AV92ExtraWWContagemResultadoRelComparacaoDemandasDS_19_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV92ExtraWWContagemResultadoRelComparacaoDemandasDS_19_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int1[25] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 0 ) && ( ! (DateTime.MinValue==AV93ExtraWWContagemResultadoRelComparacaoDemandasDS_20_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV93ExtraWWContagemResultadoRelComparacaoDemandasDS_20_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int1[26] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 0 ) && ( ! (DateTime.MinValue==AV94ExtraWWContagemResultadoRelComparacaoDemandasDS_21_Contagemresultado_dataentregareal2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] < @AV94ExtraWWContagemResultadoRelComparacaoDemandasDS_21_Contagemresultado_dataentregareal2)";
         }
         else
         {
            GXv_int1[27] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 1 ) || ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 2 ) || ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 3 ) ) && ( ! (DateTime.MinValue==AV94ExtraWWContagemResultadoRelComparacaoDemandasDS_21_Contagemresultado_dataentregareal2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] >= @AV94ExtraWWContagemResultadoRelComparacaoDemandasDS_21_Contagemresultado_dataentregareal2)";
         }
         else
         {
            GXv_int1[28] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 1 ) && ( ! (DateTime.MinValue==AV95ExtraWWContagemResultadoRelComparacaoDemandasDS_22_Contagemresultado_dataentregareal_to2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] < @AV95ExtraWWContagemResultadoRelComparacaoDemandasDS_22_Contagemresultado_dataentregareal_to2)";
         }
         else
         {
            GXv_int1[29] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 3 ) && ( ! (DateTime.MinValue==AV95ExtraWWContagemResultadoRelComparacaoDemandasDS_22_Contagemresultado_dataentregareal_to2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] <= @AV95ExtraWWContagemResultadoRelComparacaoDemandasDS_22_Contagemresultado_dataentregareal_to2)";
         }
         else
         {
            GXv_int1[30] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96ExtraWWContagemResultadoRelComparacaoDemandasDS_23_Contagemresultado_statusdmn2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_StatusDmn] = @AV96ExtraWWContagemResultadoRelComparacaoDemandasDS_23_Contagemresultado_statusdmn2)";
         }
         else
         {
            GXv_int1[31] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSCNT") == 0 ) && ( ! (0==AV97ExtraWWContagemResultadoRelComparacaoDemandasDS_24_Contagemresultado_statuscnt2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusCnt] = @AV97ExtraWWContagemResultadoRelComparacaoDemandasDS_24_Contagemresultado_statuscnt2)";
         }
         else
         {
            GXv_int1[32] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like @lV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2)";
         }
         else
         {
            GXv_int1[33] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2)";
         }
         else
         {
            GXv_int1[34] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] = @AV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2)";
         }
         else
         {
            GXv_int1[35] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 0 ) && ( ! (0==AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] < @AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int1[36] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 1 ) && ( ! (0==AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int1[37] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 2 ) && ( ! (0==AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] > @AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int1[38] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2)";
         }
         else
         {
            GXv_int1[39] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] like @lV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2)";
         }
         else
         {
            GXv_int1[40] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] like '%' + @lV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2)";
         }
         else
         {
            GXv_int1[41] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int1[42] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int1[43] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like '%' + @lV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int1[44] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 0 ) && ( ! (DateTime.MinValue==AV105ExtraWWContagemResultadoRelComparacaoDemandasDS_32_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV105ExtraWWContagemResultadoRelComparacaoDemandasDS_32_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int1[45] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 0 ) && ( ! (DateTime.MinValue==AV106ExtraWWContagemResultadoRelComparacaoDemandasDS_33_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV106ExtraWWContagemResultadoRelComparacaoDemandasDS_33_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int1[46] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 0 ) && ( ! (DateTime.MinValue==AV107ExtraWWContagemResultadoRelComparacaoDemandasDS_34_Contagemresultado_dataentregareal3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] < @AV107ExtraWWContagemResultadoRelComparacaoDemandasDS_34_Contagemresultado_dataentregareal3)";
         }
         else
         {
            GXv_int1[47] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 1 ) || ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 2 ) || ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 3 ) ) && ( ! (DateTime.MinValue==AV107ExtraWWContagemResultadoRelComparacaoDemandasDS_34_Contagemresultado_dataentregareal3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] >= @AV107ExtraWWContagemResultadoRelComparacaoDemandasDS_34_Contagemresultado_dataentregareal3)";
         }
         else
         {
            GXv_int1[48] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 1 ) && ( ! (DateTime.MinValue==AV108ExtraWWContagemResultadoRelComparacaoDemandasDS_35_Contagemresultado_dataentregareal_to3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] < @AV108ExtraWWContagemResultadoRelComparacaoDemandasDS_35_Contagemresultado_dataentregareal_to3)";
         }
         else
         {
            GXv_int1[49] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 3 ) && ( ! (DateTime.MinValue==AV108ExtraWWContagemResultadoRelComparacaoDemandasDS_35_Contagemresultado_dataentregareal_to3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] <= @AV108ExtraWWContagemResultadoRelComparacaoDemandasDS_35_Contagemresultado_dataentregareal_to3)";
         }
         else
         {
            GXv_int1[50] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109ExtraWWContagemResultadoRelComparacaoDemandasDS_36_Contagemresultado_statusdmn3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_StatusDmn] = @AV109ExtraWWContagemResultadoRelComparacaoDemandasDS_36_Contagemresultado_statusdmn3)";
         }
         else
         {
            GXv_int1[51] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSCNT") == 0 ) && ( ! (0==AV110ExtraWWContagemResultadoRelComparacaoDemandasDS_37_Contagemresultado_statuscnt3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusCnt] = @AV110ExtraWWContagemResultadoRelComparacaoDemandasDS_37_Contagemresultado_statuscnt3)";
         }
         else
         {
            GXv_int1[52] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like @lV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3)";
         }
         else
         {
            GXv_int1[53] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3)";
         }
         else
         {
            GXv_int1[54] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] = @AV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3)";
         }
         else
         {
            GXv_int1[55] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 0 ) && ( ! (0==AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] < @AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int1[56] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 1 ) && ( ! (0==AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int1[57] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 2 ) && ( ! (0==AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] > @AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int1[58] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3)";
         }
         else
         {
            GXv_int1[59] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] like @lV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3)";
         }
         else
         {
            GXv_int1[60] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] like '%' + @lV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3)";
         }
         else
         {
            GXv_int1[61] = 1;
         }
         if ( ! (0==AV115ExtraWWContagemResultadoRelComparacaoDemandasDS_42_Contagemresultado_servicogrupo) )
         {
            sWhereString = sWhereString + " and (T4.[ServicoGrupo_Codigo] = @AV115ExtraWWContagemResultadoRelComparacaoDemandasDS_42_Contagemresultado_servicogrupo)";
         }
         else
         {
            GXv_int1[62] = 1;
         }
         if ( ! (0==AV116ExtraWWContagemResultadoRelComparacaoDemandasDS_43_Contagemresultado_servico) )
         {
            sWhereString = sWhereString + " and (T3.[Servico_Codigo] = @AV116ExtraWWContagemResultadoRelComparacaoDemandasDS_43_Contagemresultado_servico)";
         }
         else
         {
            GXv_int1[63] = 1;
         }
         if ( ! (0==AV117ExtraWWContagemResultadoRelComparacaoDemandasDS_44_Contagemresultado_cntadaosvinc) )
         {
            sWhereString = sWhereString + " and (T7.[ContagemResultado_ContratadaCod] = @AV117ExtraWWContagemResultadoRelComparacaoDemandasDS_44_Contagemresultado_cntadaosvinc)";
         }
         else
         {
            GXv_int1[64] = 1;
         }
         if ( ! (0==AV118ExtraWWContagemResultadoRelComparacaoDemandasDS_45_Contagemresultado_sergrupovinc) )
         {
            sWhereString = sWhereString + " and (T9.[ServicoGrupo_Codigo] = @AV118ExtraWWContagemResultadoRelComparacaoDemandasDS_45_Contagemresultado_sergrupovinc)";
         }
         else
         {
            GXv_int1[65] = 1;
         }
         if ( ! (0==AV119ExtraWWContagemResultadoRelComparacaoDemandasDS_46_Contagemresultado_codsrvvnc) )
         {
            sWhereString = sWhereString + " and (T8.[Servico_Codigo] = @AV119ExtraWWContagemResultadoRelComparacaoDemandasDS_46_Contagemresultado_codsrvvnc)";
         }
         else
         {
            GXv_int1[66] = 1;
         }
         sOrderString = sOrderString + " ORDER BY T2.[ContagemResultado_DemandaFM]";
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H00TB12( IGxContext context ,
                                              String AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1 ,
                                              short AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 ,
                                              String AV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1 ,
                                              DateTime AV79ExtraWWContagemResultadoRelComparacaoDemandasDS_6_Contagemresultado_datadmn1 ,
                                              DateTime AV80ExtraWWContagemResultadoRelComparacaoDemandasDS_7_Contagemresultado_datadmn_to1 ,
                                              DateTime AV81ExtraWWContagemResultadoRelComparacaoDemandasDS_8_Contagemresultado_dataentregareal1 ,
                                              DateTime AV82ExtraWWContagemResultadoRelComparacaoDemandasDS_9_Contagemresultado_dataentregareal_to1 ,
                                              String AV83ExtraWWContagemResultadoRelComparacaoDemandasDS_10_Contagemresultado_statusdmn1 ,
                                              short AV84ExtraWWContagemResultadoRelComparacaoDemandasDS_11_Contagemresultado_statuscnt1 ,
                                              String AV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1 ,
                                              int AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1 ,
                                              String AV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1 ,
                                              bool AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 ,
                                              String AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2 ,
                                              short AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 ,
                                              String AV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2 ,
                                              DateTime AV92ExtraWWContagemResultadoRelComparacaoDemandasDS_19_Contagemresultado_datadmn2 ,
                                              DateTime AV93ExtraWWContagemResultadoRelComparacaoDemandasDS_20_Contagemresultado_datadmn_to2 ,
                                              DateTime AV94ExtraWWContagemResultadoRelComparacaoDemandasDS_21_Contagemresultado_dataentregareal2 ,
                                              DateTime AV95ExtraWWContagemResultadoRelComparacaoDemandasDS_22_Contagemresultado_dataentregareal_to2 ,
                                              String AV96ExtraWWContagemResultadoRelComparacaoDemandasDS_23_Contagemresultado_statusdmn2 ,
                                              short AV97ExtraWWContagemResultadoRelComparacaoDemandasDS_24_Contagemresultado_statuscnt2 ,
                                              String AV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2 ,
                                              int AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2 ,
                                              String AV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2 ,
                                              bool AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 ,
                                              String AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3 ,
                                              short AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 ,
                                              String AV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3 ,
                                              DateTime AV105ExtraWWContagemResultadoRelComparacaoDemandasDS_32_Contagemresultado_datadmn3 ,
                                              DateTime AV106ExtraWWContagemResultadoRelComparacaoDemandasDS_33_Contagemresultado_datadmn_to3 ,
                                              DateTime AV107ExtraWWContagemResultadoRelComparacaoDemandasDS_34_Contagemresultado_dataentregareal3 ,
                                              DateTime AV108ExtraWWContagemResultadoRelComparacaoDemandasDS_35_Contagemresultado_dataentregareal_to3 ,
                                              String AV109ExtraWWContagemResultadoRelComparacaoDemandasDS_36_Contagemresultado_statusdmn3 ,
                                              short AV110ExtraWWContagemResultadoRelComparacaoDemandasDS_37_Contagemresultado_statuscnt3 ,
                                              String AV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3 ,
                                              int AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3 ,
                                              String AV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3 ,
                                              int AV115ExtraWWContagemResultadoRelComparacaoDemandasDS_42_Contagemresultado_servicogrupo ,
                                              int AV116ExtraWWContagemResultadoRelComparacaoDemandasDS_43_Contagemresultado_servico ,
                                              int AV117ExtraWWContagemResultadoRelComparacaoDemandasDS_44_Contagemresultado_cntadaosvinc ,
                                              int AV118ExtraWWContagemResultadoRelComparacaoDemandasDS_45_Contagemresultado_sergrupovinc ,
                                              int AV119ExtraWWContagemResultadoRelComparacaoDemandasDS_46_Contagemresultado_codsrvvnc ,
                                              String A493ContagemResultado_DemandaFM ,
                                              DateTime A471ContagemResultado_DataDmn ,
                                              DateTime A2017ContagemResultado_DataEntregaReal ,
                                              String A484ContagemResultado_StatusDmn ,
                                              short A483ContagemResultado_StatusCnt ,
                                              String A494ContagemResultado_Descricao ,
                                              int A489ContagemResultado_SistemaCod ,
                                              String A1046ContagemResultado_Agrupador ,
                                              int A764ContagemResultado_ServicoGrupo ,
                                              int A601ContagemResultado_Servico ,
                                              int A2145ContagemResultado_CntadaOsVinc ,
                                              int A2146ContagemResultado_SerGrupoVinc ,
                                              int A1591ContagemResultado_CodSrvVnc ,
                                              int A602ContagemResultado_OSVinculada ,
                                              int A52Contratada_AreaTrabalhoCod ,
                                              int AV6WWPContext_gxTpr_Areatrabalho_codigo ,
                                              int A490ContagemResultado_ContratadaCod ,
                                              int AV114ExtraWWContagemResultadoRelComparacaoDemandasDS_41_Contagemresultado_contratadacod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [67] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (((((((([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [ContratoServicos] T5 WITH (NOLOCK) ON T5.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T6 WITH (NOLOCK) ON T6.[Servico_Codigo] = T5.[Servico_Codigo]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) LEFT JOIN [Sistema] T4 WITH (NOLOCK) ON T4.[Sistema_Codigo] = T2.[ContagemResultado_SistemaCod]) LEFT JOIN [ContagemResultado] T7 WITH (NOLOCK) ON T7.[ContagemResultado_Codigo] = T2.[ContagemResultado_OSVinculada]) LEFT JOIN [ContratoServicos] T8 WITH (NOLOCK) ON T8.[ContratoServicos_Codigo] = T7.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T9 WITH (NOLOCK) ON T9.[Servico_Codigo] = T8.[Servico_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T2.[ContagemResultado_OSVinculada] > 0)";
         scmdbuf = scmdbuf + " and (T3.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         scmdbuf = scmdbuf + " and (T2.[ContagemResultado_ContratadaCod] = @AV114ExtraWWContagemResultadoRelComparacaoDemandasDS_41_Contagemresultado_contratadacod)";
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like '%' + @lV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! (DateTime.MinValue==AV79ExtraWWContagemResultadoRelComparacaoDemandasDS_6_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV79ExtraWWContagemResultadoRelComparacaoDemandasDS_6_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! (DateTime.MinValue==AV80ExtraWWContagemResultadoRelComparacaoDemandasDS_7_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV80ExtraWWContagemResultadoRelComparacaoDemandasDS_7_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! (DateTime.MinValue==AV81ExtraWWContagemResultadoRelComparacaoDemandasDS_8_Contagemresultado_dataentregareal1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] < @AV81ExtraWWContagemResultadoRelComparacaoDemandasDS_8_Contagemresultado_dataentregareal1)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 1 ) || ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 2 ) || ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 3 ) ) && ( ! (DateTime.MinValue==AV81ExtraWWContagemResultadoRelComparacaoDemandasDS_8_Contagemresultado_dataentregareal1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] >= @AV81ExtraWWContagemResultadoRelComparacaoDemandasDS_8_Contagemresultado_dataentregareal1)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! (DateTime.MinValue==AV82ExtraWWContagemResultadoRelComparacaoDemandasDS_9_Contagemresultado_dataentregareal_to1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] < @AV82ExtraWWContagemResultadoRelComparacaoDemandasDS_9_Contagemresultado_dataentregareal_to1)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 3 ) && ( ! (DateTime.MinValue==AV82ExtraWWContagemResultadoRelComparacaoDemandasDS_9_Contagemresultado_dataentregareal_to1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] <= @AV82ExtraWWContagemResultadoRelComparacaoDemandasDS_9_Contagemresultado_dataentregareal_to1)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83ExtraWWContagemResultadoRelComparacaoDemandasDS_10_Contagemresultado_statusdmn1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_StatusDmn] = @AV83ExtraWWContagemResultadoRelComparacaoDemandasDS_10_Contagemresultado_statusdmn1)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_STATUSCNT") == 0 ) && ( ! (0==AV84ExtraWWContagemResultadoRelComparacaoDemandasDS_11_Contagemresultado_statuscnt1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusCnt] = @AV84ExtraWWContagemResultadoRelComparacaoDemandasDS_11_Contagemresultado_statuscnt1)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like @lV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1)";
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1)";
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] = @AV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1)";
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! (0==AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] < @AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! (0==AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 2 ) && ( ! (0==AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] > @AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1)";
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] like @lV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1)";
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ( StringUtil.StrCmp(AV76ExtraWWContagemResultadoRelComparacaoDemandasDS_3_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV77ExtraWWContagemResultadoRelComparacaoDemandasDS_4_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] like '%' + @lV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1)";
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like '%' + @lV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2)";
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 0 ) && ( ! (DateTime.MinValue==AV92ExtraWWContagemResultadoRelComparacaoDemandasDS_19_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV92ExtraWWContagemResultadoRelComparacaoDemandasDS_19_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int3[25] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 0 ) && ( ! (DateTime.MinValue==AV93ExtraWWContagemResultadoRelComparacaoDemandasDS_20_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV93ExtraWWContagemResultadoRelComparacaoDemandasDS_20_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int3[26] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 0 ) && ( ! (DateTime.MinValue==AV94ExtraWWContagemResultadoRelComparacaoDemandasDS_21_Contagemresultado_dataentregareal2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] < @AV94ExtraWWContagemResultadoRelComparacaoDemandasDS_21_Contagemresultado_dataentregareal2)";
         }
         else
         {
            GXv_int3[27] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 1 ) || ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 2 ) || ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 3 ) ) && ( ! (DateTime.MinValue==AV94ExtraWWContagemResultadoRelComparacaoDemandasDS_21_Contagemresultado_dataentregareal2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] >= @AV94ExtraWWContagemResultadoRelComparacaoDemandasDS_21_Contagemresultado_dataentregareal2)";
         }
         else
         {
            GXv_int3[28] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 1 ) && ( ! (DateTime.MinValue==AV95ExtraWWContagemResultadoRelComparacaoDemandasDS_22_Contagemresultado_dataentregareal_to2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] < @AV95ExtraWWContagemResultadoRelComparacaoDemandasDS_22_Contagemresultado_dataentregareal_to2)";
         }
         else
         {
            GXv_int3[29] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 3 ) && ( ! (DateTime.MinValue==AV95ExtraWWContagemResultadoRelComparacaoDemandasDS_22_Contagemresultado_dataentregareal_to2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] <= @AV95ExtraWWContagemResultadoRelComparacaoDemandasDS_22_Contagemresultado_dataentregareal_to2)";
         }
         else
         {
            GXv_int3[30] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96ExtraWWContagemResultadoRelComparacaoDemandasDS_23_Contagemresultado_statusdmn2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_StatusDmn] = @AV96ExtraWWContagemResultadoRelComparacaoDemandasDS_23_Contagemresultado_statusdmn2)";
         }
         else
         {
            GXv_int3[31] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_STATUSCNT") == 0 ) && ( ! (0==AV97ExtraWWContagemResultadoRelComparacaoDemandasDS_24_Contagemresultado_statuscnt2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusCnt] = @AV97ExtraWWContagemResultadoRelComparacaoDemandasDS_24_Contagemresultado_statuscnt2)";
         }
         else
         {
            GXv_int3[32] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like @lV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2)";
         }
         else
         {
            GXv_int3[33] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2)";
         }
         else
         {
            GXv_int3[34] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] = @AV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2)";
         }
         else
         {
            GXv_int3[35] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 0 ) && ( ! (0==AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] < @AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int3[36] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 1 ) && ( ! (0==AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int3[37] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 2 ) && ( ! (0==AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] > @AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int3[38] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2)";
         }
         else
         {
            GXv_int3[39] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] like @lV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2)";
         }
         else
         {
            GXv_int3[40] = 1;
         }
         if ( AV88ExtraWWContagemResultadoRelComparacaoDemandasDS_15_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89ExtraWWContagemResultadoRelComparacaoDemandasDS_16_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV90ExtraWWContagemResultadoRelComparacaoDemandasDS_17_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] like '%' + @lV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2)";
         }
         else
         {
            GXv_int3[41] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] = @AV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int3[42] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like @lV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int3[43] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DemandaFM] like '%' + @lV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3)";
         }
         else
         {
            GXv_int3[44] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 0 ) && ( ! (DateTime.MinValue==AV105ExtraWWContagemResultadoRelComparacaoDemandasDS_32_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV105ExtraWWContagemResultadoRelComparacaoDemandasDS_32_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int3[45] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 0 ) && ( ! (DateTime.MinValue==AV106ExtraWWContagemResultadoRelComparacaoDemandasDS_33_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV106ExtraWWContagemResultadoRelComparacaoDemandasDS_33_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int3[46] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 0 ) && ( ! (DateTime.MinValue==AV107ExtraWWContagemResultadoRelComparacaoDemandasDS_34_Contagemresultado_dataentregareal3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] < @AV107ExtraWWContagemResultadoRelComparacaoDemandasDS_34_Contagemresultado_dataentregareal3)";
         }
         else
         {
            GXv_int3[47] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 1 ) || ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 2 ) || ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 3 ) ) && ( ! (DateTime.MinValue==AV107ExtraWWContagemResultadoRelComparacaoDemandasDS_34_Contagemresultado_dataentregareal3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] >= @AV107ExtraWWContagemResultadoRelComparacaoDemandasDS_34_Contagemresultado_dataentregareal3)";
         }
         else
         {
            GXv_int3[48] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 1 ) && ( ! (DateTime.MinValue==AV108ExtraWWContagemResultadoRelComparacaoDemandasDS_35_Contagemresultado_dataentregareal_to3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] < @AV108ExtraWWContagemResultadoRelComparacaoDemandasDS_35_Contagemresultado_dataentregareal_to3)";
         }
         else
         {
            GXv_int3[49] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAENTREGAREAL") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 3 ) && ( ! (DateTime.MinValue==AV108ExtraWWContagemResultadoRelComparacaoDemandasDS_35_Contagemresultado_dataentregareal_to3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataEntregaReal] <= @AV108ExtraWWContagemResultadoRelComparacaoDemandasDS_35_Contagemresultado_dataentregareal_to3)";
         }
         else
         {
            GXv_int3[50] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109ExtraWWContagemResultadoRelComparacaoDemandasDS_36_Contagemresultado_statusdmn3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_StatusDmn] = @AV109ExtraWWContagemResultadoRelComparacaoDemandasDS_36_Contagemresultado_statusdmn3)";
         }
         else
         {
            GXv_int3[51] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_STATUSCNT") == 0 ) && ( ! (0==AV110ExtraWWContagemResultadoRelComparacaoDemandasDS_37_Contagemresultado_statuscnt3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusCnt] = @AV110ExtraWWContagemResultadoRelComparacaoDemandasDS_37_Contagemresultado_statuscnt3)";
         }
         else
         {
            GXv_int3[52] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like @lV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3)";
         }
         else
         {
            GXv_int3[53] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3)";
         }
         else
         {
            GXv_int3[54] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] = @AV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3)";
         }
         else
         {
            GXv_int3[55] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 0 ) && ( ! (0==AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] < @AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int3[56] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 1 ) && ( ! (0==AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int3[57] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 2 ) && ( ! (0==AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] > @AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int3[58] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3)";
         }
         else
         {
            GXv_int3[59] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] like @lV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3)";
         }
         else
         {
            GXv_int3[60] = 1;
         }
         if ( AV101ExtraWWContagemResultadoRelComparacaoDemandasDS_28_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV102ExtraWWContagemResultadoRelComparacaoDemandasDS_29_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( AV103ExtraWWContagemResultadoRelComparacaoDemandasDS_30_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] like '%' + @lV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3)";
         }
         else
         {
            GXv_int3[61] = 1;
         }
         if ( ! (0==AV115ExtraWWContagemResultadoRelComparacaoDemandasDS_42_Contagemresultado_servicogrupo) )
         {
            sWhereString = sWhereString + " and (T6.[ServicoGrupo_Codigo] = @AV115ExtraWWContagemResultadoRelComparacaoDemandasDS_42_Contagemresultado_servicogrupo)";
         }
         else
         {
            GXv_int3[62] = 1;
         }
         if ( ! (0==AV116ExtraWWContagemResultadoRelComparacaoDemandasDS_43_Contagemresultado_servico) )
         {
            sWhereString = sWhereString + " and (T5.[Servico_Codigo] = @AV116ExtraWWContagemResultadoRelComparacaoDemandasDS_43_Contagemresultado_servico)";
         }
         else
         {
            GXv_int3[63] = 1;
         }
         if ( ! (0==AV117ExtraWWContagemResultadoRelComparacaoDemandasDS_44_Contagemresultado_cntadaosvinc) )
         {
            sWhereString = sWhereString + " and (T7.[ContagemResultado_ContratadaCod] = @AV117ExtraWWContagemResultadoRelComparacaoDemandasDS_44_Contagemresultado_cntadaosvinc)";
         }
         else
         {
            GXv_int3[64] = 1;
         }
         if ( ! (0==AV118ExtraWWContagemResultadoRelComparacaoDemandasDS_45_Contagemresultado_sergrupovinc) )
         {
            sWhereString = sWhereString + " and (T9.[ServicoGrupo_Codigo] = @AV118ExtraWWContagemResultadoRelComparacaoDemandasDS_45_Contagemresultado_sergrupovinc)";
         }
         else
         {
            GXv_int3[65] = 1;
         }
         if ( ! (0==AV119ExtraWWContagemResultadoRelComparacaoDemandasDS_46_Contagemresultado_codsrvvnc) )
         {
            sWhereString = sWhereString + " and (T8.[Servico_Codigo] = @AV119ExtraWWContagemResultadoRelComparacaoDemandasDS_46_Contagemresultado_codsrvvnc)";
         }
         else
         {
            GXv_int3[66] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + "";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 9 :
                     return conditional_H00TB11(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (String)dynConstraints[11] , (bool)dynConstraints[12] , (String)dynConstraints[13] , (short)dynConstraints[14] , (String)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (DateTime)dynConstraints[19] , (String)dynConstraints[20] , (short)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (bool)dynConstraints[25] , (String)dynConstraints[26] , (short)dynConstraints[27] , (String)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (DateTime)dynConstraints[31] , (DateTime)dynConstraints[32] , (String)dynConstraints[33] , (short)dynConstraints[34] , (String)dynConstraints[35] , (int)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (int)dynConstraints[41] , (int)dynConstraints[42] , (String)dynConstraints[43] , (DateTime)dynConstraints[44] , (DateTime)dynConstraints[45] , (String)dynConstraints[46] , (short)dynConstraints[47] , (String)dynConstraints[48] , (int)dynConstraints[49] , (String)dynConstraints[50] , (int)dynConstraints[51] , (int)dynConstraints[52] , (int)dynConstraints[53] , (int)dynConstraints[54] , (int)dynConstraints[55] , (int)dynConstraints[56] , (int)dynConstraints[57] , (int)dynConstraints[58] , (int)dynConstraints[59] , (int)dynConstraints[60] );
               case 10 :
                     return conditional_H00TB12(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (String)dynConstraints[11] , (bool)dynConstraints[12] , (String)dynConstraints[13] , (short)dynConstraints[14] , (String)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (DateTime)dynConstraints[19] , (String)dynConstraints[20] , (short)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (bool)dynConstraints[25] , (String)dynConstraints[26] , (short)dynConstraints[27] , (String)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (DateTime)dynConstraints[31] , (DateTime)dynConstraints[32] , (String)dynConstraints[33] , (short)dynConstraints[34] , (String)dynConstraints[35] , (int)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (int)dynConstraints[40] , (int)dynConstraints[41] , (int)dynConstraints[42] , (String)dynConstraints[43] , (DateTime)dynConstraints[44] , (DateTime)dynConstraints[45] , (String)dynConstraints[46] , (short)dynConstraints[47] , (String)dynConstraints[48] , (int)dynConstraints[49] , (String)dynConstraints[50] , (int)dynConstraints[51] , (int)dynConstraints[52] , (int)dynConstraints[53] , (int)dynConstraints[54] , (int)dynConstraints[55] , (int)dynConstraints[56] , (int)dynConstraints[57] , (int)dynConstraints[58] , (int)dynConstraints[59] , (int)dynConstraints[60] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00TB2 ;
          prmH00TB2 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00TB3 ;
          prmH00TB3 = new Object[] {
          new Object[] {"@AV66ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00TB4 ;
          prmH00TB4 = new Object[] {
          new Object[] {"@AV66ContagemResultado_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV67ContagemResultado_ServicoGrupo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00TB5 ;
          prmH00TB5 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00TB6 ;
          prmH00TB6 = new Object[] {
          new Object[] {"@AV69ContagemResultado_CntadaOsVinc",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00TB7 ;
          prmH00TB7 = new Object[] {
          new Object[] {"@AV69ContagemResultado_CntadaOsVinc",SqlDbType.Int,6,0} ,
          new Object[] {"@AV71ContagemResultado_SerGrupoVinc",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00TB8 ;
          prmH00TB8 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00TB9 ;
          prmH00TB9 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00TB10 ;
          prmH00TB10 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00TB11 ;
          prmH00TB11 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV114ExtraWWContagemResultadoRelComparacaoDemandasDS_41_Contagemresultado_contratadacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV79ExtraWWContagemResultadoRelComparacaoDemandasDS_6_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV80ExtraWWContagemResultadoRelComparacaoDemandasDS_7_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV81ExtraWWContagemResultadoRelComparacaoDemandasDS_8_Contagemresultado_dataentregareal1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV81ExtraWWContagemResultadoRelComparacaoDemandasDS_8_Contagemresultado_dataentregareal1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV82ExtraWWContagemResultadoRelComparacaoDemandasDS_9_Contagemresultado_dataentregareal_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV82ExtraWWContagemResultadoRelComparacaoDemandasDS_9_Contagemresultado_dataentregareal_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV83ExtraWWContagemResultadoRelComparacaoDemandasDS_10_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV84ExtraWWContagemResultadoRelComparacaoDemandasDS_11_Contagemresultado_statuscnt1",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@AV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV92ExtraWWContagemResultadoRelComparacaoDemandasDS_19_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV93ExtraWWContagemResultadoRelComparacaoDemandasDS_20_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV94ExtraWWContagemResultadoRelComparacaoDemandasDS_21_Contagemresultado_dataentregareal2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV94ExtraWWContagemResultadoRelComparacaoDemandasDS_21_Contagemresultado_dataentregareal2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV95ExtraWWContagemResultadoRelComparacaoDemandasDS_22_Contagemresultado_dataentregareal_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV95ExtraWWContagemResultadoRelComparacaoDemandasDS_22_Contagemresultado_dataentregareal_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV96ExtraWWContagemResultadoRelComparacaoDemandasDS_23_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV97ExtraWWContagemResultadoRelComparacaoDemandasDS_24_Contagemresultado_statuscnt2",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@AV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV105ExtraWWContagemResultadoRelComparacaoDemandasDS_32_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV106ExtraWWContagemResultadoRelComparacaoDemandasDS_33_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV107ExtraWWContagemResultadoRelComparacaoDemandasDS_34_Contagemresultado_dataentregareal3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV107ExtraWWContagemResultadoRelComparacaoDemandasDS_34_Contagemresultado_dataentregareal3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV108ExtraWWContagemResultadoRelComparacaoDemandasDS_35_Contagemresultado_dataentregareal_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV108ExtraWWContagemResultadoRelComparacaoDemandasDS_35_Contagemresultado_dataentregareal_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV109ExtraWWContagemResultadoRelComparacaoDemandasDS_36_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV110ExtraWWContagemResultadoRelComparacaoDemandasDS_37_Contagemresultado_statuscnt3",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@AV115ExtraWWContagemResultadoRelComparacaoDemandasDS_42_Contagemresultado_servicogrupo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV116ExtraWWContagemResultadoRelComparacaoDemandasDS_43_Contagemresultado_servico",SqlDbType.Int,6,0} ,
          new Object[] {"@AV117ExtraWWContagemResultadoRelComparacaoDemandasDS_44_Contagemresultado_cntadaosvinc",SqlDbType.Int,6,0} ,
          new Object[] {"@AV118ExtraWWContagemResultadoRelComparacaoDemandasDS_45_Contagemresultado_sergrupovinc",SqlDbType.Int,6,0} ,
          new Object[] {"@AV119ExtraWWContagemResultadoRelComparacaoDemandasDS_46_Contagemresultado_codsrvvnc",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00TB12 ;
          prmH00TB12 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV114ExtraWWContagemResultadoRelComparacaoDemandasDS_41_Contagemresultado_contratadacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV78ExtraWWContagemResultadoRelComparacaoDemandasDS_5_Contagemresultado_demandafm1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV79ExtraWWContagemResultadoRelComparacaoDemandasDS_6_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV80ExtraWWContagemResultadoRelComparacaoDemandasDS_7_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV81ExtraWWContagemResultadoRelComparacaoDemandasDS_8_Contagemresultado_dataentregareal1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV81ExtraWWContagemResultadoRelComparacaoDemandasDS_8_Contagemresultado_dataentregareal1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV82ExtraWWContagemResultadoRelComparacaoDemandasDS_9_Contagemresultado_dataentregareal_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV82ExtraWWContagemResultadoRelComparacaoDemandasDS_9_Contagemresultado_dataentregareal_to1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV83ExtraWWContagemResultadoRelComparacaoDemandasDS_10_Contagemresultado_statusdmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV84ExtraWWContagemResultadoRelComparacaoDemandasDS_11_Contagemresultado_statuscnt1",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV85ExtraWWContagemResultadoRelComparacaoDemandasDS_12_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV86ExtraWWContagemResultadoRelComparacaoDemandasDS_13_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV87ExtraWWContagemResultadoRelComparacaoDemandasDS_14_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@AV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV91ExtraWWContagemResultadoRelComparacaoDemandasDS_18_Contagemresultado_demandafm2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV92ExtraWWContagemResultadoRelComparacaoDemandasDS_19_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV93ExtraWWContagemResultadoRelComparacaoDemandasDS_20_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV94ExtraWWContagemResultadoRelComparacaoDemandasDS_21_Contagemresultado_dataentregareal2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV94ExtraWWContagemResultadoRelComparacaoDemandasDS_21_Contagemresultado_dataentregareal2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV95ExtraWWContagemResultadoRelComparacaoDemandasDS_22_Contagemresultado_dataentregareal_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV95ExtraWWContagemResultadoRelComparacaoDemandasDS_22_Contagemresultado_dataentregareal_to2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV96ExtraWWContagemResultadoRelComparacaoDemandasDS_23_Contagemresultado_statusdmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV97ExtraWWContagemResultadoRelComparacaoDemandasDS_24_Contagemresultado_statuscnt2",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV98ExtraWWContagemResultadoRelComparacaoDemandasDS_25_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV99ExtraWWContagemResultadoRelComparacaoDemandasDS_26_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV100ExtraWWContagemResultadoRelComparacaoDemandasDS_27_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@AV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV104ExtraWWContagemResultadoRelComparacaoDemandasDS_31_Contagemresultado_demandafm3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV105ExtraWWContagemResultadoRelComparacaoDemandasDS_32_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV106ExtraWWContagemResultadoRelComparacaoDemandasDS_33_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV107ExtraWWContagemResultadoRelComparacaoDemandasDS_34_Contagemresultado_dataentregareal3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV107ExtraWWContagemResultadoRelComparacaoDemandasDS_34_Contagemresultado_dataentregareal3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV108ExtraWWContagemResultadoRelComparacaoDemandasDS_35_Contagemresultado_dataentregareal_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV108ExtraWWContagemResultadoRelComparacaoDemandasDS_35_Contagemresultado_dataentregareal_to3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV109ExtraWWContagemResultadoRelComparacaoDemandasDS_36_Contagemresultado_statusdmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV110ExtraWWContagemResultadoRelComparacaoDemandasDS_37_Contagemresultado_statuscnt3",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@lV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@lV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV111ExtraWWContagemResultadoRelComparacaoDemandasDS_38_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV112ExtraWWContagemResultadoRelComparacaoDemandasDS_39_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV113ExtraWWContagemResultadoRelComparacaoDemandasDS_40_Contagemresultado_agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@AV115ExtraWWContagemResultadoRelComparacaoDemandasDS_42_Contagemresultado_servicogrupo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV116ExtraWWContagemResultadoRelComparacaoDemandasDS_43_Contagemresultado_servico",SqlDbType.Int,6,0} ,
          new Object[] {"@AV117ExtraWWContagemResultadoRelComparacaoDemandasDS_44_Contagemresultado_cntadaosvinc",SqlDbType.Int,6,0} ,
          new Object[] {"@AV118ExtraWWContagemResultadoRelComparacaoDemandasDS_45_Contagemresultado_sergrupovinc",SqlDbType.Int,6,0} ,
          new Object[] {"@AV119ExtraWWContagemResultadoRelComparacaoDemandasDS_46_Contagemresultado_codsrvvnc",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00TB2", "SELECT [Contratada_Codigo], [Contratada_Sigla], [Contratada_AreaTrabalhoCod] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo ORDER BY [Contratada_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TB2,0,0,true,false )
             ,new CursorDef("H00TB3", "SELECT T4.[Contrato_Codigo], T2.[Servico_Codigo], T1.[ContratoServicos_Codigo], T2.[ServicoGrupo_Codigo], T3.[ServicoGrupo_Descricao], T4.[Contratada_Codigo], T1.[ContratoServicos_Ativo] FROM ((([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [ServicoGrupo] T3 WITH (NOLOCK) ON T3.[ServicoGrupo_Codigo] = T2.[ServicoGrupo_Codigo]) INNER JOIN [Contrato] T4 WITH (NOLOCK) ON T4.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T1.[ContratoServicos_Ativo] = 1) AND (T4.[Contratada_Codigo] = @AV66ContagemResultado_ContratadaCod) ORDER BY T3.[ServicoGrupo_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TB3,0,0,true,false )
             ,new CursorDef("H00TB4", "SELECT T3.[Contrato_Codigo], T1.[ContratoServicos_Codigo], T1.[Servico_Codigo], T2.[Servico_Sigla], T3.[Contratada_Codigo], T2.[ServicoGrupo_Codigo], T1.[ContratoServicos_Ativo] FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T1.[ContratoServicos_Ativo] = 1) AND (T3.[Contratada_Codigo] = @AV66ContagemResultado_ContratadaCod) AND (T2.[ServicoGrupo_Codigo] = @AV67ContagemResultado_ServicoGrupo) ORDER BY T2.[Servico_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TB4,0,0,true,false )
             ,new CursorDef("H00TB5", "SELECT [Contratada_Codigo], [Contratada_Sigla], [Contratada_AreaTrabalhoCod] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo ORDER BY [Contratada_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TB5,0,0,true,false )
             ,new CursorDef("H00TB6", "SELECT T4.[Contrato_Codigo], T2.[Servico_Codigo], T1.[ContratoServicos_Codigo], T2.[ServicoGrupo_Codigo], T3.[ServicoGrupo_Descricao], T4.[Contratada_Codigo], T1.[ContratoServicos_Ativo] FROM ((([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [ServicoGrupo] T3 WITH (NOLOCK) ON T3.[ServicoGrupo_Codigo] = T2.[ServicoGrupo_Codigo]) INNER JOIN [Contrato] T4 WITH (NOLOCK) ON T4.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T1.[ContratoServicos_Ativo] = 1) AND (T4.[Contratada_Codigo] = @AV69ContagemResultado_CntadaOsVinc) ORDER BY T3.[ServicoGrupo_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TB6,0,0,true,false )
             ,new CursorDef("H00TB7", "SELECT T3.[Contrato_Codigo], T1.[ContratoServicos_Codigo], T1.[Servico_Codigo], T2.[Servico_Sigla], T3.[Contratada_Codigo], T2.[ServicoGrupo_Codigo], T1.[ContratoServicos_Ativo] FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T1.[ContratoServicos_Ativo] = 1) AND (T3.[Contratada_Codigo] = @AV69ContagemResultado_CntadaOsVinc) AND (T2.[ServicoGrupo_Codigo] = @AV71ContagemResultado_SerGrupoVinc) ORDER BY T2.[Servico_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TB7,0,0,true,false )
             ,new CursorDef("H00TB8", "SELECT [Sistema_Codigo], [Sistema_Sigla], [Sistema_AreaTrabalhoCod], [Sistema_Ativo] FROM [Sistema] WITH (NOLOCK) WHERE ([Sistema_Ativo] = 1) AND ([Sistema_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo) ORDER BY [Sistema_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TB8,0,0,true,false )
             ,new CursorDef("H00TB9", "SELECT [Sistema_Codigo], [Sistema_Sigla], [Sistema_AreaTrabalhoCod], [Sistema_Ativo] FROM [Sistema] WITH (NOLOCK) WHERE ([Sistema_Ativo] = 1) AND ([Sistema_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo) ORDER BY [Sistema_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TB9,0,0,true,false )
             ,new CursorDef("H00TB10", "SELECT [Sistema_Codigo], [Sistema_Sigla], [Sistema_AreaTrabalhoCod], [Sistema_Ativo] FROM [Sistema] WITH (NOLOCK) WHERE ([Sistema_Ativo] = 1) AND ([Sistema_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo) ORDER BY [Sistema_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TB10,0,0,true,false )
             ,new CursorDef("H00TB11", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TB11,11,0,true,false )
             ,new CursorDef("H00TB12", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TB12,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((bool[]) buf[6])[0] = rslt.getBool(7) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 15) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((bool[]) buf[6])[0] = rslt.getBool(7) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((bool[]) buf[6])[0] = rslt.getBool(7) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 15) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((bool[]) buf[6])[0] = rslt.getBool(7) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 25) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 25) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 25) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                return;
             case 9 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 5) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((String[]) buf[20])[0] = rslt.getString(12, 15) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((int[]) buf[22])[0] = rslt.getInt(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((short[]) buf[24])[0] = rslt.getShort(14) ;
                ((String[]) buf[25])[0] = rslt.getString(15, 1) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((int[]) buf[27])[0] = rslt.getInt(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((String[]) buf[29])[0] = rslt.getString(17, 25) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(17);
                ((String[]) buf[31])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(18);
                ((DateTime[]) buf[33])[0] = rslt.getGXDateTime(19) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(19);
                ((DateTime[]) buf[35])[0] = rslt.getGXDate(20) ;
                ((String[]) buf[36])[0] = rslt.getVarchar(21) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(21);
                ((String[]) buf[38])[0] = rslt.getVarchar(22) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(22);
                ((int[]) buf[40])[0] = rslt.getInt(23) ;
                return;
             case 10 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[72]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[73]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[74]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[75]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[77]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[78]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[79]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[80]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[81]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[82]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[83]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[84]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[85]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[86]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[87]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[88]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[89]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[90]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[91]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[92]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[93]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[94]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[95]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[96]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[97]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[98]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[99]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[100]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[101]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[102]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[103]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[104]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[105]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[106]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[107]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[108]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[109]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[110]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[111]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[112]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[113]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[114]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[115]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[116]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[117]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[118]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[119]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[120]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[121]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[122]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[123]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[124]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[125]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[126]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[127]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[128]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[129]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[130]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[131]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[132]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[133]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[134]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[135]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[136]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[137]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[138]);
                }
                if ( (short)parms[67] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[139]);
                }
                if ( (short)parms[68] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[140]);
                }
                if ( (short)parms[69] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[141]);
                }
                if ( (short)parms[70] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[142]);
                }
                if ( (short)parms[71] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[143]);
                }
                return;
             case 10 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[67]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[68]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[70]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[71]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[72]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[73]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[74]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[75]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[76]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[77]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[78]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[79]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[80]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[81]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[82]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[83]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[84]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[85]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[86]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[87]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[88]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[89]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[90]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[91]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[92]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[93]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[94]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[95]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[96]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[97]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[98]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[99]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[100]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[101]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[102]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[103]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[104]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[105]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[106]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[107]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[108]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[109]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[110]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[111]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[112]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[113]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[114]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[115]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[116]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[117]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[118]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[119]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[120]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[121]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[122]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[123]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[124]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[125]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[126]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[127]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[128]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[129]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[130]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[131]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[132]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[133]);
                }
                return;
       }
    }

 }

}
