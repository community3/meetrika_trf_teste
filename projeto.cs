/*
               File: Projeto
        Description: Projeto
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:30:14.31
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class projeto : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"PROJETO_TIPOPROJETOCOD") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAPROJETO_TIPOPROJETOCOD2986( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"PROJETO_GPOOBJCTRLCODIGO") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAPROJETO_GPOOBJCTRLCODIGO2986( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"PROJETO_SISTEMACODIGO") == 0 )
         {
            ajax_req_read_hidden_sdt(GetNextPar( ), AV8WWPContext);
            A2153Projeto_GpoObjCtrlCodigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n2153Projeto_GpoObjCtrlCodigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2153Projeto_GpoObjCtrlCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2153Projeto_GpoObjCtrlCodigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAPROJETO_SISTEMACODIGO2986( AV8WWPContext, A2153Projeto_GpoObjCtrlCodigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"PROJETO_MODULOCODIGO") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAPROJETO_MODULOCODIGO2986( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"TIPODOCUMENTO_CODIGO") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLATIPODOCUMENTO_CODIGO29235( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel5"+"_"+"PROJETO_SISTEMACOD") == 0 )
         {
            A648Projeto_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX5ASAPROJETO_SISTEMACOD2986( A648Projeto_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_39") == 0 )
         {
            A983Projeto_TipoProjetoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n983Projeto_TipoProjetoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A983Projeto_TipoProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A983Projeto_TipoProjetoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_39( A983Projeto_TipoProjetoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_41") == 0 )
         {
            A2153Projeto_GpoObjCtrlCodigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n2153Projeto_GpoObjCtrlCodigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2153Projeto_GpoObjCtrlCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2153Projeto_GpoObjCtrlCodigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_41( A2153Projeto_GpoObjCtrlCodigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_42") == 0 )
         {
            A2155Projeto_SistemaCodigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n2155Projeto_SistemaCodigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2155Projeto_SistemaCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2155Projeto_SistemaCodigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_42( A2155Projeto_SistemaCodigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_44") == 0 )
         {
            A2155Projeto_SistemaCodigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n2155Projeto_SistemaCodigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2155Projeto_SistemaCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2155Projeto_SistemaCodigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_44( A2155Projeto_SistemaCodigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_43") == 0 )
         {
            A2158Projeto_ModuloCodigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n2158Projeto_ModuloCodigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2158Projeto_ModuloCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2158Projeto_ModuloCodigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_43( A2158Projeto_ModuloCodigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_40") == 0 )
         {
            A2151Projeto_AreaTrabalhoCodigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n2151Projeto_AreaTrabalhoCodigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2151Projeto_AreaTrabalhoCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2151Projeto_AreaTrabalhoCodigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_40( A2151Projeto_AreaTrabalhoCodigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_46") == 0 )
         {
            A645TipoDocumento_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n645TipoDocumento_Codigo = false;
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_46( A645TipoDocumento_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridanexos") == 0 )
         {
            nRC_GXsfl_154 = (short)(NumberUtil.Val( GetNextPar( ), "."));
            nGXsfl_154_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
            sGXsfl_154_idx = GetNextPar( );
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxnrGridanexos_newrow( ) ;
            return  ;
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Projeto_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Projeto_Codigo), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynProjeto_TipoProjetoCod.Name = "PROJETO_TIPOPROJETOCOD";
         dynProjeto_TipoProjetoCod.WebTags = "";
         cmbProjeto_Status.Name = "PROJETO_STATUS";
         cmbProjeto_Status.WebTags = "";
         cmbProjeto_Status.addItem("A", "Aberto", 0);
         cmbProjeto_Status.addItem("E", "Em Contagem", 0);
         cmbProjeto_Status.addItem("C", "Contado", 0);
         if ( cmbProjeto_Status.ItemCount > 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A658Projeto_Status)) )
            {
               A658Projeto_Status = "A";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A658Projeto_Status", A658Projeto_Status);
            }
            A658Projeto_Status = cmbProjeto_Status.getValidValue(A658Projeto_Status);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A658Projeto_Status", A658Projeto_Status);
         }
         dynProjeto_GpoObjCtrlCodigo.Name = "PROJETO_GPOOBJCTRLCODIGO";
         dynProjeto_GpoObjCtrlCodigo.WebTags = "";
         dynProjeto_SistemaCodigo.Name = "PROJETO_SISTEMACODIGO";
         dynProjeto_SistemaCodigo.WebTags = "";
         dynProjeto_ModuloCodigo.Name = "PROJETO_MODULOCODIGO";
         dynProjeto_ModuloCodigo.WebTags = "";
         chkProjeto_Incremental.Name = "PROJETO_INCREMENTAL";
         chkProjeto_Incremental.WebTags = "";
         chkProjeto_Incremental.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkProjeto_Incremental_Internalname, "TitleCaption", chkProjeto_Incremental.Caption);
         chkProjeto_Incremental.CheckedValue = "false";
         GXCCtl = "TIPODOCUMENTO_CODIGO_" + sGXsfl_154_idx;
         dynTipoDocumento_Codigo.Name = GXCCtl;
         dynTipoDocumento_Codigo.WebTags = "";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Projeto", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtProjeto_Sigla_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public projeto( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public projeto( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Projeto_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Projeto_Codigo = aP1_Projeto_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynProjeto_TipoProjetoCod = new GXCombobox();
         cmbProjeto_Status = new GXCombobox();
         dynProjeto_GpoObjCtrlCodigo = new GXCombobox();
         dynProjeto_SistemaCodigo = new GXCombobox();
         dynProjeto_ModuloCodigo = new GXCombobox();
         chkProjeto_Incremental = new GXCheckbox();
         dynTipoDocumento_Codigo = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynProjeto_TipoProjetoCod.ItemCount > 0 )
         {
            A983Projeto_TipoProjetoCod = (int)(NumberUtil.Val( dynProjeto_TipoProjetoCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A983Projeto_TipoProjetoCod), 6, 0))), "."));
            n983Projeto_TipoProjetoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A983Projeto_TipoProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A983Projeto_TipoProjetoCod), 6, 0)));
         }
         if ( cmbProjeto_Status.ItemCount > 0 )
         {
            A658Projeto_Status = cmbProjeto_Status.getValidValue(A658Projeto_Status);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A658Projeto_Status", A658Projeto_Status);
         }
         if ( dynProjeto_GpoObjCtrlCodigo.ItemCount > 0 )
         {
            A2153Projeto_GpoObjCtrlCodigo = (int)(NumberUtil.Val( dynProjeto_GpoObjCtrlCodigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2153Projeto_GpoObjCtrlCodigo), 6, 0))), "."));
            n2153Projeto_GpoObjCtrlCodigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2153Projeto_GpoObjCtrlCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2153Projeto_GpoObjCtrlCodigo), 6, 0)));
         }
         if ( dynProjeto_SistemaCodigo.ItemCount > 0 )
         {
            A2155Projeto_SistemaCodigo = (int)(NumberUtil.Val( dynProjeto_SistemaCodigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2155Projeto_SistemaCodigo), 6, 0))), "."));
            n2155Projeto_SistemaCodigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2155Projeto_SistemaCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2155Projeto_SistemaCodigo), 6, 0)));
         }
         if ( dynProjeto_ModuloCodigo.ItemCount > 0 )
         {
            A2158Projeto_ModuloCodigo = (int)(NumberUtil.Val( dynProjeto_ModuloCodigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2158Projeto_ModuloCodigo), 6, 0))), "."));
            n2158Projeto_ModuloCodigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2158Projeto_ModuloCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2158Projeto_ModuloCodigo), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2986( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2986e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProjeto_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A648Projeto_Codigo), 6, 0, ",", "")), ((edtProjeto_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A648Projeto_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A648Projeto_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtProjeto_Codigo_Visible, edtProjeto_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Projeto.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2986( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2986( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2986e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_164_2986( true) ;
         }
         return  ;
      }

      protected void wb_table3_164_2986e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2986e( true) ;
         }
         else
         {
            wb_table1_2_2986e( false) ;
         }
      }

      protected void wb_table3_164_2986( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 167,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 169,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 171,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_164_2986e( true) ;
         }
         else
         {
            wb_table3_164_2986e( false) ;
         }
      }

      protected void wb_table2_5_2986( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_11_2986( true) ;
         }
         return  ;
      }

      protected void wb_table4_11_2986e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2986e( true) ;
         }
         else
         {
            wb_table2_5_2986e( false) ;
         }
      }

      protected void wb_table4_11_2986( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"TitleTabProjeto"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Projeto") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"TabProjeto"+"\" style=\"display:none;\">") ;
            wb_table5_17_2986( true) ;
         }
         return  ;
      }

      protected void wb_table5_17_2986e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"TitleTabEquipe"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Equipe") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"TabEquipe"+"\" style=\"display:none;\">") ;
            wb_table6_60_2986( true) ;
         }
         return  ;
      }

      protected void wb_table6_60_2986e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"TitleTabObjeto"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Objeto") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"TabObjeto"+"\" style=\"display:none;\">") ;
            wb_table7_69_2986( true) ;
         }
         return  ;
      }

      protected void wb_table7_69_2986e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"TitleTabEstimativa"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Estimativa") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"TabEstimativa"+"\" style=\"display:none;\">") ;
            wb_table8_87_2986( true) ;
         }
         return  ;
      }

      protected void wb_table8_87_2986e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"TitleTabAnexos"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Anexos") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABMAINContainer"+"TabAnexos"+"\" style=\"display:none;\">") ;
            wb_table9_145_2986( true) ;
         }
         return  ;
      }

      protected void wb_table9_145_2986e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_2986e( true) ;
         }
         else
         {
            wb_table4_11_2986e( false) ;
         }
      }

      protected void wb_table9_145_2986( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableanexos_Internalname, tblTableanexos_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            wb_table10_148_2986( true) ;
         }
         return  ;
      }

      protected void wb_table10_148_2986e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridanexosContainer.AddObjectProperty("GridName", "Gridanexos");
            GridanexosContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
            GridanexosContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
            GridanexosContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
            GridanexosContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridanexos_Backcolorstyle), 1, 0, ".", "")));
            GridanexosContainer.AddObjectProperty("CmpContext", "");
            GridanexosContainer.AddObjectProperty("InMasterPage", "false");
            GridanexosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridanexosColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2163ProjetoAnexos_Codigo), 6, 0, ".", "")));
            GridanexosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProjetoAnexos_Codigo_Enabled), 5, 0, ".", "")));
            GridanexosContainer.AddColumnProperties(GridanexosColumn);
            GridanexosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridanexosColumn.AddObjectProperty("Value", A2165ProjetoAnexos_Arquivo);
            GridanexosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProjetoAnexos_Arquivo_Enabled), 5, 0, ".", "")));
            GridanexosContainer.AddColumnProperties(GridanexosColumn);
            GridanexosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridanexosColumn.AddObjectProperty("Value", StringUtil.RTrim( A2166ProjetoAnexos_ArquivoNome));
            GridanexosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProjetoAnexos_ArquivoNome_Enabled), 5, 0, ".", "")));
            GridanexosContainer.AddColumnProperties(GridanexosColumn);
            GridanexosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridanexosColumn.AddObjectProperty("Value", StringUtil.RTrim( A646TipoDocumento_Nome));
            GridanexosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTipoDocumento_Nome_Enabled), 5, 0, ".", "")));
            GridanexosContainer.AddColumnProperties(GridanexosColumn);
            GridanexosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridanexosColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A645TipoDocumento_Codigo), 6, 0, ".", "")));
            GridanexosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(dynTipoDocumento_Codigo.Enabled), 5, 0, ".", "")));
            GridanexosContainer.AddColumnProperties(GridanexosColumn);
            GridanexosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridanexosColumn.AddObjectProperty("Value", StringUtil.RTrim( A2167ProjetoAnexos_ArquivoTipo));
            GridanexosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProjetoAnexos_ArquivoTipo_Enabled), 5, 0, ".", "")));
            GridanexosContainer.AddColumnProperties(GridanexosColumn);
            GridanexosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridanexosColumn.AddObjectProperty("Value", context.localUtil.TToC( A2169ProjetoAnexos_Data, 10, 8, 0, 3, "/", ":", " "));
            GridanexosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProjetoAnexos_Data_Enabled), 5, 0, ".", "")));
            GridanexosContainer.AddColumnProperties(GridanexosColumn);
            GridanexosContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridanexos_Allowselection), 1, 0, ".", "")));
            GridanexosContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridanexos_Selectioncolor), 9, 0, ".", "")));
            GridanexosContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridanexos_Allowhovering), 1, 0, ".", "")));
            GridanexosContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridanexos_Hoveringcolor), 9, 0, ".", "")));
            GridanexosContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridanexos_Allowcollapsing), 1, 0, ".", "")));
            GridanexosContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridanexos_Collapsed), 1, 0, ".", "")));
            nGXsfl_154_idx = 0;
            if ( ( nKeyPressed == 1 ) && ( AnyError == 0 ) )
            {
               /* Enter key processing. */
               nBlankRcdCount235 = (short)(subGridanexos_Rows);
               if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
               {
                  /* Display confirmed (stored) records */
                  nRcdExists_235 = 1;
                  ScanStart29235( ) ;
                  while ( RcdFound235 != 0 )
                  {
                     init_level_properties235( ) ;
                     getByPrimaryKey29235( ) ;
                     AddRow29235( ) ;
                     ScanNext29235( ) ;
                  }
                  ScanEnd29235( ) ;
                  nBlankRcdCount235 = (short)(subGridanexos_Rows);
               }
            }
            else if ( ( nKeyPressed == 3 ) || ( nKeyPressed == 4 ) || ( ( nKeyPressed == 1 ) && ( AnyError != 0 ) ) )
            {
               /* Button check  or addlines. */
               B2170Projeto_AnexoSequecial = A2170Projeto_AnexoSequecial;
               n2170Projeto_AnexoSequecial = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2170Projeto_AnexoSequecial", StringUtil.LTrim( StringUtil.Str( (decimal)(A2170Projeto_AnexoSequecial), 4, 0)));
               standaloneNotModal29235( ) ;
               standaloneModal29235( ) ;
               sMode235 = Gx_mode;
               while ( nGXsfl_154_idx < nRC_GXsfl_154 )
               {
                  ReadRow29235( ) ;
                  edtProjetoAnexos_Codigo_Enabled = (int)(context.localUtil.CToN( cgiGet( "PROJETOANEXOS_CODIGO_"+sGXsfl_154_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoAnexos_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoAnexos_Codigo_Enabled), 5, 0)));
                  edtProjetoAnexos_Arquivo_Enabled = (int)(context.localUtil.CToN( cgiGet( "PROJETOANEXOS_ARQUIVO_"+sGXsfl_154_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoAnexos_Arquivo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoAnexos_Arquivo_Enabled), 5, 0)));
                  edtProjetoAnexos_ArquivoNome_Enabled = (int)(context.localUtil.CToN( cgiGet( "PROJETOANEXOS_ARQUIVONOME_"+sGXsfl_154_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoAnexos_ArquivoNome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoAnexos_ArquivoNome_Enabled), 5, 0)));
                  edtTipoDocumento_Nome_Enabled = (int)(context.localUtil.CToN( cgiGet( "TIPODOCUMENTO_NOME_"+sGXsfl_154_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoDocumento_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoDocumento_Nome_Enabled), 5, 0)));
                  dynTipoDocumento_Codigo.Enabled = (int)(context.localUtil.CToN( cgiGet( "TIPODOCUMENTO_CODIGO_"+sGXsfl_154_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTipoDocumento_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynTipoDocumento_Codigo.Enabled), 5, 0)));
                  edtProjetoAnexos_ArquivoTipo_Enabled = (int)(context.localUtil.CToN( cgiGet( "PROJETOANEXOS_ARQUIVOTIPO_"+sGXsfl_154_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoAnexos_ArquivoTipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoAnexos_ArquivoTipo_Enabled), 5, 0)));
                  edtProjetoAnexos_Data_Enabled = (int)(context.localUtil.CToN( cgiGet( "PROJETOANEXOS_DATA_"+sGXsfl_154_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoAnexos_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoAnexos_Data_Enabled), 5, 0)));
                  if ( ( nRcdExists_235 == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     standaloneModal29235( ) ;
                  }
                  SendRow29235( ) ;
               }
               Gx_mode = sMode235;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               A2170Projeto_AnexoSequecial = B2170Projeto_AnexoSequecial;
               n2170Projeto_AnexoSequecial = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2170Projeto_AnexoSequecial", StringUtil.LTrim( StringUtil.Str( (decimal)(A2170Projeto_AnexoSequecial), 4, 0)));
            }
            else
            {
               /* Get or get-alike key processing. */
               nBlankRcdCount235 = (short)(subGridanexos_Rows);
               nRcdExists_235 = 1;
               if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
               {
                  ScanStart29235( ) ;
                  while ( RcdFound235 != 0 )
                  {
                     sGXsfl_154_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_154_idx+1), 4, 0)), 4, "0");
                     SubsflControlProps_154235( ) ;
                     init_level_properties235( ) ;
                     standaloneNotModal29235( ) ;
                     getByPrimaryKey29235( ) ;
                     standaloneModal29235( ) ;
                     AddRow29235( ) ;
                     ScanNext29235( ) ;
                  }
                  ScanEnd29235( ) ;
               }
            }
            /* Initialize fields for 'new' records and send them. */
            if ( ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 ) && ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 ) )
            {
               sMode235 = Gx_mode;
               Gx_mode = "INS";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               sGXsfl_154_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_154_idx+1), 4, 0)), 4, "0");
               SubsflControlProps_154235( ) ;
               InitAll29235( ) ;
               init_level_properties235( ) ;
               B2170Projeto_AnexoSequecial = A2170Projeto_AnexoSequecial;
               n2170Projeto_AnexoSequecial = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2170Projeto_AnexoSequecial", StringUtil.LTrim( StringUtil.Str( (decimal)(A2170Projeto_AnexoSequecial), 4, 0)));
               standaloneNotModal29235( ) ;
               standaloneModal29235( ) ;
               nRcdExists_235 = 0;
               nIsMod_235 = 0;
               nRcdDeleted_235 = 0;
               nBlankRcdCount235 = (short)(nBlankRcdUsr235+nBlankRcdCount235);
               fRowAdded = 0;
               while ( nBlankRcdCount235 > 0 )
               {
                  AddRow29235( ) ;
                  if ( ( nKeyPressed == 4 ) && ( fRowAdded == 0 ) )
                  {
                     fRowAdded = 1;
                     GX_FocusControl = edtProjetoAnexos_Arquivo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  nBlankRcdCount235 = (short)(nBlankRcdCount235-1);
               }
               Gx_mode = sMode235;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               A2170Projeto_AnexoSequecial = B2170Projeto_AnexoSequecial;
               n2170Projeto_AnexoSequecial = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2170Projeto_AnexoSequecial", StringUtil.LTrim( StringUtil.Str( (decimal)(A2170Projeto_AnexoSequecial), 4, 0)));
            }
            sStyleString = "";
            context.WriteHtmlText( "<div id=\""+"GridanexosContainer"+"Div\" "+sStyleString+">"+"</div>") ;
            context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridanexos", GridanexosContainer);
            if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
            {
               GxWebStd.gx_hidden_field( context, "GridanexosContainerData", GridanexosContainer.ToJavascriptSource());
            }
            if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
            {
               GxWebStd.gx_hidden_field( context, "GridanexosContainerData"+"V", GridanexosContainer.GridValuesHidden());
            }
            else
            {
               context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridanexosContainerData"+"V"+"\" value='"+GridanexosContainer.GridValuesHidden()+"'/>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_145_2986e( true) ;
         }
         else
         {
            wb_table9_145_2986e( false) ;
         }
      }

      protected void wb_table10_148_2986( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions2_Internalname, tblTableactions2_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 151,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBntanexos_Internalname, context.GetImagePath( "35d9ce8e-0cd2-4075-b379-43e1e9bf02ad", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBntanexos_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBntanexos_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOBNTANEXOS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_148_2986e( true) ;
         }
         else
         {
            wb_table10_148_2986e( false) ;
         }
      }

      protected void wb_table8_87_2986( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableestimativa_Internalname, tblTableestimativa_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table11_90_2986( true) ;
         }
         return  ;
      }

      protected void wb_table11_90_2986e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table12_118_2986( true) ;
         }
         return  ;
      }

      protected void wb_table12_118_2986e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_87_2986e( true) ;
         }
         else
         {
            wb_table8_87_2986e( false) ;
         }
      }

      protected void wb_table12_118_2986( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableestimativa2_Internalname, tblTableestimativa2_Internalname, "", "Table", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_fatorescala_Internalname, "Fator de Escala", "", "", lblTextblockprojeto_fatorescala_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table13_123_2986( true) ;
         }
         return  ;
      }

      protected void wb_table13_123_2986e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_incremental_Internalname, "Incremental", "", "", lblTextblockprojeto_incremental_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 142,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkProjeto_Incremental_Internalname, StringUtil.BoolToStr( A1232Projeto_Incremental), "", "", 1, chkProjeto_Incremental.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(142, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,142);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_118_2986e( true) ;
         }
         else
         {
            wb_table12_118_2986e( false) ;
         }
      }

      protected void wb_table13_123_2986( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedprojeto_fatorescala_Internalname, tblTablemergedprojeto_fatorescala_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProjeto_FatorEscala_Internalname, StringUtil.LTrim( StringUtil.NToC( A985Projeto_FatorEscala, 6, 2, ",", "")), ((edtProjeto_FatorEscala_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A985Projeto_FatorEscala, "ZZ9.99")) : context.localUtil.Format( A985Projeto_FatorEscala, "ZZ9.99")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_FatorEscala_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtProjeto_FatorEscala_Enabled, 0, "text", "", 50, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'',0)\"";
            ClassString = "btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfatorescala_Internalname, "", "Calcular", bttBtnfatorescala_Jsonclick, 7, "Fator de Escala", "", StyleString, ClassString, bttBtnfatorescala_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"e112986_client"+"'", TempTags, "", 2, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_fatormultiplicador_Internalname, "Fator Multiplicador", "", "", lblTextblockprojeto_fatormultiplicador_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProjeto_FatorMultiplicador_Internalname, StringUtil.LTrim( StringUtil.NToC( A989Projeto_FatorMultiplicador, 6, 2, ",", "")), ((edtProjeto_FatorMultiplicador_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A989Projeto_FatorMultiplicador, "ZZ9.99")) : context.localUtil.Format( A989Projeto_FatorMultiplicador, "ZZ9.99")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_FatorMultiplicador_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtProjeto_FatorMultiplicador_Enabled, 0, "text", "", 50, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 134,'',false,'',0)\"";
            ClassString = "btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfatormultiplicador_Internalname, "", "Calcular", bttBtnfatormultiplicador_Jsonclick, 7, "Fator Multiplicador", "", StyleString, ClassString, bttBtnfatormultiplicador_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"e122986_client"+"'", TempTags, "", 2, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_constacocomo_Internalname, "Constante A (Cocomo)", "", "", lblTextblockprojeto_constacocomo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 138,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProjeto_ConstACocomo_Internalname, StringUtil.LTrim( StringUtil.NToC( A990Projeto_ConstACocomo, 6, 2, ",", "")), ((edtProjeto_ConstACocomo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A990Projeto_ConstACocomo, "ZZ9.99")) : context.localUtil.Format( A990Projeto_ConstACocomo, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,138);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_ConstACocomo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtProjeto_ConstACocomo_Enabled, 0, "text", "", 50, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table13_123_2986e( true) ;
         }
         else
         {
            wb_table13_123_2986e( false) ;
         }
      }

      protected void wb_table11_90_2986( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableestimativa1_Internalname, tblTableestimativa1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_dtinicio_Internalname, "Data Prevista de In�cio", "", "", lblTextblockprojeto_dtinicio_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtProjeto_DTInicio_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtProjeto_DTInicio_Internalname, context.localUtil.Format(A2146Projeto_DTInicio, "99/99/99"), context.localUtil.Format( A2146Projeto_DTInicio, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,95);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_DTInicio_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtProjeto_DTInicio_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Projeto.htm");
            GxWebStd.gx_bitmap( context, edtProjeto_DTInicio_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtProjeto_DTInicio_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Projeto.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_dtfim_Internalname, "Data Prevista de T�rmino", "", "", lblTextblockprojeto_dtfim_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtProjeto_DTFim_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtProjeto_DTFim_Internalname, context.localUtil.Format(A2147Projeto_DTFim, "99/99/99"), context.localUtil.Format( A2147Projeto_DTFim, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,100);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_DTFim_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtProjeto_DTFim_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Projeto.htm");
            GxWebStd.gx_bitmap( context, edtProjeto_DTFim_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtProjeto_DTFim_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Projeto.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_prazoprevisto_Internalname, "Previs�o de Prazo", "", "", lblTextblockprojeto_prazoprevisto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProjeto_PrazoPrevisto_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2148Projeto_PrazoPrevisto), 4, 0, ",", "")), ((edtProjeto_PrazoPrevisto_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A2148Projeto_PrazoPrevisto), "ZZZ9")) : context.localUtil.Format( (decimal)(A2148Projeto_PrazoPrevisto), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_PrazoPrevisto_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtProjeto_PrazoPrevisto_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_esforcoprevisto_Internalname, "Previs�o de Esfor�o", "", "", lblTextblockprojeto_esforcoprevisto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProjeto_EsforcoPrevisto_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2149Projeto_EsforcoPrevisto), 4, 0, ",", "")), ((edtProjeto_EsforcoPrevisto_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A2149Projeto_EsforcoPrevisto), "ZZZ9")) : context.localUtil.Format( (decimal)(A2149Projeto_EsforcoPrevisto), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_EsforcoPrevisto_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtProjeto_EsforcoPrevisto_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_custoprevisto_Internalname, "Previs�o de Custo", "", "", lblTextblockprojeto_custoprevisto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProjeto_CustoPrevisto_Internalname, StringUtil.LTrim( StringUtil.NToC( A2150Projeto_CustoPrevisto, 14, 2, ",", "")), ((edtProjeto_CustoPrevisto_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A2150Projeto_CustoPrevisto, "ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( A2150Projeto_CustoPrevisto, "ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_CustoPrevisto_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtProjeto_CustoPrevisto_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_90_2986e( true) ;
         }
         else
         {
            wb_table11_90_2986e( false) ;
         }
      }

      protected void wb_table7_69_2986( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableobjeto_Internalname, tblTableobjeto_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_gpoobjctrlcodigo_Internalname, "Grupos de Objetos de Controle", "", "", lblTextblockprojeto_gpoobjctrlcodigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynProjeto_GpoObjCtrlCodigo, dynProjeto_GpoObjCtrlCodigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A2153Projeto_GpoObjCtrlCodigo), 6, 0)), 1, dynProjeto_GpoObjCtrlCodigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynProjeto_GpoObjCtrlCodigo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "", true, "HLP_Projeto.htm");
            dynProjeto_GpoObjCtrlCodigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2153Projeto_GpoObjCtrlCodigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynProjeto_GpoObjCtrlCodigo_Internalname, "Values", (String)(dynProjeto_GpoObjCtrlCodigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_sistemacodigo_Internalname, "Sistema", "", "", lblTextblockprojeto_sistemacodigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynProjeto_SistemaCodigo, dynProjeto_SistemaCodigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A2155Projeto_SistemaCodigo), 6, 0)), 1, dynProjeto_SistemaCodigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynProjeto_SistemaCodigo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,79);\"", "", true, "HLP_Projeto.htm");
            dynProjeto_SistemaCodigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2155Projeto_SistemaCodigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynProjeto_SistemaCodigo_Internalname, "Values", (String)(dynProjeto_SistemaCodigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_modulocodigo_Internalname, "M�dulo", "", "", lblTextblockprojeto_modulocodigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynProjeto_ModuloCodigo, dynProjeto_ModuloCodigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A2158Projeto_ModuloCodigo), 6, 0)), 1, dynProjeto_ModuloCodigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynProjeto_ModuloCodigo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,84);\"", "", true, "HLP_Projeto.htm");
            dynProjeto_ModuloCodigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2158Projeto_ModuloCodigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynProjeto_ModuloCodigo_Internalname, "Values", (String)(dynProjeto_ModuloCodigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_69_2986e( true) ;
         }
         else
         {
            wb_table7_69_2986e( false) ;
         }
      }

      protected void wb_table6_60_2986( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableequipe_Internalname, tblTableequipe_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            wb_table14_63_2986( true) ;
         }
         return  ;
      }

      protected void wb_table14_63_2986e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_60_2986e( true) ;
         }
         else
         {
            wb_table6_60_2986e( false) ;
         }
      }

      protected void wb_table14_63_2986( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions3_Internalname, tblTableactions3_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBntequipe_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBntequipe_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 7, imgBntequipe_Jsonclick, "'"+""+"'"+",false,"+"'"+"e132986_client"+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table14_63_2986e( true) ;
         }
         else
         {
            wb_table14_63_2986e( false) ;
         }
      }

      protected void wb_table5_17_2986( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableprojeto_Internalname, tblTableprojeto_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table15_20_2986( true) ;
         }
         return  ;
      }

      protected void wb_table15_20_2986e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_17_2986e( true) ;
         }
         else
         {
            wb_table5_17_2986e( false) ;
         }
      }

      protected void wb_table15_20_2986( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableprojeto1_Internalname, tblTableprojeto1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_sigla_Internalname, "Sigla", "", "", lblTextblockprojeto_sigla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table16_25_2986( true) ;
         }
         return  ;
      }

      protected void wb_table16_25_2986e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_nome_Internalname, "Nome", "", "", lblTextblockprojeto_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProjeto_Nome_Internalname, StringUtil.RTrim( A649Projeto_Nome), StringUtil.RTrim( context.localUtil.Format( A649Projeto_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtProjeto_Nome_Enabled, 0, "text", "", 100, "%", 1, "row", 50, 0, 0, 0, 1, -1, 0, true, "Nome", "left", true, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_tipoprojetocod_Internalname, "Tipo de Projeto", "", "", lblTextblockprojeto_tipoprojetocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynProjeto_TipoProjetoCod, dynProjeto_TipoProjetoCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A983Projeto_TipoProjetoCod), 6, 0)), 1, dynProjeto_TipoProjetoCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynProjeto_TipoProjetoCod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", "", true, "HLP_Projeto.htm");
            dynProjeto_TipoProjetoCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A983Projeto_TipoProjetoCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynProjeto_TipoProjetoCod_Internalname, "Values", (String)(dynProjeto_TipoProjetoCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_objetivo_Internalname, "Objetivo", "", "", lblTextblockprojeto_objetivo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtProjeto_Objetivo_Internalname, A2145Projeto_Objetivo, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", 0, 1, edtProjeto_Objetivo_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_escopo_Internalname, "Escopo", "", "", lblTextblockprojeto_escopo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtProjeto_Escopo_Internalname, A653Projeto_Escopo, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", 0, 1, edtProjeto_Escopo_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_status_Internalname, "Status", "", "", lblTextblockprojeto_status_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbProjeto_Status, cmbProjeto_Status_Internalname, StringUtil.RTrim( A658Projeto_Status), 1, cmbProjeto_Status_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbProjeto_Status.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_Projeto.htm");
            cmbProjeto_Status.CurrentValue = StringUtil.RTrim( A658Projeto_Status);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbProjeto_Status_Internalname, "Values", (String)(cmbProjeto_Status.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table15_20_2986e( true) ;
         }
         else
         {
            wb_table15_20_2986e( false) ;
         }
      }

      protected void wb_table16_25_2986( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedprojeto_sigla_Internalname, tblTablemergedprojeto_sigla_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProjeto_Sigla_Internalname, StringUtil.RTrim( A650Projeto_Sigla), StringUtil.RTrim( context.localUtil.Format( A650Projeto_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_Sigla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtProjeto_Sigla_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_identificador_Internalname, "Identificador", "", "", lblTextblockprojeto_identificador_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtProjeto_Identificador_Internalname, A2144Projeto_Identificador, StringUtil.RTrim( context.localUtil.Format( A2144Projeto_Identificador, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,32);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_Identificador_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtProjeto_Identificador_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Projeto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table16_25_2986e( true) ;
         }
         else
         {
            wb_table16_25_2986e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E14292 */
         E14292 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               ajax_req_read_hidden_sdt(cgiGet( "vWWPCONTEXT"), AV8WWPContext);
               /* Read variables values. */
               A650Projeto_Sigla = StringUtil.Upper( cgiGet( edtProjeto_Sigla_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A650Projeto_Sigla", A650Projeto_Sigla);
               A2144Projeto_Identificador = cgiGet( edtProjeto_Identificador_Internalname);
               n2144Projeto_Identificador = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2144Projeto_Identificador", A2144Projeto_Identificador);
               n2144Projeto_Identificador = (String.IsNullOrEmpty(StringUtil.RTrim( A2144Projeto_Identificador)) ? true : false);
               A649Projeto_Nome = StringUtil.Upper( cgiGet( edtProjeto_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A649Projeto_Nome", A649Projeto_Nome);
               dynProjeto_TipoProjetoCod.Name = dynProjeto_TipoProjetoCod_Internalname;
               dynProjeto_TipoProjetoCod.CurrentValue = cgiGet( dynProjeto_TipoProjetoCod_Internalname);
               A983Projeto_TipoProjetoCod = (int)(NumberUtil.Val( cgiGet( dynProjeto_TipoProjetoCod_Internalname), "."));
               n983Projeto_TipoProjetoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A983Projeto_TipoProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A983Projeto_TipoProjetoCod), 6, 0)));
               n983Projeto_TipoProjetoCod = ((0==A983Projeto_TipoProjetoCod) ? true : false);
               A2145Projeto_Objetivo = cgiGet( edtProjeto_Objetivo_Internalname);
               n2145Projeto_Objetivo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2145Projeto_Objetivo", A2145Projeto_Objetivo);
               n2145Projeto_Objetivo = (String.IsNullOrEmpty(StringUtil.RTrim( A2145Projeto_Objetivo)) ? true : false);
               A653Projeto_Escopo = cgiGet( edtProjeto_Escopo_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A653Projeto_Escopo", A653Projeto_Escopo);
               cmbProjeto_Status.Name = cmbProjeto_Status_Internalname;
               cmbProjeto_Status.CurrentValue = cgiGet( cmbProjeto_Status_Internalname);
               A658Projeto_Status = cgiGet( cmbProjeto_Status_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A658Projeto_Status", A658Projeto_Status);
               dynProjeto_GpoObjCtrlCodigo.Name = dynProjeto_GpoObjCtrlCodigo_Internalname;
               dynProjeto_GpoObjCtrlCodigo.CurrentValue = cgiGet( dynProjeto_GpoObjCtrlCodigo_Internalname);
               A2153Projeto_GpoObjCtrlCodigo = (int)(NumberUtil.Val( cgiGet( dynProjeto_GpoObjCtrlCodigo_Internalname), "."));
               n2153Projeto_GpoObjCtrlCodigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2153Projeto_GpoObjCtrlCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2153Projeto_GpoObjCtrlCodigo), 6, 0)));
               n2153Projeto_GpoObjCtrlCodigo = ((0==A2153Projeto_GpoObjCtrlCodigo) ? true : false);
               dynProjeto_SistemaCodigo.Name = dynProjeto_SistemaCodigo_Internalname;
               dynProjeto_SistemaCodigo.CurrentValue = cgiGet( dynProjeto_SistemaCodigo_Internalname);
               A2155Projeto_SistemaCodigo = (int)(NumberUtil.Val( cgiGet( dynProjeto_SistemaCodigo_Internalname), "."));
               n2155Projeto_SistemaCodigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2155Projeto_SistemaCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2155Projeto_SistemaCodigo), 6, 0)));
               n2155Projeto_SistemaCodigo = ((0==A2155Projeto_SistemaCodigo) ? true : false);
               dynProjeto_ModuloCodigo.Name = dynProjeto_ModuloCodigo_Internalname;
               dynProjeto_ModuloCodigo.CurrentValue = cgiGet( dynProjeto_ModuloCodigo_Internalname);
               A2158Projeto_ModuloCodigo = (int)(NumberUtil.Val( cgiGet( dynProjeto_ModuloCodigo_Internalname), "."));
               n2158Projeto_ModuloCodigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2158Projeto_ModuloCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2158Projeto_ModuloCodigo), 6, 0)));
               n2158Projeto_ModuloCodigo = ((0==A2158Projeto_ModuloCodigo) ? true : false);
               if ( context.localUtil.VCDate( cgiGet( edtProjeto_DTInicio_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data Prevista de In�cio"}), 1, "PROJETO_DTINICIO");
                  AnyError = 1;
                  GX_FocusControl = edtProjeto_DTInicio_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2146Projeto_DTInicio = DateTime.MinValue;
                  n2146Projeto_DTInicio = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2146Projeto_DTInicio", context.localUtil.Format(A2146Projeto_DTInicio, "99/99/99"));
               }
               else
               {
                  A2146Projeto_DTInicio = context.localUtil.CToD( cgiGet( edtProjeto_DTInicio_Internalname), 2);
                  n2146Projeto_DTInicio = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2146Projeto_DTInicio", context.localUtil.Format(A2146Projeto_DTInicio, "99/99/99"));
               }
               n2146Projeto_DTInicio = ((DateTime.MinValue==A2146Projeto_DTInicio) ? true : false);
               if ( context.localUtil.VCDate( cgiGet( edtProjeto_DTFim_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data Prevista de T�rmino"}), 1, "PROJETO_DTFIM");
                  AnyError = 1;
                  GX_FocusControl = edtProjeto_DTFim_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2147Projeto_DTFim = DateTime.MinValue;
                  n2147Projeto_DTFim = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2147Projeto_DTFim", context.localUtil.Format(A2147Projeto_DTFim, "99/99/99"));
               }
               else
               {
                  A2147Projeto_DTFim = context.localUtil.CToD( cgiGet( edtProjeto_DTFim_Internalname), 2);
                  n2147Projeto_DTFim = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2147Projeto_DTFim", context.localUtil.Format(A2147Projeto_DTFim, "99/99/99"));
               }
               n2147Projeto_DTFim = ((DateTime.MinValue==A2147Projeto_DTFim) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtProjeto_PrazoPrevisto_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtProjeto_PrazoPrevisto_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PROJETO_PRAZOPREVISTO");
                  AnyError = 1;
                  GX_FocusControl = edtProjeto_PrazoPrevisto_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2148Projeto_PrazoPrevisto = 0;
                  n2148Projeto_PrazoPrevisto = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2148Projeto_PrazoPrevisto", StringUtil.LTrim( StringUtil.Str( (decimal)(A2148Projeto_PrazoPrevisto), 4, 0)));
               }
               else
               {
                  A2148Projeto_PrazoPrevisto = (short)(context.localUtil.CToN( cgiGet( edtProjeto_PrazoPrevisto_Internalname), ",", "."));
                  n2148Projeto_PrazoPrevisto = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2148Projeto_PrazoPrevisto", StringUtil.LTrim( StringUtil.Str( (decimal)(A2148Projeto_PrazoPrevisto), 4, 0)));
               }
               n2148Projeto_PrazoPrevisto = ((0==A2148Projeto_PrazoPrevisto) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtProjeto_EsforcoPrevisto_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtProjeto_EsforcoPrevisto_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PROJETO_ESFORCOPREVISTO");
                  AnyError = 1;
                  GX_FocusControl = edtProjeto_EsforcoPrevisto_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2149Projeto_EsforcoPrevisto = 0;
                  n2149Projeto_EsforcoPrevisto = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2149Projeto_EsforcoPrevisto", StringUtil.LTrim( StringUtil.Str( (decimal)(A2149Projeto_EsforcoPrevisto), 4, 0)));
               }
               else
               {
                  A2149Projeto_EsforcoPrevisto = (short)(context.localUtil.CToN( cgiGet( edtProjeto_EsforcoPrevisto_Internalname), ",", "."));
                  n2149Projeto_EsforcoPrevisto = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2149Projeto_EsforcoPrevisto", StringUtil.LTrim( StringUtil.Str( (decimal)(A2149Projeto_EsforcoPrevisto), 4, 0)));
               }
               n2149Projeto_EsforcoPrevisto = ((0==A2149Projeto_EsforcoPrevisto) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtProjeto_CustoPrevisto_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtProjeto_CustoPrevisto_Internalname), ",", ".") > 999999999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PROJETO_CUSTOPREVISTO");
                  AnyError = 1;
                  GX_FocusControl = edtProjeto_CustoPrevisto_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2150Projeto_CustoPrevisto = 0;
                  n2150Projeto_CustoPrevisto = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2150Projeto_CustoPrevisto", StringUtil.LTrim( StringUtil.Str( A2150Projeto_CustoPrevisto, 12, 2)));
               }
               else
               {
                  A2150Projeto_CustoPrevisto = context.localUtil.CToN( cgiGet( edtProjeto_CustoPrevisto_Internalname), ",", ".");
                  n2150Projeto_CustoPrevisto = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2150Projeto_CustoPrevisto", StringUtil.LTrim( StringUtil.Str( A2150Projeto_CustoPrevisto, 12, 2)));
               }
               n2150Projeto_CustoPrevisto = ((Convert.ToDecimal(0)==A2150Projeto_CustoPrevisto) ? true : false);
               A985Projeto_FatorEscala = context.localUtil.CToN( cgiGet( edtProjeto_FatorEscala_Internalname), ",", ".");
               n985Projeto_FatorEscala = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A985Projeto_FatorEscala", StringUtil.LTrim( StringUtil.Str( A985Projeto_FatorEscala, 6, 2)));
               A989Projeto_FatorMultiplicador = context.localUtil.CToN( cgiGet( edtProjeto_FatorMultiplicador_Internalname), ",", ".");
               n989Projeto_FatorMultiplicador = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A989Projeto_FatorMultiplicador", StringUtil.LTrim( StringUtil.Str( A989Projeto_FatorMultiplicador, 6, 2)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtProjeto_ConstACocomo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtProjeto_ConstACocomo_Internalname), ",", ".") > 999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PROJETO_CONSTACOCOMO");
                  AnyError = 1;
                  GX_FocusControl = edtProjeto_ConstACocomo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A990Projeto_ConstACocomo = 0;
                  n990Projeto_ConstACocomo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A990Projeto_ConstACocomo", StringUtil.LTrim( StringUtil.Str( A990Projeto_ConstACocomo, 6, 2)));
               }
               else
               {
                  A990Projeto_ConstACocomo = context.localUtil.CToN( cgiGet( edtProjeto_ConstACocomo_Internalname), ",", ".");
                  n990Projeto_ConstACocomo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A990Projeto_ConstACocomo", StringUtil.LTrim( StringUtil.Str( A990Projeto_ConstACocomo, 6, 2)));
               }
               n990Projeto_ConstACocomo = ((Convert.ToDecimal(0)==A990Projeto_ConstACocomo) ? true : false);
               A1232Projeto_Incremental = StringUtil.StrToBool( cgiGet( chkProjeto_Incremental_Internalname));
               n1232Projeto_Incremental = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1232Projeto_Incremental", A1232Projeto_Incremental);
               n1232Projeto_Incremental = ((false==A1232Projeto_Incremental) ? true : false);
               A648Projeto_Codigo = (int)(context.localUtil.CToN( cgiGet( edtProjeto_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
               /* Read saved values. */
               Z648Projeto_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z648Projeto_Codigo"), ",", "."));
               Z649Projeto_Nome = cgiGet( "Z649Projeto_Nome");
               Z650Projeto_Sigla = cgiGet( "Z650Projeto_Sigla");
               Z652Projeto_TecnicaContagem = cgiGet( "Z652Projeto_TecnicaContagem");
               Z1541Projeto_Previsao = context.localUtil.CToD( cgiGet( "Z1541Projeto_Previsao"), 0);
               n1541Projeto_Previsao = ((DateTime.MinValue==A1541Projeto_Previsao) ? true : false);
               Z1542Projeto_GerenteCod = (int)(context.localUtil.CToN( cgiGet( "Z1542Projeto_GerenteCod"), ",", "."));
               n1542Projeto_GerenteCod = ((0==A1542Projeto_GerenteCod) ? true : false);
               Z1543Projeto_ServicoCod = (int)(context.localUtil.CToN( cgiGet( "Z1543Projeto_ServicoCod"), ",", "."));
               n1543Projeto_ServicoCod = ((0==A1543Projeto_ServicoCod) ? true : false);
               Z658Projeto_Status = cgiGet( "Z658Projeto_Status");
               Z985Projeto_FatorEscala = context.localUtil.CToN( cgiGet( "Z985Projeto_FatorEscala"), ",", ".");
               n985Projeto_FatorEscala = ((Convert.ToDecimal(0)==A985Projeto_FatorEscala) ? true : false);
               Z989Projeto_FatorMultiplicador = context.localUtil.CToN( cgiGet( "Z989Projeto_FatorMultiplicador"), ",", ".");
               n989Projeto_FatorMultiplicador = ((Convert.ToDecimal(0)==A989Projeto_FatorMultiplicador) ? true : false);
               Z990Projeto_ConstACocomo = context.localUtil.CToN( cgiGet( "Z990Projeto_ConstACocomo"), ",", ".");
               n990Projeto_ConstACocomo = ((Convert.ToDecimal(0)==A990Projeto_ConstACocomo) ? true : false);
               Z1232Projeto_Incremental = StringUtil.StrToBool( cgiGet( "Z1232Projeto_Incremental"));
               n1232Projeto_Incremental = ((false==A1232Projeto_Incremental) ? true : false);
               Z2144Projeto_Identificador = cgiGet( "Z2144Projeto_Identificador");
               n2144Projeto_Identificador = (String.IsNullOrEmpty(StringUtil.RTrim( A2144Projeto_Identificador)) ? true : false);
               Z2146Projeto_DTInicio = context.localUtil.CToD( cgiGet( "Z2146Projeto_DTInicio"), 0);
               n2146Projeto_DTInicio = ((DateTime.MinValue==A2146Projeto_DTInicio) ? true : false);
               Z2147Projeto_DTFim = context.localUtil.CToD( cgiGet( "Z2147Projeto_DTFim"), 0);
               n2147Projeto_DTFim = ((DateTime.MinValue==A2147Projeto_DTFim) ? true : false);
               Z2148Projeto_PrazoPrevisto = (short)(context.localUtil.CToN( cgiGet( "Z2148Projeto_PrazoPrevisto"), ",", "."));
               n2148Projeto_PrazoPrevisto = ((0==A2148Projeto_PrazoPrevisto) ? true : false);
               Z2149Projeto_EsforcoPrevisto = (short)(context.localUtil.CToN( cgiGet( "Z2149Projeto_EsforcoPrevisto"), ",", "."));
               n2149Projeto_EsforcoPrevisto = ((0==A2149Projeto_EsforcoPrevisto) ? true : false);
               Z2150Projeto_CustoPrevisto = context.localUtil.CToN( cgiGet( "Z2150Projeto_CustoPrevisto"), ",", ".");
               n2150Projeto_CustoPrevisto = ((Convert.ToDecimal(0)==A2150Projeto_CustoPrevisto) ? true : false);
               Z2170Projeto_AnexoSequecial = (short)(context.localUtil.CToN( cgiGet( "Z2170Projeto_AnexoSequecial"), ",", "."));
               n2170Projeto_AnexoSequecial = ((0==A2170Projeto_AnexoSequecial) ? true : false);
               Z983Projeto_TipoProjetoCod = (int)(context.localUtil.CToN( cgiGet( "Z983Projeto_TipoProjetoCod"), ",", "."));
               n983Projeto_TipoProjetoCod = ((0==A983Projeto_TipoProjetoCod) ? true : false);
               Z2151Projeto_AreaTrabalhoCodigo = (int)(context.localUtil.CToN( cgiGet( "Z2151Projeto_AreaTrabalhoCodigo"), ",", "."));
               n2151Projeto_AreaTrabalhoCodigo = ((0==A2151Projeto_AreaTrabalhoCodigo) ? true : false);
               Z2153Projeto_GpoObjCtrlCodigo = (int)(context.localUtil.CToN( cgiGet( "Z2153Projeto_GpoObjCtrlCodigo"), ",", "."));
               n2153Projeto_GpoObjCtrlCodigo = ((0==A2153Projeto_GpoObjCtrlCodigo) ? true : false);
               Z2155Projeto_SistemaCodigo = (int)(context.localUtil.CToN( cgiGet( "Z2155Projeto_SistemaCodigo"), ",", "."));
               n2155Projeto_SistemaCodigo = ((0==A2155Projeto_SistemaCodigo) ? true : false);
               Z2158Projeto_ModuloCodigo = (int)(context.localUtil.CToN( cgiGet( "Z2158Projeto_ModuloCodigo"), ",", "."));
               n2158Projeto_ModuloCodigo = ((0==A2158Projeto_ModuloCodigo) ? true : false);
               A652Projeto_TecnicaContagem = cgiGet( "Z652Projeto_TecnicaContagem");
               A1541Projeto_Previsao = context.localUtil.CToD( cgiGet( "Z1541Projeto_Previsao"), 0);
               n1541Projeto_Previsao = false;
               n1541Projeto_Previsao = ((DateTime.MinValue==A1541Projeto_Previsao) ? true : false);
               A1542Projeto_GerenteCod = (int)(context.localUtil.CToN( cgiGet( "Z1542Projeto_GerenteCod"), ",", "."));
               n1542Projeto_GerenteCod = false;
               n1542Projeto_GerenteCod = ((0==A1542Projeto_GerenteCod) ? true : false);
               A1543Projeto_ServicoCod = (int)(context.localUtil.CToN( cgiGet( "Z1543Projeto_ServicoCod"), ",", "."));
               n1543Projeto_ServicoCod = false;
               n1543Projeto_ServicoCod = ((0==A1543Projeto_ServicoCod) ? true : false);
               A2170Projeto_AnexoSequecial = (short)(context.localUtil.CToN( cgiGet( "Z2170Projeto_AnexoSequecial"), ",", "."));
               n2170Projeto_AnexoSequecial = false;
               n2170Projeto_AnexoSequecial = ((0==A2170Projeto_AnexoSequecial) ? true : false);
               A2151Projeto_AreaTrabalhoCodigo = (int)(context.localUtil.CToN( cgiGet( "Z2151Projeto_AreaTrabalhoCodigo"), ",", "."));
               n2151Projeto_AreaTrabalhoCodigo = false;
               n2151Projeto_AreaTrabalhoCodigo = ((0==A2151Projeto_AreaTrabalhoCodigo) ? true : false);
               O2170Projeto_AnexoSequecial = (short)(context.localUtil.CToN( cgiGet( "O2170Projeto_AnexoSequecial"), ",", "."));
               n2170Projeto_AnexoSequecial = ((0==A2170Projeto_AnexoSequecial) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               nRC_GXsfl_154 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_154"), ",", "."));
               N983Projeto_TipoProjetoCod = (int)(context.localUtil.CToN( cgiGet( "N983Projeto_TipoProjetoCod"), ",", "."));
               n983Projeto_TipoProjetoCod = ((0==A983Projeto_TipoProjetoCod) ? true : false);
               N2151Projeto_AreaTrabalhoCodigo = (int)(context.localUtil.CToN( cgiGet( "N2151Projeto_AreaTrabalhoCodigo"), ",", "."));
               n2151Projeto_AreaTrabalhoCodigo = ((0==A2151Projeto_AreaTrabalhoCodigo) ? true : false);
               N2153Projeto_GpoObjCtrlCodigo = (int)(context.localUtil.CToN( cgiGet( "N2153Projeto_GpoObjCtrlCodigo"), ",", "."));
               n2153Projeto_GpoObjCtrlCodigo = ((0==A2153Projeto_GpoObjCtrlCodigo) ? true : false);
               N2155Projeto_SistemaCodigo = (int)(context.localUtil.CToN( cgiGet( "N2155Projeto_SistemaCodigo"), ",", "."));
               n2155Projeto_SistemaCodigo = ((0==A2155Projeto_SistemaCodigo) ? true : false);
               N2158Projeto_ModuloCodigo = (int)(context.localUtil.CToN( cgiGet( "N2158Projeto_ModuloCodigo"), ",", "."));
               n2158Projeto_ModuloCodigo = ((0==A2158Projeto_ModuloCodigo) ? true : false);
               A740Projeto_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "PROJETO_SISTEMACOD"), ",", "."));
               AV7Projeto_Codigo = (int)(context.localUtil.CToN( cgiGet( "vPROJETO_CODIGO"), ",", "."));
               AV12Insert_Projeto_TipoProjetoCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_PROJETO_TIPOPROJETOCOD"), ",", "."));
               AV19Insert_Projeto_AreaTrabalhoCodigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_PROJETO_AREATRABALHOCODIGO"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A2151Projeto_AreaTrabalhoCodigo = (int)(context.localUtil.CToN( cgiGet( "PROJETO_AREATRABALHOCODIGO"), ",", "."));
               n2151Projeto_AreaTrabalhoCodigo = ((0==A2151Projeto_AreaTrabalhoCodigo) ? true : false);
               AV20Insert_Projeto_GpoObjCtrlCodigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_PROJETO_GPOOBJCTRLCODIGO"), ",", "."));
               AV21Insert_Projeto_SistemaCodigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_PROJETO_SISTEMACODIGO"), ",", "."));
               AV22Insert_Projeto_ModuloCodigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_PROJETO_MODULOCODIGO"), ",", "."));
               A652Projeto_TecnicaContagem = cgiGet( "PROJETO_TECNICACONTAGEM");
               A1540Projeto_Introducao = cgiGet( "PROJETO_INTRODUCAO");
               n1540Projeto_Introducao = false;
               n1540Projeto_Introducao = (String.IsNullOrEmpty(StringUtil.RTrim( A1540Projeto_Introducao)) ? true : false);
               A1541Projeto_Previsao = context.localUtil.CToD( cgiGet( "PROJETO_PREVISAO"), 0);
               n1541Projeto_Previsao = ((DateTime.MinValue==A1541Projeto_Previsao) ? true : false);
               A1542Projeto_GerenteCod = (int)(context.localUtil.CToN( cgiGet( "PROJETO_GERENTECOD"), ",", "."));
               n1542Projeto_GerenteCod = ((0==A1542Projeto_GerenteCod) ? true : false);
               A1543Projeto_ServicoCod = (int)(context.localUtil.CToN( cgiGet( "PROJETO_SERVICOCOD"), ",", "."));
               n1543Projeto_ServicoCod = ((0==A1543Projeto_ServicoCod) ? true : false);
               A2170Projeto_AnexoSequecial = (short)(context.localUtil.CToN( cgiGet( "PROJETO_ANEXOSEQUECIAL"), ",", "."));
               n2170Projeto_AnexoSequecial = ((0==A2170Projeto_AnexoSequecial) ? true : false);
               A2152Projeto_AreaTrabalhoDescricao = cgiGet( "PROJETO_AREATRABALHODESCRICAO");
               n2152Projeto_AreaTrabalhoDescricao = false;
               A2154Projeto_GpoObjCtrlNome = cgiGet( "PROJETO_GPOOBJCTRLNOME");
               n2154Projeto_GpoObjCtrlNome = false;
               A2156Projeto_SistemaNome = cgiGet( "PROJETO_SISTEMANOME");
               n2156Projeto_SistemaNome = false;
               A2157Projeto_SistemaSigla = cgiGet( "PROJETO_SISTEMASIGLA");
               n2157Projeto_SistemaSigla = false;
               A2159Projeto_ModuloNome = cgiGet( "PROJETO_MODULONOME");
               n2159Projeto_ModuloNome = false;
               A2160Projeto_ModuloSigla = cgiGet( "PROJETO_MODULOSIGLA");
               n2160Projeto_ModuloSigla = false;
               A655Projeto_Custo = context.localUtil.CToN( cgiGet( "PROJETO_CUSTO"), ",", ".");
               A656Projeto_Prazo = (short)(context.localUtil.CToN( cgiGet( "PROJETO_PRAZO"), ",", "."));
               A657Projeto_Esforco = (short)(context.localUtil.CToN( cgiGet( "PROJETO_ESFORCO"), ",", "."));
               n657Projeto_Esforco = false;
               AV24Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               A2164ProjetoAnexos_Descricao = cgiGet( "PROJETOANEXOS_DESCRICAO");
               A2168ProjetoAnexos_Link = cgiGet( "PROJETOANEXOS_LINK");
               n2168ProjetoAnexos_Link = false;
               n2168ProjetoAnexos_Link = (String.IsNullOrEmpty(StringUtil.RTrim( A2168ProjetoAnexos_Link)) ? true : false);
               Gxuitabspanel_tabmain_Width = cgiGet( "GXUITABSPANEL_TABMAIN_Width");
               Gxuitabspanel_tabmain_Height = cgiGet( "GXUITABSPANEL_TABMAIN_Height");
               Gxuitabspanel_tabmain_Cls = cgiGet( "GXUITABSPANEL_TABMAIN_Cls");
               Gxuitabspanel_tabmain_Enabled = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABMAIN_Enabled"));
               Gxuitabspanel_tabmain_Class = cgiGet( "GXUITABSPANEL_TABMAIN_Class");
               Gxuitabspanel_tabmain_Autowidth = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABMAIN_Autowidth"));
               Gxuitabspanel_tabmain_Autoheight = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABMAIN_Autoheight"));
               Gxuitabspanel_tabmain_Autoscroll = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABMAIN_Autoscroll"));
               Gxuitabspanel_tabmain_Activetabid = cgiGet( "GXUITABSPANEL_TABMAIN_Activetabid");
               Gxuitabspanel_tabmain_Designtimetabs = cgiGet( "GXUITABSPANEL_TABMAIN_Designtimetabs");
               Gxuitabspanel_tabmain_Selectedtabindex = (int)(context.localUtil.CToN( cgiGet( "GXUITABSPANEL_TABMAIN_Selectedtabindex"), ",", "."));
               Gxuitabspanel_tabmain_Visible = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABMAIN_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Projeto";
               A658Projeto_Status = cgiGet( cmbProjeto_Status_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A658Projeto_Status", A658Projeto_Status);
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A658Projeto_Status, ""));
               A648Projeto_Codigo = (int)(context.localUtil.CToN( cgiGet( edtProjeto_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A648Projeto_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2151Projeto_AreaTrabalhoCodigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A652Projeto_TecnicaContagem, ""));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format(A1541Projeto_Previsao, "99/99/99");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1542Projeto_GerenteCod), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1543Projeto_ServicoCod), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A648Projeto_Codigo != Z648Projeto_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("projeto:[SecurityCheckFailed value for]"+"Projeto_Status:"+StringUtil.RTrim( context.localUtil.Format( A658Projeto_Status, "")));
                  GXUtil.WriteLog("projeto:[SecurityCheckFailed value for]"+"Projeto_Codigo:"+context.localUtil.Format( (decimal)(A648Projeto_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("projeto:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("projeto:[SecurityCheckFailed value for]"+"Projeto_AreaTrabalhoCodigo:"+context.localUtil.Format( (decimal)(A2151Projeto_AreaTrabalhoCodigo), "ZZZZZ9"));
                  GXUtil.WriteLog("projeto:[SecurityCheckFailed value for]"+"Projeto_TecnicaContagem:"+StringUtil.RTrim( context.localUtil.Format( A652Projeto_TecnicaContagem, "")));
                  GXUtil.WriteLog("projeto:[SecurityCheckFailed value for]"+"Projeto_Previsao:"+context.localUtil.Format(A1541Projeto_Previsao, "99/99/99"));
                  GXUtil.WriteLog("projeto:[SecurityCheckFailed value for]"+"Projeto_GerenteCod:"+context.localUtil.Format( (decimal)(A1542Projeto_GerenteCod), "ZZZZZ9"));
                  GXUtil.WriteLog("projeto:[SecurityCheckFailed value for]"+"Projeto_ServicoCod:"+context.localUtil.Format( (decimal)(A1543Projeto_ServicoCod), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               /* Check if conditions changed and reset current page numbers */
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A648Projeto_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode86 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode86;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound86 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_290( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "PROJETO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtProjeto_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14292 */
                           E14292 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15292 */
                           E15292 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOBNTANEXOS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16292 */
                           E16292 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E15292 */
            E15292 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2986( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes2986( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_290( )
      {
         BeforeValidate2986( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2986( ) ;
            }
            else
            {
               CheckExtendedTable2986( ) ;
               CloseExtendedTableCursors2986( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            /* Save parent mode. */
            sMode86 = Gx_mode;
            CONFIRM_29235( ) ;
            if ( AnyError == 0 )
            {
               /* Restore parent mode. */
               Gx_mode = sMode86;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               IsConfirmed = 1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
            }
            /* Restore parent mode. */
            Gx_mode = sMode86;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
      }

      protected void CONFIRM_29235( )
      {
         s2170Projeto_AnexoSequecial = O2170Projeto_AnexoSequecial;
         n2170Projeto_AnexoSequecial = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2170Projeto_AnexoSequecial", StringUtil.LTrim( StringUtil.Str( (decimal)(A2170Projeto_AnexoSequecial), 4, 0)));
         nGXsfl_154_idx = 0;
         while ( nGXsfl_154_idx < nRC_GXsfl_154 )
         {
            ReadRow29235( ) ;
            if ( ( nRcdExists_235 != 0 ) || ( nIsMod_235 != 0 ) )
            {
               GetKey29235( ) ;
               if ( ( nRcdExists_235 == 0 ) && ( nRcdDeleted_235 == 0 ) )
               {
                  if ( RcdFound235 == 0 )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     BeforeValidate29235( ) ;
                     if ( AnyError == 0 )
                     {
                        CheckExtendedTable29235( ) ;
                        CloseExtendedTableCursors29235( ) ;
                        if ( AnyError == 0 )
                        {
                           IsConfirmed = 1;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                        }
                        O2170Projeto_AnexoSequecial = A2170Projeto_AnexoSequecial;
                        n2170Projeto_AnexoSequecial = false;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2170Projeto_AnexoSequecial", StringUtil.LTrim( StringUtil.Str( (decimal)(A2170Projeto_AnexoSequecial), 4, 0)));
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                     AnyError = 1;
                  }
               }
               else
               {
                  if ( RcdFound235 != 0 )
                  {
                     if ( nRcdDeleted_235 != 0 )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                        getByPrimaryKey29235( ) ;
                        Load29235( ) ;
                        BeforeValidate29235( ) ;
                        if ( AnyError == 0 )
                        {
                           OnDeleteControls29235( ) ;
                           O2170Projeto_AnexoSequecial = A2170Projeto_AnexoSequecial;
                           n2170Projeto_AnexoSequecial = false;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2170Projeto_AnexoSequecial", StringUtil.LTrim( StringUtil.Str( (decimal)(A2170Projeto_AnexoSequecial), 4, 0)));
                        }
                     }
                     else
                     {
                        if ( nIsMod_235 != 0 )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                           BeforeValidate29235( ) ;
                           if ( AnyError == 0 )
                           {
                              CheckExtendedTable29235( ) ;
                              CloseExtendedTableCursors29235( ) ;
                              if ( AnyError == 0 )
                              {
                                 IsConfirmed = 1;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                              }
                              O2170Projeto_AnexoSequecial = A2170Projeto_AnexoSequecial;
                              n2170Projeto_AnexoSequecial = false;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2170Projeto_AnexoSequecial", StringUtil.LTrim( StringUtil.Str( (decimal)(A2170Projeto_AnexoSequecial), 4, 0)));
                           }
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_235 == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            ChangePostValue( edtProjetoAnexos_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2163ProjetoAnexos_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( edtProjetoAnexos_Arquivo_Internalname, A2165ProjetoAnexos_Arquivo) ;
            ChangePostValue( edtProjetoAnexos_ArquivoNome_Internalname, StringUtil.RTrim( A2166ProjetoAnexos_ArquivoNome)) ;
            ChangePostValue( edtTipoDocumento_Nome_Internalname, StringUtil.RTrim( A646TipoDocumento_Nome)) ;
            ChangePostValue( dynTipoDocumento_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A645TipoDocumento_Codigo), 6, 0, ".", ""))) ;
            ChangePostValue( edtProjetoAnexos_ArquivoTipo_Internalname, StringUtil.RTrim( A2167ProjetoAnexos_ArquivoTipo)) ;
            ChangePostValue( edtProjetoAnexos_Data_Internalname, context.localUtil.TToC( A2169ProjetoAnexos_Data, 10, 8, 0, 3, "/", ":", " ")) ;
            ChangePostValue( "ZT_"+"Z2163ProjetoAnexos_Codigo_"+sGXsfl_154_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2163ProjetoAnexos_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z2169ProjetoAnexos_Data_"+sGXsfl_154_idx, context.localUtil.TToC( Z2169ProjetoAnexos_Data, 10, 8, 0, 0, "/", ":", " ")) ;
            ChangePostValue( "ZT_"+"Z645TipoDocumento_Codigo_"+sGXsfl_154_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z645TipoDocumento_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( "nRcdDeleted_235_"+sGXsfl_154_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_235), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_235_"+sGXsfl_154_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_235), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_235_"+sGXsfl_154_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_235), 4, 0, ",", ""))) ;
            if ( nIsMod_235 != 0 )
            {
               ChangePostValue( "PROJETOANEXOS_CODIGO_"+sGXsfl_154_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProjetoAnexos_Codigo_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "PROJETOANEXOS_ARQUIVO_"+sGXsfl_154_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProjetoAnexos_Arquivo_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "PROJETOANEXOS_ARQUIVONOME_"+sGXsfl_154_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProjetoAnexos_ArquivoNome_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "TIPODOCUMENTO_NOME_"+sGXsfl_154_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTipoDocumento_Nome_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "TIPODOCUMENTO_CODIGO_"+sGXsfl_154_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(dynTipoDocumento_Codigo.Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "PROJETOANEXOS_ARQUIVOTIPO_"+sGXsfl_154_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProjetoAnexos_ArquivoTipo_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "PROJETOANEXOS_DATA_"+sGXsfl_154_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProjetoAnexos_Data_Enabled), 5, 0, ".", ""))) ;
            }
         }
         O2170Projeto_AnexoSequecial = s2170Projeto_AnexoSequecial;
         n2170Projeto_AnexoSequecial = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2170Projeto_AnexoSequecial", StringUtil.LTrim( StringUtil.Str( (decimal)(A2170Projeto_AnexoSequecial), 4, 0)));
         /* Start of After( level) rules */
         /* End of After( level) rules */
      }

      protected void ResetCaption290( )
      {
      }

      protected void E14292( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV24Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV25GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25GXV1), 8, 0)));
            while ( AV25GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV25GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Projeto_TipoProjetoCod") == 0 )
               {
                  AV12Insert_Projeto_TipoProjetoCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_Projeto_TipoProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_Projeto_TipoProjetoCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Projeto_AreaTrabalhoCodigo") == 0 )
               {
                  AV19Insert_Projeto_AreaTrabalhoCodigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Insert_Projeto_AreaTrabalhoCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19Insert_Projeto_AreaTrabalhoCodigo), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Projeto_GpoObjCtrlCodigo") == 0 )
               {
                  AV20Insert_Projeto_GpoObjCtrlCodigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Insert_Projeto_GpoObjCtrlCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Insert_Projeto_GpoObjCtrlCodigo), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Projeto_SistemaCodigo") == 0 )
               {
                  AV21Insert_Projeto_SistemaCodigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Insert_Projeto_SistemaCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Insert_Projeto_SistemaCodigo), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Projeto_ModuloCodigo") == 0 )
               {
                  AV22Insert_Projeto_ModuloCodigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Insert_Projeto_ModuloCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Insert_Projeto_ModuloCodigo), 6, 0)));
               }
               AV25GXV1 = (int)(AV25GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25GXV1), 8, 0)));
            }
         }
         edtProjeto_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_Codigo_Visible), 5, 0)));
         AV16FatorEscala = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16FatorEscala", StringUtil.LTrim( StringUtil.Str( AV16FatorEscala, 6, 2)));
         AV15FatorMultiplicador = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15FatorMultiplicador", StringUtil.LTrim( StringUtil.Str( AV15FatorMultiplicador, 6, 2)));
         subGridanexos_Rows = 1;
      }

      protected void E15292( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwprojeto.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E16292( )
      {
         /* 'DoBntAnexos' Routine */
         subgridanexos_addlines( 1) ;
      }

      protected void ZM2986( short GX_JID )
      {
         if ( ( GX_JID == 38 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z649Projeto_Nome = T00296_A649Projeto_Nome[0];
               Z650Projeto_Sigla = T00296_A650Projeto_Sigla[0];
               Z652Projeto_TecnicaContagem = T00296_A652Projeto_TecnicaContagem[0];
               Z1541Projeto_Previsao = T00296_A1541Projeto_Previsao[0];
               Z1542Projeto_GerenteCod = T00296_A1542Projeto_GerenteCod[0];
               Z1543Projeto_ServicoCod = T00296_A1543Projeto_ServicoCod[0];
               Z658Projeto_Status = T00296_A658Projeto_Status[0];
               Z985Projeto_FatorEscala = T00296_A985Projeto_FatorEscala[0];
               Z989Projeto_FatorMultiplicador = T00296_A989Projeto_FatorMultiplicador[0];
               Z990Projeto_ConstACocomo = T00296_A990Projeto_ConstACocomo[0];
               Z1232Projeto_Incremental = T00296_A1232Projeto_Incremental[0];
               Z2144Projeto_Identificador = T00296_A2144Projeto_Identificador[0];
               Z2146Projeto_DTInicio = T00296_A2146Projeto_DTInicio[0];
               Z2147Projeto_DTFim = T00296_A2147Projeto_DTFim[0];
               Z2148Projeto_PrazoPrevisto = T00296_A2148Projeto_PrazoPrevisto[0];
               Z2149Projeto_EsforcoPrevisto = T00296_A2149Projeto_EsforcoPrevisto[0];
               Z2150Projeto_CustoPrevisto = T00296_A2150Projeto_CustoPrevisto[0];
               Z2170Projeto_AnexoSequecial = T00296_A2170Projeto_AnexoSequecial[0];
               Z983Projeto_TipoProjetoCod = T00296_A983Projeto_TipoProjetoCod[0];
               Z2151Projeto_AreaTrabalhoCodigo = T00296_A2151Projeto_AreaTrabalhoCodigo[0];
               Z2153Projeto_GpoObjCtrlCodigo = T00296_A2153Projeto_GpoObjCtrlCodigo[0];
               Z2155Projeto_SistemaCodigo = T00296_A2155Projeto_SistemaCodigo[0];
               Z2158Projeto_ModuloCodigo = T00296_A2158Projeto_ModuloCodigo[0];
            }
            else
            {
               Z649Projeto_Nome = A649Projeto_Nome;
               Z650Projeto_Sigla = A650Projeto_Sigla;
               Z652Projeto_TecnicaContagem = A652Projeto_TecnicaContagem;
               Z1541Projeto_Previsao = A1541Projeto_Previsao;
               Z1542Projeto_GerenteCod = A1542Projeto_GerenteCod;
               Z1543Projeto_ServicoCod = A1543Projeto_ServicoCod;
               Z658Projeto_Status = A658Projeto_Status;
               Z985Projeto_FatorEscala = A985Projeto_FatorEscala;
               Z989Projeto_FatorMultiplicador = A989Projeto_FatorMultiplicador;
               Z990Projeto_ConstACocomo = A990Projeto_ConstACocomo;
               Z1232Projeto_Incremental = A1232Projeto_Incremental;
               Z2144Projeto_Identificador = A2144Projeto_Identificador;
               Z2146Projeto_DTInicio = A2146Projeto_DTInicio;
               Z2147Projeto_DTFim = A2147Projeto_DTFim;
               Z2148Projeto_PrazoPrevisto = A2148Projeto_PrazoPrevisto;
               Z2149Projeto_EsforcoPrevisto = A2149Projeto_EsforcoPrevisto;
               Z2150Projeto_CustoPrevisto = A2150Projeto_CustoPrevisto;
               Z2170Projeto_AnexoSequecial = A2170Projeto_AnexoSequecial;
               Z983Projeto_TipoProjetoCod = A983Projeto_TipoProjetoCod;
               Z2151Projeto_AreaTrabalhoCodigo = A2151Projeto_AreaTrabalhoCodigo;
               Z2153Projeto_GpoObjCtrlCodigo = A2153Projeto_GpoObjCtrlCodigo;
               Z2155Projeto_SistemaCodigo = A2155Projeto_SistemaCodigo;
               Z2158Projeto_ModuloCodigo = A2158Projeto_ModuloCodigo;
            }
         }
         if ( GX_JID == -38 )
         {
            Z648Projeto_Codigo = A648Projeto_Codigo;
            Z649Projeto_Nome = A649Projeto_Nome;
            Z650Projeto_Sigla = A650Projeto_Sigla;
            Z652Projeto_TecnicaContagem = A652Projeto_TecnicaContagem;
            Z1540Projeto_Introducao = A1540Projeto_Introducao;
            Z653Projeto_Escopo = A653Projeto_Escopo;
            Z1541Projeto_Previsao = A1541Projeto_Previsao;
            Z1542Projeto_GerenteCod = A1542Projeto_GerenteCod;
            Z1543Projeto_ServicoCod = A1543Projeto_ServicoCod;
            Z658Projeto_Status = A658Projeto_Status;
            Z985Projeto_FatorEscala = A985Projeto_FatorEscala;
            Z989Projeto_FatorMultiplicador = A989Projeto_FatorMultiplicador;
            Z990Projeto_ConstACocomo = A990Projeto_ConstACocomo;
            Z1232Projeto_Incremental = A1232Projeto_Incremental;
            Z2144Projeto_Identificador = A2144Projeto_Identificador;
            Z2145Projeto_Objetivo = A2145Projeto_Objetivo;
            Z2146Projeto_DTInicio = A2146Projeto_DTInicio;
            Z2147Projeto_DTFim = A2147Projeto_DTFim;
            Z2148Projeto_PrazoPrevisto = A2148Projeto_PrazoPrevisto;
            Z2149Projeto_EsforcoPrevisto = A2149Projeto_EsforcoPrevisto;
            Z2150Projeto_CustoPrevisto = A2150Projeto_CustoPrevisto;
            Z2170Projeto_AnexoSequecial = A2170Projeto_AnexoSequecial;
            Z983Projeto_TipoProjetoCod = A983Projeto_TipoProjetoCod;
            Z2151Projeto_AreaTrabalhoCodigo = A2151Projeto_AreaTrabalhoCodigo;
            Z2153Projeto_GpoObjCtrlCodigo = A2153Projeto_GpoObjCtrlCodigo;
            Z2155Projeto_SistemaCodigo = A2155Projeto_SistemaCodigo;
            Z2158Projeto_ModuloCodigo = A2158Projeto_ModuloCodigo;
            Z2152Projeto_AreaTrabalhoDescricao = A2152Projeto_AreaTrabalhoDescricao;
            Z2154Projeto_GpoObjCtrlNome = A2154Projeto_GpoObjCtrlNome;
            Z2156Projeto_SistemaNome = A2156Projeto_SistemaNome;
            Z2157Projeto_SistemaSigla = A2157Projeto_SistemaSigla;
            Z655Projeto_Custo = A655Projeto_Custo;
            Z656Projeto_Prazo = A656Projeto_Prazo;
            Z657Projeto_Esforco = A657Projeto_Esforco;
            Z2159Projeto_ModuloNome = A2159Projeto_ModuloNome;
            Z2160Projeto_ModuloSigla = A2160Projeto_ModuloSigla;
         }
      }

      protected void standaloneNotModal( )
      {
         GXAPROJETO_TIPOPROJETOCOD_html2986( ) ;
         GXAPROJETO_GPOOBJCTRLCODIGO_html2986( ) ;
         GXAPROJETO_MODULOCODIGO_html2986( ) ;
         edtProjeto_FatorEscala_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_FatorEscala_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_FatorEscala_Enabled), 5, 0)));
         edtProjeto_FatorMultiplicador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_FatorMultiplicador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_FatorMultiplicador_Enabled), 5, 0)));
         cmbProjeto_Status.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbProjeto_Status_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbProjeto_Status.Enabled), 5, 0)));
         edtProjeto_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_Codigo_Enabled), 5, 0)));
         AV24Pgmname = "Projeto";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Pgmname", AV24Pgmname);
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         edtProjeto_FatorEscala_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_FatorEscala_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_FatorEscala_Enabled), 5, 0)));
         edtProjeto_FatorMultiplicador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_FatorMultiplicador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_FatorMultiplicador_Enabled), 5, 0)));
         cmbProjeto_Status.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbProjeto_Status_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbProjeto_Status.Enabled), 5, 0)));
         edtProjeto_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7Projeto_Codigo) )
         {
            A648Projeto_Codigo = AV7Projeto_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Projeto_TipoProjetoCod) )
         {
            dynProjeto_TipoProjetoCod.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynProjeto_TipoProjetoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynProjeto_TipoProjetoCod.Enabled), 5, 0)));
         }
         else
         {
            dynProjeto_TipoProjetoCod.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynProjeto_TipoProjetoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynProjeto_TipoProjetoCod.Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV20Insert_Projeto_GpoObjCtrlCodigo) )
         {
            dynProjeto_GpoObjCtrlCodigo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynProjeto_GpoObjCtrlCodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynProjeto_GpoObjCtrlCodigo.Enabled), 5, 0)));
         }
         else
         {
            dynProjeto_GpoObjCtrlCodigo.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynProjeto_GpoObjCtrlCodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynProjeto_GpoObjCtrlCodigo.Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV21Insert_Projeto_SistemaCodigo) )
         {
            dynProjeto_SistemaCodigo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynProjeto_SistemaCodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynProjeto_SistemaCodigo.Enabled), 5, 0)));
         }
         else
         {
            dynProjeto_SistemaCodigo.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynProjeto_SistemaCodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynProjeto_SistemaCodigo.Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV22Insert_Projeto_ModuloCodigo) )
         {
            dynProjeto_ModuloCodigo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynProjeto_ModuloCodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynProjeto_ModuloCodigo.Enabled), 5, 0)));
         }
         else
         {
            dynProjeto_ModuloCodigo.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynProjeto_ModuloCodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynProjeto_ModuloCodigo.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV22Insert_Projeto_ModuloCodigo) )
         {
            A2158Projeto_ModuloCodigo = AV22Insert_Projeto_ModuloCodigo;
            n2158Projeto_ModuloCodigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2158Projeto_ModuloCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2158Projeto_ModuloCodigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV21Insert_Projeto_SistemaCodigo) )
         {
            A2155Projeto_SistemaCodigo = AV21Insert_Projeto_SistemaCodigo;
            n2155Projeto_SistemaCodigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2155Projeto_SistemaCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2155Projeto_SistemaCodigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV20Insert_Projeto_GpoObjCtrlCodigo) )
         {
            A2153Projeto_GpoObjCtrlCodigo = AV20Insert_Projeto_GpoObjCtrlCodigo;
            n2153Projeto_GpoObjCtrlCodigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2153Projeto_GpoObjCtrlCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2153Projeto_GpoObjCtrlCodigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Projeto_TipoProjetoCod) )
         {
            A983Projeto_TipoProjetoCod = AV12Insert_Projeto_TipoProjetoCod;
            n983Projeto_TipoProjetoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A983Projeto_TipoProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A983Projeto_TipoProjetoCod), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV19Insert_Projeto_AreaTrabalhoCodigo) )
         {
            A2151Projeto_AreaTrabalhoCodigo = AV19Insert_Projeto_AreaTrabalhoCodigo;
            n2151Projeto_AreaTrabalhoCodigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2151Projeto_AreaTrabalhoCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2151Projeto_AreaTrabalhoCodigo), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A2151Projeto_AreaTrabalhoCodigo) && ( Gx_BScreen == 0 ) )
            {
               A2151Projeto_AreaTrabalhoCodigo = AV8WWPContext.gxTpr_Areatrabalho_codigo;
               n2151Projeto_AreaTrabalhoCodigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2151Projeto_AreaTrabalhoCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2151Projeto_AreaTrabalhoCodigo), 6, 0)));
            }
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A658Projeto_Status)) && ( Gx_BScreen == 0 ) )
         {
            A658Projeto_Status = "A";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A658Projeto_Status", A658Projeto_Status);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T00299 */
            pr_default.execute(7, new Object[] {n2153Projeto_GpoObjCtrlCodigo, A2153Projeto_GpoObjCtrlCodigo});
            A2154Projeto_GpoObjCtrlNome = T00299_A2154Projeto_GpoObjCtrlNome[0];
            n2154Projeto_GpoObjCtrlNome = T00299_n2154Projeto_GpoObjCtrlNome[0];
            pr_default.close(7);
            /* Using cursor T002911 */
            pr_default.execute(9, new Object[] {n2158Projeto_ModuloCodigo, A2158Projeto_ModuloCodigo});
            A2159Projeto_ModuloNome = T002911_A2159Projeto_ModuloNome[0];
            n2159Projeto_ModuloNome = T002911_n2159Projeto_ModuloNome[0];
            A2160Projeto_ModuloSigla = T002911_A2160Projeto_ModuloSigla[0];
            n2160Projeto_ModuloSigla = T002911_n2160Projeto_ModuloSigla[0];
            pr_default.close(9);
            GXt_int1 = A740Projeto_SistemaCod;
            new prc_projeto_sistemacod(context ).execute( ref  A648Projeto_Codigo, ref  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
            A740Projeto_SistemaCod = GXt_int1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A740Projeto_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A740Projeto_SistemaCod), 6, 0)));
            /* Using cursor T002910 */
            pr_default.execute(8, new Object[] {n2155Projeto_SistemaCodigo, A2155Projeto_SistemaCodigo});
            A2156Projeto_SistemaNome = T002910_A2156Projeto_SistemaNome[0];
            n2156Projeto_SistemaNome = T002910_n2156Projeto_SistemaNome[0];
            A2157Projeto_SistemaSigla = T002910_A2157Projeto_SistemaSigla[0];
            n2157Projeto_SistemaSigla = T002910_n2157Projeto_SistemaSigla[0];
            pr_default.close(8);
            /* Using cursor T002913 */
            pr_default.execute(10, new Object[] {n2155Projeto_SistemaCodigo, A2155Projeto_SistemaCodigo});
            if ( (pr_default.getStatus(10) != 101) )
            {
               A655Projeto_Custo = T002913_A655Projeto_Custo[0];
               A656Projeto_Prazo = T002913_A656Projeto_Prazo[0];
               A657Projeto_Esforco = T002913_A657Projeto_Esforco[0];
               n657Projeto_Esforco = T002913_n657Projeto_Esforco[0];
            }
            else
            {
               A657Projeto_Esforco = 0;
               n657Projeto_Esforco = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A657Projeto_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A657Projeto_Esforco), 4, 0)));
               A656Projeto_Prazo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A656Projeto_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A656Projeto_Prazo), 4, 0)));
               A655Projeto_Custo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A655Projeto_Custo", StringUtil.LTrim( StringUtil.Str( A655Projeto_Custo, 12, 2)));
            }
            pr_default.close(10);
            /* Using cursor T00298 */
            pr_default.execute(6, new Object[] {n2151Projeto_AreaTrabalhoCodigo, A2151Projeto_AreaTrabalhoCodigo});
            A2152Projeto_AreaTrabalhoDescricao = T00298_A2152Projeto_AreaTrabalhoDescricao[0];
            n2152Projeto_AreaTrabalhoDescricao = T00298_n2152Projeto_AreaTrabalhoDescricao[0];
            pr_default.close(6);
            GXAPROJETO_SISTEMACODIGO_html2986( AV8WWPContext, A2153Projeto_GpoObjCtrlCodigo) ;
         }
      }

      protected void Load2986( )
      {
         /* Using cursor T002915 */
         pr_default.execute(11, new Object[] {A648Projeto_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound86 = 1;
            A649Projeto_Nome = T002915_A649Projeto_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A649Projeto_Nome", A649Projeto_Nome);
            A650Projeto_Sigla = T002915_A650Projeto_Sigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A650Projeto_Sigla", A650Projeto_Sigla);
            A652Projeto_TecnicaContagem = T002915_A652Projeto_TecnicaContagem[0];
            A1540Projeto_Introducao = T002915_A1540Projeto_Introducao[0];
            n1540Projeto_Introducao = T002915_n1540Projeto_Introducao[0];
            A653Projeto_Escopo = T002915_A653Projeto_Escopo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A653Projeto_Escopo", A653Projeto_Escopo);
            A1541Projeto_Previsao = T002915_A1541Projeto_Previsao[0];
            n1541Projeto_Previsao = T002915_n1541Projeto_Previsao[0];
            A1542Projeto_GerenteCod = T002915_A1542Projeto_GerenteCod[0];
            n1542Projeto_GerenteCod = T002915_n1542Projeto_GerenteCod[0];
            A1543Projeto_ServicoCod = T002915_A1543Projeto_ServicoCod[0];
            n1543Projeto_ServicoCod = T002915_n1543Projeto_ServicoCod[0];
            A658Projeto_Status = T002915_A658Projeto_Status[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A658Projeto_Status", A658Projeto_Status);
            A985Projeto_FatorEscala = T002915_A985Projeto_FatorEscala[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A985Projeto_FatorEscala", StringUtil.LTrim( StringUtil.Str( A985Projeto_FatorEscala, 6, 2)));
            n985Projeto_FatorEscala = T002915_n985Projeto_FatorEscala[0];
            A989Projeto_FatorMultiplicador = T002915_A989Projeto_FatorMultiplicador[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A989Projeto_FatorMultiplicador", StringUtil.LTrim( StringUtil.Str( A989Projeto_FatorMultiplicador, 6, 2)));
            n989Projeto_FatorMultiplicador = T002915_n989Projeto_FatorMultiplicador[0];
            A990Projeto_ConstACocomo = T002915_A990Projeto_ConstACocomo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A990Projeto_ConstACocomo", StringUtil.LTrim( StringUtil.Str( A990Projeto_ConstACocomo, 6, 2)));
            n990Projeto_ConstACocomo = T002915_n990Projeto_ConstACocomo[0];
            A1232Projeto_Incremental = T002915_A1232Projeto_Incremental[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1232Projeto_Incremental", A1232Projeto_Incremental);
            n1232Projeto_Incremental = T002915_n1232Projeto_Incremental[0];
            A2144Projeto_Identificador = T002915_A2144Projeto_Identificador[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2144Projeto_Identificador", A2144Projeto_Identificador);
            n2144Projeto_Identificador = T002915_n2144Projeto_Identificador[0];
            A2145Projeto_Objetivo = T002915_A2145Projeto_Objetivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2145Projeto_Objetivo", A2145Projeto_Objetivo);
            n2145Projeto_Objetivo = T002915_n2145Projeto_Objetivo[0];
            A2146Projeto_DTInicio = T002915_A2146Projeto_DTInicio[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2146Projeto_DTInicio", context.localUtil.Format(A2146Projeto_DTInicio, "99/99/99"));
            n2146Projeto_DTInicio = T002915_n2146Projeto_DTInicio[0];
            A2147Projeto_DTFim = T002915_A2147Projeto_DTFim[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2147Projeto_DTFim", context.localUtil.Format(A2147Projeto_DTFim, "99/99/99"));
            n2147Projeto_DTFim = T002915_n2147Projeto_DTFim[0];
            A2148Projeto_PrazoPrevisto = T002915_A2148Projeto_PrazoPrevisto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2148Projeto_PrazoPrevisto", StringUtil.LTrim( StringUtil.Str( (decimal)(A2148Projeto_PrazoPrevisto), 4, 0)));
            n2148Projeto_PrazoPrevisto = T002915_n2148Projeto_PrazoPrevisto[0];
            A2149Projeto_EsforcoPrevisto = T002915_A2149Projeto_EsforcoPrevisto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2149Projeto_EsforcoPrevisto", StringUtil.LTrim( StringUtil.Str( (decimal)(A2149Projeto_EsforcoPrevisto), 4, 0)));
            n2149Projeto_EsforcoPrevisto = T002915_n2149Projeto_EsforcoPrevisto[0];
            A2150Projeto_CustoPrevisto = T002915_A2150Projeto_CustoPrevisto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2150Projeto_CustoPrevisto", StringUtil.LTrim( StringUtil.Str( A2150Projeto_CustoPrevisto, 12, 2)));
            n2150Projeto_CustoPrevisto = T002915_n2150Projeto_CustoPrevisto[0];
            A2152Projeto_AreaTrabalhoDescricao = T002915_A2152Projeto_AreaTrabalhoDescricao[0];
            n2152Projeto_AreaTrabalhoDescricao = T002915_n2152Projeto_AreaTrabalhoDescricao[0];
            A2154Projeto_GpoObjCtrlNome = T002915_A2154Projeto_GpoObjCtrlNome[0];
            n2154Projeto_GpoObjCtrlNome = T002915_n2154Projeto_GpoObjCtrlNome[0];
            A2156Projeto_SistemaNome = T002915_A2156Projeto_SistemaNome[0];
            n2156Projeto_SistemaNome = T002915_n2156Projeto_SistemaNome[0];
            A2157Projeto_SistemaSigla = T002915_A2157Projeto_SistemaSigla[0];
            n2157Projeto_SistemaSigla = T002915_n2157Projeto_SistemaSigla[0];
            A2159Projeto_ModuloNome = T002915_A2159Projeto_ModuloNome[0];
            n2159Projeto_ModuloNome = T002915_n2159Projeto_ModuloNome[0];
            A2160Projeto_ModuloSigla = T002915_A2160Projeto_ModuloSigla[0];
            n2160Projeto_ModuloSigla = T002915_n2160Projeto_ModuloSigla[0];
            A2170Projeto_AnexoSequecial = T002915_A2170Projeto_AnexoSequecial[0];
            n2170Projeto_AnexoSequecial = T002915_n2170Projeto_AnexoSequecial[0];
            A983Projeto_TipoProjetoCod = T002915_A983Projeto_TipoProjetoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A983Projeto_TipoProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A983Projeto_TipoProjetoCod), 6, 0)));
            n983Projeto_TipoProjetoCod = T002915_n983Projeto_TipoProjetoCod[0];
            A2151Projeto_AreaTrabalhoCodigo = T002915_A2151Projeto_AreaTrabalhoCodigo[0];
            n2151Projeto_AreaTrabalhoCodigo = T002915_n2151Projeto_AreaTrabalhoCodigo[0];
            A2153Projeto_GpoObjCtrlCodigo = T002915_A2153Projeto_GpoObjCtrlCodigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2153Projeto_GpoObjCtrlCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2153Projeto_GpoObjCtrlCodigo), 6, 0)));
            n2153Projeto_GpoObjCtrlCodigo = T002915_n2153Projeto_GpoObjCtrlCodigo[0];
            A2155Projeto_SistemaCodigo = T002915_A2155Projeto_SistemaCodigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2155Projeto_SistemaCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2155Projeto_SistemaCodigo), 6, 0)));
            n2155Projeto_SistemaCodigo = T002915_n2155Projeto_SistemaCodigo[0];
            A2158Projeto_ModuloCodigo = T002915_A2158Projeto_ModuloCodigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2158Projeto_ModuloCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2158Projeto_ModuloCodigo), 6, 0)));
            n2158Projeto_ModuloCodigo = T002915_n2158Projeto_ModuloCodigo[0];
            A655Projeto_Custo = T002915_A655Projeto_Custo[0];
            A656Projeto_Prazo = T002915_A656Projeto_Prazo[0];
            A657Projeto_Esforco = T002915_A657Projeto_Esforco[0];
            n657Projeto_Esforco = T002915_n657Projeto_Esforco[0];
            ZM2986( -38) ;
         }
         pr_default.close(11);
         OnLoadActions2986( ) ;
      }

      protected void OnLoadActions2986( )
      {
         GXAPROJETO_SISTEMACODIGO_html2986( AV8WWPContext, A2153Projeto_GpoObjCtrlCodigo) ;
         GXt_int1 = A740Projeto_SistemaCod;
         new prc_projeto_sistemacod(context ).execute( ref  A648Projeto_Codigo, ref  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
         A740Projeto_SistemaCod = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A740Projeto_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A740Projeto_SistemaCod), 6, 0)));
      }

      protected void CheckExtendedTable2986( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         GXAPROJETO_SISTEMACODIGO_html2986( AV8WWPContext, A2153Projeto_GpoObjCtrlCodigo) ;
         GXt_int1 = A740Projeto_SistemaCod;
         new prc_projeto_sistemacod(context ).execute( ref  A648Projeto_Codigo, ref  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
         A740Projeto_SistemaCod = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A740Projeto_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A740Projeto_SistemaCod), 6, 0)));
         /* Using cursor T00297 */
         pr_default.execute(5, new Object[] {n983Projeto_TipoProjetoCod, A983Projeto_TipoProjetoCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A983Projeto_TipoProjetoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Projeto_Tipo Projeto'.", "ForeignKeyNotFound", 1, "PROJETO_TIPOPROJETOCOD");
               AnyError = 1;
               GX_FocusControl = dynProjeto_TipoProjetoCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(5);
         if ( ! ( (DateTime.MinValue==A2146Projeto_DTInicio) || ( A2146Projeto_DTInicio >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data Prevista de In�cio fora do intervalo", "OutOfRange", 1, "PROJETO_DTINICIO");
            AnyError = 1;
            GX_FocusControl = edtProjeto_DTInicio_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A2147Projeto_DTFim) || ( A2147Projeto_DTFim >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data Prevista de T�rmino fora do intervalo", "OutOfRange", 1, "PROJETO_DTFIM");
            AnyError = 1;
            GX_FocusControl = edtProjeto_DTFim_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T00299 */
         pr_default.execute(7, new Object[] {n2153Projeto_GpoObjCtrlCodigo, A2153Projeto_GpoObjCtrlCodigo});
         if ( (pr_default.getStatus(7) == 101) )
         {
            if ( ! ( (0==A2153Projeto_GpoObjCtrlCodigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Grupo Objeto Controle'.", "ForeignKeyNotFound", 1, "PROJETO_GPOOBJCTRLCODIGO");
               AnyError = 1;
               GX_FocusControl = dynProjeto_GpoObjCtrlCodigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A2154Projeto_GpoObjCtrlNome = T00299_A2154Projeto_GpoObjCtrlNome[0];
         n2154Projeto_GpoObjCtrlNome = T00299_n2154Projeto_GpoObjCtrlNome[0];
         pr_default.close(7);
         /* Using cursor T002910 */
         pr_default.execute(8, new Object[] {n2155Projeto_SistemaCodigo, A2155Projeto_SistemaCodigo});
         if ( (pr_default.getStatus(8) == 101) )
         {
            if ( ! ( (0==A2155Projeto_SistemaCodigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "PROJETO_SISTEMACODIGO");
               AnyError = 1;
               GX_FocusControl = dynProjeto_SistemaCodigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A2156Projeto_SistemaNome = T002910_A2156Projeto_SistemaNome[0];
         n2156Projeto_SistemaNome = T002910_n2156Projeto_SistemaNome[0];
         A2157Projeto_SistemaSigla = T002910_A2157Projeto_SistemaSigla[0];
         n2157Projeto_SistemaSigla = T002910_n2157Projeto_SistemaSigla[0];
         pr_default.close(8);
         /* Using cursor T002913 */
         pr_default.execute(10, new Object[] {n2155Projeto_SistemaCodigo, A2155Projeto_SistemaCodigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            A655Projeto_Custo = T002913_A655Projeto_Custo[0];
            A656Projeto_Prazo = T002913_A656Projeto_Prazo[0];
            A657Projeto_Esforco = T002913_A657Projeto_Esforco[0];
            n657Projeto_Esforco = T002913_n657Projeto_Esforco[0];
         }
         else
         {
            A657Projeto_Esforco = 0;
            n657Projeto_Esforco = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A657Projeto_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A657Projeto_Esforco), 4, 0)));
            A656Projeto_Prazo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A656Projeto_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A656Projeto_Prazo), 4, 0)));
            A655Projeto_Custo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A655Projeto_Custo", StringUtil.LTrim( StringUtil.Str( A655Projeto_Custo, 12, 2)));
         }
         pr_default.close(10);
         /* Using cursor T002911 */
         pr_default.execute(9, new Object[] {n2158Projeto_ModuloCodigo, A2158Projeto_ModuloCodigo});
         if ( (pr_default.getStatus(9) == 101) )
         {
            if ( ! ( (0==A2158Projeto_ModuloCodigo) ) )
            {
               GX_msglist.addItem("N�o existe 'M�dulo '.", "ForeignKeyNotFound", 1, "PROJETO_MODULOCODIGO");
               AnyError = 1;
               GX_FocusControl = dynProjeto_ModuloCodigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A2159Projeto_ModuloNome = T002911_A2159Projeto_ModuloNome[0];
         n2159Projeto_ModuloNome = T002911_n2159Projeto_ModuloNome[0];
         A2160Projeto_ModuloSigla = T002911_A2160Projeto_ModuloSigla[0];
         n2160Projeto_ModuloSigla = T002911_n2160Projeto_ModuloSigla[0];
         pr_default.close(9);
         /* Using cursor T00298 */
         pr_default.execute(6, new Object[] {n2151Projeto_AreaTrabalhoCodigo, A2151Projeto_AreaTrabalhoCodigo});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( (0==A2151Projeto_AreaTrabalhoCodigo) ) )
            {
               GX_msglist.addItem("N�o existe '�real de Trabalho'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A2152Projeto_AreaTrabalhoDescricao = T00298_A2152Projeto_AreaTrabalhoDescricao[0];
         n2152Projeto_AreaTrabalhoDescricao = T00298_n2152Projeto_AreaTrabalhoDescricao[0];
         pr_default.close(6);
      }

      protected void CloseExtendedTableCursors2986( )
      {
         pr_default.close(5);
         pr_default.close(7);
         pr_default.close(8);
         pr_default.close(10);
         pr_default.close(9);
         pr_default.close(6);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_39( int A983Projeto_TipoProjetoCod )
      {
         /* Using cursor T002916 */
         pr_default.execute(12, new Object[] {n983Projeto_TipoProjetoCod, A983Projeto_TipoProjetoCod});
         if ( (pr_default.getStatus(12) == 101) )
         {
            if ( ! ( (0==A983Projeto_TipoProjetoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Projeto_Tipo Projeto'.", "ForeignKeyNotFound", 1, "PROJETO_TIPOPROJETOCOD");
               AnyError = 1;
               GX_FocusControl = dynProjeto_TipoProjetoCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(12) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(12);
      }

      protected void gxLoad_41( int A2153Projeto_GpoObjCtrlCodigo )
      {
         /* Using cursor T002917 */
         pr_default.execute(13, new Object[] {n2153Projeto_GpoObjCtrlCodigo, A2153Projeto_GpoObjCtrlCodigo});
         if ( (pr_default.getStatus(13) == 101) )
         {
            if ( ! ( (0==A2153Projeto_GpoObjCtrlCodigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Grupo Objeto Controle'.", "ForeignKeyNotFound", 1, "PROJETO_GPOOBJCTRLCODIGO");
               AnyError = 1;
               GX_FocusControl = dynProjeto_GpoObjCtrlCodigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A2154Projeto_GpoObjCtrlNome = T002917_A2154Projeto_GpoObjCtrlNome[0];
         n2154Projeto_GpoObjCtrlNome = T002917_n2154Projeto_GpoObjCtrlNome[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A2154Projeto_GpoObjCtrlNome))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(13) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(13);
      }

      protected void gxLoad_42( int A2155Projeto_SistemaCodigo )
      {
         /* Using cursor T002918 */
         pr_default.execute(14, new Object[] {n2155Projeto_SistemaCodigo, A2155Projeto_SistemaCodigo});
         if ( (pr_default.getStatus(14) == 101) )
         {
            if ( ! ( (0==A2155Projeto_SistemaCodigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "PROJETO_SISTEMACODIGO");
               AnyError = 1;
               GX_FocusControl = dynProjeto_SistemaCodigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A2156Projeto_SistemaNome = T002918_A2156Projeto_SistemaNome[0];
         n2156Projeto_SistemaNome = T002918_n2156Projeto_SistemaNome[0];
         A2157Projeto_SistemaSigla = T002918_A2157Projeto_SistemaSigla[0];
         n2157Projeto_SistemaSigla = T002918_n2157Projeto_SistemaSigla[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A2156Projeto_SistemaNome)+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A2157Projeto_SistemaSigla))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(14) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(14);
      }

      protected void gxLoad_44( int A2155Projeto_SistemaCodigo )
      {
         /* Using cursor T002920 */
         pr_default.execute(15, new Object[] {n2155Projeto_SistemaCodigo, A2155Projeto_SistemaCodigo});
         if ( (pr_default.getStatus(15) != 101) )
         {
            A655Projeto_Custo = T002920_A655Projeto_Custo[0];
            A656Projeto_Prazo = T002920_A656Projeto_Prazo[0];
            A657Projeto_Esforco = T002920_A657Projeto_Esforco[0];
            n657Projeto_Esforco = T002920_n657Projeto_Esforco[0];
         }
         else
         {
            A657Projeto_Esforco = 0;
            n657Projeto_Esforco = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A657Projeto_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A657Projeto_Esforco), 4, 0)));
            A656Projeto_Prazo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A656Projeto_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A656Projeto_Prazo), 4, 0)));
            A655Projeto_Custo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A655Projeto_Custo", StringUtil.LTrim( StringUtil.Str( A655Projeto_Custo, 12, 2)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( A655Projeto_Custo, 12, 2, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A656Projeto_Prazo), 4, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A657Projeto_Esforco), 4, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(15) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(15);
      }

      protected void gxLoad_43( int A2158Projeto_ModuloCodigo )
      {
         /* Using cursor T002921 */
         pr_default.execute(16, new Object[] {n2158Projeto_ModuloCodigo, A2158Projeto_ModuloCodigo});
         if ( (pr_default.getStatus(16) == 101) )
         {
            if ( ! ( (0==A2158Projeto_ModuloCodigo) ) )
            {
               GX_msglist.addItem("N�o existe 'M�dulo '.", "ForeignKeyNotFound", 1, "PROJETO_MODULOCODIGO");
               AnyError = 1;
               GX_FocusControl = dynProjeto_ModuloCodigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A2159Projeto_ModuloNome = T002921_A2159Projeto_ModuloNome[0];
         n2159Projeto_ModuloNome = T002921_n2159Projeto_ModuloNome[0];
         A2160Projeto_ModuloSigla = T002921_A2160Projeto_ModuloSigla[0];
         n2160Projeto_ModuloSigla = T002921_n2160Projeto_ModuloSigla[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A2159Projeto_ModuloNome))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A2160Projeto_ModuloSigla))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(16) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(16);
      }

      protected void gxLoad_40( int A2151Projeto_AreaTrabalhoCodigo )
      {
         /* Using cursor T002922 */
         pr_default.execute(17, new Object[] {n2151Projeto_AreaTrabalhoCodigo, A2151Projeto_AreaTrabalhoCodigo});
         if ( (pr_default.getStatus(17) == 101) )
         {
            if ( ! ( (0==A2151Projeto_AreaTrabalhoCodigo) ) )
            {
               GX_msglist.addItem("N�o existe '�real de Trabalho'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A2152Projeto_AreaTrabalhoDescricao = T002922_A2152Projeto_AreaTrabalhoDescricao[0];
         n2152Projeto_AreaTrabalhoDescricao = T002922_n2152Projeto_AreaTrabalhoDescricao[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A2152Projeto_AreaTrabalhoDescricao)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(17) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(17);
      }

      protected void GetKey2986( )
      {
         /* Using cursor T002923 */
         pr_default.execute(18, new Object[] {A648Projeto_Codigo});
         if ( (pr_default.getStatus(18) != 101) )
         {
            RcdFound86 = 1;
         }
         else
         {
            RcdFound86 = 0;
         }
         pr_default.close(18);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00296 */
         pr_default.execute(4, new Object[] {A648Projeto_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            ZM2986( 38) ;
            RcdFound86 = 1;
            A648Projeto_Codigo = T00296_A648Projeto_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
            A649Projeto_Nome = T00296_A649Projeto_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A649Projeto_Nome", A649Projeto_Nome);
            A650Projeto_Sigla = T00296_A650Projeto_Sigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A650Projeto_Sigla", A650Projeto_Sigla);
            A652Projeto_TecnicaContagem = T00296_A652Projeto_TecnicaContagem[0];
            A1540Projeto_Introducao = T00296_A1540Projeto_Introducao[0];
            n1540Projeto_Introducao = T00296_n1540Projeto_Introducao[0];
            A653Projeto_Escopo = T00296_A653Projeto_Escopo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A653Projeto_Escopo", A653Projeto_Escopo);
            A1541Projeto_Previsao = T00296_A1541Projeto_Previsao[0];
            n1541Projeto_Previsao = T00296_n1541Projeto_Previsao[0];
            A1542Projeto_GerenteCod = T00296_A1542Projeto_GerenteCod[0];
            n1542Projeto_GerenteCod = T00296_n1542Projeto_GerenteCod[0];
            A1543Projeto_ServicoCod = T00296_A1543Projeto_ServicoCod[0];
            n1543Projeto_ServicoCod = T00296_n1543Projeto_ServicoCod[0];
            A658Projeto_Status = T00296_A658Projeto_Status[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A658Projeto_Status", A658Projeto_Status);
            A985Projeto_FatorEscala = T00296_A985Projeto_FatorEscala[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A985Projeto_FatorEscala", StringUtil.LTrim( StringUtil.Str( A985Projeto_FatorEscala, 6, 2)));
            n985Projeto_FatorEscala = T00296_n985Projeto_FatorEscala[0];
            A989Projeto_FatorMultiplicador = T00296_A989Projeto_FatorMultiplicador[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A989Projeto_FatorMultiplicador", StringUtil.LTrim( StringUtil.Str( A989Projeto_FatorMultiplicador, 6, 2)));
            n989Projeto_FatorMultiplicador = T00296_n989Projeto_FatorMultiplicador[0];
            A990Projeto_ConstACocomo = T00296_A990Projeto_ConstACocomo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A990Projeto_ConstACocomo", StringUtil.LTrim( StringUtil.Str( A990Projeto_ConstACocomo, 6, 2)));
            n990Projeto_ConstACocomo = T00296_n990Projeto_ConstACocomo[0];
            A1232Projeto_Incremental = T00296_A1232Projeto_Incremental[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1232Projeto_Incremental", A1232Projeto_Incremental);
            n1232Projeto_Incremental = T00296_n1232Projeto_Incremental[0];
            A2144Projeto_Identificador = T00296_A2144Projeto_Identificador[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2144Projeto_Identificador", A2144Projeto_Identificador);
            n2144Projeto_Identificador = T00296_n2144Projeto_Identificador[0];
            A2145Projeto_Objetivo = T00296_A2145Projeto_Objetivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2145Projeto_Objetivo", A2145Projeto_Objetivo);
            n2145Projeto_Objetivo = T00296_n2145Projeto_Objetivo[0];
            A2146Projeto_DTInicio = T00296_A2146Projeto_DTInicio[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2146Projeto_DTInicio", context.localUtil.Format(A2146Projeto_DTInicio, "99/99/99"));
            n2146Projeto_DTInicio = T00296_n2146Projeto_DTInicio[0];
            A2147Projeto_DTFim = T00296_A2147Projeto_DTFim[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2147Projeto_DTFim", context.localUtil.Format(A2147Projeto_DTFim, "99/99/99"));
            n2147Projeto_DTFim = T00296_n2147Projeto_DTFim[0];
            A2148Projeto_PrazoPrevisto = T00296_A2148Projeto_PrazoPrevisto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2148Projeto_PrazoPrevisto", StringUtil.LTrim( StringUtil.Str( (decimal)(A2148Projeto_PrazoPrevisto), 4, 0)));
            n2148Projeto_PrazoPrevisto = T00296_n2148Projeto_PrazoPrevisto[0];
            A2149Projeto_EsforcoPrevisto = T00296_A2149Projeto_EsforcoPrevisto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2149Projeto_EsforcoPrevisto", StringUtil.LTrim( StringUtil.Str( (decimal)(A2149Projeto_EsforcoPrevisto), 4, 0)));
            n2149Projeto_EsforcoPrevisto = T00296_n2149Projeto_EsforcoPrevisto[0];
            A2150Projeto_CustoPrevisto = T00296_A2150Projeto_CustoPrevisto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2150Projeto_CustoPrevisto", StringUtil.LTrim( StringUtil.Str( A2150Projeto_CustoPrevisto, 12, 2)));
            n2150Projeto_CustoPrevisto = T00296_n2150Projeto_CustoPrevisto[0];
            A2170Projeto_AnexoSequecial = T00296_A2170Projeto_AnexoSequecial[0];
            n2170Projeto_AnexoSequecial = T00296_n2170Projeto_AnexoSequecial[0];
            A983Projeto_TipoProjetoCod = T00296_A983Projeto_TipoProjetoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A983Projeto_TipoProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A983Projeto_TipoProjetoCod), 6, 0)));
            n983Projeto_TipoProjetoCod = T00296_n983Projeto_TipoProjetoCod[0];
            A2151Projeto_AreaTrabalhoCodigo = T00296_A2151Projeto_AreaTrabalhoCodigo[0];
            n2151Projeto_AreaTrabalhoCodigo = T00296_n2151Projeto_AreaTrabalhoCodigo[0];
            A2153Projeto_GpoObjCtrlCodigo = T00296_A2153Projeto_GpoObjCtrlCodigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2153Projeto_GpoObjCtrlCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2153Projeto_GpoObjCtrlCodigo), 6, 0)));
            n2153Projeto_GpoObjCtrlCodigo = T00296_n2153Projeto_GpoObjCtrlCodigo[0];
            A2155Projeto_SistemaCodigo = T00296_A2155Projeto_SistemaCodigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2155Projeto_SistemaCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2155Projeto_SistemaCodigo), 6, 0)));
            n2155Projeto_SistemaCodigo = T00296_n2155Projeto_SistemaCodigo[0];
            A2158Projeto_ModuloCodigo = T00296_A2158Projeto_ModuloCodigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2158Projeto_ModuloCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2158Projeto_ModuloCodigo), 6, 0)));
            n2158Projeto_ModuloCodigo = T00296_n2158Projeto_ModuloCodigo[0];
            O2170Projeto_AnexoSequecial = A2170Projeto_AnexoSequecial;
            n2170Projeto_AnexoSequecial = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2170Projeto_AnexoSequecial", StringUtil.LTrim( StringUtil.Str( (decimal)(A2170Projeto_AnexoSequecial), 4, 0)));
            Z648Projeto_Codigo = A648Projeto_Codigo;
            sMode86 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load2986( ) ;
            if ( AnyError == 1 )
            {
               RcdFound86 = 0;
               InitializeNonKey2986( ) ;
            }
            Gx_mode = sMode86;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound86 = 0;
            InitializeNonKey2986( ) ;
            sMode86 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode86;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(4);
      }

      protected void getEqualNoModal( )
      {
         GetKey2986( ) ;
         if ( RcdFound86 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound86 = 0;
         /* Using cursor T002924 */
         pr_default.execute(19, new Object[] {A648Projeto_Codigo});
         if ( (pr_default.getStatus(19) != 101) )
         {
            while ( (pr_default.getStatus(19) != 101) && ( ( T002924_A648Projeto_Codigo[0] < A648Projeto_Codigo ) ) )
            {
               pr_default.readNext(19);
            }
            if ( (pr_default.getStatus(19) != 101) && ( ( T002924_A648Projeto_Codigo[0] > A648Projeto_Codigo ) ) )
            {
               A648Projeto_Codigo = T002924_A648Projeto_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
               RcdFound86 = 1;
            }
         }
         pr_default.close(19);
      }

      protected void move_previous( )
      {
         RcdFound86 = 0;
         /* Using cursor T002925 */
         pr_default.execute(20, new Object[] {A648Projeto_Codigo});
         if ( (pr_default.getStatus(20) != 101) )
         {
            while ( (pr_default.getStatus(20) != 101) && ( ( T002925_A648Projeto_Codigo[0] > A648Projeto_Codigo ) ) )
            {
               pr_default.readNext(20);
            }
            if ( (pr_default.getStatus(20) != 101) && ( ( T002925_A648Projeto_Codigo[0] < A648Projeto_Codigo ) ) )
            {
               A648Projeto_Codigo = T002925_A648Projeto_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
               RcdFound86 = 1;
            }
         }
         pr_default.close(20);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2986( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            A2170Projeto_AnexoSequecial = O2170Projeto_AnexoSequecial;
            n2170Projeto_AnexoSequecial = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2170Projeto_AnexoSequecial", StringUtil.LTrim( StringUtil.Str( (decimal)(A2170Projeto_AnexoSequecial), 4, 0)));
            GX_FocusControl = edtProjeto_Sigla_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2986( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound86 == 1 )
            {
               if ( A648Projeto_Codigo != Z648Projeto_Codigo )
               {
                  A648Projeto_Codigo = Z648Projeto_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "PROJETO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtProjeto_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  A2170Projeto_AnexoSequecial = O2170Projeto_AnexoSequecial;
                  n2170Projeto_AnexoSequecial = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2170Projeto_AnexoSequecial", StringUtil.LTrim( StringUtil.Str( (decimal)(A2170Projeto_AnexoSequecial), 4, 0)));
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtProjeto_Sigla_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  A2170Projeto_AnexoSequecial = O2170Projeto_AnexoSequecial;
                  n2170Projeto_AnexoSequecial = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2170Projeto_AnexoSequecial", StringUtil.LTrim( StringUtil.Str( (decimal)(A2170Projeto_AnexoSequecial), 4, 0)));
                  Update2986( ) ;
                  GX_FocusControl = edtProjeto_Sigla_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A648Projeto_Codigo != Z648Projeto_Codigo )
               {
                  /* Insert record */
                  A2170Projeto_AnexoSequecial = O2170Projeto_AnexoSequecial;
                  n2170Projeto_AnexoSequecial = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2170Projeto_AnexoSequecial", StringUtil.LTrim( StringUtil.Str( (decimal)(A2170Projeto_AnexoSequecial), 4, 0)));
                  GX_FocusControl = edtProjeto_Sigla_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2986( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "PROJETO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtProjeto_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     A2170Projeto_AnexoSequecial = O2170Projeto_AnexoSequecial;
                     n2170Projeto_AnexoSequecial = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2170Projeto_AnexoSequecial", StringUtil.LTrim( StringUtil.Str( (decimal)(A2170Projeto_AnexoSequecial), 4, 0)));
                     GX_FocusControl = edtProjeto_Sigla_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2986( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A648Projeto_Codigo != Z648Projeto_Codigo )
         {
            A648Projeto_Codigo = Z648Projeto_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "PROJETO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtProjeto_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            A2170Projeto_AnexoSequecial = O2170Projeto_AnexoSequecial;
            n2170Projeto_AnexoSequecial = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2170Projeto_AnexoSequecial", StringUtil.LTrim( StringUtil.Str( (decimal)(A2170Projeto_AnexoSequecial), 4, 0)));
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtProjeto_Sigla_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void subgridanexos_addlines( int nLines )
      {
         nKeyPressed = 4;
         nBlankRcdUsr235 = (short)(nBlankRcdUsr235+nLines);
         if ( isFullAjaxMode( ) && ( nGXsfl_154_Refreshing == 0 ) )
         {
            context.DoAjaxAddLines(154, nLines);
         }
      }

      protected void CheckOptimisticConcurrency2986( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00295 */
            pr_default.execute(3, new Object[] {A648Projeto_Codigo});
            if ( (pr_default.getStatus(3) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Projeto"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(3) == 101) || ( StringUtil.StrCmp(Z649Projeto_Nome, T00295_A649Projeto_Nome[0]) != 0 ) || ( StringUtil.StrCmp(Z650Projeto_Sigla, T00295_A650Projeto_Sigla[0]) != 0 ) || ( StringUtil.StrCmp(Z652Projeto_TecnicaContagem, T00295_A652Projeto_TecnicaContagem[0]) != 0 ) || ( Z1541Projeto_Previsao != T00295_A1541Projeto_Previsao[0] ) || ( Z1542Projeto_GerenteCod != T00295_A1542Projeto_GerenteCod[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1543Projeto_ServicoCod != T00295_A1543Projeto_ServicoCod[0] ) || ( StringUtil.StrCmp(Z658Projeto_Status, T00295_A658Projeto_Status[0]) != 0 ) || ( Z985Projeto_FatorEscala != T00295_A985Projeto_FatorEscala[0] ) || ( Z989Projeto_FatorMultiplicador != T00295_A989Projeto_FatorMultiplicador[0] ) || ( Z990Projeto_ConstACocomo != T00295_A990Projeto_ConstACocomo[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1232Projeto_Incremental != T00295_A1232Projeto_Incremental[0] ) || ( StringUtil.StrCmp(Z2144Projeto_Identificador, T00295_A2144Projeto_Identificador[0]) != 0 ) || ( Z2146Projeto_DTInicio != T00295_A2146Projeto_DTInicio[0] ) || ( Z2147Projeto_DTFim != T00295_A2147Projeto_DTFim[0] ) || ( Z2148Projeto_PrazoPrevisto != T00295_A2148Projeto_PrazoPrevisto[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z2149Projeto_EsforcoPrevisto != T00295_A2149Projeto_EsforcoPrevisto[0] ) || ( Z2150Projeto_CustoPrevisto != T00295_A2150Projeto_CustoPrevisto[0] ) || ( Z2170Projeto_AnexoSequecial != T00295_A2170Projeto_AnexoSequecial[0] ) || ( Z983Projeto_TipoProjetoCod != T00295_A983Projeto_TipoProjetoCod[0] ) || ( Z2151Projeto_AreaTrabalhoCodigo != T00295_A2151Projeto_AreaTrabalhoCodigo[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z2153Projeto_GpoObjCtrlCodigo != T00295_A2153Projeto_GpoObjCtrlCodigo[0] ) || ( Z2155Projeto_SistemaCodigo != T00295_A2155Projeto_SistemaCodigo[0] ) || ( Z2158Projeto_ModuloCodigo != T00295_A2158Projeto_ModuloCodigo[0] ) )
            {
               if ( StringUtil.StrCmp(Z649Projeto_Nome, T00295_A649Projeto_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z649Projeto_Nome);
                  GXUtil.WriteLogRaw("Current: ",T00295_A649Projeto_Nome[0]);
               }
               if ( StringUtil.StrCmp(Z650Projeto_Sigla, T00295_A650Projeto_Sigla[0]) != 0 )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_Sigla");
                  GXUtil.WriteLogRaw("Old: ",Z650Projeto_Sigla);
                  GXUtil.WriteLogRaw("Current: ",T00295_A650Projeto_Sigla[0]);
               }
               if ( StringUtil.StrCmp(Z652Projeto_TecnicaContagem, T00295_A652Projeto_TecnicaContagem[0]) != 0 )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_TecnicaContagem");
                  GXUtil.WriteLogRaw("Old: ",Z652Projeto_TecnicaContagem);
                  GXUtil.WriteLogRaw("Current: ",T00295_A652Projeto_TecnicaContagem[0]);
               }
               if ( Z1541Projeto_Previsao != T00295_A1541Projeto_Previsao[0] )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_Previsao");
                  GXUtil.WriteLogRaw("Old: ",Z1541Projeto_Previsao);
                  GXUtil.WriteLogRaw("Current: ",T00295_A1541Projeto_Previsao[0]);
               }
               if ( Z1542Projeto_GerenteCod != T00295_A1542Projeto_GerenteCod[0] )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_GerenteCod");
                  GXUtil.WriteLogRaw("Old: ",Z1542Projeto_GerenteCod);
                  GXUtil.WriteLogRaw("Current: ",T00295_A1542Projeto_GerenteCod[0]);
               }
               if ( Z1543Projeto_ServicoCod != T00295_A1543Projeto_ServicoCod[0] )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_ServicoCod");
                  GXUtil.WriteLogRaw("Old: ",Z1543Projeto_ServicoCod);
                  GXUtil.WriteLogRaw("Current: ",T00295_A1543Projeto_ServicoCod[0]);
               }
               if ( StringUtil.StrCmp(Z658Projeto_Status, T00295_A658Projeto_Status[0]) != 0 )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_Status");
                  GXUtil.WriteLogRaw("Old: ",Z658Projeto_Status);
                  GXUtil.WriteLogRaw("Current: ",T00295_A658Projeto_Status[0]);
               }
               if ( Z985Projeto_FatorEscala != T00295_A985Projeto_FatorEscala[0] )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_FatorEscala");
                  GXUtil.WriteLogRaw("Old: ",Z985Projeto_FatorEscala);
                  GXUtil.WriteLogRaw("Current: ",T00295_A985Projeto_FatorEscala[0]);
               }
               if ( Z989Projeto_FatorMultiplicador != T00295_A989Projeto_FatorMultiplicador[0] )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_FatorMultiplicador");
                  GXUtil.WriteLogRaw("Old: ",Z989Projeto_FatorMultiplicador);
                  GXUtil.WriteLogRaw("Current: ",T00295_A989Projeto_FatorMultiplicador[0]);
               }
               if ( Z990Projeto_ConstACocomo != T00295_A990Projeto_ConstACocomo[0] )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_ConstACocomo");
                  GXUtil.WriteLogRaw("Old: ",Z990Projeto_ConstACocomo);
                  GXUtil.WriteLogRaw("Current: ",T00295_A990Projeto_ConstACocomo[0]);
               }
               if ( Z1232Projeto_Incremental != T00295_A1232Projeto_Incremental[0] )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_Incremental");
                  GXUtil.WriteLogRaw("Old: ",Z1232Projeto_Incremental);
                  GXUtil.WriteLogRaw("Current: ",T00295_A1232Projeto_Incremental[0]);
               }
               if ( StringUtil.StrCmp(Z2144Projeto_Identificador, T00295_A2144Projeto_Identificador[0]) != 0 )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_Identificador");
                  GXUtil.WriteLogRaw("Old: ",Z2144Projeto_Identificador);
                  GXUtil.WriteLogRaw("Current: ",T00295_A2144Projeto_Identificador[0]);
               }
               if ( Z2146Projeto_DTInicio != T00295_A2146Projeto_DTInicio[0] )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_DTInicio");
                  GXUtil.WriteLogRaw("Old: ",Z2146Projeto_DTInicio);
                  GXUtil.WriteLogRaw("Current: ",T00295_A2146Projeto_DTInicio[0]);
               }
               if ( Z2147Projeto_DTFim != T00295_A2147Projeto_DTFim[0] )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_DTFim");
                  GXUtil.WriteLogRaw("Old: ",Z2147Projeto_DTFim);
                  GXUtil.WriteLogRaw("Current: ",T00295_A2147Projeto_DTFim[0]);
               }
               if ( Z2148Projeto_PrazoPrevisto != T00295_A2148Projeto_PrazoPrevisto[0] )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_PrazoPrevisto");
                  GXUtil.WriteLogRaw("Old: ",Z2148Projeto_PrazoPrevisto);
                  GXUtil.WriteLogRaw("Current: ",T00295_A2148Projeto_PrazoPrevisto[0]);
               }
               if ( Z2149Projeto_EsforcoPrevisto != T00295_A2149Projeto_EsforcoPrevisto[0] )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_EsforcoPrevisto");
                  GXUtil.WriteLogRaw("Old: ",Z2149Projeto_EsforcoPrevisto);
                  GXUtil.WriteLogRaw("Current: ",T00295_A2149Projeto_EsforcoPrevisto[0]);
               }
               if ( Z2150Projeto_CustoPrevisto != T00295_A2150Projeto_CustoPrevisto[0] )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_CustoPrevisto");
                  GXUtil.WriteLogRaw("Old: ",Z2150Projeto_CustoPrevisto);
                  GXUtil.WriteLogRaw("Current: ",T00295_A2150Projeto_CustoPrevisto[0]);
               }
               if ( Z2170Projeto_AnexoSequecial != T00295_A2170Projeto_AnexoSequecial[0] )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_AnexoSequecial");
                  GXUtil.WriteLogRaw("Old: ",Z2170Projeto_AnexoSequecial);
                  GXUtil.WriteLogRaw("Current: ",T00295_A2170Projeto_AnexoSequecial[0]);
               }
               if ( Z983Projeto_TipoProjetoCod != T00295_A983Projeto_TipoProjetoCod[0] )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_TipoProjetoCod");
                  GXUtil.WriteLogRaw("Old: ",Z983Projeto_TipoProjetoCod);
                  GXUtil.WriteLogRaw("Current: ",T00295_A983Projeto_TipoProjetoCod[0]);
               }
               if ( Z2151Projeto_AreaTrabalhoCodigo != T00295_A2151Projeto_AreaTrabalhoCodigo[0] )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_AreaTrabalhoCodigo");
                  GXUtil.WriteLogRaw("Old: ",Z2151Projeto_AreaTrabalhoCodigo);
                  GXUtil.WriteLogRaw("Current: ",T00295_A2151Projeto_AreaTrabalhoCodigo[0]);
               }
               if ( Z2153Projeto_GpoObjCtrlCodigo != T00295_A2153Projeto_GpoObjCtrlCodigo[0] )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_GpoObjCtrlCodigo");
                  GXUtil.WriteLogRaw("Old: ",Z2153Projeto_GpoObjCtrlCodigo);
                  GXUtil.WriteLogRaw("Current: ",T00295_A2153Projeto_GpoObjCtrlCodigo[0]);
               }
               if ( Z2155Projeto_SistemaCodigo != T00295_A2155Projeto_SistemaCodigo[0] )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_SistemaCodigo");
                  GXUtil.WriteLogRaw("Old: ",Z2155Projeto_SistemaCodigo);
                  GXUtil.WriteLogRaw("Current: ",T00295_A2155Projeto_SistemaCodigo[0]);
               }
               if ( Z2158Projeto_ModuloCodigo != T00295_A2158Projeto_ModuloCodigo[0] )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"Projeto_ModuloCodigo");
                  GXUtil.WriteLogRaw("Old: ",Z2158Projeto_ModuloCodigo);
                  GXUtil.WriteLogRaw("Current: ",T00295_A2158Projeto_ModuloCodigo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Projeto"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2986( )
      {
         BeforeValidate2986( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2986( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2986( 0) ;
            CheckOptimisticConcurrency2986( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2986( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2986( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002926 */
                     pr_default.execute(21, new Object[] {A649Projeto_Nome, A650Projeto_Sigla, A652Projeto_TecnicaContagem, n1540Projeto_Introducao, A1540Projeto_Introducao, A653Projeto_Escopo, n1541Projeto_Previsao, A1541Projeto_Previsao, n1542Projeto_GerenteCod, A1542Projeto_GerenteCod, n1543Projeto_ServicoCod, A1543Projeto_ServicoCod, A658Projeto_Status, n985Projeto_FatorEscala, A985Projeto_FatorEscala, n989Projeto_FatorMultiplicador, A989Projeto_FatorMultiplicador, n990Projeto_ConstACocomo, A990Projeto_ConstACocomo, n1232Projeto_Incremental, A1232Projeto_Incremental, n2144Projeto_Identificador, A2144Projeto_Identificador, n2145Projeto_Objetivo, A2145Projeto_Objetivo, n2146Projeto_DTInicio, A2146Projeto_DTInicio, n2147Projeto_DTFim, A2147Projeto_DTFim, n2148Projeto_PrazoPrevisto, A2148Projeto_PrazoPrevisto, n2149Projeto_EsforcoPrevisto, A2149Projeto_EsforcoPrevisto, n2150Projeto_CustoPrevisto, A2150Projeto_CustoPrevisto, n2170Projeto_AnexoSequecial, A2170Projeto_AnexoSequecial, n983Projeto_TipoProjetoCod, A983Projeto_TipoProjetoCod, n2151Projeto_AreaTrabalhoCodigo, A2151Projeto_AreaTrabalhoCodigo, n2153Projeto_GpoObjCtrlCodigo, A2153Projeto_GpoObjCtrlCodigo, n2155Projeto_SistemaCodigo, A2155Projeto_SistemaCodigo, n2158Projeto_ModuloCodigo, A2158Projeto_ModuloCodigo});
                     A648Projeto_Codigo = T002926_A648Projeto_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
                     pr_default.close(21);
                     dsDefault.SmartCacheProvider.SetUpdated("Projeto") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel2986( ) ;
                           if ( AnyError == 0 )
                           {
                              /* Save values for previous() function. */
                              GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                              ResetCaption290( ) ;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2986( ) ;
            }
            EndLevel2986( ) ;
         }
         CloseExtendedTableCursors2986( ) ;
      }

      protected void Update2986( )
      {
         BeforeValidate2986( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2986( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2986( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2986( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2986( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002927 */
                     pr_default.execute(22, new Object[] {A649Projeto_Nome, A650Projeto_Sigla, A652Projeto_TecnicaContagem, n1540Projeto_Introducao, A1540Projeto_Introducao, A653Projeto_Escopo, n1541Projeto_Previsao, A1541Projeto_Previsao, n1542Projeto_GerenteCod, A1542Projeto_GerenteCod, n1543Projeto_ServicoCod, A1543Projeto_ServicoCod, A658Projeto_Status, n985Projeto_FatorEscala, A985Projeto_FatorEscala, n989Projeto_FatorMultiplicador, A989Projeto_FatorMultiplicador, n990Projeto_ConstACocomo, A990Projeto_ConstACocomo, n1232Projeto_Incremental, A1232Projeto_Incremental, n2144Projeto_Identificador, A2144Projeto_Identificador, n2145Projeto_Objetivo, A2145Projeto_Objetivo, n2146Projeto_DTInicio, A2146Projeto_DTInicio, n2147Projeto_DTFim, A2147Projeto_DTFim, n2148Projeto_PrazoPrevisto, A2148Projeto_PrazoPrevisto, n2149Projeto_EsforcoPrevisto, A2149Projeto_EsforcoPrevisto, n2150Projeto_CustoPrevisto, A2150Projeto_CustoPrevisto, n2170Projeto_AnexoSequecial, A2170Projeto_AnexoSequecial, n983Projeto_TipoProjetoCod, A983Projeto_TipoProjetoCod, n2151Projeto_AreaTrabalhoCodigo, A2151Projeto_AreaTrabalhoCodigo, n2153Projeto_GpoObjCtrlCodigo, A2153Projeto_GpoObjCtrlCodigo, n2155Projeto_SistemaCodigo, A2155Projeto_SistemaCodigo, n2158Projeto_ModuloCodigo, A2158Projeto_ModuloCodigo, A648Projeto_Codigo});
                     pr_default.close(22);
                     dsDefault.SmartCacheProvider.SetUpdated("Projeto") ;
                     if ( (pr_default.getStatus(22) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Projeto"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2986( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel2986( ) ;
                           if ( AnyError == 0 )
                           {
                              if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                              {
                                 if ( AnyError == 0 )
                                 {
                                    context.nUserReturn = 1;
                                 }
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2986( ) ;
         }
         CloseExtendedTableCursors2986( ) ;
      }

      protected void DeferredUpdate2986( )
      {
      }

      protected void delete( )
      {
         BeforeValidate2986( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2986( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2986( ) ;
            AfterConfirm2986( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2986( ) ;
               if ( AnyError == 0 )
               {
                  A2170Projeto_AnexoSequecial = O2170Projeto_AnexoSequecial;
                  n2170Projeto_AnexoSequecial = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2170Projeto_AnexoSequecial", StringUtil.LTrim( StringUtil.Str( (decimal)(A2170Projeto_AnexoSequecial), 4, 0)));
                  ScanStart29235( ) ;
                  while ( RcdFound235 != 0 )
                  {
                     getByPrimaryKey29235( ) ;
                     Delete29235( ) ;
                     ScanNext29235( ) ;
                     O2170Projeto_AnexoSequecial = A2170Projeto_AnexoSequecial;
                     n2170Projeto_AnexoSequecial = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2170Projeto_AnexoSequecial", StringUtil.LTrim( StringUtil.Str( (decimal)(A2170Projeto_AnexoSequecial), 4, 0)));
                  }
                  ScanEnd29235( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002928 */
                     pr_default.execute(23, new Object[] {A648Projeto_Codigo});
                     pr_default.close(23);
                     dsDefault.SmartCacheProvider.SetUpdated("Projeto") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( delete) rules */
                        /* End of After( delete) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
         sMode86 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel2986( ) ;
         Gx_mode = sMode86;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls2986( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            GXt_int1 = A740Projeto_SistemaCod;
            new prc_projeto_sistemacod(context ).execute( ref  A648Projeto_Codigo, ref  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
            A740Projeto_SistemaCod = GXt_int1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A740Projeto_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A740Projeto_SistemaCod), 6, 0)));
            /* Using cursor T002929 */
            pr_default.execute(24, new Object[] {n2153Projeto_GpoObjCtrlCodigo, A2153Projeto_GpoObjCtrlCodigo});
            A2154Projeto_GpoObjCtrlNome = T002929_A2154Projeto_GpoObjCtrlNome[0];
            n2154Projeto_GpoObjCtrlNome = T002929_n2154Projeto_GpoObjCtrlNome[0];
            pr_default.close(24);
            GXAPROJETO_SISTEMACODIGO_html2986( AV8WWPContext, A2153Projeto_GpoObjCtrlCodigo) ;
            /* Using cursor T002930 */
            pr_default.execute(25, new Object[] {n2155Projeto_SistemaCodigo, A2155Projeto_SistemaCodigo});
            A2156Projeto_SistemaNome = T002930_A2156Projeto_SistemaNome[0];
            n2156Projeto_SistemaNome = T002930_n2156Projeto_SistemaNome[0];
            A2157Projeto_SistemaSigla = T002930_A2157Projeto_SistemaSigla[0];
            n2157Projeto_SistemaSigla = T002930_n2157Projeto_SistemaSigla[0];
            pr_default.close(25);
            /* Using cursor T002932 */
            pr_default.execute(26, new Object[] {n2155Projeto_SistemaCodigo, A2155Projeto_SistemaCodigo});
            if ( (pr_default.getStatus(26) != 101) )
            {
               A655Projeto_Custo = T002932_A655Projeto_Custo[0];
               A656Projeto_Prazo = T002932_A656Projeto_Prazo[0];
               A657Projeto_Esforco = T002932_A657Projeto_Esforco[0];
               n657Projeto_Esforco = T002932_n657Projeto_Esforco[0];
            }
            else
            {
               A657Projeto_Esforco = 0;
               n657Projeto_Esforco = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A657Projeto_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A657Projeto_Esforco), 4, 0)));
               A656Projeto_Prazo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A656Projeto_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A656Projeto_Prazo), 4, 0)));
               A655Projeto_Custo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A655Projeto_Custo", StringUtil.LTrim( StringUtil.Str( A655Projeto_Custo, 12, 2)));
            }
            pr_default.close(26);
            /* Using cursor T002933 */
            pr_default.execute(27, new Object[] {n2158Projeto_ModuloCodigo, A2158Projeto_ModuloCodigo});
            A2159Projeto_ModuloNome = T002933_A2159Projeto_ModuloNome[0];
            n2159Projeto_ModuloNome = T002933_n2159Projeto_ModuloNome[0];
            A2160Projeto_ModuloSigla = T002933_A2160Projeto_ModuloSigla[0];
            n2160Projeto_ModuloSigla = T002933_n2160Projeto_ModuloSigla[0];
            pr_default.close(27);
            /* Using cursor T002934 */
            pr_default.execute(28, new Object[] {n2151Projeto_AreaTrabalhoCodigo, A2151Projeto_AreaTrabalhoCodigo});
            A2152Projeto_AreaTrabalhoDescricao = T002934_A2152Projeto_AreaTrabalhoDescricao[0];
            n2152Projeto_AreaTrabalhoDescricao = T002934_n2152Projeto_AreaTrabalhoDescricao[0];
            pr_default.close(28);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T002935 */
            pr_default.execute(29, new Object[] {A648Projeto_Codigo});
            if ( (pr_default.getStatus(29) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Vari�vel Cocomo"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(29);
            /* Using cursor T002936 */
            pr_default.execute(30, new Object[] {A648Projeto_Codigo});
            if ( (pr_default.getStatus(30) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(30);
            /* Using cursor T002937 */
            pr_default.execute(31, new Object[] {A648Projeto_Codigo});
            if ( (pr_default.getStatus(31) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Projeto Melhoria"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(31);
            /* Using cursor T002938 */
            pr_default.execute(32, new Object[] {A648Projeto_Codigo});
            if ( (pr_default.getStatus(32) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Proposta"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(32);
         }
      }

      protected void ProcessNestedLevel29235( )
      {
         s2170Projeto_AnexoSequecial = O2170Projeto_AnexoSequecial;
         n2170Projeto_AnexoSequecial = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2170Projeto_AnexoSequecial", StringUtil.LTrim( StringUtil.Str( (decimal)(A2170Projeto_AnexoSequecial), 4, 0)));
         nGXsfl_154_idx = 0;
         while ( nGXsfl_154_idx < nRC_GXsfl_154 )
         {
            ReadRow29235( ) ;
            if ( ( nRcdExists_235 != 0 ) || ( nIsMod_235 != 0 ) )
            {
               standaloneNotModal29235( ) ;
               GetKey29235( ) ;
               if ( ( nRcdExists_235 == 0 ) && ( nRcdDeleted_235 == 0 ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  Insert29235( ) ;
               }
               else
               {
                  if ( RcdFound235 != 0 )
                  {
                     if ( ( nRcdDeleted_235 != 0 ) && ( nRcdExists_235 != 0 ) )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                        Delete29235( ) ;
                     }
                     else
                     {
                        if ( ( nIsMod_235 != 0 ) && ( nRcdExists_235 != 0 ) )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                           Update29235( ) ;
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_235 == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
               O2170Projeto_AnexoSequecial = A2170Projeto_AnexoSequecial;
               n2170Projeto_AnexoSequecial = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2170Projeto_AnexoSequecial", StringUtil.LTrim( StringUtil.Str( (decimal)(A2170Projeto_AnexoSequecial), 4, 0)));
            }
            ChangePostValue( edtProjetoAnexos_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2163ProjetoAnexos_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( edtProjetoAnexos_Arquivo_Internalname, A2165ProjetoAnexos_Arquivo) ;
            ChangePostValue( edtProjetoAnexos_ArquivoNome_Internalname, StringUtil.RTrim( A2166ProjetoAnexos_ArquivoNome)) ;
            ChangePostValue( edtTipoDocumento_Nome_Internalname, StringUtil.RTrim( A646TipoDocumento_Nome)) ;
            ChangePostValue( dynTipoDocumento_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A645TipoDocumento_Codigo), 6, 0, ".", ""))) ;
            ChangePostValue( edtProjetoAnexos_ArquivoTipo_Internalname, StringUtil.RTrim( A2167ProjetoAnexos_ArquivoTipo)) ;
            ChangePostValue( edtProjetoAnexos_Data_Internalname, context.localUtil.TToC( A2169ProjetoAnexos_Data, 10, 8, 0, 3, "/", ":", " ")) ;
            ChangePostValue( "ZT_"+"Z2163ProjetoAnexos_Codigo_"+sGXsfl_154_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2163ProjetoAnexos_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z2169ProjetoAnexos_Data_"+sGXsfl_154_idx, context.localUtil.TToC( Z2169ProjetoAnexos_Data, 10, 8, 0, 0, "/", ":", " ")) ;
            ChangePostValue( "ZT_"+"Z645TipoDocumento_Codigo_"+sGXsfl_154_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z645TipoDocumento_Codigo), 6, 0, ",", ""))) ;
            ChangePostValue( "nRcdDeleted_235_"+sGXsfl_154_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_235), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_235_"+sGXsfl_154_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_235), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_235_"+sGXsfl_154_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_235), 4, 0, ",", ""))) ;
            if ( nIsMod_235 != 0 )
            {
               ChangePostValue( "PROJETOANEXOS_CODIGO_"+sGXsfl_154_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProjetoAnexos_Codigo_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "PROJETOANEXOS_ARQUIVO_"+sGXsfl_154_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProjetoAnexos_Arquivo_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "PROJETOANEXOS_ARQUIVONOME_"+sGXsfl_154_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProjetoAnexos_ArquivoNome_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "TIPODOCUMENTO_NOME_"+sGXsfl_154_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTipoDocumento_Nome_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "TIPODOCUMENTO_CODIGO_"+sGXsfl_154_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(dynTipoDocumento_Codigo.Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "PROJETOANEXOS_ARQUIVOTIPO_"+sGXsfl_154_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProjetoAnexos_ArquivoTipo_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "PROJETOANEXOS_DATA_"+sGXsfl_154_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProjetoAnexos_Data_Enabled), 5, 0, ".", ""))) ;
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
         InitAll29235( ) ;
         if ( AnyError != 0 )
         {
            O2170Projeto_AnexoSequecial = s2170Projeto_AnexoSequecial;
            n2170Projeto_AnexoSequecial = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2170Projeto_AnexoSequecial", StringUtil.LTrim( StringUtil.Str( (decimal)(A2170Projeto_AnexoSequecial), 4, 0)));
         }
         nRcdExists_235 = 0;
         nIsMod_235 = 0;
         nRcdDeleted_235 = 0;
      }

      protected void ProcessLevel2986( )
      {
         /* Save parent mode. */
         sMode86 = Gx_mode;
         ProcessNestedLevel29235( ) ;
         if ( AnyError != 0 )
         {
            O2170Projeto_AnexoSequecial = s2170Projeto_AnexoSequecial;
            n2170Projeto_AnexoSequecial = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2170Projeto_AnexoSequecial", StringUtil.LTrim( StringUtil.Str( (decimal)(A2170Projeto_AnexoSequecial), 4, 0)));
         }
         /* Restore parent mode. */
         Gx_mode = sMode86;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         /* ' Update level parameters */
         /* Using cursor T002939 */
         pr_default.execute(33, new Object[] {n2170Projeto_AnexoSequecial, A2170Projeto_AnexoSequecial, A648Projeto_Codigo});
         pr_default.close(33);
         dsDefault.SmartCacheProvider.SetUpdated("Projeto") ;
      }

      protected void EndLevel2986( )
      {
         pr_default.close(3);
         if ( AnyError == 0 )
         {
            BeforeComplete2986( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(4);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(28);
            pr_default.close(24);
            pr_default.close(25);
            pr_default.close(27);
            pr_default.close(26);
            pr_default.close(2);
            context.CommitDataStores( "Projeto");
            if ( AnyError == 0 )
            {
               ConfirmValues290( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(4);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(28);
            pr_default.close(24);
            pr_default.close(25);
            pr_default.close(27);
            pr_default.close(26);
            pr_default.close(2);
            context.RollbackDataStores( "Projeto");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2986( )
      {
         /* Scan By routine */
         /* Using cursor T002940 */
         pr_default.execute(34);
         RcdFound86 = 0;
         if ( (pr_default.getStatus(34) != 101) )
         {
            RcdFound86 = 1;
            A648Projeto_Codigo = T002940_A648Projeto_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2986( )
      {
         /* Scan next routine */
         pr_default.readNext(34);
         RcdFound86 = 0;
         if ( (pr_default.getStatus(34) != 101) )
         {
            RcdFound86 = 1;
            A648Projeto_Codigo = T002940_A648Projeto_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd2986( )
      {
         pr_default.close(34);
      }

      protected void AfterConfirm2986( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2986( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2986( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2986( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2986( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2986( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2986( )
      {
         edtProjeto_Sigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_Sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_Sigla_Enabled), 5, 0)));
         edtProjeto_Identificador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_Identificador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_Identificador_Enabled), 5, 0)));
         edtProjeto_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_Nome_Enabled), 5, 0)));
         dynProjeto_TipoProjetoCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynProjeto_TipoProjetoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynProjeto_TipoProjetoCod.Enabled), 5, 0)));
         edtProjeto_Objetivo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_Objetivo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_Objetivo_Enabled), 5, 0)));
         edtProjeto_Escopo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_Escopo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_Escopo_Enabled), 5, 0)));
         cmbProjeto_Status.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbProjeto_Status_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbProjeto_Status.Enabled), 5, 0)));
         dynProjeto_GpoObjCtrlCodigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynProjeto_GpoObjCtrlCodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynProjeto_GpoObjCtrlCodigo.Enabled), 5, 0)));
         dynProjeto_SistemaCodigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynProjeto_SistemaCodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynProjeto_SistemaCodigo.Enabled), 5, 0)));
         dynProjeto_ModuloCodigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynProjeto_ModuloCodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynProjeto_ModuloCodigo.Enabled), 5, 0)));
         edtProjeto_DTInicio_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_DTInicio_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_DTInicio_Enabled), 5, 0)));
         edtProjeto_DTFim_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_DTFim_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_DTFim_Enabled), 5, 0)));
         edtProjeto_PrazoPrevisto_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_PrazoPrevisto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_PrazoPrevisto_Enabled), 5, 0)));
         edtProjeto_EsforcoPrevisto_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_EsforcoPrevisto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_EsforcoPrevisto_Enabled), 5, 0)));
         edtProjeto_CustoPrevisto_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_CustoPrevisto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_CustoPrevisto_Enabled), 5, 0)));
         edtProjeto_FatorEscala_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_FatorEscala_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_FatorEscala_Enabled), 5, 0)));
         edtProjeto_FatorMultiplicador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_FatorMultiplicador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_FatorMultiplicador_Enabled), 5, 0)));
         edtProjeto_ConstACocomo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_ConstACocomo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_ConstACocomo_Enabled), 5, 0)));
         chkProjeto_Incremental.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkProjeto_Incremental_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkProjeto_Incremental.Enabled), 5, 0)));
         edtProjeto_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjeto_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjeto_Codigo_Enabled), 5, 0)));
      }

      protected void ZM29235( short GX_JID )
      {
         if ( ( GX_JID == 45 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z2169ProjetoAnexos_Data = T00293_A2169ProjetoAnexos_Data[0];
               Z645TipoDocumento_Codigo = T00293_A645TipoDocumento_Codigo[0];
            }
            else
            {
               Z2169ProjetoAnexos_Data = A2169ProjetoAnexos_Data;
               Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            }
         }
         if ( GX_JID == -45 )
         {
            Z648Projeto_Codigo = A648Projeto_Codigo;
            Z2163ProjetoAnexos_Codigo = A2163ProjetoAnexos_Codigo;
            Z2164ProjetoAnexos_Descricao = A2164ProjetoAnexos_Descricao;
            Z2165ProjetoAnexos_Arquivo = A2165ProjetoAnexos_Arquivo;
            Z2168ProjetoAnexos_Link = A2168ProjetoAnexos_Link;
            Z2169ProjetoAnexos_Data = A2169ProjetoAnexos_Data;
            Z2167ProjetoAnexos_ArquivoTipo = A2167ProjetoAnexos_ArquivoTipo;
            Z2166ProjetoAnexos_ArquivoNome = A2166ProjetoAnexos_ArquivoNome;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
         }
      }

      protected void standaloneNotModal29235( )
      {
         GXATIPODOCUMENTO_CODIGO_html29235( ) ;
         edtProjetoAnexos_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoAnexos_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoAnexos_Data_Enabled), 5, 0)));
         edtProjetoAnexos_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoAnexos_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoAnexos_Codigo_Enabled), 5, 0)));
         edtProjetoAnexos_ArquivoNome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoAnexos_ArquivoNome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoAnexos_ArquivoNome_Enabled), 5, 0)));
         edtTipoDocumento_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoDocumento_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoDocumento_Nome_Enabled), 5, 0)));
         edtProjetoAnexos_ArquivoTipo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoAnexos_ArquivoTipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoAnexos_ArquivoTipo_Enabled), 5, 0)));
      }

      protected void standaloneModal29235( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A2170Projeto_AnexoSequecial = (short)(O2170Projeto_AnexoSequecial+1);
            n2170Projeto_AnexoSequecial = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2170Projeto_AnexoSequecial", StringUtil.LTrim( StringUtil.Str( (decimal)(A2170Projeto_AnexoSequecial), 4, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && ( Gx_BScreen == 1 ) )
         {
            A2163ProjetoAnexos_Codigo = A2170Projeto_AnexoSequecial;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T00294 */
            pr_default.execute(2, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
            A646TipoDocumento_Nome = T00294_A646TipoDocumento_Nome[0];
            pr_default.close(2);
         }
      }

      protected void Load29235( )
      {
         /* Using cursor T002941 */
         pr_default.execute(35, new Object[] {A648Projeto_Codigo, A2163ProjetoAnexos_Codigo});
         if ( (pr_default.getStatus(35) != 101) )
         {
            RcdFound235 = 1;
            A2164ProjetoAnexos_Descricao = T002941_A2164ProjetoAnexos_Descricao[0];
            A2168ProjetoAnexos_Link = T002941_A2168ProjetoAnexos_Link[0];
            n2168ProjetoAnexos_Link = T002941_n2168ProjetoAnexos_Link[0];
            A2169ProjetoAnexos_Data = T002941_A2169ProjetoAnexos_Data[0];
            n2169ProjetoAnexos_Data = T002941_n2169ProjetoAnexos_Data[0];
            A646TipoDocumento_Nome = T002941_A646TipoDocumento_Nome[0];
            A2167ProjetoAnexos_ArquivoTipo = T002941_A2167ProjetoAnexos_ArquivoTipo[0];
            n2167ProjetoAnexos_ArquivoTipo = T002941_n2167ProjetoAnexos_ArquivoTipo[0];
            edtProjetoAnexos_Arquivo_Filetype = A2167ProjetoAnexos_ArquivoTipo;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoAnexos_Arquivo_Internalname, "Filetype", edtProjetoAnexos_Arquivo_Filetype);
            A2166ProjetoAnexos_ArquivoNome = T002941_A2166ProjetoAnexos_ArquivoNome[0];
            n2166ProjetoAnexos_ArquivoNome = T002941_n2166ProjetoAnexos_ArquivoNome[0];
            edtProjetoAnexos_Arquivo_Filename = A2166ProjetoAnexos_ArquivoNome;
            A645TipoDocumento_Codigo = T002941_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = T002941_n645TipoDocumento_Codigo[0];
            A2165ProjetoAnexos_Arquivo = T002941_A2165ProjetoAnexos_Arquivo[0];
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoAnexos_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A2165ProjetoAnexos_Arquivo));
            n2165ProjetoAnexos_Arquivo = T002941_n2165ProjetoAnexos_Arquivo[0];
            ZM29235( -45) ;
         }
         pr_default.close(35);
         OnLoadActions29235( ) ;
      }

      protected void OnLoadActions29235( )
      {
      }

      protected void CheckExtendedTable29235( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal29235( ) ;
         /* Using cursor T00294 */
         pr_default.execute(2, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A645TipoDocumento_Codigo) ) )
            {
               GXCCtl = "TIPODOCUMENTO_CODIGO_" + sGXsfl_154_idx;
               GX_msglist.addItem("N�o existe 'Tipo de Documentos'.", "ForeignKeyNotFound", 1, GXCCtl);
               AnyError = 1;
               GX_FocusControl = dynTipoDocumento_Codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A646TipoDocumento_Nome = T00294_A646TipoDocumento_Nome[0];
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors29235( )
      {
         pr_default.close(2);
      }

      protected void enableDisable29235( )
      {
      }

      protected void gxLoad_46( int A645TipoDocumento_Codigo )
      {
         /* Using cursor T002942 */
         pr_default.execute(36, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
         if ( (pr_default.getStatus(36) == 101) )
         {
            if ( ! ( (0==A645TipoDocumento_Codigo) ) )
            {
               GXCCtl = "TIPODOCUMENTO_CODIGO_" + sGXsfl_154_idx;
               GX_msglist.addItem("N�o existe 'Tipo de Documentos'.", "ForeignKeyNotFound", 1, GXCCtl);
               AnyError = 1;
               GX_FocusControl = dynTipoDocumento_Codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A646TipoDocumento_Nome = T002942_A646TipoDocumento_Nome[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A646TipoDocumento_Nome))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(36) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(36);
      }

      protected void GetKey29235( )
      {
         /* Using cursor T002943 */
         pr_default.execute(37, new Object[] {A648Projeto_Codigo, A2163ProjetoAnexos_Codigo});
         if ( (pr_default.getStatus(37) != 101) )
         {
            RcdFound235 = 1;
         }
         else
         {
            RcdFound235 = 0;
         }
         pr_default.close(37);
      }

      protected void getByPrimaryKey29235( )
      {
         /* Using cursor T00293 */
         pr_default.execute(1, new Object[] {A648Projeto_Codigo, A2163ProjetoAnexos_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM29235( 45) ;
            RcdFound235 = 1;
            InitializeNonKey29235( ) ;
            A2163ProjetoAnexos_Codigo = T00293_A2163ProjetoAnexos_Codigo[0];
            A2164ProjetoAnexos_Descricao = T00293_A2164ProjetoAnexos_Descricao[0];
            A2168ProjetoAnexos_Link = T00293_A2168ProjetoAnexos_Link[0];
            n2168ProjetoAnexos_Link = T00293_n2168ProjetoAnexos_Link[0];
            A2169ProjetoAnexos_Data = T00293_A2169ProjetoAnexos_Data[0];
            n2169ProjetoAnexos_Data = T00293_n2169ProjetoAnexos_Data[0];
            A2167ProjetoAnexos_ArquivoTipo = T00293_A2167ProjetoAnexos_ArquivoTipo[0];
            n2167ProjetoAnexos_ArquivoTipo = T00293_n2167ProjetoAnexos_ArquivoTipo[0];
            edtProjetoAnexos_Arquivo_Filetype = A2167ProjetoAnexos_ArquivoTipo;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoAnexos_Arquivo_Internalname, "Filetype", edtProjetoAnexos_Arquivo_Filetype);
            A2166ProjetoAnexos_ArquivoNome = T00293_A2166ProjetoAnexos_ArquivoNome[0];
            n2166ProjetoAnexos_ArquivoNome = T00293_n2166ProjetoAnexos_ArquivoNome[0];
            edtProjetoAnexos_Arquivo_Filename = A2166ProjetoAnexos_ArquivoNome;
            A645TipoDocumento_Codigo = T00293_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = T00293_n645TipoDocumento_Codigo[0];
            A2165ProjetoAnexos_Arquivo = T00293_A2165ProjetoAnexos_Arquivo[0];
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoAnexos_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A2165ProjetoAnexos_Arquivo));
            n2165ProjetoAnexos_Arquivo = T00293_n2165ProjetoAnexos_Arquivo[0];
            Z648Projeto_Codigo = A648Projeto_Codigo;
            Z2163ProjetoAnexos_Codigo = A2163ProjetoAnexos_Codigo;
            sMode235 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load29235( ) ;
            Gx_mode = sMode235;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound235 = 0;
            InitializeNonKey29235( ) ;
            sMode235 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal29235( ) ;
            Gx_mode = sMode235;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            DisableAttributes29235( ) ;
         }
         pr_default.close(1);
      }

      protected void CheckOptimisticConcurrency29235( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00292 */
            pr_default.execute(0, new Object[] {A648Projeto_Codigo, A2163ProjetoAnexos_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ProjetoAnexos"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z2169ProjetoAnexos_Data != T00292_A2169ProjetoAnexos_Data[0] ) || ( Z645TipoDocumento_Codigo != T00292_A645TipoDocumento_Codigo[0] ) )
            {
               if ( Z2169ProjetoAnexos_Data != T00292_A2169ProjetoAnexos_Data[0] )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"ProjetoAnexos_Data");
                  GXUtil.WriteLogRaw("Old: ",Z2169ProjetoAnexos_Data);
                  GXUtil.WriteLogRaw("Current: ",T00292_A2169ProjetoAnexos_Data[0]);
               }
               if ( Z645TipoDocumento_Codigo != T00292_A645TipoDocumento_Codigo[0] )
               {
                  GXUtil.WriteLog("projeto:[seudo value changed for attri]"+"TipoDocumento_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z645TipoDocumento_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T00292_A645TipoDocumento_Codigo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ProjetoAnexos"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert29235( )
      {
         BeforeValidate29235( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable29235( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM29235( 0) ;
            CheckOptimisticConcurrency29235( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm29235( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert29235( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002944 */
                     A2167ProjetoAnexos_ArquivoTipo = edtProjetoAnexos_Arquivo_Filetype;
                     n2167ProjetoAnexos_ArquivoTipo = false;
                     A2166ProjetoAnexos_ArquivoNome = edtProjetoAnexos_Arquivo_Filename;
                     n2166ProjetoAnexos_ArquivoNome = false;
                     pr_default.execute(38, new Object[] {A648Projeto_Codigo, A2163ProjetoAnexos_Codigo, A2164ProjetoAnexos_Descricao, n2165ProjetoAnexos_Arquivo, A2165ProjetoAnexos_Arquivo, n2168ProjetoAnexos_Link, A2168ProjetoAnexos_Link, n2169ProjetoAnexos_Data, A2169ProjetoAnexos_Data, n2167ProjetoAnexos_ArquivoTipo, A2167ProjetoAnexos_ArquivoTipo, n2166ProjetoAnexos_ArquivoNome, A2166ProjetoAnexos_ArquivoNome, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
                     pr_default.close(38);
                     dsDefault.SmartCacheProvider.SetUpdated("ProjetoAnexos") ;
                     if ( (pr_default.getStatus(38) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load29235( ) ;
            }
            EndLevel29235( ) ;
         }
         CloseExtendedTableCursors29235( ) ;
      }

      protected void Update29235( )
      {
         BeforeValidate29235( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable29235( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency29235( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm29235( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate29235( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002945 */
                     A2167ProjetoAnexos_ArquivoTipo = edtProjetoAnexos_Arquivo_Filetype;
                     n2167ProjetoAnexos_ArquivoTipo = false;
                     A2166ProjetoAnexos_ArquivoNome = edtProjetoAnexos_Arquivo_Filename;
                     n2166ProjetoAnexos_ArquivoNome = false;
                     pr_default.execute(39, new Object[] {A2164ProjetoAnexos_Descricao, n2168ProjetoAnexos_Link, A2168ProjetoAnexos_Link, n2169ProjetoAnexos_Data, A2169ProjetoAnexos_Data, n2167ProjetoAnexos_ArquivoTipo, A2167ProjetoAnexos_ArquivoTipo, n2166ProjetoAnexos_ArquivoNome, A2166ProjetoAnexos_ArquivoNome, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo, A648Projeto_Codigo, A2163ProjetoAnexos_Codigo});
                     pr_default.close(39);
                     dsDefault.SmartCacheProvider.SetUpdated("ProjetoAnexos") ;
                     if ( (pr_default.getStatus(39) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ProjetoAnexos"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate29235( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey29235( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel29235( ) ;
         }
         CloseExtendedTableCursors29235( ) ;
      }

      protected void DeferredUpdate29235( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor T002946 */
            pr_default.execute(40, new Object[] {n2165ProjetoAnexos_Arquivo, A2165ProjetoAnexos_Arquivo, A648Projeto_Codigo, A2163ProjetoAnexos_Codigo});
            pr_default.close(40);
            dsDefault.SmartCacheProvider.SetUpdated("ProjetoAnexos") ;
         }
      }

      protected void Delete29235( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         BeforeValidate29235( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency29235( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls29235( ) ;
            AfterConfirm29235( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete29235( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002947 */
                  pr_default.execute(41, new Object[] {A648Projeto_Codigo, A2163ProjetoAnexos_Codigo});
                  pr_default.close(41);
                  dsDefault.SmartCacheProvider.SetUpdated("ProjetoAnexos") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode235 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel29235( ) ;
         Gx_mode = sMode235;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls29235( )
      {
         standaloneModal29235( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T002948 */
            pr_default.execute(42, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
            A646TipoDocumento_Nome = T002948_A646TipoDocumento_Nome[0];
            pr_default.close(42);
         }
      }

      protected void EndLevel29235( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart29235( )
      {
         /* Scan By routine */
         /* Using cursor T002949 */
         pr_default.execute(43, new Object[] {A648Projeto_Codigo});
         RcdFound235 = 0;
         if ( (pr_default.getStatus(43) != 101) )
         {
            RcdFound235 = 1;
            A2163ProjetoAnexos_Codigo = T002949_A2163ProjetoAnexos_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext29235( )
      {
         /* Scan next routine */
         pr_default.readNext(43);
         RcdFound235 = 0;
         if ( (pr_default.getStatus(43) != 101) )
         {
            RcdFound235 = 1;
            A2163ProjetoAnexos_Codigo = T002949_A2163ProjetoAnexos_Codigo[0];
         }
      }

      protected void ScanEnd29235( )
      {
         pr_default.close(43);
      }

      protected void AfterConfirm29235( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert29235( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate29235( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete29235( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete29235( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate29235( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes29235( )
      {
         edtProjetoAnexos_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoAnexos_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoAnexos_Codigo_Enabled), 5, 0)));
         edtProjetoAnexos_Arquivo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoAnexos_Arquivo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoAnexos_Arquivo_Enabled), 5, 0)));
         edtProjetoAnexos_ArquivoNome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoAnexos_ArquivoNome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoAnexos_ArquivoNome_Enabled), 5, 0)));
         edtTipoDocumento_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoDocumento_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoDocumento_Nome_Enabled), 5, 0)));
         dynTipoDocumento_Codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTipoDocumento_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynTipoDocumento_Codigo.Enabled), 5, 0)));
         edtProjetoAnexos_ArquivoTipo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoAnexos_ArquivoTipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoAnexos_ArquivoTipo_Enabled), 5, 0)));
         edtProjetoAnexos_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoAnexos_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoAnexos_Data_Enabled), 5, 0)));
      }

      protected void SubsflControlProps_154235( )
      {
         edtProjetoAnexos_Codigo_Internalname = "PROJETOANEXOS_CODIGO_"+sGXsfl_154_idx;
         edtProjetoAnexos_Arquivo_Internalname = "PROJETOANEXOS_ARQUIVO_"+sGXsfl_154_idx;
         edtProjetoAnexos_ArquivoNome_Internalname = "PROJETOANEXOS_ARQUIVONOME_"+sGXsfl_154_idx;
         edtTipoDocumento_Nome_Internalname = "TIPODOCUMENTO_NOME_"+sGXsfl_154_idx;
         dynTipoDocumento_Codigo_Internalname = "TIPODOCUMENTO_CODIGO_"+sGXsfl_154_idx;
         edtProjetoAnexos_ArquivoTipo_Internalname = "PROJETOANEXOS_ARQUIVOTIPO_"+sGXsfl_154_idx;
         edtProjetoAnexos_Data_Internalname = "PROJETOANEXOS_DATA_"+sGXsfl_154_idx;
      }

      protected void SubsflControlProps_fel_154235( )
      {
         edtProjetoAnexos_Codigo_Internalname = "PROJETOANEXOS_CODIGO_"+sGXsfl_154_fel_idx;
         edtProjetoAnexos_Arquivo_Internalname = "PROJETOANEXOS_ARQUIVO_"+sGXsfl_154_fel_idx;
         edtProjetoAnexos_ArquivoNome_Internalname = "PROJETOANEXOS_ARQUIVONOME_"+sGXsfl_154_fel_idx;
         edtTipoDocumento_Nome_Internalname = "TIPODOCUMENTO_NOME_"+sGXsfl_154_fel_idx;
         dynTipoDocumento_Codigo_Internalname = "TIPODOCUMENTO_CODIGO_"+sGXsfl_154_fel_idx;
         edtProjetoAnexos_ArquivoTipo_Internalname = "PROJETOANEXOS_ARQUIVOTIPO_"+sGXsfl_154_fel_idx;
         edtProjetoAnexos_Data_Internalname = "PROJETOANEXOS_DATA_"+sGXsfl_154_fel_idx;
      }

      protected void AddRow29235( )
      {
         nGXsfl_154_idx = (short)(nGXsfl_154_idx+1);
         sGXsfl_154_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_154_idx), 4, 0)), 4, "0");
         SubsflControlProps_154235( ) ;
         SendRow29235( ) ;
      }

      protected void SendRow29235( )
      {
         GridanexosRow = GXWebRow.GetNew(context);
         if ( subGridanexos_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGridanexos_Backstyle = 0;
            if ( StringUtil.StrCmp(subGridanexos_Class, "") != 0 )
            {
               subGridanexos_Linesclass = subGridanexos_Class+"Odd";
            }
         }
         else if ( subGridanexos_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGridanexos_Backstyle = 0;
            subGridanexos_Backcolor = subGridanexos_Allbackcolor;
            if ( StringUtil.StrCmp(subGridanexos_Class, "") != 0 )
            {
               subGridanexos_Linesclass = subGridanexos_Class+"Uniform";
            }
         }
         else if ( subGridanexos_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGridanexos_Backstyle = 1;
            if ( StringUtil.StrCmp(subGridanexos_Class, "") != 0 )
            {
               subGridanexos_Linesclass = subGridanexos_Class+"Odd";
            }
            subGridanexos_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subGridanexos_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGridanexos_Backstyle = 1;
            if ( ((int)((nGXsfl_154_idx) % (2))) == 0 )
            {
               subGridanexos_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGridanexos_Class, "") != 0 )
               {
                  subGridanexos_Linesclass = subGridanexos_Class+"Even";
               }
            }
            else
            {
               subGridanexos_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGridanexos_Class, "") != 0 )
               {
                  subGridanexos_Linesclass = subGridanexos_Class+"Odd";
               }
            }
         }
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridanexosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjetoAnexos_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A2163ProjetoAnexos_Codigo), 6, 0, ",", "")),((edtProjetoAnexos_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A2163ProjetoAnexos_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A2163ProjetoAnexos_Codigo), "ZZZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProjetoAnexos_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtProjetoAnexos_Codigo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)154,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
         /* Subfile cell */
         ClassString = "BootstrapAttribute";
         StyleString = "";
         TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_235_" + sGXsfl_154_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 156,'',false,'" + sGXsfl_154_idx + "',154)\"";
         edtProjetoAnexos_Arquivo_Filename = A2166ProjetoAnexos_ArquivoNome;
         edtProjetoAnexos_Arquivo_Filetype = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoAnexos_Arquivo_Internalname, "Filetype", edtProjetoAnexos_Arquivo_Filetype);
         edtProjetoAnexos_Arquivo_Filetype = A2167ProjetoAnexos_ArquivoTipo;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoAnexos_Arquivo_Internalname, "Filetype", edtProjetoAnexos_Arquivo_Filetype);
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A2165ProjetoAnexos_Arquivo)) )
         {
            gxblobfileaux.Source = A2165ProjetoAnexos_Arquivo;
            if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtProjetoAnexos_Arquivo_Filetype, "tmp") != 0 ) )
            {
               gxblobfileaux.SetExtension(StringUtil.Trim( edtProjetoAnexos_Arquivo_Filetype));
            }
            if ( gxblobfileaux.ErrCode == 0 )
            {
               A2165ProjetoAnexos_Arquivo = gxblobfileaux.GetAbsoluteName();
               n2165ProjetoAnexos_Arquivo = false;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoAnexos_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A2165ProjetoAnexos_Arquivo));
               edtProjetoAnexos_Arquivo_Filetype = gxblobfileaux.GetExtension();
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoAnexos_Arquivo_Internalname, "Filetype", edtProjetoAnexos_Arquivo_Filetype);
            }
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoAnexos_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A2165ProjetoAnexos_Arquivo));
         }
         GridanexosRow.AddColumnProperties("blob", 2, isAjaxCallMode( ), new Object[] {(String)edtProjetoAnexos_Arquivo_Internalname,StringUtil.RTrim( A2165ProjetoAnexos_Arquivo),context.PathToRelativeUrl( A2165ProjetoAnexos_Arquivo),(String.IsNullOrEmpty(StringUtil.RTrim( edtProjetoAnexos_Arquivo_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtProjetoAnexos_Arquivo_Filetype)) ? A2165ProjetoAnexos_Arquivo : edtProjetoAnexos_Arquivo_Filetype)) : edtProjetoAnexos_Arquivo_Contenttype),(bool)true,(String)"",(String)edtProjetoAnexos_Arquivo_Parameters,(short)0,(int)edtProjetoAnexos_Arquivo_Enabled,(short)-1,(String)"",(String)"",(short)0,(short)-1,(short)0,(String)"px",(short)60,(String)"px",(short)0,(short)0,(short)0,(String)edtProjetoAnexos_Arquivo_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)StyleString,(String)ClassString,(String)"",""+TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,156);\"",(String)"",(String)""});
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridanexosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjetoAnexos_ArquivoNome_Internalname,StringUtil.RTrim( A2166ProjetoAnexos_ArquivoNome),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProjetoAnexos_ArquivoNome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtProjetoAnexos_ArquivoNome_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)154,(short)1,(short)-1,(short)-1,(bool)true,(String)"NomeArq",(String)"left",(bool)true});
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridanexosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTipoDocumento_Nome_Internalname,StringUtil.RTrim( A646TipoDocumento_Nome),StringUtil.RTrim( context.localUtil.Format( A646TipoDocumento_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTipoDocumento_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtTipoDocumento_Nome_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)154,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
         GXATIPODOCUMENTO_CODIGO_html29235( ) ;
         /* Subfile cell */
         TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_235_" + sGXsfl_154_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 159,'',false,'" + sGXsfl_154_idx + "',154)\"";
         if ( ( nGXsfl_154_idx == 1 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "TIPODOCUMENTO_CODIGO_" + sGXsfl_154_idx;
            dynTipoDocumento_Codigo.Name = GXCCtl;
            dynTipoDocumento_Codigo.WebTags = "";
         }
         /* ComboBox */
         GridanexosRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)dynTipoDocumento_Codigo,(String)dynTipoDocumento_Codigo_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)),(short)1,(String)dynTipoDocumento_Codigo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,dynTipoDocumento_Codigo.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,159);\"",(String)"",(bool)true});
         dynTipoDocumento_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTipoDocumento_Codigo_Internalname, "Values", (String)(dynTipoDocumento_Codigo.ToJavascriptSource()));
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridanexosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjetoAnexos_ArquivoTipo_Internalname,StringUtil.RTrim( A2167ProjetoAnexos_ArquivoTipo),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProjetoAnexos_ArquivoTipo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtProjetoAnexos_ArquivoTipo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)154,(short)1,(short)-1,(short)-1,(bool)true,(String)"TipoArq",(String)"left",(bool)true});
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "BootstrapAttributeDateTime";
         GridanexosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjetoAnexos_Data_Internalname,context.localUtil.TToC( A2169ProjetoAnexos_Data, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A2169ProjetoAnexos_Data, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProjetoAnexos_Data_Jsonclick,(short)0,(String)"BootstrapAttributeDateTime",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtProjetoAnexos_Data_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)154,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         context.httpAjaxContext.ajax_sending_grid_row(GridanexosRow);
         GXCCtl = "Z2163ProjetoAnexos_Codigo_" + sGXsfl_154_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2163ProjetoAnexos_Codigo), 6, 0, ",", "")));
         GXCCtl = "Z2169ProjetoAnexos_Data_" + sGXsfl_154_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, context.localUtil.TToC( Z2169ProjetoAnexos_Data, 10, 8, 0, 0, "/", ":", " "));
         GXCCtl = "Z645TipoDocumento_Codigo_" + sGXsfl_154_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z645TipoDocumento_Codigo), 6, 0, ",", "")));
         GXCCtl = "nRcdDeleted_235_" + sGXsfl_154_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_235), 4, 0, ",", "")));
         GXCCtl = "nRcdExists_235_" + sGXsfl_154_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_235), 4, 0, ",", "")));
         GXCCtl = "nIsMod_235_" + sGXsfl_154_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_235), 4, 0, ",", "")));
         GXCCtl = "vMODE_" + sGXsfl_154_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.RTrim( Gx_mode));
         GXCCtl = "vTRNCONTEXT_" + sGXsfl_154_idx;
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, GXCCtl, AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(GXCCtl, AV9TrnContext);
         }
         GXCCtl = "vWWPCONTEXT_" + sGXsfl_154_idx;
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, GXCCtl, AV8WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(GXCCtl, AV8WWPContext);
         }
         GXCCtl = "vPROJETO_CODIGO_" + sGXsfl_154_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Projeto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_PROJETOANEXOS_DATA"+"_"+sGXsfl_154_idx, GetSecureSignedToken( sGXsfl_154_idx, context.localUtil.Format( A2169ProjetoAnexos_Data, "99/99/99 99:99")));
         GXCCtl = "PROJETOANEXOS_ARQUIVO_" + sGXsfl_154_idx;
         GXCCtlgxBlob = GXCCtl + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, A2165ProjetoAnexos_Arquivo);
         GXCCtl = "PROJETOANEXOS_ARQUIVO_Filetype_" + sGXsfl_154_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.RTrim( edtProjetoAnexos_Arquivo_Filetype));
         GXCCtl = "PROJETOANEXOS_ARQUIVO_Filename_" + sGXsfl_154_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.RTrim( edtProjetoAnexos_Arquivo_Filename));
         GXCCtl = "PROJETOANEXOS_ARQUIVO_Filename_" + sGXsfl_154_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.RTrim( edtProjetoAnexos_Arquivo_Filename));
         GXCCtl = "PROJETOANEXOS_ARQUIVO_Filetype_" + sGXsfl_154_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.RTrim( edtProjetoAnexos_Arquivo_Filetype));
         GxWebStd.gx_hidden_field( context, "PROJETOANEXOS_CODIGO_"+sGXsfl_154_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProjetoAnexos_Codigo_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "PROJETOANEXOS_ARQUIVO_"+sGXsfl_154_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProjetoAnexos_Arquivo_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "PROJETOANEXOS_ARQUIVONOME_"+sGXsfl_154_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProjetoAnexos_ArquivoNome_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "TIPODOCUMENTO_NOME_"+sGXsfl_154_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTipoDocumento_Nome_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "TIPODOCUMENTO_CODIGO_"+sGXsfl_154_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(dynTipoDocumento_Codigo.Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "PROJETOANEXOS_ARQUIVOTIPO_"+sGXsfl_154_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProjetoAnexos_ArquivoTipo_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "PROJETOANEXOS_DATA_"+sGXsfl_154_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProjetoAnexos_Data_Enabled), 5, 0, ".", "")));
         context.httpAjaxContext.ajax_sending_grid_row(null);
         GridanexosContainer.AddRow(GridanexosRow);
      }

      protected void ReadRow29235( )
      {
         nGXsfl_154_idx = (short)(nGXsfl_154_idx+1);
         sGXsfl_154_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_154_idx), 4, 0)), 4, "0");
         SubsflControlProps_154235( ) ;
         edtProjetoAnexos_Codigo_Enabled = (int)(context.localUtil.CToN( cgiGet( "PROJETOANEXOS_CODIGO_"+sGXsfl_154_idx+"Enabled"), ",", "."));
         edtProjetoAnexos_Arquivo_Enabled = (int)(context.localUtil.CToN( cgiGet( "PROJETOANEXOS_ARQUIVO_"+sGXsfl_154_idx+"Enabled"), ",", "."));
         edtProjetoAnexos_ArquivoNome_Enabled = (int)(context.localUtil.CToN( cgiGet( "PROJETOANEXOS_ARQUIVONOME_"+sGXsfl_154_idx+"Enabled"), ",", "."));
         edtTipoDocumento_Nome_Enabled = (int)(context.localUtil.CToN( cgiGet( "TIPODOCUMENTO_NOME_"+sGXsfl_154_idx+"Enabled"), ",", "."));
         dynTipoDocumento_Codigo.Enabled = (int)(context.localUtil.CToN( cgiGet( "TIPODOCUMENTO_CODIGO_"+sGXsfl_154_idx+"Enabled"), ",", "."));
         edtProjetoAnexos_ArquivoTipo_Enabled = (int)(context.localUtil.CToN( cgiGet( "PROJETOANEXOS_ARQUIVOTIPO_"+sGXsfl_154_idx+"Enabled"), ",", "."));
         edtProjetoAnexos_Data_Enabled = (int)(context.localUtil.CToN( cgiGet( "PROJETOANEXOS_DATA_"+sGXsfl_154_idx+"Enabled"), ",", "."));
         GXCCtl = "PROJETOANEXOS_ARQUIVO_Filetype_" + sGXsfl_154_idx;
         edtProjetoAnexos_Arquivo_Filetype = cgiGet( GXCCtl);
         GXCCtl = "PROJETOANEXOS_ARQUIVO_Filename_" + sGXsfl_154_idx;
         edtProjetoAnexos_Arquivo_Filename = cgiGet( GXCCtl);
         GXCCtl = "PROJETOANEXOS_ARQUIVO_Filename_" + sGXsfl_154_idx;
         edtProjetoAnexos_Arquivo_Filename = cgiGet( GXCCtl);
         GXCCtl = "PROJETOANEXOS_ARQUIVO_Filetype_" + sGXsfl_154_idx;
         edtProjetoAnexos_Arquivo_Filetype = cgiGet( GXCCtl);
         A2163ProjetoAnexos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtProjetoAnexos_Codigo_Internalname), ",", "."));
         A2165ProjetoAnexos_Arquivo = cgiGet( edtProjetoAnexos_Arquivo_Internalname);
         n2165ProjetoAnexos_Arquivo = false;
         n2165ProjetoAnexos_Arquivo = (String.IsNullOrEmpty(StringUtil.RTrim( A2165ProjetoAnexos_Arquivo)) ? true : false);
         A646TipoDocumento_Nome = StringUtil.Upper( cgiGet( edtTipoDocumento_Nome_Internalname));
         dynTipoDocumento_Codigo.Name = dynTipoDocumento_Codigo_Internalname;
         dynTipoDocumento_Codigo.CurrentValue = cgiGet( dynTipoDocumento_Codigo_Internalname);
         A645TipoDocumento_Codigo = (int)(NumberUtil.Val( cgiGet( dynTipoDocumento_Codigo_Internalname), "."));
         n645TipoDocumento_Codigo = false;
         n645TipoDocumento_Codigo = ((0==A645TipoDocumento_Codigo) ? true : false);
         A2169ProjetoAnexos_Data = context.localUtil.CToT( cgiGet( edtProjetoAnexos_Data_Internalname));
         n2169ProjetoAnexos_Data = false;
         GXCCtl = "Z2163ProjetoAnexos_Codigo_" + sGXsfl_154_idx;
         Z2163ProjetoAnexos_Codigo = (int)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "Z2169ProjetoAnexos_Data_" + sGXsfl_154_idx;
         Z2169ProjetoAnexos_Data = context.localUtil.CToT( cgiGet( GXCCtl), 0);
         n2169ProjetoAnexos_Data = ((DateTime.MinValue==A2169ProjetoAnexos_Data) ? true : false);
         GXCCtl = "Z645TipoDocumento_Codigo_" + sGXsfl_154_idx;
         Z645TipoDocumento_Codigo = (int)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         n645TipoDocumento_Codigo = ((0==A645TipoDocumento_Codigo) ? true : false);
         GXCCtl = "nRcdDeleted_235_" + sGXsfl_154_idx;
         nRcdDeleted_235 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nRcdExists_235_" + sGXsfl_154_idx;
         nRcdExists_235 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nIsMod_235_" + sGXsfl_154_idx;
         nIsMod_235 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         if ( ( nRcdDeleted_235 == 0 ) && ( nIsMod_235 == 1 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( A2165ProjetoAnexos_Arquivo)) )
         {
            edtProjetoAnexos_Arquivo_Filename = (String)(CGIGetFileName(edtProjetoAnexos_Arquivo_Internalname));
            edtProjetoAnexos_Arquivo_Filetype = (String)(CGIGetFileType(edtProjetoAnexos_Arquivo_Internalname));
         }
         A2167ProjetoAnexos_ArquivoTipo = edtProjetoAnexos_Arquivo_Filetype;
         n2167ProjetoAnexos_ArquivoTipo = false;
         A2166ProjetoAnexos_ArquivoNome = edtProjetoAnexos_Arquivo_Filename;
         n2166ProjetoAnexos_ArquivoNome = false;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A2165ProjetoAnexos_Arquivo)) )
         {
            GXCCtl = "PROJETOANEXOS_ARQUIVO_" + sGXsfl_154_idx;
            GXCCtlgxBlob = GXCCtl + "_gxBlob";
            A2165ProjetoAnexos_Arquivo = cgiGet( GXCCtlgxBlob);
            n2165ProjetoAnexos_Arquivo = false;
         }
      }

      protected void assign_properties_default( )
      {
         defedtProjetoAnexos_Data_Enabled = edtProjetoAnexos_Data_Enabled;
         defedtProjetoAnexos_ArquivoTipo_Enabled = edtProjetoAnexos_ArquivoTipo_Enabled;
         defedtTipoDocumento_Nome_Enabled = edtTipoDocumento_Nome_Enabled;
         defedtProjetoAnexos_ArquivoNome_Enabled = edtProjetoAnexos_ArquivoNome_Enabled;
         defedtProjetoAnexos_Codigo_Enabled = edtProjetoAnexos_Codigo_Enabled;
      }

      protected void ConfirmValues290( )
      {
         nGXsfl_154_idx = 0;
         sGXsfl_154_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_154_idx), 4, 0)), 4, "0");
         SubsflControlProps_154235( ) ;
         while ( nGXsfl_154_idx < nRC_GXsfl_154 )
         {
            nGXsfl_154_idx = (short)(nGXsfl_154_idx+1);
            sGXsfl_154_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_154_idx), 4, 0)), 4, "0");
            SubsflControlProps_154235( ) ;
            ChangePostValue( "Z2163ProjetoAnexos_Codigo_"+sGXsfl_154_idx, cgiGet( "ZT_"+"Z2163ProjetoAnexos_Codigo_"+sGXsfl_154_idx)) ;
            DeletePostValue( "ZT_"+"Z2163ProjetoAnexos_Codigo_"+sGXsfl_154_idx) ;
            ChangePostValue( "Z2169ProjetoAnexos_Data_"+sGXsfl_154_idx, cgiGet( "ZT_"+"Z2169ProjetoAnexos_Data_"+sGXsfl_154_idx)) ;
            DeletePostValue( "ZT_"+"Z2169ProjetoAnexos_Data_"+sGXsfl_154_idx) ;
            ChangePostValue( "Z645TipoDocumento_Codigo_"+sGXsfl_154_idx, cgiGet( "ZT_"+"Z645TipoDocumento_Codigo_"+sGXsfl_154_idx)) ;
            DeletePostValue( "ZT_"+"Z645TipoDocumento_Codigo_"+sGXsfl_154_idx) ;
         }
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299302190");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/TabsPanel/BootstrapTabsPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("projeto.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Projeto_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z648Projeto_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z648Projeto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z649Projeto_Nome", StringUtil.RTrim( Z649Projeto_Nome));
         GxWebStd.gx_hidden_field( context, "Z650Projeto_Sigla", StringUtil.RTrim( Z650Projeto_Sigla));
         GxWebStd.gx_hidden_field( context, "Z652Projeto_TecnicaContagem", StringUtil.RTrim( Z652Projeto_TecnicaContagem));
         GxWebStd.gx_hidden_field( context, "Z1541Projeto_Previsao", context.localUtil.DToC( Z1541Projeto_Previsao, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z1542Projeto_GerenteCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1542Projeto_GerenteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1543Projeto_ServicoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1543Projeto_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z658Projeto_Status", StringUtil.RTrim( Z658Projeto_Status));
         GxWebStd.gx_hidden_field( context, "Z985Projeto_FatorEscala", StringUtil.LTrim( StringUtil.NToC( Z985Projeto_FatorEscala, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z989Projeto_FatorMultiplicador", StringUtil.LTrim( StringUtil.NToC( Z989Projeto_FatorMultiplicador, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z990Projeto_ConstACocomo", StringUtil.LTrim( StringUtil.NToC( Z990Projeto_ConstACocomo, 6, 2, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z1232Projeto_Incremental", Z1232Projeto_Incremental);
         GxWebStd.gx_hidden_field( context, "Z2144Projeto_Identificador", Z2144Projeto_Identificador);
         GxWebStd.gx_hidden_field( context, "Z2146Projeto_DTInicio", context.localUtil.DToC( Z2146Projeto_DTInicio, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z2147Projeto_DTFim", context.localUtil.DToC( Z2147Projeto_DTFim, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z2148Projeto_PrazoPrevisto", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2148Projeto_PrazoPrevisto), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2149Projeto_EsforcoPrevisto", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2149Projeto_EsforcoPrevisto), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2150Projeto_CustoPrevisto", StringUtil.LTrim( StringUtil.NToC( Z2150Projeto_CustoPrevisto, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2170Projeto_AnexoSequecial", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2170Projeto_AnexoSequecial), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z983Projeto_TipoProjetoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z983Projeto_TipoProjetoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2151Projeto_AreaTrabalhoCodigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2151Projeto_AreaTrabalhoCodigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2153Projeto_GpoObjCtrlCodigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2153Projeto_GpoObjCtrlCodigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2155Projeto_SistemaCodigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2155Projeto_SistemaCodigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2158Projeto_ModuloCodigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2158Projeto_ModuloCodigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "O2170Projeto_AnexoSequecial", StringUtil.LTrim( StringUtil.NToC( (decimal)(O2170Projeto_AnexoSequecial), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_154", StringUtil.LTrim( StringUtil.NToC( (decimal)(nGXsfl_154_idx), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N983Projeto_TipoProjetoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A983Projeto_TipoProjetoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N2151Projeto_AreaTrabalhoCodigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2151Projeto_AreaTrabalhoCodigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N2153Projeto_GpoObjCtrlCodigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2153Projeto_GpoObjCtrlCodigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N2155Projeto_SistemaCodigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2155Projeto_SistemaCodigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N2158Projeto_ModuloCodigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2158Projeto_ModuloCodigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "PROJETO_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A740Projeto_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPROJETO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Projeto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_PROJETO_TIPOPROJETOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Insert_Projeto_TipoProjetoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_PROJETO_AREATRABALHOCODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19Insert_Projeto_AreaTrabalhoCodigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PROJETO_AREATRABALHOCODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2151Projeto_AreaTrabalhoCodigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_PROJETO_GPOOBJCTRLCODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20Insert_Projeto_GpoObjCtrlCodigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_PROJETO_SISTEMACODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21Insert_Projeto_SistemaCodigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_PROJETO_MODULOCODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22Insert_Projeto_ModuloCodigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV8WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV8WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "PROJETO_TECNICACONTAGEM", StringUtil.RTrim( A652Projeto_TecnicaContagem));
         GxWebStd.gx_hidden_field( context, "PROJETO_INTRODUCAO", A1540Projeto_Introducao);
         GxWebStd.gx_hidden_field( context, "PROJETO_PREVISAO", context.localUtil.DToC( A1541Projeto_Previsao, 0, "/"));
         GxWebStd.gx_hidden_field( context, "PROJETO_GERENTECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1542Projeto_GerenteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PROJETO_SERVICOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1543Projeto_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PROJETO_ANEXOSEQUECIAL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2170Projeto_AnexoSequecial), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PROJETO_AREATRABALHODESCRICAO", A2152Projeto_AreaTrabalhoDescricao);
         GxWebStd.gx_hidden_field( context, "PROJETO_GPOOBJCTRLNOME", StringUtil.RTrim( A2154Projeto_GpoObjCtrlNome));
         GxWebStd.gx_hidden_field( context, "PROJETO_SISTEMANOME", A2156Projeto_SistemaNome);
         GxWebStd.gx_hidden_field( context, "PROJETO_SISTEMASIGLA", StringUtil.RTrim( A2157Projeto_SistemaSigla));
         GxWebStd.gx_hidden_field( context, "PROJETO_MODULONOME", StringUtil.RTrim( A2159Projeto_ModuloNome));
         GxWebStd.gx_hidden_field( context, "PROJETO_MODULOSIGLA", StringUtil.RTrim( A2160Projeto_ModuloSigla));
         GxWebStd.gx_hidden_field( context, "PROJETO_CUSTO", StringUtil.LTrim( StringUtil.NToC( A655Projeto_Custo, 12, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "PROJETO_PRAZO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A656Projeto_Prazo), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PROJETO_ESFORCO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A657Projeto_Esforco), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV24Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "PROJETOANEXOS_DESCRICAO", A2164ProjetoAnexos_Descricao);
         GxWebStd.gx_hidden_field( context, "PROJETOANEXOS_LINK", A2168ProjetoAnexos_Link);
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABMAIN_Width", StringUtil.RTrim( Gxuitabspanel_tabmain_Width));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABMAIN_Cls", StringUtil.RTrim( Gxuitabspanel_tabmain_Cls));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABMAIN_Enabled", StringUtil.BoolToStr( Gxuitabspanel_tabmain_Enabled));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABMAIN_Autowidth", StringUtil.BoolToStr( Gxuitabspanel_tabmain_Autowidth));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABMAIN_Autoheight", StringUtil.BoolToStr( Gxuitabspanel_tabmain_Autoheight));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABMAIN_Autoscroll", StringUtil.BoolToStr( Gxuitabspanel_tabmain_Autoscroll));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABMAIN_Designtimetabs", StringUtil.RTrim( Gxuitabspanel_tabmain_Designtimetabs));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Projeto";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A658Projeto_Status, ""));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A648Projeto_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2151Projeto_AreaTrabalhoCodigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A652Projeto_TecnicaContagem, ""));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format(A1541Projeto_Previsao, "99/99/99");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1542Projeto_GerenteCod), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1543Projeto_ServicoCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("projeto:[SendSecurityCheck value for]"+"Projeto_Status:"+StringUtil.RTrim( context.localUtil.Format( A658Projeto_Status, "")));
         GXUtil.WriteLog("projeto:[SendSecurityCheck value for]"+"Projeto_Codigo:"+context.localUtil.Format( (decimal)(A648Projeto_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("projeto:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("projeto:[SendSecurityCheck value for]"+"Projeto_AreaTrabalhoCodigo:"+context.localUtil.Format( (decimal)(A2151Projeto_AreaTrabalhoCodigo), "ZZZZZ9"));
         GXUtil.WriteLog("projeto:[SendSecurityCheck value for]"+"Projeto_TecnicaContagem:"+StringUtil.RTrim( context.localUtil.Format( A652Projeto_TecnicaContagem, "")));
         GXUtil.WriteLog("projeto:[SendSecurityCheck value for]"+"Projeto_Previsao:"+context.localUtil.Format(A1541Projeto_Previsao, "99/99/99"));
         GXUtil.WriteLog("projeto:[SendSecurityCheck value for]"+"Projeto_GerenteCod:"+context.localUtil.Format( (decimal)(A1542Projeto_GerenteCod), "ZZZZZ9"));
         GXUtil.WriteLog("projeto:[SendSecurityCheck value for]"+"Projeto_ServicoCod:"+context.localUtil.Format( (decimal)(A1543Projeto_ServicoCod), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("projeto.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Projeto_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Projeto" ;
      }

      public override String GetPgmdesc( )
      {
         return "Projeto" ;
      }

      protected void InitializeNonKey2986( )
      {
         A983Projeto_TipoProjetoCod = 0;
         n983Projeto_TipoProjetoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A983Projeto_TipoProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A983Projeto_TipoProjetoCod), 6, 0)));
         n983Projeto_TipoProjetoCod = ((0==A983Projeto_TipoProjetoCod) ? true : false);
         A2153Projeto_GpoObjCtrlCodigo = 0;
         n2153Projeto_GpoObjCtrlCodigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2153Projeto_GpoObjCtrlCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2153Projeto_GpoObjCtrlCodigo), 6, 0)));
         n2153Projeto_GpoObjCtrlCodigo = ((0==A2153Projeto_GpoObjCtrlCodigo) ? true : false);
         A2155Projeto_SistemaCodigo = 0;
         n2155Projeto_SistemaCodigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2155Projeto_SistemaCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2155Projeto_SistemaCodigo), 6, 0)));
         n2155Projeto_SistemaCodigo = ((0==A2155Projeto_SistemaCodigo) ? true : false);
         A2158Projeto_ModuloCodigo = 0;
         n2158Projeto_ModuloCodigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2158Projeto_ModuloCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2158Projeto_ModuloCodigo), 6, 0)));
         n2158Projeto_ModuloCodigo = ((0==A2158Projeto_ModuloCodigo) ? true : false);
         A740Projeto_SistemaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A740Projeto_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A740Projeto_SistemaCod), 6, 0)));
         A649Projeto_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A649Projeto_Nome", A649Projeto_Nome);
         A650Projeto_Sigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A650Projeto_Sigla", A650Projeto_Sigla);
         A652Projeto_TecnicaContagem = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A652Projeto_TecnicaContagem", A652Projeto_TecnicaContagem);
         A1540Projeto_Introducao = "";
         n1540Projeto_Introducao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1540Projeto_Introducao", A1540Projeto_Introducao);
         A653Projeto_Escopo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A653Projeto_Escopo", A653Projeto_Escopo);
         A1541Projeto_Previsao = DateTime.MinValue;
         n1541Projeto_Previsao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1541Projeto_Previsao", context.localUtil.Format(A1541Projeto_Previsao, "99/99/99"));
         A1542Projeto_GerenteCod = 0;
         n1542Projeto_GerenteCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1542Projeto_GerenteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1542Projeto_GerenteCod), 6, 0)));
         A1543Projeto_ServicoCod = 0;
         n1543Projeto_ServicoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1543Projeto_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1543Projeto_ServicoCod), 6, 0)));
         A655Projeto_Custo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A655Projeto_Custo", StringUtil.LTrim( StringUtil.Str( A655Projeto_Custo, 12, 2)));
         A656Projeto_Prazo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A656Projeto_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A656Projeto_Prazo), 4, 0)));
         A657Projeto_Esforco = 0;
         n657Projeto_Esforco = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A657Projeto_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A657Projeto_Esforco), 4, 0)));
         A985Projeto_FatorEscala = 0;
         n985Projeto_FatorEscala = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A985Projeto_FatorEscala", StringUtil.LTrim( StringUtil.Str( A985Projeto_FatorEscala, 6, 2)));
         n985Projeto_FatorEscala = ((Convert.ToDecimal(0)==A985Projeto_FatorEscala) ? true : false);
         A989Projeto_FatorMultiplicador = 0;
         n989Projeto_FatorMultiplicador = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A989Projeto_FatorMultiplicador", StringUtil.LTrim( StringUtil.Str( A989Projeto_FatorMultiplicador, 6, 2)));
         n989Projeto_FatorMultiplicador = ((Convert.ToDecimal(0)==A989Projeto_FatorMultiplicador) ? true : false);
         A990Projeto_ConstACocomo = 0;
         n990Projeto_ConstACocomo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A990Projeto_ConstACocomo", StringUtil.LTrim( StringUtil.Str( A990Projeto_ConstACocomo, 6, 2)));
         n990Projeto_ConstACocomo = ((Convert.ToDecimal(0)==A990Projeto_ConstACocomo) ? true : false);
         A1232Projeto_Incremental = false;
         n1232Projeto_Incremental = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1232Projeto_Incremental", A1232Projeto_Incremental);
         n1232Projeto_Incremental = ((false==A1232Projeto_Incremental) ? true : false);
         A2144Projeto_Identificador = "";
         n2144Projeto_Identificador = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2144Projeto_Identificador", A2144Projeto_Identificador);
         n2144Projeto_Identificador = (String.IsNullOrEmpty(StringUtil.RTrim( A2144Projeto_Identificador)) ? true : false);
         A2145Projeto_Objetivo = "";
         n2145Projeto_Objetivo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2145Projeto_Objetivo", A2145Projeto_Objetivo);
         n2145Projeto_Objetivo = (String.IsNullOrEmpty(StringUtil.RTrim( A2145Projeto_Objetivo)) ? true : false);
         A2146Projeto_DTInicio = DateTime.MinValue;
         n2146Projeto_DTInicio = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2146Projeto_DTInicio", context.localUtil.Format(A2146Projeto_DTInicio, "99/99/99"));
         n2146Projeto_DTInicio = ((DateTime.MinValue==A2146Projeto_DTInicio) ? true : false);
         A2147Projeto_DTFim = DateTime.MinValue;
         n2147Projeto_DTFim = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2147Projeto_DTFim", context.localUtil.Format(A2147Projeto_DTFim, "99/99/99"));
         n2147Projeto_DTFim = ((DateTime.MinValue==A2147Projeto_DTFim) ? true : false);
         A2148Projeto_PrazoPrevisto = 0;
         n2148Projeto_PrazoPrevisto = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2148Projeto_PrazoPrevisto", StringUtil.LTrim( StringUtil.Str( (decimal)(A2148Projeto_PrazoPrevisto), 4, 0)));
         n2148Projeto_PrazoPrevisto = ((0==A2148Projeto_PrazoPrevisto) ? true : false);
         A2149Projeto_EsforcoPrevisto = 0;
         n2149Projeto_EsforcoPrevisto = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2149Projeto_EsforcoPrevisto", StringUtil.LTrim( StringUtil.Str( (decimal)(A2149Projeto_EsforcoPrevisto), 4, 0)));
         n2149Projeto_EsforcoPrevisto = ((0==A2149Projeto_EsforcoPrevisto) ? true : false);
         A2150Projeto_CustoPrevisto = 0;
         n2150Projeto_CustoPrevisto = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2150Projeto_CustoPrevisto", StringUtil.LTrim( StringUtil.Str( A2150Projeto_CustoPrevisto, 12, 2)));
         n2150Projeto_CustoPrevisto = ((Convert.ToDecimal(0)==A2150Projeto_CustoPrevisto) ? true : false);
         A2152Projeto_AreaTrabalhoDescricao = "";
         n2152Projeto_AreaTrabalhoDescricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2152Projeto_AreaTrabalhoDescricao", A2152Projeto_AreaTrabalhoDescricao);
         A2154Projeto_GpoObjCtrlNome = "";
         n2154Projeto_GpoObjCtrlNome = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2154Projeto_GpoObjCtrlNome", A2154Projeto_GpoObjCtrlNome);
         A2156Projeto_SistemaNome = "";
         n2156Projeto_SistemaNome = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2156Projeto_SistemaNome", A2156Projeto_SistemaNome);
         A2157Projeto_SistemaSigla = "";
         n2157Projeto_SistemaSigla = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2157Projeto_SistemaSigla", A2157Projeto_SistemaSigla);
         A2159Projeto_ModuloNome = "";
         n2159Projeto_ModuloNome = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2159Projeto_ModuloNome", A2159Projeto_ModuloNome);
         A2160Projeto_ModuloSigla = "";
         n2160Projeto_ModuloSigla = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2160Projeto_ModuloSigla", A2160Projeto_ModuloSigla);
         A2170Projeto_AnexoSequecial = 0;
         n2170Projeto_AnexoSequecial = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2170Projeto_AnexoSequecial", StringUtil.LTrim( StringUtil.Str( (decimal)(A2170Projeto_AnexoSequecial), 4, 0)));
         A2151Projeto_AreaTrabalhoCodigo = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         n2151Projeto_AreaTrabalhoCodigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2151Projeto_AreaTrabalhoCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2151Projeto_AreaTrabalhoCodigo), 6, 0)));
         A658Projeto_Status = "A";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A658Projeto_Status", A658Projeto_Status);
         O2170Projeto_AnexoSequecial = A2170Projeto_AnexoSequecial;
         n2170Projeto_AnexoSequecial = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2170Projeto_AnexoSequecial", StringUtil.LTrim( StringUtil.Str( (decimal)(A2170Projeto_AnexoSequecial), 4, 0)));
         Z649Projeto_Nome = "";
         Z650Projeto_Sigla = "";
         Z652Projeto_TecnicaContagem = "";
         Z1541Projeto_Previsao = DateTime.MinValue;
         Z1542Projeto_GerenteCod = 0;
         Z1543Projeto_ServicoCod = 0;
         Z658Projeto_Status = "";
         Z985Projeto_FatorEscala = 0;
         Z989Projeto_FatorMultiplicador = 0;
         Z990Projeto_ConstACocomo = 0;
         Z1232Projeto_Incremental = false;
         Z2144Projeto_Identificador = "";
         Z2146Projeto_DTInicio = DateTime.MinValue;
         Z2147Projeto_DTFim = DateTime.MinValue;
         Z2148Projeto_PrazoPrevisto = 0;
         Z2149Projeto_EsforcoPrevisto = 0;
         Z2150Projeto_CustoPrevisto = 0;
         Z2170Projeto_AnexoSequecial = 0;
         Z983Projeto_TipoProjetoCod = 0;
         Z2151Projeto_AreaTrabalhoCodigo = 0;
         Z2153Projeto_GpoObjCtrlCodigo = 0;
         Z2155Projeto_SistemaCodigo = 0;
         Z2158Projeto_ModuloCodigo = 0;
      }

      protected void InitAll2986( )
      {
         A648Projeto_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
         InitializeNonKey2986( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A2151Projeto_AreaTrabalhoCodigo = i2151Projeto_AreaTrabalhoCodigo;
         n2151Projeto_AreaTrabalhoCodigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2151Projeto_AreaTrabalhoCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2151Projeto_AreaTrabalhoCodigo), 6, 0)));
         A658Projeto_Status = i658Projeto_Status;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A658Projeto_Status", A658Projeto_Status);
      }

      protected void InitializeNonKey29235( )
      {
         A2164ProjetoAnexos_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2164ProjetoAnexos_Descricao", A2164ProjetoAnexos_Descricao);
         A2165ProjetoAnexos_Arquivo = "";
         n2165ProjetoAnexos_Arquivo = false;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoAnexos_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A2165ProjetoAnexos_Arquivo));
         A2168ProjetoAnexos_Link = "";
         n2168ProjetoAnexos_Link = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2168ProjetoAnexos_Link", A2168ProjetoAnexos_Link);
         A2169ProjetoAnexos_Data = (DateTime)(DateTime.MinValue);
         n2169ProjetoAnexos_Data = false;
         A645TipoDocumento_Codigo = 0;
         n645TipoDocumento_Codigo = false;
         A646TipoDocumento_Nome = "";
         A2167ProjetoAnexos_ArquivoTipo = "";
         n2167ProjetoAnexos_ArquivoTipo = false;
         A2166ProjetoAnexos_ArquivoNome = "";
         n2166ProjetoAnexos_ArquivoNome = false;
         Z2169ProjetoAnexos_Data = (DateTime)(DateTime.MinValue);
         Z645TipoDocumento_Codigo = 0;
      }

      protected void InitAll29235( )
      {
         A2163ProjetoAnexos_Codigo = 0;
         InitializeNonKey29235( ) ;
      }

      protected void StandaloneModalInsert29235( )
      {
         A2170Projeto_AnexoSequecial = i2170Projeto_AnexoSequecial;
         n2170Projeto_AnexoSequecial = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2170Projeto_AnexoSequecial", StringUtil.LTrim( StringUtil.Str( (decimal)(A2170Projeto_AnexoSequecial), 4, 0)));
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299302267");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("projeto.js", "?20205299302268");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/TabsPanel/BootstrapTabsPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_level_properties235( )
      {
         edtProjetoAnexos_Data_Enabled = defedtProjetoAnexos_Data_Enabled;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoAnexos_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoAnexos_Data_Enabled), 5, 0)));
         edtProjetoAnexos_ArquivoTipo_Enabled = defedtProjetoAnexos_ArquivoTipo_Enabled;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoAnexos_ArquivoTipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoAnexos_ArquivoTipo_Enabled), 5, 0)));
         edtTipoDocumento_Nome_Enabled = defedtTipoDocumento_Nome_Enabled;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoDocumento_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoDocumento_Nome_Enabled), 5, 0)));
         edtProjetoAnexos_ArquivoNome_Enabled = defedtProjetoAnexos_ArquivoNome_Enabled;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoAnexos_ArquivoNome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoAnexos_ArquivoNome_Enabled), 5, 0)));
         edtProjetoAnexos_Codigo_Enabled = defedtProjetoAnexos_Codigo_Enabled;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoAnexos_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtProjetoAnexos_Codigo_Enabled), 5, 0)));
      }

      protected void init_default_properties( )
      {
         lblTextblockprojeto_sigla_Internalname = "TEXTBLOCKPROJETO_SIGLA";
         edtProjeto_Sigla_Internalname = "PROJETO_SIGLA";
         lblTextblockprojeto_identificador_Internalname = "TEXTBLOCKPROJETO_IDENTIFICADOR";
         edtProjeto_Identificador_Internalname = "PROJETO_IDENTIFICADOR";
         tblTablemergedprojeto_sigla_Internalname = "TABLEMERGEDPROJETO_SIGLA";
         lblTextblockprojeto_nome_Internalname = "TEXTBLOCKPROJETO_NOME";
         edtProjeto_Nome_Internalname = "PROJETO_NOME";
         lblTextblockprojeto_tipoprojetocod_Internalname = "TEXTBLOCKPROJETO_TIPOPROJETOCOD";
         dynProjeto_TipoProjetoCod_Internalname = "PROJETO_TIPOPROJETOCOD";
         lblTextblockprojeto_objetivo_Internalname = "TEXTBLOCKPROJETO_OBJETIVO";
         edtProjeto_Objetivo_Internalname = "PROJETO_OBJETIVO";
         lblTextblockprojeto_escopo_Internalname = "TEXTBLOCKPROJETO_ESCOPO";
         edtProjeto_Escopo_Internalname = "PROJETO_ESCOPO";
         lblTextblockprojeto_status_Internalname = "TEXTBLOCKPROJETO_STATUS";
         cmbProjeto_Status_Internalname = "PROJETO_STATUS";
         tblTableprojeto1_Internalname = "TABLEPROJETO1";
         tblTableprojeto_Internalname = "TABLEPROJETO";
         imgBntequipe_Internalname = "BNTEQUIPE";
         tblTableactions3_Internalname = "TABLEACTIONS3";
         tblTableequipe_Internalname = "TABLEEQUIPE";
         lblTextblockprojeto_gpoobjctrlcodigo_Internalname = "TEXTBLOCKPROJETO_GPOOBJCTRLCODIGO";
         dynProjeto_GpoObjCtrlCodigo_Internalname = "PROJETO_GPOOBJCTRLCODIGO";
         lblTextblockprojeto_sistemacodigo_Internalname = "TEXTBLOCKPROJETO_SISTEMACODIGO";
         dynProjeto_SistemaCodigo_Internalname = "PROJETO_SISTEMACODIGO";
         lblTextblockprojeto_modulocodigo_Internalname = "TEXTBLOCKPROJETO_MODULOCODIGO";
         dynProjeto_ModuloCodigo_Internalname = "PROJETO_MODULOCODIGO";
         tblTableobjeto_Internalname = "TABLEOBJETO";
         lblTextblockprojeto_dtinicio_Internalname = "TEXTBLOCKPROJETO_DTINICIO";
         edtProjeto_DTInicio_Internalname = "PROJETO_DTINICIO";
         lblTextblockprojeto_dtfim_Internalname = "TEXTBLOCKPROJETO_DTFIM";
         edtProjeto_DTFim_Internalname = "PROJETO_DTFIM";
         lblTextblockprojeto_prazoprevisto_Internalname = "TEXTBLOCKPROJETO_PRAZOPREVISTO";
         edtProjeto_PrazoPrevisto_Internalname = "PROJETO_PRAZOPREVISTO";
         lblTextblockprojeto_esforcoprevisto_Internalname = "TEXTBLOCKPROJETO_ESFORCOPREVISTO";
         edtProjeto_EsforcoPrevisto_Internalname = "PROJETO_ESFORCOPREVISTO";
         lblTextblockprojeto_custoprevisto_Internalname = "TEXTBLOCKPROJETO_CUSTOPREVISTO";
         edtProjeto_CustoPrevisto_Internalname = "PROJETO_CUSTOPREVISTO";
         tblTableestimativa1_Internalname = "TABLEESTIMATIVA1";
         lblTextblockprojeto_fatorescala_Internalname = "TEXTBLOCKPROJETO_FATORESCALA";
         edtProjeto_FatorEscala_Internalname = "PROJETO_FATORESCALA";
         bttBtnfatorescala_Internalname = "BTNFATORESCALA";
         lblTextblockprojeto_fatormultiplicador_Internalname = "TEXTBLOCKPROJETO_FATORMULTIPLICADOR";
         edtProjeto_FatorMultiplicador_Internalname = "PROJETO_FATORMULTIPLICADOR";
         bttBtnfatormultiplicador_Internalname = "BTNFATORMULTIPLICADOR";
         lblTextblockprojeto_constacocomo_Internalname = "TEXTBLOCKPROJETO_CONSTACOCOMO";
         edtProjeto_ConstACocomo_Internalname = "PROJETO_CONSTACOCOMO";
         tblTablemergedprojeto_fatorescala_Internalname = "TABLEMERGEDPROJETO_FATORESCALA";
         lblTextblockprojeto_incremental_Internalname = "TEXTBLOCKPROJETO_INCREMENTAL";
         chkProjeto_Incremental_Internalname = "PROJETO_INCREMENTAL";
         tblTableestimativa2_Internalname = "TABLEESTIMATIVA2";
         tblTableestimativa_Internalname = "TABLEESTIMATIVA";
         imgBntanexos_Internalname = "BNTANEXOS";
         tblTableactions2_Internalname = "TABLEACTIONS2";
         edtProjetoAnexos_Codigo_Internalname = "PROJETOANEXOS_CODIGO";
         edtProjetoAnexos_Arquivo_Internalname = "PROJETOANEXOS_ARQUIVO";
         edtProjetoAnexos_ArquivoNome_Internalname = "PROJETOANEXOS_ARQUIVONOME";
         edtTipoDocumento_Nome_Internalname = "TIPODOCUMENTO_NOME";
         dynTipoDocumento_Codigo_Internalname = "TIPODOCUMENTO_CODIGO";
         edtProjetoAnexos_ArquivoTipo_Internalname = "PROJETOANEXOS_ARQUIVOTIPO";
         edtProjetoAnexos_Data_Internalname = "PROJETOANEXOS_DATA";
         tblTableanexos_Internalname = "TABLEANEXOS";
         Gxuitabspanel_tabmain_Internalname = "GXUITABSPANEL_TABMAIN";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtProjeto_Codigo_Internalname = "PROJETO_CODIGO";
         Form.Internalname = "FORM";
         subGridanexos_Internalname = "GRIDANEXOS";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Gxuitabspanel_tabmain_Designtimetabs = "[{\"id\":\"TabProjeto\"},{\"id\":\"TabEquipe\"},{\"id\":\"TabObjeto\"},{\"id\":\"TabEstimativa\"},{\"id\":\"TabAnexos\"}]";
         Gxuitabspanel_tabmain_Autoscroll = Convert.ToBoolean( -1);
         Gxuitabspanel_tabmain_Autoheight = Convert.ToBoolean( -1);
         Gxuitabspanel_tabmain_Autowidth = Convert.ToBoolean( 0);
         Gxuitabspanel_tabmain_Cls = "GXUI-DVelop-Tabs";
         Gxuitabspanel_tabmain_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Projeto";
         edtProjetoAnexos_Data_Jsonclick = "";
         edtProjetoAnexos_ArquivoTipo_Jsonclick = "";
         dynTipoDocumento_Codigo_Jsonclick = "";
         edtTipoDocumento_Nome_Jsonclick = "";
         edtProjetoAnexos_ArquivoNome_Jsonclick = "";
         edtProjetoAnexos_Arquivo_Jsonclick = "";
         edtProjetoAnexos_Arquivo_Parameters = "";
         edtProjetoAnexos_Arquivo_Contenttype = "";
         edtProjetoAnexos_Codigo_Jsonclick = "";
         subGridanexos_Class = "WorkWithBorder WorkWith";
         edtProjetoAnexos_Arquivo_Filename = "";
         edtProjetoAnexos_Arquivo_Filetype = "";
         edtProjeto_Identificador_Jsonclick = "";
         edtProjeto_Identificador_Enabled = 1;
         edtProjeto_Sigla_Jsonclick = "";
         edtProjeto_Sigla_Enabled = 1;
         cmbProjeto_Status_Jsonclick = "";
         cmbProjeto_Status.Enabled = 0;
         edtProjeto_Escopo_Enabled = 1;
         edtProjeto_Objetivo_Enabled = 1;
         dynProjeto_TipoProjetoCod_Jsonclick = "";
         dynProjeto_TipoProjetoCod.Enabled = 1;
         edtProjeto_Nome_Jsonclick = "";
         edtProjeto_Nome_Enabled = 1;
         imgBntequipe_Visible = 1;
         dynProjeto_ModuloCodigo_Jsonclick = "";
         dynProjeto_ModuloCodigo.Enabled = 1;
         dynProjeto_SistemaCodigo_Jsonclick = "";
         dynProjeto_SistemaCodigo.Enabled = 1;
         dynProjeto_GpoObjCtrlCodigo_Jsonclick = "";
         dynProjeto_GpoObjCtrlCodigo.Enabled = 1;
         edtProjeto_CustoPrevisto_Jsonclick = "";
         edtProjeto_CustoPrevisto_Enabled = 1;
         edtProjeto_EsforcoPrevisto_Jsonclick = "";
         edtProjeto_EsforcoPrevisto_Enabled = 1;
         edtProjeto_PrazoPrevisto_Jsonclick = "";
         edtProjeto_PrazoPrevisto_Enabled = 1;
         edtProjeto_DTFim_Jsonclick = "";
         edtProjeto_DTFim_Enabled = 1;
         edtProjeto_DTInicio_Jsonclick = "";
         edtProjeto_DTInicio_Enabled = 1;
         edtProjeto_ConstACocomo_Jsonclick = "";
         edtProjeto_ConstACocomo_Enabled = 1;
         bttBtnfatormultiplicador_Visible = 1;
         edtProjeto_FatorMultiplicador_Jsonclick = "";
         edtProjeto_FatorMultiplicador_Enabled = 0;
         bttBtnfatorescala_Visible = 1;
         edtProjeto_FatorEscala_Jsonclick = "";
         edtProjeto_FatorEscala_Enabled = 0;
         chkProjeto_Incremental.Enabled = 1;
         imgBntanexos_Visible = 1;
         subGridanexos_Rows = 5;
         subGridanexos_Allowcollapsing = 0;
         subGridanexos_Allowselection = 0;
         edtProjetoAnexos_Data_Enabled = 0;
         edtProjetoAnexos_ArquivoTipo_Enabled = 0;
         dynTipoDocumento_Codigo.Enabled = 1;
         edtTipoDocumento_Nome_Enabled = 0;
         edtProjetoAnexos_ArquivoNome_Enabled = 0;
         edtProjetoAnexos_Arquivo_Enabled = 1;
         edtProjetoAnexos_Codigo_Enabled = 0;
         subGridanexos_Backcolorstyle = 3;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtProjeto_Codigo_Jsonclick = "";
         edtProjeto_Codigo_Enabled = 0;
         edtProjeto_Codigo_Visible = 1;
         chkProjeto_Incremental.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         GXAPROJETO_SISTEMACODIGO_html2986( AV8WWPContext, A2153Projeto_GpoObjCtrlCodigo) ;
         GXAPROJETO_SISTEMACODIGO_html2986( AV8WWPContext, A2153Projeto_GpoObjCtrlCodigo) ;
         /* End function dynload_actions */
      }

      protected void GXDLAPROJETO_TIPOPROJETOCOD2986( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAPROJETO_TIPOPROJETOCOD_data2986( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAPROJETO_TIPOPROJETOCOD_html2986( )
      {
         int gxdynajaxvalue ;
         GXDLAPROJETO_TIPOPROJETOCOD_data2986( ) ;
         gxdynajaxindex = 1;
         dynProjeto_TipoProjetoCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynProjeto_TipoProjetoCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAPROJETO_TIPOPROJETOCOD_data2986( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T002950 */
         pr_default.execute(44);
         while ( (pr_default.getStatus(44) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T002950_A983Projeto_TipoProjetoCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T002950_A980TipoProjeto_Nome[0]));
            pr_default.readNext(44);
         }
         pr_default.close(44);
      }

      protected void GXDLAPROJETO_GPOOBJCTRLCODIGO2986( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAPROJETO_GPOOBJCTRLCODIGO_data2986( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAPROJETO_GPOOBJCTRLCODIGO_html2986( )
      {
         int gxdynajaxvalue ;
         GXDLAPROJETO_GPOOBJCTRLCODIGO_data2986( ) ;
         gxdynajaxindex = 1;
         dynProjeto_GpoObjCtrlCodigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynProjeto_GpoObjCtrlCodigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAPROJETO_GPOOBJCTRLCODIGO_data2986( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor T002951 */
         pr_default.execute(45);
         while ( (pr_default.getStatus(45) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T002951_A2153Projeto_GpoObjCtrlCodigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T002951_A2154Projeto_GpoObjCtrlNome[0]));
            pr_default.readNext(45);
         }
         pr_default.close(45);
      }

      protected void GXDLAPROJETO_SISTEMACODIGO2986( wwpbaseobjects.SdtWWPContext AV8WWPContext ,
                                                     int A2153Projeto_GpoObjCtrlCodigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAPROJETO_SISTEMACODIGO_data2986( AV8WWPContext, A2153Projeto_GpoObjCtrlCodigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAPROJETO_SISTEMACODIGO_html2986( wwpbaseobjects.SdtWWPContext AV8WWPContext ,
                                                        int A2153Projeto_GpoObjCtrlCodigo )
      {
         int gxdynajaxvalue ;
         GXDLAPROJETO_SISTEMACODIGO_data2986( AV8WWPContext, A2153Projeto_GpoObjCtrlCodigo) ;
         gxdynajaxindex = 1;
         dynProjeto_SistemaCodigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynProjeto_SistemaCodigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAPROJETO_SISTEMACODIGO_data2986( wwpbaseobjects.SdtWWPContext AV8WWPContext ,
                                                          int A2153Projeto_GpoObjCtrlCodigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor T002952 */
         pr_default.execute(46, new Object[] {AV8WWPContext.gxTpr_Areatrabalho_codigo, n2153Projeto_GpoObjCtrlCodigo, A2153Projeto_GpoObjCtrlCodigo});
         while ( (pr_default.getStatus(46) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T002952_A127Sistema_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(T002952_A416Sistema_Nome[0]);
            pr_default.readNext(46);
         }
         pr_default.close(46);
      }

      protected void GXDLAPROJETO_MODULOCODIGO2986( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAPROJETO_MODULOCODIGO_data2986( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAPROJETO_MODULOCODIGO_html2986( )
      {
         int gxdynajaxvalue ;
         GXDLAPROJETO_MODULOCODIGO_data2986( ) ;
         gxdynajaxindex = 1;
         dynProjeto_ModuloCodigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynProjeto_ModuloCodigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAPROJETO_MODULOCODIGO_data2986( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor T002953 */
         pr_default.execute(47, new Object[] {n127Sistema_Codigo, A127Sistema_Codigo});
         while ( (pr_default.getStatus(47) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T002953_A2158Projeto_ModuloCodigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T002953_A2160Projeto_ModuloSigla[0]));
            pr_default.readNext(47);
         }
         pr_default.close(47);
      }

      protected void GXDLATIPODOCUMENTO_CODIGO29235( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLATIPODOCUMENTO_CODIGO_data29235( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXATIPODOCUMENTO_CODIGO_html29235( )
      {
         int gxdynajaxvalue ;
         GXDLATIPODOCUMENTO_CODIGO_data29235( ) ;
         gxdynajaxindex = 1;
         dynTipoDocumento_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynTipoDocumento_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLATIPODOCUMENTO_CODIGO_data29235( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor T002954 */
         pr_default.execute(48);
         while ( (pr_default.getStatus(48) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T002954_A645TipoDocumento_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T002954_A646TipoDocumento_Nome[0]));
            pr_default.readNext(48);
         }
         pr_default.close(48);
      }

      protected void GX5ASAPROJETO_SISTEMACOD2986( int A648Projeto_Codigo )
      {
         GXt_int1 = A740Projeto_SistemaCod;
         new prc_projeto_sistemacod(context ).execute( ref  A648Projeto_Codigo, ref  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
         A740Projeto_SistemaCod = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A740Projeto_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A740Projeto_SistemaCod), 6, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A740Projeto_SistemaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void gxnrGridanexos_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         Gx_mode = "INS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         SubsflControlProps_154235( ) ;
         while ( nGXsfl_154_idx <= nRC_GXsfl_154 )
         {
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            standaloneNotModal29235( ) ;
            standaloneModal29235( ) ;
            dynProjeto_TipoProjetoCod.Name = "PROJETO_TIPOPROJETOCOD";
            dynProjeto_TipoProjetoCod.WebTags = "";
            cmbProjeto_Status.Name = "PROJETO_STATUS";
            cmbProjeto_Status.WebTags = "";
            cmbProjeto_Status.addItem("A", "Aberto", 0);
            cmbProjeto_Status.addItem("E", "Em Contagem", 0);
            cmbProjeto_Status.addItem("C", "Contado", 0);
            if ( cmbProjeto_Status.ItemCount > 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( A658Projeto_Status)) )
               {
                  A658Projeto_Status = "A";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A658Projeto_Status", A658Projeto_Status);
               }
               A658Projeto_Status = cmbProjeto_Status.getValidValue(A658Projeto_Status);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A658Projeto_Status", A658Projeto_Status);
            }
            dynProjeto_GpoObjCtrlCodigo.Name = "PROJETO_GPOOBJCTRLCODIGO";
            dynProjeto_GpoObjCtrlCodigo.WebTags = "";
            dynProjeto_SistemaCodigo.Name = "PROJETO_SISTEMACODIGO";
            dynProjeto_SistemaCodigo.WebTags = "";
            dynProjeto_ModuloCodigo.Name = "PROJETO_MODULOCODIGO";
            dynProjeto_ModuloCodigo.WebTags = "";
            chkProjeto_Incremental.Name = "PROJETO_INCREMENTAL";
            chkProjeto_Incremental.WebTags = "";
            chkProjeto_Incremental.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkProjeto_Incremental_Internalname, "TitleCaption", chkProjeto_Incremental.Caption);
            chkProjeto_Incremental.CheckedValue = "false";
            GXCCtl = "TIPODOCUMENTO_CODIGO_" + sGXsfl_154_idx;
            dynTipoDocumento_Codigo.Name = GXCCtl;
            dynTipoDocumento_Codigo.WebTags = "";
            dynload_actions( ) ;
            SendRow29235( ) ;
            nGXsfl_154_idx = (short)(nGXsfl_154_idx+1);
            sGXsfl_154_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_154_idx), 4, 0)), 4, "0");
            SubsflControlProps_154235( ) ;
         }
         context.GX_webresponse.AddString(GridanexosContainer.ToJavascriptSource());
         /* End function gxnrGridanexos_newrow */
      }

      public void Valid_Projeto_codigo( int GX_Parm1 ,
                                        int GX_Parm2 )
      {
         A648Projeto_Codigo = GX_Parm1;
         A740Projeto_SistemaCod = GX_Parm2;
         GXt_int1 = A740Projeto_SistemaCod;
         new prc_projeto_sistemacod(context ).execute( ref  A648Projeto_Codigo, ref  GXt_int1) ;
         A740Projeto_SistemaCod = GXt_int1;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A740Projeto_SistemaCod), 6, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Projeto_tipoprojetocod( GXCombobox dynGX_Parm1 )
      {
         dynProjeto_TipoProjetoCod = dynGX_Parm1;
         A983Projeto_TipoProjetoCod = (int)(NumberUtil.Val( dynProjeto_TipoProjetoCod.CurrentValue, "."));
         n983Projeto_TipoProjetoCod = false;
         /* Using cursor T002955 */
         pr_default.execute(49, new Object[] {n983Projeto_TipoProjetoCod, A983Projeto_TipoProjetoCod});
         if ( (pr_default.getStatus(49) == 101) )
         {
            if ( ! ( (0==A983Projeto_TipoProjetoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Projeto_Tipo Projeto'.", "ForeignKeyNotFound", 1, "PROJETO_TIPOPROJETOCOD");
               AnyError = 1;
               GX_FocusControl = dynProjeto_TipoProjetoCod_Internalname;
            }
         }
         pr_default.close(49);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Projeto_gpoobjctrlcodigo( GXCombobox dynGX_Parm1 ,
                                                  wwpbaseobjects.SdtWWPContext GX_Parm2 ,
                                                  String GX_Parm3 ,
                                                  GXCombobox dynGX_Parm4 )
      {
         dynProjeto_GpoObjCtrlCodigo = dynGX_Parm1;
         A2153Projeto_GpoObjCtrlCodigo = (int)(NumberUtil.Val( dynProjeto_GpoObjCtrlCodigo.CurrentValue, "."));
         n2153Projeto_GpoObjCtrlCodigo = false;
         AV8WWPContext = GX_Parm2;
         A2154Projeto_GpoObjCtrlNome = GX_Parm3;
         n2154Projeto_GpoObjCtrlNome = false;
         dynProjeto_SistemaCodigo = dynGX_Parm4;
         A2155Projeto_SistemaCodigo = (int)(NumberUtil.Val( dynProjeto_SistemaCodigo.CurrentValue, "."));
         n2155Projeto_SistemaCodigo = false;
         /* Using cursor T002929 */
         pr_default.execute(24, new Object[] {n2153Projeto_GpoObjCtrlCodigo, A2153Projeto_GpoObjCtrlCodigo});
         if ( (pr_default.getStatus(24) == 101) )
         {
            if ( ! ( (0==A2153Projeto_GpoObjCtrlCodigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Grupo Objeto Controle'.", "ForeignKeyNotFound", 1, "PROJETO_GPOOBJCTRLCODIGO");
               AnyError = 1;
               GX_FocusControl = dynProjeto_GpoObjCtrlCodigo_Internalname;
            }
         }
         A2154Projeto_GpoObjCtrlNome = T002929_A2154Projeto_GpoObjCtrlNome[0];
         n2154Projeto_GpoObjCtrlNome = T002929_n2154Projeto_GpoObjCtrlNome[0];
         pr_default.close(24);
         GXAPROJETO_SISTEMACODIGO_html2986( AV8WWPContext, A2153Projeto_GpoObjCtrlCodigo) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A2154Projeto_GpoObjCtrlNome = "";
            n2154Projeto_GpoObjCtrlNome = false;
         }
         dynProjeto_SistemaCodigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2155Projeto_SistemaCodigo), 6, 0));
         isValidOutput.Add(StringUtil.RTrim( A2154Projeto_GpoObjCtrlNome));
         if ( dynProjeto_SistemaCodigo.ItemCount > 0 )
         {
            A2155Projeto_SistemaCodigo = (int)(NumberUtil.Val( dynProjeto_SistemaCodigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2155Projeto_SistemaCodigo), 6, 0))), "."));
            n2155Projeto_SistemaCodigo = false;
         }
         dynProjeto_SistemaCodigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2155Projeto_SistemaCodigo), 6, 0));
         isValidOutput.Add(dynProjeto_SistemaCodigo);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Projeto_sistemacodigo( GXCombobox dynGX_Parm1 ,
                                               String GX_Parm2 ,
                                               String GX_Parm3 ,
                                               decimal GX_Parm4 ,
                                               short GX_Parm5 ,
                                               short GX_Parm6 )
      {
         dynProjeto_SistemaCodigo = dynGX_Parm1;
         A2155Projeto_SistemaCodigo = (int)(NumberUtil.Val( dynProjeto_SistemaCodigo.CurrentValue, "."));
         n2155Projeto_SistemaCodigo = false;
         A2156Projeto_SistemaNome = GX_Parm2;
         n2156Projeto_SistemaNome = false;
         A2157Projeto_SistemaSigla = GX_Parm3;
         n2157Projeto_SistemaSigla = false;
         A655Projeto_Custo = GX_Parm4;
         A656Projeto_Prazo = GX_Parm5;
         A657Projeto_Esforco = GX_Parm6;
         n657Projeto_Esforco = false;
         /* Using cursor T002930 */
         pr_default.execute(25, new Object[] {n2155Projeto_SistemaCodigo, A2155Projeto_SistemaCodigo});
         if ( (pr_default.getStatus(25) == 101) )
         {
            if ( ! ( (0==A2155Projeto_SistemaCodigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "PROJETO_SISTEMACODIGO");
               AnyError = 1;
               GX_FocusControl = dynProjeto_SistemaCodigo_Internalname;
            }
         }
         A2156Projeto_SistemaNome = T002930_A2156Projeto_SistemaNome[0];
         n2156Projeto_SistemaNome = T002930_n2156Projeto_SistemaNome[0];
         A2157Projeto_SistemaSigla = T002930_A2157Projeto_SistemaSigla[0];
         n2157Projeto_SistemaSigla = T002930_n2157Projeto_SistemaSigla[0];
         pr_default.close(25);
         /* Using cursor T002932 */
         pr_default.execute(26, new Object[] {n2155Projeto_SistemaCodigo, A2155Projeto_SistemaCodigo});
         if ( (pr_default.getStatus(26) != 101) )
         {
            A655Projeto_Custo = T002932_A655Projeto_Custo[0];
            A656Projeto_Prazo = T002932_A656Projeto_Prazo[0];
            A657Projeto_Esforco = T002932_A657Projeto_Esforco[0];
            n657Projeto_Esforco = T002932_n657Projeto_Esforco[0];
         }
         else
         {
            A657Projeto_Esforco = 0;
            n657Projeto_Esforco = false;
            A656Projeto_Prazo = 0;
            A655Projeto_Custo = 0;
         }
         pr_default.close(26);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A2156Projeto_SistemaNome = "";
            n2156Projeto_SistemaNome = false;
            A2157Projeto_SistemaSigla = "";
            n2157Projeto_SistemaSigla = false;
            A655Projeto_Custo = 0;
            A656Projeto_Prazo = 0;
            A657Projeto_Esforco = 0;
            n657Projeto_Esforco = false;
         }
         isValidOutput.Add(A2156Projeto_SistemaNome);
         isValidOutput.Add(StringUtil.RTrim( A2157Projeto_SistemaSigla));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A655Projeto_Custo, 12, 2, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A656Projeto_Prazo), 4, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A657Projeto_Esforco), 4, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Projeto_modulocodigo( GXCombobox dynGX_Parm1 ,
                                              String GX_Parm2 ,
                                              String GX_Parm3 )
      {
         dynProjeto_ModuloCodigo = dynGX_Parm1;
         A2158Projeto_ModuloCodigo = (int)(NumberUtil.Val( dynProjeto_ModuloCodigo.CurrentValue, "."));
         n2158Projeto_ModuloCodigo = false;
         A2159Projeto_ModuloNome = GX_Parm2;
         n2159Projeto_ModuloNome = false;
         A2160Projeto_ModuloSigla = GX_Parm3;
         n2160Projeto_ModuloSigla = false;
         /* Using cursor T002933 */
         pr_default.execute(27, new Object[] {n2158Projeto_ModuloCodigo, A2158Projeto_ModuloCodigo});
         if ( (pr_default.getStatus(27) == 101) )
         {
            if ( ! ( (0==A2158Projeto_ModuloCodigo) ) )
            {
               GX_msglist.addItem("N�o existe 'M�dulo '.", "ForeignKeyNotFound", 1, "PROJETO_MODULOCODIGO");
               AnyError = 1;
               GX_FocusControl = dynProjeto_ModuloCodigo_Internalname;
            }
         }
         A2159Projeto_ModuloNome = T002933_A2159Projeto_ModuloNome[0];
         n2159Projeto_ModuloNome = T002933_n2159Projeto_ModuloNome[0];
         A2160Projeto_ModuloSigla = T002933_A2160Projeto_ModuloSigla[0];
         n2160Projeto_ModuloSigla = T002933_n2160Projeto_ModuloSigla[0];
         pr_default.close(27);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A2159Projeto_ModuloNome = "";
            n2159Projeto_ModuloNome = false;
            A2160Projeto_ModuloSigla = "";
            n2160Projeto_ModuloSigla = false;
         }
         isValidOutput.Add(StringUtil.RTrim( A2159Projeto_ModuloNome));
         isValidOutput.Add(StringUtil.RTrim( A2160Projeto_ModuloSigla));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Tipodocumento_codigo( GXCombobox dynGX_Parm1 ,
                                              String GX_Parm2 )
      {
         dynTipoDocumento_Codigo = dynGX_Parm1;
         A645TipoDocumento_Codigo = (int)(NumberUtil.Val( dynTipoDocumento_Codigo.CurrentValue, "."));
         n645TipoDocumento_Codigo = false;
         A646TipoDocumento_Nome = GX_Parm2;
         /* Using cursor T002956 */
         pr_default.execute(50, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
         if ( (pr_default.getStatus(50) == 101) )
         {
            if ( ! ( (0==A645TipoDocumento_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Tipo de Documentos'.", "ForeignKeyNotFound", 1, "TIPODOCUMENTO_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynTipoDocumento_Codigo_Internalname;
            }
         }
         A646TipoDocumento_Nome = T002956_A646TipoDocumento_Nome[0];
         pr_default.close(50);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A646TipoDocumento_Nome = "";
         }
         isValidOutput.Add(StringUtil.RTrim( A646TipoDocumento_Nome));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Projeto_Codigo',fld:'vPROJETO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E15292',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("'DOBNTANEXOS'","{handler:'E16292',iparms:[],oparms:[]}");
         setEventMetadata("'DOFATORESCALA'","{handler:'E112986',iparms:[{av:'AV8WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV7Projeto_Codigo',fld:'vPROJETO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A985Projeto_FatorEscala',fld:'PROJETO_FATORESCALA',pic:'ZZ9.99',nv:0.0},{av:'AV7Projeto_Codigo',fld:'vPROJETO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOFATORMULTIPLICADOR'","{handler:'E122986',iparms:[{av:'AV8WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV7Projeto_Codigo',fld:'vPROJETO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A989Projeto_FatorMultiplicador',fld:'PROJETO_FATORMULTIPLICADOR',pic:'ZZ9.99',nv:0.0},{av:'AV7Projeto_Codigo',fld:'vPROJETO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOBNTEQUIPE'","{handler:'E132986',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(50);
         pr_default.close(42);
         pr_default.close(4);
         pr_default.close(49);
         pr_default.close(28);
         pr_default.close(24);
         pr_default.close(25);
         pr_default.close(27);
         pr_default.close(26);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z649Projeto_Nome = "";
         Z650Projeto_Sigla = "";
         Z652Projeto_TecnicaContagem = "";
         Z1541Projeto_Previsao = DateTime.MinValue;
         Z658Projeto_Status = "";
         Z2144Projeto_Identificador = "";
         Z2146Projeto_DTInicio = DateTime.MinValue;
         Z2147Projeto_DTFim = DateTime.MinValue;
         Z2169ProjetoAnexos_Data = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         A658Projeto_Status = "";
         GXCCtl = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         GridanexosContainer = new GXWebGrid( context);
         GridanexosColumn = new GXWebColumn();
         A2165ProjetoAnexos_Arquivo = "";
         A2166ProjetoAnexos_ArquivoNome = "";
         A646TipoDocumento_Nome = "";
         A2167ProjetoAnexos_ArquivoTipo = "";
         A2169ProjetoAnexos_Data = (DateTime)(DateTime.MinValue);
         sMode235 = "";
         imgBntanexos_Jsonclick = "";
         lblTextblockprojeto_fatorescala_Jsonclick = "";
         lblTextblockprojeto_incremental_Jsonclick = "";
         bttBtnfatorescala_Jsonclick = "";
         lblTextblockprojeto_fatormultiplicador_Jsonclick = "";
         bttBtnfatormultiplicador_Jsonclick = "";
         lblTextblockprojeto_constacocomo_Jsonclick = "";
         lblTextblockprojeto_dtinicio_Jsonclick = "";
         A2146Projeto_DTInicio = DateTime.MinValue;
         lblTextblockprojeto_dtfim_Jsonclick = "";
         A2147Projeto_DTFim = DateTime.MinValue;
         lblTextblockprojeto_prazoprevisto_Jsonclick = "";
         lblTextblockprojeto_esforcoprevisto_Jsonclick = "";
         lblTextblockprojeto_custoprevisto_Jsonclick = "";
         lblTextblockprojeto_gpoobjctrlcodigo_Jsonclick = "";
         lblTextblockprojeto_sistemacodigo_Jsonclick = "";
         lblTextblockprojeto_modulocodigo_Jsonclick = "";
         imgBntequipe_Jsonclick = "";
         lblTextblockprojeto_sigla_Jsonclick = "";
         lblTextblockprojeto_nome_Jsonclick = "";
         A649Projeto_Nome = "";
         lblTextblockprojeto_tipoprojetocod_Jsonclick = "";
         lblTextblockprojeto_objetivo_Jsonclick = "";
         A2145Projeto_Objetivo = "";
         lblTextblockprojeto_escopo_Jsonclick = "";
         A653Projeto_Escopo = "";
         lblTextblockprojeto_status_Jsonclick = "";
         A650Projeto_Sigla = "";
         lblTextblockprojeto_identificador_Jsonclick = "";
         A2144Projeto_Identificador = "";
         A1541Projeto_Previsao = DateTime.MinValue;
         A652Projeto_TecnicaContagem = "";
         A1540Projeto_Introducao = "";
         A2152Projeto_AreaTrabalhoDescricao = "";
         A2154Projeto_GpoObjCtrlNome = "";
         A2156Projeto_SistemaNome = "";
         A2157Projeto_SistemaSigla = "";
         A2159Projeto_ModuloNome = "";
         A2160Projeto_ModuloSigla = "";
         AV24Pgmname = "";
         A2164ProjetoAnexos_Descricao = "";
         A2168ProjetoAnexos_Link = "";
         Gxuitabspanel_tabmain_Height = "";
         Gxuitabspanel_tabmain_Class = "";
         Gxuitabspanel_tabmain_Activetabid = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode86 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z1540Projeto_Introducao = "";
         Z653Projeto_Escopo = "";
         Z2145Projeto_Objetivo = "";
         Z2152Projeto_AreaTrabalhoDescricao = "";
         Z2154Projeto_GpoObjCtrlNome = "";
         Z2156Projeto_SistemaNome = "";
         Z2157Projeto_SistemaSigla = "";
         Z2159Projeto_ModuloNome = "";
         Z2160Projeto_ModuloSigla = "";
         T00299_A2154Projeto_GpoObjCtrlNome = new String[] {""} ;
         T00299_n2154Projeto_GpoObjCtrlNome = new bool[] {false} ;
         T002911_A2159Projeto_ModuloNome = new String[] {""} ;
         T002911_n2159Projeto_ModuloNome = new bool[] {false} ;
         T002911_A2160Projeto_ModuloSigla = new String[] {""} ;
         T002911_n2160Projeto_ModuloSigla = new bool[] {false} ;
         T002910_A2156Projeto_SistemaNome = new String[] {""} ;
         T002910_n2156Projeto_SistemaNome = new bool[] {false} ;
         T002910_A2157Projeto_SistemaSigla = new String[] {""} ;
         T002910_n2157Projeto_SistemaSigla = new bool[] {false} ;
         T002913_A655Projeto_Custo = new decimal[1] ;
         T002913_A656Projeto_Prazo = new short[1] ;
         T002913_A657Projeto_Esforco = new short[1] ;
         T002913_n657Projeto_Esforco = new bool[] {false} ;
         T00298_A2152Projeto_AreaTrabalhoDescricao = new String[] {""} ;
         T00298_n2152Projeto_AreaTrabalhoDescricao = new bool[] {false} ;
         T002915_A648Projeto_Codigo = new int[1] ;
         T002915_A649Projeto_Nome = new String[] {""} ;
         T002915_A650Projeto_Sigla = new String[] {""} ;
         T002915_A652Projeto_TecnicaContagem = new String[] {""} ;
         T002915_A1540Projeto_Introducao = new String[] {""} ;
         T002915_n1540Projeto_Introducao = new bool[] {false} ;
         T002915_A653Projeto_Escopo = new String[] {""} ;
         T002915_A1541Projeto_Previsao = new DateTime[] {DateTime.MinValue} ;
         T002915_n1541Projeto_Previsao = new bool[] {false} ;
         T002915_A1542Projeto_GerenteCod = new int[1] ;
         T002915_n1542Projeto_GerenteCod = new bool[] {false} ;
         T002915_A1543Projeto_ServicoCod = new int[1] ;
         T002915_n1543Projeto_ServicoCod = new bool[] {false} ;
         T002915_A658Projeto_Status = new String[] {""} ;
         T002915_A985Projeto_FatorEscala = new decimal[1] ;
         T002915_n985Projeto_FatorEscala = new bool[] {false} ;
         T002915_A989Projeto_FatorMultiplicador = new decimal[1] ;
         T002915_n989Projeto_FatorMultiplicador = new bool[] {false} ;
         T002915_A990Projeto_ConstACocomo = new decimal[1] ;
         T002915_n990Projeto_ConstACocomo = new bool[] {false} ;
         T002915_A1232Projeto_Incremental = new bool[] {false} ;
         T002915_n1232Projeto_Incremental = new bool[] {false} ;
         T002915_A2144Projeto_Identificador = new String[] {""} ;
         T002915_n2144Projeto_Identificador = new bool[] {false} ;
         T002915_A2145Projeto_Objetivo = new String[] {""} ;
         T002915_n2145Projeto_Objetivo = new bool[] {false} ;
         T002915_A2146Projeto_DTInicio = new DateTime[] {DateTime.MinValue} ;
         T002915_n2146Projeto_DTInicio = new bool[] {false} ;
         T002915_A2147Projeto_DTFim = new DateTime[] {DateTime.MinValue} ;
         T002915_n2147Projeto_DTFim = new bool[] {false} ;
         T002915_A2148Projeto_PrazoPrevisto = new short[1] ;
         T002915_n2148Projeto_PrazoPrevisto = new bool[] {false} ;
         T002915_A2149Projeto_EsforcoPrevisto = new short[1] ;
         T002915_n2149Projeto_EsforcoPrevisto = new bool[] {false} ;
         T002915_A2150Projeto_CustoPrevisto = new decimal[1] ;
         T002915_n2150Projeto_CustoPrevisto = new bool[] {false} ;
         T002915_A2152Projeto_AreaTrabalhoDescricao = new String[] {""} ;
         T002915_n2152Projeto_AreaTrabalhoDescricao = new bool[] {false} ;
         T002915_A2154Projeto_GpoObjCtrlNome = new String[] {""} ;
         T002915_n2154Projeto_GpoObjCtrlNome = new bool[] {false} ;
         T002915_A2156Projeto_SistemaNome = new String[] {""} ;
         T002915_n2156Projeto_SistemaNome = new bool[] {false} ;
         T002915_A2157Projeto_SistemaSigla = new String[] {""} ;
         T002915_n2157Projeto_SistemaSigla = new bool[] {false} ;
         T002915_A2159Projeto_ModuloNome = new String[] {""} ;
         T002915_n2159Projeto_ModuloNome = new bool[] {false} ;
         T002915_A2160Projeto_ModuloSigla = new String[] {""} ;
         T002915_n2160Projeto_ModuloSigla = new bool[] {false} ;
         T002915_A2170Projeto_AnexoSequecial = new short[1] ;
         T002915_n2170Projeto_AnexoSequecial = new bool[] {false} ;
         T002915_A983Projeto_TipoProjetoCod = new int[1] ;
         T002915_n983Projeto_TipoProjetoCod = new bool[] {false} ;
         T002915_A2151Projeto_AreaTrabalhoCodigo = new int[1] ;
         T002915_n2151Projeto_AreaTrabalhoCodigo = new bool[] {false} ;
         T002915_A2153Projeto_GpoObjCtrlCodigo = new int[1] ;
         T002915_n2153Projeto_GpoObjCtrlCodigo = new bool[] {false} ;
         T002915_A2155Projeto_SistemaCodigo = new int[1] ;
         T002915_n2155Projeto_SistemaCodigo = new bool[] {false} ;
         T002915_A2158Projeto_ModuloCodigo = new int[1] ;
         T002915_n2158Projeto_ModuloCodigo = new bool[] {false} ;
         T002915_A655Projeto_Custo = new decimal[1] ;
         T002915_A656Projeto_Prazo = new short[1] ;
         T002915_A657Projeto_Esforco = new short[1] ;
         T002915_n657Projeto_Esforco = new bool[] {false} ;
         T00297_A983Projeto_TipoProjetoCod = new int[1] ;
         T00297_n983Projeto_TipoProjetoCod = new bool[] {false} ;
         T002916_A983Projeto_TipoProjetoCod = new int[1] ;
         T002916_n983Projeto_TipoProjetoCod = new bool[] {false} ;
         T002917_A2154Projeto_GpoObjCtrlNome = new String[] {""} ;
         T002917_n2154Projeto_GpoObjCtrlNome = new bool[] {false} ;
         T002918_A2156Projeto_SistemaNome = new String[] {""} ;
         T002918_n2156Projeto_SistemaNome = new bool[] {false} ;
         T002918_A2157Projeto_SistemaSigla = new String[] {""} ;
         T002918_n2157Projeto_SistemaSigla = new bool[] {false} ;
         T002920_A655Projeto_Custo = new decimal[1] ;
         T002920_A656Projeto_Prazo = new short[1] ;
         T002920_A657Projeto_Esforco = new short[1] ;
         T002920_n657Projeto_Esforco = new bool[] {false} ;
         T002921_A2159Projeto_ModuloNome = new String[] {""} ;
         T002921_n2159Projeto_ModuloNome = new bool[] {false} ;
         T002921_A2160Projeto_ModuloSigla = new String[] {""} ;
         T002921_n2160Projeto_ModuloSigla = new bool[] {false} ;
         T002922_A2152Projeto_AreaTrabalhoDescricao = new String[] {""} ;
         T002922_n2152Projeto_AreaTrabalhoDescricao = new bool[] {false} ;
         T002923_A648Projeto_Codigo = new int[1] ;
         T00296_A648Projeto_Codigo = new int[1] ;
         T00296_A649Projeto_Nome = new String[] {""} ;
         T00296_A650Projeto_Sigla = new String[] {""} ;
         T00296_A652Projeto_TecnicaContagem = new String[] {""} ;
         T00296_A1540Projeto_Introducao = new String[] {""} ;
         T00296_n1540Projeto_Introducao = new bool[] {false} ;
         T00296_A653Projeto_Escopo = new String[] {""} ;
         T00296_A1541Projeto_Previsao = new DateTime[] {DateTime.MinValue} ;
         T00296_n1541Projeto_Previsao = new bool[] {false} ;
         T00296_A1542Projeto_GerenteCod = new int[1] ;
         T00296_n1542Projeto_GerenteCod = new bool[] {false} ;
         T00296_A1543Projeto_ServicoCod = new int[1] ;
         T00296_n1543Projeto_ServicoCod = new bool[] {false} ;
         T00296_A658Projeto_Status = new String[] {""} ;
         T00296_A985Projeto_FatorEscala = new decimal[1] ;
         T00296_n985Projeto_FatorEscala = new bool[] {false} ;
         T00296_A989Projeto_FatorMultiplicador = new decimal[1] ;
         T00296_n989Projeto_FatorMultiplicador = new bool[] {false} ;
         T00296_A990Projeto_ConstACocomo = new decimal[1] ;
         T00296_n990Projeto_ConstACocomo = new bool[] {false} ;
         T00296_A1232Projeto_Incremental = new bool[] {false} ;
         T00296_n1232Projeto_Incremental = new bool[] {false} ;
         T00296_A2144Projeto_Identificador = new String[] {""} ;
         T00296_n2144Projeto_Identificador = new bool[] {false} ;
         T00296_A2145Projeto_Objetivo = new String[] {""} ;
         T00296_n2145Projeto_Objetivo = new bool[] {false} ;
         T00296_A2146Projeto_DTInicio = new DateTime[] {DateTime.MinValue} ;
         T00296_n2146Projeto_DTInicio = new bool[] {false} ;
         T00296_A2147Projeto_DTFim = new DateTime[] {DateTime.MinValue} ;
         T00296_n2147Projeto_DTFim = new bool[] {false} ;
         T00296_A2148Projeto_PrazoPrevisto = new short[1] ;
         T00296_n2148Projeto_PrazoPrevisto = new bool[] {false} ;
         T00296_A2149Projeto_EsforcoPrevisto = new short[1] ;
         T00296_n2149Projeto_EsforcoPrevisto = new bool[] {false} ;
         T00296_A2150Projeto_CustoPrevisto = new decimal[1] ;
         T00296_n2150Projeto_CustoPrevisto = new bool[] {false} ;
         T00296_A2170Projeto_AnexoSequecial = new short[1] ;
         T00296_n2170Projeto_AnexoSequecial = new bool[] {false} ;
         T00296_A983Projeto_TipoProjetoCod = new int[1] ;
         T00296_n983Projeto_TipoProjetoCod = new bool[] {false} ;
         T00296_A2151Projeto_AreaTrabalhoCodigo = new int[1] ;
         T00296_n2151Projeto_AreaTrabalhoCodigo = new bool[] {false} ;
         T00296_A2153Projeto_GpoObjCtrlCodigo = new int[1] ;
         T00296_n2153Projeto_GpoObjCtrlCodigo = new bool[] {false} ;
         T00296_A2155Projeto_SistemaCodigo = new int[1] ;
         T00296_n2155Projeto_SistemaCodigo = new bool[] {false} ;
         T00296_A2158Projeto_ModuloCodigo = new int[1] ;
         T00296_n2158Projeto_ModuloCodigo = new bool[] {false} ;
         T002924_A648Projeto_Codigo = new int[1] ;
         T002925_A648Projeto_Codigo = new int[1] ;
         T00295_A648Projeto_Codigo = new int[1] ;
         T00295_A649Projeto_Nome = new String[] {""} ;
         T00295_A650Projeto_Sigla = new String[] {""} ;
         T00295_A652Projeto_TecnicaContagem = new String[] {""} ;
         T00295_A1540Projeto_Introducao = new String[] {""} ;
         T00295_n1540Projeto_Introducao = new bool[] {false} ;
         T00295_A653Projeto_Escopo = new String[] {""} ;
         T00295_A1541Projeto_Previsao = new DateTime[] {DateTime.MinValue} ;
         T00295_n1541Projeto_Previsao = new bool[] {false} ;
         T00295_A1542Projeto_GerenteCod = new int[1] ;
         T00295_n1542Projeto_GerenteCod = new bool[] {false} ;
         T00295_A1543Projeto_ServicoCod = new int[1] ;
         T00295_n1543Projeto_ServicoCod = new bool[] {false} ;
         T00295_A658Projeto_Status = new String[] {""} ;
         T00295_A985Projeto_FatorEscala = new decimal[1] ;
         T00295_n985Projeto_FatorEscala = new bool[] {false} ;
         T00295_A989Projeto_FatorMultiplicador = new decimal[1] ;
         T00295_n989Projeto_FatorMultiplicador = new bool[] {false} ;
         T00295_A990Projeto_ConstACocomo = new decimal[1] ;
         T00295_n990Projeto_ConstACocomo = new bool[] {false} ;
         T00295_A1232Projeto_Incremental = new bool[] {false} ;
         T00295_n1232Projeto_Incremental = new bool[] {false} ;
         T00295_A2144Projeto_Identificador = new String[] {""} ;
         T00295_n2144Projeto_Identificador = new bool[] {false} ;
         T00295_A2145Projeto_Objetivo = new String[] {""} ;
         T00295_n2145Projeto_Objetivo = new bool[] {false} ;
         T00295_A2146Projeto_DTInicio = new DateTime[] {DateTime.MinValue} ;
         T00295_n2146Projeto_DTInicio = new bool[] {false} ;
         T00295_A2147Projeto_DTFim = new DateTime[] {DateTime.MinValue} ;
         T00295_n2147Projeto_DTFim = new bool[] {false} ;
         T00295_A2148Projeto_PrazoPrevisto = new short[1] ;
         T00295_n2148Projeto_PrazoPrevisto = new bool[] {false} ;
         T00295_A2149Projeto_EsforcoPrevisto = new short[1] ;
         T00295_n2149Projeto_EsforcoPrevisto = new bool[] {false} ;
         T00295_A2150Projeto_CustoPrevisto = new decimal[1] ;
         T00295_n2150Projeto_CustoPrevisto = new bool[] {false} ;
         T00295_A2170Projeto_AnexoSequecial = new short[1] ;
         T00295_n2170Projeto_AnexoSequecial = new bool[] {false} ;
         T00295_A983Projeto_TipoProjetoCod = new int[1] ;
         T00295_n983Projeto_TipoProjetoCod = new bool[] {false} ;
         T00295_A2151Projeto_AreaTrabalhoCodigo = new int[1] ;
         T00295_n2151Projeto_AreaTrabalhoCodigo = new bool[] {false} ;
         T00295_A2153Projeto_GpoObjCtrlCodigo = new int[1] ;
         T00295_n2153Projeto_GpoObjCtrlCodigo = new bool[] {false} ;
         T00295_A2155Projeto_SistemaCodigo = new int[1] ;
         T00295_n2155Projeto_SistemaCodigo = new bool[] {false} ;
         T00295_A2158Projeto_ModuloCodigo = new int[1] ;
         T00295_n2158Projeto_ModuloCodigo = new bool[] {false} ;
         T002926_A648Projeto_Codigo = new int[1] ;
         T002929_A2154Projeto_GpoObjCtrlNome = new String[] {""} ;
         T002929_n2154Projeto_GpoObjCtrlNome = new bool[] {false} ;
         T002930_A2156Projeto_SistemaNome = new String[] {""} ;
         T002930_n2156Projeto_SistemaNome = new bool[] {false} ;
         T002930_A2157Projeto_SistemaSigla = new String[] {""} ;
         T002930_n2157Projeto_SistemaSigla = new bool[] {false} ;
         T002932_A655Projeto_Custo = new decimal[1] ;
         T002932_A656Projeto_Prazo = new short[1] ;
         T002932_A657Projeto_Esforco = new short[1] ;
         T002932_n657Projeto_Esforco = new bool[] {false} ;
         T002933_A2159Projeto_ModuloNome = new String[] {""} ;
         T002933_n2159Projeto_ModuloNome = new bool[] {false} ;
         T002933_A2160Projeto_ModuloSigla = new String[] {""} ;
         T002933_n2160Projeto_ModuloSigla = new bool[] {false} ;
         T002934_A2152Projeto_AreaTrabalhoDescricao = new String[] {""} ;
         T002934_n2152Projeto_AreaTrabalhoDescricao = new bool[] {false} ;
         T002935_A961VariavelCocomo_AreaTrabalhoCod = new int[1] ;
         T002935_A962VariavelCocomo_Sigla = new String[] {""} ;
         T002935_A964VariavelCocomo_Tipo = new String[] {""} ;
         T002935_A992VariavelCocomo_Sequencial = new short[1] ;
         T002936_A192Contagem_Codigo = new int[1] ;
         T002937_A736ProjetoMelhoria_Codigo = new int[1] ;
         T002938_A1685Proposta_Codigo = new int[1] ;
         T002940_A648Projeto_Codigo = new int[1] ;
         Z2164ProjetoAnexos_Descricao = "";
         Z2165ProjetoAnexos_Arquivo = "";
         Z2168ProjetoAnexos_Link = "";
         Z2167ProjetoAnexos_ArquivoTipo = "";
         Z2166ProjetoAnexos_ArquivoNome = "";
         Z646TipoDocumento_Nome = "";
         T00294_A646TipoDocumento_Nome = new String[] {""} ;
         T002941_A648Projeto_Codigo = new int[1] ;
         T002941_A2163ProjetoAnexos_Codigo = new int[1] ;
         T002941_A2164ProjetoAnexos_Descricao = new String[] {""} ;
         T002941_A2168ProjetoAnexos_Link = new String[] {""} ;
         T002941_n2168ProjetoAnexos_Link = new bool[] {false} ;
         T002941_A2169ProjetoAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         T002941_n2169ProjetoAnexos_Data = new bool[] {false} ;
         T002941_A646TipoDocumento_Nome = new String[] {""} ;
         T002941_A2167ProjetoAnexos_ArquivoTipo = new String[] {""} ;
         T002941_n2167ProjetoAnexos_ArquivoTipo = new bool[] {false} ;
         T002941_A2166ProjetoAnexos_ArquivoNome = new String[] {""} ;
         T002941_n2166ProjetoAnexos_ArquivoNome = new bool[] {false} ;
         T002941_A645TipoDocumento_Codigo = new int[1] ;
         T002941_n645TipoDocumento_Codigo = new bool[] {false} ;
         T002941_A2165ProjetoAnexos_Arquivo = new String[] {""} ;
         T002941_n2165ProjetoAnexos_Arquivo = new bool[] {false} ;
         T002942_A646TipoDocumento_Nome = new String[] {""} ;
         T002943_A648Projeto_Codigo = new int[1] ;
         T002943_A2163ProjetoAnexos_Codigo = new int[1] ;
         T00293_A648Projeto_Codigo = new int[1] ;
         T00293_A2163ProjetoAnexos_Codigo = new int[1] ;
         T00293_A2164ProjetoAnexos_Descricao = new String[] {""} ;
         T00293_A2168ProjetoAnexos_Link = new String[] {""} ;
         T00293_n2168ProjetoAnexos_Link = new bool[] {false} ;
         T00293_A2169ProjetoAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         T00293_n2169ProjetoAnexos_Data = new bool[] {false} ;
         T00293_A2167ProjetoAnexos_ArquivoTipo = new String[] {""} ;
         T00293_n2167ProjetoAnexos_ArquivoTipo = new bool[] {false} ;
         T00293_A2166ProjetoAnexos_ArquivoNome = new String[] {""} ;
         T00293_n2166ProjetoAnexos_ArquivoNome = new bool[] {false} ;
         T00293_A645TipoDocumento_Codigo = new int[1] ;
         T00293_n645TipoDocumento_Codigo = new bool[] {false} ;
         T00293_A2165ProjetoAnexos_Arquivo = new String[] {""} ;
         T00293_n2165ProjetoAnexos_Arquivo = new bool[] {false} ;
         T00292_A648Projeto_Codigo = new int[1] ;
         T00292_A2163ProjetoAnexos_Codigo = new int[1] ;
         T00292_A2164ProjetoAnexos_Descricao = new String[] {""} ;
         T00292_A2168ProjetoAnexos_Link = new String[] {""} ;
         T00292_n2168ProjetoAnexos_Link = new bool[] {false} ;
         T00292_A2169ProjetoAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         T00292_n2169ProjetoAnexos_Data = new bool[] {false} ;
         T00292_A2167ProjetoAnexos_ArquivoTipo = new String[] {""} ;
         T00292_n2167ProjetoAnexos_ArquivoTipo = new bool[] {false} ;
         T00292_A2166ProjetoAnexos_ArquivoNome = new String[] {""} ;
         T00292_n2166ProjetoAnexos_ArquivoNome = new bool[] {false} ;
         T00292_A645TipoDocumento_Codigo = new int[1] ;
         T00292_n645TipoDocumento_Codigo = new bool[] {false} ;
         T00292_A2165ProjetoAnexos_Arquivo = new String[] {""} ;
         T00292_n2165ProjetoAnexos_Arquivo = new bool[] {false} ;
         T002948_A646TipoDocumento_Nome = new String[] {""} ;
         T002949_A648Projeto_Codigo = new int[1] ;
         T002949_A2163ProjetoAnexos_Codigo = new int[1] ;
         GridanexosRow = new GXWebRow();
         subGridanexos_Linesclass = "";
         ROClassString = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         GXCCtlgxBlob = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         i658Projeto_Status = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T002950_A983Projeto_TipoProjetoCod = new int[1] ;
         T002950_n983Projeto_TipoProjetoCod = new bool[] {false} ;
         T002950_A980TipoProjeto_Nome = new String[] {""} ;
         T002950_n980TipoProjeto_Nome = new bool[] {false} ;
         T002951_A2153Projeto_GpoObjCtrlCodigo = new int[1] ;
         T002951_n2153Projeto_GpoObjCtrlCodigo = new bool[] {false} ;
         T002951_A2154Projeto_GpoObjCtrlNome = new String[] {""} ;
         T002951_n2154Projeto_GpoObjCtrlNome = new bool[] {false} ;
         T002952_A127Sistema_Codigo = new int[1] ;
         T002952_n127Sistema_Codigo = new bool[] {false} ;
         T002952_A416Sistema_Nome = new String[] {""} ;
         T002952_A135Sistema_AreaTrabalhoCod = new int[1] ;
         T002952_A2161Sistema_GpoObjCtrlCod = new int[1] ;
         T002952_n2161Sistema_GpoObjCtrlCod = new bool[] {false} ;
         T002953_A2158Projeto_ModuloCodigo = new int[1] ;
         T002953_n2158Projeto_ModuloCodigo = new bool[] {false} ;
         T002953_A2160Projeto_ModuloSigla = new String[] {""} ;
         T002953_n2160Projeto_ModuloSigla = new bool[] {false} ;
         T002953_A127Sistema_Codigo = new int[1] ;
         T002953_n127Sistema_Codigo = new bool[] {false} ;
         T002954_A645TipoDocumento_Codigo = new int[1] ;
         T002954_n645TipoDocumento_Codigo = new bool[] {false} ;
         T002954_A646TipoDocumento_Nome = new String[] {""} ;
         T002954_A647TipoDocumento_Ativo = new bool[] {false} ;
         isValidOutput = new GxUnknownObjectCollection();
         T002955_A983Projeto_TipoProjetoCod = new int[1] ;
         T002955_n983Projeto_TipoProjetoCod = new bool[] {false} ;
         T002956_A646TipoDocumento_Nome = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.projeto__default(),
            new Object[][] {
                new Object[] {
               T00292_A648Projeto_Codigo, T00292_A2163ProjetoAnexos_Codigo, T00292_A2164ProjetoAnexos_Descricao, T00292_A2168ProjetoAnexos_Link, T00292_n2168ProjetoAnexos_Link, T00292_A2169ProjetoAnexos_Data, T00292_n2169ProjetoAnexos_Data, T00292_A2167ProjetoAnexos_ArquivoTipo, T00292_n2167ProjetoAnexos_ArquivoTipo, T00292_A2166ProjetoAnexos_ArquivoNome,
               T00292_n2166ProjetoAnexos_ArquivoNome, T00292_A645TipoDocumento_Codigo, T00292_n645TipoDocumento_Codigo, T00292_A2165ProjetoAnexos_Arquivo, T00292_n2165ProjetoAnexos_Arquivo
               }
               , new Object[] {
               T00293_A648Projeto_Codigo, T00293_A2163ProjetoAnexos_Codigo, T00293_A2164ProjetoAnexos_Descricao, T00293_A2168ProjetoAnexos_Link, T00293_n2168ProjetoAnexos_Link, T00293_A2169ProjetoAnexos_Data, T00293_n2169ProjetoAnexos_Data, T00293_A2167ProjetoAnexos_ArquivoTipo, T00293_n2167ProjetoAnexos_ArquivoTipo, T00293_A2166ProjetoAnexos_ArquivoNome,
               T00293_n2166ProjetoAnexos_ArquivoNome, T00293_A645TipoDocumento_Codigo, T00293_n645TipoDocumento_Codigo, T00293_A2165ProjetoAnexos_Arquivo, T00293_n2165ProjetoAnexos_Arquivo
               }
               , new Object[] {
               T00294_A646TipoDocumento_Nome
               }
               , new Object[] {
               T00295_A648Projeto_Codigo, T00295_A649Projeto_Nome, T00295_A650Projeto_Sigla, T00295_A652Projeto_TecnicaContagem, T00295_A1540Projeto_Introducao, T00295_n1540Projeto_Introducao, T00295_A653Projeto_Escopo, T00295_A1541Projeto_Previsao, T00295_n1541Projeto_Previsao, T00295_A1542Projeto_GerenteCod,
               T00295_n1542Projeto_GerenteCod, T00295_A1543Projeto_ServicoCod, T00295_n1543Projeto_ServicoCod, T00295_A658Projeto_Status, T00295_A985Projeto_FatorEscala, T00295_n985Projeto_FatorEscala, T00295_A989Projeto_FatorMultiplicador, T00295_n989Projeto_FatorMultiplicador, T00295_A990Projeto_ConstACocomo, T00295_n990Projeto_ConstACocomo,
               T00295_A1232Projeto_Incremental, T00295_n1232Projeto_Incremental, T00295_A2144Projeto_Identificador, T00295_n2144Projeto_Identificador, T00295_A2145Projeto_Objetivo, T00295_n2145Projeto_Objetivo, T00295_A2146Projeto_DTInicio, T00295_n2146Projeto_DTInicio, T00295_A2147Projeto_DTFim, T00295_n2147Projeto_DTFim,
               T00295_A2148Projeto_PrazoPrevisto, T00295_n2148Projeto_PrazoPrevisto, T00295_A2149Projeto_EsforcoPrevisto, T00295_n2149Projeto_EsforcoPrevisto, T00295_A2150Projeto_CustoPrevisto, T00295_n2150Projeto_CustoPrevisto, T00295_A2170Projeto_AnexoSequecial, T00295_n2170Projeto_AnexoSequecial, T00295_A983Projeto_TipoProjetoCod, T00295_n983Projeto_TipoProjetoCod,
               T00295_A2151Projeto_AreaTrabalhoCodigo, T00295_n2151Projeto_AreaTrabalhoCodigo, T00295_A2153Projeto_GpoObjCtrlCodigo, T00295_n2153Projeto_GpoObjCtrlCodigo, T00295_A2155Projeto_SistemaCodigo, T00295_n2155Projeto_SistemaCodigo, T00295_A2158Projeto_ModuloCodigo, T00295_n2158Projeto_ModuloCodigo
               }
               , new Object[] {
               T00296_A648Projeto_Codigo, T00296_A649Projeto_Nome, T00296_A650Projeto_Sigla, T00296_A652Projeto_TecnicaContagem, T00296_A1540Projeto_Introducao, T00296_n1540Projeto_Introducao, T00296_A653Projeto_Escopo, T00296_A1541Projeto_Previsao, T00296_n1541Projeto_Previsao, T00296_A1542Projeto_GerenteCod,
               T00296_n1542Projeto_GerenteCod, T00296_A1543Projeto_ServicoCod, T00296_n1543Projeto_ServicoCod, T00296_A658Projeto_Status, T00296_A985Projeto_FatorEscala, T00296_n985Projeto_FatorEscala, T00296_A989Projeto_FatorMultiplicador, T00296_n989Projeto_FatorMultiplicador, T00296_A990Projeto_ConstACocomo, T00296_n990Projeto_ConstACocomo,
               T00296_A1232Projeto_Incremental, T00296_n1232Projeto_Incremental, T00296_A2144Projeto_Identificador, T00296_n2144Projeto_Identificador, T00296_A2145Projeto_Objetivo, T00296_n2145Projeto_Objetivo, T00296_A2146Projeto_DTInicio, T00296_n2146Projeto_DTInicio, T00296_A2147Projeto_DTFim, T00296_n2147Projeto_DTFim,
               T00296_A2148Projeto_PrazoPrevisto, T00296_n2148Projeto_PrazoPrevisto, T00296_A2149Projeto_EsforcoPrevisto, T00296_n2149Projeto_EsforcoPrevisto, T00296_A2150Projeto_CustoPrevisto, T00296_n2150Projeto_CustoPrevisto, T00296_A2170Projeto_AnexoSequecial, T00296_n2170Projeto_AnexoSequecial, T00296_A983Projeto_TipoProjetoCod, T00296_n983Projeto_TipoProjetoCod,
               T00296_A2151Projeto_AreaTrabalhoCodigo, T00296_n2151Projeto_AreaTrabalhoCodigo, T00296_A2153Projeto_GpoObjCtrlCodigo, T00296_n2153Projeto_GpoObjCtrlCodigo, T00296_A2155Projeto_SistemaCodigo, T00296_n2155Projeto_SistemaCodigo, T00296_A2158Projeto_ModuloCodigo, T00296_n2158Projeto_ModuloCodigo
               }
               , new Object[] {
               T00297_A983Projeto_TipoProjetoCod
               }
               , new Object[] {
               T00298_A2152Projeto_AreaTrabalhoDescricao, T00298_n2152Projeto_AreaTrabalhoDescricao
               }
               , new Object[] {
               T00299_A2154Projeto_GpoObjCtrlNome, T00299_n2154Projeto_GpoObjCtrlNome
               }
               , new Object[] {
               T002910_A2156Projeto_SistemaNome, T002910_n2156Projeto_SistemaNome, T002910_A2157Projeto_SistemaSigla, T002910_n2157Projeto_SistemaSigla
               }
               , new Object[] {
               T002911_A2159Projeto_ModuloNome, T002911_n2159Projeto_ModuloNome, T002911_A2160Projeto_ModuloSigla, T002911_n2160Projeto_ModuloSigla
               }
               , new Object[] {
               T002913_A655Projeto_Custo, T002913_A656Projeto_Prazo, T002913_A657Projeto_Esforco, T002913_n657Projeto_Esforco
               }
               , new Object[] {
               T002915_A648Projeto_Codigo, T002915_A649Projeto_Nome, T002915_A650Projeto_Sigla, T002915_A652Projeto_TecnicaContagem, T002915_A1540Projeto_Introducao, T002915_n1540Projeto_Introducao, T002915_A653Projeto_Escopo, T002915_A1541Projeto_Previsao, T002915_n1541Projeto_Previsao, T002915_A1542Projeto_GerenteCod,
               T002915_n1542Projeto_GerenteCod, T002915_A1543Projeto_ServicoCod, T002915_n1543Projeto_ServicoCod, T002915_A658Projeto_Status, T002915_A985Projeto_FatorEscala, T002915_n985Projeto_FatorEscala, T002915_A989Projeto_FatorMultiplicador, T002915_n989Projeto_FatorMultiplicador, T002915_A990Projeto_ConstACocomo, T002915_n990Projeto_ConstACocomo,
               T002915_A1232Projeto_Incremental, T002915_n1232Projeto_Incremental, T002915_A2144Projeto_Identificador, T002915_n2144Projeto_Identificador, T002915_A2145Projeto_Objetivo, T002915_n2145Projeto_Objetivo, T002915_A2146Projeto_DTInicio, T002915_n2146Projeto_DTInicio, T002915_A2147Projeto_DTFim, T002915_n2147Projeto_DTFim,
               T002915_A2148Projeto_PrazoPrevisto, T002915_n2148Projeto_PrazoPrevisto, T002915_A2149Projeto_EsforcoPrevisto, T002915_n2149Projeto_EsforcoPrevisto, T002915_A2150Projeto_CustoPrevisto, T002915_n2150Projeto_CustoPrevisto, T002915_A2152Projeto_AreaTrabalhoDescricao, T002915_n2152Projeto_AreaTrabalhoDescricao, T002915_A2154Projeto_GpoObjCtrlNome, T002915_n2154Projeto_GpoObjCtrlNome,
               T002915_A2156Projeto_SistemaNome, T002915_n2156Projeto_SistemaNome, T002915_A2157Projeto_SistemaSigla, T002915_n2157Projeto_SistemaSigla, T002915_A2159Projeto_ModuloNome, T002915_n2159Projeto_ModuloNome, T002915_A2160Projeto_ModuloSigla, T002915_n2160Projeto_ModuloSigla, T002915_A2170Projeto_AnexoSequecial, T002915_n2170Projeto_AnexoSequecial,
               T002915_A983Projeto_TipoProjetoCod, T002915_n983Projeto_TipoProjetoCod, T002915_A2151Projeto_AreaTrabalhoCodigo, T002915_n2151Projeto_AreaTrabalhoCodigo, T002915_A2153Projeto_GpoObjCtrlCodigo, T002915_n2153Projeto_GpoObjCtrlCodigo, T002915_A2155Projeto_SistemaCodigo, T002915_n2155Projeto_SistemaCodigo, T002915_A2158Projeto_ModuloCodigo, T002915_n2158Projeto_ModuloCodigo,
               T002915_A655Projeto_Custo, T002915_A656Projeto_Prazo, T002915_A657Projeto_Esforco, T002915_n657Projeto_Esforco
               }
               , new Object[] {
               T002916_A983Projeto_TipoProjetoCod
               }
               , new Object[] {
               T002917_A2154Projeto_GpoObjCtrlNome, T002917_n2154Projeto_GpoObjCtrlNome
               }
               , new Object[] {
               T002918_A2156Projeto_SistemaNome, T002918_n2156Projeto_SistemaNome, T002918_A2157Projeto_SistemaSigla, T002918_n2157Projeto_SistemaSigla
               }
               , new Object[] {
               T002920_A655Projeto_Custo, T002920_A656Projeto_Prazo, T002920_A657Projeto_Esforco, T002920_n657Projeto_Esforco
               }
               , new Object[] {
               T002921_A2159Projeto_ModuloNome, T002921_n2159Projeto_ModuloNome, T002921_A2160Projeto_ModuloSigla, T002921_n2160Projeto_ModuloSigla
               }
               , new Object[] {
               T002922_A2152Projeto_AreaTrabalhoDescricao, T002922_n2152Projeto_AreaTrabalhoDescricao
               }
               , new Object[] {
               T002923_A648Projeto_Codigo
               }
               , new Object[] {
               T002924_A648Projeto_Codigo
               }
               , new Object[] {
               T002925_A648Projeto_Codigo
               }
               , new Object[] {
               T002926_A648Projeto_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002929_A2154Projeto_GpoObjCtrlNome, T002929_n2154Projeto_GpoObjCtrlNome
               }
               , new Object[] {
               T002930_A2156Projeto_SistemaNome, T002930_n2156Projeto_SistemaNome, T002930_A2157Projeto_SistemaSigla, T002930_n2157Projeto_SistemaSigla
               }
               , new Object[] {
               T002932_A655Projeto_Custo, T002932_A656Projeto_Prazo, T002932_A657Projeto_Esforco, T002932_n657Projeto_Esforco
               }
               , new Object[] {
               T002933_A2159Projeto_ModuloNome, T002933_n2159Projeto_ModuloNome, T002933_A2160Projeto_ModuloSigla, T002933_n2160Projeto_ModuloSigla
               }
               , new Object[] {
               T002934_A2152Projeto_AreaTrabalhoDescricao, T002934_n2152Projeto_AreaTrabalhoDescricao
               }
               , new Object[] {
               T002935_A961VariavelCocomo_AreaTrabalhoCod, T002935_A962VariavelCocomo_Sigla, T002935_A964VariavelCocomo_Tipo, T002935_A992VariavelCocomo_Sequencial
               }
               , new Object[] {
               T002936_A192Contagem_Codigo
               }
               , new Object[] {
               T002937_A736ProjetoMelhoria_Codigo
               }
               , new Object[] {
               T002938_A1685Proposta_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               T002940_A648Projeto_Codigo
               }
               , new Object[] {
               T002941_A648Projeto_Codigo, T002941_A2163ProjetoAnexos_Codigo, T002941_A2164ProjetoAnexos_Descricao, T002941_A2168ProjetoAnexos_Link, T002941_n2168ProjetoAnexos_Link, T002941_A2169ProjetoAnexos_Data, T002941_n2169ProjetoAnexos_Data, T002941_A646TipoDocumento_Nome, T002941_A2167ProjetoAnexos_ArquivoTipo, T002941_n2167ProjetoAnexos_ArquivoTipo,
               T002941_A2166ProjetoAnexos_ArquivoNome, T002941_n2166ProjetoAnexos_ArquivoNome, T002941_A645TipoDocumento_Codigo, T002941_n645TipoDocumento_Codigo, T002941_A2165ProjetoAnexos_Arquivo, T002941_n2165ProjetoAnexos_Arquivo
               }
               , new Object[] {
               T002942_A646TipoDocumento_Nome
               }
               , new Object[] {
               T002943_A648Projeto_Codigo, T002943_A2163ProjetoAnexos_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002948_A646TipoDocumento_Nome
               }
               , new Object[] {
               T002949_A648Projeto_Codigo, T002949_A2163ProjetoAnexos_Codigo
               }
               , new Object[] {
               T002950_A983Projeto_TipoProjetoCod, T002950_A980TipoProjeto_Nome, T002950_n980TipoProjeto_Nome
               }
               , new Object[] {
               T002951_A2153Projeto_GpoObjCtrlCodigo, T002951_A2154Projeto_GpoObjCtrlNome, T002951_n2154Projeto_GpoObjCtrlNome
               }
               , new Object[] {
               T002952_A127Sistema_Codigo, T002952_A416Sistema_Nome, T002952_A135Sistema_AreaTrabalhoCod, T002952_A2161Sistema_GpoObjCtrlCod, T002952_n2161Sistema_GpoObjCtrlCod
               }
               , new Object[] {
               T002953_A2158Projeto_ModuloCodigo, T002953_A2160Projeto_ModuloSigla, T002953_n2160Projeto_ModuloSigla, T002953_A127Sistema_Codigo, T002953_n127Sistema_Codigo
               }
               , new Object[] {
               T002954_A645TipoDocumento_Codigo, T002954_A646TipoDocumento_Nome, T002954_A647TipoDocumento_Ativo
               }
               , new Object[] {
               T002955_A983Projeto_TipoProjetoCod
               }
               , new Object[] {
               T002956_A646TipoDocumento_Nome
               }
            }
         );
         Z658Projeto_Status = "A";
         A658Projeto_Status = "A";
         i658Projeto_Status = "A";
         AV24Pgmname = "Projeto";
         Z2151Projeto_AreaTrabalhoCodigo = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         n2151Projeto_AreaTrabalhoCodigo = false;
         N2151Projeto_AreaTrabalhoCodigo = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         n2151Projeto_AreaTrabalhoCodigo = false;
         i2151Projeto_AreaTrabalhoCodigo = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         n2151Projeto_AreaTrabalhoCodigo = false;
         A2151Projeto_AreaTrabalhoCodigo = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         n2151Projeto_AreaTrabalhoCodigo = false;
      }

      private short Z2148Projeto_PrazoPrevisto ;
      private short Z2149Projeto_EsforcoPrevisto ;
      private short Z2170Projeto_AnexoSequecial ;
      private short O2170Projeto_AnexoSequecial ;
      private short nRC_GXsfl_154 ;
      private short nGXsfl_154_idx=1 ;
      private short nRcdDeleted_235 ;
      private short nRcdExists_235 ;
      private short nIsMod_235 ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short subGridanexos_Backcolorstyle ;
      private short subGridanexos_Allowselection ;
      private short subGridanexos_Allowhovering ;
      private short subGridanexos_Allowcollapsing ;
      private short subGridanexos_Collapsed ;
      private short nBlankRcdCount235 ;
      private short RcdFound235 ;
      private short B2170Projeto_AnexoSequecial ;
      private short A2170Projeto_AnexoSequecial ;
      private short nBlankRcdUsr235 ;
      private short A2148Projeto_PrazoPrevisto ;
      private short A2149Projeto_EsforcoPrevisto ;
      private short Gx_BScreen ;
      private short A656Projeto_Prazo ;
      private short A657Projeto_Esforco ;
      private short RcdFound86 ;
      private short s2170Projeto_AnexoSequecial ;
      private short GX_JID ;
      private short Z656Projeto_Prazo ;
      private short Z657Projeto_Esforco ;
      private short nGXsfl_154_Refreshing=0 ;
      private short subGridanexos_Backstyle ;
      private short gxajaxcallmode ;
      private short i2170Projeto_AnexoSequecial ;
      private short wbTemp ;
      private int wcpOAV7Projeto_Codigo ;
      private int Z648Projeto_Codigo ;
      private int Z1542Projeto_GerenteCod ;
      private int Z1543Projeto_ServicoCod ;
      private int Z983Projeto_TipoProjetoCod ;
      private int Z2151Projeto_AreaTrabalhoCodigo ;
      private int Z2153Projeto_GpoObjCtrlCodigo ;
      private int Z2155Projeto_SistemaCodigo ;
      private int Z2158Projeto_ModuloCodigo ;
      private int N983Projeto_TipoProjetoCod ;
      private int N2151Projeto_AreaTrabalhoCodigo ;
      private int N2153Projeto_GpoObjCtrlCodigo ;
      private int N2155Projeto_SistemaCodigo ;
      private int N2158Projeto_ModuloCodigo ;
      private int Z2163ProjetoAnexos_Codigo ;
      private int Z645TipoDocumento_Codigo ;
      private int A2153Projeto_GpoObjCtrlCodigo ;
      private int A648Projeto_Codigo ;
      private int A983Projeto_TipoProjetoCod ;
      private int A2155Projeto_SistemaCodigo ;
      private int A2158Projeto_ModuloCodigo ;
      private int A2151Projeto_AreaTrabalhoCodigo ;
      private int A645TipoDocumento_Codigo ;
      private int AV7Projeto_Codigo ;
      private int trnEnded ;
      private int edtProjeto_Codigo_Enabled ;
      private int edtProjeto_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int A2163ProjetoAnexos_Codigo ;
      private int edtProjetoAnexos_Codigo_Enabled ;
      private int edtProjetoAnexos_Arquivo_Enabled ;
      private int edtProjetoAnexos_ArquivoNome_Enabled ;
      private int edtTipoDocumento_Nome_Enabled ;
      private int edtProjetoAnexos_ArquivoTipo_Enabled ;
      private int edtProjetoAnexos_Data_Enabled ;
      private int subGridanexos_Selectioncolor ;
      private int subGridanexos_Hoveringcolor ;
      private int subGridanexos_Rows ;
      private int fRowAdded ;
      private int imgBntanexos_Visible ;
      private int edtProjeto_FatorEscala_Enabled ;
      private int bttBtnfatorescala_Visible ;
      private int edtProjeto_FatorMultiplicador_Enabled ;
      private int bttBtnfatormultiplicador_Visible ;
      private int edtProjeto_ConstACocomo_Enabled ;
      private int edtProjeto_DTInicio_Enabled ;
      private int edtProjeto_DTFim_Enabled ;
      private int edtProjeto_PrazoPrevisto_Enabled ;
      private int edtProjeto_EsforcoPrevisto_Enabled ;
      private int edtProjeto_CustoPrevisto_Enabled ;
      private int imgBntequipe_Visible ;
      private int edtProjeto_Nome_Enabled ;
      private int edtProjeto_Objetivo_Enabled ;
      private int edtProjeto_Escopo_Enabled ;
      private int edtProjeto_Sigla_Enabled ;
      private int edtProjeto_Identificador_Enabled ;
      private int A1542Projeto_GerenteCod ;
      private int A1543Projeto_ServicoCod ;
      private int A740Projeto_SistemaCod ;
      private int AV12Insert_Projeto_TipoProjetoCod ;
      private int AV19Insert_Projeto_AreaTrabalhoCodigo ;
      private int AV20Insert_Projeto_GpoObjCtrlCodigo ;
      private int AV21Insert_Projeto_SistemaCodigo ;
      private int AV22Insert_Projeto_ModuloCodigo ;
      private int Gxuitabspanel_tabmain_Selectedtabindex ;
      private int AV25GXV1 ;
      private int subGridanexos_Backcolor ;
      private int subGridanexos_Allbackcolor ;
      private int defedtProjetoAnexos_Data_Enabled ;
      private int defedtProjetoAnexos_ArquivoTipo_Enabled ;
      private int defedtTipoDocumento_Nome_Enabled ;
      private int defedtProjetoAnexos_ArquivoNome_Enabled ;
      private int defedtProjetoAnexos_Codigo_Enabled ;
      private int i2151Projeto_AreaTrabalhoCodigo ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private int A127Sistema_Codigo ;
      private int GXt_int1 ;
      private long GRIDANEXOS_nFirstRecordOnPage ;
      private decimal Z985Projeto_FatorEscala ;
      private decimal Z989Projeto_FatorMultiplicador ;
      private decimal Z990Projeto_ConstACocomo ;
      private decimal Z2150Projeto_CustoPrevisto ;
      private decimal A985Projeto_FatorEscala ;
      private decimal A989Projeto_FatorMultiplicador ;
      private decimal A990Projeto_ConstACocomo ;
      private decimal A2150Projeto_CustoPrevisto ;
      private decimal A655Projeto_Custo ;
      private decimal AV16FatorEscala ;
      private decimal AV15FatorMultiplicador ;
      private decimal Z655Projeto_Custo ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z649Projeto_Nome ;
      private String Z650Projeto_Sigla ;
      private String Z652Projeto_TecnicaContagem ;
      private String Z658Projeto_Status ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_154_idx="0001" ;
      private String Gx_mode ;
      private String GXKey ;
      private String A658Projeto_Status ;
      private String chkProjeto_Incremental_Internalname ;
      private String GXCCtl ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtProjeto_Sigla_Internalname ;
      private String edtProjeto_Codigo_Internalname ;
      private String edtProjeto_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String tblTableanexos_Internalname ;
      private String A2166ProjetoAnexos_ArquivoNome ;
      private String A646TipoDocumento_Nome ;
      private String A2167ProjetoAnexos_ArquivoTipo ;
      private String sMode235 ;
      private String edtProjetoAnexos_Codigo_Internalname ;
      private String edtProjetoAnexos_Arquivo_Internalname ;
      private String edtProjetoAnexos_ArquivoNome_Internalname ;
      private String edtTipoDocumento_Nome_Internalname ;
      private String dynTipoDocumento_Codigo_Internalname ;
      private String edtProjetoAnexos_ArquivoTipo_Internalname ;
      private String edtProjetoAnexos_Data_Internalname ;
      private String tblTableactions2_Internalname ;
      private String imgBntanexos_Internalname ;
      private String imgBntanexos_Jsonclick ;
      private String tblTableestimativa_Internalname ;
      private String tblTableestimativa2_Internalname ;
      private String lblTextblockprojeto_fatorescala_Internalname ;
      private String lblTextblockprojeto_fatorescala_Jsonclick ;
      private String lblTextblockprojeto_incremental_Internalname ;
      private String lblTextblockprojeto_incremental_Jsonclick ;
      private String tblTablemergedprojeto_fatorescala_Internalname ;
      private String edtProjeto_FatorEscala_Internalname ;
      private String edtProjeto_FatorEscala_Jsonclick ;
      private String bttBtnfatorescala_Internalname ;
      private String bttBtnfatorescala_Jsonclick ;
      private String lblTextblockprojeto_fatormultiplicador_Internalname ;
      private String lblTextblockprojeto_fatormultiplicador_Jsonclick ;
      private String edtProjeto_FatorMultiplicador_Internalname ;
      private String edtProjeto_FatorMultiplicador_Jsonclick ;
      private String bttBtnfatormultiplicador_Internalname ;
      private String bttBtnfatormultiplicador_Jsonclick ;
      private String lblTextblockprojeto_constacocomo_Internalname ;
      private String lblTextblockprojeto_constacocomo_Jsonclick ;
      private String edtProjeto_ConstACocomo_Internalname ;
      private String edtProjeto_ConstACocomo_Jsonclick ;
      private String tblTableestimativa1_Internalname ;
      private String lblTextblockprojeto_dtinicio_Internalname ;
      private String lblTextblockprojeto_dtinicio_Jsonclick ;
      private String edtProjeto_DTInicio_Internalname ;
      private String edtProjeto_DTInicio_Jsonclick ;
      private String lblTextblockprojeto_dtfim_Internalname ;
      private String lblTextblockprojeto_dtfim_Jsonclick ;
      private String edtProjeto_DTFim_Internalname ;
      private String edtProjeto_DTFim_Jsonclick ;
      private String lblTextblockprojeto_prazoprevisto_Internalname ;
      private String lblTextblockprojeto_prazoprevisto_Jsonclick ;
      private String edtProjeto_PrazoPrevisto_Internalname ;
      private String edtProjeto_PrazoPrevisto_Jsonclick ;
      private String lblTextblockprojeto_esforcoprevisto_Internalname ;
      private String lblTextblockprojeto_esforcoprevisto_Jsonclick ;
      private String edtProjeto_EsforcoPrevisto_Internalname ;
      private String edtProjeto_EsforcoPrevisto_Jsonclick ;
      private String lblTextblockprojeto_custoprevisto_Internalname ;
      private String lblTextblockprojeto_custoprevisto_Jsonclick ;
      private String edtProjeto_CustoPrevisto_Internalname ;
      private String edtProjeto_CustoPrevisto_Jsonclick ;
      private String tblTableobjeto_Internalname ;
      private String lblTextblockprojeto_gpoobjctrlcodigo_Internalname ;
      private String lblTextblockprojeto_gpoobjctrlcodigo_Jsonclick ;
      private String dynProjeto_GpoObjCtrlCodigo_Internalname ;
      private String dynProjeto_GpoObjCtrlCodigo_Jsonclick ;
      private String lblTextblockprojeto_sistemacodigo_Internalname ;
      private String lblTextblockprojeto_sistemacodigo_Jsonclick ;
      private String dynProjeto_SistemaCodigo_Internalname ;
      private String dynProjeto_SistemaCodigo_Jsonclick ;
      private String lblTextblockprojeto_modulocodigo_Internalname ;
      private String lblTextblockprojeto_modulocodigo_Jsonclick ;
      private String dynProjeto_ModuloCodigo_Internalname ;
      private String dynProjeto_ModuloCodigo_Jsonclick ;
      private String tblTableequipe_Internalname ;
      private String tblTableactions3_Internalname ;
      private String imgBntequipe_Internalname ;
      private String imgBntequipe_Jsonclick ;
      private String tblTableprojeto_Internalname ;
      private String tblTableprojeto1_Internalname ;
      private String lblTextblockprojeto_sigla_Internalname ;
      private String lblTextblockprojeto_sigla_Jsonclick ;
      private String lblTextblockprojeto_nome_Internalname ;
      private String lblTextblockprojeto_nome_Jsonclick ;
      private String edtProjeto_Nome_Internalname ;
      private String A649Projeto_Nome ;
      private String edtProjeto_Nome_Jsonclick ;
      private String lblTextblockprojeto_tipoprojetocod_Internalname ;
      private String lblTextblockprojeto_tipoprojetocod_Jsonclick ;
      private String dynProjeto_TipoProjetoCod_Internalname ;
      private String dynProjeto_TipoProjetoCod_Jsonclick ;
      private String lblTextblockprojeto_objetivo_Internalname ;
      private String lblTextblockprojeto_objetivo_Jsonclick ;
      private String edtProjeto_Objetivo_Internalname ;
      private String lblTextblockprojeto_escopo_Internalname ;
      private String lblTextblockprojeto_escopo_Jsonclick ;
      private String edtProjeto_Escopo_Internalname ;
      private String lblTextblockprojeto_status_Internalname ;
      private String lblTextblockprojeto_status_Jsonclick ;
      private String cmbProjeto_Status_Internalname ;
      private String cmbProjeto_Status_Jsonclick ;
      private String tblTablemergedprojeto_sigla_Internalname ;
      private String A650Projeto_Sigla ;
      private String edtProjeto_Sigla_Jsonclick ;
      private String lblTextblockprojeto_identificador_Internalname ;
      private String lblTextblockprojeto_identificador_Jsonclick ;
      private String edtProjeto_Identificador_Internalname ;
      private String edtProjeto_Identificador_Jsonclick ;
      private String A652Projeto_TecnicaContagem ;
      private String A2154Projeto_GpoObjCtrlNome ;
      private String A2157Projeto_SistemaSigla ;
      private String A2159Projeto_ModuloNome ;
      private String A2160Projeto_ModuloSigla ;
      private String AV24Pgmname ;
      private String Gxuitabspanel_tabmain_Width ;
      private String Gxuitabspanel_tabmain_Height ;
      private String Gxuitabspanel_tabmain_Cls ;
      private String Gxuitabspanel_tabmain_Class ;
      private String Gxuitabspanel_tabmain_Activetabid ;
      private String Gxuitabspanel_tabmain_Designtimetabs ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode86 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z2154Projeto_GpoObjCtrlNome ;
      private String Z2157Projeto_SistemaSigla ;
      private String Z2159Projeto_ModuloNome ;
      private String Z2160Projeto_ModuloSigla ;
      private String Z2167ProjetoAnexos_ArquivoTipo ;
      private String Z2166ProjetoAnexos_ArquivoNome ;
      private String Z646TipoDocumento_Nome ;
      private String edtProjetoAnexos_Arquivo_Filetype ;
      private String edtProjetoAnexos_Arquivo_Filename ;
      private String sGXsfl_154_fel_idx="0001" ;
      private String subGridanexos_Class ;
      private String subGridanexos_Linesclass ;
      private String ROClassString ;
      private String edtProjetoAnexos_Codigo_Jsonclick ;
      private String edtProjetoAnexos_Arquivo_Contenttype ;
      private String edtProjetoAnexos_Arquivo_Parameters ;
      private String edtProjetoAnexos_Arquivo_Jsonclick ;
      private String edtProjetoAnexos_ArquivoNome_Jsonclick ;
      private String edtTipoDocumento_Nome_Jsonclick ;
      private String dynTipoDocumento_Codigo_Jsonclick ;
      private String edtProjetoAnexos_ArquivoTipo_Jsonclick ;
      private String edtProjetoAnexos_Data_Jsonclick ;
      private String GXCCtlgxBlob ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String i658Projeto_Status ;
      private String Gxuitabspanel_tabmain_Internalname ;
      private String subGridanexos_Internalname ;
      private String gxwrpcisep ;
      private DateTime Z2169ProjetoAnexos_Data ;
      private DateTime A2169ProjetoAnexos_Data ;
      private DateTime Z1541Projeto_Previsao ;
      private DateTime Z2146Projeto_DTInicio ;
      private DateTime Z2147Projeto_DTFim ;
      private DateTime A2146Projeto_DTInicio ;
      private DateTime A2147Projeto_DTFim ;
      private DateTime A1541Projeto_Previsao ;
      private bool Z1232Projeto_Incremental ;
      private bool entryPointCalled ;
      private bool n2153Projeto_GpoObjCtrlCodigo ;
      private bool n983Projeto_TipoProjetoCod ;
      private bool n2155Projeto_SistemaCodigo ;
      private bool n2158Projeto_ModuloCodigo ;
      private bool n2151Projeto_AreaTrabalhoCodigo ;
      private bool n645TipoDocumento_Codigo ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n2170Projeto_AnexoSequecial ;
      private bool A1232Projeto_Incremental ;
      private bool n2144Projeto_Identificador ;
      private bool n2145Projeto_Objetivo ;
      private bool n2146Projeto_DTInicio ;
      private bool n2147Projeto_DTFim ;
      private bool n2148Projeto_PrazoPrevisto ;
      private bool n2149Projeto_EsforcoPrevisto ;
      private bool n2150Projeto_CustoPrevisto ;
      private bool n985Projeto_FatorEscala ;
      private bool n989Projeto_FatorMultiplicador ;
      private bool n990Projeto_ConstACocomo ;
      private bool n1232Projeto_Incremental ;
      private bool n1541Projeto_Previsao ;
      private bool n1542Projeto_GerenteCod ;
      private bool n1543Projeto_ServicoCod ;
      private bool n1540Projeto_Introducao ;
      private bool n2152Projeto_AreaTrabalhoDescricao ;
      private bool n2154Projeto_GpoObjCtrlNome ;
      private bool n2156Projeto_SistemaNome ;
      private bool n2157Projeto_SistemaSigla ;
      private bool n2159Projeto_ModuloNome ;
      private bool n2160Projeto_ModuloSigla ;
      private bool n657Projeto_Esforco ;
      private bool n2168ProjetoAnexos_Link ;
      private bool Gxuitabspanel_tabmain_Enabled ;
      private bool Gxuitabspanel_tabmain_Autowidth ;
      private bool Gxuitabspanel_tabmain_Autoheight ;
      private bool Gxuitabspanel_tabmain_Autoscroll ;
      private bool Gxuitabspanel_tabmain_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private bool n2169ProjetoAnexos_Data ;
      private bool n2167ProjetoAnexos_ArquivoTipo ;
      private bool n2166ProjetoAnexos_ArquivoNome ;
      private bool n2165ProjetoAnexos_Arquivo ;
      private bool n127Sistema_Codigo ;
      private String A2145Projeto_Objetivo ;
      private String A653Projeto_Escopo ;
      private String A1540Projeto_Introducao ;
      private String A2164ProjetoAnexos_Descricao ;
      private String A2168ProjetoAnexos_Link ;
      private String Z1540Projeto_Introducao ;
      private String Z653Projeto_Escopo ;
      private String Z2145Projeto_Objetivo ;
      private String Z2164ProjetoAnexos_Descricao ;
      private String Z2168ProjetoAnexos_Link ;
      private String Z2144Projeto_Identificador ;
      private String A2144Projeto_Identificador ;
      private String A2152Projeto_AreaTrabalhoDescricao ;
      private String A2156Projeto_SistemaNome ;
      private String Z2152Projeto_AreaTrabalhoDescricao ;
      private String Z2156Projeto_SistemaNome ;
      private String A2165ProjetoAnexos_Arquivo ;
      private String Z2165ProjetoAnexos_Arquivo ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private GxFile gxblobfileaux ;
      private GXWebGrid GridanexosContainer ;
      private GXWebRow GridanexosRow ;
      private GXWebColumn GridanexosColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynProjeto_TipoProjetoCod ;
      private GXCombobox cmbProjeto_Status ;
      private GXCombobox dynProjeto_GpoObjCtrlCodigo ;
      private GXCombobox dynProjeto_SistemaCodigo ;
      private GXCombobox dynProjeto_ModuloCodigo ;
      private GXCheckbox chkProjeto_Incremental ;
      private GXCombobox dynTipoDocumento_Codigo ;
      private IDataStoreProvider pr_default ;
      private String[] T00299_A2154Projeto_GpoObjCtrlNome ;
      private bool[] T00299_n2154Projeto_GpoObjCtrlNome ;
      private String[] T002911_A2159Projeto_ModuloNome ;
      private bool[] T002911_n2159Projeto_ModuloNome ;
      private String[] T002911_A2160Projeto_ModuloSigla ;
      private bool[] T002911_n2160Projeto_ModuloSigla ;
      private String[] T002910_A2156Projeto_SistemaNome ;
      private bool[] T002910_n2156Projeto_SistemaNome ;
      private String[] T002910_A2157Projeto_SistemaSigla ;
      private bool[] T002910_n2157Projeto_SistemaSigla ;
      private decimal[] T002913_A655Projeto_Custo ;
      private short[] T002913_A656Projeto_Prazo ;
      private short[] T002913_A657Projeto_Esforco ;
      private bool[] T002913_n657Projeto_Esforco ;
      private String[] T00298_A2152Projeto_AreaTrabalhoDescricao ;
      private bool[] T00298_n2152Projeto_AreaTrabalhoDescricao ;
      private int[] T002915_A648Projeto_Codigo ;
      private String[] T002915_A649Projeto_Nome ;
      private String[] T002915_A650Projeto_Sigla ;
      private String[] T002915_A652Projeto_TecnicaContagem ;
      private String[] T002915_A1540Projeto_Introducao ;
      private bool[] T002915_n1540Projeto_Introducao ;
      private String[] T002915_A653Projeto_Escopo ;
      private DateTime[] T002915_A1541Projeto_Previsao ;
      private bool[] T002915_n1541Projeto_Previsao ;
      private int[] T002915_A1542Projeto_GerenteCod ;
      private bool[] T002915_n1542Projeto_GerenteCod ;
      private int[] T002915_A1543Projeto_ServicoCod ;
      private bool[] T002915_n1543Projeto_ServicoCod ;
      private String[] T002915_A658Projeto_Status ;
      private decimal[] T002915_A985Projeto_FatorEscala ;
      private bool[] T002915_n985Projeto_FatorEscala ;
      private decimal[] T002915_A989Projeto_FatorMultiplicador ;
      private bool[] T002915_n989Projeto_FatorMultiplicador ;
      private decimal[] T002915_A990Projeto_ConstACocomo ;
      private bool[] T002915_n990Projeto_ConstACocomo ;
      private bool[] T002915_A1232Projeto_Incremental ;
      private bool[] T002915_n1232Projeto_Incremental ;
      private String[] T002915_A2144Projeto_Identificador ;
      private bool[] T002915_n2144Projeto_Identificador ;
      private String[] T002915_A2145Projeto_Objetivo ;
      private bool[] T002915_n2145Projeto_Objetivo ;
      private DateTime[] T002915_A2146Projeto_DTInicio ;
      private bool[] T002915_n2146Projeto_DTInicio ;
      private DateTime[] T002915_A2147Projeto_DTFim ;
      private bool[] T002915_n2147Projeto_DTFim ;
      private short[] T002915_A2148Projeto_PrazoPrevisto ;
      private bool[] T002915_n2148Projeto_PrazoPrevisto ;
      private short[] T002915_A2149Projeto_EsforcoPrevisto ;
      private bool[] T002915_n2149Projeto_EsforcoPrevisto ;
      private decimal[] T002915_A2150Projeto_CustoPrevisto ;
      private bool[] T002915_n2150Projeto_CustoPrevisto ;
      private String[] T002915_A2152Projeto_AreaTrabalhoDescricao ;
      private bool[] T002915_n2152Projeto_AreaTrabalhoDescricao ;
      private String[] T002915_A2154Projeto_GpoObjCtrlNome ;
      private bool[] T002915_n2154Projeto_GpoObjCtrlNome ;
      private String[] T002915_A2156Projeto_SistemaNome ;
      private bool[] T002915_n2156Projeto_SistemaNome ;
      private String[] T002915_A2157Projeto_SistemaSigla ;
      private bool[] T002915_n2157Projeto_SistemaSigla ;
      private String[] T002915_A2159Projeto_ModuloNome ;
      private bool[] T002915_n2159Projeto_ModuloNome ;
      private String[] T002915_A2160Projeto_ModuloSigla ;
      private bool[] T002915_n2160Projeto_ModuloSigla ;
      private short[] T002915_A2170Projeto_AnexoSequecial ;
      private bool[] T002915_n2170Projeto_AnexoSequecial ;
      private int[] T002915_A983Projeto_TipoProjetoCod ;
      private bool[] T002915_n983Projeto_TipoProjetoCod ;
      private int[] T002915_A2151Projeto_AreaTrabalhoCodigo ;
      private bool[] T002915_n2151Projeto_AreaTrabalhoCodigo ;
      private int[] T002915_A2153Projeto_GpoObjCtrlCodigo ;
      private bool[] T002915_n2153Projeto_GpoObjCtrlCodigo ;
      private int[] T002915_A2155Projeto_SistemaCodigo ;
      private bool[] T002915_n2155Projeto_SistemaCodigo ;
      private int[] T002915_A2158Projeto_ModuloCodigo ;
      private bool[] T002915_n2158Projeto_ModuloCodigo ;
      private decimal[] T002915_A655Projeto_Custo ;
      private short[] T002915_A656Projeto_Prazo ;
      private short[] T002915_A657Projeto_Esforco ;
      private bool[] T002915_n657Projeto_Esforco ;
      private int[] T00297_A983Projeto_TipoProjetoCod ;
      private bool[] T00297_n983Projeto_TipoProjetoCod ;
      private int[] T002916_A983Projeto_TipoProjetoCod ;
      private bool[] T002916_n983Projeto_TipoProjetoCod ;
      private String[] T002917_A2154Projeto_GpoObjCtrlNome ;
      private bool[] T002917_n2154Projeto_GpoObjCtrlNome ;
      private String[] T002918_A2156Projeto_SistemaNome ;
      private bool[] T002918_n2156Projeto_SistemaNome ;
      private String[] T002918_A2157Projeto_SistemaSigla ;
      private bool[] T002918_n2157Projeto_SistemaSigla ;
      private decimal[] T002920_A655Projeto_Custo ;
      private short[] T002920_A656Projeto_Prazo ;
      private short[] T002920_A657Projeto_Esforco ;
      private bool[] T002920_n657Projeto_Esforco ;
      private String[] T002921_A2159Projeto_ModuloNome ;
      private bool[] T002921_n2159Projeto_ModuloNome ;
      private String[] T002921_A2160Projeto_ModuloSigla ;
      private bool[] T002921_n2160Projeto_ModuloSigla ;
      private String[] T002922_A2152Projeto_AreaTrabalhoDescricao ;
      private bool[] T002922_n2152Projeto_AreaTrabalhoDescricao ;
      private int[] T002923_A648Projeto_Codigo ;
      private int[] T00296_A648Projeto_Codigo ;
      private String[] T00296_A649Projeto_Nome ;
      private String[] T00296_A650Projeto_Sigla ;
      private String[] T00296_A652Projeto_TecnicaContagem ;
      private String[] T00296_A1540Projeto_Introducao ;
      private bool[] T00296_n1540Projeto_Introducao ;
      private String[] T00296_A653Projeto_Escopo ;
      private DateTime[] T00296_A1541Projeto_Previsao ;
      private bool[] T00296_n1541Projeto_Previsao ;
      private int[] T00296_A1542Projeto_GerenteCod ;
      private bool[] T00296_n1542Projeto_GerenteCod ;
      private int[] T00296_A1543Projeto_ServicoCod ;
      private bool[] T00296_n1543Projeto_ServicoCod ;
      private String[] T00296_A658Projeto_Status ;
      private decimal[] T00296_A985Projeto_FatorEscala ;
      private bool[] T00296_n985Projeto_FatorEscala ;
      private decimal[] T00296_A989Projeto_FatorMultiplicador ;
      private bool[] T00296_n989Projeto_FatorMultiplicador ;
      private decimal[] T00296_A990Projeto_ConstACocomo ;
      private bool[] T00296_n990Projeto_ConstACocomo ;
      private bool[] T00296_A1232Projeto_Incremental ;
      private bool[] T00296_n1232Projeto_Incremental ;
      private String[] T00296_A2144Projeto_Identificador ;
      private bool[] T00296_n2144Projeto_Identificador ;
      private String[] T00296_A2145Projeto_Objetivo ;
      private bool[] T00296_n2145Projeto_Objetivo ;
      private DateTime[] T00296_A2146Projeto_DTInicio ;
      private bool[] T00296_n2146Projeto_DTInicio ;
      private DateTime[] T00296_A2147Projeto_DTFim ;
      private bool[] T00296_n2147Projeto_DTFim ;
      private short[] T00296_A2148Projeto_PrazoPrevisto ;
      private bool[] T00296_n2148Projeto_PrazoPrevisto ;
      private short[] T00296_A2149Projeto_EsforcoPrevisto ;
      private bool[] T00296_n2149Projeto_EsforcoPrevisto ;
      private decimal[] T00296_A2150Projeto_CustoPrevisto ;
      private bool[] T00296_n2150Projeto_CustoPrevisto ;
      private short[] T00296_A2170Projeto_AnexoSequecial ;
      private bool[] T00296_n2170Projeto_AnexoSequecial ;
      private int[] T00296_A983Projeto_TipoProjetoCod ;
      private bool[] T00296_n983Projeto_TipoProjetoCod ;
      private int[] T00296_A2151Projeto_AreaTrabalhoCodigo ;
      private bool[] T00296_n2151Projeto_AreaTrabalhoCodigo ;
      private int[] T00296_A2153Projeto_GpoObjCtrlCodigo ;
      private bool[] T00296_n2153Projeto_GpoObjCtrlCodigo ;
      private int[] T00296_A2155Projeto_SistemaCodigo ;
      private bool[] T00296_n2155Projeto_SistemaCodigo ;
      private int[] T00296_A2158Projeto_ModuloCodigo ;
      private bool[] T00296_n2158Projeto_ModuloCodigo ;
      private int[] T002924_A648Projeto_Codigo ;
      private int[] T002925_A648Projeto_Codigo ;
      private int[] T00295_A648Projeto_Codigo ;
      private String[] T00295_A649Projeto_Nome ;
      private String[] T00295_A650Projeto_Sigla ;
      private String[] T00295_A652Projeto_TecnicaContagem ;
      private String[] T00295_A1540Projeto_Introducao ;
      private bool[] T00295_n1540Projeto_Introducao ;
      private String[] T00295_A653Projeto_Escopo ;
      private DateTime[] T00295_A1541Projeto_Previsao ;
      private bool[] T00295_n1541Projeto_Previsao ;
      private int[] T00295_A1542Projeto_GerenteCod ;
      private bool[] T00295_n1542Projeto_GerenteCod ;
      private int[] T00295_A1543Projeto_ServicoCod ;
      private bool[] T00295_n1543Projeto_ServicoCod ;
      private String[] T00295_A658Projeto_Status ;
      private decimal[] T00295_A985Projeto_FatorEscala ;
      private bool[] T00295_n985Projeto_FatorEscala ;
      private decimal[] T00295_A989Projeto_FatorMultiplicador ;
      private bool[] T00295_n989Projeto_FatorMultiplicador ;
      private decimal[] T00295_A990Projeto_ConstACocomo ;
      private bool[] T00295_n990Projeto_ConstACocomo ;
      private bool[] T00295_A1232Projeto_Incremental ;
      private bool[] T00295_n1232Projeto_Incremental ;
      private String[] T00295_A2144Projeto_Identificador ;
      private bool[] T00295_n2144Projeto_Identificador ;
      private String[] T00295_A2145Projeto_Objetivo ;
      private bool[] T00295_n2145Projeto_Objetivo ;
      private DateTime[] T00295_A2146Projeto_DTInicio ;
      private bool[] T00295_n2146Projeto_DTInicio ;
      private DateTime[] T00295_A2147Projeto_DTFim ;
      private bool[] T00295_n2147Projeto_DTFim ;
      private short[] T00295_A2148Projeto_PrazoPrevisto ;
      private bool[] T00295_n2148Projeto_PrazoPrevisto ;
      private short[] T00295_A2149Projeto_EsforcoPrevisto ;
      private bool[] T00295_n2149Projeto_EsforcoPrevisto ;
      private decimal[] T00295_A2150Projeto_CustoPrevisto ;
      private bool[] T00295_n2150Projeto_CustoPrevisto ;
      private short[] T00295_A2170Projeto_AnexoSequecial ;
      private bool[] T00295_n2170Projeto_AnexoSequecial ;
      private int[] T00295_A983Projeto_TipoProjetoCod ;
      private bool[] T00295_n983Projeto_TipoProjetoCod ;
      private int[] T00295_A2151Projeto_AreaTrabalhoCodigo ;
      private bool[] T00295_n2151Projeto_AreaTrabalhoCodigo ;
      private int[] T00295_A2153Projeto_GpoObjCtrlCodigo ;
      private bool[] T00295_n2153Projeto_GpoObjCtrlCodigo ;
      private int[] T00295_A2155Projeto_SistemaCodigo ;
      private bool[] T00295_n2155Projeto_SistemaCodigo ;
      private int[] T00295_A2158Projeto_ModuloCodigo ;
      private bool[] T00295_n2158Projeto_ModuloCodigo ;
      private int[] T002926_A648Projeto_Codigo ;
      private String[] T002929_A2154Projeto_GpoObjCtrlNome ;
      private bool[] T002929_n2154Projeto_GpoObjCtrlNome ;
      private String[] T002930_A2156Projeto_SistemaNome ;
      private bool[] T002930_n2156Projeto_SistemaNome ;
      private String[] T002930_A2157Projeto_SistemaSigla ;
      private bool[] T002930_n2157Projeto_SistemaSigla ;
      private decimal[] T002932_A655Projeto_Custo ;
      private short[] T002932_A656Projeto_Prazo ;
      private short[] T002932_A657Projeto_Esforco ;
      private bool[] T002932_n657Projeto_Esforco ;
      private String[] T002933_A2159Projeto_ModuloNome ;
      private bool[] T002933_n2159Projeto_ModuloNome ;
      private String[] T002933_A2160Projeto_ModuloSigla ;
      private bool[] T002933_n2160Projeto_ModuloSigla ;
      private String[] T002934_A2152Projeto_AreaTrabalhoDescricao ;
      private bool[] T002934_n2152Projeto_AreaTrabalhoDescricao ;
      private int[] T002935_A961VariavelCocomo_AreaTrabalhoCod ;
      private String[] T002935_A962VariavelCocomo_Sigla ;
      private String[] T002935_A964VariavelCocomo_Tipo ;
      private short[] T002935_A992VariavelCocomo_Sequencial ;
      private int[] T002936_A192Contagem_Codigo ;
      private int[] T002937_A736ProjetoMelhoria_Codigo ;
      private int[] T002938_A1685Proposta_Codigo ;
      private int[] T002940_A648Projeto_Codigo ;
      private String[] T00294_A646TipoDocumento_Nome ;
      private int[] T002941_A648Projeto_Codigo ;
      private int[] T002941_A2163ProjetoAnexos_Codigo ;
      private String[] T002941_A2164ProjetoAnexos_Descricao ;
      private String[] T002941_A2168ProjetoAnexos_Link ;
      private bool[] T002941_n2168ProjetoAnexos_Link ;
      private DateTime[] T002941_A2169ProjetoAnexos_Data ;
      private bool[] T002941_n2169ProjetoAnexos_Data ;
      private String[] T002941_A646TipoDocumento_Nome ;
      private String[] T002941_A2167ProjetoAnexos_ArquivoTipo ;
      private bool[] T002941_n2167ProjetoAnexos_ArquivoTipo ;
      private String[] T002941_A2166ProjetoAnexos_ArquivoNome ;
      private bool[] T002941_n2166ProjetoAnexos_ArquivoNome ;
      private int[] T002941_A645TipoDocumento_Codigo ;
      private bool[] T002941_n645TipoDocumento_Codigo ;
      private String[] T002941_A2165ProjetoAnexos_Arquivo ;
      private bool[] T002941_n2165ProjetoAnexos_Arquivo ;
      private String[] T002942_A646TipoDocumento_Nome ;
      private int[] T002943_A648Projeto_Codigo ;
      private int[] T002943_A2163ProjetoAnexos_Codigo ;
      private int[] T00293_A648Projeto_Codigo ;
      private int[] T00293_A2163ProjetoAnexos_Codigo ;
      private String[] T00293_A2164ProjetoAnexos_Descricao ;
      private String[] T00293_A2168ProjetoAnexos_Link ;
      private bool[] T00293_n2168ProjetoAnexos_Link ;
      private DateTime[] T00293_A2169ProjetoAnexos_Data ;
      private bool[] T00293_n2169ProjetoAnexos_Data ;
      private String[] T00293_A2167ProjetoAnexos_ArquivoTipo ;
      private bool[] T00293_n2167ProjetoAnexos_ArquivoTipo ;
      private String[] T00293_A2166ProjetoAnexos_ArquivoNome ;
      private bool[] T00293_n2166ProjetoAnexos_ArquivoNome ;
      private int[] T00293_A645TipoDocumento_Codigo ;
      private bool[] T00293_n645TipoDocumento_Codigo ;
      private String[] T00293_A2165ProjetoAnexos_Arquivo ;
      private bool[] T00293_n2165ProjetoAnexos_Arquivo ;
      private int[] T00292_A648Projeto_Codigo ;
      private int[] T00292_A2163ProjetoAnexos_Codigo ;
      private String[] T00292_A2164ProjetoAnexos_Descricao ;
      private String[] T00292_A2168ProjetoAnexos_Link ;
      private bool[] T00292_n2168ProjetoAnexos_Link ;
      private DateTime[] T00292_A2169ProjetoAnexos_Data ;
      private bool[] T00292_n2169ProjetoAnexos_Data ;
      private String[] T00292_A2167ProjetoAnexos_ArquivoTipo ;
      private bool[] T00292_n2167ProjetoAnexos_ArquivoTipo ;
      private String[] T00292_A2166ProjetoAnexos_ArquivoNome ;
      private bool[] T00292_n2166ProjetoAnexos_ArquivoNome ;
      private int[] T00292_A645TipoDocumento_Codigo ;
      private bool[] T00292_n645TipoDocumento_Codigo ;
      private String[] T00292_A2165ProjetoAnexos_Arquivo ;
      private bool[] T00292_n2165ProjetoAnexos_Arquivo ;
      private String[] T002948_A646TipoDocumento_Nome ;
      private int[] T002949_A648Projeto_Codigo ;
      private int[] T002949_A2163ProjetoAnexos_Codigo ;
      private int[] T002950_A983Projeto_TipoProjetoCod ;
      private bool[] T002950_n983Projeto_TipoProjetoCod ;
      private String[] T002950_A980TipoProjeto_Nome ;
      private bool[] T002950_n980TipoProjeto_Nome ;
      private int[] T002951_A2153Projeto_GpoObjCtrlCodigo ;
      private bool[] T002951_n2153Projeto_GpoObjCtrlCodigo ;
      private String[] T002951_A2154Projeto_GpoObjCtrlNome ;
      private bool[] T002951_n2154Projeto_GpoObjCtrlNome ;
      private int[] T002952_A127Sistema_Codigo ;
      private bool[] T002952_n127Sistema_Codigo ;
      private String[] T002952_A416Sistema_Nome ;
      private int[] T002952_A135Sistema_AreaTrabalhoCod ;
      private int[] T002952_A2161Sistema_GpoObjCtrlCod ;
      private bool[] T002952_n2161Sistema_GpoObjCtrlCod ;
      private int[] T002953_A2158Projeto_ModuloCodigo ;
      private bool[] T002953_n2158Projeto_ModuloCodigo ;
      private String[] T002953_A2160Projeto_ModuloSigla ;
      private bool[] T002953_n2160Projeto_ModuloSigla ;
      private int[] T002953_A127Sistema_Codigo ;
      private bool[] T002953_n127Sistema_Codigo ;
      private int[] T002954_A645TipoDocumento_Codigo ;
      private bool[] T002954_n645TipoDocumento_Codigo ;
      private String[] T002954_A646TipoDocumento_Nome ;
      private bool[] T002954_A647TipoDocumento_Ativo ;
      private int[] T002955_A983Projeto_TipoProjetoCod ;
      private bool[] T002955_n983Projeto_TipoProjetoCod ;
      private String[] T002956_A646TipoDocumento_Nome ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class projeto__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new UpdateCursor(def[22])
         ,new UpdateCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
         ,new ForEachCursor(def[32])
         ,new UpdateCursor(def[33])
         ,new ForEachCursor(def[34])
         ,new ForEachCursor(def[35])
         ,new ForEachCursor(def[36])
         ,new ForEachCursor(def[37])
         ,new UpdateCursor(def[38])
         ,new UpdateCursor(def[39])
         ,new UpdateCursor(def[40])
         ,new UpdateCursor(def[41])
         ,new ForEachCursor(def[42])
         ,new ForEachCursor(def[43])
         ,new ForEachCursor(def[44])
         ,new ForEachCursor(def[45])
         ,new ForEachCursor(def[46])
         ,new ForEachCursor(def[47])
         ,new ForEachCursor(def[48])
         ,new ForEachCursor(def[49])
         ,new ForEachCursor(def[50])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT002915 ;
          prmT002915 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferT002915 ;
          cmdBufferT002915=" SELECT TM1.[Projeto_Codigo], TM1.[Projeto_Nome], TM1.[Projeto_Sigla], TM1.[Projeto_TecnicaContagem], TM1.[Projeto_Introducao], TM1.[Projeto_Escopo], TM1.[Projeto_Previsao], TM1.[Projeto_GerenteCod], TM1.[Projeto_ServicoCod], TM1.[Projeto_Status], TM1.[Projeto_FatorEscala], TM1.[Projeto_FatorMultiplicador], TM1.[Projeto_ConstACocomo], TM1.[Projeto_Incremental], TM1.[Projeto_Identificador], TM1.[Projeto_Objetivo], TM1.[Projeto_DTInicio], TM1.[Projeto_DTFim], TM1.[Projeto_PrazoPrevisto], TM1.[Projeto_EsforcoPrevisto], TM1.[Projeto_CustoPrevisto], T2.[AreaTrabalho_Descricao] AS Projeto_AreaTrabalhoDescricao, T3.[GpoObjCtrl_Nome] AS Projeto_GpoObjCtrlNome, T4.[Sistema_Nome] AS Projeto_SistemaNome, T4.[Sistema_Sigla] AS Projeto_SistemaSigla, T6.[Modulo_Nome] AS Projeto_ModuloNome, T6.[Modulo_Sigla] AS Projeto_ModuloSigla, TM1.[Projeto_AnexoSequecial], TM1.[Projeto_TipoProjetoCod] AS Projeto_TipoProjetoCod, TM1.[Projeto_AreaTrabalhoCodigo] AS Projeto_AreaTrabalhoCodigo, TM1.[Projeto_GpoObjCtrlCodigo] AS Projeto_GpoObjCtrlCodigo, TM1.[Projeto_SistemaCodigo] AS Projeto_SistemaCodigo, TM1.[Projeto_ModuloCodigo] AS Projeto_ModuloCodigo, COALESCE( T5.[Projeto_Custo], 0) AS Projeto_Custo, COALESCE( T5.[Projeto_Prazo], 0) AS Projeto_Prazo, COALESCE( T5.[Projeto_Esforco], 0) AS Projeto_Esforco FROM ((((([Projeto] TM1 WITH (NOLOCK) LEFT JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = TM1.[Projeto_AreaTrabalhoCodigo]) LEFT JOIN [GrupoObjetoControle] T3 WITH (NOLOCK) ON T3.[GpoObjCtrl_Codigo] = TM1.[Projeto_GpoObjCtrlCodigo]) LEFT JOIN [Sistema] T4 WITH (NOLOCK) ON T4.[Sistema_Codigo] = TM1.[Projeto_SistemaCodigo]) LEFT JOIN (SELECT [Sistema_Esforco] AS Projeto_Esforco, [Sistema_Codigo], [Sistema_Prazo] AS Projeto_Prazo, [Sistema_Custo] AS Projeto_Custo FROM [Sistema] WITH "
          + " (NOLOCK) ) T5 ON T5.[Sistema_Codigo] = TM1.[Projeto_SistemaCodigo]) LEFT JOIN [Modulo] T6 WITH (NOLOCK) ON T6.[Modulo_Codigo] = TM1.[Projeto_ModuloCodigo]) WHERE TM1.[Projeto_Codigo] = @Projeto_Codigo ORDER BY TM1.[Projeto_Codigo]  OPTION (FAST 100)" ;
          Object[] prmT00297 ;
          prmT00297 = new Object[] {
          new Object[] {"@Projeto_TipoProjetoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00299 ;
          prmT00299 = new Object[] {
          new Object[] {"@Projeto_GpoObjCtrlCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002910 ;
          prmT002910 = new Object[] {
          new Object[] {"@Projeto_SistemaCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002913 ;
          prmT002913 = new Object[] {
          new Object[] {"@Projeto_SistemaCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002911 ;
          prmT002911 = new Object[] {
          new Object[] {"@Projeto_ModuloCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00298 ;
          prmT00298 = new Object[] {
          new Object[] {"@Projeto_AreaTrabalhoCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002916 ;
          prmT002916 = new Object[] {
          new Object[] {"@Projeto_TipoProjetoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002917 ;
          prmT002917 = new Object[] {
          new Object[] {"@Projeto_GpoObjCtrlCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002918 ;
          prmT002918 = new Object[] {
          new Object[] {"@Projeto_SistemaCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002920 ;
          prmT002920 = new Object[] {
          new Object[] {"@Projeto_SistemaCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002921 ;
          prmT002921 = new Object[] {
          new Object[] {"@Projeto_ModuloCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002922 ;
          prmT002922 = new Object[] {
          new Object[] {"@Projeto_AreaTrabalhoCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002923 ;
          prmT002923 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00296 ;
          prmT00296 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002924 ;
          prmT002924 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002925 ;
          prmT002925 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00295 ;
          prmT00295 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002926 ;
          prmT002926 = new Object[] {
          new Object[] {"@Projeto_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Projeto_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@Projeto_TecnicaContagem",SqlDbType.Char,1,0} ,
          new Object[] {"@Projeto_Introducao",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Projeto_Escopo",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Projeto_Previsao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Projeto_GerenteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@Projeto_FatorEscala",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Projeto_FatorMultiplicador",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Projeto_ConstACocomo",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Projeto_Incremental",SqlDbType.Bit,4,0} ,
          new Object[] {"@Projeto_Identificador",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Projeto_Objetivo",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Projeto_DTInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Projeto_DTFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Projeto_PrazoPrevisto",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Projeto_EsforcoPrevisto",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Projeto_CustoPrevisto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@Projeto_AnexoSequecial",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Projeto_TipoProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_AreaTrabalhoCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_GpoObjCtrlCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_SistemaCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_ModuloCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002927 ;
          prmT002927 = new Object[] {
          new Object[] {"@Projeto_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Projeto_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@Projeto_TecnicaContagem",SqlDbType.Char,1,0} ,
          new Object[] {"@Projeto_Introducao",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Projeto_Escopo",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Projeto_Previsao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Projeto_GerenteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@Projeto_FatorEscala",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Projeto_FatorMultiplicador",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Projeto_ConstACocomo",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Projeto_Incremental",SqlDbType.Bit,4,0} ,
          new Object[] {"@Projeto_Identificador",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Projeto_Objetivo",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Projeto_DTInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Projeto_DTFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Projeto_PrazoPrevisto",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Projeto_EsforcoPrevisto",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Projeto_CustoPrevisto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@Projeto_AnexoSequecial",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Projeto_TipoProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_AreaTrabalhoCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_GpoObjCtrlCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_SistemaCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_ModuloCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002928 ;
          prmT002928 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002934 ;
          prmT002934 = new Object[] {
          new Object[] {"@Projeto_AreaTrabalhoCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002935 ;
          prmT002935 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002936 ;
          prmT002936 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002937 ;
          prmT002937 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002938 ;
          prmT002938 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002939 ;
          prmT002939 = new Object[] {
          new Object[] {"@Projeto_AnexoSequecial",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002940 ;
          prmT002940 = new Object[] {
          } ;
          Object[] prmT002941 ;
          prmT002941 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00294 ;
          prmT00294 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002942 ;
          prmT002942 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002943 ;
          prmT002943 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00293 ;
          prmT00293 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00292 ;
          prmT00292 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002944 ;
          prmT002944 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoAnexos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoAnexos_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ProjetoAnexos_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ProjetoAnexos_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ProjetoAnexos_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ProjetoAnexos_ArquivoTipo",SqlDbType.Char,10,0} ,
          new Object[] {"@ProjetoAnexos_ArquivoNome",SqlDbType.Char,50,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002945 ;
          prmT002945 = new Object[] {
          new Object[] {"@ProjetoAnexos_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ProjetoAnexos_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ProjetoAnexos_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ProjetoAnexos_ArquivoTipo",SqlDbType.Char,10,0} ,
          new Object[] {"@ProjetoAnexos_ArquivoNome",SqlDbType.Char,50,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002946 ;
          prmT002946 = new Object[] {
          new Object[] {"@ProjetoAnexos_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002947 ;
          prmT002947 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002948 ;
          prmT002948 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002949 ;
          prmT002949 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002950 ;
          prmT002950 = new Object[] {
          } ;
          Object[] prmT002951 ;
          prmT002951 = new Object[] {
          } ;
          Object[] prmT002952 ;
          prmT002952 = new Object[] {
          new Object[] {"@AV8WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_GpoObjCtrlCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002953 ;
          prmT002953 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002954 ;
          prmT002954 = new Object[] {
          } ;
          Object[] prmT002955 ;
          prmT002955 = new Object[] {
          new Object[] {"@Projeto_TipoProjetoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002929 ;
          prmT002929 = new Object[] {
          new Object[] {"@Projeto_GpoObjCtrlCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002930 ;
          prmT002930 = new Object[] {
          new Object[] {"@Projeto_SistemaCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002932 ;
          prmT002932 = new Object[] {
          new Object[] {"@Projeto_SistemaCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002933 ;
          prmT002933 = new Object[] {
          new Object[] {"@Projeto_ModuloCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002956 ;
          prmT002956 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00292", "SELECT [Projeto_Codigo], [ProjetoAnexos_Codigo], [ProjetoAnexos_Descricao], [ProjetoAnexos_Link], [ProjetoAnexos_Data], [ProjetoAnexos_ArquivoTipo], [ProjetoAnexos_ArquivoNome], [TipoDocumento_Codigo], [ProjetoAnexos_Arquivo] FROM [ProjetoAnexos] WITH (UPDLOCK) WHERE [Projeto_Codigo] = @Projeto_Codigo AND [ProjetoAnexos_Codigo] = @ProjetoAnexos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00292,1,0,true,false )
             ,new CursorDef("T00293", "SELECT [Projeto_Codigo], [ProjetoAnexos_Codigo], [ProjetoAnexos_Descricao], [ProjetoAnexos_Link], [ProjetoAnexos_Data], [ProjetoAnexos_ArquivoTipo], [ProjetoAnexos_ArquivoNome], [TipoDocumento_Codigo], [ProjetoAnexos_Arquivo] FROM [ProjetoAnexos] WITH (NOLOCK) WHERE [Projeto_Codigo] = @Projeto_Codigo AND [ProjetoAnexos_Codigo] = @ProjetoAnexos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00293,1,0,true,false )
             ,new CursorDef("T00294", "SELECT [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00294,1,0,true,false )
             ,new CursorDef("T00295", "SELECT [Projeto_Codigo], [Projeto_Nome], [Projeto_Sigla], [Projeto_TecnicaContagem], [Projeto_Introducao], [Projeto_Escopo], [Projeto_Previsao], [Projeto_GerenteCod], [Projeto_ServicoCod], [Projeto_Status], [Projeto_FatorEscala], [Projeto_FatorMultiplicador], [Projeto_ConstACocomo], [Projeto_Incremental], [Projeto_Identificador], [Projeto_Objetivo], [Projeto_DTInicio], [Projeto_DTFim], [Projeto_PrazoPrevisto], [Projeto_EsforcoPrevisto], [Projeto_CustoPrevisto], [Projeto_AnexoSequecial], [Projeto_TipoProjetoCod] AS Projeto_TipoProjetoCod, [Projeto_AreaTrabalhoCodigo] AS Projeto_AreaTrabalhoCodigo, [Projeto_GpoObjCtrlCodigo] AS Projeto_GpoObjCtrlCodigo, [Projeto_SistemaCodigo] AS Projeto_SistemaCodigo, [Projeto_ModuloCodigo] AS Projeto_ModuloCodigo FROM [Projeto] WITH (UPDLOCK) WHERE [Projeto_Codigo] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00295,1,0,true,false )
             ,new CursorDef("T00296", "SELECT [Projeto_Codigo], [Projeto_Nome], [Projeto_Sigla], [Projeto_TecnicaContagem], [Projeto_Introducao], [Projeto_Escopo], [Projeto_Previsao], [Projeto_GerenteCod], [Projeto_ServicoCod], [Projeto_Status], [Projeto_FatorEscala], [Projeto_FatorMultiplicador], [Projeto_ConstACocomo], [Projeto_Incremental], [Projeto_Identificador], [Projeto_Objetivo], [Projeto_DTInicio], [Projeto_DTFim], [Projeto_PrazoPrevisto], [Projeto_EsforcoPrevisto], [Projeto_CustoPrevisto], [Projeto_AnexoSequecial], [Projeto_TipoProjetoCod] AS Projeto_TipoProjetoCod, [Projeto_AreaTrabalhoCodigo] AS Projeto_AreaTrabalhoCodigo, [Projeto_GpoObjCtrlCodigo] AS Projeto_GpoObjCtrlCodigo, [Projeto_SistemaCodigo] AS Projeto_SistemaCodigo, [Projeto_ModuloCodigo] AS Projeto_ModuloCodigo FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_Codigo] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00296,1,0,true,false )
             ,new CursorDef("T00297", "SELECT [TipoProjeto_Codigo] AS Projeto_TipoProjetoCod FROM [TipoProjeto] WITH (NOLOCK) WHERE [TipoProjeto_Codigo] = @Projeto_TipoProjetoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00297,1,0,true,false )
             ,new CursorDef("T00298", "SELECT [AreaTrabalho_Descricao] AS Projeto_AreaTrabalhoDescricao FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Projeto_AreaTrabalhoCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00298,1,0,true,false )
             ,new CursorDef("T00299", "SELECT [GpoObjCtrl_Nome] AS Projeto_GpoObjCtrlNome FROM [GrupoObjetoControle] WITH (NOLOCK) WHERE [GpoObjCtrl_Codigo] = @Projeto_GpoObjCtrlCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00299,1,0,true,false )
             ,new CursorDef("T002910", "SELECT [Sistema_Nome] AS Projeto_SistemaNome, [Sistema_Sigla] AS Projeto_SistemaSigla FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Projeto_SistemaCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002910,1,0,true,false )
             ,new CursorDef("T002911", "SELECT [Modulo_Nome] AS Projeto_ModuloNome, [Modulo_Sigla] AS Projeto_ModuloSigla FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Projeto_ModuloCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002911,1,0,true,false )
             ,new CursorDef("T002913", "SELECT COALESCE( T1.[Projeto_Custo], 0) AS Projeto_Custo, COALESCE( T1.[Projeto_Prazo], 0) AS Projeto_Prazo, COALESCE( T1.[Projeto_Esforco], 0) AS Projeto_Esforco FROM (SELECT [Sistema_Esforco] AS Projeto_Esforco, [Sistema_Codigo], [Sistema_Prazo] AS Projeto_Prazo, [Sistema_Custo] AS Projeto_Custo FROM [Sistema] WITH (NOLOCK) ) T1 WHERE T1.[Sistema_Codigo] = @Projeto_SistemaCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002913,1,0,true,false )
             ,new CursorDef("T002915", cmdBufferT002915,true, GxErrorMask.GX_NOMASK, false, this,prmT002915,100,0,true,false )
             ,new CursorDef("T002916", "SELECT [TipoProjeto_Codigo] AS Projeto_TipoProjetoCod FROM [TipoProjeto] WITH (NOLOCK) WHERE [TipoProjeto_Codigo] = @Projeto_TipoProjetoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002916,1,0,true,false )
             ,new CursorDef("T002917", "SELECT [GpoObjCtrl_Nome] AS Projeto_GpoObjCtrlNome FROM [GrupoObjetoControle] WITH (NOLOCK) WHERE [GpoObjCtrl_Codigo] = @Projeto_GpoObjCtrlCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002917,1,0,true,false )
             ,new CursorDef("T002918", "SELECT [Sistema_Nome] AS Projeto_SistemaNome, [Sistema_Sigla] AS Projeto_SistemaSigla FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Projeto_SistemaCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002918,1,0,true,false )
             ,new CursorDef("T002920", "SELECT COALESCE( T1.[Projeto_Custo], 0) AS Projeto_Custo, COALESCE( T1.[Projeto_Prazo], 0) AS Projeto_Prazo, COALESCE( T1.[Projeto_Esforco], 0) AS Projeto_Esforco FROM (SELECT [Sistema_Esforco] AS Projeto_Esforco, [Sistema_Codigo], [Sistema_Prazo] AS Projeto_Prazo, [Sistema_Custo] AS Projeto_Custo FROM [Sistema] WITH (NOLOCK) ) T1 WHERE T1.[Sistema_Codigo] = @Projeto_SistemaCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002920,1,0,true,false )
             ,new CursorDef("T002921", "SELECT [Modulo_Nome] AS Projeto_ModuloNome, [Modulo_Sigla] AS Projeto_ModuloSigla FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Projeto_ModuloCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002921,1,0,true,false )
             ,new CursorDef("T002922", "SELECT [AreaTrabalho_Descricao] AS Projeto_AreaTrabalhoDescricao FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Projeto_AreaTrabalhoCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002922,1,0,true,false )
             ,new CursorDef("T002923", "SELECT [Projeto_Codigo] FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_Codigo] = @Projeto_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002923,1,0,true,false )
             ,new CursorDef("T002924", "SELECT TOP 1 [Projeto_Codigo] FROM [Projeto] WITH (NOLOCK) WHERE ( [Projeto_Codigo] > @Projeto_Codigo) ORDER BY [Projeto_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002924,1,0,true,true )
             ,new CursorDef("T002925", "SELECT TOP 1 [Projeto_Codigo] FROM [Projeto] WITH (NOLOCK) WHERE ( [Projeto_Codigo] < @Projeto_Codigo) ORDER BY [Projeto_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002925,1,0,true,true )
             ,new CursorDef("T002926", "INSERT INTO [Projeto]([Projeto_Nome], [Projeto_Sigla], [Projeto_TecnicaContagem], [Projeto_Introducao], [Projeto_Escopo], [Projeto_Previsao], [Projeto_GerenteCod], [Projeto_ServicoCod], [Projeto_Status], [Projeto_FatorEscala], [Projeto_FatorMultiplicador], [Projeto_ConstACocomo], [Projeto_Incremental], [Projeto_Identificador], [Projeto_Objetivo], [Projeto_DTInicio], [Projeto_DTFim], [Projeto_PrazoPrevisto], [Projeto_EsforcoPrevisto], [Projeto_CustoPrevisto], [Projeto_AnexoSequecial], [Projeto_TipoProjetoCod], [Projeto_AreaTrabalhoCodigo], [Projeto_GpoObjCtrlCodigo], [Projeto_SistemaCodigo], [Projeto_ModuloCodigo]) VALUES(@Projeto_Nome, @Projeto_Sigla, @Projeto_TecnicaContagem, @Projeto_Introducao, @Projeto_Escopo, @Projeto_Previsao, @Projeto_GerenteCod, @Projeto_ServicoCod, @Projeto_Status, @Projeto_FatorEscala, @Projeto_FatorMultiplicador, @Projeto_ConstACocomo, @Projeto_Incremental, @Projeto_Identificador, @Projeto_Objetivo, @Projeto_DTInicio, @Projeto_DTFim, @Projeto_PrazoPrevisto, @Projeto_EsforcoPrevisto, @Projeto_CustoPrevisto, @Projeto_AnexoSequecial, @Projeto_TipoProjetoCod, @Projeto_AreaTrabalhoCodigo, @Projeto_GpoObjCtrlCodigo, @Projeto_SistemaCodigo, @Projeto_ModuloCodigo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT002926)
             ,new CursorDef("T002927", "UPDATE [Projeto] SET [Projeto_Nome]=@Projeto_Nome, [Projeto_Sigla]=@Projeto_Sigla, [Projeto_TecnicaContagem]=@Projeto_TecnicaContagem, [Projeto_Introducao]=@Projeto_Introducao, [Projeto_Escopo]=@Projeto_Escopo, [Projeto_Previsao]=@Projeto_Previsao, [Projeto_GerenteCod]=@Projeto_GerenteCod, [Projeto_ServicoCod]=@Projeto_ServicoCod, [Projeto_Status]=@Projeto_Status, [Projeto_FatorEscala]=@Projeto_FatorEscala, [Projeto_FatorMultiplicador]=@Projeto_FatorMultiplicador, [Projeto_ConstACocomo]=@Projeto_ConstACocomo, [Projeto_Incremental]=@Projeto_Incremental, [Projeto_Identificador]=@Projeto_Identificador, [Projeto_Objetivo]=@Projeto_Objetivo, [Projeto_DTInicio]=@Projeto_DTInicio, [Projeto_DTFim]=@Projeto_DTFim, [Projeto_PrazoPrevisto]=@Projeto_PrazoPrevisto, [Projeto_EsforcoPrevisto]=@Projeto_EsforcoPrevisto, [Projeto_CustoPrevisto]=@Projeto_CustoPrevisto, [Projeto_AnexoSequecial]=@Projeto_AnexoSequecial, [Projeto_TipoProjetoCod]=@Projeto_TipoProjetoCod, [Projeto_AreaTrabalhoCodigo]=@Projeto_AreaTrabalhoCodigo, [Projeto_GpoObjCtrlCodigo]=@Projeto_GpoObjCtrlCodigo, [Projeto_SistemaCodigo]=@Projeto_SistemaCodigo, [Projeto_ModuloCodigo]=@Projeto_ModuloCodigo  WHERE [Projeto_Codigo] = @Projeto_Codigo", GxErrorMask.GX_NOMASK,prmT002927)
             ,new CursorDef("T002928", "DELETE FROM [Projeto]  WHERE [Projeto_Codigo] = @Projeto_Codigo", GxErrorMask.GX_NOMASK,prmT002928)
             ,new CursorDef("T002929", "SELECT [GpoObjCtrl_Nome] AS Projeto_GpoObjCtrlNome FROM [GrupoObjetoControle] WITH (NOLOCK) WHERE [GpoObjCtrl_Codigo] = @Projeto_GpoObjCtrlCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002929,1,0,true,false )
             ,new CursorDef("T002930", "SELECT [Sistema_Nome] AS Projeto_SistemaNome, [Sistema_Sigla] AS Projeto_SistemaSigla FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Projeto_SistemaCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002930,1,0,true,false )
             ,new CursorDef("T002932", "SELECT COALESCE( T1.[Projeto_Custo], 0) AS Projeto_Custo, COALESCE( T1.[Projeto_Prazo], 0) AS Projeto_Prazo, COALESCE( T1.[Projeto_Esforco], 0) AS Projeto_Esforco FROM (SELECT [Sistema_Esforco] AS Projeto_Esforco, [Sistema_Codigo], [Sistema_Prazo] AS Projeto_Prazo, [Sistema_Custo] AS Projeto_Custo FROM [Sistema] WITH (NOLOCK) ) T1 WHERE T1.[Sistema_Codigo] = @Projeto_SistemaCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002932,1,0,true,false )
             ,new CursorDef("T002933", "SELECT [Modulo_Nome] AS Projeto_ModuloNome, [Modulo_Sigla] AS Projeto_ModuloSigla FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Projeto_ModuloCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002933,1,0,true,false )
             ,new CursorDef("T002934", "SELECT [AreaTrabalho_Descricao] AS Projeto_AreaTrabalhoDescricao FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Projeto_AreaTrabalhoCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002934,1,0,true,false )
             ,new CursorDef("T002935", "SELECT TOP 1 [VariavelCocomo_AreaTrabalhoCod], [VariavelCocomo_Sigla], [VariavelCocomo_Tipo], [VariavelCocomo_Sequencial] FROM [VariaveisCocomo] WITH (NOLOCK) WHERE [VariavelCocomo_ProjetoCod] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002935,1,0,true,true )
             ,new CursorDef("T002936", "SELECT TOP 1 [Contagem_Codigo] FROM [Contagem] WITH (NOLOCK) WHERE [Contagem_ProjetoCod] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002936,1,0,true,true )
             ,new CursorDef("T002937", "SELECT TOP 1 [ProjetoMelhoria_Codigo] FROM [ProjetoMelhoria] WITH (NOLOCK) WHERE [ProjetoMelhoria_ProjetoCod] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002937,1,0,true,true )
             ,new CursorDef("T002938", "SELECT TOP 1 [Proposta_Codigo] FROM dbo.[Proposta] WITH (NOLOCK) WHERE [Projeto_Codigo] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002938,1,0,true,true )
             ,new CursorDef("T002939", "UPDATE [Projeto] SET [Projeto_AnexoSequecial]=@Projeto_AnexoSequecial  WHERE [Projeto_Codigo] = @Projeto_Codigo", GxErrorMask.GX_NOMASK,prmT002939)
             ,new CursorDef("T002940", "SELECT [Projeto_Codigo] FROM [Projeto] WITH (NOLOCK) ORDER BY [Projeto_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002940,100,0,true,false )
             ,new CursorDef("T002941", "SELECT T1.[Projeto_Codigo], T1.[ProjetoAnexos_Codigo], T1.[ProjetoAnexos_Descricao], T1.[ProjetoAnexos_Link], T1.[ProjetoAnexos_Data], T2.[TipoDocumento_Nome], T1.[ProjetoAnexos_ArquivoTipo], T1.[ProjetoAnexos_ArquivoNome], T1.[TipoDocumento_Codigo], T1.[ProjetoAnexos_Arquivo] FROM ([ProjetoAnexos] T1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = T1.[TipoDocumento_Codigo]) WHERE T1.[Projeto_Codigo] = @Projeto_Codigo and T1.[ProjetoAnexos_Codigo] = @ProjetoAnexos_Codigo ORDER BY T1.[Projeto_Codigo], T1.[ProjetoAnexos_Codigo] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002941,11,0,true,false )
             ,new CursorDef("T002942", "SELECT [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002942,1,0,true,false )
             ,new CursorDef("T002943", "SELECT [Projeto_Codigo], [ProjetoAnexos_Codigo] FROM [ProjetoAnexos] WITH (NOLOCK) WHERE [Projeto_Codigo] = @Projeto_Codigo AND [ProjetoAnexos_Codigo] = @ProjetoAnexos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002943,1,0,true,false )
             ,new CursorDef("T002944", "INSERT INTO [ProjetoAnexos]([Projeto_Codigo], [ProjetoAnexos_Codigo], [ProjetoAnexos_Descricao], [ProjetoAnexos_Arquivo], [ProjetoAnexos_Link], [ProjetoAnexos_Data], [ProjetoAnexos_ArquivoTipo], [ProjetoAnexos_ArquivoNome], [TipoDocumento_Codigo]) VALUES(@Projeto_Codigo, @ProjetoAnexos_Codigo, @ProjetoAnexos_Descricao, @ProjetoAnexos_Arquivo, @ProjetoAnexos_Link, @ProjetoAnexos_Data, @ProjetoAnexos_ArquivoTipo, @ProjetoAnexos_ArquivoNome, @TipoDocumento_Codigo)", GxErrorMask.GX_NOMASK,prmT002944)
             ,new CursorDef("T002945", "UPDATE [ProjetoAnexos] SET [ProjetoAnexos_Descricao]=@ProjetoAnexos_Descricao, [ProjetoAnexos_Link]=@ProjetoAnexos_Link, [ProjetoAnexos_Data]=@ProjetoAnexos_Data, [ProjetoAnexos_ArquivoTipo]=@ProjetoAnexos_ArquivoTipo, [ProjetoAnexos_ArquivoNome]=@ProjetoAnexos_ArquivoNome, [TipoDocumento_Codigo]=@TipoDocumento_Codigo  WHERE [Projeto_Codigo] = @Projeto_Codigo AND [ProjetoAnexos_Codigo] = @ProjetoAnexos_Codigo", GxErrorMask.GX_NOMASK,prmT002945)
             ,new CursorDef("T002946", "UPDATE [ProjetoAnexos] SET [ProjetoAnexos_Arquivo]=@ProjetoAnexos_Arquivo  WHERE [Projeto_Codigo] = @Projeto_Codigo AND [ProjetoAnexos_Codigo] = @ProjetoAnexos_Codigo", GxErrorMask.GX_NOMASK,prmT002946)
             ,new CursorDef("T002947", "DELETE FROM [ProjetoAnexos]  WHERE [Projeto_Codigo] = @Projeto_Codigo AND [ProjetoAnexos_Codigo] = @ProjetoAnexos_Codigo", GxErrorMask.GX_NOMASK,prmT002947)
             ,new CursorDef("T002948", "SELECT [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002948,1,0,true,false )
             ,new CursorDef("T002949", "SELECT [Projeto_Codigo], [ProjetoAnexos_Codigo] FROM [ProjetoAnexos] WITH (NOLOCK) WHERE [Projeto_Codigo] = @Projeto_Codigo ORDER BY [Projeto_Codigo], [ProjetoAnexos_Codigo] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002949,11,0,true,false )
             ,new CursorDef("T002950", "SELECT [TipoProjeto_Codigo] AS Projeto_TipoProjetoCod, [TipoProjeto_Nome] FROM [TipoProjeto] WITH (NOLOCK) ORDER BY [TipoProjeto_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002950,0,0,true,false )
             ,new CursorDef("T002951", "SELECT [GpoObjCtrl_Codigo] AS Projeto_GpoObjCtrlCodigo, [GpoObjCtrl_Nome] AS Projeto_GpoObjCtrlNome FROM [GrupoObjetoControle] WITH (NOLOCK) ORDER BY [GpoObjCtrl_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002951,0,0,true,false )
             ,new CursorDef("T002952", "SELECT [Sistema_Codigo], [Sistema_Nome], [Sistema_AreaTrabalhoCod], [Sistema_GpoObjCtrlCod] FROM [Sistema] WITH (NOLOCK) WHERE ([Sistema_AreaTrabalhoCod] = @AV8WWPCo_1Areatrabalho_codigo) AND ([Sistema_GpoObjCtrlCod] = @Projeto_GpoObjCtrlCodigo) ORDER BY [Sistema_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002952,0,0,true,false )
             ,new CursorDef("T002953", "SELECT [Modulo_Codigo] AS Projeto_ModuloCodigo, [Modulo_Sigla] AS Projeto_ModuloSigla, [Sistema_Codigo] FROM [Modulo] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ORDER BY [Modulo_Sigla] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002953,0,0,true,false )
             ,new CursorDef("T002954", "SELECT [TipoDocumento_Codigo], [TipoDocumento_Nome], [TipoDocumento_Ativo] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Ativo] = 1 ORDER BY [TipoDocumento_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002954,0,0,true,false )
             ,new CursorDef("T002955", "SELECT [TipoProjeto_Codigo] AS Projeto_TipoProjetoCod FROM [TipoProjeto] WITH (NOLOCK) WHERE [TipoProjeto_Codigo] = @Projeto_TipoProjetoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002955,1,0,true,false )
             ,new CursorDef("T002956", "SELECT [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002956,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 10) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getBLOBFile(9, rslt.getString(6, 10), rslt.getString(7, 50)) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 10) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getBLOBFile(9, rslt.getString(6, 10), rslt.getString(7, 50)) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(6) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getString(10, 1) ;
                ((decimal[]) buf[14])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(11);
                ((decimal[]) buf[16])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(12);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(13);
                ((bool[]) buf[20])[0] = rslt.getBool(14) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(14);
                ((String[]) buf[22])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(15);
                ((String[]) buf[24])[0] = rslt.getLongVarchar(16) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(16);
                ((DateTime[]) buf[26])[0] = rslt.getGXDate(17) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(17);
                ((DateTime[]) buf[28])[0] = rslt.getGXDate(18) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(18);
                ((short[]) buf[30])[0] = rslt.getShort(19) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(19);
                ((short[]) buf[32])[0] = rslt.getShort(20) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(20);
                ((decimal[]) buf[34])[0] = rslt.getDecimal(21) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(21);
                ((short[]) buf[36])[0] = rslt.getShort(22) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(22);
                ((int[]) buf[38])[0] = rslt.getInt(23) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(23);
                ((int[]) buf[40])[0] = rslt.getInt(24) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(24);
                ((int[]) buf[42])[0] = rslt.getInt(25) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(25);
                ((int[]) buf[44])[0] = rslt.getInt(26) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(26);
                ((int[]) buf[46])[0] = rslt.getInt(27) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(27);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(6) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getString(10, 1) ;
                ((decimal[]) buf[14])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(11);
                ((decimal[]) buf[16])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(12);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(13);
                ((bool[]) buf[20])[0] = rslt.getBool(14) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(14);
                ((String[]) buf[22])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(15);
                ((String[]) buf[24])[0] = rslt.getLongVarchar(16) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(16);
                ((DateTime[]) buf[26])[0] = rslt.getGXDate(17) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(17);
                ((DateTime[]) buf[28])[0] = rslt.getGXDate(18) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(18);
                ((short[]) buf[30])[0] = rslt.getShort(19) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(19);
                ((short[]) buf[32])[0] = rslt.getShort(20) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(20);
                ((decimal[]) buf[34])[0] = rslt.getDecimal(21) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(21);
                ((short[]) buf[36])[0] = rslt.getShort(22) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(22);
                ((int[]) buf[38])[0] = rslt.getInt(23) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(23);
                ((int[]) buf[40])[0] = rslt.getInt(24) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(24);
                ((int[]) buf[42])[0] = rslt.getInt(25) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(25);
                ((int[]) buf[44])[0] = rslt.getInt(26) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(26);
                ((int[]) buf[46])[0] = rslt.getInt(27) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(27);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 25) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 10 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(6) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getString(10, 1) ;
                ((decimal[]) buf[14])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(11);
                ((decimal[]) buf[16])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(12);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(13);
                ((bool[]) buf[20])[0] = rslt.getBool(14) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(14);
                ((String[]) buf[22])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(15);
                ((String[]) buf[24])[0] = rslt.getLongVarchar(16) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(16);
                ((DateTime[]) buf[26])[0] = rslt.getGXDate(17) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(17);
                ((DateTime[]) buf[28])[0] = rslt.getGXDate(18) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(18);
                ((short[]) buf[30])[0] = rslt.getShort(19) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(19);
                ((short[]) buf[32])[0] = rslt.getShort(20) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(20);
                ((decimal[]) buf[34])[0] = rslt.getDecimal(21) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(21);
                ((String[]) buf[36])[0] = rslt.getVarchar(22) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(22);
                ((String[]) buf[38])[0] = rslt.getString(23, 50) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(23);
                ((String[]) buf[40])[0] = rslt.getVarchar(24) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(24);
                ((String[]) buf[42])[0] = rslt.getString(25, 25) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(25);
                ((String[]) buf[44])[0] = rslt.getString(26, 50) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(26);
                ((String[]) buf[46])[0] = rslt.getString(27, 15) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(27);
                ((short[]) buf[48])[0] = rslt.getShort(28) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(28);
                ((int[]) buf[50])[0] = rslt.getInt(29) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(29);
                ((int[]) buf[52])[0] = rslt.getInt(30) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(30);
                ((int[]) buf[54])[0] = rslt.getInt(31) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(31);
                ((int[]) buf[56])[0] = rslt.getInt(32) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(32);
                ((int[]) buf[58])[0] = rslt.getInt(33) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(33);
                ((decimal[]) buf[60])[0] = rslt.getDecimal(34) ;
                ((short[]) buf[61])[0] = rslt.getShort(35) ;
                ((short[]) buf[62])[0] = rslt.getShort(36) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(36);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 25) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 15 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 16 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 24 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 25 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 25) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 26 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 27 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 28 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 29 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 31 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 32 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 34 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 35 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[8])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getBLOBFile(10, rslt.getString(7, 10), rslt.getString(8, 50)) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                return;
             case 36 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 37 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 42 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 43 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 44 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 45 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 46 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                return;
             case 47 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 48 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 49 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 50 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[4]);
                }
                stmt.SetParameter(5, (String)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 6 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(6, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[11]);
                }
                stmt.SetParameter(9, (String)parms[12]);
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 10 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(10, (decimal)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 11 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(11, (decimal)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 12 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(12, (decimal)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 13 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(13, (bool)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 14 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 15 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(15, (String)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 16 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(16, (DateTime)parms[26]);
                }
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 17 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(17, (DateTime)parms[28]);
                }
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 18 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(18, (short)parms[30]);
                }
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 19 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(19, (short)parms[32]);
                }
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 20 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(20, (decimal)parms[34]);
                }
                if ( (bool)parms[35] )
                {
                   stmt.setNull( 21 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(21, (short)parms[36]);
                }
                if ( (bool)parms[37] )
                {
                   stmt.setNull( 22 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(22, (int)parms[38]);
                }
                if ( (bool)parms[39] )
                {
                   stmt.setNull( 23 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(23, (int)parms[40]);
                }
                if ( (bool)parms[41] )
                {
                   stmt.setNull( 24 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(24, (int)parms[42]);
                }
                if ( (bool)parms[43] )
                {
                   stmt.setNull( 25 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(25, (int)parms[44]);
                }
                if ( (bool)parms[45] )
                {
                   stmt.setNull( 26 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(26, (int)parms[46]);
                }
                return;
             case 22 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[4]);
                }
                stmt.SetParameter(5, (String)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 6 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(6, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[11]);
                }
                stmt.SetParameter(9, (String)parms[12]);
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 10 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(10, (decimal)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 11 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(11, (decimal)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 12 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(12, (decimal)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 13 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(13, (bool)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 14 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 15 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(15, (String)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 16 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(16, (DateTime)parms[26]);
                }
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 17 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(17, (DateTime)parms[28]);
                }
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 18 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(18, (short)parms[30]);
                }
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 19 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(19, (short)parms[32]);
                }
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 20 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(20, (decimal)parms[34]);
                }
                if ( (bool)parms[35] )
                {
                   stmt.setNull( 21 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(21, (short)parms[36]);
                }
                if ( (bool)parms[37] )
                {
                   stmt.setNull( 22 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(22, (int)parms[38]);
                }
                if ( (bool)parms[39] )
                {
                   stmt.setNull( 23 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(23, (int)parms[40]);
                }
                if ( (bool)parms[41] )
                {
                   stmt.setNull( 24 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(24, (int)parms[42]);
                }
                if ( (bool)parms[43] )
                {
                   stmt.setNull( 25 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(25, (int)parms[44]);
                }
                if ( (bool)parms[45] )
                {
                   stmt.setNull( 26 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(26, (int)parms[46]);
                }
                stmt.SetParameter(27, (int)parms[47]);
                return;
             case 23 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 24 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 25 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 26 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 27 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 28 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 29 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 31 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 32 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 33 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 35 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 36 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 37 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 38 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(6, (DateTime)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[14]);
                }
                return;
             case 39 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(3, (DateTime)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[10]);
                }
                stmt.SetParameter(7, (int)parms[11]);
                stmt.SetParameter(8, (int)parms[12]);
                return;
             case 40 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 41 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 42 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 43 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 46 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[2]);
                }
                return;
             case 47 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 49 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 50 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
