/*
               File: PRC_TipoDeDiasDoPrazo
        Description: Tipo De Dias Do Prazo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:18.82
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_tipodediasdoprazo : GXProcedure
   {
      public prc_tipodediasdoprazo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_tipodediasdoprazo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratoServicos_Codigo ,
                           out String aP1_TipoDias )
      {
         this.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV8TipoDias = "" ;
         initialize();
         executePrivate();
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP1_TipoDias=this.AV8TipoDias;
      }

      public String executeUdp( ref int aP0_ContratoServicos_Codigo )
      {
         this.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV8TipoDias = "" ;
         initialize();
         executePrivate();
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP1_TipoDias=this.AV8TipoDias;
         return AV8TipoDias ;
      }

      public void executeSubmit( ref int aP0_ContratoServicos_Codigo ,
                                 out String aP1_TipoDias )
      {
         prc_tipodediasdoprazo objprc_tipodediasdoprazo;
         objprc_tipodediasdoprazo = new prc_tipodediasdoprazo();
         objprc_tipodediasdoprazo.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         objprc_tipodediasdoprazo.AV8TipoDias = "" ;
         objprc_tipodediasdoprazo.context.SetSubmitInitialConfig(context);
         objprc_tipodediasdoprazo.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_tipodediasdoprazo);
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP1_TipoDias=this.AV8TipoDias;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_tipodediasdoprazo)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00AI2 */
         pr_default.execute(0, new Object[] {A160ContratoServicos_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1454ContratoServicos_PrazoTpDias = P00AI2_A1454ContratoServicos_PrazoTpDias[0];
            n1454ContratoServicos_PrazoTpDias = P00AI2_n1454ContratoServicos_PrazoTpDias[0];
            AV8TipoDias = A1454ContratoServicos_PrazoTpDias;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00AI2_A160ContratoServicos_Codigo = new int[1] ;
         P00AI2_A1454ContratoServicos_PrazoTpDias = new String[] {""} ;
         P00AI2_n1454ContratoServicos_PrazoTpDias = new bool[] {false} ;
         A1454ContratoServicos_PrazoTpDias = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_tipodediasdoprazo__default(),
            new Object[][] {
                new Object[] {
               P00AI2_A160ContratoServicos_Codigo, P00AI2_A1454ContratoServicos_PrazoTpDias, P00AI2_n1454ContratoServicos_PrazoTpDias
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A160ContratoServicos_Codigo ;
      private String AV8TipoDias ;
      private String scmdbuf ;
      private String A1454ContratoServicos_PrazoTpDias ;
      private bool n1454ContratoServicos_PrazoTpDias ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratoServicos_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00AI2_A160ContratoServicos_Codigo ;
      private String[] P00AI2_A1454ContratoServicos_PrazoTpDias ;
      private bool[] P00AI2_n1454ContratoServicos_PrazoTpDias ;
      private String aP1_TipoDias ;
   }

   public class prc_tipodediasdoprazo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00AI2 ;
          prmP00AI2 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00AI2", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicos_PrazoTpDias] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AI2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
