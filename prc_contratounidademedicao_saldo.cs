/*
               File: Prc_ContratoUnidadeMedicao_Saldo
        Description: Saldo do Contrato por Unidade Medi��o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:55.52
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_contratounidademedicao_saldo : GXProcedure
   {
      public prc_contratounidademedicao_saldo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_contratounidademedicao_saldo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contrato_Codigo ,
                           int aP1_SaldoContrato_UnidadeMedicao_Codigo ,
                           out decimal aP2_Contrato_Saldo )
      {
         this.AV8Contrato_Codigo = aP0_Contrato_Codigo;
         this.AV10SaldoContrato_UnidadeMedicao_Codigo = aP1_SaldoContrato_UnidadeMedicao_Codigo;
         this.AV9Contrato_Saldo = 0 ;
         initialize();
         executePrivate();
         aP2_Contrato_Saldo=this.AV9Contrato_Saldo;
      }

      public decimal executeUdp( int aP0_Contrato_Codigo ,
                                 int aP1_SaldoContrato_UnidadeMedicao_Codigo )
      {
         this.AV8Contrato_Codigo = aP0_Contrato_Codigo;
         this.AV10SaldoContrato_UnidadeMedicao_Codigo = aP1_SaldoContrato_UnidadeMedicao_Codigo;
         this.AV9Contrato_Saldo = 0 ;
         initialize();
         executePrivate();
         aP2_Contrato_Saldo=this.AV9Contrato_Saldo;
         return AV9Contrato_Saldo ;
      }

      public void executeSubmit( int aP0_Contrato_Codigo ,
                                 int aP1_SaldoContrato_UnidadeMedicao_Codigo ,
                                 out decimal aP2_Contrato_Saldo )
      {
         prc_contratounidademedicao_saldo objprc_contratounidademedicao_saldo;
         objprc_contratounidademedicao_saldo = new prc_contratounidademedicao_saldo();
         objprc_contratounidademedicao_saldo.AV8Contrato_Codigo = aP0_Contrato_Codigo;
         objprc_contratounidademedicao_saldo.AV10SaldoContrato_UnidadeMedicao_Codigo = aP1_SaldoContrato_UnidadeMedicao_Codigo;
         objprc_contratounidademedicao_saldo.AV9Contrato_Saldo = 0 ;
         objprc_contratounidademedicao_saldo.context.SetSubmitInitialConfig(context);
         objprc_contratounidademedicao_saldo.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_contratounidademedicao_saldo);
         aP2_Contrato_Saldo=this.AV9Contrato_Saldo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_contratounidademedicao_saldo)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00D52 */
         pr_default.execute(0, new Object[] {AV8Contrato_Codigo, AV10SaldoContrato_UnidadeMedicao_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1781SaldoContrato_Ativo = P00D52_A1781SaldoContrato_Ativo[0];
            A1783SaldoContrato_UnidadeMedicao_Codigo = P00D52_A1783SaldoContrato_UnidadeMedicao_Codigo[0];
            A74Contrato_Codigo = P00D52_A74Contrato_Codigo[0];
            A1574SaldoContrato_Reservado = P00D52_A1574SaldoContrato_Reservado[0];
            A1576SaldoContrato_Saldo = P00D52_A1576SaldoContrato_Saldo[0];
            A1561SaldoContrato_Codigo = P00D52_A1561SaldoContrato_Codigo[0];
            AV9Contrato_Saldo = (decimal)((A1576SaldoContrato_Saldo-A1574SaldoContrato_Reservado));
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00D52_A1781SaldoContrato_Ativo = new bool[] {false} ;
         P00D52_A1783SaldoContrato_UnidadeMedicao_Codigo = new int[1] ;
         P00D52_A74Contrato_Codigo = new int[1] ;
         P00D52_A1574SaldoContrato_Reservado = new decimal[1] ;
         P00D52_A1576SaldoContrato_Saldo = new decimal[1] ;
         P00D52_A1561SaldoContrato_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_contratounidademedicao_saldo__default(),
            new Object[][] {
                new Object[] {
               P00D52_A1781SaldoContrato_Ativo, P00D52_A1783SaldoContrato_UnidadeMedicao_Codigo, P00D52_A74Contrato_Codigo, P00D52_A1574SaldoContrato_Reservado, P00D52_A1576SaldoContrato_Saldo, P00D52_A1561SaldoContrato_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8Contrato_Codigo ;
      private int AV10SaldoContrato_UnidadeMedicao_Codigo ;
      private int A1783SaldoContrato_UnidadeMedicao_Codigo ;
      private int A74Contrato_Codigo ;
      private int A1561SaldoContrato_Codigo ;
      private decimal AV9Contrato_Saldo ;
      private decimal A1574SaldoContrato_Reservado ;
      private decimal A1576SaldoContrato_Saldo ;
      private String scmdbuf ;
      private bool A1781SaldoContrato_Ativo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private bool[] P00D52_A1781SaldoContrato_Ativo ;
      private int[] P00D52_A1783SaldoContrato_UnidadeMedicao_Codigo ;
      private int[] P00D52_A74Contrato_Codigo ;
      private decimal[] P00D52_A1574SaldoContrato_Reservado ;
      private decimal[] P00D52_A1576SaldoContrato_Saldo ;
      private int[] P00D52_A1561SaldoContrato_Codigo ;
      private decimal aP2_Contrato_Saldo ;
   }

   public class prc_contratounidademedicao_saldo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00D52 ;
          prmP00D52 = new Object[] {
          new Object[] {"@AV8Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10SaldoContrato_UnidadeMedicao_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00D52", "SELECT [SaldoContrato_Ativo], [SaldoContrato_UnidadeMedicao_Codigo], [Contrato_Codigo], [SaldoContrato_Reservado], [SaldoContrato_Saldo], [SaldoContrato_Codigo] FROM [SaldoContrato] WITH (NOLOCK) WHERE ([Contrato_Codigo] = @AV8Contrato_Codigo) AND ([SaldoContrato_Ativo] = 1) AND ([SaldoContrato_UnidadeMedicao_Codigo] = @AV10SaldoContrato_UnidadeMedicao_Codigo) ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00D52,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
