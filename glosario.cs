/*
               File: Glosario
        Description: Gloss�rio
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/6/2020 23:55:42.84
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class glosario : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_13") == 0 )
         {
            A1346Glosario_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1346Glosario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1346Glosario_AreaTrabalhoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_13( A1346Glosario_AreaTrabalhoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV11Glosario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Glosario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Glosario_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vGLOSARIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11Glosario_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Gloss�rio", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtGlosario_Termo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public glosario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public glosario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Glosario_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV11Glosario_Codigo = aP1_Glosario_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2P163( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2P163e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2P163( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2P163( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2P163e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_39_2P163( true) ;
         }
         return  ;
      }

      protected void wb_table3_39_2P163e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2P163e( true) ;
         }
         else
         {
            wb_table1_2_2P163e( false) ;
         }
      }

      protected void wb_table3_39_2P163( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Glosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Glosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Glosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_39_2P163e( true) ;
         }
         else
         {
            wb_table3_39_2P163e( false) ;
         }
      }

      protected void wb_table2_5_2P163( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_2P163( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_2P163e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2P163e( true) ;
         }
         else
         {
            wb_table2_5_2P163e( false) ;
         }
      }

      protected void wb_table4_13_2P163( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "Table", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockglosario_termo_Internalname, "Termo", "", "", lblTextblockglosario_termo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Glosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtGlosario_Termo_Internalname, A858Glosario_Termo, StringUtil.RTrim( context.localUtil.Format( A858Glosario_Termo, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGlosario_Termo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtGlosario_Termo_Enabled, 0, "text", "", 570, "px", 1, "row", 255, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Glosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockglosario_descricao_Internalname, "Descri��o", "", "", lblTextblockglosario_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Glosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtGlosario_Descricao_Internalname, A859Glosario_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", 0, 1, edtGlosario_Descricao_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_Glosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockglosario_arquivo_Internalname, "Arquivo", "", "", lblTextblockglosario_arquivo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Glosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            ClassString = "BootstrapAttribute";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            edtGlosario_Arquivo_Filename = A1343Glosario_NomeArq;
            edtGlosario_Arquivo_Filetype = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Arquivo_Internalname, "Filetype", edtGlosario_Arquivo_Filetype);
            edtGlosario_Arquivo_Filetype = A1344Glosario_TipoArq;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Arquivo_Internalname, "Filetype", edtGlosario_Arquivo_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1342Glosario_Arquivo)) )
            {
               gxblobfileaux.Source = A1342Glosario_Arquivo;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtGlosario_Arquivo_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtGlosario_Arquivo_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  A1342Glosario_Arquivo = gxblobfileaux.GetAbsoluteName();
                  n1342Glosario_Arquivo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1342Glosario_Arquivo", A1342Glosario_Arquivo);
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A1342Glosario_Arquivo));
                  edtGlosario_Arquivo_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Arquivo_Internalname, "Filetype", edtGlosario_Arquivo_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A1342Glosario_Arquivo));
            }
            GxWebStd.gx_blob_field( context, edtGlosario_Arquivo_Internalname, StringUtil.RTrim( A1342Glosario_Arquivo), context.PathToRelativeUrl( A1342Glosario_Arquivo), (String.IsNullOrEmpty(StringUtil.RTrim( edtGlosario_Arquivo_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtGlosario_Arquivo_Filetype)) ? A1342Glosario_Arquivo : edtGlosario_Arquivo_Filetype)) : edtGlosario_Arquivo_Contenttype), true, edtGlosario_Arquivo_Linktarget, edtGlosario_Arquivo_Parameters, edtGlosario_Arquivo_Display, edtGlosario_Arquivo_Enabled, 1, "", edtGlosario_Arquivo_Tooltiptext, 0, -1, 250, "px", 60, "px", 0, 0, 0, edtGlosario_Arquivo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", "", "HLP_Glosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockglosario_nomearq_Internalname, "Nome de Arquivo", "", "", lblTextblockglosario_nomearq_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockglosario_nomearq_Visible, 1, 0, "HLP_Glosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtGlosario_NomeArq_Internalname, StringUtil.RTrim( A1343Glosario_NomeArq), StringUtil.RTrim( context.localUtil.Format( A1343Glosario_NomeArq, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGlosario_NomeArq_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtGlosario_NomeArq_Visible, edtGlosario_NomeArq_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "NomeArq", "left", true, "HLP_Glosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockglosario_tipoarq_Internalname, "Tipo de Arquivo", "", "", lblTextblockglosario_tipoarq_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockglosario_tipoarq_Visible, 1, 0, "HLP_Glosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtGlosario_TipoArq_Internalname, StringUtil.RTrim( A1344Glosario_TipoArq), StringUtil.RTrim( context.localUtil.Format( A1344Glosario_TipoArq, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGlosario_TipoArq_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtGlosario_TipoArq_Visible, edtGlosario_TipoArq_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "TipoArq", "left", true, "HLP_Glosario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_2P163e( true) ;
         }
         else
         {
            wb_table4_13_2P163e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E112P2 */
         E112P2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A858Glosario_Termo = cgiGet( edtGlosario_Termo_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A858Glosario_Termo", A858Glosario_Termo);
               A859Glosario_Descricao = cgiGet( edtGlosario_Descricao_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A859Glosario_Descricao", A859Glosario_Descricao);
               A1342Glosario_Arquivo = cgiGet( edtGlosario_Arquivo_Internalname);
               n1342Glosario_Arquivo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1342Glosario_Arquivo", A1342Glosario_Arquivo);
               n1342Glosario_Arquivo = (String.IsNullOrEmpty(StringUtil.RTrim( A1342Glosario_Arquivo)) ? true : false);
               /* Read saved values. */
               Z1347Glosario_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1347Glosario_Codigo"), ",", "."));
               Z858Glosario_Termo = cgiGet( "Z858Glosario_Termo");
               Z1346Glosario_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z1346Glosario_AreaTrabalhoCod"), ",", "."));
               A1346Glosario_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z1346Glosario_AreaTrabalhoCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N1346Glosario_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "N1346Glosario_AreaTrabalhoCod"), ",", "."));
               AV11Glosario_Codigo = (int)(context.localUtil.CToN( cgiGet( "vGLOSARIO_CODIGO"), ",", "."));
               A1347Glosario_Codigo = (int)(context.localUtil.CToN( cgiGet( "GLOSARIO_CODIGO"), ",", "."));
               AV12Insert_Glosario_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_GLOSARIO_AREATRABALHOCOD"), ",", "."));
               A1346Glosario_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "GLOSARIO_AREATRABALHOCOD"), ",", "."));
               AV14Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               edtGlosario_Arquivo_Filetype = cgiGet( "GLOSARIO_ARQUIVO_Filetype");
               edtGlosario_Arquivo_Filename = cgiGet( "GLOSARIO_ARQUIVO_Filename");
               edtGlosario_Arquivo_Filename = cgiGet( "GLOSARIO_ARQUIVO_Filename");
               edtGlosario_Arquivo_Filetype = cgiGet( "GLOSARIO_ARQUIVO_Filetype");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1342Glosario_Arquivo)) )
               {
                  edtGlosario_Arquivo_Filename = (String)(CGIGetFileName(edtGlosario_Arquivo_Internalname));
                  edtGlosario_Arquivo_Filetype = (String)(CGIGetFileType(edtGlosario_Arquivo_Internalname));
               }
               A1344Glosario_TipoArq = edtGlosario_Arquivo_Filetype;
               n1344Glosario_TipoArq = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1344Glosario_TipoArq", A1344Glosario_TipoArq);
               A1343Glosario_NomeArq = edtGlosario_Arquivo_Filename;
               n1343Glosario_NomeArq = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1343Glosario_NomeArq", A1343Glosario_NomeArq);
               if ( String.IsNullOrEmpty(StringUtil.RTrim( A1342Glosario_Arquivo)) )
               {
                  GXCCtlgxBlob = "GLOSARIO_ARQUIVO" + "_gxBlob";
                  A1342Glosario_Arquivo = cgiGet( GXCCtlgxBlob);
                  n1342Glosario_Arquivo = false;
                  n1342Glosario_Arquivo = (String.IsNullOrEmpty(StringUtil.RTrim( A1342Glosario_Arquivo)) ? true : false);
               }
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Glosario";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1347Glosario_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1346Glosario_AreaTrabalhoCod), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("glosario:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("glosario:[SecurityCheckFailed value for]"+"Glosario_Codigo:"+context.localUtil.Format( (decimal)(A1347Glosario_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("glosario:[SecurityCheckFailed value for]"+"Glosario_AreaTrabalhoCod:"+context.localUtil.Format( (decimal)(A1346Glosario_AreaTrabalhoCod), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1347Glosario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1347Glosario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1347Glosario_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode163 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode163;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound163 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_2P0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E112P2 */
                           E112P2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E122P2 */
                           E122P2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E122P2 */
            E122P2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2P163( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes2P163( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_2P0( )
      {
         BeforeValidate2P163( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2P163( ) ;
            }
            else
            {
               CheckExtendedTable2P163( ) ;
               CloseExtendedTableCursors2P163( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption2P0( )
      {
      }

      protected void E112P2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV14Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV15GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            while ( AV15GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV15GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Glosario_AreaTrabalhoCod") == 0 )
               {
                  AV12Insert_Glosario_AreaTrabalhoCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_Glosario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_Glosario_AreaTrabalhoCod), 6, 0)));
               }
               AV15GXV1 = (int)(AV15GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            }
         }
         edtGlosario_Arquivo_Display = 1;
         edtGlosario_Arquivo_Tooltiptext = "Arquivo anexo";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Arquivo_Internalname, "Tooltiptext", edtGlosario_Arquivo_Tooltiptext);
         edtGlosario_Arquivo_Linktarget = "_blank";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Arquivo_Internalname, "Linktarget", edtGlosario_Arquivo_Linktarget);
      }

      protected void E122P2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwglosario.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM2P163( short GX_JID )
      {
         if ( ( GX_JID == 12 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z858Glosario_Termo = T002P3_A858Glosario_Termo[0];
               Z1346Glosario_AreaTrabalhoCod = T002P3_A1346Glosario_AreaTrabalhoCod[0];
            }
            else
            {
               Z858Glosario_Termo = A858Glosario_Termo;
               Z1346Glosario_AreaTrabalhoCod = A1346Glosario_AreaTrabalhoCod;
            }
         }
         if ( GX_JID == -12 )
         {
            Z1347Glosario_Codigo = A1347Glosario_Codigo;
            Z858Glosario_Termo = A858Glosario_Termo;
            Z859Glosario_Descricao = A859Glosario_Descricao;
            Z1342Glosario_Arquivo = A1342Glosario_Arquivo;
            Z1344Glosario_TipoArq = A1344Glosario_TipoArq;
            Z1343Glosario_NomeArq = A1343Glosario_NomeArq;
            Z1346Glosario_AreaTrabalhoCod = A1346Glosario_AreaTrabalhoCod;
         }
      }

      protected void standaloneNotModal( )
      {
         edtGlosario_NomeArq_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_NomeArq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGlosario_NomeArq_Enabled), 5, 0)));
         edtGlosario_TipoArq_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_TipoArq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGlosario_TipoArq_Enabled), 5, 0)));
         AV14Pgmname = "Glosario";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
         edtGlosario_NomeArq_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_NomeArq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGlosario_NomeArq_Enabled), 5, 0)));
         edtGlosario_TipoArq_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_TipoArq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGlosario_TipoArq_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV11Glosario_Codigo) )
         {
            A1347Glosario_Codigo = AV11Glosario_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1347Glosario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1347Glosario_Codigo), 6, 0)));
         }
      }

      protected void standaloneModal( )
      {
         edtGlosario_NomeArq_Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_NomeArq_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGlosario_NomeArq_Visible), 5, 0)));
         lblTextblockglosario_nomearq_Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockglosario_nomearq_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockglosario_nomearq_Visible), 5, 0)));
         edtGlosario_TipoArq_Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_TipoArq_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGlosario_TipoArq_Visible), 5, 0)));
         lblTextblockglosario_tipoarq_Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockglosario_tipoarq_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockglosario_tipoarq_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Glosario_AreaTrabalhoCod) )
         {
            A1346Glosario_AreaTrabalhoCod = AV12Insert_Glosario_AreaTrabalhoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1346Glosario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1346Glosario_AreaTrabalhoCod), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && (0==AV12Insert_Glosario_AreaTrabalhoCod) )
            {
               A1346Glosario_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1346Glosario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1346Glosario_AreaTrabalhoCod), 6, 0)));
            }
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
      }

      protected void Load2P163( )
      {
         /* Using cursor T002P5 */
         pr_default.execute(3, new Object[] {A1347Glosario_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound163 = 1;
            A858Glosario_Termo = T002P5_A858Glosario_Termo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A858Glosario_Termo", A858Glosario_Termo);
            A859Glosario_Descricao = T002P5_A859Glosario_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A859Glosario_Descricao", A859Glosario_Descricao);
            A1344Glosario_TipoArq = T002P5_A1344Glosario_TipoArq[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1344Glosario_TipoArq", A1344Glosario_TipoArq);
            n1344Glosario_TipoArq = T002P5_n1344Glosario_TipoArq[0];
            edtGlosario_Arquivo_Filetype = A1344Glosario_TipoArq;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Arquivo_Internalname, "Filetype", edtGlosario_Arquivo_Filetype);
            A1343Glosario_NomeArq = T002P5_A1343Glosario_NomeArq[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1343Glosario_NomeArq", A1343Glosario_NomeArq);
            n1343Glosario_NomeArq = T002P5_n1343Glosario_NomeArq[0];
            edtGlosario_Arquivo_Filename = A1343Glosario_NomeArq;
            A1346Glosario_AreaTrabalhoCod = T002P5_A1346Glosario_AreaTrabalhoCod[0];
            A1342Glosario_Arquivo = T002P5_A1342Glosario_Arquivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1342Glosario_Arquivo", A1342Glosario_Arquivo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A1342Glosario_Arquivo));
            n1342Glosario_Arquivo = T002P5_n1342Glosario_Arquivo[0];
            ZM2P163( -12) ;
         }
         pr_default.close(3);
         OnLoadActions2P163( ) ;
      }

      protected void OnLoadActions2P163( )
      {
      }

      protected void CheckExtendedTable2P163( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T002P4 */
         pr_default.execute(2, new Object[] {A1346Glosario_AreaTrabalhoCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Glosario_Area Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors2P163( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_13( int A1346Glosario_AreaTrabalhoCod )
      {
         /* Using cursor T002P6 */
         pr_default.execute(4, new Object[] {A1346Glosario_AreaTrabalhoCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Glosario_Area Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey2P163( )
      {
         /* Using cursor T002P7 */
         pr_default.execute(5, new Object[] {A1347Glosario_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound163 = 1;
         }
         else
         {
            RcdFound163 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T002P3 */
         pr_default.execute(1, new Object[] {A1347Glosario_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2P163( 12) ;
            RcdFound163 = 1;
            A1347Glosario_Codigo = T002P3_A1347Glosario_Codigo[0];
            A858Glosario_Termo = T002P3_A858Glosario_Termo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A858Glosario_Termo", A858Glosario_Termo);
            A859Glosario_Descricao = T002P3_A859Glosario_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A859Glosario_Descricao", A859Glosario_Descricao);
            A1344Glosario_TipoArq = T002P3_A1344Glosario_TipoArq[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1344Glosario_TipoArq", A1344Glosario_TipoArq);
            n1344Glosario_TipoArq = T002P3_n1344Glosario_TipoArq[0];
            edtGlosario_Arquivo_Filetype = A1344Glosario_TipoArq;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Arquivo_Internalname, "Filetype", edtGlosario_Arquivo_Filetype);
            A1343Glosario_NomeArq = T002P3_A1343Glosario_NomeArq[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1343Glosario_NomeArq", A1343Glosario_NomeArq);
            n1343Glosario_NomeArq = T002P3_n1343Glosario_NomeArq[0];
            edtGlosario_Arquivo_Filename = A1343Glosario_NomeArq;
            A1346Glosario_AreaTrabalhoCod = T002P3_A1346Glosario_AreaTrabalhoCod[0];
            A1342Glosario_Arquivo = T002P3_A1342Glosario_Arquivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1342Glosario_Arquivo", A1342Glosario_Arquivo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A1342Glosario_Arquivo));
            n1342Glosario_Arquivo = T002P3_n1342Glosario_Arquivo[0];
            Z1347Glosario_Codigo = A1347Glosario_Codigo;
            sMode163 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load2P163( ) ;
            if ( AnyError == 1 )
            {
               RcdFound163 = 0;
               InitializeNonKey2P163( ) ;
            }
            Gx_mode = sMode163;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound163 = 0;
            InitializeNonKey2P163( ) ;
            sMode163 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode163;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2P163( ) ;
         if ( RcdFound163 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound163 = 0;
         /* Using cursor T002P8 */
         pr_default.execute(6, new Object[] {A1347Glosario_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T002P8_A1347Glosario_Codigo[0] < A1347Glosario_Codigo ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T002P8_A1347Glosario_Codigo[0] > A1347Glosario_Codigo ) ) )
            {
               A1347Glosario_Codigo = T002P8_A1347Glosario_Codigo[0];
               RcdFound163 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound163 = 0;
         /* Using cursor T002P9 */
         pr_default.execute(7, new Object[] {A1347Glosario_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T002P9_A1347Glosario_Codigo[0] > A1347Glosario_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T002P9_A1347Glosario_Codigo[0] < A1347Glosario_Codigo ) ) )
            {
               A1347Glosario_Codigo = T002P9_A1347Glosario_Codigo[0];
               RcdFound163 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2P163( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtGlosario_Termo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2P163( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound163 == 1 )
            {
               if ( A1347Glosario_Codigo != Z1347Glosario_Codigo )
               {
                  A1347Glosario_Codigo = Z1347Glosario_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1347Glosario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1347Glosario_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtGlosario_Termo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update2P163( ) ;
                  GX_FocusControl = edtGlosario_Termo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1347Glosario_Codigo != Z1347Glosario_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtGlosario_Termo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2P163( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                     AnyError = 1;
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtGlosario_Termo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2P163( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A1347Glosario_Codigo != Z1347Glosario_Codigo )
         {
            A1347Glosario_Codigo = Z1347Glosario_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1347Glosario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1347Glosario_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "");
            AnyError = 1;
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtGlosario_Termo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency2P163( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T002P2 */
            pr_default.execute(0, new Object[] {A1347Glosario_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Glosario"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z858Glosario_Termo, T002P2_A858Glosario_Termo[0]) != 0 ) || ( Z1346Glosario_AreaTrabalhoCod != T002P2_A1346Glosario_AreaTrabalhoCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z858Glosario_Termo, T002P2_A858Glosario_Termo[0]) != 0 )
               {
                  GXUtil.WriteLog("glosario:[seudo value changed for attri]"+"Glosario_Termo");
                  GXUtil.WriteLogRaw("Old: ",Z858Glosario_Termo);
                  GXUtil.WriteLogRaw("Current: ",T002P2_A858Glosario_Termo[0]);
               }
               if ( Z1346Glosario_AreaTrabalhoCod != T002P2_A1346Glosario_AreaTrabalhoCod[0] )
               {
                  GXUtil.WriteLog("glosario:[seudo value changed for attri]"+"Glosario_AreaTrabalhoCod");
                  GXUtil.WriteLogRaw("Old: ",Z1346Glosario_AreaTrabalhoCod);
                  GXUtil.WriteLogRaw("Current: ",T002P2_A1346Glosario_AreaTrabalhoCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Glosario"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2P163( )
      {
         BeforeValidate2P163( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2P163( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2P163( 0) ;
            CheckOptimisticConcurrency2P163( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2P163( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2P163( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002P10 */
                     A1344Glosario_TipoArq = edtGlosario_Arquivo_Filetype;
                     n1344Glosario_TipoArq = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1344Glosario_TipoArq", A1344Glosario_TipoArq);
                     A1343Glosario_NomeArq = edtGlosario_Arquivo_Filename;
                     n1343Glosario_NomeArq = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1343Glosario_NomeArq", A1343Glosario_NomeArq);
                     pr_default.execute(8, new Object[] {A858Glosario_Termo, A859Glosario_Descricao, n1342Glosario_Arquivo, A1342Glosario_Arquivo, n1344Glosario_TipoArq, A1344Glosario_TipoArq, n1343Glosario_NomeArq, A1343Glosario_NomeArq, A1346Glosario_AreaTrabalhoCod});
                     A1347Glosario_Codigo = T002P10_A1347Glosario_Codigo[0];
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("Glosario") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption2P0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2P163( ) ;
            }
            EndLevel2P163( ) ;
         }
         CloseExtendedTableCursors2P163( ) ;
      }

      protected void Update2P163( )
      {
         BeforeValidate2P163( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2P163( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2P163( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2P163( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2P163( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002P11 */
                     A1344Glosario_TipoArq = edtGlosario_Arquivo_Filetype;
                     n1344Glosario_TipoArq = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1344Glosario_TipoArq", A1344Glosario_TipoArq);
                     A1343Glosario_NomeArq = edtGlosario_Arquivo_Filename;
                     n1343Glosario_NomeArq = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1343Glosario_NomeArq", A1343Glosario_NomeArq);
                     pr_default.execute(9, new Object[] {A858Glosario_Termo, A859Glosario_Descricao, n1344Glosario_TipoArq, A1344Glosario_TipoArq, n1343Glosario_NomeArq, A1343Glosario_NomeArq, A1346Glosario_AreaTrabalhoCod, A1347Glosario_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("Glosario") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Glosario"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2P163( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2P163( ) ;
         }
         CloseExtendedTableCursors2P163( ) ;
      }

      protected void DeferredUpdate2P163( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor T002P12 */
            pr_default.execute(10, new Object[] {n1342Glosario_Arquivo, A1342Glosario_Arquivo, A1347Glosario_Codigo});
            pr_default.close(10);
            dsDefault.SmartCacheProvider.SetUpdated("Glosario") ;
         }
      }

      protected void delete( )
      {
         BeforeValidate2P163( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2P163( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2P163( ) ;
            AfterConfirm2P163( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2P163( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002P13 */
                  pr_default.execute(11, new Object[] {A1347Glosario_Codigo});
                  pr_default.close(11);
                  dsDefault.SmartCacheProvider.SetUpdated("Glosario") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode163 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel2P163( ) ;
         Gx_mode = sMode163;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls2P163( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel2P163( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2P163( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "Glosario");
            if ( AnyError == 0 )
            {
               ConfirmValues2P0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "Glosario");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2P163( )
      {
         /* Scan By routine */
         /* Using cursor T002P14 */
         pr_default.execute(12);
         RcdFound163 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound163 = 1;
            A1347Glosario_Codigo = T002P14_A1347Glosario_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2P163( )
      {
         /* Scan next routine */
         pr_default.readNext(12);
         RcdFound163 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound163 = 1;
            A1347Glosario_Codigo = T002P14_A1347Glosario_Codigo[0];
         }
      }

      protected void ScanEnd2P163( )
      {
         pr_default.close(12);
      }

      protected void AfterConfirm2P163( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2P163( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2P163( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2P163( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2P163( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2P163( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2P163( )
      {
         edtGlosario_Termo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Termo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGlosario_Termo_Enabled), 5, 0)));
         edtGlosario_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGlosario_Descricao_Enabled), 5, 0)));
         edtGlosario_Arquivo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Arquivo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGlosario_Arquivo_Enabled), 5, 0)));
         edtGlosario_NomeArq_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_NomeArq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGlosario_NomeArq_Enabled), 5, 0)));
         edtGlosario_TipoArq_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_TipoArq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGlosario_TipoArq_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues2P0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205623554371");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("glosario.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV11Glosario_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1347Glosario_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1347Glosario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z858Glosario_Termo", Z858Glosario_Termo);
         GxWebStd.gx_hidden_field( context, "Z1346Glosario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1346Glosario_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N1346Glosario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1346Glosario_AreaTrabalhoCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vGLOSARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Glosario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GLOSARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1347Glosario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_GLOSARIO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Insert_Glosario_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GLOSARIO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1346Glosario_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV14Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vGLOSARIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11Glosario_Codigo), "ZZZZZ9")));
         GXCCtlgxBlob = "GLOSARIO_ARQUIVO" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, A1342Glosario_Arquivo);
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GxWebStd.gx_hidden_field( context, "GLOSARIO_ARQUIVO_Filetype", StringUtil.RTrim( edtGlosario_Arquivo_Filetype));
         GxWebStd.gx_hidden_field( context, "GLOSARIO_ARQUIVO_Filename", StringUtil.RTrim( edtGlosario_Arquivo_Filename));
         GxWebStd.gx_hidden_field( context, "GLOSARIO_ARQUIVO_Filename", StringUtil.RTrim( edtGlosario_Arquivo_Filename));
         GxWebStd.gx_hidden_field( context, "GLOSARIO_ARQUIVO_Filetype", StringUtil.RTrim( edtGlosario_Arquivo_Filetype));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Glosario";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1347Glosario_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1346Glosario_AreaTrabalhoCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("glosario:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("glosario:[SendSecurityCheck value for]"+"Glosario_Codigo:"+context.localUtil.Format( (decimal)(A1347Glosario_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("glosario:[SendSecurityCheck value for]"+"Glosario_AreaTrabalhoCod:"+context.localUtil.Format( (decimal)(A1346Glosario_AreaTrabalhoCod), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("glosario.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV11Glosario_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Glosario" ;
      }

      public override String GetPgmdesc( )
      {
         return "Gloss�rio" ;
      }

      protected void InitializeNonKey2P163( )
      {
         A1346Glosario_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1346Glosario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1346Glosario_AreaTrabalhoCod), 6, 0)));
         A858Glosario_Termo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A858Glosario_Termo", A858Glosario_Termo);
         A859Glosario_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A859Glosario_Descricao", A859Glosario_Descricao);
         A1342Glosario_Arquivo = "";
         n1342Glosario_Arquivo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1342Glosario_Arquivo", A1342Glosario_Arquivo);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGlosario_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A1342Glosario_Arquivo));
         n1342Glosario_Arquivo = (String.IsNullOrEmpty(StringUtil.RTrim( A1342Glosario_Arquivo)) ? true : false);
         A1344Glosario_TipoArq = "";
         n1344Glosario_TipoArq = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1344Glosario_TipoArq", A1344Glosario_TipoArq);
         n1344Glosario_TipoArq = (String.IsNullOrEmpty(StringUtil.RTrim( A1344Glosario_TipoArq)) ? true : false);
         A1343Glosario_NomeArq = "";
         n1343Glosario_NomeArq = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1343Glosario_NomeArq", A1343Glosario_NomeArq);
         n1343Glosario_NomeArq = (String.IsNullOrEmpty(StringUtil.RTrim( A1343Glosario_NomeArq)) ? true : false);
         Z858Glosario_Termo = "";
         Z1346Glosario_AreaTrabalhoCod = 0;
      }

      protected void InitAll2P163( )
      {
         A1347Glosario_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1347Glosario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1347Glosario_Codigo), 6, 0)));
         InitializeNonKey2P163( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205623554388");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("glosario.js", "?20205623554388");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockglosario_termo_Internalname = "TEXTBLOCKGLOSARIO_TERMO";
         edtGlosario_Termo_Internalname = "GLOSARIO_TERMO";
         lblTextblockglosario_descricao_Internalname = "TEXTBLOCKGLOSARIO_DESCRICAO";
         edtGlosario_Descricao_Internalname = "GLOSARIO_DESCRICAO";
         lblTextblockglosario_arquivo_Internalname = "TEXTBLOCKGLOSARIO_ARQUIVO";
         edtGlosario_Arquivo_Internalname = "GLOSARIO_ARQUIVO";
         lblTextblockglosario_nomearq_Internalname = "TEXTBLOCKGLOSARIO_NOMEARQ";
         edtGlosario_NomeArq_Internalname = "GLOSARIO_NOMEARQ";
         lblTextblockglosario_tipoarq_Internalname = "TEXTBLOCKGLOSARIO_TIPOARQ";
         edtGlosario_TipoArq_Internalname = "GLOSARIO_TIPOARQ";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Glosario";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Gloss�rio";
         edtGlosario_Arquivo_Filename = "";
         edtGlosario_TipoArq_Jsonclick = "";
         edtGlosario_TipoArq_Enabled = 0;
         edtGlosario_TipoArq_Visible = 1;
         lblTextblockglosario_tipoarq_Visible = 1;
         edtGlosario_NomeArq_Jsonclick = "";
         edtGlosario_NomeArq_Enabled = 0;
         edtGlosario_NomeArq_Visible = 1;
         lblTextblockglosario_nomearq_Visible = 1;
         edtGlosario_Arquivo_Jsonclick = "";
         edtGlosario_Arquivo_Parameters = "";
         edtGlosario_Arquivo_Contenttype = "";
         edtGlosario_Arquivo_Filetype = "";
         edtGlosario_Arquivo_Linktarget = "";
         edtGlosario_Arquivo_Tooltiptext = "";
         edtGlosario_Arquivo_Display = 0;
         edtGlosario_Arquivo_Enabled = 1;
         edtGlosario_Descricao_Enabled = 1;
         edtGlosario_Termo_Jsonclick = "";
         edtGlosario_Termo_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV11Glosario_Codigo',fld:'vGLOSARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E122P2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z858Glosario_Termo = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockglosario_termo_Jsonclick = "";
         A858Glosario_Termo = "";
         lblTextblockglosario_descricao_Jsonclick = "";
         A859Glosario_Descricao = "";
         lblTextblockglosario_arquivo_Jsonclick = "";
         A1343Glosario_NomeArq = "";
         A1344Glosario_TipoArq = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         A1342Glosario_Arquivo = "";
         lblTextblockglosario_nomearq_Jsonclick = "";
         lblTextblockglosario_tipoarq_Jsonclick = "";
         AV14Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         GXCCtlgxBlob = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode163 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z859Glosario_Descricao = "";
         Z1342Glosario_Arquivo = "";
         Z1344Glosario_TipoArq = "";
         Z1343Glosario_NomeArq = "";
         T002P5_A1347Glosario_Codigo = new int[1] ;
         T002P5_A858Glosario_Termo = new String[] {""} ;
         T002P5_A859Glosario_Descricao = new String[] {""} ;
         T002P5_A1344Glosario_TipoArq = new String[] {""} ;
         T002P5_n1344Glosario_TipoArq = new bool[] {false} ;
         T002P5_A1343Glosario_NomeArq = new String[] {""} ;
         T002P5_n1343Glosario_NomeArq = new bool[] {false} ;
         T002P5_A1346Glosario_AreaTrabalhoCod = new int[1] ;
         T002P5_A1342Glosario_Arquivo = new String[] {""} ;
         T002P5_n1342Glosario_Arquivo = new bool[] {false} ;
         T002P4_A1346Glosario_AreaTrabalhoCod = new int[1] ;
         T002P6_A1346Glosario_AreaTrabalhoCod = new int[1] ;
         T002P7_A1347Glosario_Codigo = new int[1] ;
         T002P3_A1347Glosario_Codigo = new int[1] ;
         T002P3_A858Glosario_Termo = new String[] {""} ;
         T002P3_A859Glosario_Descricao = new String[] {""} ;
         T002P3_A1344Glosario_TipoArq = new String[] {""} ;
         T002P3_n1344Glosario_TipoArq = new bool[] {false} ;
         T002P3_A1343Glosario_NomeArq = new String[] {""} ;
         T002P3_n1343Glosario_NomeArq = new bool[] {false} ;
         T002P3_A1346Glosario_AreaTrabalhoCod = new int[1] ;
         T002P3_A1342Glosario_Arquivo = new String[] {""} ;
         T002P3_n1342Glosario_Arquivo = new bool[] {false} ;
         T002P8_A1347Glosario_Codigo = new int[1] ;
         T002P9_A1347Glosario_Codigo = new int[1] ;
         T002P2_A1347Glosario_Codigo = new int[1] ;
         T002P2_A858Glosario_Termo = new String[] {""} ;
         T002P2_A859Glosario_Descricao = new String[] {""} ;
         T002P2_A1344Glosario_TipoArq = new String[] {""} ;
         T002P2_n1344Glosario_TipoArq = new bool[] {false} ;
         T002P2_A1343Glosario_NomeArq = new String[] {""} ;
         T002P2_n1343Glosario_NomeArq = new bool[] {false} ;
         T002P2_A1346Glosario_AreaTrabalhoCod = new int[1] ;
         T002P2_A1342Glosario_Arquivo = new String[] {""} ;
         T002P2_n1342Glosario_Arquivo = new bool[] {false} ;
         T002P10_A1347Glosario_Codigo = new int[1] ;
         T002P14_A1347Glosario_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.glosario__default(),
            new Object[][] {
                new Object[] {
               T002P2_A1347Glosario_Codigo, T002P2_A858Glosario_Termo, T002P2_A859Glosario_Descricao, T002P2_A1344Glosario_TipoArq, T002P2_n1344Glosario_TipoArq, T002P2_A1343Glosario_NomeArq, T002P2_n1343Glosario_NomeArq, T002P2_A1346Glosario_AreaTrabalhoCod, T002P2_A1342Glosario_Arquivo, T002P2_n1342Glosario_Arquivo
               }
               , new Object[] {
               T002P3_A1347Glosario_Codigo, T002P3_A858Glosario_Termo, T002P3_A859Glosario_Descricao, T002P3_A1344Glosario_TipoArq, T002P3_n1344Glosario_TipoArq, T002P3_A1343Glosario_NomeArq, T002P3_n1343Glosario_NomeArq, T002P3_A1346Glosario_AreaTrabalhoCod, T002P3_A1342Glosario_Arquivo, T002P3_n1342Glosario_Arquivo
               }
               , new Object[] {
               T002P4_A1346Glosario_AreaTrabalhoCod
               }
               , new Object[] {
               T002P5_A1347Glosario_Codigo, T002P5_A858Glosario_Termo, T002P5_A859Glosario_Descricao, T002P5_A1344Glosario_TipoArq, T002P5_n1344Glosario_TipoArq, T002P5_A1343Glosario_NomeArq, T002P5_n1343Glosario_NomeArq, T002P5_A1346Glosario_AreaTrabalhoCod, T002P5_A1342Glosario_Arquivo, T002P5_n1342Glosario_Arquivo
               }
               , new Object[] {
               T002P6_A1346Glosario_AreaTrabalhoCod
               }
               , new Object[] {
               T002P7_A1347Glosario_Codigo
               }
               , new Object[] {
               T002P8_A1347Glosario_Codigo
               }
               , new Object[] {
               T002P9_A1347Glosario_Codigo
               }
               , new Object[] {
               T002P10_A1347Glosario_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002P14_A1347Glosario_Codigo
               }
            }
         );
         AV14Pgmname = "Glosario";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short edtGlosario_Arquivo_Display ;
      private short RcdFound163 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private int wcpOAV11Glosario_Codigo ;
      private int Z1347Glosario_Codigo ;
      private int Z1346Glosario_AreaTrabalhoCod ;
      private int N1346Glosario_AreaTrabalhoCod ;
      private int A1346Glosario_AreaTrabalhoCod ;
      private int AV11Glosario_Codigo ;
      private int trnEnded ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtGlosario_Termo_Enabled ;
      private int edtGlosario_Descricao_Enabled ;
      private int edtGlosario_Arquivo_Enabled ;
      private int lblTextblockglosario_nomearq_Visible ;
      private int edtGlosario_NomeArq_Visible ;
      private int edtGlosario_NomeArq_Enabled ;
      private int lblTextblockglosario_tipoarq_Visible ;
      private int edtGlosario_TipoArq_Visible ;
      private int edtGlosario_TipoArq_Enabled ;
      private int A1347Glosario_Codigo ;
      private int AV12Insert_Glosario_AreaTrabalhoCod ;
      private int AV15GXV1 ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtGlosario_Termo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockglosario_termo_Internalname ;
      private String lblTextblockglosario_termo_Jsonclick ;
      private String edtGlosario_Termo_Jsonclick ;
      private String lblTextblockglosario_descricao_Internalname ;
      private String lblTextblockglosario_descricao_Jsonclick ;
      private String edtGlosario_Descricao_Internalname ;
      private String lblTextblockglosario_arquivo_Internalname ;
      private String lblTextblockglosario_arquivo_Jsonclick ;
      private String edtGlosario_Arquivo_Filename ;
      private String A1343Glosario_NomeArq ;
      private String edtGlosario_Arquivo_Filetype ;
      private String edtGlosario_Arquivo_Internalname ;
      private String A1344Glosario_TipoArq ;
      private String edtGlosario_Arquivo_Contenttype ;
      private String edtGlosario_Arquivo_Linktarget ;
      private String edtGlosario_Arquivo_Parameters ;
      private String edtGlosario_Arquivo_Tooltiptext ;
      private String edtGlosario_Arquivo_Jsonclick ;
      private String lblTextblockglosario_nomearq_Internalname ;
      private String lblTextblockglosario_nomearq_Jsonclick ;
      private String edtGlosario_NomeArq_Internalname ;
      private String edtGlosario_NomeArq_Jsonclick ;
      private String lblTextblockglosario_tipoarq_Internalname ;
      private String lblTextblockglosario_tipoarq_Jsonclick ;
      private String edtGlosario_TipoArq_Internalname ;
      private String edtGlosario_TipoArq_Jsonclick ;
      private String AV14Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String GXCCtlgxBlob ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode163 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z1344Glosario_TipoArq ;
      private String Z1343Glosario_NomeArq ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1342Glosario_Arquivo ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool n1344Glosario_TipoArq ;
      private bool n1343Glosario_NomeArq ;
      private bool returnInSub ;
      private String A859Glosario_Descricao ;
      private String Z859Glosario_Descricao ;
      private String Z858Glosario_Termo ;
      private String A858Glosario_Termo ;
      private String A1342Glosario_Arquivo ;
      private String Z1342Glosario_Arquivo ;
      private IGxSession AV10WebSession ;
      private GxFile gxblobfileaux ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] T002P5_A1347Glosario_Codigo ;
      private String[] T002P5_A858Glosario_Termo ;
      private String[] T002P5_A859Glosario_Descricao ;
      private String[] T002P5_A1344Glosario_TipoArq ;
      private bool[] T002P5_n1344Glosario_TipoArq ;
      private String[] T002P5_A1343Glosario_NomeArq ;
      private bool[] T002P5_n1343Glosario_NomeArq ;
      private int[] T002P5_A1346Glosario_AreaTrabalhoCod ;
      private String[] T002P5_A1342Glosario_Arquivo ;
      private bool[] T002P5_n1342Glosario_Arquivo ;
      private int[] T002P4_A1346Glosario_AreaTrabalhoCod ;
      private int[] T002P6_A1346Glosario_AreaTrabalhoCod ;
      private int[] T002P7_A1347Glosario_Codigo ;
      private int[] T002P3_A1347Glosario_Codigo ;
      private String[] T002P3_A858Glosario_Termo ;
      private String[] T002P3_A859Glosario_Descricao ;
      private String[] T002P3_A1344Glosario_TipoArq ;
      private bool[] T002P3_n1344Glosario_TipoArq ;
      private String[] T002P3_A1343Glosario_NomeArq ;
      private bool[] T002P3_n1343Glosario_NomeArq ;
      private int[] T002P3_A1346Glosario_AreaTrabalhoCod ;
      private String[] T002P3_A1342Glosario_Arquivo ;
      private bool[] T002P3_n1342Glosario_Arquivo ;
      private int[] T002P8_A1347Glosario_Codigo ;
      private int[] T002P9_A1347Glosario_Codigo ;
      private int[] T002P2_A1347Glosario_Codigo ;
      private String[] T002P2_A858Glosario_Termo ;
      private String[] T002P2_A859Glosario_Descricao ;
      private String[] T002P2_A1344Glosario_TipoArq ;
      private bool[] T002P2_n1344Glosario_TipoArq ;
      private String[] T002P2_A1343Glosario_NomeArq ;
      private bool[] T002P2_n1343Glosario_NomeArq ;
      private int[] T002P2_A1346Glosario_AreaTrabalhoCod ;
      private String[] T002P2_A1342Glosario_Arquivo ;
      private bool[] T002P2_n1342Glosario_Arquivo ;
      private int[] T002P10_A1347Glosario_Codigo ;
      private int[] T002P14_A1347Glosario_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
   }

   public class glosario__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT002P5 ;
          prmT002P5 = new Object[] {
          new Object[] {"@Glosario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002P4 ;
          prmT002P4 = new Object[] {
          new Object[] {"@Glosario_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002P6 ;
          prmT002P6 = new Object[] {
          new Object[] {"@Glosario_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002P7 ;
          prmT002P7 = new Object[] {
          new Object[] {"@Glosario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002P3 ;
          prmT002P3 = new Object[] {
          new Object[] {"@Glosario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002P8 ;
          prmT002P8 = new Object[] {
          new Object[] {"@Glosario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002P9 ;
          prmT002P9 = new Object[] {
          new Object[] {"@Glosario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002P2 ;
          prmT002P2 = new Object[] {
          new Object[] {"@Glosario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002P10 ;
          prmT002P10 = new Object[] {
          new Object[] {"@Glosario_Termo",SqlDbType.VarChar,255,0} ,
          new Object[] {"@Glosario_Descricao",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Glosario_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Glosario_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@Glosario_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@Glosario_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002P11 ;
          prmT002P11 = new Object[] {
          new Object[] {"@Glosario_Termo",SqlDbType.VarChar,255,0} ,
          new Object[] {"@Glosario_Descricao",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Glosario_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@Glosario_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@Glosario_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Glosario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002P12 ;
          prmT002P12 = new Object[] {
          new Object[] {"@Glosario_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Glosario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002P13 ;
          prmT002P13 = new Object[] {
          new Object[] {"@Glosario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002P14 ;
          prmT002P14 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T002P2", "SELECT [Glosario_Codigo], [Glosario_Termo], [Glosario_Descricao], [Glosario_TipoArq], [Glosario_NomeArq], [Glosario_AreaTrabalhoCod] AS Glosario_AreaTrabalhoCod, [Glosario_Arquivo] FROM [Glosario] WITH (UPDLOCK) WHERE [Glosario_Codigo] = @Glosario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002P2,1,0,true,false )
             ,new CursorDef("T002P3", "SELECT [Glosario_Codigo], [Glosario_Termo], [Glosario_Descricao], [Glosario_TipoArq], [Glosario_NomeArq], [Glosario_AreaTrabalhoCod] AS Glosario_AreaTrabalhoCod, [Glosario_Arquivo] FROM [Glosario] WITH (NOLOCK) WHERE [Glosario_Codigo] = @Glosario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002P3,1,0,true,false )
             ,new CursorDef("T002P4", "SELECT [AreaTrabalho_Codigo] AS Glosario_AreaTrabalhoCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Glosario_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002P4,1,0,true,false )
             ,new CursorDef("T002P5", "SELECT TM1.[Glosario_Codigo], TM1.[Glosario_Termo], TM1.[Glosario_Descricao], TM1.[Glosario_TipoArq], TM1.[Glosario_NomeArq], TM1.[Glosario_AreaTrabalhoCod] AS Glosario_AreaTrabalhoCod, TM1.[Glosario_Arquivo] FROM [Glosario] TM1 WITH (NOLOCK) WHERE TM1.[Glosario_Codigo] = @Glosario_Codigo ORDER BY TM1.[Glosario_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002P5,100,0,true,false )
             ,new CursorDef("T002P6", "SELECT [AreaTrabalho_Codigo] AS Glosario_AreaTrabalhoCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Glosario_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002P6,1,0,true,false )
             ,new CursorDef("T002P7", "SELECT [Glosario_Codigo] FROM [Glosario] WITH (NOLOCK) WHERE [Glosario_Codigo] = @Glosario_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002P7,1,0,true,false )
             ,new CursorDef("T002P8", "SELECT TOP 1 [Glosario_Codigo] FROM [Glosario] WITH (NOLOCK) WHERE ( [Glosario_Codigo] > @Glosario_Codigo) ORDER BY [Glosario_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002P8,1,0,true,true )
             ,new CursorDef("T002P9", "SELECT TOP 1 [Glosario_Codigo] FROM [Glosario] WITH (NOLOCK) WHERE ( [Glosario_Codigo] < @Glosario_Codigo) ORDER BY [Glosario_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002P9,1,0,true,true )
             ,new CursorDef("T002P10", "INSERT INTO [Glosario]([Glosario_Termo], [Glosario_Descricao], [Glosario_Arquivo], [Glosario_TipoArq], [Glosario_NomeArq], [Glosario_AreaTrabalhoCod]) VALUES(@Glosario_Termo, @Glosario_Descricao, @Glosario_Arquivo, @Glosario_TipoArq, @Glosario_NomeArq, @Glosario_AreaTrabalhoCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT002P10)
             ,new CursorDef("T002P11", "UPDATE [Glosario] SET [Glosario_Termo]=@Glosario_Termo, [Glosario_Descricao]=@Glosario_Descricao, [Glosario_TipoArq]=@Glosario_TipoArq, [Glosario_NomeArq]=@Glosario_NomeArq, [Glosario_AreaTrabalhoCod]=@Glosario_AreaTrabalhoCod  WHERE [Glosario_Codigo] = @Glosario_Codigo", GxErrorMask.GX_NOMASK,prmT002P11)
             ,new CursorDef("T002P12", "UPDATE [Glosario] SET [Glosario_Arquivo]=@Glosario_Arquivo  WHERE [Glosario_Codigo] = @Glosario_Codigo", GxErrorMask.GX_NOMASK,prmT002P12)
             ,new CursorDef("T002P13", "DELETE FROM [Glosario]  WHERE [Glosario_Codigo] = @Glosario_Codigo", GxErrorMask.GX_NOMASK,prmT002P13)
             ,new CursorDef("T002P14", "SELECT [Glosario_Codigo] FROM [Glosario] WITH (NOLOCK) ORDER BY [Glosario_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002P14,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((String[]) buf[8])[0] = rslt.getBLOBFile(7, rslt.getString(4, 10), rslt.getString(5, 50)) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((String[]) buf[8])[0] = rslt.getBLOBFile(7, rslt.getString(4, 10), rslt.getString(5, 50)) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((String[]) buf[8])[0] = rslt.getBLOBFile(7, rslt.getString(4, 10), rslt.getString(5, 50)) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                stmt.SetParameter(6, (int)parms[8]);
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                stmt.SetParameter(5, (int)parms[6]);
                stmt.SetParameter(6, (int)parms[7]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
