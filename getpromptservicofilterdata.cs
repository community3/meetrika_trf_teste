/*
               File: GetPromptServicoFilterData
        Description: Get Prompt Servico Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:53:27.33
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptservicofilterdata : GXProcedure
   {
      public getpromptservicofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptservicofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptservicofilterdata objgetpromptservicofilterdata;
         objgetpromptservicofilterdata = new getpromptservicofilterdata();
         objgetpromptservicofilterdata.AV18DDOName = aP0_DDOName;
         objgetpromptservicofilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetpromptservicofilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptservicofilterdata.AV22OptionsJson = "" ;
         objgetpromptservicofilterdata.AV25OptionsDescJson = "" ;
         objgetpromptservicofilterdata.AV27OptionIndexesJson = "" ;
         objgetpromptservicofilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptservicofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptservicofilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptservicofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_SERVICO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADSERVICO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_SERVICO_SIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADSERVICO_SIGLAOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_SERVICOGRUPO_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADSERVICOGRUPO_DESCRICAOOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("PromptServicoGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptServicoGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("PromptServicoGridState"), "");
         }
         AV51GXV1 = 1;
         while ( AV51GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV51GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "SERVICO_ATIVO") == 0 )
            {
               AV34Servico_Ativo = BooleanUtil.Val( AV32GridStateFilterValue.gxTpr_Value);
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFSERVICO_NOME") == 0 )
            {
               AV10TFServico_Nome = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFSERVICO_NOME_SEL") == 0 )
            {
               AV11TFServico_Nome_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFSERVICO_SIGLA") == 0 )
            {
               AV12TFServico_Sigla = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFSERVICO_SIGLA_SEL") == 0 )
            {
               AV13TFServico_Sigla_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFSERVICOGRUPO_DESCRICAO") == 0 )
            {
               AV14TFServicoGrupo_Descricao = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFSERVICOGRUPO_DESCRICAO_SEL") == 0 )
            {
               AV15TFServicoGrupo_Descricao_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            AV51GXV1 = (int)(AV51GXV1+1);
         }
         if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(1));
            AV35DynamicFiltersSelector1 = AV33GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "SERVICO_SIGLA") == 0 )
            {
               AV36Servico_Sigla1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "SERVICO_NOME") == 0 )
            {
               AV37Servico_Nome1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "SERVICOGRUPO_CODIGO") == 0 )
            {
               AV38ServicoGrupo_Codigo1 = (int)(NumberUtil.Val( AV33GridStateDynamicFilter.gxTpr_Value, "."));
            }
            if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV39DynamicFiltersEnabled2 = true;
               AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(2));
               AV40DynamicFiltersSelector2 = AV33GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "SERVICO_SIGLA") == 0 )
               {
                  AV41Servico_Sigla2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "SERVICO_NOME") == 0 )
               {
                  AV42Servico_Nome2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "SERVICOGRUPO_CODIGO") == 0 )
               {
                  AV43ServicoGrupo_Codigo2 = (int)(NumberUtil.Val( AV33GridStateDynamicFilter.gxTpr_Value, "."));
               }
               if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV44DynamicFiltersEnabled3 = true;
                  AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(3));
                  AV45DynamicFiltersSelector3 = AV33GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "SERVICO_SIGLA") == 0 )
                  {
                     AV46Servico_Sigla3 = AV33GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "SERVICO_NOME") == 0 )
                  {
                     AV47Servico_Nome3 = AV33GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "SERVICOGRUPO_CODIGO") == 0 )
                  {
                     AV48ServicoGrupo_Codigo3 = (int)(NumberUtil.Val( AV33GridStateDynamicFilter.gxTpr_Value, "."));
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADSERVICO_NOMEOPTIONS' Routine */
         AV10TFServico_Nome = AV16SearchTxt;
         AV11TFServico_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV35DynamicFiltersSelector1 ,
                                              AV36Servico_Sigla1 ,
                                              AV37Servico_Nome1 ,
                                              AV38ServicoGrupo_Codigo1 ,
                                              AV39DynamicFiltersEnabled2 ,
                                              AV40DynamicFiltersSelector2 ,
                                              AV41Servico_Sigla2 ,
                                              AV42Servico_Nome2 ,
                                              AV43ServicoGrupo_Codigo2 ,
                                              AV44DynamicFiltersEnabled3 ,
                                              AV45DynamicFiltersSelector3 ,
                                              AV46Servico_Sigla3 ,
                                              AV47Servico_Nome3 ,
                                              AV48ServicoGrupo_Codigo3 ,
                                              AV11TFServico_Nome_Sel ,
                                              AV10TFServico_Nome ,
                                              AV13TFServico_Sigla_Sel ,
                                              AV12TFServico_Sigla ,
                                              AV15TFServicoGrupo_Descricao_Sel ,
                                              AV14TFServicoGrupo_Descricao ,
                                              A605Servico_Sigla ,
                                              A608Servico_Nome ,
                                              A157ServicoGrupo_Codigo ,
                                              A159ServicoGrupo_Ativo ,
                                              A158ServicoGrupo_Descricao ,
                                              A632Servico_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV37Servico_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV37Servico_Nome1), 50, "%");
         lV42Servico_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV42Servico_Nome2), 50, "%");
         lV47Servico_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV47Servico_Nome3), 50, "%");
         lV10TFServico_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFServico_Nome), 50, "%");
         lV12TFServico_Sigla = StringUtil.PadR( StringUtil.RTrim( AV12TFServico_Sigla), 15, "%");
         lV14TFServicoGrupo_Descricao = StringUtil.Concat( StringUtil.RTrim( AV14TFServicoGrupo_Descricao), "%", "");
         /* Using cursor P00JN2 */
         pr_default.execute(0, new Object[] {AV36Servico_Sigla1, lV37Servico_Nome1, AV38ServicoGrupo_Codigo1, AV41Servico_Sigla2, lV42Servico_Nome2, AV43ServicoGrupo_Codigo2, AV46Servico_Sigla3, lV47Servico_Nome3, AV48ServicoGrupo_Codigo3, lV10TFServico_Nome, AV11TFServico_Nome_Sel, lV12TFServico_Sigla, AV13TFServico_Sigla_Sel, lV14TFServicoGrupo_Descricao, AV15TFServicoGrupo_Descricao_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKJN2 = false;
            A608Servico_Nome = P00JN2_A608Servico_Nome[0];
            A158ServicoGrupo_Descricao = P00JN2_A158ServicoGrupo_Descricao[0];
            A159ServicoGrupo_Ativo = P00JN2_A159ServicoGrupo_Ativo[0];
            A157ServicoGrupo_Codigo = P00JN2_A157ServicoGrupo_Codigo[0];
            A605Servico_Sigla = P00JN2_A605Servico_Sigla[0];
            A632Servico_Ativo = P00JN2_A632Servico_Ativo[0];
            A155Servico_Codigo = P00JN2_A155Servico_Codigo[0];
            A158ServicoGrupo_Descricao = P00JN2_A158ServicoGrupo_Descricao[0];
            A159ServicoGrupo_Ativo = P00JN2_A159ServicoGrupo_Ativo[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00JN2_A608Servico_Nome[0], A608Servico_Nome) == 0 ) )
            {
               BRKJN2 = false;
               A155Servico_Codigo = P00JN2_A155Servico_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKJN2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A608Servico_Nome)) )
            {
               AV20Option = A608Servico_Nome;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJN2 )
            {
               BRKJN2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADSERVICO_SIGLAOPTIONS' Routine */
         AV12TFServico_Sigla = AV16SearchTxt;
         AV13TFServico_Sigla_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV35DynamicFiltersSelector1 ,
                                              AV36Servico_Sigla1 ,
                                              AV37Servico_Nome1 ,
                                              AV38ServicoGrupo_Codigo1 ,
                                              AV39DynamicFiltersEnabled2 ,
                                              AV40DynamicFiltersSelector2 ,
                                              AV41Servico_Sigla2 ,
                                              AV42Servico_Nome2 ,
                                              AV43ServicoGrupo_Codigo2 ,
                                              AV44DynamicFiltersEnabled3 ,
                                              AV45DynamicFiltersSelector3 ,
                                              AV46Servico_Sigla3 ,
                                              AV47Servico_Nome3 ,
                                              AV48ServicoGrupo_Codigo3 ,
                                              AV11TFServico_Nome_Sel ,
                                              AV10TFServico_Nome ,
                                              AV13TFServico_Sigla_Sel ,
                                              AV12TFServico_Sigla ,
                                              AV15TFServicoGrupo_Descricao_Sel ,
                                              AV14TFServicoGrupo_Descricao ,
                                              A605Servico_Sigla ,
                                              A608Servico_Nome ,
                                              A157ServicoGrupo_Codigo ,
                                              A159ServicoGrupo_Ativo ,
                                              A158ServicoGrupo_Descricao ,
                                              A632Servico_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV37Servico_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV37Servico_Nome1), 50, "%");
         lV42Servico_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV42Servico_Nome2), 50, "%");
         lV47Servico_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV47Servico_Nome3), 50, "%");
         lV10TFServico_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFServico_Nome), 50, "%");
         lV12TFServico_Sigla = StringUtil.PadR( StringUtil.RTrim( AV12TFServico_Sigla), 15, "%");
         lV14TFServicoGrupo_Descricao = StringUtil.Concat( StringUtil.RTrim( AV14TFServicoGrupo_Descricao), "%", "");
         /* Using cursor P00JN3 */
         pr_default.execute(1, new Object[] {AV36Servico_Sigla1, lV37Servico_Nome1, AV38ServicoGrupo_Codigo1, AV41Servico_Sigla2, lV42Servico_Nome2, AV43ServicoGrupo_Codigo2, AV46Servico_Sigla3, lV47Servico_Nome3, AV48ServicoGrupo_Codigo3, lV10TFServico_Nome, AV11TFServico_Nome_Sel, lV12TFServico_Sigla, AV13TFServico_Sigla_Sel, lV14TFServicoGrupo_Descricao, AV15TFServicoGrupo_Descricao_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKJN4 = false;
            A632Servico_Ativo = P00JN3_A632Servico_Ativo[0];
            A605Servico_Sigla = P00JN3_A605Servico_Sigla[0];
            A158ServicoGrupo_Descricao = P00JN3_A158ServicoGrupo_Descricao[0];
            A159ServicoGrupo_Ativo = P00JN3_A159ServicoGrupo_Ativo[0];
            A157ServicoGrupo_Codigo = P00JN3_A157ServicoGrupo_Codigo[0];
            A608Servico_Nome = P00JN3_A608Servico_Nome[0];
            A155Servico_Codigo = P00JN3_A155Servico_Codigo[0];
            A158ServicoGrupo_Descricao = P00JN3_A158ServicoGrupo_Descricao[0];
            A159ServicoGrupo_Ativo = P00JN3_A159ServicoGrupo_Ativo[0];
            AV28count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00JN3_A605Servico_Sigla[0], A605Servico_Sigla) == 0 ) )
            {
               BRKJN4 = false;
               A155Servico_Codigo = P00JN3_A155Servico_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKJN4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A605Servico_Sigla)) )
            {
               AV20Option = A605Servico_Sigla;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJN4 )
            {
               BRKJN4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADSERVICOGRUPO_DESCRICAOOPTIONS' Routine */
         AV14TFServicoGrupo_Descricao = AV16SearchTxt;
         AV15TFServicoGrupo_Descricao_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV35DynamicFiltersSelector1 ,
                                              AV36Servico_Sigla1 ,
                                              AV37Servico_Nome1 ,
                                              AV38ServicoGrupo_Codigo1 ,
                                              AV39DynamicFiltersEnabled2 ,
                                              AV40DynamicFiltersSelector2 ,
                                              AV41Servico_Sigla2 ,
                                              AV42Servico_Nome2 ,
                                              AV43ServicoGrupo_Codigo2 ,
                                              AV44DynamicFiltersEnabled3 ,
                                              AV45DynamicFiltersSelector3 ,
                                              AV46Servico_Sigla3 ,
                                              AV47Servico_Nome3 ,
                                              AV48ServicoGrupo_Codigo3 ,
                                              AV11TFServico_Nome_Sel ,
                                              AV10TFServico_Nome ,
                                              AV13TFServico_Sigla_Sel ,
                                              AV12TFServico_Sigla ,
                                              AV15TFServicoGrupo_Descricao_Sel ,
                                              AV14TFServicoGrupo_Descricao ,
                                              A605Servico_Sigla ,
                                              A608Servico_Nome ,
                                              A157ServicoGrupo_Codigo ,
                                              A159ServicoGrupo_Ativo ,
                                              A158ServicoGrupo_Descricao ,
                                              A632Servico_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV37Servico_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV37Servico_Nome1), 50, "%");
         lV42Servico_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV42Servico_Nome2), 50, "%");
         lV47Servico_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV47Servico_Nome3), 50, "%");
         lV10TFServico_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFServico_Nome), 50, "%");
         lV12TFServico_Sigla = StringUtil.PadR( StringUtil.RTrim( AV12TFServico_Sigla), 15, "%");
         lV14TFServicoGrupo_Descricao = StringUtil.Concat( StringUtil.RTrim( AV14TFServicoGrupo_Descricao), "%", "");
         /* Using cursor P00JN4 */
         pr_default.execute(2, new Object[] {AV36Servico_Sigla1, lV37Servico_Nome1, AV38ServicoGrupo_Codigo1, AV41Servico_Sigla2, lV42Servico_Nome2, AV43ServicoGrupo_Codigo2, AV46Servico_Sigla3, lV47Servico_Nome3, AV48ServicoGrupo_Codigo3, lV10TFServico_Nome, AV11TFServico_Nome_Sel, lV12TFServico_Sigla, AV13TFServico_Sigla_Sel, lV14TFServicoGrupo_Descricao, AV15TFServicoGrupo_Descricao_Sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKJN6 = false;
            A157ServicoGrupo_Codigo = P00JN4_A157ServicoGrupo_Codigo[0];
            A158ServicoGrupo_Descricao = P00JN4_A158ServicoGrupo_Descricao[0];
            A159ServicoGrupo_Ativo = P00JN4_A159ServicoGrupo_Ativo[0];
            A608Servico_Nome = P00JN4_A608Servico_Nome[0];
            A605Servico_Sigla = P00JN4_A605Servico_Sigla[0];
            A632Servico_Ativo = P00JN4_A632Servico_Ativo[0];
            A155Servico_Codigo = P00JN4_A155Servico_Codigo[0];
            A158ServicoGrupo_Descricao = P00JN4_A158ServicoGrupo_Descricao[0];
            A159ServicoGrupo_Ativo = P00JN4_A159ServicoGrupo_Ativo[0];
            AV28count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( P00JN4_A157ServicoGrupo_Codigo[0] == A157ServicoGrupo_Codigo ) )
            {
               BRKJN6 = false;
               A155Servico_Codigo = P00JN4_A155Servico_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKJN6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A158ServicoGrupo_Descricao)) )
            {
               AV20Option = A158ServicoGrupo_Descricao;
               AV19InsertIndex = 1;
               while ( ( AV19InsertIndex <= AV21Options.Count ) && ( StringUtil.StrCmp(((String)AV21Options.Item(AV19InsertIndex)), AV20Option) < 0 ) )
               {
                  AV19InsertIndex = (int)(AV19InsertIndex+1);
               }
               AV21Options.Add(AV20Option, AV19InsertIndex);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), AV19InsertIndex);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJN6 )
            {
               BRKJN6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV34Servico_Ativo = true;
         AV10TFServico_Nome = "";
         AV11TFServico_Nome_Sel = "";
         AV12TFServico_Sigla = "";
         AV13TFServico_Sigla_Sel = "";
         AV14TFServicoGrupo_Descricao = "";
         AV15TFServicoGrupo_Descricao_Sel = "";
         AV33GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV35DynamicFiltersSelector1 = "";
         AV36Servico_Sigla1 = "";
         AV37Servico_Nome1 = "";
         AV40DynamicFiltersSelector2 = "";
         AV41Servico_Sigla2 = "";
         AV42Servico_Nome2 = "";
         AV45DynamicFiltersSelector3 = "";
         AV46Servico_Sigla3 = "";
         AV47Servico_Nome3 = "";
         scmdbuf = "";
         lV37Servico_Nome1 = "";
         lV42Servico_Nome2 = "";
         lV47Servico_Nome3 = "";
         lV10TFServico_Nome = "";
         lV12TFServico_Sigla = "";
         lV14TFServicoGrupo_Descricao = "";
         A605Servico_Sigla = "";
         A608Servico_Nome = "";
         A158ServicoGrupo_Descricao = "";
         P00JN2_A608Servico_Nome = new String[] {""} ;
         P00JN2_A158ServicoGrupo_Descricao = new String[] {""} ;
         P00JN2_A159ServicoGrupo_Ativo = new bool[] {false} ;
         P00JN2_A157ServicoGrupo_Codigo = new int[1] ;
         P00JN2_A605Servico_Sigla = new String[] {""} ;
         P00JN2_A632Servico_Ativo = new bool[] {false} ;
         P00JN2_A155Servico_Codigo = new int[1] ;
         AV20Option = "";
         P00JN3_A632Servico_Ativo = new bool[] {false} ;
         P00JN3_A605Servico_Sigla = new String[] {""} ;
         P00JN3_A158ServicoGrupo_Descricao = new String[] {""} ;
         P00JN3_A159ServicoGrupo_Ativo = new bool[] {false} ;
         P00JN3_A157ServicoGrupo_Codigo = new int[1] ;
         P00JN3_A608Servico_Nome = new String[] {""} ;
         P00JN3_A155Servico_Codigo = new int[1] ;
         P00JN4_A157ServicoGrupo_Codigo = new int[1] ;
         P00JN4_A158ServicoGrupo_Descricao = new String[] {""} ;
         P00JN4_A159ServicoGrupo_Ativo = new bool[] {false} ;
         P00JN4_A608Servico_Nome = new String[] {""} ;
         P00JN4_A605Servico_Sigla = new String[] {""} ;
         P00JN4_A632Servico_Ativo = new bool[] {false} ;
         P00JN4_A155Servico_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptservicofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00JN2_A608Servico_Nome, P00JN2_A158ServicoGrupo_Descricao, P00JN2_A159ServicoGrupo_Ativo, P00JN2_A157ServicoGrupo_Codigo, P00JN2_A605Servico_Sigla, P00JN2_A632Servico_Ativo, P00JN2_A155Servico_Codigo
               }
               , new Object[] {
               P00JN3_A632Servico_Ativo, P00JN3_A605Servico_Sigla, P00JN3_A158ServicoGrupo_Descricao, P00JN3_A159ServicoGrupo_Ativo, P00JN3_A157ServicoGrupo_Codigo, P00JN3_A608Servico_Nome, P00JN3_A155Servico_Codigo
               }
               , new Object[] {
               P00JN4_A157ServicoGrupo_Codigo, P00JN4_A158ServicoGrupo_Descricao, P00JN4_A159ServicoGrupo_Ativo, P00JN4_A608Servico_Nome, P00JN4_A605Servico_Sigla, P00JN4_A632Servico_Ativo, P00JN4_A155Servico_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV51GXV1 ;
      private int AV38ServicoGrupo_Codigo1 ;
      private int AV43ServicoGrupo_Codigo2 ;
      private int AV48ServicoGrupo_Codigo3 ;
      private int A157ServicoGrupo_Codigo ;
      private int A155Servico_Codigo ;
      private int AV19InsertIndex ;
      private long AV28count ;
      private String AV10TFServico_Nome ;
      private String AV11TFServico_Nome_Sel ;
      private String AV12TFServico_Sigla ;
      private String AV13TFServico_Sigla_Sel ;
      private String AV36Servico_Sigla1 ;
      private String AV37Servico_Nome1 ;
      private String AV41Servico_Sigla2 ;
      private String AV42Servico_Nome2 ;
      private String AV46Servico_Sigla3 ;
      private String AV47Servico_Nome3 ;
      private String scmdbuf ;
      private String lV37Servico_Nome1 ;
      private String lV42Servico_Nome2 ;
      private String lV47Servico_Nome3 ;
      private String lV10TFServico_Nome ;
      private String lV12TFServico_Sigla ;
      private String A605Servico_Sigla ;
      private String A608Servico_Nome ;
      private bool returnInSub ;
      private bool AV34Servico_Ativo ;
      private bool AV39DynamicFiltersEnabled2 ;
      private bool AV44DynamicFiltersEnabled3 ;
      private bool A159ServicoGrupo_Ativo ;
      private bool A632Servico_Ativo ;
      private bool BRKJN2 ;
      private bool BRKJN4 ;
      private bool BRKJN6 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV14TFServicoGrupo_Descricao ;
      private String AV15TFServicoGrupo_Descricao_Sel ;
      private String AV35DynamicFiltersSelector1 ;
      private String AV40DynamicFiltersSelector2 ;
      private String AV45DynamicFiltersSelector3 ;
      private String lV14TFServicoGrupo_Descricao ;
      private String A158ServicoGrupo_Descricao ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00JN2_A608Servico_Nome ;
      private String[] P00JN2_A158ServicoGrupo_Descricao ;
      private bool[] P00JN2_A159ServicoGrupo_Ativo ;
      private int[] P00JN2_A157ServicoGrupo_Codigo ;
      private String[] P00JN2_A605Servico_Sigla ;
      private bool[] P00JN2_A632Servico_Ativo ;
      private int[] P00JN2_A155Servico_Codigo ;
      private bool[] P00JN3_A632Servico_Ativo ;
      private String[] P00JN3_A605Servico_Sigla ;
      private String[] P00JN3_A158ServicoGrupo_Descricao ;
      private bool[] P00JN3_A159ServicoGrupo_Ativo ;
      private int[] P00JN3_A157ServicoGrupo_Codigo ;
      private String[] P00JN3_A608Servico_Nome ;
      private int[] P00JN3_A155Servico_Codigo ;
      private int[] P00JN4_A157ServicoGrupo_Codigo ;
      private String[] P00JN4_A158ServicoGrupo_Descricao ;
      private bool[] P00JN4_A159ServicoGrupo_Ativo ;
      private String[] P00JN4_A608Servico_Nome ;
      private String[] P00JN4_A605Servico_Sigla ;
      private bool[] P00JN4_A632Servico_Ativo ;
      private int[] P00JN4_A155Servico_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV33GridStateDynamicFilter ;
   }

   public class getpromptservicofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00JN2( IGxContext context ,
                                             String AV35DynamicFiltersSelector1 ,
                                             String AV36Servico_Sigla1 ,
                                             String AV37Servico_Nome1 ,
                                             int AV38ServicoGrupo_Codigo1 ,
                                             bool AV39DynamicFiltersEnabled2 ,
                                             String AV40DynamicFiltersSelector2 ,
                                             String AV41Servico_Sigla2 ,
                                             String AV42Servico_Nome2 ,
                                             int AV43ServicoGrupo_Codigo2 ,
                                             bool AV44DynamicFiltersEnabled3 ,
                                             String AV45DynamicFiltersSelector3 ,
                                             String AV46Servico_Sigla3 ,
                                             String AV47Servico_Nome3 ,
                                             int AV48ServicoGrupo_Codigo3 ,
                                             String AV11TFServico_Nome_Sel ,
                                             String AV10TFServico_Nome ,
                                             String AV13TFServico_Sigla_Sel ,
                                             String AV12TFServico_Sigla ,
                                             String AV15TFServicoGrupo_Descricao_Sel ,
                                             String AV14TFServicoGrupo_Descricao ,
                                             String A605Servico_Sigla ,
                                             String A608Servico_Nome ,
                                             int A157ServicoGrupo_Codigo ,
                                             bool A159ServicoGrupo_Ativo ,
                                             String A158ServicoGrupo_Descricao ,
                                             bool A632Servico_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [15] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Servico_Nome], T2.[ServicoGrupo_Descricao], T2.[ServicoGrupo_Ativo], T1.[ServicoGrupo_Codigo], T1.[Servico_Sigla], T1.[Servico_Ativo], T1.[Servico_Codigo] FROM ([Servico] T1 WITH (NOLOCK) INNER JOIN [ServicoGrupo] T2 WITH (NOLOCK) ON T2.[ServicoGrupo_Codigo] = T1.[ServicoGrupo_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Servico_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "SERVICO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36Servico_Sigla1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV36Servico_Sigla1)";
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Servico_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV37Servico_Nome1 + '%')";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "SERVICOGRUPO_CODIGO") == 0 ) && ( ! (0==AV38ServicoGrupo_Codigo1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ServicoGrupo_Codigo] = @AV38ServicoGrupo_Codigo1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "SERVICOGRUPO_CODIGO") == 0 )
         {
            sWhereString = sWhereString + " and (T2.[ServicoGrupo_Ativo] = 1)";
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "SERVICO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Servico_Sigla2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV41Servico_Sigla2)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Servico_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV42Servico_Nome2 + '%')";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "SERVICOGRUPO_CODIGO") == 0 ) && ( ! (0==AV43ServicoGrupo_Codigo2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ServicoGrupo_Codigo] = @AV43ServicoGrupo_Codigo2)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "SERVICOGRUPO_CODIGO") == 0 ) )
         {
            sWhereString = sWhereString + " and (T2.[ServicoGrupo_Ativo] = 1)";
         }
         if ( AV44DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "SERVICO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46Servico_Sigla3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV46Servico_Sigla3)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV44DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47Servico_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV47Servico_Nome3 + '%')";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV44DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "SERVICOGRUPO_CODIGO") == 0 ) && ( ! (0==AV48ServicoGrupo_Codigo3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ServicoGrupo_Codigo] = @AV48ServicoGrupo_Codigo3)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV44DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "SERVICOGRUPO_CODIGO") == 0 ) )
         {
            sWhereString = sWhereString + " and (T2.[ServicoGrupo_Ativo] = 1)";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFServico_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFServico_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like @lV10TFServico_Nome)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFServico_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] = @AV11TFServico_Nome_Sel)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFServico_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFServico_Sigla)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Sigla] like @lV12TFServico_Sigla)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFServico_Sigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV13TFServico_Sigla_Sel)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFServicoGrupo_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFServicoGrupo_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ServicoGrupo_Descricao] like @lV14TFServicoGrupo_Descricao)";
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFServicoGrupo_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ServicoGrupo_Descricao] = @AV15TFServicoGrupo_Descricao_Sel)";
         }
         else
         {
            GXv_int1[14] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Servico_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00JN3( IGxContext context ,
                                             String AV35DynamicFiltersSelector1 ,
                                             String AV36Servico_Sigla1 ,
                                             String AV37Servico_Nome1 ,
                                             int AV38ServicoGrupo_Codigo1 ,
                                             bool AV39DynamicFiltersEnabled2 ,
                                             String AV40DynamicFiltersSelector2 ,
                                             String AV41Servico_Sigla2 ,
                                             String AV42Servico_Nome2 ,
                                             int AV43ServicoGrupo_Codigo2 ,
                                             bool AV44DynamicFiltersEnabled3 ,
                                             String AV45DynamicFiltersSelector3 ,
                                             String AV46Servico_Sigla3 ,
                                             String AV47Servico_Nome3 ,
                                             int AV48ServicoGrupo_Codigo3 ,
                                             String AV11TFServico_Nome_Sel ,
                                             String AV10TFServico_Nome ,
                                             String AV13TFServico_Sigla_Sel ,
                                             String AV12TFServico_Sigla ,
                                             String AV15TFServicoGrupo_Descricao_Sel ,
                                             String AV14TFServicoGrupo_Descricao ,
                                             String A605Servico_Sigla ,
                                             String A608Servico_Nome ,
                                             int A157ServicoGrupo_Codigo ,
                                             bool A159ServicoGrupo_Ativo ,
                                             String A158ServicoGrupo_Descricao ,
                                             bool A632Servico_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [15] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Servico_Ativo], T1.[Servico_Sigla], T2.[ServicoGrupo_Descricao], T2.[ServicoGrupo_Ativo], T1.[ServicoGrupo_Codigo], T1.[Servico_Nome], T1.[Servico_Codigo] FROM ([Servico] T1 WITH (NOLOCK) INNER JOIN [ServicoGrupo] T2 WITH (NOLOCK) ON T2.[ServicoGrupo_Codigo] = T1.[ServicoGrupo_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Servico_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "SERVICO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36Servico_Sigla1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV36Servico_Sigla1)";
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Servico_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV37Servico_Nome1 + '%')";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "SERVICOGRUPO_CODIGO") == 0 ) && ( ! (0==AV38ServicoGrupo_Codigo1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ServicoGrupo_Codigo] = @AV38ServicoGrupo_Codigo1)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "SERVICOGRUPO_CODIGO") == 0 )
         {
            sWhereString = sWhereString + " and (T2.[ServicoGrupo_Ativo] = 1)";
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "SERVICO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Servico_Sigla2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV41Servico_Sigla2)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Servico_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV42Servico_Nome2 + '%')";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "SERVICOGRUPO_CODIGO") == 0 ) && ( ! (0==AV43ServicoGrupo_Codigo2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ServicoGrupo_Codigo] = @AV43ServicoGrupo_Codigo2)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "SERVICOGRUPO_CODIGO") == 0 ) )
         {
            sWhereString = sWhereString + " and (T2.[ServicoGrupo_Ativo] = 1)";
         }
         if ( AV44DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "SERVICO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46Servico_Sigla3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV46Servico_Sigla3)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV44DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47Servico_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV47Servico_Nome3 + '%')";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV44DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "SERVICOGRUPO_CODIGO") == 0 ) && ( ! (0==AV48ServicoGrupo_Codigo3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ServicoGrupo_Codigo] = @AV48ServicoGrupo_Codigo3)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV44DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "SERVICOGRUPO_CODIGO") == 0 ) )
         {
            sWhereString = sWhereString + " and (T2.[ServicoGrupo_Ativo] = 1)";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFServico_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFServico_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like @lV10TFServico_Nome)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFServico_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] = @AV11TFServico_Nome_Sel)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFServico_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFServico_Sigla)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Sigla] like @lV12TFServico_Sigla)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFServico_Sigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV13TFServico_Sigla_Sel)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFServicoGrupo_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFServicoGrupo_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ServicoGrupo_Descricao] like @lV14TFServicoGrupo_Descricao)";
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFServicoGrupo_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ServicoGrupo_Descricao] = @AV15TFServicoGrupo_Descricao_Sel)";
         }
         else
         {
            GXv_int3[14] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Servico_Sigla]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00JN4( IGxContext context ,
                                             String AV35DynamicFiltersSelector1 ,
                                             String AV36Servico_Sigla1 ,
                                             String AV37Servico_Nome1 ,
                                             int AV38ServicoGrupo_Codigo1 ,
                                             bool AV39DynamicFiltersEnabled2 ,
                                             String AV40DynamicFiltersSelector2 ,
                                             String AV41Servico_Sigla2 ,
                                             String AV42Servico_Nome2 ,
                                             int AV43ServicoGrupo_Codigo2 ,
                                             bool AV44DynamicFiltersEnabled3 ,
                                             String AV45DynamicFiltersSelector3 ,
                                             String AV46Servico_Sigla3 ,
                                             String AV47Servico_Nome3 ,
                                             int AV48ServicoGrupo_Codigo3 ,
                                             String AV11TFServico_Nome_Sel ,
                                             String AV10TFServico_Nome ,
                                             String AV13TFServico_Sigla_Sel ,
                                             String AV12TFServico_Sigla ,
                                             String AV15TFServicoGrupo_Descricao_Sel ,
                                             String AV14TFServicoGrupo_Descricao ,
                                             String A605Servico_Sigla ,
                                             String A608Servico_Nome ,
                                             int A157ServicoGrupo_Codigo ,
                                             bool A159ServicoGrupo_Ativo ,
                                             String A158ServicoGrupo_Descricao ,
                                             bool A632Servico_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [15] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[ServicoGrupo_Codigo], T2.[ServicoGrupo_Descricao], T2.[ServicoGrupo_Ativo], T1.[Servico_Nome], T1.[Servico_Sigla], T1.[Servico_Ativo], T1.[Servico_Codigo] FROM ([Servico] T1 WITH (NOLOCK) INNER JOIN [ServicoGrupo] T2 WITH (NOLOCK) ON T2.[ServicoGrupo_Codigo] = T1.[ServicoGrupo_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Servico_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "SERVICO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36Servico_Sigla1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV36Servico_Sigla1)";
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Servico_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV37Servico_Nome1 + '%')";
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "SERVICOGRUPO_CODIGO") == 0 ) && ( ! (0==AV38ServicoGrupo_Codigo1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ServicoGrupo_Codigo] = @AV38ServicoGrupo_Codigo1)";
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "SERVICOGRUPO_CODIGO") == 0 )
         {
            sWhereString = sWhereString + " and (T2.[ServicoGrupo_Ativo] = 1)";
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "SERVICO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Servico_Sigla2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV41Servico_Sigla2)";
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Servico_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV42Servico_Nome2 + '%')";
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "SERVICOGRUPO_CODIGO") == 0 ) && ( ! (0==AV43ServicoGrupo_Codigo2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ServicoGrupo_Codigo] = @AV43ServicoGrupo_Codigo2)";
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "SERVICOGRUPO_CODIGO") == 0 ) )
         {
            sWhereString = sWhereString + " and (T2.[ServicoGrupo_Ativo] = 1)";
         }
         if ( AV44DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "SERVICO_SIGLA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46Servico_Sigla3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV46Servico_Sigla3)";
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV44DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47Servico_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like '%' + @lV47Servico_Nome3 + '%')";
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV44DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "SERVICOGRUPO_CODIGO") == 0 ) && ( ! (0==AV48ServicoGrupo_Codigo3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ServicoGrupo_Codigo] = @AV48ServicoGrupo_Codigo3)";
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV44DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "SERVICOGRUPO_CODIGO") == 0 ) )
         {
            sWhereString = sWhereString + " and (T2.[ServicoGrupo_Ativo] = 1)";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFServico_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFServico_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] like @lV10TFServico_Nome)";
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFServico_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Nome] = @AV11TFServico_Nome_Sel)";
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFServico_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFServico_Sigla)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Sigla] like @lV12TFServico_Sigla)";
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFServico_Sigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Servico_Sigla] = @AV13TFServico_Sigla_Sel)";
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFServicoGrupo_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFServicoGrupo_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ServicoGrupo_Descricao] like @lV14TFServicoGrupo_Descricao)";
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFServicoGrupo_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ServicoGrupo_Descricao] = @AV15TFServicoGrupo_Descricao_Sel)";
         }
         else
         {
            GXv_int5[14] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ServicoGrupo_Codigo]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00JN2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (bool)dynConstraints[23] , (String)dynConstraints[24] , (bool)dynConstraints[25] );
               case 1 :
                     return conditional_P00JN3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (bool)dynConstraints[23] , (String)dynConstraints[24] , (bool)dynConstraints[25] );
               case 2 :
                     return conditional_P00JN4(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (bool)dynConstraints[23] , (String)dynConstraints[24] , (bool)dynConstraints[25] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00JN2 ;
          prmP00JN2 = new Object[] {
          new Object[] {"@AV36Servico_Sigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV37Servico_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV38ServicoGrupo_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV41Servico_Sigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV42Servico_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV43ServicoGrupo_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46Servico_Sigla3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV47Servico_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV48ServicoGrupo_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFServico_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFServico_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFServico_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV13TFServico_Sigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV14TFServicoGrupo_Descricao",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV15TFServicoGrupo_Descricao_Sel",SqlDbType.VarChar,100,0}
          } ;
          Object[] prmP00JN3 ;
          prmP00JN3 = new Object[] {
          new Object[] {"@AV36Servico_Sigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV37Servico_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV38ServicoGrupo_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV41Servico_Sigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV42Servico_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV43ServicoGrupo_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46Servico_Sigla3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV47Servico_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV48ServicoGrupo_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFServico_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFServico_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFServico_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV13TFServico_Sigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV14TFServicoGrupo_Descricao",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV15TFServicoGrupo_Descricao_Sel",SqlDbType.VarChar,100,0}
          } ;
          Object[] prmP00JN4 ;
          prmP00JN4 = new Object[] {
          new Object[] {"@AV36Servico_Sigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV37Servico_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV38ServicoGrupo_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV41Servico_Sigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV42Servico_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV43ServicoGrupo_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46Servico_Sigla3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV47Servico_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV48ServicoGrupo_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFServico_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFServico_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFServico_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV13TFServico_Sigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV14TFServicoGrupo_Descricao",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV15TFServicoGrupo_Descricao_Sel",SqlDbType.VarChar,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00JN2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00JN2,100,0,true,false )
             ,new CursorDef("P00JN3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00JN3,100,0,true,false )
             ,new CursorDef("P00JN4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00JN4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[5])[0] = rslt.getBool(6) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 50) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[5])[0] = rslt.getBool(6) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptservicofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptservicofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptservicofilterdata") )
          {
             return  ;
          }
          getpromptservicofilterdata worker = new getpromptservicofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
