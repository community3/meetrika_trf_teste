/*
               File: PromptContagemResultadoExecucao
        Description: Selecione Contagem Resultado Execucao
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:47:48.33
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptcontagemresultadoexecucao : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptcontagemresultadoexecucao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptcontagemresultadoexecucao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutContagemResultadoExecucao_Codigo ,
                           ref DateTime aP1_InOutContagemResultadoExecucao_Inicio )
      {
         this.AV7InOutContagemResultadoExecucao_Codigo = aP0_InOutContagemResultadoExecucao_Codigo;
         this.AV8InOutContagemResultadoExecucao_Inicio = aP1_InOutContagemResultadoExecucao_Inicio;
         executePrivate();
         aP0_InOutContagemResultadoExecucao_Codigo=this.AV7InOutContagemResultadoExecucao_Codigo;
         aP1_InOutContagemResultadoExecucao_Inicio=this.AV8InOutContagemResultadoExecucao_Inicio;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_86 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_86_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_86_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16ContagemResultadoExecucao_Inicio1 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultadoExecucao_Inicio1", context.localUtil.TToC( AV16ContagemResultadoExecucao_Inicio1, 8, 5, 0, 3, "/", ":", " "));
               AV17ContagemResultadoExecucao_Inicio_To1 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultadoExecucao_Inicio_To1", context.localUtil.TToC( AV17ContagemResultadoExecucao_Inicio_To1, 8, 5, 0, 3, "/", ":", " "));
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20ContagemResultadoExecucao_Inicio2 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultadoExecucao_Inicio2", context.localUtil.TToC( AV20ContagemResultadoExecucao_Inicio2, 8, 5, 0, 3, "/", ":", " "));
               AV21ContagemResultadoExecucao_Inicio_To2 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultadoExecucao_Inicio_To2", context.localUtil.TToC( AV21ContagemResultadoExecucao_Inicio_To2, 8, 5, 0, 3, "/", ":", " "));
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24ContagemResultadoExecucao_Inicio3 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultadoExecucao_Inicio3", context.localUtil.TToC( AV24ContagemResultadoExecucao_Inicio3, 8, 5, 0, 3, "/", ":", " "));
               AV25ContagemResultadoExecucao_Inicio_To3 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContagemResultadoExecucao_Inicio_To3", context.localUtil.TToC( AV25ContagemResultadoExecucao_Inicio_To3, 8, 5, 0, 3, "/", ":", " "));
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV31TFContagemResultadoExecucao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFContagemResultadoExecucao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContagemResultadoExecucao_Codigo), 6, 0)));
               AV32TFContagemResultadoExecucao_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContagemResultadoExecucao_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContagemResultadoExecucao_Codigo_To), 6, 0)));
               AV35TFContagemResultadoExecucao_OSCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContagemResultadoExecucao_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFContagemResultadoExecucao_OSCod), 6, 0)));
               AV36TFContagemResultadoExecucao_OSCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContagemResultadoExecucao_OSCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContagemResultadoExecucao_OSCod_To), 6, 0)));
               AV39TFContagemResultadoExecucao_Inicio = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContagemResultadoExecucao_Inicio", context.localUtil.TToC( AV39TFContagemResultadoExecucao_Inicio, 8, 5, 0, 3, "/", ":", " "));
               AV40TFContagemResultadoExecucao_Inicio_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContagemResultadoExecucao_Inicio_To", context.localUtil.TToC( AV40TFContagemResultadoExecucao_Inicio_To, 8, 5, 0, 3, "/", ":", " "));
               AV45TFContagemResultadoExecucao_PrazoDias = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContagemResultadoExecucao_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFContagemResultadoExecucao_PrazoDias), 4, 0)));
               AV46TFContagemResultadoExecucao_PrazoDias_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContagemResultadoExecucao_PrazoDias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFContagemResultadoExecucao_PrazoDias_To), 4, 0)));
               AV49TFContagemResultadoExecucao_Prevista = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContagemResultadoExecucao_Prevista", context.localUtil.TToC( AV49TFContagemResultadoExecucao_Prevista, 8, 5, 0, 3, "/", ":", " "));
               AV50TFContagemResultadoExecucao_Prevista_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContagemResultadoExecucao_Prevista_To", context.localUtil.TToC( AV50TFContagemResultadoExecucao_Prevista_To, 8, 5, 0, 3, "/", ":", " "));
               AV55TFContagemResultadoExecucao_Fim = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContagemResultadoExecucao_Fim", context.localUtil.TToC( AV55TFContagemResultadoExecucao_Fim, 8, 5, 0, 3, "/", ":", " "));
               AV56TFContagemResultadoExecucao_Fim_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContagemResultadoExecucao_Fim_To", context.localUtil.TToC( AV56TFContagemResultadoExecucao_Fim_To, 8, 5, 0, 3, "/", ":", " "));
               AV61TFContagemResultadoExecucao_Dias = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContagemResultadoExecucao_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFContagemResultadoExecucao_Dias), 4, 0)));
               AV62TFContagemResultadoExecucao_Dias_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContagemResultadoExecucao_Dias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFContagemResultadoExecucao_Dias_To), 4, 0)));
               AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace", AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace);
               AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace", AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace);
               AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace", AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace);
               AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace", AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace);
               AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace", AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace);
               AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace", AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace);
               AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace", AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContagemResultadoExecucao_Inicio1, AV17ContagemResultadoExecucao_Inicio_To1, AV19DynamicFiltersSelector2, AV20ContagemResultadoExecucao_Inicio2, AV21ContagemResultadoExecucao_Inicio_To2, AV23DynamicFiltersSelector3, AV24ContagemResultadoExecucao_Inicio3, AV25ContagemResultadoExecucao_Inicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContagemResultadoExecucao_Codigo, AV32TFContagemResultadoExecucao_Codigo_To, AV35TFContagemResultadoExecucao_OSCod, AV36TFContagemResultadoExecucao_OSCod_To, AV39TFContagemResultadoExecucao_Inicio, AV40TFContagemResultadoExecucao_Inicio_To, AV45TFContagemResultadoExecucao_PrazoDias, AV46TFContagemResultadoExecucao_PrazoDias_To, AV49TFContagemResultadoExecucao_Prevista, AV50TFContagemResultadoExecucao_Prevista_To, AV55TFContagemResultadoExecucao_Fim, AV56TFContagemResultadoExecucao_Fim_To, AV61TFContagemResultadoExecucao_Dias, AV62TFContagemResultadoExecucao_Dias_To, AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace, AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace, AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace, AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace, AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace, AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace, AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutContagemResultadoExecucao_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContagemResultadoExecucao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContagemResultadoExecucao_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutContagemResultadoExecucao_Inicio = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContagemResultadoExecucao_Inicio", context.localUtil.TToC( AV8InOutContagemResultadoExecucao_Inicio, 8, 5, 0, 3, "/", ":", " "));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAKJ2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WSKJ2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEKJ2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042823474870");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptcontagemresultadoexecucao.aspx") + "?" + UrlEncode("" +AV7InOutContagemResultadoExecucao_Codigo) + "," + UrlEncode(DateTimeUtil.FormatDateTimeParm(AV8InOutContagemResultadoExecucao_Inicio))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO1", context.localUtil.TToC( AV16ContagemResultadoExecucao_Inicio1, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1", context.localUtil.TToC( AV17ContagemResultadoExecucao_Inicio_To1, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO2", context.localUtil.TToC( AV20ContagemResultadoExecucao_Inicio2, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2", context.localUtil.TToC( AV21ContagemResultadoExecucao_Inicio_To2, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO3", context.localUtil.TToC( AV24ContagemResultadoExecucao_Inicio3, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3", context.localUtil.TToC( AV25ContagemResultadoExecucao_Inicio_To3, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEXECUCAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31TFContagemResultadoExecucao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEXECUCAO_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFContagemResultadoExecucao_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEXECUCAO_OSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFContagemResultadoExecucao_OSCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEXECUCAO_OSCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFContagemResultadoExecucao_OSCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEXECUCAO_INICIO", context.localUtil.TToC( AV39TFContagemResultadoExecucao_Inicio, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO", context.localUtil.TToC( AV40TFContagemResultadoExecucao_Inicio_To, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45TFContagemResultadoExecucao_PrazoDias), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV46TFContagemResultadoExecucao_PrazoDias_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA", context.localUtil.TToC( AV49TFContagemResultadoExecucao_Prevista, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO", context.localUtil.TToC( AV50TFContagemResultadoExecucao_Prevista_To, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEXECUCAO_FIM", context.localUtil.TToC( AV55TFContagemResultadoExecucao_Fim, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO", context.localUtil.TToC( AV56TFContagemResultadoExecucao_Fim_To, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEXECUCAO_DIAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV61TFContagemResultadoExecucao_Dias), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV62TFContagemResultadoExecucao_Dias_To), 4, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_86", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_86), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV66GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV67GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV64DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV64DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADOEXECUCAO_CODIGOTITLEFILTERDATA", AV30ContagemResultadoExecucao_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADOEXECUCAO_CODIGOTITLEFILTERDATA", AV30ContagemResultadoExecucao_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADOEXECUCAO_OSCODTITLEFILTERDATA", AV34ContagemResultadoExecucao_OSCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADOEXECUCAO_OSCODTITLEFILTERDATA", AV34ContagemResultadoExecucao_OSCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADOEXECUCAO_INICIOTITLEFILTERDATA", AV38ContagemResultadoExecucao_InicioTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADOEXECUCAO_INICIOTITLEFILTERDATA", AV38ContagemResultadoExecucao_InicioTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLEFILTERDATA", AV44ContagemResultadoExecucao_PrazoDiasTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLEFILTERDATA", AV44ContagemResultadoExecucao_PrazoDiasTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADOEXECUCAO_PREVISTATITLEFILTERDATA", AV48ContagemResultadoExecucao_PrevistaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADOEXECUCAO_PREVISTATITLEFILTERDATA", AV48ContagemResultadoExecucao_PrevistaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADOEXECUCAO_FIMTITLEFILTERDATA", AV54ContagemResultadoExecucao_FimTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADOEXECUCAO_FIMTITLEFILTERDATA", AV54ContagemResultadoExecucao_FimTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTAGEMRESULTADOEXECUCAO_DIASTITLEFILTERDATA", AV60ContagemResultadoExecucao_DiasTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTAGEMRESULTADOEXECUCAO_DIASTITLEFILTERDATA", AV60ContagemResultadoExecucao_DiasTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTCONTAGEMRESULTADOEXECUCAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutContagemResultadoExecucao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTCONTAGEMRESULTADOEXECUCAO_INICIO", context.localUtil.TToC( AV8InOutContagemResultadoExecucao_Inicio, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Caption", StringUtil.RTrim( Ddo_contagemresultadoexecucao_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Tooltip", StringUtil.RTrim( Ddo_contagemresultadoexecucao_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Cls", StringUtil.RTrim( Ddo_contagemresultadoexecucao_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultadoexecucao_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultadoexecucao_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultadoexecucao_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultadoexecucao_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultadoexecucao_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Filtertype", StringUtil.RTrim( Ddo_contagemresultadoexecucao_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Datalistfixedvalues", StringUtil.RTrim( Ddo_contagemresultadoexecucao_codigo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemresultadoexecucao_codigo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Sortasc", StringUtil.RTrim( Ddo_contagemresultadoexecucao_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_contagemresultadoexecucao_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Loadingdata", StringUtil.RTrim( Ddo_contagemresultadoexecucao_codigo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultadoexecucao_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultadoexecucao_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultadoexecucao_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Noresultsfound", StringUtil.RTrim( Ddo_contagemresultadoexecucao_codigo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultadoexecucao_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Caption", StringUtil.RTrim( Ddo_contagemresultadoexecucao_oscod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Tooltip", StringUtil.RTrim( Ddo_contagemresultadoexecucao_oscod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Cls", StringUtil.RTrim( Ddo_contagemresultadoexecucao_oscod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultadoexecucao_oscod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultadoexecucao_oscod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultadoexecucao_oscod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultadoexecucao_oscod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_oscod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_oscod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultadoexecucao_oscod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_oscod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Filtertype", StringUtil.RTrim( Ddo_contagemresultadoexecucao_oscod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_oscod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_oscod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Datalistfixedvalues", StringUtil.RTrim( Ddo_contagemresultadoexecucao_oscod_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemresultadoexecucao_oscod_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Sortasc", StringUtil.RTrim( Ddo_contagemresultadoexecucao_oscod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Sortdsc", StringUtil.RTrim( Ddo_contagemresultadoexecucao_oscod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Loadingdata", StringUtil.RTrim( Ddo_contagemresultadoexecucao_oscod_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultadoexecucao_oscod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultadoexecucao_oscod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultadoexecucao_oscod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Noresultsfound", StringUtil.RTrim( Ddo_contagemresultadoexecucao_oscod_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultadoexecucao_oscod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Caption", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Tooltip", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Cls", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_inicio_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_inicio_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_inicio_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Filtertype", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_inicio_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_inicio_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Datalistfixedvalues", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemresultadoexecucao_inicio_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Sortasc", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Sortdsc", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Loadingdata", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Noresultsfound", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Caption", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Tooltip", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Cls", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_prazodias_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_prazodias_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_prazodias_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Filtertype", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_prazodias_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_prazodias_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Datalistfixedvalues", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemresultadoexecucao_prazodias_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Sortasc", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Sortdsc", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Loadingdata", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Noresultsfound", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Caption", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Tooltip", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Cls", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_prevista_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_prevista_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_prevista_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Filtertype", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_prevista_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_prevista_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Datalistfixedvalues", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemresultadoexecucao_prevista_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Sortasc", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Sortdsc", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Loadingdata", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Noresultsfound", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Caption", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Tooltip", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Cls", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_fim_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_fim_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_fim_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Filtertype", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_fim_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_fim_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Datalistfixedvalues", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemresultadoexecucao_fim_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Sortasc", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Sortdsc", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Loadingdata", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Noresultsfound", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Caption", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Tooltip", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Cls", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Filteredtext_set", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Filteredtextto_set", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Dropdownoptionstype", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Includesortasc", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_dias_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Includesortdsc", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_dias_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Sortedstatus", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Includefilter", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_dias_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Filtertype", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Filterisrange", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_dias_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Includedatalist", StringUtil.BoolToStr( Ddo_contagemresultadoexecucao_dias_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Datalistfixedvalues", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contagemresultadoexecucao_dias_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Sortasc", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Sortdsc", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Loadingdata", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Cleanfilter", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Rangefilterfrom", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Rangefilterto", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Noresultsfound", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Searchbuttontext", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultadoexecucao_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultadoexecucao_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultadoexecucao_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultadoexecucao_oscod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultadoexecucao_oscod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultadoexecucao_oscod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultadoexecucao_inicio_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prazodias_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultadoexecucao_prevista_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultadoexecucao_fim_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Activeeventkey", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Filteredtext_get", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Filteredtextto_get", StringUtil.RTrim( Ddo_contagemresultadoexecucao_dias_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormKJ2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptContagemResultadoExecucao" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Contagem Resultado Execucao" ;
      }

      protected void WBKJ0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_KJ2( true) ;
         }
         else
         {
            wb_table1_2_KJ2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_KJ2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(99, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,99);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(100, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,100);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoexecucao_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31TFContagemResultadoExecucao_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV31TFContagemResultadoExecucao_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoexecucao_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoexecucao_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoExecucao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoexecucao_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFContagemResultadoExecucao_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV32TFContagemResultadoExecucao_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoexecucao_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoexecucao_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoExecucao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoexecucao_oscod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFContagemResultadoExecucao_OSCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV35TFContagemResultadoExecucao_OSCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoexecucao_oscod_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoexecucao_oscod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoExecucao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoexecucao_oscod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFContagemResultadoExecucao_OSCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV36TFContagemResultadoExecucao_OSCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoexecucao_oscod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoexecucao_oscod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoExecucao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemresultadoexecucao_inicio_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoexecucao_inicio_Internalname, context.localUtil.TToC( AV39TFContagemResultadoExecucao_Inicio, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV39TFContagemResultadoExecucao_Inicio, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoexecucao_inicio_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoexecucao_inicio_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemresultadoexecucao_inicio_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemresultadoexecucao_inicio_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemresultadoexecucao_inicio_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoexecucao_inicio_to_Internalname, context.localUtil.TToC( AV40TFContagemResultadoExecucao_Inicio_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV40TFContagemResultadoExecucao_Inicio_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoexecucao_inicio_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoexecucao_inicio_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemresultadoexecucao_inicio_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemresultadoexecucao_inicio_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contagemresultadoexecucao_inicioauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemresultadoexecucao_inicioauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemresultadoexecucao_inicioauxdate_Internalname, context.localUtil.Format(AV41DDO_ContagemResultadoExecucao_InicioAuxDate, "99/99/99"), context.localUtil.Format( AV41DDO_ContagemResultadoExecucao_InicioAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemresultadoexecucao_inicioauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemresultadoexecucao_inicioauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemresultadoexecucao_inicioauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemresultadoexecucao_inicioauxdateto_Internalname, context.localUtil.Format(AV42DDO_ContagemResultadoExecucao_InicioAuxDateTo, "99/99/99"), context.localUtil.Format( AV42DDO_ContagemResultadoExecucao_InicioAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemresultadoexecucao_inicioauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemresultadoexecucao_inicioauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoexecucao_prazodias_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45TFContagemResultadoExecucao_PrazoDias), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV45TFContagemResultadoExecucao_PrazoDias), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoexecucao_prazodias_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoexecucao_prazodias_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoExecucao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoexecucao_prazodias_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV46TFContagemResultadoExecucao_PrazoDias_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV46TFContagemResultadoExecucao_PrazoDias_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoexecucao_prazodias_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoexecucao_prazodias_to_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoExecucao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemresultadoexecucao_prevista_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoexecucao_prevista_Internalname, context.localUtil.TToC( AV49TFContagemResultadoExecucao_Prevista, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV49TFContagemResultadoExecucao_Prevista, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,112);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoexecucao_prevista_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoexecucao_prevista_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemresultadoexecucao_prevista_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemresultadoexecucao_prevista_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemresultadoexecucao_prevista_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoexecucao_prevista_to_Internalname, context.localUtil.TToC( AV50TFContagemResultadoExecucao_Prevista_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV50TFContagemResultadoExecucao_Prevista_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoexecucao_prevista_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoexecucao_prevista_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemresultadoexecucao_prevista_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemresultadoexecucao_prevista_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contagemresultadoexecucao_previstaauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemresultadoexecucao_previstaauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemresultadoexecucao_previstaauxdate_Internalname, context.localUtil.Format(AV51DDO_ContagemResultadoExecucao_PrevistaAuxDate, "99/99/99"), context.localUtil.Format( AV51DDO_ContagemResultadoExecucao_PrevistaAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemresultadoexecucao_previstaauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemresultadoexecucao_previstaauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemresultadoexecucao_previstaauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemresultadoexecucao_previstaauxdateto_Internalname, context.localUtil.Format(AV52DDO_ContagemResultadoExecucao_PrevistaAuxDateTo, "99/99/99"), context.localUtil.Format( AV52DDO_ContagemResultadoExecucao_PrevistaAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemresultadoexecucao_previstaauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemresultadoexecucao_previstaauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemresultadoexecucao_fim_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoexecucao_fim_Internalname, context.localUtil.TToC( AV55TFContagemResultadoExecucao_Fim, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV55TFContagemResultadoExecucao_Fim, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,117);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoexecucao_fim_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoexecucao_fim_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemresultadoexecucao_fim_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemresultadoexecucao_fim_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontagemresultadoexecucao_fim_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoexecucao_fim_to_Internalname, context.localUtil.TToC( AV56TFContagemResultadoExecucao_Fim_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV56TFContagemResultadoExecucao_Fim_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,118);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoexecucao_fim_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoexecucao_fim_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontagemresultadoexecucao_fim_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontagemresultadoexecucao_fim_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contagemresultadoexecucao_fimauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemresultadoexecucao_fimauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemresultadoexecucao_fimauxdate_Internalname, context.localUtil.Format(AV57DDO_ContagemResultadoExecucao_FimAuxDate, "99/99/99"), context.localUtil.Format( AV57DDO_ContagemResultadoExecucao_FimAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,120);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemresultadoexecucao_fimauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemresultadoexecucao_fimauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contagemresultadoexecucao_fimauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contagemresultadoexecucao_fimauxdateto_Internalname, context.localUtil.Format(AV58DDO_ContagemResultadoExecucao_FimAuxDateTo, "99/99/99"), context.localUtil.Format( AV58DDO_ContagemResultadoExecucao_FimAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,121);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contagemresultadoexecucao_fimauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contagemresultadoexecucao_fimauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoexecucao_dias_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV61TFContagemResultadoExecucao_Dias), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV61TFContagemResultadoExecucao_Dias), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,122);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoexecucao_dias_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoexecucao_dias_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoExecucao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontagemresultadoexecucao_dias_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV62TFContagemResultadoExecucao_Dias_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV62TFContagemResultadoExecucao_Dias_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,123);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontagemresultadoexecucao_dias_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontagemresultadoexecucao_dias_to_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoExecucao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADOEXECUCAO_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultadoexecucao_codigotitlecontrolidtoreplace_Internalname, AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,125);\"", 0, edtavDdo_contagemresultadoexecucao_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoExecucao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADOEXECUCAO_OSCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultadoexecucao_oscodtitlecontrolidtoreplace_Internalname, AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,127);\"", 0, edtavDdo_contagemresultadoexecucao_oscodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoExecucao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADOEXECUCAO_INICIOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 129,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultadoexecucao_iniciotitlecontrolidtoreplace_Internalname, AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,129);\"", 0, edtavDdo_contagemresultadoexecucao_iniciotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoExecucao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultadoexecucao_prazodiastitlecontrolidtoreplace_Internalname, AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,131);\"", 0, edtavDdo_contagemresultadoexecucao_prazodiastitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoExecucao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultadoexecucao_previstatitlecontrolidtoreplace_Internalname, AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,133);\"", 0, edtavDdo_contagemresultadoexecucao_previstatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoExecucao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADOEXECUCAO_FIMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultadoexecucao_fimtitlecontrolidtoreplace_Internalname, AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,135);\"", 0, edtavDdo_contagemresultadoexecucao_fimtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoExecucao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTAGEMRESULTADOEXECUCAO_DIASContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 137,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contagemresultadoexecucao_diastitlecontrolidtoreplace_Internalname, AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,137);\"", 0, edtavDdo_contagemresultadoexecucao_diastitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoExecucao.htm");
         }
         wbLoad = true;
      }

      protected void STARTKJ2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Contagem Resultado Execucao", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPKJ0( ) ;
      }

      protected void WSKJ2( )
      {
         STARTKJ2( ) ;
         EVTKJ2( ) ;
      }

      protected void EVTKJ2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11KJ2 */
                           E11KJ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12KJ2 */
                           E12KJ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13KJ2 */
                           E13KJ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14KJ2 */
                           E14KJ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15KJ2 */
                           E15KJ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16KJ2 */
                           E16KJ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADOEXECUCAO_FIM.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17KJ2 */
                           E17KJ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18KJ2 */
                           E18KJ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19KJ2 */
                           E19KJ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20KJ2 */
                           E20KJ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21KJ2 */
                           E21KJ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E22KJ2 */
                           E22KJ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E23KJ2 */
                           E23KJ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E24KJ2 */
                           E24KJ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E25KJ2 */
                           E25KJ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_86_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
                           SubsflControlProps_862( ) ;
                           AV28Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV70Select_GXI : context.convertURL( context.PathToRelativeUrl( AV28Select))));
                           A1405ContagemResultadoExecucao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoExecucao_Codigo_Internalname), ",", "."));
                           A1404ContagemResultadoExecucao_OSCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoExecucao_OSCod_Internalname), ",", "."));
                           A1406ContagemResultadoExecucao_Inicio = context.localUtil.CToT( cgiGet( edtContagemResultadoExecucao_Inicio_Internalname), 0);
                           A1411ContagemResultadoExecucao_PrazoDias = (short)(context.localUtil.CToN( cgiGet( edtContagemResultadoExecucao_PrazoDias_Internalname), ",", "."));
                           n1411ContagemResultadoExecucao_PrazoDias = false;
                           A1408ContagemResultadoExecucao_Prevista = context.localUtil.CToT( cgiGet( edtContagemResultadoExecucao_Prevista_Internalname), 0);
                           n1408ContagemResultadoExecucao_Prevista = false;
                           A1407ContagemResultadoExecucao_Fim = context.localUtil.CToT( cgiGet( edtContagemResultadoExecucao_Fim_Internalname), 0);
                           n1407ContagemResultadoExecucao_Fim = false;
                           A1410ContagemResultadoExecucao_Dias = (short)(context.localUtil.CToN( cgiGet( edtContagemResultadoExecucao_Dias_Internalname), ",", "."));
                           n1410ContagemResultadoExecucao_Dias = false;
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E26KJ2 */
                                 E26KJ2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E27KJ2 */
                                 E27KJ2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E28KJ2 */
                                 E28KJ2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultadoexecucao_inicio1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO1"), 0) != AV16ContagemResultadoExecucao_Inicio1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultadoexecucao_inicio_to1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1"), 0) != AV17ContagemResultadoExecucao_Inicio_To1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultadoexecucao_inicio2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO2"), 0) != AV20ContagemResultadoExecucao_Inicio2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultadoexecucao_inicio_to2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2"), 0) != AV21ContagemResultadoExecucao_Inicio_To2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultadoexecucao_inicio3 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO3"), 0) != AV24ContagemResultadoExecucao_Inicio3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Contagemresultadoexecucao_inicio_to3 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3"), 0) != AV25ContagemResultadoExecucao_Inicio_To3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultadoexecucao_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_CODIGO"), ",", ".") != Convert.ToDecimal( AV31TFContagemResultadoExecucao_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultadoexecucao_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV32TFContagemResultadoExecucao_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultadoexecucao_oscod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_OSCOD"), ",", ".") != Convert.ToDecimal( AV35TFContagemResultadoExecucao_OSCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultadoexecucao_oscod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_OSCOD_TO"), ",", ".") != Convert.ToDecimal( AV36TFContagemResultadoExecucao_OSCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultadoexecucao_inicio Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_INICIO"), 0) != AV39TFContagemResultadoExecucao_Inicio )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultadoexecucao_inicio_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO"), 0) != AV40TFContagemResultadoExecucao_Inicio_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultadoexecucao_prazodias Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS"), ",", ".") != Convert.ToDecimal( AV45TFContagemResultadoExecucao_PrazoDias )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultadoexecucao_prazodias_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO"), ",", ".") != Convert.ToDecimal( AV46TFContagemResultadoExecucao_PrazoDias_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultadoexecucao_prevista Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA"), 0) != AV49TFContagemResultadoExecucao_Prevista )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultadoexecucao_prevista_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO"), 0) != AV50TFContagemResultadoExecucao_Prevista_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultadoexecucao_fim Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_FIM"), 0) != AV55TFContagemResultadoExecucao_Fim )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultadoexecucao_fim_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO"), 0) != AV56TFContagemResultadoExecucao_Fim_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultadoexecucao_dias Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_DIAS"), ",", ".") != Convert.ToDecimal( AV61TFContagemResultadoExecucao_Dias )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontagemresultadoexecucao_dias_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO"), ",", ".") != Convert.ToDecimal( AV62TFContagemResultadoExecucao_Dias_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E29KJ2 */
                                       E29KJ2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEKJ2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormKJ2( ) ;
            }
         }
      }

      protected void PAKJ2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTAGEMRESULTADOEXECUCAO_INICIO", "Inicio", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTAGEMRESULTADOEXECUCAO_INICIO", "Inicio", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTAGEMRESULTADOEXECUCAO_INICIO", "Inicio", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_862( ) ;
         while ( nGXsfl_86_idx <= nRC_GXsfl_86 )
         {
            sendrow_862( ) ;
            nGXsfl_86_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_86_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_86_idx+1));
            sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
            SubsflControlProps_862( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       DateTime AV16ContagemResultadoExecucao_Inicio1 ,
                                       DateTime AV17ContagemResultadoExecucao_Inicio_To1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       DateTime AV20ContagemResultadoExecucao_Inicio2 ,
                                       DateTime AV21ContagemResultadoExecucao_Inicio_To2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       DateTime AV24ContagemResultadoExecucao_Inicio3 ,
                                       DateTime AV25ContagemResultadoExecucao_Inicio_To3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       int AV31TFContagemResultadoExecucao_Codigo ,
                                       int AV32TFContagemResultadoExecucao_Codigo_To ,
                                       int AV35TFContagemResultadoExecucao_OSCod ,
                                       int AV36TFContagemResultadoExecucao_OSCod_To ,
                                       DateTime AV39TFContagemResultadoExecucao_Inicio ,
                                       DateTime AV40TFContagemResultadoExecucao_Inicio_To ,
                                       short AV45TFContagemResultadoExecucao_PrazoDias ,
                                       short AV46TFContagemResultadoExecucao_PrazoDias_To ,
                                       DateTime AV49TFContagemResultadoExecucao_Prevista ,
                                       DateTime AV50TFContagemResultadoExecucao_Prevista_To ,
                                       DateTime AV55TFContagemResultadoExecucao_Fim ,
                                       DateTime AV56TFContagemResultadoExecucao_Fim_To ,
                                       short AV61TFContagemResultadoExecucao_Dias ,
                                       short AV62TFContagemResultadoExecucao_Dias_To ,
                                       String AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace ,
                                       String AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace ,
                                       String AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace ,
                                       String AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace ,
                                       String AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace ,
                                       String AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace ,
                                       String AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFKJ2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEXECUCAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1405ContagemResultadoExecucao_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEXECUCAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1405ContagemResultadoExecucao_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEXECUCAO_OSCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1404ContagemResultadoExecucao_OSCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEXECUCAO_OSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1404ContagemResultadoExecucao_OSCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEXECUCAO_INICIO", GetSecureSignedToken( "", context.localUtil.Format( A1406ContagemResultadoExecucao_Inicio, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEXECUCAO_INICIO", context.localUtil.TToC( A1406ContagemResultadoExecucao_Inicio, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEXECUCAO_PRAZODIAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEXECUCAO_PREVISTA", GetSecureSignedToken( "", context.localUtil.Format( A1408ContagemResultadoExecucao_Prevista, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEXECUCAO_PREVISTA", context.localUtil.TToC( A1408ContagemResultadoExecucao_Prevista, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEXECUCAO_FIM", GetSecureSignedToken( "", context.localUtil.Format( A1407ContagemResultadoExecucao_Fim, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEXECUCAO_FIM", context.localUtil.TToC( A1407ContagemResultadoExecucao_Fim, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEXECUCAO_DIAS", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1410ContagemResultadoExecucao_Dias), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEXECUCAO_DIAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1410ContagemResultadoExecucao_Dias), 4, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFKJ2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFKJ2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 86;
         /* Execute user event: E27KJ2 */
         E27KJ2 ();
         nGXsfl_86_idx = 1;
         sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
         SubsflControlProps_862( ) ;
         nGXsfl_86_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_862( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16ContagemResultadoExecucao_Inicio1 ,
                                                 AV17ContagemResultadoExecucao_Inicio_To1 ,
                                                 AV18DynamicFiltersEnabled2 ,
                                                 AV19DynamicFiltersSelector2 ,
                                                 AV20ContagemResultadoExecucao_Inicio2 ,
                                                 AV21ContagemResultadoExecucao_Inicio_To2 ,
                                                 AV22DynamicFiltersEnabled3 ,
                                                 AV23DynamicFiltersSelector3 ,
                                                 AV24ContagemResultadoExecucao_Inicio3 ,
                                                 AV25ContagemResultadoExecucao_Inicio_To3 ,
                                                 AV31TFContagemResultadoExecucao_Codigo ,
                                                 AV32TFContagemResultadoExecucao_Codigo_To ,
                                                 AV35TFContagemResultadoExecucao_OSCod ,
                                                 AV36TFContagemResultadoExecucao_OSCod_To ,
                                                 AV39TFContagemResultadoExecucao_Inicio ,
                                                 AV40TFContagemResultadoExecucao_Inicio_To ,
                                                 AV45TFContagemResultadoExecucao_PrazoDias ,
                                                 AV46TFContagemResultadoExecucao_PrazoDias_To ,
                                                 AV49TFContagemResultadoExecucao_Prevista ,
                                                 AV50TFContagemResultadoExecucao_Prevista_To ,
                                                 AV55TFContagemResultadoExecucao_Fim ,
                                                 AV56TFContagemResultadoExecucao_Fim_To ,
                                                 AV61TFContagemResultadoExecucao_Dias ,
                                                 AV62TFContagemResultadoExecucao_Dias_To ,
                                                 A1406ContagemResultadoExecucao_Inicio ,
                                                 A1405ContagemResultadoExecucao_Codigo ,
                                                 A1404ContagemResultadoExecucao_OSCod ,
                                                 A1411ContagemResultadoExecucao_PrazoDias ,
                                                 A1408ContagemResultadoExecucao_Prevista ,
                                                 A1407ContagemResultadoExecucao_Fim ,
                                                 A1410ContagemResultadoExecucao_Dias ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                                 TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor H00KJ2 */
            pr_default.execute(0, new Object[] {AV16ContagemResultadoExecucao_Inicio1, AV17ContagemResultadoExecucao_Inicio_To1, AV20ContagemResultadoExecucao_Inicio2, AV21ContagemResultadoExecucao_Inicio_To2, AV24ContagemResultadoExecucao_Inicio3, AV25ContagemResultadoExecucao_Inicio_To3, AV31TFContagemResultadoExecucao_Codigo, AV32TFContagemResultadoExecucao_Codigo_To, AV35TFContagemResultadoExecucao_OSCod, AV36TFContagemResultadoExecucao_OSCod_To, AV39TFContagemResultadoExecucao_Inicio, AV40TFContagemResultadoExecucao_Inicio_To, AV45TFContagemResultadoExecucao_PrazoDias, AV46TFContagemResultadoExecucao_PrazoDias_To, AV49TFContagemResultadoExecucao_Prevista, AV50TFContagemResultadoExecucao_Prevista_To, AV55TFContagemResultadoExecucao_Fim, AV56TFContagemResultadoExecucao_Fim_To, AV61TFContagemResultadoExecucao_Dias, AV62TFContagemResultadoExecucao_Dias_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_86_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1410ContagemResultadoExecucao_Dias = H00KJ2_A1410ContagemResultadoExecucao_Dias[0];
               n1410ContagemResultadoExecucao_Dias = H00KJ2_n1410ContagemResultadoExecucao_Dias[0];
               A1407ContagemResultadoExecucao_Fim = H00KJ2_A1407ContagemResultadoExecucao_Fim[0];
               n1407ContagemResultadoExecucao_Fim = H00KJ2_n1407ContagemResultadoExecucao_Fim[0];
               A1408ContagemResultadoExecucao_Prevista = H00KJ2_A1408ContagemResultadoExecucao_Prevista[0];
               n1408ContagemResultadoExecucao_Prevista = H00KJ2_n1408ContagemResultadoExecucao_Prevista[0];
               A1411ContagemResultadoExecucao_PrazoDias = H00KJ2_A1411ContagemResultadoExecucao_PrazoDias[0];
               n1411ContagemResultadoExecucao_PrazoDias = H00KJ2_n1411ContagemResultadoExecucao_PrazoDias[0];
               A1406ContagemResultadoExecucao_Inicio = H00KJ2_A1406ContagemResultadoExecucao_Inicio[0];
               A1404ContagemResultadoExecucao_OSCod = H00KJ2_A1404ContagemResultadoExecucao_OSCod[0];
               A1405ContagemResultadoExecucao_Codigo = H00KJ2_A1405ContagemResultadoExecucao_Codigo[0];
               /* Execute user event: E28KJ2 */
               E28KJ2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 86;
            WBKJ0( ) ;
         }
         nGXsfl_86_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV16ContagemResultadoExecucao_Inicio1 ,
                                              AV17ContagemResultadoExecucao_Inicio_To1 ,
                                              AV18DynamicFiltersEnabled2 ,
                                              AV19DynamicFiltersSelector2 ,
                                              AV20ContagemResultadoExecucao_Inicio2 ,
                                              AV21ContagemResultadoExecucao_Inicio_To2 ,
                                              AV22DynamicFiltersEnabled3 ,
                                              AV23DynamicFiltersSelector3 ,
                                              AV24ContagemResultadoExecucao_Inicio3 ,
                                              AV25ContagemResultadoExecucao_Inicio_To3 ,
                                              AV31TFContagemResultadoExecucao_Codigo ,
                                              AV32TFContagemResultadoExecucao_Codigo_To ,
                                              AV35TFContagemResultadoExecucao_OSCod ,
                                              AV36TFContagemResultadoExecucao_OSCod_To ,
                                              AV39TFContagemResultadoExecucao_Inicio ,
                                              AV40TFContagemResultadoExecucao_Inicio_To ,
                                              AV45TFContagemResultadoExecucao_PrazoDias ,
                                              AV46TFContagemResultadoExecucao_PrazoDias_To ,
                                              AV49TFContagemResultadoExecucao_Prevista ,
                                              AV50TFContagemResultadoExecucao_Prevista_To ,
                                              AV55TFContagemResultadoExecucao_Fim ,
                                              AV56TFContagemResultadoExecucao_Fim_To ,
                                              AV61TFContagemResultadoExecucao_Dias ,
                                              AV62TFContagemResultadoExecucao_Dias_To ,
                                              A1406ContagemResultadoExecucao_Inicio ,
                                              A1405ContagemResultadoExecucao_Codigo ,
                                              A1404ContagemResultadoExecucao_OSCod ,
                                              A1411ContagemResultadoExecucao_PrazoDias ,
                                              A1408ContagemResultadoExecucao_Prevista ,
                                              A1407ContagemResultadoExecucao_Fim ,
                                              A1410ContagemResultadoExecucao_Dias ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00KJ3 */
         pr_default.execute(1, new Object[] {AV16ContagemResultadoExecucao_Inicio1, AV17ContagemResultadoExecucao_Inicio_To1, AV20ContagemResultadoExecucao_Inicio2, AV21ContagemResultadoExecucao_Inicio_To2, AV24ContagemResultadoExecucao_Inicio3, AV25ContagemResultadoExecucao_Inicio_To3, AV31TFContagemResultadoExecucao_Codigo, AV32TFContagemResultadoExecucao_Codigo_To, AV35TFContagemResultadoExecucao_OSCod, AV36TFContagemResultadoExecucao_OSCod_To, AV39TFContagemResultadoExecucao_Inicio, AV40TFContagemResultadoExecucao_Inicio_To, AV45TFContagemResultadoExecucao_PrazoDias, AV46TFContagemResultadoExecucao_PrazoDias_To, AV49TFContagemResultadoExecucao_Prevista, AV50TFContagemResultadoExecucao_Prevista_To, AV55TFContagemResultadoExecucao_Fim, AV56TFContagemResultadoExecucao_Fim_To, AV61TFContagemResultadoExecucao_Dias, AV62TFContagemResultadoExecucao_Dias_To});
         GRID_nRecordCount = H00KJ3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContagemResultadoExecucao_Inicio1, AV17ContagemResultadoExecucao_Inicio_To1, AV19DynamicFiltersSelector2, AV20ContagemResultadoExecucao_Inicio2, AV21ContagemResultadoExecucao_Inicio_To2, AV23DynamicFiltersSelector3, AV24ContagemResultadoExecucao_Inicio3, AV25ContagemResultadoExecucao_Inicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContagemResultadoExecucao_Codigo, AV32TFContagemResultadoExecucao_Codigo_To, AV35TFContagemResultadoExecucao_OSCod, AV36TFContagemResultadoExecucao_OSCod_To, AV39TFContagemResultadoExecucao_Inicio, AV40TFContagemResultadoExecucao_Inicio_To, AV45TFContagemResultadoExecucao_PrazoDias, AV46TFContagemResultadoExecucao_PrazoDias_To, AV49TFContagemResultadoExecucao_Prevista, AV50TFContagemResultadoExecucao_Prevista_To, AV55TFContagemResultadoExecucao_Fim, AV56TFContagemResultadoExecucao_Fim_To, AV61TFContagemResultadoExecucao_Dias, AV62TFContagemResultadoExecucao_Dias_To, AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace, AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace, AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace, AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace, AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace, AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace, AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContagemResultadoExecucao_Inicio1, AV17ContagemResultadoExecucao_Inicio_To1, AV19DynamicFiltersSelector2, AV20ContagemResultadoExecucao_Inicio2, AV21ContagemResultadoExecucao_Inicio_To2, AV23DynamicFiltersSelector3, AV24ContagemResultadoExecucao_Inicio3, AV25ContagemResultadoExecucao_Inicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContagemResultadoExecucao_Codigo, AV32TFContagemResultadoExecucao_Codigo_To, AV35TFContagemResultadoExecucao_OSCod, AV36TFContagemResultadoExecucao_OSCod_To, AV39TFContagemResultadoExecucao_Inicio, AV40TFContagemResultadoExecucao_Inicio_To, AV45TFContagemResultadoExecucao_PrazoDias, AV46TFContagemResultadoExecucao_PrazoDias_To, AV49TFContagemResultadoExecucao_Prevista, AV50TFContagemResultadoExecucao_Prevista_To, AV55TFContagemResultadoExecucao_Fim, AV56TFContagemResultadoExecucao_Fim_To, AV61TFContagemResultadoExecucao_Dias, AV62TFContagemResultadoExecucao_Dias_To, AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace, AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace, AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace, AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace, AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace, AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace, AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContagemResultadoExecucao_Inicio1, AV17ContagemResultadoExecucao_Inicio_To1, AV19DynamicFiltersSelector2, AV20ContagemResultadoExecucao_Inicio2, AV21ContagemResultadoExecucao_Inicio_To2, AV23DynamicFiltersSelector3, AV24ContagemResultadoExecucao_Inicio3, AV25ContagemResultadoExecucao_Inicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContagemResultadoExecucao_Codigo, AV32TFContagemResultadoExecucao_Codigo_To, AV35TFContagemResultadoExecucao_OSCod, AV36TFContagemResultadoExecucao_OSCod_To, AV39TFContagemResultadoExecucao_Inicio, AV40TFContagemResultadoExecucao_Inicio_To, AV45TFContagemResultadoExecucao_PrazoDias, AV46TFContagemResultadoExecucao_PrazoDias_To, AV49TFContagemResultadoExecucao_Prevista, AV50TFContagemResultadoExecucao_Prevista_To, AV55TFContagemResultadoExecucao_Fim, AV56TFContagemResultadoExecucao_Fim_To, AV61TFContagemResultadoExecucao_Dias, AV62TFContagemResultadoExecucao_Dias_To, AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace, AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace, AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace, AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace, AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace, AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace, AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContagemResultadoExecucao_Inicio1, AV17ContagemResultadoExecucao_Inicio_To1, AV19DynamicFiltersSelector2, AV20ContagemResultadoExecucao_Inicio2, AV21ContagemResultadoExecucao_Inicio_To2, AV23DynamicFiltersSelector3, AV24ContagemResultadoExecucao_Inicio3, AV25ContagemResultadoExecucao_Inicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContagemResultadoExecucao_Codigo, AV32TFContagemResultadoExecucao_Codigo_To, AV35TFContagemResultadoExecucao_OSCod, AV36TFContagemResultadoExecucao_OSCod_To, AV39TFContagemResultadoExecucao_Inicio, AV40TFContagemResultadoExecucao_Inicio_To, AV45TFContagemResultadoExecucao_PrazoDias, AV46TFContagemResultadoExecucao_PrazoDias_To, AV49TFContagemResultadoExecucao_Prevista, AV50TFContagemResultadoExecucao_Prevista_To, AV55TFContagemResultadoExecucao_Fim, AV56TFContagemResultadoExecucao_Fim_To, AV61TFContagemResultadoExecucao_Dias, AV62TFContagemResultadoExecucao_Dias_To, AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace, AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace, AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace, AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace, AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace, AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace, AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContagemResultadoExecucao_Inicio1, AV17ContagemResultadoExecucao_Inicio_To1, AV19DynamicFiltersSelector2, AV20ContagemResultadoExecucao_Inicio2, AV21ContagemResultadoExecucao_Inicio_To2, AV23DynamicFiltersSelector3, AV24ContagemResultadoExecucao_Inicio3, AV25ContagemResultadoExecucao_Inicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContagemResultadoExecucao_Codigo, AV32TFContagemResultadoExecucao_Codigo_To, AV35TFContagemResultadoExecucao_OSCod, AV36TFContagemResultadoExecucao_OSCod_To, AV39TFContagemResultadoExecucao_Inicio, AV40TFContagemResultadoExecucao_Inicio_To, AV45TFContagemResultadoExecucao_PrazoDias, AV46TFContagemResultadoExecucao_PrazoDias_To, AV49TFContagemResultadoExecucao_Prevista, AV50TFContagemResultadoExecucao_Prevista_To, AV55TFContagemResultadoExecucao_Fim, AV56TFContagemResultadoExecucao_Fim_To, AV61TFContagemResultadoExecucao_Dias, AV62TFContagemResultadoExecucao_Dias_To, AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace, AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace, AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace, AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace, AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace, AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace, AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace) ;
         }
         return (int)(0) ;
      }

      protected void STRUPKJ0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E26KJ2 */
         E26KJ2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV64DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADOEXECUCAO_CODIGOTITLEFILTERDATA"), AV30ContagemResultadoExecucao_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADOEXECUCAO_OSCODTITLEFILTERDATA"), AV34ContagemResultadoExecucao_OSCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADOEXECUCAO_INICIOTITLEFILTERDATA"), AV38ContagemResultadoExecucao_InicioTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLEFILTERDATA"), AV44ContagemResultadoExecucao_PrazoDiasTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADOEXECUCAO_PREVISTATITLEFILTERDATA"), AV48ContagemResultadoExecucao_PrevistaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADOEXECUCAO_FIMTITLEFILTERDATA"), AV54ContagemResultadoExecucao_FimTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTAGEMRESULTADOEXECUCAO_DIASTITLEFILTERDATA"), AV60ContagemResultadoExecucao_DiasTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoexecucao_inicio1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Execucao_Inicio1"}), 1, "vCONTAGEMRESULTADOEXECUCAO_INICIO1");
               GX_FocusControl = edtavContagemresultadoexecucao_inicio1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV16ContagemResultadoExecucao_Inicio1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultadoExecucao_Inicio1", context.localUtil.TToC( AV16ContagemResultadoExecucao_Inicio1, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV16ContagemResultadoExecucao_Inicio1 = context.localUtil.CToT( cgiGet( edtavContagemresultadoexecucao_inicio1_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultadoExecucao_Inicio1", context.localUtil.TToC( AV16ContagemResultadoExecucao_Inicio1, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoexecucao_inicio_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Execucao_Inicio_To1"}), 1, "vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1");
               GX_FocusControl = edtavContagemresultadoexecucao_inicio_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17ContagemResultadoExecucao_Inicio_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultadoExecucao_Inicio_To1", context.localUtil.TToC( AV17ContagemResultadoExecucao_Inicio_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV17ContagemResultadoExecucao_Inicio_To1 = context.localUtil.CToT( cgiGet( edtavContagemresultadoexecucao_inicio_to1_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultadoExecucao_Inicio_To1", context.localUtil.TToC( AV17ContagemResultadoExecucao_Inicio_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoexecucao_inicio2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Execucao_Inicio2"}), 1, "vCONTAGEMRESULTADOEXECUCAO_INICIO2");
               GX_FocusControl = edtavContagemresultadoexecucao_inicio2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20ContagemResultadoExecucao_Inicio2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultadoExecucao_Inicio2", context.localUtil.TToC( AV20ContagemResultadoExecucao_Inicio2, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV20ContagemResultadoExecucao_Inicio2 = context.localUtil.CToT( cgiGet( edtavContagemresultadoexecucao_inicio2_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultadoExecucao_Inicio2", context.localUtil.TToC( AV20ContagemResultadoExecucao_Inicio2, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoexecucao_inicio_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Execucao_Inicio_To2"}), 1, "vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2");
               GX_FocusControl = edtavContagemresultadoexecucao_inicio_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21ContagemResultadoExecucao_Inicio_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultadoExecucao_Inicio_To2", context.localUtil.TToC( AV21ContagemResultadoExecucao_Inicio_To2, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV21ContagemResultadoExecucao_Inicio_To2 = context.localUtil.CToT( cgiGet( edtavContagemresultadoexecucao_inicio_to2_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultadoExecucao_Inicio_To2", context.localUtil.TToC( AV21ContagemResultadoExecucao_Inicio_To2, 8, 5, 0, 3, "/", ":", " "));
            }
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoexecucao_inicio3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Execucao_Inicio3"}), 1, "vCONTAGEMRESULTADOEXECUCAO_INICIO3");
               GX_FocusControl = edtavContagemresultadoexecucao_inicio3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24ContagemResultadoExecucao_Inicio3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultadoExecucao_Inicio3", context.localUtil.TToC( AV24ContagemResultadoExecucao_Inicio3, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV24ContagemResultadoExecucao_Inicio3 = context.localUtil.CToT( cgiGet( edtavContagemresultadoexecucao_inicio3_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultadoExecucao_Inicio3", context.localUtil.TToC( AV24ContagemResultadoExecucao_Inicio3, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultadoexecucao_inicio_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Contagem Resultado Execucao_Inicio_To3"}), 1, "vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3");
               GX_FocusControl = edtavContagemresultadoexecucao_inicio_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV25ContagemResultadoExecucao_Inicio_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContagemResultadoExecucao_Inicio_To3", context.localUtil.TToC( AV25ContagemResultadoExecucao_Inicio_To3, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV25ContagemResultadoExecucao_Inicio_To3 = context.localUtil.CToT( cgiGet( edtavContagemresultadoexecucao_inicio_to3_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContagemResultadoExecucao_Inicio_To3", context.localUtil.TToC( AV25ContagemResultadoExecucao_Inicio_To3, 8, 5, 0, 3, "/", ":", " "));
            }
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADOEXECUCAO_CODIGO");
               GX_FocusControl = edtavTfcontagemresultadoexecucao_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV31TFContagemResultadoExecucao_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFContagemResultadoExecucao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContagemResultadoExecucao_Codigo), 6, 0)));
            }
            else
            {
               AV31TFContagemResultadoExecucao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFContagemResultadoExecucao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContagemResultadoExecucao_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADOEXECUCAO_CODIGO_TO");
               GX_FocusControl = edtavTfcontagemresultadoexecucao_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32TFContagemResultadoExecucao_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContagemResultadoExecucao_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContagemResultadoExecucao_Codigo_To), 6, 0)));
            }
            else
            {
               AV32TFContagemResultadoExecucao_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContagemResultadoExecucao_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContagemResultadoExecucao_Codigo_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_oscod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_oscod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADOEXECUCAO_OSCOD");
               GX_FocusControl = edtavTfcontagemresultadoexecucao_oscod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35TFContagemResultadoExecucao_OSCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContagemResultadoExecucao_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFContagemResultadoExecucao_OSCod), 6, 0)));
            }
            else
            {
               AV35TFContagemResultadoExecucao_OSCod = (int)(context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_oscod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContagemResultadoExecucao_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFContagemResultadoExecucao_OSCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_oscod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_oscod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADOEXECUCAO_OSCOD_TO");
               GX_FocusControl = edtavTfcontagemresultadoexecucao_oscod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36TFContagemResultadoExecucao_OSCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContagemResultadoExecucao_OSCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContagemResultadoExecucao_OSCod_To), 6, 0)));
            }
            else
            {
               AV36TFContagemResultadoExecucao_OSCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_oscod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContagemResultadoExecucao_OSCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContagemResultadoExecucao_OSCod_To), 6, 0)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemresultadoexecucao_inicio_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFContagem Resultado Execucao_Inicio"}), 1, "vTFCONTAGEMRESULTADOEXECUCAO_INICIO");
               GX_FocusControl = edtavTfcontagemresultadoexecucao_inicio_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39TFContagemResultadoExecucao_Inicio = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContagemResultadoExecucao_Inicio", context.localUtil.TToC( AV39TFContagemResultadoExecucao_Inicio, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV39TFContagemResultadoExecucao_Inicio = context.localUtil.CToT( cgiGet( edtavTfcontagemresultadoexecucao_inicio_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContagemResultadoExecucao_Inicio", context.localUtil.TToC( AV39TFContagemResultadoExecucao_Inicio, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemresultadoexecucao_inicio_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFContagem Resultado Execucao_Inicio_To"}), 1, "vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO");
               GX_FocusControl = edtavTfcontagemresultadoexecucao_inicio_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40TFContagemResultadoExecucao_Inicio_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContagemResultadoExecucao_Inicio_To", context.localUtil.TToC( AV40TFContagemResultadoExecucao_Inicio_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV40TFContagemResultadoExecucao_Inicio_To = context.localUtil.CToT( cgiGet( edtavTfcontagemresultadoexecucao_inicio_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContagemResultadoExecucao_Inicio_To", context.localUtil.TToC( AV40TFContagemResultadoExecucao_Inicio_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemresultadoexecucao_inicioauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Resultado Execucao_Inicio Aux Date"}), 1, "vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOAUXDATE");
               GX_FocusControl = edtavDdo_contagemresultadoexecucao_inicioauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV41DDO_ContagemResultadoExecucao_InicioAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41DDO_ContagemResultadoExecucao_InicioAuxDate", context.localUtil.Format(AV41DDO_ContagemResultadoExecucao_InicioAuxDate, "99/99/99"));
            }
            else
            {
               AV41DDO_ContagemResultadoExecucao_InicioAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemresultadoexecucao_inicioauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41DDO_ContagemResultadoExecucao_InicioAuxDate", context.localUtil.Format(AV41DDO_ContagemResultadoExecucao_InicioAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemresultadoexecucao_inicioauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Resultado Execucao_Inicio Aux Date To"}), 1, "vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOAUXDATETO");
               GX_FocusControl = edtavDdo_contagemresultadoexecucao_inicioauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV42DDO_ContagemResultadoExecucao_InicioAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42DDO_ContagemResultadoExecucao_InicioAuxDateTo", context.localUtil.Format(AV42DDO_ContagemResultadoExecucao_InicioAuxDateTo, "99/99/99"));
            }
            else
            {
               AV42DDO_ContagemResultadoExecucao_InicioAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemresultadoexecucao_inicioauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42DDO_ContagemResultadoExecucao_InicioAuxDateTo", context.localUtil.Format(AV42DDO_ContagemResultadoExecucao_InicioAuxDateTo, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_prazodias_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_prazodias_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS");
               GX_FocusControl = edtavTfcontagemresultadoexecucao_prazodias_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV45TFContagemResultadoExecucao_PrazoDias = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContagemResultadoExecucao_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFContagemResultadoExecucao_PrazoDias), 4, 0)));
            }
            else
            {
               AV45TFContagemResultadoExecucao_PrazoDias = (short)(context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_prazodias_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContagemResultadoExecucao_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFContagemResultadoExecucao_PrazoDias), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_prazodias_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_prazodias_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO");
               GX_FocusControl = edtavTfcontagemresultadoexecucao_prazodias_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV46TFContagemResultadoExecucao_PrazoDias_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContagemResultadoExecucao_PrazoDias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFContagemResultadoExecucao_PrazoDias_To), 4, 0)));
            }
            else
            {
               AV46TFContagemResultadoExecucao_PrazoDias_To = (short)(context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_prazodias_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContagemResultadoExecucao_PrazoDias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFContagemResultadoExecucao_PrazoDias_To), 4, 0)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemresultadoexecucao_prevista_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFContagem Resultado Execucao_Prevista"}), 1, "vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA");
               GX_FocusControl = edtavTfcontagemresultadoexecucao_prevista_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV49TFContagemResultadoExecucao_Prevista = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContagemResultadoExecucao_Prevista", context.localUtil.TToC( AV49TFContagemResultadoExecucao_Prevista, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV49TFContagemResultadoExecucao_Prevista = context.localUtil.CToT( cgiGet( edtavTfcontagemresultadoexecucao_prevista_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContagemResultadoExecucao_Prevista", context.localUtil.TToC( AV49TFContagemResultadoExecucao_Prevista, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemresultadoexecucao_prevista_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFContagem Resultado Execucao_Prevista_To"}), 1, "vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO");
               GX_FocusControl = edtavTfcontagemresultadoexecucao_prevista_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV50TFContagemResultadoExecucao_Prevista_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContagemResultadoExecucao_Prevista_To", context.localUtil.TToC( AV50TFContagemResultadoExecucao_Prevista_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV50TFContagemResultadoExecucao_Prevista_To = context.localUtil.CToT( cgiGet( edtavTfcontagemresultadoexecucao_prevista_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContagemResultadoExecucao_Prevista_To", context.localUtil.TToC( AV50TFContagemResultadoExecucao_Prevista_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemresultadoexecucao_previstaauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Resultado Execucao_Prevista Aux Date"}), 1, "vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAAUXDATE");
               GX_FocusControl = edtavDdo_contagemresultadoexecucao_previstaauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV51DDO_ContagemResultadoExecucao_PrevistaAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51DDO_ContagemResultadoExecucao_PrevistaAuxDate", context.localUtil.Format(AV51DDO_ContagemResultadoExecucao_PrevistaAuxDate, "99/99/99"));
            }
            else
            {
               AV51DDO_ContagemResultadoExecucao_PrevistaAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemresultadoexecucao_previstaauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51DDO_ContagemResultadoExecucao_PrevistaAuxDate", context.localUtil.Format(AV51DDO_ContagemResultadoExecucao_PrevistaAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemresultadoexecucao_previstaauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Resultado Execucao_Prevista Aux Date To"}), 1, "vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAAUXDATETO");
               GX_FocusControl = edtavDdo_contagemresultadoexecucao_previstaauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV52DDO_ContagemResultadoExecucao_PrevistaAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52DDO_ContagemResultadoExecucao_PrevistaAuxDateTo", context.localUtil.Format(AV52DDO_ContagemResultadoExecucao_PrevistaAuxDateTo, "99/99/99"));
            }
            else
            {
               AV52DDO_ContagemResultadoExecucao_PrevistaAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemresultadoexecucao_previstaauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52DDO_ContagemResultadoExecucao_PrevistaAuxDateTo", context.localUtil.Format(AV52DDO_ContagemResultadoExecucao_PrevistaAuxDateTo, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemresultadoexecucao_fim_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFContagem Resultado Execucao_Fim"}), 1, "vTFCONTAGEMRESULTADOEXECUCAO_FIM");
               GX_FocusControl = edtavTfcontagemresultadoexecucao_fim_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV55TFContagemResultadoExecucao_Fim = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContagemResultadoExecucao_Fim", context.localUtil.TToC( AV55TFContagemResultadoExecucao_Fim, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV55TFContagemResultadoExecucao_Fim = context.localUtil.CToT( cgiGet( edtavTfcontagemresultadoexecucao_fim_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContagemResultadoExecucao_Fim", context.localUtil.TToC( AV55TFContagemResultadoExecucao_Fim, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontagemresultadoexecucao_fim_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFContagem Resultado Execucao_Fim_To"}), 1, "vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO");
               GX_FocusControl = edtavTfcontagemresultadoexecucao_fim_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV56TFContagemResultadoExecucao_Fim_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContagemResultadoExecucao_Fim_To", context.localUtil.TToC( AV56TFContagemResultadoExecucao_Fim_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV56TFContagemResultadoExecucao_Fim_To = context.localUtil.CToT( cgiGet( edtavTfcontagemresultadoexecucao_fim_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContagemResultadoExecucao_Fim_To", context.localUtil.TToC( AV56TFContagemResultadoExecucao_Fim_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemresultadoexecucao_fimauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Resultado Execucao_Fim Aux Date"}), 1, "vDDO_CONTAGEMRESULTADOEXECUCAO_FIMAUXDATE");
               GX_FocusControl = edtavDdo_contagemresultadoexecucao_fimauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV57DDO_ContagemResultadoExecucao_FimAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57DDO_ContagemResultadoExecucao_FimAuxDate", context.localUtil.Format(AV57DDO_ContagemResultadoExecucao_FimAuxDate, "99/99/99"));
            }
            else
            {
               AV57DDO_ContagemResultadoExecucao_FimAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemresultadoexecucao_fimauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57DDO_ContagemResultadoExecucao_FimAuxDate", context.localUtil.Format(AV57DDO_ContagemResultadoExecucao_FimAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contagemresultadoexecucao_fimauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contagem Resultado Execucao_Fim Aux Date To"}), 1, "vDDO_CONTAGEMRESULTADOEXECUCAO_FIMAUXDATETO");
               GX_FocusControl = edtavDdo_contagemresultadoexecucao_fimauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV58DDO_ContagemResultadoExecucao_FimAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58DDO_ContagemResultadoExecucao_FimAuxDateTo", context.localUtil.Format(AV58DDO_ContagemResultadoExecucao_FimAuxDateTo, "99/99/99"));
            }
            else
            {
               AV58DDO_ContagemResultadoExecucao_FimAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contagemresultadoexecucao_fimauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58DDO_ContagemResultadoExecucao_FimAuxDateTo", context.localUtil.Format(AV58DDO_ContagemResultadoExecucao_FimAuxDateTo, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_dias_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_dias_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADOEXECUCAO_DIAS");
               GX_FocusControl = edtavTfcontagemresultadoexecucao_dias_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV61TFContagemResultadoExecucao_Dias = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContagemResultadoExecucao_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFContagemResultadoExecucao_Dias), 4, 0)));
            }
            else
            {
               AV61TFContagemResultadoExecucao_Dias = (short)(context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_dias_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContagemResultadoExecucao_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFContagemResultadoExecucao_Dias), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_dias_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_dias_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO");
               GX_FocusControl = edtavTfcontagemresultadoexecucao_dias_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV62TFContagemResultadoExecucao_Dias_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContagemResultadoExecucao_Dias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFContagemResultadoExecucao_Dias_To), 4, 0)));
            }
            else
            {
               AV62TFContagemResultadoExecucao_Dias_To = (short)(context.localUtil.CToN( cgiGet( edtavTfcontagemresultadoexecucao_dias_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContagemResultadoExecucao_Dias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFContagemResultadoExecucao_Dias_To), 4, 0)));
            }
            AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultadoexecucao_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace", AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace);
            AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultadoexecucao_oscodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace", AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace);
            AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultadoexecucao_iniciotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace", AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace);
            AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultadoexecucao_prazodiastitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace", AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace);
            AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultadoexecucao_previstatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace", AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace);
            AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultadoexecucao_fimtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace", AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace);
            AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace = cgiGet( edtavDdo_contagemresultadoexecucao_diastitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace", AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_86 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_86"), ",", "."));
            AV66GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV67GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contagemresultadoexecucao_codigo_Caption = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Caption");
            Ddo_contagemresultadoexecucao_codigo_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Tooltip");
            Ddo_contagemresultadoexecucao_codigo_Cls = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Cls");
            Ddo_contagemresultadoexecucao_codigo_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Filteredtext_set");
            Ddo_contagemresultadoexecucao_codigo_Filteredtextto_set = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Filteredtextto_set");
            Ddo_contagemresultadoexecucao_codigo_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Dropdownoptionstype");
            Ddo_contagemresultadoexecucao_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Titlecontrolidtoreplace");
            Ddo_contagemresultadoexecucao_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Includesortasc"));
            Ddo_contagemresultadoexecucao_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Includesortdsc"));
            Ddo_contagemresultadoexecucao_codigo_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Sortedstatus");
            Ddo_contagemresultadoexecucao_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Includefilter"));
            Ddo_contagemresultadoexecucao_codigo_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Filtertype");
            Ddo_contagemresultadoexecucao_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Filterisrange"));
            Ddo_contagemresultadoexecucao_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Includedatalist"));
            Ddo_contagemresultadoexecucao_codigo_Datalistfixedvalues = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Datalistfixedvalues");
            Ddo_contagemresultadoexecucao_codigo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemresultadoexecucao_codigo_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Sortasc");
            Ddo_contagemresultadoexecucao_codigo_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Sortdsc");
            Ddo_contagemresultadoexecucao_codigo_Loadingdata = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Loadingdata");
            Ddo_contagemresultadoexecucao_codigo_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Cleanfilter");
            Ddo_contagemresultadoexecucao_codigo_Rangefilterfrom = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Rangefilterfrom");
            Ddo_contagemresultadoexecucao_codigo_Rangefilterto = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Rangefilterto");
            Ddo_contagemresultadoexecucao_codigo_Noresultsfound = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Noresultsfound");
            Ddo_contagemresultadoexecucao_codigo_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Searchbuttontext");
            Ddo_contagemresultadoexecucao_oscod_Caption = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Caption");
            Ddo_contagemresultadoexecucao_oscod_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Tooltip");
            Ddo_contagemresultadoexecucao_oscod_Cls = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Cls");
            Ddo_contagemresultadoexecucao_oscod_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Filteredtext_set");
            Ddo_contagemresultadoexecucao_oscod_Filteredtextto_set = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Filteredtextto_set");
            Ddo_contagemresultadoexecucao_oscod_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Dropdownoptionstype");
            Ddo_contagemresultadoexecucao_oscod_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Titlecontrolidtoreplace");
            Ddo_contagemresultadoexecucao_oscod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Includesortasc"));
            Ddo_contagemresultadoexecucao_oscod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Includesortdsc"));
            Ddo_contagemresultadoexecucao_oscod_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Sortedstatus");
            Ddo_contagemresultadoexecucao_oscod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Includefilter"));
            Ddo_contagemresultadoexecucao_oscod_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Filtertype");
            Ddo_contagemresultadoexecucao_oscod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Filterisrange"));
            Ddo_contagemresultadoexecucao_oscod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Includedatalist"));
            Ddo_contagemresultadoexecucao_oscod_Datalistfixedvalues = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Datalistfixedvalues");
            Ddo_contagemresultadoexecucao_oscod_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemresultadoexecucao_oscod_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Sortasc");
            Ddo_contagemresultadoexecucao_oscod_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Sortdsc");
            Ddo_contagemresultadoexecucao_oscod_Loadingdata = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Loadingdata");
            Ddo_contagemresultadoexecucao_oscod_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Cleanfilter");
            Ddo_contagemresultadoexecucao_oscod_Rangefilterfrom = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Rangefilterfrom");
            Ddo_contagemresultadoexecucao_oscod_Rangefilterto = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Rangefilterto");
            Ddo_contagemresultadoexecucao_oscod_Noresultsfound = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Noresultsfound");
            Ddo_contagemresultadoexecucao_oscod_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Searchbuttontext");
            Ddo_contagemresultadoexecucao_inicio_Caption = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Caption");
            Ddo_contagemresultadoexecucao_inicio_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Tooltip");
            Ddo_contagemresultadoexecucao_inicio_Cls = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Cls");
            Ddo_contagemresultadoexecucao_inicio_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Filteredtext_set");
            Ddo_contagemresultadoexecucao_inicio_Filteredtextto_set = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Filteredtextto_set");
            Ddo_contagemresultadoexecucao_inicio_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Dropdownoptionstype");
            Ddo_contagemresultadoexecucao_inicio_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Titlecontrolidtoreplace");
            Ddo_contagemresultadoexecucao_inicio_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Includesortasc"));
            Ddo_contagemresultadoexecucao_inicio_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Includesortdsc"));
            Ddo_contagemresultadoexecucao_inicio_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Sortedstatus");
            Ddo_contagemresultadoexecucao_inicio_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Includefilter"));
            Ddo_contagemresultadoexecucao_inicio_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Filtertype");
            Ddo_contagemresultadoexecucao_inicio_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Filterisrange"));
            Ddo_contagemresultadoexecucao_inicio_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Includedatalist"));
            Ddo_contagemresultadoexecucao_inicio_Datalistfixedvalues = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Datalistfixedvalues");
            Ddo_contagemresultadoexecucao_inicio_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemresultadoexecucao_inicio_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Sortasc");
            Ddo_contagemresultadoexecucao_inicio_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Sortdsc");
            Ddo_contagemresultadoexecucao_inicio_Loadingdata = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Loadingdata");
            Ddo_contagemresultadoexecucao_inicio_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Cleanfilter");
            Ddo_contagemresultadoexecucao_inicio_Rangefilterfrom = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Rangefilterfrom");
            Ddo_contagemresultadoexecucao_inicio_Rangefilterto = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Rangefilterto");
            Ddo_contagemresultadoexecucao_inicio_Noresultsfound = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Noresultsfound");
            Ddo_contagemresultadoexecucao_inicio_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Searchbuttontext");
            Ddo_contagemresultadoexecucao_prazodias_Caption = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Caption");
            Ddo_contagemresultadoexecucao_prazodias_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Tooltip");
            Ddo_contagemresultadoexecucao_prazodias_Cls = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Cls");
            Ddo_contagemresultadoexecucao_prazodias_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Filteredtext_set");
            Ddo_contagemresultadoexecucao_prazodias_Filteredtextto_set = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Filteredtextto_set");
            Ddo_contagemresultadoexecucao_prazodias_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Dropdownoptionstype");
            Ddo_contagemresultadoexecucao_prazodias_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Titlecontrolidtoreplace");
            Ddo_contagemresultadoexecucao_prazodias_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Includesortasc"));
            Ddo_contagemresultadoexecucao_prazodias_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Includesortdsc"));
            Ddo_contagemresultadoexecucao_prazodias_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Sortedstatus");
            Ddo_contagemresultadoexecucao_prazodias_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Includefilter"));
            Ddo_contagemresultadoexecucao_prazodias_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Filtertype");
            Ddo_contagemresultadoexecucao_prazodias_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Filterisrange"));
            Ddo_contagemresultadoexecucao_prazodias_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Includedatalist"));
            Ddo_contagemresultadoexecucao_prazodias_Datalistfixedvalues = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Datalistfixedvalues");
            Ddo_contagemresultadoexecucao_prazodias_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemresultadoexecucao_prazodias_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Sortasc");
            Ddo_contagemresultadoexecucao_prazodias_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Sortdsc");
            Ddo_contagemresultadoexecucao_prazodias_Loadingdata = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Loadingdata");
            Ddo_contagemresultadoexecucao_prazodias_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Cleanfilter");
            Ddo_contagemresultadoexecucao_prazodias_Rangefilterfrom = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Rangefilterfrom");
            Ddo_contagemresultadoexecucao_prazodias_Rangefilterto = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Rangefilterto");
            Ddo_contagemresultadoexecucao_prazodias_Noresultsfound = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Noresultsfound");
            Ddo_contagemresultadoexecucao_prazodias_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Searchbuttontext");
            Ddo_contagemresultadoexecucao_prevista_Caption = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Caption");
            Ddo_contagemresultadoexecucao_prevista_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Tooltip");
            Ddo_contagemresultadoexecucao_prevista_Cls = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Cls");
            Ddo_contagemresultadoexecucao_prevista_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Filteredtext_set");
            Ddo_contagemresultadoexecucao_prevista_Filteredtextto_set = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Filteredtextto_set");
            Ddo_contagemresultadoexecucao_prevista_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Dropdownoptionstype");
            Ddo_contagemresultadoexecucao_prevista_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Titlecontrolidtoreplace");
            Ddo_contagemresultadoexecucao_prevista_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Includesortasc"));
            Ddo_contagemresultadoexecucao_prevista_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Includesortdsc"));
            Ddo_contagemresultadoexecucao_prevista_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Sortedstatus");
            Ddo_contagemresultadoexecucao_prevista_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Includefilter"));
            Ddo_contagemresultadoexecucao_prevista_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Filtertype");
            Ddo_contagemresultadoexecucao_prevista_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Filterisrange"));
            Ddo_contagemresultadoexecucao_prevista_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Includedatalist"));
            Ddo_contagemresultadoexecucao_prevista_Datalistfixedvalues = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Datalistfixedvalues");
            Ddo_contagemresultadoexecucao_prevista_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemresultadoexecucao_prevista_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Sortasc");
            Ddo_contagemresultadoexecucao_prevista_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Sortdsc");
            Ddo_contagemresultadoexecucao_prevista_Loadingdata = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Loadingdata");
            Ddo_contagemresultadoexecucao_prevista_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Cleanfilter");
            Ddo_contagemresultadoexecucao_prevista_Rangefilterfrom = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Rangefilterfrom");
            Ddo_contagemresultadoexecucao_prevista_Rangefilterto = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Rangefilterto");
            Ddo_contagemresultadoexecucao_prevista_Noresultsfound = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Noresultsfound");
            Ddo_contagemresultadoexecucao_prevista_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Searchbuttontext");
            Ddo_contagemresultadoexecucao_fim_Caption = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Caption");
            Ddo_contagemresultadoexecucao_fim_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Tooltip");
            Ddo_contagemresultadoexecucao_fim_Cls = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Cls");
            Ddo_contagemresultadoexecucao_fim_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Filteredtext_set");
            Ddo_contagemresultadoexecucao_fim_Filteredtextto_set = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Filteredtextto_set");
            Ddo_contagemresultadoexecucao_fim_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Dropdownoptionstype");
            Ddo_contagemresultadoexecucao_fim_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Titlecontrolidtoreplace");
            Ddo_contagemresultadoexecucao_fim_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Includesortasc"));
            Ddo_contagemresultadoexecucao_fim_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Includesortdsc"));
            Ddo_contagemresultadoexecucao_fim_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Sortedstatus");
            Ddo_contagemresultadoexecucao_fim_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Includefilter"));
            Ddo_contagemresultadoexecucao_fim_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Filtertype");
            Ddo_contagemresultadoexecucao_fim_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Filterisrange"));
            Ddo_contagemresultadoexecucao_fim_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Includedatalist"));
            Ddo_contagemresultadoexecucao_fim_Datalistfixedvalues = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Datalistfixedvalues");
            Ddo_contagemresultadoexecucao_fim_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemresultadoexecucao_fim_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Sortasc");
            Ddo_contagemresultadoexecucao_fim_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Sortdsc");
            Ddo_contagemresultadoexecucao_fim_Loadingdata = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Loadingdata");
            Ddo_contagemresultadoexecucao_fim_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Cleanfilter");
            Ddo_contagemresultadoexecucao_fim_Rangefilterfrom = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Rangefilterfrom");
            Ddo_contagemresultadoexecucao_fim_Rangefilterto = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Rangefilterto");
            Ddo_contagemresultadoexecucao_fim_Noresultsfound = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Noresultsfound");
            Ddo_contagemresultadoexecucao_fim_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Searchbuttontext");
            Ddo_contagemresultadoexecucao_dias_Caption = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Caption");
            Ddo_contagemresultadoexecucao_dias_Tooltip = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Tooltip");
            Ddo_contagemresultadoexecucao_dias_Cls = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Cls");
            Ddo_contagemresultadoexecucao_dias_Filteredtext_set = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Filteredtext_set");
            Ddo_contagemresultadoexecucao_dias_Filteredtextto_set = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Filteredtextto_set");
            Ddo_contagemresultadoexecucao_dias_Dropdownoptionstype = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Dropdownoptionstype");
            Ddo_contagemresultadoexecucao_dias_Titlecontrolidtoreplace = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Titlecontrolidtoreplace");
            Ddo_contagemresultadoexecucao_dias_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Includesortasc"));
            Ddo_contagemresultadoexecucao_dias_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Includesortdsc"));
            Ddo_contagemresultadoexecucao_dias_Sortedstatus = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Sortedstatus");
            Ddo_contagemresultadoexecucao_dias_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Includefilter"));
            Ddo_contagemresultadoexecucao_dias_Filtertype = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Filtertype");
            Ddo_contagemresultadoexecucao_dias_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Filterisrange"));
            Ddo_contagemresultadoexecucao_dias_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Includedatalist"));
            Ddo_contagemresultadoexecucao_dias_Datalistfixedvalues = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Datalistfixedvalues");
            Ddo_contagemresultadoexecucao_dias_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contagemresultadoexecucao_dias_Sortasc = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Sortasc");
            Ddo_contagemresultadoexecucao_dias_Sortdsc = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Sortdsc");
            Ddo_contagemresultadoexecucao_dias_Loadingdata = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Loadingdata");
            Ddo_contagemresultadoexecucao_dias_Cleanfilter = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Cleanfilter");
            Ddo_contagemresultadoexecucao_dias_Rangefilterfrom = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Rangefilterfrom");
            Ddo_contagemresultadoexecucao_dias_Rangefilterto = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Rangefilterto");
            Ddo_contagemresultadoexecucao_dias_Noresultsfound = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Noresultsfound");
            Ddo_contagemresultadoexecucao_dias_Searchbuttontext = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contagemresultadoexecucao_codigo_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Activeeventkey");
            Ddo_contagemresultadoexecucao_codigo_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Filteredtext_get");
            Ddo_contagemresultadoexecucao_codigo_Filteredtextto_get = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO_Filteredtextto_get");
            Ddo_contagemresultadoexecucao_oscod_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Activeeventkey");
            Ddo_contagemresultadoexecucao_oscod_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Filteredtext_get");
            Ddo_contagemresultadoexecucao_oscod_Filteredtextto_get = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD_Filteredtextto_get");
            Ddo_contagemresultadoexecucao_inicio_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Activeeventkey");
            Ddo_contagemresultadoexecucao_inicio_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Filteredtext_get");
            Ddo_contagemresultadoexecucao_inicio_Filteredtextto_get = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO_Filteredtextto_get");
            Ddo_contagemresultadoexecucao_prazodias_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Activeeventkey");
            Ddo_contagemresultadoexecucao_prazodias_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Filteredtext_get");
            Ddo_contagemresultadoexecucao_prazodias_Filteredtextto_get = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_Filteredtextto_get");
            Ddo_contagemresultadoexecucao_prevista_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Activeeventkey");
            Ddo_contagemresultadoexecucao_prevista_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Filteredtext_get");
            Ddo_contagemresultadoexecucao_prevista_Filteredtextto_get = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA_Filteredtextto_get");
            Ddo_contagemresultadoexecucao_fim_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Activeeventkey");
            Ddo_contagemresultadoexecucao_fim_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Filteredtext_get");
            Ddo_contagemresultadoexecucao_fim_Filteredtextto_get = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_FIM_Filteredtextto_get");
            Ddo_contagemresultadoexecucao_dias_Activeeventkey = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Activeeventkey");
            Ddo_contagemresultadoexecucao_dias_Filteredtext_get = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Filteredtext_get");
            Ddo_contagemresultadoexecucao_dias_Filteredtextto_get = cgiGet( "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO1"), 0) != AV16ContagemResultadoExecucao_Inicio1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1"), 0) != AV17ContagemResultadoExecucao_Inicio_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO2"), 0) != AV20ContagemResultadoExecucao_Inicio2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2"), 0) != AV21ContagemResultadoExecucao_Inicio_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO3"), 0) != AV24ContagemResultadoExecucao_Inicio3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3"), 0) != AV25ContagemResultadoExecucao_Inicio_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_CODIGO"), ",", ".") != Convert.ToDecimal( AV31TFContagemResultadoExecucao_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV32TFContagemResultadoExecucao_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_OSCOD"), ",", ".") != Convert.ToDecimal( AV35TFContagemResultadoExecucao_OSCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_OSCOD_TO"), ",", ".") != Convert.ToDecimal( AV36TFContagemResultadoExecucao_OSCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_INICIO"), 0) != AV39TFContagemResultadoExecucao_Inicio )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO"), 0) != AV40TFContagemResultadoExecucao_Inicio_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS"), ",", ".") != Convert.ToDecimal( AV45TFContagemResultadoExecucao_PrazoDias )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO"), ",", ".") != Convert.ToDecimal( AV46TFContagemResultadoExecucao_PrazoDias_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA"), 0) != AV49TFContagemResultadoExecucao_Prevista )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO"), 0) != AV50TFContagemResultadoExecucao_Prevista_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_FIM"), 0) != AV55TFContagemResultadoExecucao_Fim )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO"), 0) != AV56TFContagemResultadoExecucao_Fim_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_DIAS"), ",", ".") != Convert.ToDecimal( AV61TFContagemResultadoExecucao_Dias )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO"), ",", ".") != Convert.ToDecimal( AV62TFContagemResultadoExecucao_Dias_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E26KJ2 */
         E26KJ2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E26KJ2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "CONTAGEMRESULTADOEXECUCAO_INICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "CONTAGEMRESULTADOEXECUCAO_INICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "CONTAGEMRESULTADOEXECUCAO_INICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfcontagemresultadoexecucao_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoexecucao_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoexecucao_codigo_Visible), 5, 0)));
         edtavTfcontagemresultadoexecucao_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoexecucao_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoexecucao_codigo_to_Visible), 5, 0)));
         edtavTfcontagemresultadoexecucao_oscod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoexecucao_oscod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoexecucao_oscod_Visible), 5, 0)));
         edtavTfcontagemresultadoexecucao_oscod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoexecucao_oscod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoexecucao_oscod_to_Visible), 5, 0)));
         edtavTfcontagemresultadoexecucao_inicio_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoexecucao_inicio_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoexecucao_inicio_Visible), 5, 0)));
         edtavTfcontagemresultadoexecucao_inicio_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoexecucao_inicio_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoexecucao_inicio_to_Visible), 5, 0)));
         edtavTfcontagemresultadoexecucao_prazodias_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoexecucao_prazodias_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoexecucao_prazodias_Visible), 5, 0)));
         edtavTfcontagemresultadoexecucao_prazodias_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoexecucao_prazodias_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoexecucao_prazodias_to_Visible), 5, 0)));
         edtavTfcontagemresultadoexecucao_prevista_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoexecucao_prevista_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoexecucao_prevista_Visible), 5, 0)));
         edtavTfcontagemresultadoexecucao_prevista_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoexecucao_prevista_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoexecucao_prevista_to_Visible), 5, 0)));
         edtavTfcontagemresultadoexecucao_fim_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoexecucao_fim_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoexecucao_fim_Visible), 5, 0)));
         edtavTfcontagemresultadoexecucao_fim_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoexecucao_fim_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoexecucao_fim_to_Visible), 5, 0)));
         edtavTfcontagemresultadoexecucao_dias_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoexecucao_dias_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoexecucao_dias_Visible), 5, 0)));
         edtavTfcontagemresultadoexecucao_dias_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontagemresultadoexecucao_dias_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontagemresultadoexecucao_dias_to_Visible), 5, 0)));
         Ddo_contagemresultadoexecucao_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultadoExecucao_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_codigo_Internalname, "TitleControlIdToReplace", Ddo_contagemresultadoexecucao_codigo_Titlecontrolidtoreplace);
         AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace = Ddo_contagemresultadoexecucao_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace", AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace);
         edtavDdo_contagemresultadoexecucao_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultadoexecucao_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultadoexecucao_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultadoexecucao_oscod_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultadoExecucao_OSCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_oscod_Internalname, "TitleControlIdToReplace", Ddo_contagemresultadoexecucao_oscod_Titlecontrolidtoreplace);
         AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace = Ddo_contagemresultadoexecucao_oscod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace", AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace);
         edtavDdo_contagemresultadoexecucao_oscodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultadoexecucao_oscodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultadoexecucao_oscodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultadoexecucao_inicio_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultadoExecucao_Inicio";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_inicio_Internalname, "TitleControlIdToReplace", Ddo_contagemresultadoexecucao_inicio_Titlecontrolidtoreplace);
         AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace = Ddo_contagemresultadoexecucao_inicio_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace", AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace);
         edtavDdo_contagemresultadoexecucao_iniciotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultadoexecucao_iniciotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultadoexecucao_iniciotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultadoexecucao_prazodias_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultadoExecucao_PrazoDias";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_prazodias_Internalname, "TitleControlIdToReplace", Ddo_contagemresultadoexecucao_prazodias_Titlecontrolidtoreplace);
         AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace = Ddo_contagemresultadoexecucao_prazodias_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace", AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace);
         edtavDdo_contagemresultadoexecucao_prazodiastitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultadoexecucao_prazodiastitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultadoexecucao_prazodiastitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultadoexecucao_prevista_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultadoExecucao_Prevista";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_prevista_Internalname, "TitleControlIdToReplace", Ddo_contagemresultadoexecucao_prevista_Titlecontrolidtoreplace);
         AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace = Ddo_contagemresultadoexecucao_prevista_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace", AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace);
         edtavDdo_contagemresultadoexecucao_previstatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultadoexecucao_previstatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultadoexecucao_previstatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultadoexecucao_fim_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultadoExecucao_Fim";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_fim_Internalname, "TitleControlIdToReplace", Ddo_contagemresultadoexecucao_fim_Titlecontrolidtoreplace);
         AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace = Ddo_contagemresultadoexecucao_fim_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace", AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace);
         edtavDdo_contagemresultadoexecucao_fimtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultadoexecucao_fimtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultadoexecucao_fimtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contagemresultadoexecucao_dias_Titlecontrolidtoreplace = subGrid_Internalname+"_ContagemResultadoExecucao_Dias";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_dias_Internalname, "TitleControlIdToReplace", Ddo_contagemresultadoexecucao_dias_Titlecontrolidtoreplace);
         AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace = Ddo_contagemresultadoexecucao_dias_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace", AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace);
         edtavDdo_contagemresultadoexecucao_diastitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contagemresultadoexecucao_diastitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contagemresultadoexecucao_diastitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Contagem Resultado Execucao";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Inicio", 0);
         cmbavOrderedby.addItem("2", "Codigo", 0);
         cmbavOrderedby.addItem("3", "Demada", 0);
         cmbavOrderedby.addItem("4", "Dias", 0);
         cmbavOrderedby.addItem("5", "Previsto", 0);
         cmbavOrderedby.addItem("6", "Fim", 0);
         cmbavOrderedby.addItem("7", "Usado", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV64DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV64DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E27KJ2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV30ContagemResultadoExecucao_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34ContagemResultadoExecucao_OSCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38ContagemResultadoExecucao_InicioTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44ContagemResultadoExecucao_PrazoDiasTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV48ContagemResultadoExecucao_PrevistaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54ContagemResultadoExecucao_FimTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV60ContagemResultadoExecucao_DiasTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         edtContagemResultadoExecucao_Codigo_Titleformat = 2;
         edtContagemResultadoExecucao_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Codigo", AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoExecucao_Codigo_Internalname, "Title", edtContagemResultadoExecucao_Codigo_Title);
         edtContagemResultadoExecucao_OSCod_Titleformat = 2;
         edtContagemResultadoExecucao_OSCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Demada", AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoExecucao_OSCod_Internalname, "Title", edtContagemResultadoExecucao_OSCod_Title);
         edtContagemResultadoExecucao_Inicio_Titleformat = 2;
         edtContagemResultadoExecucao_Inicio_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Inicio", AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoExecucao_Inicio_Internalname, "Title", edtContagemResultadoExecucao_Inicio_Title);
         edtContagemResultadoExecucao_PrazoDias_Titleformat = 2;
         edtContagemResultadoExecucao_PrazoDias_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Dias", AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoExecucao_PrazoDias_Internalname, "Title", edtContagemResultadoExecucao_PrazoDias_Title);
         edtContagemResultadoExecucao_Prevista_Titleformat = 2;
         edtContagemResultadoExecucao_Prevista_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Previsto", AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoExecucao_Prevista_Internalname, "Title", edtContagemResultadoExecucao_Prevista_Title);
         edtContagemResultadoExecucao_Fim_Titleformat = 2;
         edtContagemResultadoExecucao_Fim_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Fim", AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoExecucao_Fim_Internalname, "Title", edtContagemResultadoExecucao_Fim_Title);
         edtContagemResultadoExecucao_Dias_Titleformat = 2;
         edtContagemResultadoExecucao_Dias_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Usado", AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoExecucao_Dias_Internalname, "Title", edtContagemResultadoExecucao_Dias_Title);
         AV66GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV66GridCurrentPage), 10, 0)));
         AV67GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV30ContagemResultadoExecucao_CodigoTitleFilterData", AV30ContagemResultadoExecucao_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34ContagemResultadoExecucao_OSCodTitleFilterData", AV34ContagemResultadoExecucao_OSCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV38ContagemResultadoExecucao_InicioTitleFilterData", AV38ContagemResultadoExecucao_InicioTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV44ContagemResultadoExecucao_PrazoDiasTitleFilterData", AV44ContagemResultadoExecucao_PrazoDiasTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV48ContagemResultadoExecucao_PrevistaTitleFilterData", AV48ContagemResultadoExecucao_PrevistaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV54ContagemResultadoExecucao_FimTitleFilterData", AV54ContagemResultadoExecucao_FimTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV60ContagemResultadoExecucao_DiasTitleFilterData", AV60ContagemResultadoExecucao_DiasTitleFilterData);
      }

      protected void E11KJ2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV65PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV65PageToGo) ;
         }
      }

      protected void E12KJ2( )
      {
         /* Ddo_contagemresultadoexecucao_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultadoexecucao_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadoexecucao_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_codigo_Internalname, "SortedStatus", Ddo_contagemresultadoexecucao_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadoexecucao_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadoexecucao_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_codigo_Internalname, "SortedStatus", Ddo_contagemresultadoexecucao_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadoexecucao_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV31TFContagemResultadoExecucao_Codigo = (int)(NumberUtil.Val( Ddo_contagemresultadoexecucao_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFContagemResultadoExecucao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContagemResultadoExecucao_Codigo), 6, 0)));
            AV32TFContagemResultadoExecucao_Codigo_To = (int)(NumberUtil.Val( Ddo_contagemresultadoexecucao_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContagemResultadoExecucao_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContagemResultadoExecucao_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13KJ2( )
      {
         /* Ddo_contagemresultadoexecucao_oscod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultadoexecucao_oscod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadoexecucao_oscod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_oscod_Internalname, "SortedStatus", Ddo_contagemresultadoexecucao_oscod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadoexecucao_oscod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadoexecucao_oscod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_oscod_Internalname, "SortedStatus", Ddo_contagemresultadoexecucao_oscod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadoexecucao_oscod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV35TFContagemResultadoExecucao_OSCod = (int)(NumberUtil.Val( Ddo_contagemresultadoexecucao_oscod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContagemResultadoExecucao_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFContagemResultadoExecucao_OSCod), 6, 0)));
            AV36TFContagemResultadoExecucao_OSCod_To = (int)(NumberUtil.Val( Ddo_contagemresultadoexecucao_oscod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContagemResultadoExecucao_OSCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContagemResultadoExecucao_OSCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14KJ2( )
      {
         /* Ddo_contagemresultadoexecucao_inicio_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultadoexecucao_inicio_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadoexecucao_inicio_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_inicio_Internalname, "SortedStatus", Ddo_contagemresultadoexecucao_inicio_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadoexecucao_inicio_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadoexecucao_inicio_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_inicio_Internalname, "SortedStatus", Ddo_contagemresultadoexecucao_inicio_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadoexecucao_inicio_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV39TFContagemResultadoExecucao_Inicio = context.localUtil.CToT( Ddo_contagemresultadoexecucao_inicio_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContagemResultadoExecucao_Inicio", context.localUtil.TToC( AV39TFContagemResultadoExecucao_Inicio, 8, 5, 0, 3, "/", ":", " "));
            AV40TFContagemResultadoExecucao_Inicio_To = context.localUtil.CToT( Ddo_contagemresultadoexecucao_inicio_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContagemResultadoExecucao_Inicio_To", context.localUtil.TToC( AV40TFContagemResultadoExecucao_Inicio_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV40TFContagemResultadoExecucao_Inicio_To) )
            {
               AV40TFContagemResultadoExecucao_Inicio_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV40TFContagemResultadoExecucao_Inicio_To)), (short)(DateTimeUtil.Month( AV40TFContagemResultadoExecucao_Inicio_To)), (short)(DateTimeUtil.Day( AV40TFContagemResultadoExecucao_Inicio_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContagemResultadoExecucao_Inicio_To", context.localUtil.TToC( AV40TFContagemResultadoExecucao_Inicio_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15KJ2( )
      {
         /* Ddo_contagemresultadoexecucao_prazodias_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultadoexecucao_prazodias_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadoexecucao_prazodias_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_prazodias_Internalname, "SortedStatus", Ddo_contagemresultadoexecucao_prazodias_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadoexecucao_prazodias_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadoexecucao_prazodias_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_prazodias_Internalname, "SortedStatus", Ddo_contagemresultadoexecucao_prazodias_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadoexecucao_prazodias_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV45TFContagemResultadoExecucao_PrazoDias = (short)(NumberUtil.Val( Ddo_contagemresultadoexecucao_prazodias_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContagemResultadoExecucao_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFContagemResultadoExecucao_PrazoDias), 4, 0)));
            AV46TFContagemResultadoExecucao_PrazoDias_To = (short)(NumberUtil.Val( Ddo_contagemresultadoexecucao_prazodias_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContagemResultadoExecucao_PrazoDias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFContagemResultadoExecucao_PrazoDias_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16KJ2( )
      {
         /* Ddo_contagemresultadoexecucao_prevista_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultadoexecucao_prevista_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadoexecucao_prevista_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_prevista_Internalname, "SortedStatus", Ddo_contagemresultadoexecucao_prevista_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadoexecucao_prevista_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadoexecucao_prevista_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_prevista_Internalname, "SortedStatus", Ddo_contagemresultadoexecucao_prevista_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadoexecucao_prevista_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV49TFContagemResultadoExecucao_Prevista = context.localUtil.CToT( Ddo_contagemresultadoexecucao_prevista_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContagemResultadoExecucao_Prevista", context.localUtil.TToC( AV49TFContagemResultadoExecucao_Prevista, 8, 5, 0, 3, "/", ":", " "));
            AV50TFContagemResultadoExecucao_Prevista_To = context.localUtil.CToT( Ddo_contagemresultadoexecucao_prevista_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContagemResultadoExecucao_Prevista_To", context.localUtil.TToC( AV50TFContagemResultadoExecucao_Prevista_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV50TFContagemResultadoExecucao_Prevista_To) )
            {
               AV50TFContagemResultadoExecucao_Prevista_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV50TFContagemResultadoExecucao_Prevista_To)), (short)(DateTimeUtil.Month( AV50TFContagemResultadoExecucao_Prevista_To)), (short)(DateTimeUtil.Day( AV50TFContagemResultadoExecucao_Prevista_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContagemResultadoExecucao_Prevista_To", context.localUtil.TToC( AV50TFContagemResultadoExecucao_Prevista_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E17KJ2( )
      {
         /* Ddo_contagemresultadoexecucao_fim_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultadoexecucao_fim_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadoexecucao_fim_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_fim_Internalname, "SortedStatus", Ddo_contagemresultadoexecucao_fim_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadoexecucao_fim_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadoexecucao_fim_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_fim_Internalname, "SortedStatus", Ddo_contagemresultadoexecucao_fim_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadoexecucao_fim_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV55TFContagemResultadoExecucao_Fim = context.localUtil.CToT( Ddo_contagemresultadoexecucao_fim_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContagemResultadoExecucao_Fim", context.localUtil.TToC( AV55TFContagemResultadoExecucao_Fim, 8, 5, 0, 3, "/", ":", " "));
            AV56TFContagemResultadoExecucao_Fim_To = context.localUtil.CToT( Ddo_contagemresultadoexecucao_fim_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContagemResultadoExecucao_Fim_To", context.localUtil.TToC( AV56TFContagemResultadoExecucao_Fim_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV56TFContagemResultadoExecucao_Fim_To) )
            {
               AV56TFContagemResultadoExecucao_Fim_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV56TFContagemResultadoExecucao_Fim_To)), (short)(DateTimeUtil.Month( AV56TFContagemResultadoExecucao_Fim_To)), (short)(DateTimeUtil.Day( AV56TFContagemResultadoExecucao_Fim_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContagemResultadoExecucao_Fim_To", context.localUtil.TToC( AV56TFContagemResultadoExecucao_Fim_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E18KJ2( )
      {
         /* Ddo_contagemresultadoexecucao_dias_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contagemresultadoexecucao_dias_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadoexecucao_dias_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_dias_Internalname, "SortedStatus", Ddo_contagemresultadoexecucao_dias_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadoexecucao_dias_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contagemresultadoexecucao_dias_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_dias_Internalname, "SortedStatus", Ddo_contagemresultadoexecucao_dias_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contagemresultadoexecucao_dias_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV61TFContagemResultadoExecucao_Dias = (short)(NumberUtil.Val( Ddo_contagemresultadoexecucao_dias_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContagemResultadoExecucao_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFContagemResultadoExecucao_Dias), 4, 0)));
            AV62TFContagemResultadoExecucao_Dias_To = (short)(NumberUtil.Val( Ddo_contagemresultadoexecucao_dias_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContagemResultadoExecucao_Dias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFContagemResultadoExecucao_Dias_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E28KJ2( )
      {
         /* Grid_Load Routine */
         AV28Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV28Select);
         AV70Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 86;
         }
         sendrow_862( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_86_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(86, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E29KJ2 */
         E29KJ2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E29KJ2( )
      {
         /* Enter Routine */
         AV7InOutContagemResultadoExecucao_Codigo = A1405ContagemResultadoExecucao_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContagemResultadoExecucao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContagemResultadoExecucao_Codigo), 6, 0)));
         AV8InOutContagemResultadoExecucao_Inicio = A1406ContagemResultadoExecucao_Inicio;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContagemResultadoExecucao_Inicio", context.localUtil.TToC( AV8InOutContagemResultadoExecucao_Inicio, 8, 5, 0, 3, "/", ":", " "));
         context.setWebReturnParms(new Object[] {(int)AV7InOutContagemResultadoExecucao_Codigo,context.localUtil.Format( AV8InOutContagemResultadoExecucao_Inicio, "99/99/99 99:99")});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E19KJ2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E24KJ2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E20KJ2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContagemResultadoExecucao_Inicio1, AV17ContagemResultadoExecucao_Inicio_To1, AV19DynamicFiltersSelector2, AV20ContagemResultadoExecucao_Inicio2, AV21ContagemResultadoExecucao_Inicio_To2, AV23DynamicFiltersSelector3, AV24ContagemResultadoExecucao_Inicio3, AV25ContagemResultadoExecucao_Inicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContagemResultadoExecucao_Codigo, AV32TFContagemResultadoExecucao_Codigo_To, AV35TFContagemResultadoExecucao_OSCod, AV36TFContagemResultadoExecucao_OSCod_To, AV39TFContagemResultadoExecucao_Inicio, AV40TFContagemResultadoExecucao_Inicio_To, AV45TFContagemResultadoExecucao_PrazoDias, AV46TFContagemResultadoExecucao_PrazoDias_To, AV49TFContagemResultadoExecucao_Prevista, AV50TFContagemResultadoExecucao_Prevista_To, AV55TFContagemResultadoExecucao_Fim, AV56TFContagemResultadoExecucao_Fim_To, AV61TFContagemResultadoExecucao_Dias, AV62TFContagemResultadoExecucao_Dias_To, AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace, AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace, AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace, AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace, AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace, AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace, AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E25KJ2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E21KJ2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContagemResultadoExecucao_Inicio1, AV17ContagemResultadoExecucao_Inicio_To1, AV19DynamicFiltersSelector2, AV20ContagemResultadoExecucao_Inicio2, AV21ContagemResultadoExecucao_Inicio_To2, AV23DynamicFiltersSelector3, AV24ContagemResultadoExecucao_Inicio3, AV25ContagemResultadoExecucao_Inicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContagemResultadoExecucao_Codigo, AV32TFContagemResultadoExecucao_Codigo_To, AV35TFContagemResultadoExecucao_OSCod, AV36TFContagemResultadoExecucao_OSCod_To, AV39TFContagemResultadoExecucao_Inicio, AV40TFContagemResultadoExecucao_Inicio_To, AV45TFContagemResultadoExecucao_PrazoDias, AV46TFContagemResultadoExecucao_PrazoDias_To, AV49TFContagemResultadoExecucao_Prevista, AV50TFContagemResultadoExecucao_Prevista_To, AV55TFContagemResultadoExecucao_Fim, AV56TFContagemResultadoExecucao_Fim_To, AV61TFContagemResultadoExecucao_Dias, AV62TFContagemResultadoExecucao_Dias_To, AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace, AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace, AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace, AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace, AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace, AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace, AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E22KJ2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContagemResultadoExecucao_Inicio1, AV17ContagemResultadoExecucao_Inicio_To1, AV19DynamicFiltersSelector2, AV20ContagemResultadoExecucao_Inicio2, AV21ContagemResultadoExecucao_Inicio_To2, AV23DynamicFiltersSelector3, AV24ContagemResultadoExecucao_Inicio3, AV25ContagemResultadoExecucao_Inicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFContagemResultadoExecucao_Codigo, AV32TFContagemResultadoExecucao_Codigo_To, AV35TFContagemResultadoExecucao_OSCod, AV36TFContagemResultadoExecucao_OSCod_To, AV39TFContagemResultadoExecucao_Inicio, AV40TFContagemResultadoExecucao_Inicio_To, AV45TFContagemResultadoExecucao_PrazoDias, AV46TFContagemResultadoExecucao_PrazoDias_To, AV49TFContagemResultadoExecucao_Prevista, AV50TFContagemResultadoExecucao_Prevista_To, AV55TFContagemResultadoExecucao_Fim, AV56TFContagemResultadoExecucao_Fim_To, AV61TFContagemResultadoExecucao_Dias, AV62TFContagemResultadoExecucao_Dias_To, AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace, AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace, AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace, AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace, AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace, AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace, AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E23KJ2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void S162( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contagemresultadoexecucao_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_codigo_Internalname, "SortedStatus", Ddo_contagemresultadoexecucao_codigo_Sortedstatus);
         Ddo_contagemresultadoexecucao_oscod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_oscod_Internalname, "SortedStatus", Ddo_contagemresultadoexecucao_oscod_Sortedstatus);
         Ddo_contagemresultadoexecucao_inicio_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_inicio_Internalname, "SortedStatus", Ddo_contagemresultadoexecucao_inicio_Sortedstatus);
         Ddo_contagemresultadoexecucao_prazodias_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_prazodias_Internalname, "SortedStatus", Ddo_contagemresultadoexecucao_prazodias_Sortedstatus);
         Ddo_contagemresultadoexecucao_prevista_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_prevista_Internalname, "SortedStatus", Ddo_contagemresultadoexecucao_prevista_Sortedstatus);
         Ddo_contagemresultadoexecucao_fim_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_fim_Internalname, "SortedStatus", Ddo_contagemresultadoexecucao_fim_Sortedstatus);
         Ddo_contagemresultadoexecucao_dias_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_dias_Internalname, "SortedStatus", Ddo_contagemresultadoexecucao_dias_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_contagemresultadoexecucao_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_codigo_Internalname, "SortedStatus", Ddo_contagemresultadoexecucao_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contagemresultadoexecucao_oscod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_oscod_Internalname, "SortedStatus", Ddo_contagemresultadoexecucao_oscod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_contagemresultadoexecucao_inicio_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_inicio_Internalname, "SortedStatus", Ddo_contagemresultadoexecucao_inicio_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_contagemresultadoexecucao_prazodias_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_prazodias_Internalname, "SortedStatus", Ddo_contagemresultadoexecucao_prazodias_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_contagemresultadoexecucao_prevista_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_prevista_Internalname, "SortedStatus", Ddo_contagemresultadoexecucao_prevista_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_contagemresultadoexecucao_fim_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_fim_Internalname, "SortedStatus", Ddo_contagemresultadoexecucao_fim_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_contagemresultadoexecucao_dias_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_dias_Internalname, "SortedStatus", Ddo_contagemresultadoexecucao_dias_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 )
         {
            tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "CONTAGEMRESULTADOEXECUCAO_INICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20ContagemResultadoExecucao_Inicio2 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultadoExecucao_Inicio2", context.localUtil.TToC( AV20ContagemResultadoExecucao_Inicio2, 8, 5, 0, 3, "/", ":", " "));
         AV21ContagemResultadoExecucao_Inicio_To2 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultadoExecucao_Inicio_To2", context.localUtil.TToC( AV21ContagemResultadoExecucao_Inicio_To2, 8, 5, 0, 3, "/", ":", " "));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "CONTAGEMRESULTADOEXECUCAO_INICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24ContagemResultadoExecucao_Inicio3 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultadoExecucao_Inicio3", context.localUtil.TToC( AV24ContagemResultadoExecucao_Inicio3, 8, 5, 0, 3, "/", ":", " "));
         AV25ContagemResultadoExecucao_Inicio_To3 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContagemResultadoExecucao_Inicio_To3", context.localUtil.TToC( AV25ContagemResultadoExecucao_Inicio_To3, 8, 5, 0, 3, "/", ":", " "));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S192( )
      {
         /* 'CLEANFILTERS' Routine */
         AV31TFContagemResultadoExecucao_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFContagemResultadoExecucao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFContagemResultadoExecucao_Codigo), 6, 0)));
         Ddo_contagemresultadoexecucao_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_codigo_Internalname, "FilteredText_set", Ddo_contagemresultadoexecucao_codigo_Filteredtext_set);
         AV32TFContagemResultadoExecucao_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFContagemResultadoExecucao_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFContagemResultadoExecucao_Codigo_To), 6, 0)));
         Ddo_contagemresultadoexecucao_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_codigo_Internalname, "FilteredTextTo_set", Ddo_contagemresultadoexecucao_codigo_Filteredtextto_set);
         AV35TFContagemResultadoExecucao_OSCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContagemResultadoExecucao_OSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFContagemResultadoExecucao_OSCod), 6, 0)));
         Ddo_contagemresultadoexecucao_oscod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_oscod_Internalname, "FilteredText_set", Ddo_contagemresultadoexecucao_oscod_Filteredtext_set);
         AV36TFContagemResultadoExecucao_OSCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContagemResultadoExecucao_OSCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContagemResultadoExecucao_OSCod_To), 6, 0)));
         Ddo_contagemresultadoexecucao_oscod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_oscod_Internalname, "FilteredTextTo_set", Ddo_contagemresultadoexecucao_oscod_Filteredtextto_set);
         AV39TFContagemResultadoExecucao_Inicio = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFContagemResultadoExecucao_Inicio", context.localUtil.TToC( AV39TFContagemResultadoExecucao_Inicio, 8, 5, 0, 3, "/", ":", " "));
         Ddo_contagemresultadoexecucao_inicio_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_inicio_Internalname, "FilteredText_set", Ddo_contagemresultadoexecucao_inicio_Filteredtext_set);
         AV40TFContagemResultadoExecucao_Inicio_To = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContagemResultadoExecucao_Inicio_To", context.localUtil.TToC( AV40TFContagemResultadoExecucao_Inicio_To, 8, 5, 0, 3, "/", ":", " "));
         Ddo_contagemresultadoexecucao_inicio_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_inicio_Internalname, "FilteredTextTo_set", Ddo_contagemresultadoexecucao_inicio_Filteredtextto_set);
         AV45TFContagemResultadoExecucao_PrazoDias = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContagemResultadoExecucao_PrazoDias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFContagemResultadoExecucao_PrazoDias), 4, 0)));
         Ddo_contagemresultadoexecucao_prazodias_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_prazodias_Internalname, "FilteredText_set", Ddo_contagemresultadoexecucao_prazodias_Filteredtext_set);
         AV46TFContagemResultadoExecucao_PrazoDias_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContagemResultadoExecucao_PrazoDias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFContagemResultadoExecucao_PrazoDias_To), 4, 0)));
         Ddo_contagemresultadoexecucao_prazodias_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_prazodias_Internalname, "FilteredTextTo_set", Ddo_contagemresultadoexecucao_prazodias_Filteredtextto_set);
         AV49TFContagemResultadoExecucao_Prevista = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContagemResultadoExecucao_Prevista", context.localUtil.TToC( AV49TFContagemResultadoExecucao_Prevista, 8, 5, 0, 3, "/", ":", " "));
         Ddo_contagemresultadoexecucao_prevista_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_prevista_Internalname, "FilteredText_set", Ddo_contagemresultadoexecucao_prevista_Filteredtext_set);
         AV50TFContagemResultadoExecucao_Prevista_To = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContagemResultadoExecucao_Prevista_To", context.localUtil.TToC( AV50TFContagemResultadoExecucao_Prevista_To, 8, 5, 0, 3, "/", ":", " "));
         Ddo_contagemresultadoexecucao_prevista_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_prevista_Internalname, "FilteredTextTo_set", Ddo_contagemresultadoexecucao_prevista_Filteredtextto_set);
         AV55TFContagemResultadoExecucao_Fim = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFContagemResultadoExecucao_Fim", context.localUtil.TToC( AV55TFContagemResultadoExecucao_Fim, 8, 5, 0, 3, "/", ":", " "));
         Ddo_contagemresultadoexecucao_fim_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_fim_Internalname, "FilteredText_set", Ddo_contagemresultadoexecucao_fim_Filteredtext_set);
         AV56TFContagemResultadoExecucao_Fim_To = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContagemResultadoExecucao_Fim_To", context.localUtil.TToC( AV56TFContagemResultadoExecucao_Fim_To, 8, 5, 0, 3, "/", ":", " "));
         Ddo_contagemresultadoexecucao_fim_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_fim_Internalname, "FilteredTextTo_set", Ddo_contagemresultadoexecucao_fim_Filteredtextto_set);
         AV61TFContagemResultadoExecucao_Dias = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContagemResultadoExecucao_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFContagemResultadoExecucao_Dias), 4, 0)));
         Ddo_contagemresultadoexecucao_dias_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_dias_Internalname, "FilteredText_set", Ddo_contagemresultadoexecucao_dias_Filteredtext_set);
         AV62TFContagemResultadoExecucao_Dias_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFContagemResultadoExecucao_Dias_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFContagemResultadoExecucao_Dias_To), 4, 0)));
         Ddo_contagemresultadoexecucao_dias_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contagemresultadoexecucao_dias_Internalname, "FilteredTextTo_set", Ddo_contagemresultadoexecucao_dias_Filteredtextto_set);
         AV15DynamicFiltersSelector1 = "CONTAGEMRESULTADOEXECUCAO_INICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16ContagemResultadoExecucao_Inicio1 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultadoExecucao_Inicio1", context.localUtil.TToC( AV16ContagemResultadoExecucao_Inicio1, 8, 5, 0, 3, "/", ":", " "));
         AV17ContagemResultadoExecucao_Inicio_To1 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultadoExecucao_Inicio_To1", context.localUtil.TToC( AV17ContagemResultadoExecucao_Inicio_To1, 8, 5, 0, 3, "/", ":", " "));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 )
            {
               AV16ContagemResultadoExecucao_Inicio1 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContagemResultadoExecucao_Inicio1", context.localUtil.TToC( AV16ContagemResultadoExecucao_Inicio1, 8, 5, 0, 3, "/", ":", " "));
               AV17ContagemResultadoExecucao_Inicio_To1 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultadoExecucao_Inicio_To1", context.localUtil.TToC( AV17ContagemResultadoExecucao_Inicio_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 )
               {
                  AV20ContagemResultadoExecucao_Inicio2 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultadoExecucao_Inicio2", context.localUtil.TToC( AV20ContagemResultadoExecucao_Inicio2, 8, 5, 0, 3, "/", ":", " "));
                  AV21ContagemResultadoExecucao_Inicio_To2 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultadoExecucao_Inicio_To2", context.localUtil.TToC( AV21ContagemResultadoExecucao_Inicio_To2, 8, 5, 0, 3, "/", ":", " "));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 )
                  {
                     AV24ContagemResultadoExecucao_Inicio3 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContagemResultadoExecucao_Inicio3", context.localUtil.TToC( AV24ContagemResultadoExecucao_Inicio3, 8, 5, 0, 3, "/", ":", " "));
                     AV25ContagemResultadoExecucao_Inicio_To3 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContagemResultadoExecucao_Inicio_To3", context.localUtil.TToC( AV25ContagemResultadoExecucao_Inicio_To3, 8, 5, 0, 3, "/", ":", " "));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ! ( (DateTime.MinValue==AV16ContagemResultadoExecucao_Inicio1) && (DateTime.MinValue==AV17ContagemResultadoExecucao_Inicio_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV16ContagemResultadoExecucao_Inicio1, 8, 5, 0, 3, "/", ":", " ");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV17ContagemResultadoExecucao_Inicio_To1, 8, 5, 0, 3, "/", ":", " ");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ! ( (DateTime.MinValue==AV20ContagemResultadoExecucao_Inicio2) && (DateTime.MinValue==AV21ContagemResultadoExecucao_Inicio_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV20ContagemResultadoExecucao_Inicio2, 8, 5, 0, 3, "/", ":", " ");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV21ContagemResultadoExecucao_Inicio_To2, 8, 5, 0, 3, "/", ":", " ");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ! ( (DateTime.MinValue==AV24ContagemResultadoExecucao_Inicio3) && (DateTime.MinValue==AV25ContagemResultadoExecucao_Inicio_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV24ContagemResultadoExecucao_Inicio3, 8, 5, 0, 3, "/", ":", " ");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV25ContagemResultadoExecucao_Inicio_To3, 8, 5, 0, 3, "/", ":", " ");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_KJ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_KJ2( true) ;
         }
         else
         {
            wb_table2_5_KJ2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_KJ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_80_KJ2( true) ;
         }
         else
         {
            wb_table3_80_KJ2( false) ;
         }
         return  ;
      }

      protected void wb_table3_80_KJ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_KJ2e( true) ;
         }
         else
         {
            wb_table1_2_KJ2e( false) ;
         }
      }

      protected void wb_table3_80_KJ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_83_KJ2( true) ;
         }
         else
         {
            wb_table4_83_KJ2( false) ;
         }
         return  ;
      }

      protected void wb_table4_83_KJ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_80_KJ2e( true) ;
         }
         else
         {
            wb_table3_80_KJ2e( false) ;
         }
      }

      protected void wb_table4_83_KJ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"86\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoExecucao_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoExecucao_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoExecucao_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoExecucao_OSCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoExecucao_OSCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoExecucao_OSCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoExecucao_Inicio_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoExecucao_Inicio_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoExecucao_Inicio_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoExecucao_PrazoDias_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoExecucao_PrazoDias_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoExecucao_PrazoDias_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoExecucao_Prevista_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoExecucao_Prevista_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoExecucao_Prevista_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoExecucao_Fim_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoExecucao_Fim_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoExecucao_Fim_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContagemResultadoExecucao_Dias_Titleformat == 0 )
               {
                  context.SendWebValue( edtContagemResultadoExecucao_Dias_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContagemResultadoExecucao_Dias_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1405ContagemResultadoExecucao_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoExecucao_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoExecucao_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1404ContagemResultadoExecucao_OSCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoExecucao_OSCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoExecucao_OSCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A1406ContagemResultadoExecucao_Inicio, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoExecucao_Inicio_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoExecucao_Inicio_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoExecucao_PrazoDias_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoExecucao_PrazoDias_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A1408ContagemResultadoExecucao_Prevista, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoExecucao_Prevista_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoExecucao_Prevista_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A1407ContagemResultadoExecucao_Fim, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoExecucao_Fim_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoExecucao_Fim_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1410ContagemResultadoExecucao_Dias), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContagemResultadoExecucao_Dias_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultadoExecucao_Dias_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 86 )
         {
            wbEnd = 0;
            nRC_GXsfl_86 = (short)(nGXsfl_86_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_83_KJ2e( true) ;
         }
         else
         {
            wb_table4_83_KJ2e( false) ;
         }
      }

      protected void wb_table2_5_KJ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptContagemResultadoExecucao.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_KJ2( true) ;
         }
         else
         {
            wb_table5_14_KJ2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_KJ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_KJ2e( true) ;
         }
         else
         {
            wb_table2_5_KJ2e( false) ;
         }
      }

      protected void wb_table5_14_KJ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_KJ2( true) ;
         }
         else
         {
            wb_table6_19_KJ2( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_KJ2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_KJ2e( true) ;
         }
         else
         {
            wb_table5_14_KJ2e( false) ;
         }
      }

      protected void wb_table6_19_KJ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e30kj1_client"+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptContagemResultadoExecucao.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_KJ2( true) ;
         }
         else
         {
            wb_table7_28_KJ2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_KJ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoExecucao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e31kj1_client"+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", "", true, "HLP_PromptContagemResultadoExecucao.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_47_KJ2( true) ;
         }
         else
         {
            wb_table8_47_KJ2( false) ;
         }
         return  ;
      }

      protected void wb_table8_47_KJ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoExecucao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e32kj1_client"+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", "", true, "HLP_PromptContagemResultadoExecucao.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_66_KJ2( true) ;
         }
         else
         {
            wb_table9_66_KJ2( false) ;
         }
         return  ;
      }

      protected void wb_table9_66_KJ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_KJ2e( true) ;
         }
         else
         {
            wb_table6_19_KJ2e( false) ;
         }
      }

      protected void wb_table9_66_KJ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Internalname, tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoexecucao_inicio3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoexecucao_inicio3_Internalname, context.localUtil.TToC( AV24ContagemResultadoExecucao_Inicio3, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV24ContagemResultadoExecucao_Inicio3, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoexecucao_inicio3_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoexecucao_inicio3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoexecucao_inicio_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoexecucao_inicio_to3_Internalname, context.localUtil.TToC( AV25ContagemResultadoExecucao_Inicio_To3, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV25ContagemResultadoExecucao_Inicio_To3, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,73);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoexecucao_inicio_to3_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoexecucao_inicio_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_66_KJ2e( true) ;
         }
         else
         {
            wb_table9_66_KJ2e( false) ;
         }
      }

      protected void wb_table8_47_KJ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Internalname, tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoexecucao_inicio2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoexecucao_inicio2_Internalname, context.localUtil.TToC( AV20ContagemResultadoExecucao_Inicio2, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV20ContagemResultadoExecucao_Inicio2, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoexecucao_inicio2_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoexecucao_inicio2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoexecucao_inicio_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoexecucao_inicio_to2_Internalname, context.localUtil.TToC( AV21ContagemResultadoExecucao_Inicio_To2, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV21ContagemResultadoExecucao_Inicio_To2, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoexecucao_inicio_to2_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoexecucao_inicio_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_47_KJ2e( true) ;
         }
         else
         {
            wb_table8_47_KJ2e( false) ;
         }
      }

      protected void wb_table7_28_KJ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Internalname, tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoexecucao_inicio1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoexecucao_inicio1_Internalname, context.localUtil.TToC( AV16ContagemResultadoExecucao_Inicio1, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV16ContagemResultadoExecucao_Inicio1, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,31);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoexecucao_inicio1_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoexecucao_inicio1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultadoexecucao_inicio_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultadoexecucao_inicio_to1_Internalname, context.localUtil.TToC( AV17ContagemResultadoExecucao_Inicio_To1, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV17ContagemResultadoExecucao_Inicio_To1, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,35);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultadoexecucao_inicio_to1_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultadoexecucao_inicio_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_KJ2e( true) ;
         }
         else
         {
            wb_table7_28_KJ2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutContagemResultadoExecucao_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutContagemResultadoExecucao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutContagemResultadoExecucao_Codigo), 6, 0)));
         AV8InOutContagemResultadoExecucao_Inicio = (DateTime)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutContagemResultadoExecucao_Inicio", context.localUtil.TToC( AV8InOutContagemResultadoExecucao_Inicio, 8, 5, 0, 3, "/", ":", " "));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAKJ2( ) ;
         WSKJ2( ) ;
         WEKJ2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042823475955");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptcontagemresultadoexecucao.js", "?202042823475955");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_862( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_86_idx;
         edtContagemResultadoExecucao_Codigo_Internalname = "CONTAGEMRESULTADOEXECUCAO_CODIGO_"+sGXsfl_86_idx;
         edtContagemResultadoExecucao_OSCod_Internalname = "CONTAGEMRESULTADOEXECUCAO_OSCOD_"+sGXsfl_86_idx;
         edtContagemResultadoExecucao_Inicio_Internalname = "CONTAGEMRESULTADOEXECUCAO_INICIO_"+sGXsfl_86_idx;
         edtContagemResultadoExecucao_PrazoDias_Internalname = "CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_"+sGXsfl_86_idx;
         edtContagemResultadoExecucao_Prevista_Internalname = "CONTAGEMRESULTADOEXECUCAO_PREVISTA_"+sGXsfl_86_idx;
         edtContagemResultadoExecucao_Fim_Internalname = "CONTAGEMRESULTADOEXECUCAO_FIM_"+sGXsfl_86_idx;
         edtContagemResultadoExecucao_Dias_Internalname = "CONTAGEMRESULTADOEXECUCAO_DIAS_"+sGXsfl_86_idx;
      }

      protected void SubsflControlProps_fel_862( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_86_fel_idx;
         edtContagemResultadoExecucao_Codigo_Internalname = "CONTAGEMRESULTADOEXECUCAO_CODIGO_"+sGXsfl_86_fel_idx;
         edtContagemResultadoExecucao_OSCod_Internalname = "CONTAGEMRESULTADOEXECUCAO_OSCOD_"+sGXsfl_86_fel_idx;
         edtContagemResultadoExecucao_Inicio_Internalname = "CONTAGEMRESULTADOEXECUCAO_INICIO_"+sGXsfl_86_fel_idx;
         edtContagemResultadoExecucao_PrazoDias_Internalname = "CONTAGEMRESULTADOEXECUCAO_PRAZODIAS_"+sGXsfl_86_fel_idx;
         edtContagemResultadoExecucao_Prevista_Internalname = "CONTAGEMRESULTADOEXECUCAO_PREVISTA_"+sGXsfl_86_fel_idx;
         edtContagemResultadoExecucao_Fim_Internalname = "CONTAGEMRESULTADOEXECUCAO_FIM_"+sGXsfl_86_fel_idx;
         edtContagemResultadoExecucao_Dias_Internalname = "CONTAGEMRESULTADOEXECUCAO_DIAS_"+sGXsfl_86_fel_idx;
      }

      protected void sendrow_862( )
      {
         SubsflControlProps_862( ) ;
         WBKJ0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_86_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_86_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_86_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 87,'',false,'',86)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV28Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV70Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV70Select_GXI : context.PathToRelativeUrl( AV28Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_86_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV28Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoExecucao_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1405ContagemResultadoExecucao_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1405ContagemResultadoExecucao_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoExecucao_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoExecucao_OSCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1404ContagemResultadoExecucao_OSCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1404ContagemResultadoExecucao_OSCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoExecucao_OSCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoExecucao_Inicio_Internalname,context.localUtil.TToC( A1406ContagemResultadoExecucao_Inicio, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A1406ContagemResultadoExecucao_Inicio, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoExecucao_Inicio_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoExecucao_PrazoDias_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoExecucao_PrazoDias_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoExecucao_Prevista_Internalname,context.localUtil.TToC( A1408ContagemResultadoExecucao_Prevista, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A1408ContagemResultadoExecucao_Prevista, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoExecucao_Prevista_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoExecucao_Fim_Internalname,context.localUtil.TToC( A1407ContagemResultadoExecucao_Fim, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A1407ContagemResultadoExecucao_Fim, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoExecucao_Fim_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoExecucao_Dias_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1410ContagemResultadoExecucao_Dias), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A1410ContagemResultadoExecucao_Dias), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoExecucao_Dias_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEXECUCAO_CODIGO"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( (decimal)(A1405ContagemResultadoExecucao_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEXECUCAO_OSCOD"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( (decimal)(A1404ContagemResultadoExecucao_OSCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEXECUCAO_INICIO"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( A1406ContagemResultadoExecucao_Inicio, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( (decimal)(A1411ContagemResultadoExecucao_PrazoDias), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEXECUCAO_PREVISTA"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( A1408ContagemResultadoExecucao_Prevista, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEXECUCAO_FIM"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( A1407ContagemResultadoExecucao_Fim, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADOEXECUCAO_DIAS"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( (decimal)(A1410ContagemResultadoExecucao_Dias), "ZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_86_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_86_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_86_idx+1));
            sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
            SubsflControlProps_862( ) ;
         }
         /* End function sendrow_862 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavContagemresultadoexecucao_inicio1_Internalname = "vCONTAGEMRESULTADOEXECUCAO_INICIO1";
         lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext1_Internalname = "DYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO_RANGEMIDDLETEXT1";
         edtavContagemresultadoexecucao_inicio_to1_Internalname = "vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1";
         tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavContagemresultadoexecucao_inicio2_Internalname = "vCONTAGEMRESULTADOEXECUCAO_INICIO2";
         lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext2_Internalname = "DYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO_RANGEMIDDLETEXT2";
         edtavContagemresultadoexecucao_inicio_to2_Internalname = "vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2";
         tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavContagemresultadoexecucao_inicio3_Internalname = "vCONTAGEMRESULTADOEXECUCAO_INICIO3";
         lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext3_Internalname = "DYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO_RANGEMIDDLETEXT3";
         edtavContagemresultadoexecucao_inicio_to3_Internalname = "vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3";
         tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtContagemResultadoExecucao_Codigo_Internalname = "CONTAGEMRESULTADOEXECUCAO_CODIGO";
         edtContagemResultadoExecucao_OSCod_Internalname = "CONTAGEMRESULTADOEXECUCAO_OSCOD";
         edtContagemResultadoExecucao_Inicio_Internalname = "CONTAGEMRESULTADOEXECUCAO_INICIO";
         edtContagemResultadoExecucao_PrazoDias_Internalname = "CONTAGEMRESULTADOEXECUCAO_PRAZODIAS";
         edtContagemResultadoExecucao_Prevista_Internalname = "CONTAGEMRESULTADOEXECUCAO_PREVISTA";
         edtContagemResultadoExecucao_Fim_Internalname = "CONTAGEMRESULTADOEXECUCAO_FIM";
         edtContagemResultadoExecucao_Dias_Internalname = "CONTAGEMRESULTADOEXECUCAO_DIAS";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfcontagemresultadoexecucao_codigo_Internalname = "vTFCONTAGEMRESULTADOEXECUCAO_CODIGO";
         edtavTfcontagemresultadoexecucao_codigo_to_Internalname = "vTFCONTAGEMRESULTADOEXECUCAO_CODIGO_TO";
         edtavTfcontagemresultadoexecucao_oscod_Internalname = "vTFCONTAGEMRESULTADOEXECUCAO_OSCOD";
         edtavTfcontagemresultadoexecucao_oscod_to_Internalname = "vTFCONTAGEMRESULTADOEXECUCAO_OSCOD_TO";
         edtavTfcontagemresultadoexecucao_inicio_Internalname = "vTFCONTAGEMRESULTADOEXECUCAO_INICIO";
         edtavTfcontagemresultadoexecucao_inicio_to_Internalname = "vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO";
         edtavDdo_contagemresultadoexecucao_inicioauxdate_Internalname = "vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOAUXDATE";
         edtavDdo_contagemresultadoexecucao_inicioauxdateto_Internalname = "vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOAUXDATETO";
         divDdo_contagemresultadoexecucao_inicioauxdates_Internalname = "DDO_CONTAGEMRESULTADOEXECUCAO_INICIOAUXDATES";
         edtavTfcontagemresultadoexecucao_prazodias_Internalname = "vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS";
         edtavTfcontagemresultadoexecucao_prazodias_to_Internalname = "vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO";
         edtavTfcontagemresultadoexecucao_prevista_Internalname = "vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA";
         edtavTfcontagemresultadoexecucao_prevista_to_Internalname = "vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO";
         edtavDdo_contagemresultadoexecucao_previstaauxdate_Internalname = "vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAAUXDATE";
         edtavDdo_contagemresultadoexecucao_previstaauxdateto_Internalname = "vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAAUXDATETO";
         divDdo_contagemresultadoexecucao_previstaauxdates_Internalname = "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTAAUXDATES";
         edtavTfcontagemresultadoexecucao_fim_Internalname = "vTFCONTAGEMRESULTADOEXECUCAO_FIM";
         edtavTfcontagemresultadoexecucao_fim_to_Internalname = "vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO";
         edtavDdo_contagemresultadoexecucao_fimauxdate_Internalname = "vDDO_CONTAGEMRESULTADOEXECUCAO_FIMAUXDATE";
         edtavDdo_contagemresultadoexecucao_fimauxdateto_Internalname = "vDDO_CONTAGEMRESULTADOEXECUCAO_FIMAUXDATETO";
         divDdo_contagemresultadoexecucao_fimauxdates_Internalname = "DDO_CONTAGEMRESULTADOEXECUCAO_FIMAUXDATES";
         edtavTfcontagemresultadoexecucao_dias_Internalname = "vTFCONTAGEMRESULTADOEXECUCAO_DIAS";
         edtavTfcontagemresultadoexecucao_dias_to_Internalname = "vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO";
         Ddo_contagemresultadoexecucao_codigo_Internalname = "DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO";
         edtavDdo_contagemresultadoexecucao_codigotitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADOEXECUCAO_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultadoexecucao_oscod_Internalname = "DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD";
         edtavDdo_contagemresultadoexecucao_oscodtitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADOEXECUCAO_OSCODTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultadoexecucao_inicio_Internalname = "DDO_CONTAGEMRESULTADOEXECUCAO_INICIO";
         edtavDdo_contagemresultadoexecucao_iniciotitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultadoexecucao_prazodias_Internalname = "DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS";
         edtavDdo_contagemresultadoexecucao_prazodiastitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultadoexecucao_prevista_Internalname = "DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA";
         edtavDdo_contagemresultadoexecucao_previstatitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE";
         Ddo_contagemresultadoexecucao_fim_Internalname = "DDO_CONTAGEMRESULTADOEXECUCAO_FIM";
         edtavDdo_contagemresultadoexecucao_fimtitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE";
         Ddo_contagemresultadoexecucao_dias_Internalname = "DDO_CONTAGEMRESULTADOEXECUCAO_DIAS";
         edtavDdo_contagemresultadoexecucao_diastitlecontrolidtoreplace_Internalname = "vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContagemResultadoExecucao_Dias_Jsonclick = "";
         edtContagemResultadoExecucao_Fim_Jsonclick = "";
         edtContagemResultadoExecucao_Prevista_Jsonclick = "";
         edtContagemResultadoExecucao_PrazoDias_Jsonclick = "";
         edtContagemResultadoExecucao_Inicio_Jsonclick = "";
         edtContagemResultadoExecucao_OSCod_Jsonclick = "";
         edtContagemResultadoExecucao_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavContagemresultadoexecucao_inicio_to1_Jsonclick = "";
         edtavContagemresultadoexecucao_inicio1_Jsonclick = "";
         edtavContagemresultadoexecucao_inicio_to2_Jsonclick = "";
         edtavContagemresultadoexecucao_inicio2_Jsonclick = "";
         edtavContagemresultadoexecucao_inicio_to3_Jsonclick = "";
         edtavContagemresultadoexecucao_inicio3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtContagemResultadoExecucao_Dias_Titleformat = 0;
         edtContagemResultadoExecucao_Fim_Titleformat = 0;
         edtContagemResultadoExecucao_Prevista_Titleformat = 0;
         edtContagemResultadoExecucao_PrazoDias_Titleformat = 0;
         edtContagemResultadoExecucao_Inicio_Titleformat = 0;
         edtContagemResultadoExecucao_OSCod_Titleformat = 0;
         edtContagemResultadoExecucao_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Visible = 1;
         tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Visible = 1;
         edtContagemResultadoExecucao_Dias_Title = "Usado";
         edtContagemResultadoExecucao_Fim_Title = "Fim";
         edtContagemResultadoExecucao_Prevista_Title = "Previsto";
         edtContagemResultadoExecucao_PrazoDias_Title = "Dias";
         edtContagemResultadoExecucao_Inicio_Title = "Inicio";
         edtContagemResultadoExecucao_OSCod_Title = "Demada";
         edtContagemResultadoExecucao_Codigo_Title = "Codigo";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_contagemresultadoexecucao_diastitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultadoexecucao_fimtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultadoexecucao_previstatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultadoexecucao_prazodiastitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultadoexecucao_iniciotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultadoexecucao_oscodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contagemresultadoexecucao_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfcontagemresultadoexecucao_dias_to_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_dias_to_Visible = 1;
         edtavTfcontagemresultadoexecucao_dias_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_dias_Visible = 1;
         edtavDdo_contagemresultadoexecucao_fimauxdateto_Jsonclick = "";
         edtavDdo_contagemresultadoexecucao_fimauxdate_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_fim_to_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_fim_to_Visible = 1;
         edtavTfcontagemresultadoexecucao_fim_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_fim_Visible = 1;
         edtavDdo_contagemresultadoexecucao_previstaauxdateto_Jsonclick = "";
         edtavDdo_contagemresultadoexecucao_previstaauxdate_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_prevista_to_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_prevista_to_Visible = 1;
         edtavTfcontagemresultadoexecucao_prevista_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_prevista_Visible = 1;
         edtavTfcontagemresultadoexecucao_prazodias_to_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_prazodias_to_Visible = 1;
         edtavTfcontagemresultadoexecucao_prazodias_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_prazodias_Visible = 1;
         edtavDdo_contagemresultadoexecucao_inicioauxdateto_Jsonclick = "";
         edtavDdo_contagemresultadoexecucao_inicioauxdate_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_inicio_to_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_inicio_to_Visible = 1;
         edtavTfcontagemresultadoexecucao_inicio_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_inicio_Visible = 1;
         edtavTfcontagemresultadoexecucao_oscod_to_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_oscod_to_Visible = 1;
         edtavTfcontagemresultadoexecucao_oscod_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_oscod_Visible = 1;
         edtavTfcontagemresultadoexecucao_codigo_to_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_codigo_to_Visible = 1;
         edtavTfcontagemresultadoexecucao_codigo_Jsonclick = "";
         edtavTfcontagemresultadoexecucao_codigo_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_contagemresultadoexecucao_dias_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultadoexecucao_dias_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemresultadoexecucao_dias_Rangefilterto = "At�";
         Ddo_contagemresultadoexecucao_dias_Rangefilterfrom = "Desde";
         Ddo_contagemresultadoexecucao_dias_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultadoexecucao_dias_Loadingdata = "Carregando dados...";
         Ddo_contagemresultadoexecucao_dias_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultadoexecucao_dias_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultadoexecucao_dias_Datalistupdateminimumcharacters = 0;
         Ddo_contagemresultadoexecucao_dias_Datalistfixedvalues = "";
         Ddo_contagemresultadoexecucao_dias_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultadoexecucao_dias_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_dias_Filtertype = "Numeric";
         Ddo_contagemresultadoexecucao_dias_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_dias_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_dias_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_dias_Titlecontrolidtoreplace = "";
         Ddo_contagemresultadoexecucao_dias_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultadoexecucao_dias_Cls = "ColumnSettings";
         Ddo_contagemresultadoexecucao_dias_Tooltip = "Op��es";
         Ddo_contagemresultadoexecucao_dias_Caption = "";
         Ddo_contagemresultadoexecucao_fim_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultadoexecucao_fim_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemresultadoexecucao_fim_Rangefilterto = "At�";
         Ddo_contagemresultadoexecucao_fim_Rangefilterfrom = "Desde";
         Ddo_contagemresultadoexecucao_fim_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultadoexecucao_fim_Loadingdata = "Carregando dados...";
         Ddo_contagemresultadoexecucao_fim_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultadoexecucao_fim_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultadoexecucao_fim_Datalistupdateminimumcharacters = 0;
         Ddo_contagemresultadoexecucao_fim_Datalistfixedvalues = "";
         Ddo_contagemresultadoexecucao_fim_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultadoexecucao_fim_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_fim_Filtertype = "Date";
         Ddo_contagemresultadoexecucao_fim_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_fim_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_fim_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_fim_Titlecontrolidtoreplace = "";
         Ddo_contagemresultadoexecucao_fim_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultadoexecucao_fim_Cls = "ColumnSettings";
         Ddo_contagemresultadoexecucao_fim_Tooltip = "Op��es";
         Ddo_contagemresultadoexecucao_fim_Caption = "";
         Ddo_contagemresultadoexecucao_prevista_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultadoexecucao_prevista_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemresultadoexecucao_prevista_Rangefilterto = "At�";
         Ddo_contagemresultadoexecucao_prevista_Rangefilterfrom = "Desde";
         Ddo_contagemresultadoexecucao_prevista_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultadoexecucao_prevista_Loadingdata = "Carregando dados...";
         Ddo_contagemresultadoexecucao_prevista_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultadoexecucao_prevista_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultadoexecucao_prevista_Datalistupdateminimumcharacters = 0;
         Ddo_contagemresultadoexecucao_prevista_Datalistfixedvalues = "";
         Ddo_contagemresultadoexecucao_prevista_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultadoexecucao_prevista_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_prevista_Filtertype = "Date";
         Ddo_contagemresultadoexecucao_prevista_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_prevista_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_prevista_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_prevista_Titlecontrolidtoreplace = "";
         Ddo_contagemresultadoexecucao_prevista_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultadoexecucao_prevista_Cls = "ColumnSettings";
         Ddo_contagemresultadoexecucao_prevista_Tooltip = "Op��es";
         Ddo_contagemresultadoexecucao_prevista_Caption = "";
         Ddo_contagemresultadoexecucao_prazodias_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultadoexecucao_prazodias_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemresultadoexecucao_prazodias_Rangefilterto = "At�";
         Ddo_contagemresultadoexecucao_prazodias_Rangefilterfrom = "Desde";
         Ddo_contagemresultadoexecucao_prazodias_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultadoexecucao_prazodias_Loadingdata = "Carregando dados...";
         Ddo_contagemresultadoexecucao_prazodias_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultadoexecucao_prazodias_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultadoexecucao_prazodias_Datalistupdateminimumcharacters = 0;
         Ddo_contagemresultadoexecucao_prazodias_Datalistfixedvalues = "";
         Ddo_contagemresultadoexecucao_prazodias_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultadoexecucao_prazodias_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_prazodias_Filtertype = "Numeric";
         Ddo_contagemresultadoexecucao_prazodias_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_prazodias_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_prazodias_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_prazodias_Titlecontrolidtoreplace = "";
         Ddo_contagemresultadoexecucao_prazodias_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultadoexecucao_prazodias_Cls = "ColumnSettings";
         Ddo_contagemresultadoexecucao_prazodias_Tooltip = "Op��es";
         Ddo_contagemresultadoexecucao_prazodias_Caption = "";
         Ddo_contagemresultadoexecucao_inicio_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultadoexecucao_inicio_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemresultadoexecucao_inicio_Rangefilterto = "At�";
         Ddo_contagemresultadoexecucao_inicio_Rangefilterfrom = "Desde";
         Ddo_contagemresultadoexecucao_inicio_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultadoexecucao_inicio_Loadingdata = "Carregando dados...";
         Ddo_contagemresultadoexecucao_inicio_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultadoexecucao_inicio_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultadoexecucao_inicio_Datalistupdateminimumcharacters = 0;
         Ddo_contagemresultadoexecucao_inicio_Datalistfixedvalues = "";
         Ddo_contagemresultadoexecucao_inicio_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultadoexecucao_inicio_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_inicio_Filtertype = "Date";
         Ddo_contagemresultadoexecucao_inicio_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_inicio_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_inicio_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_inicio_Titlecontrolidtoreplace = "";
         Ddo_contagemresultadoexecucao_inicio_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultadoexecucao_inicio_Cls = "ColumnSettings";
         Ddo_contagemresultadoexecucao_inicio_Tooltip = "Op��es";
         Ddo_contagemresultadoexecucao_inicio_Caption = "";
         Ddo_contagemresultadoexecucao_oscod_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultadoexecucao_oscod_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemresultadoexecucao_oscod_Rangefilterto = "At�";
         Ddo_contagemresultadoexecucao_oscod_Rangefilterfrom = "Desde";
         Ddo_contagemresultadoexecucao_oscod_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultadoexecucao_oscod_Loadingdata = "Carregando dados...";
         Ddo_contagemresultadoexecucao_oscod_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultadoexecucao_oscod_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultadoexecucao_oscod_Datalistupdateminimumcharacters = 0;
         Ddo_contagemresultadoexecucao_oscod_Datalistfixedvalues = "";
         Ddo_contagemresultadoexecucao_oscod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultadoexecucao_oscod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_oscod_Filtertype = "Numeric";
         Ddo_contagemresultadoexecucao_oscod_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_oscod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_oscod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_oscod_Titlecontrolidtoreplace = "";
         Ddo_contagemresultadoexecucao_oscod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultadoexecucao_oscod_Cls = "ColumnSettings";
         Ddo_contagemresultadoexecucao_oscod_Tooltip = "Op��es";
         Ddo_contagemresultadoexecucao_oscod_Caption = "";
         Ddo_contagemresultadoexecucao_codigo_Searchbuttontext = "Pesquisar";
         Ddo_contagemresultadoexecucao_codigo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contagemresultadoexecucao_codigo_Rangefilterto = "At�";
         Ddo_contagemresultadoexecucao_codigo_Rangefilterfrom = "Desde";
         Ddo_contagemresultadoexecucao_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_contagemresultadoexecucao_codigo_Loadingdata = "Carregando dados...";
         Ddo_contagemresultadoexecucao_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_contagemresultadoexecucao_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_contagemresultadoexecucao_codigo_Datalistupdateminimumcharacters = 0;
         Ddo_contagemresultadoexecucao_codigo_Datalistfixedvalues = "";
         Ddo_contagemresultadoexecucao_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contagemresultadoexecucao_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_codigo_Filtertype = "Numeric";
         Ddo_contagemresultadoexecucao_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contagemresultadoexecucao_codigo_Titlecontrolidtoreplace = "";
         Ddo_contagemresultadoexecucao_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contagemresultadoexecucao_codigo_Cls = "ColumnSettings";
         Ddo_contagemresultadoexecucao_codigo_Tooltip = "Op��es";
         Ddo_contagemresultadoexecucao_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Contagem Resultado Execucao";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContagemResultadoExecucao_Codigo',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContagemResultadoExecucao_Codigo_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContagemResultadoExecucao_OSCod',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFContagemResultadoExecucao_OSCod_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV40TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV45TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV46TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV49TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV50TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV55TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV56TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV61TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV62TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_OSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''}],oparms:[{av:'AV30ContagemResultadoExecucao_CodigoTitleFilterData',fld:'vCONTAGEMRESULTADOEXECUCAO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV34ContagemResultadoExecucao_OSCodTitleFilterData',fld:'vCONTAGEMRESULTADOEXECUCAO_OSCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV38ContagemResultadoExecucao_InicioTitleFilterData',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIOTITLEFILTERDATA',pic:'',nv:null},{av:'AV44ContagemResultadoExecucao_PrazoDiasTitleFilterData',fld:'vCONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLEFILTERDATA',pic:'',nv:null},{av:'AV48ContagemResultadoExecucao_PrevistaTitleFilterData',fld:'vCONTAGEMRESULTADOEXECUCAO_PREVISTATITLEFILTERDATA',pic:'',nv:null},{av:'AV54ContagemResultadoExecucao_FimTitleFilterData',fld:'vCONTAGEMRESULTADOEXECUCAO_FIMTITLEFILTERDATA',pic:'',nv:null},{av:'AV60ContagemResultadoExecucao_DiasTitleFilterData',fld:'vCONTAGEMRESULTADOEXECUCAO_DIASTITLEFILTERDATA',pic:'',nv:null},{av:'edtContagemResultadoExecucao_Codigo_Titleformat',ctrl:'CONTAGEMRESULTADOEXECUCAO_CODIGO',prop:'Titleformat'},{av:'edtContagemResultadoExecucao_Codigo_Title',ctrl:'CONTAGEMRESULTADOEXECUCAO_CODIGO',prop:'Title'},{av:'edtContagemResultadoExecucao_OSCod_Titleformat',ctrl:'CONTAGEMRESULTADOEXECUCAO_OSCOD',prop:'Titleformat'},{av:'edtContagemResultadoExecucao_OSCod_Title',ctrl:'CONTAGEMRESULTADOEXECUCAO_OSCOD',prop:'Title'},{av:'edtContagemResultadoExecucao_Inicio_Titleformat',ctrl:'CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'Titleformat'},{av:'edtContagemResultadoExecucao_Inicio_Title',ctrl:'CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'Title'},{av:'edtContagemResultadoExecucao_PrazoDias_Titleformat',ctrl:'CONTAGEMRESULTADOEXECUCAO_PRAZODIAS',prop:'Titleformat'},{av:'edtContagemResultadoExecucao_PrazoDias_Title',ctrl:'CONTAGEMRESULTADOEXECUCAO_PRAZODIAS',prop:'Title'},{av:'edtContagemResultadoExecucao_Prevista_Titleformat',ctrl:'CONTAGEMRESULTADOEXECUCAO_PREVISTA',prop:'Titleformat'},{av:'edtContagemResultadoExecucao_Prevista_Title',ctrl:'CONTAGEMRESULTADOEXECUCAO_PREVISTA',prop:'Title'},{av:'edtContagemResultadoExecucao_Fim_Titleformat',ctrl:'CONTAGEMRESULTADOEXECUCAO_FIM',prop:'Titleformat'},{av:'edtContagemResultadoExecucao_Fim_Title',ctrl:'CONTAGEMRESULTADOEXECUCAO_FIM',prop:'Title'},{av:'edtContagemResultadoExecucao_Dias_Titleformat',ctrl:'CONTAGEMRESULTADOEXECUCAO_DIAS',prop:'Titleformat'},{av:'edtContagemResultadoExecucao_Dias_Title',ctrl:'CONTAGEMRESULTADOEXECUCAO_DIAS',prop:'Title'},{av:'AV66GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV67GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11KJ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContagemResultadoExecucao_Codigo',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContagemResultadoExecucao_Codigo_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContagemResultadoExecucao_OSCod',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFContagemResultadoExecucao_OSCod_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV40TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV45TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV46TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV49TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV50TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV55TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV56TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV61TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV62TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_OSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO.ONOPTIONCLICKED","{handler:'E12KJ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContagemResultadoExecucao_Codigo',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContagemResultadoExecucao_Codigo_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContagemResultadoExecucao_OSCod',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFContagemResultadoExecucao_OSCod_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV40TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV45TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV46TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV49TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV50TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV55TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV56TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV61TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV62TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_OSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_contagemresultadoexecucao_codigo_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_contagemresultadoexecucao_codigo_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO',prop:'FilteredText_get'},{av:'Ddo_contagemresultadoexecucao_codigo_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultadoexecucao_codigo_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO',prop:'SortedStatus'},{av:'AV31TFContagemResultadoExecucao_Codigo',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContagemResultadoExecucao_Codigo_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemresultadoexecucao_oscod_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_inicio_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_prazodias_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_prevista_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_fim_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_FIM',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_dias_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_DIAS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD.ONOPTIONCLICKED","{handler:'E13KJ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContagemResultadoExecucao_Codigo',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContagemResultadoExecucao_Codigo_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContagemResultadoExecucao_OSCod',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFContagemResultadoExecucao_OSCod_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV40TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV45TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV46TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV49TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV50TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV55TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV56TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV61TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV62TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_OSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_contagemresultadoexecucao_oscod_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD',prop:'ActiveEventKey'},{av:'Ddo_contagemresultadoexecucao_oscod_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD',prop:'FilteredText_get'},{av:'Ddo_contagemresultadoexecucao_oscod_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultadoexecucao_oscod_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD',prop:'SortedStatus'},{av:'AV35TFContagemResultadoExecucao_OSCod',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFContagemResultadoExecucao_OSCod_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemresultadoexecucao_codigo_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_inicio_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_prazodias_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_prevista_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_fim_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_FIM',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_dias_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_DIAS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADOEXECUCAO_INICIO.ONOPTIONCLICKED","{handler:'E14KJ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContagemResultadoExecucao_Codigo',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContagemResultadoExecucao_Codigo_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContagemResultadoExecucao_OSCod',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFContagemResultadoExecucao_OSCod_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV40TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV45TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV46TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV49TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV50TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV55TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV56TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV61TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV62TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_OSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_contagemresultadoexecucao_inicio_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'ActiveEventKey'},{av:'Ddo_contagemresultadoexecucao_inicio_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'FilteredText_get'},{av:'Ddo_contagemresultadoexecucao_inicio_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultadoexecucao_inicio_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'SortedStatus'},{av:'AV39TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV40TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_contagemresultadoexecucao_codigo_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_oscod_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_prazodias_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_prevista_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_fim_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_FIM',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_dias_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_DIAS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS.ONOPTIONCLICKED","{handler:'E15KJ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContagemResultadoExecucao_Codigo',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContagemResultadoExecucao_Codigo_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContagemResultadoExecucao_OSCod',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFContagemResultadoExecucao_OSCod_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV40TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV45TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV46TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV49TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV50TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV55TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV56TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV61TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV62TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_OSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_contagemresultadoexecucao_prazodias_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS',prop:'ActiveEventKey'},{av:'Ddo_contagemresultadoexecucao_prazodias_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS',prop:'FilteredText_get'},{av:'Ddo_contagemresultadoexecucao_prazodias_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultadoexecucao_prazodias_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS',prop:'SortedStatus'},{av:'AV45TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV46TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'Ddo_contagemresultadoexecucao_codigo_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_oscod_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_inicio_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_prevista_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_fim_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_FIM',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_dias_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_DIAS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA.ONOPTIONCLICKED","{handler:'E16KJ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContagemResultadoExecucao_Codigo',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContagemResultadoExecucao_Codigo_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContagemResultadoExecucao_OSCod',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFContagemResultadoExecucao_OSCod_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV40TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV45TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV46TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV49TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV50TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV55TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV56TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV61TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV62TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_OSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_contagemresultadoexecucao_prevista_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA',prop:'ActiveEventKey'},{av:'Ddo_contagemresultadoexecucao_prevista_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA',prop:'FilteredText_get'},{av:'Ddo_contagemresultadoexecucao_prevista_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultadoexecucao_prevista_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA',prop:'SortedStatus'},{av:'AV49TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV50TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_contagemresultadoexecucao_codigo_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_oscod_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_inicio_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_prazodias_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_fim_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_FIM',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_dias_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_DIAS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADOEXECUCAO_FIM.ONOPTIONCLICKED","{handler:'E17KJ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContagemResultadoExecucao_Codigo',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContagemResultadoExecucao_Codigo_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContagemResultadoExecucao_OSCod',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFContagemResultadoExecucao_OSCod_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV40TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV45TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV46TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV49TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV50TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV55TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV56TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV61TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV62TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_OSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_contagemresultadoexecucao_fim_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_FIM',prop:'ActiveEventKey'},{av:'Ddo_contagemresultadoexecucao_fim_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_FIM',prop:'FilteredText_get'},{av:'Ddo_contagemresultadoexecucao_fim_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_FIM',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultadoexecucao_fim_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_FIM',prop:'SortedStatus'},{av:'AV55TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV56TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_contagemresultadoexecucao_codigo_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_oscod_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_inicio_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_prazodias_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_prevista_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_dias_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_DIAS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTAGEMRESULTADOEXECUCAO_DIAS.ONOPTIONCLICKED","{handler:'E18KJ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContagemResultadoExecucao_Codigo',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContagemResultadoExecucao_Codigo_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContagemResultadoExecucao_OSCod',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFContagemResultadoExecucao_OSCod_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV40TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV45TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV46TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV49TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV50TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV55TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV56TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV61TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV62TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_OSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'Ddo_contagemresultadoexecucao_dias_Activeeventkey',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_DIAS',prop:'ActiveEventKey'},{av:'Ddo_contagemresultadoexecucao_dias_Filteredtext_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_DIAS',prop:'FilteredText_get'},{av:'Ddo_contagemresultadoexecucao_dias_Filteredtextto_get',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_DIAS',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contagemresultadoexecucao_dias_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_DIAS',prop:'SortedStatus'},{av:'AV61TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV62TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'Ddo_contagemresultadoexecucao_codigo_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_oscod_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_inicio_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_prazodias_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_prevista_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA',prop:'SortedStatus'},{av:'Ddo_contagemresultadoexecucao_fim_Sortedstatus',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_FIM',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E28KJ2',iparms:[],oparms:[{av:'AV28Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E29KJ2',iparms:[{av:'A1405ContagemResultadoExecucao_Codigo',fld:'CONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1406ContagemResultadoExecucao_Inicio',fld:'CONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',hsh:true,nv:''}],oparms:[{av:'AV7InOutContagemResultadoExecucao_Codigo',fld:'vINOUTCONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutContagemResultadoExecucao_Inicio',fld:'vINOUTCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E19KJ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContagemResultadoExecucao_Codigo',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContagemResultadoExecucao_Codigo_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContagemResultadoExecucao_OSCod',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFContagemResultadoExecucao_OSCod_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV40TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV45TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV46TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV49TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV50TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV55TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV56TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV61TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV62TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_OSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E24KJ2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E20KJ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContagemResultadoExecucao_Codigo',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContagemResultadoExecucao_Codigo_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContagemResultadoExecucao_OSCod',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFContagemResultadoExecucao_OSCod_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV40TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV45TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV46TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV49TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV50TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV55TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV56TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV61TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV62TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_OSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E30KJ1',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E25KJ2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E21KJ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContagemResultadoExecucao_Codigo',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContagemResultadoExecucao_Codigo_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContagemResultadoExecucao_OSCod',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFContagemResultadoExecucao_OSCod_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV40TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV45TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV46TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV49TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV50TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV55TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV56TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV61TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV62TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_OSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E31KJ1',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E22KJ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContagemResultadoExecucao_Codigo',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContagemResultadoExecucao_Codigo_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContagemResultadoExecucao_OSCod',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFContagemResultadoExecucao_OSCod_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV40TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV45TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV46TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV49TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV50TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV55TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV56TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV61TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV62TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_OSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E32KJ1',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E23KJ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFContagemResultadoExecucao_Codigo',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFContagemResultadoExecucao_Codigo_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContagemResultadoExecucao_OSCod',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFContagemResultadoExecucao_OSCod_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'AV40TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV45TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'AV46TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'AV49TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'AV50TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV55TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'AV56TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'AV61TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'AV62TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_OSCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_INICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_PREVISTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_FIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace',fld:'vDDO_CONTAGEMRESULTADOEXECUCAO_DIASTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV31TFContagemResultadoExecucao_Codigo',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemresultadoexecucao_codigo_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO',prop:'FilteredText_set'},{av:'AV32TFContagemResultadoExecucao_Codigo_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemresultadoexecucao_codigo_Filteredtextto_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV35TFContagemResultadoExecucao_OSCod',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemresultadoexecucao_oscod_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD',prop:'FilteredText_set'},{av:'AV36TFContagemResultadoExecucao_OSCod_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_OSCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contagemresultadoexecucao_oscod_Filteredtextto_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_OSCOD',prop:'FilteredTextTo_set'},{av:'AV39TFContagemResultadoExecucao_Inicio',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_contagemresultadoexecucao_inicio_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'FilteredText_set'},{av:'AV40TFContagemResultadoExecucao_Inicio_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_INICIO_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_contagemresultadoexecucao_inicio_Filteredtextto_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_INICIO',prop:'FilteredTextTo_set'},{av:'AV45TFContagemResultadoExecucao_PrazoDias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS',pic:'ZZZ9',nv:0},{av:'Ddo_contagemresultadoexecucao_prazodias_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS',prop:'FilteredText_set'},{av:'AV46TFContagemResultadoExecucao_PrazoDias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PRAZODIAS_TO',pic:'ZZZ9',nv:0},{av:'Ddo_contagemresultadoexecucao_prazodias_Filteredtextto_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PRAZODIAS',prop:'FilteredTextTo_set'},{av:'AV49TFContagemResultadoExecucao_Prevista',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA',pic:'99/99/99 99:99',nv:''},{av:'Ddo_contagemresultadoexecucao_prevista_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA',prop:'FilteredText_set'},{av:'AV50TFContagemResultadoExecucao_Prevista_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_PREVISTA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_contagemresultadoexecucao_prevista_Filteredtextto_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_PREVISTA',prop:'FilteredTextTo_set'},{av:'AV55TFContagemResultadoExecucao_Fim',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM',pic:'99/99/99 99:99',nv:''},{av:'Ddo_contagemresultadoexecucao_fim_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_FIM',prop:'FilteredText_set'},{av:'AV56TFContagemResultadoExecucao_Fim_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_FIM_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_contagemresultadoexecucao_fim_Filteredtextto_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_FIM',prop:'FilteredTextTo_set'},{av:'AV61TFContagemResultadoExecucao_Dias',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS',pic:'ZZZ9',nv:0},{av:'Ddo_contagemresultadoexecucao_dias_Filteredtext_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_DIAS',prop:'FilteredText_set'},{av:'AV62TFContagemResultadoExecucao_Dias_To',fld:'vTFCONTAGEMRESULTADOEXECUCAO_DIAS_TO',pic:'ZZZ9',nv:0},{av:'Ddo_contagemresultadoexecucao_dias_Filteredtextto_set',ctrl:'DDO_CONTAGEMRESULTADOEXECUCAO_DIAS',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContagemResultadoExecucao_Inicio1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO1',pic:'99/99/99 99:99',nv:''},{av:'AV17ContagemResultadoExecucao_Inicio_To1',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO1',prop:'Visible'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContagemResultadoExecucao_Inicio2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO2',pic:'99/99/99 99:99',nv:''},{av:'AV21ContagemResultadoExecucao_Inicio_To2',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContagemResultadoExecucao_Inicio3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO3',pic:'99/99/99 99:99',nv:''},{av:'AV25ContagemResultadoExecucao_Inicio_To3',fld:'vCONTAGEMRESULTADOEXECUCAO_INICIO_TO3',pic:'99/99/99 99:99',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTAGEMRESULTADOEXECUCAO_INICIO3',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutContagemResultadoExecucao_Inicio = (DateTime)(DateTime.MinValue);
         Gridpaginationbar_Selectedpage = "";
         Ddo_contagemresultadoexecucao_codigo_Activeeventkey = "";
         Ddo_contagemresultadoexecucao_codigo_Filteredtext_get = "";
         Ddo_contagemresultadoexecucao_codigo_Filteredtextto_get = "";
         Ddo_contagemresultadoexecucao_oscod_Activeeventkey = "";
         Ddo_contagemresultadoexecucao_oscod_Filteredtext_get = "";
         Ddo_contagemresultadoexecucao_oscod_Filteredtextto_get = "";
         Ddo_contagemresultadoexecucao_inicio_Activeeventkey = "";
         Ddo_contagemresultadoexecucao_inicio_Filteredtext_get = "";
         Ddo_contagemresultadoexecucao_inicio_Filteredtextto_get = "";
         Ddo_contagemresultadoexecucao_prazodias_Activeeventkey = "";
         Ddo_contagemresultadoexecucao_prazodias_Filteredtext_get = "";
         Ddo_contagemresultadoexecucao_prazodias_Filteredtextto_get = "";
         Ddo_contagemresultadoexecucao_prevista_Activeeventkey = "";
         Ddo_contagemresultadoexecucao_prevista_Filteredtext_get = "";
         Ddo_contagemresultadoexecucao_prevista_Filteredtextto_get = "";
         Ddo_contagemresultadoexecucao_fim_Activeeventkey = "";
         Ddo_contagemresultadoexecucao_fim_Filteredtext_get = "";
         Ddo_contagemresultadoexecucao_fim_Filteredtextto_get = "";
         Ddo_contagemresultadoexecucao_dias_Activeeventkey = "";
         Ddo_contagemresultadoexecucao_dias_Filteredtext_get = "";
         Ddo_contagemresultadoexecucao_dias_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV16ContagemResultadoExecucao_Inicio1 = (DateTime)(DateTime.MinValue);
         AV17ContagemResultadoExecucao_Inicio_To1 = (DateTime)(DateTime.MinValue);
         AV19DynamicFiltersSelector2 = "";
         AV20ContagemResultadoExecucao_Inicio2 = (DateTime)(DateTime.MinValue);
         AV21ContagemResultadoExecucao_Inicio_To2 = (DateTime)(DateTime.MinValue);
         AV23DynamicFiltersSelector3 = "";
         AV24ContagemResultadoExecucao_Inicio3 = (DateTime)(DateTime.MinValue);
         AV25ContagemResultadoExecucao_Inicio_To3 = (DateTime)(DateTime.MinValue);
         AV39TFContagemResultadoExecucao_Inicio = (DateTime)(DateTime.MinValue);
         AV40TFContagemResultadoExecucao_Inicio_To = (DateTime)(DateTime.MinValue);
         AV49TFContagemResultadoExecucao_Prevista = (DateTime)(DateTime.MinValue);
         AV50TFContagemResultadoExecucao_Prevista_To = (DateTime)(DateTime.MinValue);
         AV55TFContagemResultadoExecucao_Fim = (DateTime)(DateTime.MinValue);
         AV56TFContagemResultadoExecucao_Fim_To = (DateTime)(DateTime.MinValue);
         AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace = "";
         AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace = "";
         AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace = "";
         AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace = "";
         AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace = "";
         AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace = "";
         AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV64DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV30ContagemResultadoExecucao_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34ContagemResultadoExecucao_OSCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38ContagemResultadoExecucao_InicioTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44ContagemResultadoExecucao_PrazoDiasTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV48ContagemResultadoExecucao_PrevistaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54ContagemResultadoExecucao_FimTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV60ContagemResultadoExecucao_DiasTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         Ddo_contagemresultadoexecucao_codigo_Filteredtext_set = "";
         Ddo_contagemresultadoexecucao_codigo_Filteredtextto_set = "";
         Ddo_contagemresultadoexecucao_codigo_Sortedstatus = "";
         Ddo_contagemresultadoexecucao_oscod_Filteredtext_set = "";
         Ddo_contagemresultadoexecucao_oscod_Filteredtextto_set = "";
         Ddo_contagemresultadoexecucao_oscod_Sortedstatus = "";
         Ddo_contagemresultadoexecucao_inicio_Filteredtext_set = "";
         Ddo_contagemresultadoexecucao_inicio_Filteredtextto_set = "";
         Ddo_contagemresultadoexecucao_inicio_Sortedstatus = "";
         Ddo_contagemresultadoexecucao_prazodias_Filteredtext_set = "";
         Ddo_contagemresultadoexecucao_prazodias_Filteredtextto_set = "";
         Ddo_contagemresultadoexecucao_prazodias_Sortedstatus = "";
         Ddo_contagemresultadoexecucao_prevista_Filteredtext_set = "";
         Ddo_contagemresultadoexecucao_prevista_Filteredtextto_set = "";
         Ddo_contagemresultadoexecucao_prevista_Sortedstatus = "";
         Ddo_contagemresultadoexecucao_fim_Filteredtext_set = "";
         Ddo_contagemresultadoexecucao_fim_Filteredtextto_set = "";
         Ddo_contagemresultadoexecucao_fim_Sortedstatus = "";
         Ddo_contagemresultadoexecucao_dias_Filteredtext_set = "";
         Ddo_contagemresultadoexecucao_dias_Filteredtextto_set = "";
         Ddo_contagemresultadoexecucao_dias_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV41DDO_ContagemResultadoExecucao_InicioAuxDate = DateTime.MinValue;
         AV42DDO_ContagemResultadoExecucao_InicioAuxDateTo = DateTime.MinValue;
         AV51DDO_ContagemResultadoExecucao_PrevistaAuxDate = DateTime.MinValue;
         AV52DDO_ContagemResultadoExecucao_PrevistaAuxDateTo = DateTime.MinValue;
         AV57DDO_ContagemResultadoExecucao_FimAuxDate = DateTime.MinValue;
         AV58DDO_ContagemResultadoExecucao_FimAuxDateTo = DateTime.MinValue;
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Select = "";
         AV70Select_GXI = "";
         A1406ContagemResultadoExecucao_Inicio = (DateTime)(DateTime.MinValue);
         A1408ContagemResultadoExecucao_Prevista = (DateTime)(DateTime.MinValue);
         A1407ContagemResultadoExecucao_Fim = (DateTime)(DateTime.MinValue);
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00KJ2_A1410ContagemResultadoExecucao_Dias = new short[1] ;
         H00KJ2_n1410ContagemResultadoExecucao_Dias = new bool[] {false} ;
         H00KJ2_A1407ContagemResultadoExecucao_Fim = new DateTime[] {DateTime.MinValue} ;
         H00KJ2_n1407ContagemResultadoExecucao_Fim = new bool[] {false} ;
         H00KJ2_A1408ContagemResultadoExecucao_Prevista = new DateTime[] {DateTime.MinValue} ;
         H00KJ2_n1408ContagemResultadoExecucao_Prevista = new bool[] {false} ;
         H00KJ2_A1411ContagemResultadoExecucao_PrazoDias = new short[1] ;
         H00KJ2_n1411ContagemResultadoExecucao_PrazoDias = new bool[] {false} ;
         H00KJ2_A1406ContagemResultadoExecucao_Inicio = new DateTime[] {DateTime.MinValue} ;
         H00KJ2_A1404ContagemResultadoExecucao_OSCod = new int[1] ;
         H00KJ2_A1405ContagemResultadoExecucao_Codigo = new int[1] ;
         H00KJ3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptcontagemresultadoexecucao__default(),
            new Object[][] {
                new Object[] {
               H00KJ2_A1410ContagemResultadoExecucao_Dias, H00KJ2_n1410ContagemResultadoExecucao_Dias, H00KJ2_A1407ContagemResultadoExecucao_Fim, H00KJ2_n1407ContagemResultadoExecucao_Fim, H00KJ2_A1408ContagemResultadoExecucao_Prevista, H00KJ2_n1408ContagemResultadoExecucao_Prevista, H00KJ2_A1411ContagemResultadoExecucao_PrazoDias, H00KJ2_n1411ContagemResultadoExecucao_PrazoDias, H00KJ2_A1406ContagemResultadoExecucao_Inicio, H00KJ2_A1404ContagemResultadoExecucao_OSCod,
               H00KJ2_A1405ContagemResultadoExecucao_Codigo
               }
               , new Object[] {
               H00KJ3_AGRID_nRecordCount
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_86 ;
      private short nGXsfl_86_idx=1 ;
      private short AV13OrderedBy ;
      private short AV45TFContagemResultadoExecucao_PrazoDias ;
      private short AV46TFContagemResultadoExecucao_PrazoDias_To ;
      private short AV61TFContagemResultadoExecucao_Dias ;
      private short AV62TFContagemResultadoExecucao_Dias_To ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A1411ContagemResultadoExecucao_PrazoDias ;
      private short A1410ContagemResultadoExecucao_Dias ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_86_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContagemResultadoExecucao_Codigo_Titleformat ;
      private short edtContagemResultadoExecucao_OSCod_Titleformat ;
      private short edtContagemResultadoExecucao_Inicio_Titleformat ;
      private short edtContagemResultadoExecucao_PrazoDias_Titleformat ;
      private short edtContagemResultadoExecucao_Prevista_Titleformat ;
      private short edtContagemResultadoExecucao_Fim_Titleformat ;
      private short edtContagemResultadoExecucao_Dias_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutContagemResultadoExecucao_Codigo ;
      private int wcpOAV7InOutContagemResultadoExecucao_Codigo ;
      private int subGrid_Rows ;
      private int AV31TFContagemResultadoExecucao_Codigo ;
      private int AV32TFContagemResultadoExecucao_Codigo_To ;
      private int AV35TFContagemResultadoExecucao_OSCod ;
      private int AV36TFContagemResultadoExecucao_OSCod_To ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contagemresultadoexecucao_codigo_Datalistupdateminimumcharacters ;
      private int Ddo_contagemresultadoexecucao_oscod_Datalistupdateminimumcharacters ;
      private int Ddo_contagemresultadoexecucao_inicio_Datalistupdateminimumcharacters ;
      private int Ddo_contagemresultadoexecucao_prazodias_Datalistupdateminimumcharacters ;
      private int Ddo_contagemresultadoexecucao_prevista_Datalistupdateminimumcharacters ;
      private int Ddo_contagemresultadoexecucao_fim_Datalistupdateminimumcharacters ;
      private int Ddo_contagemresultadoexecucao_dias_Datalistupdateminimumcharacters ;
      private int edtavTfcontagemresultadoexecucao_codigo_Visible ;
      private int edtavTfcontagemresultadoexecucao_codigo_to_Visible ;
      private int edtavTfcontagemresultadoexecucao_oscod_Visible ;
      private int edtavTfcontagemresultadoexecucao_oscod_to_Visible ;
      private int edtavTfcontagemresultadoexecucao_inicio_Visible ;
      private int edtavTfcontagemresultadoexecucao_inicio_to_Visible ;
      private int edtavTfcontagemresultadoexecucao_prazodias_Visible ;
      private int edtavTfcontagemresultadoexecucao_prazodias_to_Visible ;
      private int edtavTfcontagemresultadoexecucao_prevista_Visible ;
      private int edtavTfcontagemresultadoexecucao_prevista_to_Visible ;
      private int edtavTfcontagemresultadoexecucao_fim_Visible ;
      private int edtavTfcontagemresultadoexecucao_fim_to_Visible ;
      private int edtavTfcontagemresultadoexecucao_dias_Visible ;
      private int edtavTfcontagemresultadoexecucao_dias_to_Visible ;
      private int edtavDdo_contagemresultadoexecucao_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultadoexecucao_oscodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultadoexecucao_iniciotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultadoexecucao_prazodiastitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultadoexecucao_previstatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultadoexecucao_fimtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contagemresultadoexecucao_diastitlecontrolidtoreplace_Visible ;
      private int A1405ContagemResultadoExecucao_Codigo ;
      private int A1404ContagemResultadoExecucao_OSCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV65PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Visible ;
      private int tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV66GridCurrentPage ;
      private long AV67GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contagemresultadoexecucao_codigo_Activeeventkey ;
      private String Ddo_contagemresultadoexecucao_codigo_Filteredtext_get ;
      private String Ddo_contagemresultadoexecucao_codigo_Filteredtextto_get ;
      private String Ddo_contagemresultadoexecucao_oscod_Activeeventkey ;
      private String Ddo_contagemresultadoexecucao_oscod_Filteredtext_get ;
      private String Ddo_contagemresultadoexecucao_oscod_Filteredtextto_get ;
      private String Ddo_contagemresultadoexecucao_inicio_Activeeventkey ;
      private String Ddo_contagemresultadoexecucao_inicio_Filteredtext_get ;
      private String Ddo_contagemresultadoexecucao_inicio_Filteredtextto_get ;
      private String Ddo_contagemresultadoexecucao_prazodias_Activeeventkey ;
      private String Ddo_contagemresultadoexecucao_prazodias_Filteredtext_get ;
      private String Ddo_contagemresultadoexecucao_prazodias_Filteredtextto_get ;
      private String Ddo_contagemresultadoexecucao_prevista_Activeeventkey ;
      private String Ddo_contagemresultadoexecucao_prevista_Filteredtext_get ;
      private String Ddo_contagemresultadoexecucao_prevista_Filteredtextto_get ;
      private String Ddo_contagemresultadoexecucao_fim_Activeeventkey ;
      private String Ddo_contagemresultadoexecucao_fim_Filteredtext_get ;
      private String Ddo_contagemresultadoexecucao_fim_Filteredtextto_get ;
      private String Ddo_contagemresultadoexecucao_dias_Activeeventkey ;
      private String Ddo_contagemresultadoexecucao_dias_Filteredtext_get ;
      private String Ddo_contagemresultadoexecucao_dias_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_86_idx="0001" ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contagemresultadoexecucao_codigo_Caption ;
      private String Ddo_contagemresultadoexecucao_codigo_Tooltip ;
      private String Ddo_contagemresultadoexecucao_codigo_Cls ;
      private String Ddo_contagemresultadoexecucao_codigo_Filteredtext_set ;
      private String Ddo_contagemresultadoexecucao_codigo_Filteredtextto_set ;
      private String Ddo_contagemresultadoexecucao_codigo_Dropdownoptionstype ;
      private String Ddo_contagemresultadoexecucao_codigo_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultadoexecucao_codigo_Sortedstatus ;
      private String Ddo_contagemresultadoexecucao_codigo_Filtertype ;
      private String Ddo_contagemresultadoexecucao_codigo_Datalistfixedvalues ;
      private String Ddo_contagemresultadoexecucao_codigo_Sortasc ;
      private String Ddo_contagemresultadoexecucao_codigo_Sortdsc ;
      private String Ddo_contagemresultadoexecucao_codigo_Loadingdata ;
      private String Ddo_contagemresultadoexecucao_codigo_Cleanfilter ;
      private String Ddo_contagemresultadoexecucao_codigo_Rangefilterfrom ;
      private String Ddo_contagemresultadoexecucao_codigo_Rangefilterto ;
      private String Ddo_contagemresultadoexecucao_codigo_Noresultsfound ;
      private String Ddo_contagemresultadoexecucao_codigo_Searchbuttontext ;
      private String Ddo_contagemresultadoexecucao_oscod_Caption ;
      private String Ddo_contagemresultadoexecucao_oscod_Tooltip ;
      private String Ddo_contagemresultadoexecucao_oscod_Cls ;
      private String Ddo_contagemresultadoexecucao_oscod_Filteredtext_set ;
      private String Ddo_contagemresultadoexecucao_oscod_Filteredtextto_set ;
      private String Ddo_contagemresultadoexecucao_oscod_Dropdownoptionstype ;
      private String Ddo_contagemresultadoexecucao_oscod_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultadoexecucao_oscod_Sortedstatus ;
      private String Ddo_contagemresultadoexecucao_oscod_Filtertype ;
      private String Ddo_contagemresultadoexecucao_oscod_Datalistfixedvalues ;
      private String Ddo_contagemresultadoexecucao_oscod_Sortasc ;
      private String Ddo_contagemresultadoexecucao_oscod_Sortdsc ;
      private String Ddo_contagemresultadoexecucao_oscod_Loadingdata ;
      private String Ddo_contagemresultadoexecucao_oscod_Cleanfilter ;
      private String Ddo_contagemresultadoexecucao_oscod_Rangefilterfrom ;
      private String Ddo_contagemresultadoexecucao_oscod_Rangefilterto ;
      private String Ddo_contagemresultadoexecucao_oscod_Noresultsfound ;
      private String Ddo_contagemresultadoexecucao_oscod_Searchbuttontext ;
      private String Ddo_contagemresultadoexecucao_inicio_Caption ;
      private String Ddo_contagemresultadoexecucao_inicio_Tooltip ;
      private String Ddo_contagemresultadoexecucao_inicio_Cls ;
      private String Ddo_contagemresultadoexecucao_inicio_Filteredtext_set ;
      private String Ddo_contagemresultadoexecucao_inicio_Filteredtextto_set ;
      private String Ddo_contagemresultadoexecucao_inicio_Dropdownoptionstype ;
      private String Ddo_contagemresultadoexecucao_inicio_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultadoexecucao_inicio_Sortedstatus ;
      private String Ddo_contagemresultadoexecucao_inicio_Filtertype ;
      private String Ddo_contagemresultadoexecucao_inicio_Datalistfixedvalues ;
      private String Ddo_contagemresultadoexecucao_inicio_Sortasc ;
      private String Ddo_contagemresultadoexecucao_inicio_Sortdsc ;
      private String Ddo_contagemresultadoexecucao_inicio_Loadingdata ;
      private String Ddo_contagemresultadoexecucao_inicio_Cleanfilter ;
      private String Ddo_contagemresultadoexecucao_inicio_Rangefilterfrom ;
      private String Ddo_contagemresultadoexecucao_inicio_Rangefilterto ;
      private String Ddo_contagemresultadoexecucao_inicio_Noresultsfound ;
      private String Ddo_contagemresultadoexecucao_inicio_Searchbuttontext ;
      private String Ddo_contagemresultadoexecucao_prazodias_Caption ;
      private String Ddo_contagemresultadoexecucao_prazodias_Tooltip ;
      private String Ddo_contagemresultadoexecucao_prazodias_Cls ;
      private String Ddo_contagemresultadoexecucao_prazodias_Filteredtext_set ;
      private String Ddo_contagemresultadoexecucao_prazodias_Filteredtextto_set ;
      private String Ddo_contagemresultadoexecucao_prazodias_Dropdownoptionstype ;
      private String Ddo_contagemresultadoexecucao_prazodias_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultadoexecucao_prazodias_Sortedstatus ;
      private String Ddo_contagemresultadoexecucao_prazodias_Filtertype ;
      private String Ddo_contagemresultadoexecucao_prazodias_Datalistfixedvalues ;
      private String Ddo_contagemresultadoexecucao_prazodias_Sortasc ;
      private String Ddo_contagemresultadoexecucao_prazodias_Sortdsc ;
      private String Ddo_contagemresultadoexecucao_prazodias_Loadingdata ;
      private String Ddo_contagemresultadoexecucao_prazodias_Cleanfilter ;
      private String Ddo_contagemresultadoexecucao_prazodias_Rangefilterfrom ;
      private String Ddo_contagemresultadoexecucao_prazodias_Rangefilterto ;
      private String Ddo_contagemresultadoexecucao_prazodias_Noresultsfound ;
      private String Ddo_contagemresultadoexecucao_prazodias_Searchbuttontext ;
      private String Ddo_contagemresultadoexecucao_prevista_Caption ;
      private String Ddo_contagemresultadoexecucao_prevista_Tooltip ;
      private String Ddo_contagemresultadoexecucao_prevista_Cls ;
      private String Ddo_contagemresultadoexecucao_prevista_Filteredtext_set ;
      private String Ddo_contagemresultadoexecucao_prevista_Filteredtextto_set ;
      private String Ddo_contagemresultadoexecucao_prevista_Dropdownoptionstype ;
      private String Ddo_contagemresultadoexecucao_prevista_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultadoexecucao_prevista_Sortedstatus ;
      private String Ddo_contagemresultadoexecucao_prevista_Filtertype ;
      private String Ddo_contagemresultadoexecucao_prevista_Datalistfixedvalues ;
      private String Ddo_contagemresultadoexecucao_prevista_Sortasc ;
      private String Ddo_contagemresultadoexecucao_prevista_Sortdsc ;
      private String Ddo_contagemresultadoexecucao_prevista_Loadingdata ;
      private String Ddo_contagemresultadoexecucao_prevista_Cleanfilter ;
      private String Ddo_contagemresultadoexecucao_prevista_Rangefilterfrom ;
      private String Ddo_contagemresultadoexecucao_prevista_Rangefilterto ;
      private String Ddo_contagemresultadoexecucao_prevista_Noresultsfound ;
      private String Ddo_contagemresultadoexecucao_prevista_Searchbuttontext ;
      private String Ddo_contagemresultadoexecucao_fim_Caption ;
      private String Ddo_contagemresultadoexecucao_fim_Tooltip ;
      private String Ddo_contagemresultadoexecucao_fim_Cls ;
      private String Ddo_contagemresultadoexecucao_fim_Filteredtext_set ;
      private String Ddo_contagemresultadoexecucao_fim_Filteredtextto_set ;
      private String Ddo_contagemresultadoexecucao_fim_Dropdownoptionstype ;
      private String Ddo_contagemresultadoexecucao_fim_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultadoexecucao_fim_Sortedstatus ;
      private String Ddo_contagemresultadoexecucao_fim_Filtertype ;
      private String Ddo_contagemresultadoexecucao_fim_Datalistfixedvalues ;
      private String Ddo_contagemresultadoexecucao_fim_Sortasc ;
      private String Ddo_contagemresultadoexecucao_fim_Sortdsc ;
      private String Ddo_contagemresultadoexecucao_fim_Loadingdata ;
      private String Ddo_contagemresultadoexecucao_fim_Cleanfilter ;
      private String Ddo_contagemresultadoexecucao_fim_Rangefilterfrom ;
      private String Ddo_contagemresultadoexecucao_fim_Rangefilterto ;
      private String Ddo_contagemresultadoexecucao_fim_Noresultsfound ;
      private String Ddo_contagemresultadoexecucao_fim_Searchbuttontext ;
      private String Ddo_contagemresultadoexecucao_dias_Caption ;
      private String Ddo_contagemresultadoexecucao_dias_Tooltip ;
      private String Ddo_contagemresultadoexecucao_dias_Cls ;
      private String Ddo_contagemresultadoexecucao_dias_Filteredtext_set ;
      private String Ddo_contagemresultadoexecucao_dias_Filteredtextto_set ;
      private String Ddo_contagemresultadoexecucao_dias_Dropdownoptionstype ;
      private String Ddo_contagemresultadoexecucao_dias_Titlecontrolidtoreplace ;
      private String Ddo_contagemresultadoexecucao_dias_Sortedstatus ;
      private String Ddo_contagemresultadoexecucao_dias_Filtertype ;
      private String Ddo_contagemresultadoexecucao_dias_Datalistfixedvalues ;
      private String Ddo_contagemresultadoexecucao_dias_Sortasc ;
      private String Ddo_contagemresultadoexecucao_dias_Sortdsc ;
      private String Ddo_contagemresultadoexecucao_dias_Loadingdata ;
      private String Ddo_contagemresultadoexecucao_dias_Cleanfilter ;
      private String Ddo_contagemresultadoexecucao_dias_Rangefilterfrom ;
      private String Ddo_contagemresultadoexecucao_dias_Rangefilterto ;
      private String Ddo_contagemresultadoexecucao_dias_Noresultsfound ;
      private String Ddo_contagemresultadoexecucao_dias_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfcontagemresultadoexecucao_codigo_Internalname ;
      private String edtavTfcontagemresultadoexecucao_codigo_Jsonclick ;
      private String edtavTfcontagemresultadoexecucao_codigo_to_Internalname ;
      private String edtavTfcontagemresultadoexecucao_codigo_to_Jsonclick ;
      private String edtavTfcontagemresultadoexecucao_oscod_Internalname ;
      private String edtavTfcontagemresultadoexecucao_oscod_Jsonclick ;
      private String edtavTfcontagemresultadoexecucao_oscod_to_Internalname ;
      private String edtavTfcontagemresultadoexecucao_oscod_to_Jsonclick ;
      private String edtavTfcontagemresultadoexecucao_inicio_Internalname ;
      private String edtavTfcontagemresultadoexecucao_inicio_Jsonclick ;
      private String edtavTfcontagemresultadoexecucao_inicio_to_Internalname ;
      private String edtavTfcontagemresultadoexecucao_inicio_to_Jsonclick ;
      private String divDdo_contagemresultadoexecucao_inicioauxdates_Internalname ;
      private String edtavDdo_contagemresultadoexecucao_inicioauxdate_Internalname ;
      private String edtavDdo_contagemresultadoexecucao_inicioauxdate_Jsonclick ;
      private String edtavDdo_contagemresultadoexecucao_inicioauxdateto_Internalname ;
      private String edtavDdo_contagemresultadoexecucao_inicioauxdateto_Jsonclick ;
      private String edtavTfcontagemresultadoexecucao_prazodias_Internalname ;
      private String edtavTfcontagemresultadoexecucao_prazodias_Jsonclick ;
      private String edtavTfcontagemresultadoexecucao_prazodias_to_Internalname ;
      private String edtavTfcontagemresultadoexecucao_prazodias_to_Jsonclick ;
      private String edtavTfcontagemresultadoexecucao_prevista_Internalname ;
      private String edtavTfcontagemresultadoexecucao_prevista_Jsonclick ;
      private String edtavTfcontagemresultadoexecucao_prevista_to_Internalname ;
      private String edtavTfcontagemresultadoexecucao_prevista_to_Jsonclick ;
      private String divDdo_contagemresultadoexecucao_previstaauxdates_Internalname ;
      private String edtavDdo_contagemresultadoexecucao_previstaauxdate_Internalname ;
      private String edtavDdo_contagemresultadoexecucao_previstaauxdate_Jsonclick ;
      private String edtavDdo_contagemresultadoexecucao_previstaauxdateto_Internalname ;
      private String edtavDdo_contagemresultadoexecucao_previstaauxdateto_Jsonclick ;
      private String edtavTfcontagemresultadoexecucao_fim_Internalname ;
      private String edtavTfcontagemresultadoexecucao_fim_Jsonclick ;
      private String edtavTfcontagemresultadoexecucao_fim_to_Internalname ;
      private String edtavTfcontagemresultadoexecucao_fim_to_Jsonclick ;
      private String divDdo_contagemresultadoexecucao_fimauxdates_Internalname ;
      private String edtavDdo_contagemresultadoexecucao_fimauxdate_Internalname ;
      private String edtavDdo_contagemresultadoexecucao_fimauxdate_Jsonclick ;
      private String edtavDdo_contagemresultadoexecucao_fimauxdateto_Internalname ;
      private String edtavDdo_contagemresultadoexecucao_fimauxdateto_Jsonclick ;
      private String edtavTfcontagemresultadoexecucao_dias_Internalname ;
      private String edtavTfcontagemresultadoexecucao_dias_Jsonclick ;
      private String edtavTfcontagemresultadoexecucao_dias_to_Internalname ;
      private String edtavTfcontagemresultadoexecucao_dias_to_Jsonclick ;
      private String edtavDdo_contagemresultadoexecucao_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultadoexecucao_oscodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultadoexecucao_iniciotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultadoexecucao_prazodiastitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultadoexecucao_previstatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultadoexecucao_fimtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contagemresultadoexecucao_diastitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtContagemResultadoExecucao_Codigo_Internalname ;
      private String edtContagemResultadoExecucao_OSCod_Internalname ;
      private String edtContagemResultadoExecucao_Inicio_Internalname ;
      private String edtContagemResultadoExecucao_PrazoDias_Internalname ;
      private String edtContagemResultadoExecucao_Prevista_Internalname ;
      private String edtContagemResultadoExecucao_Fim_Internalname ;
      private String edtContagemResultadoExecucao_Dias_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavContagemresultadoexecucao_inicio1_Internalname ;
      private String edtavContagemresultadoexecucao_inicio_to1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavContagemresultadoexecucao_inicio2_Internalname ;
      private String edtavContagemresultadoexecucao_inicio_to2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavContagemresultadoexecucao_inicio3_Internalname ;
      private String edtavContagemresultadoexecucao_inicio_to3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contagemresultadoexecucao_codigo_Internalname ;
      private String Ddo_contagemresultadoexecucao_oscod_Internalname ;
      private String Ddo_contagemresultadoexecucao_inicio_Internalname ;
      private String Ddo_contagemresultadoexecucao_prazodias_Internalname ;
      private String Ddo_contagemresultadoexecucao_prevista_Internalname ;
      private String Ddo_contagemresultadoexecucao_fim_Internalname ;
      private String Ddo_contagemresultadoexecucao_dias_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContagemResultadoExecucao_Codigo_Title ;
      private String edtContagemResultadoExecucao_OSCod_Title ;
      private String edtContagemResultadoExecucao_Inicio_Title ;
      private String edtContagemResultadoExecucao_PrazoDias_Title ;
      private String edtContagemResultadoExecucao_Prevista_Title ;
      private String edtContagemResultadoExecucao_Fim_Title ;
      private String edtContagemResultadoExecucao_Dias_Title ;
      private String edtavSelect_Tooltiptext ;
      private String tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio1_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio2_Internalname ;
      private String tblTablemergeddynamicfilterscontagemresultadoexecucao_inicio3_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavContagemresultadoexecucao_inicio3_Jsonclick ;
      private String lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext3_Internalname ;
      private String lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext3_Jsonclick ;
      private String edtavContagemresultadoexecucao_inicio_to3_Jsonclick ;
      private String edtavContagemresultadoexecucao_inicio2_Jsonclick ;
      private String lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext2_Internalname ;
      private String lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext2_Jsonclick ;
      private String edtavContagemresultadoexecucao_inicio_to2_Jsonclick ;
      private String edtavContagemresultadoexecucao_inicio1_Jsonclick ;
      private String lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext1_Internalname ;
      private String lblDynamicfilterscontagemresultadoexecucao_inicio_rangemiddletext1_Jsonclick ;
      private String edtavContagemresultadoexecucao_inicio_to1_Jsonclick ;
      private String sGXsfl_86_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtContagemResultadoExecucao_Codigo_Jsonclick ;
      private String edtContagemResultadoExecucao_OSCod_Jsonclick ;
      private String edtContagemResultadoExecucao_Inicio_Jsonclick ;
      private String edtContagemResultadoExecucao_PrazoDias_Jsonclick ;
      private String edtContagemResultadoExecucao_Prevista_Jsonclick ;
      private String edtContagemResultadoExecucao_Fim_Jsonclick ;
      private String edtContagemResultadoExecucao_Dias_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV8InOutContagemResultadoExecucao_Inicio ;
      private DateTime wcpOAV8InOutContagemResultadoExecucao_Inicio ;
      private DateTime AV16ContagemResultadoExecucao_Inicio1 ;
      private DateTime AV17ContagemResultadoExecucao_Inicio_To1 ;
      private DateTime AV20ContagemResultadoExecucao_Inicio2 ;
      private DateTime AV21ContagemResultadoExecucao_Inicio_To2 ;
      private DateTime AV24ContagemResultadoExecucao_Inicio3 ;
      private DateTime AV25ContagemResultadoExecucao_Inicio_To3 ;
      private DateTime AV39TFContagemResultadoExecucao_Inicio ;
      private DateTime AV40TFContagemResultadoExecucao_Inicio_To ;
      private DateTime AV49TFContagemResultadoExecucao_Prevista ;
      private DateTime AV50TFContagemResultadoExecucao_Prevista_To ;
      private DateTime AV55TFContagemResultadoExecucao_Fim ;
      private DateTime AV56TFContagemResultadoExecucao_Fim_To ;
      private DateTime A1406ContagemResultadoExecucao_Inicio ;
      private DateTime A1408ContagemResultadoExecucao_Prevista ;
      private DateTime A1407ContagemResultadoExecucao_Fim ;
      private DateTime AV41DDO_ContagemResultadoExecucao_InicioAuxDate ;
      private DateTime AV42DDO_ContagemResultadoExecucao_InicioAuxDateTo ;
      private DateTime AV51DDO_ContagemResultadoExecucao_PrevistaAuxDate ;
      private DateTime AV52DDO_ContagemResultadoExecucao_PrevistaAuxDateTo ;
      private DateTime AV57DDO_ContagemResultadoExecucao_FimAuxDate ;
      private DateTime AV58DDO_ContagemResultadoExecucao_FimAuxDateTo ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool toggleJsOutput ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contagemresultadoexecucao_codigo_Includesortasc ;
      private bool Ddo_contagemresultadoexecucao_codigo_Includesortdsc ;
      private bool Ddo_contagemresultadoexecucao_codigo_Includefilter ;
      private bool Ddo_contagemresultadoexecucao_codigo_Filterisrange ;
      private bool Ddo_contagemresultadoexecucao_codigo_Includedatalist ;
      private bool Ddo_contagemresultadoexecucao_oscod_Includesortasc ;
      private bool Ddo_contagemresultadoexecucao_oscod_Includesortdsc ;
      private bool Ddo_contagemresultadoexecucao_oscod_Includefilter ;
      private bool Ddo_contagemresultadoexecucao_oscod_Filterisrange ;
      private bool Ddo_contagemresultadoexecucao_oscod_Includedatalist ;
      private bool Ddo_contagemresultadoexecucao_inicio_Includesortasc ;
      private bool Ddo_contagemresultadoexecucao_inicio_Includesortdsc ;
      private bool Ddo_contagemresultadoexecucao_inicio_Includefilter ;
      private bool Ddo_contagemresultadoexecucao_inicio_Filterisrange ;
      private bool Ddo_contagemresultadoexecucao_inicio_Includedatalist ;
      private bool Ddo_contagemresultadoexecucao_prazodias_Includesortasc ;
      private bool Ddo_contagemresultadoexecucao_prazodias_Includesortdsc ;
      private bool Ddo_contagemresultadoexecucao_prazodias_Includefilter ;
      private bool Ddo_contagemresultadoexecucao_prazodias_Filterisrange ;
      private bool Ddo_contagemresultadoexecucao_prazodias_Includedatalist ;
      private bool Ddo_contagemresultadoexecucao_prevista_Includesortasc ;
      private bool Ddo_contagemresultadoexecucao_prevista_Includesortdsc ;
      private bool Ddo_contagemresultadoexecucao_prevista_Includefilter ;
      private bool Ddo_contagemresultadoexecucao_prevista_Filterisrange ;
      private bool Ddo_contagemresultadoexecucao_prevista_Includedatalist ;
      private bool Ddo_contagemresultadoexecucao_fim_Includesortasc ;
      private bool Ddo_contagemresultadoexecucao_fim_Includesortdsc ;
      private bool Ddo_contagemresultadoexecucao_fim_Includefilter ;
      private bool Ddo_contagemresultadoexecucao_fim_Filterisrange ;
      private bool Ddo_contagemresultadoexecucao_fim_Includedatalist ;
      private bool Ddo_contagemresultadoexecucao_dias_Includesortasc ;
      private bool Ddo_contagemresultadoexecucao_dias_Includesortdsc ;
      private bool Ddo_contagemresultadoexecucao_dias_Includefilter ;
      private bool Ddo_contagemresultadoexecucao_dias_Filterisrange ;
      private bool Ddo_contagemresultadoexecucao_dias_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1411ContagemResultadoExecucao_PrazoDias ;
      private bool n1408ContagemResultadoExecucao_Prevista ;
      private bool n1407ContagemResultadoExecucao_Fim ;
      private bool n1410ContagemResultadoExecucao_Dias ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Select_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV33ddo_ContagemResultadoExecucao_CodigoTitleControlIdToReplace ;
      private String AV37ddo_ContagemResultadoExecucao_OSCodTitleControlIdToReplace ;
      private String AV43ddo_ContagemResultadoExecucao_InicioTitleControlIdToReplace ;
      private String AV47ddo_ContagemResultadoExecucao_PrazoDiasTitleControlIdToReplace ;
      private String AV53ddo_ContagemResultadoExecucao_PrevistaTitleControlIdToReplace ;
      private String AV59ddo_ContagemResultadoExecucao_FimTitleControlIdToReplace ;
      private String AV63ddo_ContagemResultadoExecucao_DiasTitleControlIdToReplace ;
      private String AV70Select_GXI ;
      private String AV28Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutContagemResultadoExecucao_Codigo ;
      private DateTime aP1_InOutContagemResultadoExecucao_Inicio ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private short[] H00KJ2_A1410ContagemResultadoExecucao_Dias ;
      private bool[] H00KJ2_n1410ContagemResultadoExecucao_Dias ;
      private DateTime[] H00KJ2_A1407ContagemResultadoExecucao_Fim ;
      private bool[] H00KJ2_n1407ContagemResultadoExecucao_Fim ;
      private DateTime[] H00KJ2_A1408ContagemResultadoExecucao_Prevista ;
      private bool[] H00KJ2_n1408ContagemResultadoExecucao_Prevista ;
      private short[] H00KJ2_A1411ContagemResultadoExecucao_PrazoDias ;
      private bool[] H00KJ2_n1411ContagemResultadoExecucao_PrazoDias ;
      private DateTime[] H00KJ2_A1406ContagemResultadoExecucao_Inicio ;
      private int[] H00KJ2_A1404ContagemResultadoExecucao_OSCod ;
      private int[] H00KJ2_A1405ContagemResultadoExecucao_Codigo ;
      private long[] H00KJ3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV30ContagemResultadoExecucao_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV34ContagemResultadoExecucao_OSCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38ContagemResultadoExecucao_InicioTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV44ContagemResultadoExecucao_PrazoDiasTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV48ContagemResultadoExecucao_PrevistaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV54ContagemResultadoExecucao_FimTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV60ContagemResultadoExecucao_DiasTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV64DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptcontagemresultadoexecucao__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00KJ2( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             DateTime AV16ContagemResultadoExecucao_Inicio1 ,
                                             DateTime AV17ContagemResultadoExecucao_Inicio_To1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             DateTime AV20ContagemResultadoExecucao_Inicio2 ,
                                             DateTime AV21ContagemResultadoExecucao_Inicio_To2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             DateTime AV24ContagemResultadoExecucao_Inicio3 ,
                                             DateTime AV25ContagemResultadoExecucao_Inicio_To3 ,
                                             int AV31TFContagemResultadoExecucao_Codigo ,
                                             int AV32TFContagemResultadoExecucao_Codigo_To ,
                                             int AV35TFContagemResultadoExecucao_OSCod ,
                                             int AV36TFContagemResultadoExecucao_OSCod_To ,
                                             DateTime AV39TFContagemResultadoExecucao_Inicio ,
                                             DateTime AV40TFContagemResultadoExecucao_Inicio_To ,
                                             short AV45TFContagemResultadoExecucao_PrazoDias ,
                                             short AV46TFContagemResultadoExecucao_PrazoDias_To ,
                                             DateTime AV49TFContagemResultadoExecucao_Prevista ,
                                             DateTime AV50TFContagemResultadoExecucao_Prevista_To ,
                                             DateTime AV55TFContagemResultadoExecucao_Fim ,
                                             DateTime AV56TFContagemResultadoExecucao_Fim_To ,
                                             short AV61TFContagemResultadoExecucao_Dias ,
                                             short AV62TFContagemResultadoExecucao_Dias_To ,
                                             DateTime A1406ContagemResultadoExecucao_Inicio ,
                                             int A1405ContagemResultadoExecucao_Codigo ,
                                             int A1404ContagemResultadoExecucao_OSCod ,
                                             short A1411ContagemResultadoExecucao_PrazoDias ,
                                             DateTime A1408ContagemResultadoExecucao_Prevista ,
                                             DateTime A1407ContagemResultadoExecucao_Fim ,
                                             short A1410ContagemResultadoExecucao_Dias ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [25] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [ContagemResultadoExecucao_Dias], [ContagemResultadoExecucao_Fim], [ContagemResultadoExecucao_Prevista], [ContagemResultadoExecucao_PrazoDias], [ContagemResultadoExecucao_Inicio], [ContagemResultadoExecucao_OSCod], [ContagemResultadoExecucao_Codigo]";
         sFromString = " FROM [ContagemResultadoExecucao] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV16ContagemResultadoExecucao_Inicio1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Inicio] >= @AV16ContagemResultadoExecucao_Inicio1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Inicio] >= @AV16ContagemResultadoExecucao_Inicio1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV17ContagemResultadoExecucao_Inicio_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Inicio] <= @AV17ContagemResultadoExecucao_Inicio_To1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Inicio] <= @AV17ContagemResultadoExecucao_Inicio_To1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV20ContagemResultadoExecucao_Inicio2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Inicio] >= @AV20ContagemResultadoExecucao_Inicio2)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Inicio] >= @AV20ContagemResultadoExecucao_Inicio2)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV21ContagemResultadoExecucao_Inicio_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Inicio] <= @AV21ContagemResultadoExecucao_Inicio_To2)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Inicio] <= @AV21ContagemResultadoExecucao_Inicio_To2)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV24ContagemResultadoExecucao_Inicio3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Inicio] >= @AV24ContagemResultadoExecucao_Inicio3)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Inicio] >= @AV24ContagemResultadoExecucao_Inicio3)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV25ContagemResultadoExecucao_Inicio_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Inicio] <= @AV25ContagemResultadoExecucao_Inicio_To3)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Inicio] <= @AV25ContagemResultadoExecucao_Inicio_To3)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (0==AV31TFContagemResultadoExecucao_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Codigo] >= @AV31TFContagemResultadoExecucao_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Codigo] >= @AV31TFContagemResultadoExecucao_Codigo)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! (0==AV32TFContagemResultadoExecucao_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Codigo] <= @AV32TFContagemResultadoExecucao_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Codigo] <= @AV32TFContagemResultadoExecucao_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! (0==AV35TFContagemResultadoExecucao_OSCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_OSCod] >= @AV35TFContagemResultadoExecucao_OSCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_OSCod] >= @AV35TFContagemResultadoExecucao_OSCod)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! (0==AV36TFContagemResultadoExecucao_OSCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_OSCod] <= @AV36TFContagemResultadoExecucao_OSCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_OSCod] <= @AV36TFContagemResultadoExecucao_OSCod_To)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV39TFContagemResultadoExecucao_Inicio) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Inicio] >= @AV39TFContagemResultadoExecucao_Inicio)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Inicio] >= @AV39TFContagemResultadoExecucao_Inicio)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV40TFContagemResultadoExecucao_Inicio_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Inicio] <= @AV40TFContagemResultadoExecucao_Inicio_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Inicio] <= @AV40TFContagemResultadoExecucao_Inicio_To)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (0==AV45TFContagemResultadoExecucao_PrazoDias) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_PrazoDias] >= @AV45TFContagemResultadoExecucao_PrazoDias)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_PrazoDias] >= @AV45TFContagemResultadoExecucao_PrazoDias)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! (0==AV46TFContagemResultadoExecucao_PrazoDias_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_PrazoDias] <= @AV46TFContagemResultadoExecucao_PrazoDias_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_PrazoDias] <= @AV46TFContagemResultadoExecucao_PrazoDias_To)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! (DateTime.MinValue==AV49TFContagemResultadoExecucao_Prevista) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Prevista] >= @AV49TFContagemResultadoExecucao_Prevista)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Prevista] >= @AV49TFContagemResultadoExecucao_Prevista)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV50TFContagemResultadoExecucao_Prevista_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Prevista] <= @AV50TFContagemResultadoExecucao_Prevista_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Prevista] <= @AV50TFContagemResultadoExecucao_Prevista_To)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV55TFContagemResultadoExecucao_Fim) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Fim] >= @AV55TFContagemResultadoExecucao_Fim)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Fim] >= @AV55TFContagemResultadoExecucao_Fim)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV56TFContagemResultadoExecucao_Fim_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Fim] <= @AV56TFContagemResultadoExecucao_Fim_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Fim] <= @AV56TFContagemResultadoExecucao_Fim_To)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( ! (0==AV61TFContagemResultadoExecucao_Dias) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Dias] >= @AV61TFContagemResultadoExecucao_Dias)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Dias] >= @AV61TFContagemResultadoExecucao_Dias)";
            }
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( ! (0==AV62TFContagemResultadoExecucao_Dias_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Dias] <= @AV62TFContagemResultadoExecucao_Dias_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Dias] <= @AV62TFContagemResultadoExecucao_Dias_To)";
            }
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultadoExecucao_Inicio]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultadoExecucao_Inicio] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultadoExecucao_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultadoExecucao_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultadoExecucao_OSCod]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultadoExecucao_OSCod] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultadoExecucao_PrazoDias]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultadoExecucao_PrazoDias] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultadoExecucao_Prevista]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultadoExecucao_Prevista] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultadoExecucao_Fim]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultadoExecucao_Fim] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultadoExecucao_Dias]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultadoExecucao_Dias] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [ContagemResultadoExecucao_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00KJ3( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             DateTime AV16ContagemResultadoExecucao_Inicio1 ,
                                             DateTime AV17ContagemResultadoExecucao_Inicio_To1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             DateTime AV20ContagemResultadoExecucao_Inicio2 ,
                                             DateTime AV21ContagemResultadoExecucao_Inicio_To2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             DateTime AV24ContagemResultadoExecucao_Inicio3 ,
                                             DateTime AV25ContagemResultadoExecucao_Inicio_To3 ,
                                             int AV31TFContagemResultadoExecucao_Codigo ,
                                             int AV32TFContagemResultadoExecucao_Codigo_To ,
                                             int AV35TFContagemResultadoExecucao_OSCod ,
                                             int AV36TFContagemResultadoExecucao_OSCod_To ,
                                             DateTime AV39TFContagemResultadoExecucao_Inicio ,
                                             DateTime AV40TFContagemResultadoExecucao_Inicio_To ,
                                             short AV45TFContagemResultadoExecucao_PrazoDias ,
                                             short AV46TFContagemResultadoExecucao_PrazoDias_To ,
                                             DateTime AV49TFContagemResultadoExecucao_Prevista ,
                                             DateTime AV50TFContagemResultadoExecucao_Prevista_To ,
                                             DateTime AV55TFContagemResultadoExecucao_Fim ,
                                             DateTime AV56TFContagemResultadoExecucao_Fim_To ,
                                             short AV61TFContagemResultadoExecucao_Dias ,
                                             short AV62TFContagemResultadoExecucao_Dias_To ,
                                             DateTime A1406ContagemResultadoExecucao_Inicio ,
                                             int A1405ContagemResultadoExecucao_Codigo ,
                                             int A1404ContagemResultadoExecucao_OSCod ,
                                             short A1411ContagemResultadoExecucao_PrazoDias ,
                                             DateTime A1408ContagemResultadoExecucao_Prevista ,
                                             DateTime A1407ContagemResultadoExecucao_Fim ,
                                             short A1410ContagemResultadoExecucao_Dias ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [20] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [ContagemResultadoExecucao] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV16ContagemResultadoExecucao_Inicio1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Inicio] >= @AV16ContagemResultadoExecucao_Inicio1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Inicio] >= @AV16ContagemResultadoExecucao_Inicio1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV17ContagemResultadoExecucao_Inicio_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Inicio] <= @AV17ContagemResultadoExecucao_Inicio_To1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Inicio] <= @AV17ContagemResultadoExecucao_Inicio_To1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV20ContagemResultadoExecucao_Inicio2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Inicio] >= @AV20ContagemResultadoExecucao_Inicio2)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Inicio] >= @AV20ContagemResultadoExecucao_Inicio2)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV21ContagemResultadoExecucao_Inicio_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Inicio] <= @AV21ContagemResultadoExecucao_Inicio_To2)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Inicio] <= @AV21ContagemResultadoExecucao_Inicio_To2)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV24ContagemResultadoExecucao_Inicio3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Inicio] >= @AV24ContagemResultadoExecucao_Inicio3)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Inicio] >= @AV24ContagemResultadoExecucao_Inicio3)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTAGEMRESULTADOEXECUCAO_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV25ContagemResultadoExecucao_Inicio_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Inicio] <= @AV25ContagemResultadoExecucao_Inicio_To3)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Inicio] <= @AV25ContagemResultadoExecucao_Inicio_To3)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (0==AV31TFContagemResultadoExecucao_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Codigo] >= @AV31TFContagemResultadoExecucao_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Codigo] >= @AV31TFContagemResultadoExecucao_Codigo)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! (0==AV32TFContagemResultadoExecucao_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Codigo] <= @AV32TFContagemResultadoExecucao_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Codigo] <= @AV32TFContagemResultadoExecucao_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! (0==AV35TFContagemResultadoExecucao_OSCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_OSCod] >= @AV35TFContagemResultadoExecucao_OSCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_OSCod] >= @AV35TFContagemResultadoExecucao_OSCod)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! (0==AV36TFContagemResultadoExecucao_OSCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_OSCod] <= @AV36TFContagemResultadoExecucao_OSCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_OSCod] <= @AV36TFContagemResultadoExecucao_OSCod_To)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV39TFContagemResultadoExecucao_Inicio) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Inicio] >= @AV39TFContagemResultadoExecucao_Inicio)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Inicio] >= @AV39TFContagemResultadoExecucao_Inicio)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV40TFContagemResultadoExecucao_Inicio_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Inicio] <= @AV40TFContagemResultadoExecucao_Inicio_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Inicio] <= @AV40TFContagemResultadoExecucao_Inicio_To)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (0==AV45TFContagemResultadoExecucao_PrazoDias) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_PrazoDias] >= @AV45TFContagemResultadoExecucao_PrazoDias)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_PrazoDias] >= @AV45TFContagemResultadoExecucao_PrazoDias)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! (0==AV46TFContagemResultadoExecucao_PrazoDias_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_PrazoDias] <= @AV46TFContagemResultadoExecucao_PrazoDias_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_PrazoDias] <= @AV46TFContagemResultadoExecucao_PrazoDias_To)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! (DateTime.MinValue==AV49TFContagemResultadoExecucao_Prevista) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Prevista] >= @AV49TFContagemResultadoExecucao_Prevista)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Prevista] >= @AV49TFContagemResultadoExecucao_Prevista)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV50TFContagemResultadoExecucao_Prevista_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Prevista] <= @AV50TFContagemResultadoExecucao_Prevista_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Prevista] <= @AV50TFContagemResultadoExecucao_Prevista_To)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV55TFContagemResultadoExecucao_Fim) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Fim] >= @AV55TFContagemResultadoExecucao_Fim)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Fim] >= @AV55TFContagemResultadoExecucao_Fim)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV56TFContagemResultadoExecucao_Fim_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Fim] <= @AV56TFContagemResultadoExecucao_Fim_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Fim] <= @AV56TFContagemResultadoExecucao_Fim_To)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( ! (0==AV61TFContagemResultadoExecucao_Dias) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Dias] >= @AV61TFContagemResultadoExecucao_Dias)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Dias] >= @AV61TFContagemResultadoExecucao_Dias)";
            }
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( ! (0==AV62TFContagemResultadoExecucao_Dias_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoExecucao_Dias] <= @AV62TFContagemResultadoExecucao_Dias_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoExecucao_Dias] <= @AV62TFContagemResultadoExecucao_Dias_To)";
            }
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00KJ2(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (short)dynConstraints[17] , (short)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (short)dynConstraints[23] , (short)dynConstraints[24] , (DateTime)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (short)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (short)dynConstraints[31] , (short)dynConstraints[32] , (bool)dynConstraints[33] );
               case 1 :
                     return conditional_H00KJ3(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (short)dynConstraints[17] , (short)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (short)dynConstraints[23] , (short)dynConstraints[24] , (DateTime)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (short)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (short)dynConstraints[31] , (short)dynConstraints[32] , (bool)dynConstraints[33] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00KJ2 ;
          prmH00KJ2 = new Object[] {
          new Object[] {"@AV16ContagemResultadoExecucao_Inicio1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV17ContagemResultadoExecucao_Inicio_To1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV20ContagemResultadoExecucao_Inicio2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV21ContagemResultadoExecucao_Inicio_To2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV24ContagemResultadoExecucao_Inicio3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV25ContagemResultadoExecucao_Inicio_To3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV31TFContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32TFContagemResultadoExecucao_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35TFContagemResultadoExecucao_OSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36TFContagemResultadoExecucao_OSCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV39TFContagemResultadoExecucao_Inicio",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV40TFContagemResultadoExecucao_Inicio_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV45TFContagemResultadoExecucao_PrazoDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV46TFContagemResultadoExecucao_PrazoDias_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV49TFContagemResultadoExecucao_Prevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV50TFContagemResultadoExecucao_Prevista_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV55TFContagemResultadoExecucao_Fim",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV56TFContagemResultadoExecucao_Fim_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV61TFContagemResultadoExecucao_Dias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV62TFContagemResultadoExecucao_Dias_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00KJ3 ;
          prmH00KJ3 = new Object[] {
          new Object[] {"@AV16ContagemResultadoExecucao_Inicio1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV17ContagemResultadoExecucao_Inicio_To1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV20ContagemResultadoExecucao_Inicio2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV21ContagemResultadoExecucao_Inicio_To2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV24ContagemResultadoExecucao_Inicio3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV25ContagemResultadoExecucao_Inicio_To3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV31TFContagemResultadoExecucao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32TFContagemResultadoExecucao_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35TFContagemResultadoExecucao_OSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36TFContagemResultadoExecucao_OSCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV39TFContagemResultadoExecucao_Inicio",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV40TFContagemResultadoExecucao_Inicio_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV45TFContagemResultadoExecucao_PrazoDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV46TFContagemResultadoExecucao_PrazoDias_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV49TFContagemResultadoExecucao_Prevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV50TFContagemResultadoExecucao_Prevista_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV55TFContagemResultadoExecucao_Fim",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV56TFContagemResultadoExecucao_Fim_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV61TFContagemResultadoExecucao_Dias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV62TFContagemResultadoExecucao_Dias_To",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00KJ2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KJ2,11,0,true,false )
             ,new CursorDef("H00KJ3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KJ3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[4])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((short[]) buf[6])[0] = rslt.getShort(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[8])[0] = rslt.getGXDateTime(5) ;
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[20]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[21]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[22]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[32]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[33]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[38]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[39]);
                }
                return;
       }
    }

 }

}
