/*
               File: ContagemItemAtributosFSoftware
        Description: Contagem Item Atributos FSoftware
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:0:5.74
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemitematributosfsoftware : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_2") == 0 )
         {
            A224ContagemItem_Lancamento = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_2( A224ContagemItem_Lancamento) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_3") == 0 )
         {
            A379ContagemItem_AtributosCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A379ContagemItem_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A379ContagemItem_AtributosCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_3( A379ContagemItem_AtributosCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_4") == 0 )
         {
            A381ContagemItem_AtrTabelaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n381ContagemItem_AtrTabelaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A381ContagemItem_AtrTabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A381ContagemItem_AtrTabelaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_4( A381ContagemItem_AtrTabelaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contagem Item Atributos FSoftware", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContagemItem_Lancamento_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contagemitematributosfsoftware( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemitematributosfsoftware( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_1861( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1861e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1861( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_1861( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_1861e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Contagem Item Atributos FSoftware", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_ContagemItemAtributosFSoftware.htm");
            wb_table3_28_1861( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_1861e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1861e( true) ;
         }
         else
         {
            wb_table1_2_1861e( false) ;
         }
      }

      protected void wb_table3_28_1861( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_1861( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_1861e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemItemAtributosFSoftware.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemItemAtributosFSoftware.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemItemAtributosFSoftware.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_1861e( true) ;
         }
         else
         {
            wb_table3_28_1861e( false) ;
         }
      }

      protected void wb_table4_34_1861( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_lancamento_Internalname, "Item do Lan�amento", "", "", lblTextblockcontagemitem_lancamento_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemItemAtributosFSoftware.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemItem_Lancamento_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A224ContagemItem_Lancamento), 6, 0, ",", "")), ((edtContagemItem_Lancamento_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A224ContagemItem_Lancamento), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A224ContagemItem_Lancamento), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_Lancamento_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemItem_Lancamento_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItemAtributosFSoftware.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_atributoscod_Internalname, "Atributo", "", "", lblTextblockcontagemitem_atributoscod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemItemAtributosFSoftware.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemItem_AtributosCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A379ContagemItem_AtributosCod), 6, 0, ",", "")), ((edtContagemItem_AtributosCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A379ContagemItem_AtributosCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A379ContagemItem_AtributosCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_AtributosCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemItem_AtributosCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItemAtributosFSoftware.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_atributosnom_Internalname, "Atributo", "", "", lblTextblockcontagemitem_atributosnom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemItemAtributosFSoftware.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_AtributosNom_Internalname, StringUtil.RTrim( A380ContagemItem_AtributosNom), StringUtil.RTrim( context.localUtil.Format( A380ContagemItem_AtributosNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_AtributosNom_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemItem_AtributosNom_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContagemItemAtributosFSoftware.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_atrtabelacod_Internalname, "Tabela Cod", "", "", lblTextblockcontagemitem_atrtabelacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemItemAtributosFSoftware.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_AtrTabelaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A381ContagemItem_AtrTabelaCod), 6, 0, ",", "")), ((edtContagemItem_AtrTabelaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A381ContagemItem_AtrTabelaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A381ContagemItem_AtrTabelaCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_AtrTabelaCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemItem_AtrTabelaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItemAtributosFSoftware.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_atrtabelanom_Internalname, "Tabela Nom", "", "", lblTextblockcontagemitem_atrtabelanom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemItemAtributosFSoftware.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_AtrTabelaNom_Internalname, StringUtil.RTrim( A382ContagemItem_AtrTabelaNom), StringUtil.RTrim( context.localUtil.Format( A382ContagemItem_AtrTabelaNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_AtrTabelaNom_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemItem_AtrTabelaNom_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContagemItemAtributosFSoftware.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_1861e( true) ;
         }
         else
         {
            wb_table4_34_1861e( false) ;
         }
      }

      protected void wb_table2_5_1861( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFSoftware.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFSoftware.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFSoftware.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFSoftware.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFSoftware.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFSoftware.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFSoftware.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFSoftware.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFSoftware.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFSoftware.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFSoftware.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFSoftware.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFSoftware.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFSoftware.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFSoftware.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemItemAtributosFSoftware.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_1861e( true) ;
         }
         else
         {
            wb_table2_5_1861e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemItem_Lancamento_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemItem_Lancamento_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMITEM_LANCAMENTO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemItem_Lancamento_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A224ContagemItem_Lancamento = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
               }
               else
               {
                  A224ContagemItem_Lancamento = (int)(context.localUtil.CToN( cgiGet( edtContagemItem_Lancamento_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemItem_AtributosCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemItem_AtributosCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMITEM_ATRIBUTOSCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagemItem_AtributosCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A379ContagemItem_AtributosCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A379ContagemItem_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A379ContagemItem_AtributosCod), 6, 0)));
               }
               else
               {
                  A379ContagemItem_AtributosCod = (int)(context.localUtil.CToN( cgiGet( edtContagemItem_AtributosCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A379ContagemItem_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A379ContagemItem_AtributosCod), 6, 0)));
               }
               A380ContagemItem_AtributosNom = StringUtil.Upper( cgiGet( edtContagemItem_AtributosNom_Internalname));
               n380ContagemItem_AtributosNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A380ContagemItem_AtributosNom", A380ContagemItem_AtributosNom);
               A381ContagemItem_AtrTabelaCod = (int)(context.localUtil.CToN( cgiGet( edtContagemItem_AtrTabelaCod_Internalname), ",", "."));
               n381ContagemItem_AtrTabelaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A381ContagemItem_AtrTabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A381ContagemItem_AtrTabelaCod), 6, 0)));
               A382ContagemItem_AtrTabelaNom = StringUtil.Upper( cgiGet( edtContagemItem_AtrTabelaNom_Internalname));
               n382ContagemItem_AtrTabelaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A382ContagemItem_AtrTabelaNom", A382ContagemItem_AtrTabelaNom);
               /* Read saved values. */
               Z224ContagemItem_Lancamento = (int)(context.localUtil.CToN( cgiGet( "Z224ContagemItem_Lancamento"), ",", "."));
               Z379ContagemItem_AtributosCod = (int)(context.localUtil.CToN( cgiGet( "Z379ContagemItem_AtributosCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A224ContagemItem_Lancamento = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
                  A379ContagemItem_AtributosCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A379ContagemItem_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A379ContagemItem_AtributosCod), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1861( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes1861( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption180( )
      {
      }

      protected void ZM1861( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
            }
            else
            {
            }
         }
         if ( GX_JID == -1 )
         {
            Z224ContagemItem_Lancamento = A224ContagemItem_Lancamento;
            Z379ContagemItem_AtributosCod = A379ContagemItem_AtributosCod;
            Z380ContagemItem_AtributosNom = A380ContagemItem_AtributosNom;
            Z381ContagemItem_AtrTabelaCod = A381ContagemItem_AtrTabelaCod;
            Z382ContagemItem_AtrTabelaNom = A382ContagemItem_AtrTabelaNom;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load1861( )
      {
         /* Using cursor T00187 */
         pr_default.execute(5, new Object[] {A224ContagemItem_Lancamento, A379ContagemItem_AtributosCod});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound61 = 1;
            A380ContagemItem_AtributosNom = T00187_A380ContagemItem_AtributosNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A380ContagemItem_AtributosNom", A380ContagemItem_AtributosNom);
            n380ContagemItem_AtributosNom = T00187_n380ContagemItem_AtributosNom[0];
            A382ContagemItem_AtrTabelaNom = T00187_A382ContagemItem_AtrTabelaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A382ContagemItem_AtrTabelaNom", A382ContagemItem_AtrTabelaNom);
            n382ContagemItem_AtrTabelaNom = T00187_n382ContagemItem_AtrTabelaNom[0];
            A381ContagemItem_AtrTabelaCod = T00187_A381ContagemItem_AtrTabelaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A381ContagemItem_AtrTabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A381ContagemItem_AtrTabelaCod), 6, 0)));
            n381ContagemItem_AtrTabelaCod = T00187_n381ContagemItem_AtrTabelaCod[0];
            ZM1861( -1) ;
         }
         pr_default.close(5);
         OnLoadActions1861( ) ;
      }

      protected void OnLoadActions1861( )
      {
      }

      protected void CheckExtendedTable1861( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T00184 */
         pr_default.execute(2, new Object[] {A224ContagemItem_Lancamento});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Item'.", "ForeignKeyNotFound", 1, "CONTAGEMITEM_LANCAMENTO");
            AnyError = 1;
            GX_FocusControl = edtContagemItem_Lancamento_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
         /* Using cursor T00185 */
         pr_default.execute(3, new Object[] {A379ContagemItem_AtributosCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Item_Atributos'.", "ForeignKeyNotFound", 1, "CONTAGEMITEM_ATRIBUTOSCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemItem_AtributosCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A380ContagemItem_AtributosNom = T00185_A380ContagemItem_AtributosNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A380ContagemItem_AtributosNom", A380ContagemItem_AtributosNom);
         n380ContagemItem_AtributosNom = T00185_n380ContagemItem_AtributosNom[0];
         A381ContagemItem_AtrTabelaCod = T00185_A381ContagemItem_AtrTabelaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A381ContagemItem_AtrTabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A381ContagemItem_AtrTabelaCod), 6, 0)));
         n381ContagemItem_AtrTabelaCod = T00185_n381ContagemItem_AtrTabelaCod[0];
         pr_default.close(3);
         /* Using cursor T00186 */
         pr_default.execute(4, new Object[] {n381ContagemItem_AtrTabelaCod, A381ContagemItem_AtrTabelaCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Tabela'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A382ContagemItem_AtrTabelaNom = T00186_A382ContagemItem_AtrTabelaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A382ContagemItem_AtrTabelaNom", A382ContagemItem_AtrTabelaNom);
         n382ContagemItem_AtrTabelaNom = T00186_n382ContagemItem_AtrTabelaNom[0];
         pr_default.close(4);
      }

      protected void CloseExtendedTableCursors1861( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(4);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_2( int A224ContagemItem_Lancamento )
      {
         /* Using cursor T00188 */
         pr_default.execute(6, new Object[] {A224ContagemItem_Lancamento});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Item'.", "ForeignKeyNotFound", 1, "CONTAGEMITEM_LANCAMENTO");
            AnyError = 1;
            GX_FocusControl = edtContagemItem_Lancamento_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void gxLoad_3( int A379ContagemItem_AtributosCod )
      {
         /* Using cursor T00189 */
         pr_default.execute(7, new Object[] {A379ContagemItem_AtributosCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Item_Atributos'.", "ForeignKeyNotFound", 1, "CONTAGEMITEM_ATRIBUTOSCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemItem_AtributosCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A380ContagemItem_AtributosNom = T00189_A380ContagemItem_AtributosNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A380ContagemItem_AtributosNom", A380ContagemItem_AtributosNom);
         n380ContagemItem_AtributosNom = T00189_n380ContagemItem_AtributosNom[0];
         A381ContagemItem_AtrTabelaCod = T00189_A381ContagemItem_AtrTabelaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A381ContagemItem_AtrTabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A381ContagemItem_AtrTabelaCod), 6, 0)));
         n381ContagemItem_AtrTabelaCod = T00189_n381ContagemItem_AtrTabelaCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A380ContagemItem_AtributosNom))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A381ContagemItem_AtrTabelaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void gxLoad_4( int A381ContagemItem_AtrTabelaCod )
      {
         /* Using cursor T001810 */
         pr_default.execute(8, new Object[] {n381ContagemItem_AtrTabelaCod, A381ContagemItem_AtrTabelaCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Tabela'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A382ContagemItem_AtrTabelaNom = T001810_A382ContagemItem_AtrTabelaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A382ContagemItem_AtrTabelaNom", A382ContagemItem_AtrTabelaNom);
         n382ContagemItem_AtrTabelaNom = T001810_n382ContagemItem_AtrTabelaNom[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A382ContagemItem_AtrTabelaNom))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void GetKey1861( )
      {
         /* Using cursor T001811 */
         pr_default.execute(9, new Object[] {A224ContagemItem_Lancamento, A379ContagemItem_AtributosCod});
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound61 = 1;
         }
         else
         {
            RcdFound61 = 0;
         }
         pr_default.close(9);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00183 */
         pr_default.execute(1, new Object[] {A224ContagemItem_Lancamento, A379ContagemItem_AtributosCod});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1861( 1) ;
            RcdFound61 = 1;
            A224ContagemItem_Lancamento = T00183_A224ContagemItem_Lancamento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
            A379ContagemItem_AtributosCod = T00183_A379ContagemItem_AtributosCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A379ContagemItem_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A379ContagemItem_AtributosCod), 6, 0)));
            Z224ContagemItem_Lancamento = A224ContagemItem_Lancamento;
            Z379ContagemItem_AtributosCod = A379ContagemItem_AtributosCod;
            sMode61 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load1861( ) ;
            if ( AnyError == 1 )
            {
               RcdFound61 = 0;
               InitializeNonKey1861( ) ;
            }
            Gx_mode = sMode61;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound61 = 0;
            InitializeNonKey1861( ) ;
            sMode61 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode61;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1861( ) ;
         if ( RcdFound61 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound61 = 0;
         /* Using cursor T001812 */
         pr_default.execute(10, new Object[] {A224ContagemItem_Lancamento, A379ContagemItem_AtributosCod});
         if ( (pr_default.getStatus(10) != 101) )
         {
            while ( (pr_default.getStatus(10) != 101) && ( ( T001812_A224ContagemItem_Lancamento[0] < A224ContagemItem_Lancamento ) || ( T001812_A224ContagemItem_Lancamento[0] == A224ContagemItem_Lancamento ) && ( T001812_A379ContagemItem_AtributosCod[0] < A379ContagemItem_AtributosCod ) ) )
            {
               pr_default.readNext(10);
            }
            if ( (pr_default.getStatus(10) != 101) && ( ( T001812_A224ContagemItem_Lancamento[0] > A224ContagemItem_Lancamento ) || ( T001812_A224ContagemItem_Lancamento[0] == A224ContagemItem_Lancamento ) && ( T001812_A379ContagemItem_AtributosCod[0] > A379ContagemItem_AtributosCod ) ) )
            {
               A224ContagemItem_Lancamento = T001812_A224ContagemItem_Lancamento[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
               A379ContagemItem_AtributosCod = T001812_A379ContagemItem_AtributosCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A379ContagemItem_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A379ContagemItem_AtributosCod), 6, 0)));
               RcdFound61 = 1;
            }
         }
         pr_default.close(10);
      }

      protected void move_previous( )
      {
         RcdFound61 = 0;
         /* Using cursor T001813 */
         pr_default.execute(11, new Object[] {A224ContagemItem_Lancamento, A379ContagemItem_AtributosCod});
         if ( (pr_default.getStatus(11) != 101) )
         {
            while ( (pr_default.getStatus(11) != 101) && ( ( T001813_A224ContagemItem_Lancamento[0] > A224ContagemItem_Lancamento ) || ( T001813_A224ContagemItem_Lancamento[0] == A224ContagemItem_Lancamento ) && ( T001813_A379ContagemItem_AtributosCod[0] > A379ContagemItem_AtributosCod ) ) )
            {
               pr_default.readNext(11);
            }
            if ( (pr_default.getStatus(11) != 101) && ( ( T001813_A224ContagemItem_Lancamento[0] < A224ContagemItem_Lancamento ) || ( T001813_A224ContagemItem_Lancamento[0] == A224ContagemItem_Lancamento ) && ( T001813_A379ContagemItem_AtributosCod[0] < A379ContagemItem_AtributosCod ) ) )
            {
               A224ContagemItem_Lancamento = T001813_A224ContagemItem_Lancamento[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
               A379ContagemItem_AtributosCod = T001813_A379ContagemItem_AtributosCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A379ContagemItem_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A379ContagemItem_AtributosCod), 6, 0)));
               RcdFound61 = 1;
            }
         }
         pr_default.close(11);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1861( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContagemItem_Lancamento_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert1861( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound61 == 1 )
            {
               if ( ( A224ContagemItem_Lancamento != Z224ContagemItem_Lancamento ) || ( A379ContagemItem_AtributosCod != Z379ContagemItem_AtributosCod ) )
               {
                  A224ContagemItem_Lancamento = Z224ContagemItem_Lancamento;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
                  A379ContagemItem_AtributosCod = Z379ContagemItem_AtributosCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A379ContagemItem_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A379ContagemItem_AtributosCod), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTAGEMITEM_LANCAMENTO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemItem_Lancamento_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContagemItem_Lancamento_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update1861( ) ;
                  GX_FocusControl = edtContagemItem_Lancamento_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A224ContagemItem_Lancamento != Z224ContagemItem_Lancamento ) || ( A379ContagemItem_AtributosCod != Z379ContagemItem_AtributosCod ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtContagemItem_Lancamento_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert1861( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTAGEMITEM_LANCAMENTO");
                     AnyError = 1;
                     GX_FocusControl = edtContagemItem_Lancamento_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtContagemItem_Lancamento_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert1861( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( ( A224ContagemItem_Lancamento != Z224ContagemItem_Lancamento ) || ( A379ContagemItem_AtributosCod != Z379ContagemItem_AtributosCod ) )
         {
            A224ContagemItem_Lancamento = Z224ContagemItem_Lancamento;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
            A379ContagemItem_AtributosCod = Z379ContagemItem_AtributosCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A379ContagemItem_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A379ContagemItem_AtributosCod), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTAGEMITEM_LANCAMENTO");
            AnyError = 1;
            GX_FocusControl = edtContagemItem_Lancamento_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContagemItem_Lancamento_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound61 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "CONTAGEMITEM_LANCAMENTO");
            AnyError = 1;
            GX_FocusControl = edtContagemItem_Lancamento_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart1861( ) ;
         if ( RcdFound61 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         ScanEnd1861( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound61 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound61 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart1861( ) ;
         if ( RcdFound61 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound61 != 0 )
            {
               ScanNext1861( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         ScanEnd1861( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency1861( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00182 */
            pr_default.execute(0, new Object[] {A224ContagemItem_Lancamento, A379ContagemItem_AtributosCod});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemItemAtributosFSoftware"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemItemAtributosFSoftware"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1861( )
      {
         BeforeValidate1861( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1861( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1861( 0) ;
            CheckOptimisticConcurrency1861( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1861( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1861( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001814 */
                     pr_default.execute(12, new Object[] {A224ContagemItem_Lancamento, A379ContagemItem_AtributosCod});
                     pr_default.close(12);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemItemAtributosFSoftware") ;
                     if ( (pr_default.getStatus(12) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption180( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1861( ) ;
            }
            EndLevel1861( ) ;
         }
         CloseExtendedTableCursors1861( ) ;
      }

      protected void Update1861( )
      {
         BeforeValidate1861( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1861( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1861( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1861( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1861( ) ;
                  if ( AnyError == 0 )
                  {
                     /* No attributes to update on table [ContagemItemAtributosFSoftware] */
                     DeferredUpdate1861( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption180( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1861( ) ;
         }
         CloseExtendedTableCursors1861( ) ;
      }

      protected void DeferredUpdate1861( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate1861( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1861( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1861( ) ;
            AfterConfirm1861( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1861( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001815 */
                  pr_default.execute(13, new Object[] {A224ContagemItem_Lancamento, A379ContagemItem_AtributosCod});
                  pr_default.close(13);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemItemAtributosFSoftware") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound61 == 0 )
                        {
                           InitAll1861( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption180( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode61 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel1861( ) ;
         Gx_mode = sMode61;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls1861( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T001816 */
            pr_default.execute(14, new Object[] {A379ContagemItem_AtributosCod});
            A380ContagemItem_AtributosNom = T001816_A380ContagemItem_AtributosNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A380ContagemItem_AtributosNom", A380ContagemItem_AtributosNom);
            n380ContagemItem_AtributosNom = T001816_n380ContagemItem_AtributosNom[0];
            A381ContagemItem_AtrTabelaCod = T001816_A381ContagemItem_AtrTabelaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A381ContagemItem_AtrTabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A381ContagemItem_AtrTabelaCod), 6, 0)));
            n381ContagemItem_AtrTabelaCod = T001816_n381ContagemItem_AtrTabelaCod[0];
            pr_default.close(14);
            /* Using cursor T001817 */
            pr_default.execute(15, new Object[] {n381ContagemItem_AtrTabelaCod, A381ContagemItem_AtrTabelaCod});
            A382ContagemItem_AtrTabelaNom = T001817_A382ContagemItem_AtrTabelaNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A382ContagemItem_AtrTabelaNom", A382ContagemItem_AtrTabelaNom);
            n382ContagemItem_AtrTabelaNom = T001817_n382ContagemItem_AtrTabelaNom[0];
            pr_default.close(15);
         }
      }

      protected void EndLevel1861( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1861( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(14);
            pr_default.close(15);
            context.CommitDataStores( "ContagemItemAtributosFSoftware");
            if ( AnyError == 0 )
            {
               ConfirmValues180( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(14);
            pr_default.close(15);
            context.RollbackDataStores( "ContagemItemAtributosFSoftware");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1861( )
      {
         /* Using cursor T001818 */
         pr_default.execute(16);
         RcdFound61 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound61 = 1;
            A224ContagemItem_Lancamento = T001818_A224ContagemItem_Lancamento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
            A379ContagemItem_AtributosCod = T001818_A379ContagemItem_AtributosCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A379ContagemItem_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A379ContagemItem_AtributosCod), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1861( )
      {
         /* Scan next routine */
         pr_default.readNext(16);
         RcdFound61 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound61 = 1;
            A224ContagemItem_Lancamento = T001818_A224ContagemItem_Lancamento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
            A379ContagemItem_AtributosCod = T001818_A379ContagemItem_AtributosCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A379ContagemItem_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A379ContagemItem_AtributosCod), 6, 0)));
         }
      }

      protected void ScanEnd1861( )
      {
         pr_default.close(16);
      }

      protected void AfterConfirm1861( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1861( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1861( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1861( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1861( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1861( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1861( )
      {
         edtContagemItem_Lancamento_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_Lancamento_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_Lancamento_Enabled), 5, 0)));
         edtContagemItem_AtributosCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_AtributosCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_AtributosCod_Enabled), 5, 0)));
         edtContagemItem_AtributosNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_AtributosNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_AtributosNom_Enabled), 5, 0)));
         edtContagemItem_AtrTabelaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_AtrTabelaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_AtrTabelaCod_Enabled), 5, 0)));
         edtContagemItem_AtrTabelaNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemItem_AtrTabelaNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_AtrTabelaNom_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues180( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020428230725");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemitematributosfsoftware.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z224ContagemItem_Lancamento), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z379ContagemItem_AtributosCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z379ContagemItem_AtributosCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contagemitematributosfsoftware.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "ContagemItemAtributosFSoftware" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Item Atributos FSoftware" ;
      }

      protected void InitializeNonKey1861( )
      {
         A380ContagemItem_AtributosNom = "";
         n380ContagemItem_AtributosNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A380ContagemItem_AtributosNom", A380ContagemItem_AtributosNom);
         A381ContagemItem_AtrTabelaCod = 0;
         n381ContagemItem_AtrTabelaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A381ContagemItem_AtrTabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A381ContagemItem_AtrTabelaCod), 6, 0)));
         A382ContagemItem_AtrTabelaNom = "";
         n382ContagemItem_AtrTabelaNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A382ContagemItem_AtrTabelaNom", A382ContagemItem_AtrTabelaNom);
      }

      protected void InitAll1861( )
      {
         A224ContagemItem_Lancamento = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
         A379ContagemItem_AtributosCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A379ContagemItem_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A379ContagemItem_AtributosCod), 6, 0)));
         InitializeNonKey1861( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020428230729");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contagemitematributosfsoftware.js", "?2020428230730");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockcontagemitem_lancamento_Internalname = "TEXTBLOCKCONTAGEMITEM_LANCAMENTO";
         edtContagemItem_Lancamento_Internalname = "CONTAGEMITEM_LANCAMENTO";
         lblTextblockcontagemitem_atributoscod_Internalname = "TEXTBLOCKCONTAGEMITEM_ATRIBUTOSCOD";
         edtContagemItem_AtributosCod_Internalname = "CONTAGEMITEM_ATRIBUTOSCOD";
         lblTextblockcontagemitem_atributosnom_Internalname = "TEXTBLOCKCONTAGEMITEM_ATRIBUTOSNOM";
         edtContagemItem_AtributosNom_Internalname = "CONTAGEMITEM_ATRIBUTOSNOM";
         lblTextblockcontagemitem_atrtabelacod_Internalname = "TEXTBLOCKCONTAGEMITEM_ATRTABELACOD";
         edtContagemItem_AtrTabelaCod_Internalname = "CONTAGEMITEM_ATRTABELACOD";
         lblTextblockcontagemitem_atrtabelanom_Internalname = "TEXTBLOCKCONTAGEMITEM_ATRTABELANOM";
         edtContagemItem_AtrTabelaNom_Internalname = "CONTAGEMITEM_ATRTABELANOM";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contagem Item Atributos FSoftware";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtContagemItem_AtrTabelaNom_Jsonclick = "";
         edtContagemItem_AtrTabelaNom_Enabled = 0;
         edtContagemItem_AtrTabelaCod_Jsonclick = "";
         edtContagemItem_AtrTabelaCod_Enabled = 0;
         edtContagemItem_AtributosNom_Jsonclick = "";
         edtContagemItem_AtributosNom_Enabled = 0;
         edtContagemItem_AtributosCod_Jsonclick = "";
         edtContagemItem_AtributosCod_Enabled = 1;
         edtContagemItem_Lancamento_Jsonclick = "";
         edtContagemItem_Lancamento_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         /* Using cursor T001819 */
         pr_default.execute(17, new Object[] {A224ContagemItem_Lancamento});
         if ( (pr_default.getStatus(17) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Item'.", "ForeignKeyNotFound", 1, "CONTAGEMITEM_LANCAMENTO");
            AnyError = 1;
            GX_FocusControl = edtContagemItem_Lancamento_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(17);
         /* Using cursor T001816 */
         pr_default.execute(14, new Object[] {A379ContagemItem_AtributosCod});
         if ( (pr_default.getStatus(14) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Item_Atributos'.", "ForeignKeyNotFound", 1, "CONTAGEMITEM_ATRIBUTOSCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemItem_AtributosCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A380ContagemItem_AtributosNom = T001816_A380ContagemItem_AtributosNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A380ContagemItem_AtributosNom", A380ContagemItem_AtributosNom);
         n380ContagemItem_AtributosNom = T001816_n380ContagemItem_AtributosNom[0];
         A381ContagemItem_AtrTabelaCod = T001816_A381ContagemItem_AtrTabelaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A381ContagemItem_AtrTabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A381ContagemItem_AtrTabelaCod), 6, 0)));
         n381ContagemItem_AtrTabelaCod = T001816_n381ContagemItem_AtrTabelaCod[0];
         pr_default.close(14);
         /* Using cursor T001817 */
         pr_default.execute(15, new Object[] {n381ContagemItem_AtrTabelaCod, A381ContagemItem_AtrTabelaCod});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Tabela'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A382ContagemItem_AtrTabelaNom = T001817_A382ContagemItem_AtrTabelaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A382ContagemItem_AtrTabelaNom", A382ContagemItem_AtrTabelaNom);
         n382ContagemItem_AtrTabelaNom = T001817_n382ContagemItem_AtrTabelaNom[0];
         pr_default.close(15);
         if ( AnyError == 0 )
         {
            GX_FocusControl = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Contagemitem_lancamento( int GX_Parm1 )
      {
         A224ContagemItem_Lancamento = GX_Parm1;
         /* Using cursor T001819 */
         pr_default.execute(17, new Object[] {A224ContagemItem_Lancamento});
         if ( (pr_default.getStatus(17) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Item'.", "ForeignKeyNotFound", 1, "CONTAGEMITEM_LANCAMENTO");
            AnyError = 1;
            GX_FocusControl = edtContagemItem_Lancamento_Internalname;
         }
         pr_default.close(17);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemitem_atributoscod( int GX_Parm1 ,
                                                   int GX_Parm2 ,
                                                   int GX_Parm3 ,
                                                   String GX_Parm4 ,
                                                   String GX_Parm5 )
      {
         A224ContagemItem_Lancamento = GX_Parm1;
         A379ContagemItem_AtributosCod = GX_Parm2;
         A381ContagemItem_AtrTabelaCod = GX_Parm3;
         n381ContagemItem_AtrTabelaCod = false;
         A380ContagemItem_AtributosNom = GX_Parm4;
         n380ContagemItem_AtributosNom = false;
         A382ContagemItem_AtrTabelaNom = GX_Parm5;
         n382ContagemItem_AtrTabelaNom = false;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         /* Using cursor T001816 */
         pr_default.execute(14, new Object[] {A379ContagemItem_AtributosCod});
         if ( (pr_default.getStatus(14) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Item_Atributos'.", "ForeignKeyNotFound", 1, "CONTAGEMITEM_ATRIBUTOSCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemItem_AtributosCod_Internalname;
         }
         A380ContagemItem_AtributosNom = T001816_A380ContagemItem_AtributosNom[0];
         n380ContagemItem_AtributosNom = T001816_n380ContagemItem_AtributosNom[0];
         A381ContagemItem_AtrTabelaCod = T001816_A381ContagemItem_AtrTabelaCod[0];
         n381ContagemItem_AtrTabelaCod = T001816_n381ContagemItem_AtrTabelaCod[0];
         pr_default.close(14);
         /* Using cursor T001817 */
         pr_default.execute(15, new Object[] {n381ContagemItem_AtrTabelaCod, A381ContagemItem_AtrTabelaCod});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Tabela'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A382ContagemItem_AtrTabelaNom = T001817_A382ContagemItem_AtrTabelaNom[0];
         n382ContagemItem_AtrTabelaNom = T001817_n382ContagemItem_AtrTabelaNom[0];
         pr_default.close(15);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A380ContagemItem_AtributosNom = "";
            n380ContagemItem_AtributosNom = false;
            A381ContagemItem_AtrTabelaCod = 0;
            n381ContagemItem_AtrTabelaCod = false;
            A382ContagemItem_AtrTabelaNom = "";
            n382ContagemItem_AtrTabelaNom = false;
         }
         isValidOutput.Add(StringUtil.RTrim( A380ContagemItem_AtributosNom));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A381ContagemItem_AtrTabelaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A382ContagemItem_AtrTabelaNom));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z224ContagemItem_Lancamento), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z379ContagemItem_AtributosCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.RTrim( Z380ContagemItem_AtributosNom));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z381ContagemItem_AtrTabelaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( Z382ContagemItem_AtrTabelaNom));
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(17);
         pr_default.close(14);
         pr_default.close(15);
      }

      public override void initialize( )
      {
         sPrefix = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockcontagemitem_lancamento_Jsonclick = "";
         lblTextblockcontagemitem_atributoscod_Jsonclick = "";
         lblTextblockcontagemitem_atributosnom_Jsonclick = "";
         A380ContagemItem_AtributosNom = "";
         lblTextblockcontagemitem_atrtabelacod_Jsonclick = "";
         lblTextblockcontagemitem_atrtabelanom_Jsonclick = "";
         A382ContagemItem_AtrTabelaNom = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z380ContagemItem_AtributosNom = "";
         Z382ContagemItem_AtrTabelaNom = "";
         T00187_A380ContagemItem_AtributosNom = new String[] {""} ;
         T00187_n380ContagemItem_AtributosNom = new bool[] {false} ;
         T00187_A382ContagemItem_AtrTabelaNom = new String[] {""} ;
         T00187_n382ContagemItem_AtrTabelaNom = new bool[] {false} ;
         T00187_A224ContagemItem_Lancamento = new int[1] ;
         T00187_A379ContagemItem_AtributosCod = new int[1] ;
         T00187_A381ContagemItem_AtrTabelaCod = new int[1] ;
         T00187_n381ContagemItem_AtrTabelaCod = new bool[] {false} ;
         T00184_A224ContagemItem_Lancamento = new int[1] ;
         T00185_A380ContagemItem_AtributosNom = new String[] {""} ;
         T00185_n380ContagemItem_AtributosNom = new bool[] {false} ;
         T00185_A381ContagemItem_AtrTabelaCod = new int[1] ;
         T00185_n381ContagemItem_AtrTabelaCod = new bool[] {false} ;
         T00186_A382ContagemItem_AtrTabelaNom = new String[] {""} ;
         T00186_n382ContagemItem_AtrTabelaNom = new bool[] {false} ;
         T00188_A224ContagemItem_Lancamento = new int[1] ;
         T00189_A380ContagemItem_AtributosNom = new String[] {""} ;
         T00189_n380ContagemItem_AtributosNom = new bool[] {false} ;
         T00189_A381ContagemItem_AtrTabelaCod = new int[1] ;
         T00189_n381ContagemItem_AtrTabelaCod = new bool[] {false} ;
         T001810_A382ContagemItem_AtrTabelaNom = new String[] {""} ;
         T001810_n382ContagemItem_AtrTabelaNom = new bool[] {false} ;
         T001811_A224ContagemItem_Lancamento = new int[1] ;
         T001811_A379ContagemItem_AtributosCod = new int[1] ;
         T00183_A224ContagemItem_Lancamento = new int[1] ;
         T00183_A379ContagemItem_AtributosCod = new int[1] ;
         sMode61 = "";
         T001812_A224ContagemItem_Lancamento = new int[1] ;
         T001812_A379ContagemItem_AtributosCod = new int[1] ;
         T001813_A224ContagemItem_Lancamento = new int[1] ;
         T001813_A379ContagemItem_AtributosCod = new int[1] ;
         T00182_A224ContagemItem_Lancamento = new int[1] ;
         T00182_A379ContagemItem_AtributosCod = new int[1] ;
         T001816_A380ContagemItem_AtributosNom = new String[] {""} ;
         T001816_n380ContagemItem_AtributosNom = new bool[] {false} ;
         T001816_A381ContagemItem_AtrTabelaCod = new int[1] ;
         T001816_n381ContagemItem_AtrTabelaCod = new bool[] {false} ;
         T001817_A382ContagemItem_AtrTabelaNom = new String[] {""} ;
         T001817_n382ContagemItem_AtrTabelaNom = new bool[] {false} ;
         T001818_A224ContagemItem_Lancamento = new int[1] ;
         T001818_A379ContagemItem_AtributosCod = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         T001819_A224ContagemItem_Lancamento = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemitematributosfsoftware__default(),
            new Object[][] {
                new Object[] {
               T00182_A224ContagemItem_Lancamento, T00182_A379ContagemItem_AtributosCod
               }
               , new Object[] {
               T00183_A224ContagemItem_Lancamento, T00183_A379ContagemItem_AtributosCod
               }
               , new Object[] {
               T00184_A224ContagemItem_Lancamento
               }
               , new Object[] {
               T00185_A380ContagemItem_AtributosNom, T00185_n380ContagemItem_AtributosNom, T00185_A381ContagemItem_AtrTabelaCod, T00185_n381ContagemItem_AtrTabelaCod
               }
               , new Object[] {
               T00186_A382ContagemItem_AtrTabelaNom, T00186_n382ContagemItem_AtrTabelaNom
               }
               , new Object[] {
               T00187_A380ContagemItem_AtributosNom, T00187_n380ContagemItem_AtributosNom, T00187_A382ContagemItem_AtrTabelaNom, T00187_n382ContagemItem_AtrTabelaNom, T00187_A224ContagemItem_Lancamento, T00187_A379ContagemItem_AtributosCod, T00187_A381ContagemItem_AtrTabelaCod, T00187_n381ContagemItem_AtrTabelaCod
               }
               , new Object[] {
               T00188_A224ContagemItem_Lancamento
               }
               , new Object[] {
               T00189_A380ContagemItem_AtributosNom, T00189_n380ContagemItem_AtributosNom, T00189_A381ContagemItem_AtrTabelaCod, T00189_n381ContagemItem_AtrTabelaCod
               }
               , new Object[] {
               T001810_A382ContagemItem_AtrTabelaNom, T001810_n382ContagemItem_AtrTabelaNom
               }
               , new Object[] {
               T001811_A224ContagemItem_Lancamento, T001811_A379ContagemItem_AtributosCod
               }
               , new Object[] {
               T001812_A224ContagemItem_Lancamento, T001812_A379ContagemItem_AtributosCod
               }
               , new Object[] {
               T001813_A224ContagemItem_Lancamento, T001813_A379ContagemItem_AtributosCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001816_A380ContagemItem_AtributosNom, T001816_n380ContagemItem_AtributosNom, T001816_A381ContagemItem_AtrTabelaCod, T001816_n381ContagemItem_AtrTabelaCod
               }
               , new Object[] {
               T001817_A382ContagemItem_AtrTabelaNom, T001817_n382ContagemItem_AtrTabelaNom
               }
               , new Object[] {
               T001818_A224ContagemItem_Lancamento, T001818_A379ContagemItem_AtributosCod
               }
               , new Object[] {
               T001819_A224ContagemItem_Lancamento
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound61 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z224ContagemItem_Lancamento ;
      private int Z379ContagemItem_AtributosCod ;
      private int A224ContagemItem_Lancamento ;
      private int A379ContagemItem_AtributosCod ;
      private int A381ContagemItem_AtrTabelaCod ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int edtContagemItem_Lancamento_Enabled ;
      private int edtContagemItem_AtributosCod_Enabled ;
      private int edtContagemItem_AtributosNom_Enabled ;
      private int edtContagemItem_AtrTabelaCod_Enabled ;
      private int edtContagemItem_AtrTabelaNom_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int Z381ContagemItem_AtrTabelaCod ;
      private int idxLst ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContagemItem_Lancamento_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockcontagemitem_lancamento_Internalname ;
      private String lblTextblockcontagemitem_lancamento_Jsonclick ;
      private String edtContagemItem_Lancamento_Jsonclick ;
      private String lblTextblockcontagemitem_atributoscod_Internalname ;
      private String lblTextblockcontagemitem_atributoscod_Jsonclick ;
      private String edtContagemItem_AtributosCod_Internalname ;
      private String edtContagemItem_AtributosCod_Jsonclick ;
      private String lblTextblockcontagemitem_atributosnom_Internalname ;
      private String lblTextblockcontagemitem_atributosnom_Jsonclick ;
      private String edtContagemItem_AtributosNom_Internalname ;
      private String A380ContagemItem_AtributosNom ;
      private String edtContagemItem_AtributosNom_Jsonclick ;
      private String lblTextblockcontagemitem_atrtabelacod_Internalname ;
      private String lblTextblockcontagemitem_atrtabelacod_Jsonclick ;
      private String edtContagemItem_AtrTabelaCod_Internalname ;
      private String edtContagemItem_AtrTabelaCod_Jsonclick ;
      private String lblTextblockcontagemitem_atrtabelanom_Internalname ;
      private String lblTextblockcontagemitem_atrtabelanom_Jsonclick ;
      private String edtContagemItem_AtrTabelaNom_Internalname ;
      private String A382ContagemItem_AtrTabelaNom ;
      private String edtContagemItem_AtrTabelaNom_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z380ContagemItem_AtributosNom ;
      private String Z382ContagemItem_AtrTabelaNom ;
      private String sMode61 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool entryPointCalled ;
      private bool n381ContagemItem_AtrTabelaCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n380ContagemItem_AtributosNom ;
      private bool n382ContagemItem_AtrTabelaNom ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] T00187_A380ContagemItem_AtributosNom ;
      private bool[] T00187_n380ContagemItem_AtributosNom ;
      private String[] T00187_A382ContagemItem_AtrTabelaNom ;
      private bool[] T00187_n382ContagemItem_AtrTabelaNom ;
      private int[] T00187_A224ContagemItem_Lancamento ;
      private int[] T00187_A379ContagemItem_AtributosCod ;
      private int[] T00187_A381ContagemItem_AtrTabelaCod ;
      private bool[] T00187_n381ContagemItem_AtrTabelaCod ;
      private int[] T00184_A224ContagemItem_Lancamento ;
      private String[] T00185_A380ContagemItem_AtributosNom ;
      private bool[] T00185_n380ContagemItem_AtributosNom ;
      private int[] T00185_A381ContagemItem_AtrTabelaCod ;
      private bool[] T00185_n381ContagemItem_AtrTabelaCod ;
      private String[] T00186_A382ContagemItem_AtrTabelaNom ;
      private bool[] T00186_n382ContagemItem_AtrTabelaNom ;
      private int[] T00188_A224ContagemItem_Lancamento ;
      private String[] T00189_A380ContagemItem_AtributosNom ;
      private bool[] T00189_n380ContagemItem_AtributosNom ;
      private int[] T00189_A381ContagemItem_AtrTabelaCod ;
      private bool[] T00189_n381ContagemItem_AtrTabelaCod ;
      private String[] T001810_A382ContagemItem_AtrTabelaNom ;
      private bool[] T001810_n382ContagemItem_AtrTabelaNom ;
      private int[] T001811_A224ContagemItem_Lancamento ;
      private int[] T001811_A379ContagemItem_AtributosCod ;
      private int[] T00183_A224ContagemItem_Lancamento ;
      private int[] T00183_A379ContagemItem_AtributosCod ;
      private int[] T001812_A224ContagemItem_Lancamento ;
      private int[] T001812_A379ContagemItem_AtributosCod ;
      private int[] T001813_A224ContagemItem_Lancamento ;
      private int[] T001813_A379ContagemItem_AtributosCod ;
      private int[] T00182_A224ContagemItem_Lancamento ;
      private int[] T00182_A379ContagemItem_AtributosCod ;
      private String[] T001816_A380ContagemItem_AtributosNom ;
      private bool[] T001816_n380ContagemItem_AtributosNom ;
      private int[] T001816_A381ContagemItem_AtrTabelaCod ;
      private bool[] T001816_n381ContagemItem_AtrTabelaCod ;
      private String[] T001817_A382ContagemItem_AtrTabelaNom ;
      private bool[] T001817_n382ContagemItem_AtrTabelaNom ;
      private int[] T001818_A224ContagemItem_Lancamento ;
      private int[] T001818_A379ContagemItem_AtributosCod ;
      private int[] T001819_A224ContagemItem_Lancamento ;
      private GXWebForm Form ;
   }

   public class contagemitematributosfsoftware__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00187 ;
          prmT00187 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00184 ;
          prmT00184 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00185 ;
          prmT00185 = new Object[] {
          new Object[] {"@ContagemItem_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00186 ;
          prmT00186 = new Object[] {
          new Object[] {"@ContagemItem_AtrTabelaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00188 ;
          prmT00188 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00189 ;
          prmT00189 = new Object[] {
          new Object[] {"@ContagemItem_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001810 ;
          prmT001810 = new Object[] {
          new Object[] {"@ContagemItem_AtrTabelaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001811 ;
          prmT001811 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00183 ;
          prmT00183 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001812 ;
          prmT001812 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001813 ;
          prmT001813 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00182 ;
          prmT00182 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001814 ;
          prmT001814 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001815 ;
          prmT001815 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemItem_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001818 ;
          prmT001818 = new Object[] {
          } ;
          Object[] prmT001819 ;
          prmT001819 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001816 ;
          prmT001816 = new Object[] {
          new Object[] {"@ContagemItem_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001817 ;
          prmT001817 = new Object[] {
          new Object[] {"@ContagemItem_AtrTabelaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00182", "SELECT [ContagemItem_Lancamento], [ContagemItem_AtributosCod] AS ContagemItem_AtributosCod FROM [ContagemItemAtributosFSoftware] WITH (UPDLOCK) WHERE [ContagemItem_Lancamento] = @ContagemItem_Lancamento AND [ContagemItem_AtributosCod] = @ContagemItem_AtributosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00182,1,0,true,false )
             ,new CursorDef("T00183", "SELECT [ContagemItem_Lancamento], [ContagemItem_AtributosCod] AS ContagemItem_AtributosCod FROM [ContagemItemAtributosFSoftware] WITH (NOLOCK) WHERE [ContagemItem_Lancamento] = @ContagemItem_Lancamento AND [ContagemItem_AtributosCod] = @ContagemItem_AtributosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00183,1,0,true,false )
             ,new CursorDef("T00184", "SELECT [ContagemItem_Lancamento] FROM [ContagemItem] WITH (NOLOCK) WHERE [ContagemItem_Lancamento] = @ContagemItem_Lancamento ",true, GxErrorMask.GX_NOMASK, false, this,prmT00184,1,0,true,false )
             ,new CursorDef("T00185", "SELECT [Atributos_Nome] AS ContagemItem_AtributosNom, [Atributos_TabelaCod] AS ContagemItem_AtrTabelaCod FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_Codigo] = @ContagemItem_AtributosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00185,1,0,true,false )
             ,new CursorDef("T00186", "SELECT [Tabela_Nome] AS ContagemItem_AtrTabelaNom FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @ContagemItem_AtrTabelaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00186,1,0,true,false )
             ,new CursorDef("T00187", "SELECT T2.[Atributos_Nome] AS ContagemItem_AtributosNom, T3.[Tabela_Nome] AS ContagemItem_AtrTabelaNom, TM1.[ContagemItem_Lancamento], TM1.[ContagemItem_AtributosCod] AS ContagemItem_AtributosCod, T2.[Atributos_TabelaCod] AS ContagemItem_AtrTabelaCod FROM (([ContagemItemAtributosFSoftware] TM1 WITH (NOLOCK) INNER JOIN [Atributos] T2 WITH (NOLOCK) ON T2.[Atributos_Codigo] = TM1.[ContagemItem_AtributosCod]) LEFT JOIN [Tabela] T3 WITH (NOLOCK) ON T3.[Tabela_Codigo] = T2.[Atributos_TabelaCod]) WHERE TM1.[ContagemItem_Lancamento] = @ContagemItem_Lancamento and TM1.[ContagemItem_AtributosCod] = @ContagemItem_AtributosCod ORDER BY TM1.[ContagemItem_Lancamento], TM1.[ContagemItem_AtributosCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00187,100,0,true,false )
             ,new CursorDef("T00188", "SELECT [ContagemItem_Lancamento] FROM [ContagemItem] WITH (NOLOCK) WHERE [ContagemItem_Lancamento] = @ContagemItem_Lancamento ",true, GxErrorMask.GX_NOMASK, false, this,prmT00188,1,0,true,false )
             ,new CursorDef("T00189", "SELECT [Atributos_Nome] AS ContagemItem_AtributosNom, [Atributos_TabelaCod] AS ContagemItem_AtrTabelaCod FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_Codigo] = @ContagemItem_AtributosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00189,1,0,true,false )
             ,new CursorDef("T001810", "SELECT [Tabela_Nome] AS ContagemItem_AtrTabelaNom FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @ContagemItem_AtrTabelaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001810,1,0,true,false )
             ,new CursorDef("T001811", "SELECT [ContagemItem_Lancamento], [ContagemItem_AtributosCod] AS ContagemItem_AtributosCod FROM [ContagemItemAtributosFSoftware] WITH (NOLOCK) WHERE [ContagemItem_Lancamento] = @ContagemItem_Lancamento AND [ContagemItem_AtributosCod] = @ContagemItem_AtributosCod  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001811,1,0,true,false )
             ,new CursorDef("T001812", "SELECT TOP 1 [ContagemItem_Lancamento], [ContagemItem_AtributosCod] AS ContagemItem_AtributosCod FROM [ContagemItemAtributosFSoftware] WITH (NOLOCK) WHERE ( [ContagemItem_Lancamento] > @ContagemItem_Lancamento or [ContagemItem_Lancamento] = @ContagemItem_Lancamento and [ContagemItem_AtributosCod] > @ContagemItem_AtributosCod) ORDER BY [ContagemItem_Lancamento], [ContagemItem_AtributosCod]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001812,1,0,true,true )
             ,new CursorDef("T001813", "SELECT TOP 1 [ContagemItem_Lancamento], [ContagemItem_AtributosCod] AS ContagemItem_AtributosCod FROM [ContagemItemAtributosFSoftware] WITH (NOLOCK) WHERE ( [ContagemItem_Lancamento] < @ContagemItem_Lancamento or [ContagemItem_Lancamento] = @ContagemItem_Lancamento and [ContagemItem_AtributosCod] < @ContagemItem_AtributosCod) ORDER BY [ContagemItem_Lancamento] DESC, [ContagemItem_AtributosCod] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001813,1,0,true,true )
             ,new CursorDef("T001814", "INSERT INTO [ContagemItemAtributosFSoftware]([ContagemItem_Lancamento], [ContagemItem_AtributosCod]) VALUES(@ContagemItem_Lancamento, @ContagemItem_AtributosCod)", GxErrorMask.GX_NOMASK,prmT001814)
             ,new CursorDef("T001815", "DELETE FROM [ContagemItemAtributosFSoftware]  WHERE [ContagemItem_Lancamento] = @ContagemItem_Lancamento AND [ContagemItem_AtributosCod] = @ContagemItem_AtributosCod", GxErrorMask.GX_NOMASK,prmT001815)
             ,new CursorDef("T001816", "SELECT [Atributos_Nome] AS ContagemItem_AtributosNom, [Atributos_TabelaCod] AS ContagemItem_AtrTabelaCod FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_Codigo] = @ContagemItem_AtributosCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001816,1,0,true,false )
             ,new CursorDef("T001817", "SELECT [Tabela_Nome] AS ContagemItem_AtrTabelaNom FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @ContagemItem_AtrTabelaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001817,1,0,true,false )
             ,new CursorDef("T001818", "SELECT [ContagemItem_Lancamento], [ContagemItem_AtributosCod] AS ContagemItem_AtributosCod FROM [ContagemItemAtributosFSoftware] WITH (NOLOCK) ORDER BY [ContagemItem_Lancamento], [ContagemItem_AtributosCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001818,100,0,true,false )
             ,new CursorDef("T001819", "SELECT [ContagemItem_Lancamento] FROM [ContagemItem] WITH (NOLOCK) WHERE [ContagemItem_Lancamento] = @ContagemItem_Lancamento ",true, GxErrorMask.GX_NOMASK, false, this,prmT001819,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
