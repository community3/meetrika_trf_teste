/*
               File: GetContratoServicos_TelasWCFilterData
        Description: Get Contrato Servicos_Telas WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:4:39.85
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getcontratoservicos_telaswcfilterdata : GXProcedure
   {
      public getcontratoservicos_telaswcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getcontratoservicos_telaswcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV22DDOName = aP0_DDOName;
         this.AV20SearchTxt = aP1_SearchTxt;
         this.AV21SearchTxtTo = aP2_SearchTxtTo;
         this.AV26OptionsJson = "" ;
         this.AV29OptionsDescJson = "" ;
         this.AV31OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV22DDOName = aP0_DDOName;
         this.AV20SearchTxt = aP1_SearchTxt;
         this.AV21SearchTxtTo = aP2_SearchTxtTo;
         this.AV26OptionsJson = "" ;
         this.AV29OptionsDescJson = "" ;
         this.AV31OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
         return AV31OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getcontratoservicos_telaswcfilterdata objgetcontratoservicos_telaswcfilterdata;
         objgetcontratoservicos_telaswcfilterdata = new getcontratoservicos_telaswcfilterdata();
         objgetcontratoservicos_telaswcfilterdata.AV22DDOName = aP0_DDOName;
         objgetcontratoservicos_telaswcfilterdata.AV20SearchTxt = aP1_SearchTxt;
         objgetcontratoservicos_telaswcfilterdata.AV21SearchTxtTo = aP2_SearchTxtTo;
         objgetcontratoservicos_telaswcfilterdata.AV26OptionsJson = "" ;
         objgetcontratoservicos_telaswcfilterdata.AV29OptionsDescJson = "" ;
         objgetcontratoservicos_telaswcfilterdata.AV31OptionIndexesJson = "" ;
         objgetcontratoservicos_telaswcfilterdata.context.SetSubmitInitialConfig(context);
         objgetcontratoservicos_telaswcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetcontratoservicos_telaswcfilterdata);
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getcontratoservicos_telaswcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV25Options = (IGxCollection)(new GxSimpleCollection());
         AV28OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV30OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV22DDOName), "DDO_CONTRATADA_PESSOANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATADA_PESSOANOMOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV22DDOName), "DDO_CONTRATOSERVICOSTELAS_TELA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSERVICOSTELAS_TELAOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV22DDOName), "DDO_CONTRATOSERVICOSTELAS_LINK") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSERVICOSTELAS_LINKOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV22DDOName), "DDO_CONTRATOSERVICOSTELAS_PARMS") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSERVICOSTELAS_PARMSOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV26OptionsJson = AV25Options.ToJSonString(false);
         AV29OptionsDescJson = AV28OptionsDesc.ToJSonString(false);
         AV31OptionIndexesJson = AV30OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV33Session.Get("ContratoServicos_TelasWCGridState"), "") == 0 )
         {
            AV35GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ContratoServicos_TelasWCGridState"), "");
         }
         else
         {
            AV35GridState.FromXml(AV33Session.Get("ContratoServicos_TelasWCGridState"), "");
         }
         AV41GXV1 = 1;
         while ( AV41GXV1 <= AV35GridState.gxTpr_Filtervalues.Count )
         {
            AV36GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV35GridState.gxTpr_Filtervalues.Item(AV41GXV1));
            if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM") == 0 )
            {
               AV10TFContratada_PessoaNom = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM_SEL") == 0 )
            {
               AV11TFContratada_PessoaNom_Sel = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_TELA") == 0 )
            {
               AV12TFContratoServicosTelas_Tela = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_TELA_SEL") == 0 )
            {
               AV13TFContratoServicosTelas_Tela_Sel = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_LINK") == 0 )
            {
               AV14TFContratoServicosTelas_Link = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_LINK_SEL") == 0 )
            {
               AV15TFContratoServicosTelas_Link_Sel = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_PARMS") == 0 )
            {
               AV16TFContratoServicosTelas_Parms = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_PARMS_SEL") == 0 )
            {
               AV17TFContratoServicosTelas_Parms_Sel = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_STATUS_SEL") == 0 )
            {
               AV18TFContratoServicosTelas_Status_SelsJson = AV36GridStateFilterValue.gxTpr_Value;
               AV19TFContratoServicosTelas_Status_Sels.FromJSonString(AV18TFContratoServicosTelas_Status_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "PARM_&CONTRATOSERVICOSTELAS_CONTRATOCOD") == 0 )
            {
               AV38ContratoServicosTelas_ContratoCod = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
            }
            AV41GXV1 = (int)(AV41GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATADA_PESSOANOMOPTIONS' Routine */
         AV10TFContratada_PessoaNom = AV20SearchTxt;
         AV11TFContratada_PessoaNom_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A932ContratoServicosTelas_Status ,
                                              AV19TFContratoServicosTelas_Status_Sels ,
                                              AV11TFContratada_PessoaNom_Sel ,
                                              AV10TFContratada_PessoaNom ,
                                              AV13TFContratoServicosTelas_Tela_Sel ,
                                              AV12TFContratoServicosTelas_Tela ,
                                              AV15TFContratoServicosTelas_Link_Sel ,
                                              AV14TFContratoServicosTelas_Link ,
                                              AV17TFContratoServicosTelas_Parms_Sel ,
                                              AV16TFContratoServicosTelas_Parms ,
                                              AV19TFContratoServicosTelas_Status_Sels.Count ,
                                              A41Contratada_PessoaNom ,
                                              A931ContratoServicosTelas_Tela ,
                                              A928ContratoServicosTelas_Link ,
                                              A929ContratoServicosTelas_Parms ,
                                              AV38ContratoServicosTelas_ContratoCod ,
                                              A926ContratoServicosTelas_ContratoCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV10TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV10TFContratada_PessoaNom), 100, "%");
         lV12TFContratoServicosTelas_Tela = StringUtil.PadR( StringUtil.RTrim( AV12TFContratoServicosTelas_Tela), 50, "%");
         lV14TFContratoServicosTelas_Link = StringUtil.Concat( StringUtil.RTrim( AV14TFContratoServicosTelas_Link), "%", "");
         lV16TFContratoServicosTelas_Parms = StringUtil.Concat( StringUtil.RTrim( AV16TFContratoServicosTelas_Parms), "%", "");
         /* Using cursor P00JX2 */
         pr_default.execute(0, new Object[] {AV38ContratoServicosTelas_ContratoCod, lV10TFContratada_PessoaNom, AV11TFContratada_PessoaNom_Sel, lV12TFContratoServicosTelas_Tela, AV13TFContratoServicosTelas_Tela_Sel, lV14TFContratoServicosTelas_Link, AV15TFContratoServicosTelas_Link_Sel, lV16TFContratoServicosTelas_Parms, AV17TFContratoServicosTelas_Parms_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKJX2 = false;
            A74Contrato_Codigo = P00JX2_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00JX2_n74Contrato_Codigo[0];
            A39Contratada_Codigo = P00JX2_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00JX2_A40Contratada_PessoaCod[0];
            A926ContratoServicosTelas_ContratoCod = P00JX2_A926ContratoServicosTelas_ContratoCod[0];
            A41Contratada_PessoaNom = P00JX2_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00JX2_n41Contratada_PessoaNom[0];
            A932ContratoServicosTelas_Status = P00JX2_A932ContratoServicosTelas_Status[0];
            n932ContratoServicosTelas_Status = P00JX2_n932ContratoServicosTelas_Status[0];
            A929ContratoServicosTelas_Parms = P00JX2_A929ContratoServicosTelas_Parms[0];
            n929ContratoServicosTelas_Parms = P00JX2_n929ContratoServicosTelas_Parms[0];
            A928ContratoServicosTelas_Link = P00JX2_A928ContratoServicosTelas_Link[0];
            A931ContratoServicosTelas_Tela = P00JX2_A931ContratoServicosTelas_Tela[0];
            A938ContratoServicosTelas_Sequencial = P00JX2_A938ContratoServicosTelas_Sequencial[0];
            A74Contrato_Codigo = P00JX2_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00JX2_n74Contrato_Codigo[0];
            A39Contratada_Codigo = P00JX2_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00JX2_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P00JX2_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00JX2_n41Contratada_PessoaNom[0];
            AV32count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00JX2_A926ContratoServicosTelas_ContratoCod[0] == A926ContratoServicosTelas_ContratoCod ) && ( StringUtil.StrCmp(P00JX2_A41Contratada_PessoaNom[0], A41Contratada_PessoaNom) == 0 ) )
            {
               BRKJX2 = false;
               A74Contrato_Codigo = P00JX2_A74Contrato_Codigo[0];
               n74Contrato_Codigo = P00JX2_n74Contrato_Codigo[0];
               A39Contratada_Codigo = P00JX2_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = P00JX2_A40Contratada_PessoaCod[0];
               A938ContratoServicosTelas_Sequencial = P00JX2_A938ContratoServicosTelas_Sequencial[0];
               A74Contrato_Codigo = P00JX2_A74Contrato_Codigo[0];
               n74Contrato_Codigo = P00JX2_n74Contrato_Codigo[0];
               A39Contratada_Codigo = P00JX2_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = P00JX2_A40Contratada_PessoaCod[0];
               AV32count = (long)(AV32count+1);
               BRKJX2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A41Contratada_PessoaNom)) )
            {
               AV24Option = A41Contratada_PessoaNom;
               AV25Options.Add(AV24Option, 0);
               AV30OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV32count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV25Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJX2 )
            {
               BRKJX2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATOSERVICOSTELAS_TELAOPTIONS' Routine */
         AV12TFContratoServicosTelas_Tela = AV20SearchTxt;
         AV13TFContratoServicosTelas_Tela_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A932ContratoServicosTelas_Status ,
                                              AV19TFContratoServicosTelas_Status_Sels ,
                                              AV11TFContratada_PessoaNom_Sel ,
                                              AV10TFContratada_PessoaNom ,
                                              AV13TFContratoServicosTelas_Tela_Sel ,
                                              AV12TFContratoServicosTelas_Tela ,
                                              AV15TFContratoServicosTelas_Link_Sel ,
                                              AV14TFContratoServicosTelas_Link ,
                                              AV17TFContratoServicosTelas_Parms_Sel ,
                                              AV16TFContratoServicosTelas_Parms ,
                                              AV19TFContratoServicosTelas_Status_Sels.Count ,
                                              A41Contratada_PessoaNom ,
                                              A931ContratoServicosTelas_Tela ,
                                              A928ContratoServicosTelas_Link ,
                                              A929ContratoServicosTelas_Parms ,
                                              AV38ContratoServicosTelas_ContratoCod ,
                                              A926ContratoServicosTelas_ContratoCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV10TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV10TFContratada_PessoaNom), 100, "%");
         lV12TFContratoServicosTelas_Tela = StringUtil.PadR( StringUtil.RTrim( AV12TFContratoServicosTelas_Tela), 50, "%");
         lV14TFContratoServicosTelas_Link = StringUtil.Concat( StringUtil.RTrim( AV14TFContratoServicosTelas_Link), "%", "");
         lV16TFContratoServicosTelas_Parms = StringUtil.Concat( StringUtil.RTrim( AV16TFContratoServicosTelas_Parms), "%", "");
         /* Using cursor P00JX3 */
         pr_default.execute(1, new Object[] {AV38ContratoServicosTelas_ContratoCod, lV10TFContratada_PessoaNom, AV11TFContratada_PessoaNom_Sel, lV12TFContratoServicosTelas_Tela, AV13TFContratoServicosTelas_Tela_Sel, lV14TFContratoServicosTelas_Link, AV15TFContratoServicosTelas_Link_Sel, lV16TFContratoServicosTelas_Parms, AV17TFContratoServicosTelas_Parms_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKJX4 = false;
            A74Contrato_Codigo = P00JX3_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00JX3_n74Contrato_Codigo[0];
            A39Contratada_Codigo = P00JX3_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00JX3_A40Contratada_PessoaCod[0];
            A926ContratoServicosTelas_ContratoCod = P00JX3_A926ContratoServicosTelas_ContratoCod[0];
            A931ContratoServicosTelas_Tela = P00JX3_A931ContratoServicosTelas_Tela[0];
            A932ContratoServicosTelas_Status = P00JX3_A932ContratoServicosTelas_Status[0];
            n932ContratoServicosTelas_Status = P00JX3_n932ContratoServicosTelas_Status[0];
            A929ContratoServicosTelas_Parms = P00JX3_A929ContratoServicosTelas_Parms[0];
            n929ContratoServicosTelas_Parms = P00JX3_n929ContratoServicosTelas_Parms[0];
            A928ContratoServicosTelas_Link = P00JX3_A928ContratoServicosTelas_Link[0];
            A41Contratada_PessoaNom = P00JX3_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00JX3_n41Contratada_PessoaNom[0];
            A938ContratoServicosTelas_Sequencial = P00JX3_A938ContratoServicosTelas_Sequencial[0];
            A74Contrato_Codigo = P00JX3_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00JX3_n74Contrato_Codigo[0];
            A39Contratada_Codigo = P00JX3_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00JX3_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P00JX3_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00JX3_n41Contratada_PessoaNom[0];
            AV32count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00JX3_A926ContratoServicosTelas_ContratoCod[0] == A926ContratoServicosTelas_ContratoCod ) && ( StringUtil.StrCmp(P00JX3_A931ContratoServicosTelas_Tela[0], A931ContratoServicosTelas_Tela) == 0 ) )
            {
               BRKJX4 = false;
               A938ContratoServicosTelas_Sequencial = P00JX3_A938ContratoServicosTelas_Sequencial[0];
               AV32count = (long)(AV32count+1);
               BRKJX4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A931ContratoServicosTelas_Tela)) )
            {
               AV24Option = A931ContratoServicosTelas_Tela;
               AV25Options.Add(AV24Option, 0);
               AV30OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV32count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV25Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJX4 )
            {
               BRKJX4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTRATOSERVICOSTELAS_LINKOPTIONS' Routine */
         AV14TFContratoServicosTelas_Link = AV20SearchTxt;
         AV15TFContratoServicosTelas_Link_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A932ContratoServicosTelas_Status ,
                                              AV19TFContratoServicosTelas_Status_Sels ,
                                              AV11TFContratada_PessoaNom_Sel ,
                                              AV10TFContratada_PessoaNom ,
                                              AV13TFContratoServicosTelas_Tela_Sel ,
                                              AV12TFContratoServicosTelas_Tela ,
                                              AV15TFContratoServicosTelas_Link_Sel ,
                                              AV14TFContratoServicosTelas_Link ,
                                              AV17TFContratoServicosTelas_Parms_Sel ,
                                              AV16TFContratoServicosTelas_Parms ,
                                              AV19TFContratoServicosTelas_Status_Sels.Count ,
                                              A41Contratada_PessoaNom ,
                                              A931ContratoServicosTelas_Tela ,
                                              A928ContratoServicosTelas_Link ,
                                              A929ContratoServicosTelas_Parms ,
                                              AV38ContratoServicosTelas_ContratoCod ,
                                              A926ContratoServicosTelas_ContratoCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV10TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV10TFContratada_PessoaNom), 100, "%");
         lV12TFContratoServicosTelas_Tela = StringUtil.PadR( StringUtil.RTrim( AV12TFContratoServicosTelas_Tela), 50, "%");
         lV14TFContratoServicosTelas_Link = StringUtil.Concat( StringUtil.RTrim( AV14TFContratoServicosTelas_Link), "%", "");
         lV16TFContratoServicosTelas_Parms = StringUtil.Concat( StringUtil.RTrim( AV16TFContratoServicosTelas_Parms), "%", "");
         /* Using cursor P00JX4 */
         pr_default.execute(2, new Object[] {AV38ContratoServicosTelas_ContratoCod, lV10TFContratada_PessoaNom, AV11TFContratada_PessoaNom_Sel, lV12TFContratoServicosTelas_Tela, AV13TFContratoServicosTelas_Tela_Sel, lV14TFContratoServicosTelas_Link, AV15TFContratoServicosTelas_Link_Sel, lV16TFContratoServicosTelas_Parms, AV17TFContratoServicosTelas_Parms_Sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKJX6 = false;
            A74Contrato_Codigo = P00JX4_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00JX4_n74Contrato_Codigo[0];
            A39Contratada_Codigo = P00JX4_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00JX4_A40Contratada_PessoaCod[0];
            A926ContratoServicosTelas_ContratoCod = P00JX4_A926ContratoServicosTelas_ContratoCod[0];
            A928ContratoServicosTelas_Link = P00JX4_A928ContratoServicosTelas_Link[0];
            A932ContratoServicosTelas_Status = P00JX4_A932ContratoServicosTelas_Status[0];
            n932ContratoServicosTelas_Status = P00JX4_n932ContratoServicosTelas_Status[0];
            A929ContratoServicosTelas_Parms = P00JX4_A929ContratoServicosTelas_Parms[0];
            n929ContratoServicosTelas_Parms = P00JX4_n929ContratoServicosTelas_Parms[0];
            A931ContratoServicosTelas_Tela = P00JX4_A931ContratoServicosTelas_Tela[0];
            A41Contratada_PessoaNom = P00JX4_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00JX4_n41Contratada_PessoaNom[0];
            A938ContratoServicosTelas_Sequencial = P00JX4_A938ContratoServicosTelas_Sequencial[0];
            A74Contrato_Codigo = P00JX4_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00JX4_n74Contrato_Codigo[0];
            A39Contratada_Codigo = P00JX4_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00JX4_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P00JX4_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00JX4_n41Contratada_PessoaNom[0];
            AV32count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( P00JX4_A926ContratoServicosTelas_ContratoCod[0] == A926ContratoServicosTelas_ContratoCod ) && ( StringUtil.StrCmp(P00JX4_A928ContratoServicosTelas_Link[0], A928ContratoServicosTelas_Link) == 0 ) )
            {
               BRKJX6 = false;
               A938ContratoServicosTelas_Sequencial = P00JX4_A938ContratoServicosTelas_Sequencial[0];
               AV32count = (long)(AV32count+1);
               BRKJX6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A928ContratoServicosTelas_Link)) )
            {
               AV24Option = A928ContratoServicosTelas_Link;
               AV25Options.Add(AV24Option, 0);
               AV30OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV32count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV25Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJX6 )
            {
               BRKJX6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADCONTRATOSERVICOSTELAS_PARMSOPTIONS' Routine */
         AV16TFContratoServicosTelas_Parms = AV20SearchTxt;
         AV17TFContratoServicosTelas_Parms_Sel = "";
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              A932ContratoServicosTelas_Status ,
                                              AV19TFContratoServicosTelas_Status_Sels ,
                                              AV11TFContratada_PessoaNom_Sel ,
                                              AV10TFContratada_PessoaNom ,
                                              AV13TFContratoServicosTelas_Tela_Sel ,
                                              AV12TFContratoServicosTelas_Tela ,
                                              AV15TFContratoServicosTelas_Link_Sel ,
                                              AV14TFContratoServicosTelas_Link ,
                                              AV17TFContratoServicosTelas_Parms_Sel ,
                                              AV16TFContratoServicosTelas_Parms ,
                                              AV19TFContratoServicosTelas_Status_Sels.Count ,
                                              A41Contratada_PessoaNom ,
                                              A931ContratoServicosTelas_Tela ,
                                              A928ContratoServicosTelas_Link ,
                                              A929ContratoServicosTelas_Parms ,
                                              AV38ContratoServicosTelas_ContratoCod ,
                                              A926ContratoServicosTelas_ContratoCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV10TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV10TFContratada_PessoaNom), 100, "%");
         lV12TFContratoServicosTelas_Tela = StringUtil.PadR( StringUtil.RTrim( AV12TFContratoServicosTelas_Tela), 50, "%");
         lV14TFContratoServicosTelas_Link = StringUtil.Concat( StringUtil.RTrim( AV14TFContratoServicosTelas_Link), "%", "");
         lV16TFContratoServicosTelas_Parms = StringUtil.Concat( StringUtil.RTrim( AV16TFContratoServicosTelas_Parms), "%", "");
         /* Using cursor P00JX5 */
         pr_default.execute(3, new Object[] {AV38ContratoServicosTelas_ContratoCod, lV10TFContratada_PessoaNom, AV11TFContratada_PessoaNom_Sel, lV12TFContratoServicosTelas_Tela, AV13TFContratoServicosTelas_Tela_Sel, lV14TFContratoServicosTelas_Link, AV15TFContratoServicosTelas_Link_Sel, lV16TFContratoServicosTelas_Parms, AV17TFContratoServicosTelas_Parms_Sel});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKJX8 = false;
            A74Contrato_Codigo = P00JX5_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00JX5_n74Contrato_Codigo[0];
            A39Contratada_Codigo = P00JX5_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00JX5_A40Contratada_PessoaCod[0];
            A926ContratoServicosTelas_ContratoCod = P00JX5_A926ContratoServicosTelas_ContratoCod[0];
            A929ContratoServicosTelas_Parms = P00JX5_A929ContratoServicosTelas_Parms[0];
            n929ContratoServicosTelas_Parms = P00JX5_n929ContratoServicosTelas_Parms[0];
            A932ContratoServicosTelas_Status = P00JX5_A932ContratoServicosTelas_Status[0];
            n932ContratoServicosTelas_Status = P00JX5_n932ContratoServicosTelas_Status[0];
            A928ContratoServicosTelas_Link = P00JX5_A928ContratoServicosTelas_Link[0];
            A931ContratoServicosTelas_Tela = P00JX5_A931ContratoServicosTelas_Tela[0];
            A41Contratada_PessoaNom = P00JX5_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00JX5_n41Contratada_PessoaNom[0];
            A938ContratoServicosTelas_Sequencial = P00JX5_A938ContratoServicosTelas_Sequencial[0];
            A74Contrato_Codigo = P00JX5_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00JX5_n74Contrato_Codigo[0];
            A39Contratada_Codigo = P00JX5_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00JX5_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P00JX5_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00JX5_n41Contratada_PessoaNom[0];
            AV32count = 0;
            while ( (pr_default.getStatus(3) != 101) && ( P00JX5_A926ContratoServicosTelas_ContratoCod[0] == A926ContratoServicosTelas_ContratoCod ) && ( StringUtil.StrCmp(P00JX5_A929ContratoServicosTelas_Parms[0], A929ContratoServicosTelas_Parms) == 0 ) )
            {
               BRKJX8 = false;
               A938ContratoServicosTelas_Sequencial = P00JX5_A938ContratoServicosTelas_Sequencial[0];
               AV32count = (long)(AV32count+1);
               BRKJX8 = true;
               pr_default.readNext(3);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A929ContratoServicosTelas_Parms)) )
            {
               AV24Option = A929ContratoServicosTelas_Parms;
               AV25Options.Add(AV24Option, 0);
               AV30OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV32count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV25Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJX8 )
            {
               BRKJX8 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV25Options = new GxSimpleCollection();
         AV28OptionsDesc = new GxSimpleCollection();
         AV30OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV33Session = context.GetSession();
         AV35GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV36GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContratada_PessoaNom = "";
         AV11TFContratada_PessoaNom_Sel = "";
         AV12TFContratoServicosTelas_Tela = "";
         AV13TFContratoServicosTelas_Tela_Sel = "";
         AV14TFContratoServicosTelas_Link = "";
         AV15TFContratoServicosTelas_Link_Sel = "";
         AV16TFContratoServicosTelas_Parms = "";
         AV17TFContratoServicosTelas_Parms_Sel = "";
         AV18TFContratoServicosTelas_Status_SelsJson = "";
         AV19TFContratoServicosTelas_Status_Sels = new GxSimpleCollection();
         scmdbuf = "";
         lV10TFContratada_PessoaNom = "";
         lV12TFContratoServicosTelas_Tela = "";
         lV14TFContratoServicosTelas_Link = "";
         lV16TFContratoServicosTelas_Parms = "";
         A932ContratoServicosTelas_Status = "";
         A41Contratada_PessoaNom = "";
         A931ContratoServicosTelas_Tela = "";
         A928ContratoServicosTelas_Link = "";
         A929ContratoServicosTelas_Parms = "";
         P00JX2_A74Contrato_Codigo = new int[1] ;
         P00JX2_n74Contrato_Codigo = new bool[] {false} ;
         P00JX2_A39Contratada_Codigo = new int[1] ;
         P00JX2_A40Contratada_PessoaCod = new int[1] ;
         P00JX2_A926ContratoServicosTelas_ContratoCod = new int[1] ;
         P00JX2_A41Contratada_PessoaNom = new String[] {""} ;
         P00JX2_n41Contratada_PessoaNom = new bool[] {false} ;
         P00JX2_A932ContratoServicosTelas_Status = new String[] {""} ;
         P00JX2_n932ContratoServicosTelas_Status = new bool[] {false} ;
         P00JX2_A929ContratoServicosTelas_Parms = new String[] {""} ;
         P00JX2_n929ContratoServicosTelas_Parms = new bool[] {false} ;
         P00JX2_A928ContratoServicosTelas_Link = new String[] {""} ;
         P00JX2_A931ContratoServicosTelas_Tela = new String[] {""} ;
         P00JX2_A938ContratoServicosTelas_Sequencial = new short[1] ;
         AV24Option = "";
         P00JX3_A74Contrato_Codigo = new int[1] ;
         P00JX3_n74Contrato_Codigo = new bool[] {false} ;
         P00JX3_A39Contratada_Codigo = new int[1] ;
         P00JX3_A40Contratada_PessoaCod = new int[1] ;
         P00JX3_A926ContratoServicosTelas_ContratoCod = new int[1] ;
         P00JX3_A931ContratoServicosTelas_Tela = new String[] {""} ;
         P00JX3_A932ContratoServicosTelas_Status = new String[] {""} ;
         P00JX3_n932ContratoServicosTelas_Status = new bool[] {false} ;
         P00JX3_A929ContratoServicosTelas_Parms = new String[] {""} ;
         P00JX3_n929ContratoServicosTelas_Parms = new bool[] {false} ;
         P00JX3_A928ContratoServicosTelas_Link = new String[] {""} ;
         P00JX3_A41Contratada_PessoaNom = new String[] {""} ;
         P00JX3_n41Contratada_PessoaNom = new bool[] {false} ;
         P00JX3_A938ContratoServicosTelas_Sequencial = new short[1] ;
         P00JX4_A74Contrato_Codigo = new int[1] ;
         P00JX4_n74Contrato_Codigo = new bool[] {false} ;
         P00JX4_A39Contratada_Codigo = new int[1] ;
         P00JX4_A40Contratada_PessoaCod = new int[1] ;
         P00JX4_A926ContratoServicosTelas_ContratoCod = new int[1] ;
         P00JX4_A928ContratoServicosTelas_Link = new String[] {""} ;
         P00JX4_A932ContratoServicosTelas_Status = new String[] {""} ;
         P00JX4_n932ContratoServicosTelas_Status = new bool[] {false} ;
         P00JX4_A929ContratoServicosTelas_Parms = new String[] {""} ;
         P00JX4_n929ContratoServicosTelas_Parms = new bool[] {false} ;
         P00JX4_A931ContratoServicosTelas_Tela = new String[] {""} ;
         P00JX4_A41Contratada_PessoaNom = new String[] {""} ;
         P00JX4_n41Contratada_PessoaNom = new bool[] {false} ;
         P00JX4_A938ContratoServicosTelas_Sequencial = new short[1] ;
         P00JX5_A74Contrato_Codigo = new int[1] ;
         P00JX5_n74Contrato_Codigo = new bool[] {false} ;
         P00JX5_A39Contratada_Codigo = new int[1] ;
         P00JX5_A40Contratada_PessoaCod = new int[1] ;
         P00JX5_A926ContratoServicosTelas_ContratoCod = new int[1] ;
         P00JX5_A929ContratoServicosTelas_Parms = new String[] {""} ;
         P00JX5_n929ContratoServicosTelas_Parms = new bool[] {false} ;
         P00JX5_A932ContratoServicosTelas_Status = new String[] {""} ;
         P00JX5_n932ContratoServicosTelas_Status = new bool[] {false} ;
         P00JX5_A928ContratoServicosTelas_Link = new String[] {""} ;
         P00JX5_A931ContratoServicosTelas_Tela = new String[] {""} ;
         P00JX5_A41Contratada_PessoaNom = new String[] {""} ;
         P00JX5_n41Contratada_PessoaNom = new bool[] {false} ;
         P00JX5_A938ContratoServicosTelas_Sequencial = new short[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getcontratoservicos_telaswcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00JX2_A74Contrato_Codigo, P00JX2_n74Contrato_Codigo, P00JX2_A39Contratada_Codigo, P00JX2_A40Contratada_PessoaCod, P00JX2_A926ContratoServicosTelas_ContratoCod, P00JX2_A41Contratada_PessoaNom, P00JX2_n41Contratada_PessoaNom, P00JX2_A932ContratoServicosTelas_Status, P00JX2_n932ContratoServicosTelas_Status, P00JX2_A929ContratoServicosTelas_Parms,
               P00JX2_n929ContratoServicosTelas_Parms, P00JX2_A928ContratoServicosTelas_Link, P00JX2_A931ContratoServicosTelas_Tela, P00JX2_A938ContratoServicosTelas_Sequencial
               }
               , new Object[] {
               P00JX3_A74Contrato_Codigo, P00JX3_n74Contrato_Codigo, P00JX3_A39Contratada_Codigo, P00JX3_A40Contratada_PessoaCod, P00JX3_A926ContratoServicosTelas_ContratoCod, P00JX3_A931ContratoServicosTelas_Tela, P00JX3_A932ContratoServicosTelas_Status, P00JX3_n932ContratoServicosTelas_Status, P00JX3_A929ContratoServicosTelas_Parms, P00JX3_n929ContratoServicosTelas_Parms,
               P00JX3_A928ContratoServicosTelas_Link, P00JX3_A41Contratada_PessoaNom, P00JX3_n41Contratada_PessoaNom, P00JX3_A938ContratoServicosTelas_Sequencial
               }
               , new Object[] {
               P00JX4_A74Contrato_Codigo, P00JX4_n74Contrato_Codigo, P00JX4_A39Contratada_Codigo, P00JX4_A40Contratada_PessoaCod, P00JX4_A926ContratoServicosTelas_ContratoCod, P00JX4_A928ContratoServicosTelas_Link, P00JX4_A932ContratoServicosTelas_Status, P00JX4_n932ContratoServicosTelas_Status, P00JX4_A929ContratoServicosTelas_Parms, P00JX4_n929ContratoServicosTelas_Parms,
               P00JX4_A931ContratoServicosTelas_Tela, P00JX4_A41Contratada_PessoaNom, P00JX4_n41Contratada_PessoaNom, P00JX4_A938ContratoServicosTelas_Sequencial
               }
               , new Object[] {
               P00JX5_A74Contrato_Codigo, P00JX5_n74Contrato_Codigo, P00JX5_A39Contratada_Codigo, P00JX5_A40Contratada_PessoaCod, P00JX5_A926ContratoServicosTelas_ContratoCod, P00JX5_A929ContratoServicosTelas_Parms, P00JX5_n929ContratoServicosTelas_Parms, P00JX5_A932ContratoServicosTelas_Status, P00JX5_n932ContratoServicosTelas_Status, P00JX5_A928ContratoServicosTelas_Link,
               P00JX5_A931ContratoServicosTelas_Tela, P00JX5_A41Contratada_PessoaNom, P00JX5_n41Contratada_PessoaNom, P00JX5_A938ContratoServicosTelas_Sequencial
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A938ContratoServicosTelas_Sequencial ;
      private int AV41GXV1 ;
      private int AV38ContratoServicosTelas_ContratoCod ;
      private int AV19TFContratoServicosTelas_Status_Sels_Count ;
      private int A926ContratoServicosTelas_ContratoCod ;
      private int A74Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private long AV32count ;
      private String AV10TFContratada_PessoaNom ;
      private String AV11TFContratada_PessoaNom_Sel ;
      private String AV12TFContratoServicosTelas_Tela ;
      private String AV13TFContratoServicosTelas_Tela_Sel ;
      private String scmdbuf ;
      private String lV10TFContratada_PessoaNom ;
      private String lV12TFContratoServicosTelas_Tela ;
      private String A932ContratoServicosTelas_Status ;
      private String A41Contratada_PessoaNom ;
      private String A931ContratoServicosTelas_Tela ;
      private bool returnInSub ;
      private bool BRKJX2 ;
      private bool n74Contrato_Codigo ;
      private bool n41Contratada_PessoaNom ;
      private bool n932ContratoServicosTelas_Status ;
      private bool n929ContratoServicosTelas_Parms ;
      private bool BRKJX4 ;
      private bool BRKJX6 ;
      private bool BRKJX8 ;
      private String AV31OptionIndexesJson ;
      private String AV26OptionsJson ;
      private String AV29OptionsDescJson ;
      private String AV18TFContratoServicosTelas_Status_SelsJson ;
      private String A929ContratoServicosTelas_Parms ;
      private String AV22DDOName ;
      private String AV20SearchTxt ;
      private String AV21SearchTxtTo ;
      private String AV14TFContratoServicosTelas_Link ;
      private String AV15TFContratoServicosTelas_Link_Sel ;
      private String AV16TFContratoServicosTelas_Parms ;
      private String AV17TFContratoServicosTelas_Parms_Sel ;
      private String lV14TFContratoServicosTelas_Link ;
      private String lV16TFContratoServicosTelas_Parms ;
      private String A928ContratoServicosTelas_Link ;
      private String AV24Option ;
      private IGxSession AV33Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00JX2_A74Contrato_Codigo ;
      private bool[] P00JX2_n74Contrato_Codigo ;
      private int[] P00JX2_A39Contratada_Codigo ;
      private int[] P00JX2_A40Contratada_PessoaCod ;
      private int[] P00JX2_A926ContratoServicosTelas_ContratoCod ;
      private String[] P00JX2_A41Contratada_PessoaNom ;
      private bool[] P00JX2_n41Contratada_PessoaNom ;
      private String[] P00JX2_A932ContratoServicosTelas_Status ;
      private bool[] P00JX2_n932ContratoServicosTelas_Status ;
      private String[] P00JX2_A929ContratoServicosTelas_Parms ;
      private bool[] P00JX2_n929ContratoServicosTelas_Parms ;
      private String[] P00JX2_A928ContratoServicosTelas_Link ;
      private String[] P00JX2_A931ContratoServicosTelas_Tela ;
      private short[] P00JX2_A938ContratoServicosTelas_Sequencial ;
      private int[] P00JX3_A74Contrato_Codigo ;
      private bool[] P00JX3_n74Contrato_Codigo ;
      private int[] P00JX3_A39Contratada_Codigo ;
      private int[] P00JX3_A40Contratada_PessoaCod ;
      private int[] P00JX3_A926ContratoServicosTelas_ContratoCod ;
      private String[] P00JX3_A931ContratoServicosTelas_Tela ;
      private String[] P00JX3_A932ContratoServicosTelas_Status ;
      private bool[] P00JX3_n932ContratoServicosTelas_Status ;
      private String[] P00JX3_A929ContratoServicosTelas_Parms ;
      private bool[] P00JX3_n929ContratoServicosTelas_Parms ;
      private String[] P00JX3_A928ContratoServicosTelas_Link ;
      private String[] P00JX3_A41Contratada_PessoaNom ;
      private bool[] P00JX3_n41Contratada_PessoaNom ;
      private short[] P00JX3_A938ContratoServicosTelas_Sequencial ;
      private int[] P00JX4_A74Contrato_Codigo ;
      private bool[] P00JX4_n74Contrato_Codigo ;
      private int[] P00JX4_A39Contratada_Codigo ;
      private int[] P00JX4_A40Contratada_PessoaCod ;
      private int[] P00JX4_A926ContratoServicosTelas_ContratoCod ;
      private String[] P00JX4_A928ContratoServicosTelas_Link ;
      private String[] P00JX4_A932ContratoServicosTelas_Status ;
      private bool[] P00JX4_n932ContratoServicosTelas_Status ;
      private String[] P00JX4_A929ContratoServicosTelas_Parms ;
      private bool[] P00JX4_n929ContratoServicosTelas_Parms ;
      private String[] P00JX4_A931ContratoServicosTelas_Tela ;
      private String[] P00JX4_A41Contratada_PessoaNom ;
      private bool[] P00JX4_n41Contratada_PessoaNom ;
      private short[] P00JX4_A938ContratoServicosTelas_Sequencial ;
      private int[] P00JX5_A74Contrato_Codigo ;
      private bool[] P00JX5_n74Contrato_Codigo ;
      private int[] P00JX5_A39Contratada_Codigo ;
      private int[] P00JX5_A40Contratada_PessoaCod ;
      private int[] P00JX5_A926ContratoServicosTelas_ContratoCod ;
      private String[] P00JX5_A929ContratoServicosTelas_Parms ;
      private bool[] P00JX5_n929ContratoServicosTelas_Parms ;
      private String[] P00JX5_A932ContratoServicosTelas_Status ;
      private bool[] P00JX5_n932ContratoServicosTelas_Status ;
      private String[] P00JX5_A928ContratoServicosTelas_Link ;
      private String[] P00JX5_A931ContratoServicosTelas_Tela ;
      private String[] P00JX5_A41Contratada_PessoaNom ;
      private bool[] P00JX5_n41Contratada_PessoaNom ;
      private short[] P00JX5_A938ContratoServicosTelas_Sequencial ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV19TFContratoServicosTelas_Status_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV35GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV36GridStateFilterValue ;
   }

   public class getcontratoservicos_telaswcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00JX2( IGxContext context ,
                                             String A932ContratoServicosTelas_Status ,
                                             IGxCollection AV19TFContratoServicosTelas_Status_Sels ,
                                             String AV11TFContratada_PessoaNom_Sel ,
                                             String AV10TFContratada_PessoaNom ,
                                             String AV13TFContratoServicosTelas_Tela_Sel ,
                                             String AV12TFContratoServicosTelas_Tela ,
                                             String AV15TFContratoServicosTelas_Link_Sel ,
                                             String AV14TFContratoServicosTelas_Link ,
                                             String AV17TFContratoServicosTelas_Parms_Sel ,
                                             String AV16TFContratoServicosTelas_Parms ,
                                             int AV19TFContratoServicosTelas_Status_Sels_Count ,
                                             String A41Contratada_PessoaNom ,
                                             String A931ContratoServicosTelas_Tela ,
                                             String A928ContratoServicosTelas_Link ,
                                             String A929ContratoServicosTelas_Parms ,
                                             int AV38ContratoServicosTelas_ContratoCod ,
                                             int A926ContratoServicosTelas_ContratoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [9] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T2.[Contrato_Codigo], T3.[Contratada_Codigo], T4.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[ContratoServicosTelas_ContratoCod] AS ContratoServicosTelas_ContratoCod, T5.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[ContratoServicosTelas_Status], T1.[ContratoServicosTelas_Parms], T1.[ContratoServicosTelas_Link], T1.[ContratoServicosTelas_Tela], T1.[ContratoServicosTelas_Sequencial] FROM (((([ContratoServicosTelas] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosTelas_ContratoCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T3.[Contratada_Codigo]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratada_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoServicosTelas_ContratoCod] = @AV38ContratoServicosTelas_ContratoCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContratada_PessoaNom)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV10TFContratada_PessoaNom)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratada_PessoaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV11TFContratada_PessoaNom_Sel)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoServicosTelas_Tela_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratoServicosTelas_Tela)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] like @lV12TFContratoServicosTelas_Tela)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoServicosTelas_Tela_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] = @AV13TFContratoServicosTelas_Tela_Sel)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoServicosTelas_Link_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoServicosTelas_Link)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] like @lV14TFContratoServicosTelas_Link)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoServicosTelas_Link_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] = @AV15TFContratoServicosTelas_Link_Sel)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoServicosTelas_Parms_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContratoServicosTelas_Parms)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] like @lV16TFContratoServicosTelas_Parms)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoServicosTelas_Parms_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] = @AV17TFContratoServicosTelas_Parms_Sel)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV19TFContratoServicosTelas_Status_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV19TFContratoServicosTelas_Status_Sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoServicosTelas_ContratoCod], T5.[Pessoa_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00JX3( IGxContext context ,
                                             String A932ContratoServicosTelas_Status ,
                                             IGxCollection AV19TFContratoServicosTelas_Status_Sels ,
                                             String AV11TFContratada_PessoaNom_Sel ,
                                             String AV10TFContratada_PessoaNom ,
                                             String AV13TFContratoServicosTelas_Tela_Sel ,
                                             String AV12TFContratoServicosTelas_Tela ,
                                             String AV15TFContratoServicosTelas_Link_Sel ,
                                             String AV14TFContratoServicosTelas_Link ,
                                             String AV17TFContratoServicosTelas_Parms_Sel ,
                                             String AV16TFContratoServicosTelas_Parms ,
                                             int AV19TFContratoServicosTelas_Status_Sels_Count ,
                                             String A41Contratada_PessoaNom ,
                                             String A931ContratoServicosTelas_Tela ,
                                             String A928ContratoServicosTelas_Link ,
                                             String A929ContratoServicosTelas_Parms ,
                                             int AV38ContratoServicosTelas_ContratoCod ,
                                             int A926ContratoServicosTelas_ContratoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [9] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T2.[Contrato_Codigo], T3.[Contratada_Codigo], T4.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[ContratoServicosTelas_ContratoCod] AS ContratoServicosTelas_ContratoCod, T1.[ContratoServicosTelas_Tela], T1.[ContratoServicosTelas_Status], T1.[ContratoServicosTelas_Parms], T1.[ContratoServicosTelas_Link], T5.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[ContratoServicosTelas_Sequencial] FROM (((([ContratoServicosTelas] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosTelas_ContratoCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T3.[Contratada_Codigo]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratada_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoServicosTelas_ContratoCod] = @AV38ContratoServicosTelas_ContratoCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContratada_PessoaNom)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV10TFContratada_PessoaNom)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratada_PessoaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV11TFContratada_PessoaNom_Sel)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoServicosTelas_Tela_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratoServicosTelas_Tela)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] like @lV12TFContratoServicosTelas_Tela)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoServicosTelas_Tela_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] = @AV13TFContratoServicosTelas_Tela_Sel)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoServicosTelas_Link_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoServicosTelas_Link)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] like @lV14TFContratoServicosTelas_Link)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoServicosTelas_Link_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] = @AV15TFContratoServicosTelas_Link_Sel)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoServicosTelas_Parms_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContratoServicosTelas_Parms)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] like @lV16TFContratoServicosTelas_Parms)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoServicosTelas_Parms_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] = @AV17TFContratoServicosTelas_Parms_Sel)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV19TFContratoServicosTelas_Status_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV19TFContratoServicosTelas_Status_Sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoServicosTelas_ContratoCod], T1.[ContratoServicosTelas_Tela]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00JX4( IGxContext context ,
                                             String A932ContratoServicosTelas_Status ,
                                             IGxCollection AV19TFContratoServicosTelas_Status_Sels ,
                                             String AV11TFContratada_PessoaNom_Sel ,
                                             String AV10TFContratada_PessoaNom ,
                                             String AV13TFContratoServicosTelas_Tela_Sel ,
                                             String AV12TFContratoServicosTelas_Tela ,
                                             String AV15TFContratoServicosTelas_Link_Sel ,
                                             String AV14TFContratoServicosTelas_Link ,
                                             String AV17TFContratoServicosTelas_Parms_Sel ,
                                             String AV16TFContratoServicosTelas_Parms ,
                                             int AV19TFContratoServicosTelas_Status_Sels_Count ,
                                             String A41Contratada_PessoaNom ,
                                             String A931ContratoServicosTelas_Tela ,
                                             String A928ContratoServicosTelas_Link ,
                                             String A929ContratoServicosTelas_Parms ,
                                             int AV38ContratoServicosTelas_ContratoCod ,
                                             int A926ContratoServicosTelas_ContratoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [9] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T2.[Contrato_Codigo], T3.[Contratada_Codigo], T4.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[ContratoServicosTelas_ContratoCod] AS ContratoServicosTelas_ContratoCod, T1.[ContratoServicosTelas_Link], T1.[ContratoServicosTelas_Status], T1.[ContratoServicosTelas_Parms], T1.[ContratoServicosTelas_Tela], T5.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[ContratoServicosTelas_Sequencial] FROM (((([ContratoServicosTelas] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosTelas_ContratoCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T3.[Contratada_Codigo]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratada_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoServicosTelas_ContratoCod] = @AV38ContratoServicosTelas_ContratoCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContratada_PessoaNom)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV10TFContratada_PessoaNom)";
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratada_PessoaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV11TFContratada_PessoaNom_Sel)";
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoServicosTelas_Tela_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratoServicosTelas_Tela)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] like @lV12TFContratoServicosTelas_Tela)";
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoServicosTelas_Tela_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] = @AV13TFContratoServicosTelas_Tela_Sel)";
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoServicosTelas_Link_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoServicosTelas_Link)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] like @lV14TFContratoServicosTelas_Link)";
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoServicosTelas_Link_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] = @AV15TFContratoServicosTelas_Link_Sel)";
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoServicosTelas_Parms_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContratoServicosTelas_Parms)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] like @lV16TFContratoServicosTelas_Parms)";
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoServicosTelas_Parms_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] = @AV17TFContratoServicosTelas_Parms_Sel)";
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV19TFContratoServicosTelas_Status_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV19TFContratoServicosTelas_Status_Sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoServicosTelas_ContratoCod], T1.[ContratoServicosTelas_Link]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00JX5( IGxContext context ,
                                             String A932ContratoServicosTelas_Status ,
                                             IGxCollection AV19TFContratoServicosTelas_Status_Sels ,
                                             String AV11TFContratada_PessoaNom_Sel ,
                                             String AV10TFContratada_PessoaNom ,
                                             String AV13TFContratoServicosTelas_Tela_Sel ,
                                             String AV12TFContratoServicosTelas_Tela ,
                                             String AV15TFContratoServicosTelas_Link_Sel ,
                                             String AV14TFContratoServicosTelas_Link ,
                                             String AV17TFContratoServicosTelas_Parms_Sel ,
                                             String AV16TFContratoServicosTelas_Parms ,
                                             int AV19TFContratoServicosTelas_Status_Sels_Count ,
                                             String A41Contratada_PessoaNom ,
                                             String A931ContratoServicosTelas_Tela ,
                                             String A928ContratoServicosTelas_Link ,
                                             String A929ContratoServicosTelas_Parms ,
                                             int AV38ContratoServicosTelas_ContratoCod ,
                                             int A926ContratoServicosTelas_ContratoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [9] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T2.[Contrato_Codigo], T3.[Contratada_Codigo], T4.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[ContratoServicosTelas_ContratoCod] AS ContratoServicosTelas_ContratoCod, T1.[ContratoServicosTelas_Parms], T1.[ContratoServicosTelas_Status], T1.[ContratoServicosTelas_Link], T1.[ContratoServicosTelas_Tela], T5.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[ContratoServicosTelas_Sequencial] FROM (((([ContratoServicosTelas] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosTelas_ContratoCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T3.[Contratada_Codigo]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratada_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoServicosTelas_ContratoCod] = @AV38ContratoServicosTelas_ContratoCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContratada_PessoaNom)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV10TFContratada_PessoaNom)";
         }
         else
         {
            GXv_int7[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratada_PessoaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV11TFContratada_PessoaNom_Sel)";
         }
         else
         {
            GXv_int7[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoServicosTelas_Tela_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratoServicosTelas_Tela)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] like @lV12TFContratoServicosTelas_Tela)";
         }
         else
         {
            GXv_int7[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoServicosTelas_Tela_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] = @AV13TFContratoServicosTelas_Tela_Sel)";
         }
         else
         {
            GXv_int7[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoServicosTelas_Link_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoServicosTelas_Link)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] like @lV14TFContratoServicosTelas_Link)";
         }
         else
         {
            GXv_int7[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoServicosTelas_Link_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] = @AV15TFContratoServicosTelas_Link_Sel)";
         }
         else
         {
            GXv_int7[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoServicosTelas_Parms_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContratoServicosTelas_Parms)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] like @lV16TFContratoServicosTelas_Parms)";
         }
         else
         {
            GXv_int7[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoServicosTelas_Parms_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] = @AV17TFContratoServicosTelas_Parms_Sel)";
         }
         else
         {
            GXv_int7[8] = 1;
         }
         if ( AV19TFContratoServicosTelas_Status_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV19TFContratoServicosTelas_Status_Sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoServicosTelas_ContratoCod], T1.[ContratoServicosTelas_Parms]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00JX2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] );
               case 1 :
                     return conditional_P00JX3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] );
               case 2 :
                     return conditional_P00JX4(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] );
               case 3 :
                     return conditional_P00JX5(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00JX2 ;
          prmP00JX2 = new Object[] {
          new Object[] {"@AV38ContratoServicosTelas_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV11TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV12TFContratoServicosTelas_Tela",SqlDbType.Char,50,0} ,
          new Object[] {"@AV13TFContratoServicosTelas_Tela_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV14TFContratoServicosTelas_Link",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV15TFContratoServicosTelas_Link_Sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV16TFContratoServicosTelas_Parms",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV17TFContratoServicosTelas_Parms_Sel",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmP00JX3 ;
          prmP00JX3 = new Object[] {
          new Object[] {"@AV38ContratoServicosTelas_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV11TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV12TFContratoServicosTelas_Tela",SqlDbType.Char,50,0} ,
          new Object[] {"@AV13TFContratoServicosTelas_Tela_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV14TFContratoServicosTelas_Link",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV15TFContratoServicosTelas_Link_Sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV16TFContratoServicosTelas_Parms",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV17TFContratoServicosTelas_Parms_Sel",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmP00JX4 ;
          prmP00JX4 = new Object[] {
          new Object[] {"@AV38ContratoServicosTelas_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV11TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV12TFContratoServicosTelas_Tela",SqlDbType.Char,50,0} ,
          new Object[] {"@AV13TFContratoServicosTelas_Tela_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV14TFContratoServicosTelas_Link",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV15TFContratoServicosTelas_Link_Sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV16TFContratoServicosTelas_Parms",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV17TFContratoServicosTelas_Parms_Sel",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmP00JX5 ;
          prmP00JX5 = new Object[] {
          new Object[] {"@AV38ContratoServicosTelas_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV11TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV12TFContratoServicosTelas_Tela",SqlDbType.Char,50,0} ,
          new Object[] {"@AV13TFContratoServicosTelas_Tela_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV14TFContratoServicosTelas_Link",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV15TFContratoServicosTelas_Link_Sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV16TFContratoServicosTelas_Parms",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV17TFContratoServicosTelas_Parms_Sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00JX2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00JX2,100,0,true,false )
             ,new CursorDef("P00JX3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00JX3,100,0,true,false )
             ,new CursorDef("P00JX4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00JX4,100,0,true,false )
             ,new CursorDef("P00JX5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00JX5,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 50) ;
                ((short[]) buf[13])[0] = rslt.getShort(10) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[11])[0] = rslt.getString(9, 100) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((short[]) buf[13])[0] = rslt.getShort(10) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 50) ;
                ((String[]) buf[11])[0] = rslt.getString(9, 100) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((short[]) buf[13])[0] = rslt.getShort(10) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((String[]) buf[10])[0] = rslt.getString(8, 50) ;
                ((String[]) buf[11])[0] = rslt.getString(9, 100) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((short[]) buf[13])[0] = rslt.getShort(10) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getcontratoservicos_telaswcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getcontratoservicos_telaswcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getcontratoservicos_telaswcfilterdata") )
          {
             return  ;
          }
          getcontratoservicos_telaswcfilterdata worker = new getcontratoservicos_telaswcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
