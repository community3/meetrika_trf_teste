/*
               File: ContagemResultadoQA_BC
        Description: Contagem Resultado QA
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:18:0.24
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadoqa_bc : GXHttpHandler, IGxSilentTrn
   {
      public contagemresultadoqa_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemresultadoqa_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow4Y220( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey4Y220( ) ;
         standaloneModal( ) ;
         AddRow4Y220( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z1984ContagemResultadoQA_Codigo = A1984ContagemResultadoQA_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_4Y0( )
      {
         BeforeValidate4Y220( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls4Y220( ) ;
            }
            else
            {
               CheckExtendedTable4Y220( ) ;
               if ( AnyError == 0 )
               {
                  ZM4Y220( 8) ;
                  ZM4Y220( 9) ;
                  ZM4Y220( 10) ;
                  ZM4Y220( 11) ;
                  ZM4Y220( 12) ;
                  ZM4Y220( 13) ;
               }
               CloseExtendedTableCursors4Y220( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM4Y220( short GX_JID )
      {
         if ( ( GX_JID == 7 ) || ( GX_JID == 0 ) )
         {
            Z1986ContagemResultadoQA_DataHora = A1986ContagemResultadoQA_DataHora;
            Z1985ContagemResultadoQA_OSCod = A1985ContagemResultadoQA_OSCod;
            Z1988ContagemResultadoQA_UserCod = A1988ContagemResultadoQA_UserCod;
            Z1992ContagemResultadoQA_ParaCod = A1992ContagemResultadoQA_ParaCod;
            Z1996ContagemResultadoQA_RespostaDe = A1996ContagemResultadoQA_RespostaDe;
            Z1991ContagemResultadoQA_ContratanteDoUser = A1991ContagemResultadoQA_ContratanteDoUser;
            Z1995ContagemResultadoQA_ContratanteDoPara = A1995ContagemResultadoQA_ContratanteDoPara;
            Z1998ContagemResultadoQA_Resposta = A1998ContagemResultadoQA_Resposta;
         }
         if ( ( GX_JID == 8 ) || ( GX_JID == 0 ) )
         {
            Z493ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            Z1991ContagemResultadoQA_ContratanteDoUser = A1991ContagemResultadoQA_ContratanteDoUser;
            Z1995ContagemResultadoQA_ContratanteDoPara = A1995ContagemResultadoQA_ContratanteDoPara;
            Z1998ContagemResultadoQA_Resposta = A1998ContagemResultadoQA_Resposta;
         }
         if ( ( GX_JID == 9 ) || ( GX_JID == 0 ) )
         {
            Z1989ContagemResultadoQA_UserPesCod = A1989ContagemResultadoQA_UserPesCod;
            Z1991ContagemResultadoQA_ContratanteDoUser = A1991ContagemResultadoQA_ContratanteDoUser;
            Z1995ContagemResultadoQA_ContratanteDoPara = A1995ContagemResultadoQA_ContratanteDoPara;
            Z1998ContagemResultadoQA_Resposta = A1998ContagemResultadoQA_Resposta;
         }
         if ( ( GX_JID == 10 ) || ( GX_JID == 0 ) )
         {
            Z1993ContagemResultadoQA_ParaPesCod = A1993ContagemResultadoQA_ParaPesCod;
            Z1991ContagemResultadoQA_ContratanteDoUser = A1991ContagemResultadoQA_ContratanteDoUser;
            Z1995ContagemResultadoQA_ContratanteDoPara = A1995ContagemResultadoQA_ContratanteDoPara;
            Z1998ContagemResultadoQA_Resposta = A1998ContagemResultadoQA_Resposta;
         }
         if ( ( GX_JID == 11 ) || ( GX_JID == 0 ) )
         {
            Z1991ContagemResultadoQA_ContratanteDoUser = A1991ContagemResultadoQA_ContratanteDoUser;
            Z1995ContagemResultadoQA_ContratanteDoPara = A1995ContagemResultadoQA_ContratanteDoPara;
            Z1998ContagemResultadoQA_Resposta = A1998ContagemResultadoQA_Resposta;
         }
         if ( ( GX_JID == 12 ) || ( GX_JID == 0 ) )
         {
            Z1990ContagemResultadoQA_UserPesNom = A1990ContagemResultadoQA_UserPesNom;
            Z1991ContagemResultadoQA_ContratanteDoUser = A1991ContagemResultadoQA_ContratanteDoUser;
            Z1995ContagemResultadoQA_ContratanteDoPara = A1995ContagemResultadoQA_ContratanteDoPara;
            Z1998ContagemResultadoQA_Resposta = A1998ContagemResultadoQA_Resposta;
         }
         if ( ( GX_JID == 13 ) || ( GX_JID == 0 ) )
         {
            Z1994ContagemResultadoQA_ParaPesNom = A1994ContagemResultadoQA_ParaPesNom;
            Z1991ContagemResultadoQA_ContratanteDoUser = A1991ContagemResultadoQA_ContratanteDoUser;
            Z1995ContagemResultadoQA_ContratanteDoPara = A1995ContagemResultadoQA_ContratanteDoPara;
            Z1998ContagemResultadoQA_Resposta = A1998ContagemResultadoQA_Resposta;
         }
         if ( GX_JID == -7 )
         {
            Z1984ContagemResultadoQA_Codigo = A1984ContagemResultadoQA_Codigo;
            Z1986ContagemResultadoQA_DataHora = A1986ContagemResultadoQA_DataHora;
            Z1987ContagemResultadoQA_Texto = A1987ContagemResultadoQA_Texto;
            Z1985ContagemResultadoQA_OSCod = A1985ContagemResultadoQA_OSCod;
            Z1988ContagemResultadoQA_UserCod = A1988ContagemResultadoQA_UserCod;
            Z1992ContagemResultadoQA_ParaCod = A1992ContagemResultadoQA_ParaCod;
            Z1996ContagemResultadoQA_RespostaDe = A1996ContagemResultadoQA_RespostaDe;
            Z493ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            Z1989ContagemResultadoQA_UserPesCod = A1989ContagemResultadoQA_UserPesCod;
            Z1990ContagemResultadoQA_UserPesNom = A1990ContagemResultadoQA_UserPesNom;
            Z1993ContagemResultadoQA_ParaPesCod = A1993ContagemResultadoQA_ParaPesCod;
            Z1994ContagemResultadoQA_ParaPesNom = A1994ContagemResultadoQA_ParaPesNom;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load4Y220( )
      {
         /* Using cursor BC004Y10 */
         pr_default.execute(8, new Object[] {A1984ContagemResultadoQA_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound220 = 1;
            A1986ContagemResultadoQA_DataHora = BC004Y10_A1986ContagemResultadoQA_DataHora[0];
            A493ContagemResultado_DemandaFM = BC004Y10_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = BC004Y10_n493ContagemResultado_DemandaFM[0];
            A1987ContagemResultadoQA_Texto = BC004Y10_A1987ContagemResultadoQA_Texto[0];
            A1990ContagemResultadoQA_UserPesNom = BC004Y10_A1990ContagemResultadoQA_UserPesNom[0];
            n1990ContagemResultadoQA_UserPesNom = BC004Y10_n1990ContagemResultadoQA_UserPesNom[0];
            A1994ContagemResultadoQA_ParaPesNom = BC004Y10_A1994ContagemResultadoQA_ParaPesNom[0];
            n1994ContagemResultadoQA_ParaPesNom = BC004Y10_n1994ContagemResultadoQA_ParaPesNom[0];
            A1985ContagemResultadoQA_OSCod = BC004Y10_A1985ContagemResultadoQA_OSCod[0];
            A1988ContagemResultadoQA_UserCod = BC004Y10_A1988ContagemResultadoQA_UserCod[0];
            A1992ContagemResultadoQA_ParaCod = BC004Y10_A1992ContagemResultadoQA_ParaCod[0];
            A1996ContagemResultadoQA_RespostaDe = BC004Y10_A1996ContagemResultadoQA_RespostaDe[0];
            n1996ContagemResultadoQA_RespostaDe = BC004Y10_n1996ContagemResultadoQA_RespostaDe[0];
            A1989ContagemResultadoQA_UserPesCod = BC004Y10_A1989ContagemResultadoQA_UserPesCod[0];
            n1989ContagemResultadoQA_UserPesCod = BC004Y10_n1989ContagemResultadoQA_UserPesCod[0];
            A1993ContagemResultadoQA_ParaPesCod = BC004Y10_A1993ContagemResultadoQA_ParaPesCod[0];
            n1993ContagemResultadoQA_ParaPesCod = BC004Y10_n1993ContagemResultadoQA_ParaPesCod[0];
            ZM4Y220( -7) ;
         }
         pr_default.close(8);
         OnLoadActions4Y220( ) ;
      }

      protected void OnLoadActions4Y220( )
      {
         GXt_int1 = A1998ContagemResultadoQA_Resposta;
         new prc_qa_resposta(context ).execute( ref  A1984ContagemResultadoQA_Codigo, out  GXt_int1) ;
         A1998ContagemResultadoQA_Resposta = GXt_int1;
         GXt_int1 = A1995ContagemResultadoQA_ContratanteDoPara;
         new prc_contratantedousuario(context ).execute( ref  A1985ContagemResultadoQA_OSCod, ref  A1992ContagemResultadoQA_ParaCod, out  GXt_int1) ;
         A1995ContagemResultadoQA_ContratanteDoPara = GXt_int1;
         GXt_int1 = A1991ContagemResultadoQA_ContratanteDoUser;
         new prc_contratantedousuario(context ).execute( ref  A1985ContagemResultadoQA_OSCod, ref  A1988ContagemResultadoQA_UserCod, out  GXt_int1) ;
         A1991ContagemResultadoQA_ContratanteDoUser = GXt_int1;
      }

      protected void CheckExtendedTable4Y220( )
      {
         standaloneModal( ) ;
         GXt_int1 = A1998ContagemResultadoQA_Resposta;
         new prc_qa_resposta(context ).execute( ref  A1984ContagemResultadoQA_Codigo, out  GXt_int1) ;
         A1998ContagemResultadoQA_Resposta = GXt_int1;
         /* Using cursor BC004Y4 */
         pr_default.execute(2, new Object[] {A1985ContagemResultadoQA_OSCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado QA_OSCod'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOQA_OSCOD");
            AnyError = 1;
         }
         A493ContagemResultado_DemandaFM = BC004Y4_A493ContagemResultado_DemandaFM[0];
         n493ContagemResultado_DemandaFM = BC004Y4_n493ContagemResultado_DemandaFM[0];
         pr_default.close(2);
         GXt_int1 = A1995ContagemResultadoQA_ContratanteDoPara;
         new prc_contratantedousuario(context ).execute( ref  A1985ContagemResultadoQA_OSCod, ref  A1992ContagemResultadoQA_ParaCod, out  GXt_int1) ;
         A1995ContagemResultadoQA_ContratanteDoPara = GXt_int1;
         GXt_int1 = A1991ContagemResultadoQA_ContratanteDoUser;
         new prc_contratantedousuario(context ).execute( ref  A1985ContagemResultadoQA_OSCod, ref  A1988ContagemResultadoQA_UserCod, out  GXt_int1) ;
         A1991ContagemResultadoQA_ContratanteDoUser = GXt_int1;
         if ( ! ( (DateTime.MinValue==A1986ContagemResultadoQA_DataHora) || ( A1986ContagemResultadoQA_DataHora >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC004Y5 */
         pr_default.execute(3, new Object[] {A1988ContagemResultadoQA_UserCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado QA_Usuario'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOQA_USERCOD");
            AnyError = 1;
         }
         A1989ContagemResultadoQA_UserPesCod = BC004Y5_A1989ContagemResultadoQA_UserPesCod[0];
         n1989ContagemResultadoQA_UserPesCod = BC004Y5_n1989ContagemResultadoQA_UserPesCod[0];
         pr_default.close(3);
         /* Using cursor BC004Y8 */
         pr_default.execute(6, new Object[] {n1989ContagemResultadoQA_UserPesCod, A1989ContagemResultadoQA_UserPesCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1990ContagemResultadoQA_UserPesNom = BC004Y8_A1990ContagemResultadoQA_UserPesNom[0];
         n1990ContagemResultadoQA_UserPesNom = BC004Y8_n1990ContagemResultadoQA_UserPesNom[0];
         pr_default.close(6);
         /* Using cursor BC004Y6 */
         pr_default.execute(4, new Object[] {A1992ContagemResultadoQA_ParaCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado QA_Para'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOQA_PARACOD");
            AnyError = 1;
         }
         A1993ContagemResultadoQA_ParaPesCod = BC004Y6_A1993ContagemResultadoQA_ParaPesCod[0];
         n1993ContagemResultadoQA_ParaPesCod = BC004Y6_n1993ContagemResultadoQA_ParaPesCod[0];
         pr_default.close(4);
         /* Using cursor BC004Y9 */
         pr_default.execute(7, new Object[] {n1993ContagemResultadoQA_ParaPesCod, A1993ContagemResultadoQA_ParaPesCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1994ContagemResultadoQA_ParaPesNom = BC004Y9_A1994ContagemResultadoQA_ParaPesNom[0];
         n1994ContagemResultadoQA_ParaPesNom = BC004Y9_n1994ContagemResultadoQA_ParaPesNom[0];
         pr_default.close(7);
         /* Using cursor BC004Y7 */
         pr_default.execute(5, new Object[] {n1996ContagemResultadoQA_RespostaDe, A1996ContagemResultadoQA_RespostaDe});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A1996ContagemResultadoQA_RespostaDe) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado QA_Resposta De'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOQA_RESPOSTADE");
               AnyError = 1;
            }
         }
         pr_default.close(5);
      }

      protected void CloseExtendedTableCursors4Y220( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(6);
         pr_default.close(4);
         pr_default.close(7);
         pr_default.close(5);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey4Y220( )
      {
         /* Using cursor BC004Y11 */
         pr_default.execute(9, new Object[] {A1984ContagemResultadoQA_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound220 = 1;
         }
         else
         {
            RcdFound220 = 0;
         }
         pr_default.close(9);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC004Y3 */
         pr_default.execute(1, new Object[] {A1984ContagemResultadoQA_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4Y220( 7) ;
            RcdFound220 = 1;
            A1984ContagemResultadoQA_Codigo = BC004Y3_A1984ContagemResultadoQA_Codigo[0];
            A1986ContagemResultadoQA_DataHora = BC004Y3_A1986ContagemResultadoQA_DataHora[0];
            A1987ContagemResultadoQA_Texto = BC004Y3_A1987ContagemResultadoQA_Texto[0];
            A1985ContagemResultadoQA_OSCod = BC004Y3_A1985ContagemResultadoQA_OSCod[0];
            A1988ContagemResultadoQA_UserCod = BC004Y3_A1988ContagemResultadoQA_UserCod[0];
            A1992ContagemResultadoQA_ParaCod = BC004Y3_A1992ContagemResultadoQA_ParaCod[0];
            A1996ContagemResultadoQA_RespostaDe = BC004Y3_A1996ContagemResultadoQA_RespostaDe[0];
            n1996ContagemResultadoQA_RespostaDe = BC004Y3_n1996ContagemResultadoQA_RespostaDe[0];
            Z1984ContagemResultadoQA_Codigo = A1984ContagemResultadoQA_Codigo;
            sMode220 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load4Y220( ) ;
            if ( AnyError == 1 )
            {
               RcdFound220 = 0;
               InitializeNonKey4Y220( ) ;
            }
            Gx_mode = sMode220;
         }
         else
         {
            RcdFound220 = 0;
            InitializeNonKey4Y220( ) ;
            sMode220 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode220;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4Y220( ) ;
         if ( RcdFound220 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_4Y0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency4Y220( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC004Y2 */
            pr_default.execute(0, new Object[] {A1984ContagemResultadoQA_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoQA"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1986ContagemResultadoQA_DataHora != BC004Y2_A1986ContagemResultadoQA_DataHora[0] ) || ( Z1985ContagemResultadoQA_OSCod != BC004Y2_A1985ContagemResultadoQA_OSCod[0] ) || ( Z1988ContagemResultadoQA_UserCod != BC004Y2_A1988ContagemResultadoQA_UserCod[0] ) || ( Z1992ContagemResultadoQA_ParaCod != BC004Y2_A1992ContagemResultadoQA_ParaCod[0] ) || ( Z1996ContagemResultadoQA_RespostaDe != BC004Y2_A1996ContagemResultadoQA_RespostaDe[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultadoQA"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4Y220( )
      {
         BeforeValidate4Y220( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4Y220( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4Y220( 0) ;
            CheckOptimisticConcurrency4Y220( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4Y220( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4Y220( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC004Y12 */
                     pr_default.execute(10, new Object[] {A1986ContagemResultadoQA_DataHora, A1987ContagemResultadoQA_Texto, A1985ContagemResultadoQA_OSCod, A1988ContagemResultadoQA_UserCod, A1992ContagemResultadoQA_ParaCod, n1996ContagemResultadoQA_RespostaDe, A1996ContagemResultadoQA_RespostaDe});
                     A1984ContagemResultadoQA_Codigo = BC004Y12_A1984ContagemResultadoQA_Codigo[0];
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoQA") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4Y220( ) ;
            }
            EndLevel4Y220( ) ;
         }
         CloseExtendedTableCursors4Y220( ) ;
      }

      protected void Update4Y220( )
      {
         BeforeValidate4Y220( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4Y220( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4Y220( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4Y220( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4Y220( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC004Y13 */
                     pr_default.execute(11, new Object[] {A1986ContagemResultadoQA_DataHora, A1987ContagemResultadoQA_Texto, A1985ContagemResultadoQA_OSCod, A1988ContagemResultadoQA_UserCod, A1992ContagemResultadoQA_ParaCod, n1996ContagemResultadoQA_RespostaDe, A1996ContagemResultadoQA_RespostaDe, A1984ContagemResultadoQA_Codigo});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoQA") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoQA"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate4Y220( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4Y220( ) ;
         }
         CloseExtendedTableCursors4Y220( ) ;
      }

      protected void DeferredUpdate4Y220( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate4Y220( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4Y220( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4Y220( ) ;
            AfterConfirm4Y220( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4Y220( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC004Y14 */
                  pr_default.execute(12, new Object[] {A1984ContagemResultadoQA_Codigo});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoQA") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode220 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel4Y220( ) ;
         Gx_mode = sMode220;
      }

      protected void OnDeleteControls4Y220( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            GXt_int1 = A1998ContagemResultadoQA_Resposta;
            new prc_qa_resposta(context ).execute( ref  A1984ContagemResultadoQA_Codigo, out  GXt_int1) ;
            A1998ContagemResultadoQA_Resposta = GXt_int1;
            /* Using cursor BC004Y15 */
            pr_default.execute(13, new Object[] {A1985ContagemResultadoQA_OSCod});
            A493ContagemResultado_DemandaFM = BC004Y15_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = BC004Y15_n493ContagemResultado_DemandaFM[0];
            pr_default.close(13);
            /* Using cursor BC004Y16 */
            pr_default.execute(14, new Object[] {A1988ContagemResultadoQA_UserCod});
            A1989ContagemResultadoQA_UserPesCod = BC004Y16_A1989ContagemResultadoQA_UserPesCod[0];
            n1989ContagemResultadoQA_UserPesCod = BC004Y16_n1989ContagemResultadoQA_UserPesCod[0];
            pr_default.close(14);
            /* Using cursor BC004Y17 */
            pr_default.execute(15, new Object[] {n1989ContagemResultadoQA_UserPesCod, A1989ContagemResultadoQA_UserPesCod});
            A1990ContagemResultadoQA_UserPesNom = BC004Y17_A1990ContagemResultadoQA_UserPesNom[0];
            n1990ContagemResultadoQA_UserPesNom = BC004Y17_n1990ContagemResultadoQA_UserPesNom[0];
            pr_default.close(15);
            GXt_int1 = A1991ContagemResultadoQA_ContratanteDoUser;
            new prc_contratantedousuario(context ).execute( ref  A1985ContagemResultadoQA_OSCod, ref  A1988ContagemResultadoQA_UserCod, out  GXt_int1) ;
            A1991ContagemResultadoQA_ContratanteDoUser = GXt_int1;
            /* Using cursor BC004Y18 */
            pr_default.execute(16, new Object[] {A1992ContagemResultadoQA_ParaCod});
            A1993ContagemResultadoQA_ParaPesCod = BC004Y18_A1993ContagemResultadoQA_ParaPesCod[0];
            n1993ContagemResultadoQA_ParaPesCod = BC004Y18_n1993ContagemResultadoQA_ParaPesCod[0];
            pr_default.close(16);
            /* Using cursor BC004Y19 */
            pr_default.execute(17, new Object[] {n1993ContagemResultadoQA_ParaPesCod, A1993ContagemResultadoQA_ParaPesCod});
            A1994ContagemResultadoQA_ParaPesNom = BC004Y19_A1994ContagemResultadoQA_ParaPesNom[0];
            n1994ContagemResultadoQA_ParaPesNom = BC004Y19_n1994ContagemResultadoQA_ParaPesNom[0];
            pr_default.close(17);
            GXt_int1 = A1995ContagemResultadoQA_ContratanteDoPara;
            new prc_contratantedousuario(context ).execute( ref  A1985ContagemResultadoQA_OSCod, ref  A1992ContagemResultadoQA_ParaCod, out  GXt_int1) ;
            A1995ContagemResultadoQA_ContratanteDoPara = GXt_int1;
         }
         if ( AnyError == 0 )
         {
            /* Using cursor BC004Y20 */
            pr_default.execute(18, new Object[] {A1984ContagemResultadoQA_Codigo});
            if ( (pr_default.getStatus(18) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado QA"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(18);
         }
      }

      protected void EndLevel4Y220( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4Y220( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart4Y220( )
      {
         /* Using cursor BC004Y21 */
         pr_default.execute(19, new Object[] {A1984ContagemResultadoQA_Codigo});
         RcdFound220 = 0;
         if ( (pr_default.getStatus(19) != 101) )
         {
            RcdFound220 = 1;
            A1984ContagemResultadoQA_Codigo = BC004Y21_A1984ContagemResultadoQA_Codigo[0];
            A1986ContagemResultadoQA_DataHora = BC004Y21_A1986ContagemResultadoQA_DataHora[0];
            A493ContagemResultado_DemandaFM = BC004Y21_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = BC004Y21_n493ContagemResultado_DemandaFM[0];
            A1987ContagemResultadoQA_Texto = BC004Y21_A1987ContagemResultadoQA_Texto[0];
            A1990ContagemResultadoQA_UserPesNom = BC004Y21_A1990ContagemResultadoQA_UserPesNom[0];
            n1990ContagemResultadoQA_UserPesNom = BC004Y21_n1990ContagemResultadoQA_UserPesNom[0];
            A1994ContagemResultadoQA_ParaPesNom = BC004Y21_A1994ContagemResultadoQA_ParaPesNom[0];
            n1994ContagemResultadoQA_ParaPesNom = BC004Y21_n1994ContagemResultadoQA_ParaPesNom[0];
            A1985ContagemResultadoQA_OSCod = BC004Y21_A1985ContagemResultadoQA_OSCod[0];
            A1988ContagemResultadoQA_UserCod = BC004Y21_A1988ContagemResultadoQA_UserCod[0];
            A1992ContagemResultadoQA_ParaCod = BC004Y21_A1992ContagemResultadoQA_ParaCod[0];
            A1996ContagemResultadoQA_RespostaDe = BC004Y21_A1996ContagemResultadoQA_RespostaDe[0];
            n1996ContagemResultadoQA_RespostaDe = BC004Y21_n1996ContagemResultadoQA_RespostaDe[0];
            A1989ContagemResultadoQA_UserPesCod = BC004Y21_A1989ContagemResultadoQA_UserPesCod[0];
            n1989ContagemResultadoQA_UserPesCod = BC004Y21_n1989ContagemResultadoQA_UserPesCod[0];
            A1993ContagemResultadoQA_ParaPesCod = BC004Y21_A1993ContagemResultadoQA_ParaPesCod[0];
            n1993ContagemResultadoQA_ParaPesCod = BC004Y21_n1993ContagemResultadoQA_ParaPesCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext4Y220( )
      {
         /* Scan next routine */
         pr_default.readNext(19);
         RcdFound220 = 0;
         ScanKeyLoad4Y220( ) ;
      }

      protected void ScanKeyLoad4Y220( )
      {
         sMode220 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(19) != 101) )
         {
            RcdFound220 = 1;
            A1984ContagemResultadoQA_Codigo = BC004Y21_A1984ContagemResultadoQA_Codigo[0];
            A1986ContagemResultadoQA_DataHora = BC004Y21_A1986ContagemResultadoQA_DataHora[0];
            A493ContagemResultado_DemandaFM = BC004Y21_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = BC004Y21_n493ContagemResultado_DemandaFM[0];
            A1987ContagemResultadoQA_Texto = BC004Y21_A1987ContagemResultadoQA_Texto[0];
            A1990ContagemResultadoQA_UserPesNom = BC004Y21_A1990ContagemResultadoQA_UserPesNom[0];
            n1990ContagemResultadoQA_UserPesNom = BC004Y21_n1990ContagemResultadoQA_UserPesNom[0];
            A1994ContagemResultadoQA_ParaPesNom = BC004Y21_A1994ContagemResultadoQA_ParaPesNom[0];
            n1994ContagemResultadoQA_ParaPesNom = BC004Y21_n1994ContagemResultadoQA_ParaPesNom[0];
            A1985ContagemResultadoQA_OSCod = BC004Y21_A1985ContagemResultadoQA_OSCod[0];
            A1988ContagemResultadoQA_UserCod = BC004Y21_A1988ContagemResultadoQA_UserCod[0];
            A1992ContagemResultadoQA_ParaCod = BC004Y21_A1992ContagemResultadoQA_ParaCod[0];
            A1996ContagemResultadoQA_RespostaDe = BC004Y21_A1996ContagemResultadoQA_RespostaDe[0];
            n1996ContagemResultadoQA_RespostaDe = BC004Y21_n1996ContagemResultadoQA_RespostaDe[0];
            A1989ContagemResultadoQA_UserPesCod = BC004Y21_A1989ContagemResultadoQA_UserPesCod[0];
            n1989ContagemResultadoQA_UserPesCod = BC004Y21_n1989ContagemResultadoQA_UserPesCod[0];
            A1993ContagemResultadoQA_ParaPesCod = BC004Y21_A1993ContagemResultadoQA_ParaPesCod[0];
            n1993ContagemResultadoQA_ParaPesCod = BC004Y21_n1993ContagemResultadoQA_ParaPesCod[0];
         }
         Gx_mode = sMode220;
      }

      protected void ScanKeyEnd4Y220( )
      {
         pr_default.close(19);
      }

      protected void AfterConfirm4Y220( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4Y220( )
      {
         /* Before Insert Rules */
         A1986ContagemResultadoQA_DataHora = DateTimeUtil.ServerNow( context, "DEFAULT");
      }

      protected void BeforeUpdate4Y220( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4Y220( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4Y220( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4Y220( )
      {
         /* Before Validate Rules */
         if ( (0==A1996ContagemResultadoQA_RespostaDe) )
         {
            A1996ContagemResultadoQA_RespostaDe = 0;
            n1996ContagemResultadoQA_RespostaDe = false;
            n1996ContagemResultadoQA_RespostaDe = true;
         }
      }

      protected void DisableAttributes4Y220( )
      {
      }

      protected void AddRow4Y220( )
      {
         VarsToRow220( bcContagemResultadoQA) ;
      }

      protected void ReadRow4Y220( )
      {
         RowToVars220( bcContagemResultadoQA, 1) ;
      }

      protected void InitializeNonKey4Y220( )
      {
         A1986ContagemResultadoQA_DataHora = (DateTime)(DateTime.MinValue);
         A1991ContagemResultadoQA_ContratanteDoUser = 0;
         A1995ContagemResultadoQA_ContratanteDoPara = 0;
         A1998ContagemResultadoQA_Resposta = 0;
         A1985ContagemResultadoQA_OSCod = 0;
         A493ContagemResultado_DemandaFM = "";
         n493ContagemResultado_DemandaFM = false;
         A1987ContagemResultadoQA_Texto = "";
         A1988ContagemResultadoQA_UserCod = 0;
         A1989ContagemResultadoQA_UserPesCod = 0;
         n1989ContagemResultadoQA_UserPesCod = false;
         A1990ContagemResultadoQA_UserPesNom = "";
         n1990ContagemResultadoQA_UserPesNom = false;
         A1992ContagemResultadoQA_ParaCod = 0;
         A1993ContagemResultadoQA_ParaPesCod = 0;
         n1993ContagemResultadoQA_ParaPesCod = false;
         A1994ContagemResultadoQA_ParaPesNom = "";
         n1994ContagemResultadoQA_ParaPesNom = false;
         A1996ContagemResultadoQA_RespostaDe = 0;
         n1996ContagemResultadoQA_RespostaDe = false;
         Z1986ContagemResultadoQA_DataHora = (DateTime)(DateTime.MinValue);
         Z1985ContagemResultadoQA_OSCod = 0;
         Z1988ContagemResultadoQA_UserCod = 0;
         Z1992ContagemResultadoQA_ParaCod = 0;
         Z1996ContagemResultadoQA_RespostaDe = 0;
      }

      protected void InitAll4Y220( )
      {
         A1984ContagemResultadoQA_Codigo = 0;
         InitializeNonKey4Y220( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow220( SdtContagemResultadoQA obj220 )
      {
         obj220.gxTpr_Mode = Gx_mode;
         obj220.gxTpr_Contagemresultadoqa_datahora = A1986ContagemResultadoQA_DataHora;
         obj220.gxTpr_Contagemresultadoqa_contratantedouser = A1991ContagemResultadoQA_ContratanteDoUser;
         obj220.gxTpr_Contagemresultadoqa_contratantedopara = A1995ContagemResultadoQA_ContratanteDoPara;
         obj220.gxTpr_Contagemresultadoqa_resposta = A1998ContagemResultadoQA_Resposta;
         obj220.gxTpr_Contagemresultadoqa_oscod = A1985ContagemResultadoQA_OSCod;
         obj220.gxTpr_Contagemresultado_demandafm = A493ContagemResultado_DemandaFM;
         obj220.gxTpr_Contagemresultadoqa_texto = A1987ContagemResultadoQA_Texto;
         obj220.gxTpr_Contagemresultadoqa_usercod = A1988ContagemResultadoQA_UserCod;
         obj220.gxTpr_Contagemresultadoqa_userpescod = A1989ContagemResultadoQA_UserPesCod;
         obj220.gxTpr_Contagemresultadoqa_userpesnom = A1990ContagemResultadoQA_UserPesNom;
         obj220.gxTpr_Contagemresultadoqa_paracod = A1992ContagemResultadoQA_ParaCod;
         obj220.gxTpr_Contagemresultadoqa_parapescod = A1993ContagemResultadoQA_ParaPesCod;
         obj220.gxTpr_Contagemresultadoqa_parapesnom = A1994ContagemResultadoQA_ParaPesNom;
         obj220.gxTpr_Contagemresultadoqa_respostade = A1996ContagemResultadoQA_RespostaDe;
         obj220.gxTpr_Contagemresultadoqa_codigo = A1984ContagemResultadoQA_Codigo;
         obj220.gxTpr_Contagemresultadoqa_codigo_Z = Z1984ContagemResultadoQA_Codigo;
         obj220.gxTpr_Contagemresultadoqa_oscod_Z = Z1985ContagemResultadoQA_OSCod;
         obj220.gxTpr_Contagemresultado_demandafm_Z = Z493ContagemResultado_DemandaFM;
         obj220.gxTpr_Contagemresultadoqa_datahora_Z = Z1986ContagemResultadoQA_DataHora;
         obj220.gxTpr_Contagemresultadoqa_usercod_Z = Z1988ContagemResultadoQA_UserCod;
         obj220.gxTpr_Contagemresultadoqa_userpescod_Z = Z1989ContagemResultadoQA_UserPesCod;
         obj220.gxTpr_Contagemresultadoqa_userpesnom_Z = Z1990ContagemResultadoQA_UserPesNom;
         obj220.gxTpr_Contagemresultadoqa_contratantedouser_Z = Z1991ContagemResultadoQA_ContratanteDoUser;
         obj220.gxTpr_Contagemresultadoqa_paracod_Z = Z1992ContagemResultadoQA_ParaCod;
         obj220.gxTpr_Contagemresultadoqa_parapescod_Z = Z1993ContagemResultadoQA_ParaPesCod;
         obj220.gxTpr_Contagemresultadoqa_parapesnom_Z = Z1994ContagemResultadoQA_ParaPesNom;
         obj220.gxTpr_Contagemresultadoqa_contratantedopara_Z = Z1995ContagemResultadoQA_ContratanteDoPara;
         obj220.gxTpr_Contagemresultadoqa_respostade_Z = Z1996ContagemResultadoQA_RespostaDe;
         obj220.gxTpr_Contagemresultadoqa_resposta_Z = Z1998ContagemResultadoQA_Resposta;
         obj220.gxTpr_Contagemresultado_demandafm_N = (short)(Convert.ToInt16(n493ContagemResultado_DemandaFM));
         obj220.gxTpr_Contagemresultadoqa_userpescod_N = (short)(Convert.ToInt16(n1989ContagemResultadoQA_UserPesCod));
         obj220.gxTpr_Contagemresultadoqa_userpesnom_N = (short)(Convert.ToInt16(n1990ContagemResultadoQA_UserPesNom));
         obj220.gxTpr_Contagemresultadoqa_parapescod_N = (short)(Convert.ToInt16(n1993ContagemResultadoQA_ParaPesCod));
         obj220.gxTpr_Contagemresultadoqa_parapesnom_N = (short)(Convert.ToInt16(n1994ContagemResultadoQA_ParaPesNom));
         obj220.gxTpr_Contagemresultadoqa_respostade_N = (short)(Convert.ToInt16(n1996ContagemResultadoQA_RespostaDe));
         obj220.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow220( SdtContagemResultadoQA obj220 )
      {
         obj220.gxTpr_Contagemresultadoqa_codigo = A1984ContagemResultadoQA_Codigo;
         return  ;
      }

      public void RowToVars220( SdtContagemResultadoQA obj220 ,
                                int forceLoad )
      {
         Gx_mode = obj220.gxTpr_Mode;
         A1986ContagemResultadoQA_DataHora = obj220.gxTpr_Contagemresultadoqa_datahora;
         A1991ContagemResultadoQA_ContratanteDoUser = obj220.gxTpr_Contagemresultadoqa_contratantedouser;
         A1995ContagemResultadoQA_ContratanteDoPara = obj220.gxTpr_Contagemresultadoqa_contratantedopara;
         A1998ContagemResultadoQA_Resposta = obj220.gxTpr_Contagemresultadoqa_resposta;
         A1985ContagemResultadoQA_OSCod = obj220.gxTpr_Contagemresultadoqa_oscod;
         A493ContagemResultado_DemandaFM = obj220.gxTpr_Contagemresultado_demandafm;
         n493ContagemResultado_DemandaFM = false;
         A1987ContagemResultadoQA_Texto = obj220.gxTpr_Contagemresultadoqa_texto;
         A1988ContagemResultadoQA_UserCod = obj220.gxTpr_Contagemresultadoqa_usercod;
         A1989ContagemResultadoQA_UserPesCod = obj220.gxTpr_Contagemresultadoqa_userpescod;
         n1989ContagemResultadoQA_UserPesCod = false;
         A1990ContagemResultadoQA_UserPesNom = obj220.gxTpr_Contagemresultadoqa_userpesnom;
         n1990ContagemResultadoQA_UserPesNom = false;
         A1992ContagemResultadoQA_ParaCod = obj220.gxTpr_Contagemresultadoqa_paracod;
         A1993ContagemResultadoQA_ParaPesCod = obj220.gxTpr_Contagemresultadoqa_parapescod;
         n1993ContagemResultadoQA_ParaPesCod = false;
         A1994ContagemResultadoQA_ParaPesNom = obj220.gxTpr_Contagemresultadoqa_parapesnom;
         n1994ContagemResultadoQA_ParaPesNom = false;
         A1996ContagemResultadoQA_RespostaDe = obj220.gxTpr_Contagemresultadoqa_respostade;
         n1996ContagemResultadoQA_RespostaDe = false;
         A1984ContagemResultadoQA_Codigo = obj220.gxTpr_Contagemresultadoqa_codigo;
         Z1984ContagemResultadoQA_Codigo = obj220.gxTpr_Contagemresultadoqa_codigo_Z;
         Z1985ContagemResultadoQA_OSCod = obj220.gxTpr_Contagemresultadoqa_oscod_Z;
         Z493ContagemResultado_DemandaFM = obj220.gxTpr_Contagemresultado_demandafm_Z;
         Z1986ContagemResultadoQA_DataHora = obj220.gxTpr_Contagemresultadoqa_datahora_Z;
         Z1988ContagemResultadoQA_UserCod = obj220.gxTpr_Contagemresultadoqa_usercod_Z;
         Z1989ContagemResultadoQA_UserPesCod = obj220.gxTpr_Contagemresultadoqa_userpescod_Z;
         Z1990ContagemResultadoQA_UserPesNom = obj220.gxTpr_Contagemresultadoqa_userpesnom_Z;
         Z1991ContagemResultadoQA_ContratanteDoUser = obj220.gxTpr_Contagemresultadoqa_contratantedouser_Z;
         Z1992ContagemResultadoQA_ParaCod = obj220.gxTpr_Contagemresultadoqa_paracod_Z;
         Z1993ContagemResultadoQA_ParaPesCod = obj220.gxTpr_Contagemresultadoqa_parapescod_Z;
         Z1994ContagemResultadoQA_ParaPesNom = obj220.gxTpr_Contagemresultadoqa_parapesnom_Z;
         Z1995ContagemResultadoQA_ContratanteDoPara = obj220.gxTpr_Contagemresultadoqa_contratantedopara_Z;
         Z1996ContagemResultadoQA_RespostaDe = obj220.gxTpr_Contagemresultadoqa_respostade_Z;
         Z1998ContagemResultadoQA_Resposta = obj220.gxTpr_Contagemresultadoqa_resposta_Z;
         n493ContagemResultado_DemandaFM = (bool)(Convert.ToBoolean(obj220.gxTpr_Contagemresultado_demandafm_N));
         n1989ContagemResultadoQA_UserPesCod = (bool)(Convert.ToBoolean(obj220.gxTpr_Contagemresultadoqa_userpescod_N));
         n1990ContagemResultadoQA_UserPesNom = (bool)(Convert.ToBoolean(obj220.gxTpr_Contagemresultadoqa_userpesnom_N));
         n1993ContagemResultadoQA_ParaPesCod = (bool)(Convert.ToBoolean(obj220.gxTpr_Contagemresultadoqa_parapescod_N));
         n1994ContagemResultadoQA_ParaPesNom = (bool)(Convert.ToBoolean(obj220.gxTpr_Contagemresultadoqa_parapesnom_N));
         n1996ContagemResultadoQA_RespostaDe = (bool)(Convert.ToBoolean(obj220.gxTpr_Contagemresultadoqa_respostade_N));
         Gx_mode = obj220.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A1984ContagemResultadoQA_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey4Y220( ) ;
         ScanKeyStart4Y220( ) ;
         if ( RcdFound220 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1984ContagemResultadoQA_Codigo = A1984ContagemResultadoQA_Codigo;
         }
         ZM4Y220( -7) ;
         OnLoadActions4Y220( ) ;
         AddRow4Y220( ) ;
         ScanKeyEnd4Y220( ) ;
         if ( RcdFound220 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars220( bcContagemResultadoQA, 0) ;
         ScanKeyStart4Y220( ) ;
         if ( RcdFound220 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1984ContagemResultadoQA_Codigo = A1984ContagemResultadoQA_Codigo;
         }
         ZM4Y220( -7) ;
         OnLoadActions4Y220( ) ;
         AddRow4Y220( ) ;
         ScanKeyEnd4Y220( ) ;
         if ( RcdFound220 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars220( bcContagemResultadoQA, 0) ;
         nKeyPressed = 1;
         GetKey4Y220( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert4Y220( ) ;
         }
         else
         {
            if ( RcdFound220 == 1 )
            {
               if ( A1984ContagemResultadoQA_Codigo != Z1984ContagemResultadoQA_Codigo )
               {
                  A1984ContagemResultadoQA_Codigo = Z1984ContagemResultadoQA_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update4Y220( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A1984ContagemResultadoQA_Codigo != Z1984ContagemResultadoQA_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert4Y220( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert4Y220( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow220( bcContagemResultadoQA) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars220( bcContagemResultadoQA, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey4Y220( ) ;
         if ( RcdFound220 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A1984ContagemResultadoQA_Codigo != Z1984ContagemResultadoQA_Codigo )
            {
               A1984ContagemResultadoQA_Codigo = Z1984ContagemResultadoQA_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A1984ContagemResultadoQA_Codigo != Z1984ContagemResultadoQA_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(13);
         pr_default.close(14);
         pr_default.close(16);
         pr_default.close(15);
         pr_default.close(17);
         context.RollbackDataStores( "ContagemResultadoQA_BC");
         VarsToRow220( bcContagemResultadoQA) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcContagemResultadoQA.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcContagemResultadoQA.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcContagemResultadoQA )
         {
            bcContagemResultadoQA = (SdtContagemResultadoQA)(sdt);
            if ( StringUtil.StrCmp(bcContagemResultadoQA.gxTpr_Mode, "") == 0 )
            {
               bcContagemResultadoQA.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow220( bcContagemResultadoQA) ;
            }
            else
            {
               RowToVars220( bcContagemResultadoQA, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcContagemResultadoQA.gxTpr_Mode, "") == 0 )
            {
               bcContagemResultadoQA.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars220( bcContagemResultadoQA, 1) ;
         return  ;
      }

      public SdtContagemResultadoQA ContagemResultadoQA_BC
      {
         get {
            return bcContagemResultadoQA ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(13);
         pr_default.close(14);
         pr_default.close(16);
         pr_default.close(15);
         pr_default.close(17);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z1986ContagemResultadoQA_DataHora = (DateTime)(DateTime.MinValue);
         A1986ContagemResultadoQA_DataHora = (DateTime)(DateTime.MinValue);
         Z493ContagemResultado_DemandaFM = "";
         A493ContagemResultado_DemandaFM = "";
         Z1990ContagemResultadoQA_UserPesNom = "";
         A1990ContagemResultadoQA_UserPesNom = "";
         Z1994ContagemResultadoQA_ParaPesNom = "";
         A1994ContagemResultadoQA_ParaPesNom = "";
         Z1987ContagemResultadoQA_Texto = "";
         A1987ContagemResultadoQA_Texto = "";
         BC004Y10_A1984ContagemResultadoQA_Codigo = new int[1] ;
         BC004Y10_A1986ContagemResultadoQA_DataHora = new DateTime[] {DateTime.MinValue} ;
         BC004Y10_A493ContagemResultado_DemandaFM = new String[] {""} ;
         BC004Y10_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         BC004Y10_A1987ContagemResultadoQA_Texto = new String[] {""} ;
         BC004Y10_A1990ContagemResultadoQA_UserPesNom = new String[] {""} ;
         BC004Y10_n1990ContagemResultadoQA_UserPesNom = new bool[] {false} ;
         BC004Y10_A1994ContagemResultadoQA_ParaPesNom = new String[] {""} ;
         BC004Y10_n1994ContagemResultadoQA_ParaPesNom = new bool[] {false} ;
         BC004Y10_A1985ContagemResultadoQA_OSCod = new int[1] ;
         BC004Y10_A1988ContagemResultadoQA_UserCod = new int[1] ;
         BC004Y10_A1992ContagemResultadoQA_ParaCod = new int[1] ;
         BC004Y10_A1996ContagemResultadoQA_RespostaDe = new int[1] ;
         BC004Y10_n1996ContagemResultadoQA_RespostaDe = new bool[] {false} ;
         BC004Y10_A1989ContagemResultadoQA_UserPesCod = new int[1] ;
         BC004Y10_n1989ContagemResultadoQA_UserPesCod = new bool[] {false} ;
         BC004Y10_A1993ContagemResultadoQA_ParaPesCod = new int[1] ;
         BC004Y10_n1993ContagemResultadoQA_ParaPesCod = new bool[] {false} ;
         BC004Y4_A493ContagemResultado_DemandaFM = new String[] {""} ;
         BC004Y4_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         BC004Y5_A1989ContagemResultadoQA_UserPesCod = new int[1] ;
         BC004Y5_n1989ContagemResultadoQA_UserPesCod = new bool[] {false} ;
         BC004Y8_A1990ContagemResultadoQA_UserPesNom = new String[] {""} ;
         BC004Y8_n1990ContagemResultadoQA_UserPesNom = new bool[] {false} ;
         BC004Y6_A1993ContagemResultadoQA_ParaPesCod = new int[1] ;
         BC004Y6_n1993ContagemResultadoQA_ParaPesCod = new bool[] {false} ;
         BC004Y9_A1994ContagemResultadoQA_ParaPesNom = new String[] {""} ;
         BC004Y9_n1994ContagemResultadoQA_ParaPesNom = new bool[] {false} ;
         BC004Y7_A1996ContagemResultadoQA_RespostaDe = new int[1] ;
         BC004Y7_n1996ContagemResultadoQA_RespostaDe = new bool[] {false} ;
         BC004Y11_A1984ContagemResultadoQA_Codigo = new int[1] ;
         BC004Y3_A1984ContagemResultadoQA_Codigo = new int[1] ;
         BC004Y3_A1986ContagemResultadoQA_DataHora = new DateTime[] {DateTime.MinValue} ;
         BC004Y3_A1987ContagemResultadoQA_Texto = new String[] {""} ;
         BC004Y3_A1985ContagemResultadoQA_OSCod = new int[1] ;
         BC004Y3_A1988ContagemResultadoQA_UserCod = new int[1] ;
         BC004Y3_A1992ContagemResultadoQA_ParaCod = new int[1] ;
         BC004Y3_A1996ContagemResultadoQA_RespostaDe = new int[1] ;
         BC004Y3_n1996ContagemResultadoQA_RespostaDe = new bool[] {false} ;
         sMode220 = "";
         BC004Y2_A1984ContagemResultadoQA_Codigo = new int[1] ;
         BC004Y2_A1986ContagemResultadoQA_DataHora = new DateTime[] {DateTime.MinValue} ;
         BC004Y2_A1987ContagemResultadoQA_Texto = new String[] {""} ;
         BC004Y2_A1985ContagemResultadoQA_OSCod = new int[1] ;
         BC004Y2_A1988ContagemResultadoQA_UserCod = new int[1] ;
         BC004Y2_A1992ContagemResultadoQA_ParaCod = new int[1] ;
         BC004Y2_A1996ContagemResultadoQA_RespostaDe = new int[1] ;
         BC004Y2_n1996ContagemResultadoQA_RespostaDe = new bool[] {false} ;
         BC004Y12_A1984ContagemResultadoQA_Codigo = new int[1] ;
         BC004Y15_A493ContagemResultado_DemandaFM = new String[] {""} ;
         BC004Y15_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         BC004Y16_A1989ContagemResultadoQA_UserPesCod = new int[1] ;
         BC004Y16_n1989ContagemResultadoQA_UserPesCod = new bool[] {false} ;
         BC004Y17_A1990ContagemResultadoQA_UserPesNom = new String[] {""} ;
         BC004Y17_n1990ContagemResultadoQA_UserPesNom = new bool[] {false} ;
         BC004Y18_A1993ContagemResultadoQA_ParaPesCod = new int[1] ;
         BC004Y18_n1993ContagemResultadoQA_ParaPesCod = new bool[] {false} ;
         BC004Y19_A1994ContagemResultadoQA_ParaPesNom = new String[] {""} ;
         BC004Y19_n1994ContagemResultadoQA_ParaPesNom = new bool[] {false} ;
         BC004Y20_A1996ContagemResultadoQA_RespostaDe = new int[1] ;
         BC004Y20_n1996ContagemResultadoQA_RespostaDe = new bool[] {false} ;
         BC004Y21_A1984ContagemResultadoQA_Codigo = new int[1] ;
         BC004Y21_A1986ContagemResultadoQA_DataHora = new DateTime[] {DateTime.MinValue} ;
         BC004Y21_A493ContagemResultado_DemandaFM = new String[] {""} ;
         BC004Y21_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         BC004Y21_A1987ContagemResultadoQA_Texto = new String[] {""} ;
         BC004Y21_A1990ContagemResultadoQA_UserPesNom = new String[] {""} ;
         BC004Y21_n1990ContagemResultadoQA_UserPesNom = new bool[] {false} ;
         BC004Y21_A1994ContagemResultadoQA_ParaPesNom = new String[] {""} ;
         BC004Y21_n1994ContagemResultadoQA_ParaPesNom = new bool[] {false} ;
         BC004Y21_A1985ContagemResultadoQA_OSCod = new int[1] ;
         BC004Y21_A1988ContagemResultadoQA_UserCod = new int[1] ;
         BC004Y21_A1992ContagemResultadoQA_ParaCod = new int[1] ;
         BC004Y21_A1996ContagemResultadoQA_RespostaDe = new int[1] ;
         BC004Y21_n1996ContagemResultadoQA_RespostaDe = new bool[] {false} ;
         BC004Y21_A1989ContagemResultadoQA_UserPesCod = new int[1] ;
         BC004Y21_n1989ContagemResultadoQA_UserPesCod = new bool[] {false} ;
         BC004Y21_A1993ContagemResultadoQA_ParaPesCod = new int[1] ;
         BC004Y21_n1993ContagemResultadoQA_ParaPesCod = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadoqa_bc__default(),
            new Object[][] {
                new Object[] {
               BC004Y2_A1984ContagemResultadoQA_Codigo, BC004Y2_A1986ContagemResultadoQA_DataHora, BC004Y2_A1987ContagemResultadoQA_Texto, BC004Y2_A1985ContagemResultadoQA_OSCod, BC004Y2_A1988ContagemResultadoQA_UserCod, BC004Y2_A1992ContagemResultadoQA_ParaCod, BC004Y2_A1996ContagemResultadoQA_RespostaDe, BC004Y2_n1996ContagemResultadoQA_RespostaDe
               }
               , new Object[] {
               BC004Y3_A1984ContagemResultadoQA_Codigo, BC004Y3_A1986ContagemResultadoQA_DataHora, BC004Y3_A1987ContagemResultadoQA_Texto, BC004Y3_A1985ContagemResultadoQA_OSCod, BC004Y3_A1988ContagemResultadoQA_UserCod, BC004Y3_A1992ContagemResultadoQA_ParaCod, BC004Y3_A1996ContagemResultadoQA_RespostaDe, BC004Y3_n1996ContagemResultadoQA_RespostaDe
               }
               , new Object[] {
               BC004Y4_A493ContagemResultado_DemandaFM, BC004Y4_n493ContagemResultado_DemandaFM
               }
               , new Object[] {
               BC004Y5_A1989ContagemResultadoQA_UserPesCod, BC004Y5_n1989ContagemResultadoQA_UserPesCod
               }
               , new Object[] {
               BC004Y6_A1993ContagemResultadoQA_ParaPesCod, BC004Y6_n1993ContagemResultadoQA_ParaPesCod
               }
               , new Object[] {
               BC004Y7_A1996ContagemResultadoQA_RespostaDe
               }
               , new Object[] {
               BC004Y8_A1990ContagemResultadoQA_UserPesNom, BC004Y8_n1990ContagemResultadoQA_UserPesNom
               }
               , new Object[] {
               BC004Y9_A1994ContagemResultadoQA_ParaPesNom, BC004Y9_n1994ContagemResultadoQA_ParaPesNom
               }
               , new Object[] {
               BC004Y10_A1984ContagemResultadoQA_Codigo, BC004Y10_A1986ContagemResultadoQA_DataHora, BC004Y10_A493ContagemResultado_DemandaFM, BC004Y10_n493ContagemResultado_DemandaFM, BC004Y10_A1987ContagemResultadoQA_Texto, BC004Y10_A1990ContagemResultadoQA_UserPesNom, BC004Y10_n1990ContagemResultadoQA_UserPesNom, BC004Y10_A1994ContagemResultadoQA_ParaPesNom, BC004Y10_n1994ContagemResultadoQA_ParaPesNom, BC004Y10_A1985ContagemResultadoQA_OSCod,
               BC004Y10_A1988ContagemResultadoQA_UserCod, BC004Y10_A1992ContagemResultadoQA_ParaCod, BC004Y10_A1996ContagemResultadoQA_RespostaDe, BC004Y10_n1996ContagemResultadoQA_RespostaDe, BC004Y10_A1989ContagemResultadoQA_UserPesCod, BC004Y10_n1989ContagemResultadoQA_UserPesCod, BC004Y10_A1993ContagemResultadoQA_ParaPesCod, BC004Y10_n1993ContagemResultadoQA_ParaPesCod
               }
               , new Object[] {
               BC004Y11_A1984ContagemResultadoQA_Codigo
               }
               , new Object[] {
               BC004Y12_A1984ContagemResultadoQA_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC004Y15_A493ContagemResultado_DemandaFM, BC004Y15_n493ContagemResultado_DemandaFM
               }
               , new Object[] {
               BC004Y16_A1989ContagemResultadoQA_UserPesCod, BC004Y16_n1989ContagemResultadoQA_UserPesCod
               }
               , new Object[] {
               BC004Y17_A1990ContagemResultadoQA_UserPesNom, BC004Y17_n1990ContagemResultadoQA_UserPesNom
               }
               , new Object[] {
               BC004Y18_A1993ContagemResultadoQA_ParaPesCod, BC004Y18_n1993ContagemResultadoQA_ParaPesCod
               }
               , new Object[] {
               BC004Y19_A1994ContagemResultadoQA_ParaPesNom, BC004Y19_n1994ContagemResultadoQA_ParaPesNom
               }
               , new Object[] {
               BC004Y20_A1996ContagemResultadoQA_RespostaDe
               }
               , new Object[] {
               BC004Y21_A1984ContagemResultadoQA_Codigo, BC004Y21_A1986ContagemResultadoQA_DataHora, BC004Y21_A493ContagemResultado_DemandaFM, BC004Y21_n493ContagemResultado_DemandaFM, BC004Y21_A1987ContagemResultadoQA_Texto, BC004Y21_A1990ContagemResultadoQA_UserPesNom, BC004Y21_n1990ContagemResultadoQA_UserPesNom, BC004Y21_A1994ContagemResultadoQA_ParaPesNom, BC004Y21_n1994ContagemResultadoQA_ParaPesNom, BC004Y21_A1985ContagemResultadoQA_OSCod,
               BC004Y21_A1988ContagemResultadoQA_UserCod, BC004Y21_A1992ContagemResultadoQA_ParaCod, BC004Y21_A1996ContagemResultadoQA_RespostaDe, BC004Y21_n1996ContagemResultadoQA_RespostaDe, BC004Y21_A1989ContagemResultadoQA_UserPesCod, BC004Y21_n1989ContagemResultadoQA_UserPesCod, BC004Y21_A1993ContagemResultadoQA_ParaPesCod, BC004Y21_n1993ContagemResultadoQA_ParaPesCod
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound220 ;
      private int trnEnded ;
      private int Z1984ContagemResultadoQA_Codigo ;
      private int A1984ContagemResultadoQA_Codigo ;
      private int Z1985ContagemResultadoQA_OSCod ;
      private int A1985ContagemResultadoQA_OSCod ;
      private int Z1988ContagemResultadoQA_UserCod ;
      private int A1988ContagemResultadoQA_UserCod ;
      private int Z1992ContagemResultadoQA_ParaCod ;
      private int A1992ContagemResultadoQA_ParaCod ;
      private int Z1996ContagemResultadoQA_RespostaDe ;
      private int A1996ContagemResultadoQA_RespostaDe ;
      private int Z1991ContagemResultadoQA_ContratanteDoUser ;
      private int A1991ContagemResultadoQA_ContratanteDoUser ;
      private int Z1995ContagemResultadoQA_ContratanteDoPara ;
      private int A1995ContagemResultadoQA_ContratanteDoPara ;
      private int Z1998ContagemResultadoQA_Resposta ;
      private int A1998ContagemResultadoQA_Resposta ;
      private int Z1989ContagemResultadoQA_UserPesCod ;
      private int A1989ContagemResultadoQA_UserPesCod ;
      private int Z1993ContagemResultadoQA_ParaPesCod ;
      private int A1993ContagemResultadoQA_ParaPesCod ;
      private int GXt_int1 ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String Z1990ContagemResultadoQA_UserPesNom ;
      private String A1990ContagemResultadoQA_UserPesNom ;
      private String Z1994ContagemResultadoQA_ParaPesNom ;
      private String A1994ContagemResultadoQA_ParaPesNom ;
      private String sMode220 ;
      private DateTime Z1986ContagemResultadoQA_DataHora ;
      private DateTime A1986ContagemResultadoQA_DataHora ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n1990ContagemResultadoQA_UserPesNom ;
      private bool n1994ContagemResultadoQA_ParaPesNom ;
      private bool n1996ContagemResultadoQA_RespostaDe ;
      private bool n1989ContagemResultadoQA_UserPesCod ;
      private bool n1993ContagemResultadoQA_ParaPesCod ;
      private String Z1987ContagemResultadoQA_Texto ;
      private String A1987ContagemResultadoQA_Texto ;
      private String Z493ContagemResultado_DemandaFM ;
      private String A493ContagemResultado_DemandaFM ;
      private SdtContagemResultadoQA bcContagemResultadoQA ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC004Y10_A1984ContagemResultadoQA_Codigo ;
      private DateTime[] BC004Y10_A1986ContagemResultadoQA_DataHora ;
      private String[] BC004Y10_A493ContagemResultado_DemandaFM ;
      private bool[] BC004Y10_n493ContagemResultado_DemandaFM ;
      private String[] BC004Y10_A1987ContagemResultadoQA_Texto ;
      private String[] BC004Y10_A1990ContagemResultadoQA_UserPesNom ;
      private bool[] BC004Y10_n1990ContagemResultadoQA_UserPesNom ;
      private String[] BC004Y10_A1994ContagemResultadoQA_ParaPesNom ;
      private bool[] BC004Y10_n1994ContagemResultadoQA_ParaPesNom ;
      private int[] BC004Y10_A1985ContagemResultadoQA_OSCod ;
      private int[] BC004Y10_A1988ContagemResultadoQA_UserCod ;
      private int[] BC004Y10_A1992ContagemResultadoQA_ParaCod ;
      private int[] BC004Y10_A1996ContagemResultadoQA_RespostaDe ;
      private bool[] BC004Y10_n1996ContagemResultadoQA_RespostaDe ;
      private int[] BC004Y10_A1989ContagemResultadoQA_UserPesCod ;
      private bool[] BC004Y10_n1989ContagemResultadoQA_UserPesCod ;
      private int[] BC004Y10_A1993ContagemResultadoQA_ParaPesCod ;
      private bool[] BC004Y10_n1993ContagemResultadoQA_ParaPesCod ;
      private String[] BC004Y4_A493ContagemResultado_DemandaFM ;
      private bool[] BC004Y4_n493ContagemResultado_DemandaFM ;
      private int[] BC004Y5_A1989ContagemResultadoQA_UserPesCod ;
      private bool[] BC004Y5_n1989ContagemResultadoQA_UserPesCod ;
      private String[] BC004Y8_A1990ContagemResultadoQA_UserPesNom ;
      private bool[] BC004Y8_n1990ContagemResultadoQA_UserPesNom ;
      private int[] BC004Y6_A1993ContagemResultadoQA_ParaPesCod ;
      private bool[] BC004Y6_n1993ContagemResultadoQA_ParaPesCod ;
      private String[] BC004Y9_A1994ContagemResultadoQA_ParaPesNom ;
      private bool[] BC004Y9_n1994ContagemResultadoQA_ParaPesNom ;
      private int[] BC004Y7_A1996ContagemResultadoQA_RespostaDe ;
      private bool[] BC004Y7_n1996ContagemResultadoQA_RespostaDe ;
      private int[] BC004Y11_A1984ContagemResultadoQA_Codigo ;
      private int[] BC004Y3_A1984ContagemResultadoQA_Codigo ;
      private DateTime[] BC004Y3_A1986ContagemResultadoQA_DataHora ;
      private String[] BC004Y3_A1987ContagemResultadoQA_Texto ;
      private int[] BC004Y3_A1985ContagemResultadoQA_OSCod ;
      private int[] BC004Y3_A1988ContagemResultadoQA_UserCod ;
      private int[] BC004Y3_A1992ContagemResultadoQA_ParaCod ;
      private int[] BC004Y3_A1996ContagemResultadoQA_RespostaDe ;
      private bool[] BC004Y3_n1996ContagemResultadoQA_RespostaDe ;
      private int[] BC004Y2_A1984ContagemResultadoQA_Codigo ;
      private DateTime[] BC004Y2_A1986ContagemResultadoQA_DataHora ;
      private String[] BC004Y2_A1987ContagemResultadoQA_Texto ;
      private int[] BC004Y2_A1985ContagemResultadoQA_OSCod ;
      private int[] BC004Y2_A1988ContagemResultadoQA_UserCod ;
      private int[] BC004Y2_A1992ContagemResultadoQA_ParaCod ;
      private int[] BC004Y2_A1996ContagemResultadoQA_RespostaDe ;
      private bool[] BC004Y2_n1996ContagemResultadoQA_RespostaDe ;
      private int[] BC004Y12_A1984ContagemResultadoQA_Codigo ;
      private String[] BC004Y15_A493ContagemResultado_DemandaFM ;
      private bool[] BC004Y15_n493ContagemResultado_DemandaFM ;
      private int[] BC004Y16_A1989ContagemResultadoQA_UserPesCod ;
      private bool[] BC004Y16_n1989ContagemResultadoQA_UserPesCod ;
      private String[] BC004Y17_A1990ContagemResultadoQA_UserPesNom ;
      private bool[] BC004Y17_n1990ContagemResultadoQA_UserPesNom ;
      private int[] BC004Y18_A1993ContagemResultadoQA_ParaPesCod ;
      private bool[] BC004Y18_n1993ContagemResultadoQA_ParaPesCod ;
      private String[] BC004Y19_A1994ContagemResultadoQA_ParaPesNom ;
      private bool[] BC004Y19_n1994ContagemResultadoQA_ParaPesNom ;
      private int[] BC004Y20_A1996ContagemResultadoQA_RespostaDe ;
      private bool[] BC004Y20_n1996ContagemResultadoQA_RespostaDe ;
      private int[] BC004Y21_A1984ContagemResultadoQA_Codigo ;
      private DateTime[] BC004Y21_A1986ContagemResultadoQA_DataHora ;
      private String[] BC004Y21_A493ContagemResultado_DemandaFM ;
      private bool[] BC004Y21_n493ContagemResultado_DemandaFM ;
      private String[] BC004Y21_A1987ContagemResultadoQA_Texto ;
      private String[] BC004Y21_A1990ContagemResultadoQA_UserPesNom ;
      private bool[] BC004Y21_n1990ContagemResultadoQA_UserPesNom ;
      private String[] BC004Y21_A1994ContagemResultadoQA_ParaPesNom ;
      private bool[] BC004Y21_n1994ContagemResultadoQA_ParaPesNom ;
      private int[] BC004Y21_A1985ContagemResultadoQA_OSCod ;
      private int[] BC004Y21_A1988ContagemResultadoQA_UserCod ;
      private int[] BC004Y21_A1992ContagemResultadoQA_ParaCod ;
      private int[] BC004Y21_A1996ContagemResultadoQA_RespostaDe ;
      private bool[] BC004Y21_n1996ContagemResultadoQA_RespostaDe ;
      private int[] BC004Y21_A1989ContagemResultadoQA_UserPesCod ;
      private bool[] BC004Y21_n1989ContagemResultadoQA_UserPesCod ;
      private int[] BC004Y21_A1993ContagemResultadoQA_ParaPesCod ;
      private bool[] BC004Y21_n1993ContagemResultadoQA_ParaPesCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
   }

   public class contagemresultadoqa_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC004Y10 ;
          prmBC004Y10 = new Object[] {
          new Object[] {"@ContagemResultadoQA_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004Y4 ;
          prmBC004Y4 = new Object[] {
          new Object[] {"@ContagemResultadoQA_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004Y5 ;
          prmBC004Y5 = new Object[] {
          new Object[] {"@ContagemResultadoQA_UserCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004Y8 ;
          prmBC004Y8 = new Object[] {
          new Object[] {"@ContagemResultadoQA_UserPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004Y6 ;
          prmBC004Y6 = new Object[] {
          new Object[] {"@ContagemResultadoQA_ParaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004Y9 ;
          prmBC004Y9 = new Object[] {
          new Object[] {"@ContagemResultadoQA_ParaPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004Y7 ;
          prmBC004Y7 = new Object[] {
          new Object[] {"@ContagemResultadoQA_RespostaDe",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004Y11 ;
          prmBC004Y11 = new Object[] {
          new Object[] {"@ContagemResultadoQA_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004Y3 ;
          prmBC004Y3 = new Object[] {
          new Object[] {"@ContagemResultadoQA_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004Y2 ;
          prmBC004Y2 = new Object[] {
          new Object[] {"@ContagemResultadoQA_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004Y12 ;
          prmBC004Y12 = new Object[] {
          new Object[] {"@ContagemResultadoQA_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoQA_Texto",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ContagemResultadoQA_OSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoQA_UserCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoQA_ParaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoQA_RespostaDe",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004Y13 ;
          prmBC004Y13 = new Object[] {
          new Object[] {"@ContagemResultadoQA_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoQA_Texto",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ContagemResultadoQA_OSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoQA_UserCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoQA_ParaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoQA_RespostaDe",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoQA_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004Y14 ;
          prmBC004Y14 = new Object[] {
          new Object[] {"@ContagemResultadoQA_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004Y15 ;
          prmBC004Y15 = new Object[] {
          new Object[] {"@ContagemResultadoQA_OSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004Y16 ;
          prmBC004Y16 = new Object[] {
          new Object[] {"@ContagemResultadoQA_UserCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004Y17 ;
          prmBC004Y17 = new Object[] {
          new Object[] {"@ContagemResultadoQA_UserPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004Y18 ;
          prmBC004Y18 = new Object[] {
          new Object[] {"@ContagemResultadoQA_ParaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004Y19 ;
          prmBC004Y19 = new Object[] {
          new Object[] {"@ContagemResultadoQA_ParaPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004Y20 ;
          prmBC004Y20 = new Object[] {
          new Object[] {"@ContagemResultadoQA_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004Y21 ;
          prmBC004Y21 = new Object[] {
          new Object[] {"@ContagemResultadoQA_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC004Y2", "SELECT [ContagemResultadoQA_Codigo], [ContagemResultadoQA_DataHora], [ContagemResultadoQA_Texto], [ContagemResultadoQA_OSCod] AS ContagemResultadoQA_OSCod, [ContagemResultadoQA_UserCod] AS ContagemResultadoQA_UserCod, [ContagemResultadoQA_ParaCod] AS ContagemResultadoQA_ParaCod, [ContagemResultadoQA_RespostaDe] AS ContagemResultadoQA_RespostaDe FROM [ContagemResultadoQA] WITH (UPDLOCK) WHERE [ContagemResultadoQA_Codigo] = @ContagemResultadoQA_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004Y2,1,0,true,false )
             ,new CursorDef("BC004Y3", "SELECT [ContagemResultadoQA_Codigo], [ContagemResultadoQA_DataHora], [ContagemResultadoQA_Texto], [ContagemResultadoQA_OSCod] AS ContagemResultadoQA_OSCod, [ContagemResultadoQA_UserCod] AS ContagemResultadoQA_UserCod, [ContagemResultadoQA_ParaCod] AS ContagemResultadoQA_ParaCod, [ContagemResultadoQA_RespostaDe] AS ContagemResultadoQA_RespostaDe FROM [ContagemResultadoQA] WITH (NOLOCK) WHERE [ContagemResultadoQA_Codigo] = @ContagemResultadoQA_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004Y3,1,0,true,false )
             ,new CursorDef("BC004Y4", "SELECT [ContagemResultado_DemandaFM] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoQA_OSCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004Y4,1,0,true,false )
             ,new CursorDef("BC004Y5", "SELECT [Usuario_PessoaCod] AS ContagemResultadoQA_UserPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoQA_UserCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004Y5,1,0,true,false )
             ,new CursorDef("BC004Y6", "SELECT [Usuario_PessoaCod] AS ContagemResultadoQA_ParaPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoQA_ParaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004Y6,1,0,true,false )
             ,new CursorDef("BC004Y7", "SELECT [ContagemResultadoQA_Codigo] AS ContagemResultadoQA_RespostaDe FROM [ContagemResultadoQA] WITH (NOLOCK) WHERE [ContagemResultadoQA_Codigo] = @ContagemResultadoQA_RespostaDe ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004Y7,1,0,true,false )
             ,new CursorDef("BC004Y8", "SELECT [Pessoa_Nome] AS ContagemResultadoQA_UserPesNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoQA_UserPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004Y8,1,0,true,false )
             ,new CursorDef("BC004Y9", "SELECT [Pessoa_Nome] AS ContagemResultadoQA_ParaPesNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoQA_ParaPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004Y9,1,0,true,false )
             ,new CursorDef("BC004Y10", "SELECT TM1.[ContagemResultadoQA_Codigo], TM1.[ContagemResultadoQA_DataHora], T2.[ContagemResultado_DemandaFM], TM1.[ContagemResultadoQA_Texto], T4.[Pessoa_Nome] AS ContagemResultadoQA_UserPesNom, T6.[Pessoa_Nome] AS ContagemResultadoQA_ParaPesNom, TM1.[ContagemResultadoQA_OSCod] AS ContagemResultadoQA_OSCod, TM1.[ContagemResultadoQA_UserCod] AS ContagemResultadoQA_UserCod, TM1.[ContagemResultadoQA_ParaCod] AS ContagemResultadoQA_ParaCod, TM1.[ContagemResultadoQA_RespostaDe] AS ContagemResultadoQA_RespostaDe, T3.[Usuario_PessoaCod] AS ContagemResultadoQA_UserPesCod, T5.[Usuario_PessoaCod] AS ContagemResultadoQA_ParaPesCod FROM ((((([ContagemResultadoQA] TM1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = TM1.[ContagemResultadoQA_OSCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = TM1.[ContagemResultadoQA_UserCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) INNER JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = TM1.[ContagemResultadoQA_ParaCod]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) WHERE TM1.[ContagemResultadoQA_Codigo] = @ContagemResultadoQA_Codigo ORDER BY TM1.[ContagemResultadoQA_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004Y10,100,0,true,false )
             ,new CursorDef("BC004Y11", "SELECT [ContagemResultadoQA_Codigo] FROM [ContagemResultadoQA] WITH (NOLOCK) WHERE [ContagemResultadoQA_Codigo] = @ContagemResultadoQA_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004Y11,1,0,true,false )
             ,new CursorDef("BC004Y12", "INSERT INTO [ContagemResultadoQA]([ContagemResultadoQA_DataHora], [ContagemResultadoQA_Texto], [ContagemResultadoQA_OSCod], [ContagemResultadoQA_UserCod], [ContagemResultadoQA_ParaCod], [ContagemResultadoQA_RespostaDe]) VALUES(@ContagemResultadoQA_DataHora, @ContagemResultadoQA_Texto, @ContagemResultadoQA_OSCod, @ContagemResultadoQA_UserCod, @ContagemResultadoQA_ParaCod, @ContagemResultadoQA_RespostaDe); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC004Y12)
             ,new CursorDef("BC004Y13", "UPDATE [ContagemResultadoQA] SET [ContagemResultadoQA_DataHora]=@ContagemResultadoQA_DataHora, [ContagemResultadoQA_Texto]=@ContagemResultadoQA_Texto, [ContagemResultadoQA_OSCod]=@ContagemResultadoQA_OSCod, [ContagemResultadoQA_UserCod]=@ContagemResultadoQA_UserCod, [ContagemResultadoQA_ParaCod]=@ContagemResultadoQA_ParaCod, [ContagemResultadoQA_RespostaDe]=@ContagemResultadoQA_RespostaDe  WHERE [ContagemResultadoQA_Codigo] = @ContagemResultadoQA_Codigo", GxErrorMask.GX_NOMASK,prmBC004Y13)
             ,new CursorDef("BC004Y14", "DELETE FROM [ContagemResultadoQA]  WHERE [ContagemResultadoQA_Codigo] = @ContagemResultadoQA_Codigo", GxErrorMask.GX_NOMASK,prmBC004Y14)
             ,new CursorDef("BC004Y15", "SELECT [ContagemResultado_DemandaFM] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoQA_OSCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004Y15,1,0,true,false )
             ,new CursorDef("BC004Y16", "SELECT [Usuario_PessoaCod] AS ContagemResultadoQA_UserPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoQA_UserCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004Y16,1,0,true,false )
             ,new CursorDef("BC004Y17", "SELECT [Pessoa_Nome] AS ContagemResultadoQA_UserPesNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoQA_UserPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004Y17,1,0,true,false )
             ,new CursorDef("BC004Y18", "SELECT [Usuario_PessoaCod] AS ContagemResultadoQA_ParaPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoQA_ParaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004Y18,1,0,true,false )
             ,new CursorDef("BC004Y19", "SELECT [Pessoa_Nome] AS ContagemResultadoQA_ParaPesNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoQA_ParaPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004Y19,1,0,true,false )
             ,new CursorDef("BC004Y20", "SELECT TOP 1 [ContagemResultadoQA_Codigo] AS ContagemResultadoQA_RespostaDe FROM [ContagemResultadoQA] WITH (NOLOCK) WHERE [ContagemResultadoQA_RespostaDe] = @ContagemResultadoQA_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004Y20,1,0,true,true )
             ,new CursorDef("BC004Y21", "SELECT TM1.[ContagemResultadoQA_Codigo], TM1.[ContagemResultadoQA_DataHora], T2.[ContagemResultado_DemandaFM], TM1.[ContagemResultadoQA_Texto], T4.[Pessoa_Nome] AS ContagemResultadoQA_UserPesNom, T6.[Pessoa_Nome] AS ContagemResultadoQA_ParaPesNom, TM1.[ContagemResultadoQA_OSCod] AS ContagemResultadoQA_OSCod, TM1.[ContagemResultadoQA_UserCod] AS ContagemResultadoQA_UserCod, TM1.[ContagemResultadoQA_ParaCod] AS ContagemResultadoQA_ParaCod, TM1.[ContagemResultadoQA_RespostaDe] AS ContagemResultadoQA_RespostaDe, T3.[Usuario_PessoaCod] AS ContagemResultadoQA_UserPesCod, T5.[Usuario_PessoaCod] AS ContagemResultadoQA_ParaPesCod FROM ((((([ContagemResultadoQA] TM1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = TM1.[ContagemResultadoQA_OSCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = TM1.[ContagemResultadoQA_UserCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) INNER JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = TM1.[ContagemResultadoQA_ParaCod]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) WHERE TM1.[ContagemResultadoQA_Codigo] = @ContagemResultadoQA_Codigo ORDER BY TM1.[ContagemResultadoQA_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004Y21,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(7);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(7);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                ((int[]) buf[12])[0] = rslt.getInt(10) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(10);
                ((int[]) buf[14])[0] = rslt.getInt(11) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(11);
                ((int[]) buf[16])[0] = rslt.getInt(12) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(12);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                ((int[]) buf[12])[0] = rslt.getInt(10) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(10);
                ((int[]) buf[14])[0] = rslt.getInt(11) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(11);
                ((int[]) buf[16])[0] = rslt.getInt(12) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(12);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                stmt.SetParameter(5, (int)parms[4]);
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[6]);
                }
                return;
             case 11 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                stmt.SetParameter(5, (int)parms[4]);
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[6]);
                }
                stmt.SetParameter(7, (int)parms[7]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
