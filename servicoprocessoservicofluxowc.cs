/*
               File: ServicoProcessoServicoFluxoWC
        Description: Servico Processo Servico Fluxo WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:31:44.75
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class servicoprocessoservicofluxowc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public servicoprocessoservicofluxowc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public servicoprocessoservicofluxowc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ServicoFluxo_ServicoCod )
      {
         this.AV7ServicoFluxo_ServicoCod = aP0_ServicoFluxo_ServicoCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbServicoFluxo_ServicoTpHrq = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7ServicoFluxo_ServicoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ServicoFluxo_ServicoCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7ServicoFluxo_ServicoCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_27 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_27_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_27_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
                  AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
                  AV7ServicoFluxo_ServicoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ServicoFluxo_ServicoCod), 6, 0)));
                  AV22Pgmname = GetNextPar( );
                  A1528ServicoFluxo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV7ServicoFluxo_ServicoCod, AV22Pgmname, A1528ServicoFluxo_Codigo, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAM02( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV22Pgmname = "ServicoProcessoServicoFluxoWC";
               context.Gx_err = 0;
               WSM02( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Servico Processo Servico Fluxo WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299314484");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("servicoprocessoservicofluxowc.aspx") + "?" + UrlEncode("" +AV7ServicoFluxo_ServicoCod)+"\">") ;
               GxWebStd.gx_hidden_field( context, "_EventName", "");
               GxWebStd.gx_hidden_field( context, "_EventGridId", "");
               GxWebStd.gx_hidden_field( context, "_EventRowId", "");
               context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            }
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_27", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_27), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7ServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7ServicoFluxo_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vSERVICOFLUXO_SERVICOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ServicoFluxo_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV22Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormM02( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("servicoprocessoservicofluxowc.js", "?20205299314490");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "</form>") ;
            }
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ServicoProcessoServicoFluxoWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Servico Processo Servico Fluxo WC" ;
      }

      protected void WBM00( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "servicoprocessoservicofluxowc.aspx");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
            }
            wb_table1_2_M02( true) ;
         }
         else
         {
            wb_table1_2_M02( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_M02e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServicoFluxo_ServicoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1522ServicoFluxo_ServicoCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1522ServicoFluxo_ServicoCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServicoFluxo_ServicoCod_Jsonclick, 0, "Attribute", "", "", "", edtServicoFluxo_ServicoCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ServicoProcessoServicoFluxoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
         }
         wbLoad = true;
      }

      protected void STARTM02( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Servico Processo Servico Fluxo WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPM00( ) ;
            }
         }
      }

      protected void WSM02( )
      {
         STARTM02( ) ;
         EVTM02( ) ;
      }

      protected void EVTM02( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPM00( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPM00( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11M02 */
                                    E11M02 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPM00( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12M02 */
                                    E12M02 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPM00( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13M02 */
                                    E13M02 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPM00( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPM00( ) ;
                              }
                              sEvt = cgiGet( sPrefix+"GRIDPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPM00( ) ;
                              }
                              nGXsfl_27_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_27_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_27_idx), 4, 0)), 4, "0");
                              SubsflControlProps_272( ) ;
                              AV15Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV15Update)) ? AV20Update_GXI : context.convertURL( context.PathToRelativeUrl( AV15Update))));
                              AV16Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV16Delete)) ? AV21Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV16Delete))));
                              A1528ServicoFluxo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServicoFluxo_Codigo_Internalname), ",", "."));
                              cmbServicoFluxo_ServicoTpHrq.Name = cmbServicoFluxo_ServicoTpHrq_Internalname;
                              cmbServicoFluxo_ServicoTpHrq.CurrentValue = cgiGet( cmbServicoFluxo_ServicoTpHrq_Internalname);
                              A1533ServicoFluxo_ServicoTpHrq = (short)(NumberUtil.Val( cgiGet( cmbServicoFluxo_ServicoTpHrq_Internalname), "."));
                              n1533ServicoFluxo_ServicoTpHrq = false;
                              A1532ServicoFluxo_Ordem = (short)(context.localUtil.CToN( cgiGet( edtServicoFluxo_Ordem_Internalname), ",", "."));
                              n1532ServicoFluxo_Ordem = false;
                              A1526ServicoFluxo_ServicoPos = (int)(context.localUtil.CToN( cgiGet( edtServicoFluxo_ServicoPos_Internalname), ",", "."));
                              n1526ServicoFluxo_ServicoPos = false;
                              A1527ServicoFluxo_SrvPosSigla = StringUtil.Upper( cgiGet( edtServicoFluxo_SrvPosSigla_Internalname));
                              n1527ServicoFluxo_SrvPosSigla = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E14M02 */
                                          E14M02 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E15M02 */
                                          E15M02 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E16M02 */
                                          E16M02 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPM00( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEM02( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormM02( ) ;
            }
         }
      }

      protected void PAM02( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            GXCCtl = "SERVICOFLUXO_SERVICOTPHRQ_" + sGXsfl_27_idx;
            cmbServicoFluxo_ServicoTpHrq.Name = GXCCtl;
            cmbServicoFluxo_ServicoTpHrq.WebTags = "";
            cmbServicoFluxo_ServicoTpHrq.addItem("1", "Prim�rio", 0);
            cmbServicoFluxo_ServicoTpHrq.addItem("2", "Secund�rio", 0);
            if ( cmbServicoFluxo_ServicoTpHrq.ItemCount > 0 )
            {
               A1533ServicoFluxo_ServicoTpHrq = (short)(NumberUtil.Val( cmbServicoFluxo_ServicoTpHrq.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1533ServicoFluxo_ServicoTpHrq), 4, 0))), "."));
               n1533ServicoFluxo_ServicoTpHrq = false;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_272( ) ;
         while ( nGXsfl_27_idx <= nRC_GXsfl_27 )
         {
            sendrow_272( ) ;
            nGXsfl_27_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_27_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_27_idx+1));
            sGXsfl_27_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_27_idx), 4, 0)), 4, "0");
            SubsflControlProps_272( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       int AV7ServicoFluxo_ServicoCod ,
                                       String AV22Pgmname ,
                                       int A1528ServicoFluxo_Codigo ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFM02( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOFLUXO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1528ServicoFluxo_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICOFLUXO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1528ServicoFluxo_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOFLUXO_ORDEM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1532ServicoFluxo_Ordem), "ZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICOFLUXO_ORDEM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1532ServicoFluxo_Ordem), 3, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOFLUXO_SERVICOPOS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1526ServicoFluxo_ServicoPos), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SERVICOFLUXO_SERVICOPOS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1526ServicoFluxo_ServicoPos), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFM02( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV22Pgmname = "ServicoProcessoServicoFluxoWC";
         context.Gx_err = 0;
      }

      protected void RFM02( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 27;
         /* Execute user event: E15M02 */
         E15M02 ();
         nGXsfl_27_idx = 1;
         sGXsfl_27_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_27_idx), 4, 0)), 4, "0");
         SubsflControlProps_272( ) ;
         nGXsfl_27_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_272( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A1522ServicoFluxo_ServicoCod ,
                                                 AV7ServicoFluxo_ServicoCod },
                                                 new int[] {
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            /* Using cursor H00M02 */
            pr_default.execute(0, new Object[] {AV7ServicoFluxo_ServicoCod, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_27_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1523ServicoFluxo_ServicoSigla = H00M02_A1523ServicoFluxo_ServicoSigla[0];
               n1523ServicoFluxo_ServicoSigla = H00M02_n1523ServicoFluxo_ServicoSigla[0];
               A1522ServicoFluxo_ServicoCod = H00M02_A1522ServicoFluxo_ServicoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1522ServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1522ServicoFluxo_ServicoCod), 6, 0)));
               A1527ServicoFluxo_SrvPosSigla = H00M02_A1527ServicoFluxo_SrvPosSigla[0];
               n1527ServicoFluxo_SrvPosSigla = H00M02_n1527ServicoFluxo_SrvPosSigla[0];
               A1526ServicoFluxo_ServicoPos = H00M02_A1526ServicoFluxo_ServicoPos[0];
               n1526ServicoFluxo_ServicoPos = H00M02_n1526ServicoFluxo_ServicoPos[0];
               A1532ServicoFluxo_Ordem = H00M02_A1532ServicoFluxo_Ordem[0];
               n1532ServicoFluxo_Ordem = H00M02_n1532ServicoFluxo_Ordem[0];
               A1533ServicoFluxo_ServicoTpHrq = H00M02_A1533ServicoFluxo_ServicoTpHrq[0];
               n1533ServicoFluxo_ServicoTpHrq = H00M02_n1533ServicoFluxo_ServicoTpHrq[0];
               A1528ServicoFluxo_Codigo = H00M02_A1528ServicoFluxo_Codigo[0];
               A1523ServicoFluxo_ServicoSigla = H00M02_A1523ServicoFluxo_ServicoSigla[0];
               n1523ServicoFluxo_ServicoSigla = H00M02_n1523ServicoFluxo_ServicoSigla[0];
               A1533ServicoFluxo_ServicoTpHrq = H00M02_A1533ServicoFluxo_ServicoTpHrq[0];
               n1533ServicoFluxo_ServicoTpHrq = H00M02_n1533ServicoFluxo_ServicoTpHrq[0];
               A1527ServicoFluxo_SrvPosSigla = H00M02_A1527ServicoFluxo_SrvPosSigla[0];
               n1527ServicoFluxo_SrvPosSigla = H00M02_n1527ServicoFluxo_SrvPosSigla[0];
               /* Execute user event: E16M02 */
               E16M02 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 27;
            WBM00( ) ;
         }
         nGXsfl_27_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A1522ServicoFluxo_ServicoCod ,
                                              AV7ServicoFluxo_ServicoCod },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         /* Using cursor H00M03 */
         pr_default.execute(1, new Object[] {AV7ServicoFluxo_ServicoCod});
         GRID_nRecordCount = H00M03_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV7ServicoFluxo_ServicoCod, AV22Pgmname, A1528ServicoFluxo_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV7ServicoFluxo_ServicoCod, AV22Pgmname, A1528ServicoFluxo_Codigo, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV7ServicoFluxo_ServicoCod, AV22Pgmname, A1528ServicoFluxo_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV7ServicoFluxo_ServicoCod, AV22Pgmname, A1528ServicoFluxo_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV7ServicoFluxo_ServicoCod, AV22Pgmname, A1528ServicoFluxo_Codigo, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPM00( )
      {
         /* Before Start, stand alone formulas. */
         AV22Pgmname = "ServicoProcessoServicoFluxoWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E14M02 */
         E14M02 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            A1522ServicoFluxo_ServicoCod = (int)(context.localUtil.CToN( cgiGet( edtServicoFluxo_ServicoCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1522ServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1522ServicoFluxo_ServicoCod), 6, 0)));
            /* Read saved values. */
            nRC_GXsfl_27 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_27"), ",", "."));
            wcpOAV7ServicoFluxo_ServicoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ServicoFluxo_ServicoCod"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E14M02 */
         E14M02 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E14M02( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtServicoFluxo_ServicoCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServicoFluxo_ServicoCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoFluxo_ServicoCod_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Servi�o", 0);
         cmbavOrderedby.addItem("2", "Codigo", 0);
         cmbavOrderedby.addItem("3", "de Hierarquia", 0);
         cmbavOrderedby.addItem("4", "Ordem", 0);
         cmbavOrderedby.addItem("5", "Processo", 0);
         cmbavOrderedby.addItem("6", "A��o", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
      }

      protected void E15M02( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtServicoFluxo_Codigo_Titleformat = 2;
         edtServicoFluxo_Codigo_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV13OrderedBy==2) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Codigo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServicoFluxo_Codigo_Internalname, "Title", edtServicoFluxo_Codigo_Title);
         cmbServicoFluxo_ServicoTpHrq_Titleformat = 2;
         cmbServicoFluxo_ServicoTpHrq.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV13OrderedBy==3) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "de Hierarquia", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServicoFluxo_ServicoTpHrq_Internalname, "Title", cmbServicoFluxo_ServicoTpHrq.Title.Text);
         edtServicoFluxo_Ordem_Titleformat = 2;
         edtServicoFluxo_Ordem_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV13OrderedBy==4) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Ordem", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServicoFluxo_Ordem_Internalname, "Title", edtServicoFluxo_Ordem_Title);
         edtServicoFluxo_ServicoPos_Titleformat = 2;
         edtServicoFluxo_ServicoPos_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 5);' >%5</span>", ((AV13OrderedBy==5) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Processo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServicoFluxo_ServicoPos_Internalname, "Title", edtServicoFluxo_ServicoPos_Title);
         edtServicoFluxo_SrvPosSigla_Titleformat = 2;
         edtServicoFluxo_SrvPosSigla_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 6);' >%5</span>", ((AV13OrderedBy==6) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "A��o", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServicoFluxo_SrvPosSigla_Internalname, "Title", edtServicoFluxo_SrvPosSigla_Title);
      }

      private void E16M02( )
      {
         /* Grid_Load Routine */
         AV15Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV15Update);
         AV20Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("servicofluxo.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1528ServicoFluxo_Codigo);
         AV16Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV16Delete);
         AV21Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("servicofluxo.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1528ServicoFluxo_Codigo);
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 27;
         }
         sendrow_272( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_27_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(27, GridRow);
         }
      }

      protected void E11M02( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E12M02( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
      }

      protected void E13M02( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("servicofluxo.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S142( )
      {
         /* 'CLEANFILTERS' Routine */
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV17Session.Get(AV22Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV22Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV17Session.Get(AV22Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
      }

      protected void S132( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV17Session.Get(AV22Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         new wwpbaseobjects.savegridstate(context ).execute(  AV22Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV22Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ServicoFluxo";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ServicoFluxo_ServicoCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ServicoFluxo_ServicoCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV17Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_M02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_8_M02( true) ;
         }
         else
         {
            wb_table2_8_M02( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_M02e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"27\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(46), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(46), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServicoFluxo_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtServicoFluxo_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServicoFluxo_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbServicoFluxo_ServicoTpHrq_Titleformat == 0 )
               {
                  context.SendWebValue( cmbServicoFluxo_ServicoTpHrq.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbServicoFluxo_ServicoTpHrq.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(43), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServicoFluxo_Ordem_Titleformat == 0 )
               {
                  context.SendWebValue( edtServicoFluxo_Ordem_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServicoFluxo_Ordem_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServicoFluxo_ServicoPos_Titleformat == 0 )
               {
                  context.SendWebValue( edtServicoFluxo_ServicoPos_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServicoFluxo_ServicoPos_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServicoFluxo_SrvPosSigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtServicoFluxo_SrvPosSigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServicoFluxo_SrvPosSigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV15Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV16Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1528ServicoFluxo_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServicoFluxo_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServicoFluxo_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1533ServicoFluxo_ServicoTpHrq), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbServicoFluxo_ServicoTpHrq.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbServicoFluxo_ServicoTpHrq_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1532ServicoFluxo_Ordem), 3, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServicoFluxo_Ordem_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServicoFluxo_Ordem_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1526ServicoFluxo_ServicoPos), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServicoFluxo_ServicoPos_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServicoFluxo_ServicoPos_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1527ServicoFluxo_SrvPosSigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServicoFluxo_SrvPosSigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServicoFluxo_SrvPosSigla_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 27 )
         {
            wbEnd = 0;
            nRC_GXsfl_27 = (short)(nGXsfl_27_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridContainer.AddObjectProperty("GRID_nEOF", GRID_nEOF);
               GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_M02e( true) ;
         }
         else
         {
            wb_table1_2_M02e( false) ;
         }
      }

      protected void wb_table2_8_M02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Width100'>") ;
            wb_table3_11_M02( true) ;
         }
         else
         {
            wb_table3_11_M02( false) ;
         }
         return  ;
      }

      protected void wb_table3_11_M02e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenar por", "", "", lblOrderedtext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_ServicoProcessoServicoFluxoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'" + sGXsfl_27_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_ServicoProcessoServicoFluxoWC.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'" + sGXsfl_27_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ServicoProcessoServicoFluxoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table4_21_M02( true) ;
         }
         else
         {
            wb_table4_21_M02( false) ;
         }
         return  ;
      }

      protected void wb_table4_21_M02e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_M02e( true) ;
         }
         else
         {
            wb_table2_8_M02e( false) ;
         }
      }

      protected void wb_table4_21_M02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoProcessoServicoFluxoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_21_M02e( true) ;
         }
         else
         {
            wb_table4_21_M02e( false) ;
         }
      }

      protected void wb_table3_11_M02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ServicoProcessoServicoFluxoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_11_M02e( true) ;
         }
         else
         {
            wb_table3_11_M02e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7ServicoFluxo_ServicoCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ServicoFluxo_ServicoCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAM02( ) ;
         WSM02( ) ;
         WEM02( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7ServicoFluxo_ServicoCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAM02( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "servicoprocessoservicofluxowc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAM02( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7ServicoFluxo_ServicoCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ServicoFluxo_ServicoCod), 6, 0)));
         }
         wcpOAV7ServicoFluxo_ServicoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ServicoFluxo_ServicoCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7ServicoFluxo_ServicoCod != wcpOAV7ServicoFluxo_ServicoCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV7ServicoFluxo_ServicoCod = AV7ServicoFluxo_ServicoCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7ServicoFluxo_ServicoCod = cgiGet( sPrefix+"AV7ServicoFluxo_ServicoCod_CTRL");
         if ( StringUtil.Len( sCtrlAV7ServicoFluxo_ServicoCod) > 0 )
         {
            AV7ServicoFluxo_ServicoCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7ServicoFluxo_ServicoCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ServicoFluxo_ServicoCod), 6, 0)));
         }
         else
         {
            AV7ServicoFluxo_ServicoCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7ServicoFluxo_ServicoCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAM02( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSM02( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSM02( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7ServicoFluxo_ServicoCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ServicoFluxo_ServicoCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7ServicoFluxo_ServicoCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7ServicoFluxo_ServicoCod_CTRL", StringUtil.RTrim( sCtrlAV7ServicoFluxo_ServicoCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEM02( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299314594");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("servicoprocessoservicofluxowc.js", "?20205299314595");
            context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_272( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_27_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_27_idx;
         edtServicoFluxo_Codigo_Internalname = sPrefix+"SERVICOFLUXO_CODIGO_"+sGXsfl_27_idx;
         cmbServicoFluxo_ServicoTpHrq_Internalname = sPrefix+"SERVICOFLUXO_SERVICOTPHRQ_"+sGXsfl_27_idx;
         edtServicoFluxo_Ordem_Internalname = sPrefix+"SERVICOFLUXO_ORDEM_"+sGXsfl_27_idx;
         edtServicoFluxo_ServicoPos_Internalname = sPrefix+"SERVICOFLUXO_SERVICOPOS_"+sGXsfl_27_idx;
         edtServicoFluxo_SrvPosSigla_Internalname = sPrefix+"SERVICOFLUXO_SRVPOSSIGLA_"+sGXsfl_27_idx;
      }

      protected void SubsflControlProps_fel_272( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_27_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_27_fel_idx;
         edtServicoFluxo_Codigo_Internalname = sPrefix+"SERVICOFLUXO_CODIGO_"+sGXsfl_27_fel_idx;
         cmbServicoFluxo_ServicoTpHrq_Internalname = sPrefix+"SERVICOFLUXO_SERVICOTPHRQ_"+sGXsfl_27_fel_idx;
         edtServicoFluxo_Ordem_Internalname = sPrefix+"SERVICOFLUXO_ORDEM_"+sGXsfl_27_fel_idx;
         edtServicoFluxo_ServicoPos_Internalname = sPrefix+"SERVICOFLUXO_SERVICOPOS_"+sGXsfl_27_fel_idx;
         edtServicoFluxo_SrvPosSigla_Internalname = sPrefix+"SERVICOFLUXO_SRVPOSSIGLA_"+sGXsfl_27_fel_idx;
      }

      protected void sendrow_272( )
      {
         SubsflControlProps_272( ) ;
         WBM00( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_27_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_27_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_27_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV15Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV15Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV20Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV15Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV15Update)) ? AV20Update_GXI : context.PathToRelativeUrl( AV15Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV15Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV16Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV16Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV21Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV16Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV16Delete)) ? AV21Delete_GXI : context.PathToRelativeUrl( AV16Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV16Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoFluxo_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1528ServicoFluxo_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1528ServicoFluxo_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoFluxo_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)27,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            GXCCtl = "SERVICOFLUXO_SERVICOTPHRQ_" + sGXsfl_27_idx;
            cmbServicoFluxo_ServicoTpHrq.Name = GXCCtl;
            cmbServicoFluxo_ServicoTpHrq.WebTags = "";
            cmbServicoFluxo_ServicoTpHrq.addItem("1", "Prim�rio", 0);
            cmbServicoFluxo_ServicoTpHrq.addItem("2", "Secund�rio", 0);
            if ( cmbServicoFluxo_ServicoTpHrq.ItemCount > 0 )
            {
               A1533ServicoFluxo_ServicoTpHrq = (short)(NumberUtil.Val( cmbServicoFluxo_ServicoTpHrq.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1533ServicoFluxo_ServicoTpHrq), 4, 0))), "."));
               n1533ServicoFluxo_ServicoTpHrq = false;
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbServicoFluxo_ServicoTpHrq,(String)cmbServicoFluxo_ServicoTpHrq_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A1533ServicoFluxo_ServicoTpHrq), 4, 0)),(short)1,(String)cmbServicoFluxo_ServicoTpHrq_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbServicoFluxo_ServicoTpHrq.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1533ServicoFluxo_ServicoTpHrq), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbServicoFluxo_ServicoTpHrq_Internalname, "Values", (String)(cmbServicoFluxo_ServicoTpHrq.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoFluxo_Ordem_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1532ServicoFluxo_Ordem), 3, 0, ",", "")),context.localUtil.Format( (decimal)(A1532ServicoFluxo_Ordem), "ZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoFluxo_Ordem_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)43,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)27,(short)1,(short)-1,(short)0,(bool)true,(String)"Ordem",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoFluxo_ServicoPos_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1526ServicoFluxo_ServicoPos), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1526ServicoFluxo_ServicoPos), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoFluxo_ServicoPos_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)27,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoFluxo_SrvPosSigla_Internalname,StringUtil.RTrim( A1527ServicoFluxo_SrvPosSigla),StringUtil.RTrim( context.localUtil.Format( A1527ServicoFluxo_SrvPosSigla, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoFluxo_SrvPosSigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)27,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOFLUXO_CODIGO"+"_"+sGXsfl_27_idx, GetSecureSignedToken( sPrefix+sGXsfl_27_idx, context.localUtil.Format( (decimal)(A1528ServicoFluxo_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOFLUXO_ORDEM"+"_"+sGXsfl_27_idx, GetSecureSignedToken( sPrefix+sGXsfl_27_idx, context.localUtil.Format( (decimal)(A1532ServicoFluxo_Ordem), "ZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOFLUXO_SERVICOPOS"+"_"+sGXsfl_27_idx, GetSecureSignedToken( sPrefix+sGXsfl_27_idx, context.localUtil.Format( (decimal)(A1526ServicoFluxo_ServicoPos), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_27_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_27_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_27_idx+1));
            sGXsfl_27_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_27_idx), 4, 0)), 4, "0");
            SubsflControlProps_272( ) ;
         }
         /* End function sendrow_272 */
      }

      protected void init_default_properties( )
      {
         imgInsert_Internalname = sPrefix+"INSERT";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         lblOrderedtext_Internalname = sPrefix+"ORDEREDTEXT";
         cmbavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         imgCleanfilters_Internalname = sPrefix+"CLEANFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtServicoFluxo_Codigo_Internalname = sPrefix+"SERVICOFLUXO_CODIGO";
         cmbServicoFluxo_ServicoTpHrq_Internalname = sPrefix+"SERVICOFLUXO_SERVICOTPHRQ";
         edtServicoFluxo_Ordem_Internalname = sPrefix+"SERVICOFLUXO_ORDEM";
         edtServicoFluxo_ServicoPos_Internalname = sPrefix+"SERVICOFLUXO_SERVICOPOS";
         edtServicoFluxo_SrvPosSigla_Internalname = sPrefix+"SERVICOFLUXO_SRVPOSSIGLA";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtServicoFluxo_ServicoCod_Internalname = sPrefix+"SERVICOFLUXO_SERVICOCOD";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtServicoFluxo_SrvPosSigla_Jsonclick = "";
         edtServicoFluxo_ServicoPos_Jsonclick = "";
         edtServicoFluxo_Ordem_Jsonclick = "";
         cmbServicoFluxo_ServicoTpHrq_Jsonclick = "";
         edtServicoFluxo_Codigo_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtServicoFluxo_SrvPosSigla_Titleformat = 0;
         edtServicoFluxo_ServicoPos_Titleformat = 0;
         edtServicoFluxo_Ordem_Titleformat = 0;
         cmbServicoFluxo_ServicoTpHrq_Titleformat = 0;
         edtServicoFluxo_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWith";
         edtServicoFluxo_SrvPosSigla_Title = "A��o";
         edtServicoFluxo_ServicoPos_Title = "Processo";
         edtServicoFluxo_Ordem_Title = "Ordem";
         cmbServicoFluxo_ServicoTpHrq.Title.Text = "de Hierarquia";
         edtServicoFluxo_Codigo_Title = "Codigo";
         edtavOrdereddsc_Visible = 1;
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtServicoFluxo_ServicoCod_Jsonclick = "";
         edtServicoFluxo_ServicoCod_Visible = 1;
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7ServicoFluxo_ServicoCod',fld:'vSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'A1528ServicoFluxo_Codigo',fld:'SERVICOFLUXO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV22Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'edtServicoFluxo_Codigo_Titleformat',ctrl:'SERVICOFLUXO_CODIGO',prop:'Titleformat'},{av:'edtServicoFluxo_Codigo_Title',ctrl:'SERVICOFLUXO_CODIGO',prop:'Title'},{av:'cmbServicoFluxo_ServicoTpHrq'},{av:'edtServicoFluxo_Ordem_Titleformat',ctrl:'SERVICOFLUXO_ORDEM',prop:'Titleformat'},{av:'edtServicoFluxo_Ordem_Title',ctrl:'SERVICOFLUXO_ORDEM',prop:'Title'},{av:'edtServicoFluxo_ServicoPos_Titleformat',ctrl:'SERVICOFLUXO_SERVICOPOS',prop:'Titleformat'},{av:'edtServicoFluxo_ServicoPos_Title',ctrl:'SERVICOFLUXO_SERVICOPOS',prop:'Title'},{av:'edtServicoFluxo_SrvPosSigla_Titleformat',ctrl:'SERVICOFLUXO_SRVPOSSIGLA',prop:'Titleformat'},{av:'edtServicoFluxo_SrvPosSigla_Title',ctrl:'SERVICOFLUXO_SRVPOSSIGLA',prop:'Title'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E16M02',iparms:[{av:'A1528ServicoFluxo_Codigo',fld:'SERVICOFLUXO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV15Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV16Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E11M02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV7ServicoFluxo_ServicoCod',fld:'vSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV22Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1528ServicoFluxo_Codigo',fld:'SERVICOFLUXO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E12M02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV7ServicoFluxo_ServicoCod',fld:'vSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV22Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A1528ServicoFluxo_Codigo',fld:'SERVICOFLUXO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("'DOINSERT'","{handler:'E13M02',iparms:[{av:'A1528ServicoFluxo_Codigo',fld:'SERVICOFLUXO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("GRID_FIRSTPAGE","{handler:'subgrid_firstpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7ServicoFluxo_ServicoCod',fld:'vSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'A1528ServicoFluxo_Codigo',fld:'SERVICOFLUXO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV22Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'edtServicoFluxo_Codigo_Titleformat',ctrl:'SERVICOFLUXO_CODIGO',prop:'Titleformat'},{av:'edtServicoFluxo_Codigo_Title',ctrl:'SERVICOFLUXO_CODIGO',prop:'Title'},{av:'cmbServicoFluxo_ServicoTpHrq'},{av:'edtServicoFluxo_Ordem_Titleformat',ctrl:'SERVICOFLUXO_ORDEM',prop:'Titleformat'},{av:'edtServicoFluxo_Ordem_Title',ctrl:'SERVICOFLUXO_ORDEM',prop:'Title'},{av:'edtServicoFluxo_ServicoPos_Titleformat',ctrl:'SERVICOFLUXO_SERVICOPOS',prop:'Titleformat'},{av:'edtServicoFluxo_ServicoPos_Title',ctrl:'SERVICOFLUXO_SERVICOPOS',prop:'Title'},{av:'edtServicoFluxo_SrvPosSigla_Titleformat',ctrl:'SERVICOFLUXO_SRVPOSSIGLA',prop:'Titleformat'},{av:'edtServicoFluxo_SrvPosSigla_Title',ctrl:'SERVICOFLUXO_SRVPOSSIGLA',prop:'Title'}]}");
         setEventMetadata("GRID_PREVPAGE","{handler:'subgrid_previouspage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7ServicoFluxo_ServicoCod',fld:'vSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'A1528ServicoFluxo_Codigo',fld:'SERVICOFLUXO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV22Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'edtServicoFluxo_Codigo_Titleformat',ctrl:'SERVICOFLUXO_CODIGO',prop:'Titleformat'},{av:'edtServicoFluxo_Codigo_Title',ctrl:'SERVICOFLUXO_CODIGO',prop:'Title'},{av:'cmbServicoFluxo_ServicoTpHrq'},{av:'edtServicoFluxo_Ordem_Titleformat',ctrl:'SERVICOFLUXO_ORDEM',prop:'Titleformat'},{av:'edtServicoFluxo_Ordem_Title',ctrl:'SERVICOFLUXO_ORDEM',prop:'Title'},{av:'edtServicoFluxo_ServicoPos_Titleformat',ctrl:'SERVICOFLUXO_SERVICOPOS',prop:'Titleformat'},{av:'edtServicoFluxo_ServicoPos_Title',ctrl:'SERVICOFLUXO_SERVICOPOS',prop:'Title'},{av:'edtServicoFluxo_SrvPosSigla_Titleformat',ctrl:'SERVICOFLUXO_SRVPOSSIGLA',prop:'Titleformat'},{av:'edtServicoFluxo_SrvPosSigla_Title',ctrl:'SERVICOFLUXO_SRVPOSSIGLA',prop:'Title'}]}");
         setEventMetadata("GRID_NEXTPAGE","{handler:'subgrid_nextpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7ServicoFluxo_ServicoCod',fld:'vSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'A1528ServicoFluxo_Codigo',fld:'SERVICOFLUXO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV22Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'edtServicoFluxo_Codigo_Titleformat',ctrl:'SERVICOFLUXO_CODIGO',prop:'Titleformat'},{av:'edtServicoFluxo_Codigo_Title',ctrl:'SERVICOFLUXO_CODIGO',prop:'Title'},{av:'cmbServicoFluxo_ServicoTpHrq'},{av:'edtServicoFluxo_Ordem_Titleformat',ctrl:'SERVICOFLUXO_ORDEM',prop:'Titleformat'},{av:'edtServicoFluxo_Ordem_Title',ctrl:'SERVICOFLUXO_ORDEM',prop:'Title'},{av:'edtServicoFluxo_ServicoPos_Titleformat',ctrl:'SERVICOFLUXO_SERVICOPOS',prop:'Titleformat'},{av:'edtServicoFluxo_ServicoPos_Title',ctrl:'SERVICOFLUXO_SERVICOPOS',prop:'Title'},{av:'edtServicoFluxo_SrvPosSigla_Titleformat',ctrl:'SERVICOFLUXO_SRVPOSSIGLA',prop:'Titleformat'},{av:'edtServicoFluxo_SrvPosSigla_Title',ctrl:'SERVICOFLUXO_SRVPOSSIGLA',prop:'Title'}]}");
         setEventMetadata("GRID_LASTPAGE","{handler:'subgrid_lastpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7ServicoFluxo_ServicoCod',fld:'vSERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'A1528ServicoFluxo_Codigo',fld:'SERVICOFLUXO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV22Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'edtServicoFluxo_Codigo_Titleformat',ctrl:'SERVICOFLUXO_CODIGO',prop:'Titleformat'},{av:'edtServicoFluxo_Codigo_Title',ctrl:'SERVICOFLUXO_CODIGO',prop:'Title'},{av:'cmbServicoFluxo_ServicoTpHrq'},{av:'edtServicoFluxo_Ordem_Titleformat',ctrl:'SERVICOFLUXO_ORDEM',prop:'Titleformat'},{av:'edtServicoFluxo_Ordem_Title',ctrl:'SERVICOFLUXO_ORDEM',prop:'Title'},{av:'edtServicoFluxo_ServicoPos_Titleformat',ctrl:'SERVICOFLUXO_SERVICOPOS',prop:'Titleformat'},{av:'edtServicoFluxo_ServicoPos_Title',ctrl:'SERVICOFLUXO_SERVICOPOS',prop:'Title'},{av:'edtServicoFluxo_SrvPosSigla_Titleformat',ctrl:'SERVICOFLUXO_SRVPOSSIGLA',prop:'Titleformat'},{av:'edtServicoFluxo_SrvPosSigla_Title',ctrl:'SERVICOFLUXO_SRVPOSSIGLA',prop:'Title'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV22Pgmname = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV15Update = "";
         AV20Update_GXI = "";
         AV16Delete = "";
         AV21Delete_GXI = "";
         A1527ServicoFluxo_SrvPosSigla = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00M02_A1523ServicoFluxo_ServicoSigla = new String[] {""} ;
         H00M02_n1523ServicoFluxo_ServicoSigla = new bool[] {false} ;
         H00M02_A1522ServicoFluxo_ServicoCod = new int[1] ;
         H00M02_A1527ServicoFluxo_SrvPosSigla = new String[] {""} ;
         H00M02_n1527ServicoFluxo_SrvPosSigla = new bool[] {false} ;
         H00M02_A1526ServicoFluxo_ServicoPos = new int[1] ;
         H00M02_n1526ServicoFluxo_ServicoPos = new bool[] {false} ;
         H00M02_A1532ServicoFluxo_Ordem = new short[1] ;
         H00M02_n1532ServicoFluxo_Ordem = new bool[] {false} ;
         H00M02_A1533ServicoFluxo_ServicoTpHrq = new short[1] ;
         H00M02_n1533ServicoFluxo_ServicoTpHrq = new bool[] {false} ;
         H00M02_A1528ServicoFluxo_Codigo = new int[1] ;
         A1523ServicoFluxo_ServicoSigla = "";
         H00M03_AGRID_nRecordCount = new long[1] ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV17Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         TempTags = "";
         imgCleanfilters_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7ServicoFluxo_ServicoCod = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.servicoprocessoservicofluxowc__default(),
            new Object[][] {
                new Object[] {
               H00M02_A1523ServicoFluxo_ServicoSigla, H00M02_n1523ServicoFluxo_ServicoSigla, H00M02_A1522ServicoFluxo_ServicoCod, H00M02_A1527ServicoFluxo_SrvPosSigla, H00M02_n1527ServicoFluxo_SrvPosSigla, H00M02_A1526ServicoFluxo_ServicoPos, H00M02_n1526ServicoFluxo_ServicoPos, H00M02_A1532ServicoFluxo_Ordem, H00M02_n1532ServicoFluxo_Ordem, H00M02_A1533ServicoFluxo_ServicoTpHrq,
               H00M02_n1533ServicoFluxo_ServicoTpHrq, H00M02_A1528ServicoFluxo_Codigo
               }
               , new Object[] {
               H00M03_AGRID_nRecordCount
               }
            }
         );
         AV22Pgmname = "ServicoProcessoServicoFluxoWC";
         /* GeneXus formulas. */
         AV22Pgmname = "ServicoProcessoServicoFluxoWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_27 ;
      private short nGXsfl_27_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short nGXWrapped ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short A1533ServicoFluxo_ServicoTpHrq ;
      private short A1532ServicoFluxo_Ordem ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_27_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtServicoFluxo_Codigo_Titleformat ;
      private short cmbServicoFluxo_ServicoTpHrq_Titleformat ;
      private short edtServicoFluxo_Ordem_Titleformat ;
      private short edtServicoFluxo_ServicoPos_Titleformat ;
      private short edtServicoFluxo_SrvPosSigla_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short subGrid_Backstyle ;
      private int AV7ServicoFluxo_ServicoCod ;
      private int wcpOAV7ServicoFluxo_ServicoCod ;
      private int subGrid_Rows ;
      private int A1528ServicoFluxo_Codigo ;
      private int A1522ServicoFluxo_ServicoCod ;
      private int edtServicoFluxo_ServicoCod_Visible ;
      private int A1526ServicoFluxo_ServicoPos ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_27_idx="0001" ;
      private String AV22Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String edtServicoFluxo_ServicoCod_Internalname ;
      private String edtServicoFluxo_ServicoCod_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavOrderedby_Internalname ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtServicoFluxo_Codigo_Internalname ;
      private String cmbServicoFluxo_ServicoTpHrq_Internalname ;
      private String edtServicoFluxo_Ordem_Internalname ;
      private String edtServicoFluxo_ServicoPos_Internalname ;
      private String A1527ServicoFluxo_SrvPosSigla ;
      private String edtServicoFluxo_SrvPosSigla_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String A1523ServicoFluxo_ServicoSigla ;
      private String edtavOrdereddsc_Internalname ;
      private String edtServicoFluxo_Codigo_Title ;
      private String edtServicoFluxo_Ordem_Title ;
      private String edtServicoFluxo_ServicoPos_Title ;
      private String edtServicoFluxo_SrvPosSigla_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String TempTags ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String imgCleanfilters_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sCtrlAV7ServicoFluxo_ServicoCod ;
      private String sGXsfl_27_fel_idx="0001" ;
      private String ROClassString ;
      private String edtServicoFluxo_Codigo_Jsonclick ;
      private String cmbServicoFluxo_ServicoTpHrq_Jsonclick ;
      private String edtServicoFluxo_Ordem_Jsonclick ;
      private String edtServicoFluxo_ServicoPos_Jsonclick ;
      private String edtServicoFluxo_SrvPosSigla_Jsonclick ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1533ServicoFluxo_ServicoTpHrq ;
      private bool n1532ServicoFluxo_Ordem ;
      private bool n1526ServicoFluxo_ServicoPos ;
      private bool n1527ServicoFluxo_SrvPosSigla ;
      private bool n1523ServicoFluxo_ServicoSigla ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV15Update_IsBlob ;
      private bool AV16Delete_IsBlob ;
      private String AV20Update_GXI ;
      private String AV21Delete_GXI ;
      private String AV15Update ;
      private String AV16Delete ;
      private IGxSession AV17Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbServicoFluxo_ServicoTpHrq ;
      private IDataStoreProvider pr_default ;
      private String[] H00M02_A1523ServicoFluxo_ServicoSigla ;
      private bool[] H00M02_n1523ServicoFluxo_ServicoSigla ;
      private int[] H00M02_A1522ServicoFluxo_ServicoCod ;
      private String[] H00M02_A1527ServicoFluxo_SrvPosSigla ;
      private bool[] H00M02_n1527ServicoFluxo_SrvPosSigla ;
      private int[] H00M02_A1526ServicoFluxo_ServicoPos ;
      private bool[] H00M02_n1526ServicoFluxo_ServicoPos ;
      private short[] H00M02_A1532ServicoFluxo_Ordem ;
      private bool[] H00M02_n1532ServicoFluxo_Ordem ;
      private short[] H00M02_A1533ServicoFluxo_ServicoTpHrq ;
      private bool[] H00M02_n1533ServicoFluxo_ServicoTpHrq ;
      private int[] H00M02_A1528ServicoFluxo_Codigo ;
      private long[] H00M03_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
   }

   public class servicoprocessoservicofluxowc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00M02( IGxContext context ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A1522ServicoFluxo_ServicoCod ,
                                             int AV7ServicoFluxo_ServicoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [6] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[Servico_Sigla] AS ServicoFluxo_ServicoSigla, T1.[ServicoFluxo_ServicoCod] AS ServicoFluxo_ServicoCod, T3.[Servico_Sigla] AS ServicoFluxo_SrvPosSigla, T1.[ServicoFluxo_ServicoPos] AS ServicoFluxo_ServicoPos, T1.[ServicoFluxo_Ordem], T2.[Servico_TipoHierarquia] AS ServicoFluxo_ServicoTpHrq, T1.[ServicoFluxo_Codigo]";
         sFromString = " FROM (([ServicoFluxo] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[ServicoFluxo_ServicoCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T1.[ServicoFluxo_ServicoPos])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[ServicoFluxo_ServicoCod] = @AV7ServicoFluxo_ServicoCod)";
         if ( AV13OrderedBy == 1 )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Servico_Sigla]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ServicoFluxo_ServicoCod], T1.[ServicoFluxo_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ServicoFluxo_ServicoCod] DESC, T1.[ServicoFluxo_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ServicoFluxo_ServicoCod], T2.[Servico_TipoHierarquia]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ServicoFluxo_ServicoCod] DESC, T2.[Servico_TipoHierarquia] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ServicoFluxo_ServicoCod], T1.[ServicoFluxo_Ordem]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ServicoFluxo_ServicoCod] DESC, T1.[ServicoFluxo_Ordem] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ServicoFluxo_ServicoCod], T1.[ServicoFluxo_ServicoPos]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ServicoFluxo_ServicoCod] DESC, T1.[ServicoFluxo_ServicoPos] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ServicoFluxo_ServicoCod], T3.[Servico_Sigla]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ServicoFluxo_ServicoCod] DESC, T3.[Servico_Sigla] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ServicoFluxo_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H00M03( IGxContext context ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A1522ServicoFluxo_ServicoCod ,
                                             int AV7ServicoFluxo_ServicoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [1] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (([ServicoFluxo] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[ServicoFluxo_ServicoCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T1.[ServicoFluxo_ServicoPos])";
         scmdbuf = scmdbuf + " WHERE (T1.[ServicoFluxo_ServicoCod] = @AV7ServicoFluxo_ServicoCod)";
         if ( AV13OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00M02(context, (short)dynConstraints[0] , (bool)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
               case 1 :
                     return conditional_H00M03(context, (short)dynConstraints[0] , (bool)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00M02 ;
          prmH00M02 = new Object[] {
          new Object[] {"@AV7ServicoFluxo_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00M03 ;
          prmH00M03 = new Object[] {
          new Object[] {"@AV7ServicoFluxo_ServicoCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00M02", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00M02,11,0,true,false )
             ,new CursorDef("H00M03", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00M03,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((short[]) buf[7])[0] = rslt.getShort(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((short[]) buf[9])[0] = rslt.getShort(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
       }
    }

 }

}
