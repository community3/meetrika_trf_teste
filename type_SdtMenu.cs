/*
               File: type_SdtMenu
        Description: Menu
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:0:23.5
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Menu" )]
   [XmlType(TypeName =  "Menu" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtMenu : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtMenu( )
      {
         /* Constructor for serialization */
         gxTv_SdtMenu_Menu_nome = "";
         gxTv_SdtMenu_Menu_descricao = "";
         gxTv_SdtMenu_Menu_link = "";
         gxTv_SdtMenu_Menu_imagem = "";
         gxTv_SdtMenu_Menu_imagem_gxi = "";
         gxTv_SdtMenu_Menu_painom = "";
         gxTv_SdtMenu_Mode = "";
         gxTv_SdtMenu_Menu_nome_Z = "";
         gxTv_SdtMenu_Menu_descricao_Z = "";
         gxTv_SdtMenu_Menu_link_Z = "";
         gxTv_SdtMenu_Menu_painom_Z = "";
         gxTv_SdtMenu_Menu_imagem_gxi_Z = "";
      }

      public SdtMenu( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV277Menu_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV277Menu_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"Menu_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Menu");
         metadata.Set("BT", "Menu");
         metadata.Set("PK", "[ \"Menu_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"Menu_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Menu_Codigo\" ],\"FKMap\":[ \"Menu_PaiCod-Menu_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Menu_imagem_gxi" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_descricao_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_tipo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_link_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_ordem_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_paicod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_painom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_paitip_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_paiati_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_ativo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_imagem_gxi_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_descricao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_link_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_imagem_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_paicod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_painom_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_paitip_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_paiati_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Menu_imagem_gxi_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtMenu deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtMenu)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtMenu obj ;
         obj = this;
         obj.gxTpr_Menu_codigo = deserialized.gxTpr_Menu_codigo;
         obj.gxTpr_Menu_nome = deserialized.gxTpr_Menu_nome;
         obj.gxTpr_Menu_descricao = deserialized.gxTpr_Menu_descricao;
         obj.gxTpr_Menu_tipo = deserialized.gxTpr_Menu_tipo;
         obj.gxTpr_Menu_link = deserialized.gxTpr_Menu_link;
         obj.gxTpr_Menu_imagem = deserialized.gxTpr_Menu_imagem;
         obj.gxTpr_Menu_imagem_gxi = deserialized.gxTpr_Menu_imagem_gxi;
         obj.gxTpr_Menu_ordem = deserialized.gxTpr_Menu_ordem;
         obj.gxTpr_Menu_paicod = deserialized.gxTpr_Menu_paicod;
         obj.gxTpr_Menu_painom = deserialized.gxTpr_Menu_painom;
         obj.gxTpr_Menu_paitip = deserialized.gxTpr_Menu_paitip;
         obj.gxTpr_Menu_paiati = deserialized.gxTpr_Menu_paiati;
         obj.gxTpr_Menu_ativo = deserialized.gxTpr_Menu_ativo;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Menu_codigo_Z = deserialized.gxTpr_Menu_codigo_Z;
         obj.gxTpr_Menu_nome_Z = deserialized.gxTpr_Menu_nome_Z;
         obj.gxTpr_Menu_descricao_Z = deserialized.gxTpr_Menu_descricao_Z;
         obj.gxTpr_Menu_tipo_Z = deserialized.gxTpr_Menu_tipo_Z;
         obj.gxTpr_Menu_link_Z = deserialized.gxTpr_Menu_link_Z;
         obj.gxTpr_Menu_ordem_Z = deserialized.gxTpr_Menu_ordem_Z;
         obj.gxTpr_Menu_paicod_Z = deserialized.gxTpr_Menu_paicod_Z;
         obj.gxTpr_Menu_painom_Z = deserialized.gxTpr_Menu_painom_Z;
         obj.gxTpr_Menu_paitip_Z = deserialized.gxTpr_Menu_paitip_Z;
         obj.gxTpr_Menu_paiati_Z = deserialized.gxTpr_Menu_paiati_Z;
         obj.gxTpr_Menu_ativo_Z = deserialized.gxTpr_Menu_ativo_Z;
         obj.gxTpr_Menu_imagem_gxi_Z = deserialized.gxTpr_Menu_imagem_gxi_Z;
         obj.gxTpr_Menu_descricao_N = deserialized.gxTpr_Menu_descricao_N;
         obj.gxTpr_Menu_link_N = deserialized.gxTpr_Menu_link_N;
         obj.gxTpr_Menu_imagem_N = deserialized.gxTpr_Menu_imagem_N;
         obj.gxTpr_Menu_paicod_N = deserialized.gxTpr_Menu_paicod_N;
         obj.gxTpr_Menu_painom_N = deserialized.gxTpr_Menu_painom_N;
         obj.gxTpr_Menu_paitip_N = deserialized.gxTpr_Menu_paitip_N;
         obj.gxTpr_Menu_paiati_N = deserialized.gxTpr_Menu_paiati_N;
         obj.gxTpr_Menu_imagem_gxi_N = deserialized.gxTpr_Menu_imagem_gxi_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Codigo") )
               {
                  gxTv_SdtMenu_Menu_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Nome") )
               {
                  gxTv_SdtMenu_Menu_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Descricao") )
               {
                  gxTv_SdtMenu_Menu_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Tipo") )
               {
                  gxTv_SdtMenu_Menu_tipo = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Link") )
               {
                  gxTv_SdtMenu_Menu_link = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Imagem") )
               {
                  gxTv_SdtMenu_Menu_imagem = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Imagem_GXI") )
               {
                  gxTv_SdtMenu_Menu_imagem_gxi = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Ordem") )
               {
                  gxTv_SdtMenu_Menu_ordem = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_PaiCod") )
               {
                  gxTv_SdtMenu_Menu_paicod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_PaiNom") )
               {
                  gxTv_SdtMenu_Menu_painom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_PaiTip") )
               {
                  gxTv_SdtMenu_Menu_paitip = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_PaiAti") )
               {
                  gxTv_SdtMenu_Menu_paiati = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Ativo") )
               {
                  gxTv_SdtMenu_Menu_ativo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtMenu_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtMenu_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Codigo_Z") )
               {
                  gxTv_SdtMenu_Menu_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Nome_Z") )
               {
                  gxTv_SdtMenu_Menu_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Descricao_Z") )
               {
                  gxTv_SdtMenu_Menu_descricao_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Tipo_Z") )
               {
                  gxTv_SdtMenu_Menu_tipo_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Link_Z") )
               {
                  gxTv_SdtMenu_Menu_link_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Ordem_Z") )
               {
                  gxTv_SdtMenu_Menu_ordem_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_PaiCod_Z") )
               {
                  gxTv_SdtMenu_Menu_paicod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_PaiNom_Z") )
               {
                  gxTv_SdtMenu_Menu_painom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_PaiTip_Z") )
               {
                  gxTv_SdtMenu_Menu_paitip_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_PaiAti_Z") )
               {
                  gxTv_SdtMenu_Menu_paiati_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Ativo_Z") )
               {
                  gxTv_SdtMenu_Menu_ativo_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Imagem_GXI_Z") )
               {
                  gxTv_SdtMenu_Menu_imagem_gxi_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Descricao_N") )
               {
                  gxTv_SdtMenu_Menu_descricao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Link_N") )
               {
                  gxTv_SdtMenu_Menu_link_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Imagem_N") )
               {
                  gxTv_SdtMenu_Menu_imagem_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_PaiCod_N") )
               {
                  gxTv_SdtMenu_Menu_paicod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_PaiNom_N") )
               {
                  gxTv_SdtMenu_Menu_painom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_PaiTip_N") )
               {
                  gxTv_SdtMenu_Menu_paitip_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_PaiAti_N") )
               {
                  gxTv_SdtMenu_Menu_paiati_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Menu_Imagem_GXI_N") )
               {
                  gxTv_SdtMenu_Menu_imagem_gxi_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "Menu";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Menu_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenu_Menu_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Menu_Nome", StringUtil.RTrim( gxTv_SdtMenu_Menu_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Menu_Descricao", StringUtil.RTrim( gxTv_SdtMenu_Menu_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Menu_Tipo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenu_Menu_tipo), 2, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Menu_Link", StringUtil.RTrim( gxTv_SdtMenu_Menu_link));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Menu_Imagem", StringUtil.RTrim( gxTv_SdtMenu_Menu_imagem));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Menu_Ordem", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenu_Menu_ordem), 3, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Menu_PaiCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenu_Menu_paicod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Menu_PaiNom", StringUtil.RTrim( gxTv_SdtMenu_Menu_painom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Menu_PaiTip", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenu_Menu_paitip), 2, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Menu_PaiAti", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtMenu_Menu_paiati)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Menu_Ativo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtMenu_Menu_ativo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Menu_Imagem_GXI", StringUtil.RTrim( gxTv_SdtMenu_Menu_imagem_gxi));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtMenu_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenu_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenu_Menu_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_Nome_Z", StringUtil.RTrim( gxTv_SdtMenu_Menu_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_Descricao_Z", StringUtil.RTrim( gxTv_SdtMenu_Menu_descricao_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_Tipo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenu_Menu_tipo_Z), 2, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_Link_Z", StringUtil.RTrim( gxTv_SdtMenu_Menu_link_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_Ordem_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenu_Menu_ordem_Z), 3, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_PaiCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenu_Menu_paicod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_PaiNom_Z", StringUtil.RTrim( gxTv_SdtMenu_Menu_painom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_PaiTip_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenu_Menu_paitip_Z), 2, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_PaiAti_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtMenu_Menu_paiati_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_Ativo_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtMenu_Menu_ativo_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_Imagem_GXI_Z", StringUtil.RTrim( gxTv_SdtMenu_Menu_imagem_gxi_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_Descricao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenu_Menu_descricao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_Link_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenu_Menu_link_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_Imagem_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenu_Menu_imagem_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_PaiCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenu_Menu_paicod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_PaiNom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenu_Menu_painom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_PaiTip_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenu_Menu_paitip_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_PaiAti_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenu_Menu_paiati_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Menu_Imagem_GXI_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtMenu_Menu_imagem_gxi_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Menu_Codigo", gxTv_SdtMenu_Menu_codigo, false);
         AddObjectProperty("Menu_Nome", gxTv_SdtMenu_Menu_nome, false);
         AddObjectProperty("Menu_Descricao", gxTv_SdtMenu_Menu_descricao, false);
         AddObjectProperty("Menu_Tipo", gxTv_SdtMenu_Menu_tipo, false);
         AddObjectProperty("Menu_Link", gxTv_SdtMenu_Menu_link, false);
         AddObjectProperty("Menu_Imagem", gxTv_SdtMenu_Menu_imagem, false);
         AddObjectProperty("Menu_Ordem", gxTv_SdtMenu_Menu_ordem, false);
         AddObjectProperty("Menu_PaiCod", gxTv_SdtMenu_Menu_paicod, false);
         AddObjectProperty("Menu_PaiNom", gxTv_SdtMenu_Menu_painom, false);
         AddObjectProperty("Menu_PaiTip", gxTv_SdtMenu_Menu_paitip, false);
         AddObjectProperty("Menu_PaiAti", gxTv_SdtMenu_Menu_paiati, false);
         AddObjectProperty("Menu_Ativo", gxTv_SdtMenu_Menu_ativo, false);
         if ( includeState )
         {
            AddObjectProperty("Menu_Imagem_GXI", gxTv_SdtMenu_Menu_imagem_gxi, false);
            AddObjectProperty("Mode", gxTv_SdtMenu_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtMenu_Initialized, false);
            AddObjectProperty("Menu_Codigo_Z", gxTv_SdtMenu_Menu_codigo_Z, false);
            AddObjectProperty("Menu_Nome_Z", gxTv_SdtMenu_Menu_nome_Z, false);
            AddObjectProperty("Menu_Descricao_Z", gxTv_SdtMenu_Menu_descricao_Z, false);
            AddObjectProperty("Menu_Tipo_Z", gxTv_SdtMenu_Menu_tipo_Z, false);
            AddObjectProperty("Menu_Link_Z", gxTv_SdtMenu_Menu_link_Z, false);
            AddObjectProperty("Menu_Ordem_Z", gxTv_SdtMenu_Menu_ordem_Z, false);
            AddObjectProperty("Menu_PaiCod_Z", gxTv_SdtMenu_Menu_paicod_Z, false);
            AddObjectProperty("Menu_PaiNom_Z", gxTv_SdtMenu_Menu_painom_Z, false);
            AddObjectProperty("Menu_PaiTip_Z", gxTv_SdtMenu_Menu_paitip_Z, false);
            AddObjectProperty("Menu_PaiAti_Z", gxTv_SdtMenu_Menu_paiati_Z, false);
            AddObjectProperty("Menu_Ativo_Z", gxTv_SdtMenu_Menu_ativo_Z, false);
            AddObjectProperty("Menu_Imagem_GXI_Z", gxTv_SdtMenu_Menu_imagem_gxi_Z, false);
            AddObjectProperty("Menu_Descricao_N", gxTv_SdtMenu_Menu_descricao_N, false);
            AddObjectProperty("Menu_Link_N", gxTv_SdtMenu_Menu_link_N, false);
            AddObjectProperty("Menu_Imagem_N", gxTv_SdtMenu_Menu_imagem_N, false);
            AddObjectProperty("Menu_PaiCod_N", gxTv_SdtMenu_Menu_paicod_N, false);
            AddObjectProperty("Menu_PaiNom_N", gxTv_SdtMenu_Menu_painom_N, false);
            AddObjectProperty("Menu_PaiTip_N", gxTv_SdtMenu_Menu_paitip_N, false);
            AddObjectProperty("Menu_PaiAti_N", gxTv_SdtMenu_Menu_paiati_N, false);
            AddObjectProperty("Menu_Imagem_GXI_N", gxTv_SdtMenu_Menu_imagem_gxi_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Menu_Codigo" )]
      [  XmlElement( ElementName = "Menu_Codigo"   )]
      public int gxTpr_Menu_codigo
      {
         get {
            return gxTv_SdtMenu_Menu_codigo ;
         }

         set {
            if ( gxTv_SdtMenu_Menu_codigo != value )
            {
               gxTv_SdtMenu_Mode = "INS";
               this.gxTv_SdtMenu_Menu_codigo_Z_SetNull( );
               this.gxTv_SdtMenu_Menu_nome_Z_SetNull( );
               this.gxTv_SdtMenu_Menu_descricao_Z_SetNull( );
               this.gxTv_SdtMenu_Menu_tipo_Z_SetNull( );
               this.gxTv_SdtMenu_Menu_link_Z_SetNull( );
               this.gxTv_SdtMenu_Menu_ordem_Z_SetNull( );
               this.gxTv_SdtMenu_Menu_paicod_Z_SetNull( );
               this.gxTv_SdtMenu_Menu_painom_Z_SetNull( );
               this.gxTv_SdtMenu_Menu_paitip_Z_SetNull( );
               this.gxTv_SdtMenu_Menu_paiati_Z_SetNull( );
               this.gxTv_SdtMenu_Menu_ativo_Z_SetNull( );
               this.gxTv_SdtMenu_Menu_imagem_gxi_Z_SetNull( );
            }
            gxTv_SdtMenu_Menu_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Menu_Nome" )]
      [  XmlElement( ElementName = "Menu_Nome"   )]
      public String gxTpr_Menu_nome
      {
         get {
            return gxTv_SdtMenu_Menu_nome ;
         }

         set {
            gxTv_SdtMenu_Menu_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Menu_Descricao" )]
      [  XmlElement( ElementName = "Menu_Descricao"   )]
      public String gxTpr_Menu_descricao
      {
         get {
            return gxTv_SdtMenu_Menu_descricao ;
         }

         set {
            gxTv_SdtMenu_Menu_descricao_N = 0;
            gxTv_SdtMenu_Menu_descricao = (String)(value);
         }

      }

      public void gxTv_SdtMenu_Menu_descricao_SetNull( )
      {
         gxTv_SdtMenu_Menu_descricao_N = 1;
         gxTv_SdtMenu_Menu_descricao = "";
         return  ;
      }

      public bool gxTv_SdtMenu_Menu_descricao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_Tipo" )]
      [  XmlElement( ElementName = "Menu_Tipo"   )]
      public short gxTpr_Menu_tipo
      {
         get {
            return gxTv_SdtMenu_Menu_tipo ;
         }

         set {
            gxTv_SdtMenu_Menu_tipo = (short)(value);
         }

      }

      [  SoapElement( ElementName = "Menu_Link" )]
      [  XmlElement( ElementName = "Menu_Link"   )]
      public String gxTpr_Menu_link
      {
         get {
            return gxTv_SdtMenu_Menu_link ;
         }

         set {
            gxTv_SdtMenu_Menu_link_N = 0;
            gxTv_SdtMenu_Menu_link = (String)(value);
         }

      }

      public void gxTv_SdtMenu_Menu_link_SetNull( )
      {
         gxTv_SdtMenu_Menu_link_N = 1;
         gxTv_SdtMenu_Menu_link = "";
         return  ;
      }

      public bool gxTv_SdtMenu_Menu_link_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_Imagem" )]
      [  XmlElement( ElementName = "Menu_Imagem"   )]
      [GxUpload()]
      public String gxTpr_Menu_imagem
      {
         get {
            return gxTv_SdtMenu_Menu_imagem ;
         }

         set {
            gxTv_SdtMenu_Menu_imagem_N = 0;
            gxTv_SdtMenu_Menu_imagem = (String)(value);
         }

      }

      public void gxTv_SdtMenu_Menu_imagem_SetNull( )
      {
         gxTv_SdtMenu_Menu_imagem_N = 1;
         gxTv_SdtMenu_Menu_imagem = "";
         return  ;
      }

      public bool gxTv_SdtMenu_Menu_imagem_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_Imagem_GXI" )]
      [  XmlElement( ElementName = "Menu_Imagem_GXI"   )]
      public String gxTpr_Menu_imagem_gxi
      {
         get {
            return gxTv_SdtMenu_Menu_imagem_gxi ;
         }

         set {
            gxTv_SdtMenu_Menu_imagem_gxi_N = 0;
            gxTv_SdtMenu_Menu_imagem_gxi = (String)(value);
         }

      }

      public void gxTv_SdtMenu_Menu_imagem_gxi_SetNull( )
      {
         gxTv_SdtMenu_Menu_imagem_gxi_N = 1;
         gxTv_SdtMenu_Menu_imagem_gxi = "";
         return  ;
      }

      public bool gxTv_SdtMenu_Menu_imagem_gxi_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_Ordem" )]
      [  XmlElement( ElementName = "Menu_Ordem"   )]
      public short gxTpr_Menu_ordem
      {
         get {
            return gxTv_SdtMenu_Menu_ordem ;
         }

         set {
            gxTv_SdtMenu_Menu_ordem = (short)(value);
         }

      }

      [  SoapElement( ElementName = "Menu_PaiCod" )]
      [  XmlElement( ElementName = "Menu_PaiCod"   )]
      public int gxTpr_Menu_paicod
      {
         get {
            return gxTv_SdtMenu_Menu_paicod ;
         }

         set {
            gxTv_SdtMenu_Menu_paicod_N = 0;
            gxTv_SdtMenu_Menu_paicod = (int)(value);
         }

      }

      public void gxTv_SdtMenu_Menu_paicod_SetNull( )
      {
         gxTv_SdtMenu_Menu_paicod_N = 1;
         gxTv_SdtMenu_Menu_paicod = 0;
         return  ;
      }

      public bool gxTv_SdtMenu_Menu_paicod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_PaiNom" )]
      [  XmlElement( ElementName = "Menu_PaiNom"   )]
      public String gxTpr_Menu_painom
      {
         get {
            return gxTv_SdtMenu_Menu_painom ;
         }

         set {
            gxTv_SdtMenu_Menu_painom_N = 0;
            gxTv_SdtMenu_Menu_painom = (String)(value);
         }

      }

      public void gxTv_SdtMenu_Menu_painom_SetNull( )
      {
         gxTv_SdtMenu_Menu_painom_N = 1;
         gxTv_SdtMenu_Menu_painom = "";
         return  ;
      }

      public bool gxTv_SdtMenu_Menu_painom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_PaiTip" )]
      [  XmlElement( ElementName = "Menu_PaiTip"   )]
      public short gxTpr_Menu_paitip
      {
         get {
            return gxTv_SdtMenu_Menu_paitip ;
         }

         set {
            gxTv_SdtMenu_Menu_paitip_N = 0;
            gxTv_SdtMenu_Menu_paitip = (short)(value);
         }

      }

      public void gxTv_SdtMenu_Menu_paitip_SetNull( )
      {
         gxTv_SdtMenu_Menu_paitip_N = 1;
         gxTv_SdtMenu_Menu_paitip = 0;
         return  ;
      }

      public bool gxTv_SdtMenu_Menu_paitip_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_PaiAti" )]
      [  XmlElement( ElementName = "Menu_PaiAti"   )]
      public bool gxTpr_Menu_paiati
      {
         get {
            return gxTv_SdtMenu_Menu_paiati ;
         }

         set {
            gxTv_SdtMenu_Menu_paiati_N = 0;
            gxTv_SdtMenu_Menu_paiati = value;
         }

      }

      public void gxTv_SdtMenu_Menu_paiati_SetNull( )
      {
         gxTv_SdtMenu_Menu_paiati_N = 1;
         gxTv_SdtMenu_Menu_paiati = false;
         return  ;
      }

      public bool gxTv_SdtMenu_Menu_paiati_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_Ativo" )]
      [  XmlElement( ElementName = "Menu_Ativo"   )]
      public bool gxTpr_Menu_ativo
      {
         get {
            return gxTv_SdtMenu_Menu_ativo ;
         }

         set {
            gxTv_SdtMenu_Menu_ativo = value;
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtMenu_Mode ;
         }

         set {
            gxTv_SdtMenu_Mode = (String)(value);
         }

      }

      public void gxTv_SdtMenu_Mode_SetNull( )
      {
         gxTv_SdtMenu_Mode = "";
         return  ;
      }

      public bool gxTv_SdtMenu_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtMenu_Initialized ;
         }

         set {
            gxTv_SdtMenu_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtMenu_Initialized_SetNull( )
      {
         gxTv_SdtMenu_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtMenu_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_Codigo_Z" )]
      [  XmlElement( ElementName = "Menu_Codigo_Z"   )]
      public int gxTpr_Menu_codigo_Z
      {
         get {
            return gxTv_SdtMenu_Menu_codigo_Z ;
         }

         set {
            gxTv_SdtMenu_Menu_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtMenu_Menu_codigo_Z_SetNull( )
      {
         gxTv_SdtMenu_Menu_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtMenu_Menu_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_Nome_Z" )]
      [  XmlElement( ElementName = "Menu_Nome_Z"   )]
      public String gxTpr_Menu_nome_Z
      {
         get {
            return gxTv_SdtMenu_Menu_nome_Z ;
         }

         set {
            gxTv_SdtMenu_Menu_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtMenu_Menu_nome_Z_SetNull( )
      {
         gxTv_SdtMenu_Menu_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtMenu_Menu_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_Descricao_Z" )]
      [  XmlElement( ElementName = "Menu_Descricao_Z"   )]
      public String gxTpr_Menu_descricao_Z
      {
         get {
            return gxTv_SdtMenu_Menu_descricao_Z ;
         }

         set {
            gxTv_SdtMenu_Menu_descricao_Z = (String)(value);
         }

      }

      public void gxTv_SdtMenu_Menu_descricao_Z_SetNull( )
      {
         gxTv_SdtMenu_Menu_descricao_Z = "";
         return  ;
      }

      public bool gxTv_SdtMenu_Menu_descricao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_Tipo_Z" )]
      [  XmlElement( ElementName = "Menu_Tipo_Z"   )]
      public short gxTpr_Menu_tipo_Z
      {
         get {
            return gxTv_SdtMenu_Menu_tipo_Z ;
         }

         set {
            gxTv_SdtMenu_Menu_tipo_Z = (short)(value);
         }

      }

      public void gxTv_SdtMenu_Menu_tipo_Z_SetNull( )
      {
         gxTv_SdtMenu_Menu_tipo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtMenu_Menu_tipo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_Link_Z" )]
      [  XmlElement( ElementName = "Menu_Link_Z"   )]
      public String gxTpr_Menu_link_Z
      {
         get {
            return gxTv_SdtMenu_Menu_link_Z ;
         }

         set {
            gxTv_SdtMenu_Menu_link_Z = (String)(value);
         }

      }

      public void gxTv_SdtMenu_Menu_link_Z_SetNull( )
      {
         gxTv_SdtMenu_Menu_link_Z = "";
         return  ;
      }

      public bool gxTv_SdtMenu_Menu_link_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_Ordem_Z" )]
      [  XmlElement( ElementName = "Menu_Ordem_Z"   )]
      public short gxTpr_Menu_ordem_Z
      {
         get {
            return gxTv_SdtMenu_Menu_ordem_Z ;
         }

         set {
            gxTv_SdtMenu_Menu_ordem_Z = (short)(value);
         }

      }

      public void gxTv_SdtMenu_Menu_ordem_Z_SetNull( )
      {
         gxTv_SdtMenu_Menu_ordem_Z = 0;
         return  ;
      }

      public bool gxTv_SdtMenu_Menu_ordem_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_PaiCod_Z" )]
      [  XmlElement( ElementName = "Menu_PaiCod_Z"   )]
      public int gxTpr_Menu_paicod_Z
      {
         get {
            return gxTv_SdtMenu_Menu_paicod_Z ;
         }

         set {
            gxTv_SdtMenu_Menu_paicod_Z = (int)(value);
         }

      }

      public void gxTv_SdtMenu_Menu_paicod_Z_SetNull( )
      {
         gxTv_SdtMenu_Menu_paicod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtMenu_Menu_paicod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_PaiNom_Z" )]
      [  XmlElement( ElementName = "Menu_PaiNom_Z"   )]
      public String gxTpr_Menu_painom_Z
      {
         get {
            return gxTv_SdtMenu_Menu_painom_Z ;
         }

         set {
            gxTv_SdtMenu_Menu_painom_Z = (String)(value);
         }

      }

      public void gxTv_SdtMenu_Menu_painom_Z_SetNull( )
      {
         gxTv_SdtMenu_Menu_painom_Z = "";
         return  ;
      }

      public bool gxTv_SdtMenu_Menu_painom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_PaiTip_Z" )]
      [  XmlElement( ElementName = "Menu_PaiTip_Z"   )]
      public short gxTpr_Menu_paitip_Z
      {
         get {
            return gxTv_SdtMenu_Menu_paitip_Z ;
         }

         set {
            gxTv_SdtMenu_Menu_paitip_Z = (short)(value);
         }

      }

      public void gxTv_SdtMenu_Menu_paitip_Z_SetNull( )
      {
         gxTv_SdtMenu_Menu_paitip_Z = 0;
         return  ;
      }

      public bool gxTv_SdtMenu_Menu_paitip_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_PaiAti_Z" )]
      [  XmlElement( ElementName = "Menu_PaiAti_Z"   )]
      public bool gxTpr_Menu_paiati_Z
      {
         get {
            return gxTv_SdtMenu_Menu_paiati_Z ;
         }

         set {
            gxTv_SdtMenu_Menu_paiati_Z = value;
         }

      }

      public void gxTv_SdtMenu_Menu_paiati_Z_SetNull( )
      {
         gxTv_SdtMenu_Menu_paiati_Z = false;
         return  ;
      }

      public bool gxTv_SdtMenu_Menu_paiati_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_Ativo_Z" )]
      [  XmlElement( ElementName = "Menu_Ativo_Z"   )]
      public bool gxTpr_Menu_ativo_Z
      {
         get {
            return gxTv_SdtMenu_Menu_ativo_Z ;
         }

         set {
            gxTv_SdtMenu_Menu_ativo_Z = value;
         }

      }

      public void gxTv_SdtMenu_Menu_ativo_Z_SetNull( )
      {
         gxTv_SdtMenu_Menu_ativo_Z = false;
         return  ;
      }

      public bool gxTv_SdtMenu_Menu_ativo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_Imagem_GXI_Z" )]
      [  XmlElement( ElementName = "Menu_Imagem_GXI_Z"   )]
      public String gxTpr_Menu_imagem_gxi_Z
      {
         get {
            return gxTv_SdtMenu_Menu_imagem_gxi_Z ;
         }

         set {
            gxTv_SdtMenu_Menu_imagem_gxi_Z = (String)(value);
         }

      }

      public void gxTv_SdtMenu_Menu_imagem_gxi_Z_SetNull( )
      {
         gxTv_SdtMenu_Menu_imagem_gxi_Z = "";
         return  ;
      }

      public bool gxTv_SdtMenu_Menu_imagem_gxi_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_Descricao_N" )]
      [  XmlElement( ElementName = "Menu_Descricao_N"   )]
      public short gxTpr_Menu_descricao_N
      {
         get {
            return gxTv_SdtMenu_Menu_descricao_N ;
         }

         set {
            gxTv_SdtMenu_Menu_descricao_N = (short)(value);
         }

      }

      public void gxTv_SdtMenu_Menu_descricao_N_SetNull( )
      {
         gxTv_SdtMenu_Menu_descricao_N = 0;
         return  ;
      }

      public bool gxTv_SdtMenu_Menu_descricao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_Link_N" )]
      [  XmlElement( ElementName = "Menu_Link_N"   )]
      public short gxTpr_Menu_link_N
      {
         get {
            return gxTv_SdtMenu_Menu_link_N ;
         }

         set {
            gxTv_SdtMenu_Menu_link_N = (short)(value);
         }

      }

      public void gxTv_SdtMenu_Menu_link_N_SetNull( )
      {
         gxTv_SdtMenu_Menu_link_N = 0;
         return  ;
      }

      public bool gxTv_SdtMenu_Menu_link_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_Imagem_N" )]
      [  XmlElement( ElementName = "Menu_Imagem_N"   )]
      public short gxTpr_Menu_imagem_N
      {
         get {
            return gxTv_SdtMenu_Menu_imagem_N ;
         }

         set {
            gxTv_SdtMenu_Menu_imagem_N = (short)(value);
         }

      }

      public void gxTv_SdtMenu_Menu_imagem_N_SetNull( )
      {
         gxTv_SdtMenu_Menu_imagem_N = 0;
         return  ;
      }

      public bool gxTv_SdtMenu_Menu_imagem_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_PaiCod_N" )]
      [  XmlElement( ElementName = "Menu_PaiCod_N"   )]
      public short gxTpr_Menu_paicod_N
      {
         get {
            return gxTv_SdtMenu_Menu_paicod_N ;
         }

         set {
            gxTv_SdtMenu_Menu_paicod_N = (short)(value);
         }

      }

      public void gxTv_SdtMenu_Menu_paicod_N_SetNull( )
      {
         gxTv_SdtMenu_Menu_paicod_N = 0;
         return  ;
      }

      public bool gxTv_SdtMenu_Menu_paicod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_PaiNom_N" )]
      [  XmlElement( ElementName = "Menu_PaiNom_N"   )]
      public short gxTpr_Menu_painom_N
      {
         get {
            return gxTv_SdtMenu_Menu_painom_N ;
         }

         set {
            gxTv_SdtMenu_Menu_painom_N = (short)(value);
         }

      }

      public void gxTv_SdtMenu_Menu_painom_N_SetNull( )
      {
         gxTv_SdtMenu_Menu_painom_N = 0;
         return  ;
      }

      public bool gxTv_SdtMenu_Menu_painom_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_PaiTip_N" )]
      [  XmlElement( ElementName = "Menu_PaiTip_N"   )]
      public short gxTpr_Menu_paitip_N
      {
         get {
            return gxTv_SdtMenu_Menu_paitip_N ;
         }

         set {
            gxTv_SdtMenu_Menu_paitip_N = (short)(value);
         }

      }

      public void gxTv_SdtMenu_Menu_paitip_N_SetNull( )
      {
         gxTv_SdtMenu_Menu_paitip_N = 0;
         return  ;
      }

      public bool gxTv_SdtMenu_Menu_paitip_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_PaiAti_N" )]
      [  XmlElement( ElementName = "Menu_PaiAti_N"   )]
      public short gxTpr_Menu_paiati_N
      {
         get {
            return gxTv_SdtMenu_Menu_paiati_N ;
         }

         set {
            gxTv_SdtMenu_Menu_paiati_N = (short)(value);
         }

      }

      public void gxTv_SdtMenu_Menu_paiati_N_SetNull( )
      {
         gxTv_SdtMenu_Menu_paiati_N = 0;
         return  ;
      }

      public bool gxTv_SdtMenu_Menu_paiati_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Menu_Imagem_GXI_N" )]
      [  XmlElement( ElementName = "Menu_Imagem_GXI_N"   )]
      public short gxTpr_Menu_imagem_gxi_N
      {
         get {
            return gxTv_SdtMenu_Menu_imagem_gxi_N ;
         }

         set {
            gxTv_SdtMenu_Menu_imagem_gxi_N = (short)(value);
         }

      }

      public void gxTv_SdtMenu_Menu_imagem_gxi_N_SetNull( )
      {
         gxTv_SdtMenu_Menu_imagem_gxi_N = 0;
         return  ;
      }

      public bool gxTv_SdtMenu_Menu_imagem_gxi_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtMenu_Menu_nome = "";
         gxTv_SdtMenu_Menu_descricao = "";
         gxTv_SdtMenu_Menu_link = "";
         gxTv_SdtMenu_Menu_imagem = "";
         gxTv_SdtMenu_Menu_imagem_gxi = "";
         gxTv_SdtMenu_Menu_painom = "";
         gxTv_SdtMenu_Mode = "";
         gxTv_SdtMenu_Menu_nome_Z = "";
         gxTv_SdtMenu_Menu_descricao_Z = "";
         gxTv_SdtMenu_Menu_link_Z = "";
         gxTv_SdtMenu_Menu_painom_Z = "";
         gxTv_SdtMenu_Menu_imagem_gxi_Z = "";
         gxTv_SdtMenu_Menu_ordem = 0;
         gxTv_SdtMenu_Menu_paiati = true;
         gxTv_SdtMenu_Menu_ativo = true;
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "menu", "GeneXus.Programs.menu_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtMenu_Menu_tipo ;
      private short gxTv_SdtMenu_Menu_ordem ;
      private short gxTv_SdtMenu_Menu_paitip ;
      private short gxTv_SdtMenu_Initialized ;
      private short gxTv_SdtMenu_Menu_tipo_Z ;
      private short gxTv_SdtMenu_Menu_ordem_Z ;
      private short gxTv_SdtMenu_Menu_paitip_Z ;
      private short gxTv_SdtMenu_Menu_descricao_N ;
      private short gxTv_SdtMenu_Menu_link_N ;
      private short gxTv_SdtMenu_Menu_imagem_N ;
      private short gxTv_SdtMenu_Menu_paicod_N ;
      private short gxTv_SdtMenu_Menu_painom_N ;
      private short gxTv_SdtMenu_Menu_paitip_N ;
      private short gxTv_SdtMenu_Menu_paiati_N ;
      private short gxTv_SdtMenu_Menu_imagem_gxi_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtMenu_Menu_codigo ;
      private int gxTv_SdtMenu_Menu_paicod ;
      private int gxTv_SdtMenu_Menu_codigo_Z ;
      private int gxTv_SdtMenu_Menu_paicod_Z ;
      private String gxTv_SdtMenu_Menu_nome ;
      private String gxTv_SdtMenu_Menu_painom ;
      private String gxTv_SdtMenu_Mode ;
      private String gxTv_SdtMenu_Menu_nome_Z ;
      private String gxTv_SdtMenu_Menu_painom_Z ;
      private String sTagName ;
      private bool gxTv_SdtMenu_Menu_paiati ;
      private bool gxTv_SdtMenu_Menu_ativo ;
      private bool gxTv_SdtMenu_Menu_paiati_Z ;
      private bool gxTv_SdtMenu_Menu_ativo_Z ;
      private String gxTv_SdtMenu_Menu_descricao ;
      private String gxTv_SdtMenu_Menu_link ;
      private String gxTv_SdtMenu_Menu_imagem_gxi ;
      private String gxTv_SdtMenu_Menu_descricao_Z ;
      private String gxTv_SdtMenu_Menu_link_Z ;
      private String gxTv_SdtMenu_Menu_imagem_gxi_Z ;
      private String gxTv_SdtMenu_Menu_imagem ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"Menu", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtMenu_RESTInterface : GxGenericCollectionItem<SdtMenu>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtMenu_RESTInterface( ) : base()
      {
      }

      public SdtMenu_RESTInterface( SdtMenu psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Menu_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Menu_codigo
      {
         get {
            return sdt.gxTpr_Menu_codigo ;
         }

         set {
            sdt.gxTpr_Menu_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Menu_Nome" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Menu_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Menu_nome) ;
         }

         set {
            sdt.gxTpr_Menu_nome = (String)(value);
         }

      }

      [DataMember( Name = "Menu_Descricao" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Menu_descricao
      {
         get {
            return sdt.gxTpr_Menu_descricao ;
         }

         set {
            sdt.gxTpr_Menu_descricao = (String)(value);
         }

      }

      [DataMember( Name = "Menu_Tipo" , Order = 3 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Menu_tipo
      {
         get {
            return sdt.gxTpr_Menu_tipo ;
         }

         set {
            sdt.gxTpr_Menu_tipo = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Menu_Link" , Order = 4 )]
      [GxSeudo()]
      public String gxTpr_Menu_link
      {
         get {
            return sdt.gxTpr_Menu_link ;
         }

         set {
            sdt.gxTpr_Menu_link = (String)(value);
         }

      }

      [DataMember( Name = "Menu_Imagem" , Order = 5 )]
      [GxUpload()]
      public String gxTpr_Menu_imagem
      {
         get {
            return (!String.IsNullOrEmpty(StringUtil.RTrim( sdt.gxTpr_Menu_imagem)) ? PathUtil.RelativePath( sdt.gxTpr_Menu_imagem) : StringUtil.RTrim( sdt.gxTpr_Menu_imagem_gxi)) ;
         }

         set {
            sdt.gxTpr_Menu_imagem = (String)(value);
         }

      }

      [DataMember( Name = "Menu_Ordem" , Order = 7 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Menu_ordem
      {
         get {
            return sdt.gxTpr_Menu_ordem ;
         }

         set {
            sdt.gxTpr_Menu_ordem = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Menu_PaiCod" , Order = 8 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Menu_paicod
      {
         get {
            return sdt.gxTpr_Menu_paicod ;
         }

         set {
            sdt.gxTpr_Menu_paicod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Menu_PaiNom" , Order = 9 )]
      [GxSeudo()]
      public String gxTpr_Menu_painom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Menu_painom) ;
         }

         set {
            sdt.gxTpr_Menu_painom = (String)(value);
         }

      }

      [DataMember( Name = "Menu_PaiTip" , Order = 10 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Menu_paitip
      {
         get {
            return sdt.gxTpr_Menu_paitip ;
         }

         set {
            sdt.gxTpr_Menu_paitip = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Menu_PaiAti" , Order = 11 )]
      [GxSeudo()]
      public bool gxTpr_Menu_paiati
      {
         get {
            return sdt.gxTpr_Menu_paiati ;
         }

         set {
            sdt.gxTpr_Menu_paiati = value;
         }

      }

      [DataMember( Name = "Menu_Ativo" , Order = 12 )]
      [GxSeudo()]
      public bool gxTpr_Menu_ativo
      {
         get {
            return sdt.gxTpr_Menu_ativo ;
         }

         set {
            sdt.gxTpr_Menu_ativo = value;
         }

      }

      public SdtMenu sdt
      {
         get {
            return (SdtMenu)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtMenu() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 35 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
