/*
               File: type_SdtSDT_AuntenticacaoOUT
        Description: SDT_AuntenticacaoOUT
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:59.21
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_AuntenticacaoOUT" )]
   [XmlType(TypeName =  "SDT_AuntenticacaoOUT" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_AuntenticacaoOUT_Error ))]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_AuntenticacaoOUT_Usuario ))]
   [Serializable]
   public class SdtSDT_AuntenticacaoOUT : GxUserType
   {
      public SdtSDT_AuntenticacaoOUT( )
      {
         /* Constructor for serialization */
      }

      public SdtSDT_AuntenticacaoOUT( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_AuntenticacaoOUT deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_AuntenticacaoOUT)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_AuntenticacaoOUT obj ;
         obj = this;
         obj.gxTpr_Isok = deserialized.gxTpr_Isok;
         obj.gxTpr_Errors = deserialized.gxTpr_Errors;
         obj.gxTpr_Usuario = deserialized.gxTpr_Usuario;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "IsOk") )
               {
                  gxTv_SdtSDT_AuntenticacaoOUT_Isok = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Errors") )
               {
                  if ( gxTv_SdtSDT_AuntenticacaoOUT_Errors == null )
                  {
                     gxTv_SdtSDT_AuntenticacaoOUT_Errors = new GxObjectCollection( context, "SDT_AuntenticacaoOUT.Error", "GxEv3Up14_MeetrikaVs3", "SdtSDT_AuntenticacaoOUT_Error", "GeneXus.Programs");
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtSDT_AuntenticacaoOUT_Errors.readxmlcollection(oReader, "Errors", "Error");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario") )
               {
                  if ( gxTv_SdtSDT_AuntenticacaoOUT_Usuario == null )
                  {
                     gxTv_SdtSDT_AuntenticacaoOUT_Usuario = new SdtSDT_AuntenticacaoOUT_Usuario(context);
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtSDT_AuntenticacaoOUT_Usuario.readxml(oReader, "Usuario");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_AuntenticacaoOUT";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("IsOk", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSDT_AuntenticacaoOUT_Isok)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( gxTv_SdtSDT_AuntenticacaoOUT_Errors != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_MeetrikaVs3";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_MeetrikaVs3";
            }
            gxTv_SdtSDT_AuntenticacaoOUT_Errors.writexmlcollection(oWriter, "Errors", sNameSpace1, "Error", sNameSpace1);
         }
         if ( gxTv_SdtSDT_AuntenticacaoOUT_Usuario != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_MeetrikaVs3";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_MeetrikaVs3";
            }
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario.writexml(oWriter, "Usuario", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("IsOk", gxTv_SdtSDT_AuntenticacaoOUT_Isok, false);
         if ( gxTv_SdtSDT_AuntenticacaoOUT_Errors != null )
         {
            AddObjectProperty("Errors", gxTv_SdtSDT_AuntenticacaoOUT_Errors, false);
         }
         if ( gxTv_SdtSDT_AuntenticacaoOUT_Usuario != null )
         {
            AddObjectProperty("Usuario", gxTv_SdtSDT_AuntenticacaoOUT_Usuario, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "IsOk" )]
      [  XmlElement( ElementName = "IsOk"   )]
      public bool gxTpr_Isok
      {
         get {
            return gxTv_SdtSDT_AuntenticacaoOUT_Isok ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Isok = value;
         }

      }

      public class gxTv_SdtSDT_AuntenticacaoOUT_Errors_SdtSDT_AuntenticacaoOUT_Error_80compatibility:SdtSDT_AuntenticacaoOUT_Error {}
      [  SoapElement( ElementName = "Errors" )]
      [  XmlArray( ElementName = "Errors"  )]
      [  XmlArrayItemAttribute( Type= typeof( SdtSDT_AuntenticacaoOUT_Error ), ElementName= "Error"  , IsNullable=false)]
      [  XmlArrayItemAttribute( Type= typeof( gxTv_SdtSDT_AuntenticacaoOUT_Errors_SdtSDT_AuntenticacaoOUT_Error_80compatibility ), ElementName= "SDT_AuntenticacaoOUT.Error"  , IsNullable=false)]
      public GxObjectCollection gxTpr_Errors_GxObjectCollection
      {
         get {
            if ( gxTv_SdtSDT_AuntenticacaoOUT_Errors == null )
            {
               gxTv_SdtSDT_AuntenticacaoOUT_Errors = new GxObjectCollection( context, "SDT_AuntenticacaoOUT.Error", "GxEv3Up14_MeetrikaVs3", "SdtSDT_AuntenticacaoOUT_Error", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtSDT_AuntenticacaoOUT_Errors ;
         }

         set {
            if ( gxTv_SdtSDT_AuntenticacaoOUT_Errors == null )
            {
               gxTv_SdtSDT_AuntenticacaoOUT_Errors = new GxObjectCollection( context, "SDT_AuntenticacaoOUT.Error", "GxEv3Up14_MeetrikaVs3", "SdtSDT_AuntenticacaoOUT_Error", "GeneXus.Programs");
            }
            gxTv_SdtSDT_AuntenticacaoOUT_Errors = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Errors
      {
         get {
            if ( gxTv_SdtSDT_AuntenticacaoOUT_Errors == null )
            {
               gxTv_SdtSDT_AuntenticacaoOUT_Errors = new GxObjectCollection( context, "SDT_AuntenticacaoOUT.Error", "GxEv3Up14_MeetrikaVs3", "SdtSDT_AuntenticacaoOUT_Error", "GeneXus.Programs");
            }
            return gxTv_SdtSDT_AuntenticacaoOUT_Errors ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Errors = value;
         }

      }

      public void gxTv_SdtSDT_AuntenticacaoOUT_Errors_SetNull( )
      {
         gxTv_SdtSDT_AuntenticacaoOUT_Errors = null;
         return  ;
      }

      public bool gxTv_SdtSDT_AuntenticacaoOUT_Errors_IsNull( )
      {
         if ( gxTv_SdtSDT_AuntenticacaoOUT_Errors == null )
         {
            return true ;
         }
         return false ;
      }

      [  SoapElement( ElementName = "Usuario" )]
      [  XmlElement( ElementName = "Usuario"   )]
      public SdtSDT_AuntenticacaoOUT_Usuario gxTpr_Usuario
      {
         get {
            if ( gxTv_SdtSDT_AuntenticacaoOUT_Usuario == null )
            {
               gxTv_SdtSDT_AuntenticacaoOUT_Usuario = new SdtSDT_AuntenticacaoOUT_Usuario(context);
            }
            return gxTv_SdtSDT_AuntenticacaoOUT_Usuario ;
         }

         set {
            gxTv_SdtSDT_AuntenticacaoOUT_Usuario = value;
         }

      }

      public void gxTv_SdtSDT_AuntenticacaoOUT_Usuario_SetNull( )
      {
         gxTv_SdtSDT_AuntenticacaoOUT_Usuario = null;
         return  ;
      }

      public bool gxTv_SdtSDT_AuntenticacaoOUT_Usuario_IsNull( )
      {
         if ( gxTv_SdtSDT_AuntenticacaoOUT_Usuario == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String sTagName ;
      protected bool gxTv_SdtSDT_AuntenticacaoOUT_Isok ;
      [ObjectCollection(ItemType=typeof( SdtSDT_AuntenticacaoOUT_Error ))]
      protected IGxCollection gxTv_SdtSDT_AuntenticacaoOUT_Errors=null ;
      protected SdtSDT_AuntenticacaoOUT_Usuario gxTv_SdtSDT_AuntenticacaoOUT_Usuario=null ;
   }

   [DataContract(Name = @"SDT_AuntenticacaoOUT", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtSDT_AuntenticacaoOUT_RESTInterface : GxGenericCollectionItem<SdtSDT_AuntenticacaoOUT>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_AuntenticacaoOUT_RESTInterface( ) : base()
      {
      }

      public SdtSDT_AuntenticacaoOUT_RESTInterface( SdtSDT_AuntenticacaoOUT psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "IsOk" , Order = 0 )]
      public bool gxTpr_Isok
      {
         get {
            return sdt.gxTpr_Isok ;
         }

         set {
            sdt.gxTpr_Isok = value;
         }

      }

      [DataMember( Name = "Errors" , Order = 1 )]
      public GxGenericCollection<SdtSDT_AuntenticacaoOUT_Error_RESTInterface> gxTpr_Errors
      {
         get {
            return new GxGenericCollection<SdtSDT_AuntenticacaoOUT_Error_RESTInterface>(sdt.gxTpr_Errors) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Errors);
         }

      }

      [DataMember( Name = "Usuario" , Order = 2 )]
      public SdtSDT_AuntenticacaoOUT_Usuario_RESTInterface gxTpr_Usuario
      {
         get {
            return new SdtSDT_AuntenticacaoOUT_Usuario_RESTInterface(sdt.gxTpr_Usuario) ;
         }

         set {
            sdt.gxTpr_Usuario = value.sdt;
         }

      }

      public SdtSDT_AuntenticacaoOUT sdt
      {
         get {
            return (SdtSDT_AuntenticacaoOUT)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_AuntenticacaoOUT() ;
         }
      }

   }

}
