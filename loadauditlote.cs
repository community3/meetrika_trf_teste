/*
               File: LoadAuditLote
        Description: Load Audit Lote
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:13:0.71
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class loadauditlote : GXProcedure
   {
      public loadauditlote( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public loadauditlote( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_SaveOldValues ,
                           ref wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ,
                           int aP2_Lote_Codigo ,
                           String aP3_ActualMode )
      {
         this.AV13SaveOldValues = aP0_SaveOldValues;
         this.AV10AuditingObject = aP1_AuditingObject;
         this.AV16Lote_Codigo = aP2_Lote_Codigo;
         this.AV14ActualMode = aP3_ActualMode;
         initialize();
         executePrivate();
         aP1_AuditingObject=this.AV10AuditingObject;
      }

      public void executeSubmit( String aP0_SaveOldValues ,
                                 ref wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ,
                                 int aP2_Lote_Codigo ,
                                 String aP3_ActualMode )
      {
         loadauditlote objloadauditlote;
         objloadauditlote = new loadauditlote();
         objloadauditlote.AV13SaveOldValues = aP0_SaveOldValues;
         objloadauditlote.AV10AuditingObject = aP1_AuditingObject;
         objloadauditlote.AV16Lote_Codigo = aP2_Lote_Codigo;
         objloadauditlote.AV14ActualMode = aP3_ActualMode;
         objloadauditlote.context.SetSubmitInitialConfig(context);
         objloadauditlote.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objloadauditlote);
         aP1_AuditingObject=this.AV10AuditingObject;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((loadauditlote)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( StringUtil.StrCmp(AV13SaveOldValues, "Y") == 0 )
         {
            if ( ( StringUtil.StrCmp(AV14ActualMode, "DLT") == 0 ) || ( StringUtil.StrCmp(AV14ActualMode, "UPD") == 0 ) )
            {
               /* Execute user subroutine: 'LOADOLDVALUES' */
               S111 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
            }
         }
         else
         {
            /* Execute user subroutine: 'LOADNEWVALUES' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADOLDVALUES' Routine */
         /* Using cursor P00A19 */
         pr_default.execute(0, new Object[] {AV16Lote_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A679Lote_NFeNomeArq = P00A19_A679Lote_NFeNomeArq[0];
            n679Lote_NFeNomeArq = P00A19_n679Lote_NFeNomeArq[0];
            A680Lote_NFeTipoArq = P00A19_A680Lote_NFeTipoArq[0];
            n680Lote_NFeTipoArq = P00A19_n680Lote_NFeTipoArq[0];
            A562Lote_Numero = P00A19_A562Lote_Numero[0];
            A595Lote_AreaTrabalhoCod = P00A19_A595Lote_AreaTrabalhoCod[0];
            A855AreaTrabalho_DiasParaPagar = P00A19_A855AreaTrabalho_DiasParaPagar[0];
            n855AreaTrabalho_DiasParaPagar = P00A19_n855AreaTrabalho_DiasParaPagar[0];
            A563Lote_Nome = P00A19_A563Lote_Nome[0];
            A564Lote_Data = P00A19_A564Lote_Data[0];
            A857Lote_PrevPagamento = P00A19_A857Lote_PrevPagamento[0];
            n857Lote_PrevPagamento = P00A19_n857Lote_PrevPagamento[0];
            A844Lote_DataContrato = P00A19_A844Lote_DataContrato[0];
            n844Lote_DataContrato = P00A19_n844Lote_DataContrato[0];
            A559Lote_UserCod = P00A19_A559Lote_UserCod[0];
            A560Lote_PessoaCod = P00A19_A560Lote_PessoaCod[0];
            n560Lote_PessoaCod = P00A19_n560Lote_PessoaCod[0];
            A561Lote_UserNom = P00A19_A561Lote_UserNom[0];
            n561Lote_UserNom = P00A19_n561Lote_UserNom[0];
            A565Lote_ValorPF = P00A19_A565Lote_ValorPF[0];
            A673Lote_NFe = P00A19_A673Lote_NFe[0];
            n673Lote_NFe = P00A19_n673Lote_NFe[0];
            A674Lote_DataNfe = P00A19_A674Lote_DataNfe[0];
            n674Lote_DataNfe = P00A19_n674Lote_DataNfe[0];
            A1001Lote_NFeDataProtocolo = P00A19_A1001Lote_NFeDataProtocolo[0];
            n1001Lote_NFeDataProtocolo = P00A19_n1001Lote_NFeDataProtocolo[0];
            A675Lote_LiqBanco = P00A19_A675Lote_LiqBanco[0];
            n675Lote_LiqBanco = P00A19_n675Lote_LiqBanco[0];
            A676Lote_LiqData = P00A19_A676Lote_LiqData[0];
            n676Lote_LiqData = P00A19_n676Lote_LiqData[0];
            A1053Lote_GlsData = P00A19_A1053Lote_GlsData[0];
            n1053Lote_GlsData = P00A19_n1053Lote_GlsData[0];
            A1054Lote_GlsDescricao = P00A19_A1054Lote_GlsDescricao[0];
            n1054Lote_GlsDescricao = P00A19_n1054Lote_GlsDescricao[0];
            A1055Lote_GlsValor = P00A19_A1055Lote_GlsValor[0];
            n1055Lote_GlsValor = P00A19_n1055Lote_GlsValor[0];
            A1056Lote_GlsUser = P00A19_A1056Lote_GlsUser[0];
            n1056Lote_GlsUser = P00A19_n1056Lote_GlsUser[0];
            A1069Lote_GLSigned = P00A19_A1069Lote_GLSigned[0];
            n1069Lote_GLSigned = P00A19_n1069Lote_GLSigned[0];
            A1070Lote_OSSigned = P00A19_A1070Lote_OSSigned[0];
            n1070Lote_OSSigned = P00A19_n1070Lote_OSSigned[0];
            A1071Lote_TASigned = P00A19_A1071Lote_TASigned[0];
            n1071Lote_TASigned = P00A19_n1071Lote_TASigned[0];
            A2055Lote_CntUsado = P00A19_A2055Lote_CntUsado[0];
            n2055Lote_CntUsado = P00A19_n2055Lote_CntUsado[0];
            A2056Lote_CntSaldo = P00A19_A2056Lote_CntSaldo[0];
            n2056Lote_CntSaldo = P00A19_n2056Lote_CntSaldo[0];
            A2088Lote_ParecerFinal = P00A19_A2088Lote_ParecerFinal[0];
            n2088Lote_ParecerFinal = P00A19_n2088Lote_ParecerFinal[0];
            A2087Lote_Comentarios = P00A19_A2087Lote_Comentarios[0];
            n2087Lote_Comentarios = P00A19_n2087Lote_Comentarios[0];
            A596Lote_Codigo = P00A19_A596Lote_Codigo[0];
            A567Lote_DataIni = P00A19_A567Lote_DataIni[0];
            A568Lote_DataFim = P00A19_A568Lote_DataFim[0];
            A569Lote_QtdeDmn = P00A19_A569Lote_QtdeDmn[0];
            A575Lote_Status = P00A19_A575Lote_Status[0];
            A1231Lote_ContratadaCod = P00A19_A1231Lote_ContratadaCod[0];
            A1748Lote_ContratadaNom = P00A19_A1748Lote_ContratadaNom[0];
            n1748Lote_ContratadaNom = P00A19_n1748Lote_ContratadaNom[0];
            A677Lote_LiqValor = P00A19_A677Lote_LiqValor[0];
            n677Lote_LiqValor = P00A19_n677Lote_LiqValor[0];
            A1057Lote_ValorGlosas = P00A19_A1057Lote_ValorGlosas[0];
            n1057Lote_ValorGlosas = P00A19_n1057Lote_ValorGlosas[0];
            A678Lote_NFeArq = P00A19_A678Lote_NFeArq[0];
            n678Lote_NFeArq = P00A19_n678Lote_NFeArq[0];
            A855AreaTrabalho_DiasParaPagar = P00A19_A855AreaTrabalho_DiasParaPagar[0];
            n855AreaTrabalho_DiasParaPagar = P00A19_n855AreaTrabalho_DiasParaPagar[0];
            A560Lote_PessoaCod = P00A19_A560Lote_PessoaCod[0];
            n560Lote_PessoaCod = P00A19_n560Lote_PessoaCod[0];
            A561Lote_UserNom = P00A19_A561Lote_UserNom[0];
            n561Lote_UserNom = P00A19_n561Lote_UserNom[0];
            A567Lote_DataIni = P00A19_A567Lote_DataIni[0];
            A568Lote_DataFim = P00A19_A568Lote_DataFim[0];
            A569Lote_QtdeDmn = P00A19_A569Lote_QtdeDmn[0];
            A575Lote_Status = P00A19_A575Lote_Status[0];
            A1231Lote_ContratadaCod = P00A19_A1231Lote_ContratadaCod[0];
            A1057Lote_ValorGlosas = P00A19_A1057Lote_ValorGlosas[0];
            n1057Lote_ValorGlosas = P00A19_n1057Lote_ValorGlosas[0];
            A1748Lote_ContratadaNom = P00A19_A1748Lote_ContratadaNom[0];
            n1748Lote_ContratadaNom = P00A19_n1748Lote_ContratadaNom[0];
            GetLote_ValorOSs( A596Lote_Codigo) ;
            A572Lote_Valor = (decimal)(A1058Lote_ValorOSs-A1057Lote_ValorGlosas);
            A681Lote_LiqPerc = ((Convert.ToDecimal(0)==A677Lote_LiqValor) ? (decimal)(0) : A572Lote_Valor/ (decimal)(A677Lote_LiqValor)*100);
            AV10AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
            AV10AuditingObject.gxTpr_Mode = AV14ActualMode;
            AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
            AV11AuditingObjectRecordItem.gxTpr_Tablename = "Lote";
            AV11AuditingObjectRecordItem.gxTpr_Mode = AV14ActualMode;
            AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Lote_Codigo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A596Lote_Codigo), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_Numero";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N�mero";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A562Lote_Numero;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_AreaTrabalhoCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "de Trabalho";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A595Lote_AreaTrabalhoCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_DiasParaPagar";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Dias para pagar";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A855AreaTrabalho_DiasParaPagar), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_Nome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A563Lote_Nome;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_Data";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.TToC( A564Lote_Data, 8, 5, 0, 3, "/", ":", " ");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_PrevPagamento";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Prev. Pagamento";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.DToC( A857Lote_PrevPagamento, 2, "/");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_DataContrato";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data do Contrato";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.DToC( A844Lote_DataContrato, 2, "/");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_UserCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Usu�rio";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A559Lote_UserCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_PessoaCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Pessoa";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A560Lote_PessoaCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_UserNom";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Respons�vel";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A561Lote_UserNom;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_ValorPF";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor PF R$";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A565Lote_ValorPF, 18, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_DataIni";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Per�odo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.DToC( A567Lote_DataIni, 2, "/");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_DataFim";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "-";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.DToC( A568Lote_DataFim, 2, "/");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_QtdeDmn";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Qtde Dmn";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A569Lote_QtdeDmn), 4, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_QtdePF";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Qtde PF";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A573Lote_QtdePF, 14, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_Status";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Status";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A575Lote_Status;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_ValorGlosas";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Glosas R$";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1057Lote_ValorGlosas, 18, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_ValorOSs";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "OS R$";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1058Lote_ValorOSs, 18, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_Valor";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor R$";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A572Lote_Valor, 18, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_ContratadaCod";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratada ";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1231Lote_ContratadaCod), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_ContratadaNom";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome Contratada";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1748Lote_ContratadaNom;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_NFe";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "NFe";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A673Lote_NFe), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_NFeArq";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Arquivo NFe";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A678Lote_NFeArq;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_NFeNomeArq";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A679Lote_NFeNomeArq;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_NFeTipoArq";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A680Lote_NFeTipoArq;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_DataNfe";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data emis�o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.DToC( A674Lote_DataNfe, 2, "/");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_NFeDataProtocolo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data do protocolo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.DToC( A1001Lote_NFeDataProtocolo, 2, "/");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_LiqBanco";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Banco";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A675Lote_LiqBanco;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_LiqData";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data Liquidado";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.DToC( A676Lote_LiqData, 2, "/");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_LiqValor";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor Liquidado R$";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A677Lote_LiqValor, 18, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_LiqPerc";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A681Lote_LiqPerc, 6, 2);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_GlsData";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data da glosa";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = context.localUtil.DToC( A1053Lote_GlsData, 2, "/");
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_GlsDescricao";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Descri��o";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A1054Lote_GlsDescricao;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_GlsValor";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A1055Lote_GlsValor, 12, 2);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_GlsUser";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Usu�rio";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( (decimal)(A1056Lote_GlsUser), 6, 0);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_GLSigned";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Gerencial assinado digitalmente";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1069Lote_GLSigned);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_OSSigned";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "OS assinado digitalmente";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1070Lote_OSSigned);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_TASigned";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "TA assinado digitalmente";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.BoolToStr( A1071Lote_TASigned);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_CntUsado";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Usado";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A2055Lote_CntUsado, 14, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_CntSaldo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Saldo";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = StringUtil.Str( A2056Lote_CntSaldo, 14, 5);
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_ParecerFinal";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Parecer final";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A2088Lote_ParecerFinal;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_Comentarios";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Coment�rios";
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
            AV12AuditingObjectRecordItemAttributeItem.gxTpr_Oldvalue = A2087Lote_Comentarios;
            AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
      }

      protected void S121( )
      {
         /* 'LOADNEWVALUES' Routine */
         /* Using cursor P00A117 */
         pr_default.execute(1, new Object[] {AV16Lote_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A679Lote_NFeNomeArq = P00A117_A679Lote_NFeNomeArq[0];
            n679Lote_NFeNomeArq = P00A117_n679Lote_NFeNomeArq[0];
            A680Lote_NFeTipoArq = P00A117_A680Lote_NFeTipoArq[0];
            n680Lote_NFeTipoArq = P00A117_n680Lote_NFeTipoArq[0];
            A562Lote_Numero = P00A117_A562Lote_Numero[0];
            A595Lote_AreaTrabalhoCod = P00A117_A595Lote_AreaTrabalhoCod[0];
            A855AreaTrabalho_DiasParaPagar = P00A117_A855AreaTrabalho_DiasParaPagar[0];
            n855AreaTrabalho_DiasParaPagar = P00A117_n855AreaTrabalho_DiasParaPagar[0];
            A563Lote_Nome = P00A117_A563Lote_Nome[0];
            A564Lote_Data = P00A117_A564Lote_Data[0];
            A857Lote_PrevPagamento = P00A117_A857Lote_PrevPagamento[0];
            n857Lote_PrevPagamento = P00A117_n857Lote_PrevPagamento[0];
            A844Lote_DataContrato = P00A117_A844Lote_DataContrato[0];
            n844Lote_DataContrato = P00A117_n844Lote_DataContrato[0];
            A559Lote_UserCod = P00A117_A559Lote_UserCod[0];
            A560Lote_PessoaCod = P00A117_A560Lote_PessoaCod[0];
            n560Lote_PessoaCod = P00A117_n560Lote_PessoaCod[0];
            A561Lote_UserNom = P00A117_A561Lote_UserNom[0];
            n561Lote_UserNom = P00A117_n561Lote_UserNom[0];
            A565Lote_ValorPF = P00A117_A565Lote_ValorPF[0];
            A673Lote_NFe = P00A117_A673Lote_NFe[0];
            n673Lote_NFe = P00A117_n673Lote_NFe[0];
            A674Lote_DataNfe = P00A117_A674Lote_DataNfe[0];
            n674Lote_DataNfe = P00A117_n674Lote_DataNfe[0];
            A1001Lote_NFeDataProtocolo = P00A117_A1001Lote_NFeDataProtocolo[0];
            n1001Lote_NFeDataProtocolo = P00A117_n1001Lote_NFeDataProtocolo[0];
            A675Lote_LiqBanco = P00A117_A675Lote_LiqBanco[0];
            n675Lote_LiqBanco = P00A117_n675Lote_LiqBanco[0];
            A676Lote_LiqData = P00A117_A676Lote_LiqData[0];
            n676Lote_LiqData = P00A117_n676Lote_LiqData[0];
            A1053Lote_GlsData = P00A117_A1053Lote_GlsData[0];
            n1053Lote_GlsData = P00A117_n1053Lote_GlsData[0];
            A1054Lote_GlsDescricao = P00A117_A1054Lote_GlsDescricao[0];
            n1054Lote_GlsDescricao = P00A117_n1054Lote_GlsDescricao[0];
            A1055Lote_GlsValor = P00A117_A1055Lote_GlsValor[0];
            n1055Lote_GlsValor = P00A117_n1055Lote_GlsValor[0];
            A1056Lote_GlsUser = P00A117_A1056Lote_GlsUser[0];
            n1056Lote_GlsUser = P00A117_n1056Lote_GlsUser[0];
            A1069Lote_GLSigned = P00A117_A1069Lote_GLSigned[0];
            n1069Lote_GLSigned = P00A117_n1069Lote_GLSigned[0];
            A1070Lote_OSSigned = P00A117_A1070Lote_OSSigned[0];
            n1070Lote_OSSigned = P00A117_n1070Lote_OSSigned[0];
            A1071Lote_TASigned = P00A117_A1071Lote_TASigned[0];
            n1071Lote_TASigned = P00A117_n1071Lote_TASigned[0];
            A2055Lote_CntUsado = P00A117_A2055Lote_CntUsado[0];
            n2055Lote_CntUsado = P00A117_n2055Lote_CntUsado[0];
            A2056Lote_CntSaldo = P00A117_A2056Lote_CntSaldo[0];
            n2056Lote_CntSaldo = P00A117_n2056Lote_CntSaldo[0];
            A2088Lote_ParecerFinal = P00A117_A2088Lote_ParecerFinal[0];
            n2088Lote_ParecerFinal = P00A117_n2088Lote_ParecerFinal[0];
            A2087Lote_Comentarios = P00A117_A2087Lote_Comentarios[0];
            n2087Lote_Comentarios = P00A117_n2087Lote_Comentarios[0];
            A596Lote_Codigo = P00A117_A596Lote_Codigo[0];
            A567Lote_DataIni = P00A117_A567Lote_DataIni[0];
            A568Lote_DataFim = P00A117_A568Lote_DataFim[0];
            A569Lote_QtdeDmn = P00A117_A569Lote_QtdeDmn[0];
            A575Lote_Status = P00A117_A575Lote_Status[0];
            A1231Lote_ContratadaCod = P00A117_A1231Lote_ContratadaCod[0];
            A1748Lote_ContratadaNom = P00A117_A1748Lote_ContratadaNom[0];
            n1748Lote_ContratadaNom = P00A117_n1748Lote_ContratadaNom[0];
            A677Lote_LiqValor = P00A117_A677Lote_LiqValor[0];
            n677Lote_LiqValor = P00A117_n677Lote_LiqValor[0];
            A1057Lote_ValorGlosas = P00A117_A1057Lote_ValorGlosas[0];
            n1057Lote_ValorGlosas = P00A117_n1057Lote_ValorGlosas[0];
            A678Lote_NFeArq = P00A117_A678Lote_NFeArq[0];
            n678Lote_NFeArq = P00A117_n678Lote_NFeArq[0];
            A855AreaTrabalho_DiasParaPagar = P00A117_A855AreaTrabalho_DiasParaPagar[0];
            n855AreaTrabalho_DiasParaPagar = P00A117_n855AreaTrabalho_DiasParaPagar[0];
            A560Lote_PessoaCod = P00A117_A560Lote_PessoaCod[0];
            n560Lote_PessoaCod = P00A117_n560Lote_PessoaCod[0];
            A561Lote_UserNom = P00A117_A561Lote_UserNom[0];
            n561Lote_UserNom = P00A117_n561Lote_UserNom[0];
            A567Lote_DataIni = P00A117_A567Lote_DataIni[0];
            A568Lote_DataFim = P00A117_A568Lote_DataFim[0];
            A569Lote_QtdeDmn = P00A117_A569Lote_QtdeDmn[0];
            A575Lote_Status = P00A117_A575Lote_Status[0];
            A1231Lote_ContratadaCod = P00A117_A1231Lote_ContratadaCod[0];
            A1057Lote_ValorGlosas = P00A117_A1057Lote_ValorGlosas[0];
            n1057Lote_ValorGlosas = P00A117_n1057Lote_ValorGlosas[0];
            A1748Lote_ContratadaNom = P00A117_A1748Lote_ContratadaNom[0];
            n1748Lote_ContratadaNom = P00A117_n1748Lote_ContratadaNom[0];
            GetLote_ValorOSs( A596Lote_Codigo) ;
            A572Lote_Valor = (decimal)(A1058Lote_ValorOSs-A1057Lote_ValorGlosas);
            A681Lote_LiqPerc = ((Convert.ToDecimal(0)==A677Lote_LiqValor) ? (decimal)(0) : A572Lote_Valor/ (decimal)(A677Lote_LiqValor)*100);
            if ( StringUtil.StrCmp(AV14ActualMode, "INS") == 0 )
            {
               AV10AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
               AV10AuditingObject.gxTpr_Mode = AV14ActualMode;
               AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
               AV11AuditingObjectRecordItem.gxTpr_Tablename = "Lote";
               AV10AuditingObject.gxTpr_Record.Add(AV11AuditingObjectRecordItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Lote_Codigo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A596Lote_Codigo), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_Numero";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "N�mero";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = true;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A562Lote_Numero;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_AreaTrabalhoCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "de Trabalho";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A595Lote_AreaTrabalhoCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "AreaTrabalho_DiasParaPagar";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Dias para pagar";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A855AreaTrabalho_DiasParaPagar), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_Nome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A563Lote_Nome;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_Data";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A564Lote_Data, 8, 5, 0, 3, "/", ":", " ");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_PrevPagamento";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Prev. Pagamento";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A857Lote_PrevPagamento, 2, "/");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_DataContrato";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data do Contrato";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A844Lote_DataContrato, 2, "/");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_UserCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Usu�rio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A559Lote_UserCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_PessoaCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Pessoa";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A560Lote_PessoaCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_UserNom";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Respons�vel";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A561Lote_UserNom;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_ValorPF";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor PF R$";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A565Lote_ValorPF, 18, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_DataIni";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Per�odo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A567Lote_DataIni, 2, "/");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_DataFim";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "-";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A568Lote_DataFim, 2, "/");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_QtdeDmn";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Qtde Dmn";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A569Lote_QtdeDmn), 4, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_QtdePF";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Qtde PF";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A573Lote_QtdePF, 14, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_Status";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Status";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A575Lote_Status;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_ValorGlosas";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Glosas R$";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1057Lote_ValorGlosas, 18, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_ValorOSs";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "OS R$";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1058Lote_ValorOSs, 18, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_Valor";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor R$";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A572Lote_Valor, 18, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_ContratadaCod";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Contratada ";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1231Lote_ContratadaCod), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_ContratadaNom";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome Contratada";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1748Lote_ContratadaNom;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_NFe";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "NFe";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A673Lote_NFe), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_NFeArq";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Arquivo NFe";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A678Lote_NFeArq;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_NFeNomeArq";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Nome";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A679Lote_NFeNomeArq;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_NFeTipoArq";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Tipo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A680Lote_NFeTipoArq;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_DataNfe";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data emis�o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A674Lote_DataNfe, 2, "/");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_NFeDataProtocolo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data do protocolo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A1001Lote_NFeDataProtocolo, 2, "/");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_LiqBanco";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Banco";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A675Lote_LiqBanco;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_LiqData";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data Liquidado";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A676Lote_LiqData, 2, "/");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_LiqValor";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor Liquidado R$";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A677Lote_LiqValor, 18, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_LiqPerc";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A681Lote_LiqPerc, 6, 2);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_GlsData";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Data da glosa";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A1053Lote_GlsData, 2, "/");
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_GlsDescricao";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Descri��o";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1054Lote_GlsDescricao;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_GlsValor";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Valor";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1055Lote_GlsValor, 12, 2);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_GlsUser";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Usu�rio";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1056Lote_GlsUser), 6, 0);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_GLSigned";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Gerencial assinado digitalmente";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1069Lote_GLSigned);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_OSSigned";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "OS assinado digitalmente";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1070Lote_OSSigned);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_TASigned";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "TA assinado digitalmente";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1071Lote_TASigned);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_CntUsado";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Usado";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A2055Lote_CntUsado, 14, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_CntSaldo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Saldo";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A2056Lote_CntSaldo, 14, 5);
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_ParecerFinal";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Parecer final";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2088Lote_ParecerFinal;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
               AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name = "Lote_Comentarios";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Description = "Coment�rios";
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Ispartofkey = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Isdescriptionattribute = false;
               AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2087Lote_Comentarios;
               AV11AuditingObjectRecordItem.gxTpr_Attribute.Add(AV12AuditingObjectRecordItemAttributeItem, 0);
            }
            if ( StringUtil.StrCmp(AV14ActualMode, "UPD") == 0 )
            {
               AV21GXV1 = 1;
               while ( AV21GXV1 <= AV10AuditingObject.gxTpr_Record.Count )
               {
                  AV11AuditingObjectRecordItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem)AV10AuditingObject.gxTpr_Record.Item(AV21GXV1));
                  while ( (pr_default.getStatus(1) != 101) && ( P00A117_A596Lote_Codigo[0] == A596Lote_Codigo ) )
                  {
                     A679Lote_NFeNomeArq = P00A117_A679Lote_NFeNomeArq[0];
                     n679Lote_NFeNomeArq = P00A117_n679Lote_NFeNomeArq[0];
                     A680Lote_NFeTipoArq = P00A117_A680Lote_NFeTipoArq[0];
                     n680Lote_NFeTipoArq = P00A117_n680Lote_NFeTipoArq[0];
                     A562Lote_Numero = P00A117_A562Lote_Numero[0];
                     A595Lote_AreaTrabalhoCod = P00A117_A595Lote_AreaTrabalhoCod[0];
                     A855AreaTrabalho_DiasParaPagar = P00A117_A855AreaTrabalho_DiasParaPagar[0];
                     n855AreaTrabalho_DiasParaPagar = P00A117_n855AreaTrabalho_DiasParaPagar[0];
                     A563Lote_Nome = P00A117_A563Lote_Nome[0];
                     A564Lote_Data = P00A117_A564Lote_Data[0];
                     A857Lote_PrevPagamento = P00A117_A857Lote_PrevPagamento[0];
                     n857Lote_PrevPagamento = P00A117_n857Lote_PrevPagamento[0];
                     A844Lote_DataContrato = P00A117_A844Lote_DataContrato[0];
                     n844Lote_DataContrato = P00A117_n844Lote_DataContrato[0];
                     A559Lote_UserCod = P00A117_A559Lote_UserCod[0];
                     A560Lote_PessoaCod = P00A117_A560Lote_PessoaCod[0];
                     n560Lote_PessoaCod = P00A117_n560Lote_PessoaCod[0];
                     A561Lote_UserNom = P00A117_A561Lote_UserNom[0];
                     n561Lote_UserNom = P00A117_n561Lote_UserNom[0];
                     A565Lote_ValorPF = P00A117_A565Lote_ValorPF[0];
                     A673Lote_NFe = P00A117_A673Lote_NFe[0];
                     n673Lote_NFe = P00A117_n673Lote_NFe[0];
                     A674Lote_DataNfe = P00A117_A674Lote_DataNfe[0];
                     n674Lote_DataNfe = P00A117_n674Lote_DataNfe[0];
                     A1001Lote_NFeDataProtocolo = P00A117_A1001Lote_NFeDataProtocolo[0];
                     n1001Lote_NFeDataProtocolo = P00A117_n1001Lote_NFeDataProtocolo[0];
                     A675Lote_LiqBanco = P00A117_A675Lote_LiqBanco[0];
                     n675Lote_LiqBanco = P00A117_n675Lote_LiqBanco[0];
                     A676Lote_LiqData = P00A117_A676Lote_LiqData[0];
                     n676Lote_LiqData = P00A117_n676Lote_LiqData[0];
                     A1053Lote_GlsData = P00A117_A1053Lote_GlsData[0];
                     n1053Lote_GlsData = P00A117_n1053Lote_GlsData[0];
                     A1054Lote_GlsDescricao = P00A117_A1054Lote_GlsDescricao[0];
                     n1054Lote_GlsDescricao = P00A117_n1054Lote_GlsDescricao[0];
                     A1055Lote_GlsValor = P00A117_A1055Lote_GlsValor[0];
                     n1055Lote_GlsValor = P00A117_n1055Lote_GlsValor[0];
                     A1056Lote_GlsUser = P00A117_A1056Lote_GlsUser[0];
                     n1056Lote_GlsUser = P00A117_n1056Lote_GlsUser[0];
                     A1069Lote_GLSigned = P00A117_A1069Lote_GLSigned[0];
                     n1069Lote_GLSigned = P00A117_n1069Lote_GLSigned[0];
                     A1070Lote_OSSigned = P00A117_A1070Lote_OSSigned[0];
                     n1070Lote_OSSigned = P00A117_n1070Lote_OSSigned[0];
                     A1071Lote_TASigned = P00A117_A1071Lote_TASigned[0];
                     n1071Lote_TASigned = P00A117_n1071Lote_TASigned[0];
                     A2055Lote_CntUsado = P00A117_A2055Lote_CntUsado[0];
                     n2055Lote_CntUsado = P00A117_n2055Lote_CntUsado[0];
                     A2056Lote_CntSaldo = P00A117_A2056Lote_CntSaldo[0];
                     n2056Lote_CntSaldo = P00A117_n2056Lote_CntSaldo[0];
                     A2088Lote_ParecerFinal = P00A117_A2088Lote_ParecerFinal[0];
                     n2088Lote_ParecerFinal = P00A117_n2088Lote_ParecerFinal[0];
                     A2087Lote_Comentarios = P00A117_A2087Lote_Comentarios[0];
                     n2087Lote_Comentarios = P00A117_n2087Lote_Comentarios[0];
                     A1748Lote_ContratadaNom = P00A117_A1748Lote_ContratadaNom[0];
                     n1748Lote_ContratadaNom = P00A117_n1748Lote_ContratadaNom[0];
                     A677Lote_LiqValor = P00A117_A677Lote_LiqValor[0];
                     n677Lote_LiqValor = P00A117_n677Lote_LiqValor[0];
                     A1057Lote_ValorGlosas = P00A117_A1057Lote_ValorGlosas[0];
                     n1057Lote_ValorGlosas = P00A117_n1057Lote_ValorGlosas[0];
                     A678Lote_NFeArq = P00A117_A678Lote_NFeArq[0];
                     n678Lote_NFeArq = P00A117_n678Lote_NFeArq[0];
                     A855AreaTrabalho_DiasParaPagar = P00A117_A855AreaTrabalho_DiasParaPagar[0];
                     n855AreaTrabalho_DiasParaPagar = P00A117_n855AreaTrabalho_DiasParaPagar[0];
                     A560Lote_PessoaCod = P00A117_A560Lote_PessoaCod[0];
                     n560Lote_PessoaCod = P00A117_n560Lote_PessoaCod[0];
                     A561Lote_UserNom = P00A117_A561Lote_UserNom[0];
                     n561Lote_UserNom = P00A117_n561Lote_UserNom[0];
                     A1057Lote_ValorGlosas = P00A117_A1057Lote_ValorGlosas[0];
                     n1057Lote_ValorGlosas = P00A117_n1057Lote_ValorGlosas[0];
                     A1748Lote_ContratadaNom = P00A117_A1748Lote_ContratadaNom[0];
                     n1748Lote_ContratadaNom = P00A117_n1748Lote_ContratadaNom[0];
                     /* Using cursor P00A120 */
                     pr_default.execute(2, new Object[] {A596Lote_Codigo});
                     if ( (pr_default.getStatus(2) != 101) )
                     {
                        A567Lote_DataIni = P00A120_A567Lote_DataIni[0];
                        A568Lote_DataFim = P00A120_A568Lote_DataFim[0];
                     }
                     else
                     {
                        A567Lote_DataIni = DateTime.MinValue;
                        A568Lote_DataFim = DateTime.MinValue;
                     }
                     pr_default.close(2);
                     /* Using cursor P00A122 */
                     pr_default.execute(3, new Object[] {A596Lote_Codigo});
                     if ( (pr_default.getStatus(3) != 101) )
                     {
                        A569Lote_QtdeDmn = P00A122_A569Lote_QtdeDmn[0];
                        A575Lote_Status = P00A122_A575Lote_Status[0];
                        A1231Lote_ContratadaCod = P00A122_A1231Lote_ContratadaCod[0];
                     }
                     else
                     {
                        A569Lote_QtdeDmn = 0;
                        A575Lote_Status = "";
                        A1231Lote_ContratadaCod = 0;
                     }
                     pr_default.close(3);
                     GetLote_ValorOSs( A596Lote_Codigo) ;
                     A572Lote_Valor = (decimal)(A1058Lote_ValorOSs-A1057Lote_ValorGlosas);
                     A681Lote_LiqPerc = ((Convert.ToDecimal(0)==A677Lote_LiqValor) ? (decimal)(0) : A572Lote_Valor/ (decimal)(A677Lote_LiqValor)*100);
                     AV23GXV2 = 1;
                     while ( AV23GXV2 <= AV11AuditingObjectRecordItem.gxTpr_Attribute.Count )
                     {
                        AV12AuditingObjectRecordItemAttributeItem = ((wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem)AV11AuditingObjectRecordItem.gxTpr_Attribute.Item(AV23GXV2));
                        if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_Codigo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A596Lote_Codigo), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_Numero") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A562Lote_Numero;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_AreaTrabalhoCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A595Lote_AreaTrabalhoCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "AreaTrabalho_DiasParaPagar") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A855AreaTrabalho_DiasParaPagar), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_Nome") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A563Lote_Nome;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_Data") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.TToC( A564Lote_Data, 8, 5, 0, 3, "/", ":", " ");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_PrevPagamento") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A857Lote_PrevPagamento, 2, "/");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_DataContrato") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A844Lote_DataContrato, 2, "/");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_UserCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A559Lote_UserCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_PessoaCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A560Lote_PessoaCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_UserNom") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A561Lote_UserNom;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_ValorPF") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A565Lote_ValorPF, 18, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_DataIni") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A567Lote_DataIni, 2, "/");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_DataFim") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A568Lote_DataFim, 2, "/");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_QtdeDmn") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A569Lote_QtdeDmn), 4, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_QtdePF") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A573Lote_QtdePF, 14, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_Status") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A575Lote_Status;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_ValorGlosas") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1057Lote_ValorGlosas, 18, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_ValorOSs") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1058Lote_ValorOSs, 18, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_Valor") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A572Lote_Valor, 18, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_ContratadaCod") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1231Lote_ContratadaCod), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_ContratadaNom") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1748Lote_ContratadaNom;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_NFe") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A673Lote_NFe), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_NFeArq") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A678Lote_NFeArq;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_NFeNomeArq") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A679Lote_NFeNomeArq;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_NFeTipoArq") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A680Lote_NFeTipoArq;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_DataNfe") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A674Lote_DataNfe, 2, "/");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_NFeDataProtocolo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A1001Lote_NFeDataProtocolo, 2, "/");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_LiqBanco") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A675Lote_LiqBanco;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_LiqData") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A676Lote_LiqData, 2, "/");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_LiqValor") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A677Lote_LiqValor, 18, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_LiqPerc") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A681Lote_LiqPerc, 6, 2);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_GlsData") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = context.localUtil.DToC( A1053Lote_GlsData, 2, "/");
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_GlsDescricao") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A1054Lote_GlsDescricao;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_GlsValor") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A1055Lote_GlsValor, 12, 2);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_GlsUser") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( (decimal)(A1056Lote_GlsUser), 6, 0);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_GLSigned") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1069Lote_GLSigned);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_OSSigned") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1070Lote_OSSigned);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_TASigned") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.BoolToStr( A1071Lote_TASigned);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_CntUsado") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A2055Lote_CntUsado, 14, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_CntSaldo") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = StringUtil.Str( A2056Lote_CntSaldo, 14, 5);
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_ParecerFinal") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2088Lote_ParecerFinal;
                        }
                        else if ( StringUtil.StrCmp(AV12AuditingObjectRecordItemAttributeItem.gxTpr_Name, "Lote_Comentarios") == 0 )
                        {
                           AV12AuditingObjectRecordItemAttributeItem.gxTpr_Newvalue = A2087Lote_Comentarios;
                        }
                        AV23GXV2 = (int)(AV23GXV2+1);
                     }
                     /* Exiting from a For First loop. */
                     if (true) break;
                  }
                  AV21GXV1 = (int)(AV21GXV1+1);
               }
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
      }

      protected void GetLote_ValorOSs( int A596Lote_Codigo )
      {
         /* Create private dbobjects */
         /* Navigation */
         A1058Lote_ValorOSs = 0;
         A573Lote_QtdePF = 0;
         /* Using cursor P00A123 */
         pr_default.execute(4, new Object[] {A596Lote_Codigo});
         while ( (pr_default.getStatus(4) != 101) && ( P00A123_A597ContagemResultado_LoteAceiteCod[0] == A596Lote_Codigo ) )
         {
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  P00A123_A456ContagemResultado_Codigo[0], out  GXt_decimal1) ;
            A574ContagemResultado_PFFinal = GXt_decimal1;
            A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*P00A123_A512ContagemResultado_ValorPF[0]);
            A1058Lote_ValorOSs = (decimal)(A1058Lote_ValorOSs+A606ContagemResultado_ValorFinal);
            A573Lote_QtdePF = (decimal)(A573Lote_QtdePF+A574ContagemResultado_PFFinal);
            pr_default.readNext(4);
         }
         pr_default.close(4);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00A19_A679Lote_NFeNomeArq = new String[] {""} ;
         P00A19_n679Lote_NFeNomeArq = new bool[] {false} ;
         P00A19_A680Lote_NFeTipoArq = new String[] {""} ;
         P00A19_n680Lote_NFeTipoArq = new bool[] {false} ;
         P00A19_A562Lote_Numero = new String[] {""} ;
         P00A19_A595Lote_AreaTrabalhoCod = new int[1] ;
         P00A19_A855AreaTrabalho_DiasParaPagar = new short[1] ;
         P00A19_n855AreaTrabalho_DiasParaPagar = new bool[] {false} ;
         P00A19_A563Lote_Nome = new String[] {""} ;
         P00A19_A564Lote_Data = new DateTime[] {DateTime.MinValue} ;
         P00A19_A857Lote_PrevPagamento = new DateTime[] {DateTime.MinValue} ;
         P00A19_n857Lote_PrevPagamento = new bool[] {false} ;
         P00A19_A844Lote_DataContrato = new DateTime[] {DateTime.MinValue} ;
         P00A19_n844Lote_DataContrato = new bool[] {false} ;
         P00A19_A559Lote_UserCod = new int[1] ;
         P00A19_A560Lote_PessoaCod = new int[1] ;
         P00A19_n560Lote_PessoaCod = new bool[] {false} ;
         P00A19_A561Lote_UserNom = new String[] {""} ;
         P00A19_n561Lote_UserNom = new bool[] {false} ;
         P00A19_A565Lote_ValorPF = new decimal[1] ;
         P00A19_A673Lote_NFe = new int[1] ;
         P00A19_n673Lote_NFe = new bool[] {false} ;
         P00A19_A674Lote_DataNfe = new DateTime[] {DateTime.MinValue} ;
         P00A19_n674Lote_DataNfe = new bool[] {false} ;
         P00A19_A1001Lote_NFeDataProtocolo = new DateTime[] {DateTime.MinValue} ;
         P00A19_n1001Lote_NFeDataProtocolo = new bool[] {false} ;
         P00A19_A675Lote_LiqBanco = new String[] {""} ;
         P00A19_n675Lote_LiqBanco = new bool[] {false} ;
         P00A19_A676Lote_LiqData = new DateTime[] {DateTime.MinValue} ;
         P00A19_n676Lote_LiqData = new bool[] {false} ;
         P00A19_A1053Lote_GlsData = new DateTime[] {DateTime.MinValue} ;
         P00A19_n1053Lote_GlsData = new bool[] {false} ;
         P00A19_A1054Lote_GlsDescricao = new String[] {""} ;
         P00A19_n1054Lote_GlsDescricao = new bool[] {false} ;
         P00A19_A1055Lote_GlsValor = new decimal[1] ;
         P00A19_n1055Lote_GlsValor = new bool[] {false} ;
         P00A19_A1056Lote_GlsUser = new int[1] ;
         P00A19_n1056Lote_GlsUser = new bool[] {false} ;
         P00A19_A1069Lote_GLSigned = new bool[] {false} ;
         P00A19_n1069Lote_GLSigned = new bool[] {false} ;
         P00A19_A1070Lote_OSSigned = new bool[] {false} ;
         P00A19_n1070Lote_OSSigned = new bool[] {false} ;
         P00A19_A1071Lote_TASigned = new bool[] {false} ;
         P00A19_n1071Lote_TASigned = new bool[] {false} ;
         P00A19_A2055Lote_CntUsado = new decimal[1] ;
         P00A19_n2055Lote_CntUsado = new bool[] {false} ;
         P00A19_A2056Lote_CntSaldo = new decimal[1] ;
         P00A19_n2056Lote_CntSaldo = new bool[] {false} ;
         P00A19_A2088Lote_ParecerFinal = new String[] {""} ;
         P00A19_n2088Lote_ParecerFinal = new bool[] {false} ;
         P00A19_A2087Lote_Comentarios = new String[] {""} ;
         P00A19_n2087Lote_Comentarios = new bool[] {false} ;
         P00A19_A596Lote_Codigo = new int[1] ;
         P00A19_A567Lote_DataIni = new DateTime[] {DateTime.MinValue} ;
         P00A19_A568Lote_DataFim = new DateTime[] {DateTime.MinValue} ;
         P00A19_A569Lote_QtdeDmn = new short[1] ;
         P00A19_A575Lote_Status = new String[] {""} ;
         P00A19_A1231Lote_ContratadaCod = new int[1] ;
         P00A19_A1748Lote_ContratadaNom = new String[] {""} ;
         P00A19_n1748Lote_ContratadaNom = new bool[] {false} ;
         P00A19_A677Lote_LiqValor = new decimal[1] ;
         P00A19_n677Lote_LiqValor = new bool[] {false} ;
         P00A19_A1057Lote_ValorGlosas = new decimal[1] ;
         P00A19_n1057Lote_ValorGlosas = new bool[] {false} ;
         P00A19_A678Lote_NFeArq = new String[] {""} ;
         P00A19_n678Lote_NFeArq = new bool[] {false} ;
         A679Lote_NFeNomeArq = "";
         A680Lote_NFeTipoArq = "";
         A562Lote_Numero = "";
         A563Lote_Nome = "";
         A564Lote_Data = (DateTime)(DateTime.MinValue);
         A857Lote_PrevPagamento = DateTime.MinValue;
         A844Lote_DataContrato = DateTime.MinValue;
         A561Lote_UserNom = "";
         A674Lote_DataNfe = DateTime.MinValue;
         A1001Lote_NFeDataProtocolo = DateTime.MinValue;
         A675Lote_LiqBanco = "";
         A676Lote_LiqData = DateTime.MinValue;
         A1053Lote_GlsData = DateTime.MinValue;
         A1054Lote_GlsDescricao = "";
         A2088Lote_ParecerFinal = "";
         A2087Lote_Comentarios = "";
         A567Lote_DataIni = DateTime.MinValue;
         A568Lote_DataFim = DateTime.MinValue;
         A575Lote_Status = "";
         A1748Lote_ContratadaNom = "";
         A678Lote_NFeArq = "";
         AV11AuditingObjectRecordItem = new wwpbaseobjects.SdtAuditingObject_RecordItem(context);
         AV12AuditingObjectRecordItemAttributeItem = new wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem(context);
         P00A117_A679Lote_NFeNomeArq = new String[] {""} ;
         P00A117_n679Lote_NFeNomeArq = new bool[] {false} ;
         P00A117_A680Lote_NFeTipoArq = new String[] {""} ;
         P00A117_n680Lote_NFeTipoArq = new bool[] {false} ;
         P00A117_A562Lote_Numero = new String[] {""} ;
         P00A117_A595Lote_AreaTrabalhoCod = new int[1] ;
         P00A117_A855AreaTrabalho_DiasParaPagar = new short[1] ;
         P00A117_n855AreaTrabalho_DiasParaPagar = new bool[] {false} ;
         P00A117_A563Lote_Nome = new String[] {""} ;
         P00A117_A564Lote_Data = new DateTime[] {DateTime.MinValue} ;
         P00A117_A857Lote_PrevPagamento = new DateTime[] {DateTime.MinValue} ;
         P00A117_n857Lote_PrevPagamento = new bool[] {false} ;
         P00A117_A844Lote_DataContrato = new DateTime[] {DateTime.MinValue} ;
         P00A117_n844Lote_DataContrato = new bool[] {false} ;
         P00A117_A559Lote_UserCod = new int[1] ;
         P00A117_A560Lote_PessoaCod = new int[1] ;
         P00A117_n560Lote_PessoaCod = new bool[] {false} ;
         P00A117_A561Lote_UserNom = new String[] {""} ;
         P00A117_n561Lote_UserNom = new bool[] {false} ;
         P00A117_A565Lote_ValorPF = new decimal[1] ;
         P00A117_A673Lote_NFe = new int[1] ;
         P00A117_n673Lote_NFe = new bool[] {false} ;
         P00A117_A674Lote_DataNfe = new DateTime[] {DateTime.MinValue} ;
         P00A117_n674Lote_DataNfe = new bool[] {false} ;
         P00A117_A1001Lote_NFeDataProtocolo = new DateTime[] {DateTime.MinValue} ;
         P00A117_n1001Lote_NFeDataProtocolo = new bool[] {false} ;
         P00A117_A675Lote_LiqBanco = new String[] {""} ;
         P00A117_n675Lote_LiqBanco = new bool[] {false} ;
         P00A117_A676Lote_LiqData = new DateTime[] {DateTime.MinValue} ;
         P00A117_n676Lote_LiqData = new bool[] {false} ;
         P00A117_A1053Lote_GlsData = new DateTime[] {DateTime.MinValue} ;
         P00A117_n1053Lote_GlsData = new bool[] {false} ;
         P00A117_A1054Lote_GlsDescricao = new String[] {""} ;
         P00A117_n1054Lote_GlsDescricao = new bool[] {false} ;
         P00A117_A1055Lote_GlsValor = new decimal[1] ;
         P00A117_n1055Lote_GlsValor = new bool[] {false} ;
         P00A117_A1056Lote_GlsUser = new int[1] ;
         P00A117_n1056Lote_GlsUser = new bool[] {false} ;
         P00A117_A1069Lote_GLSigned = new bool[] {false} ;
         P00A117_n1069Lote_GLSigned = new bool[] {false} ;
         P00A117_A1070Lote_OSSigned = new bool[] {false} ;
         P00A117_n1070Lote_OSSigned = new bool[] {false} ;
         P00A117_A1071Lote_TASigned = new bool[] {false} ;
         P00A117_n1071Lote_TASigned = new bool[] {false} ;
         P00A117_A2055Lote_CntUsado = new decimal[1] ;
         P00A117_n2055Lote_CntUsado = new bool[] {false} ;
         P00A117_A2056Lote_CntSaldo = new decimal[1] ;
         P00A117_n2056Lote_CntSaldo = new bool[] {false} ;
         P00A117_A2088Lote_ParecerFinal = new String[] {""} ;
         P00A117_n2088Lote_ParecerFinal = new bool[] {false} ;
         P00A117_A2087Lote_Comentarios = new String[] {""} ;
         P00A117_n2087Lote_Comentarios = new bool[] {false} ;
         P00A117_A596Lote_Codigo = new int[1] ;
         P00A117_A567Lote_DataIni = new DateTime[] {DateTime.MinValue} ;
         P00A117_A568Lote_DataFim = new DateTime[] {DateTime.MinValue} ;
         P00A117_A569Lote_QtdeDmn = new short[1] ;
         P00A117_A575Lote_Status = new String[] {""} ;
         P00A117_A1231Lote_ContratadaCod = new int[1] ;
         P00A117_A1748Lote_ContratadaNom = new String[] {""} ;
         P00A117_n1748Lote_ContratadaNom = new bool[] {false} ;
         P00A117_A677Lote_LiqValor = new decimal[1] ;
         P00A117_n677Lote_LiqValor = new bool[] {false} ;
         P00A117_A1057Lote_ValorGlosas = new decimal[1] ;
         P00A117_n1057Lote_ValorGlosas = new bool[] {false} ;
         P00A117_A678Lote_NFeArq = new String[] {""} ;
         P00A117_n678Lote_NFeArq = new bool[] {false} ;
         P00A120_A567Lote_DataIni = new DateTime[] {DateTime.MinValue} ;
         P00A120_A568Lote_DataFim = new DateTime[] {DateTime.MinValue} ;
         P00A122_A569Lote_QtdeDmn = new short[1] ;
         P00A122_A575Lote_Status = new String[] {""} ;
         P00A122_A1231Lote_ContratadaCod = new int[1] ;
         P00A123_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P00A123_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P00A123_A512ContagemResultado_ValorPF = new decimal[1] ;
         P00A123_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P00A123_A456ContagemResultado_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.loadauditlote__default(),
            new Object[][] {
                new Object[] {
               P00A19_A679Lote_NFeNomeArq, P00A19_n679Lote_NFeNomeArq, P00A19_A680Lote_NFeTipoArq, P00A19_n680Lote_NFeTipoArq, P00A19_A562Lote_Numero, P00A19_A595Lote_AreaTrabalhoCod, P00A19_A855AreaTrabalho_DiasParaPagar, P00A19_n855AreaTrabalho_DiasParaPagar, P00A19_A563Lote_Nome, P00A19_A564Lote_Data,
               P00A19_A857Lote_PrevPagamento, P00A19_n857Lote_PrevPagamento, P00A19_A844Lote_DataContrato, P00A19_n844Lote_DataContrato, P00A19_A559Lote_UserCod, P00A19_A560Lote_PessoaCod, P00A19_n560Lote_PessoaCod, P00A19_A561Lote_UserNom, P00A19_n561Lote_UserNom, P00A19_A565Lote_ValorPF,
               P00A19_A673Lote_NFe, P00A19_n673Lote_NFe, P00A19_A674Lote_DataNfe, P00A19_n674Lote_DataNfe, P00A19_A1001Lote_NFeDataProtocolo, P00A19_n1001Lote_NFeDataProtocolo, P00A19_A675Lote_LiqBanco, P00A19_n675Lote_LiqBanco, P00A19_A676Lote_LiqData, P00A19_n676Lote_LiqData,
               P00A19_A1053Lote_GlsData, P00A19_n1053Lote_GlsData, P00A19_A1054Lote_GlsDescricao, P00A19_n1054Lote_GlsDescricao, P00A19_A1055Lote_GlsValor, P00A19_n1055Lote_GlsValor, P00A19_A1056Lote_GlsUser, P00A19_n1056Lote_GlsUser, P00A19_A1069Lote_GLSigned, P00A19_n1069Lote_GLSigned,
               P00A19_A1070Lote_OSSigned, P00A19_n1070Lote_OSSigned, P00A19_A1071Lote_TASigned, P00A19_n1071Lote_TASigned, P00A19_A2055Lote_CntUsado, P00A19_n2055Lote_CntUsado, P00A19_A2056Lote_CntSaldo, P00A19_n2056Lote_CntSaldo, P00A19_A2088Lote_ParecerFinal, P00A19_n2088Lote_ParecerFinal,
               P00A19_A2087Lote_Comentarios, P00A19_n2087Lote_Comentarios, P00A19_A596Lote_Codigo, P00A19_A567Lote_DataIni, P00A19_A568Lote_DataFim, P00A19_A569Lote_QtdeDmn, P00A19_A575Lote_Status, P00A19_A1231Lote_ContratadaCod, P00A19_A1748Lote_ContratadaNom, P00A19_n1748Lote_ContratadaNom,
               P00A19_A677Lote_LiqValor, P00A19_n677Lote_LiqValor, P00A19_A1057Lote_ValorGlosas, P00A19_n1057Lote_ValorGlosas, P00A19_A678Lote_NFeArq, P00A19_n678Lote_NFeArq
               }
               , new Object[] {
               P00A117_A679Lote_NFeNomeArq, P00A117_n679Lote_NFeNomeArq, P00A117_A680Lote_NFeTipoArq, P00A117_n680Lote_NFeTipoArq, P00A117_A562Lote_Numero, P00A117_A595Lote_AreaTrabalhoCod, P00A117_A855AreaTrabalho_DiasParaPagar, P00A117_n855AreaTrabalho_DiasParaPagar, P00A117_A563Lote_Nome, P00A117_A564Lote_Data,
               P00A117_A857Lote_PrevPagamento, P00A117_n857Lote_PrevPagamento, P00A117_A844Lote_DataContrato, P00A117_n844Lote_DataContrato, P00A117_A559Lote_UserCod, P00A117_A560Lote_PessoaCod, P00A117_n560Lote_PessoaCod, P00A117_A561Lote_UserNom, P00A117_n561Lote_UserNom, P00A117_A565Lote_ValorPF,
               P00A117_A673Lote_NFe, P00A117_n673Lote_NFe, P00A117_A674Lote_DataNfe, P00A117_n674Lote_DataNfe, P00A117_A1001Lote_NFeDataProtocolo, P00A117_n1001Lote_NFeDataProtocolo, P00A117_A675Lote_LiqBanco, P00A117_n675Lote_LiqBanco, P00A117_A676Lote_LiqData, P00A117_n676Lote_LiqData,
               P00A117_A1053Lote_GlsData, P00A117_n1053Lote_GlsData, P00A117_A1054Lote_GlsDescricao, P00A117_n1054Lote_GlsDescricao, P00A117_A1055Lote_GlsValor, P00A117_n1055Lote_GlsValor, P00A117_A1056Lote_GlsUser, P00A117_n1056Lote_GlsUser, P00A117_A1069Lote_GLSigned, P00A117_n1069Lote_GLSigned,
               P00A117_A1070Lote_OSSigned, P00A117_n1070Lote_OSSigned, P00A117_A1071Lote_TASigned, P00A117_n1071Lote_TASigned, P00A117_A2055Lote_CntUsado, P00A117_n2055Lote_CntUsado, P00A117_A2056Lote_CntSaldo, P00A117_n2056Lote_CntSaldo, P00A117_A2088Lote_ParecerFinal, P00A117_n2088Lote_ParecerFinal,
               P00A117_A2087Lote_Comentarios, P00A117_n2087Lote_Comentarios, P00A117_A596Lote_Codigo, P00A117_A567Lote_DataIni, P00A117_A568Lote_DataFim, P00A117_A569Lote_QtdeDmn, P00A117_A575Lote_Status, P00A117_A1231Lote_ContratadaCod, P00A117_A1748Lote_ContratadaNom, P00A117_n1748Lote_ContratadaNom,
               P00A117_A677Lote_LiqValor, P00A117_n677Lote_LiqValor, P00A117_A1057Lote_ValorGlosas, P00A117_n1057Lote_ValorGlosas, P00A117_A678Lote_NFeArq, P00A117_n678Lote_NFeArq
               }
               , new Object[] {
               P00A120_A567Lote_DataIni, P00A120_A568Lote_DataFim
               }
               , new Object[] {
               P00A122_A569Lote_QtdeDmn, P00A122_A575Lote_Status, P00A122_A1231Lote_ContratadaCod
               }
               , new Object[] {
               P00A123_A597ContagemResultado_LoteAceiteCod, P00A123_n597ContagemResultado_LoteAceiteCod, P00A123_A512ContagemResultado_ValorPF, P00A123_n512ContagemResultado_ValorPF, P00A123_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A855AreaTrabalho_DiasParaPagar ;
      private short A569Lote_QtdeDmn ;
      private int AV16Lote_Codigo ;
      private int A595Lote_AreaTrabalhoCod ;
      private int A559Lote_UserCod ;
      private int A560Lote_PessoaCod ;
      private int A673Lote_NFe ;
      private int A1056Lote_GlsUser ;
      private int A596Lote_Codigo ;
      private int A1231Lote_ContratadaCod ;
      private int AV21GXV1 ;
      private int AV23GXV2 ;
      private decimal A565Lote_ValorPF ;
      private decimal A1055Lote_GlsValor ;
      private decimal A2055Lote_CntUsado ;
      private decimal A2056Lote_CntSaldo ;
      private decimal A677Lote_LiqValor ;
      private decimal A1057Lote_ValorGlosas ;
      private decimal A572Lote_Valor ;
      private decimal A1058Lote_ValorOSs ;
      private decimal A681Lote_LiqPerc ;
      private decimal A573Lote_QtdePF ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal GXt_decimal1 ;
      private decimal A606ContagemResultado_ValorFinal ;
      private String AV13SaveOldValues ;
      private String AV14ActualMode ;
      private String scmdbuf ;
      private String A679Lote_NFeNomeArq ;
      private String A680Lote_NFeTipoArq ;
      private String A562Lote_Numero ;
      private String A563Lote_Nome ;
      private String A561Lote_UserNom ;
      private String A675Lote_LiqBanco ;
      private String A575Lote_Status ;
      private String A1748Lote_ContratadaNom ;
      private DateTime A564Lote_Data ;
      private DateTime A857Lote_PrevPagamento ;
      private DateTime A844Lote_DataContrato ;
      private DateTime A674Lote_DataNfe ;
      private DateTime A1001Lote_NFeDataProtocolo ;
      private DateTime A676Lote_LiqData ;
      private DateTime A1053Lote_GlsData ;
      private DateTime A567Lote_DataIni ;
      private DateTime A568Lote_DataFim ;
      private bool returnInSub ;
      private bool n679Lote_NFeNomeArq ;
      private bool n680Lote_NFeTipoArq ;
      private bool n855AreaTrabalho_DiasParaPagar ;
      private bool n857Lote_PrevPagamento ;
      private bool n844Lote_DataContrato ;
      private bool n560Lote_PessoaCod ;
      private bool n561Lote_UserNom ;
      private bool n673Lote_NFe ;
      private bool n674Lote_DataNfe ;
      private bool n1001Lote_NFeDataProtocolo ;
      private bool n675Lote_LiqBanco ;
      private bool n676Lote_LiqData ;
      private bool n1053Lote_GlsData ;
      private bool n1054Lote_GlsDescricao ;
      private bool n1055Lote_GlsValor ;
      private bool n1056Lote_GlsUser ;
      private bool A1069Lote_GLSigned ;
      private bool n1069Lote_GLSigned ;
      private bool A1070Lote_OSSigned ;
      private bool n1070Lote_OSSigned ;
      private bool A1071Lote_TASigned ;
      private bool n1071Lote_TASigned ;
      private bool n2055Lote_CntUsado ;
      private bool n2056Lote_CntSaldo ;
      private bool n2088Lote_ParecerFinal ;
      private bool n2087Lote_Comentarios ;
      private bool n1748Lote_ContratadaNom ;
      private bool n677Lote_LiqValor ;
      private bool n1057Lote_ValorGlosas ;
      private bool n678Lote_NFeArq ;
      private String A1054Lote_GlsDescricao ;
      private String A2088Lote_ParecerFinal ;
      private String A2087Lote_Comentarios ;
      private String A678Lote_NFeArq ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private wwpbaseobjects.SdtAuditingObject aP1_AuditingObject ;
      private IDataStoreProvider pr_default ;
      private String[] P00A19_A679Lote_NFeNomeArq ;
      private bool[] P00A19_n679Lote_NFeNomeArq ;
      private String[] P00A19_A680Lote_NFeTipoArq ;
      private bool[] P00A19_n680Lote_NFeTipoArq ;
      private String[] P00A19_A562Lote_Numero ;
      private int[] P00A19_A595Lote_AreaTrabalhoCod ;
      private short[] P00A19_A855AreaTrabalho_DiasParaPagar ;
      private bool[] P00A19_n855AreaTrabalho_DiasParaPagar ;
      private String[] P00A19_A563Lote_Nome ;
      private DateTime[] P00A19_A564Lote_Data ;
      private DateTime[] P00A19_A857Lote_PrevPagamento ;
      private bool[] P00A19_n857Lote_PrevPagamento ;
      private DateTime[] P00A19_A844Lote_DataContrato ;
      private bool[] P00A19_n844Lote_DataContrato ;
      private int[] P00A19_A559Lote_UserCod ;
      private int[] P00A19_A560Lote_PessoaCod ;
      private bool[] P00A19_n560Lote_PessoaCod ;
      private String[] P00A19_A561Lote_UserNom ;
      private bool[] P00A19_n561Lote_UserNom ;
      private decimal[] P00A19_A565Lote_ValorPF ;
      private int[] P00A19_A673Lote_NFe ;
      private bool[] P00A19_n673Lote_NFe ;
      private DateTime[] P00A19_A674Lote_DataNfe ;
      private bool[] P00A19_n674Lote_DataNfe ;
      private DateTime[] P00A19_A1001Lote_NFeDataProtocolo ;
      private bool[] P00A19_n1001Lote_NFeDataProtocolo ;
      private String[] P00A19_A675Lote_LiqBanco ;
      private bool[] P00A19_n675Lote_LiqBanco ;
      private DateTime[] P00A19_A676Lote_LiqData ;
      private bool[] P00A19_n676Lote_LiqData ;
      private DateTime[] P00A19_A1053Lote_GlsData ;
      private bool[] P00A19_n1053Lote_GlsData ;
      private String[] P00A19_A1054Lote_GlsDescricao ;
      private bool[] P00A19_n1054Lote_GlsDescricao ;
      private decimal[] P00A19_A1055Lote_GlsValor ;
      private bool[] P00A19_n1055Lote_GlsValor ;
      private int[] P00A19_A1056Lote_GlsUser ;
      private bool[] P00A19_n1056Lote_GlsUser ;
      private bool[] P00A19_A1069Lote_GLSigned ;
      private bool[] P00A19_n1069Lote_GLSigned ;
      private bool[] P00A19_A1070Lote_OSSigned ;
      private bool[] P00A19_n1070Lote_OSSigned ;
      private bool[] P00A19_A1071Lote_TASigned ;
      private bool[] P00A19_n1071Lote_TASigned ;
      private decimal[] P00A19_A2055Lote_CntUsado ;
      private bool[] P00A19_n2055Lote_CntUsado ;
      private decimal[] P00A19_A2056Lote_CntSaldo ;
      private bool[] P00A19_n2056Lote_CntSaldo ;
      private String[] P00A19_A2088Lote_ParecerFinal ;
      private bool[] P00A19_n2088Lote_ParecerFinal ;
      private String[] P00A19_A2087Lote_Comentarios ;
      private bool[] P00A19_n2087Lote_Comentarios ;
      private int[] P00A19_A596Lote_Codigo ;
      private DateTime[] P00A19_A567Lote_DataIni ;
      private DateTime[] P00A19_A568Lote_DataFim ;
      private short[] P00A19_A569Lote_QtdeDmn ;
      private String[] P00A19_A575Lote_Status ;
      private int[] P00A19_A1231Lote_ContratadaCod ;
      private String[] P00A19_A1748Lote_ContratadaNom ;
      private bool[] P00A19_n1748Lote_ContratadaNom ;
      private decimal[] P00A19_A677Lote_LiqValor ;
      private bool[] P00A19_n677Lote_LiqValor ;
      private decimal[] P00A19_A1057Lote_ValorGlosas ;
      private bool[] P00A19_n1057Lote_ValorGlosas ;
      private String[] P00A19_A678Lote_NFeArq ;
      private bool[] P00A19_n678Lote_NFeArq ;
      private String[] P00A117_A679Lote_NFeNomeArq ;
      private bool[] P00A117_n679Lote_NFeNomeArq ;
      private String[] P00A117_A680Lote_NFeTipoArq ;
      private bool[] P00A117_n680Lote_NFeTipoArq ;
      private String[] P00A117_A562Lote_Numero ;
      private int[] P00A117_A595Lote_AreaTrabalhoCod ;
      private short[] P00A117_A855AreaTrabalho_DiasParaPagar ;
      private bool[] P00A117_n855AreaTrabalho_DiasParaPagar ;
      private String[] P00A117_A563Lote_Nome ;
      private DateTime[] P00A117_A564Lote_Data ;
      private DateTime[] P00A117_A857Lote_PrevPagamento ;
      private bool[] P00A117_n857Lote_PrevPagamento ;
      private DateTime[] P00A117_A844Lote_DataContrato ;
      private bool[] P00A117_n844Lote_DataContrato ;
      private int[] P00A117_A559Lote_UserCod ;
      private int[] P00A117_A560Lote_PessoaCod ;
      private bool[] P00A117_n560Lote_PessoaCod ;
      private String[] P00A117_A561Lote_UserNom ;
      private bool[] P00A117_n561Lote_UserNom ;
      private decimal[] P00A117_A565Lote_ValorPF ;
      private int[] P00A117_A673Lote_NFe ;
      private bool[] P00A117_n673Lote_NFe ;
      private DateTime[] P00A117_A674Lote_DataNfe ;
      private bool[] P00A117_n674Lote_DataNfe ;
      private DateTime[] P00A117_A1001Lote_NFeDataProtocolo ;
      private bool[] P00A117_n1001Lote_NFeDataProtocolo ;
      private String[] P00A117_A675Lote_LiqBanco ;
      private bool[] P00A117_n675Lote_LiqBanco ;
      private DateTime[] P00A117_A676Lote_LiqData ;
      private bool[] P00A117_n676Lote_LiqData ;
      private DateTime[] P00A117_A1053Lote_GlsData ;
      private bool[] P00A117_n1053Lote_GlsData ;
      private String[] P00A117_A1054Lote_GlsDescricao ;
      private bool[] P00A117_n1054Lote_GlsDescricao ;
      private decimal[] P00A117_A1055Lote_GlsValor ;
      private bool[] P00A117_n1055Lote_GlsValor ;
      private int[] P00A117_A1056Lote_GlsUser ;
      private bool[] P00A117_n1056Lote_GlsUser ;
      private bool[] P00A117_A1069Lote_GLSigned ;
      private bool[] P00A117_n1069Lote_GLSigned ;
      private bool[] P00A117_A1070Lote_OSSigned ;
      private bool[] P00A117_n1070Lote_OSSigned ;
      private bool[] P00A117_A1071Lote_TASigned ;
      private bool[] P00A117_n1071Lote_TASigned ;
      private decimal[] P00A117_A2055Lote_CntUsado ;
      private bool[] P00A117_n2055Lote_CntUsado ;
      private decimal[] P00A117_A2056Lote_CntSaldo ;
      private bool[] P00A117_n2056Lote_CntSaldo ;
      private String[] P00A117_A2088Lote_ParecerFinal ;
      private bool[] P00A117_n2088Lote_ParecerFinal ;
      private String[] P00A117_A2087Lote_Comentarios ;
      private bool[] P00A117_n2087Lote_Comentarios ;
      private int[] P00A117_A596Lote_Codigo ;
      private DateTime[] P00A117_A567Lote_DataIni ;
      private DateTime[] P00A117_A568Lote_DataFim ;
      private short[] P00A117_A569Lote_QtdeDmn ;
      private String[] P00A117_A575Lote_Status ;
      private int[] P00A117_A1231Lote_ContratadaCod ;
      private String[] P00A117_A1748Lote_ContratadaNom ;
      private bool[] P00A117_n1748Lote_ContratadaNom ;
      private decimal[] P00A117_A677Lote_LiqValor ;
      private bool[] P00A117_n677Lote_LiqValor ;
      private decimal[] P00A117_A1057Lote_ValorGlosas ;
      private bool[] P00A117_n1057Lote_ValorGlosas ;
      private String[] P00A117_A678Lote_NFeArq ;
      private bool[] P00A117_n678Lote_NFeArq ;
      private DateTime[] P00A120_A567Lote_DataIni ;
      private DateTime[] P00A120_A568Lote_DataFim ;
      private short[] P00A122_A569Lote_QtdeDmn ;
      private String[] P00A122_A575Lote_Status ;
      private int[] P00A122_A1231Lote_ContratadaCod ;
      private int[] P00A123_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P00A123_n597ContagemResultado_LoteAceiteCod ;
      private decimal[] P00A123_A512ContagemResultado_ValorPF ;
      private bool[] P00A123_n512ContagemResultado_ValorPF ;
      private int[] P00A123_A456ContagemResultado_Codigo ;
      private wwpbaseobjects.SdtAuditingObject AV10AuditingObject ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem AV11AuditingObjectRecordItem ;
      private wwpbaseobjects.SdtAuditingObject_RecordItem_AttributeItem AV12AuditingObjectRecordItemAttributeItem ;
   }

   public class loadauditlote__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00A19 ;
          prmP00A19 = new Object[] {
          new Object[] {"@AV16Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferP00A19 ;
          cmdBufferP00A19=" SELECT T1.[Lote_NFeNomeArq], T1.[Lote_NFeTipoArq], T1.[Lote_Numero], T1.[Lote_AreaTrabalhoCod] AS Lote_AreaTrabalhoCod, T2.[AreaTrabalho_DiasParaPagar], T1.[Lote_Nome], T1.[Lote_Data], T1.[Lote_PrevPagamento], T1.[Lote_DataContrato], T1.[Lote_UserCod] AS Lote_UserCod, T3.[Usuario_PessoaCod] AS Lote_PessoaCod, T4.[Pessoa_Nome] AS Lote_UserNom, T1.[Lote_ValorPF], T1.[Lote_NFe], T1.[Lote_DataNfe], T1.[Lote_NFeDataProtocolo], T1.[Lote_LiqBanco], T1.[Lote_LiqData], T1.[Lote_GlsData], T1.[Lote_GlsDescricao], T1.[Lote_GlsValor], T1.[Lote_GlsUser], T1.[Lote_GLSigned], T1.[Lote_OSSigned], T1.[Lote_TASigned], T1.[Lote_CntUsado], T1.[Lote_CntSaldo], T1.[Lote_ParecerFinal], T1.[Lote_Comentarios], T1.[Lote_Codigo], COALESCE( T5.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) AS Lote_DataIni, COALESCE( T5.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) AS Lote_DataFim, COALESCE( T6.[Lote_QtdeDmn], 0) AS Lote_QtdeDmn, COALESCE( T6.[Lote_Status], '') AS Lote_Status, COALESCE( T6.[Lote_ContratadaCod], 0) AS Lote_ContratadaCod, COALESCE( T8.[Lote_ContratadaNom], '') AS Lote_ContratadaNom, T1.[Lote_LiqValor], COALESCE( T7.[Lote_ValorGlosas], 0) AS Lote_ValorGlosas, T1.[Lote_NFeArq] FROM ((((((([Lote] T1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[Lote_AreaTrabalhoCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[Lote_UserCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) LEFT JOIN (SELECT MIN(COALESCE( T10.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataIni, T9.[ContagemResultado_LoteAceiteCod], MAX(COALESCE( T10.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataFim FROM ([ContagemResultado] T9 WITH (NOLOCK) "
          + " LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T10 ON T10.[ContagemResultado_Codigo] = T9.[ContagemResultado_Codigo]) GROUP BY T9.[ContagemResultado_LoteAceiteCod] ) T5 ON T5.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo]) LEFT JOIN (SELECT COUNT(*) AS Lote_QtdeDmn, [ContagemResultado_LoteAceiteCod], MIN([ContagemResultado_StatusDmn]) AS Lote_Status, MIN([ContagemResultado_ContratadaCod]) AS Lote_ContratadaCod FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T6 ON T6.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo]) INNER JOIN (SELECT COALESCE( T11.[GXC2], 0) + COALESCE( T10.[GXC3], 0) AS Lote_ValorGlosas, T9.[Lote_Codigo] FROM (([Lote] T9 WITH (NOLOCK) LEFT JOIN (SELECT SUM(T12.[ContagemResultadoIndicadores_Valor]) AS GXC3, T13.[Lote_Codigo] FROM [ContagemResultadoIndicadores] T12 WITH (NOLOCK),  [Lote] T13 WITH (NOLOCK) WHERE T12.[ContagemResultadoIndicadores_LoteCod] = T13.[Lote_Codigo] GROUP BY T13.[Lote_Codigo] ) T10 ON T10.[Lote_Codigo] = T9.[Lote_Codigo]) LEFT JOIN (SELECT SUM([ContagemResultado_GlsValor]) AS GXC2, [ContagemResultado_LoteAceiteCod] FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T11 ON T11.[ContagemResultado_LoteAceiteCod] = T9.[Lote_Codigo]) ) T7 ON T7.[Lote_Codigo] = T1.[Lote_Codigo]) LEFT JOIN (SELECT MIN(T11.[Pessoa_Nome]) AS Lote_ContratadaNom, T9.[ContagemResultado_LoteAceiteCod] FROM (([ContagemResultado] T9 WITH (NOLOCK) LEFT JOIN [Contratada] T10 WITH (NOLOCK) ON T10.[Contratada_Codigo] = T9.[ContagemResultado_ContratadaCod]) LEFT JOIN [Pessoa] T11 WITH (NOLOCK) ON T11.[Pessoa_Codigo]"
          + " = T10.[Contratada_PessoaCod]) GROUP BY T9.[ContagemResultado_LoteAceiteCod] ) T8 ON T8.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo]) WHERE T1.[Lote_Codigo] = @AV16Lote_Codigo ORDER BY T1.[Lote_Codigo]" ;
          Object[] prmP00A117 ;
          prmP00A117 = new Object[] {
          new Object[] {"@AV16Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferP00A117 ;
          cmdBufferP00A117=" SELECT T1.[Lote_NFeNomeArq], T1.[Lote_NFeTipoArq], T1.[Lote_Numero], T1.[Lote_AreaTrabalhoCod] AS Lote_AreaTrabalhoCod, T2.[AreaTrabalho_DiasParaPagar], T1.[Lote_Nome], T1.[Lote_Data], T1.[Lote_PrevPagamento], T1.[Lote_DataContrato], T1.[Lote_UserCod] AS Lote_UserCod, T3.[Usuario_PessoaCod] AS Lote_PessoaCod, T4.[Pessoa_Nome] AS Lote_UserNom, T1.[Lote_ValorPF], T1.[Lote_NFe], T1.[Lote_DataNfe], T1.[Lote_NFeDataProtocolo], T1.[Lote_LiqBanco], T1.[Lote_LiqData], T1.[Lote_GlsData], T1.[Lote_GlsDescricao], T1.[Lote_GlsValor], T1.[Lote_GlsUser], T1.[Lote_GLSigned], T1.[Lote_OSSigned], T1.[Lote_TASigned], T1.[Lote_CntUsado], T1.[Lote_CntSaldo], T1.[Lote_ParecerFinal], T1.[Lote_Comentarios], T1.[Lote_Codigo], COALESCE( T5.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) AS Lote_DataIni, COALESCE( T5.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) AS Lote_DataFim, COALESCE( T6.[Lote_QtdeDmn], 0) AS Lote_QtdeDmn, COALESCE( T6.[Lote_Status], '') AS Lote_Status, COALESCE( T6.[Lote_ContratadaCod], 0) AS Lote_ContratadaCod, COALESCE( T8.[Lote_ContratadaNom], '') AS Lote_ContratadaNom, T1.[Lote_LiqValor], COALESCE( T7.[Lote_ValorGlosas], 0) AS Lote_ValorGlosas, T1.[Lote_NFeArq] FROM ((((((([Lote] T1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[Lote_AreaTrabalhoCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[Lote_UserCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) LEFT JOIN (SELECT MIN(COALESCE( T10.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataIni, T9.[ContagemResultado_LoteAceiteCod], MAX(COALESCE( T10.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataFim FROM ([ContagemResultado] T9 WITH (NOLOCK) "
          + " LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T10 ON T10.[ContagemResultado_Codigo] = T9.[ContagemResultado_Codigo]) GROUP BY T9.[ContagemResultado_LoteAceiteCod] ) T5 ON T5.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo]) LEFT JOIN (SELECT COUNT(*) AS Lote_QtdeDmn, [ContagemResultado_LoteAceiteCod], MIN([ContagemResultado_StatusDmn]) AS Lote_Status, MIN([ContagemResultado_ContratadaCod]) AS Lote_ContratadaCod FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T6 ON T6.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo]) INNER JOIN (SELECT COALESCE( T11.[GXC2], 0) + COALESCE( T10.[GXC3], 0) AS Lote_ValorGlosas, T9.[Lote_Codigo] FROM (([Lote] T9 WITH (NOLOCK) LEFT JOIN (SELECT SUM(T12.[ContagemResultadoIndicadores_Valor]) AS GXC3, T13.[Lote_Codigo] FROM [ContagemResultadoIndicadores] T12 WITH (NOLOCK),  [Lote] T13 WITH (NOLOCK) WHERE T12.[ContagemResultadoIndicadores_LoteCod] = T13.[Lote_Codigo] GROUP BY T13.[Lote_Codigo] ) T10 ON T10.[Lote_Codigo] = T9.[Lote_Codigo]) LEFT JOIN (SELECT SUM([ContagemResultado_GlsValor]) AS GXC2, [ContagemResultado_LoteAceiteCod] FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T11 ON T11.[ContagemResultado_LoteAceiteCod] = T9.[Lote_Codigo]) ) T7 ON T7.[Lote_Codigo] = T1.[Lote_Codigo]) LEFT JOIN (SELECT MIN(T11.[Pessoa_Nome]) AS Lote_ContratadaNom, T9.[ContagemResultado_LoteAceiteCod] FROM (([ContagemResultado] T9 WITH (NOLOCK) LEFT JOIN [Contratada] T10 WITH (NOLOCK) ON T10.[Contratada_Codigo] = T9.[ContagemResultado_ContratadaCod]) LEFT JOIN [Pessoa] T11 WITH (NOLOCK) ON T11.[Pessoa_Codigo]"
          + " = T10.[Contratada_PessoaCod]) GROUP BY T9.[ContagemResultado_LoteAceiteCod] ) T8 ON T8.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo]) WHERE T1.[Lote_Codigo] = @AV16Lote_Codigo ORDER BY T1.[Lote_Codigo]" ;
          Object[] prmP00A120 ;
          prmP00A120 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00A122 ;
          prmP00A122 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00A123 ;
          prmP00A123 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00A19", cmdBufferP00A19,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00A19,1,0,true,true )
             ,new CursorDef("P00A117", cmdBufferP00A117,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00A117,1,0,true,true )
             ,new CursorDef("P00A120", "SELECT COALESCE( T1.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) AS Lote_DataIni, COALESCE( T1.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) AS Lote_DataFim FROM (SELECT MIN(COALESCE( T3.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataIni, T2.[ContagemResultado_LoteAceiteCod], MAX(COALESCE( T3.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataFim FROM ([ContagemResultado] T2 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T3 ON T3.[ContagemResultado_Codigo] = T2.[ContagemResultado_Codigo]) GROUP BY T2.[ContagemResultado_LoteAceiteCod] ) T1 WHERE T1.[ContagemResultado_LoteAceiteCod] = @Lote_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00A120,1,0,true,true )
             ,new CursorDef("P00A122", "SELECT COALESCE( T1.[Lote_QtdeDmn], 0) AS Lote_QtdeDmn, COALESCE( T1.[Lote_Status], '') AS Lote_Status, COALESCE( T1.[Lote_ContratadaCod], 0) AS Lote_ContratadaCod FROM (SELECT COUNT(*) AS Lote_QtdeDmn, [ContagemResultado_LoteAceiteCod], MIN([ContagemResultado_StatusDmn]) AS Lote_Status, MIN([ContagemResultado_ContratadaCod]) AS Lote_ContratadaCod FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T1 WHERE T1.[ContagemResultado_LoteAceiteCod] = @Lote_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00A122,1,0,true,true )
             ,new CursorDef("P00A123", "SELECT [ContagemResultado_LoteAceiteCod], [ContagemResultado_ValorPF], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_LoteAceiteCod] = @Lote_Codigo ORDER BY [ContagemResultado_LoteAceiteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00A123,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 10) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 50) ;
                ((DateTime[]) buf[9])[0] = rslt.getGXDateTime(7) ;
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                ((int[]) buf[15])[0] = rslt.getInt(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getString(12, 100) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((decimal[]) buf[19])[0] = rslt.getDecimal(13) ;
                ((int[]) buf[20])[0] = rslt.getInt(14) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(14);
                ((DateTime[]) buf[22])[0] = rslt.getGXDate(15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[24])[0] = rslt.getGXDate(16) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(16);
                ((String[]) buf[26])[0] = rslt.getString(17, 50) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(17);
                ((DateTime[]) buf[28])[0] = rslt.getGXDate(18) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(18);
                ((DateTime[]) buf[30])[0] = rslt.getGXDate(19) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(19);
                ((String[]) buf[32])[0] = rslt.getLongVarchar(20) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(20);
                ((decimal[]) buf[34])[0] = rslt.getDecimal(21) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(21);
                ((int[]) buf[36])[0] = rslt.getInt(22) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(22);
                ((bool[]) buf[38])[0] = rslt.getBool(23) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(23);
                ((bool[]) buf[40])[0] = rslt.getBool(24) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(24);
                ((bool[]) buf[42])[0] = rslt.getBool(25) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(25);
                ((decimal[]) buf[44])[0] = rslt.getDecimal(26) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(26);
                ((decimal[]) buf[46])[0] = rslt.getDecimal(27) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(27);
                ((String[]) buf[48])[0] = rslt.getLongVarchar(28) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(28);
                ((String[]) buf[50])[0] = rslt.getLongVarchar(29) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(29);
                ((int[]) buf[52])[0] = rslt.getInt(30) ;
                ((DateTime[]) buf[53])[0] = rslt.getGXDate(31) ;
                ((DateTime[]) buf[54])[0] = rslt.getGXDate(32) ;
                ((short[]) buf[55])[0] = rslt.getShort(33) ;
                ((String[]) buf[56])[0] = rslt.getString(34, 1) ;
                ((int[]) buf[57])[0] = rslt.getInt(35) ;
                ((String[]) buf[58])[0] = rslt.getString(36, 50) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(36);
                ((decimal[]) buf[60])[0] = rslt.getDecimal(37) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(37);
                ((decimal[]) buf[62])[0] = rslt.getDecimal(38) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(38);
                ((String[]) buf[64])[0] = rslt.getBLOBFile(39, rslt.getString(2, 10), rslt.getString(1, 50)) ;
                ((bool[]) buf[65])[0] = rslt.wasNull(39);
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 10) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 50) ;
                ((DateTime[]) buf[9])[0] = rslt.getGXDateTime(7) ;
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                ((int[]) buf[15])[0] = rslt.getInt(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getString(12, 100) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((decimal[]) buf[19])[0] = rslt.getDecimal(13) ;
                ((int[]) buf[20])[0] = rslt.getInt(14) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(14);
                ((DateTime[]) buf[22])[0] = rslt.getGXDate(15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[24])[0] = rslt.getGXDate(16) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(16);
                ((String[]) buf[26])[0] = rslt.getString(17, 50) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(17);
                ((DateTime[]) buf[28])[0] = rslt.getGXDate(18) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(18);
                ((DateTime[]) buf[30])[0] = rslt.getGXDate(19) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(19);
                ((String[]) buf[32])[0] = rslt.getLongVarchar(20) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(20);
                ((decimal[]) buf[34])[0] = rslt.getDecimal(21) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(21);
                ((int[]) buf[36])[0] = rslt.getInt(22) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(22);
                ((bool[]) buf[38])[0] = rslt.getBool(23) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(23);
                ((bool[]) buf[40])[0] = rslt.getBool(24) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(24);
                ((bool[]) buf[42])[0] = rslt.getBool(25) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(25);
                ((decimal[]) buf[44])[0] = rslt.getDecimal(26) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(26);
                ((decimal[]) buf[46])[0] = rslt.getDecimal(27) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(27);
                ((String[]) buf[48])[0] = rslt.getLongVarchar(28) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(28);
                ((String[]) buf[50])[0] = rslt.getLongVarchar(29) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(29);
                ((int[]) buf[52])[0] = rslt.getInt(30) ;
                ((DateTime[]) buf[53])[0] = rslt.getGXDate(31) ;
                ((DateTime[]) buf[54])[0] = rslt.getGXDate(32) ;
                ((short[]) buf[55])[0] = rslt.getShort(33) ;
                ((String[]) buf[56])[0] = rslt.getString(34, 1) ;
                ((int[]) buf[57])[0] = rslt.getInt(35) ;
                ((String[]) buf[58])[0] = rslt.getString(36, 50) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(36);
                ((decimal[]) buf[60])[0] = rslt.getDecimal(37) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(37);
                ((decimal[]) buf[62])[0] = rslt.getDecimal(38) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(38);
                ((String[]) buf[64])[0] = rslt.getBLOBFile(39, rslt.getString(2, 10), rslt.getString(1, 50)) ;
                ((bool[]) buf[65])[0] = rslt.wasNull(39);
                return;
             case 2 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                return;
             case 3 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
