/*
               File: type_SdtGAMConnectionInfoProperties
        Description: GAMConnectionInfoProperties
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:43.79
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMConnectionInfoProperties : GxUserType, IGxExternalObject
   {
      public SdtGAMConnectionInfoProperties( )
      {
         initialize();
      }

      public SdtGAMConnectionInfoProperties( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMConnectionInfoProperties_externalReference == null )
         {
            GAMConnectionInfoProperties_externalReference = new Artech.Security.GAMConnectionInfoProperties(context);
         }
         returntostring = "";
         returntostring = (String)(GAMConnectionInfoProperties_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Id
      {
         get {
            if ( GAMConnectionInfoProperties_externalReference == null )
            {
               GAMConnectionInfoProperties_externalReference = new Artech.Security.GAMConnectionInfoProperties(context);
            }
            return GAMConnectionInfoProperties_externalReference.Id ;
         }

         set {
            if ( GAMConnectionInfoProperties_externalReference == null )
            {
               GAMConnectionInfoProperties_externalReference = new Artech.Security.GAMConnectionInfoProperties(context);
            }
            GAMConnectionInfoProperties_externalReference.Id = value;
         }

      }

      public String gxTpr_Value
      {
         get {
            if ( GAMConnectionInfoProperties_externalReference == null )
            {
               GAMConnectionInfoProperties_externalReference = new Artech.Security.GAMConnectionInfoProperties(context);
            }
            return GAMConnectionInfoProperties_externalReference.Value ;
         }

         set {
            if ( GAMConnectionInfoProperties_externalReference == null )
            {
               GAMConnectionInfoProperties_externalReference = new Artech.Security.GAMConnectionInfoProperties(context);
            }
            GAMConnectionInfoProperties_externalReference.Value = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMConnectionInfoProperties_externalReference == null )
            {
               GAMConnectionInfoProperties_externalReference = new Artech.Security.GAMConnectionInfoProperties(context);
            }
            return GAMConnectionInfoProperties_externalReference ;
         }

         set {
            GAMConnectionInfoProperties_externalReference = (Artech.Security.GAMConnectionInfoProperties)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMConnectionInfoProperties GAMConnectionInfoProperties_externalReference=null ;
   }

}
