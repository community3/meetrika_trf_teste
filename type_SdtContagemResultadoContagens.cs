/*
               File: type_SdtContagemResultadoContagens
        Description: Contagem Resultado Contagens
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:16:42.23
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ContagemResultadoContagens" )]
   [XmlType(TypeName =  "ContagemResultadoContagens" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtContagemResultadoContagens : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContagemResultadoContagens( )
      {
         /* Constructor for serialization */
         gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt = DateTime.MinValue;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_horacnt = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_osfsosfm = "";
         gxTv_SdtContagemResultadoContagens_Sistema_coordenacao = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoContagens_Contagemresultado_parecertcn = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_planilha = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla = "";
         gxTv_SdtContagemResultadoContagens_Mode = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt_Z = DateTime.MinValue;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_horacnt_Z = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_osfsosfm_Z = "";
         gxTv_SdtContagemResultadoContagens_Sistema_coordenacao_Z = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda_Z = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn_Z = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom_Z = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom_Z = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla_Z = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla_Z = "";
      }

      public SdtContagemResultadoContagens( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV456ContagemResultado_Codigo ,
                        DateTime AV473ContagemResultado_DataCnt ,
                        String AV511ContagemResultado_HoraCnt )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV456ContagemResultado_Codigo,(DateTime)AV473ContagemResultado_DataCnt,(String)AV511ContagemResultado_HoraCnt});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"ContagemResultado_Codigo", typeof(int)}, new Object[]{"ContagemResultado_DataCnt", typeof(DateTime)}, new Object[]{"ContagemResultado_HoraCnt", typeof(String)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "ContagemResultadoContagens");
         metadata.Set("BT", "ContagemResultadoContagens");
         metadata.Set("PK", "[ \"ContagemResultado_Codigo\",\"ContagemResultado_DataCnt\",\"ContagemResultado_HoraCnt\" ]");
         metadata.Set("PKAssigned", "[ \"ContagemResultado_HoraCnt\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"ContagemResultado_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"NaoConformidade_Codigo\" ],\"FKMap\":[ \"ContagemResultado_NaoCnfCntCod-NaoConformidade_Codigo\" ] },{ \"FK\":[ \"Usuario_Codigo\" ],\"FKMap\":[ \"ContagemResultado_ContadorFMCod-Usuario_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_datacnt_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_horacnt_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_osfsosfm_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_ehvalidacao_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_coordenacao_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_demanda_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_cntsrvcod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_servico_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_contratadacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_contratadaorigemcod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_areatrabalhocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_responsavel_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_osvinculada_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_naocnfdmncod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_statusdmn_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_timecnt_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadocontagens_prazo_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_pfbfs_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_pflfs_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_pfbfm_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_pflfm_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_divergencia_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_contadorfmcod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_crfmpessoacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_contadorfmnom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_crfmehcontratada_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_crfmehcontratante_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_naocnfcntcod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_naocnfcntnom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_statuscnt_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadocontagens_esforco_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_ultima_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_deflator_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_cstuntprd_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_nomepla_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_tipopla_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_rdmnissueid_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_nvlcnt_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_ehvalidacao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_coordenacao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_demanda_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_cntsrvcod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_servico_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_contratadacod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_contratadaorigemcod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratada_areatrabalhocod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_responsavel_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_osvinculada_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_naocnfdmncod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_statusdmn_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_timecnt_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadocontagens_prazo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_pfbfs_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_pflfs_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_pfbfm_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_pflfm_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_parecertcn_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_crfmpessoacod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_contadorfmnom_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_crfmehcontratada_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_crfmehcontratante_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_naocnfcntcod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_naocnfcntnom_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_deflator_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_cstuntprd_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_planilha_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_nomepla_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_tipopla_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_rdmnissueid_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_nvlcnt_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtContagemResultadoContagens deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtContagemResultadoContagens)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtContagemResultadoContagens obj ;
         obj = this;
         obj.gxTpr_Contagemresultado_codigo = deserialized.gxTpr_Contagemresultado_codigo;
         obj.gxTpr_Contagemresultado_datacnt = deserialized.gxTpr_Contagemresultado_datacnt;
         obj.gxTpr_Contagemresultado_horacnt = deserialized.gxTpr_Contagemresultado_horacnt;
         obj.gxTpr_Contagemresultado_osfsosfm = deserialized.gxTpr_Contagemresultado_osfsosfm;
         obj.gxTpr_Contagemresultado_ehvalidacao = deserialized.gxTpr_Contagemresultado_ehvalidacao;
         obj.gxTpr_Sistema_coordenacao = deserialized.gxTpr_Sistema_coordenacao;
         obj.gxTpr_Contagemresultado_demanda = deserialized.gxTpr_Contagemresultado_demanda;
         obj.gxTpr_Contagemresultado_cntsrvcod = deserialized.gxTpr_Contagemresultado_cntsrvcod;
         obj.gxTpr_Contagemresultado_servico = deserialized.gxTpr_Contagemresultado_servico;
         obj.gxTpr_Contagemresultado_contratadacod = deserialized.gxTpr_Contagemresultado_contratadacod;
         obj.gxTpr_Contagemresultado_contratadaorigemcod = deserialized.gxTpr_Contagemresultado_contratadaorigemcod;
         obj.gxTpr_Contratada_areatrabalhocod = deserialized.gxTpr_Contratada_areatrabalhocod;
         obj.gxTpr_Contagemresultado_responsavel = deserialized.gxTpr_Contagemresultado_responsavel;
         obj.gxTpr_Contagemresultado_osvinculada = deserialized.gxTpr_Contagemresultado_osvinculada;
         obj.gxTpr_Contagemresultado_naocnfdmncod = deserialized.gxTpr_Contagemresultado_naocnfdmncod;
         obj.gxTpr_Contagemresultado_statusdmn = deserialized.gxTpr_Contagemresultado_statusdmn;
         obj.gxTpr_Contagemresultado_timecnt = deserialized.gxTpr_Contagemresultado_timecnt;
         obj.gxTpr_Contagemresultadocontagens_prazo = deserialized.gxTpr_Contagemresultadocontagens_prazo;
         obj.gxTpr_Contagemresultado_pfbfs = deserialized.gxTpr_Contagemresultado_pfbfs;
         obj.gxTpr_Contagemresultado_pflfs = deserialized.gxTpr_Contagemresultado_pflfs;
         obj.gxTpr_Contagemresultado_pfbfm = deserialized.gxTpr_Contagemresultado_pfbfm;
         obj.gxTpr_Contagemresultado_pflfm = deserialized.gxTpr_Contagemresultado_pflfm;
         obj.gxTpr_Contagemresultado_divergencia = deserialized.gxTpr_Contagemresultado_divergencia;
         obj.gxTpr_Contagemresultado_parecertcn = deserialized.gxTpr_Contagemresultado_parecertcn;
         obj.gxTpr_Contagemresultado_contadorfmcod = deserialized.gxTpr_Contagemresultado_contadorfmcod;
         obj.gxTpr_Contagemresultado_crfmpessoacod = deserialized.gxTpr_Contagemresultado_crfmpessoacod;
         obj.gxTpr_Contagemresultado_contadorfmnom = deserialized.gxTpr_Contagemresultado_contadorfmnom;
         obj.gxTpr_Contagemresultado_crfmehcontratada = deserialized.gxTpr_Contagemresultado_crfmehcontratada;
         obj.gxTpr_Contagemresultado_crfmehcontratante = deserialized.gxTpr_Contagemresultado_crfmehcontratante;
         obj.gxTpr_Contagemresultado_naocnfcntcod = deserialized.gxTpr_Contagemresultado_naocnfcntcod;
         obj.gxTpr_Contagemresultado_naocnfcntnom = deserialized.gxTpr_Contagemresultado_naocnfcntnom;
         obj.gxTpr_Contagemresultado_statuscnt = deserialized.gxTpr_Contagemresultado_statuscnt;
         obj.gxTpr_Contagemresultadocontagens_esforco = deserialized.gxTpr_Contagemresultadocontagens_esforco;
         obj.gxTpr_Contagemresultado_ultima = deserialized.gxTpr_Contagemresultado_ultima;
         obj.gxTpr_Contagemresultado_deflator = deserialized.gxTpr_Contagemresultado_deflator;
         obj.gxTpr_Contagemresultado_cstuntprd = deserialized.gxTpr_Contagemresultado_cstuntprd;
         obj.gxTpr_Contagemresultado_planilha = deserialized.gxTpr_Contagemresultado_planilha;
         obj.gxTpr_Contagemresultado_nomepla = deserialized.gxTpr_Contagemresultado_nomepla;
         obj.gxTpr_Contagemresultado_tipopla = deserialized.gxTpr_Contagemresultado_tipopla;
         obj.gxTpr_Contagemresultado_rdmnissueid = deserialized.gxTpr_Contagemresultado_rdmnissueid;
         obj.gxTpr_Contagemresultado_nvlcnt = deserialized.gxTpr_Contagemresultado_nvlcnt;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Contagemresultado_codigo_Z = deserialized.gxTpr_Contagemresultado_codigo_Z;
         obj.gxTpr_Contagemresultado_datacnt_Z = deserialized.gxTpr_Contagemresultado_datacnt_Z;
         obj.gxTpr_Contagemresultado_horacnt_Z = deserialized.gxTpr_Contagemresultado_horacnt_Z;
         obj.gxTpr_Contagemresultado_osfsosfm_Z = deserialized.gxTpr_Contagemresultado_osfsosfm_Z;
         obj.gxTpr_Contagemresultado_ehvalidacao_Z = deserialized.gxTpr_Contagemresultado_ehvalidacao_Z;
         obj.gxTpr_Sistema_coordenacao_Z = deserialized.gxTpr_Sistema_coordenacao_Z;
         obj.gxTpr_Contagemresultado_demanda_Z = deserialized.gxTpr_Contagemresultado_demanda_Z;
         obj.gxTpr_Contagemresultado_cntsrvcod_Z = deserialized.gxTpr_Contagemresultado_cntsrvcod_Z;
         obj.gxTpr_Contagemresultado_servico_Z = deserialized.gxTpr_Contagemresultado_servico_Z;
         obj.gxTpr_Contagemresultado_contratadacod_Z = deserialized.gxTpr_Contagemresultado_contratadacod_Z;
         obj.gxTpr_Contagemresultado_contratadaorigemcod_Z = deserialized.gxTpr_Contagemresultado_contratadaorigemcod_Z;
         obj.gxTpr_Contratada_areatrabalhocod_Z = deserialized.gxTpr_Contratada_areatrabalhocod_Z;
         obj.gxTpr_Contagemresultado_responsavel_Z = deserialized.gxTpr_Contagemresultado_responsavel_Z;
         obj.gxTpr_Contagemresultado_osvinculada_Z = deserialized.gxTpr_Contagemresultado_osvinculada_Z;
         obj.gxTpr_Contagemresultado_naocnfdmncod_Z = deserialized.gxTpr_Contagemresultado_naocnfdmncod_Z;
         obj.gxTpr_Contagemresultado_statusdmn_Z = deserialized.gxTpr_Contagemresultado_statusdmn_Z;
         obj.gxTpr_Contagemresultado_timecnt_Z = deserialized.gxTpr_Contagemresultado_timecnt_Z;
         obj.gxTpr_Contagemresultadocontagens_prazo_Z = deserialized.gxTpr_Contagemresultadocontagens_prazo_Z;
         obj.gxTpr_Contagemresultado_pfbfs_Z = deserialized.gxTpr_Contagemresultado_pfbfs_Z;
         obj.gxTpr_Contagemresultado_pflfs_Z = deserialized.gxTpr_Contagemresultado_pflfs_Z;
         obj.gxTpr_Contagemresultado_pfbfm_Z = deserialized.gxTpr_Contagemresultado_pfbfm_Z;
         obj.gxTpr_Contagemresultado_pflfm_Z = deserialized.gxTpr_Contagemresultado_pflfm_Z;
         obj.gxTpr_Contagemresultado_divergencia_Z = deserialized.gxTpr_Contagemresultado_divergencia_Z;
         obj.gxTpr_Contagemresultado_contadorfmcod_Z = deserialized.gxTpr_Contagemresultado_contadorfmcod_Z;
         obj.gxTpr_Contagemresultado_crfmpessoacod_Z = deserialized.gxTpr_Contagemresultado_crfmpessoacod_Z;
         obj.gxTpr_Contagemresultado_contadorfmnom_Z = deserialized.gxTpr_Contagemresultado_contadorfmnom_Z;
         obj.gxTpr_Contagemresultado_crfmehcontratada_Z = deserialized.gxTpr_Contagemresultado_crfmehcontratada_Z;
         obj.gxTpr_Contagemresultado_crfmehcontratante_Z = deserialized.gxTpr_Contagemresultado_crfmehcontratante_Z;
         obj.gxTpr_Contagemresultado_naocnfcntcod_Z = deserialized.gxTpr_Contagemresultado_naocnfcntcod_Z;
         obj.gxTpr_Contagemresultado_naocnfcntnom_Z = deserialized.gxTpr_Contagemresultado_naocnfcntnom_Z;
         obj.gxTpr_Contagemresultado_statuscnt_Z = deserialized.gxTpr_Contagemresultado_statuscnt_Z;
         obj.gxTpr_Contagemresultadocontagens_esforco_Z = deserialized.gxTpr_Contagemresultadocontagens_esforco_Z;
         obj.gxTpr_Contagemresultado_ultima_Z = deserialized.gxTpr_Contagemresultado_ultima_Z;
         obj.gxTpr_Contagemresultado_deflator_Z = deserialized.gxTpr_Contagemresultado_deflator_Z;
         obj.gxTpr_Contagemresultado_cstuntprd_Z = deserialized.gxTpr_Contagemresultado_cstuntprd_Z;
         obj.gxTpr_Contagemresultado_nomepla_Z = deserialized.gxTpr_Contagemresultado_nomepla_Z;
         obj.gxTpr_Contagemresultado_tipopla_Z = deserialized.gxTpr_Contagemresultado_tipopla_Z;
         obj.gxTpr_Contagemresultado_rdmnissueid_Z = deserialized.gxTpr_Contagemresultado_rdmnissueid_Z;
         obj.gxTpr_Contagemresultado_nvlcnt_Z = deserialized.gxTpr_Contagemresultado_nvlcnt_Z;
         obj.gxTpr_Contagemresultado_ehvalidacao_N = deserialized.gxTpr_Contagemresultado_ehvalidacao_N;
         obj.gxTpr_Sistema_coordenacao_N = deserialized.gxTpr_Sistema_coordenacao_N;
         obj.gxTpr_Contagemresultado_demanda_N = deserialized.gxTpr_Contagemresultado_demanda_N;
         obj.gxTpr_Contagemresultado_cntsrvcod_N = deserialized.gxTpr_Contagemresultado_cntsrvcod_N;
         obj.gxTpr_Contagemresultado_servico_N = deserialized.gxTpr_Contagemresultado_servico_N;
         obj.gxTpr_Contagemresultado_contratadacod_N = deserialized.gxTpr_Contagemresultado_contratadacod_N;
         obj.gxTpr_Contagemresultado_contratadaorigemcod_N = deserialized.gxTpr_Contagemresultado_contratadaorigemcod_N;
         obj.gxTpr_Contratada_areatrabalhocod_N = deserialized.gxTpr_Contratada_areatrabalhocod_N;
         obj.gxTpr_Contagemresultado_responsavel_N = deserialized.gxTpr_Contagemresultado_responsavel_N;
         obj.gxTpr_Contagemresultado_osvinculada_N = deserialized.gxTpr_Contagemresultado_osvinculada_N;
         obj.gxTpr_Contagemresultado_naocnfdmncod_N = deserialized.gxTpr_Contagemresultado_naocnfdmncod_N;
         obj.gxTpr_Contagemresultado_statusdmn_N = deserialized.gxTpr_Contagemresultado_statusdmn_N;
         obj.gxTpr_Contagemresultado_timecnt_N = deserialized.gxTpr_Contagemresultado_timecnt_N;
         obj.gxTpr_Contagemresultadocontagens_prazo_N = deserialized.gxTpr_Contagemresultadocontagens_prazo_N;
         obj.gxTpr_Contagemresultado_pfbfs_N = deserialized.gxTpr_Contagemresultado_pfbfs_N;
         obj.gxTpr_Contagemresultado_pflfs_N = deserialized.gxTpr_Contagemresultado_pflfs_N;
         obj.gxTpr_Contagemresultado_pfbfm_N = deserialized.gxTpr_Contagemresultado_pfbfm_N;
         obj.gxTpr_Contagemresultado_pflfm_N = deserialized.gxTpr_Contagemresultado_pflfm_N;
         obj.gxTpr_Contagemresultado_parecertcn_N = deserialized.gxTpr_Contagemresultado_parecertcn_N;
         obj.gxTpr_Contagemresultado_crfmpessoacod_N = deserialized.gxTpr_Contagemresultado_crfmpessoacod_N;
         obj.gxTpr_Contagemresultado_contadorfmnom_N = deserialized.gxTpr_Contagemresultado_contadorfmnom_N;
         obj.gxTpr_Contagemresultado_crfmehcontratada_N = deserialized.gxTpr_Contagemresultado_crfmehcontratada_N;
         obj.gxTpr_Contagemresultado_crfmehcontratante_N = deserialized.gxTpr_Contagemresultado_crfmehcontratante_N;
         obj.gxTpr_Contagemresultado_naocnfcntcod_N = deserialized.gxTpr_Contagemresultado_naocnfcntcod_N;
         obj.gxTpr_Contagemresultado_naocnfcntnom_N = deserialized.gxTpr_Contagemresultado_naocnfcntnom_N;
         obj.gxTpr_Contagemresultado_deflator_N = deserialized.gxTpr_Contagemresultado_deflator_N;
         obj.gxTpr_Contagemresultado_cstuntprd_N = deserialized.gxTpr_Contagemresultado_cstuntprd_N;
         obj.gxTpr_Contagemresultado_planilha_N = deserialized.gxTpr_Contagemresultado_planilha_N;
         obj.gxTpr_Contagemresultado_nomepla_N = deserialized.gxTpr_Contagemresultado_nomepla_N;
         obj.gxTpr_Contagemresultado_tipopla_N = deserialized.gxTpr_Contagemresultado_tipopla_N;
         obj.gxTpr_Contagemresultado_rdmnissueid_N = deserialized.gxTpr_Contagemresultado_rdmnissueid_N;
         obj.gxTpr_Contagemresultado_nvlcnt_N = deserialized.gxTpr_Contagemresultado_nvlcnt_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Codigo") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DataCnt") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_HoraCnt") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_horacnt = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_OsFsOsFm") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_osfsosfm = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_EhValidacao") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Coordenacao") )
               {
                  gxTv_SdtContagemResultadoContagens_Sistema_coordenacao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Demanda") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntSrvCod") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Servico") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_servico = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaCod") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaOrigemCod") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_AreaTrabalhoCod") )
               {
                  gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Responsavel") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_OSVinculada") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_NaoCnfDmnCod") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_StatusDmn") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_TimeCnt") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt = DateTimeUtil.ResetDate(context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), "."))));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoContagens_Prazo") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PFBFS") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PFLFS") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PFBFM") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PFLFM") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Divergencia") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_divergencia = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ParecerTcn") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_parecertcn = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContadorFMCod") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CrFMPessoaCod") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContadorFMNom") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CrFMEhContratada") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CrFMEhContratante") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_NaoCnfCntCod") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_NaoCnfCntNom") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_StatusCnt") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_statuscnt = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoContagens_Esforco") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_esforco = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Ultima") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_ultima = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Deflator") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CstUntPrd") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Planilha") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_planilha=context.FileFromBase64( oReader.Value) ;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_NomePla") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_TipoPla") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_RdmnIssueId") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_NvlCnt") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtContagemResultadoContagens_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtContagemResultadoContagens_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Codigo_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DataCnt_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt_Z = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt_Z = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_HoraCnt_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_horacnt_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_OsFsOsFm_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_osfsosfm_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_EhValidacao_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Coordenacao_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Sistema_coordenacao_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Demanda_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntSrvCod_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Servico_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_servico_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaCod_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaOrigemCod_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_AreaTrabalhoCod_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Responsavel_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_OSVinculada_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_NaoCnfDmnCod_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_StatusDmn_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_TimeCnt_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_Z = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_Z = DateTimeUtil.ResetDate(context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), "."))));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoContagens_Prazo_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_Z = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_Z = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PFBFS_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PFLFS_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PFBFM_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PFLFM_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Divergencia_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_divergencia_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContadorFMCod_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmcod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CrFMPessoaCod_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContadorFMNom_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CrFMEhContratada_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CrFMEhContratante_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_NaoCnfCntCod_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_NaoCnfCntNom_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_StatusCnt_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_statuscnt_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoContagens_Esforco_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_esforco_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Ultima_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_ultima_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Deflator_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CstUntPrd_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_NomePla_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_TipoPla_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_RdmnIssueId_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_NvlCnt_Z") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_EhValidacao_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Coordenacao_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Sistema_coordenacao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Demanda_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntSrvCod_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Servico_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_servico_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaCod_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaOrigemCod_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_AreaTrabalhoCod_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Responsavel_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_OSVinculada_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_NaoCnfDmnCod_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_StatusDmn_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_TimeCnt_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoContagens_Prazo_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PFBFS_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PFLFS_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PFBFM_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PFLFM_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ParecerTcn_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_parecertcn_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CrFMPessoaCod_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContadorFMNom_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CrFMEhContratada_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CrFMEhContratante_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_NaoCnfCntCod_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_NaoCnfCntNom_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Deflator_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CstUntPrd_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Planilha_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_planilha_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_NomePla_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_TipoPla_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_RdmnIssueId_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_NvlCnt_N") )
               {
                  gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ContagemResultadoContagens";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContagemResultado_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( (DateTime.MinValue==gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt) )
         {
            oWriter.WriteStartElement("ContagemResultado_DataCnt");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_DataCnt", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteElement("ContagemResultado_HoraCnt", StringUtil.RTrim( gxTv_SdtContagemResultadoContagens_Contagemresultado_horacnt));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_OsFsOsFm", StringUtil.RTrim( gxTv_SdtContagemResultadoContagens_Contagemresultado_osfsosfm));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_EhValidacao", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Sistema_Coordenacao", StringUtil.RTrim( gxTv_SdtContagemResultadoContagens_Sistema_coordenacao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_Demanda", StringUtil.RTrim( gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_CntSrvCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_Servico", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_servico), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_ContratadaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_ContratadaOrigemCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contratada_AreaTrabalhoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_Responsavel", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_OSVinculada", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_NaoCnfDmnCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_StatusDmn", StringUtil.RTrim( gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( (DateTime.MinValue==gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt) )
         {
            oWriter.WriteStartElement("ContagemResultado_TimeCnt");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_TimeCnt", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo) )
         {
            oWriter.WriteStartElement("ContagemResultadoContagens_Prazo");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultadoContagens_Prazo", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteElement("ContagemResultado_PFBFS", StringUtil.Trim( StringUtil.Str( gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_PFLFS", StringUtil.Trim( StringUtil.Str( gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_PFBFM", StringUtil.Trim( StringUtil.Str( gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_PFLFM", StringUtil.Trim( StringUtil.Str( gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_Divergencia", StringUtil.Trim( StringUtil.Str( gxTv_SdtContagemResultadoContagens_Contagemresultado_divergencia, 6, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_ParecerTcn", StringUtil.RTrim( gxTv_SdtContagemResultadoContagens_Contagemresultado_parecertcn));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_ContadorFMCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_CrFMPessoaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_ContadorFMNom", StringUtil.RTrim( gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_CrFMEhContratada", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_CrFMEhContratante", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_NaoCnfCntCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_NaoCnfCntNom", StringUtil.RTrim( gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_StatusCnt", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_statuscnt), 2, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultadoContagens_Esforco", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_esforco), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_Ultima", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContagemResultadoContagens_Contagemresultado_ultima)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_Deflator", StringUtil.Trim( StringUtil.Str( gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator, 6, 3)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_CstUntPrd", StringUtil.Trim( StringUtil.Str( gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd, 18, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_Planilha", context.FileToBase64( gxTv_SdtContagemResultadoContagens_Contagemresultado_planilha));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_NomePla", StringUtil.RTrim( gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_TipoPla", StringUtil.RTrim( gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_RdmnIssueId", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_NvlCnt", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtContagemResultadoContagens_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            if ( (DateTime.MinValue==gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt_Z) )
            {
               oWriter.WriteStartElement("ContagemResultado_DataCnt_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("ContagemResultado_DataCnt_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
               }
            }
            oWriter.WriteElement("ContagemResultado_HoraCnt_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoContagens_Contagemresultado_horacnt_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_OsFsOsFm_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoContagens_Contagemresultado_osfsosfm_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_EhValidacao_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_Coordenacao_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoContagens_Sistema_coordenacao_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Demanda_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_CntSrvCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Servico_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_servico_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_ContratadaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_ContratadaOrigemCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratada_AreaTrabalhoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Responsavel_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_OSVinculada_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_NaoCnfDmnCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_StatusDmn_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            if ( (DateTime.MinValue==gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_Z) )
            {
               oWriter.WriteStartElement("ContagemResultado_TimeCnt_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "T";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("ContagemResultado_TimeCnt_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
               }
            }
            if ( (DateTime.MinValue==gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_Z) )
            {
               oWriter.WriteStartElement("ContagemResultadoContagens_Prazo_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "T";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("ContagemResultadoContagens_Prazo_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
               }
            }
            oWriter.WriteElement("ContagemResultado_PFBFS_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs_Z, 14, 5)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_PFLFS_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs_Z, 14, 5)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_PFBFM_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm_Z, 14, 5)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_PFLFM_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm_Z, 14, 5)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Divergencia_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContagemResultadoContagens_Contagemresultado_divergencia_Z, 6, 2)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_ContadorFMCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmcod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_CrFMPessoaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_ContadorFMNom_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_CrFMEhContratada_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_CrFMEhContratante_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_NaoCnfCntCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_NaoCnfCntNom_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_StatusCnt_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_statuscnt_Z), 2, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoContagens_Esforco_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_esforco_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Ultima_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContagemResultadoContagens_Contagemresultado_ultima_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Deflator_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator_Z, 6, 3)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_CstUntPrd_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd_Z, 18, 5)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_NomePla_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_TipoPla_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_RdmnIssueId_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_NvlCnt_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_EhValidacao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_Coordenacao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Sistema_coordenacao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Demanda_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_CntSrvCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Servico_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_servico_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_ContratadaCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_ContratadaOrigemCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contratada_AreaTrabalhoCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Responsavel_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_OSVinculada_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_NaoCnfDmnCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_StatusDmn_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_TimeCnt_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoContagens_Prazo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_PFBFS_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_PFLFS_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_PFBFM_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_PFLFM_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_ParecerTcn_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_parecertcn_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_CrFMPessoaCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_ContadorFMNom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_CrFMEhContratada_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_CrFMEhContratante_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_NaoCnfCntCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_NaoCnfCntNom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Deflator_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_CstUntPrd_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Planilha_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_planilha_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_NomePla_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_TipoPla_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_RdmnIssueId_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_NvlCnt_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContagemResultado_Codigo", gxTv_SdtContagemResultadoContagens_Contagemresultado_codigo, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_DataCnt", sDateCnv, false);
         AddObjectProperty("ContagemResultado_HoraCnt", gxTv_SdtContagemResultadoContagens_Contagemresultado_horacnt, false);
         AddObjectProperty("ContagemResultado_OsFsOsFm", gxTv_SdtContagemResultadoContagens_Contagemresultado_osfsosfm, false);
         AddObjectProperty("ContagemResultado_EhValidacao", gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao, false);
         AddObjectProperty("Sistema_Coordenacao", gxTv_SdtContagemResultadoContagens_Sistema_coordenacao, false);
         AddObjectProperty("ContagemResultado_Demanda", gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda, false);
         AddObjectProperty("ContagemResultado_CntSrvCod", gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod, false);
         AddObjectProperty("ContagemResultado_Servico", gxTv_SdtContagemResultadoContagens_Contagemresultado_servico, false);
         AddObjectProperty("ContagemResultado_ContratadaCod", gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod, false);
         AddObjectProperty("ContagemResultado_ContratadaOrigemCod", gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod, false);
         AddObjectProperty("Contratada_AreaTrabalhoCod", gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod, false);
         AddObjectProperty("ContagemResultado_Responsavel", gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel, false);
         AddObjectProperty("ContagemResultado_OSVinculada", gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada, false);
         AddObjectProperty("ContagemResultado_NaoCnfDmnCod", gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod, false);
         AddObjectProperty("ContagemResultado_StatusDmn", gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn, false);
         datetime_STZ = gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_TimeCnt", sDateCnv, false);
         datetime_STZ = DateTimeUtil.ResetDate(gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultadoContagens_Prazo", sDateCnv, false);
         AddObjectProperty("ContagemResultado_PFBFS", gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs, false);
         AddObjectProperty("ContagemResultado_PFLFS", gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs, false);
         AddObjectProperty("ContagemResultado_PFBFM", gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm, false);
         AddObjectProperty("ContagemResultado_PFLFM", gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm, false);
         AddObjectProperty("ContagemResultado_Divergencia", gxTv_SdtContagemResultadoContagens_Contagemresultado_divergencia, false);
         AddObjectProperty("ContagemResultado_ParecerTcn", gxTv_SdtContagemResultadoContagens_Contagemresultado_parecertcn, false);
         AddObjectProperty("ContagemResultado_ContadorFMCod", gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmcod, false);
         AddObjectProperty("ContagemResultado_CrFMPessoaCod", gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod, false);
         AddObjectProperty("ContagemResultado_ContadorFMNom", gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom, false);
         AddObjectProperty("ContagemResultado_CrFMEhContratada", gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada, false);
         AddObjectProperty("ContagemResultado_CrFMEhContratante", gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante, false);
         AddObjectProperty("ContagemResultado_NaoCnfCntCod", gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod, false);
         AddObjectProperty("ContagemResultado_NaoCnfCntNom", gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom, false);
         AddObjectProperty("ContagemResultado_StatusCnt", gxTv_SdtContagemResultadoContagens_Contagemresultado_statuscnt, false);
         AddObjectProperty("ContagemResultadoContagens_Esforco", gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_esforco, false);
         AddObjectProperty("ContagemResultado_Ultima", gxTv_SdtContagemResultadoContagens_Contagemresultado_ultima, false);
         AddObjectProperty("ContagemResultado_Deflator", gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator, false);
         AddObjectProperty("ContagemResultado_CstUntPrd", StringUtil.LTrim( StringUtil.Str( gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd, 18, 5)), false);
         AddObjectProperty("ContagemResultado_Planilha", gxTv_SdtContagemResultadoContagens_Contagemresultado_planilha, false);
         AddObjectProperty("ContagemResultado_NomePla", gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla, false);
         AddObjectProperty("ContagemResultado_TipoPla", gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla, false);
         AddObjectProperty("ContagemResultado_RdmnIssueId", gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid, false);
         AddObjectProperty("ContagemResultado_NvlCnt", gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtContagemResultadoContagens_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtContagemResultadoContagens_Initialized, false);
            AddObjectProperty("ContagemResultado_Codigo_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_codigo_Z, false);
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("ContagemResultado_DataCnt_Z", sDateCnv, false);
            AddObjectProperty("ContagemResultado_HoraCnt_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_horacnt_Z, false);
            AddObjectProperty("ContagemResultado_OsFsOsFm_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_osfsosfm_Z, false);
            AddObjectProperty("ContagemResultado_EhValidacao_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao_Z, false);
            AddObjectProperty("Sistema_Coordenacao_Z", gxTv_SdtContagemResultadoContagens_Sistema_coordenacao_Z, false);
            AddObjectProperty("ContagemResultado_Demanda_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda_Z, false);
            AddObjectProperty("ContagemResultado_CntSrvCod_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod_Z, false);
            AddObjectProperty("ContagemResultado_Servico_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_servico_Z, false);
            AddObjectProperty("ContagemResultado_ContratadaCod_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod_Z, false);
            AddObjectProperty("ContagemResultado_ContratadaOrigemCod_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod_Z, false);
            AddObjectProperty("Contratada_AreaTrabalhoCod_Z", gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod_Z, false);
            AddObjectProperty("ContagemResultado_Responsavel_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel_Z, false);
            AddObjectProperty("ContagemResultado_OSVinculada_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada_Z, false);
            AddObjectProperty("ContagemResultado_NaoCnfDmnCod_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod_Z, false);
            AddObjectProperty("ContagemResultado_StatusDmn_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn_Z, false);
            datetime_STZ = gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_Z;
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("ContagemResultado_TimeCnt_Z", sDateCnv, false);
            datetime_STZ = DateTimeUtil.ResetDate(gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_Z);
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("ContagemResultadoContagens_Prazo_Z", sDateCnv, false);
            AddObjectProperty("ContagemResultado_PFBFS_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs_Z, false);
            AddObjectProperty("ContagemResultado_PFLFS_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs_Z, false);
            AddObjectProperty("ContagemResultado_PFBFM_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm_Z, false);
            AddObjectProperty("ContagemResultado_PFLFM_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm_Z, false);
            AddObjectProperty("ContagemResultado_Divergencia_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_divergencia_Z, false);
            AddObjectProperty("ContagemResultado_ContadorFMCod_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmcod_Z, false);
            AddObjectProperty("ContagemResultado_CrFMPessoaCod_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod_Z, false);
            AddObjectProperty("ContagemResultado_ContadorFMNom_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom_Z, false);
            AddObjectProperty("ContagemResultado_CrFMEhContratada_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada_Z, false);
            AddObjectProperty("ContagemResultado_CrFMEhContratante_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante_Z, false);
            AddObjectProperty("ContagemResultado_NaoCnfCntCod_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod_Z, false);
            AddObjectProperty("ContagemResultado_NaoCnfCntNom_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom_Z, false);
            AddObjectProperty("ContagemResultado_StatusCnt_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_statuscnt_Z, false);
            AddObjectProperty("ContagemResultadoContagens_Esforco_Z", gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_esforco_Z, false);
            AddObjectProperty("ContagemResultado_Ultima_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_ultima_Z, false);
            AddObjectProperty("ContagemResultado_Deflator_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator_Z, false);
            AddObjectProperty("ContagemResultado_CstUntPrd_Z", StringUtil.LTrim( StringUtil.Str( gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd_Z, 18, 5)), false);
            AddObjectProperty("ContagemResultado_NomePla_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla_Z, false);
            AddObjectProperty("ContagemResultado_TipoPla_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla_Z, false);
            AddObjectProperty("ContagemResultado_RdmnIssueId_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid_Z, false);
            AddObjectProperty("ContagemResultado_NvlCnt_Z", gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt_Z, false);
            AddObjectProperty("ContagemResultado_EhValidacao_N", gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao_N, false);
            AddObjectProperty("Sistema_Coordenacao_N", gxTv_SdtContagemResultadoContagens_Sistema_coordenacao_N, false);
            AddObjectProperty("ContagemResultado_Demanda_N", gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda_N, false);
            AddObjectProperty("ContagemResultado_CntSrvCod_N", gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod_N, false);
            AddObjectProperty("ContagemResultado_Servico_N", gxTv_SdtContagemResultadoContagens_Contagemresultado_servico_N, false);
            AddObjectProperty("ContagemResultado_ContratadaCod_N", gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod_N, false);
            AddObjectProperty("ContagemResultado_ContratadaOrigemCod_N", gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod_N, false);
            AddObjectProperty("Contratada_AreaTrabalhoCod_N", gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod_N, false);
            AddObjectProperty("ContagemResultado_Responsavel_N", gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel_N, false);
            AddObjectProperty("ContagemResultado_OSVinculada_N", gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada_N, false);
            AddObjectProperty("ContagemResultado_NaoCnfDmnCod_N", gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod_N, false);
            AddObjectProperty("ContagemResultado_StatusDmn_N", gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn_N, false);
            AddObjectProperty("ContagemResultado_TimeCnt_N", gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_N, false);
            AddObjectProperty("ContagemResultadoContagens_Prazo_N", gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_N, false);
            AddObjectProperty("ContagemResultado_PFBFS_N", gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs_N, false);
            AddObjectProperty("ContagemResultado_PFLFS_N", gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs_N, false);
            AddObjectProperty("ContagemResultado_PFBFM_N", gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm_N, false);
            AddObjectProperty("ContagemResultado_PFLFM_N", gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm_N, false);
            AddObjectProperty("ContagemResultado_ParecerTcn_N", gxTv_SdtContagemResultadoContagens_Contagemresultado_parecertcn_N, false);
            AddObjectProperty("ContagemResultado_CrFMPessoaCod_N", gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod_N, false);
            AddObjectProperty("ContagemResultado_ContadorFMNom_N", gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom_N, false);
            AddObjectProperty("ContagemResultado_CrFMEhContratada_N", gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada_N, false);
            AddObjectProperty("ContagemResultado_CrFMEhContratante_N", gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante_N, false);
            AddObjectProperty("ContagemResultado_NaoCnfCntCod_N", gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod_N, false);
            AddObjectProperty("ContagemResultado_NaoCnfCntNom_N", gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom_N, false);
            AddObjectProperty("ContagemResultado_Deflator_N", gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator_N, false);
            AddObjectProperty("ContagemResultado_CstUntPrd_N", gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd_N, false);
            AddObjectProperty("ContagemResultado_Planilha_N", gxTv_SdtContagemResultadoContagens_Contagemresultado_planilha_N, false);
            AddObjectProperty("ContagemResultado_NomePla_N", gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla_N, false);
            AddObjectProperty("ContagemResultado_TipoPla_N", gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla_N, false);
            AddObjectProperty("ContagemResultado_RdmnIssueId_N", gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid_N, false);
            AddObjectProperty("ContagemResultado_NvlCnt_N", gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Codigo" )]
      [  XmlElement( ElementName = "ContagemResultado_Codigo"   )]
      public int gxTpr_Contagemresultado_codigo
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_codigo ;
         }

         set {
            if ( gxTv_SdtContagemResultadoContagens_Contagemresultado_codigo != value )
            {
               gxTv_SdtContagemResultadoContagens_Mode = "INS";
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_codigo_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_horacnt_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_osfsosfm_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Sistema_coordenacao_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_servico_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_divergencia_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmcod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_statuscnt_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_esforco_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_ultima_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt_Z_SetNull( );
            }
            gxTv_SdtContagemResultadoContagens_Contagemresultado_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_DataCnt" )]
      [  XmlElement( ElementName = "ContagemResultado_DataCnt"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_datacnt_Nullable
      {
         get {
            if ( gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt = DateTime.MinValue;
            else
               gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_datacnt
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt ;
         }

         set {
            if ( gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt != value )
            {
               gxTv_SdtContagemResultadoContagens_Mode = "INS";
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_codigo_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_horacnt_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_osfsosfm_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Sistema_coordenacao_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_servico_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_divergencia_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmcod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_statuscnt_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_esforco_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_ultima_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt_Z_SetNull( );
            }
            gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_HoraCnt" )]
      [  XmlElement( ElementName = "ContagemResultado_HoraCnt"   )]
      public String gxTpr_Contagemresultado_horacnt
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_horacnt ;
         }

         set {
            if ( StringUtil.StrCmp(gxTv_SdtContagemResultadoContagens_Contagemresultado_horacnt, value) != 0 )
            {
               gxTv_SdtContagemResultadoContagens_Mode = "INS";
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_codigo_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_horacnt_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_osfsosfm_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Sistema_coordenacao_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_servico_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_divergencia_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmcod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_statuscnt_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_esforco_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_ultima_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid_Z_SetNull( );
               this.gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt_Z_SetNull( );
            }
            gxTv_SdtContagemResultadoContagens_Contagemresultado_horacnt = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_OsFsOsFm" )]
      [  XmlElement( ElementName = "ContagemResultado_OsFsOsFm"   )]
      public String gxTpr_Contagemresultado_osfsosfm
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_osfsosfm ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_osfsosfm = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_osfsosfm_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_osfsosfm = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_osfsosfm_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_EhValidacao" )]
      [  XmlElement( ElementName = "ContagemResultado_EhValidacao"   )]
      public bool gxTpr_Contagemresultado_ehvalidacao
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao = value;
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao = false;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Coordenacao" )]
      [  XmlElement( ElementName = "Sistema_Coordenacao"   )]
      public String gxTpr_Sistema_coordenacao
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Sistema_coordenacao ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Sistema_coordenacao_N = 0;
            gxTv_SdtContagemResultadoContagens_Sistema_coordenacao = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Sistema_coordenacao_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Sistema_coordenacao_N = 1;
         gxTv_SdtContagemResultadoContagens_Sistema_coordenacao = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Sistema_coordenacao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Demanda" )]
      [  XmlElement( ElementName = "ContagemResultado_Demanda"   )]
      public String gxTpr_Contagemresultado_demanda
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_CntSrvCod" )]
      [  XmlElement( ElementName = "ContagemResultado_CntSrvCod"   )]
      public int gxTpr_Contagemresultado_cntsrvcod
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Servico" )]
      [  XmlElement( ElementName = "ContagemResultado_Servico"   )]
      public int gxTpr_Contagemresultado_servico
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_servico ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_servico_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_servico = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_servico_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_servico_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_servico = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_servico_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaCod" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaCod"   )]
      public int gxTpr_Contagemresultado_contratadacod
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaOrigemCod" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaOrigemCod"   )]
      public int gxTpr_Contagemresultado_contratadaorigemcod
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_AreaTrabalhoCod" )]
      [  XmlElement( ElementName = "Contratada_AreaTrabalhoCod"   )]
      public int gxTpr_Contratada_areatrabalhocod
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod_N = 0;
            gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod_N = 1;
         gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Responsavel" )]
      [  XmlElement( ElementName = "ContagemResultado_Responsavel"   )]
      public int gxTpr_Contagemresultado_responsavel
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_OSVinculada" )]
      [  XmlElement( ElementName = "ContagemResultado_OSVinculada"   )]
      public int gxTpr_Contagemresultado_osvinculada
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_NaoCnfDmnCod" )]
      [  XmlElement( ElementName = "ContagemResultado_NaoCnfDmnCod"   )]
      public int gxTpr_Contagemresultado_naocnfdmncod
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_StatusDmn" )]
      [  XmlElement( ElementName = "ContagemResultado_StatusDmn"   )]
      public String gxTpr_Contagemresultado_statusdmn
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_TimeCnt" )]
      [  XmlElement( ElementName = "ContagemResultado_TimeCnt"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_timecnt_Nullable
      {
         get {
            if ( gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt).value ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_N = 0;
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt = DateTime.MinValue;
            else
               gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_timecnt
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt = (DateTime)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoContagens_Prazo" )]
      [  XmlElement( ElementName = "ContagemResultadoContagens_Prazo"  , IsNullable=true )]
      public string gxTpr_Contagemresultadocontagens_prazo_Nullable
      {
         get {
            if ( gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo).value ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_N = 0;
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo = DateTime.MinValue;
            else
               gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultadocontagens_prazo
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo = (DateTime)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_PFBFS" )]
      [  XmlElement( ElementName = "ContagemResultado_PFBFS"   )]
      public double gxTpr_Contagemresultado_pfbfs_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs) ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_pfbfs
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs = (decimal)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_PFLFS" )]
      [  XmlElement( ElementName = "ContagemResultado_PFLFS"   )]
      public double gxTpr_Contagemresultado_pflfs_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs) ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_pflfs
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs = (decimal)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_PFBFM" )]
      [  XmlElement( ElementName = "ContagemResultado_PFBFM"   )]
      public double gxTpr_Contagemresultado_pfbfm_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm) ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_pfbfm
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm = (decimal)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_PFLFM" )]
      [  XmlElement( ElementName = "ContagemResultado_PFLFM"   )]
      public double gxTpr_Contagemresultado_pflfm_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm) ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_pflfm
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm = (decimal)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Divergencia" )]
      [  XmlElement( ElementName = "ContagemResultado_Divergencia"   )]
      public double gxTpr_Contagemresultado_divergencia_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContagemResultadoContagens_Contagemresultado_divergencia) ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_divergencia = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_divergencia
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_divergencia ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_divergencia = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ParecerTcn" )]
      [  XmlElement( ElementName = "ContagemResultado_ParecerTcn"   )]
      public String gxTpr_Contagemresultado_parecertcn
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_parecertcn ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_parecertcn_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_parecertcn = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_parecertcn_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_parecertcn_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_parecertcn = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_parecertcn_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_ContadorFMCod" )]
      [  XmlElement( ElementName = "ContagemResultado_ContadorFMCod"   )]
      public int gxTpr_Contagemresultado_contadorfmcod
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmcod ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmcod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CrFMPessoaCod" )]
      [  XmlElement( ElementName = "ContagemResultado_CrFMPessoaCod"   )]
      public int gxTpr_Contagemresultado_crfmpessoacod
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_ContadorFMNom" )]
      [  XmlElement( ElementName = "ContagemResultado_ContadorFMNom"   )]
      public String gxTpr_Contagemresultado_contadorfmnom
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_CrFMEhContratada" )]
      [  XmlElement( ElementName = "ContagemResultado_CrFMEhContratada"   )]
      public bool gxTpr_Contagemresultado_crfmehcontratada
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada = value;
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada = false;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_CrFMEhContratante" )]
      [  XmlElement( ElementName = "ContagemResultado_CrFMEhContratante"   )]
      public bool gxTpr_Contagemresultado_crfmehcontratante
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante = value;
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante = false;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_NaoCnfCntCod" )]
      [  XmlElement( ElementName = "ContagemResultado_NaoCnfCntCod"   )]
      public int gxTpr_Contagemresultado_naocnfcntcod
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_NaoCnfCntNom" )]
      [  XmlElement( ElementName = "ContagemResultado_NaoCnfCntNom"   )]
      public String gxTpr_Contagemresultado_naocnfcntnom
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_StatusCnt" )]
      [  XmlElement( ElementName = "ContagemResultado_StatusCnt"   )]
      public short gxTpr_Contagemresultado_statuscnt
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_statuscnt ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_statuscnt = (short)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoContagens_Esforco" )]
      [  XmlElement( ElementName = "ContagemResultadoContagens_Esforco"   )]
      public short gxTpr_Contagemresultadocontagens_esforco
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_esforco ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_esforco = (short)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Ultima" )]
      [  XmlElement( ElementName = "ContagemResultado_Ultima"   )]
      public bool gxTpr_Contagemresultado_ultima
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_ultima ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_ultima = value;
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Deflator" )]
      [  XmlElement( ElementName = "ContagemResultado_Deflator"   )]
      public double gxTpr_Contagemresultado_deflator_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator) ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_deflator
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator = (decimal)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_CstUntPrd" )]
      [  XmlElement( ElementName = "ContagemResultado_CstUntPrd"   )]
      public double gxTpr_Contagemresultado_cstuntprd_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd) ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_cstuntprd
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd = (decimal)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Planilha" )]
      [  XmlElement( ElementName = "ContagemResultado_Planilha"   )]
      [GxUpload()]
      public byte[] gxTpr_Contagemresultado_planilha_Blob
      {
         get {
            IGxContext context = this.context == null ? new GxContext() : this.context;
            return context.FileToByteArray( gxTv_SdtContagemResultadoContagens_Contagemresultado_planilha) ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_planilha_N = 0;
            IGxContext context = this.context == null ? new GxContext() : this.context;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_planilha=context.FileFromByteArray( value) ;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      [GxUpload()]
      public String gxTpr_Contagemresultado_planilha
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_planilha ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_planilha_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_planilha = value;
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_planilha_SetBlob( String blob ,
                                                                                         String fileName ,
                                                                                         String fileType )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_planilha = blob;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla = fileName;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla = fileType;
         return  ;
      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_planilha_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_planilha_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_planilha = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_planilha_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_NomePla" )]
      [  XmlElement( ElementName = "ContagemResultado_NomePla"   )]
      public String gxTpr_Contagemresultado_nomepla
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_TipoPla" )]
      [  XmlElement( ElementName = "ContagemResultado_TipoPla"   )]
      public String gxTpr_Contagemresultado_tipopla
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_RdmnIssueId" )]
      [  XmlElement( ElementName = "ContagemResultado_RdmnIssueId"   )]
      public int gxTpr_Contagemresultado_rdmnissueid
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_NvlCnt" )]
      [  XmlElement( ElementName = "ContagemResultado_NvlCnt"   )]
      public short gxTpr_Contagemresultado_nvlcnt
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt_N = 0;
            gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt_N = 1;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Mode ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Mode = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Mode_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Mode = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Initialized ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Initialized_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Codigo_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_Codigo_Z"   )]
      public int gxTpr_Contagemresultado_codigo_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_codigo_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_codigo_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_DataCnt_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_DataCnt_Z"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_datacnt_Z_Nullable
      {
         get {
            if ( gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt_Z == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt_Z).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt_Z = DateTime.MinValue;
            else
               gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_datacnt_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_HoraCnt_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_HoraCnt_Z"   )]
      public String gxTpr_Contagemresultado_horacnt_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_horacnt_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_horacnt_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_horacnt_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_horacnt_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_horacnt_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_OsFsOsFm_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_OsFsOsFm_Z"   )]
      public String gxTpr_Contagemresultado_osfsosfm_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_osfsosfm_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_osfsosfm_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_osfsosfm_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_osfsosfm_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_osfsosfm_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_EhValidacao_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_EhValidacao_Z"   )]
      public bool gxTpr_Contagemresultado_ehvalidacao_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao_Z = value;
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao_Z = false;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Coordenacao_Z" )]
      [  XmlElement( ElementName = "Sistema_Coordenacao_Z"   )]
      public String gxTpr_Sistema_coordenacao_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Sistema_coordenacao_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Sistema_coordenacao_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Sistema_coordenacao_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Sistema_coordenacao_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Sistema_coordenacao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Demanda_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_Demanda_Z"   )]
      public String gxTpr_Contagemresultado_demanda_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_CntSrvCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_CntSrvCod_Z"   )]
      public int gxTpr_Contagemresultado_cntsrvcod_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Servico_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_Servico_Z"   )]
      public int gxTpr_Contagemresultado_servico_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_servico_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_servico_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_servico_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_servico_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_servico_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaCod_Z"   )]
      public int gxTpr_Contagemresultado_contratadacod_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaOrigemCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaOrigemCod_Z"   )]
      public int gxTpr_Contagemresultado_contratadaorigemcod_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_AreaTrabalhoCod_Z" )]
      [  XmlElement( ElementName = "Contratada_AreaTrabalhoCod_Z"   )]
      public int gxTpr_Contratada_areatrabalhocod_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Responsavel_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_Responsavel_Z"   )]
      public int gxTpr_Contagemresultado_responsavel_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_OSVinculada_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_OSVinculada_Z"   )]
      public int gxTpr_Contagemresultado_osvinculada_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_NaoCnfDmnCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_NaoCnfDmnCod_Z"   )]
      public int gxTpr_Contagemresultado_naocnfdmncod_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_StatusDmn_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_StatusDmn_Z"   )]
      public String gxTpr_Contagemresultado_statusdmn_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_TimeCnt_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_TimeCnt_Z"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_timecnt_Z_Nullable
      {
         get {
            if ( gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_Z == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_Z).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_Z = DateTime.MinValue;
            else
               gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_timecnt_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoContagens_Prazo_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoContagens_Prazo_Z"  , IsNullable=true )]
      public string gxTpr_Contagemresultadocontagens_prazo_Z_Nullable
      {
         get {
            if ( gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_Z == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_Z).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_Z = DateTime.MinValue;
            else
               gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultadocontagens_prazo_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_PFBFS_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_PFBFS_Z"   )]
      public double gxTpr_Contagemresultado_pfbfs_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs_Z) ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_pfbfs_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_PFLFS_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_PFLFS_Z"   )]
      public double gxTpr_Contagemresultado_pflfs_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs_Z) ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_pflfs_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_PFBFM_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_PFBFM_Z"   )]
      public double gxTpr_Contagemresultado_pfbfm_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm_Z) ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_pfbfm_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_PFLFM_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_PFLFM_Z"   )]
      public double gxTpr_Contagemresultado_pflfm_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm_Z) ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_pflfm_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Divergencia_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_Divergencia_Z"   )]
      public double gxTpr_Contagemresultado_divergencia_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContagemResultadoContagens_Contagemresultado_divergencia_Z) ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_divergencia_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_divergencia_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_divergencia_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_divergencia_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_divergencia_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_divergencia_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_divergencia_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_ContadorFMCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_ContadorFMCod_Z"   )]
      public int gxTpr_Contagemresultado_contadorfmcod_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmcod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmcod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmcod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmcod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmcod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_CrFMPessoaCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_CrFMPessoaCod_Z"   )]
      public int gxTpr_Contagemresultado_crfmpessoacod_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_ContadorFMNom_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_ContadorFMNom_Z"   )]
      public String gxTpr_Contagemresultado_contadorfmnom_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_CrFMEhContratada_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_CrFMEhContratada_Z"   )]
      public bool gxTpr_Contagemresultado_crfmehcontratada_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada_Z = value;
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada_Z = false;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_CrFMEhContratante_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_CrFMEhContratante_Z"   )]
      public bool gxTpr_Contagemresultado_crfmehcontratante_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante_Z = value;
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante_Z = false;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_NaoCnfCntCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_NaoCnfCntCod_Z"   )]
      public int gxTpr_Contagemresultado_naocnfcntcod_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_NaoCnfCntNom_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_NaoCnfCntNom_Z"   )]
      public String gxTpr_Contagemresultado_naocnfcntnom_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_StatusCnt_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_StatusCnt_Z"   )]
      public short gxTpr_Contagemresultado_statuscnt_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_statuscnt_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_statuscnt_Z = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_statuscnt_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_statuscnt_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_statuscnt_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoContagens_Esforco_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoContagens_Esforco_Z"   )]
      public short gxTpr_Contagemresultadocontagens_esforco_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_esforco_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_esforco_Z = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_esforco_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_esforco_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_esforco_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Ultima_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_Ultima_Z"   )]
      public bool gxTpr_Contagemresultado_ultima_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_ultima_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_ultima_Z = value;
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_ultima_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_ultima_Z = false;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_ultima_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Deflator_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_Deflator_Z"   )]
      public double gxTpr_Contagemresultado_deflator_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator_Z) ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_deflator_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_CstUntPrd_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_CstUntPrd_Z"   )]
      public double gxTpr_Contagemresultado_cstuntprd_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd_Z) ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_cstuntprd_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_NomePla_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_NomePla_Z"   )]
      public String gxTpr_Contagemresultado_nomepla_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_TipoPla_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_TipoPla_Z"   )]
      public String gxTpr_Contagemresultado_tipopla_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_RdmnIssueId_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_RdmnIssueId_Z"   )]
      public int gxTpr_Contagemresultado_rdmnissueid_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_NvlCnt_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_NvlCnt_Z"   )]
      public short gxTpr_Contagemresultado_nvlcnt_Z
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt_Z ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt_Z = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_EhValidacao_N" )]
      [  XmlElement( ElementName = "ContagemResultado_EhValidacao_N"   )]
      public short gxTpr_Contagemresultado_ehvalidacao_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Coordenacao_N" )]
      [  XmlElement( ElementName = "Sistema_Coordenacao_N"   )]
      public short gxTpr_Sistema_coordenacao_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Sistema_coordenacao_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Sistema_coordenacao_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Sistema_coordenacao_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Sistema_coordenacao_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Sistema_coordenacao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Demanda_N" )]
      [  XmlElement( ElementName = "ContagemResultado_Demanda_N"   )]
      public short gxTpr_Contagemresultado_demanda_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_CntSrvCod_N" )]
      [  XmlElement( ElementName = "ContagemResultado_CntSrvCod_N"   )]
      public short gxTpr_Contagemresultado_cntsrvcod_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Servico_N" )]
      [  XmlElement( ElementName = "ContagemResultado_Servico_N"   )]
      public short gxTpr_Contagemresultado_servico_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_servico_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_servico_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_servico_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_servico_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_servico_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaCod_N" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaCod_N"   )]
      public short gxTpr_Contagemresultado_contratadacod_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaOrigemCod_N" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaOrigemCod_N"   )]
      public short gxTpr_Contagemresultado_contratadaorigemcod_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratada_AreaTrabalhoCod_N" )]
      [  XmlElement( ElementName = "Contratada_AreaTrabalhoCod_N"   )]
      public short gxTpr_Contratada_areatrabalhocod_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Responsavel_N" )]
      [  XmlElement( ElementName = "ContagemResultado_Responsavel_N"   )]
      public short gxTpr_Contagemresultado_responsavel_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_OSVinculada_N" )]
      [  XmlElement( ElementName = "ContagemResultado_OSVinculada_N"   )]
      public short gxTpr_Contagemresultado_osvinculada_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_NaoCnfDmnCod_N" )]
      [  XmlElement( ElementName = "ContagemResultado_NaoCnfDmnCod_N"   )]
      public short gxTpr_Contagemresultado_naocnfdmncod_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_StatusDmn_N" )]
      [  XmlElement( ElementName = "ContagemResultado_StatusDmn_N"   )]
      public short gxTpr_Contagemresultado_statusdmn_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_TimeCnt_N" )]
      [  XmlElement( ElementName = "ContagemResultado_TimeCnt_N"   )]
      public short gxTpr_Contagemresultado_timecnt_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoContagens_Prazo_N" )]
      [  XmlElement( ElementName = "ContagemResultadoContagens_Prazo_N"   )]
      public short gxTpr_Contagemresultadocontagens_prazo_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_PFBFS_N" )]
      [  XmlElement( ElementName = "ContagemResultado_PFBFS_N"   )]
      public short gxTpr_Contagemresultado_pfbfs_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_PFLFS_N" )]
      [  XmlElement( ElementName = "ContagemResultado_PFLFS_N"   )]
      public short gxTpr_Contagemresultado_pflfs_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_PFBFM_N" )]
      [  XmlElement( ElementName = "ContagemResultado_PFBFM_N"   )]
      public short gxTpr_Contagemresultado_pfbfm_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_PFLFM_N" )]
      [  XmlElement( ElementName = "ContagemResultado_PFLFM_N"   )]
      public short gxTpr_Contagemresultado_pflfm_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_ParecerTcn_N" )]
      [  XmlElement( ElementName = "ContagemResultado_ParecerTcn_N"   )]
      public short gxTpr_Contagemresultado_parecertcn_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_parecertcn_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_parecertcn_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_parecertcn_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_parecertcn_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_parecertcn_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_CrFMPessoaCod_N" )]
      [  XmlElement( ElementName = "ContagemResultado_CrFMPessoaCod_N"   )]
      public short gxTpr_Contagemresultado_crfmpessoacod_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_ContadorFMNom_N" )]
      [  XmlElement( ElementName = "ContagemResultado_ContadorFMNom_N"   )]
      public short gxTpr_Contagemresultado_contadorfmnom_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_CrFMEhContratada_N" )]
      [  XmlElement( ElementName = "ContagemResultado_CrFMEhContratada_N"   )]
      public short gxTpr_Contagemresultado_crfmehcontratada_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_CrFMEhContratante_N" )]
      [  XmlElement( ElementName = "ContagemResultado_CrFMEhContratante_N"   )]
      public short gxTpr_Contagemresultado_crfmehcontratante_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_NaoCnfCntCod_N" )]
      [  XmlElement( ElementName = "ContagemResultado_NaoCnfCntCod_N"   )]
      public short gxTpr_Contagemresultado_naocnfcntcod_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_NaoCnfCntNom_N" )]
      [  XmlElement( ElementName = "ContagemResultado_NaoCnfCntNom_N"   )]
      public short gxTpr_Contagemresultado_naocnfcntnom_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Deflator_N" )]
      [  XmlElement( ElementName = "ContagemResultado_Deflator_N"   )]
      public short gxTpr_Contagemresultado_deflator_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_CstUntPrd_N" )]
      [  XmlElement( ElementName = "ContagemResultado_CstUntPrd_N"   )]
      public short gxTpr_Contagemresultado_cstuntprd_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Planilha_N" )]
      [  XmlElement( ElementName = "ContagemResultado_Planilha_N"   )]
      public short gxTpr_Contagemresultado_planilha_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_planilha_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_planilha_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_planilha_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_planilha_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_planilha_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_NomePla_N" )]
      [  XmlElement( ElementName = "ContagemResultado_NomePla_N"   )]
      public short gxTpr_Contagemresultado_nomepla_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_TipoPla_N" )]
      [  XmlElement( ElementName = "ContagemResultado_TipoPla_N"   )]
      public short gxTpr_Contagemresultado_tipopla_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_RdmnIssueId_N" )]
      [  XmlElement( ElementName = "ContagemResultado_RdmnIssueId_N"   )]
      public short gxTpr_Contagemresultado_rdmnissueid_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_NvlCnt_N" )]
      [  XmlElement( ElementName = "ContagemResultado_NvlCnt_N"   )]
      public short gxTpr_Contagemresultado_nvlcnt_N
      {
         get {
            return gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt_N ;
         }

         set {
            gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt_N_SetNull( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt = DateTime.MinValue;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_horacnt = context.localUtil.Time( );
         gxTv_SdtContagemResultadoContagens_Contagemresultado_osfsosfm = "";
         gxTv_SdtContagemResultadoContagens_Sistema_coordenacao = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoContagens_Contagemresultado_parecertcn = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_planilha = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla = "";
         gxTv_SdtContagemResultadoContagens_Mode = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt_Z = DateTime.MinValue;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_horacnt_Z = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_osfsosfm_Z = "";
         gxTv_SdtContagemResultadoContagens_Sistema_coordenacao_Z = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda_Z = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn_Z = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom_Z = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom_Z = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla_Z = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla_Z = "";
         gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao = false;
         gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_esforco = 0;
         gxTv_SdtContagemResultadoContagens_Contagemresultado_ultima = true;
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         datetime_STZ = (DateTime)(DateTime.MinValue);
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "contagemresultadocontagens", "GeneXus.Programs.contagemresultadocontagens_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_statuscnt ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_esforco ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt ;
      private short gxTv_SdtContagemResultadoContagens_Initialized ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_statuscnt_Z ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_esforco_Z ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt_Z ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao_N ;
      private short gxTv_SdtContagemResultadoContagens_Sistema_coordenacao_N ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda_N ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod_N ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_servico_N ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod_N ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod_N ;
      private short gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod_N ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel_N ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada_N ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod_N ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn_N ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_N ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_N ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs_N ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs_N ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm_N ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm_N ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_parecertcn_N ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod_N ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom_N ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada_N ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante_N ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod_N ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom_N ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator_N ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd_N ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_planilha_N ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla_N ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla_N ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid_N ;
      private short gxTv_SdtContagemResultadoContagens_Contagemresultado_nvlcnt_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtContagemResultadoContagens_Contagemresultado_codigo ;
      private int gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod ;
      private int gxTv_SdtContagemResultadoContagens_Contagemresultado_servico ;
      private int gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod ;
      private int gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod ;
      private int gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod ;
      private int gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel ;
      private int gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada ;
      private int gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod ;
      private int gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmcod ;
      private int gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod ;
      private int gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod ;
      private int gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid ;
      private int gxTv_SdtContagemResultadoContagens_Contagemresultado_codigo_Z ;
      private int gxTv_SdtContagemResultadoContagens_Contagemresultado_cntsrvcod_Z ;
      private int gxTv_SdtContagemResultadoContagens_Contagemresultado_servico_Z ;
      private int gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadacod_Z ;
      private int gxTv_SdtContagemResultadoContagens_Contagemresultado_contratadaorigemcod_Z ;
      private int gxTv_SdtContagemResultadoContagens_Contratada_areatrabalhocod_Z ;
      private int gxTv_SdtContagemResultadoContagens_Contagemresultado_responsavel_Z ;
      private int gxTv_SdtContagemResultadoContagens_Contagemresultado_osvinculada_Z ;
      private int gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfdmncod_Z ;
      private int gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmcod_Z ;
      private int gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmpessoacod_Z ;
      private int gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntcod_Z ;
      private int gxTv_SdtContagemResultadoContagens_Contagemresultado_rdmnissueid_Z ;
      private decimal gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs ;
      private decimal gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs ;
      private decimal gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm ;
      private decimal gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm ;
      private decimal gxTv_SdtContagemResultadoContagens_Contagemresultado_divergencia ;
      private decimal gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator ;
      private decimal gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd ;
      private decimal gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfs_Z ;
      private decimal gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfs_Z ;
      private decimal gxTv_SdtContagemResultadoContagens_Contagemresultado_pfbfm_Z ;
      private decimal gxTv_SdtContagemResultadoContagens_Contagemresultado_pflfm_Z ;
      private decimal gxTv_SdtContagemResultadoContagens_Contagemresultado_divergencia_Z ;
      private decimal gxTv_SdtContagemResultadoContagens_Contagemresultado_deflator_Z ;
      private decimal gxTv_SdtContagemResultadoContagens_Contagemresultado_cstuntprd_Z ;
      private String gxTv_SdtContagemResultadoContagens_Contagemresultado_horacnt ;
      private String gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn ;
      private String gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom ;
      private String gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom ;
      private String gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla ;
      private String gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla ;
      private String gxTv_SdtContagemResultadoContagens_Mode ;
      private String gxTv_SdtContagemResultadoContagens_Contagemresultado_horacnt_Z ;
      private String gxTv_SdtContagemResultadoContagens_Contagemresultado_statusdmn_Z ;
      private String gxTv_SdtContagemResultadoContagens_Contagemresultado_contadorfmnom_Z ;
      private String gxTv_SdtContagemResultadoContagens_Contagemresultado_naocnfcntnom_Z ;
      private String gxTv_SdtContagemResultadoContagens_Contagemresultado_nomepla_Z ;
      private String gxTv_SdtContagemResultadoContagens_Contagemresultado_tipopla_Z ;
      private String sTagName ;
      private String sDateCnv ;
      private String sNumToPad ;
      private DateTime gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt ;
      private DateTime gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo ;
      private DateTime gxTv_SdtContagemResultadoContagens_Contagemresultado_timecnt_Z ;
      private DateTime gxTv_SdtContagemResultadoContagens_Contagemresultadocontagens_prazo_Z ;
      private DateTime datetime_STZ ;
      private DateTime gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt ;
      private DateTime gxTv_SdtContagemResultadoContagens_Contagemresultado_datacnt_Z ;
      private bool gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao ;
      private bool gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada ;
      private bool gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante ;
      private bool gxTv_SdtContagemResultadoContagens_Contagemresultado_ultima ;
      private bool gxTv_SdtContagemResultadoContagens_Contagemresultado_ehvalidacao_Z ;
      private bool gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratada_Z ;
      private bool gxTv_SdtContagemResultadoContagens_Contagemresultado_crfmehcontratante_Z ;
      private bool gxTv_SdtContagemResultadoContagens_Contagemresultado_ultima_Z ;
      private String gxTv_SdtContagemResultadoContagens_Contagemresultado_parecertcn ;
      private String gxTv_SdtContagemResultadoContagens_Contagemresultado_osfsosfm ;
      private String gxTv_SdtContagemResultadoContagens_Sistema_coordenacao ;
      private String gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda ;
      private String gxTv_SdtContagemResultadoContagens_Contagemresultado_osfsosfm_Z ;
      private String gxTv_SdtContagemResultadoContagens_Sistema_coordenacao_Z ;
      private String gxTv_SdtContagemResultadoContagens_Contagemresultado_demanda_Z ;
      private String gxTv_SdtContagemResultadoContagens_Contagemresultado_planilha ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ContagemResultadoContagens", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtContagemResultadoContagens_RESTInterface : GxGenericCollectionItem<SdtContagemResultadoContagens>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContagemResultadoContagens_RESTInterface( ) : base()
      {
      }

      public SdtContagemResultadoContagens_RESTInterface( SdtContagemResultadoContagens psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContagemResultado_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_codigo
      {
         get {
            return sdt.gxTpr_Contagemresultado_codigo ;
         }

         set {
            sdt.gxTpr_Contagemresultado_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_DataCnt" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_datacnt
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Contagemresultado_datacnt) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_datacnt = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_HoraCnt" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_horacnt
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_horacnt) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_horacnt = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_OsFsOsFm" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_osfsosfm
      {
         get {
            return sdt.gxTpr_Contagemresultado_osfsosfm ;
         }

         set {
            sdt.gxTpr_Contagemresultado_osfsosfm = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_EhValidacao" , Order = 4 )]
      [GxSeudo()]
      public bool gxTpr_Contagemresultado_ehvalidacao
      {
         get {
            return sdt.gxTpr_Contagemresultado_ehvalidacao ;
         }

         set {
            sdt.gxTpr_Contagemresultado_ehvalidacao = value;
         }

      }

      [DataMember( Name = "Sistema_Coordenacao" , Order = 5 )]
      [GxSeudo()]
      public String gxTpr_Sistema_coordenacao
      {
         get {
            return sdt.gxTpr_Sistema_coordenacao ;
         }

         set {
            sdt.gxTpr_Sistema_coordenacao = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_Demanda" , Order = 6 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_demanda
      {
         get {
            return sdt.gxTpr_Contagemresultado_demanda ;
         }

         set {
            sdt.gxTpr_Contagemresultado_demanda = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_CntSrvCod" , Order = 7 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_cntsrvcod
      {
         get {
            return sdt.gxTpr_Contagemresultado_cntsrvcod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntsrvcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_Servico" , Order = 8 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_servico
      {
         get {
            return sdt.gxTpr_Contagemresultado_servico ;
         }

         set {
            sdt.gxTpr_Contagemresultado_servico = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_ContratadaCod" , Order = 9 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_contratadacod
      {
         get {
            return sdt.gxTpr_Contagemresultado_contratadacod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contratadacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_ContratadaOrigemCod" , Order = 10 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_contratadaorigemcod
      {
         get {
            return sdt.gxTpr_Contagemresultado_contratadaorigemcod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contratadaorigemcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contratada_AreaTrabalhoCod" , Order = 11 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratada_areatrabalhocod
      {
         get {
            return sdt.gxTpr_Contratada_areatrabalhocod ;
         }

         set {
            sdt.gxTpr_Contratada_areatrabalhocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_Responsavel" , Order = 12 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_responsavel
      {
         get {
            return sdt.gxTpr_Contagemresultado_responsavel ;
         }

         set {
            sdt.gxTpr_Contagemresultado_responsavel = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_OSVinculada" , Order = 13 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_osvinculada
      {
         get {
            return sdt.gxTpr_Contagemresultado_osvinculada ;
         }

         set {
            sdt.gxTpr_Contagemresultado_osvinculada = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_NaoCnfDmnCod" , Order = 14 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_naocnfdmncod
      {
         get {
            return sdt.gxTpr_Contagemresultado_naocnfdmncod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_naocnfdmncod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_StatusDmn" , Order = 15 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_statusdmn
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_statusdmn) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_statusdmn = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_TimeCnt" , Order = 16 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_timecnt
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Contagemresultado_timecnt) ;
         }

         set {
            GXt_dtime1 = DateTimeUtil.ResetDate(DateTimeUtil.CToT2( (String)(value)));
            sdt.gxTpr_Contagemresultado_timecnt = GXt_dtime1;
         }

      }

      [DataMember( Name = "ContagemResultadoContagens_Prazo" , Order = 17 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultadocontagens_prazo
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Contagemresultadocontagens_prazo) ;
         }

         set {
            sdt.gxTpr_Contagemresultadocontagens_prazo = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_PFBFS" , Order = 18 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_pfbfs
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_pfbfs, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_pfbfs = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_PFLFS" , Order = 19 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_pflfs
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_pflfs, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_pflfs = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_PFBFM" , Order = 20 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_pfbfm
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_pfbfm, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_pfbfm = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_PFLFM" , Order = 21 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_pflfm
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_pflfm, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_pflfm = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_Divergencia" , Order = 22 )]
      [GxSeudo()]
      public Nullable<decimal> gxTpr_Contagemresultado_divergencia
      {
         get {
            return sdt.gxTpr_Contagemresultado_divergencia ;
         }

         set {
            sdt.gxTpr_Contagemresultado_divergencia = (decimal)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_ParecerTcn" , Order = 23 )]
      public String gxTpr_Contagemresultado_parecertcn
      {
         get {
            return sdt.gxTpr_Contagemresultado_parecertcn ;
         }

         set {
            sdt.gxTpr_Contagemresultado_parecertcn = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_ContadorFMCod" , Order = 24 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_contadorfmcod
      {
         get {
            return sdt.gxTpr_Contagemresultado_contadorfmcod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contadorfmcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_CrFMPessoaCod" , Order = 25 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_crfmpessoacod
      {
         get {
            return sdt.gxTpr_Contagemresultado_crfmpessoacod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_crfmpessoacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_ContadorFMNom" , Order = 26 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_contadorfmnom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_contadorfmnom) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contadorfmnom = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_CrFMEhContratada" , Order = 27 )]
      [GxSeudo()]
      public bool gxTpr_Contagemresultado_crfmehcontratada
      {
         get {
            return sdt.gxTpr_Contagemresultado_crfmehcontratada ;
         }

         set {
            sdt.gxTpr_Contagemresultado_crfmehcontratada = value;
         }

      }

      [DataMember( Name = "ContagemResultado_CrFMEhContratante" , Order = 28 )]
      [GxSeudo()]
      public bool gxTpr_Contagemresultado_crfmehcontratante
      {
         get {
            return sdt.gxTpr_Contagemresultado_crfmehcontratante ;
         }

         set {
            sdt.gxTpr_Contagemresultado_crfmehcontratante = value;
         }

      }

      [DataMember( Name = "ContagemResultado_NaoCnfCntCod" , Order = 29 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_naocnfcntcod
      {
         get {
            return sdt.gxTpr_Contagemresultado_naocnfcntcod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_naocnfcntcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_NaoCnfCntNom" , Order = 30 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_naocnfcntnom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_naocnfcntnom) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_naocnfcntnom = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_StatusCnt" , Order = 31 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contagemresultado_statuscnt
      {
         get {
            return sdt.gxTpr_Contagemresultado_statuscnt ;
         }

         set {
            sdt.gxTpr_Contagemresultado_statuscnt = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoContagens_Esforco" , Order = 32 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contagemresultadocontagens_esforco
      {
         get {
            return sdt.gxTpr_Contagemresultadocontagens_esforco ;
         }

         set {
            sdt.gxTpr_Contagemresultadocontagens_esforco = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_Ultima" , Order = 33 )]
      [GxSeudo()]
      public bool gxTpr_Contagemresultado_ultima
      {
         get {
            return sdt.gxTpr_Contagemresultado_ultima ;
         }

         set {
            sdt.gxTpr_Contagemresultado_ultima = value;
         }

      }

      [DataMember( Name = "ContagemResultado_Deflator" , Order = 34 )]
      [GxSeudo()]
      public Nullable<decimal> gxTpr_Contagemresultado_deflator
      {
         get {
            return sdt.gxTpr_Contagemresultado_deflator ;
         }

         set {
            sdt.gxTpr_Contagemresultado_deflator = (decimal)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_CstUntPrd" , Order = 35 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_cstuntprd
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_cstuntprd, 18, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cstuntprd = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_Planilha" , Order = 36 )]
      [GxUpload()]
      public String gxTpr_Contagemresultado_planilha
      {
         get {
            return PathUtil.RelativePath( sdt.gxTpr_Contagemresultado_planilha) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_planilha = value;
         }

      }

      [DataMember( Name = "ContagemResultado_NomePla" , Order = 37 )]
      public String gxTpr_Contagemresultado_nomepla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_nomepla) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_nomepla = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_TipoPla" , Order = 38 )]
      public String gxTpr_Contagemresultado_tipopla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_tipopla) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_tipopla = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_RdmnIssueId" , Order = 39 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_rdmnissueid
      {
         get {
            return sdt.gxTpr_Contagemresultado_rdmnissueid ;
         }

         set {
            sdt.gxTpr_Contagemresultado_rdmnissueid = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_NvlCnt" , Order = 40 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contagemresultado_nvlcnt
      {
         get {
            return sdt.gxTpr_Contagemresultado_nvlcnt ;
         }

         set {
            sdt.gxTpr_Contagemresultado_nvlcnt = (short)(value.HasValue ? value.Value : 0);
         }

      }

      public SdtContagemResultadoContagens sdt
      {
         get {
            return (SdtContagemResultadoContagens)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtContagemResultadoContagens() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 114 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
      private DateTime GXt_dtime1 ;
   }

}
