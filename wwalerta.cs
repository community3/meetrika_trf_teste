/*
               File: WWAlerta
        Description:  Alertas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:44:50.97
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwalerta : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwalerta( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwalerta( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         dynAlerta_ToUser = new GXCombobox();
         cmbAlerta_ToGroup = new GXCombobox();
         dynAlerta_Owner = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"ALERTA_TOUSER") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLAALERTA_TOUSEROO2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"ALERTA_OWNER") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLAALERTA_OWNEROO2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_96 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_96_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_96_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV82Alerta_Inicio1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82Alerta_Inicio1", context.localUtil.Format(AV82Alerta_Inicio1, "99/99/99"));
               AV83Alerta_Inicio_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83Alerta_Inicio_To1", context.localUtil.Format(AV83Alerta_Inicio_To1, "99/99/99"));
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV84Alerta_Inicio2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84Alerta_Inicio2", context.localUtil.Format(AV84Alerta_Inicio2, "99/99/99"));
               AV85Alerta_Inicio_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85Alerta_Inicio_To2", context.localUtil.Format(AV85Alerta_Inicio_To2, "99/99/99"));
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV86Alerta_Inicio3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86Alerta_Inicio3", context.localUtil.Format(AV86Alerta_Inicio3, "99/99/99"));
               AV87Alerta_Inicio_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87Alerta_Inicio_To3", context.localUtil.Format(AV87Alerta_Inicio_To3, "99/99/99"));
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV2WWPContext);
               AV32ManageFiltersExecutionStep = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV32ManageFiltersExecutionStep), 1, 0));
               AV81Alerta_Owner = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81Alerta_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81Alerta_Owner), 6, 0)));
               AV105Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               A1881Alerta_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A1882Alerta_ToAreaTrabalho = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n1882Alerta_ToAreaTrabalho = false;
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV82Alerta_Inicio1, AV83Alerta_Inicio_To1, AV19DynamicFiltersSelector2, AV84Alerta_Inicio2, AV85Alerta_Inicio_To2, AV23DynamicFiltersSelector3, AV86Alerta_Inicio3, AV87Alerta_Inicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV2WWPContext, AV32ManageFiltersExecutionStep, AV81Alerta_Owner, AV105Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1881Alerta_Codigo, A1882Alerta_ToAreaTrabalho) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAOO2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTOO2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299445137");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwalerta.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vALERTA_INICIO1", context.localUtil.Format(AV82Alerta_Inicio1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vALERTA_INICIO_TO1", context.localUtil.Format(AV83Alerta_Inicio_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vALERTA_INICIO2", context.localUtil.Format(AV84Alerta_Inicio2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vALERTA_INICIO_TO2", context.localUtil.Format(AV85Alerta_Inicio_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vALERTA_INICIO3", context.localUtil.Format(AV86Alerta_Inicio3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vALERTA_INICIO_TO3", context.localUtil.Format(AV87Alerta_Inicio_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_96", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_96), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMANAGEFILTERSDATA", AV36ManageFiltersData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMANAGEFILTERSDATA", AV36ManageFiltersData);
         }
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV74GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV75GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV2WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV2WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV105Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Icon", StringUtil.RTrim( Ddo_managefilters_Icon));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Caption", StringUtil.RTrim( Ddo_managefilters_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Tooltip", StringUtil.RTrim( Ddo_managefilters_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Cls", StringUtil.RTrim( Ddo_managefilters_Cls));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Activeeventkey", StringUtil.RTrim( Ddo_managefilters_Activeeventkey));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEOO2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTOO2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwalerta.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWAlerta" ;
      }

      public override String GetPgmdesc( )
      {
         return " Alertas" ;
      }

      protected void WBOO0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_OO2( true) ;
         }
         else
         {
            wb_table1_2_OO2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_OO2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_96_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(115, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,115);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_96_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(116, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,116);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_96_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavManagefiltersexecutionstep_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32ManageFiltersExecutionStep), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV32ManageFiltersExecutionStep), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,117);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavManagefiltersexecutionstep_Jsonclick, 0, "Attribute", "", "", "", edtavManagefiltersexecutionstep_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAlerta.htm");
         }
         wbLoad = true;
      }

      protected void STARTOO2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Alertas", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPOO0( ) ;
      }

      protected void WSOO2( )
      {
         STARTOO2( ) ;
         EVTOO2( ) ;
      }

      protected void EVTOO2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MANAGEFILTERS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11OO2 */
                              E11OO2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12OO2 */
                              E12OO2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13OO2 */
                              E13OO2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14OO2 */
                              E14OO2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15OO2 */
                              E15OO2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16OO2 */
                              E16OO2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17OO2 */
                              E17OO2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18OO2 */
                              E18OO2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19OO2 */
                              E19OO2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20OO2 */
                              E20OO2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21OO2 */
                              E21OO2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22OO2 */
                              E22OO2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_96_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_96_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_96_idx), 4, 0)), 4, "0");
                              SubsflControlProps_962( ) ;
                              AV28Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV102Update_GXI : context.convertURL( context.PathToRelativeUrl( AV28Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV103Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              AV30Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV30Display)) ? AV104Display_GXI : context.convertURL( context.PathToRelativeUrl( AV30Display))));
                              A1881Alerta_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAlerta_Codigo_Internalname), ",", "."));
                              A1882Alerta_ToAreaTrabalho = (int)(context.localUtil.CToN( cgiGet( edtAlerta_ToAreaTrabalho_Internalname), ",", "."));
                              n1882Alerta_ToAreaTrabalho = false;
                              AV80AreaTrabalho_Descricao = StringUtil.Upper( cgiGet( edtavAreatrabalho_descricao_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAreatrabalho_descricao_Internalname, AV80AreaTrabalho_Descricao);
                              dynAlerta_ToUser.Name = dynAlerta_ToUser_Internalname;
                              dynAlerta_ToUser.CurrentValue = cgiGet( dynAlerta_ToUser_Internalname);
                              A1883Alerta_ToUser = (int)(NumberUtil.Val( cgiGet( dynAlerta_ToUser_Internalname), "."));
                              n1883Alerta_ToUser = false;
                              cmbAlerta_ToGroup.Name = cmbAlerta_ToGroup_Internalname;
                              cmbAlerta_ToGroup.CurrentValue = cgiGet( cmbAlerta_ToGroup_Internalname);
                              A1884Alerta_ToGroup = (short)(NumberUtil.Val( cgiGet( cmbAlerta_ToGroup_Internalname), "."));
                              n1884Alerta_ToGroup = false;
                              A1885Alerta_Inicio = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtAlerta_Inicio_Internalname), 0));
                              A1886Alerta_Fim = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtAlerta_Fim_Internalname), 0));
                              n1886Alerta_Fim = false;
                              dynAlerta_Owner.Name = dynAlerta_Owner_Internalname;
                              dynAlerta_Owner.CurrentValue = cgiGet( dynAlerta_Owner_Internalname);
                              A1887Alerta_Owner = (int)(NumberUtil.Val( cgiGet( dynAlerta_Owner_Internalname), "."));
                              A1888Alerta_Cadastro = context.localUtil.CToT( cgiGet( edtAlerta_Cadastro_Internalname), 0);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E23OO2 */
                                    E23OO2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E24OO2 */
                                    E24OO2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E25OO2 */
                                    E25OO2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Alerta_inicio1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vALERTA_INICIO1"), 0) != AV82Alerta_Inicio1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Alerta_inicio_to1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vALERTA_INICIO_TO1"), 0) != AV83Alerta_Inicio_To1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Alerta_inicio2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vALERTA_INICIO2"), 0) != AV84Alerta_Inicio2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Alerta_inicio_to2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vALERTA_INICIO_TO2"), 0) != AV85Alerta_Inicio_To2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Alerta_inicio3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vALERTA_INICIO3"), 0) != AV86Alerta_Inicio3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Alerta_inicio_to3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vALERTA_INICIO_TO3"), 0) != AV87Alerta_Inicio_To3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEOO2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAOO2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("ALERTA_INICIO", "Inicio", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("ALERTA_INICIO", "Inicio", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("ALERTA_INICIO", "Inicio", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            GXCCtl = "ALERTA_TOUSER_" + sGXsfl_96_idx;
            dynAlerta_ToUser.Name = GXCCtl;
            dynAlerta_ToUser.WebTags = "";
            GXCCtl = "ALERTA_TOGROUP_" + sGXsfl_96_idx;
            cmbAlerta_ToGroup.Name = GXCCtl;
            cmbAlerta_ToGroup.WebTags = "";
            cmbAlerta_ToGroup.addItem("0", "(Nenhum)", 0);
            cmbAlerta_ToGroup.addItem("1", "Todos os usu�rios", 0);
            cmbAlerta_ToGroup.addItem("2", "Todos os usu�rios da Contratante", 0);
            cmbAlerta_ToGroup.addItem("3", "Todos os usu�rios da Contratada", 0);
            cmbAlerta_ToGroup.addItem("4", "Todos os gestores", 0);
            cmbAlerta_ToGroup.addItem("5", "Todos os gestores da Contratante", 0);
            cmbAlerta_ToGroup.addItem("6", "Todos os gestores da Contratada", 0);
            if ( cmbAlerta_ToGroup.ItemCount > 0 )
            {
               A1884Alerta_ToGroup = (short)(NumberUtil.Val( cmbAlerta_ToGroup.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0))), "."));
               n1884Alerta_ToGroup = false;
            }
            GXCCtl = "ALERTA_OWNER_" + sGXsfl_96_idx;
            dynAlerta_Owner.Name = GXCCtl;
            dynAlerta_Owner.WebTags = "";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAALERTA_TOUSEROO2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAALERTA_TOUSER_dataOO2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAALERTA_TOUSER_htmlOO2( )
      {
         int gxdynajaxvalue ;
         GXDLAALERTA_TOUSER_dataOO2( ) ;
         gxdynajaxindex = 1;
         dynAlerta_ToUser.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynAlerta_ToUser.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAALERTA_TOUSER_dataOO2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Indiferente)");
         /* Using cursor H00OO2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00OO2_A1Usuario_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00OO2_A58Usuario_PessoaNom[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLAALERTA_OWNEROO2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAALERTA_OWNER_dataOO2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAALERTA_OWNER_htmlOO2( )
      {
         int gxdynajaxvalue ;
         GXDLAALERTA_OWNER_dataOO2( ) ;
         gxdynajaxindex = 1;
         dynAlerta_Owner.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynAlerta_Owner.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAALERTA_OWNER_dataOO2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H00OO3 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00OO3_A1Usuario_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00OO3_A58Usuario_PessoaNom[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_962( ) ;
         while ( nGXsfl_96_idx <= nRC_GXsfl_96 )
         {
            sendrow_962( ) ;
            nGXsfl_96_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_96_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_96_idx+1));
            sGXsfl_96_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_96_idx), 4, 0)), 4, "0");
            SubsflControlProps_962( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       DateTime AV82Alerta_Inicio1 ,
                                       DateTime AV83Alerta_Inicio_To1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       DateTime AV84Alerta_Inicio2 ,
                                       DateTime AV85Alerta_Inicio_To2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       DateTime AV86Alerta_Inicio3 ,
                                       DateTime AV87Alerta_Inicio_To3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       wwpbaseobjects.SdtWWPContext AV2WWPContext ,
                                       short AV32ManageFiltersExecutionStep ,
                                       int AV81Alerta_Owner ,
                                       String AV105Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       int A1881Alerta_Codigo ,
                                       int A1882Alerta_ToAreaTrabalho )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFOO2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_ALERTA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1881Alerta_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "ALERTA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1881Alerta_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_ALERTA_TOAREATRABALHO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1882Alerta_ToAreaTrabalho), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "ALERTA_TOAREATRABALHO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1882Alerta_ToAreaTrabalho), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_ALERTA_TOUSER", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1883Alerta_ToUser), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "ALERTA_TOUSER", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1883Alerta_ToUser), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_ALERTA_TOGROUP", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1884Alerta_ToGroup), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "ALERTA_TOGROUP", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1884Alerta_ToGroup), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_ALERTA_INICIO", GetSecureSignedToken( "", A1885Alerta_Inicio));
         GxWebStd.gx_hidden_field( context, "ALERTA_INICIO", context.localUtil.Format(A1885Alerta_Inicio, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_ALERTA_FIM", GetSecureSignedToken( "", A1886Alerta_Fim));
         GxWebStd.gx_hidden_field( context, "ALERTA_FIM", context.localUtil.Format(A1886Alerta_Fim, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_ALERTA_OWNER", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1887Alerta_Owner), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "ALERTA_OWNER", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1887Alerta_Owner), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_ALERTA_CADASTRO", GetSecureSignedToken( "", context.localUtil.Format( A1888Alerta_Cadastro, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "ALERTA_CADASTRO", context.localUtil.TToC( A1888Alerta_Cadastro, 10, 8, 0, 3, "/", ":", " "));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFOO2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV105Pgmname = "WWAlerta";
         context.Gx_err = 0;
         edtavAreatrabalho_descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAreatrabalho_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreatrabalho_descricao_Enabled), 5, 0)));
      }

      protected void RFOO2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 96;
         /* Execute user event: E24OO2 */
         E24OO2 ();
         nGXsfl_96_idx = 1;
         sGXsfl_96_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_96_idx), 4, 0)), 4, "0");
         SubsflControlProps_962( ) ;
         nGXsfl_96_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_962( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(2, new Object[]{ new Object[]{
                                                 AV91WWAlertaDS_2_Dynamicfiltersselector1 ,
                                                 AV92WWAlertaDS_3_Alerta_inicio1 ,
                                                 AV93WWAlertaDS_4_Alerta_inicio_to1 ,
                                                 AV94WWAlertaDS_5_Dynamicfiltersenabled2 ,
                                                 AV95WWAlertaDS_6_Dynamicfiltersselector2 ,
                                                 AV96WWAlertaDS_7_Alerta_inicio2 ,
                                                 AV97WWAlertaDS_8_Alerta_inicio_to2 ,
                                                 AV98WWAlertaDS_9_Dynamicfiltersenabled3 ,
                                                 AV99WWAlertaDS_10_Dynamicfiltersselector3 ,
                                                 AV100WWAlertaDS_11_Alerta_inicio3 ,
                                                 AV101WWAlertaDS_12_Alerta_inicio_to3 ,
                                                 A1885Alerta_Inicio ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A1887Alerta_Owner ,
                                                 AV2WWPContext.gxTpr_Userid },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT
                                                 }
            });
            /* Using cursor H00OO5 */
            pr_default.execute(2, new Object[] {AV2WWPContext.gxTpr_Userid, AV92WWAlertaDS_3_Alerta_inicio1, AV93WWAlertaDS_4_Alerta_inicio_to1, AV96WWAlertaDS_7_Alerta_inicio2, AV97WWAlertaDS_8_Alerta_inicio_to2, AV100WWAlertaDS_11_Alerta_inicio3, AV101WWAlertaDS_12_Alerta_inicio_to3, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_96_idx = 1;
            while ( ( (pr_default.getStatus(2) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1888Alerta_Cadastro = H00OO5_A1888Alerta_Cadastro[0];
               A1887Alerta_Owner = H00OO5_A1887Alerta_Owner[0];
               A1886Alerta_Fim = H00OO5_A1886Alerta_Fim[0];
               n1886Alerta_Fim = H00OO5_n1886Alerta_Fim[0];
               A1885Alerta_Inicio = H00OO5_A1885Alerta_Inicio[0];
               A1884Alerta_ToGroup = H00OO5_A1884Alerta_ToGroup[0];
               n1884Alerta_ToGroup = H00OO5_n1884Alerta_ToGroup[0];
               A1883Alerta_ToUser = H00OO5_A1883Alerta_ToUser[0];
               n1883Alerta_ToUser = H00OO5_n1883Alerta_ToUser[0];
               A1882Alerta_ToAreaTrabalho = H00OO5_A1882Alerta_ToAreaTrabalho[0];
               n1882Alerta_ToAreaTrabalho = H00OO5_n1882Alerta_ToAreaTrabalho[0];
               A1881Alerta_Codigo = H00OO5_A1881Alerta_Codigo[0];
               A40000AreaTrabalho_Descricao = H00OO5_A40000AreaTrabalho_Descricao[0];
               n40000AreaTrabalho_Descricao = H00OO5_n40000AreaTrabalho_Descricao[0];
               A40000AreaTrabalho_Descricao = H00OO5_A40000AreaTrabalho_Descricao[0];
               n40000AreaTrabalho_Descricao = H00OO5_n40000AreaTrabalho_Descricao[0];
               /* Execute user event: E25OO2 */
               E25OO2 ();
               pr_default.readNext(2);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(2) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(2);
            wbEnd = 96;
            WBOO0( ) ;
         }
         nGXsfl_96_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV90WWAlertaDS_1_Alerta_owner = AV81Alerta_Owner;
         AV91WWAlertaDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV92WWAlertaDS_3_Alerta_inicio1 = AV82Alerta_Inicio1;
         AV93WWAlertaDS_4_Alerta_inicio_to1 = AV83Alerta_Inicio_To1;
         AV94WWAlertaDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV95WWAlertaDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV96WWAlertaDS_7_Alerta_inicio2 = AV84Alerta_Inicio2;
         AV97WWAlertaDS_8_Alerta_inicio_to2 = AV85Alerta_Inicio_To2;
         AV98WWAlertaDS_9_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV99WWAlertaDS_10_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV100WWAlertaDS_11_Alerta_inicio3 = AV86Alerta_Inicio3;
         AV101WWAlertaDS_12_Alerta_inicio_to3 = AV87Alerta_Inicio_To3;
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV91WWAlertaDS_2_Dynamicfiltersselector1 ,
                                              AV92WWAlertaDS_3_Alerta_inicio1 ,
                                              AV93WWAlertaDS_4_Alerta_inicio_to1 ,
                                              AV94WWAlertaDS_5_Dynamicfiltersenabled2 ,
                                              AV95WWAlertaDS_6_Dynamicfiltersselector2 ,
                                              AV96WWAlertaDS_7_Alerta_inicio2 ,
                                              AV97WWAlertaDS_8_Alerta_inicio_to2 ,
                                              AV98WWAlertaDS_9_Dynamicfiltersenabled3 ,
                                              AV99WWAlertaDS_10_Dynamicfiltersselector3 ,
                                              AV100WWAlertaDS_11_Alerta_inicio3 ,
                                              AV101WWAlertaDS_12_Alerta_inicio_to3 ,
                                              A1885Alerta_Inicio ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A1887Alerta_Owner ,
                                              AV2WWPContext.gxTpr_Userid },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT
                                              }
         });
         /* Using cursor H00OO7 */
         pr_default.execute(3, new Object[] {AV2WWPContext.gxTpr_Userid, AV92WWAlertaDS_3_Alerta_inicio1, AV93WWAlertaDS_4_Alerta_inicio_to1, AV96WWAlertaDS_7_Alerta_inicio2, AV97WWAlertaDS_8_Alerta_inicio_to2, AV100WWAlertaDS_11_Alerta_inicio3, AV101WWAlertaDS_12_Alerta_inicio_to3});
         GRID_nRecordCount = H00OO7_AGRID_nRecordCount[0];
         pr_default.close(3);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV90WWAlertaDS_1_Alerta_owner = AV81Alerta_Owner;
         AV91WWAlertaDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV92WWAlertaDS_3_Alerta_inicio1 = AV82Alerta_Inicio1;
         AV93WWAlertaDS_4_Alerta_inicio_to1 = AV83Alerta_Inicio_To1;
         AV94WWAlertaDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV95WWAlertaDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV96WWAlertaDS_7_Alerta_inicio2 = AV84Alerta_Inicio2;
         AV97WWAlertaDS_8_Alerta_inicio_to2 = AV85Alerta_Inicio_To2;
         AV98WWAlertaDS_9_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV99WWAlertaDS_10_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV100WWAlertaDS_11_Alerta_inicio3 = AV86Alerta_Inicio3;
         AV101WWAlertaDS_12_Alerta_inicio_to3 = AV87Alerta_Inicio_To3;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV82Alerta_Inicio1, AV83Alerta_Inicio_To1, AV19DynamicFiltersSelector2, AV84Alerta_Inicio2, AV85Alerta_Inicio_To2, AV23DynamicFiltersSelector3, AV86Alerta_Inicio3, AV87Alerta_Inicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV2WWPContext, AV32ManageFiltersExecutionStep, AV81Alerta_Owner, AV105Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1881Alerta_Codigo, A1882Alerta_ToAreaTrabalho) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV90WWAlertaDS_1_Alerta_owner = AV81Alerta_Owner;
         AV91WWAlertaDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV92WWAlertaDS_3_Alerta_inicio1 = AV82Alerta_Inicio1;
         AV93WWAlertaDS_4_Alerta_inicio_to1 = AV83Alerta_Inicio_To1;
         AV94WWAlertaDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV95WWAlertaDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV96WWAlertaDS_7_Alerta_inicio2 = AV84Alerta_Inicio2;
         AV97WWAlertaDS_8_Alerta_inicio_to2 = AV85Alerta_Inicio_To2;
         AV98WWAlertaDS_9_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV99WWAlertaDS_10_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV100WWAlertaDS_11_Alerta_inicio3 = AV86Alerta_Inicio3;
         AV101WWAlertaDS_12_Alerta_inicio_to3 = AV87Alerta_Inicio_To3;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV82Alerta_Inicio1, AV83Alerta_Inicio_To1, AV19DynamicFiltersSelector2, AV84Alerta_Inicio2, AV85Alerta_Inicio_To2, AV23DynamicFiltersSelector3, AV86Alerta_Inicio3, AV87Alerta_Inicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV2WWPContext, AV32ManageFiltersExecutionStep, AV81Alerta_Owner, AV105Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1881Alerta_Codigo, A1882Alerta_ToAreaTrabalho) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV90WWAlertaDS_1_Alerta_owner = AV81Alerta_Owner;
         AV91WWAlertaDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV92WWAlertaDS_3_Alerta_inicio1 = AV82Alerta_Inicio1;
         AV93WWAlertaDS_4_Alerta_inicio_to1 = AV83Alerta_Inicio_To1;
         AV94WWAlertaDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV95WWAlertaDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV96WWAlertaDS_7_Alerta_inicio2 = AV84Alerta_Inicio2;
         AV97WWAlertaDS_8_Alerta_inicio_to2 = AV85Alerta_Inicio_To2;
         AV98WWAlertaDS_9_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV99WWAlertaDS_10_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV100WWAlertaDS_11_Alerta_inicio3 = AV86Alerta_Inicio3;
         AV101WWAlertaDS_12_Alerta_inicio_to3 = AV87Alerta_Inicio_To3;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV82Alerta_Inicio1, AV83Alerta_Inicio_To1, AV19DynamicFiltersSelector2, AV84Alerta_Inicio2, AV85Alerta_Inicio_To2, AV23DynamicFiltersSelector3, AV86Alerta_Inicio3, AV87Alerta_Inicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV2WWPContext, AV32ManageFiltersExecutionStep, AV81Alerta_Owner, AV105Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1881Alerta_Codigo, A1882Alerta_ToAreaTrabalho) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV90WWAlertaDS_1_Alerta_owner = AV81Alerta_Owner;
         AV91WWAlertaDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV92WWAlertaDS_3_Alerta_inicio1 = AV82Alerta_Inicio1;
         AV93WWAlertaDS_4_Alerta_inicio_to1 = AV83Alerta_Inicio_To1;
         AV94WWAlertaDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV95WWAlertaDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV96WWAlertaDS_7_Alerta_inicio2 = AV84Alerta_Inicio2;
         AV97WWAlertaDS_8_Alerta_inicio_to2 = AV85Alerta_Inicio_To2;
         AV98WWAlertaDS_9_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV99WWAlertaDS_10_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV100WWAlertaDS_11_Alerta_inicio3 = AV86Alerta_Inicio3;
         AV101WWAlertaDS_12_Alerta_inicio_to3 = AV87Alerta_Inicio_To3;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV82Alerta_Inicio1, AV83Alerta_Inicio_To1, AV19DynamicFiltersSelector2, AV84Alerta_Inicio2, AV85Alerta_Inicio_To2, AV23DynamicFiltersSelector3, AV86Alerta_Inicio3, AV87Alerta_Inicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV2WWPContext, AV32ManageFiltersExecutionStep, AV81Alerta_Owner, AV105Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1881Alerta_Codigo, A1882Alerta_ToAreaTrabalho) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV90WWAlertaDS_1_Alerta_owner = AV81Alerta_Owner;
         AV91WWAlertaDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV92WWAlertaDS_3_Alerta_inicio1 = AV82Alerta_Inicio1;
         AV93WWAlertaDS_4_Alerta_inicio_to1 = AV83Alerta_Inicio_To1;
         AV94WWAlertaDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV95WWAlertaDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV96WWAlertaDS_7_Alerta_inicio2 = AV84Alerta_Inicio2;
         AV97WWAlertaDS_8_Alerta_inicio_to2 = AV85Alerta_Inicio_To2;
         AV98WWAlertaDS_9_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV99WWAlertaDS_10_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV100WWAlertaDS_11_Alerta_inicio3 = AV86Alerta_Inicio3;
         AV101WWAlertaDS_12_Alerta_inicio_to3 = AV87Alerta_Inicio_To3;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV82Alerta_Inicio1, AV83Alerta_Inicio_To1, AV19DynamicFiltersSelector2, AV84Alerta_Inicio2, AV85Alerta_Inicio_To2, AV23DynamicFiltersSelector3, AV86Alerta_Inicio3, AV87Alerta_Inicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV2WWPContext, AV32ManageFiltersExecutionStep, AV81Alerta_Owner, AV105Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1881Alerta_Codigo, A1882Alerta_ToAreaTrabalho) ;
         }
         return (int)(0) ;
      }

      protected void STRUPOO0( )
      {
         /* Before Start, stand alone formulas. */
         AV105Pgmname = "WWAlerta";
         context.Gx_err = 0;
         edtavAreatrabalho_descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAreatrabalho_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreatrabalho_descricao_Enabled), 5, 0)));
         GXAALERTA_TOUSER_htmlOO2( ) ;
         GXAALERTA_OWNER_htmlOO2( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E23OO2 */
         E23OO2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vMANAGEFILTERSDATA"), AV36ManageFiltersData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavAlerta_owner_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavAlerta_owner_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vALERTA_OWNER");
               GX_FocusControl = edtavAlerta_owner_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV81Alerta_Owner = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81Alerta_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81Alerta_Owner), 6, 0)));
            }
            else
            {
               AV81Alerta_Owner = (int)(context.localUtil.CToN( cgiGet( edtavAlerta_owner_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81Alerta_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81Alerta_Owner), 6, 0)));
            }
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( context.localUtil.VCDateTime( cgiGet( edtavAlerta_inicio1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Alerta_Inicio1"}), 1, "vALERTA_INICIO1");
               GX_FocusControl = edtavAlerta_inicio1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV82Alerta_Inicio1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82Alerta_Inicio1", context.localUtil.Format(AV82Alerta_Inicio1, "99/99/99"));
            }
            else
            {
               AV82Alerta_Inicio1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavAlerta_inicio1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82Alerta_Inicio1", context.localUtil.Format(AV82Alerta_Inicio1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavAlerta_inicio_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Alerta_Inicio_To1"}), 1, "vALERTA_INICIO_TO1");
               GX_FocusControl = edtavAlerta_inicio_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV83Alerta_Inicio_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83Alerta_Inicio_To1", context.localUtil.Format(AV83Alerta_Inicio_To1, "99/99/99"));
            }
            else
            {
               AV83Alerta_Inicio_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavAlerta_inicio_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83Alerta_Inicio_To1", context.localUtil.Format(AV83Alerta_Inicio_To1, "99/99/99"));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            if ( context.localUtil.VCDateTime( cgiGet( edtavAlerta_inicio2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Alerta_Inicio2"}), 1, "vALERTA_INICIO2");
               GX_FocusControl = edtavAlerta_inicio2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV84Alerta_Inicio2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84Alerta_Inicio2", context.localUtil.Format(AV84Alerta_Inicio2, "99/99/99"));
            }
            else
            {
               AV84Alerta_Inicio2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavAlerta_inicio2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84Alerta_Inicio2", context.localUtil.Format(AV84Alerta_Inicio2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavAlerta_inicio_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Alerta_Inicio_To2"}), 1, "vALERTA_INICIO_TO2");
               GX_FocusControl = edtavAlerta_inicio_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV85Alerta_Inicio_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85Alerta_Inicio_To2", context.localUtil.Format(AV85Alerta_Inicio_To2, "99/99/99"));
            }
            else
            {
               AV85Alerta_Inicio_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavAlerta_inicio_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85Alerta_Inicio_To2", context.localUtil.Format(AV85Alerta_Inicio_To2, "99/99/99"));
            }
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            if ( context.localUtil.VCDateTime( cgiGet( edtavAlerta_inicio3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Alerta_Inicio3"}), 1, "vALERTA_INICIO3");
               GX_FocusControl = edtavAlerta_inicio3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV86Alerta_Inicio3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86Alerta_Inicio3", context.localUtil.Format(AV86Alerta_Inicio3, "99/99/99"));
            }
            else
            {
               AV86Alerta_Inicio3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavAlerta_inicio3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86Alerta_Inicio3", context.localUtil.Format(AV86Alerta_Inicio3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavAlerta_inicio_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Alerta_Inicio_To3"}), 1, "vALERTA_INICIO_TO3");
               GX_FocusControl = edtavAlerta_inicio_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV87Alerta_Inicio_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87Alerta_Inicio_To3", context.localUtil.Format(AV87Alerta_Inicio_To3, "99/99/99"));
            }
            else
            {
               AV87Alerta_Inicio_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavAlerta_inicio_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87Alerta_Inicio_To3", context.localUtil.Format(AV87Alerta_Inicio_To3, "99/99/99"));
            }
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vMANAGEFILTERSEXECUTIONSTEP");
               GX_FocusControl = edtavManagefiltersexecutionstep_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32ManageFiltersExecutionStep = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV32ManageFiltersExecutionStep), 1, 0));
            }
            else
            {
               AV32ManageFiltersExecutionStep = (short)(context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV32ManageFiltersExecutionStep), 1, 0));
            }
            /* Read saved values. */
            nRC_GXsfl_96 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_96"), ",", "."));
            AV74GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV75GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_managefilters_Icon = cgiGet( "DDO_MANAGEFILTERS_Icon");
            Ddo_managefilters_Caption = cgiGet( "DDO_MANAGEFILTERS_Caption");
            Ddo_managefilters_Tooltip = cgiGet( "DDO_MANAGEFILTERS_Tooltip");
            Ddo_managefilters_Cls = cgiGet( "DDO_MANAGEFILTERS_Cls");
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_managefilters_Activeeventkey = cgiGet( "DDO_MANAGEFILTERS_Activeeventkey");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vALERTA_INICIO1"), 0) != AV82Alerta_Inicio1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vALERTA_INICIO_TO1"), 0) != AV83Alerta_Inicio_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vALERTA_INICIO2"), 0) != AV84Alerta_Inicio2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vALERTA_INICIO_TO2"), 0) != AV85Alerta_Inicio_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vALERTA_INICIO3"), 0) != AV86Alerta_Inicio3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vALERTA_INICIO_TO3"), 0) != AV87Alerta_Inicio_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E23OO2 */
         E23OO2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E23OO2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV2WWPContext) ;
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         if ( StringUtil.StrCmp(AV7HTTPRequest.Method, "GET") == 0 )
         {
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            edtavManagefiltersexecutionstep_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavManagefiltersexecutionstep_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavManagefiltersexecutionstep_Visible), 5, 0)));
         }
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "ALERTA_INICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "ALERTA_INICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "ALERTA_INICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         Form.Caption = " Alertas";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Usu�rio", 0);
         cmbavOrderedby.addItem("2", "Grupo", 0);
         cmbavOrderedby.addItem("3", "Inicio", 0);
         cmbavOrderedby.addItem("4", "Fim", 0);
         cmbavOrderedby.addItem("5", "Owner", 0);
         cmbavOrderedby.addItem("6", "Cadastro", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         Ddo_managefilters_Icon = context.convertURL( (String)(context.GetImagePath( "5efb9e2b-46db-43f4-8f74-dd3f5818d30e", "", context.GetTheme( ))));
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "Icon", Ddo_managefilters_Icon);
      }

      protected void E24OO2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV2WWPContext) ;
         if ( AV32ManageFiltersExecutionStep == 1 )
         {
            AV32ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV32ManageFiltersExecutionStep), 1, 0));
         }
         else if ( AV32ManageFiltersExecutionStep == 2 )
         {
            AV32ManageFiltersExecutionStep = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV32ManageFiltersExecutionStep), 1, 0));
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         dynAlerta_ToUser_Titleformat = 2;
         dynAlerta_ToUser.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 1);' >%5</span>", ((AV13OrderedBy==1) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Usu�rio", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynAlerta_ToUser_Internalname, "Title", dynAlerta_ToUser.Title.Text);
         cmbAlerta_ToGroup_Titleformat = 2;
         cmbAlerta_ToGroup.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV13OrderedBy==2) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Grupo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAlerta_ToGroup_Internalname, "Title", cmbAlerta_ToGroup.Title.Text);
         edtAlerta_Inicio_Titleformat = 2;
         edtAlerta_Inicio_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV13OrderedBy==3) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Inicio", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAlerta_Inicio_Internalname, "Title", edtAlerta_Inicio_Title);
         edtAlerta_Fim_Titleformat = 2;
         edtAlerta_Fim_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV13OrderedBy==4) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Fim", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAlerta_Fim_Internalname, "Title", edtAlerta_Fim_Title);
         dynAlerta_Owner_Titleformat = 2;
         dynAlerta_Owner.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 5);' >%5</span>", ((AV13OrderedBy==5) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Owner", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynAlerta_Owner_Internalname, "Title", dynAlerta_Owner.Title.Text);
         edtAlerta_Cadastro_Titleformat = 2;
         edtAlerta_Cadastro_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 6);' >%5</span>", ((AV13OrderedBy==6) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Cadastro", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAlerta_Cadastro_Internalname, "Title", edtAlerta_Cadastro_Title);
         AV74GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV74GridCurrentPage), 10, 0)));
         AV75GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV75GridPageCount), 10, 0)));
         imgInsert_Visible = (AV2WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Visible), 5, 0)));
         edtavUpdate_Visible = (AV2WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV2WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         AV90WWAlertaDS_1_Alerta_owner = AV81Alerta_Owner;
         AV91WWAlertaDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV92WWAlertaDS_3_Alerta_inicio1 = AV82Alerta_Inicio1;
         AV93WWAlertaDS_4_Alerta_inicio_to1 = AV83Alerta_Inicio_To1;
         AV94WWAlertaDS_5_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV95WWAlertaDS_6_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV96WWAlertaDS_7_Alerta_inicio2 = AV84Alerta_Inicio2;
         AV97WWAlertaDS_8_Alerta_inicio_to2 = AV85Alerta_Inicio_To2;
         AV98WWAlertaDS_9_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV99WWAlertaDS_10_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV100WWAlertaDS_11_Alerta_inicio3 = AV86Alerta_Inicio3;
         AV101WWAlertaDS_12_Alerta_inicio_to3 = AV87Alerta_Inicio_To3;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV2WWPContext", AV2WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36ManageFiltersData", AV36ManageFiltersData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E12OO2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV73PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV73PageToGo) ;
         }
      }

      private void E25OO2( )
      {
         /* Grid_Load Routine */
         AV28Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
         AV102Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("alerta.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1881Alerta_Codigo);
         AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
         AV103Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("alerta.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1881Alerta_Codigo);
         AV30Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV30Display);
         AV104Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = formatLink("viewalerta.aspx") + "?" + UrlEncode("" +A1881Alerta_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         if ( A1882Alerta_ToAreaTrabalho > 0 )
         {
            AV80AreaTrabalho_Descricao = A40000AreaTrabalho_Descricao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAreatrabalho_descricao_Internalname, AV80AreaTrabalho_Descricao);
         }
         else
         {
            AV80AreaTrabalho_Descricao = "(Indiferente)";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAreatrabalho_descricao_Internalname, AV80AreaTrabalho_Descricao);
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 96;
         }
         sendrow_962( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_96_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(96, GridRow);
         }
      }

      protected void E13OO2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E18OO2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E14OO2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV82Alerta_Inicio1, AV83Alerta_Inicio_To1, AV19DynamicFiltersSelector2, AV84Alerta_Inicio2, AV85Alerta_Inicio_To2, AV23DynamicFiltersSelector3, AV86Alerta_Inicio3, AV87Alerta_Inicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV2WWPContext, AV32ManageFiltersExecutionStep, AV81Alerta_Owner, AV105Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1881Alerta_Codigo, A1882Alerta_ToAreaTrabalho) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E19OO2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E20OO2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E15OO2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV82Alerta_Inicio1, AV83Alerta_Inicio_To1, AV19DynamicFiltersSelector2, AV84Alerta_Inicio2, AV85Alerta_Inicio_To2, AV23DynamicFiltersSelector3, AV86Alerta_Inicio3, AV87Alerta_Inicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV2WWPContext, AV32ManageFiltersExecutionStep, AV81Alerta_Owner, AV105Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1881Alerta_Codigo, A1882Alerta_ToAreaTrabalho) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E21OO2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E16OO2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV82Alerta_Inicio1, AV83Alerta_Inicio_To1, AV19DynamicFiltersSelector2, AV84Alerta_Inicio2, AV85Alerta_Inicio_To2, AV23DynamicFiltersSelector3, AV86Alerta_Inicio3, AV87Alerta_Inicio_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV2WWPContext, AV32ManageFiltersExecutionStep, AV81Alerta_Owner, AV105Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1881Alerta_Codigo, A1882Alerta_ToAreaTrabalho) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E22OO2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11OO2( )
      {
         /* Ddo_managefilters_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Clean#>") == 0 )
         {
            /* Execute user subroutine: 'CLEANFILTERS' */
            S212 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Save#>") == 0 )
         {
            /* Execute user subroutine: 'SAVEGRIDSTATE' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            context.PopUp(formatLink("wwpbaseobjects.savefilteras.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWAlertaFilters")) + "," + UrlEncode(StringUtil.RTrim(AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3"))), new Object[] {});
            AV32ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV32ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Manage#>") == 0 )
         {
            context.PopUp(formatLink("wwpbaseobjects.managefilters.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWAlertaFilters")), new Object[] {});
            AV32ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV32ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else
         {
            GXt_char1 = AV33ManageFiltersXml;
            new wwpbaseobjects.getfilterbyname(context ).execute(  "WWAlertaFilters",  Ddo_managefilters_Activeeventkey, out  GXt_char1) ;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "ActiveEventKey", Ddo_managefilters_Activeeventkey);
            AV33ManageFiltersXml = GXt_char1;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV33ManageFiltersXml)) )
            {
               GX_msglist.addItem("O filtro selecionado n�o existe mais.");
            }
            else
            {
               /* Execute user subroutine: 'CLEANFILTERS' */
               S212 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               new wwpbaseobjects.savegridstate(context ).execute(  AV105Pgmname+"GridState",  AV33ManageFiltersXml) ;
               AV10GridState.FromXml(AV33ManageFiltersXml, "");
               AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
               S222 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
               S202 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               subgrid_firstpage( ) ;
               context.DoAjaxRefresh();
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void E17OO2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("alerta.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         tblTablemergeddynamicfiltersalerta_inicio1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersalerta_inicio1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersalerta_inicio1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ALERTA_INICIO") == 0 )
         {
            tblTablemergeddynamicfiltersalerta_inicio1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersalerta_inicio1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersalerta_inicio1_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         tblTablemergeddynamicfiltersalerta_inicio2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersalerta_inicio2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersalerta_inicio2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "ALERTA_INICIO") == 0 )
         {
            tblTablemergeddynamicfiltersalerta_inicio2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersalerta_inicio2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersalerta_inicio2_Visible), 5, 0)));
         }
      }

      protected void S142( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         tblTablemergeddynamicfiltersalerta_inicio3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersalerta_inicio3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersalerta_inicio3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "ALERTA_INICIO") == 0 )
         {
            tblTablemergeddynamicfiltersalerta_inicio3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersalerta_inicio3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersalerta_inicio3_Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "ALERTA_INICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV84Alerta_Inicio2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84Alerta_Inicio2", context.localUtil.Format(AV84Alerta_Inicio2, "99/99/99"));
         AV85Alerta_Inicio_To2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85Alerta_Inicio_To2", context.localUtil.Format(AV85Alerta_Inicio_To2, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "ALERTA_INICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV86Alerta_Inicio3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86Alerta_Inicio3", context.localUtil.Format(AV86Alerta_Inicio3, "99/99/99"));
         AV87Alerta_Inicio_To3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87Alerta_Inicio_To3", context.localUtil.Format(AV87Alerta_Inicio_To3, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S112( )
      {
         /* 'LOADSAVEDFILTERS' Routine */
         AV36ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV37ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV37ManageFiltersDataItem.gxTpr_Title = "Limpar filtros";
         AV37ManageFiltersDataItem.gxTpr_Eventkey = "<#Clean#>";
         AV37ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV37ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "63d2ae92-4e43-4a70-af61-0943e39ea422", "", context.GetTheme( ))));
         AV37ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
         AV36ManageFiltersData.Add(AV37ManageFiltersDataItem, 0);
         AV37ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV37ManageFiltersDataItem.gxTpr_Title = "Salvar filtro como...";
         AV37ManageFiltersDataItem.gxTpr_Eventkey = "<#Save#>";
         AV37ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV37ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "6eee63e8-73c7-4738-beee-f98e3a8d2841", "", context.GetTheme( ))));
         AV36ManageFiltersData.Add(AV37ManageFiltersDataItem, 0);
         AV37ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV37ManageFiltersDataItem.gxTpr_Isdivider = true;
         AV36ManageFiltersData.Add(AV37ManageFiltersDataItem, 0);
         AV34ManageFiltersItems.FromXml(new wwpbaseobjects.loadmanagefiltersstate(context).executeUdp(  "WWAlertaFilters"), "");
         AV106GXV1 = 1;
         while ( AV106GXV1 <= AV34ManageFiltersItems.Count )
         {
            AV35ManageFiltersItem = ((wwpbaseobjects.SdtGridStateCollection_Item)AV34ManageFiltersItems.Item(AV106GXV1));
            AV37ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV37ManageFiltersDataItem.gxTpr_Title = AV35ManageFiltersItem.gxTpr_Title;
            AV37ManageFiltersDataItem.gxTpr_Eventkey = AV35ManageFiltersItem.gxTpr_Title;
            AV37ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV37ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
            AV36ManageFiltersData.Add(AV37ManageFiltersDataItem, 0);
            if ( AV36ManageFiltersData.Count == 13 )
            {
               if (true) break;
            }
            AV106GXV1 = (int)(AV106GXV1+1);
         }
         if ( AV36ManageFiltersData.Count > 3 )
         {
            AV37ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV37ManageFiltersDataItem.gxTpr_Isdivider = true;
            AV36ManageFiltersData.Add(AV37ManageFiltersDataItem, 0);
            AV37ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV37ManageFiltersDataItem.gxTpr_Title = "Gerenciar filtros";
            AV37ManageFiltersDataItem.gxTpr_Eventkey = "<#Manage#>";
            AV37ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV37ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "653f6166-5d82-407a-af84-19e0dde65efd", "", context.GetTheme( ))));
            AV37ManageFiltersDataItem.gxTpr_Jsonclickevent = "";
            AV36ManageFiltersData.Add(AV37ManageFiltersDataItem, 0);
         }
      }

      protected void S212( )
      {
         /* 'CLEANFILTERS' Routine */
         AV81Alerta_Owner = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81Alerta_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81Alerta_Owner), 6, 0)));
         AV15DynamicFiltersSelector1 = "ALERTA_INICIO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV82Alerta_Inicio1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82Alerta_Inicio1", context.localUtil.Format(AV82Alerta_Inicio1, "99/99/99"));
         AV83Alerta_Inicio_To1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83Alerta_Inicio_To1", context.localUtil.Format(AV83Alerta_Inicio_To1, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S162( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV31Session.Get(AV105Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV105Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV31Session.Get(AV105Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S222( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV107GXV2 = 1;
         while ( AV107GXV2 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV107GXV2));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "ALERTA_OWNER") == 0 )
            {
               AV81Alerta_Owner = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81Alerta_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81Alerta_Owner), 6, 0)));
            }
            AV107GXV2 = (int)(AV107GXV2+1);
         }
      }

      protected void S202( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ALERTA_INICIO") == 0 )
            {
               AV82Alerta_Inicio1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82Alerta_Inicio1", context.localUtil.Format(AV82Alerta_Inicio1, "99/99/99"));
               AV83Alerta_Inicio_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83Alerta_Inicio_To1", context.localUtil.Format(AV83Alerta_Inicio_To1, "99/99/99"));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "ALERTA_INICIO") == 0 )
               {
                  AV84Alerta_Inicio2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84Alerta_Inicio2", context.localUtil.Format(AV84Alerta_Inicio2, "99/99/99"));
                  AV85Alerta_Inicio_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85Alerta_Inicio_To2", context.localUtil.Format(AV85Alerta_Inicio_To2, "99/99/99"));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S132 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "ALERTA_INICIO") == 0 )
                  {
                     AV86Alerta_Inicio3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86Alerta_Inicio3", context.localUtil.Format(AV86Alerta_Inicio3, "99/99/99"));
                     AV87Alerta_Inicio_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87Alerta_Inicio_To3", context.localUtil.Format(AV87Alerta_Inicio_To3, "99/99/99"));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S142 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV31Session.Get(AV105Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV81Alerta_Owner) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "ALERTA_OWNER";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV81Alerta_Owner), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV105Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ALERTA_INICIO") == 0 ) && ! ( (DateTime.MinValue==AV82Alerta_Inicio1) && (DateTime.MinValue==AV83Alerta_Inicio_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV82Alerta_Inicio1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV83Alerta_Inicio_To1, 2, "/");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "ALERTA_INICIO") == 0 ) && ! ( (DateTime.MinValue==AV84Alerta_Inicio2) && (DateTime.MinValue==AV85Alerta_Inicio_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV84Alerta_Inicio2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV85Alerta_Inicio_To2, 2, "/");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "ALERTA_INICIO") == 0 ) && ! ( (DateTime.MinValue==AV86Alerta_Inicio3) && (DateTime.MinValue==AV87Alerta_Inicio_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV86Alerta_Inicio3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV87Alerta_Inicio_To3, 2, "/");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S152( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV105Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Alerta";
         AV31Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_OO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_OO2( true) ;
         }
         else
         {
            wb_table2_8_OO2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_OO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_90_OO2( true) ;
         }
         else
         {
            wb_table3_90_OO2( false) ;
         }
         return  ;
      }

      protected void wb_table3_90_OO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_OO2e( true) ;
         }
         else
         {
            wb_table1_2_OO2e( false) ;
         }
      }

      protected void wb_table3_90_OO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 5, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table4_93_OO2( true) ;
         }
         else
         {
            wb_table4_93_OO2( false) ;
         }
         return  ;
      }

      protected void wb_table4_93_OO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgInsert_Visible, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAlerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_90_OO2e( true) ;
         }
         else
         {
            wb_table3_90_OO2e( false) ;
         }
      }

      protected void wb_table4_93_OO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"96\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "�rea de Trabalho") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "�rea de Trabalho") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( dynAlerta_ToUser_Titleformat == 0 )
               {
                  context.SendWebValue( dynAlerta_ToUser.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( dynAlerta_ToUser.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbAlerta_ToGroup_Titleformat == 0 )
               {
                  context.SendWebValue( cmbAlerta_ToGroup.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbAlerta_ToGroup.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAlerta_Inicio_Titleformat == 0 )
               {
                  context.SendWebValue( edtAlerta_Inicio_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAlerta_Inicio_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAlerta_Fim_Titleformat == 0 )
               {
                  context.SendWebValue( edtAlerta_Fim_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAlerta_Fim_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( dynAlerta_Owner_Titleformat == 0 )
               {
                  context.SendWebValue( dynAlerta_Owner.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( dynAlerta_Owner.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAlerta_Cadastro_Titleformat == 0 )
               {
                  context.SendWebValue( edtAlerta_Cadastro_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAlerta_Cadastro_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV30Display));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1881Alerta_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1882Alerta_ToAreaTrabalho), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", AV80AreaTrabalho_Descricao);
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAreatrabalho_descricao_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1883Alerta_ToUser), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( dynAlerta_ToUser.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(dynAlerta_ToUser_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1884Alerta_ToGroup), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbAlerta_ToGroup.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbAlerta_ToGroup_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A1885Alerta_Inicio, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAlerta_Inicio_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAlerta_Inicio_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A1886Alerta_Fim, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAlerta_Fim_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAlerta_Fim_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1887Alerta_Owner), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( dynAlerta_Owner.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(dynAlerta_Owner_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A1888Alerta_Cadastro, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAlerta_Cadastro_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAlerta_Cadastro_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 96 )
         {
            wbEnd = 0;
            nRC_GXsfl_96 = (short)(nGXsfl_96_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_93_OO2e( true) ;
         }
         else
         {
            wb_table4_93_OO2e( false) ;
         }
      }

      protected void wb_table2_8_OO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_11_OO2( true) ;
         }
         else
         {
            wb_table5_11_OO2( false) ;
         }
         return  ;
      }

      protected void wb_table5_11_OO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            wb_table6_21_OO2( true) ;
         }
         else
         {
            wb_table6_21_OO2( false) ;
         }
         return  ;
      }

      protected void wb_table6_21_OO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_OO2e( true) ;
         }
         else
         {
            wb_table2_8_OO2e( false) ;
         }
      }

      protected void wb_table6_21_OO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MANAGEFILTERSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextalerta_owner_Internalname, "Owner", "", "", lblFiltertextalerta_owner_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAlerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'" + sGXsfl_96_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAlerta_owner_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV81Alerta_Owner), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV81Alerta_Owner), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAlerta_owner_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAlerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_30_OO2( true) ;
         }
         else
         {
            wb_table7_30_OO2( false) ;
         }
         return  ;
      }

      protected void wb_table7_30_OO2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWAlerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_21_OO2e( true) ;
         }
         else
         {
            wb_table6_21_OO2e( false) ;
         }
      }

      protected void wb_table7_30_OO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAlerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'" + sGXsfl_96_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,35);\"", "", true, "HLP_WWAlerta.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAlerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_39_OO2( true) ;
         }
         else
         {
            wb_table8_39_OO2( false) ;
         }
         return  ;
      }

      protected void wb_table8_39_OO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAlerta.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAlerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAlerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_96_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", "", true, "HLP_WWAlerta.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAlerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_58_OO2( true) ;
         }
         else
         {
            wb_table9_58_OO2( false) ;
         }
         return  ;
      }

      protected void wb_table9_58_OO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAlerta.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAlerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAlerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'" + sGXsfl_96_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,73);\"", "", true, "HLP_WWAlerta.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAlerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_77_OO2( true) ;
         }
         else
         {
            wb_table10_77_OO2( false) ;
         }
         return  ;
      }

      protected void wb_table10_77_OO2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAlerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_30_OO2e( true) ;
         }
         else
         {
            wb_table7_30_OO2e( false) ;
         }
      }

      protected void wb_table10_77_OO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfiltersalerta_inicio3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfiltersalerta_inicio3_Internalname, tblTablemergeddynamicfiltersalerta_inicio3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'" + sGXsfl_96_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavAlerta_inicio3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavAlerta_inicio3_Internalname, context.localUtil.Format(AV86Alerta_Inicio3, "99/99/99"), context.localUtil.Format( AV86Alerta_Inicio3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,80);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAlerta_inicio3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAlerta.htm");
            GxWebStd.gx_bitmap( context, edtavAlerta_inicio3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAlerta.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersalerta_inicio_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfiltersalerta_inicio_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAlerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'" + sGXsfl_96_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavAlerta_inicio_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavAlerta_inicio_to3_Internalname, context.localUtil.Format(AV87Alerta_Inicio_To3, "99/99/99"), context.localUtil.Format( AV87Alerta_Inicio_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,84);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAlerta_inicio_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAlerta.htm");
            GxWebStd.gx_bitmap( context, edtavAlerta_inicio_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAlerta.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_77_OO2e( true) ;
         }
         else
         {
            wb_table10_77_OO2e( false) ;
         }
      }

      protected void wb_table9_58_OO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfiltersalerta_inicio2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfiltersalerta_inicio2_Internalname, tblTablemergeddynamicfiltersalerta_inicio2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_96_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavAlerta_inicio2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavAlerta_inicio2_Internalname, context.localUtil.Format(AV84Alerta_Inicio2, "99/99/99"), context.localUtil.Format( AV84Alerta_Inicio2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAlerta_inicio2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAlerta.htm");
            GxWebStd.gx_bitmap( context, edtavAlerta_inicio2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAlerta.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersalerta_inicio_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfiltersalerta_inicio_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAlerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_96_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavAlerta_inicio_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavAlerta_inicio_to2_Internalname, context.localUtil.Format(AV85Alerta_Inicio_To2, "99/99/99"), context.localUtil.Format( AV85Alerta_Inicio_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,65);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAlerta_inicio_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAlerta.htm");
            GxWebStd.gx_bitmap( context, edtavAlerta_inicio_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAlerta.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_58_OO2e( true) ;
         }
         else
         {
            wb_table9_58_OO2e( false) ;
         }
      }

      protected void wb_table8_39_OO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfiltersalerta_inicio1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfiltersalerta_inicio1_Internalname, tblTablemergeddynamicfiltersalerta_inicio1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_96_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavAlerta_inicio1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavAlerta_inicio1_Internalname, context.localUtil.Format(AV82Alerta_Inicio1, "99/99/99"), context.localUtil.Format( AV82Alerta_Inicio1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAlerta_inicio1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAlerta.htm");
            GxWebStd.gx_bitmap( context, edtavAlerta_inicio1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAlerta.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersalerta_inicio_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfiltersalerta_inicio_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAlerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_96_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavAlerta_inicio_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavAlerta_inicio_to1_Internalname, context.localUtil.Format(AV83Alerta_Inicio_To1, "99/99/99"), context.localUtil.Format( AV83Alerta_Inicio_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAlerta_inicio_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAlerta.htm");
            GxWebStd.gx_bitmap( context, edtavAlerta_inicio_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWAlerta.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_39_OO2e( true) ;
         }
         else
         {
            wb_table8_39_OO2e( false) ;
         }
      }

      protected void wb_table5_11_OO2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAlertatitle_Internalname, "Alertas", "", "", lblAlertatitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWAlerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWAlerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'" + sGXsfl_96_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_WWAlerta.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'" + sGXsfl_96_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWAlerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_11_OO2e( true) ;
         }
         else
         {
            wb_table5_11_OO2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAOO2( ) ;
         WSOO2( ) ;
         WEOO2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299445572");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwalerta.js", "?20205299445572");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_962( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_96_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_96_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_96_idx;
         edtAlerta_Codigo_Internalname = "ALERTA_CODIGO_"+sGXsfl_96_idx;
         edtAlerta_ToAreaTrabalho_Internalname = "ALERTA_TOAREATRABALHO_"+sGXsfl_96_idx;
         edtavAreatrabalho_descricao_Internalname = "vAREATRABALHO_DESCRICAO_"+sGXsfl_96_idx;
         dynAlerta_ToUser_Internalname = "ALERTA_TOUSER_"+sGXsfl_96_idx;
         cmbAlerta_ToGroup_Internalname = "ALERTA_TOGROUP_"+sGXsfl_96_idx;
         edtAlerta_Inicio_Internalname = "ALERTA_INICIO_"+sGXsfl_96_idx;
         edtAlerta_Fim_Internalname = "ALERTA_FIM_"+sGXsfl_96_idx;
         dynAlerta_Owner_Internalname = "ALERTA_OWNER_"+sGXsfl_96_idx;
         edtAlerta_Cadastro_Internalname = "ALERTA_CADASTRO_"+sGXsfl_96_idx;
      }

      protected void SubsflControlProps_fel_962( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_96_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_96_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_96_fel_idx;
         edtAlerta_Codigo_Internalname = "ALERTA_CODIGO_"+sGXsfl_96_fel_idx;
         edtAlerta_ToAreaTrabalho_Internalname = "ALERTA_TOAREATRABALHO_"+sGXsfl_96_fel_idx;
         edtavAreatrabalho_descricao_Internalname = "vAREATRABALHO_DESCRICAO_"+sGXsfl_96_fel_idx;
         dynAlerta_ToUser_Internalname = "ALERTA_TOUSER_"+sGXsfl_96_fel_idx;
         cmbAlerta_ToGroup_Internalname = "ALERTA_TOGROUP_"+sGXsfl_96_fel_idx;
         edtAlerta_Inicio_Internalname = "ALERTA_INICIO_"+sGXsfl_96_fel_idx;
         edtAlerta_Fim_Internalname = "ALERTA_FIM_"+sGXsfl_96_fel_idx;
         dynAlerta_Owner_Internalname = "ALERTA_OWNER_"+sGXsfl_96_fel_idx;
         edtAlerta_Cadastro_Internalname = "ALERTA_CADASTRO_"+sGXsfl_96_fel_idx;
      }

      protected void sendrow_962( )
      {
         SubsflControlProps_962( ) ;
         WBOO0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_96_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_96_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_96_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV102Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV102Update_GXI : context.PathToRelativeUrl( AV28Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV103Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV103Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV30Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV30Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV104Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV30Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV30Display)) ? AV104Display_GXI : context.PathToRelativeUrl( AV30Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV30Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAlerta_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1881Alerta_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1881Alerta_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAlerta_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)96,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAlerta_ToAreaTrabalho_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1882Alerta_ToAreaTrabalho), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1882Alerta_ToAreaTrabalho), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAlerta_ToAreaTrabalho_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)96,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavAreatrabalho_descricao_Internalname,(String)AV80AreaTrabalho_Descricao,StringUtil.RTrim( context.localUtil.Format( AV80AreaTrabalho_Descricao, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavAreatrabalho_descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavAreatrabalho_descricao_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)96,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            GXAALERTA_TOUSER_htmlOO2( ) ;
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_96_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "ALERTA_TOUSER_" + sGXsfl_96_idx;
               dynAlerta_ToUser.Name = GXCCtl;
               dynAlerta_ToUser.WebTags = "";
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)dynAlerta_ToUser,(String)dynAlerta_ToUser_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A1883Alerta_ToUser), 6, 0)),(short)1,(String)dynAlerta_ToUser_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            dynAlerta_ToUser.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1883Alerta_ToUser), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynAlerta_ToUser_Internalname, "Values", (String)(dynAlerta_ToUser.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_96_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "ALERTA_TOGROUP_" + sGXsfl_96_idx;
               cmbAlerta_ToGroup.Name = GXCCtl;
               cmbAlerta_ToGroup.WebTags = "";
               cmbAlerta_ToGroup.addItem("0", "(Nenhum)", 0);
               cmbAlerta_ToGroup.addItem("1", "Todos os usu�rios", 0);
               cmbAlerta_ToGroup.addItem("2", "Todos os usu�rios da Contratante", 0);
               cmbAlerta_ToGroup.addItem("3", "Todos os usu�rios da Contratada", 0);
               cmbAlerta_ToGroup.addItem("4", "Todos os gestores", 0);
               cmbAlerta_ToGroup.addItem("5", "Todos os gestores da Contratante", 0);
               cmbAlerta_ToGroup.addItem("6", "Todos os gestores da Contratada", 0);
               if ( cmbAlerta_ToGroup.ItemCount > 0 )
               {
                  A1884Alerta_ToGroup = (short)(NumberUtil.Val( cmbAlerta_ToGroup.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0))), "."));
                  n1884Alerta_ToGroup = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbAlerta_ToGroup,(String)cmbAlerta_ToGroup_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0)),(short)1,(String)cmbAlerta_ToGroup_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbAlerta_ToGroup.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAlerta_ToGroup_Internalname, "Values", (String)(cmbAlerta_ToGroup.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAlerta_Inicio_Internalname,context.localUtil.Format(A1885Alerta_Inicio, "99/99/99"),context.localUtil.Format( A1885Alerta_Inicio, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAlerta_Inicio_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)96,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAlerta_Fim_Internalname,context.localUtil.Format(A1886Alerta_Fim, "99/99/99"),context.localUtil.Format( A1886Alerta_Fim, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAlerta_Fim_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)96,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GXAALERTA_OWNER_htmlOO2( ) ;
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_96_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "ALERTA_OWNER_" + sGXsfl_96_idx;
               dynAlerta_Owner.Name = GXCCtl;
               dynAlerta_Owner.WebTags = "";
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)dynAlerta_Owner,(String)dynAlerta_Owner_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A1887Alerta_Owner), 6, 0)),(short)1,(String)dynAlerta_Owner_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            dynAlerta_Owner.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1887Alerta_Owner), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynAlerta_Owner_Internalname, "Values", (String)(dynAlerta_Owner.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAlerta_Cadastro_Internalname,context.localUtil.TToC( A1888Alerta_Cadastro, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A1888Alerta_Cadastro, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAlerta_Cadastro_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)96,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_ALERTA_CODIGO"+"_"+sGXsfl_96_idx, GetSecureSignedToken( sGXsfl_96_idx, context.localUtil.Format( (decimal)(A1881Alerta_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_ALERTA_TOAREATRABALHO"+"_"+sGXsfl_96_idx, GetSecureSignedToken( sGXsfl_96_idx, context.localUtil.Format( (decimal)(A1882Alerta_ToAreaTrabalho), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_ALERTA_TOUSER"+"_"+sGXsfl_96_idx, GetSecureSignedToken( sGXsfl_96_idx, context.localUtil.Format( (decimal)(A1883Alerta_ToUser), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_ALERTA_TOGROUP"+"_"+sGXsfl_96_idx, GetSecureSignedToken( sGXsfl_96_idx, context.localUtil.Format( (decimal)(A1884Alerta_ToGroup), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_ALERTA_INICIO"+"_"+sGXsfl_96_idx, GetSecureSignedToken( sGXsfl_96_idx, A1885Alerta_Inicio));
            GxWebStd.gx_hidden_field( context, "gxhash_ALERTA_FIM"+"_"+sGXsfl_96_idx, GetSecureSignedToken( sGXsfl_96_idx, A1886Alerta_Fim));
            GxWebStd.gx_hidden_field( context, "gxhash_ALERTA_OWNER"+"_"+sGXsfl_96_idx, GetSecureSignedToken( sGXsfl_96_idx, context.localUtil.Format( (decimal)(A1887Alerta_Owner), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_ALERTA_CADASTRO"+"_"+sGXsfl_96_idx, GetSecureSignedToken( sGXsfl_96_idx, context.localUtil.Format( A1888Alerta_Cadastro, "99/99/99 99:99")));
            GridContainer.AddRow(GridRow);
            nGXsfl_96_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_96_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_96_idx+1));
            sGXsfl_96_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_96_idx), 4, 0)), 4, "0");
            SubsflControlProps_962( ) ;
         }
         /* End function sendrow_962 */
      }

      protected void init_default_properties( )
      {
         lblAlertatitle_Internalname = "ALERTATITLE";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         tblTableactions_Internalname = "TABLEACTIONS";
         Ddo_managefilters_Internalname = "DDO_MANAGEFILTERS";
         lblFiltertextalerta_owner_Internalname = "FILTERTEXTALERTA_OWNER";
         edtavAlerta_owner_Internalname = "vALERTA_OWNER";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavAlerta_inicio1_Internalname = "vALERTA_INICIO1";
         lblDynamicfiltersalerta_inicio_rangemiddletext1_Internalname = "DYNAMICFILTERSALERTA_INICIO_RANGEMIDDLETEXT1";
         edtavAlerta_inicio_to1_Internalname = "vALERTA_INICIO_TO1";
         tblTablemergeddynamicfiltersalerta_inicio1_Internalname = "TABLEMERGEDDYNAMICFILTERSALERTA_INICIO1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavAlerta_inicio2_Internalname = "vALERTA_INICIO2";
         lblDynamicfiltersalerta_inicio_rangemiddletext2_Internalname = "DYNAMICFILTERSALERTA_INICIO_RANGEMIDDLETEXT2";
         edtavAlerta_inicio_to2_Internalname = "vALERTA_INICIO_TO2";
         tblTablemergeddynamicfiltersalerta_inicio2_Internalname = "TABLEMERGEDDYNAMICFILTERSALERTA_INICIO2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavAlerta_inicio3_Internalname = "vALERTA_INICIO3";
         lblDynamicfiltersalerta_inicio_rangemiddletext3_Internalname = "DYNAMICFILTERSALERTA_INICIO_RANGEMIDDLETEXT3";
         edtavAlerta_inicio_to3_Internalname = "vALERTA_INICIO_TO3";
         tblTablemergeddynamicfiltersalerta_inicio3_Internalname = "TABLEMERGEDDYNAMICFILTERSALERTA_INICIO3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtAlerta_Codigo_Internalname = "ALERTA_CODIGO";
         edtAlerta_ToAreaTrabalho_Internalname = "ALERTA_TOAREATRABALHO";
         edtavAreatrabalho_descricao_Internalname = "vAREATRABALHO_DESCRICAO";
         dynAlerta_ToUser_Internalname = "ALERTA_TOUSER";
         cmbAlerta_ToGroup_Internalname = "ALERTA_TOGROUP";
         edtAlerta_Inicio_Internalname = "ALERTA_INICIO";
         edtAlerta_Fim_Internalname = "ALERTA_FIM";
         dynAlerta_Owner_Internalname = "ALERTA_OWNER";
         edtAlerta_Cadastro_Internalname = "ALERTA_CADASTRO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         imgInsert_Internalname = "INSERT";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavManagefiltersexecutionstep_Internalname = "vMANAGEFILTERSEXECUTIONSTEP";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtAlerta_Cadastro_Jsonclick = "";
         dynAlerta_Owner_Jsonclick = "";
         edtAlerta_Fim_Jsonclick = "";
         edtAlerta_Inicio_Jsonclick = "";
         cmbAlerta_ToGroup_Jsonclick = "";
         dynAlerta_ToUser_Jsonclick = "";
         edtavAreatrabalho_descricao_Jsonclick = "";
         edtAlerta_ToAreaTrabalho_Jsonclick = "";
         edtAlerta_Codigo_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         edtavAlerta_inicio_to1_Jsonclick = "";
         edtavAlerta_inicio1_Jsonclick = "";
         edtavAlerta_inicio_to2_Jsonclick = "";
         edtavAlerta_inicio2_Jsonclick = "";
         edtavAlerta_inicio_to3_Jsonclick = "";
         edtavAlerta_inicio3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavAlerta_owner_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavAreatrabalho_descricao_Enabled = 0;
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtAlerta_Cadastro_Titleformat = 0;
         dynAlerta_Owner_Titleformat = 0;
         edtAlerta_Fim_Titleformat = 0;
         edtAlerta_Inicio_Titleformat = 0;
         cmbAlerta_ToGroup_Titleformat = 0;
         dynAlerta_ToUser_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         imgInsert_Visible = 1;
         tblTablemergeddynamicfiltersalerta_inicio3_Visible = 1;
         tblTablemergeddynamicfiltersalerta_inicio2_Visible = 1;
         tblTablemergeddynamicfiltersalerta_inicio1_Visible = 1;
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         edtAlerta_Cadastro_Title = "Cadastro";
         dynAlerta_Owner.Title.Text = "Owner";
         edtAlerta_Fim_Title = "Fim";
         edtAlerta_Inicio_Title = "Inicio";
         cmbAlerta_ToGroup.Title.Text = "Grupo";
         dynAlerta_ToUser.Title.Text = "Usu�rio";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavManagefiltersexecutionstep_Jsonclick = "";
         edtavManagefiltersexecutionstep_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Ddo_managefilters_Cls = "ManageFilters";
         Ddo_managefilters_Tooltip = "Gerenciar filtros";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Alertas";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV2WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A1881Alerta_Codigo',fld:'ALERTA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1882Alerta_ToAreaTrabalho',fld:'ALERTA_TOAREATRABALHO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV81Alerta_Owner',fld:'vALERTA_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV82Alerta_Inicio1',fld:'vALERTA_INICIO1',pic:'',nv:''},{av:'AV83Alerta_Inicio_To1',fld:'vALERTA_INICIO_TO1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV84Alerta_Inicio2',fld:'vALERTA_INICIO2',pic:'',nv:''},{av:'AV85Alerta_Inicio_To2',fld:'vALERTA_INICIO_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV86Alerta_Inicio3',fld:'vALERTA_INICIO3',pic:'',nv:''},{av:'AV87Alerta_Inicio_To3',fld:'vALERTA_INICIO_TO3',pic:'',nv:''},{av:'AV105Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV2WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'dynAlerta_ToUser'},{av:'cmbAlerta_ToGroup'},{av:'edtAlerta_Inicio_Titleformat',ctrl:'ALERTA_INICIO',prop:'Titleformat'},{av:'edtAlerta_Inicio_Title',ctrl:'ALERTA_INICIO',prop:'Title'},{av:'edtAlerta_Fim_Titleformat',ctrl:'ALERTA_FIM',prop:'Titleformat'},{av:'edtAlerta_Fim_Title',ctrl:'ALERTA_FIM',prop:'Title'},{av:'dynAlerta_Owner'},{av:'edtAlerta_Cadastro_Titleformat',ctrl:'ALERTA_CADASTRO',prop:'Titleformat'},{av:'edtAlerta_Cadastro_Title',ctrl:'ALERTA_CADASTRO',prop:'Title'},{av:'AV74GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV75GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'AV36ManageFiltersData',fld:'vMANAGEFILTERSDATA',pic:'',nv:null},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E12OO2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV82Alerta_Inicio1',fld:'vALERTA_INICIO1',pic:'',nv:''},{av:'AV83Alerta_Inicio_To1',fld:'vALERTA_INICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV84Alerta_Inicio2',fld:'vALERTA_INICIO2',pic:'',nv:''},{av:'AV85Alerta_Inicio_To2',fld:'vALERTA_INICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV86Alerta_Inicio3',fld:'vALERTA_INICIO3',pic:'',nv:''},{av:'AV87Alerta_Inicio_To3',fld:'vALERTA_INICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV2WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV81Alerta_Owner',fld:'vALERTA_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV105Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1881Alerta_Codigo',fld:'ALERTA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1882Alerta_ToAreaTrabalho',fld:'ALERTA_TOAREATRABALHO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E25OO2',iparms:[{av:'A1881Alerta_Codigo',fld:'ALERTA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1882Alerta_ToAreaTrabalho',fld:'ALERTA_TOAREATRABALHO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV30Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV80AreaTrabalho_Descricao',fld:'vAREATRABALHO_DESCRICAO',pic:'@!',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E13OO2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV82Alerta_Inicio1',fld:'vALERTA_INICIO1',pic:'',nv:''},{av:'AV83Alerta_Inicio_To1',fld:'vALERTA_INICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV84Alerta_Inicio2',fld:'vALERTA_INICIO2',pic:'',nv:''},{av:'AV85Alerta_Inicio_To2',fld:'vALERTA_INICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV86Alerta_Inicio3',fld:'vALERTA_INICIO3',pic:'',nv:''},{av:'AV87Alerta_Inicio_To3',fld:'vALERTA_INICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV2WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV81Alerta_Owner',fld:'vALERTA_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV105Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1881Alerta_Codigo',fld:'ALERTA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1882Alerta_ToAreaTrabalho',fld:'ALERTA_TOAREATRABALHO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E18OO2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E14OO2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV82Alerta_Inicio1',fld:'vALERTA_INICIO1',pic:'',nv:''},{av:'AV83Alerta_Inicio_To1',fld:'vALERTA_INICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV84Alerta_Inicio2',fld:'vALERTA_INICIO2',pic:'',nv:''},{av:'AV85Alerta_Inicio_To2',fld:'vALERTA_INICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV86Alerta_Inicio3',fld:'vALERTA_INICIO3',pic:'',nv:''},{av:'AV87Alerta_Inicio_To3',fld:'vALERTA_INICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV2WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV81Alerta_Owner',fld:'vALERTA_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV105Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1881Alerta_Codigo',fld:'ALERTA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1882Alerta_ToAreaTrabalho',fld:'ALERTA_TOAREATRABALHO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV84Alerta_Inicio2',fld:'vALERTA_INICIO2',pic:'',nv:''},{av:'AV85Alerta_Inicio_To2',fld:'vALERTA_INICIO_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV86Alerta_Inicio3',fld:'vALERTA_INICIO3',pic:'',nv:''},{av:'AV87Alerta_Inicio_To3',fld:'vALERTA_INICIO_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV82Alerta_Inicio1',fld:'vALERTA_INICIO1',pic:'',nv:''},{av:'AV83Alerta_Inicio_To1',fld:'vALERTA_INICIO_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfiltersalerta_inicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSALERTA_INICIO2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersalerta_inicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSALERTA_INICIO3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersalerta_inicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSALERTA_INICIO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E19OO2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfiltersalerta_inicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSALERTA_INICIO1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E20OO2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E15OO2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV82Alerta_Inicio1',fld:'vALERTA_INICIO1',pic:'',nv:''},{av:'AV83Alerta_Inicio_To1',fld:'vALERTA_INICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV84Alerta_Inicio2',fld:'vALERTA_INICIO2',pic:'',nv:''},{av:'AV85Alerta_Inicio_To2',fld:'vALERTA_INICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV86Alerta_Inicio3',fld:'vALERTA_INICIO3',pic:'',nv:''},{av:'AV87Alerta_Inicio_To3',fld:'vALERTA_INICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV2WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV81Alerta_Owner',fld:'vALERTA_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV105Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1881Alerta_Codigo',fld:'ALERTA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1882Alerta_ToAreaTrabalho',fld:'ALERTA_TOAREATRABALHO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV84Alerta_Inicio2',fld:'vALERTA_INICIO2',pic:'',nv:''},{av:'AV85Alerta_Inicio_To2',fld:'vALERTA_INICIO_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV86Alerta_Inicio3',fld:'vALERTA_INICIO3',pic:'',nv:''},{av:'AV87Alerta_Inicio_To3',fld:'vALERTA_INICIO_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV82Alerta_Inicio1',fld:'vALERTA_INICIO1',pic:'',nv:''},{av:'AV83Alerta_Inicio_To1',fld:'vALERTA_INICIO_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfiltersalerta_inicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSALERTA_INICIO2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersalerta_inicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSALERTA_INICIO3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersalerta_inicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSALERTA_INICIO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E21OO2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfiltersalerta_inicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSALERTA_INICIO2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E16OO2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV82Alerta_Inicio1',fld:'vALERTA_INICIO1',pic:'',nv:''},{av:'AV83Alerta_Inicio_To1',fld:'vALERTA_INICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV84Alerta_Inicio2',fld:'vALERTA_INICIO2',pic:'',nv:''},{av:'AV85Alerta_Inicio_To2',fld:'vALERTA_INICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV86Alerta_Inicio3',fld:'vALERTA_INICIO3',pic:'',nv:''},{av:'AV87Alerta_Inicio_To3',fld:'vALERTA_INICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV2WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV81Alerta_Owner',fld:'vALERTA_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV105Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1881Alerta_Codigo',fld:'ALERTA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1882Alerta_ToAreaTrabalho',fld:'ALERTA_TOAREATRABALHO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV84Alerta_Inicio2',fld:'vALERTA_INICIO2',pic:'',nv:''},{av:'AV85Alerta_Inicio_To2',fld:'vALERTA_INICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV86Alerta_Inicio3',fld:'vALERTA_INICIO3',pic:'',nv:''},{av:'AV87Alerta_Inicio_To3',fld:'vALERTA_INICIO_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV82Alerta_Inicio1',fld:'vALERTA_INICIO1',pic:'',nv:''},{av:'AV83Alerta_Inicio_To1',fld:'vALERTA_INICIO_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfiltersalerta_inicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSALERTA_INICIO2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersalerta_inicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSALERTA_INICIO3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersalerta_inicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSALERTA_INICIO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E22OO2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfiltersalerta_inicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSALERTA_INICIO3',prop:'Visible'}]}");
         setEventMetadata("DDO_MANAGEFILTERS.ONOPTIONCLICKED","{handler:'E11OO2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV82Alerta_Inicio1',fld:'vALERTA_INICIO1',pic:'',nv:''},{av:'AV83Alerta_Inicio_To1',fld:'vALERTA_INICIO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV84Alerta_Inicio2',fld:'vALERTA_INICIO2',pic:'',nv:''},{av:'AV85Alerta_Inicio_To2',fld:'vALERTA_INICIO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV86Alerta_Inicio3',fld:'vALERTA_INICIO3',pic:'',nv:''},{av:'AV87Alerta_Inicio_To3',fld:'vALERTA_INICIO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV2WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV81Alerta_Owner',fld:'vALERTA_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV105Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1881Alerta_Codigo',fld:'ALERTA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1882Alerta_ToAreaTrabalho',fld:'ALERTA_TOAREATRABALHO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_managefilters_Activeeventkey',ctrl:'DDO_MANAGEFILTERS',prop:'ActiveEventKey'}],oparms:[{av:'AV32ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV81Alerta_Owner',fld:'vALERTA_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV82Alerta_Inicio1',fld:'vALERTA_INICIO1',pic:'',nv:''},{av:'AV83Alerta_Inicio_To1',fld:'vALERTA_INICIO_TO1',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV84Alerta_Inicio2',fld:'vALERTA_INICIO2',pic:'',nv:''},{av:'AV85Alerta_Inicio_To2',fld:'vALERTA_INICIO_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV86Alerta_Inicio3',fld:'vALERTA_INICIO3',pic:'',nv:''},{av:'AV87Alerta_Inicio_To3',fld:'vALERTA_INICIO_TO3',pic:'',nv:''},{av:'tblTablemergeddynamicfiltersalerta_inicio1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSALERTA_INICIO1',prop:'Visible'},{av:'tblTablemergeddynamicfiltersalerta_inicio2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSALERTA_INICIO2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersalerta_inicio3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSALERTA_INICIO3',prop:'Visible'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E17OO2',iparms:[{av:'A1881Alerta_Codigo',fld:'ALERTA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_managefilters_Activeeventkey = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV82Alerta_Inicio1 = DateTime.MinValue;
         AV83Alerta_Inicio_To1 = DateTime.MinValue;
         AV19DynamicFiltersSelector2 = "";
         AV84Alerta_Inicio2 = DateTime.MinValue;
         AV85Alerta_Inicio_To2 = DateTime.MinValue;
         AV23DynamicFiltersSelector3 = "";
         AV86Alerta_Inicio3 = DateTime.MinValue;
         AV87Alerta_Inicio_To3 = DateTime.MinValue;
         AV2WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV81Alerta_Owner = AV2WWPContext.gxTpr_Userid;
         AV105Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV36ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_managefilters_Icon = "";
         Ddo_managefilters_Caption = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Update = "";
         AV102Update_GXI = "";
         AV29Delete = "";
         AV103Delete_GXI = "";
         AV30Display = "";
         AV104Display_GXI = "";
         AV80AreaTrabalho_Descricao = "";
         A1885Alerta_Inicio = DateTime.MinValue;
         A1886Alerta_Fim = DateTime.MinValue;
         A1888Alerta_Cadastro = (DateTime)(DateTime.MinValue);
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00OO2_A57Usuario_PessoaCod = new int[1] ;
         H00OO2_A1Usuario_Codigo = new int[1] ;
         H00OO2_A58Usuario_PessoaNom = new String[] {""} ;
         H00OO2_n58Usuario_PessoaNom = new bool[] {false} ;
         H00OO3_A57Usuario_PessoaCod = new int[1] ;
         H00OO3_A1Usuario_Codigo = new int[1] ;
         H00OO3_A58Usuario_PessoaNom = new String[] {""} ;
         H00OO3_n58Usuario_PessoaNom = new bool[] {false} ;
         GridContainer = new GXWebGrid( context);
         AV91WWAlertaDS_2_Dynamicfiltersselector1 = "";
         AV92WWAlertaDS_3_Alerta_inicio1 = DateTime.MinValue;
         AV93WWAlertaDS_4_Alerta_inicio_to1 = DateTime.MinValue;
         AV95WWAlertaDS_6_Dynamicfiltersselector2 = "";
         AV96WWAlertaDS_7_Alerta_inicio2 = DateTime.MinValue;
         AV97WWAlertaDS_8_Alerta_inicio_to2 = DateTime.MinValue;
         AV99WWAlertaDS_10_Dynamicfiltersselector3 = "";
         AV100WWAlertaDS_11_Alerta_inicio3 = DateTime.MinValue;
         AV101WWAlertaDS_12_Alerta_inicio_to3 = DateTime.MinValue;
         H00OO5_A1888Alerta_Cadastro = new DateTime[] {DateTime.MinValue} ;
         H00OO5_A1887Alerta_Owner = new int[1] ;
         H00OO5_A1886Alerta_Fim = new DateTime[] {DateTime.MinValue} ;
         H00OO5_n1886Alerta_Fim = new bool[] {false} ;
         H00OO5_A1885Alerta_Inicio = new DateTime[] {DateTime.MinValue} ;
         H00OO5_A1884Alerta_ToGroup = new short[1] ;
         H00OO5_n1884Alerta_ToGroup = new bool[] {false} ;
         H00OO5_A1883Alerta_ToUser = new int[1] ;
         H00OO5_n1883Alerta_ToUser = new bool[] {false} ;
         H00OO5_A1882Alerta_ToAreaTrabalho = new int[1] ;
         H00OO5_n1882Alerta_ToAreaTrabalho = new bool[] {false} ;
         H00OO5_A1881Alerta_Codigo = new int[1] ;
         H00OO5_A40000AreaTrabalho_Descricao = new String[] {""} ;
         H00OO5_n40000AreaTrabalho_Descricao = new bool[] {false} ;
         A40000AreaTrabalho_Descricao = "";
         H00OO7_AGRID_nRecordCount = new long[1] ;
         AV7HTTPRequest = new GxHttpRequest( context);
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         GridRow = new GXWebRow();
         AV33ManageFiltersXml = "";
         GXt_char1 = "";
         AV37ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV34ManageFiltersItems = new GxObjectCollection( context, "GridStateCollection.Item", "", "wwpbaseobjects.SdtGridStateCollection_Item", "GeneXus.Programs");
         AV35ManageFiltersItem = new wwpbaseobjects.SdtGridStateCollection_Item(context);
         AV31Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         sStyleString = "";
         imgInsert_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblFiltertextalerta_owner_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfiltersalerta_inicio_rangemiddletext3_Jsonclick = "";
         lblDynamicfiltersalerta_inicio_rangemiddletext2_Jsonclick = "";
         lblDynamicfiltersalerta_inicio_rangemiddletext1_Jsonclick = "";
         lblAlertatitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwalerta__default(),
            new Object[][] {
                new Object[] {
               H00OO2_A57Usuario_PessoaCod, H00OO2_A1Usuario_Codigo, H00OO2_A58Usuario_PessoaNom, H00OO2_n58Usuario_PessoaNom
               }
               , new Object[] {
               H00OO3_A57Usuario_PessoaCod, H00OO3_A1Usuario_Codigo, H00OO3_A58Usuario_PessoaNom, H00OO3_n58Usuario_PessoaNom
               }
               , new Object[] {
               H00OO5_A1888Alerta_Cadastro, H00OO5_A1887Alerta_Owner, H00OO5_A1886Alerta_Fim, H00OO5_n1886Alerta_Fim, H00OO5_A1885Alerta_Inicio, H00OO5_A1884Alerta_ToGroup, H00OO5_n1884Alerta_ToGroup, H00OO5_A1883Alerta_ToUser, H00OO5_n1883Alerta_ToUser, H00OO5_A1882Alerta_ToAreaTrabalho,
               H00OO5_n1882Alerta_ToAreaTrabalho, H00OO5_A1881Alerta_Codigo, H00OO5_A40000AreaTrabalho_Descricao, H00OO5_n40000AreaTrabalho_Descricao
               }
               , new Object[] {
               H00OO7_AGRID_nRecordCount
               }
            }
         );
         AV105Pgmname = "WWAlerta";
         /* GeneXus formulas. */
         AV105Pgmname = "WWAlerta";
         context.Gx_err = 0;
         edtavAreatrabalho_descricao_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_96 ;
      private short nGXsfl_96_idx=1 ;
      private short AV13OrderedBy ;
      private short AV32ManageFiltersExecutionStep ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A1884Alerta_ToGroup ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_96_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV2WWPContext_gxTpr_Userid ;
      private short dynAlerta_ToUser_Titleformat ;
      private short cmbAlerta_ToGroup_Titleformat ;
      private short edtAlerta_Inicio_Titleformat ;
      private short edtAlerta_Fim_Titleformat ;
      private short dynAlerta_Owner_Titleformat ;
      private short edtAlerta_Cadastro_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV81Alerta_Owner ;
      private int A1881Alerta_Codigo ;
      private int A1882Alerta_ToAreaTrabalho ;
      private int Gridpaginationbar_Pagestoshow ;
      private int edtavManagefiltersexecutionstep_Visible ;
      private int A1883Alerta_ToUser ;
      private int A1887Alerta_Owner ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int edtavAreatrabalho_descricao_Enabled ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV90WWAlertaDS_1_Alerta_owner ;
      private int edtavOrdereddsc_Visible ;
      private int imgInsert_Visible ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int AV73PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int tblTablemergeddynamicfiltersalerta_inicio1_Visible ;
      private int tblTablemergeddynamicfiltersalerta_inicio2_Visible ;
      private int tblTablemergeddynamicfiltersalerta_inicio3_Visible ;
      private int AV106GXV1 ;
      private int AV107GXV2 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV74GridCurrentPage ;
      private long AV75GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_managefilters_Activeeventkey ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_96_idx="0001" ;
      private String AV105Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_managefilters_Icon ;
      private String Ddo_managefilters_Caption ;
      private String Ddo_managefilters_Tooltip ;
      private String Ddo_managefilters_Cls ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavManagefiltersexecutionstep_Internalname ;
      private String edtavManagefiltersexecutionstep_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtAlerta_Codigo_Internalname ;
      private String edtAlerta_ToAreaTrabalho_Internalname ;
      private String edtavAreatrabalho_descricao_Internalname ;
      private String dynAlerta_ToUser_Internalname ;
      private String cmbAlerta_ToGroup_Internalname ;
      private String edtAlerta_Inicio_Internalname ;
      private String edtAlerta_Fim_Internalname ;
      private String dynAlerta_Owner_Internalname ;
      private String edtAlerta_Cadastro_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavAlerta_owner_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavAlerta_inicio1_Internalname ;
      private String edtavAlerta_inicio_to1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavAlerta_inicio2_Internalname ;
      private String edtavAlerta_inicio_to2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavAlerta_inicio3_Internalname ;
      private String edtavAlerta_inicio_to3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String Ddo_managefilters_Internalname ;
      private String edtAlerta_Inicio_Title ;
      private String edtAlerta_Fim_Title ;
      private String edtAlerta_Cadastro_Title ;
      private String imgInsert_Internalname ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String GXt_char1 ;
      private String tblTablemergeddynamicfiltersalerta_inicio1_Internalname ;
      private String tblTablemergeddynamicfiltersalerta_inicio2_Internalname ;
      private String tblTablemergeddynamicfiltersalerta_inicio3_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String imgInsert_Jsonclick ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextalerta_owner_Internalname ;
      private String lblFiltertextalerta_owner_Jsonclick ;
      private String edtavAlerta_owner_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavAlerta_inicio3_Jsonclick ;
      private String lblDynamicfiltersalerta_inicio_rangemiddletext3_Internalname ;
      private String lblDynamicfiltersalerta_inicio_rangemiddletext3_Jsonclick ;
      private String edtavAlerta_inicio_to3_Jsonclick ;
      private String edtavAlerta_inicio2_Jsonclick ;
      private String lblDynamicfiltersalerta_inicio_rangemiddletext2_Internalname ;
      private String lblDynamicfiltersalerta_inicio_rangemiddletext2_Jsonclick ;
      private String edtavAlerta_inicio_to2_Jsonclick ;
      private String edtavAlerta_inicio1_Jsonclick ;
      private String lblDynamicfiltersalerta_inicio_rangemiddletext1_Internalname ;
      private String lblDynamicfiltersalerta_inicio_rangemiddletext1_Jsonclick ;
      private String edtavAlerta_inicio_to1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String lblAlertatitle_Internalname ;
      private String lblAlertatitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String sGXsfl_96_fel_idx="0001" ;
      private String ROClassString ;
      private String edtAlerta_Codigo_Jsonclick ;
      private String edtAlerta_ToAreaTrabalho_Jsonclick ;
      private String edtavAreatrabalho_descricao_Jsonclick ;
      private String dynAlerta_ToUser_Jsonclick ;
      private String cmbAlerta_ToGroup_Jsonclick ;
      private String edtAlerta_Inicio_Jsonclick ;
      private String edtAlerta_Fim_Jsonclick ;
      private String dynAlerta_Owner_Jsonclick ;
      private String edtAlerta_Cadastro_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime A1888Alerta_Cadastro ;
      private DateTime AV82Alerta_Inicio1 ;
      private DateTime AV83Alerta_Inicio_To1 ;
      private DateTime AV84Alerta_Inicio2 ;
      private DateTime AV85Alerta_Inicio_To2 ;
      private DateTime AV86Alerta_Inicio3 ;
      private DateTime AV87Alerta_Inicio_To3 ;
      private DateTime A1885Alerta_Inicio ;
      private DateTime A1886Alerta_Fim ;
      private DateTime AV92WWAlertaDS_3_Alerta_inicio1 ;
      private DateTime AV93WWAlertaDS_4_Alerta_inicio_to1 ;
      private DateTime AV96WWAlertaDS_7_Alerta_inicio2 ;
      private DateTime AV97WWAlertaDS_8_Alerta_inicio_to2 ;
      private DateTime AV100WWAlertaDS_11_Alerta_inicio3 ;
      private DateTime AV101WWAlertaDS_12_Alerta_inicio_to3 ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool n1882Alerta_ToAreaTrabalho ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1883Alerta_ToUser ;
      private bool n1884Alerta_ToGroup ;
      private bool n1886Alerta_Fim ;
      private bool AV94WWAlertaDS_5_Dynamicfiltersenabled2 ;
      private bool AV98WWAlertaDS_9_Dynamicfiltersenabled3 ;
      private bool n40000AreaTrabalho_Descricao ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private bool AV30Display_IsBlob ;
      private String AV33ManageFiltersXml ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV102Update_GXI ;
      private String AV103Delete_GXI ;
      private String AV104Display_GXI ;
      private String AV80AreaTrabalho_Descricao ;
      private String AV91WWAlertaDS_2_Dynamicfiltersselector1 ;
      private String AV95WWAlertaDS_6_Dynamicfiltersselector2 ;
      private String AV99WWAlertaDS_10_Dynamicfiltersselector3 ;
      private String A40000AreaTrabalho_Descricao ;
      private String AV28Update ;
      private String AV29Delete ;
      private String AV30Display ;
      private IGxSession AV31Session ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox dynAlerta_ToUser ;
      private GXCombobox cmbAlerta_ToGroup ;
      private GXCombobox dynAlerta_Owner ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00OO2_A57Usuario_PessoaCod ;
      private int[] H00OO2_A1Usuario_Codigo ;
      private String[] H00OO2_A58Usuario_PessoaNom ;
      private bool[] H00OO2_n58Usuario_PessoaNom ;
      private int[] H00OO3_A57Usuario_PessoaCod ;
      private int[] H00OO3_A1Usuario_Codigo ;
      private String[] H00OO3_A58Usuario_PessoaNom ;
      private bool[] H00OO3_n58Usuario_PessoaNom ;
      private DateTime[] H00OO5_A1888Alerta_Cadastro ;
      private int[] H00OO5_A1887Alerta_Owner ;
      private DateTime[] H00OO5_A1886Alerta_Fim ;
      private bool[] H00OO5_n1886Alerta_Fim ;
      private DateTime[] H00OO5_A1885Alerta_Inicio ;
      private short[] H00OO5_A1884Alerta_ToGroup ;
      private bool[] H00OO5_n1884Alerta_ToGroup ;
      private int[] H00OO5_A1883Alerta_ToUser ;
      private bool[] H00OO5_n1883Alerta_ToUser ;
      private int[] H00OO5_A1882Alerta_ToAreaTrabalho ;
      private bool[] H00OO5_n1882Alerta_ToAreaTrabalho ;
      private int[] H00OO5_A1881Alerta_Codigo ;
      private String[] H00OO5_A40000AreaTrabalho_Descricao ;
      private bool[] H00OO5_n40000AreaTrabalho_Descricao ;
      private long[] H00OO7_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtGridStateCollection_Item ))]
      private IGxCollection AV34ManageFiltersItems ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV36ManageFiltersData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV2WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtGridStateCollection_Item AV35ManageFiltersItem ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item AV37ManageFiltersDataItem ;
   }

   public class wwalerta__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00OO5( IGxContext context ,
                                             String AV91WWAlertaDS_2_Dynamicfiltersselector1 ,
                                             DateTime AV92WWAlertaDS_3_Alerta_inicio1 ,
                                             DateTime AV93WWAlertaDS_4_Alerta_inicio_to1 ,
                                             bool AV94WWAlertaDS_5_Dynamicfiltersenabled2 ,
                                             String AV95WWAlertaDS_6_Dynamicfiltersselector2 ,
                                             DateTime AV96WWAlertaDS_7_Alerta_inicio2 ,
                                             DateTime AV97WWAlertaDS_8_Alerta_inicio_to2 ,
                                             bool AV98WWAlertaDS_9_Dynamicfiltersenabled3 ,
                                             String AV99WWAlertaDS_10_Dynamicfiltersselector3 ,
                                             DateTime AV100WWAlertaDS_11_Alerta_inicio3 ,
                                             DateTime AV101WWAlertaDS_12_Alerta_inicio_to3 ,
                                             DateTime A1885Alerta_Inicio ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A1887Alerta_Owner ,
                                             short AV2WWPContext_gxTpr_Userid )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [12] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Alerta_Cadastro], T1.[Alerta_Owner], T1.[Alerta_Fim], T1.[Alerta_Inicio], T1.[Alerta_ToGroup], T1.[Alerta_ToUser], T1.[Alerta_ToAreaTrabalho], T1.[Alerta_Codigo], COALESCE( T2.[AreaTrabalho_Descricao], '') AS AreaTrabalho_Descricao";
         sFromString = " FROM ([Alerta] T1 WITH (NOLOCK) LEFT JOIN (SELECT [AreaTrabalho_Descricao] AS AreaTrabalho_Descricao, [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) ) T2 ON T2.[AreaTrabalho_Codigo] = T1.[Alerta_ToAreaTrabalho])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[Alerta_Owner] = @AV2WWPContext__Userid)";
         if ( ( StringUtil.StrCmp(AV91WWAlertaDS_2_Dynamicfiltersselector1, "ALERTA_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV92WWAlertaDS_3_Alerta_inicio1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Alerta_Inicio] >= @AV92WWAlertaDS_3_Alerta_inicio1)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV91WWAlertaDS_2_Dynamicfiltersselector1, "ALERTA_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV93WWAlertaDS_4_Alerta_inicio_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Alerta_Inicio] <= @AV93WWAlertaDS_4_Alerta_inicio_to1)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV94WWAlertaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAlertaDS_6_Dynamicfiltersselector2, "ALERTA_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV96WWAlertaDS_7_Alerta_inicio2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Alerta_Inicio] >= @AV96WWAlertaDS_7_Alerta_inicio2)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV94WWAlertaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAlertaDS_6_Dynamicfiltersselector2, "ALERTA_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV97WWAlertaDS_8_Alerta_inicio_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Alerta_Inicio] <= @AV97WWAlertaDS_8_Alerta_inicio_to2)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV98WWAlertaDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWAlertaDS_10_Dynamicfiltersselector3, "ALERTA_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV100WWAlertaDS_11_Alerta_inicio3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Alerta_Inicio] >= @AV100WWAlertaDS_11_Alerta_inicio3)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV98WWAlertaDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWAlertaDS_10_Dynamicfiltersselector3, "ALERTA_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV101WWAlertaDS_12_Alerta_inicio_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Alerta_Inicio] <= @AV101WWAlertaDS_12_Alerta_inicio_to3)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Alerta_ToUser]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Alerta_ToUser] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Alerta_ToGroup]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Alerta_ToGroup] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Alerta_Inicio]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Alerta_Inicio] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Alerta_Fim]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Alerta_Fim] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Alerta_Owner]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Alerta_Owner] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Alerta_Cadastro]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Alerta_Cadastro] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Alerta_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00OO7( IGxContext context ,
                                             String AV91WWAlertaDS_2_Dynamicfiltersselector1 ,
                                             DateTime AV92WWAlertaDS_3_Alerta_inicio1 ,
                                             DateTime AV93WWAlertaDS_4_Alerta_inicio_to1 ,
                                             bool AV94WWAlertaDS_5_Dynamicfiltersenabled2 ,
                                             String AV95WWAlertaDS_6_Dynamicfiltersselector2 ,
                                             DateTime AV96WWAlertaDS_7_Alerta_inicio2 ,
                                             DateTime AV97WWAlertaDS_8_Alerta_inicio_to2 ,
                                             bool AV98WWAlertaDS_9_Dynamicfiltersenabled3 ,
                                             String AV99WWAlertaDS_10_Dynamicfiltersselector3 ,
                                             DateTime AV100WWAlertaDS_11_Alerta_inicio3 ,
                                             DateTime AV101WWAlertaDS_12_Alerta_inicio_to3 ,
                                             DateTime A1885Alerta_Inicio ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A1887Alerta_Owner ,
                                             short AV2WWPContext_gxTpr_Userid )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [7] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([Alerta] T1 WITH (NOLOCK) LEFT JOIN (SELECT [AreaTrabalho_Descricao] AS AreaTrabalho_Descricao, [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) ) T2 ON T2.[AreaTrabalho_Codigo] = T1.[Alerta_ToAreaTrabalho])";
         scmdbuf = scmdbuf + " WHERE (T1.[Alerta_Owner] = @AV2WWPContext__Userid)";
         if ( ( StringUtil.StrCmp(AV91WWAlertaDS_2_Dynamicfiltersselector1, "ALERTA_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV92WWAlertaDS_3_Alerta_inicio1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Alerta_Inicio] >= @AV92WWAlertaDS_3_Alerta_inicio1)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV91WWAlertaDS_2_Dynamicfiltersselector1, "ALERTA_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV93WWAlertaDS_4_Alerta_inicio_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Alerta_Inicio] <= @AV93WWAlertaDS_4_Alerta_inicio_to1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV94WWAlertaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAlertaDS_6_Dynamicfiltersselector2, "ALERTA_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV96WWAlertaDS_7_Alerta_inicio2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Alerta_Inicio] >= @AV96WWAlertaDS_7_Alerta_inicio2)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV94WWAlertaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV95WWAlertaDS_6_Dynamicfiltersselector2, "ALERTA_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV97WWAlertaDS_8_Alerta_inicio_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Alerta_Inicio] <= @AV97WWAlertaDS_8_Alerta_inicio_to2)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV98WWAlertaDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWAlertaDS_10_Dynamicfiltersselector3, "ALERTA_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV100WWAlertaDS_11_Alerta_inicio3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Alerta_Inicio] >= @AV100WWAlertaDS_11_Alerta_inicio3)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV98WWAlertaDS_9_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV99WWAlertaDS_10_Dynamicfiltersselector3, "ALERTA_INICIO") == 0 ) && ( ! (DateTime.MinValue==AV101WWAlertaDS_12_Alerta_inicio_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Alerta_Inicio] <= @AV101WWAlertaDS_12_Alerta_inicio_to3)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 2 :
                     return conditional_H00OO5(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (DateTime)dynConstraints[11] , (short)dynConstraints[12] , (bool)dynConstraints[13] , (int)dynConstraints[14] , (short)dynConstraints[15] );
               case 3 :
                     return conditional_H00OO7(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (DateTime)dynConstraints[11] , (short)dynConstraints[12] , (bool)dynConstraints[13] , (int)dynConstraints[14] , (short)dynConstraints[15] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00OO2 ;
          prmH00OO2 = new Object[] {
          } ;
          Object[] prmH00OO3 ;
          prmH00OO3 = new Object[] {
          } ;
          Object[] prmH00OO5 ;
          prmH00OO5 = new Object[] {
          new Object[] {"@AV2WWPContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV92WWAlertaDS_3_Alerta_inicio1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV93WWAlertaDS_4_Alerta_inicio_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV96WWAlertaDS_7_Alerta_inicio2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV97WWAlertaDS_8_Alerta_inicio_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV100WWAlertaDS_11_Alerta_inicio3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV101WWAlertaDS_12_Alerta_inicio_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00OO7 ;
          prmH00OO7 = new Object[] {
          new Object[] {"@AV2WWPContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV92WWAlertaDS_3_Alerta_inicio1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV93WWAlertaDS_4_Alerta_inicio_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV96WWAlertaDS_7_Alerta_inicio2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV97WWAlertaDS_8_Alerta_inicio_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV100WWAlertaDS_11_Alerta_inicio3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV101WWAlertaDS_12_Alerta_inicio_to3",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00OO2", "SELECT T2.[Pessoa_Codigo] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00OO2,0,0,true,false )
             ,new CursorDef("H00OO3", "SELECT T2.[Pessoa_Codigo] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00OO3,0,0,true,false )
             ,new CursorDef("H00OO5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00OO5,11,0,true,false )
             ,new CursorDef("H00OO7", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00OO7,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDateTime(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((String[]) buf[12])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                return;
             case 3 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[13]);
                }
                return;
       }
    }

 }

}
