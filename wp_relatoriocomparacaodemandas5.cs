/*
               File: WP_RelatorioComparacaoDemandas5
        Description: WP_RelatorioComparacaoDemandas5
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/16/2020 1:9:47.72
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_relatoriocomparacaodemandas5 : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_relatoriocomparacaodemandas5( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_relatoriocomparacaodemandas5( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_35 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_35_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_35_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               A493ContagemResultado_DemandaFM = GetNextPar( );
               n493ContagemResultado_DemandaFM = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A493ContagemResultado_DemandaFM", A493ContagemResultado_DemandaFM);
               A457ContagemResultado_Demanda = GetNextPar( );
               n457ContagemResultado_Demanda = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A457ContagemResultado_Demanda", A457ContagemResultado_Demanda);
               A471ContagemResultado_DataDmn = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A471ContagemResultado_DataDmn", context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"));
               A2017ContagemResultado_DataEntregaReal = context.localUtil.ParseDTimeParm( GetNextPar( ));
               n2017ContagemResultado_DataEntregaReal = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2017ContagemResultado_DataEntregaReal", context.localUtil.TToC( A2017ContagemResultado_DataEntregaReal, 8, 5, 0, 3, "/", ":", " "));
               A494ContagemResultado_Descricao = GetNextPar( );
               n494ContagemResultado_Descricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A494ContagemResultado_Descricao", A494ContagemResultado_Descricao);
               A509ContagemrResultado_SistemaSigla = GetNextPar( );
               n509ContagemrResultado_SistemaSigla = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A509ContagemrResultado_SistemaSigla", A509ContagemrResultado_SistemaSigla);
               A602ContagemResultado_OSVinculada = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n602ContagemResultado_OSVinculada = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A602ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A602ContagemResultado_OSVinculada), 6, 0)));
               A1326ContagemResultado_ContratadaTipoFab = GetNextPar( );
               n1326ContagemResultado_ContratadaTipoFab = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1326ContagemResultado_ContratadaTipoFab", A1326ContagemResultado_ContratadaTipoFab);
               A682ContagemResultado_PFBFMUltima = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A682ContagemResultado_PFBFMUltima", StringUtil.LTrim( StringUtil.Str( A682ContagemResultado_PFBFMUltima, 14, 5)));
               A684ContagemResultado_PFBFSUltima = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A684ContagemResultado_PFBFSUltima", StringUtil.LTrim( StringUtil.Str( A684ContagemResultado_PFBFSUltima, 14, 5)));
               AV6ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, A456ContagemResultado_Codigo, A493ContagemResultado_DemandaFM, A457ContagemResultado_Demanda, A471ContagemResultado_DataDmn, A2017ContagemResultado_DataEntregaReal, A494ContagemResultado_Descricao, A509ContagemrResultado_SistemaSigla, A602ContagemResultado_OSVinculada, A1326ContagemResultado_ContratadaTipoFab, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, AV6ContagemResultado_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PATP2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTTP2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020416194776");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_relatoriocomparacaodemandas5.aspx") +"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_35", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_35), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV10GridPageCount), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDAFM", A493ContagemResultado_DemandaFM);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDA", A457ContagemResultado_Demanda);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATADMN", context.localUtil.DToC( A471ContagemResultado_DataDmn, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATAENTREGAREAL", context.localUtil.TToC( A2017ContagemResultado_DataEntregaReal, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DESCRICAO", A494ContagemResultado_Descricao);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRRESULTADO_SISTEMASIGLA", StringUtil.RTrim( A509ContagemrResultado_SistemaSigla));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_OSVINCULADA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A602ContagemResultado_OSVinculada), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADATIPOFAB", StringUtil.RTrim( A1326ContagemResultado_ContratadaTipoFab));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFMULTIMA", StringUtil.LTrim( StringUtil.NToC( A682ContagemResultado_PFBFMUltima, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFSULTIMA", StringUtil.LTrim( StringUtil.NToC( A684ContagemResultado_PFBFSUltima, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "</form>") ;
         }
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WETP2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTTP2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_relatoriocomparacaodemandas5.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_RelatorioComparacaoDemandas5" ;
      }

      public override String GetPgmdesc( )
      {
         return "WP_RelatorioComparacaoDemandas5" ;
      }

      protected void WBTP0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_TP2( true) ;
         }
         else
         {
            wb_table1_2_TP2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_TP2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTTP2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "WP_RelatorioComparacaoDemandas5", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPTP0( ) ;
      }

      protected void WSTP2( )
      {
         STARTTP2( ) ;
         EVTTP2( ) ;
      }

      protected void EVTTP2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11TP2 */
                              E11TP2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_35_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_35_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_35_idx), 4, 0)), 4, "0");
                              SubsflControlProps_352( ) ;
                              AV6ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavContagemresultado_codigo_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_Codigo), 6, 0)));
                              AV7ContagemResultado_OSVinculada = (int)(context.localUtil.CToN( cgiGet( edtavContagemresultado_osvinculada_Internalname), ",", "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_osvinculada_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_OSVinculada), 6, 0)));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E12TP2 */
                                    E12TP2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13TP2 */
                                    E13TP2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14TP2 */
                                    E14TP2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WETP2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PATP2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavGridcurrentpage_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_352( ) ;
         while ( nGXsfl_35_idx <= nRC_GXsfl_35 )
         {
            sendrow_352( ) ;
            nGXsfl_35_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_35_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_35_idx+1));
            sGXsfl_35_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_35_idx), 4, 0)), 4, "0");
            SubsflControlProps_352( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       int A456ContagemResultado_Codigo ,
                                       String A493ContagemResultado_DemandaFM ,
                                       String A457ContagemResultado_Demanda ,
                                       DateTime A471ContagemResultado_DataDmn ,
                                       DateTime A2017ContagemResultado_DataEntregaReal ,
                                       String A494ContagemResultado_Descricao ,
                                       String A509ContagemrResultado_SistemaSigla ,
                                       int A602ContagemResultado_OSVinculada ,
                                       String A1326ContagemResultado_ContratadaTipoFab ,
                                       decimal A682ContagemResultado_PFBFMUltima ,
                                       decimal A684ContagemResultado_PFBFSUltima ,
                                       int AV6ContagemResultado_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFTP2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFTP2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavContagemresultado_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_codigo_Enabled), 5, 0)));
         edtavContagemresultado_osvinculada_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_osvinculada_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_osvinculada_Enabled), 5, 0)));
      }

      protected void RFTP2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 35;
         /* Execute user event: E13TP2 */
         E13TP2 ();
         nGXsfl_35_idx = 1;
         sGXsfl_35_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_35_idx), 4, 0)), 4, "0");
         SubsflControlProps_352( ) ;
         nGXsfl_35_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_352( ) ;
            /* Execute user event: E14TP2 */
            E14TP2 ();
            if ( ( GRID_nCurrentRecord > 0 ) && ( GRID_nGridOutOfScope == 0 ) && ( nGXsfl_35_idx == 1 ) )
            {
               GRID_nCurrentRecord = 0;
               GRID_nGridOutOfScope = 1;
               subgrid_firstpage( ) ;
               /* Execute user event: E14TP2 */
               E14TP2 ();
            }
            wbEnd = 35;
            WBTP0( ) ;
         }
         nGXsfl_35_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A456ContagemResultado_Codigo, A493ContagemResultado_DemandaFM, A457ContagemResultado_Demanda, A471ContagemResultado_DataDmn, A2017ContagemResultado_DataEntregaReal, A494ContagemResultado_Descricao, A509ContagemrResultado_SistemaSigla, A602ContagemResultado_OSVinculada, A1326ContagemResultado_ContratadaTipoFab, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, AV6ContagemResultado_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         if ( GRID_nEOF == 0 )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A456ContagemResultado_Codigo, A493ContagemResultado_DemandaFM, A457ContagemResultado_Demanda, A471ContagemResultado_DataDmn, A2017ContagemResultado_DataEntregaReal, A494ContagemResultado_Descricao, A509ContagemrResultado_SistemaSigla, A602ContagemResultado_OSVinculada, A1326ContagemResultado_ContratadaTipoFab, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, AV6ContagemResultado_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A456ContagemResultado_Codigo, A493ContagemResultado_DemandaFM, A457ContagemResultado_Demanda, A471ContagemResultado_DataDmn, A2017ContagemResultado_DataEntregaReal, A494ContagemResultado_Descricao, A509ContagemrResultado_SistemaSigla, A602ContagemResultado_OSVinculada, A1326ContagemResultado_ContratadaTipoFab, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, AV6ContagemResultado_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         subGrid_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A456ContagemResultado_Codigo, A493ContagemResultado_DemandaFM, A457ContagemResultado_Demanda, A471ContagemResultado_DataDmn, A2017ContagemResultado_DataEntregaReal, A494ContagemResultado_Descricao, A509ContagemrResultado_SistemaSigla, A602ContagemResultado_OSVinculada, A1326ContagemResultado_ContratadaTipoFab, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, AV6ContagemResultado_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A456ContagemResultado_Codigo, A493ContagemResultado_DemandaFM, A457ContagemResultado_Demanda, A471ContagemResultado_DataDmn, A2017ContagemResultado_DataEntregaReal, A494ContagemResultado_Descricao, A509ContagemrResultado_SistemaSigla, A602ContagemResultado_OSVinculada, A1326ContagemResultado_ContratadaTipoFab, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, AV6ContagemResultado_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPTP0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavContagemresultado_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_codigo_Enabled), 5, 0)));
         edtavContagemresultado_osvinculada_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_osvinculada_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_osvinculada_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12TP2 */
         E12TP2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavGridcurrentpage_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavGridcurrentpage_Internalname), ",", ".") > Convert.ToDecimal( 9999999999L )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vGRIDCURRENTPAGE");
               GX_FocusControl = edtavGridcurrentpage_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV9GridCurrentPage = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9GridCurrentPage), 10, 0)));
            }
            else
            {
               AV9GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( edtavGridcurrentpage_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9GridCurrentPage), 10, 0)));
            }
            /* Read saved values. */
            nRC_GXsfl_35 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_35"), ",", "."));
            AV10GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            nGXsfl_35_idx = (short)(context.localUtil.CToN( cgiGet( subGrid_Internalname+"_ROW"), ",", "."));
            sGXsfl_35_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_35_idx), 4, 0)), 4, "0");
            SubsflControlProps_352( ) ;
            if ( nGXsfl_35_idx > 0 )
            {
               AV6ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavContagemresultado_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_Codigo), 6, 0)));
               AV7ContagemResultado_OSVinculada = (int)(context.localUtil.CToN( cgiGet( edtavContagemresultado_osvinculada_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_osvinculada_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_OSVinculada), 6, 0)));
            }
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E12TP2 */
         E12TP2 ();
         if (returnInSub) return;
      }

      protected void E12TP2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         AV9GridCurrentPage = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9GridCurrentPage), 10, 0)));
         edtavGridcurrentpage_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGridcurrentpage_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGridcurrentpage_Visible), 5, 0)));
         AV10GridPageCount = -1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10GridPageCount), 10, 0)));
      }

      protected void E13TP2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV5Context) ;
      }

      private void E14TP2( )
      {
         /* Grid_Load Routine */
         /* Using cursor H00TP3 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A490ContagemResultado_ContratadaCod = H00TP3_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00TP3_n490ContagemResultado_ContratadaCod[0];
            A489ContagemResultado_SistemaCod = H00TP3_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = H00TP3_n489ContagemResultado_SistemaCod[0];
            A456ContagemResultado_Codigo = H00TP3_A456ContagemResultado_Codigo[0];
            A493ContagemResultado_DemandaFM = H00TP3_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = H00TP3_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = H00TP3_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = H00TP3_n457ContagemResultado_Demanda[0];
            A471ContagemResultado_DataDmn = H00TP3_A471ContagemResultado_DataDmn[0];
            A2017ContagemResultado_DataEntregaReal = H00TP3_A2017ContagemResultado_DataEntregaReal[0];
            n2017ContagemResultado_DataEntregaReal = H00TP3_n2017ContagemResultado_DataEntregaReal[0];
            A494ContagemResultado_Descricao = H00TP3_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = H00TP3_n494ContagemResultado_Descricao[0];
            A509ContagemrResultado_SistemaSigla = H00TP3_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = H00TP3_n509ContagemrResultado_SistemaSigla[0];
            A602ContagemResultado_OSVinculada = H00TP3_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = H00TP3_n602ContagemResultado_OSVinculada[0];
            A1326ContagemResultado_ContratadaTipoFab = H00TP3_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = H00TP3_n1326ContagemResultado_ContratadaTipoFab[0];
            A682ContagemResultado_PFBFMUltima = H00TP3_A682ContagemResultado_PFBFMUltima[0];
            A684ContagemResultado_PFBFSUltima = H00TP3_A684ContagemResultado_PFBFSUltima[0];
            A1326ContagemResultado_ContratadaTipoFab = H00TP3_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = H00TP3_n1326ContagemResultado_ContratadaTipoFab[0];
            A509ContagemrResultado_SistemaSigla = H00TP3_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = H00TP3_n509ContagemrResultado_SistemaSigla[0];
            A682ContagemResultado_PFBFMUltima = H00TP3_A682ContagemResultado_PFBFMUltima[0];
            A684ContagemResultado_PFBFSUltima = H00TP3_A684ContagemResultado_PFBFSUltima[0];
            AV6ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_Codigo), 6, 0)));
            AV11ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            AV12ContagemResultado_Demanda = A457ContagemResultado_Demanda;
            AV13ContagemResultado_DataDmn = A471ContagemResultado_DataDmn;
            AV14ContagemResultado_DataEntregaReal = A2017ContagemResultado_DataEntregaReal;
            AV15ContagemResultado_Descricao = A494ContagemResultado_Descricao;
            AV16ContagemrResultado_SistemaSigla = A509ContagemrResultado_SistemaSigla;
            AV7ContagemResultado_OSVinculada = A602ContagemResultado_OSVinculada;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_osvinculada_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_OSVinculada), 6, 0)));
            AV17ContagemResultado_ContratadaTipoFab = A1326ContagemResultado_ContratadaTipoFab;
            AV18ContagemResultado_PFBFMUltima = A682ContagemResultado_PFBFMUltima;
            AV19ContagemResultado_PFBFSUltima = A684ContagemResultado_PFBFSUltima;
            AV23Esforco = ((StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "M")==0) ? A682ContagemResultado_PFBFMUltima : ((StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "S")==0) ? A684ContagemResultado_PFBFSUltima : (decimal)(0)));
            /* Execute user subroutine: 'SUBOSVINCULADA' */
            S113 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               returnInSub = true;
               if (true) return;
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 35;
         }
         if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
         {
            sendrow_352( ) ;
            GRID_nEOF = 1;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
            {
               GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
            }
         }
         if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
         {
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
         }
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_35_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(35, GridRow);
         }
      }

      protected void E11TP2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            AV9GridCurrentPage = (long)(AV9GridCurrentPage-1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9GridCurrentPage), 10, 0)));
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            AV9GridCurrentPage = (long)(AV9GridCurrentPage+1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9GridCurrentPage), 10, 0)));
            subgrid_nextpage( ) ;
         }
         else
         {
            AV8PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            AV9GridCurrentPage = AV8PageToGo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9GridCurrentPage), 10, 0)));
            subgrid_gotopage( AV8PageToGo) ;
         }
         context.DoAjaxRefresh();
      }

      protected void S113( )
      {
         /* 'SUBOSVINCULADA' Routine */
         GXt_char1 = "SubOsVinculada";
         new geralog(context ).execute( ref  GXt_char1) ;
         /* Using cursor H00TP5 */
         pr_default.execute(1, new Object[] {AV6ContagemResultado_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A490ContagemResultado_ContratadaCod = H00TP5_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00TP5_n490ContagemResultado_ContratadaCod[0];
            A489ContagemResultado_SistemaCod = H00TP5_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = H00TP5_n489ContagemResultado_SistemaCod[0];
            A456ContagemResultado_Codigo = H00TP5_A456ContagemResultado_Codigo[0];
            A493ContagemResultado_DemandaFM = H00TP5_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = H00TP5_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = H00TP5_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = H00TP5_n457ContagemResultado_Demanda[0];
            A471ContagemResultado_DataDmn = H00TP5_A471ContagemResultado_DataDmn[0];
            A2017ContagemResultado_DataEntregaReal = H00TP5_A2017ContagemResultado_DataEntregaReal[0];
            n2017ContagemResultado_DataEntregaReal = H00TP5_n2017ContagemResultado_DataEntregaReal[0];
            A494ContagemResultado_Descricao = H00TP5_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = H00TP5_n494ContagemResultado_Descricao[0];
            A509ContagemrResultado_SistemaSigla = H00TP5_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = H00TP5_n509ContagemrResultado_SistemaSigla[0];
            A602ContagemResultado_OSVinculada = H00TP5_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = H00TP5_n602ContagemResultado_OSVinculada[0];
            A1326ContagemResultado_ContratadaTipoFab = H00TP5_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = H00TP5_n1326ContagemResultado_ContratadaTipoFab[0];
            A682ContagemResultado_PFBFMUltima = H00TP5_A682ContagemResultado_PFBFMUltima[0];
            A684ContagemResultado_PFBFSUltima = H00TP5_A684ContagemResultado_PFBFSUltima[0];
            A1326ContagemResultado_ContratadaTipoFab = H00TP5_A1326ContagemResultado_ContratadaTipoFab[0];
            n1326ContagemResultado_ContratadaTipoFab = H00TP5_n1326ContagemResultado_ContratadaTipoFab[0];
            A509ContagemrResultado_SistemaSigla = H00TP5_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = H00TP5_n509ContagemrResultado_SistemaSigla[0];
            A682ContagemResultado_PFBFMUltima = H00TP5_A682ContagemResultado_PFBFMUltima[0];
            A684ContagemResultado_PFBFSUltima = H00TP5_A684ContagemResultado_PFBFSUltima[0];
            AV6ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_Codigo), 6, 0)));
            AV11ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            AV12ContagemResultado_Demanda = A457ContagemResultado_Demanda;
            AV13ContagemResultado_DataDmn = A471ContagemResultado_DataDmn;
            AV14ContagemResultado_DataEntregaReal = A2017ContagemResultado_DataEntregaReal;
            AV15ContagemResultado_Descricao = A494ContagemResultado_Descricao;
            AV16ContagemrResultado_SistemaSigla = A509ContagemrResultado_SistemaSigla;
            AV7ContagemResultado_OSVinculada = A602ContagemResultado_OSVinculada;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_osvinculada_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_OSVinculada), 6, 0)));
            AV17ContagemResultado_ContratadaTipoFab = A1326ContagemResultado_ContratadaTipoFab;
            AV18ContagemResultado_PFBFMUltima = A682ContagemResultado_PFBFMUltima;
            AV19ContagemResultado_PFBFSUltima = A684ContagemResultado_PFBFSUltima;
            AV23Esforco = ((StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "M")==0) ? A682ContagemResultado_PFBFMUltima : ((StringUtil.StrCmp(A1326ContagemResultado_ContratadaTipoFab, "S")==0) ? A684ContagemResultado_PFBFSUltima : (decimal)(0)));
            GXt_char1 = "&ContagemResultado_Codigo             = " + context.localUtil.Format( (decimal)(AV6ContagemResultado_Codigo), "ZZZZZ9");
            new geralog(context ).execute( ref  GXt_char1) ;
            GXt_char2 = "&ContagemResultado_DemandaFM          = " + AV11ContagemResultado_DemandaFM;
            new geralog(context ).execute( ref  GXt_char2) ;
            GXt_char3 = "&ContagemResultado_Demanda            = " + AV12ContagemResultado_Demanda;
            new geralog(context ).execute( ref  GXt_char3) ;
            GXt_char4 = "&ContagemResultado_DataDmn            = " + context.localUtil.Format( AV13ContagemResultado_DataDmn, "99/99/99");
            new geralog(context ).execute( ref  GXt_char4) ;
            GXt_char5 = "&ContagemResultado_DataEntregaReal    = " + context.localUtil.Format( AV14ContagemResultado_DataEntregaReal, "99/99/99 99:99");
            new geralog(context ).execute( ref  GXt_char5) ;
            GXt_char6 = "&ContagemResultado_Descricao          = " + AV15ContagemResultado_Descricao;
            new geralog(context ).execute( ref  GXt_char6) ;
            GXt_char7 = "&ContagemrResultado_SistemaSigla      = " + StringUtil.RTrim( context.localUtil.Format( AV16ContagemrResultado_SistemaSigla, "@!"));
            new geralog(context ).execute( ref  GXt_char7) ;
            GXt_char8 = "&ContagemResultado_OSVinculada        = " + context.localUtil.Format( (decimal)(AV7ContagemResultado_OSVinculada), "ZZZZZ9");
            new geralog(context ).execute( ref  GXt_char8) ;
            GXt_char9 = "&ContagemResultado_ContratadaTipoFab  = " + StringUtil.RTrim( context.localUtil.Format( AV17ContagemResultado_ContratadaTipoFab, ""));
            new geralog(context ).execute( ref  GXt_char9) ;
            GXt_char10 = "&ContagemResultado_PFBFMUltima        = " + context.localUtil.Format( AV18ContagemResultado_PFBFMUltima, "ZZ,ZZZ,ZZ9.999");
            new geralog(context ).execute( ref  GXt_char10) ;
            GXt_char11 = "&ContagemResultado_PFBFSUltima		= " + context.localUtil.Format( AV19ContagemResultado_PFBFSUltima, "ZZ,ZZZ,ZZ9.999");
            new geralog(context ).execute( ref  GXt_char11) ;
            GXt_char12 = "SubOsVinculada " + context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9");
            new geralog(context ).execute( ref  GXt_char12) ;
            if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
            {
               sendrow_352( ) ;
               GRID_nEOF = 1;
               GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
               if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
               {
                  GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
               }
            }
            if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
            {
               GRID_nEOF = 0;
               GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            }
            GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
            if ( isFullAjaxMode( ) && ( nGXsfl_35_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(35, GridRow);
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
      }

      protected void wb_table1_2_TP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_TP2( true) ;
         }
         else
         {
            wb_table2_8_TP2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_TP2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_29_TP2( true) ;
         }
         else
         {
            wb_table3_29_TP2( false) ;
         }
         return  ;
      }

      protected void wb_table3_29_TP2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_TP2e( true) ;
         }
         else
         {
            wb_table1_2_TP2e( false) ;
         }
      }

      protected void wb_table3_29_TP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_32_TP2( true) ;
         }
         else
         {
            wb_table4_32_TP2( false) ;
         }
         return  ;
      }

      protected void wb_table4_32_TP2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_29_TP2e( true) ;
         }
         else
         {
            wb_table3_29_TP2e( false) ;
         }
      }

      protected void wb_table4_32_TP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"35\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Contagem Resultado_Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Os vinculada") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6ContagemResultado_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultado_codigo_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContagemResultado_OSVinculada), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultado_osvinculada_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 35 )
         {
            wbEnd = 0;
            nRC_GXsfl_35 = (short)(nGXsfl_35_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_35_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGridcurrentpage_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9GridCurrentPage), 10, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV9GridCurrentPage), "ZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGridcurrentpage_Jsonclick, 0, "Attribute", "", "", "", edtavGridcurrentpage_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_RelatorioComparacaoDemandas5.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_32_TP2e( true) ;
         }
         else
         {
            wb_table4_32_TP2e( false) ;
         }
      }

      protected void wb_table2_8_TP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktitle_Internalname, "T�tulo de Trabalhar com", "", "", lblTextblocktitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_RelatorioComparacaoDemandas5.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_13_TP2( true) ;
         }
         else
         {
            wb_table5_13_TP2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_TP2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_TP2e( true) ;
         }
         else
         {
            wb_table2_8_TP2e( false) ;
         }
      }

      protected void wb_table5_13_TP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_16_TP2( true) ;
         }
         else
         {
            wb_table6_16_TP2( false) ;
         }
         return  ;
      }

      protected void wb_table6_16_TP2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_20_TP2( true) ;
         }
         else
         {
            wb_table7_20_TP2( false) ;
         }
         return  ;
      }

      protected void wb_table7_20_TP2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            wb_table8_24_TP2( true) ;
         }
         else
         {
            wb_table8_24_TP2( false) ;
         }
         return  ;
      }

      protected void wb_table8_24_TP2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_TP2e( true) ;
         }
         else
         {
            wb_table5_13_TP2e( false) ;
         }
      }

      protected void wb_table8_24_TP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefiltersextras_Internalname, tblTablefiltersextras_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_24_TP2e( true) ;
         }
         else
         {
            wb_table8_24_TP2e( false) ;
         }
      }

      protected void wb_table7_20_TP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefiltersb_Internalname, tblTablefiltersb_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_20_TP2e( true) ;
         }
         else
         {
            wb_table7_20_TP2e( false) ;
         }
      }

      protected void wb_table6_16_TP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefiltersa_Internalname, tblTablefiltersa_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_16_TP2e( true) ;
         }
         else
         {
            wb_table6_16_TP2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PATP2( ) ;
         WSTP2( ) ;
         WETP2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?5113033");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020416194849");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
            context.AddJavascriptSource("wp_relatoriocomparacaodemandas5.js", "?2020416194849");
            context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_352( )
      {
         edtavContagemresultado_codigo_Internalname = "vCONTAGEMRESULTADO_CODIGO_"+sGXsfl_35_idx;
         edtavContagemresultado_osvinculada_Internalname = "vCONTAGEMRESULTADO_OSVINCULADA_"+sGXsfl_35_idx;
      }

      protected void SubsflControlProps_fel_352( )
      {
         edtavContagemresultado_codigo_Internalname = "vCONTAGEMRESULTADO_CODIGO_"+sGXsfl_35_fel_idx;
         edtavContagemresultado_osvinculada_Internalname = "vCONTAGEMRESULTADO_OSVINCULADA_"+sGXsfl_35_fel_idx;
      }

      protected void sendrow_352( )
      {
         SubsflControlProps_352( ) ;
         WBTP0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_35_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_35_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_35_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContagemresultado_codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6ContagemResultado_Codigo), 6, 0, ",", "")),((edtavContagemresultado_codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV6ContagemResultado_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV6ContagemResultado_Codigo), "ZZZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContagemresultado_codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavContagemresultado_codigo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)35,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContagemresultado_osvinculada_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContagemResultado_OSVinculada), 6, 0, ",", "")),((edtavContagemresultado_osvinculada_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV7ContagemResultado_OSVinculada), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV7ContagemResultado_OSVinculada), "ZZZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContagemresultado_osvinculada_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavContagemresultado_osvinculada_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)35,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GridContainer.AddRow(GridRow);
            nGXsfl_35_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_35_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_35_idx+1));
            sGXsfl_35_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_35_idx), 4, 0)), 4, "0");
            SubsflControlProps_352( ) ;
         }
         /* End function sendrow_352 */
      }

      protected void init_default_properties( )
      {
         lblTextblocktitle_Internalname = "TEXTBLOCKTITLE";
         tblTablefiltersa_Internalname = "TABLEFILTERSA";
         tblTablefiltersb_Internalname = "TABLEFILTERSB";
         tblTablefiltersextras_Internalname = "TABLEFILTERSEXTRAS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavContagemresultado_codigo_Internalname = "vCONTAGEMRESULTADO_CODIGO";
         edtavContagemresultado_osvinculada_Internalname = "vCONTAGEMRESULTADO_OSVINCULADA";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         edtavGridcurrentpage_Internalname = "vGRIDCURRENTPAGE";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavContagemresultado_osvinculada_Jsonclick = "";
         edtavContagemresultado_codigo_Jsonclick = "";
         edtavGridcurrentpage_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowhovering = 0;
         subGrid_Selectioncolor = (int)(0xC4F0C4);
         subGrid_Allowselection = 1;
         edtavContagemresultado_osvinculada_Enabled = 0;
         edtavContagemresultado_codigo_Enabled = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavGridcurrentpage_Visible = 1;
         subGrid_Backcolorstyle = 3;
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "WP_RelatorioComparacaoDemandas5";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'A457ContagemResultado_Demanda',fld:'CONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'A2017ContagemResultado_DataEntregaReal',fld:'CONTAGEMRESULTADO_DATAENTREGAREAL',pic:'99/99/99 99:99',nv:''},{av:'A494ContagemResultado_Descricao',fld:'CONTAGEMRESULTADO_DESCRICAO',pic:'',nv:''},{av:'A509ContagemrResultado_SistemaSigla',fld:'CONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A1326ContagemResultado_ContratadaTipoFab',fld:'CONTAGEMRESULTADO_CONTRATADATIPOFAB',pic:'',nv:''},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV6ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E14TP2',iparms:[{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'A457ContagemResultado_Demanda',fld:'CONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'A2017ContagemResultado_DataEntregaReal',fld:'CONTAGEMRESULTADO_DATAENTREGAREAL',pic:'99/99/99 99:99',nv:''},{av:'A494ContagemResultado_Descricao',fld:'CONTAGEMRESULTADO_DESCRICAO',pic:'',nv:''},{av:'A509ContagemrResultado_SistemaSigla',fld:'CONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A1326ContagemResultado_ContratadaTipoFab',fld:'CONTAGEMRESULTADO_CONTRATADATIPOFAB',pic:'',nv:''},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV6ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV6ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11TP2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'A457ContagemResultado_Demanda',fld:'CONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'A2017ContagemResultado_DataEntregaReal',fld:'CONTAGEMRESULTADO_DATAENTREGAREAL',pic:'99/99/99 99:99',nv:''},{av:'A494ContagemResultado_Descricao',fld:'CONTAGEMRESULTADO_DESCRICAO',pic:'',nv:''},{av:'A509ContagemrResultado_SistemaSigla',fld:'CONTAGEMRRESULTADO_SISTEMASIGLA',pic:'@!',nv:''},{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A1326ContagemResultado_ContratadaTipoFab',fld:'CONTAGEMRESULTADO_CONTRATADATIPOFAB',pic:'',nv:''},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV6ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'},{av:'AV9GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0}],oparms:[{av:'AV9GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         A493ContagemResultado_DemandaFM = "";
         A457ContagemResultado_Demanda = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A2017ContagemResultado_DataEntregaReal = (DateTime)(DateTime.MinValue);
         A494ContagemResultado_Descricao = "";
         A509ContagemrResultado_SistemaSigla = "";
         A1326ContagemResultado_ContratadaTipoFab = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GridContainer = new GXWebGrid( context);
         AV5Context = new wwpbaseobjects.SdtWWPContext(context);
         scmdbuf = "";
         H00TP3_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00TP3_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00TP3_A489ContagemResultado_SistemaCod = new int[1] ;
         H00TP3_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00TP3_A456ContagemResultado_Codigo = new int[1] ;
         H00TP3_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00TP3_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00TP3_A457ContagemResultado_Demanda = new String[] {""} ;
         H00TP3_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00TP3_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00TP3_A2017ContagemResultado_DataEntregaReal = new DateTime[] {DateTime.MinValue} ;
         H00TP3_n2017ContagemResultado_DataEntregaReal = new bool[] {false} ;
         H00TP3_A494ContagemResultado_Descricao = new String[] {""} ;
         H00TP3_n494ContagemResultado_Descricao = new bool[] {false} ;
         H00TP3_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         H00TP3_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         H00TP3_A602ContagemResultado_OSVinculada = new int[1] ;
         H00TP3_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         H00TP3_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         H00TP3_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         H00TP3_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         H00TP3_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         AV11ContagemResultado_DemandaFM = "";
         AV12ContagemResultado_Demanda = "";
         AV13ContagemResultado_DataDmn = DateTime.MinValue;
         AV14ContagemResultado_DataEntregaReal = (DateTime)(DateTime.MinValue);
         AV15ContagemResultado_Descricao = "";
         AV16ContagemrResultado_SistemaSigla = "";
         AV17ContagemResultado_ContratadaTipoFab = "S";
         GridRow = new GXWebRow();
         H00TP5_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00TP5_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00TP5_A489ContagemResultado_SistemaCod = new int[1] ;
         H00TP5_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00TP5_A456ContagemResultado_Codigo = new int[1] ;
         H00TP5_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00TP5_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00TP5_A457ContagemResultado_Demanda = new String[] {""} ;
         H00TP5_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00TP5_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00TP5_A2017ContagemResultado_DataEntregaReal = new DateTime[] {DateTime.MinValue} ;
         H00TP5_n2017ContagemResultado_DataEntregaReal = new bool[] {false} ;
         H00TP5_A494ContagemResultado_Descricao = new String[] {""} ;
         H00TP5_n494ContagemResultado_Descricao = new bool[] {false} ;
         H00TP5_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         H00TP5_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         H00TP5_A602ContagemResultado_OSVinculada = new int[1] ;
         H00TP5_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         H00TP5_A1326ContagemResultado_ContratadaTipoFab = new String[] {""} ;
         H00TP5_n1326ContagemResultado_ContratadaTipoFab = new bool[] {false} ;
         H00TP5_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         H00TP5_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         GXt_char1 = "";
         GXt_char2 = "";
         GXt_char3 = "";
         GXt_char4 = "";
         GXt_char5 = "";
         GXt_char6 = "";
         GXt_char7 = "";
         GXt_char8 = "";
         GXt_char9 = "";
         GXt_char10 = "";
         GXt_char11 = "";
         GXt_char12 = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         TempTags = "";
         lblTextblocktitle_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_relatoriocomparacaodemandas5__default(),
            new Object[][] {
                new Object[] {
               H00TP3_A490ContagemResultado_ContratadaCod, H00TP3_n490ContagemResultado_ContratadaCod, H00TP3_A489ContagemResultado_SistemaCod, H00TP3_n489ContagemResultado_SistemaCod, H00TP3_A456ContagemResultado_Codigo, H00TP3_A493ContagemResultado_DemandaFM, H00TP3_n493ContagemResultado_DemandaFM, H00TP3_A457ContagemResultado_Demanda, H00TP3_n457ContagemResultado_Demanda, H00TP3_A471ContagemResultado_DataDmn,
               H00TP3_A2017ContagemResultado_DataEntregaReal, H00TP3_n2017ContagemResultado_DataEntregaReal, H00TP3_A494ContagemResultado_Descricao, H00TP3_n494ContagemResultado_Descricao, H00TP3_A509ContagemrResultado_SistemaSigla, H00TP3_n509ContagemrResultado_SistemaSigla, H00TP3_A602ContagemResultado_OSVinculada, H00TP3_n602ContagemResultado_OSVinculada, H00TP3_A1326ContagemResultado_ContratadaTipoFab, H00TP3_n1326ContagemResultado_ContratadaTipoFab,
               H00TP3_A682ContagemResultado_PFBFMUltima, H00TP3_A684ContagemResultado_PFBFSUltima
               }
               , new Object[] {
               H00TP5_A490ContagemResultado_ContratadaCod, H00TP5_n490ContagemResultado_ContratadaCod, H00TP5_A489ContagemResultado_SistemaCod, H00TP5_n489ContagemResultado_SistemaCod, H00TP5_A456ContagemResultado_Codigo, H00TP5_A493ContagemResultado_DemandaFM, H00TP5_n493ContagemResultado_DemandaFM, H00TP5_A457ContagemResultado_Demanda, H00TP5_n457ContagemResultado_Demanda, H00TP5_A471ContagemResultado_DataDmn,
               H00TP5_A2017ContagemResultado_DataEntregaReal, H00TP5_n2017ContagemResultado_DataEntregaReal, H00TP5_A494ContagemResultado_Descricao, H00TP5_n494ContagemResultado_Descricao, H00TP5_A509ContagemrResultado_SistemaSigla, H00TP5_n509ContagemrResultado_SistemaSigla, H00TP5_A602ContagemResultado_OSVinculada, H00TP5_n602ContagemResultado_OSVinculada, H00TP5_A1326ContagemResultado_ContratadaTipoFab, H00TP5_n1326ContagemResultado_ContratadaTipoFab,
               H00TP5_A682ContagemResultado_PFBFMUltima, H00TP5_A684ContagemResultado_PFBFSUltima
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavContagemresultado_codigo_Enabled = 0;
         edtavContagemresultado_osvinculada_Enabled = 0;
      }

      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_35 ;
      private short nGXsfl_35_idx=1 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short nGXWrapped ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_35_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A456ContagemResultado_Codigo ;
      private int A602ContagemResultado_OSVinculada ;
      private int AV6ContagemResultado_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int AV7ContagemResultado_OSVinculada ;
      private int subGrid_Islastpage ;
      private int edtavContagemresultado_codigo_Enabled ;
      private int edtavContagemresultado_osvinculada_Enabled ;
      private int GRID_nGridOutOfScope ;
      private int edtavGridcurrentpage_Visible ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A489ContagemResultado_SistemaCod ;
      private int AV8PageToGo ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV10GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private long AV9GridCurrentPage ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal AV18ContagemResultado_PFBFMUltima ;
      private decimal AV19ContagemResultado_PFBFSUltima ;
      private decimal AV23Esforco ;
      private String Gridpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_35_idx="0001" ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String A1326ContagemResultado_ContratadaTipoFab ;
      private String edtavContagemresultado_codigo_Internalname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavContagemresultado_osvinculada_Internalname ;
      private String edtavGridcurrentpage_Internalname ;
      private String subGrid_Internalname ;
      private String scmdbuf ;
      private String AV16ContagemrResultado_SistemaSigla ;
      private String AV17ContagemResultado_ContratadaTipoFab ;
      private String GXt_char1 ;
      private String GXt_char2 ;
      private String GXt_char3 ;
      private String GXt_char4 ;
      private String GXt_char5 ;
      private String GXt_char6 ;
      private String GXt_char7 ;
      private String GXt_char8 ;
      private String GXt_char9 ;
      private String GXt_char10 ;
      private String GXt_char11 ;
      private String GXt_char12 ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String TempTags ;
      private String edtavGridcurrentpage_Jsonclick ;
      private String tblTableheader_Internalname ;
      private String lblTextblocktitle_Internalname ;
      private String lblTextblocktitle_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String tblTablefiltersextras_Internalname ;
      private String tblTablefiltersb_Internalname ;
      private String tblTablefiltersa_Internalname ;
      private String sGXsfl_35_fel_idx="0001" ;
      private String ROClassString ;
      private String edtavContagemresultado_codigo_Jsonclick ;
      private String edtavContagemresultado_osvinculada_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private DateTime A2017ContagemResultado_DataEntregaReal ;
      private DateTime AV14ContagemResultado_DataEntregaReal ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime AV13ContagemResultado_DataDmn ;
      private bool entryPointCalled ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n457ContagemResultado_Demanda ;
      private bool n2017ContagemResultado_DataEntregaReal ;
      private bool n494ContagemResultado_Descricao ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n1326ContagemResultado_ContratadaTipoFab ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n489ContagemResultado_SistemaCod ;
      private String A493ContagemResultado_DemandaFM ;
      private String A457ContagemResultado_Demanda ;
      private String A494ContagemResultado_Descricao ;
      private String AV11ContagemResultado_DemandaFM ;
      private String AV12ContagemResultado_Demanda ;
      private String AV15ContagemResultado_Descricao ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00TP3_A490ContagemResultado_ContratadaCod ;
      private bool[] H00TP3_n490ContagemResultado_ContratadaCod ;
      private int[] H00TP3_A489ContagemResultado_SistemaCod ;
      private bool[] H00TP3_n489ContagemResultado_SistemaCod ;
      private int[] H00TP3_A456ContagemResultado_Codigo ;
      private String[] H00TP3_A493ContagemResultado_DemandaFM ;
      private bool[] H00TP3_n493ContagemResultado_DemandaFM ;
      private String[] H00TP3_A457ContagemResultado_Demanda ;
      private bool[] H00TP3_n457ContagemResultado_Demanda ;
      private DateTime[] H00TP3_A471ContagemResultado_DataDmn ;
      private DateTime[] H00TP3_A2017ContagemResultado_DataEntregaReal ;
      private bool[] H00TP3_n2017ContagemResultado_DataEntregaReal ;
      private String[] H00TP3_A494ContagemResultado_Descricao ;
      private bool[] H00TP3_n494ContagemResultado_Descricao ;
      private String[] H00TP3_A509ContagemrResultado_SistemaSigla ;
      private bool[] H00TP3_n509ContagemrResultado_SistemaSigla ;
      private int[] H00TP3_A602ContagemResultado_OSVinculada ;
      private bool[] H00TP3_n602ContagemResultado_OSVinculada ;
      private String[] H00TP3_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] H00TP3_n1326ContagemResultado_ContratadaTipoFab ;
      private decimal[] H00TP3_A682ContagemResultado_PFBFMUltima ;
      private decimal[] H00TP3_A684ContagemResultado_PFBFSUltima ;
      private int[] H00TP5_A490ContagemResultado_ContratadaCod ;
      private bool[] H00TP5_n490ContagemResultado_ContratadaCod ;
      private int[] H00TP5_A489ContagemResultado_SistemaCod ;
      private bool[] H00TP5_n489ContagemResultado_SistemaCod ;
      private int[] H00TP5_A456ContagemResultado_Codigo ;
      private String[] H00TP5_A493ContagemResultado_DemandaFM ;
      private bool[] H00TP5_n493ContagemResultado_DemandaFM ;
      private String[] H00TP5_A457ContagemResultado_Demanda ;
      private bool[] H00TP5_n457ContagemResultado_Demanda ;
      private DateTime[] H00TP5_A471ContagemResultado_DataDmn ;
      private DateTime[] H00TP5_A2017ContagemResultado_DataEntregaReal ;
      private bool[] H00TP5_n2017ContagemResultado_DataEntregaReal ;
      private String[] H00TP5_A494ContagemResultado_Descricao ;
      private bool[] H00TP5_n494ContagemResultado_Descricao ;
      private String[] H00TP5_A509ContagemrResultado_SistemaSigla ;
      private bool[] H00TP5_n509ContagemrResultado_SistemaSigla ;
      private int[] H00TP5_A602ContagemResultado_OSVinculada ;
      private bool[] H00TP5_n602ContagemResultado_OSVinculada ;
      private String[] H00TP5_A1326ContagemResultado_ContratadaTipoFab ;
      private bool[] H00TP5_n1326ContagemResultado_ContratadaTipoFab ;
      private decimal[] H00TP5_A682ContagemResultado_PFBFMUltima ;
      private decimal[] H00TP5_A684ContagemResultado_PFBFSUltima ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV5Context ;
   }

   public class wp_relatoriocomparacaodemandas5__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00TP3 ;
          prmH00TP3 = new Object[] {
          } ;
          Object[] prmH00TP5 ;
          prmH00TP5 = new Object[] {
          new Object[] {"@AV6ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00TP3", "SELECT T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DataEntregaReal], T1.[ContagemResultado_Descricao], T3.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_OSVinculada], T2.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, COALESCE( T4.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T4.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima FROM ((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T4 ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_Codigo] IN (97155,93435) ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TP3,100,0,true,false )
             ,new CursorDef("H00TP5", "SELECT T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DataEntregaReal], T1.[ContagemResultado_Descricao], T3.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_OSVinculada], T2.[Contratada_TipoFabrica] AS ContagemResultado_ContratadaTipoFab, COALESCE( T4.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T4.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima FROM ((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T4 ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @AV6ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TP5,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(6) ;
                ((DateTime[]) buf[10])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 25) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((decimal[]) buf[20])[0] = rslt.getDecimal(12) ;
                ((decimal[]) buf[21])[0] = rslt.getDecimal(13) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(6) ;
                ((DateTime[]) buf[10])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 25) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((decimal[]) buf[20])[0] = rslt.getDecimal(12) ;
                ((decimal[]) buf[21])[0] = rslt.getDecimal(13) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
