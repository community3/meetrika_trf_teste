/*
               File: ContagemResultadoNotas_BC
        Description: Nota da OS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:17:36.89
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadonotas_bc : GXHttpHandler, IGxSilentTrn
   {
      public contagemresultadonotas_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemresultadonotas_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow3K161( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey3K161( ) ;
         standaloneModal( ) ;
         AddRow3K161( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z1331ContagemResultadoNota_Codigo = A1331ContagemResultadoNota_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_3K0( )
      {
         BeforeValidate3K161( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls3K161( ) ;
            }
            else
            {
               CheckExtendedTable3K161( ) ;
               if ( AnyError == 0 )
               {
                  ZM3K161( 4) ;
                  ZM3K161( 5) ;
                  ZM3K161( 6) ;
               }
               CloseExtendedTableCursors3K161( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM3K161( short GX_JID )
      {
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            Z1332ContagemResultadoNota_DataHora = A1332ContagemResultadoNota_DataHora;
            Z1857ContagemResultadoNotas_Ativo = A1857ContagemResultadoNotas_Ativo;
            Z1327ContagemResultadoNota_DemandaCod = A1327ContagemResultadoNota_DemandaCod;
            Z1328ContagemResultadoNota_UsuarioCod = A1328ContagemResultadoNota_UsuarioCod;
         }
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 5 ) || ( GX_JID == 0 ) )
         {
            Z1329ContagemResultadoNota_UsuarioPesCod = A1329ContagemResultadoNota_UsuarioPesCod;
         }
         if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
         {
            Z1330ContagemResultadoNota_UsuarioPesNom = A1330ContagemResultadoNota_UsuarioPesNom;
         }
         if ( GX_JID == -3 )
         {
            Z1331ContagemResultadoNota_Codigo = A1331ContagemResultadoNota_Codigo;
            Z1332ContagemResultadoNota_DataHora = A1332ContagemResultadoNota_DataHora;
            Z1334ContagemResultadoNota_Nota = A1334ContagemResultadoNota_Nota;
            Z1857ContagemResultadoNotas_Ativo = A1857ContagemResultadoNotas_Ativo;
            Z1327ContagemResultadoNota_DemandaCod = A1327ContagemResultadoNota_DemandaCod;
            Z1328ContagemResultadoNota_UsuarioCod = A1328ContagemResultadoNota_UsuarioCod;
            Z1329ContagemResultadoNota_UsuarioPesCod = A1329ContagemResultadoNota_UsuarioPesCod;
            Z1330ContagemResultadoNota_UsuarioPesNom = A1330ContagemResultadoNota_UsuarioPesNom;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A1857ContagemResultadoNotas_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A1857ContagemResultadoNotas_Ativo = true;
            n1857ContagemResultadoNotas_Ativo = false;
         }
      }

      protected void Load3K161( )
      {
         /* Using cursor BC003K7 */
         pr_default.execute(5, new Object[] {A1331ContagemResultadoNota_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound161 = 1;
            A1332ContagemResultadoNota_DataHora = BC003K7_A1332ContagemResultadoNota_DataHora[0];
            A1334ContagemResultadoNota_Nota = BC003K7_A1334ContagemResultadoNota_Nota[0];
            A1330ContagemResultadoNota_UsuarioPesNom = BC003K7_A1330ContagemResultadoNota_UsuarioPesNom[0];
            n1330ContagemResultadoNota_UsuarioPesNom = BC003K7_n1330ContagemResultadoNota_UsuarioPesNom[0];
            A1857ContagemResultadoNotas_Ativo = BC003K7_A1857ContagemResultadoNotas_Ativo[0];
            n1857ContagemResultadoNotas_Ativo = BC003K7_n1857ContagemResultadoNotas_Ativo[0];
            A1327ContagemResultadoNota_DemandaCod = BC003K7_A1327ContagemResultadoNota_DemandaCod[0];
            A1328ContagemResultadoNota_UsuarioCod = BC003K7_A1328ContagemResultadoNota_UsuarioCod[0];
            A1329ContagemResultadoNota_UsuarioPesCod = BC003K7_A1329ContagemResultadoNota_UsuarioPesCod[0];
            n1329ContagemResultadoNota_UsuarioPesCod = BC003K7_n1329ContagemResultadoNota_UsuarioPesCod[0];
            ZM3K161( -3) ;
         }
         pr_default.close(5);
         OnLoadActions3K161( ) ;
      }

      protected void OnLoadActions3K161( )
      {
      }

      protected void CheckExtendedTable3K161( )
      {
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A1332ContagemResultadoNota_DataHora) || ( A1332ContagemResultadoNota_DataHora >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC003K4 */
         pr_default.execute(2, new Object[] {A1327ContagemResultadoNota_DemandaCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Nota_Contagem Resultado'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADONOTA_DEMANDACOD");
            AnyError = 1;
         }
         pr_default.close(2);
         /* Using cursor BC003K5 */
         pr_default.execute(3, new Object[] {A1328ContagemResultadoNota_UsuarioCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Nota_Usuario'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADONOTA_USUARIOCOD");
            AnyError = 1;
         }
         A1329ContagemResultadoNota_UsuarioPesCod = BC003K5_A1329ContagemResultadoNota_UsuarioPesCod[0];
         n1329ContagemResultadoNota_UsuarioPesCod = BC003K5_n1329ContagemResultadoNota_UsuarioPesCod[0];
         pr_default.close(3);
         /* Using cursor BC003K6 */
         pr_default.execute(4, new Object[] {n1329ContagemResultadoNota_UsuarioPesCod, A1329ContagemResultadoNota_UsuarioPesCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1330ContagemResultadoNota_UsuarioPesNom = BC003K6_A1330ContagemResultadoNota_UsuarioPesNom[0];
         n1330ContagemResultadoNota_UsuarioPesNom = BC003K6_n1330ContagemResultadoNota_UsuarioPesNom[0];
         pr_default.close(4);
      }

      protected void CloseExtendedTableCursors3K161( )
      {
         pr_default.close(2);
         pr_default.close(3);
         pr_default.close(4);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey3K161( )
      {
         /* Using cursor BC003K8 */
         pr_default.execute(6, new Object[] {A1331ContagemResultadoNota_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound161 = 1;
         }
         else
         {
            RcdFound161 = 0;
         }
         pr_default.close(6);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC003K3 */
         pr_default.execute(1, new Object[] {A1331ContagemResultadoNota_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3K161( 3) ;
            RcdFound161 = 1;
            A1331ContagemResultadoNota_Codigo = BC003K3_A1331ContagemResultadoNota_Codigo[0];
            A1332ContagemResultadoNota_DataHora = BC003K3_A1332ContagemResultadoNota_DataHora[0];
            A1334ContagemResultadoNota_Nota = BC003K3_A1334ContagemResultadoNota_Nota[0];
            A1857ContagemResultadoNotas_Ativo = BC003K3_A1857ContagemResultadoNotas_Ativo[0];
            n1857ContagemResultadoNotas_Ativo = BC003K3_n1857ContagemResultadoNotas_Ativo[0];
            A1327ContagemResultadoNota_DemandaCod = BC003K3_A1327ContagemResultadoNota_DemandaCod[0];
            A1328ContagemResultadoNota_UsuarioCod = BC003K3_A1328ContagemResultadoNota_UsuarioCod[0];
            Z1331ContagemResultadoNota_Codigo = A1331ContagemResultadoNota_Codigo;
            sMode161 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load3K161( ) ;
            if ( AnyError == 1 )
            {
               RcdFound161 = 0;
               InitializeNonKey3K161( ) ;
            }
            Gx_mode = sMode161;
         }
         else
         {
            RcdFound161 = 0;
            InitializeNonKey3K161( ) ;
            sMode161 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode161;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey3K161( ) ;
         if ( RcdFound161 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_3K0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency3K161( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC003K2 */
            pr_default.execute(0, new Object[] {A1331ContagemResultadoNota_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoNotas"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1332ContagemResultadoNota_DataHora != BC003K2_A1332ContagemResultadoNota_DataHora[0] ) || ( Z1857ContagemResultadoNotas_Ativo != BC003K2_A1857ContagemResultadoNotas_Ativo[0] ) || ( Z1327ContagemResultadoNota_DemandaCod != BC003K2_A1327ContagemResultadoNota_DemandaCod[0] ) || ( Z1328ContagemResultadoNota_UsuarioCod != BC003K2_A1328ContagemResultadoNota_UsuarioCod[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultadoNotas"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3K161( )
      {
         BeforeValidate3K161( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3K161( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3K161( 0) ;
            CheckOptimisticConcurrency3K161( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3K161( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3K161( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC003K9 */
                     pr_default.execute(7, new Object[] {A1332ContagemResultadoNota_DataHora, A1334ContagemResultadoNota_Nota, n1857ContagemResultadoNotas_Ativo, A1857ContagemResultadoNotas_Ativo, A1327ContagemResultadoNota_DemandaCod, A1328ContagemResultadoNota_UsuarioCod});
                     A1331ContagemResultadoNota_Codigo = BC003K9_A1331ContagemResultadoNota_Codigo[0];
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoNotas") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3K161( ) ;
            }
            EndLevel3K161( ) ;
         }
         CloseExtendedTableCursors3K161( ) ;
      }

      protected void Update3K161( )
      {
         BeforeValidate3K161( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3K161( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3K161( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3K161( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3K161( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC003K10 */
                     pr_default.execute(8, new Object[] {A1332ContagemResultadoNota_DataHora, A1334ContagemResultadoNota_Nota, n1857ContagemResultadoNotas_Ativo, A1857ContagemResultadoNotas_Ativo, A1327ContagemResultadoNota_DemandaCod, A1328ContagemResultadoNota_UsuarioCod, A1331ContagemResultadoNota_Codigo});
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoNotas") ;
                     if ( (pr_default.getStatus(8) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoNotas"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3K161( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3K161( ) ;
         }
         CloseExtendedTableCursors3K161( ) ;
      }

      protected void DeferredUpdate3K161( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate3K161( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3K161( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3K161( ) ;
            AfterConfirm3K161( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3K161( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC003K11 */
                  pr_default.execute(9, new Object[] {A1331ContagemResultadoNota_Codigo});
                  pr_default.close(9);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoNotas") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode161 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel3K161( ) ;
         Gx_mode = sMode161;
      }

      protected void OnDeleteControls3K161( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC003K12 */
            pr_default.execute(10, new Object[] {A1328ContagemResultadoNota_UsuarioCod});
            A1329ContagemResultadoNota_UsuarioPesCod = BC003K12_A1329ContagemResultadoNota_UsuarioPesCod[0];
            n1329ContagemResultadoNota_UsuarioPesCod = BC003K12_n1329ContagemResultadoNota_UsuarioPesCod[0];
            pr_default.close(10);
            /* Using cursor BC003K13 */
            pr_default.execute(11, new Object[] {n1329ContagemResultadoNota_UsuarioPesCod, A1329ContagemResultadoNota_UsuarioPesCod});
            A1330ContagemResultadoNota_UsuarioPesNom = BC003K13_A1330ContagemResultadoNota_UsuarioPesNom[0];
            n1330ContagemResultadoNota_UsuarioPesNom = BC003K13_n1330ContagemResultadoNota_UsuarioPesNom[0];
            pr_default.close(11);
         }
      }

      protected void EndLevel3K161( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3K161( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart3K161( )
      {
         /* Using cursor BC003K14 */
         pr_default.execute(12, new Object[] {A1331ContagemResultadoNota_Codigo});
         RcdFound161 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound161 = 1;
            A1331ContagemResultadoNota_Codigo = BC003K14_A1331ContagemResultadoNota_Codigo[0];
            A1332ContagemResultadoNota_DataHora = BC003K14_A1332ContagemResultadoNota_DataHora[0];
            A1334ContagemResultadoNota_Nota = BC003K14_A1334ContagemResultadoNota_Nota[0];
            A1330ContagemResultadoNota_UsuarioPesNom = BC003K14_A1330ContagemResultadoNota_UsuarioPesNom[0];
            n1330ContagemResultadoNota_UsuarioPesNom = BC003K14_n1330ContagemResultadoNota_UsuarioPesNom[0];
            A1857ContagemResultadoNotas_Ativo = BC003K14_A1857ContagemResultadoNotas_Ativo[0];
            n1857ContagemResultadoNotas_Ativo = BC003K14_n1857ContagemResultadoNotas_Ativo[0];
            A1327ContagemResultadoNota_DemandaCod = BC003K14_A1327ContagemResultadoNota_DemandaCod[0];
            A1328ContagemResultadoNota_UsuarioCod = BC003K14_A1328ContagemResultadoNota_UsuarioCod[0];
            A1329ContagemResultadoNota_UsuarioPesCod = BC003K14_A1329ContagemResultadoNota_UsuarioPesCod[0];
            n1329ContagemResultadoNota_UsuarioPesCod = BC003K14_n1329ContagemResultadoNota_UsuarioPesCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext3K161( )
      {
         /* Scan next routine */
         pr_default.readNext(12);
         RcdFound161 = 0;
         ScanKeyLoad3K161( ) ;
      }

      protected void ScanKeyLoad3K161( )
      {
         sMode161 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound161 = 1;
            A1331ContagemResultadoNota_Codigo = BC003K14_A1331ContagemResultadoNota_Codigo[0];
            A1332ContagemResultadoNota_DataHora = BC003K14_A1332ContagemResultadoNota_DataHora[0];
            A1334ContagemResultadoNota_Nota = BC003K14_A1334ContagemResultadoNota_Nota[0];
            A1330ContagemResultadoNota_UsuarioPesNom = BC003K14_A1330ContagemResultadoNota_UsuarioPesNom[0];
            n1330ContagemResultadoNota_UsuarioPesNom = BC003K14_n1330ContagemResultadoNota_UsuarioPesNom[0];
            A1857ContagemResultadoNotas_Ativo = BC003K14_A1857ContagemResultadoNotas_Ativo[0];
            n1857ContagemResultadoNotas_Ativo = BC003K14_n1857ContagemResultadoNotas_Ativo[0];
            A1327ContagemResultadoNota_DemandaCod = BC003K14_A1327ContagemResultadoNota_DemandaCod[0];
            A1328ContagemResultadoNota_UsuarioCod = BC003K14_A1328ContagemResultadoNota_UsuarioCod[0];
            A1329ContagemResultadoNota_UsuarioPesCod = BC003K14_A1329ContagemResultadoNota_UsuarioPesCod[0];
            n1329ContagemResultadoNota_UsuarioPesCod = BC003K14_n1329ContagemResultadoNota_UsuarioPesCod[0];
         }
         Gx_mode = sMode161;
      }

      protected void ScanKeyEnd3K161( )
      {
         pr_default.close(12);
      }

      protected void AfterConfirm3K161( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3K161( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3K161( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3K161( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3K161( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3K161( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3K161( )
      {
      }

      protected void AddRow3K161( )
      {
         VarsToRow161( bcContagemResultadoNotas) ;
      }

      protected void ReadRow3K161( )
      {
         RowToVars161( bcContagemResultadoNotas, 1) ;
      }

      protected void InitializeNonKey3K161( )
      {
         A1332ContagemResultadoNota_DataHora = (DateTime)(DateTime.MinValue);
         A1334ContagemResultadoNota_Nota = "";
         A1327ContagemResultadoNota_DemandaCod = 0;
         A1328ContagemResultadoNota_UsuarioCod = 0;
         A1329ContagemResultadoNota_UsuarioPesCod = 0;
         n1329ContagemResultadoNota_UsuarioPesCod = false;
         A1330ContagemResultadoNota_UsuarioPesNom = "";
         n1330ContagemResultadoNota_UsuarioPesNom = false;
         A1857ContagemResultadoNotas_Ativo = true;
         n1857ContagemResultadoNotas_Ativo = false;
         Z1332ContagemResultadoNota_DataHora = (DateTime)(DateTime.MinValue);
         Z1857ContagemResultadoNotas_Ativo = false;
         Z1327ContagemResultadoNota_DemandaCod = 0;
         Z1328ContagemResultadoNota_UsuarioCod = 0;
      }

      protected void InitAll3K161( )
      {
         A1331ContagemResultadoNota_Codigo = 0;
         InitializeNonKey3K161( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A1857ContagemResultadoNotas_Ativo = i1857ContagemResultadoNotas_Ativo;
         n1857ContagemResultadoNotas_Ativo = false;
      }

      public void VarsToRow161( SdtContagemResultadoNotas obj161 )
      {
         obj161.gxTpr_Mode = Gx_mode;
         obj161.gxTpr_Contagemresultadonota_datahora = A1332ContagemResultadoNota_DataHora;
         obj161.gxTpr_Contagemresultadonota_nota = A1334ContagemResultadoNota_Nota;
         obj161.gxTpr_Contagemresultadonota_demandacod = A1327ContagemResultadoNota_DemandaCod;
         obj161.gxTpr_Contagemresultadonota_usuariocod = A1328ContagemResultadoNota_UsuarioCod;
         obj161.gxTpr_Contagemresultadonota_usuariopescod = A1329ContagemResultadoNota_UsuarioPesCod;
         obj161.gxTpr_Contagemresultadonota_usuariopesnom = A1330ContagemResultadoNota_UsuarioPesNom;
         obj161.gxTpr_Contagemresultadonotas_ativo = A1857ContagemResultadoNotas_Ativo;
         obj161.gxTpr_Contagemresultadonota_codigo = A1331ContagemResultadoNota_Codigo;
         obj161.gxTpr_Contagemresultadonota_codigo_Z = Z1331ContagemResultadoNota_Codigo;
         obj161.gxTpr_Contagemresultadonota_datahora_Z = Z1332ContagemResultadoNota_DataHora;
         obj161.gxTpr_Contagemresultadonota_demandacod_Z = Z1327ContagemResultadoNota_DemandaCod;
         obj161.gxTpr_Contagemresultadonota_usuariocod_Z = Z1328ContagemResultadoNota_UsuarioCod;
         obj161.gxTpr_Contagemresultadonota_usuariopescod_Z = Z1329ContagemResultadoNota_UsuarioPesCod;
         obj161.gxTpr_Contagemresultadonota_usuariopesnom_Z = Z1330ContagemResultadoNota_UsuarioPesNom;
         obj161.gxTpr_Contagemresultadonotas_ativo_Z = Z1857ContagemResultadoNotas_Ativo;
         obj161.gxTpr_Contagemresultadonota_usuariopescod_N = (short)(Convert.ToInt16(n1329ContagemResultadoNota_UsuarioPesCod));
         obj161.gxTpr_Contagemresultadonota_usuariopesnom_N = (short)(Convert.ToInt16(n1330ContagemResultadoNota_UsuarioPesNom));
         obj161.gxTpr_Contagemresultadonotas_ativo_N = (short)(Convert.ToInt16(n1857ContagemResultadoNotas_Ativo));
         obj161.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow161( SdtContagemResultadoNotas obj161 )
      {
         obj161.gxTpr_Contagemresultadonota_codigo = A1331ContagemResultadoNota_Codigo;
         return  ;
      }

      public void RowToVars161( SdtContagemResultadoNotas obj161 ,
                                int forceLoad )
      {
         Gx_mode = obj161.gxTpr_Mode;
         A1332ContagemResultadoNota_DataHora = obj161.gxTpr_Contagemresultadonota_datahora;
         A1334ContagemResultadoNota_Nota = obj161.gxTpr_Contagemresultadonota_nota;
         A1327ContagemResultadoNota_DemandaCod = obj161.gxTpr_Contagemresultadonota_demandacod;
         A1328ContagemResultadoNota_UsuarioCod = obj161.gxTpr_Contagemresultadonota_usuariocod;
         A1329ContagemResultadoNota_UsuarioPesCod = obj161.gxTpr_Contagemresultadonota_usuariopescod;
         n1329ContagemResultadoNota_UsuarioPesCod = false;
         A1330ContagemResultadoNota_UsuarioPesNom = obj161.gxTpr_Contagemresultadonota_usuariopesnom;
         n1330ContagemResultadoNota_UsuarioPesNom = false;
         A1857ContagemResultadoNotas_Ativo = obj161.gxTpr_Contagemresultadonotas_ativo;
         n1857ContagemResultadoNotas_Ativo = false;
         A1331ContagemResultadoNota_Codigo = obj161.gxTpr_Contagemresultadonota_codigo;
         Z1331ContagemResultadoNota_Codigo = obj161.gxTpr_Contagemresultadonota_codigo_Z;
         Z1332ContagemResultadoNota_DataHora = obj161.gxTpr_Contagemresultadonota_datahora_Z;
         Z1327ContagemResultadoNota_DemandaCod = obj161.gxTpr_Contagemresultadonota_demandacod_Z;
         Z1328ContagemResultadoNota_UsuarioCod = obj161.gxTpr_Contagemresultadonota_usuariocod_Z;
         Z1329ContagemResultadoNota_UsuarioPesCod = obj161.gxTpr_Contagemresultadonota_usuariopescod_Z;
         Z1330ContagemResultadoNota_UsuarioPesNom = obj161.gxTpr_Contagemresultadonota_usuariopesnom_Z;
         Z1857ContagemResultadoNotas_Ativo = obj161.gxTpr_Contagemresultadonotas_ativo_Z;
         n1329ContagemResultadoNota_UsuarioPesCod = (bool)(Convert.ToBoolean(obj161.gxTpr_Contagemresultadonota_usuariopescod_N));
         n1330ContagemResultadoNota_UsuarioPesNom = (bool)(Convert.ToBoolean(obj161.gxTpr_Contagemresultadonota_usuariopesnom_N));
         n1857ContagemResultadoNotas_Ativo = (bool)(Convert.ToBoolean(obj161.gxTpr_Contagemresultadonotas_ativo_N));
         Gx_mode = obj161.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A1331ContagemResultadoNota_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey3K161( ) ;
         ScanKeyStart3K161( ) ;
         if ( RcdFound161 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1331ContagemResultadoNota_Codigo = A1331ContagemResultadoNota_Codigo;
         }
         ZM3K161( -3) ;
         OnLoadActions3K161( ) ;
         AddRow3K161( ) ;
         ScanKeyEnd3K161( ) ;
         if ( RcdFound161 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars161( bcContagemResultadoNotas, 0) ;
         ScanKeyStart3K161( ) ;
         if ( RcdFound161 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1331ContagemResultadoNota_Codigo = A1331ContagemResultadoNota_Codigo;
         }
         ZM3K161( -3) ;
         OnLoadActions3K161( ) ;
         AddRow3K161( ) ;
         ScanKeyEnd3K161( ) ;
         if ( RcdFound161 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars161( bcContagemResultadoNotas, 0) ;
         nKeyPressed = 1;
         GetKey3K161( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert3K161( ) ;
         }
         else
         {
            if ( RcdFound161 == 1 )
            {
               if ( A1331ContagemResultadoNota_Codigo != Z1331ContagemResultadoNota_Codigo )
               {
                  A1331ContagemResultadoNota_Codigo = Z1331ContagemResultadoNota_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update3K161( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A1331ContagemResultadoNota_Codigo != Z1331ContagemResultadoNota_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert3K161( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert3K161( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow161( bcContagemResultadoNotas) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars161( bcContagemResultadoNotas, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey3K161( ) ;
         if ( RcdFound161 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A1331ContagemResultadoNota_Codigo != Z1331ContagemResultadoNota_Codigo )
            {
               A1331ContagemResultadoNota_Codigo = Z1331ContagemResultadoNota_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A1331ContagemResultadoNota_Codigo != Z1331ContagemResultadoNota_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(10);
         pr_default.close(11);
         context.RollbackDataStores( "ContagemResultadoNotas_BC");
         VarsToRow161( bcContagemResultadoNotas) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcContagemResultadoNotas.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcContagemResultadoNotas.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcContagemResultadoNotas )
         {
            bcContagemResultadoNotas = (SdtContagemResultadoNotas)(sdt);
            if ( StringUtil.StrCmp(bcContagemResultadoNotas.gxTpr_Mode, "") == 0 )
            {
               bcContagemResultadoNotas.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow161( bcContagemResultadoNotas) ;
            }
            else
            {
               RowToVars161( bcContagemResultadoNotas, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcContagemResultadoNotas.gxTpr_Mode, "") == 0 )
            {
               bcContagemResultadoNotas.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars161( bcContagemResultadoNotas, 1) ;
         return  ;
      }

      public SdtContagemResultadoNotas ContagemResultadoNotas_BC
      {
         get {
            return bcContagemResultadoNotas ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(10);
         pr_default.close(11);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z1332ContagemResultadoNota_DataHora = (DateTime)(DateTime.MinValue);
         A1332ContagemResultadoNota_DataHora = (DateTime)(DateTime.MinValue);
         Z1330ContagemResultadoNota_UsuarioPesNom = "";
         A1330ContagemResultadoNota_UsuarioPesNom = "";
         Z1334ContagemResultadoNota_Nota = "";
         A1334ContagemResultadoNota_Nota = "";
         BC003K7_A1331ContagemResultadoNota_Codigo = new int[1] ;
         BC003K7_A1332ContagemResultadoNota_DataHora = new DateTime[] {DateTime.MinValue} ;
         BC003K7_A1334ContagemResultadoNota_Nota = new String[] {""} ;
         BC003K7_A1330ContagemResultadoNota_UsuarioPesNom = new String[] {""} ;
         BC003K7_n1330ContagemResultadoNota_UsuarioPesNom = new bool[] {false} ;
         BC003K7_A1857ContagemResultadoNotas_Ativo = new bool[] {false} ;
         BC003K7_n1857ContagemResultadoNotas_Ativo = new bool[] {false} ;
         BC003K7_A1327ContagemResultadoNota_DemandaCod = new int[1] ;
         BC003K7_A1328ContagemResultadoNota_UsuarioCod = new int[1] ;
         BC003K7_A1329ContagemResultadoNota_UsuarioPesCod = new int[1] ;
         BC003K7_n1329ContagemResultadoNota_UsuarioPesCod = new bool[] {false} ;
         BC003K4_A1327ContagemResultadoNota_DemandaCod = new int[1] ;
         BC003K5_A1329ContagemResultadoNota_UsuarioPesCod = new int[1] ;
         BC003K5_n1329ContagemResultadoNota_UsuarioPesCod = new bool[] {false} ;
         BC003K6_A1330ContagemResultadoNota_UsuarioPesNom = new String[] {""} ;
         BC003K6_n1330ContagemResultadoNota_UsuarioPesNom = new bool[] {false} ;
         BC003K8_A1331ContagemResultadoNota_Codigo = new int[1] ;
         BC003K3_A1331ContagemResultadoNota_Codigo = new int[1] ;
         BC003K3_A1332ContagemResultadoNota_DataHora = new DateTime[] {DateTime.MinValue} ;
         BC003K3_A1334ContagemResultadoNota_Nota = new String[] {""} ;
         BC003K3_A1857ContagemResultadoNotas_Ativo = new bool[] {false} ;
         BC003K3_n1857ContagemResultadoNotas_Ativo = new bool[] {false} ;
         BC003K3_A1327ContagemResultadoNota_DemandaCod = new int[1] ;
         BC003K3_A1328ContagemResultadoNota_UsuarioCod = new int[1] ;
         sMode161 = "";
         BC003K2_A1331ContagemResultadoNota_Codigo = new int[1] ;
         BC003K2_A1332ContagemResultadoNota_DataHora = new DateTime[] {DateTime.MinValue} ;
         BC003K2_A1334ContagemResultadoNota_Nota = new String[] {""} ;
         BC003K2_A1857ContagemResultadoNotas_Ativo = new bool[] {false} ;
         BC003K2_n1857ContagemResultadoNotas_Ativo = new bool[] {false} ;
         BC003K2_A1327ContagemResultadoNota_DemandaCod = new int[1] ;
         BC003K2_A1328ContagemResultadoNota_UsuarioCod = new int[1] ;
         BC003K9_A1331ContagemResultadoNota_Codigo = new int[1] ;
         BC003K12_A1329ContagemResultadoNota_UsuarioPesCod = new int[1] ;
         BC003K12_n1329ContagemResultadoNota_UsuarioPesCod = new bool[] {false} ;
         BC003K13_A1330ContagemResultadoNota_UsuarioPesNom = new String[] {""} ;
         BC003K13_n1330ContagemResultadoNota_UsuarioPesNom = new bool[] {false} ;
         BC003K14_A1331ContagemResultadoNota_Codigo = new int[1] ;
         BC003K14_A1332ContagemResultadoNota_DataHora = new DateTime[] {DateTime.MinValue} ;
         BC003K14_A1334ContagemResultadoNota_Nota = new String[] {""} ;
         BC003K14_A1330ContagemResultadoNota_UsuarioPesNom = new String[] {""} ;
         BC003K14_n1330ContagemResultadoNota_UsuarioPesNom = new bool[] {false} ;
         BC003K14_A1857ContagemResultadoNotas_Ativo = new bool[] {false} ;
         BC003K14_n1857ContagemResultadoNotas_Ativo = new bool[] {false} ;
         BC003K14_A1327ContagemResultadoNota_DemandaCod = new int[1] ;
         BC003K14_A1328ContagemResultadoNota_UsuarioCod = new int[1] ;
         BC003K14_A1329ContagemResultadoNota_UsuarioPesCod = new int[1] ;
         BC003K14_n1329ContagemResultadoNota_UsuarioPesCod = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadonotas_bc__default(),
            new Object[][] {
                new Object[] {
               BC003K2_A1331ContagemResultadoNota_Codigo, BC003K2_A1332ContagemResultadoNota_DataHora, BC003K2_A1334ContagemResultadoNota_Nota, BC003K2_A1857ContagemResultadoNotas_Ativo, BC003K2_n1857ContagemResultadoNotas_Ativo, BC003K2_A1327ContagemResultadoNota_DemandaCod, BC003K2_A1328ContagemResultadoNota_UsuarioCod
               }
               , new Object[] {
               BC003K3_A1331ContagemResultadoNota_Codigo, BC003K3_A1332ContagemResultadoNota_DataHora, BC003K3_A1334ContagemResultadoNota_Nota, BC003K3_A1857ContagemResultadoNotas_Ativo, BC003K3_n1857ContagemResultadoNotas_Ativo, BC003K3_A1327ContagemResultadoNota_DemandaCod, BC003K3_A1328ContagemResultadoNota_UsuarioCod
               }
               , new Object[] {
               BC003K4_A1327ContagemResultadoNota_DemandaCod
               }
               , new Object[] {
               BC003K5_A1329ContagemResultadoNota_UsuarioPesCod, BC003K5_n1329ContagemResultadoNota_UsuarioPesCod
               }
               , new Object[] {
               BC003K6_A1330ContagemResultadoNota_UsuarioPesNom, BC003K6_n1330ContagemResultadoNota_UsuarioPesNom
               }
               , new Object[] {
               BC003K7_A1331ContagemResultadoNota_Codigo, BC003K7_A1332ContagemResultadoNota_DataHora, BC003K7_A1334ContagemResultadoNota_Nota, BC003K7_A1330ContagemResultadoNota_UsuarioPesNom, BC003K7_n1330ContagemResultadoNota_UsuarioPesNom, BC003K7_A1857ContagemResultadoNotas_Ativo, BC003K7_n1857ContagemResultadoNotas_Ativo, BC003K7_A1327ContagemResultadoNota_DemandaCod, BC003K7_A1328ContagemResultadoNota_UsuarioCod, BC003K7_A1329ContagemResultadoNota_UsuarioPesCod,
               BC003K7_n1329ContagemResultadoNota_UsuarioPesCod
               }
               , new Object[] {
               BC003K8_A1331ContagemResultadoNota_Codigo
               }
               , new Object[] {
               BC003K9_A1331ContagemResultadoNota_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC003K12_A1329ContagemResultadoNota_UsuarioPesCod, BC003K12_n1329ContagemResultadoNota_UsuarioPesCod
               }
               , new Object[] {
               BC003K13_A1330ContagemResultadoNota_UsuarioPesNom, BC003K13_n1330ContagemResultadoNota_UsuarioPesNom
               }
               , new Object[] {
               BC003K14_A1331ContagemResultadoNota_Codigo, BC003K14_A1332ContagemResultadoNota_DataHora, BC003K14_A1334ContagemResultadoNota_Nota, BC003K14_A1330ContagemResultadoNota_UsuarioPesNom, BC003K14_n1330ContagemResultadoNota_UsuarioPesNom, BC003K14_A1857ContagemResultadoNotas_Ativo, BC003K14_n1857ContagemResultadoNotas_Ativo, BC003K14_A1327ContagemResultadoNota_DemandaCod, BC003K14_A1328ContagemResultadoNota_UsuarioCod, BC003K14_A1329ContagemResultadoNota_UsuarioPesCod,
               BC003K14_n1329ContagemResultadoNota_UsuarioPesCod
               }
            }
         );
         Z1857ContagemResultadoNotas_Ativo = true;
         n1857ContagemResultadoNotas_Ativo = false;
         A1857ContagemResultadoNotas_Ativo = true;
         n1857ContagemResultadoNotas_Ativo = false;
         i1857ContagemResultadoNotas_Ativo = true;
         n1857ContagemResultadoNotas_Ativo = false;
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short RcdFound161 ;
      private int trnEnded ;
      private int Z1331ContagemResultadoNota_Codigo ;
      private int A1331ContagemResultadoNota_Codigo ;
      private int Z1327ContagemResultadoNota_DemandaCod ;
      private int A1327ContagemResultadoNota_DemandaCod ;
      private int Z1328ContagemResultadoNota_UsuarioCod ;
      private int A1328ContagemResultadoNota_UsuarioCod ;
      private int Z1329ContagemResultadoNota_UsuarioPesCod ;
      private int A1329ContagemResultadoNota_UsuarioPesCod ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String Z1330ContagemResultadoNota_UsuarioPesNom ;
      private String A1330ContagemResultadoNota_UsuarioPesNom ;
      private String sMode161 ;
      private DateTime Z1332ContagemResultadoNota_DataHora ;
      private DateTime A1332ContagemResultadoNota_DataHora ;
      private bool Z1857ContagemResultadoNotas_Ativo ;
      private bool A1857ContagemResultadoNotas_Ativo ;
      private bool n1857ContagemResultadoNotas_Ativo ;
      private bool n1330ContagemResultadoNota_UsuarioPesNom ;
      private bool n1329ContagemResultadoNota_UsuarioPesCod ;
      private bool i1857ContagemResultadoNotas_Ativo ;
      private String Z1334ContagemResultadoNota_Nota ;
      private String A1334ContagemResultadoNota_Nota ;
      private SdtContagemResultadoNotas bcContagemResultadoNotas ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC003K7_A1331ContagemResultadoNota_Codigo ;
      private DateTime[] BC003K7_A1332ContagemResultadoNota_DataHora ;
      private String[] BC003K7_A1334ContagemResultadoNota_Nota ;
      private String[] BC003K7_A1330ContagemResultadoNota_UsuarioPesNom ;
      private bool[] BC003K7_n1330ContagemResultadoNota_UsuarioPesNom ;
      private bool[] BC003K7_A1857ContagemResultadoNotas_Ativo ;
      private bool[] BC003K7_n1857ContagemResultadoNotas_Ativo ;
      private int[] BC003K7_A1327ContagemResultadoNota_DemandaCod ;
      private int[] BC003K7_A1328ContagemResultadoNota_UsuarioCod ;
      private int[] BC003K7_A1329ContagemResultadoNota_UsuarioPesCod ;
      private bool[] BC003K7_n1329ContagemResultadoNota_UsuarioPesCod ;
      private int[] BC003K4_A1327ContagemResultadoNota_DemandaCod ;
      private int[] BC003K5_A1329ContagemResultadoNota_UsuarioPesCod ;
      private bool[] BC003K5_n1329ContagemResultadoNota_UsuarioPesCod ;
      private String[] BC003K6_A1330ContagemResultadoNota_UsuarioPesNom ;
      private bool[] BC003K6_n1330ContagemResultadoNota_UsuarioPesNom ;
      private int[] BC003K8_A1331ContagemResultadoNota_Codigo ;
      private int[] BC003K3_A1331ContagemResultadoNota_Codigo ;
      private DateTime[] BC003K3_A1332ContagemResultadoNota_DataHora ;
      private String[] BC003K3_A1334ContagemResultadoNota_Nota ;
      private bool[] BC003K3_A1857ContagemResultadoNotas_Ativo ;
      private bool[] BC003K3_n1857ContagemResultadoNotas_Ativo ;
      private int[] BC003K3_A1327ContagemResultadoNota_DemandaCod ;
      private int[] BC003K3_A1328ContagemResultadoNota_UsuarioCod ;
      private int[] BC003K2_A1331ContagemResultadoNota_Codigo ;
      private DateTime[] BC003K2_A1332ContagemResultadoNota_DataHora ;
      private String[] BC003K2_A1334ContagemResultadoNota_Nota ;
      private bool[] BC003K2_A1857ContagemResultadoNotas_Ativo ;
      private bool[] BC003K2_n1857ContagemResultadoNotas_Ativo ;
      private int[] BC003K2_A1327ContagemResultadoNota_DemandaCod ;
      private int[] BC003K2_A1328ContagemResultadoNota_UsuarioCod ;
      private int[] BC003K9_A1331ContagemResultadoNota_Codigo ;
      private int[] BC003K12_A1329ContagemResultadoNota_UsuarioPesCod ;
      private bool[] BC003K12_n1329ContagemResultadoNota_UsuarioPesCod ;
      private String[] BC003K13_A1330ContagemResultadoNota_UsuarioPesNom ;
      private bool[] BC003K13_n1330ContagemResultadoNota_UsuarioPesNom ;
      private int[] BC003K14_A1331ContagemResultadoNota_Codigo ;
      private DateTime[] BC003K14_A1332ContagemResultadoNota_DataHora ;
      private String[] BC003K14_A1334ContagemResultadoNota_Nota ;
      private String[] BC003K14_A1330ContagemResultadoNota_UsuarioPesNom ;
      private bool[] BC003K14_n1330ContagemResultadoNota_UsuarioPesNom ;
      private bool[] BC003K14_A1857ContagemResultadoNotas_Ativo ;
      private bool[] BC003K14_n1857ContagemResultadoNotas_Ativo ;
      private int[] BC003K14_A1327ContagemResultadoNota_DemandaCod ;
      private int[] BC003K14_A1328ContagemResultadoNota_UsuarioCod ;
      private int[] BC003K14_A1329ContagemResultadoNota_UsuarioPesCod ;
      private bool[] BC003K14_n1329ContagemResultadoNota_UsuarioPesCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
   }

   public class contagemresultadonotas_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC003K7 ;
          prmBC003K7 = new Object[] {
          new Object[] {"@ContagemResultadoNota_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003K4 ;
          prmBC003K4 = new Object[] {
          new Object[] {"@ContagemResultadoNota_DemandaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003K5 ;
          prmBC003K5 = new Object[] {
          new Object[] {"@ContagemResultadoNota_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003K6 ;
          prmBC003K6 = new Object[] {
          new Object[] {"@ContagemResultadoNota_UsuarioPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003K8 ;
          prmBC003K8 = new Object[] {
          new Object[] {"@ContagemResultadoNota_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003K3 ;
          prmBC003K3 = new Object[] {
          new Object[] {"@ContagemResultadoNota_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003K2 ;
          prmBC003K2 = new Object[] {
          new Object[] {"@ContagemResultadoNota_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003K9 ;
          prmBC003K9 = new Object[] {
          new Object[] {"@ContagemResultadoNota_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoNota_Nota",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ContagemResultadoNotas_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultadoNota_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoNota_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003K10 ;
          prmBC003K10 = new Object[] {
          new Object[] {"@ContagemResultadoNota_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoNota_Nota",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ContagemResultadoNotas_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultadoNota_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoNota_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoNota_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003K11 ;
          prmBC003K11 = new Object[] {
          new Object[] {"@ContagemResultadoNota_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003K12 ;
          prmBC003K12 = new Object[] {
          new Object[] {"@ContagemResultadoNota_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003K13 ;
          prmBC003K13 = new Object[] {
          new Object[] {"@ContagemResultadoNota_UsuarioPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003K14 ;
          prmBC003K14 = new Object[] {
          new Object[] {"@ContagemResultadoNota_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC003K2", "SELECT [ContagemResultadoNota_Codigo], [ContagemResultadoNota_DataHora], [ContagemResultadoNota_Nota], [ContagemResultadoNotas_Ativo], [ContagemResultadoNota_DemandaCod] AS ContagemResultadoNota_DemandaCod, [ContagemResultadoNota_UsuarioCod] AS ContagemResultadoNota_UsuarioCod FROM [ContagemResultadoNotas] WITH (UPDLOCK) WHERE [ContagemResultadoNota_Codigo] = @ContagemResultadoNota_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003K2,1,0,true,false )
             ,new CursorDef("BC003K3", "SELECT [ContagemResultadoNota_Codigo], [ContagemResultadoNota_DataHora], [ContagemResultadoNota_Nota], [ContagemResultadoNotas_Ativo], [ContagemResultadoNota_DemandaCod] AS ContagemResultadoNota_DemandaCod, [ContagemResultadoNota_UsuarioCod] AS ContagemResultadoNota_UsuarioCod FROM [ContagemResultadoNotas] WITH (NOLOCK) WHERE [ContagemResultadoNota_Codigo] = @ContagemResultadoNota_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003K3,1,0,true,false )
             ,new CursorDef("BC003K4", "SELECT [ContagemResultado_Codigo] AS ContagemResultadoNota_DemandaCod FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoNota_DemandaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003K4,1,0,true,false )
             ,new CursorDef("BC003K5", "SELECT [Usuario_PessoaCod] AS ContagemResultadoNota_UsuarioPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoNota_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003K5,1,0,true,false )
             ,new CursorDef("BC003K6", "SELECT [Pessoa_Nome] AS ContagemResultadoNota_UsuarioPesNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoNota_UsuarioPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003K6,1,0,true,false )
             ,new CursorDef("BC003K7", "SELECT TM1.[ContagemResultadoNota_Codigo], TM1.[ContagemResultadoNota_DataHora], TM1.[ContagemResultadoNota_Nota], T3.[Pessoa_Nome] AS ContagemResultadoNota_UsuarioPesNom, TM1.[ContagemResultadoNotas_Ativo], TM1.[ContagemResultadoNota_DemandaCod] AS ContagemResultadoNota_DemandaCod, TM1.[ContagemResultadoNota_UsuarioCod] AS ContagemResultadoNota_UsuarioCod, T2.[Usuario_PessoaCod] AS ContagemResultadoNota_UsuarioPesCod FROM (([ContagemResultadoNotas] TM1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = TM1.[ContagemResultadoNota_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE TM1.[ContagemResultadoNota_Codigo] = @ContagemResultadoNota_Codigo ORDER BY TM1.[ContagemResultadoNota_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003K7,100,0,true,false )
             ,new CursorDef("BC003K8", "SELECT [ContagemResultadoNota_Codigo] FROM [ContagemResultadoNotas] WITH (NOLOCK) WHERE [ContagemResultadoNota_Codigo] = @ContagemResultadoNota_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003K8,1,0,true,false )
             ,new CursorDef("BC003K9", "INSERT INTO [ContagemResultadoNotas]([ContagemResultadoNota_DataHora], [ContagemResultadoNota_Nota], [ContagemResultadoNotas_Ativo], [ContagemResultadoNota_DemandaCod], [ContagemResultadoNota_UsuarioCod]) VALUES(@ContagemResultadoNota_DataHora, @ContagemResultadoNota_Nota, @ContagemResultadoNotas_Ativo, @ContagemResultadoNota_DemandaCod, @ContagemResultadoNota_UsuarioCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC003K9)
             ,new CursorDef("BC003K10", "UPDATE [ContagemResultadoNotas] SET [ContagemResultadoNota_DataHora]=@ContagemResultadoNota_DataHora, [ContagemResultadoNota_Nota]=@ContagemResultadoNota_Nota, [ContagemResultadoNotas_Ativo]=@ContagemResultadoNotas_Ativo, [ContagemResultadoNota_DemandaCod]=@ContagemResultadoNota_DemandaCod, [ContagemResultadoNota_UsuarioCod]=@ContagemResultadoNota_UsuarioCod  WHERE [ContagemResultadoNota_Codigo] = @ContagemResultadoNota_Codigo", GxErrorMask.GX_NOMASK,prmBC003K10)
             ,new CursorDef("BC003K11", "DELETE FROM [ContagemResultadoNotas]  WHERE [ContagemResultadoNota_Codigo] = @ContagemResultadoNota_Codigo", GxErrorMask.GX_NOMASK,prmBC003K11)
             ,new CursorDef("BC003K12", "SELECT [Usuario_PessoaCod] AS ContagemResultadoNota_UsuarioPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoNota_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003K12,1,0,true,false )
             ,new CursorDef("BC003K13", "SELECT [Pessoa_Nome] AS ContagemResultadoNota_UsuarioPesNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoNota_UsuarioPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003K13,1,0,true,false )
             ,new CursorDef("BC003K14", "SELECT TM1.[ContagemResultadoNota_Codigo], TM1.[ContagemResultadoNota_DataHora], TM1.[ContagemResultadoNota_Nota], T3.[Pessoa_Nome] AS ContagemResultadoNota_UsuarioPesNom, TM1.[ContagemResultadoNotas_Ativo], TM1.[ContagemResultadoNota_DemandaCod] AS ContagemResultadoNota_DemandaCod, TM1.[ContagemResultadoNota_UsuarioCod] AS ContagemResultadoNota_UsuarioCod, T2.[Usuario_PessoaCod] AS ContagemResultadoNota_UsuarioPesCod FROM (([ContagemResultadoNotas] TM1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = TM1.[ContagemResultadoNota_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE TM1.[ContagemResultadoNota_Codigo] = @ContagemResultadoNota_Codigo ORDER BY TM1.[ContagemResultadoNota_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003K14,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(3, (bool)parms[3]);
                }
                stmt.SetParameter(4, (int)parms[4]);
                stmt.SetParameter(5, (int)parms[5]);
                return;
             case 8 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(3, (bool)parms[3]);
                }
                stmt.SetParameter(4, (int)parms[4]);
                stmt.SetParameter(5, (int)parms[5]);
                stmt.SetParameter(6, (int)parms[6]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
