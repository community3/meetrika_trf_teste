/*
               File: LoteGeneral
        Description: Lote General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:20:14.32
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class lotegeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public lotegeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public lotegeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Lote_Codigo )
      {
         this.A596Lote_Codigo = aP0_Lote_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A596Lote_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A596Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A596Lote_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A596Lote_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PABS2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV15Pgmname = "LoteGeneral";
               context.Gx_err = 0;
               /* Using cursor H00BS5 */
               pr_default.execute(0, new Object[] {A596Lote_Codigo});
               if ( (pr_default.getStatus(0) != 101) )
               {
                  A1057Lote_ValorGlosas = H00BS5_A1057Lote_ValorGlosas[0];
                  n1057Lote_ValorGlosas = H00BS5_n1057Lote_ValorGlosas[0];
               }
               else
               {
                  A1057Lote_ValorGlosas = 0;
                  n1057Lote_ValorGlosas = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1057Lote_ValorGlosas", StringUtil.LTrim( StringUtil.Str( A1057Lote_ValorGlosas, 18, 5)));
               }
               pr_default.close(0);
               /* Using cursor H00BS7 */
               pr_default.execute(1, new Object[] {A596Lote_Codigo});
               if ( (pr_default.getStatus(1) != 101) )
               {
                  A569Lote_QtdeDmn = H00BS7_A569Lote_QtdeDmn[0];
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A569Lote_QtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A569Lote_QtdeDmn), 4, 0)));
                  n569Lote_QtdeDmn = H00BS7_n569Lote_QtdeDmn[0];
               }
               else
               {
                  A569Lote_QtdeDmn = 0;
                  n569Lote_QtdeDmn = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A569Lote_QtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A569Lote_QtdeDmn), 4, 0)));
               }
               pr_default.close(1);
               /* Using cursor H00BS10 */
               pr_default.execute(2, new Object[] {A596Lote_Codigo});
               if ( (pr_default.getStatus(2) != 101) )
               {
                  A568Lote_DataFim = H00BS10_A568Lote_DataFim[0];
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A568Lote_DataFim", context.localUtil.Format(A568Lote_DataFim, "99/99/99"));
                  A567Lote_DataIni = H00BS10_A567Lote_DataIni[0];
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A567Lote_DataIni", context.localUtil.Format(A567Lote_DataIni, "99/99/99"));
               }
               else
               {
                  A568Lote_DataFim = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A568Lote_DataFim", context.localUtil.Format(A568Lote_DataFim, "99/99/99"));
                  A567Lote_DataIni = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A567Lote_DataIni", context.localUtil.Format(A567Lote_DataIni, "99/99/99"));
               }
               pr_default.close(2);
               GetLote_ValorOSs( A596Lote_Codigo) ;
               A572Lote_Valor = (decimal)(A1058Lote_ValorOSs-A1057Lote_ValorGlosas);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A572Lote_Valor", StringUtil.LTrim( StringUtil.Str( A572Lote_Valor, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_VALOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A572Lote_Valor, "ZZ,ZZZ,ZZZ,ZZ9.99")));
               WSBS2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void GetLote_ValorOSs( int A596Lote_Codigo )
      {
         /* Create private dbobjects */
         /* Navigation */
         A1058Lote_ValorOSs = 0;
         /* Using cursor H00BS11 */
         pr_default.execute(3, new Object[] {A596Lote_Codigo});
         while ( (pr_default.getStatus(3) != 101) && ( H00BS11_A597ContagemResultado_LoteAceiteCod[0] == A596Lote_Codigo ) )
         {
            GXt_decimal1 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  H00BS11_A456ContagemResultado_Codigo[0], out  GXt_decimal1) ;
            A574ContagemResultado_PFFinal = GXt_decimal1;
            A606ContagemResultado_ValorFinal = (decimal)(A574ContagemResultado_PFFinal*H00BS11_A512ContagemResultado_ValorPF[0]);
            A1058Lote_ValorOSs = (decimal)(A1058Lote_ValorOSs+A606ContagemResultado_ValorFinal);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1058Lote_ValorOSs", StringUtil.LTrim( StringUtil.Str( A1058Lote_ValorOSs, 18, 5)));
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Lote General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216201454");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("lotegeneral.aspx") + "?" + UrlEncode("" +A596Lote_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA596Lote_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA596Lote_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"LOTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A596Lote_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"LOTE_VALOROSS", StringUtil.LTrim( StringUtil.NToC( A1058Lote_ValorOSs, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"LOTE_VALORGLOSAS", StringUtil.LTrim( StringUtil.NToC( A1057Lote_ValorGlosas, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOTE_NUMERO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A562Lote_Numero, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOTE_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A563Lote_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOTE_DATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A564Lote_Data, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOTE_PARECERFINAL", GetSecureSignedToken( sPrefix, A2088Lote_ParecerFinal));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOTE_COMENTARIOS", GetSecureSignedToken( sPrefix, A2087Lote_Comentarios));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOTE_DATACONTRATO", GetSecureSignedToken( sPrefix, A844Lote_DataContrato));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOTE_VALORPF", GetSecureSignedToken( sPrefix, context.localUtil.Format( A565Lote_ValorPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOTE_NFE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A673Lote_NFe), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOTE_DATANFE", GetSecureSignedToken( sPrefix, A674Lote_DataNfe));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOTE_VALOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A572Lote_Valor, "ZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOTE_NFEDATAPROTOCOLO", GetSecureSignedToken( sPrefix, A1001Lote_NFeDataProtocolo));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOTE_PREVPAGAMENTO", GetSecureSignedToken( sPrefix, A857Lote_PrevPagamento));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOTE_LIQBANCO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A675Lote_LiqBanco, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOTE_LIQDATA", GetSecureSignedToken( sPrefix, A676Lote_LiqData));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOTE_LIQVALOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A677Lote_LiqValor, "ZZ,ZZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOTE_LIQPERC", GetSecureSignedToken( sPrefix, context.localUtil.Format( A681Lote_LiqPerc, "ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOTE_AREATRABALHOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A595Lote_AreaTrabalhoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_LOTE_USERCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A559Lote_UserCod), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "LoteGeneral";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A559Lote_UserCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("lotegeneral:[SendSecurityCheck value for]"+"Lote_UserCod:"+context.localUtil.Format( (decimal)(A559Lote_UserCod), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseFormBS2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("lotegeneral.js", "?20206216201458");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "LoteGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Lote General" ;
      }

      protected void WBBS0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "lotegeneral.aspx");
            }
            wb_table1_2_BS2( true) ;
         }
         else
         {
            wb_table1_2_BS2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_BS2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLote_AreaTrabalhoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A595Lote_AreaTrabalhoCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A595Lote_AreaTrabalhoCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_AreaTrabalhoCod_Jsonclick, 0, "Attribute", "", "", "", edtLote_AreaTrabalhoCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_LoteGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLote_UserCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A559Lote_UserCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A559Lote_UserCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_UserCod_Jsonclick, 0, "Attribute", "", "", "", edtLote_UserCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_LoteGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLote_PessoaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A560Lote_PessoaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A560Lote_PessoaCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_PessoaCod_Jsonclick, 0, "Attribute", "", "", "", edtLote_PessoaCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_LoteGeneral.htm");
         }
         wbLoad = true;
      }

      protected void STARTBS2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Lote General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPBS0( ) ;
            }
         }
      }

      protected void WSBS2( )
      {
         STARTBS2( ) ;
         EVTBS2( ) ;
      }

      protected void EVTBS2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPBS0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPBS0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11BS2 */
                                    E11BS2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPBS0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12BS2 */
                                    E12BS2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPBS0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13BS2 */
                                    E13BS2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPBS0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14BS2 */
                                    E14BS2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPBS0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15BS2 */
                                    E15BS2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPBS0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPBS0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEBS2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormBS2( ) ;
            }
         }
      }

      protected void PABS2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFBS2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV15Pgmname = "LoteGeneral";
         context.Gx_err = 0;
      }

      protected void RFBS2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00BS18 */
            pr_default.execute(4, new Object[] {A596Lote_Codigo});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A560Lote_PessoaCod = H00BS18_A560Lote_PessoaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A560Lote_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A560Lote_PessoaCod), 6, 0)));
               n560Lote_PessoaCod = H00BS18_n560Lote_PessoaCod[0];
               A559Lote_UserCod = H00BS18_A559Lote_UserCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A559Lote_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A559Lote_UserCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_USERCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A559Lote_UserCod), "ZZZZZ9")));
               A595Lote_AreaTrabalhoCod = H00BS18_A595Lote_AreaTrabalhoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A595Lote_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A595Lote_AreaTrabalhoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_AREATRABALHOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A595Lote_AreaTrabalhoCod), "ZZZZZ9")));
               A676Lote_LiqData = H00BS18_A676Lote_LiqData[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A676Lote_LiqData", context.localUtil.Format(A676Lote_LiqData, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_LIQDATA", GetSecureSignedToken( sPrefix, A676Lote_LiqData));
               n676Lote_LiqData = H00BS18_n676Lote_LiqData[0];
               A675Lote_LiqBanco = H00BS18_A675Lote_LiqBanco[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A675Lote_LiqBanco", A675Lote_LiqBanco);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_LIQBANCO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A675Lote_LiqBanco, "@!"))));
               n675Lote_LiqBanco = H00BS18_n675Lote_LiqBanco[0];
               A857Lote_PrevPagamento = H00BS18_A857Lote_PrevPagamento[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A857Lote_PrevPagamento", context.localUtil.Format(A857Lote_PrevPagamento, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_PREVPAGAMENTO", GetSecureSignedToken( sPrefix, A857Lote_PrevPagamento));
               n857Lote_PrevPagamento = H00BS18_n857Lote_PrevPagamento[0];
               A1001Lote_NFeDataProtocolo = H00BS18_A1001Lote_NFeDataProtocolo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1001Lote_NFeDataProtocolo", context.localUtil.Format(A1001Lote_NFeDataProtocolo, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_NFEDATAPROTOCOLO", GetSecureSignedToken( sPrefix, A1001Lote_NFeDataProtocolo));
               n1001Lote_NFeDataProtocolo = H00BS18_n1001Lote_NFeDataProtocolo[0];
               A674Lote_DataNfe = H00BS18_A674Lote_DataNfe[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A674Lote_DataNfe", context.localUtil.Format(A674Lote_DataNfe, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_DATANFE", GetSecureSignedToken( sPrefix, A674Lote_DataNfe));
               n674Lote_DataNfe = H00BS18_n674Lote_DataNfe[0];
               A673Lote_NFe = H00BS18_A673Lote_NFe[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A673Lote_NFe", StringUtil.LTrim( StringUtil.Str( (decimal)(A673Lote_NFe), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_NFE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A673Lote_NFe), "ZZZZZ9")));
               n673Lote_NFe = H00BS18_n673Lote_NFe[0];
               A565Lote_ValorPF = H00BS18_A565Lote_ValorPF[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A565Lote_ValorPF", StringUtil.LTrim( StringUtil.Str( A565Lote_ValorPF, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_VALORPF", GetSecureSignedToken( sPrefix, context.localUtil.Format( A565Lote_ValorPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               A844Lote_DataContrato = H00BS18_A844Lote_DataContrato[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A844Lote_DataContrato", context.localUtil.Format(A844Lote_DataContrato, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_DATACONTRATO", GetSecureSignedToken( sPrefix, A844Lote_DataContrato));
               n844Lote_DataContrato = H00BS18_n844Lote_DataContrato[0];
               A2087Lote_Comentarios = H00BS18_A2087Lote_Comentarios[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2087Lote_Comentarios", A2087Lote_Comentarios);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_COMENTARIOS", GetSecureSignedToken( sPrefix, A2087Lote_Comentarios));
               n2087Lote_Comentarios = H00BS18_n2087Lote_Comentarios[0];
               A2088Lote_ParecerFinal = H00BS18_A2088Lote_ParecerFinal[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2088Lote_ParecerFinal", A2088Lote_ParecerFinal);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_PARECERFINAL", GetSecureSignedToken( sPrefix, A2088Lote_ParecerFinal));
               n2088Lote_ParecerFinal = H00BS18_n2088Lote_ParecerFinal[0];
               A561Lote_UserNom = H00BS18_A561Lote_UserNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A561Lote_UserNom", A561Lote_UserNom);
               n561Lote_UserNom = H00BS18_n561Lote_UserNom[0];
               A564Lote_Data = H00BS18_A564Lote_Data[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A564Lote_Data", context.localUtil.TToC( A564Lote_Data, 8, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_DATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A564Lote_Data, "99/99/99 99:99")));
               A563Lote_Nome = H00BS18_A563Lote_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A563Lote_Nome", A563Lote_Nome);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A563Lote_Nome, "@!"))));
               A562Lote_Numero = H00BS18_A562Lote_Numero[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A562Lote_Numero", A562Lote_Numero);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_NUMERO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A562Lote_Numero, ""))));
               A569Lote_QtdeDmn = H00BS18_A569Lote_QtdeDmn[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A569Lote_QtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A569Lote_QtdeDmn), 4, 0)));
               n569Lote_QtdeDmn = H00BS18_n569Lote_QtdeDmn[0];
               A568Lote_DataFim = H00BS18_A568Lote_DataFim[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A568Lote_DataFim", context.localUtil.Format(A568Lote_DataFim, "99/99/99"));
               A567Lote_DataIni = H00BS18_A567Lote_DataIni[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A567Lote_DataIni", context.localUtil.Format(A567Lote_DataIni, "99/99/99"));
               A1057Lote_ValorGlosas = H00BS18_A1057Lote_ValorGlosas[0];
               n1057Lote_ValorGlosas = H00BS18_n1057Lote_ValorGlosas[0];
               A677Lote_LiqValor = H00BS18_A677Lote_LiqValor[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A677Lote_LiqValor", StringUtil.LTrim( StringUtil.Str( A677Lote_LiqValor, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_LIQVALOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A677Lote_LiqValor, "ZZ,ZZZ,ZZZ,ZZ9.999")));
               n677Lote_LiqValor = H00BS18_n677Lote_LiqValor[0];
               A560Lote_PessoaCod = H00BS18_A560Lote_PessoaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A560Lote_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A560Lote_PessoaCod), 6, 0)));
               n560Lote_PessoaCod = H00BS18_n560Lote_PessoaCod[0];
               A561Lote_UserNom = H00BS18_A561Lote_UserNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A561Lote_UserNom", A561Lote_UserNom);
               n561Lote_UserNom = H00BS18_n561Lote_UserNom[0];
               A1057Lote_ValorGlosas = H00BS18_A1057Lote_ValorGlosas[0];
               n1057Lote_ValorGlosas = H00BS18_n1057Lote_ValorGlosas[0];
               A569Lote_QtdeDmn = H00BS18_A569Lote_QtdeDmn[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A569Lote_QtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A569Lote_QtdeDmn), 4, 0)));
               n569Lote_QtdeDmn = H00BS18_n569Lote_QtdeDmn[0];
               A568Lote_DataFim = H00BS18_A568Lote_DataFim[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A568Lote_DataFim", context.localUtil.Format(A568Lote_DataFim, "99/99/99"));
               A567Lote_DataIni = H00BS18_A567Lote_DataIni[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A567Lote_DataIni", context.localUtil.Format(A567Lote_DataIni, "99/99/99"));
               A681Lote_LiqPerc = ((Convert.ToDecimal(0)==A677Lote_LiqValor) ? (decimal)(0) : A572Lote_Valor/ (decimal)(A677Lote_LiqValor)*100);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A681Lote_LiqPerc", StringUtil.LTrim( StringUtil.Str( A681Lote_LiqPerc, 6, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_LIQPERC", GetSecureSignedToken( sPrefix, context.localUtil.Format( A681Lote_LiqPerc, "ZZ9.99")));
               /* Execute user event: E12BS2 */
               E12BS2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(4);
            WBBS0( ) ;
         }
      }

      protected void STRUPBS0( )
      {
         /* Before Start, stand alone formulas. */
         AV15Pgmname = "LoteGeneral";
         context.Gx_err = 0;
         /* Using cursor H00BS22 */
         pr_default.execute(5, new Object[] {A596Lote_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            A1057Lote_ValorGlosas = H00BS22_A1057Lote_ValorGlosas[0];
            n1057Lote_ValorGlosas = H00BS22_n1057Lote_ValorGlosas[0];
         }
         else
         {
            A1057Lote_ValorGlosas = 0;
            n1057Lote_ValorGlosas = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1057Lote_ValorGlosas", StringUtil.LTrim( StringUtil.Str( A1057Lote_ValorGlosas, 18, 5)));
         }
         pr_default.close(5);
         /* Using cursor H00BS24 */
         pr_default.execute(6, new Object[] {A596Lote_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            A569Lote_QtdeDmn = H00BS24_A569Lote_QtdeDmn[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A569Lote_QtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A569Lote_QtdeDmn), 4, 0)));
            n569Lote_QtdeDmn = H00BS24_n569Lote_QtdeDmn[0];
         }
         else
         {
            A569Lote_QtdeDmn = 0;
            n569Lote_QtdeDmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A569Lote_QtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A569Lote_QtdeDmn), 4, 0)));
         }
         pr_default.close(6);
         /* Using cursor H00BS27 */
         pr_default.execute(7, new Object[] {A596Lote_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            A568Lote_DataFim = H00BS27_A568Lote_DataFim[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A568Lote_DataFim", context.localUtil.Format(A568Lote_DataFim, "99/99/99"));
            A567Lote_DataIni = H00BS27_A567Lote_DataIni[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A567Lote_DataIni", context.localUtil.Format(A567Lote_DataIni, "99/99/99"));
         }
         else
         {
            A568Lote_DataFim = DateTime.MinValue;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A568Lote_DataFim", context.localUtil.Format(A568Lote_DataFim, "99/99/99"));
            A567Lote_DataIni = DateTime.MinValue;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A567Lote_DataIni", context.localUtil.Format(A567Lote_DataIni, "99/99/99"));
         }
         pr_default.close(7);
         GetLote_ValorOSs( A596Lote_Codigo) ;
         A572Lote_Valor = (decimal)(A1058Lote_ValorOSs-A1057Lote_ValorGlosas);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A572Lote_Valor", StringUtil.LTrim( StringUtil.Str( A572Lote_Valor, 18, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_VALOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A572Lote_Valor, "ZZ,ZZZ,ZZZ,ZZ9.99")));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11BS2 */
         E11BS2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A562Lote_Numero = cgiGet( edtLote_Numero_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A562Lote_Numero", A562Lote_Numero);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_NUMERO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A562Lote_Numero, ""))));
            A563Lote_Nome = StringUtil.Upper( cgiGet( edtLote_Nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A563Lote_Nome", A563Lote_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A563Lote_Nome, "@!"))));
            A564Lote_Data = context.localUtil.CToT( cgiGet( edtLote_Data_Internalname), 0);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A564Lote_Data", context.localUtil.TToC( A564Lote_Data, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_DATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A564Lote_Data, "99/99/99 99:99")));
            A561Lote_UserNom = StringUtil.Upper( cgiGet( edtLote_UserNom_Internalname));
            n561Lote_UserNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A561Lote_UserNom", A561Lote_UserNom);
            A567Lote_DataIni = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtLote_DataIni_Internalname), 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A567Lote_DataIni", context.localUtil.Format(A567Lote_DataIni, "99/99/99"));
            A568Lote_DataFim = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtLote_DataFim_Internalname), 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A568Lote_DataFim", context.localUtil.Format(A568Lote_DataFim, "99/99/99"));
            A2088Lote_ParecerFinal = cgiGet( edtLote_ParecerFinal_Internalname);
            n2088Lote_ParecerFinal = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2088Lote_ParecerFinal", A2088Lote_ParecerFinal);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_PARECERFINAL", GetSecureSignedToken( sPrefix, A2088Lote_ParecerFinal));
            A2087Lote_Comentarios = cgiGet( edtLote_Comentarios_Internalname);
            n2087Lote_Comentarios = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2087Lote_Comentarios", A2087Lote_Comentarios);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_COMENTARIOS", GetSecureSignedToken( sPrefix, A2087Lote_Comentarios));
            A844Lote_DataContrato = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtLote_DataContrato_Internalname), 0));
            n844Lote_DataContrato = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A844Lote_DataContrato", context.localUtil.Format(A844Lote_DataContrato, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_DATACONTRATO", GetSecureSignedToken( sPrefix, A844Lote_DataContrato));
            A565Lote_ValorPF = context.localUtil.CToN( cgiGet( edtLote_ValorPF_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A565Lote_ValorPF", StringUtil.LTrim( StringUtil.Str( A565Lote_ValorPF, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_VALORPF", GetSecureSignedToken( sPrefix, context.localUtil.Format( A565Lote_ValorPF, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            A569Lote_QtdeDmn = (short)(context.localUtil.CToN( cgiGet( edtLote_QtdeDmn_Internalname), ",", "."));
            n569Lote_QtdeDmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A569Lote_QtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(A569Lote_QtdeDmn), 4, 0)));
            A673Lote_NFe = (int)(context.localUtil.CToN( cgiGet( edtLote_NFe_Internalname), ",", "."));
            n673Lote_NFe = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A673Lote_NFe", StringUtil.LTrim( StringUtil.Str( (decimal)(A673Lote_NFe), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_NFE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A673Lote_NFe), "ZZZZZ9")));
            A674Lote_DataNfe = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtLote_DataNfe_Internalname), 0));
            n674Lote_DataNfe = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A674Lote_DataNfe", context.localUtil.Format(A674Lote_DataNfe, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_DATANFE", GetSecureSignedToken( sPrefix, A674Lote_DataNfe));
            A572Lote_Valor = context.localUtil.CToN( cgiGet( edtLote_Valor_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A572Lote_Valor", StringUtil.LTrim( StringUtil.Str( A572Lote_Valor, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_VALOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A572Lote_Valor, "ZZ,ZZZ,ZZZ,ZZ9.99")));
            A1001Lote_NFeDataProtocolo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtLote_NFeDataProtocolo_Internalname), 0));
            n1001Lote_NFeDataProtocolo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1001Lote_NFeDataProtocolo", context.localUtil.Format(A1001Lote_NFeDataProtocolo, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_NFEDATAPROTOCOLO", GetSecureSignedToken( sPrefix, A1001Lote_NFeDataProtocolo));
            A857Lote_PrevPagamento = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtLote_PrevPagamento_Internalname), 0));
            n857Lote_PrevPagamento = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A857Lote_PrevPagamento", context.localUtil.Format(A857Lote_PrevPagamento, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_PREVPAGAMENTO", GetSecureSignedToken( sPrefix, A857Lote_PrevPagamento));
            A675Lote_LiqBanco = StringUtil.Upper( cgiGet( edtLote_LiqBanco_Internalname));
            n675Lote_LiqBanco = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A675Lote_LiqBanco", A675Lote_LiqBanco);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_LIQBANCO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A675Lote_LiqBanco, "@!"))));
            A676Lote_LiqData = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtLote_LiqData_Internalname), 0));
            n676Lote_LiqData = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A676Lote_LiqData", context.localUtil.Format(A676Lote_LiqData, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_LIQDATA", GetSecureSignedToken( sPrefix, A676Lote_LiqData));
            A677Lote_LiqValor = context.localUtil.CToN( cgiGet( edtLote_LiqValor_Internalname), ",", ".");
            n677Lote_LiqValor = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A677Lote_LiqValor", StringUtil.LTrim( StringUtil.Str( A677Lote_LiqValor, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_LIQVALOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A677Lote_LiqValor, "ZZ,ZZZ,ZZZ,ZZ9.999")));
            A681Lote_LiqPerc = context.localUtil.CToN( cgiGet( edtLote_LiqPerc_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A681Lote_LiqPerc", StringUtil.LTrim( StringUtil.Str( A681Lote_LiqPerc, 6, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_LIQPERC", GetSecureSignedToken( sPrefix, context.localUtil.Format( A681Lote_LiqPerc, "ZZ9.99")));
            A595Lote_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtLote_AreaTrabalhoCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A595Lote_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A595Lote_AreaTrabalhoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_AREATRABALHOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A595Lote_AreaTrabalhoCod), "ZZZZZ9")));
            A559Lote_UserCod = (int)(context.localUtil.CToN( cgiGet( edtLote_UserCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A559Lote_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A559Lote_UserCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_USERCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A559Lote_UserCod), "ZZZZZ9")));
            A560Lote_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtLote_PessoaCod_Internalname), ",", "."));
            n560Lote_PessoaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A560Lote_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A560Lote_PessoaCod), 6, 0)));
            /* Read saved values. */
            wcpOA596Lote_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA596Lote_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "LoteGeneral";
            A559Lote_UserCod = (int)(context.localUtil.CToN( cgiGet( edtLote_UserCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A559Lote_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A559Lote_UserCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_LOTE_USERCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A559Lote_UserCod), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A559Lote_UserCod), "ZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("lotegeneral:[SecurityCheckFailed value for]"+"Lote_UserCod:"+context.localUtil.Format( (decimal)(A559Lote_UserCod), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11BS2 */
         E11BS2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11BS2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete&&AV6WWPContext.gxTpr_Userehadministradorgam ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E12BS2( )
      {
         /* Load Routine */
         edtLote_UserNom_Link = formatLink("viewusuario.aspx") + "?" + UrlEncode("" +A559Lote_UserCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtLote_UserNom_Internalname, "Link", edtLote_UserNom_Link);
         edtLote_AreaTrabalhoCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtLote_AreaTrabalhoCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_AreaTrabalhoCod_Visible), 5, 0)));
         edtLote_UserCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtLote_UserCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_UserCod_Visible), 5, 0)));
         edtLote_PessoaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtLote_PessoaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLote_PessoaCod_Visible), 5, 0)));
      }

      protected void E13BS2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("lote.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A596Lote_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14BS2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("lote.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A596Lote_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E15BS2( )
      {
         /* 'DoFechar' Routine */
         context.wjLoc = formatLink("wwlote.aspx") ;
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV15Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Lote";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "Lote_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV12Lote_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_BS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_BS2( true) ;
         }
         else
         {
            wb_table2_8_BS2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_BS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_135_BS2( true) ;
         }
         else
         {
            wb_table3_135_BS2( false) ;
         }
         return  ;
      }

      protected void wb_table3_135_BS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_BS2e( true) ;
         }
         else
         {
            wb_table1_2_BS2e( false) ;
         }
      }

      protected void wb_table3_135_BS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 138,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 140,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 142,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_135_BS2e( true) ;
         }
         else
         {
            wb_table3_135_BS2e( false) ;
         }
      }

      protected void wb_table2_8_BS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_numero_Internalname, "N�mero", "", "", lblTextblocklote_numero_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLote_Numero_Internalname, StringUtil.RTrim( A562Lote_Numero), StringUtil.RTrim( context.localUtil.Format( A562Lote_Numero, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_Numero_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_nome_Internalname, "Nome", "", "", lblTextblocklote_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLote_Nome_Internalname, StringUtil.RTrim( A563Lote_Nome), StringUtil.RTrim( context.localUtil.Format( A563Lote_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_data_Internalname, "Data", "", "", lblTextblocklote_data_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtLote_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtLote_Data_Internalname, context.localUtil.TToC( A564Lote_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A564Lote_Data, "99/99/99 99:99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_Data_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_LoteGeneral.htm");
            GxWebStd.gx_bitmap( context, edtLote_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_LoteGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_usernom_Internalname, "Respons�vel", "", "", lblTextblocklote_usernom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLote_UserNom_Internalname, StringUtil.RTrim( A561Lote_UserNom), StringUtil.RTrim( context.localUtil.Format( A561Lote_UserNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtLote_UserNom_Link, "", "", "", edtLote_UserNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_dataini_Internalname, "Per�odo", "", "", lblTextblocklote_dataini_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table4_34_BS2( true) ;
         }
         else
         {
            wb_table4_34_BS2( false) ;
         }
         return  ;
      }

      protected void wb_table4_34_BS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_parecerfinal_Internalname, "Parecer final", "", "", lblTextblocklote_parecerfinal_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtLote_ParecerFinal_Internalname, A2088Lote_ParecerFinal, "", "", 0, 1, 0, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_comentarios_Internalname, "Coment�rios", "", "", lblTextblocklote_comentarios_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtLote_Comentarios_Internalname, A2087Lote_Comentarios, "", "", 0, 1, 0, 0, 80, "chr", 6, "row", StyleString, ClassString, "", "2097152", 1, "", "", -1, true, "", "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_datacontrato_Internalname, "Data do Contrato", "", "", lblTextblocklote_datacontrato_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtLote_DataContrato_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtLote_DataContrato_Internalname, context.localUtil.Format(A844Lote_DataContrato, "99/99/99"), context.localUtil.Format( A844Lote_DataContrato, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_DataContrato_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_LoteGeneral.htm");
            GxWebStd.gx_bitmap( context, edtLote_DataContrato_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_LoteGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_valorpf_Internalname, "Valor PF R$", "", "", lblTextblocklote_valorpf_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLote_ValorPF_Internalname, StringUtil.LTrim( StringUtil.NToC( A565Lote_ValorPF, 18, 5, ",", "")), context.localUtil.Format( A565Lote_ValorPF, "ZZZ,ZZZ,ZZZ,ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_ValorPF_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_qtdedmn_Internalname, "Qtde Dmn", "", "", lblTextblocklote_qtdedmn_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLote_QtdeDmn_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A569Lote_QtdeDmn), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A569Lote_QtdeDmn), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_QtdeDmn_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_nfe_Internalname, "NFe", "", "", lblTextblocklote_nfe_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLote_NFe_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A673Lote_NFe), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A673Lote_NFe), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_NFe_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 54, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_datanfe_Internalname, "Data emis�o", "", "", lblTextblocklote_datanfe_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtLote_DataNfe_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtLote_DataNfe_Internalname, context.localUtil.Format(A674Lote_DataNfe, "99/99/99"), context.localUtil.Format( A674Lote_DataNfe, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_DataNfe_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_LoteGeneral.htm");
            GxWebStd.gx_bitmap( context, edtLote_DataNfe_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_LoteGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_valor_Internalname, "Valor R$", "", "", lblTextblocklote_valor_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLote_Valor_Internalname, StringUtil.LTrim( StringUtil.NToC( A572Lote_Valor, 18, 5, ",", "")), context.localUtil.Format( A572Lote_Valor, "ZZ,ZZZ,ZZZ,ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_Valor_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor3Casas", "right", false, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_nfedataprotocolo_Internalname, "Data do protocolo", "", "", lblTextblocklote_nfedataprotocolo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtLote_NFeDataProtocolo_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtLote_NFeDataProtocolo_Internalname, context.localUtil.Format(A1001Lote_NFeDataProtocolo, "99/99/99"), context.localUtil.Format( A1001Lote_NFeDataProtocolo, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_NFeDataProtocolo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_LoteGeneral.htm");
            GxWebStd.gx_bitmap( context, edtLote_NFeDataProtocolo_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_LoteGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_prevpagamento_Internalname, "Prev. de pagamento", "", "", lblTextblocklote_prevpagamento_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtLote_PrevPagamento_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtLote_PrevPagamento_Internalname, context.localUtil.Format(A857Lote_PrevPagamento, "99/99/99"), context.localUtil.Format( A857Lote_PrevPagamento, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_PrevPagamento_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_LoteGeneral.htm");
            GxWebStd.gx_bitmap( context, edtLote_PrevPagamento_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_LoteGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_liqbanco_Internalname, "Banco", "", "", lblTextblocklote_liqbanco_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLote_LiqBanco_Internalname, StringUtil.RTrim( A675Lote_LiqBanco), StringUtil.RTrim( context.localUtil.Format( A675Lote_LiqBanco, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_LiqBanco_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_liqdata_Internalname, "Data liquidado", "", "", lblTextblocklote_liqdata_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtLote_LiqData_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtLote_LiqData_Internalname, context.localUtil.Format(A676Lote_LiqData, "99/99/99"), context.localUtil.Format( A676Lote_LiqData, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_LiqData_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_LoteGeneral.htm");
            GxWebStd.gx_bitmap( context, edtLote_LiqData_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_LoteGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_liqvalor_Internalname, "Liquidado R$", "", "", lblTextblocklote_liqvalor_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            wb_table5_121_BS2( true) ;
         }
         else
         {
            wb_table5_121_BS2( false) ;
         }
         return  ;
      }

      protected void wb_table5_121_BS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_BS2e( true) ;
         }
         else
         {
            wb_table2_8_BS2e( false) ;
         }
      }

      protected void wb_table5_121_BS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedlote_liqvalor_Internalname, tblTablemergedlote_liqvalor_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLote_LiqValor_Internalname, StringUtil.LTrim( StringUtil.NToC( A677Lote_LiqValor, 18, 5, ",", "")), context.localUtil.Format( A677Lote_LiqValor, "ZZ,ZZZ,ZZZ,ZZ9.999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_LiqValor_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 90, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor3Casas", "right", false, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_liqperc_Internalname, "", "", "", lblTextblocklote_liqperc_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLote_LiqPerc_Internalname, StringUtil.LTrim( StringUtil.NToC( A681Lote_LiqPerc, 6, 2, ",", "")), context.localUtil.Format( A681Lote_LiqPerc, "ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_LiqPerc_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 45, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Percentual", "right", false, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellLote_liqperc_righttext_cell_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLote_liqperc_righttext_Internalname, "%", "", "", lblLote_liqperc_righttext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_121_BS2e( true) ;
         }
         else
         {
            wb_table5_121_BS2e( false) ;
         }
      }

      protected void wb_table4_34_BS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedlote_dataini_Internalname, tblTablemergedlote_dataini_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtLote_DataIni_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtLote_DataIni_Internalname, context.localUtil.Format(A567Lote_DataIni, "99/99/99"), context.localUtil.Format( A567Lote_DataIni, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_DataIni_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_LoteGeneral.htm");
            GxWebStd.gx_bitmap( context, edtLote_DataIni_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_LoteGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklote_datafim_Internalname, "-", "", "", lblTextblocklote_datafim_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtLote_DataFim_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtLote_DataFim_Internalname, context.localUtil.Format(A568Lote_DataFim, "99/99/99"), context.localUtil.Format( A568Lote_DataFim, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLote_DataFim_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_LoteGeneral.htm");
            GxWebStd.gx_bitmap( context, edtLote_DataFim_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_LoteGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_BS2e( true) ;
         }
         else
         {
            wb_table4_34_BS2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A596Lote_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A596Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A596Lote_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PABS2( ) ;
         WSBS2( ) ;
         WEBS2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA596Lote_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PABS2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "lotegeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PABS2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A596Lote_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A596Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A596Lote_Codigo), 6, 0)));
         }
         wcpOA596Lote_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA596Lote_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A596Lote_Codigo != wcpOA596Lote_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA596Lote_Codigo = A596Lote_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA596Lote_Codigo = cgiGet( sPrefix+"A596Lote_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA596Lote_Codigo) > 0 )
         {
            A596Lote_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA596Lote_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A596Lote_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A596Lote_Codigo), 6, 0)));
         }
         else
         {
            A596Lote_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A596Lote_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PABS2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSBS2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSBS2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A596Lote_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A596Lote_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA596Lote_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A596Lote_Codigo_CTRL", StringUtil.RTrim( sCtrlA596Lote_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEBS2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216201577");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("lotegeneral.js", "?20206216201577");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocklote_numero_Internalname = sPrefix+"TEXTBLOCKLOTE_NUMERO";
         edtLote_Numero_Internalname = sPrefix+"LOTE_NUMERO";
         lblTextblocklote_nome_Internalname = sPrefix+"TEXTBLOCKLOTE_NOME";
         edtLote_Nome_Internalname = sPrefix+"LOTE_NOME";
         lblTextblocklote_data_Internalname = sPrefix+"TEXTBLOCKLOTE_DATA";
         edtLote_Data_Internalname = sPrefix+"LOTE_DATA";
         lblTextblocklote_usernom_Internalname = sPrefix+"TEXTBLOCKLOTE_USERNOM";
         edtLote_UserNom_Internalname = sPrefix+"LOTE_USERNOM";
         lblTextblocklote_dataini_Internalname = sPrefix+"TEXTBLOCKLOTE_DATAINI";
         edtLote_DataIni_Internalname = sPrefix+"LOTE_DATAINI";
         lblTextblocklote_datafim_Internalname = sPrefix+"TEXTBLOCKLOTE_DATAFIM";
         edtLote_DataFim_Internalname = sPrefix+"LOTE_DATAFIM";
         tblTablemergedlote_dataini_Internalname = sPrefix+"TABLEMERGEDLOTE_DATAINI";
         lblTextblocklote_parecerfinal_Internalname = sPrefix+"TEXTBLOCKLOTE_PARECERFINAL";
         edtLote_ParecerFinal_Internalname = sPrefix+"LOTE_PARECERFINAL";
         lblTextblocklote_comentarios_Internalname = sPrefix+"TEXTBLOCKLOTE_COMENTARIOS";
         edtLote_Comentarios_Internalname = sPrefix+"LOTE_COMENTARIOS";
         lblTextblocklote_datacontrato_Internalname = sPrefix+"TEXTBLOCKLOTE_DATACONTRATO";
         edtLote_DataContrato_Internalname = sPrefix+"LOTE_DATACONTRATO";
         lblTextblocklote_valorpf_Internalname = sPrefix+"TEXTBLOCKLOTE_VALORPF";
         edtLote_ValorPF_Internalname = sPrefix+"LOTE_VALORPF";
         lblTextblocklote_qtdedmn_Internalname = sPrefix+"TEXTBLOCKLOTE_QTDEDMN";
         edtLote_QtdeDmn_Internalname = sPrefix+"LOTE_QTDEDMN";
         lblTextblocklote_nfe_Internalname = sPrefix+"TEXTBLOCKLOTE_NFE";
         edtLote_NFe_Internalname = sPrefix+"LOTE_NFE";
         lblTextblocklote_datanfe_Internalname = sPrefix+"TEXTBLOCKLOTE_DATANFE";
         edtLote_DataNfe_Internalname = sPrefix+"LOTE_DATANFE";
         lblTextblocklote_valor_Internalname = sPrefix+"TEXTBLOCKLOTE_VALOR";
         edtLote_Valor_Internalname = sPrefix+"LOTE_VALOR";
         lblTextblocklote_nfedataprotocolo_Internalname = sPrefix+"TEXTBLOCKLOTE_NFEDATAPROTOCOLO";
         edtLote_NFeDataProtocolo_Internalname = sPrefix+"LOTE_NFEDATAPROTOCOLO";
         lblTextblocklote_prevpagamento_Internalname = sPrefix+"TEXTBLOCKLOTE_PREVPAGAMENTO";
         edtLote_PrevPagamento_Internalname = sPrefix+"LOTE_PREVPAGAMENTO";
         lblTextblocklote_liqbanco_Internalname = sPrefix+"TEXTBLOCKLOTE_LIQBANCO";
         edtLote_LiqBanco_Internalname = sPrefix+"LOTE_LIQBANCO";
         lblTextblocklote_liqdata_Internalname = sPrefix+"TEXTBLOCKLOTE_LIQDATA";
         edtLote_LiqData_Internalname = sPrefix+"LOTE_LIQDATA";
         lblTextblocklote_liqvalor_Internalname = sPrefix+"TEXTBLOCKLOTE_LIQVALOR";
         edtLote_LiqValor_Internalname = sPrefix+"LOTE_LIQVALOR";
         lblTextblocklote_liqperc_Internalname = sPrefix+"TEXTBLOCKLOTE_LIQPERC";
         edtLote_LiqPerc_Internalname = sPrefix+"LOTE_LIQPERC";
         lblLote_liqperc_righttext_Internalname = sPrefix+"LOTE_LIQPERC_RIGHTTEXT";
         cellLote_liqperc_righttext_cell_Internalname = sPrefix+"LOTE_LIQPERC_RIGHTTEXT_CELL";
         tblTablemergedlote_liqvalor_Internalname = sPrefix+"TABLEMERGEDLOTE_LIQVALOR";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         bttBtnfechar_Internalname = sPrefix+"BTNFECHAR";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtLote_AreaTrabalhoCod_Internalname = sPrefix+"LOTE_AREATRABALHOCOD";
         edtLote_UserCod_Internalname = sPrefix+"LOTE_USERCOD";
         edtLote_PessoaCod_Internalname = sPrefix+"LOTE_PESSOACOD";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtLote_DataFim_Jsonclick = "";
         edtLote_DataIni_Jsonclick = "";
         edtLote_LiqPerc_Jsonclick = "";
         edtLote_LiqValor_Jsonclick = "";
         edtLote_LiqData_Jsonclick = "";
         edtLote_LiqBanco_Jsonclick = "";
         edtLote_PrevPagamento_Jsonclick = "";
         edtLote_NFeDataProtocolo_Jsonclick = "";
         edtLote_Valor_Jsonclick = "";
         edtLote_DataNfe_Jsonclick = "";
         edtLote_NFe_Jsonclick = "";
         edtLote_QtdeDmn_Jsonclick = "";
         edtLote_ValorPF_Jsonclick = "";
         edtLote_DataContrato_Jsonclick = "";
         edtLote_UserNom_Jsonclick = "";
         edtLote_Data_Jsonclick = "";
         edtLote_Nome_Jsonclick = "";
         edtLote_Numero_Jsonclick = "";
         bttBtndelete_Visible = 1;
         bttBtnupdate_Visible = 1;
         edtLote_UserNom_Link = "";
         edtLote_PessoaCod_Jsonclick = "";
         edtLote_PessoaCod_Visible = 1;
         edtLote_UserCod_Jsonclick = "";
         edtLote_UserCod_Visible = 1;
         edtLote_AreaTrabalhoCod_Jsonclick = "";
         edtLote_AreaTrabalhoCod_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13BS2',iparms:[{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14BS2',iparms:[{av:'A596Lote_Codigo',fld:'LOTE_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOFECHAR'","{handler:'E15BS2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV15Pgmname = "";
         scmdbuf = "";
         H00BS5_A1057Lote_ValorGlosas = new decimal[1] ;
         H00BS5_n1057Lote_ValorGlosas = new bool[] {false} ;
         H00BS7_A569Lote_QtdeDmn = new short[1] ;
         H00BS7_n569Lote_QtdeDmn = new bool[] {false} ;
         H00BS10_A568Lote_DataFim = new DateTime[] {DateTime.MinValue} ;
         H00BS10_A567Lote_DataIni = new DateTime[] {DateTime.MinValue} ;
         A568Lote_DataFim = DateTime.MinValue;
         A567Lote_DataIni = DateTime.MinValue;
         H00BS11_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         H00BS11_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         H00BS11_A512ContagemResultado_ValorPF = new decimal[1] ;
         H00BS11_n512ContagemResultado_ValorPF = new bool[] {false} ;
         H00BS11_A456ContagemResultado_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A562Lote_Numero = "";
         A563Lote_Nome = "";
         A564Lote_Data = (DateTime)(DateTime.MinValue);
         A2088Lote_ParecerFinal = "";
         A2087Lote_Comentarios = "";
         A844Lote_DataContrato = DateTime.MinValue;
         A674Lote_DataNfe = DateTime.MinValue;
         A1001Lote_NFeDataProtocolo = DateTime.MinValue;
         A857Lote_PrevPagamento = DateTime.MinValue;
         A675Lote_LiqBanco = "";
         A676Lote_LiqData = DateTime.MinValue;
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         H00BS18_A560Lote_PessoaCod = new int[1] ;
         H00BS18_n560Lote_PessoaCod = new bool[] {false} ;
         H00BS18_A559Lote_UserCod = new int[1] ;
         H00BS18_A595Lote_AreaTrabalhoCod = new int[1] ;
         H00BS18_A676Lote_LiqData = new DateTime[] {DateTime.MinValue} ;
         H00BS18_n676Lote_LiqData = new bool[] {false} ;
         H00BS18_A675Lote_LiqBanco = new String[] {""} ;
         H00BS18_n675Lote_LiqBanco = new bool[] {false} ;
         H00BS18_A857Lote_PrevPagamento = new DateTime[] {DateTime.MinValue} ;
         H00BS18_n857Lote_PrevPagamento = new bool[] {false} ;
         H00BS18_A1001Lote_NFeDataProtocolo = new DateTime[] {DateTime.MinValue} ;
         H00BS18_n1001Lote_NFeDataProtocolo = new bool[] {false} ;
         H00BS18_A674Lote_DataNfe = new DateTime[] {DateTime.MinValue} ;
         H00BS18_n674Lote_DataNfe = new bool[] {false} ;
         H00BS18_A673Lote_NFe = new int[1] ;
         H00BS18_n673Lote_NFe = new bool[] {false} ;
         H00BS18_A565Lote_ValorPF = new decimal[1] ;
         H00BS18_A844Lote_DataContrato = new DateTime[] {DateTime.MinValue} ;
         H00BS18_n844Lote_DataContrato = new bool[] {false} ;
         H00BS18_A2087Lote_Comentarios = new String[] {""} ;
         H00BS18_n2087Lote_Comentarios = new bool[] {false} ;
         H00BS18_A2088Lote_ParecerFinal = new String[] {""} ;
         H00BS18_n2088Lote_ParecerFinal = new bool[] {false} ;
         H00BS18_A561Lote_UserNom = new String[] {""} ;
         H00BS18_n561Lote_UserNom = new bool[] {false} ;
         H00BS18_A564Lote_Data = new DateTime[] {DateTime.MinValue} ;
         H00BS18_A563Lote_Nome = new String[] {""} ;
         H00BS18_A562Lote_Numero = new String[] {""} ;
         H00BS18_A596Lote_Codigo = new int[1] ;
         H00BS18_A569Lote_QtdeDmn = new short[1] ;
         H00BS18_n569Lote_QtdeDmn = new bool[] {false} ;
         H00BS18_A568Lote_DataFim = new DateTime[] {DateTime.MinValue} ;
         H00BS18_A567Lote_DataIni = new DateTime[] {DateTime.MinValue} ;
         H00BS18_A1057Lote_ValorGlosas = new decimal[1] ;
         H00BS18_n1057Lote_ValorGlosas = new bool[] {false} ;
         H00BS18_A677Lote_LiqValor = new decimal[1] ;
         H00BS18_n677Lote_LiqValor = new bool[] {false} ;
         A561Lote_UserNom = "";
         H00BS22_A1057Lote_ValorGlosas = new decimal[1] ;
         H00BS22_n1057Lote_ValorGlosas = new bool[] {false} ;
         H00BS24_A569Lote_QtdeDmn = new short[1] ;
         H00BS24_n569Lote_QtdeDmn = new bool[] {false} ;
         H00BS27_A568Lote_DataFim = new DateTime[] {DateTime.MinValue} ;
         H00BS27_A567Lote_DataIni = new DateTime[] {DateTime.MinValue} ;
         hsh = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblocklote_numero_Jsonclick = "";
         lblTextblocklote_nome_Jsonclick = "";
         lblTextblocklote_data_Jsonclick = "";
         lblTextblocklote_usernom_Jsonclick = "";
         lblTextblocklote_dataini_Jsonclick = "";
         lblTextblocklote_parecerfinal_Jsonclick = "";
         lblTextblocklote_comentarios_Jsonclick = "";
         lblTextblocklote_datacontrato_Jsonclick = "";
         lblTextblocklote_valorpf_Jsonclick = "";
         lblTextblocklote_qtdedmn_Jsonclick = "";
         lblTextblocklote_nfe_Jsonclick = "";
         lblTextblocklote_datanfe_Jsonclick = "";
         lblTextblocklote_valor_Jsonclick = "";
         lblTextblocklote_nfedataprotocolo_Jsonclick = "";
         lblTextblocklote_prevpagamento_Jsonclick = "";
         lblTextblocklote_liqbanco_Jsonclick = "";
         lblTextblocklote_liqdata_Jsonclick = "";
         lblTextblocklote_liqvalor_Jsonclick = "";
         lblTextblocklote_liqperc_Jsonclick = "";
         lblLote_liqperc_righttext_Jsonclick = "";
         lblTextblocklote_datafim_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA596Lote_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.lotegeneral__default(),
            new Object[][] {
                new Object[] {
               H00BS5_A1057Lote_ValorGlosas, H00BS5_n1057Lote_ValorGlosas
               }
               , new Object[] {
               H00BS7_A569Lote_QtdeDmn, H00BS7_n569Lote_QtdeDmn
               }
               , new Object[] {
               H00BS10_A568Lote_DataFim, H00BS10_A567Lote_DataIni
               }
               , new Object[] {
               H00BS11_A597ContagemResultado_LoteAceiteCod, H00BS11_n597ContagemResultado_LoteAceiteCod, H00BS11_A512ContagemResultado_ValorPF, H00BS11_n512ContagemResultado_ValorPF, H00BS11_A456ContagemResultado_Codigo
               }
               , new Object[] {
               H00BS18_A560Lote_PessoaCod, H00BS18_n560Lote_PessoaCod, H00BS18_A559Lote_UserCod, H00BS18_A595Lote_AreaTrabalhoCod, H00BS18_A676Lote_LiqData, H00BS18_n676Lote_LiqData, H00BS18_A675Lote_LiqBanco, H00BS18_n675Lote_LiqBanco, H00BS18_A857Lote_PrevPagamento, H00BS18_n857Lote_PrevPagamento,
               H00BS18_A1001Lote_NFeDataProtocolo, H00BS18_n1001Lote_NFeDataProtocolo, H00BS18_A674Lote_DataNfe, H00BS18_n674Lote_DataNfe, H00BS18_A673Lote_NFe, H00BS18_n673Lote_NFe, H00BS18_A565Lote_ValorPF, H00BS18_A844Lote_DataContrato, H00BS18_n844Lote_DataContrato, H00BS18_A2087Lote_Comentarios,
               H00BS18_n2087Lote_Comentarios, H00BS18_A2088Lote_ParecerFinal, H00BS18_n2088Lote_ParecerFinal, H00BS18_A561Lote_UserNom, H00BS18_n561Lote_UserNom, H00BS18_A564Lote_Data, H00BS18_A563Lote_Nome, H00BS18_A562Lote_Numero, H00BS18_A596Lote_Codigo, H00BS18_A569Lote_QtdeDmn,
               H00BS18_n569Lote_QtdeDmn, H00BS18_A568Lote_DataFim, H00BS18_A567Lote_DataIni, H00BS18_A1057Lote_ValorGlosas, H00BS18_n1057Lote_ValorGlosas, H00BS18_A677Lote_LiqValor, H00BS18_n677Lote_LiqValor
               }
               , new Object[] {
               H00BS22_A1057Lote_ValorGlosas, H00BS22_n1057Lote_ValorGlosas
               }
               , new Object[] {
               H00BS24_A569Lote_QtdeDmn, H00BS24_n569Lote_QtdeDmn
               }
               , new Object[] {
               H00BS27_A568Lote_DataFim, H00BS27_A567Lote_DataIni
               }
            }
         );
         AV15Pgmname = "LoteGeneral";
         /* GeneXus formulas. */
         AV15Pgmname = "LoteGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A569Lote_QtdeDmn ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A596Lote_Codigo ;
      private int wcpOA596Lote_Codigo ;
      private int A673Lote_NFe ;
      private int A595Lote_AreaTrabalhoCod ;
      private int A559Lote_UserCod ;
      private int edtLote_AreaTrabalhoCod_Visible ;
      private int edtLote_UserCod_Visible ;
      private int A560Lote_PessoaCod ;
      private int edtLote_PessoaCod_Visible ;
      private int bttBtndelete_Visible ;
      private int bttBtnupdate_Visible ;
      private int AV12Lote_Codigo ;
      private int idxLst ;
      private decimal A1057Lote_ValorGlosas ;
      private decimal A572Lote_Valor ;
      private decimal A1058Lote_ValorOSs ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal GXt_decimal1 ;
      private decimal A606ContagemResultado_ValorFinal ;
      private decimal A565Lote_ValorPF ;
      private decimal A677Lote_LiqValor ;
      private decimal A681Lote_LiqPerc ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV15Pgmname ;
      private String scmdbuf ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A562Lote_Numero ;
      private String A563Lote_Nome ;
      private String A675Lote_LiqBanco ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String edtLote_AreaTrabalhoCod_Internalname ;
      private String edtLote_AreaTrabalhoCod_Jsonclick ;
      private String edtLote_UserCod_Internalname ;
      private String edtLote_UserCod_Jsonclick ;
      private String edtLote_PessoaCod_Internalname ;
      private String edtLote_PessoaCod_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String A561Lote_UserNom ;
      private String edtLote_Numero_Internalname ;
      private String edtLote_Nome_Internalname ;
      private String edtLote_Data_Internalname ;
      private String edtLote_UserNom_Internalname ;
      private String edtLote_DataIni_Internalname ;
      private String edtLote_DataFim_Internalname ;
      private String edtLote_ParecerFinal_Internalname ;
      private String edtLote_Comentarios_Internalname ;
      private String edtLote_DataContrato_Internalname ;
      private String edtLote_ValorPF_Internalname ;
      private String edtLote_QtdeDmn_Internalname ;
      private String edtLote_NFe_Internalname ;
      private String edtLote_DataNfe_Internalname ;
      private String edtLote_Valor_Internalname ;
      private String edtLote_NFeDataProtocolo_Internalname ;
      private String edtLote_PrevPagamento_Internalname ;
      private String edtLote_LiqBanco_Internalname ;
      private String edtLote_LiqData_Internalname ;
      private String edtLote_LiqValor_Internalname ;
      private String edtLote_LiqPerc_Internalname ;
      private String hsh ;
      private String bttBtndelete_Internalname ;
      private String bttBtnupdate_Internalname ;
      private String edtLote_UserNom_Link ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocklote_numero_Internalname ;
      private String lblTextblocklote_numero_Jsonclick ;
      private String edtLote_Numero_Jsonclick ;
      private String lblTextblocklote_nome_Internalname ;
      private String lblTextblocklote_nome_Jsonclick ;
      private String edtLote_Nome_Jsonclick ;
      private String lblTextblocklote_data_Internalname ;
      private String lblTextblocklote_data_Jsonclick ;
      private String edtLote_Data_Jsonclick ;
      private String lblTextblocklote_usernom_Internalname ;
      private String lblTextblocklote_usernom_Jsonclick ;
      private String edtLote_UserNom_Jsonclick ;
      private String lblTextblocklote_dataini_Internalname ;
      private String lblTextblocklote_dataini_Jsonclick ;
      private String lblTextblocklote_parecerfinal_Internalname ;
      private String lblTextblocklote_parecerfinal_Jsonclick ;
      private String lblTextblocklote_comentarios_Internalname ;
      private String lblTextblocklote_comentarios_Jsonclick ;
      private String lblTextblocklote_datacontrato_Internalname ;
      private String lblTextblocklote_datacontrato_Jsonclick ;
      private String edtLote_DataContrato_Jsonclick ;
      private String lblTextblocklote_valorpf_Internalname ;
      private String lblTextblocklote_valorpf_Jsonclick ;
      private String edtLote_ValorPF_Jsonclick ;
      private String lblTextblocklote_qtdedmn_Internalname ;
      private String lblTextblocklote_qtdedmn_Jsonclick ;
      private String edtLote_QtdeDmn_Jsonclick ;
      private String lblTextblocklote_nfe_Internalname ;
      private String lblTextblocklote_nfe_Jsonclick ;
      private String edtLote_NFe_Jsonclick ;
      private String lblTextblocklote_datanfe_Internalname ;
      private String lblTextblocklote_datanfe_Jsonclick ;
      private String edtLote_DataNfe_Jsonclick ;
      private String lblTextblocklote_valor_Internalname ;
      private String lblTextblocklote_valor_Jsonclick ;
      private String edtLote_Valor_Jsonclick ;
      private String lblTextblocklote_nfedataprotocolo_Internalname ;
      private String lblTextblocklote_nfedataprotocolo_Jsonclick ;
      private String edtLote_NFeDataProtocolo_Jsonclick ;
      private String lblTextblocklote_prevpagamento_Internalname ;
      private String lblTextblocklote_prevpagamento_Jsonclick ;
      private String edtLote_PrevPagamento_Jsonclick ;
      private String lblTextblocklote_liqbanco_Internalname ;
      private String lblTextblocklote_liqbanco_Jsonclick ;
      private String edtLote_LiqBanco_Jsonclick ;
      private String lblTextblocklote_liqdata_Internalname ;
      private String lblTextblocklote_liqdata_Jsonclick ;
      private String edtLote_LiqData_Jsonclick ;
      private String lblTextblocklote_liqvalor_Internalname ;
      private String lblTextblocklote_liqvalor_Jsonclick ;
      private String tblTablemergedlote_liqvalor_Internalname ;
      private String edtLote_LiqValor_Jsonclick ;
      private String lblTextblocklote_liqperc_Internalname ;
      private String lblTextblocklote_liqperc_Jsonclick ;
      private String edtLote_LiqPerc_Jsonclick ;
      private String cellLote_liqperc_righttext_cell_Internalname ;
      private String lblLote_liqperc_righttext_Internalname ;
      private String lblLote_liqperc_righttext_Jsonclick ;
      private String tblTablemergedlote_dataini_Internalname ;
      private String edtLote_DataIni_Jsonclick ;
      private String lblTextblocklote_datafim_Internalname ;
      private String lblTextblocklote_datafim_Jsonclick ;
      private String edtLote_DataFim_Jsonclick ;
      private String sCtrlA596Lote_Codigo ;
      private DateTime A564Lote_Data ;
      private DateTime A568Lote_DataFim ;
      private DateTime A567Lote_DataIni ;
      private DateTime A844Lote_DataContrato ;
      private DateTime A674Lote_DataNfe ;
      private DateTime A1001Lote_NFeDataProtocolo ;
      private DateTime A857Lote_PrevPagamento ;
      private DateTime A676Lote_LiqData ;
      private bool entryPointCalled ;
      private bool n1057Lote_ValorGlosas ;
      private bool n569Lote_QtdeDmn ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n560Lote_PessoaCod ;
      private bool n676Lote_LiqData ;
      private bool n675Lote_LiqBanco ;
      private bool n857Lote_PrevPagamento ;
      private bool n1001Lote_NFeDataProtocolo ;
      private bool n674Lote_DataNfe ;
      private bool n673Lote_NFe ;
      private bool n844Lote_DataContrato ;
      private bool n2087Lote_Comentarios ;
      private bool n2088Lote_ParecerFinal ;
      private bool n561Lote_UserNom ;
      private bool n677Lote_LiqValor ;
      private bool returnInSub ;
      private String A2088Lote_ParecerFinal ;
      private String A2087Lote_Comentarios ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private decimal[] H00BS5_A1057Lote_ValorGlosas ;
      private bool[] H00BS5_n1057Lote_ValorGlosas ;
      private short[] H00BS7_A569Lote_QtdeDmn ;
      private bool[] H00BS7_n569Lote_QtdeDmn ;
      private DateTime[] H00BS10_A568Lote_DataFim ;
      private DateTime[] H00BS10_A567Lote_DataIni ;
      private int[] H00BS11_A597ContagemResultado_LoteAceiteCod ;
      private bool[] H00BS11_n597ContagemResultado_LoteAceiteCod ;
      private decimal[] H00BS11_A512ContagemResultado_ValorPF ;
      private bool[] H00BS11_n512ContagemResultado_ValorPF ;
      private int[] H00BS11_A456ContagemResultado_Codigo ;
      private int[] H00BS18_A560Lote_PessoaCod ;
      private bool[] H00BS18_n560Lote_PessoaCod ;
      private int[] H00BS18_A559Lote_UserCod ;
      private int[] H00BS18_A595Lote_AreaTrabalhoCod ;
      private DateTime[] H00BS18_A676Lote_LiqData ;
      private bool[] H00BS18_n676Lote_LiqData ;
      private String[] H00BS18_A675Lote_LiqBanco ;
      private bool[] H00BS18_n675Lote_LiqBanco ;
      private DateTime[] H00BS18_A857Lote_PrevPagamento ;
      private bool[] H00BS18_n857Lote_PrevPagamento ;
      private DateTime[] H00BS18_A1001Lote_NFeDataProtocolo ;
      private bool[] H00BS18_n1001Lote_NFeDataProtocolo ;
      private DateTime[] H00BS18_A674Lote_DataNfe ;
      private bool[] H00BS18_n674Lote_DataNfe ;
      private int[] H00BS18_A673Lote_NFe ;
      private bool[] H00BS18_n673Lote_NFe ;
      private decimal[] H00BS18_A565Lote_ValorPF ;
      private DateTime[] H00BS18_A844Lote_DataContrato ;
      private bool[] H00BS18_n844Lote_DataContrato ;
      private String[] H00BS18_A2087Lote_Comentarios ;
      private bool[] H00BS18_n2087Lote_Comentarios ;
      private String[] H00BS18_A2088Lote_ParecerFinal ;
      private bool[] H00BS18_n2088Lote_ParecerFinal ;
      private String[] H00BS18_A561Lote_UserNom ;
      private bool[] H00BS18_n561Lote_UserNom ;
      private DateTime[] H00BS18_A564Lote_Data ;
      private String[] H00BS18_A563Lote_Nome ;
      private String[] H00BS18_A562Lote_Numero ;
      private int[] H00BS18_A596Lote_Codigo ;
      private short[] H00BS18_A569Lote_QtdeDmn ;
      private bool[] H00BS18_n569Lote_QtdeDmn ;
      private DateTime[] H00BS18_A568Lote_DataFim ;
      private DateTime[] H00BS18_A567Lote_DataIni ;
      private decimal[] H00BS18_A1057Lote_ValorGlosas ;
      private bool[] H00BS18_n1057Lote_ValorGlosas ;
      private decimal[] H00BS18_A677Lote_LiqValor ;
      private bool[] H00BS18_n677Lote_LiqValor ;
      private decimal[] H00BS22_A1057Lote_ValorGlosas ;
      private bool[] H00BS22_n1057Lote_ValorGlosas ;
      private short[] H00BS24_A569Lote_QtdeDmn ;
      private bool[] H00BS24_n569Lote_QtdeDmn ;
      private DateTime[] H00BS27_A568Lote_DataFim ;
      private DateTime[] H00BS27_A567Lote_DataIni ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class lotegeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00BS5 ;
          prmH00BS5 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BS7 ;
          prmH00BS7 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BS10 ;
          prmH00BS10 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BS11 ;
          prmH00BS11 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BS18 ;
          prmH00BS18 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferH00BS18 ;
          cmdBufferH00BS18=" SELECT T2.[Usuario_PessoaCod] AS Lote_PessoaCod, T1.[Lote_UserCod] AS Lote_UserCod, T1.[Lote_AreaTrabalhoCod], T1.[Lote_LiqData], T1.[Lote_LiqBanco], T1.[Lote_PrevPagamento], T1.[Lote_NFeDataProtocolo], T1.[Lote_DataNfe], T1.[Lote_NFe], T1.[Lote_ValorPF], T1.[Lote_DataContrato], T1.[Lote_Comentarios], T1.[Lote_ParecerFinal], T3.[Pessoa_Nome] AS Lote_UserNom, T1.[Lote_Data], T1.[Lote_Nome], T1.[Lote_Numero], T1.[Lote_Codigo], COALESCE( T5.[Lote_QtdeDmn], 0) AS Lote_QtdeDmn, COALESCE( T6.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) AS Lote_DataFim, COALESCE( T6.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) AS Lote_DataIni, COALESCE( T4.[Lote_ValorGlosas], 0) AS Lote_ValorGlosas, T1.[Lote_LiqValor] FROM ((((([Lote] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[Lote_UserCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN (SELECT COALESCE( T9.[GXC2], 0) + COALESCE( T8.[GXC3], 0) AS Lote_ValorGlosas, T7.[Lote_Codigo] FROM (([Lote] T7 WITH (NOLOCK) LEFT JOIN (SELECT SUM(T10.[ContagemResultadoIndicadores_Valor]) AS GXC3, T11.[Lote_Codigo] FROM [ContagemResultadoIndicadores] T10 WITH (NOLOCK),  [Lote] T11 WITH (NOLOCK) WHERE T10.[ContagemResultadoIndicadores_LoteCod] = T11.[Lote_Codigo] GROUP BY T11.[Lote_Codigo] ) T8 ON T8.[Lote_Codigo] = T7.[Lote_Codigo]) LEFT JOIN (SELECT SUM([ContagemResultado_GlsValor]) AS GXC2, [ContagemResultado_LoteAceiteCod] FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T9 ON T9.[ContagemResultado_LoteAceiteCod] = T7.[Lote_Codigo]) ) T4 ON T4.[Lote_Codigo] = T1.[Lote_Codigo]) LEFT JOIN (SELECT COUNT(*) AS Lote_QtdeDmn, [ContagemResultado_LoteAceiteCod] FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] "
          + " ) T5 ON T5.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo]) LEFT JOIN (SELECT MAX(COALESCE( T8.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataFim, T7.[ContagemResultado_LoteAceiteCod], MIN(COALESCE( T8.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataIni FROM ([ContagemResultado] T7 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T8 ON T8.[ContagemResultado_Codigo] = T7.[ContagemResultado_Codigo]) GROUP BY T7.[ContagemResultado_LoteAceiteCod] ) T6 ON T6.[ContagemResultado_LoteAceiteCod] = T1.[Lote_Codigo]) WHERE T1.[Lote_Codigo] = @Lote_Codigo ORDER BY T1.[Lote_Codigo]" ;
          Object[] prmH00BS22 ;
          prmH00BS22 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BS24 ;
          prmH00BS24 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00BS27 ;
          prmH00BS27 = new Object[] {
          new Object[] {"@Lote_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00BS5", "SELECT COALESCE( T1.[Lote_ValorGlosas], 0) AS Lote_ValorGlosas FROM (SELECT COALESCE( T4.[GXC2], 0) + COALESCE( T3.[GXC3], 0) AS Lote_ValorGlosas, T2.[Lote_Codigo] FROM (([Lote] T2 WITH (NOLOCK) LEFT JOIN (SELECT SUM(T5.[ContagemResultadoIndicadores_Valor]) AS GXC3, T6.[Lote_Codigo] FROM [ContagemResultadoIndicadores] T5 WITH (NOLOCK),  [Lote] T6 WITH (NOLOCK) WHERE T5.[ContagemResultadoIndicadores_LoteCod] = T6.[Lote_Codigo] GROUP BY T6.[Lote_Codigo] ) T3 ON T3.[Lote_Codigo] = T2.[Lote_Codigo]) LEFT JOIN (SELECT SUM([ContagemResultado_GlsValor]) AS GXC2, [ContagemResultado_LoteAceiteCod] FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T4 ON T4.[ContagemResultado_LoteAceiteCod] = T2.[Lote_Codigo]) ) T1 WHERE T1.[Lote_Codigo] = @Lote_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BS5,1,0,true,true )
             ,new CursorDef("H00BS7", "SELECT COALESCE( T1.[Lote_QtdeDmn], 0) AS Lote_QtdeDmn FROM (SELECT COUNT(*) AS Lote_QtdeDmn, [ContagemResultado_LoteAceiteCod] FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T1 WHERE T1.[ContagemResultado_LoteAceiteCod] = @Lote_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BS7,1,0,true,true )
             ,new CursorDef("H00BS10", "SELECT COALESCE( T1.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) AS Lote_DataFim, COALESCE( T1.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) AS Lote_DataIni FROM (SELECT MAX(COALESCE( T3.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataFim, T2.[ContagemResultado_LoteAceiteCod], MIN(COALESCE( T3.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataIni FROM ([ContagemResultado] T2 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T3 ON T3.[ContagemResultado_Codigo] = T2.[ContagemResultado_Codigo]) GROUP BY T2.[ContagemResultado_LoteAceiteCod] ) T1 WHERE T1.[ContagemResultado_LoteAceiteCod] = @Lote_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BS10,1,0,true,true )
             ,new CursorDef("H00BS11", "SELECT [ContagemResultado_LoteAceiteCod], [ContagemResultado_ValorPF], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_LoteAceiteCod] = @Lote_Codigo ORDER BY [ContagemResultado_LoteAceiteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BS11,100,0,true,false )
             ,new CursorDef("H00BS18", cmdBufferH00BS18,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BS18,1,0,true,true )
             ,new CursorDef("H00BS22", "SELECT COALESCE( T1.[Lote_ValorGlosas], 0) AS Lote_ValorGlosas FROM (SELECT COALESCE( T4.[GXC2], 0) + COALESCE( T3.[GXC3], 0) AS Lote_ValorGlosas, T2.[Lote_Codigo] FROM (([Lote] T2 WITH (NOLOCK) LEFT JOIN (SELECT SUM(T5.[ContagemResultadoIndicadores_Valor]) AS GXC3, T6.[Lote_Codigo] FROM [ContagemResultadoIndicadores] T5 WITH (NOLOCK),  [Lote] T6 WITH (NOLOCK) WHERE T5.[ContagemResultadoIndicadores_LoteCod] = T6.[Lote_Codigo] GROUP BY T6.[Lote_Codigo] ) T3 ON T3.[Lote_Codigo] = T2.[Lote_Codigo]) LEFT JOIN (SELECT SUM([ContagemResultado_GlsValor]) AS GXC2, [ContagemResultado_LoteAceiteCod] FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T4 ON T4.[ContagemResultado_LoteAceiteCod] = T2.[Lote_Codigo]) ) T1 WHERE T1.[Lote_Codigo] = @Lote_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BS22,1,0,true,false )
             ,new CursorDef("H00BS24", "SELECT COALESCE( T1.[Lote_QtdeDmn], 0) AS Lote_QtdeDmn FROM (SELECT COUNT(*) AS Lote_QtdeDmn, [ContagemResultado_LoteAceiteCod] FROM [ContagemResultado] WITH (NOLOCK) GROUP BY [ContagemResultado_LoteAceiteCod] ) T1 WHERE T1.[ContagemResultado_LoteAceiteCod] = @Lote_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BS24,1,0,true,false )
             ,new CursorDef("H00BS27", "SELECT COALESCE( T1.[Lote_DataFim], convert( DATETIME, '17530101', 112 )) AS Lote_DataFim, COALESCE( T1.[Lote_DataIni], convert( DATETIME, '17530101', 112 )) AS Lote_DataIni FROM (SELECT MAX(COALESCE( T3.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataFim, T2.[ContagemResultado_LoteAceiteCod], MIN(COALESCE( T3.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) AS Lote_DataIni FROM ([ContagemResultado] T2 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T3 ON T3.[ContagemResultado_Codigo] = T2.[ContagemResultado_Codigo]) GROUP BY T2.[ContagemResultado_LoteAceiteCod] ) T1 WHERE T1.[ContagemResultado_LoteAceiteCod] = @Lote_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00BS27,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 1 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 2 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((decimal[]) buf[16])[0] = rslt.getDecimal(10) ;
                ((DateTime[]) buf[17])[0] = rslt.getGXDate(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getLongVarchar(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((String[]) buf[21])[0] = rslt.getLongVarchar(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getString(14, 100) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((DateTime[]) buf[25])[0] = rslt.getGXDateTime(15) ;
                ((String[]) buf[26])[0] = rslt.getString(16, 50) ;
                ((String[]) buf[27])[0] = rslt.getString(17, 10) ;
                ((int[]) buf[28])[0] = rslt.getInt(18) ;
                ((short[]) buf[29])[0] = rslt.getShort(19) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(19);
                ((DateTime[]) buf[31])[0] = rslt.getGXDate(20) ;
                ((DateTime[]) buf[32])[0] = rslt.getGXDate(21) ;
                ((decimal[]) buf[33])[0] = rslt.getDecimal(22) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(22);
                ((decimal[]) buf[35])[0] = rslt.getDecimal(23) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(23);
                return;
             case 5 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
