/*
               File: PRC_FuncaoDadosDltTabela
        Description: Delete tabela da Fun��o de Dados
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:50.10
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_funcaodadosdlttabela : GXProcedure
   {
      public prc_funcaodadosdlttabela( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_funcaodadosdlttabela( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_FuncaoDados_Codigo ,
                           ref int aP1_Tabela_Codigo )
      {
         this.A368FuncaoDados_Codigo = aP0_FuncaoDados_Codigo;
         this.A172Tabela_Codigo = aP1_Tabela_Codigo;
         initialize();
         executePrivate();
         aP0_FuncaoDados_Codigo=this.A368FuncaoDados_Codigo;
         aP1_Tabela_Codigo=this.A172Tabela_Codigo;
      }

      public int executeUdp( ref int aP0_FuncaoDados_Codigo )
      {
         this.A368FuncaoDados_Codigo = aP0_FuncaoDados_Codigo;
         this.A172Tabela_Codigo = aP1_Tabela_Codigo;
         initialize();
         executePrivate();
         aP0_FuncaoDados_Codigo=this.A368FuncaoDados_Codigo;
         aP1_Tabela_Codigo=this.A172Tabela_Codigo;
         return A172Tabela_Codigo ;
      }

      public void executeSubmit( ref int aP0_FuncaoDados_Codigo ,
                                 ref int aP1_Tabela_Codigo )
      {
         prc_funcaodadosdlttabela objprc_funcaodadosdlttabela;
         objprc_funcaodadosdlttabela = new prc_funcaodadosdlttabela();
         objprc_funcaodadosdlttabela.A368FuncaoDados_Codigo = aP0_FuncaoDados_Codigo;
         objprc_funcaodadosdlttabela.A172Tabela_Codigo = aP1_Tabela_Codigo;
         objprc_funcaodadosdlttabela.context.SetSubmitInitialConfig(context);
         objprc_funcaodadosdlttabela.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_funcaodadosdlttabela);
         aP0_FuncaoDados_Codigo=this.A368FuncaoDados_Codigo;
         aP1_Tabela_Codigo=this.A172Tabela_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_funcaodadosdlttabela)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Optimized DELETE. */
         /* Using cursor P00282 */
         pr_default.execute(0, new Object[] {A368FuncaoDados_Codigo, A172Tabela_Codigo});
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("FuncaoDadosTabela") ;
         /* End optimized DELETE. */
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_FuncaoDadosDltTabela");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_funcaodadosdlttabela__default(),
            new Object[][] {
                new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A368FuncaoDados_Codigo ;
      private int A172Tabela_Codigo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_FuncaoDados_Codigo ;
      private int aP1_Tabela_Codigo ;
      private IDataStoreProvider pr_default ;
   }

   public class prc_funcaodadosdlttabela__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new UpdateCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00282 ;
          prmP00282 = new Object[] {
          new Object[] {"@FuncaoDados_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00282", "DELETE FROM [FuncaoDadosTabela]  WHERE [FuncaoDados_Codigo] = @FuncaoDados_Codigo and [Tabela_Codigo] = @Tabela_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00282)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
