

--Demanda_147_ALTER_TABLE_Projeto.sql


ALTER TABLE [Projeto] ADD [Projeto_Identificador]  	   VARCHAR(50) 	NULL;
ALTER TABLE [Projeto] ADD [Projeto_Objetivo]           VARCHAR(MAX) NULL;
ALTER TABLE [Projeto] ADD [Projeto_DTInicio]           DATETIME     NULL;
ALTER TABLE [Projeto] ADD [Projeto_DTFim]              DATETIME     NULL;
ALTER TABLE [Projeto] ADD [Projeto_PrazoPrevisto]      SMALLINT     NULL;
ALTER TABLE [Projeto] ADD [Projeto_EsforcoPrevisto]    SMALLINT     NULL;
ALTER TABLE [Projeto] ADD [Projeto_CustoPrevisto]      MONEY     	NULL;
