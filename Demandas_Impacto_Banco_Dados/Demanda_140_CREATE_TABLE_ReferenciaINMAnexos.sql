
--Demanda_140_CREATE_TABLE_ReferenciaINMAnexos.sql

CREATE TABLE [ReferenciaINMAnexos] (
  [ReferenciaINM_Codigo]            INT     		 NOT NULL,
  [ReferenciaINMAnexos_Codigo]      INT     		 NOT NULL,
  [ReferenciaINMAnexos_Descricao]   VARCHAR(MAX)     NOT NULL,
  [ReferenciaINMAnexos_Link] 		VARCHAR(MAX)     NULL,
  [ReferenciaINMAnexos_Arquivo]     VARBINARY(MAX)   NULL,
  [ReferenciaINMAnexos_ArquivoNome] CHAR(50)     	 NULL,
  [ReferenciaINMAnexos_ArquivoTipo] CHAR(10)     	 NULL,
  [ReferenciaINMAnexos_Data]        DATETIME     	 NULL,
  [TipoDocumento_Codigo]            INT     		 NULL,
  PRIMARY KEY ( [ReferenciaINM_Codigo],[ReferenciaINMAnexos_Codigo] ));

CREATE NONCLUSTERED INDEX [IREFERENCIAINMANEXOS1] ON [ReferenciaINMAnexos] ([TipoDocumento_Codigo]);

ALTER TABLE [ReferenciaINMAnexos] ADD CONSTRAINT [IREFERENCIAINMANEXOS2] FOREIGN KEY ( [ReferenciaINM_Codigo] ) REFERENCES [ReferenciaINM]([ReferenciaINM_Codigo]);

ALTER TABLE [ReferenciaINMAnexos]
 ADD CONSTRAINT [IREFERENCIAINMANEXOS1] FOREIGN KEY ( [TipoDocumento_Codigo] ) REFERENCES [TipoDocumento]([TipoDocumento_Codigo]);

 
 
 

