
-- Demanda_16_ALTER_TABLE_Servico.sql


ALTER TABLE [Servico]
ADD [Servico_PausaSLA] BIT     NOT NULL CONSTRAINT Servico_PausaSLAServico_DEFAULT DEFAULT Convert(BIT,0);

ALTER TABLE [Servico] DROP CONSTRAINT Servico_PausaSLAServico_DEFAULT;

 
