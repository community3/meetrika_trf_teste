/*
               File: WP_HelpSistema
        Description: Help
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:52:26.2
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_helpsistema : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_helpsistema( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_helpsistema( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_HelpSistema_Objeto )
      {
         this.AV5HelpSistema_Objeto = aP0_HelpSistema_Objeto;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV5HelpSistema_Objeto = gxfirstwebparm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5HelpSistema_Objeto", AV5HelpSistema_Objeto);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vHELPSISTEMA_OBJETO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV5HelpSistema_Objeto, ""))));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAML2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTML2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282352264");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" novalidate action=\""+formatLink("wp_helpsistema.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV5HelpSistema_Objeto))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vHELPSISTEMA_DESCRICAO", AV8HelpSistema_Descricao);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vHELPSISTEMA", AV6HelpSistema);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vHELPSISTEMA", AV6HelpSistema);
         }
         GxWebStd.gx_hidden_field( context, "vHELPSISTEMA_OBJETO", AV5HelpSistema_Objeto);
         GxWebStd.gx_hidden_field( context, "gxhash_vHELPSISTEMA_OBJETO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV5HelpSistema_Objeto, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vHELPSISTEMA_OBJETO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV5HelpSistema_Objeto, ""))));
         GxWebStd.gx_hidden_field( context, "HELPSISTEMA_DESCRICAO_Width", StringUtil.RTrim( Helpsistema_descricao_Width));
         GxWebStd.gx_hidden_field( context, "HELPSISTEMA_DESCRICAO_Height", StringUtil.RTrim( Helpsistema_descricao_Height));
         GxWebStd.gx_hidden_field( context, "HELPSISTEMA_DESCRICAO_Skin", StringUtil.RTrim( Helpsistema_descricao_Skin));
         GxWebStd.gx_hidden_field( context, "HELPSISTEMA_DESCRICAO_Toolbar", StringUtil.RTrim( Helpsistema_descricao_Toolbar));
         GxWebStd.gx_hidden_field( context, "HELPSISTEMA_DESCRICAO_Enabled", StringUtil.BoolToStr( Helpsistema_descricao_Enabled));
         GxWebStd.gx_hidden_field( context, "HELPSISTEMA_DESCRICAO_Customtoolbar", StringUtil.RTrim( Helpsistema_descricao_Customtoolbar));
         GxWebStd.gx_hidden_field( context, "HELPSISTEMA_DESCRICAO_Customconfiguration", StringUtil.RTrim( Helpsistema_descricao_Customconfiguration));
         GxWebStd.gx_hidden_field( context, "HELPSISTEMA_DESCRICAO_Captionclass", StringUtil.RTrim( Helpsistema_descricao_Captionclass));
         GxWebStd.gx_hidden_field( context, "HELPSISTEMA_DESCRICAO_Captionposition", StringUtil.RTrim( Helpsistema_descricao_Captionposition));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEML2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTML2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_helpsistema.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV5HelpSistema_Objeto)) ;
      }

      public override String GetPgmname( )
      {
         return "WP_HelpSistema" ;
      }

      public override String GetPgmdesc( )
      {
         return "Help" ;
      }

      protected void WBML0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            wb_table1_3_ML2( true) ;
         }
         else
         {
            wb_table1_3_ML2( false) ;
         }
         return  ;
      }

      protected void wb_table1_3_ML2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top");
         }
         wbLoad = true;
      }

      protected void STARTML2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Help", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPML0( ) ;
      }

      protected void WSML2( )
      {
         STARTML2( ) ;
         EVTML2( ) ;
      }

      protected void EVTML2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11ML2 */
                              E11ML2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'INCLUIR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12ML2 */
                              E12ML2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13ML2 */
                              E13ML2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEML2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAML2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFML2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFML2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E13ML2 */
            E13ML2 ();
            WBML0( ) ;
         }
      }

      protected void STRUPML0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11ML2 */
         E11ML2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            /* Read saved values. */
            AV8HelpSistema_Descricao = cgiGet( "vHELPSISTEMA_DESCRICAO");
            Helpsistema_descricao_Width = cgiGet( "HELPSISTEMA_DESCRICAO_Width");
            Helpsistema_descricao_Height = cgiGet( "HELPSISTEMA_DESCRICAO_Height");
            Helpsistema_descricao_Skin = cgiGet( "HELPSISTEMA_DESCRICAO_Skin");
            Helpsistema_descricao_Toolbar = cgiGet( "HELPSISTEMA_DESCRICAO_Toolbar");
            Helpsistema_descricao_Enabled = StringUtil.StrToBool( cgiGet( "HELPSISTEMA_DESCRICAO_Enabled"));
            Helpsistema_descricao_Customtoolbar = cgiGet( "HELPSISTEMA_DESCRICAO_Customtoolbar");
            Helpsistema_descricao_Customconfiguration = cgiGet( "HELPSISTEMA_DESCRICAO_Customconfiguration");
            Helpsistema_descricao_Captionclass = cgiGet( "HELPSISTEMA_DESCRICAO_Captionclass");
            Helpsistema_descricao_Captionposition = cgiGet( "HELPSISTEMA_DESCRICAO_Captionposition");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11ML2 */
         E11ML2 ();
         if (returnInSub) return;
      }

      protected void E11ML2( )
      {
         /* Start Routine */
         AV11GXLvl2 = 0;
         /* Using cursor H00ML2 */
         pr_default.execute(0, new Object[] {AV5HelpSistema_Objeto});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1639HelpSistema_Objeto = H00ML2_A1639HelpSistema_Objeto[0];
            A1638HelpSistema_Codigo = H00ML2_A1638HelpSistema_Codigo[0];
            AV11GXLvl2 = 1;
            AV6HelpSistema.Load(A1638HelpSistema_Codigo);
            AV8HelpSistema_Descricao = AV6HelpSistema.gxTpr_Helpsistema_descricao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8HelpSistema_Descricao", AV8HelpSistema_Descricao);
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( AV11GXLvl2 == 0 )
         {
            AV6HelpSistema.Load(0);
            AV6HelpSistema.gxTpr_Helpsistema_objeto = AV5HelpSistema_Objeto;
         }
      }

      protected void E12ML2( )
      {
         /* 'Incluir' Routine */
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV8HelpSistema_Descricao)) )
         {
            GX_msglist.addItem("Descri��o deve ser informada.");
            GX_FocusControl = Helpsistema_descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else
         {
            AV6HelpSistema.gxTpr_Helpsistema_descricao = AV8HelpSistema_Descricao;
            if ( AV6HelpSistema.Success() )
            {
               AV6HelpSistema.Save();
               context.CommitDataStores( "WP_HelpSistema");
               context.setWebReturnParms(new Object[] {});
               context.wjLocDisableFrm = 1;
               context.nUserReturn = 1;
               returnInSub = true;
               if (true) return;
            }
            else
            {
               AV13GXV2 = 1;
               AV12GXV1 = AV6HelpSistema.GetMessages();
               while ( AV13GXV2 <= AV12GXV1.Count )
               {
                  AV7Message = ((SdtMessages_Message)AV12GXV1.Item(AV13GXV2));
                  GX_msglist.addItem(StringUtil.Trim( AV7Message.gxTpr_Description));
                  AV13GXV2 = (int)(AV13GXV2+1);
               }
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6HelpSistema", AV6HelpSistema);
      }

      protected void nextLoad( )
      {
      }

      protected void E13ML2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_3_ML2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblMaintable_Internalname, tblMaintable_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divTable1_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"HELPSISTEMA_DESCRICAOContainer"+"\"></div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTable2_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-6", "Right", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttIncluir_Internalname, "", "Incluir", bttIncluir_Jsonclick, 5, "Incluir", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'INCLUIR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_HelpSistema.htm");
            GxWebStd.gx_div_end( context, "Right", "top");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-6", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttCancel_Internalname, "", "Fechar", bttCancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_HelpSistema.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_3_ML2e( true) ;
         }
         else
         {
            wb_table1_3_ML2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV5HelpSistema_Objeto = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5HelpSistema_Objeto", AV5HelpSistema_Objeto);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vHELPSISTEMA_OBJETO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV5HelpSistema_Objeto, ""))));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAML2( ) ;
         WSML2( ) ;
         WEML2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042823522629");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_helpsistema.js", "?202042823522630");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         div_Internalname = "";
         div_Internalname = "";
         Helpsistema_descricao_Internalname = "HELPSISTEMA_DESCRICAO";
         div_Internalname = "";
         div_Internalname = "";
         div_Internalname = "";
         bttIncluir_Internalname = "INCLUIR";
         div_Internalname = "";
         bttCancel_Internalname = "CANCEL";
         div_Internalname = "";
         div_Internalname = "";
         divTable2_Internalname = "TABLE2";
         div_Internalname = "";
         div_Internalname = "";
         divTable1_Internalname = "TABLE1";
         tblMaintable_Internalname = "MAINTABLE";
         div_Internalname = "";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Helpsistema_descricao_Enabled = Convert.ToBoolean( 1);
         Helpsistema_descricao_Captionposition = "None";
         Helpsistema_descricao_Captionclass = "col-sm-3 AttributeLabel";
         Helpsistema_descricao_Customconfiguration = "myconfig.js";
         Helpsistema_descricao_Customtoolbar = "myToolbar";
         Helpsistema_descricao_Toolbar = "Custom";
         Helpsistema_descricao_Skin = "default";
         Helpsistema_descricao_Height = "300";
         Helpsistema_descricao_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Help";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'INCLUIR'","{handler:'E12ML2',iparms:[{av:'AV8HelpSistema_Descricao',fld:'vHELPSISTEMA_DESCRICAO',pic:'',nv:''},{av:'AV6HelpSistema',fld:'vHELPSISTEMA',pic:'',nv:null}],oparms:[{av:'AV6HelpSistema',fld:'vHELPSISTEMA',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV5HelpSistema_Objeto = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV8HelpSistema_Descricao = "";
         AV6HelpSistema = new SdtHelpSistema(context);
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00ML2_A1639HelpSistema_Objeto = new String[] {""} ;
         H00ML2_A1638HelpSistema_Codigo = new int[1] ;
         A1639HelpSistema_Objeto = "";
         AV12GXV1 = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV7Message = new SdtMessages_Message(context);
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttIncluir_Jsonclick = "";
         bttCancel_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_helpsistema__default(),
            new Object[][] {
                new Object[] {
               H00ML2_A1639HelpSistema_Objeto, H00ML2_A1638HelpSistema_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV11GXLvl2 ;
      private short nGXWrapped ;
      private int A1638HelpSistema_Codigo ;
      private int AV13GXV2 ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Helpsistema_descricao_Width ;
      private String Helpsistema_descricao_Height ;
      private String Helpsistema_descricao_Skin ;
      private String Helpsistema_descricao_Toolbar ;
      private String Helpsistema_descricao_Customtoolbar ;
      private String Helpsistema_descricao_Customconfiguration ;
      private String Helpsistema_descricao_Captionclass ;
      private String Helpsistema_descricao_Captionposition ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String Helpsistema_descricao_Internalname ;
      private String sStyleString ;
      private String tblMaintable_Internalname ;
      private String divTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String divTable2_Internalname ;
      private String TempTags ;
      private String bttIncluir_Internalname ;
      private String bttIncluir_Jsonclick ;
      private String bttCancel_Internalname ;
      private String bttCancel_Jsonclick ;
      private String div_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool Helpsistema_descricao_Enabled ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private String AV8HelpSistema_Descricao ;
      private String AV5HelpSistema_Objeto ;
      private String wcpOAV5HelpSistema_Objeto ;
      private String A1639HelpSistema_Objeto ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] H00ML2_A1639HelpSistema_Objeto ;
      private int[] H00ML2_A1638HelpSistema_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV12GXV1 ;
      private GXWebForm Form ;
      private SdtHelpSistema AV6HelpSistema ;
      private SdtMessages_Message AV7Message ;
   }

   public class wp_helpsistema__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00ML2 ;
          prmH00ML2 = new Object[] {
          new Object[] {"@AV5HelpSistema_Objeto",SqlDbType.VarChar,80,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00ML2", "SELECT [HelpSistema_Objeto], [HelpSistema_Codigo] FROM [HelpSistema] WITH (NOLOCK) WHERE [HelpSistema_Objeto] = @AV5HelpSistema_Objeto ORDER BY [HelpSistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00ML2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
       }
    }

 }

}
