/*
               File: ParametrosSistema_BC
        Description: Parametros do Sistema
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:0:48.16
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Mail;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class parametrossistema_bc : GXHttpHandler, IGxSilentTrn
   {
      public parametrossistema_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public parametrossistema_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow1J56( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey1J56( ) ;
         standaloneModal( ) ;
         AddRow1J56( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E111J2 */
            E111J2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z330ParametrosSistema_Codigo = A330ParametrosSistema_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_1J0( )
      {
         BeforeValidate1J56( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1J56( ) ;
            }
            else
            {
               CheckExtendedTable1J56( ) ;
               if ( AnyError == 0 )
               {
               }
               CloseExtendedTableCursors1J56( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E121J2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
      }

      protected void E111J2( )
      {
         /* After Trn Routine */
      }

      protected void E131J2( )
      {
         /* 'DoAssinar' Routine */
         context.PopUp(formatLink("wp_testeassinatura.aspx") + "?" + UrlEncode(StringUtil.RTrim(A1021ParametrosSistema_PathCrtf)), new Object[] {"A1021ParametrosSistema_PathCrtf"});
      }

      protected void E141J2( )
      {
         /* 'DoBntImagemLogin' Routine */
      }

      protected void E151J2( )
      {
         /* 'DoTestarEmail' Routine */
      }

      protected void ZM1J56( short GX_JID )
      {
         if ( ( GX_JID == 5 ) || ( GX_JID == 0 ) )
         {
            Z331ParametrosSistema_NomeSistema = A331ParametrosSistema_NomeSistema;
            Z334ParametrosSistema_AppID = A334ParametrosSistema_AppID;
            Z399ParametrosSistema_LicensiadoCadastrado = A399ParametrosSistema_LicensiadoCadastrado;
            Z417ParametrosSistema_PadronizarStrings = A417ParametrosSistema_PadronizarStrings;
            Z532ParametrosSistema_EmailSdaHost = A532ParametrosSistema_EmailSdaHost;
            Z533ParametrosSistema_EmailSdaUser = A533ParametrosSistema_EmailSdaUser;
            Z534ParametrosSistema_EmailSdaPass = A534ParametrosSistema_EmailSdaPass;
            Z537ParametrosSistema_EmailSdaKey = A537ParametrosSistema_EmailSdaKey;
            Z535ParametrosSistema_EmailSdaAut = A535ParametrosSistema_EmailSdaAut;
            Z536ParametrosSistema_EmailSdaPort = A536ParametrosSistema_EmailSdaPort;
            Z2032ParametrosSistema_EmailSdaSec = A2032ParametrosSistema_EmailSdaSec;
            Z708ParametrosSistema_FatorAjuste = A708ParametrosSistema_FatorAjuste;
            Z1021ParametrosSistema_PathCrtf = A1021ParametrosSistema_PathCrtf;
            Z1163ParametrosSistema_HostMensuracao = A1163ParametrosSistema_HostMensuracao;
            Z1171ParametrosSistema_SQLComputerName = A1171ParametrosSistema_SQLComputerName;
            Z1172ParametrosSistema_SQLServerName = A1172ParametrosSistema_SQLServerName;
            Z1168ParametrosSistema_SQLEdition = A1168ParametrosSistema_SQLEdition;
            Z1169ParametrosSistema_SQLInstance = A1169ParametrosSistema_SQLInstance;
            Z1170ParametrosSistema_DBName = A1170ParametrosSistema_DBName;
            Z1499ParametrosSistema_FlsEvd = A1499ParametrosSistema_FlsEvd;
            Z1679ParametrosSistema_URLApp = A1679ParametrosSistema_URLApp;
            Z1952ParametrosSistema_URLOtherVer = A1952ParametrosSistema_URLOtherVer;
            Z1954ParametrosSistema_Validacao = A1954ParametrosSistema_Validacao;
         }
         if ( GX_JID == -5 )
         {
            Z330ParametrosSistema_Codigo = A330ParametrosSistema_Codigo;
            Z331ParametrosSistema_NomeSistema = A331ParametrosSistema_NomeSistema;
            Z334ParametrosSistema_AppID = A334ParametrosSistema_AppID;
            Z332ParametrosSistema_TextoHome = A332ParametrosSistema_TextoHome;
            Z399ParametrosSistema_LicensiadoCadastrado = A399ParametrosSistema_LicensiadoCadastrado;
            Z417ParametrosSistema_PadronizarStrings = A417ParametrosSistema_PadronizarStrings;
            Z532ParametrosSistema_EmailSdaHost = A532ParametrosSistema_EmailSdaHost;
            Z533ParametrosSistema_EmailSdaUser = A533ParametrosSistema_EmailSdaUser;
            Z534ParametrosSistema_EmailSdaPass = A534ParametrosSistema_EmailSdaPass;
            Z537ParametrosSistema_EmailSdaKey = A537ParametrosSistema_EmailSdaKey;
            Z535ParametrosSistema_EmailSdaAut = A535ParametrosSistema_EmailSdaAut;
            Z536ParametrosSistema_EmailSdaPort = A536ParametrosSistema_EmailSdaPort;
            Z2032ParametrosSistema_EmailSdaSec = A2032ParametrosSistema_EmailSdaSec;
            Z708ParametrosSistema_FatorAjuste = A708ParametrosSistema_FatorAjuste;
            Z1021ParametrosSistema_PathCrtf = A1021ParametrosSistema_PathCrtf;
            Z1163ParametrosSistema_HostMensuracao = A1163ParametrosSistema_HostMensuracao;
            Z1171ParametrosSistema_SQLComputerName = A1171ParametrosSistema_SQLComputerName;
            Z1172ParametrosSistema_SQLServerName = A1172ParametrosSistema_SQLServerName;
            Z1168ParametrosSistema_SQLEdition = A1168ParametrosSistema_SQLEdition;
            Z1169ParametrosSistema_SQLInstance = A1169ParametrosSistema_SQLInstance;
            Z1170ParametrosSistema_DBName = A1170ParametrosSistema_DBName;
            Z1499ParametrosSistema_FlsEvd = A1499ParametrosSistema_FlsEvd;
            Z1679ParametrosSistema_URLApp = A1679ParametrosSistema_URLApp;
            Z1952ParametrosSistema_URLOtherVer = A1952ParametrosSistema_URLOtherVer;
            Z1954ParametrosSistema_Validacao = A1954ParametrosSistema_Validacao;
            Z2117ParametrosSistema_Imagem = A2117ParametrosSistema_Imagem;
            Z40000ParametrosSistema_Imagem_GXI = A40000ParametrosSistema_Imagem_GXI;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Convert.ToDecimal(0)==A708ParametrosSistema_FatorAjuste) && ( Gx_BScreen == 0 ) )
         {
            A708ParametrosSistema_FatorAjuste = (decimal)(1);
            n708ParametrosSistema_FatorAjuste = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A535ParametrosSistema_EmailSdaAut) && ( Gx_BScreen == 0 ) )
         {
            A535ParametrosSistema_EmailSdaAut = false;
            n535ParametrosSistema_EmailSdaAut = false;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A417ParametrosSistema_PadronizarStrings) && ( Gx_BScreen == 0 ) )
         {
            A417ParametrosSistema_PadronizarStrings = true;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A399ParametrosSistema_LicensiadoCadastrado) && ( Gx_BScreen == 0 ) )
         {
            A399ParametrosSistema_LicensiadoCadastrado = false;
            n399ParametrosSistema_LicensiadoCadastrado = false;
         }
      }

      protected void Load1J56( )
      {
         /* Using cursor BC001J4 */
         pr_default.execute(2, new Object[] {A330ParametrosSistema_Codigo});
         if ( (pr_default.getStatus(2) != 101) )
         {
            RcdFound56 = 1;
            A331ParametrosSistema_NomeSistema = BC001J4_A331ParametrosSistema_NomeSistema[0];
            A334ParametrosSistema_AppID = BC001J4_A334ParametrosSistema_AppID[0];
            A332ParametrosSistema_TextoHome = BC001J4_A332ParametrosSistema_TextoHome[0];
            A399ParametrosSistema_LicensiadoCadastrado = BC001J4_A399ParametrosSistema_LicensiadoCadastrado[0];
            n399ParametrosSistema_LicensiadoCadastrado = BC001J4_n399ParametrosSistema_LicensiadoCadastrado[0];
            A417ParametrosSistema_PadronizarStrings = BC001J4_A417ParametrosSistema_PadronizarStrings[0];
            A532ParametrosSistema_EmailSdaHost = BC001J4_A532ParametrosSistema_EmailSdaHost[0];
            n532ParametrosSistema_EmailSdaHost = BC001J4_n532ParametrosSistema_EmailSdaHost[0];
            A533ParametrosSistema_EmailSdaUser = BC001J4_A533ParametrosSistema_EmailSdaUser[0];
            n533ParametrosSistema_EmailSdaUser = BC001J4_n533ParametrosSistema_EmailSdaUser[0];
            A534ParametrosSistema_EmailSdaPass = BC001J4_A534ParametrosSistema_EmailSdaPass[0];
            n534ParametrosSistema_EmailSdaPass = BC001J4_n534ParametrosSistema_EmailSdaPass[0];
            A537ParametrosSistema_EmailSdaKey = BC001J4_A537ParametrosSistema_EmailSdaKey[0];
            n537ParametrosSistema_EmailSdaKey = BC001J4_n537ParametrosSistema_EmailSdaKey[0];
            A535ParametrosSistema_EmailSdaAut = BC001J4_A535ParametrosSistema_EmailSdaAut[0];
            n535ParametrosSistema_EmailSdaAut = BC001J4_n535ParametrosSistema_EmailSdaAut[0];
            A536ParametrosSistema_EmailSdaPort = BC001J4_A536ParametrosSistema_EmailSdaPort[0];
            n536ParametrosSistema_EmailSdaPort = BC001J4_n536ParametrosSistema_EmailSdaPort[0];
            A2032ParametrosSistema_EmailSdaSec = BC001J4_A2032ParametrosSistema_EmailSdaSec[0];
            n2032ParametrosSistema_EmailSdaSec = BC001J4_n2032ParametrosSistema_EmailSdaSec[0];
            A708ParametrosSistema_FatorAjuste = BC001J4_A708ParametrosSistema_FatorAjuste[0];
            n708ParametrosSistema_FatorAjuste = BC001J4_n708ParametrosSistema_FatorAjuste[0];
            A1021ParametrosSistema_PathCrtf = BC001J4_A1021ParametrosSistema_PathCrtf[0];
            n1021ParametrosSistema_PathCrtf = BC001J4_n1021ParametrosSistema_PathCrtf[0];
            A1163ParametrosSistema_HostMensuracao = BC001J4_A1163ParametrosSistema_HostMensuracao[0];
            n1163ParametrosSistema_HostMensuracao = BC001J4_n1163ParametrosSistema_HostMensuracao[0];
            A1171ParametrosSistema_SQLComputerName = BC001J4_A1171ParametrosSistema_SQLComputerName[0];
            n1171ParametrosSistema_SQLComputerName = BC001J4_n1171ParametrosSistema_SQLComputerName[0];
            A1172ParametrosSistema_SQLServerName = BC001J4_A1172ParametrosSistema_SQLServerName[0];
            n1172ParametrosSistema_SQLServerName = BC001J4_n1172ParametrosSistema_SQLServerName[0];
            A1168ParametrosSistema_SQLEdition = BC001J4_A1168ParametrosSistema_SQLEdition[0];
            n1168ParametrosSistema_SQLEdition = BC001J4_n1168ParametrosSistema_SQLEdition[0];
            A1169ParametrosSistema_SQLInstance = BC001J4_A1169ParametrosSistema_SQLInstance[0];
            n1169ParametrosSistema_SQLInstance = BC001J4_n1169ParametrosSistema_SQLInstance[0];
            A1170ParametrosSistema_DBName = BC001J4_A1170ParametrosSistema_DBName[0];
            n1170ParametrosSistema_DBName = BC001J4_n1170ParametrosSistema_DBName[0];
            A1499ParametrosSistema_FlsEvd = BC001J4_A1499ParametrosSistema_FlsEvd[0];
            n1499ParametrosSistema_FlsEvd = BC001J4_n1499ParametrosSistema_FlsEvd[0];
            A1679ParametrosSistema_URLApp = BC001J4_A1679ParametrosSistema_URLApp[0];
            n1679ParametrosSistema_URLApp = BC001J4_n1679ParametrosSistema_URLApp[0];
            A1952ParametrosSistema_URLOtherVer = BC001J4_A1952ParametrosSistema_URLOtherVer[0];
            n1952ParametrosSistema_URLOtherVer = BC001J4_n1952ParametrosSistema_URLOtherVer[0];
            A1954ParametrosSistema_Validacao = BC001J4_A1954ParametrosSistema_Validacao[0];
            n1954ParametrosSistema_Validacao = BC001J4_n1954ParametrosSistema_Validacao[0];
            A40000ParametrosSistema_Imagem_GXI = BC001J4_A40000ParametrosSistema_Imagem_GXI[0];
            n40000ParametrosSistema_Imagem_GXI = BC001J4_n40000ParametrosSistema_Imagem_GXI[0];
            A2117ParametrosSistema_Imagem = BC001J4_A2117ParametrosSistema_Imagem[0];
            n2117ParametrosSistema_Imagem = BC001J4_n2117ParametrosSistema_Imagem[0];
            ZM1J56( -5) ;
         }
         pr_default.close(2);
         OnLoadActions1J56( ) ;
      }

      protected void OnLoadActions1J56( )
      {
      }

      protected void CheckExtendedTable1J56( )
      {
         standaloneModal( ) ;
      }

      protected void CloseExtendedTableCursors1J56( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey1J56( )
      {
         /* Using cursor BC001J5 */
         pr_default.execute(3, new Object[] {A330ParametrosSistema_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound56 = 1;
         }
         else
         {
            RcdFound56 = 0;
         }
         pr_default.close(3);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC001J3 */
         pr_default.execute(1, new Object[] {A330ParametrosSistema_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1J56( 5) ;
            RcdFound56 = 1;
            A330ParametrosSistema_Codigo = BC001J3_A330ParametrosSistema_Codigo[0];
            A331ParametrosSistema_NomeSistema = BC001J3_A331ParametrosSistema_NomeSistema[0];
            A334ParametrosSistema_AppID = BC001J3_A334ParametrosSistema_AppID[0];
            A332ParametrosSistema_TextoHome = BC001J3_A332ParametrosSistema_TextoHome[0];
            A399ParametrosSistema_LicensiadoCadastrado = BC001J3_A399ParametrosSistema_LicensiadoCadastrado[0];
            n399ParametrosSistema_LicensiadoCadastrado = BC001J3_n399ParametrosSistema_LicensiadoCadastrado[0];
            A417ParametrosSistema_PadronizarStrings = BC001J3_A417ParametrosSistema_PadronizarStrings[0];
            A532ParametrosSistema_EmailSdaHost = BC001J3_A532ParametrosSistema_EmailSdaHost[0];
            n532ParametrosSistema_EmailSdaHost = BC001J3_n532ParametrosSistema_EmailSdaHost[0];
            A533ParametrosSistema_EmailSdaUser = BC001J3_A533ParametrosSistema_EmailSdaUser[0];
            n533ParametrosSistema_EmailSdaUser = BC001J3_n533ParametrosSistema_EmailSdaUser[0];
            A534ParametrosSistema_EmailSdaPass = BC001J3_A534ParametrosSistema_EmailSdaPass[0];
            n534ParametrosSistema_EmailSdaPass = BC001J3_n534ParametrosSistema_EmailSdaPass[0];
            A537ParametrosSistema_EmailSdaKey = BC001J3_A537ParametrosSistema_EmailSdaKey[0];
            n537ParametrosSistema_EmailSdaKey = BC001J3_n537ParametrosSistema_EmailSdaKey[0];
            A535ParametrosSistema_EmailSdaAut = BC001J3_A535ParametrosSistema_EmailSdaAut[0];
            n535ParametrosSistema_EmailSdaAut = BC001J3_n535ParametrosSistema_EmailSdaAut[0];
            A536ParametrosSistema_EmailSdaPort = BC001J3_A536ParametrosSistema_EmailSdaPort[0];
            n536ParametrosSistema_EmailSdaPort = BC001J3_n536ParametrosSistema_EmailSdaPort[0];
            A2032ParametrosSistema_EmailSdaSec = BC001J3_A2032ParametrosSistema_EmailSdaSec[0];
            n2032ParametrosSistema_EmailSdaSec = BC001J3_n2032ParametrosSistema_EmailSdaSec[0];
            A708ParametrosSistema_FatorAjuste = BC001J3_A708ParametrosSistema_FatorAjuste[0];
            n708ParametrosSistema_FatorAjuste = BC001J3_n708ParametrosSistema_FatorAjuste[0];
            A1021ParametrosSistema_PathCrtf = BC001J3_A1021ParametrosSistema_PathCrtf[0];
            n1021ParametrosSistema_PathCrtf = BC001J3_n1021ParametrosSistema_PathCrtf[0];
            A1163ParametrosSistema_HostMensuracao = BC001J3_A1163ParametrosSistema_HostMensuracao[0];
            n1163ParametrosSistema_HostMensuracao = BC001J3_n1163ParametrosSistema_HostMensuracao[0];
            A1171ParametrosSistema_SQLComputerName = BC001J3_A1171ParametrosSistema_SQLComputerName[0];
            n1171ParametrosSistema_SQLComputerName = BC001J3_n1171ParametrosSistema_SQLComputerName[0];
            A1172ParametrosSistema_SQLServerName = BC001J3_A1172ParametrosSistema_SQLServerName[0];
            n1172ParametrosSistema_SQLServerName = BC001J3_n1172ParametrosSistema_SQLServerName[0];
            A1168ParametrosSistema_SQLEdition = BC001J3_A1168ParametrosSistema_SQLEdition[0];
            n1168ParametrosSistema_SQLEdition = BC001J3_n1168ParametrosSistema_SQLEdition[0];
            A1169ParametrosSistema_SQLInstance = BC001J3_A1169ParametrosSistema_SQLInstance[0];
            n1169ParametrosSistema_SQLInstance = BC001J3_n1169ParametrosSistema_SQLInstance[0];
            A1170ParametrosSistema_DBName = BC001J3_A1170ParametrosSistema_DBName[0];
            n1170ParametrosSistema_DBName = BC001J3_n1170ParametrosSistema_DBName[0];
            A1499ParametrosSistema_FlsEvd = BC001J3_A1499ParametrosSistema_FlsEvd[0];
            n1499ParametrosSistema_FlsEvd = BC001J3_n1499ParametrosSistema_FlsEvd[0];
            A1679ParametrosSistema_URLApp = BC001J3_A1679ParametrosSistema_URLApp[0];
            n1679ParametrosSistema_URLApp = BC001J3_n1679ParametrosSistema_URLApp[0];
            A1952ParametrosSistema_URLOtherVer = BC001J3_A1952ParametrosSistema_URLOtherVer[0];
            n1952ParametrosSistema_URLOtherVer = BC001J3_n1952ParametrosSistema_URLOtherVer[0];
            A1954ParametrosSistema_Validacao = BC001J3_A1954ParametrosSistema_Validacao[0];
            n1954ParametrosSistema_Validacao = BC001J3_n1954ParametrosSistema_Validacao[0];
            A40000ParametrosSistema_Imagem_GXI = BC001J3_A40000ParametrosSistema_Imagem_GXI[0];
            n40000ParametrosSistema_Imagem_GXI = BC001J3_n40000ParametrosSistema_Imagem_GXI[0];
            A2117ParametrosSistema_Imagem = BC001J3_A2117ParametrosSistema_Imagem[0];
            n2117ParametrosSistema_Imagem = BC001J3_n2117ParametrosSistema_Imagem[0];
            Z330ParametrosSistema_Codigo = A330ParametrosSistema_Codigo;
            sMode56 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load1J56( ) ;
            if ( AnyError == 1 )
            {
               RcdFound56 = 0;
               InitializeNonKey1J56( ) ;
            }
            Gx_mode = sMode56;
         }
         else
         {
            RcdFound56 = 0;
            InitializeNonKey1J56( ) ;
            sMode56 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode56;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1J56( ) ;
         if ( RcdFound56 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_1J0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency1J56( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC001J2 */
            pr_default.execute(0, new Object[] {A330ParametrosSistema_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ParametrosSistema"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z331ParametrosSistema_NomeSistema, BC001J2_A331ParametrosSistema_NomeSistema[0]) != 0 ) || ( Z334ParametrosSistema_AppID != BC001J2_A334ParametrosSistema_AppID[0] ) || ( Z399ParametrosSistema_LicensiadoCadastrado != BC001J2_A399ParametrosSistema_LicensiadoCadastrado[0] ) || ( Z417ParametrosSistema_PadronizarStrings != BC001J2_A417ParametrosSistema_PadronizarStrings[0] ) || ( StringUtil.StrCmp(Z532ParametrosSistema_EmailSdaHost, BC001J2_A532ParametrosSistema_EmailSdaHost[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z533ParametrosSistema_EmailSdaUser, BC001J2_A533ParametrosSistema_EmailSdaUser[0]) != 0 ) || ( StringUtil.StrCmp(Z534ParametrosSistema_EmailSdaPass, BC001J2_A534ParametrosSistema_EmailSdaPass[0]) != 0 ) || ( StringUtil.StrCmp(Z537ParametrosSistema_EmailSdaKey, BC001J2_A537ParametrosSistema_EmailSdaKey[0]) != 0 ) || ( Z535ParametrosSistema_EmailSdaAut != BC001J2_A535ParametrosSistema_EmailSdaAut[0] ) || ( Z536ParametrosSistema_EmailSdaPort != BC001J2_A536ParametrosSistema_EmailSdaPort[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z2032ParametrosSistema_EmailSdaSec != BC001J2_A2032ParametrosSistema_EmailSdaSec[0] ) || ( Z708ParametrosSistema_FatorAjuste != BC001J2_A708ParametrosSistema_FatorAjuste[0] ) || ( StringUtil.StrCmp(Z1021ParametrosSistema_PathCrtf, BC001J2_A1021ParametrosSistema_PathCrtf[0]) != 0 ) || ( StringUtil.StrCmp(Z1163ParametrosSistema_HostMensuracao, BC001J2_A1163ParametrosSistema_HostMensuracao[0]) != 0 ) || ( StringUtil.StrCmp(Z1171ParametrosSistema_SQLComputerName, BC001J2_A1171ParametrosSistema_SQLComputerName[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z1172ParametrosSistema_SQLServerName, BC001J2_A1172ParametrosSistema_SQLServerName[0]) != 0 ) || ( StringUtil.StrCmp(Z1168ParametrosSistema_SQLEdition, BC001J2_A1168ParametrosSistema_SQLEdition[0]) != 0 ) || ( StringUtil.StrCmp(Z1169ParametrosSistema_SQLInstance, BC001J2_A1169ParametrosSistema_SQLInstance[0]) != 0 ) || ( StringUtil.StrCmp(Z1170ParametrosSistema_DBName, BC001J2_A1170ParametrosSistema_DBName[0]) != 0 ) || ( StringUtil.StrCmp(Z1499ParametrosSistema_FlsEvd, BC001J2_A1499ParametrosSistema_FlsEvd[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z1679ParametrosSistema_URLApp, BC001J2_A1679ParametrosSistema_URLApp[0]) != 0 ) || ( StringUtil.StrCmp(Z1952ParametrosSistema_URLOtherVer, BC001J2_A1952ParametrosSistema_URLOtherVer[0]) != 0 ) || ( StringUtil.StrCmp(Z1954ParametrosSistema_Validacao, BC001J2_A1954ParametrosSistema_Validacao[0]) != 0 ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ParametrosSistema"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1J56( )
      {
         BeforeValidate1J56( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1J56( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1J56( 0) ;
            CheckOptimisticConcurrency1J56( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1J56( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1J56( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC001J6 */
                     pr_default.execute(4, new Object[] {A330ParametrosSistema_Codigo, A331ParametrosSistema_NomeSistema, A334ParametrosSistema_AppID, A332ParametrosSistema_TextoHome, n399ParametrosSistema_LicensiadoCadastrado, A399ParametrosSistema_LicensiadoCadastrado, A417ParametrosSistema_PadronizarStrings, n532ParametrosSistema_EmailSdaHost, A532ParametrosSistema_EmailSdaHost, n533ParametrosSistema_EmailSdaUser, A533ParametrosSistema_EmailSdaUser, n534ParametrosSistema_EmailSdaPass, A534ParametrosSistema_EmailSdaPass, n537ParametrosSistema_EmailSdaKey, A537ParametrosSistema_EmailSdaKey, n535ParametrosSistema_EmailSdaAut, A535ParametrosSistema_EmailSdaAut, n536ParametrosSistema_EmailSdaPort, A536ParametrosSistema_EmailSdaPort, n2032ParametrosSistema_EmailSdaSec, A2032ParametrosSistema_EmailSdaSec, n708ParametrosSistema_FatorAjuste, A708ParametrosSistema_FatorAjuste, n1021ParametrosSistema_PathCrtf, A1021ParametrosSistema_PathCrtf, n1163ParametrosSistema_HostMensuracao, A1163ParametrosSistema_HostMensuracao, n1171ParametrosSistema_SQLComputerName, A1171ParametrosSistema_SQLComputerName, n1172ParametrosSistema_SQLServerName, A1172ParametrosSistema_SQLServerName, n1168ParametrosSistema_SQLEdition, A1168ParametrosSistema_SQLEdition, n1169ParametrosSistema_SQLInstance, A1169ParametrosSistema_SQLInstance, n1170ParametrosSistema_DBName, A1170ParametrosSistema_DBName, n1499ParametrosSistema_FlsEvd, A1499ParametrosSistema_FlsEvd, n1679ParametrosSistema_URLApp, A1679ParametrosSistema_URLApp, n1952ParametrosSistema_URLOtherVer, A1952ParametrosSistema_URLOtherVer, n1954ParametrosSistema_Validacao, A1954ParametrosSistema_Validacao, n2117ParametrosSistema_Imagem, A2117ParametrosSistema_Imagem, n40000ParametrosSistema_Imagem_GXI, A40000ParametrosSistema_Imagem_GXI});
                     pr_default.close(4);
                     dsDefault.SmartCacheProvider.SetUpdated("ParametrosSistema") ;
                     if ( (pr_default.getStatus(4) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1J56( ) ;
            }
            EndLevel1J56( ) ;
         }
         CloseExtendedTableCursors1J56( ) ;
      }

      protected void Update1J56( )
      {
         BeforeValidate1J56( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1J56( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1J56( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1J56( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1J56( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC001J7 */
                     pr_default.execute(5, new Object[] {A331ParametrosSistema_NomeSistema, A334ParametrosSistema_AppID, A332ParametrosSistema_TextoHome, n399ParametrosSistema_LicensiadoCadastrado, A399ParametrosSistema_LicensiadoCadastrado, A417ParametrosSistema_PadronizarStrings, n532ParametrosSistema_EmailSdaHost, A532ParametrosSistema_EmailSdaHost, n533ParametrosSistema_EmailSdaUser, A533ParametrosSistema_EmailSdaUser, n534ParametrosSistema_EmailSdaPass, A534ParametrosSistema_EmailSdaPass, n537ParametrosSistema_EmailSdaKey, A537ParametrosSistema_EmailSdaKey, n535ParametrosSistema_EmailSdaAut, A535ParametrosSistema_EmailSdaAut, n536ParametrosSistema_EmailSdaPort, A536ParametrosSistema_EmailSdaPort, n2032ParametrosSistema_EmailSdaSec, A2032ParametrosSistema_EmailSdaSec, n708ParametrosSistema_FatorAjuste, A708ParametrosSistema_FatorAjuste, n1021ParametrosSistema_PathCrtf, A1021ParametrosSistema_PathCrtf, n1163ParametrosSistema_HostMensuracao, A1163ParametrosSistema_HostMensuracao, n1171ParametrosSistema_SQLComputerName, A1171ParametrosSistema_SQLComputerName, n1172ParametrosSistema_SQLServerName, A1172ParametrosSistema_SQLServerName, n1168ParametrosSistema_SQLEdition, A1168ParametrosSistema_SQLEdition, n1169ParametrosSistema_SQLInstance, A1169ParametrosSistema_SQLInstance, n1170ParametrosSistema_DBName, A1170ParametrosSistema_DBName, n1499ParametrosSistema_FlsEvd, A1499ParametrosSistema_FlsEvd, n1679ParametrosSistema_URLApp, A1679ParametrosSistema_URLApp, n1952ParametrosSistema_URLOtherVer, A1952ParametrosSistema_URLOtherVer, n1954ParametrosSistema_Validacao, A1954ParametrosSistema_Validacao, A330ParametrosSistema_Codigo});
                     pr_default.close(5);
                     dsDefault.SmartCacheProvider.SetUpdated("ParametrosSistema") ;
                     if ( (pr_default.getStatus(5) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ParametrosSistema"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1J56( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1J56( ) ;
         }
         CloseExtendedTableCursors1J56( ) ;
      }

      protected void DeferredUpdate1J56( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor BC001J8 */
            pr_default.execute(6, new Object[] {n2117ParametrosSistema_Imagem, A2117ParametrosSistema_Imagem, n40000ParametrosSistema_Imagem_GXI, A40000ParametrosSistema_Imagem_GXI, A330ParametrosSistema_Codigo});
            pr_default.close(6);
            dsDefault.SmartCacheProvider.SetUpdated("ParametrosSistema") ;
         }
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate1J56( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1J56( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1J56( ) ;
            AfterConfirm1J56( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1J56( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC001J9 */
                  pr_default.execute(7, new Object[] {A330ParametrosSistema_Codigo});
                  pr_default.close(7);
                  dsDefault.SmartCacheProvider.SetUpdated("ParametrosSistema") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode56 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel1J56( ) ;
         Gx_mode = sMode56;
      }

      protected void OnDeleteControls1J56( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel1J56( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1J56( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart1J56( )
      {
         /* Scan By routine */
         /* Using cursor BC001J10 */
         pr_default.execute(8, new Object[] {A330ParametrosSistema_Codigo});
         RcdFound56 = 0;
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound56 = 1;
            A330ParametrosSistema_Codigo = BC001J10_A330ParametrosSistema_Codigo[0];
            A331ParametrosSistema_NomeSistema = BC001J10_A331ParametrosSistema_NomeSistema[0];
            A334ParametrosSistema_AppID = BC001J10_A334ParametrosSistema_AppID[0];
            A332ParametrosSistema_TextoHome = BC001J10_A332ParametrosSistema_TextoHome[0];
            A399ParametrosSistema_LicensiadoCadastrado = BC001J10_A399ParametrosSistema_LicensiadoCadastrado[0];
            n399ParametrosSistema_LicensiadoCadastrado = BC001J10_n399ParametrosSistema_LicensiadoCadastrado[0];
            A417ParametrosSistema_PadronizarStrings = BC001J10_A417ParametrosSistema_PadronizarStrings[0];
            A532ParametrosSistema_EmailSdaHost = BC001J10_A532ParametrosSistema_EmailSdaHost[0];
            n532ParametrosSistema_EmailSdaHost = BC001J10_n532ParametrosSistema_EmailSdaHost[0];
            A533ParametrosSistema_EmailSdaUser = BC001J10_A533ParametrosSistema_EmailSdaUser[0];
            n533ParametrosSistema_EmailSdaUser = BC001J10_n533ParametrosSistema_EmailSdaUser[0];
            A534ParametrosSistema_EmailSdaPass = BC001J10_A534ParametrosSistema_EmailSdaPass[0];
            n534ParametrosSistema_EmailSdaPass = BC001J10_n534ParametrosSistema_EmailSdaPass[0];
            A537ParametrosSistema_EmailSdaKey = BC001J10_A537ParametrosSistema_EmailSdaKey[0];
            n537ParametrosSistema_EmailSdaKey = BC001J10_n537ParametrosSistema_EmailSdaKey[0];
            A535ParametrosSistema_EmailSdaAut = BC001J10_A535ParametrosSistema_EmailSdaAut[0];
            n535ParametrosSistema_EmailSdaAut = BC001J10_n535ParametrosSistema_EmailSdaAut[0];
            A536ParametrosSistema_EmailSdaPort = BC001J10_A536ParametrosSistema_EmailSdaPort[0];
            n536ParametrosSistema_EmailSdaPort = BC001J10_n536ParametrosSistema_EmailSdaPort[0];
            A2032ParametrosSistema_EmailSdaSec = BC001J10_A2032ParametrosSistema_EmailSdaSec[0];
            n2032ParametrosSistema_EmailSdaSec = BC001J10_n2032ParametrosSistema_EmailSdaSec[0];
            A708ParametrosSistema_FatorAjuste = BC001J10_A708ParametrosSistema_FatorAjuste[0];
            n708ParametrosSistema_FatorAjuste = BC001J10_n708ParametrosSistema_FatorAjuste[0];
            A1021ParametrosSistema_PathCrtf = BC001J10_A1021ParametrosSistema_PathCrtf[0];
            n1021ParametrosSistema_PathCrtf = BC001J10_n1021ParametrosSistema_PathCrtf[0];
            A1163ParametrosSistema_HostMensuracao = BC001J10_A1163ParametrosSistema_HostMensuracao[0];
            n1163ParametrosSistema_HostMensuracao = BC001J10_n1163ParametrosSistema_HostMensuracao[0];
            A1171ParametrosSistema_SQLComputerName = BC001J10_A1171ParametrosSistema_SQLComputerName[0];
            n1171ParametrosSistema_SQLComputerName = BC001J10_n1171ParametrosSistema_SQLComputerName[0];
            A1172ParametrosSistema_SQLServerName = BC001J10_A1172ParametrosSistema_SQLServerName[0];
            n1172ParametrosSistema_SQLServerName = BC001J10_n1172ParametrosSistema_SQLServerName[0];
            A1168ParametrosSistema_SQLEdition = BC001J10_A1168ParametrosSistema_SQLEdition[0];
            n1168ParametrosSistema_SQLEdition = BC001J10_n1168ParametrosSistema_SQLEdition[0];
            A1169ParametrosSistema_SQLInstance = BC001J10_A1169ParametrosSistema_SQLInstance[0];
            n1169ParametrosSistema_SQLInstance = BC001J10_n1169ParametrosSistema_SQLInstance[0];
            A1170ParametrosSistema_DBName = BC001J10_A1170ParametrosSistema_DBName[0];
            n1170ParametrosSistema_DBName = BC001J10_n1170ParametrosSistema_DBName[0];
            A1499ParametrosSistema_FlsEvd = BC001J10_A1499ParametrosSistema_FlsEvd[0];
            n1499ParametrosSistema_FlsEvd = BC001J10_n1499ParametrosSistema_FlsEvd[0];
            A1679ParametrosSistema_URLApp = BC001J10_A1679ParametrosSistema_URLApp[0];
            n1679ParametrosSistema_URLApp = BC001J10_n1679ParametrosSistema_URLApp[0];
            A1952ParametrosSistema_URLOtherVer = BC001J10_A1952ParametrosSistema_URLOtherVer[0];
            n1952ParametrosSistema_URLOtherVer = BC001J10_n1952ParametrosSistema_URLOtherVer[0];
            A1954ParametrosSistema_Validacao = BC001J10_A1954ParametrosSistema_Validacao[0];
            n1954ParametrosSistema_Validacao = BC001J10_n1954ParametrosSistema_Validacao[0];
            A40000ParametrosSistema_Imagem_GXI = BC001J10_A40000ParametrosSistema_Imagem_GXI[0];
            n40000ParametrosSistema_Imagem_GXI = BC001J10_n40000ParametrosSistema_Imagem_GXI[0];
            A2117ParametrosSistema_Imagem = BC001J10_A2117ParametrosSistema_Imagem[0];
            n2117ParametrosSistema_Imagem = BC001J10_n2117ParametrosSistema_Imagem[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext1J56( )
      {
         /* Scan next routine */
         pr_default.readNext(8);
         RcdFound56 = 0;
         ScanKeyLoad1J56( ) ;
      }

      protected void ScanKeyLoad1J56( )
      {
         sMode56 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound56 = 1;
            A330ParametrosSistema_Codigo = BC001J10_A330ParametrosSistema_Codigo[0];
            A331ParametrosSistema_NomeSistema = BC001J10_A331ParametrosSistema_NomeSistema[0];
            A334ParametrosSistema_AppID = BC001J10_A334ParametrosSistema_AppID[0];
            A332ParametrosSistema_TextoHome = BC001J10_A332ParametrosSistema_TextoHome[0];
            A399ParametrosSistema_LicensiadoCadastrado = BC001J10_A399ParametrosSistema_LicensiadoCadastrado[0];
            n399ParametrosSistema_LicensiadoCadastrado = BC001J10_n399ParametrosSistema_LicensiadoCadastrado[0];
            A417ParametrosSistema_PadronizarStrings = BC001J10_A417ParametrosSistema_PadronizarStrings[0];
            A532ParametrosSistema_EmailSdaHost = BC001J10_A532ParametrosSistema_EmailSdaHost[0];
            n532ParametrosSistema_EmailSdaHost = BC001J10_n532ParametrosSistema_EmailSdaHost[0];
            A533ParametrosSistema_EmailSdaUser = BC001J10_A533ParametrosSistema_EmailSdaUser[0];
            n533ParametrosSistema_EmailSdaUser = BC001J10_n533ParametrosSistema_EmailSdaUser[0];
            A534ParametrosSistema_EmailSdaPass = BC001J10_A534ParametrosSistema_EmailSdaPass[0];
            n534ParametrosSistema_EmailSdaPass = BC001J10_n534ParametrosSistema_EmailSdaPass[0];
            A537ParametrosSistema_EmailSdaKey = BC001J10_A537ParametrosSistema_EmailSdaKey[0];
            n537ParametrosSistema_EmailSdaKey = BC001J10_n537ParametrosSistema_EmailSdaKey[0];
            A535ParametrosSistema_EmailSdaAut = BC001J10_A535ParametrosSistema_EmailSdaAut[0];
            n535ParametrosSistema_EmailSdaAut = BC001J10_n535ParametrosSistema_EmailSdaAut[0];
            A536ParametrosSistema_EmailSdaPort = BC001J10_A536ParametrosSistema_EmailSdaPort[0];
            n536ParametrosSistema_EmailSdaPort = BC001J10_n536ParametrosSistema_EmailSdaPort[0];
            A2032ParametrosSistema_EmailSdaSec = BC001J10_A2032ParametrosSistema_EmailSdaSec[0];
            n2032ParametrosSistema_EmailSdaSec = BC001J10_n2032ParametrosSistema_EmailSdaSec[0];
            A708ParametrosSistema_FatorAjuste = BC001J10_A708ParametrosSistema_FatorAjuste[0];
            n708ParametrosSistema_FatorAjuste = BC001J10_n708ParametrosSistema_FatorAjuste[0];
            A1021ParametrosSistema_PathCrtf = BC001J10_A1021ParametrosSistema_PathCrtf[0];
            n1021ParametrosSistema_PathCrtf = BC001J10_n1021ParametrosSistema_PathCrtf[0];
            A1163ParametrosSistema_HostMensuracao = BC001J10_A1163ParametrosSistema_HostMensuracao[0];
            n1163ParametrosSistema_HostMensuracao = BC001J10_n1163ParametrosSistema_HostMensuracao[0];
            A1171ParametrosSistema_SQLComputerName = BC001J10_A1171ParametrosSistema_SQLComputerName[0];
            n1171ParametrosSistema_SQLComputerName = BC001J10_n1171ParametrosSistema_SQLComputerName[0];
            A1172ParametrosSistema_SQLServerName = BC001J10_A1172ParametrosSistema_SQLServerName[0];
            n1172ParametrosSistema_SQLServerName = BC001J10_n1172ParametrosSistema_SQLServerName[0];
            A1168ParametrosSistema_SQLEdition = BC001J10_A1168ParametrosSistema_SQLEdition[0];
            n1168ParametrosSistema_SQLEdition = BC001J10_n1168ParametrosSistema_SQLEdition[0];
            A1169ParametrosSistema_SQLInstance = BC001J10_A1169ParametrosSistema_SQLInstance[0];
            n1169ParametrosSistema_SQLInstance = BC001J10_n1169ParametrosSistema_SQLInstance[0];
            A1170ParametrosSistema_DBName = BC001J10_A1170ParametrosSistema_DBName[0];
            n1170ParametrosSistema_DBName = BC001J10_n1170ParametrosSistema_DBName[0];
            A1499ParametrosSistema_FlsEvd = BC001J10_A1499ParametrosSistema_FlsEvd[0];
            n1499ParametrosSistema_FlsEvd = BC001J10_n1499ParametrosSistema_FlsEvd[0];
            A1679ParametrosSistema_URLApp = BC001J10_A1679ParametrosSistema_URLApp[0];
            n1679ParametrosSistema_URLApp = BC001J10_n1679ParametrosSistema_URLApp[0];
            A1952ParametrosSistema_URLOtherVer = BC001J10_A1952ParametrosSistema_URLOtherVer[0];
            n1952ParametrosSistema_URLOtherVer = BC001J10_n1952ParametrosSistema_URLOtherVer[0];
            A1954ParametrosSistema_Validacao = BC001J10_A1954ParametrosSistema_Validacao[0];
            n1954ParametrosSistema_Validacao = BC001J10_n1954ParametrosSistema_Validacao[0];
            A40000ParametrosSistema_Imagem_GXI = BC001J10_A40000ParametrosSistema_Imagem_GXI[0];
            n40000ParametrosSistema_Imagem_GXI = BC001J10_n40000ParametrosSistema_Imagem_GXI[0];
            A2117ParametrosSistema_Imagem = BC001J10_A2117ParametrosSistema_Imagem[0];
            n2117ParametrosSistema_Imagem = BC001J10_n2117ParametrosSistema_Imagem[0];
         }
         Gx_mode = sMode56;
      }

      protected void ScanKeyEnd1J56( )
      {
         pr_default.close(8);
      }

      protected void AfterConfirm1J56( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1J56( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1J56( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1J56( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1J56( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1J56( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1J56( )
      {
      }

      protected void AddRow1J56( )
      {
         VarsToRow56( bcParametrosSistema) ;
      }

      protected void ReadRow1J56( )
      {
         RowToVars56( bcParametrosSistema, 1) ;
      }

      protected void InitializeNonKey1J56( )
      {
         A331ParametrosSistema_NomeSistema = "";
         A334ParametrosSistema_AppID = 0;
         A332ParametrosSistema_TextoHome = "";
         A532ParametrosSistema_EmailSdaHost = "";
         n532ParametrosSistema_EmailSdaHost = false;
         A533ParametrosSistema_EmailSdaUser = "";
         n533ParametrosSistema_EmailSdaUser = false;
         A534ParametrosSistema_EmailSdaPass = "";
         n534ParametrosSistema_EmailSdaPass = false;
         A537ParametrosSistema_EmailSdaKey = "";
         n537ParametrosSistema_EmailSdaKey = false;
         A536ParametrosSistema_EmailSdaPort = 0;
         n536ParametrosSistema_EmailSdaPort = false;
         A2032ParametrosSistema_EmailSdaSec = 0;
         n2032ParametrosSistema_EmailSdaSec = false;
         A1021ParametrosSistema_PathCrtf = "";
         n1021ParametrosSistema_PathCrtf = false;
         A1163ParametrosSistema_HostMensuracao = "";
         n1163ParametrosSistema_HostMensuracao = false;
         A1171ParametrosSistema_SQLComputerName = "";
         n1171ParametrosSistema_SQLComputerName = false;
         A1172ParametrosSistema_SQLServerName = "";
         n1172ParametrosSistema_SQLServerName = false;
         A1168ParametrosSistema_SQLEdition = "";
         n1168ParametrosSistema_SQLEdition = false;
         A1169ParametrosSistema_SQLInstance = "";
         n1169ParametrosSistema_SQLInstance = false;
         A1170ParametrosSistema_DBName = "";
         n1170ParametrosSistema_DBName = false;
         A1499ParametrosSistema_FlsEvd = "";
         n1499ParametrosSistema_FlsEvd = false;
         A1679ParametrosSistema_URLApp = "";
         n1679ParametrosSistema_URLApp = false;
         A1952ParametrosSistema_URLOtherVer = "";
         n1952ParametrosSistema_URLOtherVer = false;
         A1954ParametrosSistema_Validacao = "";
         n1954ParametrosSistema_Validacao = false;
         A2117ParametrosSistema_Imagem = "";
         n2117ParametrosSistema_Imagem = false;
         A40000ParametrosSistema_Imagem_GXI = "";
         n40000ParametrosSistema_Imagem_GXI = false;
         A399ParametrosSistema_LicensiadoCadastrado = false;
         n399ParametrosSistema_LicensiadoCadastrado = false;
         A417ParametrosSistema_PadronizarStrings = true;
         A535ParametrosSistema_EmailSdaAut = false;
         n535ParametrosSistema_EmailSdaAut = false;
         A708ParametrosSistema_FatorAjuste = (decimal)(1);
         n708ParametrosSistema_FatorAjuste = false;
         Z331ParametrosSistema_NomeSistema = "";
         Z334ParametrosSistema_AppID = 0;
         Z399ParametrosSistema_LicensiadoCadastrado = false;
         Z417ParametrosSistema_PadronizarStrings = false;
         Z532ParametrosSistema_EmailSdaHost = "";
         Z533ParametrosSistema_EmailSdaUser = "";
         Z534ParametrosSistema_EmailSdaPass = "";
         Z537ParametrosSistema_EmailSdaKey = "";
         Z535ParametrosSistema_EmailSdaAut = false;
         Z536ParametrosSistema_EmailSdaPort = 0;
         Z2032ParametrosSistema_EmailSdaSec = 0;
         Z708ParametrosSistema_FatorAjuste = 0;
         Z1021ParametrosSistema_PathCrtf = "";
         Z1163ParametrosSistema_HostMensuracao = "";
         Z1171ParametrosSistema_SQLComputerName = "";
         Z1172ParametrosSistema_SQLServerName = "";
         Z1168ParametrosSistema_SQLEdition = "";
         Z1169ParametrosSistema_SQLInstance = "";
         Z1170ParametrosSistema_DBName = "";
         Z1499ParametrosSistema_FlsEvd = "";
         Z1679ParametrosSistema_URLApp = "";
         Z1952ParametrosSistema_URLOtherVer = "";
         Z1954ParametrosSistema_Validacao = "";
      }

      protected void InitAll1J56( )
      {
         A330ParametrosSistema_Codigo = 0;
         InitializeNonKey1J56( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A708ParametrosSistema_FatorAjuste = i708ParametrosSistema_FatorAjuste;
         n708ParametrosSistema_FatorAjuste = false;
         A535ParametrosSistema_EmailSdaAut = i535ParametrosSistema_EmailSdaAut;
         n535ParametrosSistema_EmailSdaAut = false;
         A417ParametrosSistema_PadronizarStrings = i417ParametrosSistema_PadronizarStrings;
         A399ParametrosSistema_LicensiadoCadastrado = i399ParametrosSistema_LicensiadoCadastrado;
         n399ParametrosSistema_LicensiadoCadastrado = false;
      }

      public void VarsToRow56( SdtParametrosSistema obj56 )
      {
         obj56.gxTpr_Mode = Gx_mode;
         obj56.gxTpr_Parametrossistema_nomesistema = A331ParametrosSistema_NomeSistema;
         obj56.gxTpr_Parametrossistema_appid = A334ParametrosSistema_AppID;
         obj56.gxTpr_Parametrossistema_textohome = A332ParametrosSistema_TextoHome;
         obj56.gxTpr_Parametrossistema_emailsdahost = A532ParametrosSistema_EmailSdaHost;
         obj56.gxTpr_Parametrossistema_emailsdauser = A533ParametrosSistema_EmailSdaUser;
         obj56.gxTpr_Parametrossistema_emailsdapass = A534ParametrosSistema_EmailSdaPass;
         obj56.gxTpr_Parametrossistema_emailsdakey = A537ParametrosSistema_EmailSdaKey;
         obj56.gxTpr_Parametrossistema_emailsdaport = A536ParametrosSistema_EmailSdaPort;
         obj56.gxTpr_Parametrossistema_emailsdasec = A2032ParametrosSistema_EmailSdaSec;
         obj56.gxTpr_Parametrossistema_pathcrtf = A1021ParametrosSistema_PathCrtf;
         obj56.gxTpr_Parametrossistema_hostmensuracao = A1163ParametrosSistema_HostMensuracao;
         obj56.gxTpr_Parametrossistema_sqlcomputername = A1171ParametrosSistema_SQLComputerName;
         obj56.gxTpr_Parametrossistema_sqlservername = A1172ParametrosSistema_SQLServerName;
         obj56.gxTpr_Parametrossistema_sqledition = A1168ParametrosSistema_SQLEdition;
         obj56.gxTpr_Parametrossistema_sqlinstance = A1169ParametrosSistema_SQLInstance;
         obj56.gxTpr_Parametrossistema_dbname = A1170ParametrosSistema_DBName;
         obj56.gxTpr_Parametrossistema_flsevd = A1499ParametrosSistema_FlsEvd;
         obj56.gxTpr_Parametrossistema_urlapp = A1679ParametrosSistema_URLApp;
         obj56.gxTpr_Parametrossistema_urlotherver = A1952ParametrosSistema_URLOtherVer;
         obj56.gxTpr_Parametrossistema_validacao = A1954ParametrosSistema_Validacao;
         obj56.gxTpr_Parametrossistema_imagem = A2117ParametrosSistema_Imagem;
         obj56.gxTpr_Parametrossistema_imagem_gxi = A40000ParametrosSistema_Imagem_GXI;
         obj56.gxTpr_Parametrossistema_licensiadocadastrado = A399ParametrosSistema_LicensiadoCadastrado;
         obj56.gxTpr_Parametrossistema_padronizarstrings = A417ParametrosSistema_PadronizarStrings;
         obj56.gxTpr_Parametrossistema_emailsdaaut = A535ParametrosSistema_EmailSdaAut;
         obj56.gxTpr_Parametrossistema_fatorajuste = A708ParametrosSistema_FatorAjuste;
         obj56.gxTpr_Parametrossistema_codigo = A330ParametrosSistema_Codigo;
         obj56.gxTpr_Parametrossistema_codigo_Z = Z330ParametrosSistema_Codigo;
         obj56.gxTpr_Parametrossistema_nomesistema_Z = Z331ParametrosSistema_NomeSistema;
         obj56.gxTpr_Parametrossistema_appid_Z = Z334ParametrosSistema_AppID;
         obj56.gxTpr_Parametrossistema_licensiadocadastrado_Z = Z399ParametrosSistema_LicensiadoCadastrado;
         obj56.gxTpr_Parametrossistema_padronizarstrings_Z = Z417ParametrosSistema_PadronizarStrings;
         obj56.gxTpr_Parametrossistema_emailsdahost_Z = Z532ParametrosSistema_EmailSdaHost;
         obj56.gxTpr_Parametrossistema_emailsdauser_Z = Z533ParametrosSistema_EmailSdaUser;
         obj56.gxTpr_Parametrossistema_emailsdapass_Z = Z534ParametrosSistema_EmailSdaPass;
         obj56.gxTpr_Parametrossistema_emailsdakey_Z = Z537ParametrosSistema_EmailSdaKey;
         obj56.gxTpr_Parametrossistema_emailsdaaut_Z = Z535ParametrosSistema_EmailSdaAut;
         obj56.gxTpr_Parametrossistema_emailsdaport_Z = Z536ParametrosSistema_EmailSdaPort;
         obj56.gxTpr_Parametrossistema_emailsdasec_Z = Z2032ParametrosSistema_EmailSdaSec;
         obj56.gxTpr_Parametrossistema_fatorajuste_Z = Z708ParametrosSistema_FatorAjuste;
         obj56.gxTpr_Parametrossistema_pathcrtf_Z = Z1021ParametrosSistema_PathCrtf;
         obj56.gxTpr_Parametrossistema_hostmensuracao_Z = Z1163ParametrosSistema_HostMensuracao;
         obj56.gxTpr_Parametrossistema_sqlcomputername_Z = Z1171ParametrosSistema_SQLComputerName;
         obj56.gxTpr_Parametrossistema_sqlservername_Z = Z1172ParametrosSistema_SQLServerName;
         obj56.gxTpr_Parametrossistema_sqledition_Z = Z1168ParametrosSistema_SQLEdition;
         obj56.gxTpr_Parametrossistema_sqlinstance_Z = Z1169ParametrosSistema_SQLInstance;
         obj56.gxTpr_Parametrossistema_dbname_Z = Z1170ParametrosSistema_DBName;
         obj56.gxTpr_Parametrossistema_flsevd_Z = Z1499ParametrosSistema_FlsEvd;
         obj56.gxTpr_Parametrossistema_urlapp_Z = Z1679ParametrosSistema_URLApp;
         obj56.gxTpr_Parametrossistema_urlotherver_Z = Z1952ParametrosSistema_URLOtherVer;
         obj56.gxTpr_Parametrossistema_validacao_Z = Z1954ParametrosSistema_Validacao;
         obj56.gxTpr_Parametrossistema_imagem_gxi_Z = Z40000ParametrosSistema_Imagem_GXI;
         obj56.gxTpr_Parametrossistema_licensiadocadastrado_N = (short)(Convert.ToInt16(n399ParametrosSistema_LicensiadoCadastrado));
         obj56.gxTpr_Parametrossistema_emailsdahost_N = (short)(Convert.ToInt16(n532ParametrosSistema_EmailSdaHost));
         obj56.gxTpr_Parametrossistema_emailsdauser_N = (short)(Convert.ToInt16(n533ParametrosSistema_EmailSdaUser));
         obj56.gxTpr_Parametrossistema_emailsdapass_N = (short)(Convert.ToInt16(n534ParametrosSistema_EmailSdaPass));
         obj56.gxTpr_Parametrossistema_emailsdakey_N = (short)(Convert.ToInt16(n537ParametrosSistema_EmailSdaKey));
         obj56.gxTpr_Parametrossistema_emailsdaaut_N = (short)(Convert.ToInt16(n535ParametrosSistema_EmailSdaAut));
         obj56.gxTpr_Parametrossistema_emailsdaport_N = (short)(Convert.ToInt16(n536ParametrosSistema_EmailSdaPort));
         obj56.gxTpr_Parametrossistema_emailsdasec_N = (short)(Convert.ToInt16(n2032ParametrosSistema_EmailSdaSec));
         obj56.gxTpr_Parametrossistema_fatorajuste_N = (short)(Convert.ToInt16(n708ParametrosSistema_FatorAjuste));
         obj56.gxTpr_Parametrossistema_pathcrtf_N = (short)(Convert.ToInt16(n1021ParametrosSistema_PathCrtf));
         obj56.gxTpr_Parametrossistema_hostmensuracao_N = (short)(Convert.ToInt16(n1163ParametrosSistema_HostMensuracao));
         obj56.gxTpr_Parametrossistema_sqlcomputername_N = (short)(Convert.ToInt16(n1171ParametrosSistema_SQLComputerName));
         obj56.gxTpr_Parametrossistema_sqlservername_N = (short)(Convert.ToInt16(n1172ParametrosSistema_SQLServerName));
         obj56.gxTpr_Parametrossistema_sqledition_N = (short)(Convert.ToInt16(n1168ParametrosSistema_SQLEdition));
         obj56.gxTpr_Parametrossistema_sqlinstance_N = (short)(Convert.ToInt16(n1169ParametrosSistema_SQLInstance));
         obj56.gxTpr_Parametrossistema_dbname_N = (short)(Convert.ToInt16(n1170ParametrosSistema_DBName));
         obj56.gxTpr_Parametrossistema_flsevd_N = (short)(Convert.ToInt16(n1499ParametrosSistema_FlsEvd));
         obj56.gxTpr_Parametrossistema_urlapp_N = (short)(Convert.ToInt16(n1679ParametrosSistema_URLApp));
         obj56.gxTpr_Parametrossistema_urlotherver_N = (short)(Convert.ToInt16(n1952ParametrosSistema_URLOtherVer));
         obj56.gxTpr_Parametrossistema_validacao_N = (short)(Convert.ToInt16(n1954ParametrosSistema_Validacao));
         obj56.gxTpr_Parametrossistema_imagem_N = (short)(Convert.ToInt16(n2117ParametrosSistema_Imagem));
         obj56.gxTpr_Parametrossistema_imagem_gxi_N = (short)(Convert.ToInt16(n40000ParametrosSistema_Imagem_GXI));
         obj56.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow56( SdtParametrosSistema obj56 )
      {
         obj56.gxTpr_Parametrossistema_codigo = A330ParametrosSistema_Codigo;
         return  ;
      }

      public void RowToVars56( SdtParametrosSistema obj56 ,
                               int forceLoad )
      {
         Gx_mode = obj56.gxTpr_Mode;
         A331ParametrosSistema_NomeSistema = obj56.gxTpr_Parametrossistema_nomesistema;
         A334ParametrosSistema_AppID = obj56.gxTpr_Parametrossistema_appid;
         A332ParametrosSistema_TextoHome = obj56.gxTpr_Parametrossistema_textohome;
         A532ParametrosSistema_EmailSdaHost = obj56.gxTpr_Parametrossistema_emailsdahost;
         n532ParametrosSistema_EmailSdaHost = false;
         A533ParametrosSistema_EmailSdaUser = obj56.gxTpr_Parametrossistema_emailsdauser;
         n533ParametrosSistema_EmailSdaUser = false;
         A534ParametrosSistema_EmailSdaPass = obj56.gxTpr_Parametrossistema_emailsdapass;
         n534ParametrosSistema_EmailSdaPass = false;
         A537ParametrosSistema_EmailSdaKey = obj56.gxTpr_Parametrossistema_emailsdakey;
         n537ParametrosSistema_EmailSdaKey = false;
         A536ParametrosSistema_EmailSdaPort = obj56.gxTpr_Parametrossistema_emailsdaport;
         n536ParametrosSistema_EmailSdaPort = false;
         A2032ParametrosSistema_EmailSdaSec = obj56.gxTpr_Parametrossistema_emailsdasec;
         n2032ParametrosSistema_EmailSdaSec = false;
         A1021ParametrosSistema_PathCrtf = obj56.gxTpr_Parametrossistema_pathcrtf;
         n1021ParametrosSistema_PathCrtf = false;
         A1163ParametrosSistema_HostMensuracao = obj56.gxTpr_Parametrossistema_hostmensuracao;
         n1163ParametrosSistema_HostMensuracao = false;
         A1171ParametrosSistema_SQLComputerName = obj56.gxTpr_Parametrossistema_sqlcomputername;
         n1171ParametrosSistema_SQLComputerName = false;
         A1172ParametrosSistema_SQLServerName = obj56.gxTpr_Parametrossistema_sqlservername;
         n1172ParametrosSistema_SQLServerName = false;
         A1168ParametrosSistema_SQLEdition = obj56.gxTpr_Parametrossistema_sqledition;
         n1168ParametrosSistema_SQLEdition = false;
         A1169ParametrosSistema_SQLInstance = obj56.gxTpr_Parametrossistema_sqlinstance;
         n1169ParametrosSistema_SQLInstance = false;
         A1170ParametrosSistema_DBName = obj56.gxTpr_Parametrossistema_dbname;
         n1170ParametrosSistema_DBName = false;
         A1499ParametrosSistema_FlsEvd = obj56.gxTpr_Parametrossistema_flsevd;
         n1499ParametrosSistema_FlsEvd = false;
         A1679ParametrosSistema_URLApp = obj56.gxTpr_Parametrossistema_urlapp;
         n1679ParametrosSistema_URLApp = false;
         A1952ParametrosSistema_URLOtherVer = obj56.gxTpr_Parametrossistema_urlotherver;
         n1952ParametrosSistema_URLOtherVer = false;
         A1954ParametrosSistema_Validacao = obj56.gxTpr_Parametrossistema_validacao;
         n1954ParametrosSistema_Validacao = false;
         A2117ParametrosSistema_Imagem = obj56.gxTpr_Parametrossistema_imagem;
         n2117ParametrosSistema_Imagem = false;
         A40000ParametrosSistema_Imagem_GXI = obj56.gxTpr_Parametrossistema_imagem_gxi;
         n40000ParametrosSistema_Imagem_GXI = false;
         A399ParametrosSistema_LicensiadoCadastrado = obj56.gxTpr_Parametrossistema_licensiadocadastrado;
         n399ParametrosSistema_LicensiadoCadastrado = false;
         A417ParametrosSistema_PadronizarStrings = obj56.gxTpr_Parametrossistema_padronizarstrings;
         A535ParametrosSistema_EmailSdaAut = obj56.gxTpr_Parametrossistema_emailsdaaut;
         n535ParametrosSistema_EmailSdaAut = false;
         A708ParametrosSistema_FatorAjuste = obj56.gxTpr_Parametrossistema_fatorajuste;
         n708ParametrosSistema_FatorAjuste = false;
         A330ParametrosSistema_Codigo = obj56.gxTpr_Parametrossistema_codigo;
         Z330ParametrosSistema_Codigo = obj56.gxTpr_Parametrossistema_codigo_Z;
         Z331ParametrosSistema_NomeSistema = obj56.gxTpr_Parametrossistema_nomesistema_Z;
         Z334ParametrosSistema_AppID = obj56.gxTpr_Parametrossistema_appid_Z;
         Z399ParametrosSistema_LicensiadoCadastrado = obj56.gxTpr_Parametrossistema_licensiadocadastrado_Z;
         Z417ParametrosSistema_PadronizarStrings = obj56.gxTpr_Parametrossistema_padronizarstrings_Z;
         Z532ParametrosSistema_EmailSdaHost = obj56.gxTpr_Parametrossistema_emailsdahost_Z;
         Z533ParametrosSistema_EmailSdaUser = obj56.gxTpr_Parametrossistema_emailsdauser_Z;
         Z534ParametrosSistema_EmailSdaPass = obj56.gxTpr_Parametrossistema_emailsdapass_Z;
         Z537ParametrosSistema_EmailSdaKey = obj56.gxTpr_Parametrossistema_emailsdakey_Z;
         Z535ParametrosSistema_EmailSdaAut = obj56.gxTpr_Parametrossistema_emailsdaaut_Z;
         Z536ParametrosSistema_EmailSdaPort = obj56.gxTpr_Parametrossistema_emailsdaport_Z;
         Z2032ParametrosSistema_EmailSdaSec = obj56.gxTpr_Parametrossistema_emailsdasec_Z;
         Z708ParametrosSistema_FatorAjuste = obj56.gxTpr_Parametrossistema_fatorajuste_Z;
         Z1021ParametrosSistema_PathCrtf = obj56.gxTpr_Parametrossistema_pathcrtf_Z;
         Z1163ParametrosSistema_HostMensuracao = obj56.gxTpr_Parametrossistema_hostmensuracao_Z;
         Z1171ParametrosSistema_SQLComputerName = obj56.gxTpr_Parametrossistema_sqlcomputername_Z;
         Z1172ParametrosSistema_SQLServerName = obj56.gxTpr_Parametrossistema_sqlservername_Z;
         Z1168ParametrosSistema_SQLEdition = obj56.gxTpr_Parametrossistema_sqledition_Z;
         Z1169ParametrosSistema_SQLInstance = obj56.gxTpr_Parametrossistema_sqlinstance_Z;
         Z1170ParametrosSistema_DBName = obj56.gxTpr_Parametrossistema_dbname_Z;
         Z1499ParametrosSistema_FlsEvd = obj56.gxTpr_Parametrossistema_flsevd_Z;
         Z1679ParametrosSistema_URLApp = obj56.gxTpr_Parametrossistema_urlapp_Z;
         Z1952ParametrosSistema_URLOtherVer = obj56.gxTpr_Parametrossistema_urlotherver_Z;
         Z1954ParametrosSistema_Validacao = obj56.gxTpr_Parametrossistema_validacao_Z;
         Z40000ParametrosSistema_Imagem_GXI = obj56.gxTpr_Parametrossistema_imagem_gxi_Z;
         n399ParametrosSistema_LicensiadoCadastrado = (bool)(Convert.ToBoolean(obj56.gxTpr_Parametrossistema_licensiadocadastrado_N));
         n532ParametrosSistema_EmailSdaHost = (bool)(Convert.ToBoolean(obj56.gxTpr_Parametrossistema_emailsdahost_N));
         n533ParametrosSistema_EmailSdaUser = (bool)(Convert.ToBoolean(obj56.gxTpr_Parametrossistema_emailsdauser_N));
         n534ParametrosSistema_EmailSdaPass = (bool)(Convert.ToBoolean(obj56.gxTpr_Parametrossistema_emailsdapass_N));
         n537ParametrosSistema_EmailSdaKey = (bool)(Convert.ToBoolean(obj56.gxTpr_Parametrossistema_emailsdakey_N));
         n535ParametrosSistema_EmailSdaAut = (bool)(Convert.ToBoolean(obj56.gxTpr_Parametrossistema_emailsdaaut_N));
         n536ParametrosSistema_EmailSdaPort = (bool)(Convert.ToBoolean(obj56.gxTpr_Parametrossistema_emailsdaport_N));
         n2032ParametrosSistema_EmailSdaSec = (bool)(Convert.ToBoolean(obj56.gxTpr_Parametrossistema_emailsdasec_N));
         n708ParametrosSistema_FatorAjuste = (bool)(Convert.ToBoolean(obj56.gxTpr_Parametrossistema_fatorajuste_N));
         n1021ParametrosSistema_PathCrtf = (bool)(Convert.ToBoolean(obj56.gxTpr_Parametrossistema_pathcrtf_N));
         n1163ParametrosSistema_HostMensuracao = (bool)(Convert.ToBoolean(obj56.gxTpr_Parametrossistema_hostmensuracao_N));
         n1171ParametrosSistema_SQLComputerName = (bool)(Convert.ToBoolean(obj56.gxTpr_Parametrossistema_sqlcomputername_N));
         n1172ParametrosSistema_SQLServerName = (bool)(Convert.ToBoolean(obj56.gxTpr_Parametrossistema_sqlservername_N));
         n1168ParametrosSistema_SQLEdition = (bool)(Convert.ToBoolean(obj56.gxTpr_Parametrossistema_sqledition_N));
         n1169ParametrosSistema_SQLInstance = (bool)(Convert.ToBoolean(obj56.gxTpr_Parametrossistema_sqlinstance_N));
         n1170ParametrosSistema_DBName = (bool)(Convert.ToBoolean(obj56.gxTpr_Parametrossistema_dbname_N));
         n1499ParametrosSistema_FlsEvd = (bool)(Convert.ToBoolean(obj56.gxTpr_Parametrossistema_flsevd_N));
         n1679ParametrosSistema_URLApp = (bool)(Convert.ToBoolean(obj56.gxTpr_Parametrossistema_urlapp_N));
         n1952ParametrosSistema_URLOtherVer = (bool)(Convert.ToBoolean(obj56.gxTpr_Parametrossistema_urlotherver_N));
         n1954ParametrosSistema_Validacao = (bool)(Convert.ToBoolean(obj56.gxTpr_Parametrossistema_validacao_N));
         n2117ParametrosSistema_Imagem = (bool)(Convert.ToBoolean(obj56.gxTpr_Parametrossistema_imagem_N));
         n40000ParametrosSistema_Imagem_GXI = (bool)(Convert.ToBoolean(obj56.gxTpr_Parametrossistema_imagem_gxi_N));
         Gx_mode = obj56.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A330ParametrosSistema_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey1J56( ) ;
         ScanKeyStart1J56( ) ;
         if ( RcdFound56 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z330ParametrosSistema_Codigo = A330ParametrosSistema_Codigo;
         }
         ZM1J56( -5) ;
         OnLoadActions1J56( ) ;
         AddRow1J56( ) ;
         ScanKeyEnd1J56( ) ;
         if ( RcdFound56 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars56( bcParametrosSistema, 0) ;
         ScanKeyStart1J56( ) ;
         if ( RcdFound56 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z330ParametrosSistema_Codigo = A330ParametrosSistema_Codigo;
         }
         ZM1J56( -5) ;
         OnLoadActions1J56( ) ;
         AddRow1J56( ) ;
         ScanKeyEnd1J56( ) ;
         if ( RcdFound56 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars56( bcParametrosSistema, 0) ;
         nKeyPressed = 1;
         GetKey1J56( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert1J56( ) ;
         }
         else
         {
            if ( RcdFound56 == 1 )
            {
               if ( A330ParametrosSistema_Codigo != Z330ParametrosSistema_Codigo )
               {
                  A330ParametrosSistema_Codigo = Z330ParametrosSistema_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update1J56( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A330ParametrosSistema_Codigo != Z330ParametrosSistema_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1J56( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1J56( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow56( bcParametrosSistema) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars56( bcParametrosSistema, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey1J56( ) ;
         if ( RcdFound56 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A330ParametrosSistema_Codigo != Z330ParametrosSistema_Codigo )
            {
               A330ParametrosSistema_Codigo = Z330ParametrosSistema_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A330ParametrosSistema_Codigo != Z330ParametrosSistema_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         context.RollbackDataStores( "ParametrosSistema_BC");
         VarsToRow56( bcParametrosSistema) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcParametrosSistema.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcParametrosSistema.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcParametrosSistema )
         {
            bcParametrosSistema = (SdtParametrosSistema)(sdt);
            if ( StringUtil.StrCmp(bcParametrosSistema.gxTpr_Mode, "") == 0 )
            {
               bcParametrosSistema.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow56( bcParametrosSistema) ;
            }
            else
            {
               RowToVars56( bcParametrosSistema, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcParametrosSistema.gxTpr_Mode, "") == 0 )
            {
               bcParametrosSistema.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars56( bcParametrosSistema, 1) ;
         return  ;
      }

      public SdtParametrosSistema ParametrosSistema_BC
      {
         get {
            return bcParametrosSistema ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         A1021ParametrosSistema_PathCrtf = "";
         Z331ParametrosSistema_NomeSistema = "";
         A331ParametrosSistema_NomeSistema = "";
         Z532ParametrosSistema_EmailSdaHost = "";
         A532ParametrosSistema_EmailSdaHost = "";
         Z533ParametrosSistema_EmailSdaUser = "";
         A533ParametrosSistema_EmailSdaUser = "";
         Z534ParametrosSistema_EmailSdaPass = "";
         A534ParametrosSistema_EmailSdaPass = "";
         Z537ParametrosSistema_EmailSdaKey = "";
         A537ParametrosSistema_EmailSdaKey = "";
         Z1021ParametrosSistema_PathCrtf = "";
         Z1163ParametrosSistema_HostMensuracao = "";
         A1163ParametrosSistema_HostMensuracao = "";
         Z1171ParametrosSistema_SQLComputerName = "";
         A1171ParametrosSistema_SQLComputerName = "";
         Z1172ParametrosSistema_SQLServerName = "";
         A1172ParametrosSistema_SQLServerName = "";
         Z1168ParametrosSistema_SQLEdition = "";
         A1168ParametrosSistema_SQLEdition = "";
         Z1169ParametrosSistema_SQLInstance = "";
         A1169ParametrosSistema_SQLInstance = "";
         Z1170ParametrosSistema_DBName = "";
         A1170ParametrosSistema_DBName = "";
         Z1499ParametrosSistema_FlsEvd = "";
         A1499ParametrosSistema_FlsEvd = "";
         Z1679ParametrosSistema_URLApp = "";
         A1679ParametrosSistema_URLApp = "";
         Z1952ParametrosSistema_URLOtherVer = "";
         A1952ParametrosSistema_URLOtherVer = "";
         Z1954ParametrosSistema_Validacao = "";
         A1954ParametrosSistema_Validacao = "";
         Z332ParametrosSistema_TextoHome = "";
         A332ParametrosSistema_TextoHome = "";
         Z2117ParametrosSistema_Imagem = "";
         A2117ParametrosSistema_Imagem = "";
         Z40000ParametrosSistema_Imagem_GXI = "";
         A40000ParametrosSistema_Imagem_GXI = "";
         BC001J4_A330ParametrosSistema_Codigo = new int[1] ;
         BC001J4_A331ParametrosSistema_NomeSistema = new String[] {""} ;
         BC001J4_A334ParametrosSistema_AppID = new long[1] ;
         BC001J4_A332ParametrosSistema_TextoHome = new String[] {""} ;
         BC001J4_A399ParametrosSistema_LicensiadoCadastrado = new bool[] {false} ;
         BC001J4_n399ParametrosSistema_LicensiadoCadastrado = new bool[] {false} ;
         BC001J4_A417ParametrosSistema_PadronizarStrings = new bool[] {false} ;
         BC001J4_A532ParametrosSistema_EmailSdaHost = new String[] {""} ;
         BC001J4_n532ParametrosSistema_EmailSdaHost = new bool[] {false} ;
         BC001J4_A533ParametrosSistema_EmailSdaUser = new String[] {""} ;
         BC001J4_n533ParametrosSistema_EmailSdaUser = new bool[] {false} ;
         BC001J4_A534ParametrosSistema_EmailSdaPass = new String[] {""} ;
         BC001J4_n534ParametrosSistema_EmailSdaPass = new bool[] {false} ;
         BC001J4_A537ParametrosSistema_EmailSdaKey = new String[] {""} ;
         BC001J4_n537ParametrosSistema_EmailSdaKey = new bool[] {false} ;
         BC001J4_A535ParametrosSistema_EmailSdaAut = new bool[] {false} ;
         BC001J4_n535ParametrosSistema_EmailSdaAut = new bool[] {false} ;
         BC001J4_A536ParametrosSistema_EmailSdaPort = new short[1] ;
         BC001J4_n536ParametrosSistema_EmailSdaPort = new bool[] {false} ;
         BC001J4_A2032ParametrosSistema_EmailSdaSec = new short[1] ;
         BC001J4_n2032ParametrosSistema_EmailSdaSec = new bool[] {false} ;
         BC001J4_A708ParametrosSistema_FatorAjuste = new decimal[1] ;
         BC001J4_n708ParametrosSistema_FatorAjuste = new bool[] {false} ;
         BC001J4_A1021ParametrosSistema_PathCrtf = new String[] {""} ;
         BC001J4_n1021ParametrosSistema_PathCrtf = new bool[] {false} ;
         BC001J4_A1163ParametrosSistema_HostMensuracao = new String[] {""} ;
         BC001J4_n1163ParametrosSistema_HostMensuracao = new bool[] {false} ;
         BC001J4_A1171ParametrosSistema_SQLComputerName = new String[] {""} ;
         BC001J4_n1171ParametrosSistema_SQLComputerName = new bool[] {false} ;
         BC001J4_A1172ParametrosSistema_SQLServerName = new String[] {""} ;
         BC001J4_n1172ParametrosSistema_SQLServerName = new bool[] {false} ;
         BC001J4_A1168ParametrosSistema_SQLEdition = new String[] {""} ;
         BC001J4_n1168ParametrosSistema_SQLEdition = new bool[] {false} ;
         BC001J4_A1169ParametrosSistema_SQLInstance = new String[] {""} ;
         BC001J4_n1169ParametrosSistema_SQLInstance = new bool[] {false} ;
         BC001J4_A1170ParametrosSistema_DBName = new String[] {""} ;
         BC001J4_n1170ParametrosSistema_DBName = new bool[] {false} ;
         BC001J4_A1499ParametrosSistema_FlsEvd = new String[] {""} ;
         BC001J4_n1499ParametrosSistema_FlsEvd = new bool[] {false} ;
         BC001J4_A1679ParametrosSistema_URLApp = new String[] {""} ;
         BC001J4_n1679ParametrosSistema_URLApp = new bool[] {false} ;
         BC001J4_A1952ParametrosSistema_URLOtherVer = new String[] {""} ;
         BC001J4_n1952ParametrosSistema_URLOtherVer = new bool[] {false} ;
         BC001J4_A1954ParametrosSistema_Validacao = new String[] {""} ;
         BC001J4_n1954ParametrosSistema_Validacao = new bool[] {false} ;
         BC001J4_A40000ParametrosSistema_Imagem_GXI = new String[] {""} ;
         BC001J4_n40000ParametrosSistema_Imagem_GXI = new bool[] {false} ;
         BC001J4_A2117ParametrosSistema_Imagem = new String[] {""} ;
         BC001J4_n2117ParametrosSistema_Imagem = new bool[] {false} ;
         BC001J5_A330ParametrosSistema_Codigo = new int[1] ;
         BC001J3_A330ParametrosSistema_Codigo = new int[1] ;
         BC001J3_A331ParametrosSistema_NomeSistema = new String[] {""} ;
         BC001J3_A334ParametrosSistema_AppID = new long[1] ;
         BC001J3_A332ParametrosSistema_TextoHome = new String[] {""} ;
         BC001J3_A399ParametrosSistema_LicensiadoCadastrado = new bool[] {false} ;
         BC001J3_n399ParametrosSistema_LicensiadoCadastrado = new bool[] {false} ;
         BC001J3_A417ParametrosSistema_PadronizarStrings = new bool[] {false} ;
         BC001J3_A532ParametrosSistema_EmailSdaHost = new String[] {""} ;
         BC001J3_n532ParametrosSistema_EmailSdaHost = new bool[] {false} ;
         BC001J3_A533ParametrosSistema_EmailSdaUser = new String[] {""} ;
         BC001J3_n533ParametrosSistema_EmailSdaUser = new bool[] {false} ;
         BC001J3_A534ParametrosSistema_EmailSdaPass = new String[] {""} ;
         BC001J3_n534ParametrosSistema_EmailSdaPass = new bool[] {false} ;
         BC001J3_A537ParametrosSistema_EmailSdaKey = new String[] {""} ;
         BC001J3_n537ParametrosSistema_EmailSdaKey = new bool[] {false} ;
         BC001J3_A535ParametrosSistema_EmailSdaAut = new bool[] {false} ;
         BC001J3_n535ParametrosSistema_EmailSdaAut = new bool[] {false} ;
         BC001J3_A536ParametrosSistema_EmailSdaPort = new short[1] ;
         BC001J3_n536ParametrosSistema_EmailSdaPort = new bool[] {false} ;
         BC001J3_A2032ParametrosSistema_EmailSdaSec = new short[1] ;
         BC001J3_n2032ParametrosSistema_EmailSdaSec = new bool[] {false} ;
         BC001J3_A708ParametrosSistema_FatorAjuste = new decimal[1] ;
         BC001J3_n708ParametrosSistema_FatorAjuste = new bool[] {false} ;
         BC001J3_A1021ParametrosSistema_PathCrtf = new String[] {""} ;
         BC001J3_n1021ParametrosSistema_PathCrtf = new bool[] {false} ;
         BC001J3_A1163ParametrosSistema_HostMensuracao = new String[] {""} ;
         BC001J3_n1163ParametrosSistema_HostMensuracao = new bool[] {false} ;
         BC001J3_A1171ParametrosSistema_SQLComputerName = new String[] {""} ;
         BC001J3_n1171ParametrosSistema_SQLComputerName = new bool[] {false} ;
         BC001J3_A1172ParametrosSistema_SQLServerName = new String[] {""} ;
         BC001J3_n1172ParametrosSistema_SQLServerName = new bool[] {false} ;
         BC001J3_A1168ParametrosSistema_SQLEdition = new String[] {""} ;
         BC001J3_n1168ParametrosSistema_SQLEdition = new bool[] {false} ;
         BC001J3_A1169ParametrosSistema_SQLInstance = new String[] {""} ;
         BC001J3_n1169ParametrosSistema_SQLInstance = new bool[] {false} ;
         BC001J3_A1170ParametrosSistema_DBName = new String[] {""} ;
         BC001J3_n1170ParametrosSistema_DBName = new bool[] {false} ;
         BC001J3_A1499ParametrosSistema_FlsEvd = new String[] {""} ;
         BC001J3_n1499ParametrosSistema_FlsEvd = new bool[] {false} ;
         BC001J3_A1679ParametrosSistema_URLApp = new String[] {""} ;
         BC001J3_n1679ParametrosSistema_URLApp = new bool[] {false} ;
         BC001J3_A1952ParametrosSistema_URLOtherVer = new String[] {""} ;
         BC001J3_n1952ParametrosSistema_URLOtherVer = new bool[] {false} ;
         BC001J3_A1954ParametrosSistema_Validacao = new String[] {""} ;
         BC001J3_n1954ParametrosSistema_Validacao = new bool[] {false} ;
         BC001J3_A40000ParametrosSistema_Imagem_GXI = new String[] {""} ;
         BC001J3_n40000ParametrosSistema_Imagem_GXI = new bool[] {false} ;
         BC001J3_A2117ParametrosSistema_Imagem = new String[] {""} ;
         BC001J3_n2117ParametrosSistema_Imagem = new bool[] {false} ;
         sMode56 = "";
         BC001J2_A330ParametrosSistema_Codigo = new int[1] ;
         BC001J2_A331ParametrosSistema_NomeSistema = new String[] {""} ;
         BC001J2_A334ParametrosSistema_AppID = new long[1] ;
         BC001J2_A332ParametrosSistema_TextoHome = new String[] {""} ;
         BC001J2_A399ParametrosSistema_LicensiadoCadastrado = new bool[] {false} ;
         BC001J2_n399ParametrosSistema_LicensiadoCadastrado = new bool[] {false} ;
         BC001J2_A417ParametrosSistema_PadronizarStrings = new bool[] {false} ;
         BC001J2_A532ParametrosSistema_EmailSdaHost = new String[] {""} ;
         BC001J2_n532ParametrosSistema_EmailSdaHost = new bool[] {false} ;
         BC001J2_A533ParametrosSistema_EmailSdaUser = new String[] {""} ;
         BC001J2_n533ParametrosSistema_EmailSdaUser = new bool[] {false} ;
         BC001J2_A534ParametrosSistema_EmailSdaPass = new String[] {""} ;
         BC001J2_n534ParametrosSistema_EmailSdaPass = new bool[] {false} ;
         BC001J2_A537ParametrosSistema_EmailSdaKey = new String[] {""} ;
         BC001J2_n537ParametrosSistema_EmailSdaKey = new bool[] {false} ;
         BC001J2_A535ParametrosSistema_EmailSdaAut = new bool[] {false} ;
         BC001J2_n535ParametrosSistema_EmailSdaAut = new bool[] {false} ;
         BC001J2_A536ParametrosSistema_EmailSdaPort = new short[1] ;
         BC001J2_n536ParametrosSistema_EmailSdaPort = new bool[] {false} ;
         BC001J2_A2032ParametrosSistema_EmailSdaSec = new short[1] ;
         BC001J2_n2032ParametrosSistema_EmailSdaSec = new bool[] {false} ;
         BC001J2_A708ParametrosSistema_FatorAjuste = new decimal[1] ;
         BC001J2_n708ParametrosSistema_FatorAjuste = new bool[] {false} ;
         BC001J2_A1021ParametrosSistema_PathCrtf = new String[] {""} ;
         BC001J2_n1021ParametrosSistema_PathCrtf = new bool[] {false} ;
         BC001J2_A1163ParametrosSistema_HostMensuracao = new String[] {""} ;
         BC001J2_n1163ParametrosSistema_HostMensuracao = new bool[] {false} ;
         BC001J2_A1171ParametrosSistema_SQLComputerName = new String[] {""} ;
         BC001J2_n1171ParametrosSistema_SQLComputerName = new bool[] {false} ;
         BC001J2_A1172ParametrosSistema_SQLServerName = new String[] {""} ;
         BC001J2_n1172ParametrosSistema_SQLServerName = new bool[] {false} ;
         BC001J2_A1168ParametrosSistema_SQLEdition = new String[] {""} ;
         BC001J2_n1168ParametrosSistema_SQLEdition = new bool[] {false} ;
         BC001J2_A1169ParametrosSistema_SQLInstance = new String[] {""} ;
         BC001J2_n1169ParametrosSistema_SQLInstance = new bool[] {false} ;
         BC001J2_A1170ParametrosSistema_DBName = new String[] {""} ;
         BC001J2_n1170ParametrosSistema_DBName = new bool[] {false} ;
         BC001J2_A1499ParametrosSistema_FlsEvd = new String[] {""} ;
         BC001J2_n1499ParametrosSistema_FlsEvd = new bool[] {false} ;
         BC001J2_A1679ParametrosSistema_URLApp = new String[] {""} ;
         BC001J2_n1679ParametrosSistema_URLApp = new bool[] {false} ;
         BC001J2_A1952ParametrosSistema_URLOtherVer = new String[] {""} ;
         BC001J2_n1952ParametrosSistema_URLOtherVer = new bool[] {false} ;
         BC001J2_A1954ParametrosSistema_Validacao = new String[] {""} ;
         BC001J2_n1954ParametrosSistema_Validacao = new bool[] {false} ;
         BC001J2_A40000ParametrosSistema_Imagem_GXI = new String[] {""} ;
         BC001J2_n40000ParametrosSistema_Imagem_GXI = new bool[] {false} ;
         BC001J2_A2117ParametrosSistema_Imagem = new String[] {""} ;
         BC001J2_n2117ParametrosSistema_Imagem = new bool[] {false} ;
         BC001J10_A330ParametrosSistema_Codigo = new int[1] ;
         BC001J10_A331ParametrosSistema_NomeSistema = new String[] {""} ;
         BC001J10_A334ParametrosSistema_AppID = new long[1] ;
         BC001J10_A332ParametrosSistema_TextoHome = new String[] {""} ;
         BC001J10_A399ParametrosSistema_LicensiadoCadastrado = new bool[] {false} ;
         BC001J10_n399ParametrosSistema_LicensiadoCadastrado = new bool[] {false} ;
         BC001J10_A417ParametrosSistema_PadronizarStrings = new bool[] {false} ;
         BC001J10_A532ParametrosSistema_EmailSdaHost = new String[] {""} ;
         BC001J10_n532ParametrosSistema_EmailSdaHost = new bool[] {false} ;
         BC001J10_A533ParametrosSistema_EmailSdaUser = new String[] {""} ;
         BC001J10_n533ParametrosSistema_EmailSdaUser = new bool[] {false} ;
         BC001J10_A534ParametrosSistema_EmailSdaPass = new String[] {""} ;
         BC001J10_n534ParametrosSistema_EmailSdaPass = new bool[] {false} ;
         BC001J10_A537ParametrosSistema_EmailSdaKey = new String[] {""} ;
         BC001J10_n537ParametrosSistema_EmailSdaKey = new bool[] {false} ;
         BC001J10_A535ParametrosSistema_EmailSdaAut = new bool[] {false} ;
         BC001J10_n535ParametrosSistema_EmailSdaAut = new bool[] {false} ;
         BC001J10_A536ParametrosSistema_EmailSdaPort = new short[1] ;
         BC001J10_n536ParametrosSistema_EmailSdaPort = new bool[] {false} ;
         BC001J10_A2032ParametrosSistema_EmailSdaSec = new short[1] ;
         BC001J10_n2032ParametrosSistema_EmailSdaSec = new bool[] {false} ;
         BC001J10_A708ParametrosSistema_FatorAjuste = new decimal[1] ;
         BC001J10_n708ParametrosSistema_FatorAjuste = new bool[] {false} ;
         BC001J10_A1021ParametrosSistema_PathCrtf = new String[] {""} ;
         BC001J10_n1021ParametrosSistema_PathCrtf = new bool[] {false} ;
         BC001J10_A1163ParametrosSistema_HostMensuracao = new String[] {""} ;
         BC001J10_n1163ParametrosSistema_HostMensuracao = new bool[] {false} ;
         BC001J10_A1171ParametrosSistema_SQLComputerName = new String[] {""} ;
         BC001J10_n1171ParametrosSistema_SQLComputerName = new bool[] {false} ;
         BC001J10_A1172ParametrosSistema_SQLServerName = new String[] {""} ;
         BC001J10_n1172ParametrosSistema_SQLServerName = new bool[] {false} ;
         BC001J10_A1168ParametrosSistema_SQLEdition = new String[] {""} ;
         BC001J10_n1168ParametrosSistema_SQLEdition = new bool[] {false} ;
         BC001J10_A1169ParametrosSistema_SQLInstance = new String[] {""} ;
         BC001J10_n1169ParametrosSistema_SQLInstance = new bool[] {false} ;
         BC001J10_A1170ParametrosSistema_DBName = new String[] {""} ;
         BC001J10_n1170ParametrosSistema_DBName = new bool[] {false} ;
         BC001J10_A1499ParametrosSistema_FlsEvd = new String[] {""} ;
         BC001J10_n1499ParametrosSistema_FlsEvd = new bool[] {false} ;
         BC001J10_A1679ParametrosSistema_URLApp = new String[] {""} ;
         BC001J10_n1679ParametrosSistema_URLApp = new bool[] {false} ;
         BC001J10_A1952ParametrosSistema_URLOtherVer = new String[] {""} ;
         BC001J10_n1952ParametrosSistema_URLOtherVer = new bool[] {false} ;
         BC001J10_A1954ParametrosSistema_Validacao = new String[] {""} ;
         BC001J10_n1954ParametrosSistema_Validacao = new bool[] {false} ;
         BC001J10_A40000ParametrosSistema_Imagem_GXI = new String[] {""} ;
         BC001J10_n40000ParametrosSistema_Imagem_GXI = new bool[] {false} ;
         BC001J10_A2117ParametrosSistema_Imagem = new String[] {""} ;
         BC001J10_n2117ParametrosSistema_Imagem = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.parametrossistema_bc__default(),
            new Object[][] {
                new Object[] {
               BC001J2_A330ParametrosSistema_Codigo, BC001J2_A331ParametrosSistema_NomeSistema, BC001J2_A334ParametrosSistema_AppID, BC001J2_A332ParametrosSistema_TextoHome, BC001J2_A399ParametrosSistema_LicensiadoCadastrado, BC001J2_n399ParametrosSistema_LicensiadoCadastrado, BC001J2_A417ParametrosSistema_PadronizarStrings, BC001J2_A532ParametrosSistema_EmailSdaHost, BC001J2_n532ParametrosSistema_EmailSdaHost, BC001J2_A533ParametrosSistema_EmailSdaUser,
               BC001J2_n533ParametrosSistema_EmailSdaUser, BC001J2_A534ParametrosSistema_EmailSdaPass, BC001J2_n534ParametrosSistema_EmailSdaPass, BC001J2_A537ParametrosSistema_EmailSdaKey, BC001J2_n537ParametrosSistema_EmailSdaKey, BC001J2_A535ParametrosSistema_EmailSdaAut, BC001J2_n535ParametrosSistema_EmailSdaAut, BC001J2_A536ParametrosSistema_EmailSdaPort, BC001J2_n536ParametrosSistema_EmailSdaPort, BC001J2_A2032ParametrosSistema_EmailSdaSec,
               BC001J2_n2032ParametrosSistema_EmailSdaSec, BC001J2_A708ParametrosSistema_FatorAjuste, BC001J2_n708ParametrosSistema_FatorAjuste, BC001J2_A1021ParametrosSistema_PathCrtf, BC001J2_n1021ParametrosSistema_PathCrtf, BC001J2_A1163ParametrosSistema_HostMensuracao, BC001J2_n1163ParametrosSistema_HostMensuracao, BC001J2_A1171ParametrosSistema_SQLComputerName, BC001J2_n1171ParametrosSistema_SQLComputerName, BC001J2_A1172ParametrosSistema_SQLServerName,
               BC001J2_n1172ParametrosSistema_SQLServerName, BC001J2_A1168ParametrosSistema_SQLEdition, BC001J2_n1168ParametrosSistema_SQLEdition, BC001J2_A1169ParametrosSistema_SQLInstance, BC001J2_n1169ParametrosSistema_SQLInstance, BC001J2_A1170ParametrosSistema_DBName, BC001J2_n1170ParametrosSistema_DBName, BC001J2_A1499ParametrosSistema_FlsEvd, BC001J2_n1499ParametrosSistema_FlsEvd, BC001J2_A1679ParametrosSistema_URLApp,
               BC001J2_n1679ParametrosSistema_URLApp, BC001J2_A1952ParametrosSistema_URLOtherVer, BC001J2_n1952ParametrosSistema_URLOtherVer, BC001J2_A1954ParametrosSistema_Validacao, BC001J2_n1954ParametrosSistema_Validacao, BC001J2_A40000ParametrosSistema_Imagem_GXI, BC001J2_n40000ParametrosSistema_Imagem_GXI, BC001J2_A2117ParametrosSistema_Imagem, BC001J2_n2117ParametrosSistema_Imagem
               }
               , new Object[] {
               BC001J3_A330ParametrosSistema_Codigo, BC001J3_A331ParametrosSistema_NomeSistema, BC001J3_A334ParametrosSistema_AppID, BC001J3_A332ParametrosSistema_TextoHome, BC001J3_A399ParametrosSistema_LicensiadoCadastrado, BC001J3_n399ParametrosSistema_LicensiadoCadastrado, BC001J3_A417ParametrosSistema_PadronizarStrings, BC001J3_A532ParametrosSistema_EmailSdaHost, BC001J3_n532ParametrosSistema_EmailSdaHost, BC001J3_A533ParametrosSistema_EmailSdaUser,
               BC001J3_n533ParametrosSistema_EmailSdaUser, BC001J3_A534ParametrosSistema_EmailSdaPass, BC001J3_n534ParametrosSistema_EmailSdaPass, BC001J3_A537ParametrosSistema_EmailSdaKey, BC001J3_n537ParametrosSistema_EmailSdaKey, BC001J3_A535ParametrosSistema_EmailSdaAut, BC001J3_n535ParametrosSistema_EmailSdaAut, BC001J3_A536ParametrosSistema_EmailSdaPort, BC001J3_n536ParametrosSistema_EmailSdaPort, BC001J3_A2032ParametrosSistema_EmailSdaSec,
               BC001J3_n2032ParametrosSistema_EmailSdaSec, BC001J3_A708ParametrosSistema_FatorAjuste, BC001J3_n708ParametrosSistema_FatorAjuste, BC001J3_A1021ParametrosSistema_PathCrtf, BC001J3_n1021ParametrosSistema_PathCrtf, BC001J3_A1163ParametrosSistema_HostMensuracao, BC001J3_n1163ParametrosSistema_HostMensuracao, BC001J3_A1171ParametrosSistema_SQLComputerName, BC001J3_n1171ParametrosSistema_SQLComputerName, BC001J3_A1172ParametrosSistema_SQLServerName,
               BC001J3_n1172ParametrosSistema_SQLServerName, BC001J3_A1168ParametrosSistema_SQLEdition, BC001J3_n1168ParametrosSistema_SQLEdition, BC001J3_A1169ParametrosSistema_SQLInstance, BC001J3_n1169ParametrosSistema_SQLInstance, BC001J3_A1170ParametrosSistema_DBName, BC001J3_n1170ParametrosSistema_DBName, BC001J3_A1499ParametrosSistema_FlsEvd, BC001J3_n1499ParametrosSistema_FlsEvd, BC001J3_A1679ParametrosSistema_URLApp,
               BC001J3_n1679ParametrosSistema_URLApp, BC001J3_A1952ParametrosSistema_URLOtherVer, BC001J3_n1952ParametrosSistema_URLOtherVer, BC001J3_A1954ParametrosSistema_Validacao, BC001J3_n1954ParametrosSistema_Validacao, BC001J3_A40000ParametrosSistema_Imagem_GXI, BC001J3_n40000ParametrosSistema_Imagem_GXI, BC001J3_A2117ParametrosSistema_Imagem, BC001J3_n2117ParametrosSistema_Imagem
               }
               , new Object[] {
               BC001J4_A330ParametrosSistema_Codigo, BC001J4_A331ParametrosSistema_NomeSistema, BC001J4_A334ParametrosSistema_AppID, BC001J4_A332ParametrosSistema_TextoHome, BC001J4_A399ParametrosSistema_LicensiadoCadastrado, BC001J4_n399ParametrosSistema_LicensiadoCadastrado, BC001J4_A417ParametrosSistema_PadronizarStrings, BC001J4_A532ParametrosSistema_EmailSdaHost, BC001J4_n532ParametrosSistema_EmailSdaHost, BC001J4_A533ParametrosSistema_EmailSdaUser,
               BC001J4_n533ParametrosSistema_EmailSdaUser, BC001J4_A534ParametrosSistema_EmailSdaPass, BC001J4_n534ParametrosSistema_EmailSdaPass, BC001J4_A537ParametrosSistema_EmailSdaKey, BC001J4_n537ParametrosSistema_EmailSdaKey, BC001J4_A535ParametrosSistema_EmailSdaAut, BC001J4_n535ParametrosSistema_EmailSdaAut, BC001J4_A536ParametrosSistema_EmailSdaPort, BC001J4_n536ParametrosSistema_EmailSdaPort, BC001J4_A2032ParametrosSistema_EmailSdaSec,
               BC001J4_n2032ParametrosSistema_EmailSdaSec, BC001J4_A708ParametrosSistema_FatorAjuste, BC001J4_n708ParametrosSistema_FatorAjuste, BC001J4_A1021ParametrosSistema_PathCrtf, BC001J4_n1021ParametrosSistema_PathCrtf, BC001J4_A1163ParametrosSistema_HostMensuracao, BC001J4_n1163ParametrosSistema_HostMensuracao, BC001J4_A1171ParametrosSistema_SQLComputerName, BC001J4_n1171ParametrosSistema_SQLComputerName, BC001J4_A1172ParametrosSistema_SQLServerName,
               BC001J4_n1172ParametrosSistema_SQLServerName, BC001J4_A1168ParametrosSistema_SQLEdition, BC001J4_n1168ParametrosSistema_SQLEdition, BC001J4_A1169ParametrosSistema_SQLInstance, BC001J4_n1169ParametrosSistema_SQLInstance, BC001J4_A1170ParametrosSistema_DBName, BC001J4_n1170ParametrosSistema_DBName, BC001J4_A1499ParametrosSistema_FlsEvd, BC001J4_n1499ParametrosSistema_FlsEvd, BC001J4_A1679ParametrosSistema_URLApp,
               BC001J4_n1679ParametrosSistema_URLApp, BC001J4_A1952ParametrosSistema_URLOtherVer, BC001J4_n1952ParametrosSistema_URLOtherVer, BC001J4_A1954ParametrosSistema_Validacao, BC001J4_n1954ParametrosSistema_Validacao, BC001J4_A40000ParametrosSistema_Imagem_GXI, BC001J4_n40000ParametrosSistema_Imagem_GXI, BC001J4_A2117ParametrosSistema_Imagem, BC001J4_n2117ParametrosSistema_Imagem
               }
               , new Object[] {
               BC001J5_A330ParametrosSistema_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC001J10_A330ParametrosSistema_Codigo, BC001J10_A331ParametrosSistema_NomeSistema, BC001J10_A334ParametrosSistema_AppID, BC001J10_A332ParametrosSistema_TextoHome, BC001J10_A399ParametrosSistema_LicensiadoCadastrado, BC001J10_n399ParametrosSistema_LicensiadoCadastrado, BC001J10_A417ParametrosSistema_PadronizarStrings, BC001J10_A532ParametrosSistema_EmailSdaHost, BC001J10_n532ParametrosSistema_EmailSdaHost, BC001J10_A533ParametrosSistema_EmailSdaUser,
               BC001J10_n533ParametrosSistema_EmailSdaUser, BC001J10_A534ParametrosSistema_EmailSdaPass, BC001J10_n534ParametrosSistema_EmailSdaPass, BC001J10_A537ParametrosSistema_EmailSdaKey, BC001J10_n537ParametrosSistema_EmailSdaKey, BC001J10_A535ParametrosSistema_EmailSdaAut, BC001J10_n535ParametrosSistema_EmailSdaAut, BC001J10_A536ParametrosSistema_EmailSdaPort, BC001J10_n536ParametrosSistema_EmailSdaPort, BC001J10_A2032ParametrosSistema_EmailSdaSec,
               BC001J10_n2032ParametrosSistema_EmailSdaSec, BC001J10_A708ParametrosSistema_FatorAjuste, BC001J10_n708ParametrosSistema_FatorAjuste, BC001J10_A1021ParametrosSistema_PathCrtf, BC001J10_n1021ParametrosSistema_PathCrtf, BC001J10_A1163ParametrosSistema_HostMensuracao, BC001J10_n1163ParametrosSistema_HostMensuracao, BC001J10_A1171ParametrosSistema_SQLComputerName, BC001J10_n1171ParametrosSistema_SQLComputerName, BC001J10_A1172ParametrosSistema_SQLServerName,
               BC001J10_n1172ParametrosSistema_SQLServerName, BC001J10_A1168ParametrosSistema_SQLEdition, BC001J10_n1168ParametrosSistema_SQLEdition, BC001J10_A1169ParametrosSistema_SQLInstance, BC001J10_n1169ParametrosSistema_SQLInstance, BC001J10_A1170ParametrosSistema_DBName, BC001J10_n1170ParametrosSistema_DBName, BC001J10_A1499ParametrosSistema_FlsEvd, BC001J10_n1499ParametrosSistema_FlsEvd, BC001J10_A1679ParametrosSistema_URLApp,
               BC001J10_n1679ParametrosSistema_URLApp, BC001J10_A1952ParametrosSistema_URLOtherVer, BC001J10_n1952ParametrosSistema_URLOtherVer, BC001J10_A1954ParametrosSistema_Validacao, BC001J10_n1954ParametrosSistema_Validacao, BC001J10_A40000ParametrosSistema_Imagem_GXI, BC001J10_n40000ParametrosSistema_Imagem_GXI, BC001J10_A2117ParametrosSistema_Imagem, BC001J10_n2117ParametrosSistema_Imagem
               }
            }
         );
         Z708ParametrosSistema_FatorAjuste = (decimal)(1);
         n708ParametrosSistema_FatorAjuste = false;
         A708ParametrosSistema_FatorAjuste = (decimal)(1);
         n708ParametrosSistema_FatorAjuste = false;
         i708ParametrosSistema_FatorAjuste = (decimal)(1);
         n708ParametrosSistema_FatorAjuste = false;
         Z535ParametrosSistema_EmailSdaAut = false;
         n535ParametrosSistema_EmailSdaAut = false;
         A535ParametrosSistema_EmailSdaAut = false;
         n535ParametrosSistema_EmailSdaAut = false;
         i535ParametrosSistema_EmailSdaAut = false;
         n535ParametrosSistema_EmailSdaAut = false;
         Z417ParametrosSistema_PadronizarStrings = true;
         A417ParametrosSistema_PadronizarStrings = true;
         i417ParametrosSistema_PadronizarStrings = true;
         Z399ParametrosSistema_LicensiadoCadastrado = false;
         n399ParametrosSistema_LicensiadoCadastrado = false;
         A399ParametrosSistema_LicensiadoCadastrado = false;
         n399ParametrosSistema_LicensiadoCadastrado = false;
         i399ParametrosSistema_LicensiadoCadastrado = false;
         n399ParametrosSistema_LicensiadoCadastrado = false;
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E121J2 */
         E121J2 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short Z536ParametrosSistema_EmailSdaPort ;
      private short A536ParametrosSistema_EmailSdaPort ;
      private short Z2032ParametrosSistema_EmailSdaSec ;
      private short A2032ParametrosSistema_EmailSdaSec ;
      private short Gx_BScreen ;
      private short RcdFound56 ;
      private int trnEnded ;
      private int Z330ParametrosSistema_Codigo ;
      private int A330ParametrosSistema_Codigo ;
      private long Z334ParametrosSistema_AppID ;
      private long A334ParametrosSistema_AppID ;
      private decimal Z708ParametrosSistema_FatorAjuste ;
      private decimal A708ParametrosSistema_FatorAjuste ;
      private decimal i708ParametrosSistema_FatorAjuste ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String Z331ParametrosSistema_NomeSistema ;
      private String A331ParametrosSistema_NomeSistema ;
      private String Z537ParametrosSistema_EmailSdaKey ;
      private String A537ParametrosSistema_EmailSdaKey ;
      private String sMode56 ;
      private bool Z399ParametrosSistema_LicensiadoCadastrado ;
      private bool A399ParametrosSistema_LicensiadoCadastrado ;
      private bool Z417ParametrosSistema_PadronizarStrings ;
      private bool A417ParametrosSistema_PadronizarStrings ;
      private bool Z535ParametrosSistema_EmailSdaAut ;
      private bool A535ParametrosSistema_EmailSdaAut ;
      private bool n708ParametrosSistema_FatorAjuste ;
      private bool n535ParametrosSistema_EmailSdaAut ;
      private bool n399ParametrosSistema_LicensiadoCadastrado ;
      private bool n532ParametrosSistema_EmailSdaHost ;
      private bool n533ParametrosSistema_EmailSdaUser ;
      private bool n534ParametrosSistema_EmailSdaPass ;
      private bool n537ParametrosSistema_EmailSdaKey ;
      private bool n536ParametrosSistema_EmailSdaPort ;
      private bool n2032ParametrosSistema_EmailSdaSec ;
      private bool n1021ParametrosSistema_PathCrtf ;
      private bool n1163ParametrosSistema_HostMensuracao ;
      private bool n1171ParametrosSistema_SQLComputerName ;
      private bool n1172ParametrosSistema_SQLServerName ;
      private bool n1168ParametrosSistema_SQLEdition ;
      private bool n1169ParametrosSistema_SQLInstance ;
      private bool n1170ParametrosSistema_DBName ;
      private bool n1499ParametrosSistema_FlsEvd ;
      private bool n1679ParametrosSistema_URLApp ;
      private bool n1952ParametrosSistema_URLOtherVer ;
      private bool n1954ParametrosSistema_Validacao ;
      private bool n40000ParametrosSistema_Imagem_GXI ;
      private bool n2117ParametrosSistema_Imagem ;
      private bool Gx_longc ;
      private bool i535ParametrosSistema_EmailSdaAut ;
      private bool i417ParametrosSistema_PadronizarStrings ;
      private bool i399ParametrosSistema_LicensiadoCadastrado ;
      private String Z332ParametrosSistema_TextoHome ;
      private String A332ParametrosSistema_TextoHome ;
      private String A1021ParametrosSistema_PathCrtf ;
      private String Z532ParametrosSistema_EmailSdaHost ;
      private String A532ParametrosSistema_EmailSdaHost ;
      private String Z533ParametrosSistema_EmailSdaUser ;
      private String A533ParametrosSistema_EmailSdaUser ;
      private String Z534ParametrosSistema_EmailSdaPass ;
      private String A534ParametrosSistema_EmailSdaPass ;
      private String Z1021ParametrosSistema_PathCrtf ;
      private String Z1163ParametrosSistema_HostMensuracao ;
      private String A1163ParametrosSistema_HostMensuracao ;
      private String Z1171ParametrosSistema_SQLComputerName ;
      private String A1171ParametrosSistema_SQLComputerName ;
      private String Z1172ParametrosSistema_SQLServerName ;
      private String A1172ParametrosSistema_SQLServerName ;
      private String Z1168ParametrosSistema_SQLEdition ;
      private String A1168ParametrosSistema_SQLEdition ;
      private String Z1169ParametrosSistema_SQLInstance ;
      private String A1169ParametrosSistema_SQLInstance ;
      private String Z1170ParametrosSistema_DBName ;
      private String A1170ParametrosSistema_DBName ;
      private String Z1499ParametrosSistema_FlsEvd ;
      private String A1499ParametrosSistema_FlsEvd ;
      private String Z1679ParametrosSistema_URLApp ;
      private String A1679ParametrosSistema_URLApp ;
      private String Z1952ParametrosSistema_URLOtherVer ;
      private String A1952ParametrosSistema_URLOtherVer ;
      private String Z1954ParametrosSistema_Validacao ;
      private String A1954ParametrosSistema_Validacao ;
      private String Z40000ParametrosSistema_Imagem_GXI ;
      private String A40000ParametrosSistema_Imagem_GXI ;
      private String Z2117ParametrosSistema_Imagem ;
      private String A2117ParametrosSistema_Imagem ;
      private IGxSession AV10WebSession ;
      private SdtParametrosSistema bcParametrosSistema ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC001J4_A330ParametrosSistema_Codigo ;
      private String[] BC001J4_A331ParametrosSistema_NomeSistema ;
      private long[] BC001J4_A334ParametrosSistema_AppID ;
      private String[] BC001J4_A332ParametrosSistema_TextoHome ;
      private bool[] BC001J4_A399ParametrosSistema_LicensiadoCadastrado ;
      private bool[] BC001J4_n399ParametrosSistema_LicensiadoCadastrado ;
      private bool[] BC001J4_A417ParametrosSistema_PadronizarStrings ;
      private String[] BC001J4_A532ParametrosSistema_EmailSdaHost ;
      private bool[] BC001J4_n532ParametrosSistema_EmailSdaHost ;
      private String[] BC001J4_A533ParametrosSistema_EmailSdaUser ;
      private bool[] BC001J4_n533ParametrosSistema_EmailSdaUser ;
      private String[] BC001J4_A534ParametrosSistema_EmailSdaPass ;
      private bool[] BC001J4_n534ParametrosSistema_EmailSdaPass ;
      private String[] BC001J4_A537ParametrosSistema_EmailSdaKey ;
      private bool[] BC001J4_n537ParametrosSistema_EmailSdaKey ;
      private bool[] BC001J4_A535ParametrosSistema_EmailSdaAut ;
      private bool[] BC001J4_n535ParametrosSistema_EmailSdaAut ;
      private short[] BC001J4_A536ParametrosSistema_EmailSdaPort ;
      private bool[] BC001J4_n536ParametrosSistema_EmailSdaPort ;
      private short[] BC001J4_A2032ParametrosSistema_EmailSdaSec ;
      private bool[] BC001J4_n2032ParametrosSistema_EmailSdaSec ;
      private decimal[] BC001J4_A708ParametrosSistema_FatorAjuste ;
      private bool[] BC001J4_n708ParametrosSistema_FatorAjuste ;
      private String[] BC001J4_A1021ParametrosSistema_PathCrtf ;
      private bool[] BC001J4_n1021ParametrosSistema_PathCrtf ;
      private String[] BC001J4_A1163ParametrosSistema_HostMensuracao ;
      private bool[] BC001J4_n1163ParametrosSistema_HostMensuracao ;
      private String[] BC001J4_A1171ParametrosSistema_SQLComputerName ;
      private bool[] BC001J4_n1171ParametrosSistema_SQLComputerName ;
      private String[] BC001J4_A1172ParametrosSistema_SQLServerName ;
      private bool[] BC001J4_n1172ParametrosSistema_SQLServerName ;
      private String[] BC001J4_A1168ParametrosSistema_SQLEdition ;
      private bool[] BC001J4_n1168ParametrosSistema_SQLEdition ;
      private String[] BC001J4_A1169ParametrosSistema_SQLInstance ;
      private bool[] BC001J4_n1169ParametrosSistema_SQLInstance ;
      private String[] BC001J4_A1170ParametrosSistema_DBName ;
      private bool[] BC001J4_n1170ParametrosSistema_DBName ;
      private String[] BC001J4_A1499ParametrosSistema_FlsEvd ;
      private bool[] BC001J4_n1499ParametrosSistema_FlsEvd ;
      private String[] BC001J4_A1679ParametrosSistema_URLApp ;
      private bool[] BC001J4_n1679ParametrosSistema_URLApp ;
      private String[] BC001J4_A1952ParametrosSistema_URLOtherVer ;
      private bool[] BC001J4_n1952ParametrosSistema_URLOtherVer ;
      private String[] BC001J4_A1954ParametrosSistema_Validacao ;
      private bool[] BC001J4_n1954ParametrosSistema_Validacao ;
      private String[] BC001J4_A40000ParametrosSistema_Imagem_GXI ;
      private bool[] BC001J4_n40000ParametrosSistema_Imagem_GXI ;
      private String[] BC001J4_A2117ParametrosSistema_Imagem ;
      private bool[] BC001J4_n2117ParametrosSistema_Imagem ;
      private int[] BC001J5_A330ParametrosSistema_Codigo ;
      private int[] BC001J3_A330ParametrosSistema_Codigo ;
      private String[] BC001J3_A331ParametrosSistema_NomeSistema ;
      private long[] BC001J3_A334ParametrosSistema_AppID ;
      private String[] BC001J3_A332ParametrosSistema_TextoHome ;
      private bool[] BC001J3_A399ParametrosSistema_LicensiadoCadastrado ;
      private bool[] BC001J3_n399ParametrosSistema_LicensiadoCadastrado ;
      private bool[] BC001J3_A417ParametrosSistema_PadronizarStrings ;
      private String[] BC001J3_A532ParametrosSistema_EmailSdaHost ;
      private bool[] BC001J3_n532ParametrosSistema_EmailSdaHost ;
      private String[] BC001J3_A533ParametrosSistema_EmailSdaUser ;
      private bool[] BC001J3_n533ParametrosSistema_EmailSdaUser ;
      private String[] BC001J3_A534ParametrosSistema_EmailSdaPass ;
      private bool[] BC001J3_n534ParametrosSistema_EmailSdaPass ;
      private String[] BC001J3_A537ParametrosSistema_EmailSdaKey ;
      private bool[] BC001J3_n537ParametrosSistema_EmailSdaKey ;
      private bool[] BC001J3_A535ParametrosSistema_EmailSdaAut ;
      private bool[] BC001J3_n535ParametrosSistema_EmailSdaAut ;
      private short[] BC001J3_A536ParametrosSistema_EmailSdaPort ;
      private bool[] BC001J3_n536ParametrosSistema_EmailSdaPort ;
      private short[] BC001J3_A2032ParametrosSistema_EmailSdaSec ;
      private bool[] BC001J3_n2032ParametrosSistema_EmailSdaSec ;
      private decimal[] BC001J3_A708ParametrosSistema_FatorAjuste ;
      private bool[] BC001J3_n708ParametrosSistema_FatorAjuste ;
      private String[] BC001J3_A1021ParametrosSistema_PathCrtf ;
      private bool[] BC001J3_n1021ParametrosSistema_PathCrtf ;
      private String[] BC001J3_A1163ParametrosSistema_HostMensuracao ;
      private bool[] BC001J3_n1163ParametrosSistema_HostMensuracao ;
      private String[] BC001J3_A1171ParametrosSistema_SQLComputerName ;
      private bool[] BC001J3_n1171ParametrosSistema_SQLComputerName ;
      private String[] BC001J3_A1172ParametrosSistema_SQLServerName ;
      private bool[] BC001J3_n1172ParametrosSistema_SQLServerName ;
      private String[] BC001J3_A1168ParametrosSistema_SQLEdition ;
      private bool[] BC001J3_n1168ParametrosSistema_SQLEdition ;
      private String[] BC001J3_A1169ParametrosSistema_SQLInstance ;
      private bool[] BC001J3_n1169ParametrosSistema_SQLInstance ;
      private String[] BC001J3_A1170ParametrosSistema_DBName ;
      private bool[] BC001J3_n1170ParametrosSistema_DBName ;
      private String[] BC001J3_A1499ParametrosSistema_FlsEvd ;
      private bool[] BC001J3_n1499ParametrosSistema_FlsEvd ;
      private String[] BC001J3_A1679ParametrosSistema_URLApp ;
      private bool[] BC001J3_n1679ParametrosSistema_URLApp ;
      private String[] BC001J3_A1952ParametrosSistema_URLOtherVer ;
      private bool[] BC001J3_n1952ParametrosSistema_URLOtherVer ;
      private String[] BC001J3_A1954ParametrosSistema_Validacao ;
      private bool[] BC001J3_n1954ParametrosSistema_Validacao ;
      private String[] BC001J3_A40000ParametrosSistema_Imagem_GXI ;
      private bool[] BC001J3_n40000ParametrosSistema_Imagem_GXI ;
      private String[] BC001J3_A2117ParametrosSistema_Imagem ;
      private bool[] BC001J3_n2117ParametrosSistema_Imagem ;
      private int[] BC001J2_A330ParametrosSistema_Codigo ;
      private String[] BC001J2_A331ParametrosSistema_NomeSistema ;
      private long[] BC001J2_A334ParametrosSistema_AppID ;
      private String[] BC001J2_A332ParametrosSistema_TextoHome ;
      private bool[] BC001J2_A399ParametrosSistema_LicensiadoCadastrado ;
      private bool[] BC001J2_n399ParametrosSistema_LicensiadoCadastrado ;
      private bool[] BC001J2_A417ParametrosSistema_PadronizarStrings ;
      private String[] BC001J2_A532ParametrosSistema_EmailSdaHost ;
      private bool[] BC001J2_n532ParametrosSistema_EmailSdaHost ;
      private String[] BC001J2_A533ParametrosSistema_EmailSdaUser ;
      private bool[] BC001J2_n533ParametrosSistema_EmailSdaUser ;
      private String[] BC001J2_A534ParametrosSistema_EmailSdaPass ;
      private bool[] BC001J2_n534ParametrosSistema_EmailSdaPass ;
      private String[] BC001J2_A537ParametrosSistema_EmailSdaKey ;
      private bool[] BC001J2_n537ParametrosSistema_EmailSdaKey ;
      private bool[] BC001J2_A535ParametrosSistema_EmailSdaAut ;
      private bool[] BC001J2_n535ParametrosSistema_EmailSdaAut ;
      private short[] BC001J2_A536ParametrosSistema_EmailSdaPort ;
      private bool[] BC001J2_n536ParametrosSistema_EmailSdaPort ;
      private short[] BC001J2_A2032ParametrosSistema_EmailSdaSec ;
      private bool[] BC001J2_n2032ParametrosSistema_EmailSdaSec ;
      private decimal[] BC001J2_A708ParametrosSistema_FatorAjuste ;
      private bool[] BC001J2_n708ParametrosSistema_FatorAjuste ;
      private String[] BC001J2_A1021ParametrosSistema_PathCrtf ;
      private bool[] BC001J2_n1021ParametrosSistema_PathCrtf ;
      private String[] BC001J2_A1163ParametrosSistema_HostMensuracao ;
      private bool[] BC001J2_n1163ParametrosSistema_HostMensuracao ;
      private String[] BC001J2_A1171ParametrosSistema_SQLComputerName ;
      private bool[] BC001J2_n1171ParametrosSistema_SQLComputerName ;
      private String[] BC001J2_A1172ParametrosSistema_SQLServerName ;
      private bool[] BC001J2_n1172ParametrosSistema_SQLServerName ;
      private String[] BC001J2_A1168ParametrosSistema_SQLEdition ;
      private bool[] BC001J2_n1168ParametrosSistema_SQLEdition ;
      private String[] BC001J2_A1169ParametrosSistema_SQLInstance ;
      private bool[] BC001J2_n1169ParametrosSistema_SQLInstance ;
      private String[] BC001J2_A1170ParametrosSistema_DBName ;
      private bool[] BC001J2_n1170ParametrosSistema_DBName ;
      private String[] BC001J2_A1499ParametrosSistema_FlsEvd ;
      private bool[] BC001J2_n1499ParametrosSistema_FlsEvd ;
      private String[] BC001J2_A1679ParametrosSistema_URLApp ;
      private bool[] BC001J2_n1679ParametrosSistema_URLApp ;
      private String[] BC001J2_A1952ParametrosSistema_URLOtherVer ;
      private bool[] BC001J2_n1952ParametrosSistema_URLOtherVer ;
      private String[] BC001J2_A1954ParametrosSistema_Validacao ;
      private bool[] BC001J2_n1954ParametrosSistema_Validacao ;
      private String[] BC001J2_A40000ParametrosSistema_Imagem_GXI ;
      private bool[] BC001J2_n40000ParametrosSistema_Imagem_GXI ;
      private String[] BC001J2_A2117ParametrosSistema_Imagem ;
      private bool[] BC001J2_n2117ParametrosSistema_Imagem ;
      private int[] BC001J10_A330ParametrosSistema_Codigo ;
      private String[] BC001J10_A331ParametrosSistema_NomeSistema ;
      private long[] BC001J10_A334ParametrosSistema_AppID ;
      private String[] BC001J10_A332ParametrosSistema_TextoHome ;
      private bool[] BC001J10_A399ParametrosSistema_LicensiadoCadastrado ;
      private bool[] BC001J10_n399ParametrosSistema_LicensiadoCadastrado ;
      private bool[] BC001J10_A417ParametrosSistema_PadronizarStrings ;
      private String[] BC001J10_A532ParametrosSistema_EmailSdaHost ;
      private bool[] BC001J10_n532ParametrosSistema_EmailSdaHost ;
      private String[] BC001J10_A533ParametrosSistema_EmailSdaUser ;
      private bool[] BC001J10_n533ParametrosSistema_EmailSdaUser ;
      private String[] BC001J10_A534ParametrosSistema_EmailSdaPass ;
      private bool[] BC001J10_n534ParametrosSistema_EmailSdaPass ;
      private String[] BC001J10_A537ParametrosSistema_EmailSdaKey ;
      private bool[] BC001J10_n537ParametrosSistema_EmailSdaKey ;
      private bool[] BC001J10_A535ParametrosSistema_EmailSdaAut ;
      private bool[] BC001J10_n535ParametrosSistema_EmailSdaAut ;
      private short[] BC001J10_A536ParametrosSistema_EmailSdaPort ;
      private bool[] BC001J10_n536ParametrosSistema_EmailSdaPort ;
      private short[] BC001J10_A2032ParametrosSistema_EmailSdaSec ;
      private bool[] BC001J10_n2032ParametrosSistema_EmailSdaSec ;
      private decimal[] BC001J10_A708ParametrosSistema_FatorAjuste ;
      private bool[] BC001J10_n708ParametrosSistema_FatorAjuste ;
      private String[] BC001J10_A1021ParametrosSistema_PathCrtf ;
      private bool[] BC001J10_n1021ParametrosSistema_PathCrtf ;
      private String[] BC001J10_A1163ParametrosSistema_HostMensuracao ;
      private bool[] BC001J10_n1163ParametrosSistema_HostMensuracao ;
      private String[] BC001J10_A1171ParametrosSistema_SQLComputerName ;
      private bool[] BC001J10_n1171ParametrosSistema_SQLComputerName ;
      private String[] BC001J10_A1172ParametrosSistema_SQLServerName ;
      private bool[] BC001J10_n1172ParametrosSistema_SQLServerName ;
      private String[] BC001J10_A1168ParametrosSistema_SQLEdition ;
      private bool[] BC001J10_n1168ParametrosSistema_SQLEdition ;
      private String[] BC001J10_A1169ParametrosSistema_SQLInstance ;
      private bool[] BC001J10_n1169ParametrosSistema_SQLInstance ;
      private String[] BC001J10_A1170ParametrosSistema_DBName ;
      private bool[] BC001J10_n1170ParametrosSistema_DBName ;
      private String[] BC001J10_A1499ParametrosSistema_FlsEvd ;
      private bool[] BC001J10_n1499ParametrosSistema_FlsEvd ;
      private String[] BC001J10_A1679ParametrosSistema_URLApp ;
      private bool[] BC001J10_n1679ParametrosSistema_URLApp ;
      private String[] BC001J10_A1952ParametrosSistema_URLOtherVer ;
      private bool[] BC001J10_n1952ParametrosSistema_URLOtherVer ;
      private String[] BC001J10_A1954ParametrosSistema_Validacao ;
      private bool[] BC001J10_n1954ParametrosSistema_Validacao ;
      private String[] BC001J10_A40000ParametrosSistema_Imagem_GXI ;
      private bool[] BC001J10_n40000ParametrosSistema_Imagem_GXI ;
      private String[] BC001J10_A2117ParametrosSistema_Imagem ;
      private bool[] BC001J10_n2117ParametrosSistema_Imagem ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class parametrossistema_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new UpdateCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new ForEachCursor(def[8])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC001J4 ;
          prmBC001J4 = new Object[] {
          new Object[] {"@ParametrosSistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001J5 ;
          prmBC001J5 = new Object[] {
          new Object[] {"@ParametrosSistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001J3 ;
          prmBC001J3 = new Object[] {
          new Object[] {"@ParametrosSistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001J2 ;
          prmBC001J2 = new Object[] {
          new Object[] {"@ParametrosSistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001J6 ;
          prmBC001J6 = new Object[] {
          new Object[] {"@ParametrosSistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ParametrosSistema_NomeSistema",SqlDbType.Char,50,0} ,
          new Object[] {"@ParametrosSistema_AppID",SqlDbType.Decimal,12,0} ,
          new Object[] {"@ParametrosSistema_TextoHome",SqlDbType.VarChar,20000,0} ,
          new Object[] {"@ParametrosSistema_LicensiadoCadastrado",SqlDbType.Bit,4,0} ,
          new Object[] {"@ParametrosSistema_PadronizarStrings",SqlDbType.Bit,4,0} ,
          new Object[] {"@ParametrosSistema_EmailSdaHost",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ParametrosSistema_EmailSdaUser",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ParametrosSistema_EmailSdaPass",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ParametrosSistema_EmailSdaKey",SqlDbType.Char,32,0} ,
          new Object[] {"@ParametrosSistema_EmailSdaAut",SqlDbType.Bit,4,0} ,
          new Object[] {"@ParametrosSistema_EmailSdaPort",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ParametrosSistema_EmailSdaSec",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ParametrosSistema_FatorAjuste",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ParametrosSistema_PathCrtf",SqlDbType.VarChar,100,0} ,
          new Object[] {"@ParametrosSistema_HostMensuracao",SqlDbType.VarChar,255,0} ,
          new Object[] {"@ParametrosSistema_SQLComputerName",SqlDbType.VarChar,60,0} ,
          new Object[] {"@ParametrosSistema_SQLServerName",SqlDbType.VarChar,60,0} ,
          new Object[] {"@ParametrosSistema_SQLEdition",SqlDbType.VarChar,60,0} ,
          new Object[] {"@ParametrosSistema_SQLInstance",SqlDbType.VarChar,60,0} ,
          new Object[] {"@ParametrosSistema_DBName",SqlDbType.VarChar,60,0} ,
          new Object[] {"@ParametrosSistema_FlsEvd",SqlDbType.VarChar,60,0} ,
          new Object[] {"@ParametrosSistema_URLApp",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@ParametrosSistema_URLOtherVer",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@ParametrosSistema_Validacao",SqlDbType.VarChar,80,0} ,
          new Object[] {"@ParametrosSistema_Imagem",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ParametrosSistema_Imagem_GXI",SqlDbType.VarChar,2048,0}
          } ;
          Object[] prmBC001J7 ;
          prmBC001J7 = new Object[] {
          new Object[] {"@ParametrosSistema_NomeSistema",SqlDbType.Char,50,0} ,
          new Object[] {"@ParametrosSistema_AppID",SqlDbType.Decimal,12,0} ,
          new Object[] {"@ParametrosSistema_TextoHome",SqlDbType.VarChar,20000,0} ,
          new Object[] {"@ParametrosSistema_LicensiadoCadastrado",SqlDbType.Bit,4,0} ,
          new Object[] {"@ParametrosSistema_PadronizarStrings",SqlDbType.Bit,4,0} ,
          new Object[] {"@ParametrosSistema_EmailSdaHost",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ParametrosSistema_EmailSdaUser",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ParametrosSistema_EmailSdaPass",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ParametrosSistema_EmailSdaKey",SqlDbType.Char,32,0} ,
          new Object[] {"@ParametrosSistema_EmailSdaAut",SqlDbType.Bit,4,0} ,
          new Object[] {"@ParametrosSistema_EmailSdaPort",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ParametrosSistema_EmailSdaSec",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ParametrosSistema_FatorAjuste",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ParametrosSistema_PathCrtf",SqlDbType.VarChar,100,0} ,
          new Object[] {"@ParametrosSistema_HostMensuracao",SqlDbType.VarChar,255,0} ,
          new Object[] {"@ParametrosSistema_SQLComputerName",SqlDbType.VarChar,60,0} ,
          new Object[] {"@ParametrosSistema_SQLServerName",SqlDbType.VarChar,60,0} ,
          new Object[] {"@ParametrosSistema_SQLEdition",SqlDbType.VarChar,60,0} ,
          new Object[] {"@ParametrosSistema_SQLInstance",SqlDbType.VarChar,60,0} ,
          new Object[] {"@ParametrosSistema_DBName",SqlDbType.VarChar,60,0} ,
          new Object[] {"@ParametrosSistema_FlsEvd",SqlDbType.VarChar,60,0} ,
          new Object[] {"@ParametrosSistema_URLApp",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@ParametrosSistema_URLOtherVer",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@ParametrosSistema_Validacao",SqlDbType.VarChar,80,0} ,
          new Object[] {"@ParametrosSistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001J8 ;
          prmBC001J8 = new Object[] {
          new Object[] {"@ParametrosSistema_Imagem",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ParametrosSistema_Imagem_GXI",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@ParametrosSistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001J9 ;
          prmBC001J9 = new Object[] {
          new Object[] {"@ParametrosSistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001J10 ;
          prmBC001J10 = new Object[] {
          new Object[] {"@ParametrosSistema_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC001J2", "SELECT [ParametrosSistema_Codigo], [ParametrosSistema_NomeSistema], [ParametrosSistema_AppID], [ParametrosSistema_TextoHome], [ParametrosSistema_LicensiadoCadastrado], [ParametrosSistema_PadronizarStrings], [ParametrosSistema_EmailSdaHost], [ParametrosSistema_EmailSdaUser], [ParametrosSistema_EmailSdaPass], [ParametrosSistema_EmailSdaKey], [ParametrosSistema_EmailSdaAut], [ParametrosSistema_EmailSdaPort], [ParametrosSistema_EmailSdaSec], [ParametrosSistema_FatorAjuste], [ParametrosSistema_PathCrtf], [ParametrosSistema_HostMensuracao], [ParametrosSistema_SQLComputerName], [ParametrosSistema_SQLServerName], [ParametrosSistema_SQLEdition], [ParametrosSistema_SQLInstance], [ParametrosSistema_DBName], [ParametrosSistema_FlsEvd], [ParametrosSistema_URLApp], [ParametrosSistema_URLOtherVer], [ParametrosSistema_Validacao], [ParametrosSistema_Imagem_GXI], [ParametrosSistema_Imagem] FROM [ParametrosSistema] WITH (UPDLOCK) WHERE [ParametrosSistema_Codigo] = @ParametrosSistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001J2,1,0,true,false )
             ,new CursorDef("BC001J3", "SELECT [ParametrosSistema_Codigo], [ParametrosSistema_NomeSistema], [ParametrosSistema_AppID], [ParametrosSistema_TextoHome], [ParametrosSistema_LicensiadoCadastrado], [ParametrosSistema_PadronizarStrings], [ParametrosSistema_EmailSdaHost], [ParametrosSistema_EmailSdaUser], [ParametrosSistema_EmailSdaPass], [ParametrosSistema_EmailSdaKey], [ParametrosSistema_EmailSdaAut], [ParametrosSistema_EmailSdaPort], [ParametrosSistema_EmailSdaSec], [ParametrosSistema_FatorAjuste], [ParametrosSistema_PathCrtf], [ParametrosSistema_HostMensuracao], [ParametrosSistema_SQLComputerName], [ParametrosSistema_SQLServerName], [ParametrosSistema_SQLEdition], [ParametrosSistema_SQLInstance], [ParametrosSistema_DBName], [ParametrosSistema_FlsEvd], [ParametrosSistema_URLApp], [ParametrosSistema_URLOtherVer], [ParametrosSistema_Validacao], [ParametrosSistema_Imagem_GXI], [ParametrosSistema_Imagem] FROM [ParametrosSistema] WITH (NOLOCK) WHERE [ParametrosSistema_Codigo] = @ParametrosSistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001J3,1,0,true,false )
             ,new CursorDef("BC001J4", "SELECT TM1.[ParametrosSistema_Codigo], TM1.[ParametrosSistema_NomeSistema], TM1.[ParametrosSistema_AppID], TM1.[ParametrosSistema_TextoHome], TM1.[ParametrosSistema_LicensiadoCadastrado], TM1.[ParametrosSistema_PadronizarStrings], TM1.[ParametrosSistema_EmailSdaHost], TM1.[ParametrosSistema_EmailSdaUser], TM1.[ParametrosSistema_EmailSdaPass], TM1.[ParametrosSistema_EmailSdaKey], TM1.[ParametrosSistema_EmailSdaAut], TM1.[ParametrosSistema_EmailSdaPort], TM1.[ParametrosSistema_EmailSdaSec], TM1.[ParametrosSistema_FatorAjuste], TM1.[ParametrosSistema_PathCrtf], TM1.[ParametrosSistema_HostMensuracao], TM1.[ParametrosSistema_SQLComputerName], TM1.[ParametrosSistema_SQLServerName], TM1.[ParametrosSistema_SQLEdition], TM1.[ParametrosSistema_SQLInstance], TM1.[ParametrosSistema_DBName], TM1.[ParametrosSistema_FlsEvd], TM1.[ParametrosSistema_URLApp], TM1.[ParametrosSistema_URLOtherVer], TM1.[ParametrosSistema_Validacao], TM1.[ParametrosSistema_Imagem_GXI], TM1.[ParametrosSistema_Imagem] FROM [ParametrosSistema] TM1 WITH (NOLOCK) WHERE TM1.[ParametrosSistema_Codigo] = @ParametrosSistema_Codigo ORDER BY TM1.[ParametrosSistema_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001J4,100,0,true,false )
             ,new CursorDef("BC001J5", "SELECT [ParametrosSistema_Codigo] FROM [ParametrosSistema] WITH (NOLOCK) WHERE [ParametrosSistema_Codigo] = @ParametrosSistema_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001J5,1,0,true,false )
             ,new CursorDef("BC001J6", "INSERT INTO [ParametrosSistema]([ParametrosSistema_Codigo], [ParametrosSistema_NomeSistema], [ParametrosSistema_AppID], [ParametrosSistema_TextoHome], [ParametrosSistema_LicensiadoCadastrado], [ParametrosSistema_PadronizarStrings], [ParametrosSistema_EmailSdaHost], [ParametrosSistema_EmailSdaUser], [ParametrosSistema_EmailSdaPass], [ParametrosSistema_EmailSdaKey], [ParametrosSistema_EmailSdaAut], [ParametrosSistema_EmailSdaPort], [ParametrosSistema_EmailSdaSec], [ParametrosSistema_FatorAjuste], [ParametrosSistema_PathCrtf], [ParametrosSistema_HostMensuracao], [ParametrosSistema_SQLComputerName], [ParametrosSistema_SQLServerName], [ParametrosSistema_SQLEdition], [ParametrosSistema_SQLInstance], [ParametrosSistema_DBName], [ParametrosSistema_FlsEvd], [ParametrosSistema_URLApp], [ParametrosSistema_URLOtherVer], [ParametrosSistema_Validacao], [ParametrosSistema_Imagem], [ParametrosSistema_Imagem_GXI]) VALUES(@ParametrosSistema_Codigo, @ParametrosSistema_NomeSistema, @ParametrosSistema_AppID, @ParametrosSistema_TextoHome, @ParametrosSistema_LicensiadoCadastrado, @ParametrosSistema_PadronizarStrings, @ParametrosSistema_EmailSdaHost, @ParametrosSistema_EmailSdaUser, @ParametrosSistema_EmailSdaPass, @ParametrosSistema_EmailSdaKey, @ParametrosSistema_EmailSdaAut, @ParametrosSistema_EmailSdaPort, @ParametrosSistema_EmailSdaSec, @ParametrosSistema_FatorAjuste, @ParametrosSistema_PathCrtf, @ParametrosSistema_HostMensuracao, @ParametrosSistema_SQLComputerName, @ParametrosSistema_SQLServerName, @ParametrosSistema_SQLEdition, @ParametrosSistema_SQLInstance, @ParametrosSistema_DBName, @ParametrosSistema_FlsEvd, @ParametrosSistema_URLApp, @ParametrosSistema_URLOtherVer, @ParametrosSistema_Validacao, @ParametrosSistema_Imagem, @ParametrosSistema_Imagem_GXI)", GxErrorMask.GX_NOMASK,prmBC001J6)
             ,new CursorDef("BC001J7", "UPDATE [ParametrosSistema] SET [ParametrosSistema_NomeSistema]=@ParametrosSistema_NomeSistema, [ParametrosSistema_AppID]=@ParametrosSistema_AppID, [ParametrosSistema_TextoHome]=@ParametrosSistema_TextoHome, [ParametrosSistema_LicensiadoCadastrado]=@ParametrosSistema_LicensiadoCadastrado, [ParametrosSistema_PadronizarStrings]=@ParametrosSistema_PadronizarStrings, [ParametrosSistema_EmailSdaHost]=@ParametrosSistema_EmailSdaHost, [ParametrosSistema_EmailSdaUser]=@ParametrosSistema_EmailSdaUser, [ParametrosSistema_EmailSdaPass]=@ParametrosSistema_EmailSdaPass, [ParametrosSistema_EmailSdaKey]=@ParametrosSistema_EmailSdaKey, [ParametrosSistema_EmailSdaAut]=@ParametrosSistema_EmailSdaAut, [ParametrosSistema_EmailSdaPort]=@ParametrosSistema_EmailSdaPort, [ParametrosSistema_EmailSdaSec]=@ParametrosSistema_EmailSdaSec, [ParametrosSistema_FatorAjuste]=@ParametrosSistema_FatorAjuste, [ParametrosSistema_PathCrtf]=@ParametrosSistema_PathCrtf, [ParametrosSistema_HostMensuracao]=@ParametrosSistema_HostMensuracao, [ParametrosSistema_SQLComputerName]=@ParametrosSistema_SQLComputerName, [ParametrosSistema_SQLServerName]=@ParametrosSistema_SQLServerName, [ParametrosSistema_SQLEdition]=@ParametrosSistema_SQLEdition, [ParametrosSistema_SQLInstance]=@ParametrosSistema_SQLInstance, [ParametrosSistema_DBName]=@ParametrosSistema_DBName, [ParametrosSistema_FlsEvd]=@ParametrosSistema_FlsEvd, [ParametrosSistema_URLApp]=@ParametrosSistema_URLApp, [ParametrosSistema_URLOtherVer]=@ParametrosSistema_URLOtherVer, [ParametrosSistema_Validacao]=@ParametrosSistema_Validacao  WHERE [ParametrosSistema_Codigo] = @ParametrosSistema_Codigo", GxErrorMask.GX_NOMASK,prmBC001J7)
             ,new CursorDef("BC001J8", "UPDATE [ParametrosSistema] SET [ParametrosSistema_Imagem]=@ParametrosSistema_Imagem, [ParametrosSistema_Imagem_GXI]=@ParametrosSistema_Imagem_GXI  WHERE [ParametrosSistema_Codigo] = @ParametrosSistema_Codigo", GxErrorMask.GX_NOMASK,prmBC001J8)
             ,new CursorDef("BC001J9", "DELETE FROM [ParametrosSistema]  WHERE [ParametrosSistema_Codigo] = @ParametrosSistema_Codigo", GxErrorMask.GX_NOMASK,prmBC001J9)
             ,new CursorDef("BC001J10", "SELECT TM1.[ParametrosSistema_Codigo], TM1.[ParametrosSistema_NomeSistema], TM1.[ParametrosSistema_AppID], TM1.[ParametrosSistema_TextoHome], TM1.[ParametrosSistema_LicensiadoCadastrado], TM1.[ParametrosSistema_PadronizarStrings], TM1.[ParametrosSistema_EmailSdaHost], TM1.[ParametrosSistema_EmailSdaUser], TM1.[ParametrosSistema_EmailSdaPass], TM1.[ParametrosSistema_EmailSdaKey], TM1.[ParametrosSistema_EmailSdaAut], TM1.[ParametrosSistema_EmailSdaPort], TM1.[ParametrosSistema_EmailSdaSec], TM1.[ParametrosSistema_FatorAjuste], TM1.[ParametrosSistema_PathCrtf], TM1.[ParametrosSistema_HostMensuracao], TM1.[ParametrosSistema_SQLComputerName], TM1.[ParametrosSistema_SQLServerName], TM1.[ParametrosSistema_SQLEdition], TM1.[ParametrosSistema_SQLInstance], TM1.[ParametrosSistema_DBName], TM1.[ParametrosSistema_FlsEvd], TM1.[ParametrosSistema_URLApp], TM1.[ParametrosSistema_URLOtherVer], TM1.[ParametrosSistema_Validacao], TM1.[ParametrosSistema_Imagem_GXI], TM1.[ParametrosSistema_Imagem] FROM [ParametrosSistema] TM1 WITH (NOLOCK) WHERE TM1.[ParametrosSistema_Codigo] = @ParametrosSistema_Codigo ORDER BY TM1.[ParametrosSistema_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001J10,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.getBool(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((bool[]) buf[6])[0] = rslt.getBool(6) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((String[]) buf[9])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((String[]) buf[11])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getString(10, 32) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((bool[]) buf[15])[0] = rslt.getBool(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((short[]) buf[17])[0] = rslt.getShort(12) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((short[]) buf[19])[0] = rslt.getShort(13) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((decimal[]) buf[21])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(14);
                ((String[]) buf[23])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                ((String[]) buf[25])[0] = rslt.getVarchar(16) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(16);
                ((String[]) buf[27])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((String[]) buf[29])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((String[]) buf[31])[0] = rslt.getVarchar(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((String[]) buf[33])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(20);
                ((String[]) buf[35])[0] = rslt.getVarchar(21) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(21);
                ((String[]) buf[37])[0] = rslt.getVarchar(22) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(22);
                ((String[]) buf[39])[0] = rslt.getVarchar(23) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(23);
                ((String[]) buf[41])[0] = rslt.getVarchar(24) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(24);
                ((String[]) buf[43])[0] = rslt.getVarchar(25) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(25);
                ((String[]) buf[45])[0] = rslt.getMultimediaUri(26) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(26);
                ((String[]) buf[47])[0] = rslt.getMultimediaFile(27, rslt.getVarchar(26)) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(27);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.getBool(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((bool[]) buf[6])[0] = rslt.getBool(6) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((String[]) buf[9])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((String[]) buf[11])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getString(10, 32) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((bool[]) buf[15])[0] = rslt.getBool(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((short[]) buf[17])[0] = rslt.getShort(12) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((short[]) buf[19])[0] = rslt.getShort(13) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((decimal[]) buf[21])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(14);
                ((String[]) buf[23])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                ((String[]) buf[25])[0] = rslt.getVarchar(16) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(16);
                ((String[]) buf[27])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((String[]) buf[29])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((String[]) buf[31])[0] = rslt.getVarchar(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((String[]) buf[33])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(20);
                ((String[]) buf[35])[0] = rslt.getVarchar(21) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(21);
                ((String[]) buf[37])[0] = rslt.getVarchar(22) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(22);
                ((String[]) buf[39])[0] = rslt.getVarchar(23) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(23);
                ((String[]) buf[41])[0] = rslt.getVarchar(24) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(24);
                ((String[]) buf[43])[0] = rslt.getVarchar(25) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(25);
                ((String[]) buf[45])[0] = rslt.getMultimediaUri(26) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(26);
                ((String[]) buf[47])[0] = rslt.getMultimediaFile(27, rslt.getVarchar(26)) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(27);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.getBool(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((bool[]) buf[6])[0] = rslt.getBool(6) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((String[]) buf[9])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((String[]) buf[11])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getString(10, 32) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((bool[]) buf[15])[0] = rslt.getBool(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((short[]) buf[17])[0] = rslt.getShort(12) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((short[]) buf[19])[0] = rslt.getShort(13) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((decimal[]) buf[21])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(14);
                ((String[]) buf[23])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                ((String[]) buf[25])[0] = rslt.getVarchar(16) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(16);
                ((String[]) buf[27])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((String[]) buf[29])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((String[]) buf[31])[0] = rslt.getVarchar(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((String[]) buf[33])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(20);
                ((String[]) buf[35])[0] = rslt.getVarchar(21) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(21);
                ((String[]) buf[37])[0] = rslt.getVarchar(22) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(22);
                ((String[]) buf[39])[0] = rslt.getVarchar(23) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(23);
                ((String[]) buf[41])[0] = rslt.getVarchar(24) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(24);
                ((String[]) buf[43])[0] = rslt.getVarchar(25) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(25);
                ((String[]) buf[45])[0] = rslt.getMultimediaUri(26) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(26);
                ((String[]) buf[47])[0] = rslt.getMultimediaFile(27, rslt.getVarchar(26)) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(27);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.getBool(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((bool[]) buf[6])[0] = rslt.getBool(6) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((String[]) buf[9])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((String[]) buf[11])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getString(10, 32) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((bool[]) buf[15])[0] = rslt.getBool(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((short[]) buf[17])[0] = rslt.getShort(12) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((short[]) buf[19])[0] = rslt.getShort(13) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((decimal[]) buf[21])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(14);
                ((String[]) buf[23])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                ((String[]) buf[25])[0] = rslt.getVarchar(16) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(16);
                ((String[]) buf[27])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((String[]) buf[29])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((String[]) buf[31])[0] = rslt.getVarchar(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((String[]) buf[33])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(20);
                ((String[]) buf[35])[0] = rslt.getVarchar(21) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(21);
                ((String[]) buf[37])[0] = rslt.getVarchar(22) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(22);
                ((String[]) buf[39])[0] = rslt.getVarchar(23) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(23);
                ((String[]) buf[41])[0] = rslt.getVarchar(24) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(24);
                ((String[]) buf[43])[0] = rslt.getVarchar(25) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(25);
                ((String[]) buf[45])[0] = rslt.getMultimediaUri(26) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(26);
                ((String[]) buf[47])[0] = rslt.getMultimediaFile(27, rslt.getVarchar(26)) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(27);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (long)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 5 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(5, (bool)parms[5]);
                }
                stmt.SetParameter(6, (bool)parms[6]);
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 7 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 10 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 11 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(11, (bool)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 12 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(12, (short)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 13 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(13, (short)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 14 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(14, (decimal)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 15 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(15, (String)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 16 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(16, (String)parms[26]);
                }
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 17 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(17, (String)parms[28]);
                }
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 18 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(18, (String)parms[30]);
                }
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 19 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(19, (String)parms[32]);
                }
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 20 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(20, (String)parms[34]);
                }
                if ( (bool)parms[35] )
                {
                   stmt.setNull( 21 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(21, (String)parms[36]);
                }
                if ( (bool)parms[37] )
                {
                   stmt.setNull( 22 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(22, (String)parms[38]);
                }
                if ( (bool)parms[39] )
                {
                   stmt.setNull( 23 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(23, (String)parms[40]);
                }
                if ( (bool)parms[41] )
                {
                   stmt.setNull( 24 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(24, (String)parms[42]);
                }
                if ( (bool)parms[43] )
                {
                   stmt.setNull( 25 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(25, (String)parms[44]);
                }
                if ( (bool)parms[45] )
                {
                   stmt.setNull( 26 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(26, (String)parms[46]);
                }
                if ( (bool)parms[47] )
                {
                   stmt.setNull( 27 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameterMultimedia(27, (String)parms[48], (String)parms[46]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (long)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(4, (bool)parms[4]);
                }
                stmt.SetParameter(5, (bool)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 7 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 9 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 10 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(10, (bool)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 11 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(11, (short)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 12 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(12, (short)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 13 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(13, (decimal)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 14 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 15 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(15, (String)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 16 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(16, (String)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 17 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(17, (String)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 18 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(18, (String)parms[31]);
                }
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 19 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(19, (String)parms[33]);
                }
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 20 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(20, (String)parms[35]);
                }
                if ( (bool)parms[36] )
                {
                   stmt.setNull( 21 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(21, (String)parms[37]);
                }
                if ( (bool)parms[38] )
                {
                   stmt.setNull( 22 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(22, (String)parms[39]);
                }
                if ( (bool)parms[40] )
                {
                   stmt.setNull( 23 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(23, (String)parms[41]);
                }
                if ( (bool)parms[42] )
                {
                   stmt.setNull( 24 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(24, (String)parms[43]);
                }
                stmt.SetParameter(25, (int)parms[44]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameterMultimedia(2, (String)parms[3], (String)parms[1]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
