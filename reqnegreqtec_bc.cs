/*
               File: ReqNegReqTec_BC
        Description: Req Neg Req Tec
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:9:48.60
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class reqnegreqtec_bc : GXHttpHandler, IGxSilentTrn
   {
      public reqnegreqtec_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public reqnegreqtec_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow4W218( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey4W218( ) ;
         standaloneModal( ) ;
         AddRow4W218( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z1895SolicServicoReqNeg_Codigo = A1895SolicServicoReqNeg_Codigo;
               Z1919Requisito_Codigo = A1919Requisito_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_4W0( )
      {
         BeforeValidate4W218( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls4W218( ) ;
            }
            else
            {
               CheckExtendedTable4W218( ) ;
               if ( AnyError == 0 )
               {
                  ZM4W218( 2) ;
                  ZM4W218( 3) ;
               }
               CloseExtendedTableCursors4W218( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM4W218( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            Z1945ReqNegReqTec_PropostaCod = A1945ReqNegReqTec_PropostaCod;
            Z1946ReqNegReqTec_OSCod = A1946ReqNegReqTec_OSCod;
         }
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            Z1931Requisito_Ordem = A1931Requisito_Ordem;
         }
         if ( GX_JID == -1 )
         {
            Z1945ReqNegReqTec_PropostaCod = A1945ReqNegReqTec_PropostaCod;
            Z1946ReqNegReqTec_OSCod = A1946ReqNegReqTec_OSCod;
            Z1895SolicServicoReqNeg_Codigo = A1895SolicServicoReqNeg_Codigo;
            Z1919Requisito_Codigo = A1919Requisito_Codigo;
            Z1931Requisito_Ordem = A1931Requisito_Ordem;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load4W218( )
      {
         /* Using cursor BC004W6 */
         pr_default.execute(4, new Object[] {A1895SolicServicoReqNeg_Codigo, A1919Requisito_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound218 = 1;
            A1931Requisito_Ordem = BC004W6_A1931Requisito_Ordem[0];
            n1931Requisito_Ordem = BC004W6_n1931Requisito_Ordem[0];
            A1945ReqNegReqTec_PropostaCod = BC004W6_A1945ReqNegReqTec_PropostaCod[0];
            n1945ReqNegReqTec_PropostaCod = BC004W6_n1945ReqNegReqTec_PropostaCod[0];
            A1946ReqNegReqTec_OSCod = BC004W6_A1946ReqNegReqTec_OSCod[0];
            n1946ReqNegReqTec_OSCod = BC004W6_n1946ReqNegReqTec_OSCod[0];
            ZM4W218( -1) ;
         }
         pr_default.close(4);
         OnLoadActions4W218( ) ;
      }

      protected void OnLoadActions4W218( )
      {
      }

      protected void CheckExtendedTable4W218( )
      {
         standaloneModal( ) ;
         /* Using cursor BC004W4 */
         pr_default.execute(2, new Object[] {A1895SolicServicoReqNeg_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Requisitos de Neg�cio da Solicita��o de Servi�o'.", "ForeignKeyNotFound", 1, "SOLICSERVICOREQNEG_CODIGO");
            AnyError = 1;
         }
         pr_default.close(2);
         /* Using cursor BC004W5 */
         pr_default.execute(3, new Object[] {A1919Requisito_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe ' T213'.", "ForeignKeyNotFound", 1, "REQUISITO_CODIGO");
            AnyError = 1;
         }
         A1931Requisito_Ordem = BC004W5_A1931Requisito_Ordem[0];
         n1931Requisito_Ordem = BC004W5_n1931Requisito_Ordem[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors4W218( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey4W218( )
      {
         /* Using cursor BC004W7 */
         pr_default.execute(5, new Object[] {A1895SolicServicoReqNeg_Codigo, A1919Requisito_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound218 = 1;
         }
         else
         {
            RcdFound218 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC004W3 */
         pr_default.execute(1, new Object[] {A1895SolicServicoReqNeg_Codigo, A1919Requisito_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4W218( 1) ;
            RcdFound218 = 1;
            A1945ReqNegReqTec_PropostaCod = BC004W3_A1945ReqNegReqTec_PropostaCod[0];
            n1945ReqNegReqTec_PropostaCod = BC004W3_n1945ReqNegReqTec_PropostaCod[0];
            A1946ReqNegReqTec_OSCod = BC004W3_A1946ReqNegReqTec_OSCod[0];
            n1946ReqNegReqTec_OSCod = BC004W3_n1946ReqNegReqTec_OSCod[0];
            A1895SolicServicoReqNeg_Codigo = BC004W3_A1895SolicServicoReqNeg_Codigo[0];
            A1919Requisito_Codigo = BC004W3_A1919Requisito_Codigo[0];
            Z1895SolicServicoReqNeg_Codigo = A1895SolicServicoReqNeg_Codigo;
            Z1919Requisito_Codigo = A1919Requisito_Codigo;
            sMode218 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load4W218( ) ;
            if ( AnyError == 1 )
            {
               RcdFound218 = 0;
               InitializeNonKey4W218( ) ;
            }
            Gx_mode = sMode218;
         }
         else
         {
            RcdFound218 = 0;
            InitializeNonKey4W218( ) ;
            sMode218 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode218;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4W218( ) ;
         if ( RcdFound218 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_4W0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency4W218( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC004W2 */
            pr_default.execute(0, new Object[] {A1895SolicServicoReqNeg_Codigo, A1919Requisito_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ReqNegReqTec"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1945ReqNegReqTec_PropostaCod != BC004W2_A1945ReqNegReqTec_PropostaCod[0] ) || ( Z1946ReqNegReqTec_OSCod != BC004W2_A1946ReqNegReqTec_OSCod[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ReqNegReqTec"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4W218( )
      {
         BeforeValidate4W218( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4W218( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4W218( 0) ;
            CheckOptimisticConcurrency4W218( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4W218( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4W218( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC004W8 */
                     pr_default.execute(6, new Object[] {n1945ReqNegReqTec_PropostaCod, A1945ReqNegReqTec_PropostaCod, n1946ReqNegReqTec_OSCod, A1946ReqNegReqTec_OSCod, A1895SolicServicoReqNeg_Codigo, A1919Requisito_Codigo});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("ReqNegReqTec") ;
                     if ( (pr_default.getStatus(6) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4W218( ) ;
            }
            EndLevel4W218( ) ;
         }
         CloseExtendedTableCursors4W218( ) ;
      }

      protected void Update4W218( )
      {
         BeforeValidate4W218( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4W218( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4W218( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4W218( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4W218( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC004W9 */
                     pr_default.execute(7, new Object[] {n1945ReqNegReqTec_PropostaCod, A1945ReqNegReqTec_PropostaCod, n1946ReqNegReqTec_OSCod, A1946ReqNegReqTec_OSCod, A1895SolicServicoReqNeg_Codigo, A1919Requisito_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("ReqNegReqTec") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ReqNegReqTec"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate4W218( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4W218( ) ;
         }
         CloseExtendedTableCursors4W218( ) ;
      }

      protected void DeferredUpdate4W218( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate4W218( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4W218( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4W218( ) ;
            AfterConfirm4W218( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4W218( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC004W10 */
                  pr_default.execute(8, new Object[] {A1895SolicServicoReqNeg_Codigo, A1919Requisito_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("ReqNegReqTec") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode218 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel4W218( ) ;
         Gx_mode = sMode218;
      }

      protected void OnDeleteControls4W218( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC004W11 */
            pr_default.execute(9, new Object[] {A1919Requisito_Codigo});
            A1931Requisito_Ordem = BC004W11_A1931Requisito_Ordem[0];
            n1931Requisito_Ordem = BC004W11_n1931Requisito_Ordem[0];
            pr_default.close(9);
         }
      }

      protected void EndLevel4W218( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4W218( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart4W218( )
      {
         /* Using cursor BC004W12 */
         pr_default.execute(10, new Object[] {A1895SolicServicoReqNeg_Codigo, A1919Requisito_Codigo});
         RcdFound218 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound218 = 1;
            A1931Requisito_Ordem = BC004W12_A1931Requisito_Ordem[0];
            n1931Requisito_Ordem = BC004W12_n1931Requisito_Ordem[0];
            A1945ReqNegReqTec_PropostaCod = BC004W12_A1945ReqNegReqTec_PropostaCod[0];
            n1945ReqNegReqTec_PropostaCod = BC004W12_n1945ReqNegReqTec_PropostaCod[0];
            A1946ReqNegReqTec_OSCod = BC004W12_A1946ReqNegReqTec_OSCod[0];
            n1946ReqNegReqTec_OSCod = BC004W12_n1946ReqNegReqTec_OSCod[0];
            A1895SolicServicoReqNeg_Codigo = BC004W12_A1895SolicServicoReqNeg_Codigo[0];
            A1919Requisito_Codigo = BC004W12_A1919Requisito_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext4W218( )
      {
         /* Scan next routine */
         pr_default.readNext(10);
         RcdFound218 = 0;
         ScanKeyLoad4W218( ) ;
      }

      protected void ScanKeyLoad4W218( )
      {
         sMode218 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound218 = 1;
            A1931Requisito_Ordem = BC004W12_A1931Requisito_Ordem[0];
            n1931Requisito_Ordem = BC004W12_n1931Requisito_Ordem[0];
            A1945ReqNegReqTec_PropostaCod = BC004W12_A1945ReqNegReqTec_PropostaCod[0];
            n1945ReqNegReqTec_PropostaCod = BC004W12_n1945ReqNegReqTec_PropostaCod[0];
            A1946ReqNegReqTec_OSCod = BC004W12_A1946ReqNegReqTec_OSCod[0];
            n1946ReqNegReqTec_OSCod = BC004W12_n1946ReqNegReqTec_OSCod[0];
            A1895SolicServicoReqNeg_Codigo = BC004W12_A1895SolicServicoReqNeg_Codigo[0];
            A1919Requisito_Codigo = BC004W12_A1919Requisito_Codigo[0];
         }
         Gx_mode = sMode218;
      }

      protected void ScanKeyEnd4W218( )
      {
         pr_default.close(10);
      }

      protected void AfterConfirm4W218( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4W218( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4W218( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4W218( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4W218( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4W218( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4W218( )
      {
      }

      protected void AddRow4W218( )
      {
         VarsToRow218( bcReqNegReqTec) ;
      }

      protected void ReadRow4W218( )
      {
         RowToVars218( bcReqNegReqTec, 1) ;
      }

      protected void InitializeNonKey4W218( )
      {
         A1931Requisito_Ordem = 0;
         n1931Requisito_Ordem = false;
         A1945ReqNegReqTec_PropostaCod = 0;
         n1945ReqNegReqTec_PropostaCod = false;
         A1946ReqNegReqTec_OSCod = 0;
         n1946ReqNegReqTec_OSCod = false;
         Z1945ReqNegReqTec_PropostaCod = 0;
         Z1946ReqNegReqTec_OSCod = 0;
      }

      protected void InitAll4W218( )
      {
         A1895SolicServicoReqNeg_Codigo = 0;
         A1919Requisito_Codigo = 0;
         InitializeNonKey4W218( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow218( SdtReqNegReqTec obj218 )
      {
         obj218.gxTpr_Mode = Gx_mode;
         obj218.gxTpr_Requisito_ordem = A1931Requisito_Ordem;
         obj218.gxTpr_Reqnegreqtec_propostacod = A1945ReqNegReqTec_PropostaCod;
         obj218.gxTpr_Reqnegreqtec_oscod = A1946ReqNegReqTec_OSCod;
         obj218.gxTpr_Solicservicoreqneg_codigo = A1895SolicServicoReqNeg_Codigo;
         obj218.gxTpr_Requisito_codigo = A1919Requisito_Codigo;
         obj218.gxTpr_Solicservicoreqneg_codigo_Z = Z1895SolicServicoReqNeg_Codigo;
         obj218.gxTpr_Requisito_codigo_Z = Z1919Requisito_Codigo;
         obj218.gxTpr_Requisito_ordem_Z = Z1931Requisito_Ordem;
         obj218.gxTpr_Reqnegreqtec_propostacod_Z = Z1945ReqNegReqTec_PropostaCod;
         obj218.gxTpr_Reqnegreqtec_oscod_Z = Z1946ReqNegReqTec_OSCod;
         obj218.gxTpr_Requisito_ordem_N = (short)(Convert.ToInt16(n1931Requisito_Ordem));
         obj218.gxTpr_Reqnegreqtec_propostacod_N = (short)(Convert.ToInt16(n1945ReqNegReqTec_PropostaCod));
         obj218.gxTpr_Reqnegreqtec_oscod_N = (short)(Convert.ToInt16(n1946ReqNegReqTec_OSCod));
         obj218.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow218( SdtReqNegReqTec obj218 )
      {
         obj218.gxTpr_Solicservicoreqneg_codigo = A1895SolicServicoReqNeg_Codigo;
         obj218.gxTpr_Requisito_codigo = A1919Requisito_Codigo;
         return  ;
      }

      public void RowToVars218( SdtReqNegReqTec obj218 ,
                                int forceLoad )
      {
         Gx_mode = obj218.gxTpr_Mode;
         A1931Requisito_Ordem = obj218.gxTpr_Requisito_ordem;
         n1931Requisito_Ordem = false;
         A1945ReqNegReqTec_PropostaCod = obj218.gxTpr_Reqnegreqtec_propostacod;
         n1945ReqNegReqTec_PropostaCod = false;
         A1946ReqNegReqTec_OSCod = obj218.gxTpr_Reqnegreqtec_oscod;
         n1946ReqNegReqTec_OSCod = false;
         A1895SolicServicoReqNeg_Codigo = obj218.gxTpr_Solicservicoreqneg_codigo;
         A1919Requisito_Codigo = obj218.gxTpr_Requisito_codigo;
         Z1895SolicServicoReqNeg_Codigo = obj218.gxTpr_Solicservicoreqneg_codigo_Z;
         Z1919Requisito_Codigo = obj218.gxTpr_Requisito_codigo_Z;
         Z1931Requisito_Ordem = obj218.gxTpr_Requisito_ordem_Z;
         Z1945ReqNegReqTec_PropostaCod = obj218.gxTpr_Reqnegreqtec_propostacod_Z;
         Z1946ReqNegReqTec_OSCod = obj218.gxTpr_Reqnegreqtec_oscod_Z;
         n1931Requisito_Ordem = (bool)(Convert.ToBoolean(obj218.gxTpr_Requisito_ordem_N));
         n1945ReqNegReqTec_PropostaCod = (bool)(Convert.ToBoolean(obj218.gxTpr_Reqnegreqtec_propostacod_N));
         n1946ReqNegReqTec_OSCod = (bool)(Convert.ToBoolean(obj218.gxTpr_Reqnegreqtec_oscod_N));
         Gx_mode = obj218.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A1895SolicServicoReqNeg_Codigo = (long)getParm(obj,0);
         A1919Requisito_Codigo = (int)getParm(obj,1);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey4W218( ) ;
         ScanKeyStart4W218( ) ;
         if ( RcdFound218 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC004W13 */
            pr_default.execute(11, new Object[] {A1895SolicServicoReqNeg_Codigo});
            if ( (pr_default.getStatus(11) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Requisitos de Neg�cio da Solicita��o de Servi�o'.", "ForeignKeyNotFound", 1, "SOLICSERVICOREQNEG_CODIGO");
               AnyError = 1;
            }
            pr_default.close(11);
            /* Using cursor BC004W11 */
            pr_default.execute(9, new Object[] {A1919Requisito_Codigo});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe ' T213'.", "ForeignKeyNotFound", 1, "REQUISITO_CODIGO");
               AnyError = 1;
            }
            A1931Requisito_Ordem = BC004W11_A1931Requisito_Ordem[0];
            n1931Requisito_Ordem = BC004W11_n1931Requisito_Ordem[0];
            pr_default.close(9);
         }
         else
         {
            Gx_mode = "UPD";
            Z1895SolicServicoReqNeg_Codigo = A1895SolicServicoReqNeg_Codigo;
            Z1919Requisito_Codigo = A1919Requisito_Codigo;
         }
         ZM4W218( -1) ;
         OnLoadActions4W218( ) ;
         AddRow4W218( ) ;
         ScanKeyEnd4W218( ) ;
         if ( RcdFound218 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars218( bcReqNegReqTec, 0) ;
         ScanKeyStart4W218( ) ;
         if ( RcdFound218 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC004W13 */
            pr_default.execute(11, new Object[] {A1895SolicServicoReqNeg_Codigo});
            if ( (pr_default.getStatus(11) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Requisitos de Neg�cio da Solicita��o de Servi�o'.", "ForeignKeyNotFound", 1, "SOLICSERVICOREQNEG_CODIGO");
               AnyError = 1;
            }
            pr_default.close(11);
            /* Using cursor BC004W11 */
            pr_default.execute(9, new Object[] {A1919Requisito_Codigo});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe ' T213'.", "ForeignKeyNotFound", 1, "REQUISITO_CODIGO");
               AnyError = 1;
            }
            A1931Requisito_Ordem = BC004W11_A1931Requisito_Ordem[0];
            n1931Requisito_Ordem = BC004W11_n1931Requisito_Ordem[0];
            pr_default.close(9);
         }
         else
         {
            Gx_mode = "UPD";
            Z1895SolicServicoReqNeg_Codigo = A1895SolicServicoReqNeg_Codigo;
            Z1919Requisito_Codigo = A1919Requisito_Codigo;
         }
         ZM4W218( -1) ;
         OnLoadActions4W218( ) ;
         AddRow4W218( ) ;
         ScanKeyEnd4W218( ) ;
         if ( RcdFound218 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars218( bcReqNegReqTec, 0) ;
         nKeyPressed = 1;
         GetKey4W218( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert4W218( ) ;
         }
         else
         {
            if ( RcdFound218 == 1 )
            {
               if ( ( A1895SolicServicoReqNeg_Codigo != Z1895SolicServicoReqNeg_Codigo ) || ( A1919Requisito_Codigo != Z1919Requisito_Codigo ) )
               {
                  A1895SolicServicoReqNeg_Codigo = Z1895SolicServicoReqNeg_Codigo;
                  A1919Requisito_Codigo = Z1919Requisito_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update4W218( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A1895SolicServicoReqNeg_Codigo != Z1895SolicServicoReqNeg_Codigo ) || ( A1919Requisito_Codigo != Z1919Requisito_Codigo ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert4W218( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert4W218( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow218( bcReqNegReqTec) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars218( bcReqNegReqTec, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey4W218( ) ;
         if ( RcdFound218 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A1895SolicServicoReqNeg_Codigo != Z1895SolicServicoReqNeg_Codigo ) || ( A1919Requisito_Codigo != Z1919Requisito_Codigo ) )
            {
               A1895SolicServicoReqNeg_Codigo = Z1895SolicServicoReqNeg_Codigo;
               A1919Requisito_Codigo = Z1919Requisito_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A1895SolicServicoReqNeg_Codigo != Z1895SolicServicoReqNeg_Codigo ) || ( A1919Requisito_Codigo != Z1919Requisito_Codigo ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(11);
         pr_default.close(9);
         context.RollbackDataStores( "ReqNegReqTec_BC");
         VarsToRow218( bcReqNegReqTec) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcReqNegReqTec.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcReqNegReqTec.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcReqNegReqTec )
         {
            bcReqNegReqTec = (SdtReqNegReqTec)(sdt);
            if ( StringUtil.StrCmp(bcReqNegReqTec.gxTpr_Mode, "") == 0 )
            {
               bcReqNegReqTec.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow218( bcReqNegReqTec) ;
            }
            else
            {
               RowToVars218( bcReqNegReqTec, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcReqNegReqTec.gxTpr_Mode, "") == 0 )
            {
               bcReqNegReqTec.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars218( bcReqNegReqTec, 1) ;
         return  ;
      }

      public SdtReqNegReqTec ReqNegReqTec_BC
      {
         get {
            return bcReqNegReqTec ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(11);
         pr_default.close(9);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         BC004W6_A1931Requisito_Ordem = new short[1] ;
         BC004W6_n1931Requisito_Ordem = new bool[] {false} ;
         BC004W6_A1945ReqNegReqTec_PropostaCod = new int[1] ;
         BC004W6_n1945ReqNegReqTec_PropostaCod = new bool[] {false} ;
         BC004W6_A1946ReqNegReqTec_OSCod = new int[1] ;
         BC004W6_n1946ReqNegReqTec_OSCod = new bool[] {false} ;
         BC004W6_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         BC004W6_A1919Requisito_Codigo = new int[1] ;
         BC004W4_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         BC004W5_A1931Requisito_Ordem = new short[1] ;
         BC004W5_n1931Requisito_Ordem = new bool[] {false} ;
         BC004W7_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         BC004W7_A1919Requisito_Codigo = new int[1] ;
         BC004W3_A1945ReqNegReqTec_PropostaCod = new int[1] ;
         BC004W3_n1945ReqNegReqTec_PropostaCod = new bool[] {false} ;
         BC004W3_A1946ReqNegReqTec_OSCod = new int[1] ;
         BC004W3_n1946ReqNegReqTec_OSCod = new bool[] {false} ;
         BC004W3_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         BC004W3_A1919Requisito_Codigo = new int[1] ;
         sMode218 = "";
         BC004W2_A1945ReqNegReqTec_PropostaCod = new int[1] ;
         BC004W2_n1945ReqNegReqTec_PropostaCod = new bool[] {false} ;
         BC004W2_A1946ReqNegReqTec_OSCod = new int[1] ;
         BC004W2_n1946ReqNegReqTec_OSCod = new bool[] {false} ;
         BC004W2_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         BC004W2_A1919Requisito_Codigo = new int[1] ;
         BC004W11_A1931Requisito_Ordem = new short[1] ;
         BC004W11_n1931Requisito_Ordem = new bool[] {false} ;
         BC004W12_A1931Requisito_Ordem = new short[1] ;
         BC004W12_n1931Requisito_Ordem = new bool[] {false} ;
         BC004W12_A1945ReqNegReqTec_PropostaCod = new int[1] ;
         BC004W12_n1945ReqNegReqTec_PropostaCod = new bool[] {false} ;
         BC004W12_A1946ReqNegReqTec_OSCod = new int[1] ;
         BC004W12_n1946ReqNegReqTec_OSCod = new bool[] {false} ;
         BC004W12_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         BC004W12_A1919Requisito_Codigo = new int[1] ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         BC004W13_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.reqnegreqtec_bc__default(),
            new Object[][] {
                new Object[] {
               BC004W2_A1945ReqNegReqTec_PropostaCod, BC004W2_n1945ReqNegReqTec_PropostaCod, BC004W2_A1946ReqNegReqTec_OSCod, BC004W2_n1946ReqNegReqTec_OSCod, BC004W2_A1895SolicServicoReqNeg_Codigo, BC004W2_A1919Requisito_Codigo
               }
               , new Object[] {
               BC004W3_A1945ReqNegReqTec_PropostaCod, BC004W3_n1945ReqNegReqTec_PropostaCod, BC004W3_A1946ReqNegReqTec_OSCod, BC004W3_n1946ReqNegReqTec_OSCod, BC004W3_A1895SolicServicoReqNeg_Codigo, BC004W3_A1919Requisito_Codigo
               }
               , new Object[] {
               BC004W4_A1895SolicServicoReqNeg_Codigo
               }
               , new Object[] {
               BC004W5_A1931Requisito_Ordem, BC004W5_n1931Requisito_Ordem
               }
               , new Object[] {
               BC004W6_A1931Requisito_Ordem, BC004W6_n1931Requisito_Ordem, BC004W6_A1945ReqNegReqTec_PropostaCod, BC004W6_n1945ReqNegReqTec_PropostaCod, BC004W6_A1946ReqNegReqTec_OSCod, BC004W6_n1946ReqNegReqTec_OSCod, BC004W6_A1895SolicServicoReqNeg_Codigo, BC004W6_A1919Requisito_Codigo
               }
               , new Object[] {
               BC004W7_A1895SolicServicoReqNeg_Codigo, BC004W7_A1919Requisito_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC004W11_A1931Requisito_Ordem, BC004W11_n1931Requisito_Ordem
               }
               , new Object[] {
               BC004W12_A1931Requisito_Ordem, BC004W12_n1931Requisito_Ordem, BC004W12_A1945ReqNegReqTec_PropostaCod, BC004W12_n1945ReqNegReqTec_PropostaCod, BC004W12_A1946ReqNegReqTec_OSCod, BC004W12_n1946ReqNegReqTec_OSCod, BC004W12_A1895SolicServicoReqNeg_Codigo, BC004W12_A1919Requisito_Codigo
               }
               , new Object[] {
               BC004W13_A1895SolicServicoReqNeg_Codigo
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short Z1931Requisito_Ordem ;
      private short A1931Requisito_Ordem ;
      private short RcdFound218 ;
      private int trnEnded ;
      private int Z1919Requisito_Codigo ;
      private int A1919Requisito_Codigo ;
      private int Z1945ReqNegReqTec_PropostaCod ;
      private int A1945ReqNegReqTec_PropostaCod ;
      private int Z1946ReqNegReqTec_OSCod ;
      private int A1946ReqNegReqTec_OSCod ;
      private long Z1895SolicServicoReqNeg_Codigo ;
      private long A1895SolicServicoReqNeg_Codigo ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode218 ;
      private bool n1931Requisito_Ordem ;
      private bool n1945ReqNegReqTec_PropostaCod ;
      private bool n1946ReqNegReqTec_OSCod ;
      private SdtReqNegReqTec bcReqNegReqTec ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private short[] BC004W6_A1931Requisito_Ordem ;
      private bool[] BC004W6_n1931Requisito_Ordem ;
      private int[] BC004W6_A1945ReqNegReqTec_PropostaCod ;
      private bool[] BC004W6_n1945ReqNegReqTec_PropostaCod ;
      private int[] BC004W6_A1946ReqNegReqTec_OSCod ;
      private bool[] BC004W6_n1946ReqNegReqTec_OSCod ;
      private long[] BC004W6_A1895SolicServicoReqNeg_Codigo ;
      private int[] BC004W6_A1919Requisito_Codigo ;
      private long[] BC004W4_A1895SolicServicoReqNeg_Codigo ;
      private short[] BC004W5_A1931Requisito_Ordem ;
      private bool[] BC004W5_n1931Requisito_Ordem ;
      private long[] BC004W7_A1895SolicServicoReqNeg_Codigo ;
      private int[] BC004W7_A1919Requisito_Codigo ;
      private int[] BC004W3_A1945ReqNegReqTec_PropostaCod ;
      private bool[] BC004W3_n1945ReqNegReqTec_PropostaCod ;
      private int[] BC004W3_A1946ReqNegReqTec_OSCod ;
      private bool[] BC004W3_n1946ReqNegReqTec_OSCod ;
      private long[] BC004W3_A1895SolicServicoReqNeg_Codigo ;
      private int[] BC004W3_A1919Requisito_Codigo ;
      private int[] BC004W2_A1945ReqNegReqTec_PropostaCod ;
      private bool[] BC004W2_n1945ReqNegReqTec_PropostaCod ;
      private int[] BC004W2_A1946ReqNegReqTec_OSCod ;
      private bool[] BC004W2_n1946ReqNegReqTec_OSCod ;
      private long[] BC004W2_A1895SolicServicoReqNeg_Codigo ;
      private int[] BC004W2_A1919Requisito_Codigo ;
      private short[] BC004W11_A1931Requisito_Ordem ;
      private bool[] BC004W11_n1931Requisito_Ordem ;
      private short[] BC004W12_A1931Requisito_Ordem ;
      private bool[] BC004W12_n1931Requisito_Ordem ;
      private int[] BC004W12_A1945ReqNegReqTec_PropostaCod ;
      private bool[] BC004W12_n1945ReqNegReqTec_PropostaCod ;
      private int[] BC004W12_A1946ReqNegReqTec_OSCod ;
      private bool[] BC004W12_n1946ReqNegReqTec_OSCod ;
      private long[] BC004W12_A1895SolicServicoReqNeg_Codigo ;
      private int[] BC004W12_A1919Requisito_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private long[] BC004W13_A1895SolicServicoReqNeg_Codigo ;
   }

   public class reqnegreqtec_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC004W6 ;
          prmBC004W6 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004W4 ;
          prmBC004W4 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmBC004W5 ;
          prmBC004W5 = new Object[] {
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004W7 ;
          prmBC004W7 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004W3 ;
          prmBC004W3 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004W2 ;
          prmBC004W2 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004W8 ;
          prmBC004W8 = new Object[] {
          new Object[] {"@ReqNegReqTec_PropostaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ReqNegReqTec_OSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004W9 ;
          prmBC004W9 = new Object[] {
          new Object[] {"@ReqNegReqTec_PropostaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ReqNegReqTec_OSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004W10 ;
          prmBC004W10 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004W12 ;
          prmBC004W12 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0} ,
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004W13 ;
          prmBC004W13 = new Object[] {
          new Object[] {"@SolicServicoReqNeg_Codigo",SqlDbType.Decimal,10,0}
          } ;
          Object[] prmBC004W11 ;
          prmBC004W11 = new Object[] {
          new Object[] {"@Requisito_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC004W2", "SELECT [ReqNegReqTec_PropostaCod], [ReqNegReqTec_OSCod], [SolicServicoReqNeg_Codigo], [Requisito_Codigo] FROM [ReqNegReqTec] WITH (UPDLOCK) WHERE [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo AND [Requisito_Codigo] = @Requisito_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004W2,1,0,true,false )
             ,new CursorDef("BC004W3", "SELECT [ReqNegReqTec_PropostaCod], [ReqNegReqTec_OSCod], [SolicServicoReqNeg_Codigo], [Requisito_Codigo] FROM [ReqNegReqTec] WITH (NOLOCK) WHERE [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo AND [Requisito_Codigo] = @Requisito_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004W3,1,0,true,false )
             ,new CursorDef("BC004W4", "SELECT [SolicServicoReqNeg_Codigo] FROM [SolicitacaoServicoReqNegocio] WITH (NOLOCK) WHERE [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004W4,1,0,true,false )
             ,new CursorDef("BC004W5", "SELECT [Requisito_Ordem] FROM [Requisito] WITH (NOLOCK) WHERE [Requisito_Codigo] = @Requisito_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004W5,1,0,true,false )
             ,new CursorDef("BC004W6", "SELECT T2.[Requisito_Ordem], TM1.[ReqNegReqTec_PropostaCod], TM1.[ReqNegReqTec_OSCod], TM1.[SolicServicoReqNeg_Codigo], TM1.[Requisito_Codigo] FROM ([ReqNegReqTec] TM1 WITH (NOLOCK) INNER JOIN [Requisito] T2 WITH (NOLOCK) ON T2.[Requisito_Codigo] = TM1.[Requisito_Codigo]) WHERE TM1.[SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo and TM1.[Requisito_Codigo] = @Requisito_Codigo ORDER BY TM1.[SolicServicoReqNeg_Codigo], TM1.[Requisito_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004W6,100,0,true,false )
             ,new CursorDef("BC004W7", "SELECT [SolicServicoReqNeg_Codigo], [Requisito_Codigo] FROM [ReqNegReqTec] WITH (NOLOCK) WHERE [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo AND [Requisito_Codigo] = @Requisito_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004W7,1,0,true,false )
             ,new CursorDef("BC004W8", "INSERT INTO [ReqNegReqTec]([ReqNegReqTec_PropostaCod], [ReqNegReqTec_OSCod], [SolicServicoReqNeg_Codigo], [Requisito_Codigo]) VALUES(@ReqNegReqTec_PropostaCod, @ReqNegReqTec_OSCod, @SolicServicoReqNeg_Codigo, @Requisito_Codigo)", GxErrorMask.GX_NOMASK,prmBC004W8)
             ,new CursorDef("BC004W9", "UPDATE [ReqNegReqTec] SET [ReqNegReqTec_PropostaCod]=@ReqNegReqTec_PropostaCod, [ReqNegReqTec_OSCod]=@ReqNegReqTec_OSCod  WHERE [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo AND [Requisito_Codigo] = @Requisito_Codigo", GxErrorMask.GX_NOMASK,prmBC004W9)
             ,new CursorDef("BC004W10", "DELETE FROM [ReqNegReqTec]  WHERE [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo AND [Requisito_Codigo] = @Requisito_Codigo", GxErrorMask.GX_NOMASK,prmBC004W10)
             ,new CursorDef("BC004W11", "SELECT [Requisito_Ordem] FROM [Requisito] WITH (NOLOCK) WHERE [Requisito_Codigo] = @Requisito_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004W11,1,0,true,false )
             ,new CursorDef("BC004W12", "SELECT T2.[Requisito_Ordem], TM1.[ReqNegReqTec_PropostaCod], TM1.[ReqNegReqTec_OSCod], TM1.[SolicServicoReqNeg_Codigo], TM1.[Requisito_Codigo] FROM ([ReqNegReqTec] TM1 WITH (NOLOCK) INNER JOIN [Requisito] T2 WITH (NOLOCK) ON T2.[Requisito_Codigo] = TM1.[Requisito_Codigo]) WHERE TM1.[SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo and TM1.[Requisito_Codigo] = @Requisito_Codigo ORDER BY TM1.[SolicServicoReqNeg_Codigo], TM1.[Requisito_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004W12,100,0,true,false )
             ,new CursorDef("BC004W13", "SELECT [SolicServicoReqNeg_Codigo] FROM [SolicitacaoServicoReqNegocio] WITH (NOLOCK) WHERE [SolicServicoReqNeg_Codigo] = @SolicServicoReqNeg_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004W13,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((long[]) buf[4])[0] = rslt.getLong(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((long[]) buf[4])[0] = rslt.getLong(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
             case 2 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 3 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((long[]) buf[6])[0] = rslt.getLong(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 5 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((long[]) buf[6])[0] = rslt.getLong(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 11 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                stmt.SetParameter(3, (long)parms[4]);
                stmt.SetParameter(4, (int)parms[5]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                stmt.SetParameter(3, (long)parms[4]);
                stmt.SetParameter(4, (int)parms[5]);
                return;
             case 8 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 11 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
       }
    }

 }

}
