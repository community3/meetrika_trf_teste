/*
               File: PRC_DemandaAlterarQntSolicitada
        Description: PRC_DemandaAlterarQntSolicitada
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 22:31:31.35
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_demandaalterarqntsolicitada : GXProcedure
   {
      public prc_demandaalterarqntsolicitada( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_demandaalterarqntsolicitada( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           decimal aP1_ContagemResultado_QuantidadeSolicitada )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV8ContagemResultado_QuantidadeSolicitada = aP1_ContagemResultado_QuantidadeSolicitada;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_ContagemResultado_Codigo ,
                                 decimal aP1_ContagemResultado_QuantidadeSolicitada )
      {
         prc_demandaalterarqntsolicitada objprc_demandaalterarqntsolicitada;
         objprc_demandaalterarqntsolicitada = new prc_demandaalterarqntsolicitada();
         objprc_demandaalterarqntsolicitada.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_demandaalterarqntsolicitada.AV8ContagemResultado_QuantidadeSolicitada = aP1_ContagemResultado_QuantidadeSolicitada;
         objprc_demandaalterarqntsolicitada.context.SetSubmitInitialConfig(context);
         objprc_demandaalterarqntsolicitada.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_demandaalterarqntsolicitada);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_demandaalterarqntsolicitada)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Optimized UPDATE. */
         /* Using cursor P00YT2 */
         pr_default.execute(0, new Object[] {n2133ContagemResultado_QuantidadeSolicitada, AV8ContagemResultado_QuantidadeSolicitada, A456ContagemResultado_Codigo});
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
         dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
         /* End optimized UPDATE. */
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_demandaalterarqntsolicitada__default(),
            new Object[][] {
                new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A456ContagemResultado_Codigo ;
      private decimal AV8ContagemResultado_QuantidadeSolicitada ;
      private decimal A2133ContagemResultado_QuantidadeSolicitada ;
      private bool n2133ContagemResultado_QuantidadeSolicitada ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
   }

   public class prc_demandaalterarqntsolicitada__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new UpdateCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00YT2 ;
          prmP00YT2 = new Object[] {
          new Object[] {"@ContagemResultado_QuantidadeSolicitada",SqlDbType.Decimal,9,4} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00YT2", "UPDATE [ContagemResultado] SET [ContagemResultado_QuantidadeSolicitada]=@ContagemResultado_QuantidadeSolicitada  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00YT2)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
