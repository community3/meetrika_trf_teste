/*
               File: WP_CopiarColarAtributos
        Description: Copiar Colar Atributos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/6/2020 23:57:25.21
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_copiarcolaratributos : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_copiarcolaratributos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_copiarcolaratributos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Sistema_Codigo )
      {
         this.AV16Sistema_Codigo = aP0_Sistema_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridtabelas") == 0 )
            {
               nRC_GXsfl_17 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_17_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_17_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGridtabelas_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridtabelas") == 0 )
            {
               AV10Filtro = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Filtro", AV10Filtro);
               AV16Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Sistema_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV16Sistema_Codigo), "ZZZZZ9")));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGridtabelas_refresh( AV10Filtro, AV16Sistema_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV16Sistema_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Sistema_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV16Sistema_Codigo), "ZZZZZ9")));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA9Y2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START9Y2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205623572528");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_copiarcolaratributos.aspx") + "?" + UrlEncode("" +AV16Sistema_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vFILTRO", StringUtil.RTrim( AV10Filtro));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_17", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_17), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vATRIBUTOSCOLADOS", AV7AtributosColados);
         GxWebStd.gx_hidden_field( context, "ATRIBUTOS_NOME", StringUtil.RTrim( A177Atributos_Nome));
         GxWebStd.gx_hidden_field( context, "ATRIBUTOS_TABELACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A356Atributos_TabelaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vTABELA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17Tabela_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vUDPARG1", AV26Udparg1);
         GxWebStd.gx_hidden_field( context, "vATRIBUTOS_NOME", StringUtil.RTrim( AV6Atributos_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_vSISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV16Sistema_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vSISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV16Sistema_Codigo), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE9Y2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT9Y2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_copiarcolaratributos.aspx") + "?" + UrlEncode("" +AV16Sistema_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "WP_CopiarColarAtributos" ;
      }

      public override String GetPgmdesc( )
      {
         return "Copiar Colar Atributos" ;
      }

      protected void WB9Y0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            wb_table1_3_9Y2( true) ;
         }
         else
         {
            wb_table1_3_9Y2( false) ;
         }
         return  ;
      }

      protected void wb_table1_3_9Y2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table2_8_9Y2( true) ;
         }
         else
         {
            wb_table2_8_9Y2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_9Y2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START9Y2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Copiar Colar Atributos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP9Y0( ) ;
      }

      protected void WS9Y2( )
      {
         START9Y2( ) ;
         EVT9Y2( ) ;
      }

      protected void EVT9Y2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'GRAVAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E119Y2 */
                              E119Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E129Y2 */
                              E129Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 4), "LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_17_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_17_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_17_idx), 4, 0)), 4, "0");
                              SubsflControlProps_172( ) ;
                              A172Tabela_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTabela_Codigo_Internalname), ",", "."));
                              A173Tabela_Nome = StringUtil.Upper( cgiGet( edtTabela_Nome_Internalname));
                              AV9Dados = cgiGet( edtavDados_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDados_Internalname, AV9Dados);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E139Y2 */
                                    E139Y2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E149Y2 */
                                    E149Y2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Filtro Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFILTRO"), AV10Filtro) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE9Y2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA9Y2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavFiltro_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGridtabelas_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_172( ) ;
         while ( nGXsfl_17_idx <= nRC_GXsfl_17 )
         {
            sendrow_172( ) ;
            nGXsfl_17_idx = (short)(((subGridtabelas_Islastpage==1)&&(nGXsfl_17_idx+1>subGridtabelas_Recordsperpage( )) ? 1 : nGXsfl_17_idx+1));
            sGXsfl_17_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_17_idx), 4, 0)), 4, "0");
            SubsflControlProps_172( ) ;
         }
         context.GX_webresponse.AddString(GridtabelasContainer.ToJavascriptSource());
         /* End function gxnrGridtabelas_newrow */
      }

      protected void gxgrGridtabelas_refresh( String AV10Filtro ,
                                              int AV16Sistema_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRIDTABELAS_nCurrentRecord = 0;
         RF9Y2( ) ;
         /* End function gxgrGridtabelas_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_TABELA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A172Tabela_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "TABELA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A172Tabela_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_TABELA_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A173Tabela_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "TABELA_NOME", StringUtil.RTrim( A173Tabela_Nome));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF9Y2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF9Y2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridtabelasContainer.ClearRows();
         }
         wbStart = 17;
         nGXsfl_17_idx = 1;
         sGXsfl_17_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_17_idx), 4, 0)), 4, "0");
         SubsflControlProps_172( ) ;
         nGXsfl_17_Refreshing = 1;
         GridtabelasContainer.AddObjectProperty("GridName", "Gridtabelas");
         GridtabelasContainer.AddObjectProperty("CmpContext", "");
         GridtabelasContainer.AddObjectProperty("InMasterPage", "false");
         GridtabelasContainer.AddObjectProperty("Class", "WorkWith");
         GridtabelasContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridtabelasContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridtabelasContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Backcolorstyle), 1, 0, ".", "")));
         GridtabelasContainer.PageSize = subGridtabelas_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_172( ) ;
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV10Filtro ,
                                                 A173Tabela_Nome ,
                                                 A190Tabela_SistemaCod ,
                                                 AV16Sistema_Codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                                 }
            });
            lV10Filtro = StringUtil.PadR( StringUtil.RTrim( AV10Filtro), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Filtro", AV10Filtro);
            /* Using cursor H009Y2 */
            pr_default.execute(0, new Object[] {AV16Sistema_Codigo, lV10Filtro});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A190Tabela_SistemaCod = H009Y2_A190Tabela_SistemaCod[0];
               n190Tabela_SistemaCod = H009Y2_n190Tabela_SistemaCod[0];
               A173Tabela_Nome = H009Y2_A173Tabela_Nome[0];
               A172Tabela_Codigo = H009Y2_A172Tabela_Codigo[0];
               /* Execute user event: E149Y2 */
               E149Y2 ();
               pr_default.readNext(0);
            }
            pr_default.close(0);
            wbEnd = 17;
            WB9Y0( ) ;
         }
         nGXsfl_17_Refreshing = 0;
      }

      protected int subGridtabelas_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGridtabelas_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGridtabelas_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGridtabelas_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUP9Y0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E139Y2 */
         E139Y2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV10Filtro = StringUtil.Upper( cgiGet( edtavFiltro_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Filtro", AV10Filtro);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavInseridas_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavInseridas_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vINSERIDAS");
               GX_FocusControl = edtavInseridas_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV12Inseridas = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Inseridas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Inseridas), 4, 0)));
            }
            else
            {
               AV12Inseridas = (short)(context.localUtil.CToN( cgiGet( edtavInseridas_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Inseridas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Inseridas), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavJacadastradas_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavJacadastradas_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vJACADASTRADAS");
               GX_FocusControl = edtavJacadastradas_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13JaCadastradas = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13JaCadastradas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13JaCadastradas), 4, 0)));
            }
            else
            {
               AV13JaCadastradas = (short)(context.localUtil.CToN( cgiGet( edtavJacadastradas_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13JaCadastradas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13JaCadastradas), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavNaoinseridas_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavNaoinseridas_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vNAOINSERIDAS");
               GX_FocusControl = edtavNaoinseridas_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV14NaoInseridas = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14NaoInseridas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14NaoInseridas), 4, 0)));
            }
            else
            {
               AV14NaoInseridas = (short)(context.localUtil.CToN( cgiGet( edtavNaoinseridas_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14NaoInseridas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14NaoInseridas), 4, 0)));
            }
            /* Read saved values. */
            nRC_GXsfl_17 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_17"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E139Y2 */
         E139Y2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E139Y2( )
      {
         /* Start Routine */
         tblTblresultado_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblresultado_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblresultado_Visible), 5, 0)));
         /* Using cursor H009Y3 */
         pr_default.execute(1, new Object[] {AV16Sistema_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A127Sistema_Codigo = H009Y3_A127Sistema_Codigo[0];
            A416Sistema_Nome = H009Y3_A416Sistema_Nome[0];
            lblTbsistemanome_Caption = StringUtil.Trim( A416Sistema_Nome);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbsistemanome_Internalname, "Caption", lblTbsistemanome_Caption);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
         AV18FiltroRecebido = AV19WebSession.Get("FiltroRecebido");
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18FiltroRecebido)) )
         {
            AV10Filtro = AV18FiltroRecebido;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Filtro", AV10Filtro);
         }
      }

      protected void E119Y2( )
      {
         /* 'Gravar' Routine */
         AV12Inseridas = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Inseridas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Inseridas), 4, 0)));
         AV13JaCadastradas = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13JaCadastradas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13JaCadastradas), 4, 0)));
         AV14NaoInseridas = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14NaoInseridas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14NaoInseridas), 4, 0)));
         /* Start For Each Line */
         nRC_GXsfl_17 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_17"), ",", "."));
         nGXsfl_17_fel_idx = 0;
         while ( nGXsfl_17_fel_idx < nRC_GXsfl_17 )
         {
            nGXsfl_17_fel_idx = (short)(((subGridtabelas_Islastpage==1)&&(nGXsfl_17_fel_idx+1>subGridtabelas_Recordsperpage( )) ? 1 : nGXsfl_17_fel_idx+1));
            sGXsfl_17_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_17_fel_idx), 4, 0)), 4, "0");
            SubsflControlProps_fel_172( ) ;
            A172Tabela_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTabela_Codigo_Internalname), ",", "."));
            A173Tabela_Nome = StringUtil.Upper( cgiGet( edtTabela_Nome_Internalname));
            AV9Dados = cgiGet( edtavDados_Internalname);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV9Dados)) )
            {
               AV17Tabela_Codigo = A172Tabela_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Tabela_Codigo), 6, 0)));
               AV7AtributosColados = AV9Dados;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7AtributosColados", AV7AtributosColados);
               /* Execute user subroutine: 'GRAVARATRIBUTOS' */
               S112 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
            }
            /* End For Each Line */
         }
         if ( nGXsfl_17_fel_idx == 0 )
         {
            nGXsfl_17_idx = 1;
            sGXsfl_17_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_17_idx), 4, 0)), 4, "0");
            SubsflControlProps_172( ) ;
         }
         nGXsfl_17_fel_idx = 1;
         tblTblresultado_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblresultado_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblresultado_Visible), 5, 0)));
      }

      protected void S112( )
      {
         /* 'GRAVARATRIBUTOS' Routine */
         AV11i = 0;
         AV6Atributos_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Atributos_Nome", AV6Atributos_Nome);
         AV11i = 1;
         while ( AV11i <= StringUtil.Len( StringUtil.Trim( AV7AtributosColados)) )
         {
            AV15s = (short)(AV11i+1);
            AV8Char = StringUtil.Substring( AV7AtributosColados, AV11i, 1);
            if ( StringUtil.StrCmp(AV8Char, " ") == 0 )
            {
               if ( StringUtil.Asc( StringUtil.Substring( AV7AtributosColados, AV15s, 1)) > 32 )
               {
                  if ( StringUtil.Len( AV6Atributos_Nome) > 1 )
                  {
                     AV6Atributos_Nome = AV6Atributos_Nome + "_";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Atributos_Nome", AV6Atributos_Nome);
                  }
               }
               else
               {
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV6Atributos_Nome)) )
                  {
                     /* Execute user subroutine: 'NEWATRIBUTO' */
                     S122 ();
                     if ( returnInSub )
                     {
                        returnInSub = true;
                        if (true) return;
                     }
                  }
               }
            }
            else if ( StringUtil.StrCmp(AV8Char, "�") == 0 )
            {
               AV6Atributos_Nome = AV6Atributos_Nome + "o";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Atributos_Nome", AV6Atributos_Nome);
            }
            else if ( StringUtil.StrCmp(AV8Char, "�") == 0 )
            {
               AV6Atributos_Nome = AV6Atributos_Nome + "a";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Atributos_Nome", AV6Atributos_Nome);
            }
            else if ( ( StringUtil.StrCmp(AV8Char, ",") == 0 ) || ( StringUtil.StrCmp(AV8Char, ";") == 0 ) || ( StringUtil.StrCmp(AV8Char, StringUtil.Chr( 9)) == 0 ) || ( StringUtil.StrCmp(AV8Char, StringUtil.Chr( 10)) == 0 ) || ( StringUtil.StrCmp(AV8Char, StringUtil.Chr( 13)) == 0 ) )
            {
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV6Atributos_Nome)) )
               {
                  /* Execute user subroutine: 'NEWATRIBUTO' */
                  S122 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
            }
            else if ( AV11i == StringUtil.Len( StringUtil.Trim( AV7AtributosColados)) )
            {
               AV6Atributos_Nome = AV6Atributos_Nome + AV8Char;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Atributos_Nome", AV6Atributos_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV6Atributos_Nome)) )
               {
                  /* Execute user subroutine: 'NEWATRIBUTO' */
                  S122 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
            }
            else
            {
               AV6Atributos_Nome = AV6Atributos_Nome + AV8Char;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Atributos_Nome", AV6Atributos_Nome);
            }
            AV11i = (short)(AV11i+1);
         }
      }

      protected void S122( )
      {
         /* 'NEWATRIBUTO' Routine */
         AV25GXLvl68 = 0;
         AV26Udparg1 = new prc_padronizastring(context).executeUdp(  AV6Atributos_Nome);
         /* Using cursor H009Y4 */
         pr_default.execute(2, new Object[] {AV17Tabela_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A356Atributos_TabelaCod = H009Y4_A356Atributos_TabelaCod[0];
            A190Tabela_SistemaCod = H009Y4_A190Tabela_SistemaCod[0];
            n190Tabela_SistemaCod = H009Y4_n190Tabela_SistemaCod[0];
            A177Atributos_Nome = H009Y4_A177Atributos_Nome[0];
            A190Tabela_SistemaCod = H009Y4_A190Tabela_SistemaCod[0];
            n190Tabela_SistemaCod = H009Y4_n190Tabela_SistemaCod[0];
            if ( StringUtil.StrCmp(new prc_padronizastring(context).executeUdp(  A177Atributos_Nome), AV26Udparg1) == 0 )
            {
               AV25GXLvl68 = 1;
               AV13JaCadastradas = (short)(AV13JaCadastradas+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13JaCadastradas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13JaCadastradas), 4, 0)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(2);
         }
         pr_default.close(2);
         if ( AV25GXLvl68 == 0 )
         {
            AV5Atributos = new SdtAtributos(context);
            AV5Atributos.gxTpr_Atributos_nome = StringUtil.Upper( StringUtil.Trim( AV6Atributos_Nome));
            AV5Atributos.gxTpr_Atributos_tabelacod = AV17Tabela_Codigo;
            AV5Atributos.Save();
            if ( AV5Atributos.Success() )
            {
               context.CommitDataStores( "WP_CopiarColarAtributos");
               AV12Inseridas = (short)(AV12Inseridas+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Inseridas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Inseridas), 4, 0)));
            }
            else
            {
               AV14NaoInseridas = (short)(AV14NaoInseridas+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14NaoInseridas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14NaoInseridas), 4, 0)));
               context.RollbackDataStores( "WP_CopiarColarAtributos");
            }
         }
         AV6Atributos_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Atributos_Nome", AV6Atributos_Nome);
      }

      protected void E129Y2( )
      {
         /* 'DoFechar' Routine */
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      private void E149Y2( )
      {
         /* Load Routine */
         sendrow_172( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_17_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(17, GridtabelasRow);
         }
      }

      protected void wb_table2_8_9Y2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td data-align=\"left\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-left;text-align:-moz-left;text-align:-webkit-left;height:50px")+"\" class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Filtro:", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_CopiarColarAtributos.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'" + sGXsfl_17_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFiltro_Internalname, StringUtil.RTrim( AV10Filtro), StringUtil.RTrim( context.localUtil.Format( AV10Filtro, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,12);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFiltro_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_CopiarColarAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='Table'>") ;
            /*  Grid Control  */
            GridtabelasContainer.SetWrapped(nGXWrapped);
            if ( GridtabelasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridtabelasContainer"+"DivS\" data-gxgridid=\"17\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGridtabelas_Internalname, subGridtabelas_Internalname, "", "WorkWith", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGridtabelas_Backcolorstyle == 0 )
               {
                  subGridtabelas_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGridtabelas_Class) > 0 )
                  {
                     subGridtabelas_Linesclass = subGridtabelas_Class+"Title";
                  }
               }
               else
               {
                  subGridtabelas_Titlebackstyle = 1;
                  if ( subGridtabelas_Backcolorstyle == 1 )
                  {
                     subGridtabelas_Titlebackcolor = subGridtabelas_Allbackcolor;
                     if ( StringUtil.Len( subGridtabelas_Class) > 0 )
                     {
                        subGridtabelas_Linesclass = subGridtabelas_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGridtabelas_Class) > 0 )
                     {
                        subGridtabelas_Linesclass = subGridtabelas_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridtabelas_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridtabelas_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Tabela") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(400), 4, 0))+"px"+" class=\""+subGridtabelas_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Atributos para inserir") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridtabelasContainer.AddObjectProperty("GridName", "Gridtabelas");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridtabelasContainer = new GXWebGrid( context);
               }
               else
               {
                  GridtabelasContainer.Clear();
               }
               GridtabelasContainer.SetWrapped(nGXWrapped);
               GridtabelasContainer.AddObjectProperty("GridName", "Gridtabelas");
               GridtabelasContainer.AddObjectProperty("Class", "WorkWith");
               GridtabelasContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Backcolorstyle), 1, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("CmpContext", "");
               GridtabelasContainer.AddObjectProperty("InMasterPage", "false");
               GridtabelasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridtabelasColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A172Tabela_Codigo), 6, 0, ".", "")));
               GridtabelasContainer.AddColumnProperties(GridtabelasColumn);
               GridtabelasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridtabelasColumn.AddObjectProperty("Value", StringUtil.RTrim( A173Tabela_Nome));
               GridtabelasContainer.AddColumnProperties(GridtabelasColumn);
               GridtabelasColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridtabelasColumn.AddObjectProperty("Value", AV9Dados);
               GridtabelasContainer.AddColumnProperties(GridtabelasColumn);
               GridtabelasContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Allowselection), 1, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Selectioncolor), 9, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Allowhovering), 1, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Hoveringcolor), 9, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Allowcollapsing), 1, 0, ".", "")));
               GridtabelasContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtabelas_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 17 )
         {
            wbEnd = 0;
            nRC_GXsfl_17 = (short)(nGXsfl_17_idx-1);
            if ( GridtabelasContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridtabelasContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridtabelas", GridtabelasContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridtabelasContainerData", GridtabelasContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridtabelasContainerData"+"V", GridtabelasContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridtabelasContainerData"+"V"+"\" value='"+GridtabelasContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='Table'>") ;
            wb_table3_22_9Y2( true) ;
         }
         else
         {
            wb_table3_22_9Y2( false) ;
         }
         return  ;
      }

      protected void wb_table3_22_9Y2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttGravar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(17), 2, 0)+","+"null"+");", "Gravar", bttGravar_Jsonclick, 5, "Gravar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'GRAVAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_CopiarColarAtributos.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(17), 2, 0)+","+"null"+");", "Fechar", bttBtn_trn_cancel_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_CopiarColarAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_9Y2e( true) ;
         }
         else
         {
            wb_table2_8_9Y2e( false) ;
         }
      }

      protected void wb_table3_22_9Y2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblresultado_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblresultado_Internalname, tblTblresultado_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "Inseridas:", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_CopiarColarAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'" + sGXsfl_17_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavInseridas_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Inseridas), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV12Inseridas), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,27);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavInseridas_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_CopiarColarAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock4_Internalname, "Procuradas:", "", "", lblTextblock4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_CopiarColarAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'" + sGXsfl_17_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavJacadastradas_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13JaCadastradas), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13JaCadastradas), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,32);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavJacadastradas_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_CopiarColarAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock5_Internalname, "Erros:", "", "", lblTextblock5_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_CopiarColarAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_17_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavNaoinseridas_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14NaoInseridas), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV14NaoInseridas), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavNaoinseridas_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_CopiarColarAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_22_9Y2e( true) ;
         }
         else
         {
            wb_table3_22_9Y2e( false) ;
         }
      }

      protected void wb_table1_3_9Y2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock6_Internalname, "Sistema:", "", "", lblTextblock6_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_CopiarColarAtributos.htm");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbsistemanome_Internalname, lblTbsistemanome_Caption, "", "", lblTbsistemanome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:12.0pt; font-weight:bold; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_CopiarColarAtributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_3_9Y2e( true) ;
         }
         else
         {
            wb_table1_3_9Y2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV16Sistema_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Sistema_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV16Sistema_Codigo), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA9Y2( ) ;
         WS9Y2( ) ;
         WE9Y2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205623572581");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_copiarcolaratributos.js", "?20205623572581");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_172( )
      {
         edtTabela_Codigo_Internalname = "TABELA_CODIGO_"+sGXsfl_17_idx;
         edtTabela_Nome_Internalname = "TABELA_NOME_"+sGXsfl_17_idx;
         edtavDados_Internalname = "vDADOS_"+sGXsfl_17_idx;
      }

      protected void SubsflControlProps_fel_172( )
      {
         edtTabela_Codigo_Internalname = "TABELA_CODIGO_"+sGXsfl_17_fel_idx;
         edtTabela_Nome_Internalname = "TABELA_NOME_"+sGXsfl_17_fel_idx;
         edtavDados_Internalname = "vDADOS_"+sGXsfl_17_fel_idx;
      }

      protected void sendrow_172( )
      {
         SubsflControlProps_172( ) ;
         WB9Y0( ) ;
         GridtabelasRow = GXWebRow.GetNew(context,GridtabelasContainer);
         if ( subGridtabelas_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGridtabelas_Backstyle = 0;
            if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
            {
               subGridtabelas_Linesclass = subGridtabelas_Class+"Odd";
            }
         }
         else if ( subGridtabelas_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGridtabelas_Backstyle = 0;
            subGridtabelas_Backcolor = subGridtabelas_Allbackcolor;
            if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
            {
               subGridtabelas_Linesclass = subGridtabelas_Class+"Uniform";
            }
         }
         else if ( subGridtabelas_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGridtabelas_Backstyle = 1;
            if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
            {
               subGridtabelas_Linesclass = subGridtabelas_Class+"Odd";
            }
            subGridtabelas_Backcolor = (int)(0x0);
         }
         else if ( subGridtabelas_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGridtabelas_Backstyle = 1;
            if ( ((int)((nGXsfl_17_idx) % (2))) == 0 )
            {
               subGridtabelas_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
               {
                  subGridtabelas_Linesclass = subGridtabelas_Class+"Even";
               }
            }
            else
            {
               subGridtabelas_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridtabelas_Class, "") != 0 )
               {
                  subGridtabelas_Linesclass = subGridtabelas_Class+"Odd";
               }
            }
         }
         if ( GridtabelasContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGridtabelas_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_17_idx+"\">") ;
         }
         /* Subfile cell */
         if ( GridtabelasContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridtabelasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A172Tabela_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A172Tabela_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_Codigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)17,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridtabelasContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridtabelasRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTabela_Nome_Internalname,StringUtil.RTrim( A173Tabela_Nome),StringUtil.RTrim( context.localUtil.Format( A173Tabela_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTabela_Nome_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)17,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridtabelasContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Multiple line edit */
         TempTags = " " + ((edtavDados_Enabled!=0)&&(edtavDados_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 20,'',false,'"+sGXsfl_17_idx+"',17)\"" : " ");
         ClassString = "Attribute";
         StyleString = "";
         ClassString = "Attribute";
         StyleString = "";
         GridtabelasRow.AddColumnProperties("html_textarea", 1, isAjaxCallMode( ), new Object[] {(String)edtavDados_Internalname,(String)AV9Dados,(String)"",TempTags+((edtavDados_Enabled!=0)&&(edtavDados_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavDados_Enabled!=0)&&(edtavDados_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,20);\"" : " "),(short)1,(short)-1,(short)1,(short)0,(short)400,(String)"px",(short)50,(String)"px",(String)StyleString,(String)ClassString,(String)"",(String)"1000",(short)1,(String)"",(String)"",(short)-1,(bool)true,(String)""});
         GxWebStd.gx_hidden_field( context, "gxhash_TABELA_CODIGO"+"_"+sGXsfl_17_idx, GetSecureSignedToken( sGXsfl_17_idx, context.localUtil.Format( (decimal)(A172Tabela_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_TABELA_NOME"+"_"+sGXsfl_17_idx, GetSecureSignedToken( sGXsfl_17_idx, StringUtil.RTrim( context.localUtil.Format( A173Tabela_Nome, "@!"))));
         GridtabelasContainer.AddRow(GridtabelasRow);
         nGXsfl_17_idx = (short)(((subGridtabelas_Islastpage==1)&&(nGXsfl_17_idx+1>subGridtabelas_Recordsperpage( )) ? 1 : nGXsfl_17_idx+1));
         sGXsfl_17_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_17_idx), 4, 0)), 4, "0");
         SubsflControlProps_172( ) ;
         /* End function sendrow_172 */
      }

      protected void init_default_properties( )
      {
         lblTextblock6_Internalname = "TEXTBLOCK6";
         lblTbsistemanome_Internalname = "TBSISTEMANOME";
         tblTable2_Internalname = "TABLE2";
         lblTextblock1_Internalname = "TEXTBLOCK1";
         edtavFiltro_Internalname = "vFILTRO";
         edtTabela_Codigo_Internalname = "TABELA_CODIGO";
         edtTabela_Nome_Internalname = "TABELA_NOME";
         edtavDados_Internalname = "vDADOS";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         edtavInseridas_Internalname = "vINSERIDAS";
         lblTextblock4_Internalname = "TEXTBLOCK4";
         edtavJacadastradas_Internalname = "vJACADASTRADAS";
         lblTextblock5_Internalname = "TEXTBLOCK5";
         edtavNaoinseridas_Internalname = "vNAOINSERIDAS";
         tblTblresultado_Internalname = "TBLRESULTADO";
         bttGravar_Internalname = "GRAVAR";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
         subGridtabelas_Internalname = "GRIDTABELAS";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavDados_Visible = -1;
         edtavDados_Enabled = 1;
         edtTabela_Nome_Jsonclick = "";
         edtTabela_Codigo_Jsonclick = "";
         edtavNaoinseridas_Jsonclick = "";
         edtavJacadastradas_Jsonclick = "";
         edtavInseridas_Jsonclick = "";
         subGridtabelas_Allowcollapsing = 0;
         subGridtabelas_Allowselection = 0;
         subGridtabelas_Class = "WorkWith";
         edtavFiltro_Jsonclick = "";
         lblTbsistemanome_Caption = "Nome do Sistema";
         tblTblresultado_Visible = 1;
         subGridtabelas_Backcolorstyle = 0;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Copiar Colar Atributos";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRIDTABELAS_nFirstRecordOnPage',nv:0},{av:'GRIDTABELAS_nEOF',nv:0},{av:'AV10Filtro',fld:'vFILTRO',pic:'@!',nv:''},{av:'AV16Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'GRAVAR'","{handler:'E119Y2',iparms:[{av:'AV9Dados',fld:'vDADOS',grid:17,pic:'',nv:''},{av:'A172Tabela_Codigo',fld:'TABELA_CODIGO',grid:17,pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV7AtributosColados',fld:'vATRIBUTOSCOLADOS',pic:'',nv:''},{av:'A177Atributos_Nome',fld:'ATRIBUTOS_NOME',pic:'@!',nv:''},{av:'A356Atributos_TabelaCod',fld:'ATRIBUTOS_TABELACOD',pic:'ZZZZZ9',nv:0},{av:'AV17Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV26Udparg1',fld:'vUDPARG1',pic:'',nv:''},{av:'AV13JaCadastradas',fld:'vJACADASTRADAS',pic:'ZZZ9',nv:0},{av:'AV6Atributos_Nome',fld:'vATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV12Inseridas',fld:'vINSERIDAS',pic:'ZZZ9',nv:0},{av:'AV14NaoInseridas',fld:'vNAOINSERIDAS',pic:'ZZZ9',nv:0}],oparms:[{av:'AV12Inseridas',fld:'vINSERIDAS',pic:'ZZZ9',nv:0},{av:'AV13JaCadastradas',fld:'vJACADASTRADAS',pic:'ZZZ9',nv:0},{av:'AV14NaoInseridas',fld:'vNAOINSERIDAS',pic:'ZZZ9',nv:0},{av:'AV17Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7AtributosColados',fld:'vATRIBUTOSCOLADOS',pic:'',nv:''},{av:'tblTblresultado_Visible',ctrl:'TBLRESULTADO',prop:'Visible'},{av:'AV6Atributos_Nome',fld:'vATRIBUTOS_NOME',pic:'@!',nv:''}]}");
         setEventMetadata("'DOFECHAR'","{handler:'E129Y2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV10Filtro = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV7AtributosColados = "";
         A177Atributos_Nome = "";
         AV26Udparg1 = "";
         AV6Atributos_Nome = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A173Tabela_Nome = "";
         AV9Dados = "";
         GridtabelasContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV10Filtro = "";
         H009Y2_A190Tabela_SistemaCod = new int[1] ;
         H009Y2_n190Tabela_SistemaCod = new bool[] {false} ;
         H009Y2_A173Tabela_Nome = new String[] {""} ;
         H009Y2_A172Tabela_Codigo = new int[1] ;
         H009Y3_A127Sistema_Codigo = new int[1] ;
         H009Y3_A416Sistema_Nome = new String[] {""} ;
         A416Sistema_Nome = "";
         AV18FiltroRecebido = "";
         AV19WebSession = context.GetSession();
         AV8Char = "";
         H009Y4_A176Atributos_Codigo = new int[1] ;
         H009Y4_A356Atributos_TabelaCod = new int[1] ;
         H009Y4_A190Tabela_SistemaCod = new int[1] ;
         H009Y4_n190Tabela_SistemaCod = new bool[] {false} ;
         H009Y4_A177Atributos_Nome = new String[] {""} ;
         AV5Atributos = new SdtAtributos(context);
         GridtabelasRow = new GXWebRow();
         sStyleString = "";
         lblTextblock1_Jsonclick = "";
         TempTags = "";
         subGridtabelas_Linesclass = "";
         GridtabelasColumn = new GXWebColumn();
         bttGravar_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         lblTextblock3_Jsonclick = "";
         lblTextblock4_Jsonclick = "";
         lblTextblock5_Jsonclick = "";
         lblTextblock6_Jsonclick = "";
         lblTbsistemanome_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_copiarcolaratributos__default(),
            new Object[][] {
                new Object[] {
               H009Y2_A190Tabela_SistemaCod, H009Y2_A173Tabela_Nome, H009Y2_A172Tabela_Codigo
               }
               , new Object[] {
               H009Y3_A127Sistema_Codigo, H009Y3_A416Sistema_Nome
               }
               , new Object[] {
               H009Y4_A176Atributos_Codigo, H009Y4_A356Atributos_TabelaCod, H009Y4_A190Tabela_SistemaCod, H009Y4_n190Tabela_SistemaCod, H009Y4_A177Atributos_Nome
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_17 ;
      private short nGXsfl_17_idx=1 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_17_Refreshing=0 ;
      private short subGridtabelas_Backcolorstyle ;
      private short AV12Inseridas ;
      private short AV13JaCadastradas ;
      private short AV14NaoInseridas ;
      private short nGXsfl_17_fel_idx=1 ;
      private short AV11i ;
      private short AV15s ;
      private short AV25GXLvl68 ;
      private short subGridtabelas_Titlebackstyle ;
      private short subGridtabelas_Allowselection ;
      private short subGridtabelas_Allowhovering ;
      private short subGridtabelas_Allowcollapsing ;
      private short subGridtabelas_Collapsed ;
      private short nGXWrapped ;
      private short subGridtabelas_Backstyle ;
      private short GRIDTABELAS_nEOF ;
      private int AV16Sistema_Codigo ;
      private int wcpOAV16Sistema_Codigo ;
      private int A356Atributos_TabelaCod ;
      private int AV17Tabela_Codigo ;
      private int A172Tabela_Codigo ;
      private int subGridtabelas_Islastpage ;
      private int A190Tabela_SistemaCod ;
      private int tblTblresultado_Visible ;
      private int A127Sistema_Codigo ;
      private int subGridtabelas_Titlebackcolor ;
      private int subGridtabelas_Allbackcolor ;
      private int subGridtabelas_Selectioncolor ;
      private int subGridtabelas_Hoveringcolor ;
      private int idxLst ;
      private int subGridtabelas_Backcolor ;
      private int edtavDados_Enabled ;
      private int edtavDados_Visible ;
      private long GRIDTABELAS_nCurrentRecord ;
      private long GRIDTABELAS_nFirstRecordOnPage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_17_idx="0001" ;
      private String AV10Filtro ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A177Atributos_Nome ;
      private String AV6Atributos_Nome ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String ClassString ;
      private String StyleString ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtTabela_Codigo_Internalname ;
      private String A173Tabela_Nome ;
      private String edtTabela_Nome_Internalname ;
      private String edtavDados_Internalname ;
      private String edtavFiltro_Internalname ;
      private String scmdbuf ;
      private String lV10Filtro ;
      private String edtavInseridas_Internalname ;
      private String edtavJacadastradas_Internalname ;
      private String edtavNaoinseridas_Internalname ;
      private String tblTblresultado_Internalname ;
      private String lblTbsistemanome_Caption ;
      private String lblTbsistemanome_Internalname ;
      private String AV18FiltroRecebido ;
      private String sGXsfl_17_fel_idx="0001" ;
      private String AV8Char ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String TempTags ;
      private String edtavFiltro_Jsonclick ;
      private String subGridtabelas_Internalname ;
      private String subGridtabelas_Class ;
      private String subGridtabelas_Linesclass ;
      private String bttGravar_Internalname ;
      private String bttGravar_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String edtavInseridas_Jsonclick ;
      private String lblTextblock4_Internalname ;
      private String lblTextblock4_Jsonclick ;
      private String edtavJacadastradas_Jsonclick ;
      private String lblTextblock5_Internalname ;
      private String lblTextblock5_Jsonclick ;
      private String edtavNaoinseridas_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblock6_Internalname ;
      private String lblTextblock6_Jsonclick ;
      private String lblTbsistemanome_Jsonclick ;
      private String ROClassString ;
      private String edtTabela_Codigo_Jsonclick ;
      private String edtTabela_Nome_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n190Tabela_SistemaCod ;
      private bool returnInSub ;
      private String AV7AtributosColados ;
      private String AV26Udparg1 ;
      private String AV9Dados ;
      private String A416Sistema_Nome ;
      private IGxSession AV19WebSession ;
      private GXWebGrid GridtabelasContainer ;
      private GXWebRow GridtabelasRow ;
      private GXWebColumn GridtabelasColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H009Y2_A190Tabela_SistemaCod ;
      private bool[] H009Y2_n190Tabela_SistemaCod ;
      private String[] H009Y2_A173Tabela_Nome ;
      private int[] H009Y2_A172Tabela_Codigo ;
      private int[] H009Y3_A127Sistema_Codigo ;
      private String[] H009Y3_A416Sistema_Nome ;
      private int[] H009Y4_A176Atributos_Codigo ;
      private int[] H009Y4_A356Atributos_TabelaCod ;
      private int[] H009Y4_A190Tabela_SistemaCod ;
      private bool[] H009Y4_n190Tabela_SistemaCod ;
      private String[] H009Y4_A177Atributos_Nome ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
      private SdtAtributos AV5Atributos ;
   }

   public class wp_copiarcolaratributos__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H009Y2( IGxContext context ,
                                             String AV10Filtro ,
                                             String A173Tabela_Nome ,
                                             int A190Tabela_SistemaCod ,
                                             int AV16Sistema_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [2] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Tabela_SistemaCod], [Tabela_Nome], [Tabela_Codigo] FROM [Tabela] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Tabela_SistemaCod] = @AV16Sistema_Codigo)";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10Filtro)) )
         {
            sWhereString = sWhereString + " and ([Tabela_Nome] like '%' + @lV10Filtro + '%')";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Tabela_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H009Y2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH009Y3 ;
          prmH009Y3 = new Object[] {
          new Object[] {"@AV16Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH009Y4 ;
          prmH009Y4 = new Object[] {
          new Object[] {"@AV17Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH009Y2 ;
          prmH009Y2 = new Object[] {
          new Object[] {"@AV16Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10Filtro",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H009Y2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009Y2,11,0,true,false )
             ,new CursorDef("H009Y3", "SELECT [Sistema_Codigo], [Sistema_Nome] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @AV16Sistema_Codigo ORDER BY [Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009Y3,1,0,false,true )
             ,new CursorDef("H009Y4", "SELECT T1.[Atributos_Codigo], T1.[Atributos_TabelaCod] AS Atributos_TabelaCod, T2.[Tabela_SistemaCod], T1.[Atributos_Nome] FROM ([Atributos] T1 WITH (NOLOCK) INNER JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = T1.[Atributos_TabelaCod]) WHERE T1.[Atributos_TabelaCod] = @AV17Tabela_Codigo ORDER BY T1.[Atributos_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009Y4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[2]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[3]);
                }
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
