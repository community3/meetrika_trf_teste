/*
               File: type_SdtGAMLoginAdditionalParameters
        Description: GAMLoginAdditionalParameters
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 1:32:7.9
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMLoginAdditionalParameters : GxUserType, IGxExternalObject
   {
      public SdtGAMLoginAdditionalParameters( )
      {
         initialize();
      }

      public SdtGAMLoginAdditionalParameters( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMLoginAdditionalParameters_externalReference == null )
         {
            GAMLoginAdditionalParameters_externalReference = new Artech.Security.GAMLoginAdditionalParameters(context);
         }
         returntostring = "";
         returntostring = (String)(GAMLoginAdditionalParameters_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Authenticationtypename
      {
         get {
            if ( GAMLoginAdditionalParameters_externalReference == null )
            {
               GAMLoginAdditionalParameters_externalReference = new Artech.Security.GAMLoginAdditionalParameters(context);
            }
            return GAMLoginAdditionalParameters_externalReference.AuthenticationTypeName ;
         }

         set {
            if ( GAMLoginAdditionalParameters_externalReference == null )
            {
               GAMLoginAdditionalParameters_externalReference = new Artech.Security.GAMLoginAdditionalParameters(context);
            }
            GAMLoginAdditionalParameters_externalReference.AuthenticationTypeName = value;
         }

      }

      public short gxTpr_Rememberusertype
      {
         get {
            if ( GAMLoginAdditionalParameters_externalReference == null )
            {
               GAMLoginAdditionalParameters_externalReference = new Artech.Security.GAMLoginAdditionalParameters(context);
            }
            return GAMLoginAdditionalParameters_externalReference.RememberUserType ;
         }

         set {
            if ( GAMLoginAdditionalParameters_externalReference == null )
            {
               GAMLoginAdditionalParameters_externalReference = new Artech.Security.GAMLoginAdditionalParameters(context);
            }
            GAMLoginAdditionalParameters_externalReference.RememberUserType = value;
         }

      }

      public bool gxTpr_Isbatch
      {
         get {
            if ( GAMLoginAdditionalParameters_externalReference == null )
            {
               GAMLoginAdditionalParameters_externalReference = new Artech.Security.GAMLoginAdditionalParameters(context);
            }
            return GAMLoginAdditionalParameters_externalReference.isBatch ;
         }

         set {
            if ( GAMLoginAdditionalParameters_externalReference == null )
            {
               GAMLoginAdditionalParameters_externalReference = new Artech.Security.GAMLoginAdditionalParameters(context);
            }
            GAMLoginAdditionalParameters_externalReference.isBatch = value;
         }

      }

      public IGxCollection gxTpr_Properties
      {
         get {
            if ( GAMLoginAdditionalParameters_externalReference == null )
            {
               GAMLoginAdditionalParameters_externalReference = new Artech.Security.GAMLoginAdditionalParameters(context);
            }
            IGxCollection intValue ;
            intValue = new GxExternalCollection( context, "SdtGAMProperty", "GeneXus.Programs");
            System.Collections.Generic.List<Artech.Security.GAMProperty> externalParm0 ;
            externalParm0 = GAMLoginAdditionalParameters_externalReference.Properties;
            intValue.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMProperty>), externalParm0);
            return intValue ;
         }

         set {
            if ( GAMLoginAdditionalParameters_externalReference == null )
            {
               GAMLoginAdditionalParameters_externalReference = new Artech.Security.GAMLoginAdditionalParameters(context);
            }
            IGxCollection intValue ;
            System.Collections.Generic.List<Artech.Security.GAMProperty> externalParm1 ;
            intValue = value;
            externalParm1 = (System.Collections.Generic.List<Artech.Security.GAMProperty>)CollectionUtils.ConvertToExternal( typeof(System.Collections.Generic.List<Artech.Security.GAMProperty>), intValue.ExternalInstance);
            GAMLoginAdditionalParameters_externalReference.Properties = externalParm1;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMLoginAdditionalParameters_externalReference == null )
            {
               GAMLoginAdditionalParameters_externalReference = new Artech.Security.GAMLoginAdditionalParameters(context);
            }
            return GAMLoginAdditionalParameters_externalReference ;
         }

         set {
            GAMLoginAdditionalParameters_externalReference = (Artech.Security.GAMLoginAdditionalParameters)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMLoginAdditionalParameters GAMLoginAdditionalParameters_externalReference=null ;
   }

}
