/*
               File: GetPromptTipoDocumentoFilterData
        Description: Get Prompt Tipo Documento Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:54:5.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getprompttipodocumentofilterdata : GXProcedure
   {
      public getprompttipodocumentofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getprompttipodocumentofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV15DDOName = aP0_DDOName;
         this.AV13SearchTxt = aP1_SearchTxt;
         this.AV14SearchTxtTo = aP2_SearchTxtTo;
         this.AV19OptionsJson = "" ;
         this.AV22OptionsDescJson = "" ;
         this.AV24OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV15DDOName = aP0_DDOName;
         this.AV13SearchTxt = aP1_SearchTxt;
         this.AV14SearchTxtTo = aP2_SearchTxtTo;
         this.AV19OptionsJson = "" ;
         this.AV22OptionsDescJson = "" ;
         this.AV24OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
         return AV24OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getprompttipodocumentofilterdata objgetprompttipodocumentofilterdata;
         objgetprompttipodocumentofilterdata = new getprompttipodocumentofilterdata();
         objgetprompttipodocumentofilterdata.AV15DDOName = aP0_DDOName;
         objgetprompttipodocumentofilterdata.AV13SearchTxt = aP1_SearchTxt;
         objgetprompttipodocumentofilterdata.AV14SearchTxtTo = aP2_SearchTxtTo;
         objgetprompttipodocumentofilterdata.AV19OptionsJson = "" ;
         objgetprompttipodocumentofilterdata.AV22OptionsDescJson = "" ;
         objgetprompttipodocumentofilterdata.AV24OptionIndexesJson = "" ;
         objgetprompttipodocumentofilterdata.context.SetSubmitInitialConfig(context);
         objgetprompttipodocumentofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetprompttipodocumentofilterdata);
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getprompttipodocumentofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV18Options = (IGxCollection)(new GxSimpleCollection());
         AV21OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV23OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV15DDOName), "DDO_TIPODOCUMENTO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADTIPODOCUMENTO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV19OptionsJson = AV18Options.ToJSonString(false);
         AV22OptionsDescJson = AV21OptionsDesc.ToJSonString(false);
         AV24OptionIndexesJson = AV23OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV26Session.Get("PromptTipoDocumentoGridState"), "") == 0 )
         {
            AV28GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptTipoDocumentoGridState"), "");
         }
         else
         {
            AV28GridState.FromXml(AV26Session.Get("PromptTipoDocumentoGridState"), "");
         }
         AV42GXV1 = 1;
         while ( AV42GXV1 <= AV28GridState.gxTpr_Filtervalues.Count )
         {
            AV29GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV28GridState.gxTpr_Filtervalues.Item(AV42GXV1));
            if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TIPODOCUMENTO_ATIVO") == 0 )
            {
               AV31TipoDocumento_Ativo = BooleanUtil.Val( AV29GridStateFilterValue.gxTpr_Value);
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFTIPODOCUMENTO_NOME") == 0 )
            {
               AV10TFTipoDocumento_Nome = AV29GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFTIPODOCUMENTO_NOME_SEL") == 0 )
            {
               AV11TFTipoDocumento_Nome_Sel = AV29GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFTIPODOCUMENTO_ATIVO_SEL") == 0 )
            {
               AV12TFTipoDocumento_Ativo_Sel = (short)(NumberUtil.Val( AV29GridStateFilterValue.gxTpr_Value, "."));
            }
            AV42GXV1 = (int)(AV42GXV1+1);
         }
         if ( AV28GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV30GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV28GridState.gxTpr_Dynamicfilters.Item(1));
            AV32DynamicFiltersSelector1 = AV30GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "TIPODOCUMENTO_NOME") == 0 )
            {
               AV33TipoDocumento_Nome1 = AV30GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV28GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV34DynamicFiltersEnabled2 = true;
               AV30GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV28GridState.gxTpr_Dynamicfilters.Item(2));
               AV35DynamicFiltersSelector2 = AV30GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "TIPODOCUMENTO_NOME") == 0 )
               {
                  AV36TipoDocumento_Nome2 = AV30GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV28GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV37DynamicFiltersEnabled3 = true;
                  AV30GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV28GridState.gxTpr_Dynamicfilters.Item(3));
                  AV38DynamicFiltersSelector3 = AV30GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV38DynamicFiltersSelector3, "TIPODOCUMENTO_NOME") == 0 )
                  {
                     AV39TipoDocumento_Nome3 = AV30GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADTIPODOCUMENTO_NOMEOPTIONS' Routine */
         AV10TFTipoDocumento_Nome = AV13SearchTxt;
         AV11TFTipoDocumento_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV32DynamicFiltersSelector1 ,
                                              AV33TipoDocumento_Nome1 ,
                                              AV34DynamicFiltersEnabled2 ,
                                              AV35DynamicFiltersSelector2 ,
                                              AV36TipoDocumento_Nome2 ,
                                              AV37DynamicFiltersEnabled3 ,
                                              AV38DynamicFiltersSelector3 ,
                                              AV39TipoDocumento_Nome3 ,
                                              AV11TFTipoDocumento_Nome_Sel ,
                                              AV10TFTipoDocumento_Nome ,
                                              AV12TFTipoDocumento_Ativo_Sel ,
                                              A646TipoDocumento_Nome ,
                                              A647TipoDocumento_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV33TipoDocumento_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV33TipoDocumento_Nome1), 50, "%");
         lV36TipoDocumento_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV36TipoDocumento_Nome2), 50, "%");
         lV39TipoDocumento_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV39TipoDocumento_Nome3), 50, "%");
         lV10TFTipoDocumento_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFTipoDocumento_Nome), 50, "%");
         /* Using cursor P00O12 */
         pr_default.execute(0, new Object[] {lV33TipoDocumento_Nome1, lV36TipoDocumento_Nome2, lV39TipoDocumento_Nome3, lV10TFTipoDocumento_Nome, AV11TFTipoDocumento_Nome_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKO12 = false;
            A647TipoDocumento_Ativo = P00O12_A647TipoDocumento_Ativo[0];
            A646TipoDocumento_Nome = P00O12_A646TipoDocumento_Nome[0];
            A645TipoDocumento_Codigo = P00O12_A645TipoDocumento_Codigo[0];
            AV25count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00O12_A646TipoDocumento_Nome[0], A646TipoDocumento_Nome) == 0 ) )
            {
               BRKO12 = false;
               A645TipoDocumento_Codigo = P00O12_A645TipoDocumento_Codigo[0];
               AV25count = (long)(AV25count+1);
               BRKO12 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A646TipoDocumento_Nome)) )
            {
               AV17Option = A646TipoDocumento_Nome;
               AV18Options.Add(AV17Option, 0);
               AV23OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV25count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV18Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKO12 )
            {
               BRKO12 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV18Options = new GxSimpleCollection();
         AV21OptionsDesc = new GxSimpleCollection();
         AV23OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV26Session = context.GetSession();
         AV28GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV29GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV31TipoDocumento_Ativo = true;
         AV10TFTipoDocumento_Nome = "";
         AV11TFTipoDocumento_Nome_Sel = "";
         AV30GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV32DynamicFiltersSelector1 = "";
         AV33TipoDocumento_Nome1 = "";
         AV35DynamicFiltersSelector2 = "";
         AV36TipoDocumento_Nome2 = "";
         AV38DynamicFiltersSelector3 = "";
         AV39TipoDocumento_Nome3 = "";
         scmdbuf = "";
         lV33TipoDocumento_Nome1 = "";
         lV36TipoDocumento_Nome2 = "";
         lV39TipoDocumento_Nome3 = "";
         lV10TFTipoDocumento_Nome = "";
         A646TipoDocumento_Nome = "";
         P00O12_A647TipoDocumento_Ativo = new bool[] {false} ;
         P00O12_A646TipoDocumento_Nome = new String[] {""} ;
         P00O12_A645TipoDocumento_Codigo = new int[1] ;
         AV17Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getprompttipodocumentofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00O12_A647TipoDocumento_Ativo, P00O12_A646TipoDocumento_Nome, P00O12_A645TipoDocumento_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV12TFTipoDocumento_Ativo_Sel ;
      private int AV42GXV1 ;
      private int A645TipoDocumento_Codigo ;
      private long AV25count ;
      private String AV10TFTipoDocumento_Nome ;
      private String AV11TFTipoDocumento_Nome_Sel ;
      private String AV33TipoDocumento_Nome1 ;
      private String AV36TipoDocumento_Nome2 ;
      private String AV39TipoDocumento_Nome3 ;
      private String scmdbuf ;
      private String lV33TipoDocumento_Nome1 ;
      private String lV36TipoDocumento_Nome2 ;
      private String lV39TipoDocumento_Nome3 ;
      private String lV10TFTipoDocumento_Nome ;
      private String A646TipoDocumento_Nome ;
      private bool returnInSub ;
      private bool AV31TipoDocumento_Ativo ;
      private bool AV34DynamicFiltersEnabled2 ;
      private bool AV37DynamicFiltersEnabled3 ;
      private bool A647TipoDocumento_Ativo ;
      private bool BRKO12 ;
      private String AV24OptionIndexesJson ;
      private String AV19OptionsJson ;
      private String AV22OptionsDescJson ;
      private String AV15DDOName ;
      private String AV13SearchTxt ;
      private String AV14SearchTxtTo ;
      private String AV32DynamicFiltersSelector1 ;
      private String AV35DynamicFiltersSelector2 ;
      private String AV38DynamicFiltersSelector3 ;
      private String AV17Option ;
      private IGxSession AV26Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private bool[] P00O12_A647TipoDocumento_Ativo ;
      private String[] P00O12_A646TipoDocumento_Nome ;
      private int[] P00O12_A645TipoDocumento_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV18Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV28GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV29GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV30GridStateDynamicFilter ;
   }

   public class getprompttipodocumentofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00O12( IGxContext context ,
                                             String AV32DynamicFiltersSelector1 ,
                                             String AV33TipoDocumento_Nome1 ,
                                             bool AV34DynamicFiltersEnabled2 ,
                                             String AV35DynamicFiltersSelector2 ,
                                             String AV36TipoDocumento_Nome2 ,
                                             bool AV37DynamicFiltersEnabled3 ,
                                             String AV38DynamicFiltersSelector3 ,
                                             String AV39TipoDocumento_Nome3 ,
                                             String AV11TFTipoDocumento_Nome_Sel ,
                                             String AV10TFTipoDocumento_Nome ,
                                             short AV12TFTipoDocumento_Ativo_Sel ,
                                             String A646TipoDocumento_Nome ,
                                             bool A647TipoDocumento_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [5] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [TipoDocumento_Ativo], [TipoDocumento_Nome], [TipoDocumento_Codigo] FROM [TipoDocumento] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([TipoDocumento_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "TIPODOCUMENTO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TipoDocumento_Nome1)) ) )
         {
            sWhereString = sWhereString + " and ([TipoDocumento_Nome] like '%' + @lV33TipoDocumento_Nome1 + '%')";
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( AV34DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV35DynamicFiltersSelector2, "TIPODOCUMENTO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TipoDocumento_Nome2)) ) )
         {
            sWhereString = sWhereString + " and ([TipoDocumento_Nome] like '%' + @lV36TipoDocumento_Nome2 + '%')";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV37DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV38DynamicFiltersSelector3, "TIPODOCUMENTO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TipoDocumento_Nome3)) ) )
         {
            sWhereString = sWhereString + " and ([TipoDocumento_Nome] like '%' + @lV39TipoDocumento_Nome3 + '%')";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFTipoDocumento_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFTipoDocumento_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([TipoDocumento_Nome] like @lV10TFTipoDocumento_Nome)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFTipoDocumento_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([TipoDocumento_Nome] = @AV11TFTipoDocumento_Nome_Sel)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV12TFTipoDocumento_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and ([TipoDocumento_Ativo] = 1)";
         }
         if ( AV12TFTipoDocumento_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and ([TipoDocumento_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [TipoDocumento_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00O12(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (short)dynConstraints[10] , (String)dynConstraints[11] , (bool)dynConstraints[12] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00O12 ;
          prmP00O12 = new Object[] {
          new Object[] {"@lV33TipoDocumento_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV36TipoDocumento_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV39TipoDocumento_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFTipoDocumento_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFTipoDocumento_Nome_Sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00O12", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00O12,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getprompttipodocumentofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getprompttipodocumentofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getprompttipodocumentofilterdata") )
          {
             return  ;
          }
          getprompttipodocumentofilterdata worker = new getprompttipodocumentofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
