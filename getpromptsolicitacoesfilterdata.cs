/*
               File: GetPromptSolicitacoesFilterData
        Description: Get Prompt Solicitacoes Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:53:57.52
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptsolicitacoesfilterdata : GXProcedure
   {
      public getpromptsolicitacoesfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptsolicitacoesfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV33DDOName = aP0_DDOName;
         this.AV31SearchTxt = aP1_SearchTxt;
         this.AV32SearchTxtTo = aP2_SearchTxtTo;
         this.AV37OptionsJson = "" ;
         this.AV40OptionsDescJson = "" ;
         this.AV42OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV37OptionsJson;
         aP4_OptionsDescJson=this.AV40OptionsDescJson;
         aP5_OptionIndexesJson=this.AV42OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV33DDOName = aP0_DDOName;
         this.AV31SearchTxt = aP1_SearchTxt;
         this.AV32SearchTxtTo = aP2_SearchTxtTo;
         this.AV37OptionsJson = "" ;
         this.AV40OptionsDescJson = "" ;
         this.AV42OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV37OptionsJson;
         aP4_OptionsDescJson=this.AV40OptionsDescJson;
         aP5_OptionIndexesJson=this.AV42OptionIndexesJson;
         return AV42OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptsolicitacoesfilterdata objgetpromptsolicitacoesfilterdata;
         objgetpromptsolicitacoesfilterdata = new getpromptsolicitacoesfilterdata();
         objgetpromptsolicitacoesfilterdata.AV33DDOName = aP0_DDOName;
         objgetpromptsolicitacoesfilterdata.AV31SearchTxt = aP1_SearchTxt;
         objgetpromptsolicitacoesfilterdata.AV32SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptsolicitacoesfilterdata.AV37OptionsJson = "" ;
         objgetpromptsolicitacoesfilterdata.AV40OptionsDescJson = "" ;
         objgetpromptsolicitacoesfilterdata.AV42OptionIndexesJson = "" ;
         objgetpromptsolicitacoesfilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptsolicitacoesfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptsolicitacoesfilterdata);
         aP3_OptionsJson=this.AV37OptionsJson;
         aP4_OptionsDescJson=this.AV40OptionsDescJson;
         aP5_OptionIndexesJson=this.AV42OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptsolicitacoesfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV36Options = (IGxCollection)(new GxSimpleCollection());
         AV39OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV41OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV33DDOName), "DDO_SOLICITACOES_NOVO_PROJETO") == 0 )
         {
            /* Execute user subroutine: 'LOADSOLICITACOES_NOVO_PROJETOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV33DDOName), "DDO_SOLICITACOES_OBJETIVO") == 0 )
         {
            /* Execute user subroutine: 'LOADSOLICITACOES_OBJETIVOOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV37OptionsJson = AV36Options.ToJSonString(false);
         AV40OptionsDescJson = AV39OptionsDesc.ToJSonString(false);
         AV42OptionIndexesJson = AV41OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV44Session.Get("PromptSolicitacoesGridState"), "") == 0 )
         {
            AV46GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptSolicitacoesGridState"), "");
         }
         else
         {
            AV46GridState.FromXml(AV44Session.Get("PromptSolicitacoesGridState"), "");
         }
         AV62GXV1 = 1;
         while ( AV62GXV1 <= AV46GridState.gxTpr_Filtervalues.Count )
         {
            AV47GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV46GridState.gxTpr_Filtervalues.Item(AV62GXV1));
            if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFSOLICITACOES_CODIGO") == 0 )
            {
               AV10TFSolicitacoes_Codigo = (int)(NumberUtil.Val( AV47GridStateFilterValue.gxTpr_Value, "."));
               AV11TFSolicitacoes_Codigo_To = (int)(NumberUtil.Val( AV47GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_CODIGO") == 0 )
            {
               AV12TFContratada_Codigo = (int)(NumberUtil.Val( AV47GridStateFilterValue.gxTpr_Value, "."));
               AV13TFContratada_Codigo_To = (int)(NumberUtil.Val( AV47GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFSISTEMA_CODIGO") == 0 )
            {
               AV14TFSistema_Codigo = (int)(NumberUtil.Val( AV47GridStateFilterValue.gxTpr_Value, "."));
               AV15TFSistema_Codigo_To = (int)(NumberUtil.Val( AV47GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFSERVICO_CODIGO") == 0 )
            {
               AV16TFServico_Codigo = (int)(NumberUtil.Val( AV47GridStateFilterValue.gxTpr_Value, "."));
               AV17TFServico_Codigo_To = (int)(NumberUtil.Val( AV47GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFSOLICITACOES_NOVO_PROJETO_SEL") == 0 )
            {
               AV18TFSolicitacoes_Novo_Projeto_Sel = AV47GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFSOLICITACOES_OBJETIVO") == 0 )
            {
               AV19TFSolicitacoes_Objetivo = AV47GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFSOLICITACOES_OBJETIVO_SEL") == 0 )
            {
               AV20TFSolicitacoes_Objetivo_Sel = AV47GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFSOLICITACOES_USUARIO") == 0 )
            {
               AV21TFSolicitacoes_Usuario = (int)(NumberUtil.Val( AV47GridStateFilterValue.gxTpr_Value, "."));
               AV22TFSolicitacoes_Usuario_To = (int)(NumberUtil.Val( AV47GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFSOLICITACOES_DATA") == 0 )
            {
               AV23TFSolicitacoes_Data = context.localUtil.CToT( AV47GridStateFilterValue.gxTpr_Value, 2);
               AV24TFSolicitacoes_Data_To = context.localUtil.CToT( AV47GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFSOLICITACOES_USUARIO_ULT") == 0 )
            {
               AV25TFSolicitacoes_Usuario_Ult = (int)(NumberUtil.Val( AV47GridStateFilterValue.gxTpr_Value, "."));
               AV26TFSolicitacoes_Usuario_Ult_To = (int)(NumberUtil.Val( AV47GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFSOLICITACOES_DATA_ULT") == 0 )
            {
               AV27TFSolicitacoes_Data_Ult = context.localUtil.CToT( AV47GridStateFilterValue.gxTpr_Value, 2);
               AV28TFSolicitacoes_Data_Ult_To = context.localUtil.CToT( AV47GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV47GridStateFilterValue.gxTpr_Name, "TFSOLICITACOES_STATUS_SEL") == 0 )
            {
               AV29TFSolicitacoes_Status_SelsJson = AV47GridStateFilterValue.gxTpr_Value;
               AV30TFSolicitacoes_Status_Sels.FromJSonString(AV29TFSolicitacoes_Status_SelsJson);
            }
            AV62GXV1 = (int)(AV62GXV1+1);
         }
         if ( AV46GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV48GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV46GridState.gxTpr_Dynamicfilters.Item(1));
            AV49DynamicFiltersSelector1 = AV48GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV49DynamicFiltersSelector1, "SOLICITACOES_NOVO_PROJETO") == 0 )
            {
               AV50DynamicFiltersOperator1 = AV48GridStateDynamicFilter.gxTpr_Operator;
               AV51Solicitacoes_Novo_Projeto1 = AV48GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV46GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV52DynamicFiltersEnabled2 = true;
               AV48GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV46GridState.gxTpr_Dynamicfilters.Item(2));
               AV53DynamicFiltersSelector2 = AV48GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV53DynamicFiltersSelector2, "SOLICITACOES_NOVO_PROJETO") == 0 )
               {
                  AV54DynamicFiltersOperator2 = AV48GridStateDynamicFilter.gxTpr_Operator;
                  AV55Solicitacoes_Novo_Projeto2 = AV48GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV46GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV56DynamicFiltersEnabled3 = true;
                  AV48GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV46GridState.gxTpr_Dynamicfilters.Item(3));
                  AV57DynamicFiltersSelector3 = AV48GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "SOLICITACOES_NOVO_PROJETO") == 0 )
                  {
                     AV58DynamicFiltersOperator3 = AV48GridStateDynamicFilter.gxTpr_Operator;
                     AV59Solicitacoes_Novo_Projeto3 = AV48GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADSOLICITACOES_NOVO_PROJETOOPTIONS' Routine */
         AV18TFSolicitacoes_Novo_Projeto_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A446Solicitacoes_Status ,
                                              AV30TFSolicitacoes_Status_Sels ,
                                              AV49DynamicFiltersSelector1 ,
                                              AV50DynamicFiltersOperator1 ,
                                              AV51Solicitacoes_Novo_Projeto1 ,
                                              AV52DynamicFiltersEnabled2 ,
                                              AV53DynamicFiltersSelector2 ,
                                              AV54DynamicFiltersOperator2 ,
                                              AV55Solicitacoes_Novo_Projeto2 ,
                                              AV56DynamicFiltersEnabled3 ,
                                              AV57DynamicFiltersSelector3 ,
                                              AV58DynamicFiltersOperator3 ,
                                              AV59Solicitacoes_Novo_Projeto3 ,
                                              AV10TFSolicitacoes_Codigo ,
                                              AV11TFSolicitacoes_Codigo_To ,
                                              AV12TFContratada_Codigo ,
                                              AV13TFContratada_Codigo_To ,
                                              AV14TFSistema_Codigo ,
                                              AV15TFSistema_Codigo_To ,
                                              AV16TFServico_Codigo ,
                                              AV17TFServico_Codigo_To ,
                                              AV18TFSolicitacoes_Novo_Projeto_Sel ,
                                              AV20TFSolicitacoes_Objetivo_Sel ,
                                              AV19TFSolicitacoes_Objetivo ,
                                              AV21TFSolicitacoes_Usuario ,
                                              AV22TFSolicitacoes_Usuario_To ,
                                              AV23TFSolicitacoes_Data ,
                                              AV24TFSolicitacoes_Data_To ,
                                              AV25TFSolicitacoes_Usuario_Ult ,
                                              AV26TFSolicitacoes_Usuario_Ult_To ,
                                              AV27TFSolicitacoes_Data_Ult ,
                                              AV28TFSolicitacoes_Data_Ult_To ,
                                              AV30TFSolicitacoes_Status_Sels.Count ,
                                              A440Solicitacoes_Novo_Projeto ,
                                              A439Solicitacoes_Codigo ,
                                              A39Contratada_Codigo ,
                                              A127Sistema_Codigo ,
                                              A155Servico_Codigo ,
                                              A441Solicitacoes_Objetivo ,
                                              A442Solicitacoes_Usuario ,
                                              A444Solicitacoes_Data ,
                                              A443Solicitacoes_Usuario_Ult ,
                                              A445Solicitacoes_Data_Ult },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.DATE
                                              }
         });
         lV51Solicitacoes_Novo_Projeto1 = StringUtil.PadR( StringUtil.RTrim( AV51Solicitacoes_Novo_Projeto1), 1, "%");
         lV51Solicitacoes_Novo_Projeto1 = StringUtil.PadR( StringUtil.RTrim( AV51Solicitacoes_Novo_Projeto1), 1, "%");
         lV55Solicitacoes_Novo_Projeto2 = StringUtil.PadR( StringUtil.RTrim( AV55Solicitacoes_Novo_Projeto2), 1, "%");
         lV55Solicitacoes_Novo_Projeto2 = StringUtil.PadR( StringUtil.RTrim( AV55Solicitacoes_Novo_Projeto2), 1, "%");
         lV59Solicitacoes_Novo_Projeto3 = StringUtil.PadR( StringUtil.RTrim( AV59Solicitacoes_Novo_Projeto3), 1, "%");
         lV59Solicitacoes_Novo_Projeto3 = StringUtil.PadR( StringUtil.RTrim( AV59Solicitacoes_Novo_Projeto3), 1, "%");
         lV19TFSolicitacoes_Objetivo = StringUtil.Concat( StringUtil.RTrim( AV19TFSolicitacoes_Objetivo), "%", "");
         /* Using cursor P00MG2 */
         pr_default.execute(0, new Object[] {lV51Solicitacoes_Novo_Projeto1, lV51Solicitacoes_Novo_Projeto1, lV55Solicitacoes_Novo_Projeto2, lV55Solicitacoes_Novo_Projeto2, lV59Solicitacoes_Novo_Projeto3, lV59Solicitacoes_Novo_Projeto3, AV10TFSolicitacoes_Codigo, AV11TFSolicitacoes_Codigo_To, AV12TFContratada_Codigo, AV13TFContratada_Codigo_To, AV14TFSistema_Codigo, AV15TFSistema_Codigo_To, AV16TFServico_Codigo, AV17TFServico_Codigo_To, AV18TFSolicitacoes_Novo_Projeto_Sel, lV19TFSolicitacoes_Objetivo, AV20TFSolicitacoes_Objetivo_Sel, AV21TFSolicitacoes_Usuario, AV22TFSolicitacoes_Usuario_To, AV23TFSolicitacoes_Data, AV24TFSolicitacoes_Data_To, AV25TFSolicitacoes_Usuario_Ult, AV26TFSolicitacoes_Usuario_Ult_To, AV27TFSolicitacoes_Data_Ult, AV28TFSolicitacoes_Data_Ult_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKMG2 = false;
            A440Solicitacoes_Novo_Projeto = P00MG2_A440Solicitacoes_Novo_Projeto[0];
            A446Solicitacoes_Status = P00MG2_A446Solicitacoes_Status[0];
            A445Solicitacoes_Data_Ult = P00MG2_A445Solicitacoes_Data_Ult[0];
            A443Solicitacoes_Usuario_Ult = P00MG2_A443Solicitacoes_Usuario_Ult[0];
            A444Solicitacoes_Data = P00MG2_A444Solicitacoes_Data[0];
            A442Solicitacoes_Usuario = P00MG2_A442Solicitacoes_Usuario[0];
            A441Solicitacoes_Objetivo = P00MG2_A441Solicitacoes_Objetivo[0];
            n441Solicitacoes_Objetivo = P00MG2_n441Solicitacoes_Objetivo[0];
            A155Servico_Codigo = P00MG2_A155Servico_Codigo[0];
            A127Sistema_Codigo = P00MG2_A127Sistema_Codigo[0];
            A39Contratada_Codigo = P00MG2_A39Contratada_Codigo[0];
            A439Solicitacoes_Codigo = P00MG2_A439Solicitacoes_Codigo[0];
            AV43count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00MG2_A440Solicitacoes_Novo_Projeto[0], A440Solicitacoes_Novo_Projeto) == 0 ) )
            {
               BRKMG2 = false;
               A439Solicitacoes_Codigo = P00MG2_A439Solicitacoes_Codigo[0];
               AV43count = (long)(AV43count+1);
               BRKMG2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A440Solicitacoes_Novo_Projeto)) )
            {
               AV35Option = A440Solicitacoes_Novo_Projeto;
               if ( StringUtil.StrCmp(StringUtil.Trim( A440Solicitacoes_Novo_Projeto), "1") == 0 )
               {
                  AV38OptionDesc = "Sim";
               }
               else if ( StringUtil.StrCmp(StringUtil.Trim( A440Solicitacoes_Novo_Projeto), "0") == 0 )
               {
                  AV38OptionDesc = "N�o";
               }
               AV36Options.Add(AV35Option, 0);
               AV39OptionsDesc.Add(AV38OptionDesc, 0);
               AV41OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV43count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV36Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKMG2 )
            {
               BRKMG2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADSOLICITACOES_OBJETIVOOPTIONS' Routine */
         AV19TFSolicitacoes_Objetivo = AV31SearchTxt;
         AV20TFSolicitacoes_Objetivo_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A446Solicitacoes_Status ,
                                              AV30TFSolicitacoes_Status_Sels ,
                                              AV49DynamicFiltersSelector1 ,
                                              AV50DynamicFiltersOperator1 ,
                                              AV51Solicitacoes_Novo_Projeto1 ,
                                              AV52DynamicFiltersEnabled2 ,
                                              AV53DynamicFiltersSelector2 ,
                                              AV54DynamicFiltersOperator2 ,
                                              AV55Solicitacoes_Novo_Projeto2 ,
                                              AV56DynamicFiltersEnabled3 ,
                                              AV57DynamicFiltersSelector3 ,
                                              AV58DynamicFiltersOperator3 ,
                                              AV59Solicitacoes_Novo_Projeto3 ,
                                              AV10TFSolicitacoes_Codigo ,
                                              AV11TFSolicitacoes_Codigo_To ,
                                              AV12TFContratada_Codigo ,
                                              AV13TFContratada_Codigo_To ,
                                              AV14TFSistema_Codigo ,
                                              AV15TFSistema_Codigo_To ,
                                              AV16TFServico_Codigo ,
                                              AV17TFServico_Codigo_To ,
                                              AV18TFSolicitacoes_Novo_Projeto_Sel ,
                                              AV20TFSolicitacoes_Objetivo_Sel ,
                                              AV19TFSolicitacoes_Objetivo ,
                                              AV21TFSolicitacoes_Usuario ,
                                              AV22TFSolicitacoes_Usuario_To ,
                                              AV23TFSolicitacoes_Data ,
                                              AV24TFSolicitacoes_Data_To ,
                                              AV25TFSolicitacoes_Usuario_Ult ,
                                              AV26TFSolicitacoes_Usuario_Ult_To ,
                                              AV27TFSolicitacoes_Data_Ult ,
                                              AV28TFSolicitacoes_Data_Ult_To ,
                                              AV30TFSolicitacoes_Status_Sels.Count ,
                                              A440Solicitacoes_Novo_Projeto ,
                                              A439Solicitacoes_Codigo ,
                                              A39Contratada_Codigo ,
                                              A127Sistema_Codigo ,
                                              A155Servico_Codigo ,
                                              A441Solicitacoes_Objetivo ,
                                              A442Solicitacoes_Usuario ,
                                              A444Solicitacoes_Data ,
                                              A443Solicitacoes_Usuario_Ult ,
                                              A445Solicitacoes_Data_Ult },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.DATE
                                              }
         });
         lV51Solicitacoes_Novo_Projeto1 = StringUtil.PadR( StringUtil.RTrim( AV51Solicitacoes_Novo_Projeto1), 1, "%");
         lV51Solicitacoes_Novo_Projeto1 = StringUtil.PadR( StringUtil.RTrim( AV51Solicitacoes_Novo_Projeto1), 1, "%");
         lV55Solicitacoes_Novo_Projeto2 = StringUtil.PadR( StringUtil.RTrim( AV55Solicitacoes_Novo_Projeto2), 1, "%");
         lV55Solicitacoes_Novo_Projeto2 = StringUtil.PadR( StringUtil.RTrim( AV55Solicitacoes_Novo_Projeto2), 1, "%");
         lV59Solicitacoes_Novo_Projeto3 = StringUtil.PadR( StringUtil.RTrim( AV59Solicitacoes_Novo_Projeto3), 1, "%");
         lV59Solicitacoes_Novo_Projeto3 = StringUtil.PadR( StringUtil.RTrim( AV59Solicitacoes_Novo_Projeto3), 1, "%");
         lV19TFSolicitacoes_Objetivo = StringUtil.Concat( StringUtil.RTrim( AV19TFSolicitacoes_Objetivo), "%", "");
         /* Using cursor P00MG3 */
         pr_default.execute(1, new Object[] {lV51Solicitacoes_Novo_Projeto1, lV51Solicitacoes_Novo_Projeto1, lV55Solicitacoes_Novo_Projeto2, lV55Solicitacoes_Novo_Projeto2, lV59Solicitacoes_Novo_Projeto3, lV59Solicitacoes_Novo_Projeto3, AV10TFSolicitacoes_Codigo, AV11TFSolicitacoes_Codigo_To, AV12TFContratada_Codigo, AV13TFContratada_Codigo_To, AV14TFSistema_Codigo, AV15TFSistema_Codigo_To, AV16TFServico_Codigo, AV17TFServico_Codigo_To, AV18TFSolicitacoes_Novo_Projeto_Sel, lV19TFSolicitacoes_Objetivo, AV20TFSolicitacoes_Objetivo_Sel, AV21TFSolicitacoes_Usuario, AV22TFSolicitacoes_Usuario_To, AV23TFSolicitacoes_Data, AV24TFSolicitacoes_Data_To, AV25TFSolicitacoes_Usuario_Ult, AV26TFSolicitacoes_Usuario_Ult_To, AV27TFSolicitacoes_Data_Ult, AV28TFSolicitacoes_Data_Ult_To});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKMG4 = false;
            A441Solicitacoes_Objetivo = P00MG3_A441Solicitacoes_Objetivo[0];
            n441Solicitacoes_Objetivo = P00MG3_n441Solicitacoes_Objetivo[0];
            A446Solicitacoes_Status = P00MG3_A446Solicitacoes_Status[0];
            A445Solicitacoes_Data_Ult = P00MG3_A445Solicitacoes_Data_Ult[0];
            A443Solicitacoes_Usuario_Ult = P00MG3_A443Solicitacoes_Usuario_Ult[0];
            A444Solicitacoes_Data = P00MG3_A444Solicitacoes_Data[0];
            A442Solicitacoes_Usuario = P00MG3_A442Solicitacoes_Usuario[0];
            A155Servico_Codigo = P00MG3_A155Servico_Codigo[0];
            A127Sistema_Codigo = P00MG3_A127Sistema_Codigo[0];
            A39Contratada_Codigo = P00MG3_A39Contratada_Codigo[0];
            A439Solicitacoes_Codigo = P00MG3_A439Solicitacoes_Codigo[0];
            A440Solicitacoes_Novo_Projeto = P00MG3_A440Solicitacoes_Novo_Projeto[0];
            AV43count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00MG3_A441Solicitacoes_Objetivo[0], A441Solicitacoes_Objetivo) == 0 ) )
            {
               BRKMG4 = false;
               A439Solicitacoes_Codigo = P00MG3_A439Solicitacoes_Codigo[0];
               AV43count = (long)(AV43count+1);
               BRKMG4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A441Solicitacoes_Objetivo)) )
            {
               AV35Option = A441Solicitacoes_Objetivo;
               AV36Options.Add(AV35Option, 0);
               AV41OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV43count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV36Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKMG4 )
            {
               BRKMG4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV36Options = new GxSimpleCollection();
         AV39OptionsDesc = new GxSimpleCollection();
         AV41OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV44Session = context.GetSession();
         AV46GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV47GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV18TFSolicitacoes_Novo_Projeto_Sel = "";
         AV19TFSolicitacoes_Objetivo = "";
         AV20TFSolicitacoes_Objetivo_Sel = "";
         AV23TFSolicitacoes_Data = (DateTime)(DateTime.MinValue);
         AV24TFSolicitacoes_Data_To = (DateTime)(DateTime.MinValue);
         AV27TFSolicitacoes_Data_Ult = (DateTime)(DateTime.MinValue);
         AV28TFSolicitacoes_Data_Ult_To = (DateTime)(DateTime.MinValue);
         AV29TFSolicitacoes_Status_SelsJson = "";
         AV30TFSolicitacoes_Status_Sels = new GxSimpleCollection();
         AV48GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV49DynamicFiltersSelector1 = "";
         AV51Solicitacoes_Novo_Projeto1 = "";
         AV53DynamicFiltersSelector2 = "";
         AV55Solicitacoes_Novo_Projeto2 = "";
         AV57DynamicFiltersSelector3 = "";
         AV59Solicitacoes_Novo_Projeto3 = "";
         scmdbuf = "";
         lV51Solicitacoes_Novo_Projeto1 = "";
         lV55Solicitacoes_Novo_Projeto2 = "";
         lV59Solicitacoes_Novo_Projeto3 = "";
         lV19TFSolicitacoes_Objetivo = "";
         A446Solicitacoes_Status = "";
         A440Solicitacoes_Novo_Projeto = "";
         A441Solicitacoes_Objetivo = "";
         A444Solicitacoes_Data = (DateTime)(DateTime.MinValue);
         A445Solicitacoes_Data_Ult = (DateTime)(DateTime.MinValue);
         P00MG2_A440Solicitacoes_Novo_Projeto = new String[] {""} ;
         P00MG2_A446Solicitacoes_Status = new String[] {""} ;
         P00MG2_A445Solicitacoes_Data_Ult = new DateTime[] {DateTime.MinValue} ;
         P00MG2_A443Solicitacoes_Usuario_Ult = new int[1] ;
         P00MG2_A444Solicitacoes_Data = new DateTime[] {DateTime.MinValue} ;
         P00MG2_A442Solicitacoes_Usuario = new int[1] ;
         P00MG2_A441Solicitacoes_Objetivo = new String[] {""} ;
         P00MG2_n441Solicitacoes_Objetivo = new bool[] {false} ;
         P00MG2_A155Servico_Codigo = new int[1] ;
         P00MG2_A127Sistema_Codigo = new int[1] ;
         P00MG2_A39Contratada_Codigo = new int[1] ;
         P00MG2_A439Solicitacoes_Codigo = new int[1] ;
         AV35Option = "";
         AV38OptionDesc = "";
         P00MG3_A441Solicitacoes_Objetivo = new String[] {""} ;
         P00MG3_n441Solicitacoes_Objetivo = new bool[] {false} ;
         P00MG3_A446Solicitacoes_Status = new String[] {""} ;
         P00MG3_A445Solicitacoes_Data_Ult = new DateTime[] {DateTime.MinValue} ;
         P00MG3_A443Solicitacoes_Usuario_Ult = new int[1] ;
         P00MG3_A444Solicitacoes_Data = new DateTime[] {DateTime.MinValue} ;
         P00MG3_A442Solicitacoes_Usuario = new int[1] ;
         P00MG3_A155Servico_Codigo = new int[1] ;
         P00MG3_A127Sistema_Codigo = new int[1] ;
         P00MG3_A39Contratada_Codigo = new int[1] ;
         P00MG3_A439Solicitacoes_Codigo = new int[1] ;
         P00MG3_A440Solicitacoes_Novo_Projeto = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptsolicitacoesfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00MG2_A440Solicitacoes_Novo_Projeto, P00MG2_A446Solicitacoes_Status, P00MG2_A445Solicitacoes_Data_Ult, P00MG2_A443Solicitacoes_Usuario_Ult, P00MG2_A444Solicitacoes_Data, P00MG2_A442Solicitacoes_Usuario, P00MG2_A441Solicitacoes_Objetivo, P00MG2_n441Solicitacoes_Objetivo, P00MG2_A155Servico_Codigo, P00MG2_A127Sistema_Codigo,
               P00MG2_A39Contratada_Codigo, P00MG2_A439Solicitacoes_Codigo
               }
               , new Object[] {
               P00MG3_A441Solicitacoes_Objetivo, P00MG3_n441Solicitacoes_Objetivo, P00MG3_A446Solicitacoes_Status, P00MG3_A445Solicitacoes_Data_Ult, P00MG3_A443Solicitacoes_Usuario_Ult, P00MG3_A444Solicitacoes_Data, P00MG3_A442Solicitacoes_Usuario, P00MG3_A155Servico_Codigo, P00MG3_A127Sistema_Codigo, P00MG3_A39Contratada_Codigo,
               P00MG3_A439Solicitacoes_Codigo, P00MG3_A440Solicitacoes_Novo_Projeto
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV50DynamicFiltersOperator1 ;
      private short AV54DynamicFiltersOperator2 ;
      private short AV58DynamicFiltersOperator3 ;
      private int AV62GXV1 ;
      private int AV10TFSolicitacoes_Codigo ;
      private int AV11TFSolicitacoes_Codigo_To ;
      private int AV12TFContratada_Codigo ;
      private int AV13TFContratada_Codigo_To ;
      private int AV14TFSistema_Codigo ;
      private int AV15TFSistema_Codigo_To ;
      private int AV16TFServico_Codigo ;
      private int AV17TFServico_Codigo_To ;
      private int AV21TFSolicitacoes_Usuario ;
      private int AV22TFSolicitacoes_Usuario_To ;
      private int AV25TFSolicitacoes_Usuario_Ult ;
      private int AV26TFSolicitacoes_Usuario_Ult_To ;
      private int AV30TFSolicitacoes_Status_Sels_Count ;
      private int A439Solicitacoes_Codigo ;
      private int A39Contratada_Codigo ;
      private int A127Sistema_Codigo ;
      private int A155Servico_Codigo ;
      private int A442Solicitacoes_Usuario ;
      private int A443Solicitacoes_Usuario_Ult ;
      private long AV43count ;
      private String AV18TFSolicitacoes_Novo_Projeto_Sel ;
      private String AV51Solicitacoes_Novo_Projeto1 ;
      private String AV55Solicitacoes_Novo_Projeto2 ;
      private String AV59Solicitacoes_Novo_Projeto3 ;
      private String scmdbuf ;
      private String lV51Solicitacoes_Novo_Projeto1 ;
      private String lV55Solicitacoes_Novo_Projeto2 ;
      private String lV59Solicitacoes_Novo_Projeto3 ;
      private String A446Solicitacoes_Status ;
      private String A440Solicitacoes_Novo_Projeto ;
      private DateTime AV23TFSolicitacoes_Data ;
      private DateTime AV24TFSolicitacoes_Data_To ;
      private DateTime AV27TFSolicitacoes_Data_Ult ;
      private DateTime AV28TFSolicitacoes_Data_Ult_To ;
      private DateTime A444Solicitacoes_Data ;
      private DateTime A445Solicitacoes_Data_Ult ;
      private bool returnInSub ;
      private bool AV52DynamicFiltersEnabled2 ;
      private bool AV56DynamicFiltersEnabled3 ;
      private bool BRKMG2 ;
      private bool n441Solicitacoes_Objetivo ;
      private bool BRKMG4 ;
      private String AV42OptionIndexesJson ;
      private String AV37OptionsJson ;
      private String AV40OptionsDescJson ;
      private String AV29TFSolicitacoes_Status_SelsJson ;
      private String A441Solicitacoes_Objetivo ;
      private String AV33DDOName ;
      private String AV31SearchTxt ;
      private String AV32SearchTxtTo ;
      private String AV19TFSolicitacoes_Objetivo ;
      private String AV20TFSolicitacoes_Objetivo_Sel ;
      private String AV49DynamicFiltersSelector1 ;
      private String AV53DynamicFiltersSelector2 ;
      private String AV57DynamicFiltersSelector3 ;
      private String lV19TFSolicitacoes_Objetivo ;
      private String AV35Option ;
      private String AV38OptionDesc ;
      private IGxSession AV44Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00MG2_A440Solicitacoes_Novo_Projeto ;
      private String[] P00MG2_A446Solicitacoes_Status ;
      private DateTime[] P00MG2_A445Solicitacoes_Data_Ult ;
      private int[] P00MG2_A443Solicitacoes_Usuario_Ult ;
      private DateTime[] P00MG2_A444Solicitacoes_Data ;
      private int[] P00MG2_A442Solicitacoes_Usuario ;
      private String[] P00MG2_A441Solicitacoes_Objetivo ;
      private bool[] P00MG2_n441Solicitacoes_Objetivo ;
      private int[] P00MG2_A155Servico_Codigo ;
      private int[] P00MG2_A127Sistema_Codigo ;
      private int[] P00MG2_A39Contratada_Codigo ;
      private int[] P00MG2_A439Solicitacoes_Codigo ;
      private String[] P00MG3_A441Solicitacoes_Objetivo ;
      private bool[] P00MG3_n441Solicitacoes_Objetivo ;
      private String[] P00MG3_A446Solicitacoes_Status ;
      private DateTime[] P00MG3_A445Solicitacoes_Data_Ult ;
      private int[] P00MG3_A443Solicitacoes_Usuario_Ult ;
      private DateTime[] P00MG3_A444Solicitacoes_Data ;
      private int[] P00MG3_A442Solicitacoes_Usuario ;
      private int[] P00MG3_A155Servico_Codigo ;
      private int[] P00MG3_A127Sistema_Codigo ;
      private int[] P00MG3_A39Contratada_Codigo ;
      private int[] P00MG3_A439Solicitacoes_Codigo ;
      private String[] P00MG3_A440Solicitacoes_Novo_Projeto ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30TFSolicitacoes_Status_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV36Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV39OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV41OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV46GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV47GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV48GridStateDynamicFilter ;
   }

   public class getpromptsolicitacoesfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00MG2( IGxContext context ,
                                             String A446Solicitacoes_Status ,
                                             IGxCollection AV30TFSolicitacoes_Status_Sels ,
                                             String AV49DynamicFiltersSelector1 ,
                                             short AV50DynamicFiltersOperator1 ,
                                             String AV51Solicitacoes_Novo_Projeto1 ,
                                             bool AV52DynamicFiltersEnabled2 ,
                                             String AV53DynamicFiltersSelector2 ,
                                             short AV54DynamicFiltersOperator2 ,
                                             String AV55Solicitacoes_Novo_Projeto2 ,
                                             bool AV56DynamicFiltersEnabled3 ,
                                             String AV57DynamicFiltersSelector3 ,
                                             short AV58DynamicFiltersOperator3 ,
                                             String AV59Solicitacoes_Novo_Projeto3 ,
                                             int AV10TFSolicitacoes_Codigo ,
                                             int AV11TFSolicitacoes_Codigo_To ,
                                             int AV12TFContratada_Codigo ,
                                             int AV13TFContratada_Codigo_To ,
                                             int AV14TFSistema_Codigo ,
                                             int AV15TFSistema_Codigo_To ,
                                             int AV16TFServico_Codigo ,
                                             int AV17TFServico_Codigo_To ,
                                             String AV18TFSolicitacoes_Novo_Projeto_Sel ,
                                             String AV20TFSolicitacoes_Objetivo_Sel ,
                                             String AV19TFSolicitacoes_Objetivo ,
                                             int AV21TFSolicitacoes_Usuario ,
                                             int AV22TFSolicitacoes_Usuario_To ,
                                             DateTime AV23TFSolicitacoes_Data ,
                                             DateTime AV24TFSolicitacoes_Data_To ,
                                             int AV25TFSolicitacoes_Usuario_Ult ,
                                             int AV26TFSolicitacoes_Usuario_Ult_To ,
                                             DateTime AV27TFSolicitacoes_Data_Ult ,
                                             DateTime AV28TFSolicitacoes_Data_Ult_To ,
                                             int AV30TFSolicitacoes_Status_Sels_Count ,
                                             String A440Solicitacoes_Novo_Projeto ,
                                             int A439Solicitacoes_Codigo ,
                                             int A39Contratada_Codigo ,
                                             int A127Sistema_Codigo ,
                                             int A155Servico_Codigo ,
                                             String A441Solicitacoes_Objetivo ,
                                             int A442Solicitacoes_Usuario ,
                                             DateTime A444Solicitacoes_Data ,
                                             int A443Solicitacoes_Usuario_Ult ,
                                             DateTime A445Solicitacoes_Data_Ult )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [25] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Solicitacoes_Novo_Projeto], [Solicitacoes_Status], [Solicitacoes_Data_Ult], [Solicitacoes_Usuario_Ult], [Solicitacoes_Data], [Solicitacoes_Usuario], [Solicitacoes_Objetivo], [Servico_Codigo], [Sistema_Codigo], [Contratada_Codigo], [Solicitacoes_Codigo] FROM [Solicitacoes] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV49DynamicFiltersSelector1, "SOLICITACOES_NOVO_PROJETO") == 0 ) && ( AV50DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51Solicitacoes_Novo_Projeto1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Novo_Projeto] like @lV51Solicitacoes_Novo_Projeto1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Novo_Projeto] like @lV51Solicitacoes_Novo_Projeto1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV49DynamicFiltersSelector1, "SOLICITACOES_NOVO_PROJETO") == 0 ) && ( AV50DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51Solicitacoes_Novo_Projeto1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Novo_Projeto] like '%' + @lV51Solicitacoes_Novo_Projeto1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Novo_Projeto] like '%' + @lV51Solicitacoes_Novo_Projeto1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV52DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV53DynamicFiltersSelector2, "SOLICITACOES_NOVO_PROJETO") == 0 ) && ( AV54DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55Solicitacoes_Novo_Projeto2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Novo_Projeto] like @lV55Solicitacoes_Novo_Projeto2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Novo_Projeto] like @lV55Solicitacoes_Novo_Projeto2)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV52DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV53DynamicFiltersSelector2, "SOLICITACOES_NOVO_PROJETO") == 0 ) && ( AV54DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55Solicitacoes_Novo_Projeto2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Novo_Projeto] like '%' + @lV55Solicitacoes_Novo_Projeto2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Novo_Projeto] like '%' + @lV55Solicitacoes_Novo_Projeto2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "SOLICITACOES_NOVO_PROJETO") == 0 ) && ( AV58DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59Solicitacoes_Novo_Projeto3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Novo_Projeto] like @lV59Solicitacoes_Novo_Projeto3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Novo_Projeto] like @lV59Solicitacoes_Novo_Projeto3)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "SOLICITACOES_NOVO_PROJETO") == 0 ) && ( AV58DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59Solicitacoes_Novo_Projeto3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Novo_Projeto] like '%' + @lV59Solicitacoes_Novo_Projeto3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Novo_Projeto] like '%' + @lV59Solicitacoes_Novo_Projeto3)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (0==AV10TFSolicitacoes_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Codigo] >= @AV10TFSolicitacoes_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Codigo] >= @AV10TFSolicitacoes_Codigo)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! (0==AV11TFSolicitacoes_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Codigo] <= @AV11TFSolicitacoes_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Codigo] <= @AV11TFSolicitacoes_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! (0==AV12TFContratada_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Contratada_Codigo] >= @AV12TFContratada_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Contratada_Codigo] >= @AV12TFContratada_Codigo)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (0==AV13TFContratada_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Contratada_Codigo] <= @AV13TFContratada_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Contratada_Codigo] <= @AV13TFContratada_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (0==AV14TFSistema_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Sistema_Codigo] >= @AV14TFSistema_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Sistema_Codigo] >= @AV14TFSistema_Codigo)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (0==AV15TFSistema_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Sistema_Codigo] <= @AV15TFSistema_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Sistema_Codigo] <= @AV15TFSistema_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (0==AV16TFServico_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Servico_Codigo] >= @AV16TFServico_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Servico_Codigo] >= @AV16TFServico_Codigo)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! (0==AV17TFServico_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Servico_Codigo] <= @AV17TFServico_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Servico_Codigo] <= @AV17TFServico_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFSolicitacoes_Novo_Projeto_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Novo_Projeto] = @AV18TFSolicitacoes_Novo_Projeto_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Novo_Projeto] = @AV18TFSolicitacoes_Novo_Projeto_Sel)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV20TFSolicitacoes_Objetivo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFSolicitacoes_Objetivo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Objetivo] like @lV19TFSolicitacoes_Objetivo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Objetivo] like @lV19TFSolicitacoes_Objetivo)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFSolicitacoes_Objetivo_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Objetivo] = @AV20TFSolicitacoes_Objetivo_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Objetivo] = @AV20TFSolicitacoes_Objetivo_Sel)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! (0==AV21TFSolicitacoes_Usuario) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Usuario] >= @AV21TFSolicitacoes_Usuario)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Usuario] >= @AV21TFSolicitacoes_Usuario)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! (0==AV22TFSolicitacoes_Usuario_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Usuario] <= @AV22TFSolicitacoes_Usuario_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Usuario] <= @AV22TFSolicitacoes_Usuario_To)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! (DateTime.MinValue==AV23TFSolicitacoes_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Data] >= @AV23TFSolicitacoes_Data)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Data] >= @AV23TFSolicitacoes_Data)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV24TFSolicitacoes_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Data] <= @AV24TFSolicitacoes_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Data] <= @AV24TFSolicitacoes_Data_To)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! (0==AV25TFSolicitacoes_Usuario_Ult) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Usuario_Ult] >= @AV25TFSolicitacoes_Usuario_Ult)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Usuario_Ult] >= @AV25TFSolicitacoes_Usuario_Ult)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( ! (0==AV26TFSolicitacoes_Usuario_Ult_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Usuario_Ult] <= @AV26TFSolicitacoes_Usuario_Ult_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Usuario_Ult] <= @AV26TFSolicitacoes_Usuario_Ult_To)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( ! (DateTime.MinValue==AV27TFSolicitacoes_Data_Ult) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Data_Ult] >= @AV27TFSolicitacoes_Data_Ult)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Data_Ult] >= @AV27TFSolicitacoes_Data_Ult)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( ! (DateTime.MinValue==AV28TFSolicitacoes_Data_Ult_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Data_Ult] <= @AV28TFSolicitacoes_Data_Ult_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Data_Ult] <= @AV28TFSolicitacoes_Data_Ult_To)";
            }
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( AV30TFSolicitacoes_Status_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV30TFSolicitacoes_Status_Sels, "[Solicitacoes_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV30TFSolicitacoes_Status_Sels, "[Solicitacoes_Status] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [Solicitacoes_Novo_Projeto]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00MG3( IGxContext context ,
                                             String A446Solicitacoes_Status ,
                                             IGxCollection AV30TFSolicitacoes_Status_Sels ,
                                             String AV49DynamicFiltersSelector1 ,
                                             short AV50DynamicFiltersOperator1 ,
                                             String AV51Solicitacoes_Novo_Projeto1 ,
                                             bool AV52DynamicFiltersEnabled2 ,
                                             String AV53DynamicFiltersSelector2 ,
                                             short AV54DynamicFiltersOperator2 ,
                                             String AV55Solicitacoes_Novo_Projeto2 ,
                                             bool AV56DynamicFiltersEnabled3 ,
                                             String AV57DynamicFiltersSelector3 ,
                                             short AV58DynamicFiltersOperator3 ,
                                             String AV59Solicitacoes_Novo_Projeto3 ,
                                             int AV10TFSolicitacoes_Codigo ,
                                             int AV11TFSolicitacoes_Codigo_To ,
                                             int AV12TFContratada_Codigo ,
                                             int AV13TFContratada_Codigo_To ,
                                             int AV14TFSistema_Codigo ,
                                             int AV15TFSistema_Codigo_To ,
                                             int AV16TFServico_Codigo ,
                                             int AV17TFServico_Codigo_To ,
                                             String AV18TFSolicitacoes_Novo_Projeto_Sel ,
                                             String AV20TFSolicitacoes_Objetivo_Sel ,
                                             String AV19TFSolicitacoes_Objetivo ,
                                             int AV21TFSolicitacoes_Usuario ,
                                             int AV22TFSolicitacoes_Usuario_To ,
                                             DateTime AV23TFSolicitacoes_Data ,
                                             DateTime AV24TFSolicitacoes_Data_To ,
                                             int AV25TFSolicitacoes_Usuario_Ult ,
                                             int AV26TFSolicitacoes_Usuario_Ult_To ,
                                             DateTime AV27TFSolicitacoes_Data_Ult ,
                                             DateTime AV28TFSolicitacoes_Data_Ult_To ,
                                             int AV30TFSolicitacoes_Status_Sels_Count ,
                                             String A440Solicitacoes_Novo_Projeto ,
                                             int A439Solicitacoes_Codigo ,
                                             int A39Contratada_Codigo ,
                                             int A127Sistema_Codigo ,
                                             int A155Servico_Codigo ,
                                             String A441Solicitacoes_Objetivo ,
                                             int A442Solicitacoes_Usuario ,
                                             DateTime A444Solicitacoes_Data ,
                                             int A443Solicitacoes_Usuario_Ult ,
                                             DateTime A445Solicitacoes_Data_Ult )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [25] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [Solicitacoes_Objetivo], [Solicitacoes_Status], [Solicitacoes_Data_Ult], [Solicitacoes_Usuario_Ult], [Solicitacoes_Data], [Solicitacoes_Usuario], [Servico_Codigo], [Sistema_Codigo], [Contratada_Codigo], [Solicitacoes_Codigo], [Solicitacoes_Novo_Projeto] FROM [Solicitacoes] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV49DynamicFiltersSelector1, "SOLICITACOES_NOVO_PROJETO") == 0 ) && ( AV50DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51Solicitacoes_Novo_Projeto1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Novo_Projeto] like @lV51Solicitacoes_Novo_Projeto1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Novo_Projeto] like @lV51Solicitacoes_Novo_Projeto1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV49DynamicFiltersSelector1, "SOLICITACOES_NOVO_PROJETO") == 0 ) && ( AV50DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51Solicitacoes_Novo_Projeto1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Novo_Projeto] like '%' + @lV51Solicitacoes_Novo_Projeto1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Novo_Projeto] like '%' + @lV51Solicitacoes_Novo_Projeto1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV52DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV53DynamicFiltersSelector2, "SOLICITACOES_NOVO_PROJETO") == 0 ) && ( AV54DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55Solicitacoes_Novo_Projeto2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Novo_Projeto] like @lV55Solicitacoes_Novo_Projeto2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Novo_Projeto] like @lV55Solicitacoes_Novo_Projeto2)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV52DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV53DynamicFiltersSelector2, "SOLICITACOES_NOVO_PROJETO") == 0 ) && ( AV54DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55Solicitacoes_Novo_Projeto2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Novo_Projeto] like '%' + @lV55Solicitacoes_Novo_Projeto2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Novo_Projeto] like '%' + @lV55Solicitacoes_Novo_Projeto2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "SOLICITACOES_NOVO_PROJETO") == 0 ) && ( AV58DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59Solicitacoes_Novo_Projeto3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Novo_Projeto] like @lV59Solicitacoes_Novo_Projeto3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Novo_Projeto] like @lV59Solicitacoes_Novo_Projeto3)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV56DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV57DynamicFiltersSelector3, "SOLICITACOES_NOVO_PROJETO") == 0 ) && ( AV58DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59Solicitacoes_Novo_Projeto3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Novo_Projeto] like '%' + @lV59Solicitacoes_Novo_Projeto3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Novo_Projeto] like '%' + @lV59Solicitacoes_Novo_Projeto3)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (0==AV10TFSolicitacoes_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Codigo] >= @AV10TFSolicitacoes_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Codigo] >= @AV10TFSolicitacoes_Codigo)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! (0==AV11TFSolicitacoes_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Codigo] <= @AV11TFSolicitacoes_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Codigo] <= @AV11TFSolicitacoes_Codigo_To)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! (0==AV12TFContratada_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Contratada_Codigo] >= @AV12TFContratada_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Contratada_Codigo] >= @AV12TFContratada_Codigo)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! (0==AV13TFContratada_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Contratada_Codigo] <= @AV13TFContratada_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Contratada_Codigo] <= @AV13TFContratada_Codigo_To)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! (0==AV14TFSistema_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Sistema_Codigo] >= @AV14TFSistema_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Sistema_Codigo] >= @AV14TFSistema_Codigo)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! (0==AV15TFSistema_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Sistema_Codigo] <= @AV15TFSistema_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Sistema_Codigo] <= @AV15TFSistema_Codigo_To)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! (0==AV16TFServico_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Servico_Codigo] >= @AV16TFServico_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Servico_Codigo] >= @AV16TFServico_Codigo)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! (0==AV17TFServico_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Servico_Codigo] <= @AV17TFServico_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Servico_Codigo] <= @AV17TFServico_Codigo_To)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFSolicitacoes_Novo_Projeto_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Novo_Projeto] = @AV18TFSolicitacoes_Novo_Projeto_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Novo_Projeto] = @AV18TFSolicitacoes_Novo_Projeto_Sel)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV20TFSolicitacoes_Objetivo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFSolicitacoes_Objetivo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Objetivo] like @lV19TFSolicitacoes_Objetivo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Objetivo] like @lV19TFSolicitacoes_Objetivo)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFSolicitacoes_Objetivo_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Objetivo] = @AV20TFSolicitacoes_Objetivo_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Objetivo] = @AV20TFSolicitacoes_Objetivo_Sel)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! (0==AV21TFSolicitacoes_Usuario) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Usuario] >= @AV21TFSolicitacoes_Usuario)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Usuario] >= @AV21TFSolicitacoes_Usuario)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! (0==AV22TFSolicitacoes_Usuario_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Usuario] <= @AV22TFSolicitacoes_Usuario_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Usuario] <= @AV22TFSolicitacoes_Usuario_To)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! (DateTime.MinValue==AV23TFSolicitacoes_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Data] >= @AV23TFSolicitacoes_Data)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Data] >= @AV23TFSolicitacoes_Data)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV24TFSolicitacoes_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Data] <= @AV24TFSolicitacoes_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Data] <= @AV24TFSolicitacoes_Data_To)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ! (0==AV25TFSolicitacoes_Usuario_Ult) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Usuario_Ult] >= @AV25TFSolicitacoes_Usuario_Ult)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Usuario_Ult] >= @AV25TFSolicitacoes_Usuario_Ult)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( ! (0==AV26TFSolicitacoes_Usuario_Ult_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Usuario_Ult] <= @AV26TFSolicitacoes_Usuario_Ult_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Usuario_Ult] <= @AV26TFSolicitacoes_Usuario_Ult_To)";
            }
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( ! (DateTime.MinValue==AV27TFSolicitacoes_Data_Ult) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Data_Ult] >= @AV27TFSolicitacoes_Data_Ult)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Data_Ult] >= @AV27TFSolicitacoes_Data_Ult)";
            }
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( ! (DateTime.MinValue==AV28TFSolicitacoes_Data_Ult_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Data_Ult] <= @AV28TFSolicitacoes_Data_Ult_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Data_Ult] <= @AV28TFSolicitacoes_Data_Ult_To)";
            }
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( AV30TFSolicitacoes_Status_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV30TFSolicitacoes_Status_Sels, "[Solicitacoes_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV30TFSolicitacoes_Status_Sels, "[Solicitacoes_Status] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [Solicitacoes_Objetivo]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00MG2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (DateTime)dynConstraints[26] , (DateTime)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (DateTime)dynConstraints[30] , (DateTime)dynConstraints[31] , (int)dynConstraints[32] , (String)dynConstraints[33] , (int)dynConstraints[34] , (int)dynConstraints[35] , (int)dynConstraints[36] , (int)dynConstraints[37] , (String)dynConstraints[38] , (int)dynConstraints[39] , (DateTime)dynConstraints[40] , (int)dynConstraints[41] , (DateTime)dynConstraints[42] );
               case 1 :
                     return conditional_P00MG3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (DateTime)dynConstraints[26] , (DateTime)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (DateTime)dynConstraints[30] , (DateTime)dynConstraints[31] , (int)dynConstraints[32] , (String)dynConstraints[33] , (int)dynConstraints[34] , (int)dynConstraints[35] , (int)dynConstraints[36] , (int)dynConstraints[37] , (String)dynConstraints[38] , (int)dynConstraints[39] , (DateTime)dynConstraints[40] , (int)dynConstraints[41] , (DateTime)dynConstraints[42] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00MG2 ;
          prmP00MG2 = new Object[] {
          new Object[] {"@lV51Solicitacoes_Novo_Projeto1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV51Solicitacoes_Novo_Projeto1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV55Solicitacoes_Novo_Projeto2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV55Solicitacoes_Novo_Projeto2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV59Solicitacoes_Novo_Projeto3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV59Solicitacoes_Novo_Projeto3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV10TFSolicitacoes_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFSolicitacoes_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFContratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContratada_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14TFSistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV15TFSistema_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16TFServico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFServico_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18TFSolicitacoes_Novo_Projeto_Sel",SqlDbType.Char,1,0} ,
          new Object[] {"@lV19TFSolicitacoes_Objetivo",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV20TFSolicitacoes_Objetivo_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV21TFSolicitacoes_Usuario",SqlDbType.Int,6,0} ,
          new Object[] {"@AV22TFSolicitacoes_Usuario_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV23TFSolicitacoes_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV24TFSolicitacoes_Data_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV25TFSolicitacoes_Usuario_Ult",SqlDbType.Int,6,0} ,
          new Object[] {"@AV26TFSolicitacoes_Usuario_Ult_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV27TFSolicitacoes_Data_Ult",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV28TFSolicitacoes_Data_Ult_To",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmP00MG3 ;
          prmP00MG3 = new Object[] {
          new Object[] {"@lV51Solicitacoes_Novo_Projeto1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV51Solicitacoes_Novo_Projeto1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV55Solicitacoes_Novo_Projeto2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV55Solicitacoes_Novo_Projeto2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV59Solicitacoes_Novo_Projeto3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV59Solicitacoes_Novo_Projeto3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV10TFSolicitacoes_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFSolicitacoes_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFContratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContratada_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14TFSistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV15TFSistema_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV16TFServico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFServico_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18TFSolicitacoes_Novo_Projeto_Sel",SqlDbType.Char,1,0} ,
          new Object[] {"@lV19TFSolicitacoes_Objetivo",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV20TFSolicitacoes_Objetivo_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV21TFSolicitacoes_Usuario",SqlDbType.Int,6,0} ,
          new Object[] {"@AV22TFSolicitacoes_Usuario_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV23TFSolicitacoes_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV24TFSolicitacoes_Data_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV25TFSolicitacoes_Usuario_Ult",SqlDbType.Int,6,0} ,
          new Object[] {"@AV26TFSolicitacoes_Usuario_Ult_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV27TFSolicitacoes_Data_Ult",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV28TFSolicitacoes_Data_Ult_To",SqlDbType.DateTime,8,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00MG2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00MG2,100,0,true,false )
             ,new CursorDef("P00MG3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00MG3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDateTime(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((String[]) buf[6])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(7);
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                ((int[]) buf[9])[0] = rslt.getInt(9) ;
                ((int[]) buf[10])[0] = rslt.getInt(10) ;
                ((int[]) buf[11])[0] = rslt.getInt(11) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                ((int[]) buf[9])[0] = rslt.getInt(9) ;
                ((int[]) buf[10])[0] = rslt.getInt(10) ;
                ((String[]) buf[11])[0] = rslt.getString(11, 1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[49]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[49]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptsolicitacoesfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptsolicitacoesfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptsolicitacoesfilterdata") )
          {
             return  ;
          }
          getpromptsolicitacoesfilterdata worker = new getpromptsolicitacoesfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
