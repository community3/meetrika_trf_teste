/*
               File: GxOfflineEventReplicatorMap
        Description: Gx Offline Event Replicator Map
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:58.4
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gxofflineeventreplicatormap : GXProcedure
   {
      public gxofflineeventreplicatormap( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public gxofflineeventreplicatormap( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( GXProperties aP0_InMetadata ,
                           String aP1_ParentLevelName ,
                           short aP2_ThisLevelIndex ,
                           GXProperties aP3_AllAttributeMappings ,
                           GXProperties aP4_CurrentMappingInfo ,
                           SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem aP5_EventResult ,
                           GxSilentTrnSdt aP6_BC ,
                           IGxCollection aP7_InMappingConditions ,
                           String aP8_MappingType ,
                           IGxCollection aP9_PKMappingPK ,
                           GXProperties aP10_Props )
      {
         this.AV31InMetadata = aP0_InMetadata;
         this.AV46ParentLevelName = aP1_ParentLevelName;
         this.AV70ThisLevelIndex = aP2_ThisLevelIndex;
         this.AV9AllAttributeMappings = aP3_AllAttributeMappings;
         this.AV55CurrentMappingInfo = aP4_CurrentMappingInfo;
         this.AV23EventResult = aP5_EventResult;
         this.AV20BC = aP6_BC;
         this.AV30InMappingConditions = aP7_InMappingConditions;
         this.AV59MappingType = aP8_MappingType;
         this.AV51PKMappingPK = aP9_PKMappingPK;
         this.AV63Props = aP10_Props;
         initialize();
         executePrivate();
      }

      public void executeSubmit( GXProperties aP0_InMetadata ,
                                 String aP1_ParentLevelName ,
                                 short aP2_ThisLevelIndex ,
                                 GXProperties aP3_AllAttributeMappings ,
                                 GXProperties aP4_CurrentMappingInfo ,
                                 SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem aP5_EventResult ,
                                 GxSilentTrnSdt aP6_BC ,
                                 IGxCollection aP7_InMappingConditions ,
                                 String aP8_MappingType ,
                                 IGxCollection aP9_PKMappingPK ,
                                 GXProperties aP10_Props )
      {
         gxofflineeventreplicatormap objgxofflineeventreplicatormap;
         objgxofflineeventreplicatormap = new gxofflineeventreplicatormap();
         objgxofflineeventreplicatormap.AV31InMetadata = aP0_InMetadata;
         objgxofflineeventreplicatormap.AV46ParentLevelName = aP1_ParentLevelName;
         objgxofflineeventreplicatormap.AV70ThisLevelIndex = aP2_ThisLevelIndex;
         objgxofflineeventreplicatormap.AV9AllAttributeMappings = aP3_AllAttributeMappings;
         objgxofflineeventreplicatormap.AV55CurrentMappingInfo = aP4_CurrentMappingInfo;
         objgxofflineeventreplicatormap.AV23EventResult = aP5_EventResult;
         objgxofflineeventreplicatormap.AV20BC = aP6_BC;
         objgxofflineeventreplicatormap.AV30InMappingConditions = aP7_InMappingConditions;
         objgxofflineeventreplicatormap.AV59MappingType = aP8_MappingType;
         objgxofflineeventreplicatormap.AV51PKMappingPK = aP9_PKMappingPK;
         objgxofflineeventreplicatormap.AV63Props = aP10_Props;
         objgxofflineeventreplicatormap.context.SetSubmitInitialConfig(context);
         objgxofflineeventreplicatormap.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgxofflineeventreplicatormap);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((gxofflineeventreplicatormap)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV40LevelName = AV31InMetadata.Get("Name");
         AV48ThisLevelName = StringUtil.Format( "%1%2_", AV46ParentLevelName, AV40LevelName, "", "", "", "", "", "", "");
         AV47PKList.FromJSonString(AV31InMetadata.Get("PK"));
         AV42MappingConditions.Clear();
         AV73GXV1 = 1;
         while ( AV73GXV1 <= AV30InMappingConditions.Count )
         {
            AV35KeyValue = ((SdtGxKeyValue)AV30InMappingConditions.Item(AV73GXV1));
            AV42MappingConditions.Add(AV35KeyValue, 0);
            AV73GXV1 = (int)(AV73GXV1+1);
         }
         AV74GXV2 = 1;
         while ( AV74GXV2 <= AV47PKList.Count )
         {
            AV16AttName = ((String)AV47PKList.Item(AV74GXV2));
            AV35KeyValue = new SdtGxKeyValue(context);
            AV35KeyValue.gxTpr_Key = AV16AttName;
            AV50Value = AV20BC.GetValue(AV16AttName);
            AV35KeyValue.gxTpr_Value = AV50Value;
            AV42MappingConditions.Add(AV35KeyValue, 0);
            AV74GXV2 = (int)(AV74GXV2+1);
         }
         AV75GXV3 = 1;
         while ( AV75GXV3 <= AV51PKMappingPK.Count )
         {
            AV16AttName = ((String)AV51PKMappingPK.Item(AV75GXV3));
            AV47PKList.Add(AV16AttName, 0);
            AV75GXV3 = (int)(AV75GXV3+1);
         }
         AV47PKList.Sort("");
         if ( StringUtil.StrCmp(AV59MappingType, "FK") == 0 )
         {
            AV33IsFKMapping = true;
            /* Execute user subroutine: 'GETOLDMAPPINGS' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            AV28FKList.FromJSonString(AV31InMetadata.Get("FKList"));
         }
         else if ( StringUtil.StrCmp(AV59MappingType, "PK") == 0 )
         {
            AV26FKItem.gxTpr_Fk.FromJSonString(AV47PKList.ToJSonString(false));
            AV28FKList.Add(AV26FKItem, 0);
         }
         else if ( StringUtil.StrCmp(AV59MappingType, "TblUpd") == 0 )
         {
            AV56IsTblUpd = true;
            /* Execute user subroutine: 'APPLYTABLEUPDATES' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else
         {
            context.StatusMessage( StringUtil.Format( "Cannot apply Replicator Mapping type: %1", AV59MappingType, "", "", "", "", "", "", "", "") );
            this.cleanup();
            if (true) return;
         }
         if ( ! AV56IsTblUpd )
         {
            AV45NeedFKMapping = false;
            AV76GXV4 = 1;
            while ( AV76GXV4 <= AV28FKList.Count )
            {
               AV26FKItem = ((SdtGxEventFK)AV28FKList.Item(AV76GXV4));
               AV26FKItem.gxTpr_Fk.Sort("") ;
               AV10AllFK = "";
               AV11AllFKList.Clear();
               AV18AttNameMaps = new GXProperties();
               AV77GXV5 = 1;
               while ( AV77GXV5 <= AV26FKItem.gxTpr_Fkmap.Count )
               {
                  AV17AttNameMap = AV26FKItem.gxTpr_Fkmap.GetString(AV77GXV5);
                  AV29index = (short)(StringUtil.StringSearch( AV17AttNameMap, "-", 1)-1);
                  AV16AttName = StringUtil.Substring( AV17AttNameMap, 1, AV29index);
                  AV29index = (short)(AV29index+2);
                  AV36len = (short)(StringUtil.Len( AV17AttNameMap)-AV29index+1);
                  AV12AttMapName = StringUtil.Substring( AV17AttNameMap, AV29index, AV36len);
                  AV18AttNameMaps.Set(AV12AttMapName, AV16AttName);
                  AV77GXV5 = (int)(AV77GXV5+1);
               }
               AV27FKItemJSon = AV26FKItem.gxTpr_Fk.ToJSonString(false);
               AV78GXV6 = 1;
               while ( AV78GXV6 <= AV26FKItem.gxTpr_Fk.Count )
               {
                  AV12AttMapName = AV26FKItem.gxTpr_Fk.GetString(AV78GXV6);
                  AV16AttName = AV18AttNameMaps.Get(AV12AttMapName);
                  if ( String.IsNullOrEmpty(StringUtil.RTrim( AV16AttName)) )
                  {
                     AV16AttName = AV12AttMapName;
                     AV18AttNameMaps.Set(AV12AttMapName, AV12AttMapName);
                  }
                  AV14AttMappingKey = StringUtil.Format( "%1,%2", AV27FKItemJSon, AV12AttMapName, "", "", "", "", "", "", "");
                  AV15AttMappings = AV9AllAttributeMappings.Get(AV14AttMappingKey);
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15AttMappings)) )
                  {
                     if ( String.IsNullOrEmpty(StringUtil.RTrim( AV10AllFK)) )
                     {
                        AV79GXV7 = 1;
                        while ( AV79GXV7 <= AV26FKItem.gxTpr_Fk.Count )
                        {
                           AV25FKAttName = AV26FKItem.gxTpr_Fk.GetString(AV79GXV7);
                           AV35KeyValue = new SdtGxKeyValue(context);
                           AV35KeyValue.gxTpr_Key = AV25FKAttName;
                           AV50Value = AV20BC.GetValue(AV18AttNameMaps.Get(AV25FKAttName));
                           AV35KeyValue.gxTpr_Value = AV50Value;
                           AV11AllFKList.Add(AV35KeyValue, 0);
                           AV79GXV7 = (int)(AV79GXV7+1);
                        }
                        AV10AllFK = AV11AllFKList.ToJSonString(false);
                     }
                     AV19AttributeMappings.FromJSonString(AV15AttMappings);
                     AV80GXV8 = 1;
                     while ( AV80GXV8 <= AV19AttributeMappings.Count )
                     {
                        AV13AttMapping = ((SdtGxKeyValue)AV19AttributeMappings.Item(AV80GXV8));
                        if ( StringUtil.StrCmp(AV10AllFK, AV13AttMapping.gxTpr_Key) == 0 )
                        {
                           AV50Value = AV13AttMapping.gxTpr_Value;
                           if ( StringUtil.StrCmp(StringUtil.Substring( AV50Value, 1, 1), "+") == 0 )
                           {
                              AV50Value = StringUtil.Substring( AV50Value, 2, StringUtil.Len( AV50Value)-1);
                              AV20BC.SetValue(AV16AttName, AV50Value);
                              AV35KeyValue = new SdtGxKeyValue(context);
                              AV35KeyValue.gxTpr_Key = AV16AttName;
                              AV35KeyValue.gxTpr_Value = AV50Value;
                              AV43MappingUpdates.Add(AV35KeyValue, 0);
                              AV45NeedFKMapping = AV33IsFKMapping;
                              if ( ! AV33IsFKMapping )
                              {
                                 AV20BC.FromJSonString("{\"Mode\":\"UPD\"}");
                              }
                              if (true) break;
                           }
                           else
                           {
                              AV22EventError.gxTpr_Id = "InvalidFK";
                              AV22EventError.gxTpr_Description = StringUtil.Format( "Invalid Foreign Key: %1", AV13AttMapping.gxTpr_Key, "", "", "", "", "", "", "", "");
                              AV22EventError.gxTpr_Type = 1;
                              AV8Messages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
                              AV8Messages.Add(AV22EventError, 0);
                              AV23EventResult.gxTpr_Eventerrors = AV8Messages.ToJSonString(false);
                              AV23EventResult.gxTpr_Eventstatus = 6;
                              this.cleanup();
                              if (true) return;
                           }
                        }
                        AV80GXV8 = (int)(AV80GXV8+1);
                     }
                  }
                  AV78GXV6 = (int)(AV78GXV6+1);
               }
               AV76GXV4 = (int)(AV76GXV4+1);
            }
            if ( AV45NeedFKMapping )
            {
               AV24EventResultMapping = new SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem(context);
               AV24EventResultMapping.gxTpr_Table = AV31InMetadata.Get("BT");
               AV24EventResultMapping.gxTpr_Updates = AV43MappingUpdates.ToJSonString(false);
               AV24EventResultMapping.gxTpr_Conditions = AV42MappingConditions.ToJSonString(false);
               AV23EventResult.gxTpr_Mappings.Add(AV24EventResultMapping, 0);
            }
         }
         AV53AttNameList.FromJSonString(AV31InMetadata.Get("PK"));
         AV41Levels.FromJSonString(AV31InMetadata.Get("Levels"));
         if ( AV41Levels.Count != 0 )
         {
            AV81GXV9 = 1;
            while ( AV81GXV9 <= AV41Levels.Count )
            {
               AV37Level = AV41Levels.GetString(AV81GXV9);
               AV39LevelBCs = (IGxCollection)(GeneXus.Utils.GxSilentTrnSdt.CreateCollection( StringUtil.Format( "%1%2", AV48ThisLevelName, AV37Level, "", "", "", "", "", "", ""), "GeneXus.Programs", context));
               AV39LevelBCs.FromJSonString(AV20BC.GetValue(AV37Level));
               AV29index = 1;
               AV82GXV10 = 1;
               while ( AV82GXV10 <= AV39LevelBCs.Count )
               {
                  AV38LevelBC = ((GxSilentTrnSdt)AV39LevelBCs.Item(AV82GXV10));
                  AV44Metadata = AV38LevelBC.GetMetadata();
                  new gxofflineeventreplicatormap(context ).execute(  AV44Metadata,  AV48ThisLevelName,  AV29index,  AV9AllAttributeMappings,  AV55CurrentMappingInfo,  AV23EventResult,  AV38LevelBC,  AV42MappingConditions,  AV59MappingType,  AV47PKList,  AV63Props) ;
                  if ( AV23EventResult.gxTpr_Eventstatus == 6 )
                  {
                     this.cleanup();
                     if (true) return;
                  }
                  if ( ( AV29index == AV39LevelBCs.Count ) && ( StringUtil.StrCmp(AV59MappingType, "TblUpd") == 0 ) )
                  {
                     AV69SerialType = "Same";
                     /* Execute user subroutine: 'APPLYSERIALUPDATE' */
                     S141 ();
                     if ( returnInSub )
                     {
                        this.cleanup();
                        if (true) return;
                     }
                  }
                  AV29index = (short)(AV29index+1);
                  AV82GXV10 = (int)(AV82GXV10+1);
               }
               AV20BC.SetValue(AV37Level, AV39LevelBCs.ToJSonString(true));
               AV81GXV9 = (int)(AV81GXV9+1);
            }
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'GETOLDMAPPINGS' Routine */
         AV35KeyValue = new SdtGxKeyValue(context);
         AV35KeyValue.gxTpr_Key = "PK";
         AV35KeyValue.gxTpr_Value = AV42MappingConditions.ToJSonString(false);
         AV58MappingInfo.Add(AV35KeyValue, 0);
         AV53AttNameList.Clear();
         AV53AttNameList.FromJSonString(AV31InMetadata.Get("PKAssigned"));
         if ( AV53AttNameList.Count != 0 )
         {
            AV57KeyValueCol.Clear();
            AV83GXV11 = 1;
            while ( AV83GXV11 <= AV53AttNameList.Count )
            {
               AV16AttName = AV53AttNameList.GetString(AV83GXV11);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16AttName)) )
               {
                  AV35KeyValue = new SdtGxKeyValue(context);
                  AV35KeyValue.gxTpr_Key = AV16AttName;
                  AV35KeyValue.gxTpr_Value = AV20BC.GetValue(AV16AttName);
                  AV57KeyValueCol.Add(AV35KeyValue, 0);
               }
               AV83GXV11 = (int)(AV83GXV11+1);
            }
            AV35KeyValue = new SdtGxKeyValue(context);
            AV35KeyValue.gxTpr_Key = "PKAssigned";
            AV35KeyValue.gxTpr_Value = AV57KeyValueCol.ToJSonString(false);
            AV58MappingInfo.Add(AV35KeyValue, 0);
         }
         AV55CurrentMappingInfo.Set(StringUtil.Format( "%1.%2", AV48ThisLevelName, StringUtil.LTrim( StringUtil.Str( (decimal)(AV70ThisLevelIndex), 4, 0)), "", "", "", "", "", "", ""), AV58MappingInfo.ToJSonString(false));
      }

      protected void S121( )
      {
         /* 'APPLYTABLEUPDATES' Routine */
         AV50Value = AV55CurrentMappingInfo.Get(StringUtil.Format( "%1.%2", AV48ThisLevelName, StringUtil.LTrim( StringUtil.Str( (decimal)(AV70ThisLevelIndex), 4, 0)), "", "", "", "", "", "", ""));
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50Value)) )
         {
            AV58MappingInfo.FromJSonString(AV50Value);
            AV84GXV12 = 1;
            while ( AV84GXV12 <= AV58MappingInfo.Count )
            {
               AV35KeyValue = ((SdtGxKeyValue)AV58MappingInfo.Item(AV84GXV12));
               if ( StringUtil.StrCmp(AV35KeyValue.gxTpr_Key, "PK") == 0 )
               {
                  AV62PKKeyValueCol.FromJSonString(AV35KeyValue.gxTpr_Value);
                  AV52AllPKOld = AV62PKKeyValueCol.ToJSonString(false);
               }
               else if ( StringUtil.StrCmp(AV35KeyValue.gxTpr_Key, "PKAssigned") == 0 )
               {
                  AV57KeyValueCol.FromJSonString(AV35KeyValue.gxTpr_Value);
                  AV24EventResultMapping = new SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem(context);
                  AV43MappingUpdates = new GxObjectCollection( context, "GxKeyValue", "GxEv3Up14_MeetrikaVs3", "SdtGxKeyValue", "GeneXus.Programs");
                  AV85GXV13 = 1;
                  while ( AV85GXV13 <= AV57KeyValueCol.Count )
                  {
                     AV35KeyValue = ((SdtGxKeyValue)AV57KeyValueCol.Item(AV85GXV13));
                     AV16AttName = AV35KeyValue.gxTpr_Key;
                     AV61OldValue = AV35KeyValue.gxTpr_Value;
                     /* Execute user subroutine: 'GETONEMAPPING' */
                     S131 ();
                     if (returnInSub) return;
                     AV85GXV13 = (int)(AV85GXV13+1);
                  }
                  if ( AV60NeedBTMapping )
                  {
                     AV24EventResultMapping.gxTpr_Table = AV31InMetadata.Get("BT");
                     AV24EventResultMapping.gxTpr_Updates = AV43MappingUpdates.ToJSonString(false);
                     AV24EventResultMapping.gxTpr_Conditions = AV52AllPKOld;
                     AV23EventResult.gxTpr_Mappings.Add(AV24EventResultMapping, 0);
                  }
               }
               else
               {
                  context.StatusMessage( StringUtil.Format( "Cannot apply table update(%1): %2 - %3", AV48ThisLevelName, AV35KeyValue.gxTpr_Key, AV35KeyValue.gxTpr_Value, "", "", "", "", "", "") );
               }
               AV84GXV12 = (int)(AV84GXV12+1);
            }
         }
         AV38LevelBC = AV20BC;
         AV69SerialType = "Other";
         /* Execute user subroutine: 'APPLYSERIALUPDATE' */
         S141 ();
         if (returnInSub) return;
      }

      protected void S131( )
      {
         /* 'GETONEMAPPING' Routine */
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16AttName)) )
         {
            AV14AttMappingKey = StringUtil.Format( "%1,%2", AV47PKList.ToJSonString(false), AV16AttName, "", "", "", "", "", "", "");
            if ( StringUtil.StrCmp(AV63Props.Get("Result"), "Success") == 0 )
            {
               AV64RValue = AV20BC.GetValue(AV16AttName);
               AV50Value = StringUtil.Format( "+%1", AV64RValue, "", "", "", "", "", "", "", "");
            }
            else
            {
               AV64RValue = "";
               AV50Value = "!";
            }
            AV15AttMappings = AV9AllAttributeMappings.Get(AV14AttMappingKey);
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15AttMappings)) )
            {
               AV19AttributeMappings = new GxObjectCollection( context, "GxKeyValue", "GxEv3Up14_MeetrikaVs3", "SdtGxKeyValue", "GeneXus.Programs");
            }
            else
            {
               AV19AttributeMappings.FromJSonString(AV15AttMappings);
            }
            if ( StringUtil.StrCmp(AV61OldValue, AV64RValue) != 0 )
            {
               AV54AttributeMapping = new SdtGxKeyValue(context);
               AV54AttributeMapping.gxTpr_Key = AV52AllPKOld;
               AV54AttributeMapping.gxTpr_Value = AV50Value;
               AV19AttributeMappings.Add(AV54AttributeMapping, 0);
               AV9AllAttributeMappings.Set(AV14AttMappingKey, AV19AttributeMappings.ToJSonString(false));
               if ( StringUtil.StrCmp(AV63Props.Get("Result"), "Success") == 0 )
               {
                  AV60NeedBTMapping = true;
               }
               else
               {
                  AV60NeedBTMapping = false;
               }
               AV35KeyValue = new SdtGxKeyValue(context);
               AV35KeyValue.gxTpr_Key = AV16AttName;
               AV35KeyValue.gxTpr_Value = AV64RValue;
               AV43MappingUpdates.Add(AV35KeyValue, 0);
            }
         }
      }

      protected void S141( )
      {
         /* 'APPLYSERIALUPDATE' Routine */
         AV66SerialList = (IGxCollection)(new GxSimpleCollection());
         AV66SerialList.FromJSonString(AV31InMetadata.Get("Serial"));
         AV86GXV14 = 1;
         while ( AV86GXV14 <= AV66SerialList.Count )
         {
            AV64RValue = AV66SerialList.GetString(AV86GXV14);
            AV65SerialItem.FromJSonString(AV64RValue);
            if ( StringUtil.StrCmp(AV65SerialItem.GetString(1), AV69SerialType) == 0 )
            {
               AV24EventResultMapping = new SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem(context);
               AV67SerialMappingConditions = new GxObjectCollection( context, "GxKeyValue", "GxEv3Up14_MeetrikaVs3", "SdtGxKeyValue", "GeneXus.Programs");
               AV68SerialMappingUpdates = new GxObjectCollection( context, "GxKeyValue", "GxEv3Up14_MeetrikaVs3", "SdtGxKeyValue", "GeneXus.Programs");
               AV24EventResultMapping.gxTpr_Table = AV65SerialItem.GetString(2);
               AV35KeyValue = new SdtGxKeyValue(context);
               AV35KeyValue.gxTpr_Key = AV65SerialItem.GetString(3);
               AV35KeyValue.gxTpr_Value = AV38LevelBC.GetValue(AV65SerialItem.GetString(4));
               AV68SerialMappingUpdates.Add(AV35KeyValue, 0);
               AV65SerialItem.RemoveItem(4);
               AV65SerialItem.RemoveItem(3);
               AV65SerialItem.RemoveItem(2);
               AV65SerialItem.RemoveItem(1);
               AV29index = 1;
               while ( AV29index <= AV65SerialItem.Count )
               {
                  AV35KeyValue = new SdtGxKeyValue(context);
                  AV35KeyValue.gxTpr_Key = AV65SerialItem.GetString(AV29index);
                  AV50Value = AV20BC.GetValue(AV65SerialItem.GetString(AV29index+1));
                  AV35KeyValue.gxTpr_Value = AV50Value;
                  AV67SerialMappingConditions.Add(AV35KeyValue, 0);
                  AV29index = (short)(AV29index+2);
               }
               AV24EventResultMapping.gxTpr_Updates = AV68SerialMappingUpdates.ToJSonString(false);
               AV24EventResultMapping.gxTpr_Conditions = AV67SerialMappingConditions.ToJSonString(false);
               AV23EventResult.gxTpr_Mappings.Add(AV24EventResultMapping, 0);
            }
            AV86GXV14 = (int)(AV86GXV14+1);
         }
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV40LevelName = "";
         AV48ThisLevelName = "";
         AV47PKList = new GxSimpleCollection();
         AV42MappingConditions = new GxObjectCollection( context, "GxKeyValue", "GxEv3Up14_MeetrikaVs3", "SdtGxKeyValue", "GeneXus.Programs");
         AV35KeyValue = new SdtGxKeyValue(context);
         AV16AttName = "";
         AV50Value = "";
         AV28FKList = new GxObjectCollection( context, "GxEventFK", "GxEv3Up14_MeetrikaVs3", "SdtGxEventFK", "GeneXus.Programs");
         AV26FKItem = new SdtGxEventFK(context);
         AV10AllFK = "";
         AV11AllFKList = new GxObjectCollection( context, "GxKeyValue", "GxEv3Up14_MeetrikaVs3", "SdtGxKeyValue", "GeneXus.Programs");
         AV18AttNameMaps = new GXProperties();
         AV17AttNameMap = "";
         AV12AttMapName = "";
         AV27FKItemJSon = "";
         AV14AttMappingKey = "";
         AV15AttMappings = "";
         AV25FKAttName = "";
         AV19AttributeMappings = new GxObjectCollection( context, "GxKeyValue", "GxEv3Up14_MeetrikaVs3", "SdtGxKeyValue", "GeneXus.Programs");
         AV13AttMapping = new SdtGxKeyValue(context);
         AV43MappingUpdates = new GxObjectCollection( context, "GxKeyValue", "GxEv3Up14_MeetrikaVs3", "SdtGxKeyValue", "GeneXus.Programs");
         AV22EventError = new SdtMessages_Message(context);
         AV8Messages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV24EventResultMapping = new SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem(context);
         AV53AttNameList = new GxSimpleCollection();
         AV41Levels = new GxSimpleCollection();
         AV37Level = "";
         AV39LevelBCs = new GxObjectCollection( context, "businesscomponent", "", "GxSilentTrnSdt", "GeneXus.Programs");
         AV38LevelBC = new GxSilentTrnSdt(context);
         AV44Metadata = new GXProperties();
         AV69SerialType = "";
         AV58MappingInfo = new GxObjectCollection( context, "GxKeyValue", "GxEv3Up14_MeetrikaVs3", "SdtGxKeyValue", "GeneXus.Programs");
         AV57KeyValueCol = new GxObjectCollection( context, "GxKeyValue", "GxEv3Up14_MeetrikaVs3", "SdtGxKeyValue", "GeneXus.Programs");
         AV62PKKeyValueCol = new GxObjectCollection( context, "GxKeyValue", "GxEv3Up14_MeetrikaVs3", "SdtGxKeyValue", "GeneXus.Programs");
         AV52AllPKOld = "";
         AV61OldValue = "";
         AV64RValue = "";
         AV54AttributeMapping = new SdtGxKeyValue(context);
         AV66SerialList = new GxSimpleCollection();
         AV65SerialItem = new GxSimpleCollection();
         AV67SerialMappingConditions = new GxObjectCollection( context, "GxKeyValue", "GxEv3Up14_MeetrikaVs3", "SdtGxKeyValue", "GeneXus.Programs");
         AV68SerialMappingUpdates = new GxObjectCollection( context, "GxKeyValue", "GxEv3Up14_MeetrikaVs3", "SdtGxKeyValue", "GeneXus.Programs");
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV70ThisLevelIndex ;
      private short AV29index ;
      private short AV36len ;
      private int AV73GXV1 ;
      private int AV74GXV2 ;
      private int AV75GXV3 ;
      private int AV76GXV4 ;
      private int AV77GXV5 ;
      private int AV78GXV6 ;
      private int AV79GXV7 ;
      private int AV80GXV8 ;
      private int AV81GXV9 ;
      private int AV82GXV10 ;
      private int AV83GXV11 ;
      private int AV84GXV12 ;
      private int AV85GXV13 ;
      private int AV86GXV14 ;
      private String AV46ParentLevelName ;
      private String AV59MappingType ;
      private String AV40LevelName ;
      private String AV48ThisLevelName ;
      private String AV16AttName ;
      private String AV50Value ;
      private String AV17AttNameMap ;
      private String AV12AttMapName ;
      private String AV15AttMappings ;
      private String AV25FKAttName ;
      private String AV37Level ;
      private String AV69SerialType ;
      private String AV61OldValue ;
      private String AV64RValue ;
      private bool AV33IsFKMapping ;
      private bool returnInSub ;
      private bool AV56IsTblUpd ;
      private bool AV45NeedFKMapping ;
      private bool AV60NeedBTMapping ;
      private String AV10AllFK ;
      private String AV27FKItemJSon ;
      private String AV14AttMappingKey ;
      private String AV52AllPKOld ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV53AttNameList ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV41Levels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV66SerialList ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV65SerialItem ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV51PKMappingPK ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV47PKList ;
      [ObjectCollection(ItemType=typeof( SdtGxKeyValue ))]
      private IGxCollection AV30InMappingConditions ;
      [ObjectCollection(ItemType=typeof( SdtGxKeyValue ))]
      private IGxCollection AV42MappingConditions ;
      [ObjectCollection(ItemType=typeof( SdtGxKeyValue ))]
      private IGxCollection AV11AllFKList ;
      [ObjectCollection(ItemType=typeof( SdtGxKeyValue ))]
      private IGxCollection AV19AttributeMappings ;
      [ObjectCollection(ItemType=typeof( SdtGxKeyValue ))]
      private IGxCollection AV43MappingUpdates ;
      [ObjectCollection(ItemType=typeof( SdtGxKeyValue ))]
      private IGxCollection AV58MappingInfo ;
      [ObjectCollection(ItemType=typeof( SdtGxKeyValue ))]
      private IGxCollection AV57KeyValueCol ;
      [ObjectCollection(ItemType=typeof( SdtGxKeyValue ))]
      private IGxCollection AV62PKKeyValueCol ;
      [ObjectCollection(ItemType=typeof( SdtGxKeyValue ))]
      private IGxCollection AV67SerialMappingConditions ;
      [ObjectCollection(ItemType=typeof( SdtGxKeyValue ))]
      private IGxCollection AV68SerialMappingUpdates ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV8Messages ;
      [ObjectCollection(ItemType=typeof( SdtGxEventFK ))]
      private IGxCollection AV28FKList ;
      private GXProperties AV31InMetadata ;
      private GXProperties AV9AllAttributeMappings ;
      private GXProperties AV55CurrentMappingInfo ;
      private GXProperties AV63Props ;
      private GXProperties AV18AttNameMaps ;
      private GXProperties AV44Metadata ;
      private SdtGxKeyValue AV35KeyValue ;
      private SdtGxKeyValue AV13AttMapping ;
      private SdtGxKeyValue AV54AttributeMapping ;
      private SdtMessages_Message AV22EventError ;
      private SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem AV23EventResult ;
      private SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem AV24EventResultMapping ;
      private SdtGxEventFK AV26FKItem ;
      private GxSilentTrnSdt AV20BC ;
      private GxSilentTrnSdt AV38LevelBC ;
      [ObjectCollection(ItemType=typeof( GxSilentTrnSdt ))]
      private IGxCollection AV39LevelBCs ;
   }

}
