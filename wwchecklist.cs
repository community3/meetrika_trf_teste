/*
               File: WWCheckList
        Description:  Check List
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 13:36:47.74
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwchecklist : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwchecklist( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwchecklist( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavChecklist_de1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavChecklist_de2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavChecklist_de3 = new GXCombobox();
         cmbCheckList_De = new GXCombobox();
         cmbCheckList_Obrigatorio = new GXCombobox();
         chkCheckList_Ativo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_71 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_71_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_71_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV16DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
               AV17CheckList_De1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17CheckList_De1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17CheckList_De1), 4, 0)));
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20CheckList_De2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20CheckList_De2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20CheckList_De2), 4, 0)));
               AV22DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersSelector3", AV22DynamicFiltersSelector3);
               AV23CheckList_De3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23CheckList_De3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23CheckList_De3), 4, 0)));
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV21DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersEnabled3", AV21DynamicFiltersEnabled3);
               AV57TFCheckList_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFCheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57TFCheckList_Codigo), 6, 0)));
               AV58TFCheckList_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFCheckList_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFCheckList_Codigo_To), 6, 0)));
               AV61TFCheck_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFCheck_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFCheck_Codigo), 6, 0)));
               AV62TFCheck_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFCheck_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFCheck_Codigo_To), 6, 0)));
               AV65TFCheckList_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFCheckList_Descricao", AV65TFCheckList_Descricao);
               AV66TFCheckList_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFCheckList_Descricao_Sel", AV66TFCheckList_Descricao_Sel);
               AV73TFCheckList_Obrigatorio_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFCheckList_Obrigatorio_Sel", StringUtil.Str( (decimal)(AV73TFCheckList_Obrigatorio_Sel), 1, 0));
               AV76TFCheckList_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFCheckList_Ativo_Sel", StringUtil.Str( (decimal)(AV76TFCheckList_Ativo_Sel), 1, 0));
               AV50ManageFiltersExecutionStep = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV50ManageFiltersExecutionStep), 1, 0));
               AV59ddo_CheckList_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ddo_CheckList_CodigoTitleControlIdToReplace", AV59ddo_CheckList_CodigoTitleControlIdToReplace);
               AV63ddo_Check_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_Check_CodigoTitleControlIdToReplace", AV63ddo_Check_CodigoTitleControlIdToReplace);
               AV67ddo_CheckList_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_CheckList_DescricaoTitleControlIdToReplace", AV67ddo_CheckList_DescricaoTitleControlIdToReplace);
               AV71ddo_CheckList_DeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ddo_CheckList_DeTitleControlIdToReplace", AV71ddo_CheckList_DeTitleControlIdToReplace);
               AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace", AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace);
               AV77ddo_CheckList_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_CheckList_AtivoTitleControlIdToReplace", AV77ddo_CheckList_AtivoTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV70TFCheckList_De_Sels);
               AV103Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV25DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersIgnoreFirst", AV25DynamicFiltersIgnoreFirst);
               AV24DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersRemoving", AV24DynamicFiltersRemoving);
               A758CheckList_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV27Check_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17CheckList_De1, AV19DynamicFiltersSelector2, AV20CheckList_De2, AV22DynamicFiltersSelector3, AV23CheckList_De3, AV18DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV57TFCheckList_Codigo, AV58TFCheckList_Codigo_To, AV61TFCheck_Codigo, AV62TFCheck_Codigo_To, AV65TFCheckList_Descricao, AV66TFCheckList_Descricao_Sel, AV73TFCheckList_Obrigatorio_Sel, AV76TFCheckList_Ativo_Sel, AV50ManageFiltersExecutionStep, AV59ddo_CheckList_CodigoTitleControlIdToReplace, AV63ddo_Check_CodigoTitleControlIdToReplace, AV67ddo_CheckList_DescricaoTitleControlIdToReplace, AV71ddo_CheckList_DeTitleControlIdToReplace, AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace, AV77ddo_CheckList_AtivoTitleControlIdToReplace, AV70TFCheckList_De_Sels, AV103Pgmname, AV10GridState, AV25DynamicFiltersIgnoreFirst, AV24DynamicFiltersRemoving, A758CheckList_Codigo, AV27Check_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAO22( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTO22( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051813364848");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwchecklist.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vCHECKLIST_DE1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17CheckList_De1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vCHECKLIST_DE2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20CheckList_De2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV22DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vCHECKLIST_DE3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23CheckList_De3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV21DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCHECKLIST_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV57TFCheckList_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCHECKLIST_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV58TFCheckList_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCHECK_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV61TFCheck_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCHECK_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV62TFCheck_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCHECKLIST_DESCRICAO", AV65TFCheckList_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCHECKLIST_DESCRICAO_SEL", AV66TFCheckList_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCHECKLIST_OBRIGATORIO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV73TFCheckList_Obrigatorio_Sel), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCHECKLIST_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV76TFCheckList_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_71", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_71), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMANAGEFILTERSDATA", AV54ManageFiltersData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMANAGEFILTERSDATA", AV54ManageFiltersData);
         }
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV80GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV81GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV78DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV78DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCHECKLIST_CODIGOTITLEFILTERDATA", AV56CheckList_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCHECKLIST_CODIGOTITLEFILTERDATA", AV56CheckList_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCHECK_CODIGOTITLEFILTERDATA", AV60Check_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCHECK_CODIGOTITLEFILTERDATA", AV60Check_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCHECKLIST_DESCRICAOTITLEFILTERDATA", AV64CheckList_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCHECKLIST_DESCRICAOTITLEFILTERDATA", AV64CheckList_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCHECKLIST_DETITLEFILTERDATA", AV68CheckList_DeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCHECKLIST_DETITLEFILTERDATA", AV68CheckList_DeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCHECKLIST_OBRIGATORIOTITLEFILTERDATA", AV72CheckList_ObrigatorioTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCHECKLIST_OBRIGATORIOTITLEFILTERDATA", AV72CheckList_ObrigatorioTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCHECKLIST_ATIVOTITLEFILTERDATA", AV75CheckList_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCHECKLIST_ATIVOTITLEFILTERDATA", AV75CheckList_AtivoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFCHECKLIST_DE_SELS", AV70TFCheckList_De_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFCHECKLIST_DE_SELS", AV70TFCheckList_De_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV103Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV25DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV24DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vCHECK_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV27Check_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Icon", StringUtil.RTrim( Ddo_managefilters_Icon));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Caption", StringUtil.RTrim( Ddo_managefilters_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Tooltip", StringUtil.RTrim( Ddo_managefilters_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Cls", StringUtil.RTrim( Ddo_managefilters_Cls));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_CODIGO_Caption", StringUtil.RTrim( Ddo_checklist_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_CODIGO_Tooltip", StringUtil.RTrim( Ddo_checklist_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_CODIGO_Cls", StringUtil.RTrim( Ddo_checklist_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_checklist_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_checklist_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_checklist_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_checklist_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_checklist_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_checklist_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_checklist_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_checklist_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_CODIGO_Filtertype", StringUtil.RTrim( Ddo_checklist_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_checklist_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_checklist_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_CODIGO_Sortasc", StringUtil.RTrim( Ddo_checklist_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_checklist_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_checklist_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_checklist_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_checklist_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_checklist_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_CODIGO_Caption", StringUtil.RTrim( Ddo_check_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_CODIGO_Tooltip", StringUtil.RTrim( Ddo_check_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_CODIGO_Cls", StringUtil.RTrim( Ddo_check_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_check_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_check_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_check_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_check_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_check_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_check_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_check_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_check_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_CODIGO_Filtertype", StringUtil.RTrim( Ddo_check_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_check_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_check_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_CODIGO_Sortasc", StringUtil.RTrim( Ddo_check_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_check_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_check_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_check_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_check_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_check_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DESCRICAO_Caption", StringUtil.RTrim( Ddo_checklist_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_checklist_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DESCRICAO_Cls", StringUtil.RTrim( Ddo_checklist_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_checklist_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_checklist_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_checklist_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_checklist_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_checklist_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_checklist_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_checklist_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_checklist_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_checklist_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_checklist_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_checklist_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_checklist_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_checklist_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_checklist_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_checklist_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_checklist_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_checklist_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_checklist_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_checklist_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_checklist_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DE_Caption", StringUtil.RTrim( Ddo_checklist_de_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DE_Tooltip", StringUtil.RTrim( Ddo_checklist_de_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DE_Cls", StringUtil.RTrim( Ddo_checklist_de_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DE_Selectedvalue_set", StringUtil.RTrim( Ddo_checklist_de_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DE_Dropdownoptionstype", StringUtil.RTrim( Ddo_checklist_de_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_checklist_de_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DE_Includesortasc", StringUtil.BoolToStr( Ddo_checklist_de_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DE_Includesortdsc", StringUtil.BoolToStr( Ddo_checklist_de_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DE_Sortedstatus", StringUtil.RTrim( Ddo_checklist_de_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DE_Includefilter", StringUtil.BoolToStr( Ddo_checklist_de_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DE_Includedatalist", StringUtil.BoolToStr( Ddo_checklist_de_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DE_Datalisttype", StringUtil.RTrim( Ddo_checklist_de_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DE_Allowmultipleselection", StringUtil.BoolToStr( Ddo_checklist_de_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DE_Datalistfixedvalues", StringUtil.RTrim( Ddo_checklist_de_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DE_Sortasc", StringUtil.RTrim( Ddo_checklist_de_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DE_Sortdsc", StringUtil.RTrim( Ddo_checklist_de_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DE_Cleanfilter", StringUtil.RTrim( Ddo_checklist_de_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DE_Searchbuttontext", StringUtil.RTrim( Ddo_checklist_de_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_OBRIGATORIO_Caption", StringUtil.RTrim( Ddo_checklist_obrigatorio_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_OBRIGATORIO_Tooltip", StringUtil.RTrim( Ddo_checklist_obrigatorio_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_OBRIGATORIO_Cls", StringUtil.RTrim( Ddo_checklist_obrigatorio_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_OBRIGATORIO_Selectedvalue_set", StringUtil.RTrim( Ddo_checklist_obrigatorio_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_OBRIGATORIO_Dropdownoptionstype", StringUtil.RTrim( Ddo_checklist_obrigatorio_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_OBRIGATORIO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_checklist_obrigatorio_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_OBRIGATORIO_Includesortasc", StringUtil.BoolToStr( Ddo_checklist_obrigatorio_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_OBRIGATORIO_Includesortdsc", StringUtil.BoolToStr( Ddo_checklist_obrigatorio_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_OBRIGATORIO_Sortedstatus", StringUtil.RTrim( Ddo_checklist_obrigatorio_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_OBRIGATORIO_Includefilter", StringUtil.BoolToStr( Ddo_checklist_obrigatorio_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_OBRIGATORIO_Includedatalist", StringUtil.BoolToStr( Ddo_checklist_obrigatorio_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_OBRIGATORIO_Datalisttype", StringUtil.RTrim( Ddo_checklist_obrigatorio_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_OBRIGATORIO_Datalistfixedvalues", StringUtil.RTrim( Ddo_checklist_obrigatorio_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_OBRIGATORIO_Sortasc", StringUtil.RTrim( Ddo_checklist_obrigatorio_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_OBRIGATORIO_Sortdsc", StringUtil.RTrim( Ddo_checklist_obrigatorio_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_OBRIGATORIO_Cleanfilter", StringUtil.RTrim( Ddo_checklist_obrigatorio_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_OBRIGATORIO_Searchbuttontext", StringUtil.RTrim( Ddo_checklist_obrigatorio_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_ATIVO_Caption", StringUtil.RTrim( Ddo_checklist_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_ATIVO_Tooltip", StringUtil.RTrim( Ddo_checklist_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_ATIVO_Cls", StringUtil.RTrim( Ddo_checklist_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_checklist_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_checklist_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_checklist_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_checklist_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_checklist_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_checklist_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_checklist_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_checklist_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_checklist_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_checklist_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_ATIVO_Sortasc", StringUtil.RTrim( Ddo_checklist_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_checklist_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_checklist_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_checklist_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_checklist_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_checklist_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_checklist_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_check_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_check_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CHECK_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_check_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_checklist_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_checklist_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_checklist_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DE_Activeeventkey", StringUtil.RTrim( Ddo_checklist_de_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_DE_Selectedvalue_get", StringUtil.RTrim( Ddo_checklist_de_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_OBRIGATORIO_Activeeventkey", StringUtil.RTrim( Ddo_checklist_obrigatorio_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_OBRIGATORIO_Selectedvalue_get", StringUtil.RTrim( Ddo_checklist_obrigatorio_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_checklist_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CHECKLIST_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_checklist_ativo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Activeeventkey", StringUtil.RTrim( Ddo_managefilters_Activeeventkey));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEO22( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTO22( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwchecklist.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWCheckList" ;
      }

      public override String GetPgmdesc( )
      {
         return " Check List" ;
      }

      protected void WBO20( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_O22( true) ;
         }
         else
         {
            wb_table1_2_O22( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_O22e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'" + sGXsfl_71_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(84, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,84);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'" + sGXsfl_71_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV21DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(85, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,85);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'" + sGXsfl_71_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavManagefiltersexecutionstep_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV50ManageFiltersExecutionStep), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV50ManageFiltersExecutionStep), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,86);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavManagefiltersexecutionstep_Jsonclick, 0, "Attribute", "", "", "", edtavManagefiltersexecutionstep_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWCheckList.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'" + sGXsfl_71_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfchecklist_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV57TFCheckList_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV57TFCheckList_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,87);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfchecklist_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfchecklist_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWCheckList.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'" + sGXsfl_71_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfchecklist_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV58TFCheckList_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV58TFCheckList_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,88);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfchecklist_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfchecklist_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWCheckList.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'" + sGXsfl_71_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcheck_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV61TFCheck_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV61TFCheck_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,89);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcheck_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcheck_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWCheckList.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_71_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcheck_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV62TFCheck_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV62TFCheck_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,90);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcheck_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcheck_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWCheckList.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'" + sGXsfl_71_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfchecklist_descricao_Internalname, AV65TFCheckList_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,91);\"", 0, edtavTfchecklist_descricao_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWCheckList.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_71_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfchecklist_descricao_sel_Internalname, AV66TFCheckList_Descricao_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,92);\"", 0, edtavTfchecklist_descricao_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWCheckList.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'" + sGXsfl_71_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfchecklist_obrigatorio_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV73TFCheckList_Obrigatorio_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV73TFCheckList_Obrigatorio_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,93);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfchecklist_obrigatorio_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfchecklist_obrigatorio_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWCheckList.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_71_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfchecklist_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV76TFCheckList_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV76TFCheckList_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,94);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfchecklist_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfchecklist_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWCheckList.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CHECKLIST_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_71_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_checklist_codigotitlecontrolidtoreplace_Internalname, AV59ddo_CheckList_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,96);\"", 0, edtavDdo_checklist_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWCheckList.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CHECK_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_71_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_check_codigotitlecontrolidtoreplace_Internalname, AV63ddo_Check_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,98);\"", 0, edtavDdo_check_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWCheckList.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CHECKLIST_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_71_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_checklist_descricaotitlecontrolidtoreplace_Internalname, AV67ddo_CheckList_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,100);\"", 0, edtavDdo_checklist_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWCheckList.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CHECKLIST_DEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_71_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_checklist_detitlecontrolidtoreplace_Internalname, AV71ddo_CheckList_DeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,102);\"", 0, edtavDdo_checklist_detitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWCheckList.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CHECKLIST_OBRIGATORIOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_71_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_checklist_obrigatoriotitlecontrolidtoreplace_Internalname, AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"", 0, edtavDdo_checklist_obrigatoriotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWCheckList.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CHECKLIST_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_71_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_checklist_ativotitlecontrolidtoreplace_Internalname, AV77ddo_CheckList_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"", 0, edtavDdo_checklist_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWCheckList.htm");
         }
         wbLoad = true;
      }

      protected void STARTO22( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Check List", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPO20( ) ;
      }

      protected void WSO22( )
      {
         STARTO22( ) ;
         EVTO22( ) ;
      }

      protected void EVTO22( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MANAGEFILTERS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11O22 */
                              E11O22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12O22 */
                              E12O22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CHECKLIST_CODIGO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13O22 */
                              E13O22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CHECK_CODIGO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14O22 */
                              E14O22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CHECKLIST_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15O22 */
                              E15O22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CHECKLIST_DE.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16O22 */
                              E16O22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CHECKLIST_OBRIGATORIO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17O22 */
                              E17O22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CHECKLIST_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18O22 */
                              E18O22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19O22 */
                              E19O22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20O22 */
                              E20O22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21O22 */
                              E21O22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22O22 */
                              E22O22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23O22 */
                              E23O22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24O22 */
                              E24O22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25O22 */
                              E25O22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E26O22 */
                              E26O22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E27O22 */
                              E27O22 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_71_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_71_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_71_idx), 4, 0)), 4, "0");
                              SubsflControlProps_712( ) ;
                              AV26Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV26Update)) ? AV101Update_GXI : context.convertURL( context.PathToRelativeUrl( AV26Update))));
                              AV28Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Delete)) ? AV102Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV28Delete))));
                              A758CheckList_Codigo = (int)(context.localUtil.CToN( cgiGet( edtCheckList_Codigo_Internalname), ",", "."));
                              A1839Check_Codigo = (int)(context.localUtil.CToN( cgiGet( edtCheck_Codigo_Internalname), ",", "."));
                              n1839Check_Codigo = false;
                              A763CheckList_Descricao = cgiGet( edtCheckList_Descricao_Internalname);
                              cmbCheckList_De.Name = cmbCheckList_De_Internalname;
                              cmbCheckList_De.CurrentValue = cgiGet( cmbCheckList_De_Internalname);
                              A1230CheckList_De = (short)(NumberUtil.Val( cgiGet( cmbCheckList_De_Internalname), "."));
                              n1230CheckList_De = false;
                              cmbCheckList_Obrigatorio.Name = cmbCheckList_Obrigatorio_Internalname;
                              cmbCheckList_Obrigatorio.CurrentValue = cgiGet( cmbCheckList_Obrigatorio_Internalname);
                              A1845CheckList_Obrigatorio = StringUtil.StrToBool( cgiGet( cmbCheckList_Obrigatorio_Internalname));
                              n1845CheckList_Obrigatorio = false;
                              A1151CheckList_Ativo = StringUtil.StrToBool( cgiGet( chkCheckList_Ativo_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E28O22 */
                                    E28O22 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E29O22 */
                                    E29O22 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E30O22 */
                                    E30O22 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Checklist_de1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCHECKLIST_DE1"), ",", ".") != Convert.ToDecimal( AV17CheckList_De1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Checklist_de2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCHECKLIST_DE2"), ",", ".") != Convert.ToDecimal( AV20CheckList_De2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV22DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Checklist_de3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCHECKLIST_DE3"), ",", ".") != Convert.ToDecimal( AV23CheckList_De3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV21DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfchecklist_codigo Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCHECKLIST_CODIGO"), ",", ".") != Convert.ToDecimal( AV57TFCheckList_Codigo )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfchecklist_codigo_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCHECKLIST_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV58TFCheckList_Codigo_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcheck_codigo Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCHECK_CODIGO"), ",", ".") != Convert.ToDecimal( AV61TFCheck_Codigo )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcheck_codigo_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCHECK_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV62TFCheck_Codigo_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfchecklist_descricao Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCHECKLIST_DESCRICAO"), AV65TFCheckList_Descricao) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfchecklist_descricao_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCHECKLIST_DESCRICAO_SEL"), AV66TFCheckList_Descricao_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfchecklist_obrigatorio_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCHECKLIST_OBRIGATORIO_SEL"), ",", ".") != Convert.ToDecimal( AV73TFCheckList_Obrigatorio_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfchecklist_ativo_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCHECKLIST_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV76TFCheckList_Ativo_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEO22( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAO22( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CHECKLIST_DE", "List de", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavChecklist_de1.Name = "vCHECKLIST_DE1";
            cmbavChecklist_de1.WebTags = "";
            cmbavChecklist_de1.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Todos", 0);
            cmbavChecklist_de1.addItem("1", "Libera valida��o", 0);
            cmbavChecklist_de1.addItem("2", "Em an�lise", 0);
            if ( cmbavChecklist_de1.ItemCount > 0 )
            {
               AV17CheckList_De1 = (short)(NumberUtil.Val( cmbavChecklist_de1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17CheckList_De1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17CheckList_De1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17CheckList_De1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CHECKLIST_DE", "List de", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavChecklist_de2.Name = "vCHECKLIST_DE2";
            cmbavChecklist_de2.WebTags = "";
            cmbavChecklist_de2.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Todos", 0);
            cmbavChecklist_de2.addItem("1", "Libera valida��o", 0);
            cmbavChecklist_de2.addItem("2", "Em an�lise", 0);
            if ( cmbavChecklist_de2.ItemCount > 0 )
            {
               AV20CheckList_De2 = (short)(NumberUtil.Val( cmbavChecklist_de2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20CheckList_De2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20CheckList_De2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20CheckList_De2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CHECKLIST_DE", "List de", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV22DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV22DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersSelector3", AV22DynamicFiltersSelector3);
            }
            cmbavChecklist_de3.Name = "vCHECKLIST_DE3";
            cmbavChecklist_de3.WebTags = "";
            cmbavChecklist_de3.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Todos", 0);
            cmbavChecklist_de3.addItem("1", "Libera valida��o", 0);
            cmbavChecklist_de3.addItem("2", "Em an�lise", 0);
            if ( cmbavChecklist_de3.ItemCount > 0 )
            {
               AV23CheckList_De3 = (short)(NumberUtil.Val( cmbavChecklist_de3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV23CheckList_De3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23CheckList_De3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23CheckList_De3), 4, 0)));
            }
            GXCCtl = "CHECKLIST_DE_" + sGXsfl_71_idx;
            cmbCheckList_De.Name = GXCCtl;
            cmbCheckList_De.WebTags = "";
            cmbCheckList_De.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Nenhum)", 0);
            cmbCheckList_De.addItem("1", "Libera valida��o", 0);
            cmbCheckList_De.addItem("2", "Em an�lise", 0);
            if ( cmbCheckList_De.ItemCount > 0 )
            {
               A1230CheckList_De = (short)(NumberUtil.Val( cmbCheckList_De.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1230CheckList_De), 4, 0))), "."));
               n1230CheckList_De = false;
            }
            GXCCtl = "CHECKLIST_OBRIGATORIO_" + sGXsfl_71_idx;
            cmbCheckList_Obrigatorio.Name = GXCCtl;
            cmbCheckList_Obrigatorio.WebTags = "";
            cmbCheckList_Obrigatorio.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbCheckList_Obrigatorio.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbCheckList_Obrigatorio.ItemCount > 0 )
            {
               A1845CheckList_Obrigatorio = StringUtil.StrToBool( cmbCheckList_Obrigatorio.getValidValue(StringUtil.BoolToStr( A1845CheckList_Obrigatorio)));
               n1845CheckList_Obrigatorio = false;
            }
            GXCCtl = "CHECKLIST_ATIVO_" + sGXsfl_71_idx;
            chkCheckList_Ativo.Name = GXCCtl;
            chkCheckList_Ativo.WebTags = "";
            chkCheckList_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkCheckList_Ativo_Internalname, "TitleCaption", chkCheckList_Ativo.Caption);
            chkCheckList_Ativo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_712( ) ;
         while ( nGXsfl_71_idx <= nRC_GXsfl_71 )
         {
            sendrow_712( ) ;
            nGXsfl_71_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_71_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_71_idx+1));
            sGXsfl_71_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_71_idx), 4, 0)), 4, "0");
            SubsflControlProps_712( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       short AV17CheckList_De1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV20CheckList_De2 ,
                                       String AV22DynamicFiltersSelector3 ,
                                       short AV23CheckList_De3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV21DynamicFiltersEnabled3 ,
                                       int AV57TFCheckList_Codigo ,
                                       int AV58TFCheckList_Codigo_To ,
                                       int AV61TFCheck_Codigo ,
                                       int AV62TFCheck_Codigo_To ,
                                       String AV65TFCheckList_Descricao ,
                                       String AV66TFCheckList_Descricao_Sel ,
                                       short AV73TFCheckList_Obrigatorio_Sel ,
                                       short AV76TFCheckList_Ativo_Sel ,
                                       short AV50ManageFiltersExecutionStep ,
                                       String AV59ddo_CheckList_CodigoTitleControlIdToReplace ,
                                       String AV63ddo_Check_CodigoTitleControlIdToReplace ,
                                       String AV67ddo_CheckList_DescricaoTitleControlIdToReplace ,
                                       String AV71ddo_CheckList_DeTitleControlIdToReplace ,
                                       String AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace ,
                                       String AV77ddo_CheckList_AtivoTitleControlIdToReplace ,
                                       IGxCollection AV70TFCheckList_De_Sels ,
                                       String AV103Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV25DynamicFiltersIgnoreFirst ,
                                       bool AV24DynamicFiltersRemoving ,
                                       int A758CheckList_Codigo ,
                                       int AV27Check_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFO22( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CHECKLIST_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A758CheckList_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CHECKLIST_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A758CheckList_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CHECK_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1839Check_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CHECK_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1839Check_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CHECKLIST_DESCRICAO", GetSecureSignedToken( "", A763CheckList_Descricao));
         GxWebStd.gx_hidden_field( context, "CHECKLIST_DESCRICAO", A763CheckList_Descricao);
         GxWebStd.gx_hidden_field( context, "gxhash_CHECKLIST_DE", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1230CheckList_De), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "CHECKLIST_DE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1230CheckList_De), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CHECKLIST_OBRIGATORIO", GetSecureSignedToken( "", A1845CheckList_Obrigatorio));
         GxWebStd.gx_hidden_field( context, "CHECKLIST_OBRIGATORIO", StringUtil.BoolToStr( A1845CheckList_Obrigatorio));
         GxWebStd.gx_hidden_field( context, "gxhash_CHECKLIST_ATIVO", GetSecureSignedToken( "", A1151CheckList_Ativo));
         GxWebStd.gx_hidden_field( context, "CHECKLIST_ATIVO", StringUtil.BoolToStr( A1151CheckList_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavChecklist_de1.ItemCount > 0 )
         {
            AV17CheckList_De1 = (short)(NumberUtil.Val( cmbavChecklist_de1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17CheckList_De1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17CheckList_De1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17CheckList_De1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavChecklist_de2.ItemCount > 0 )
         {
            AV20CheckList_De2 = (short)(NumberUtil.Val( cmbavChecklist_de2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20CheckList_De2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20CheckList_De2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20CheckList_De2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV22DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV22DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersSelector3", AV22DynamicFiltersSelector3);
         }
         if ( cmbavChecklist_de3.ItemCount > 0 )
         {
            AV23CheckList_De3 = (short)(NumberUtil.Val( cmbavChecklist_de3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV23CheckList_De3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23CheckList_De3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23CheckList_De3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFO22( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV103Pgmname = "WWCheckList";
         context.Gx_err = 0;
      }

      protected void RFO22( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 71;
         /* Execute user event: E29O22 */
         E29O22 ();
         nGXsfl_71_idx = 1;
         sGXsfl_71_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_71_idx), 4, 0)), 4, "0");
         SubsflControlProps_712( ) ;
         nGXsfl_71_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_712( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A1230CheckList_De ,
                                                 AV98WWCheckListDS_15_Tfchecklist_de_sels ,
                                                 AV84WWCheckListDS_1_Dynamicfiltersselector1 ,
                                                 AV85WWCheckListDS_2_Checklist_de1 ,
                                                 AV86WWCheckListDS_3_Dynamicfiltersenabled2 ,
                                                 AV87WWCheckListDS_4_Dynamicfiltersselector2 ,
                                                 AV88WWCheckListDS_5_Checklist_de2 ,
                                                 AV89WWCheckListDS_6_Dynamicfiltersenabled3 ,
                                                 AV90WWCheckListDS_7_Dynamicfiltersselector3 ,
                                                 AV91WWCheckListDS_8_Checklist_de3 ,
                                                 AV92WWCheckListDS_9_Tfchecklist_codigo ,
                                                 AV93WWCheckListDS_10_Tfchecklist_codigo_to ,
                                                 AV94WWCheckListDS_11_Tfcheck_codigo ,
                                                 AV95WWCheckListDS_12_Tfcheck_codigo_to ,
                                                 AV97WWCheckListDS_14_Tfchecklist_descricao_sel ,
                                                 AV96WWCheckListDS_13_Tfchecklist_descricao ,
                                                 AV98WWCheckListDS_15_Tfchecklist_de_sels.Count ,
                                                 AV99WWCheckListDS_16_Tfchecklist_obrigatorio_sel ,
                                                 AV100WWCheckListDS_17_Tfchecklist_ativo_sel ,
                                                 A758CheckList_Codigo ,
                                                 A1839Check_Codigo ,
                                                 A763CheckList_Descricao ,
                                                 A1845CheckList_Obrigatorio ,
                                                 A1151CheckList_Ativo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT,
                                                 TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV96WWCheckListDS_13_Tfchecklist_descricao = StringUtil.Concat( StringUtil.RTrim( AV96WWCheckListDS_13_Tfchecklist_descricao), "%", "");
            /* Using cursor H00O22 */
            pr_default.execute(0, new Object[] {AV85WWCheckListDS_2_Checklist_de1, AV88WWCheckListDS_5_Checklist_de2, AV91WWCheckListDS_8_Checklist_de3, AV92WWCheckListDS_9_Tfchecklist_codigo, AV93WWCheckListDS_10_Tfchecklist_codigo_to, AV94WWCheckListDS_11_Tfcheck_codigo, AV95WWCheckListDS_12_Tfcheck_codigo_to, lV96WWCheckListDS_13_Tfchecklist_descricao, AV97WWCheckListDS_14_Tfchecklist_descricao_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_71_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1151CheckList_Ativo = H00O22_A1151CheckList_Ativo[0];
               A1845CheckList_Obrigatorio = H00O22_A1845CheckList_Obrigatorio[0];
               n1845CheckList_Obrigatorio = H00O22_n1845CheckList_Obrigatorio[0];
               A1230CheckList_De = H00O22_A1230CheckList_De[0];
               n1230CheckList_De = H00O22_n1230CheckList_De[0];
               A763CheckList_Descricao = H00O22_A763CheckList_Descricao[0];
               A1839Check_Codigo = H00O22_A1839Check_Codigo[0];
               n1839Check_Codigo = H00O22_n1839Check_Codigo[0];
               A758CheckList_Codigo = H00O22_A758CheckList_Codigo[0];
               /* Execute user event: E30O22 */
               E30O22 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 71;
            WBO20( ) ;
         }
         nGXsfl_71_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV84WWCheckListDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV85WWCheckListDS_2_Checklist_de1 = AV17CheckList_De1;
         AV86WWCheckListDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV87WWCheckListDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV88WWCheckListDS_5_Checklist_de2 = AV20CheckList_De2;
         AV89WWCheckListDS_6_Dynamicfiltersenabled3 = AV21DynamicFiltersEnabled3;
         AV90WWCheckListDS_7_Dynamicfiltersselector3 = AV22DynamicFiltersSelector3;
         AV91WWCheckListDS_8_Checklist_de3 = AV23CheckList_De3;
         AV92WWCheckListDS_9_Tfchecklist_codigo = AV57TFCheckList_Codigo;
         AV93WWCheckListDS_10_Tfchecklist_codigo_to = AV58TFCheckList_Codigo_To;
         AV94WWCheckListDS_11_Tfcheck_codigo = AV61TFCheck_Codigo;
         AV95WWCheckListDS_12_Tfcheck_codigo_to = AV62TFCheck_Codigo_To;
         AV96WWCheckListDS_13_Tfchecklist_descricao = AV65TFCheckList_Descricao;
         AV97WWCheckListDS_14_Tfchecklist_descricao_sel = AV66TFCheckList_Descricao_Sel;
         AV98WWCheckListDS_15_Tfchecklist_de_sels = AV70TFCheckList_De_Sels;
         AV99WWCheckListDS_16_Tfchecklist_obrigatorio_sel = AV73TFCheckList_Obrigatorio_Sel;
         AV100WWCheckListDS_17_Tfchecklist_ativo_sel = AV76TFCheckList_Ativo_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A1230CheckList_De ,
                                              AV98WWCheckListDS_15_Tfchecklist_de_sels ,
                                              AV84WWCheckListDS_1_Dynamicfiltersselector1 ,
                                              AV85WWCheckListDS_2_Checklist_de1 ,
                                              AV86WWCheckListDS_3_Dynamicfiltersenabled2 ,
                                              AV87WWCheckListDS_4_Dynamicfiltersselector2 ,
                                              AV88WWCheckListDS_5_Checklist_de2 ,
                                              AV89WWCheckListDS_6_Dynamicfiltersenabled3 ,
                                              AV90WWCheckListDS_7_Dynamicfiltersselector3 ,
                                              AV91WWCheckListDS_8_Checklist_de3 ,
                                              AV92WWCheckListDS_9_Tfchecklist_codigo ,
                                              AV93WWCheckListDS_10_Tfchecklist_codigo_to ,
                                              AV94WWCheckListDS_11_Tfcheck_codigo ,
                                              AV95WWCheckListDS_12_Tfcheck_codigo_to ,
                                              AV97WWCheckListDS_14_Tfchecklist_descricao_sel ,
                                              AV96WWCheckListDS_13_Tfchecklist_descricao ,
                                              AV98WWCheckListDS_15_Tfchecklist_de_sels.Count ,
                                              AV99WWCheckListDS_16_Tfchecklist_obrigatorio_sel ,
                                              AV100WWCheckListDS_17_Tfchecklist_ativo_sel ,
                                              A758CheckList_Codigo ,
                                              A1839Check_Codigo ,
                                              A763CheckList_Descricao ,
                                              A1845CheckList_Obrigatorio ,
                                              A1151CheckList_Ativo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV96WWCheckListDS_13_Tfchecklist_descricao = StringUtil.Concat( StringUtil.RTrim( AV96WWCheckListDS_13_Tfchecklist_descricao), "%", "");
         /* Using cursor H00O23 */
         pr_default.execute(1, new Object[] {AV85WWCheckListDS_2_Checklist_de1, AV88WWCheckListDS_5_Checklist_de2, AV91WWCheckListDS_8_Checklist_de3, AV92WWCheckListDS_9_Tfchecklist_codigo, AV93WWCheckListDS_10_Tfchecklist_codigo_to, AV94WWCheckListDS_11_Tfcheck_codigo, AV95WWCheckListDS_12_Tfcheck_codigo_to, lV96WWCheckListDS_13_Tfchecklist_descricao, AV97WWCheckListDS_14_Tfchecklist_descricao_sel});
         GRID_nRecordCount = H00O23_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV84WWCheckListDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV85WWCheckListDS_2_Checklist_de1 = AV17CheckList_De1;
         AV86WWCheckListDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV87WWCheckListDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV88WWCheckListDS_5_Checklist_de2 = AV20CheckList_De2;
         AV89WWCheckListDS_6_Dynamicfiltersenabled3 = AV21DynamicFiltersEnabled3;
         AV90WWCheckListDS_7_Dynamicfiltersselector3 = AV22DynamicFiltersSelector3;
         AV91WWCheckListDS_8_Checklist_de3 = AV23CheckList_De3;
         AV92WWCheckListDS_9_Tfchecklist_codigo = AV57TFCheckList_Codigo;
         AV93WWCheckListDS_10_Tfchecklist_codigo_to = AV58TFCheckList_Codigo_To;
         AV94WWCheckListDS_11_Tfcheck_codigo = AV61TFCheck_Codigo;
         AV95WWCheckListDS_12_Tfcheck_codigo_to = AV62TFCheck_Codigo_To;
         AV96WWCheckListDS_13_Tfchecklist_descricao = AV65TFCheckList_Descricao;
         AV97WWCheckListDS_14_Tfchecklist_descricao_sel = AV66TFCheckList_Descricao_Sel;
         AV98WWCheckListDS_15_Tfchecklist_de_sels = AV70TFCheckList_De_Sels;
         AV99WWCheckListDS_16_Tfchecklist_obrigatorio_sel = AV73TFCheckList_Obrigatorio_Sel;
         AV100WWCheckListDS_17_Tfchecklist_ativo_sel = AV76TFCheckList_Ativo_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17CheckList_De1, AV19DynamicFiltersSelector2, AV20CheckList_De2, AV22DynamicFiltersSelector3, AV23CheckList_De3, AV18DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV57TFCheckList_Codigo, AV58TFCheckList_Codigo_To, AV61TFCheck_Codigo, AV62TFCheck_Codigo_To, AV65TFCheckList_Descricao, AV66TFCheckList_Descricao_Sel, AV73TFCheckList_Obrigatorio_Sel, AV76TFCheckList_Ativo_Sel, AV50ManageFiltersExecutionStep, AV59ddo_CheckList_CodigoTitleControlIdToReplace, AV63ddo_Check_CodigoTitleControlIdToReplace, AV67ddo_CheckList_DescricaoTitleControlIdToReplace, AV71ddo_CheckList_DeTitleControlIdToReplace, AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace, AV77ddo_CheckList_AtivoTitleControlIdToReplace, AV70TFCheckList_De_Sels, AV103Pgmname, AV10GridState, AV25DynamicFiltersIgnoreFirst, AV24DynamicFiltersRemoving, A758CheckList_Codigo, AV27Check_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV84WWCheckListDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV85WWCheckListDS_2_Checklist_de1 = AV17CheckList_De1;
         AV86WWCheckListDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV87WWCheckListDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV88WWCheckListDS_5_Checklist_de2 = AV20CheckList_De2;
         AV89WWCheckListDS_6_Dynamicfiltersenabled3 = AV21DynamicFiltersEnabled3;
         AV90WWCheckListDS_7_Dynamicfiltersselector3 = AV22DynamicFiltersSelector3;
         AV91WWCheckListDS_8_Checklist_de3 = AV23CheckList_De3;
         AV92WWCheckListDS_9_Tfchecklist_codigo = AV57TFCheckList_Codigo;
         AV93WWCheckListDS_10_Tfchecklist_codigo_to = AV58TFCheckList_Codigo_To;
         AV94WWCheckListDS_11_Tfcheck_codigo = AV61TFCheck_Codigo;
         AV95WWCheckListDS_12_Tfcheck_codigo_to = AV62TFCheck_Codigo_To;
         AV96WWCheckListDS_13_Tfchecklist_descricao = AV65TFCheckList_Descricao;
         AV97WWCheckListDS_14_Tfchecklist_descricao_sel = AV66TFCheckList_Descricao_Sel;
         AV98WWCheckListDS_15_Tfchecklist_de_sels = AV70TFCheckList_De_Sels;
         AV99WWCheckListDS_16_Tfchecklist_obrigatorio_sel = AV73TFCheckList_Obrigatorio_Sel;
         AV100WWCheckListDS_17_Tfchecklist_ativo_sel = AV76TFCheckList_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17CheckList_De1, AV19DynamicFiltersSelector2, AV20CheckList_De2, AV22DynamicFiltersSelector3, AV23CheckList_De3, AV18DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV57TFCheckList_Codigo, AV58TFCheckList_Codigo_To, AV61TFCheck_Codigo, AV62TFCheck_Codigo_To, AV65TFCheckList_Descricao, AV66TFCheckList_Descricao_Sel, AV73TFCheckList_Obrigatorio_Sel, AV76TFCheckList_Ativo_Sel, AV50ManageFiltersExecutionStep, AV59ddo_CheckList_CodigoTitleControlIdToReplace, AV63ddo_Check_CodigoTitleControlIdToReplace, AV67ddo_CheckList_DescricaoTitleControlIdToReplace, AV71ddo_CheckList_DeTitleControlIdToReplace, AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace, AV77ddo_CheckList_AtivoTitleControlIdToReplace, AV70TFCheckList_De_Sels, AV103Pgmname, AV10GridState, AV25DynamicFiltersIgnoreFirst, AV24DynamicFiltersRemoving, A758CheckList_Codigo, AV27Check_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV84WWCheckListDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV85WWCheckListDS_2_Checklist_de1 = AV17CheckList_De1;
         AV86WWCheckListDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV87WWCheckListDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV88WWCheckListDS_5_Checklist_de2 = AV20CheckList_De2;
         AV89WWCheckListDS_6_Dynamicfiltersenabled3 = AV21DynamicFiltersEnabled3;
         AV90WWCheckListDS_7_Dynamicfiltersselector3 = AV22DynamicFiltersSelector3;
         AV91WWCheckListDS_8_Checklist_de3 = AV23CheckList_De3;
         AV92WWCheckListDS_9_Tfchecklist_codigo = AV57TFCheckList_Codigo;
         AV93WWCheckListDS_10_Tfchecklist_codigo_to = AV58TFCheckList_Codigo_To;
         AV94WWCheckListDS_11_Tfcheck_codigo = AV61TFCheck_Codigo;
         AV95WWCheckListDS_12_Tfcheck_codigo_to = AV62TFCheck_Codigo_To;
         AV96WWCheckListDS_13_Tfchecklist_descricao = AV65TFCheckList_Descricao;
         AV97WWCheckListDS_14_Tfchecklist_descricao_sel = AV66TFCheckList_Descricao_Sel;
         AV98WWCheckListDS_15_Tfchecklist_de_sels = AV70TFCheckList_De_Sels;
         AV99WWCheckListDS_16_Tfchecklist_obrigatorio_sel = AV73TFCheckList_Obrigatorio_Sel;
         AV100WWCheckListDS_17_Tfchecklist_ativo_sel = AV76TFCheckList_Ativo_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17CheckList_De1, AV19DynamicFiltersSelector2, AV20CheckList_De2, AV22DynamicFiltersSelector3, AV23CheckList_De3, AV18DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV57TFCheckList_Codigo, AV58TFCheckList_Codigo_To, AV61TFCheck_Codigo, AV62TFCheck_Codigo_To, AV65TFCheckList_Descricao, AV66TFCheckList_Descricao_Sel, AV73TFCheckList_Obrigatorio_Sel, AV76TFCheckList_Ativo_Sel, AV50ManageFiltersExecutionStep, AV59ddo_CheckList_CodigoTitleControlIdToReplace, AV63ddo_Check_CodigoTitleControlIdToReplace, AV67ddo_CheckList_DescricaoTitleControlIdToReplace, AV71ddo_CheckList_DeTitleControlIdToReplace, AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace, AV77ddo_CheckList_AtivoTitleControlIdToReplace, AV70TFCheckList_De_Sels, AV103Pgmname, AV10GridState, AV25DynamicFiltersIgnoreFirst, AV24DynamicFiltersRemoving, A758CheckList_Codigo, AV27Check_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV84WWCheckListDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV85WWCheckListDS_2_Checklist_de1 = AV17CheckList_De1;
         AV86WWCheckListDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV87WWCheckListDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV88WWCheckListDS_5_Checklist_de2 = AV20CheckList_De2;
         AV89WWCheckListDS_6_Dynamicfiltersenabled3 = AV21DynamicFiltersEnabled3;
         AV90WWCheckListDS_7_Dynamicfiltersselector3 = AV22DynamicFiltersSelector3;
         AV91WWCheckListDS_8_Checklist_de3 = AV23CheckList_De3;
         AV92WWCheckListDS_9_Tfchecklist_codigo = AV57TFCheckList_Codigo;
         AV93WWCheckListDS_10_Tfchecklist_codigo_to = AV58TFCheckList_Codigo_To;
         AV94WWCheckListDS_11_Tfcheck_codigo = AV61TFCheck_Codigo;
         AV95WWCheckListDS_12_Tfcheck_codigo_to = AV62TFCheck_Codigo_To;
         AV96WWCheckListDS_13_Tfchecklist_descricao = AV65TFCheckList_Descricao;
         AV97WWCheckListDS_14_Tfchecklist_descricao_sel = AV66TFCheckList_Descricao_Sel;
         AV98WWCheckListDS_15_Tfchecklist_de_sels = AV70TFCheckList_De_Sels;
         AV99WWCheckListDS_16_Tfchecklist_obrigatorio_sel = AV73TFCheckList_Obrigatorio_Sel;
         AV100WWCheckListDS_17_Tfchecklist_ativo_sel = AV76TFCheckList_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17CheckList_De1, AV19DynamicFiltersSelector2, AV20CheckList_De2, AV22DynamicFiltersSelector3, AV23CheckList_De3, AV18DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV57TFCheckList_Codigo, AV58TFCheckList_Codigo_To, AV61TFCheck_Codigo, AV62TFCheck_Codigo_To, AV65TFCheckList_Descricao, AV66TFCheckList_Descricao_Sel, AV73TFCheckList_Obrigatorio_Sel, AV76TFCheckList_Ativo_Sel, AV50ManageFiltersExecutionStep, AV59ddo_CheckList_CodigoTitleControlIdToReplace, AV63ddo_Check_CodigoTitleControlIdToReplace, AV67ddo_CheckList_DescricaoTitleControlIdToReplace, AV71ddo_CheckList_DeTitleControlIdToReplace, AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace, AV77ddo_CheckList_AtivoTitleControlIdToReplace, AV70TFCheckList_De_Sels, AV103Pgmname, AV10GridState, AV25DynamicFiltersIgnoreFirst, AV24DynamicFiltersRemoving, A758CheckList_Codigo, AV27Check_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV84WWCheckListDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV85WWCheckListDS_2_Checklist_de1 = AV17CheckList_De1;
         AV86WWCheckListDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV87WWCheckListDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV88WWCheckListDS_5_Checklist_de2 = AV20CheckList_De2;
         AV89WWCheckListDS_6_Dynamicfiltersenabled3 = AV21DynamicFiltersEnabled3;
         AV90WWCheckListDS_7_Dynamicfiltersselector3 = AV22DynamicFiltersSelector3;
         AV91WWCheckListDS_8_Checklist_de3 = AV23CheckList_De3;
         AV92WWCheckListDS_9_Tfchecklist_codigo = AV57TFCheckList_Codigo;
         AV93WWCheckListDS_10_Tfchecklist_codigo_to = AV58TFCheckList_Codigo_To;
         AV94WWCheckListDS_11_Tfcheck_codigo = AV61TFCheck_Codigo;
         AV95WWCheckListDS_12_Tfcheck_codigo_to = AV62TFCheck_Codigo_To;
         AV96WWCheckListDS_13_Tfchecklist_descricao = AV65TFCheckList_Descricao;
         AV97WWCheckListDS_14_Tfchecklist_descricao_sel = AV66TFCheckList_Descricao_Sel;
         AV98WWCheckListDS_15_Tfchecklist_de_sels = AV70TFCheckList_De_Sels;
         AV99WWCheckListDS_16_Tfchecklist_obrigatorio_sel = AV73TFCheckList_Obrigatorio_Sel;
         AV100WWCheckListDS_17_Tfchecklist_ativo_sel = AV76TFCheckList_Ativo_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17CheckList_De1, AV19DynamicFiltersSelector2, AV20CheckList_De2, AV22DynamicFiltersSelector3, AV23CheckList_De3, AV18DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV57TFCheckList_Codigo, AV58TFCheckList_Codigo_To, AV61TFCheck_Codigo, AV62TFCheck_Codigo_To, AV65TFCheckList_Descricao, AV66TFCheckList_Descricao_Sel, AV73TFCheckList_Obrigatorio_Sel, AV76TFCheckList_Ativo_Sel, AV50ManageFiltersExecutionStep, AV59ddo_CheckList_CodigoTitleControlIdToReplace, AV63ddo_Check_CodigoTitleControlIdToReplace, AV67ddo_CheckList_DescricaoTitleControlIdToReplace, AV71ddo_CheckList_DeTitleControlIdToReplace, AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace, AV77ddo_CheckList_AtivoTitleControlIdToReplace, AV70TFCheckList_De_Sels, AV103Pgmname, AV10GridState, AV25DynamicFiltersIgnoreFirst, AV24DynamicFiltersRemoving, A758CheckList_Codigo, AV27Check_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPO20( )
      {
         /* Before Start, stand alone formulas. */
         AV103Pgmname = "WWCheckList";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E28O22 */
         E28O22 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vMANAGEFILTERSDATA"), AV54ManageFiltersData);
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV78DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCHECKLIST_CODIGOTITLEFILTERDATA"), AV56CheckList_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCHECK_CODIGOTITLEFILTERDATA"), AV60Check_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCHECKLIST_DESCRICAOTITLEFILTERDATA"), AV64CheckList_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCHECKLIST_DETITLEFILTERDATA"), AV68CheckList_DeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCHECKLIST_OBRIGATORIOTITLEFILTERDATA"), AV72CheckList_ObrigatorioTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCHECKLIST_ATIVOTITLEFILTERDATA"), AV75CheckList_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            cmbavChecklist_de1.Name = cmbavChecklist_de1_Internalname;
            cmbavChecklist_de1.CurrentValue = cgiGet( cmbavChecklist_de1_Internalname);
            AV17CheckList_De1 = (short)(NumberUtil.Val( cgiGet( cmbavChecklist_de1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17CheckList_De1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17CheckList_De1), 4, 0)));
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavChecklist_de2.Name = cmbavChecklist_de2_Internalname;
            cmbavChecklist_de2.CurrentValue = cgiGet( cmbavChecklist_de2_Internalname);
            AV20CheckList_De2 = (short)(NumberUtil.Val( cgiGet( cmbavChecklist_de2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20CheckList_De2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20CheckList_De2), 4, 0)));
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV22DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersSelector3", AV22DynamicFiltersSelector3);
            cmbavChecklist_de3.Name = cmbavChecklist_de3_Internalname;
            cmbavChecklist_de3.CurrentValue = cgiGet( cmbavChecklist_de3_Internalname);
            AV23CheckList_De3 = (short)(NumberUtil.Val( cgiGet( cmbavChecklist_de3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23CheckList_De3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23CheckList_De3), 4, 0)));
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV21DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersEnabled3", AV21DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vMANAGEFILTERSEXECUTIONSTEP");
               GX_FocusControl = edtavManagefiltersexecutionstep_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV50ManageFiltersExecutionStep = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV50ManageFiltersExecutionStep), 1, 0));
            }
            else
            {
               AV50ManageFiltersExecutionStep = (short)(context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV50ManageFiltersExecutionStep), 1, 0));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfchecklist_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfchecklist_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCHECKLIST_CODIGO");
               GX_FocusControl = edtavTfchecklist_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV57TFCheckList_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFCheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57TFCheckList_Codigo), 6, 0)));
            }
            else
            {
               AV57TFCheckList_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfchecklist_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFCheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57TFCheckList_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfchecklist_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfchecklist_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCHECKLIST_CODIGO_TO");
               GX_FocusControl = edtavTfchecklist_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV58TFCheckList_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFCheckList_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFCheckList_Codigo_To), 6, 0)));
            }
            else
            {
               AV58TFCheckList_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfchecklist_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFCheckList_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFCheckList_Codigo_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcheck_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcheck_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCHECK_CODIGO");
               GX_FocusControl = edtavTfcheck_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV61TFCheck_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFCheck_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFCheck_Codigo), 6, 0)));
            }
            else
            {
               AV61TFCheck_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfcheck_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFCheck_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFCheck_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcheck_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcheck_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCHECK_CODIGO_TO");
               GX_FocusControl = edtavTfcheck_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV62TFCheck_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFCheck_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFCheck_Codigo_To), 6, 0)));
            }
            else
            {
               AV62TFCheck_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcheck_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFCheck_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFCheck_Codigo_To), 6, 0)));
            }
            AV65TFCheckList_Descricao = cgiGet( edtavTfchecklist_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFCheckList_Descricao", AV65TFCheckList_Descricao);
            AV66TFCheckList_Descricao_Sel = cgiGet( edtavTfchecklist_descricao_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFCheckList_Descricao_Sel", AV66TFCheckList_Descricao_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfchecklist_obrigatorio_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfchecklist_obrigatorio_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCHECKLIST_OBRIGATORIO_SEL");
               GX_FocusControl = edtavTfchecklist_obrigatorio_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV73TFCheckList_Obrigatorio_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFCheckList_Obrigatorio_Sel", StringUtil.Str( (decimal)(AV73TFCheckList_Obrigatorio_Sel), 1, 0));
            }
            else
            {
               AV73TFCheckList_Obrigatorio_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfchecklist_obrigatorio_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFCheckList_Obrigatorio_Sel", StringUtil.Str( (decimal)(AV73TFCheckList_Obrigatorio_Sel), 1, 0));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfchecklist_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfchecklist_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCHECKLIST_ATIVO_SEL");
               GX_FocusControl = edtavTfchecklist_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV76TFCheckList_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFCheckList_Ativo_Sel", StringUtil.Str( (decimal)(AV76TFCheckList_Ativo_Sel), 1, 0));
            }
            else
            {
               AV76TFCheckList_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfchecklist_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFCheckList_Ativo_Sel", StringUtil.Str( (decimal)(AV76TFCheckList_Ativo_Sel), 1, 0));
            }
            AV59ddo_CheckList_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_checklist_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ddo_CheckList_CodigoTitleControlIdToReplace", AV59ddo_CheckList_CodigoTitleControlIdToReplace);
            AV63ddo_Check_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_check_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_Check_CodigoTitleControlIdToReplace", AV63ddo_Check_CodigoTitleControlIdToReplace);
            AV67ddo_CheckList_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_checklist_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_CheckList_DescricaoTitleControlIdToReplace", AV67ddo_CheckList_DescricaoTitleControlIdToReplace);
            AV71ddo_CheckList_DeTitleControlIdToReplace = cgiGet( edtavDdo_checklist_detitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ddo_CheckList_DeTitleControlIdToReplace", AV71ddo_CheckList_DeTitleControlIdToReplace);
            AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace = cgiGet( edtavDdo_checklist_obrigatoriotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace", AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace);
            AV77ddo_CheckList_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_checklist_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_CheckList_AtivoTitleControlIdToReplace", AV77ddo_CheckList_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_71 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_71"), ",", "."));
            AV80GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV81GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_managefilters_Icon = cgiGet( "DDO_MANAGEFILTERS_Icon");
            Ddo_managefilters_Caption = cgiGet( "DDO_MANAGEFILTERS_Caption");
            Ddo_managefilters_Tooltip = cgiGet( "DDO_MANAGEFILTERS_Tooltip");
            Ddo_managefilters_Cls = cgiGet( "DDO_MANAGEFILTERS_Cls");
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_checklist_codigo_Caption = cgiGet( "DDO_CHECKLIST_CODIGO_Caption");
            Ddo_checklist_codigo_Tooltip = cgiGet( "DDO_CHECKLIST_CODIGO_Tooltip");
            Ddo_checklist_codigo_Cls = cgiGet( "DDO_CHECKLIST_CODIGO_Cls");
            Ddo_checklist_codigo_Filteredtext_set = cgiGet( "DDO_CHECKLIST_CODIGO_Filteredtext_set");
            Ddo_checklist_codigo_Filteredtextto_set = cgiGet( "DDO_CHECKLIST_CODIGO_Filteredtextto_set");
            Ddo_checklist_codigo_Dropdownoptionstype = cgiGet( "DDO_CHECKLIST_CODIGO_Dropdownoptionstype");
            Ddo_checklist_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_CHECKLIST_CODIGO_Titlecontrolidtoreplace");
            Ddo_checklist_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CHECKLIST_CODIGO_Includesortasc"));
            Ddo_checklist_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CHECKLIST_CODIGO_Includesortdsc"));
            Ddo_checklist_codigo_Sortedstatus = cgiGet( "DDO_CHECKLIST_CODIGO_Sortedstatus");
            Ddo_checklist_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CHECKLIST_CODIGO_Includefilter"));
            Ddo_checklist_codigo_Filtertype = cgiGet( "DDO_CHECKLIST_CODIGO_Filtertype");
            Ddo_checklist_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CHECKLIST_CODIGO_Filterisrange"));
            Ddo_checklist_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CHECKLIST_CODIGO_Includedatalist"));
            Ddo_checklist_codigo_Sortasc = cgiGet( "DDO_CHECKLIST_CODIGO_Sortasc");
            Ddo_checklist_codigo_Sortdsc = cgiGet( "DDO_CHECKLIST_CODIGO_Sortdsc");
            Ddo_checklist_codigo_Cleanfilter = cgiGet( "DDO_CHECKLIST_CODIGO_Cleanfilter");
            Ddo_checklist_codigo_Rangefilterfrom = cgiGet( "DDO_CHECKLIST_CODIGO_Rangefilterfrom");
            Ddo_checklist_codigo_Rangefilterto = cgiGet( "DDO_CHECKLIST_CODIGO_Rangefilterto");
            Ddo_checklist_codigo_Searchbuttontext = cgiGet( "DDO_CHECKLIST_CODIGO_Searchbuttontext");
            Ddo_check_codigo_Caption = cgiGet( "DDO_CHECK_CODIGO_Caption");
            Ddo_check_codigo_Tooltip = cgiGet( "DDO_CHECK_CODIGO_Tooltip");
            Ddo_check_codigo_Cls = cgiGet( "DDO_CHECK_CODIGO_Cls");
            Ddo_check_codigo_Filteredtext_set = cgiGet( "DDO_CHECK_CODIGO_Filteredtext_set");
            Ddo_check_codigo_Filteredtextto_set = cgiGet( "DDO_CHECK_CODIGO_Filteredtextto_set");
            Ddo_check_codigo_Dropdownoptionstype = cgiGet( "DDO_CHECK_CODIGO_Dropdownoptionstype");
            Ddo_check_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_CHECK_CODIGO_Titlecontrolidtoreplace");
            Ddo_check_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CHECK_CODIGO_Includesortasc"));
            Ddo_check_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CHECK_CODIGO_Includesortdsc"));
            Ddo_check_codigo_Sortedstatus = cgiGet( "DDO_CHECK_CODIGO_Sortedstatus");
            Ddo_check_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CHECK_CODIGO_Includefilter"));
            Ddo_check_codigo_Filtertype = cgiGet( "DDO_CHECK_CODIGO_Filtertype");
            Ddo_check_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CHECK_CODIGO_Filterisrange"));
            Ddo_check_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CHECK_CODIGO_Includedatalist"));
            Ddo_check_codigo_Sortasc = cgiGet( "DDO_CHECK_CODIGO_Sortasc");
            Ddo_check_codigo_Sortdsc = cgiGet( "DDO_CHECK_CODIGO_Sortdsc");
            Ddo_check_codigo_Cleanfilter = cgiGet( "DDO_CHECK_CODIGO_Cleanfilter");
            Ddo_check_codigo_Rangefilterfrom = cgiGet( "DDO_CHECK_CODIGO_Rangefilterfrom");
            Ddo_check_codigo_Rangefilterto = cgiGet( "DDO_CHECK_CODIGO_Rangefilterto");
            Ddo_check_codigo_Searchbuttontext = cgiGet( "DDO_CHECK_CODIGO_Searchbuttontext");
            Ddo_checklist_descricao_Caption = cgiGet( "DDO_CHECKLIST_DESCRICAO_Caption");
            Ddo_checklist_descricao_Tooltip = cgiGet( "DDO_CHECKLIST_DESCRICAO_Tooltip");
            Ddo_checklist_descricao_Cls = cgiGet( "DDO_CHECKLIST_DESCRICAO_Cls");
            Ddo_checklist_descricao_Filteredtext_set = cgiGet( "DDO_CHECKLIST_DESCRICAO_Filteredtext_set");
            Ddo_checklist_descricao_Selectedvalue_set = cgiGet( "DDO_CHECKLIST_DESCRICAO_Selectedvalue_set");
            Ddo_checklist_descricao_Dropdownoptionstype = cgiGet( "DDO_CHECKLIST_DESCRICAO_Dropdownoptionstype");
            Ddo_checklist_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_CHECKLIST_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_checklist_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CHECKLIST_DESCRICAO_Includesortasc"));
            Ddo_checklist_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CHECKLIST_DESCRICAO_Includesortdsc"));
            Ddo_checklist_descricao_Sortedstatus = cgiGet( "DDO_CHECKLIST_DESCRICAO_Sortedstatus");
            Ddo_checklist_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CHECKLIST_DESCRICAO_Includefilter"));
            Ddo_checklist_descricao_Filtertype = cgiGet( "DDO_CHECKLIST_DESCRICAO_Filtertype");
            Ddo_checklist_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CHECKLIST_DESCRICAO_Filterisrange"));
            Ddo_checklist_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CHECKLIST_DESCRICAO_Includedatalist"));
            Ddo_checklist_descricao_Datalisttype = cgiGet( "DDO_CHECKLIST_DESCRICAO_Datalisttype");
            Ddo_checklist_descricao_Datalistproc = cgiGet( "DDO_CHECKLIST_DESCRICAO_Datalistproc");
            Ddo_checklist_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CHECKLIST_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_checklist_descricao_Sortasc = cgiGet( "DDO_CHECKLIST_DESCRICAO_Sortasc");
            Ddo_checklist_descricao_Sortdsc = cgiGet( "DDO_CHECKLIST_DESCRICAO_Sortdsc");
            Ddo_checklist_descricao_Loadingdata = cgiGet( "DDO_CHECKLIST_DESCRICAO_Loadingdata");
            Ddo_checklist_descricao_Cleanfilter = cgiGet( "DDO_CHECKLIST_DESCRICAO_Cleanfilter");
            Ddo_checklist_descricao_Noresultsfound = cgiGet( "DDO_CHECKLIST_DESCRICAO_Noresultsfound");
            Ddo_checklist_descricao_Searchbuttontext = cgiGet( "DDO_CHECKLIST_DESCRICAO_Searchbuttontext");
            Ddo_checklist_de_Caption = cgiGet( "DDO_CHECKLIST_DE_Caption");
            Ddo_checklist_de_Tooltip = cgiGet( "DDO_CHECKLIST_DE_Tooltip");
            Ddo_checklist_de_Cls = cgiGet( "DDO_CHECKLIST_DE_Cls");
            Ddo_checklist_de_Selectedvalue_set = cgiGet( "DDO_CHECKLIST_DE_Selectedvalue_set");
            Ddo_checklist_de_Dropdownoptionstype = cgiGet( "DDO_CHECKLIST_DE_Dropdownoptionstype");
            Ddo_checklist_de_Titlecontrolidtoreplace = cgiGet( "DDO_CHECKLIST_DE_Titlecontrolidtoreplace");
            Ddo_checklist_de_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CHECKLIST_DE_Includesortasc"));
            Ddo_checklist_de_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CHECKLIST_DE_Includesortdsc"));
            Ddo_checklist_de_Sortedstatus = cgiGet( "DDO_CHECKLIST_DE_Sortedstatus");
            Ddo_checklist_de_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CHECKLIST_DE_Includefilter"));
            Ddo_checklist_de_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CHECKLIST_DE_Includedatalist"));
            Ddo_checklist_de_Datalisttype = cgiGet( "DDO_CHECKLIST_DE_Datalisttype");
            Ddo_checklist_de_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_CHECKLIST_DE_Allowmultipleselection"));
            Ddo_checklist_de_Datalistfixedvalues = cgiGet( "DDO_CHECKLIST_DE_Datalistfixedvalues");
            Ddo_checklist_de_Sortasc = cgiGet( "DDO_CHECKLIST_DE_Sortasc");
            Ddo_checklist_de_Sortdsc = cgiGet( "DDO_CHECKLIST_DE_Sortdsc");
            Ddo_checklist_de_Cleanfilter = cgiGet( "DDO_CHECKLIST_DE_Cleanfilter");
            Ddo_checklist_de_Searchbuttontext = cgiGet( "DDO_CHECKLIST_DE_Searchbuttontext");
            Ddo_checklist_obrigatorio_Caption = cgiGet( "DDO_CHECKLIST_OBRIGATORIO_Caption");
            Ddo_checklist_obrigatorio_Tooltip = cgiGet( "DDO_CHECKLIST_OBRIGATORIO_Tooltip");
            Ddo_checklist_obrigatorio_Cls = cgiGet( "DDO_CHECKLIST_OBRIGATORIO_Cls");
            Ddo_checklist_obrigatorio_Selectedvalue_set = cgiGet( "DDO_CHECKLIST_OBRIGATORIO_Selectedvalue_set");
            Ddo_checklist_obrigatorio_Dropdownoptionstype = cgiGet( "DDO_CHECKLIST_OBRIGATORIO_Dropdownoptionstype");
            Ddo_checklist_obrigatorio_Titlecontrolidtoreplace = cgiGet( "DDO_CHECKLIST_OBRIGATORIO_Titlecontrolidtoreplace");
            Ddo_checklist_obrigatorio_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CHECKLIST_OBRIGATORIO_Includesortasc"));
            Ddo_checklist_obrigatorio_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CHECKLIST_OBRIGATORIO_Includesortdsc"));
            Ddo_checklist_obrigatorio_Sortedstatus = cgiGet( "DDO_CHECKLIST_OBRIGATORIO_Sortedstatus");
            Ddo_checklist_obrigatorio_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CHECKLIST_OBRIGATORIO_Includefilter"));
            Ddo_checklist_obrigatorio_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CHECKLIST_OBRIGATORIO_Includedatalist"));
            Ddo_checklist_obrigatorio_Datalisttype = cgiGet( "DDO_CHECKLIST_OBRIGATORIO_Datalisttype");
            Ddo_checklist_obrigatorio_Datalistfixedvalues = cgiGet( "DDO_CHECKLIST_OBRIGATORIO_Datalistfixedvalues");
            Ddo_checklist_obrigatorio_Sortasc = cgiGet( "DDO_CHECKLIST_OBRIGATORIO_Sortasc");
            Ddo_checklist_obrigatorio_Sortdsc = cgiGet( "DDO_CHECKLIST_OBRIGATORIO_Sortdsc");
            Ddo_checklist_obrigatorio_Cleanfilter = cgiGet( "DDO_CHECKLIST_OBRIGATORIO_Cleanfilter");
            Ddo_checklist_obrigatorio_Searchbuttontext = cgiGet( "DDO_CHECKLIST_OBRIGATORIO_Searchbuttontext");
            Ddo_checklist_ativo_Caption = cgiGet( "DDO_CHECKLIST_ATIVO_Caption");
            Ddo_checklist_ativo_Tooltip = cgiGet( "DDO_CHECKLIST_ATIVO_Tooltip");
            Ddo_checklist_ativo_Cls = cgiGet( "DDO_CHECKLIST_ATIVO_Cls");
            Ddo_checklist_ativo_Selectedvalue_set = cgiGet( "DDO_CHECKLIST_ATIVO_Selectedvalue_set");
            Ddo_checklist_ativo_Dropdownoptionstype = cgiGet( "DDO_CHECKLIST_ATIVO_Dropdownoptionstype");
            Ddo_checklist_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_CHECKLIST_ATIVO_Titlecontrolidtoreplace");
            Ddo_checklist_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CHECKLIST_ATIVO_Includesortasc"));
            Ddo_checklist_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CHECKLIST_ATIVO_Includesortdsc"));
            Ddo_checklist_ativo_Sortedstatus = cgiGet( "DDO_CHECKLIST_ATIVO_Sortedstatus");
            Ddo_checklist_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CHECKLIST_ATIVO_Includefilter"));
            Ddo_checklist_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CHECKLIST_ATIVO_Includedatalist"));
            Ddo_checklist_ativo_Datalisttype = cgiGet( "DDO_CHECKLIST_ATIVO_Datalisttype");
            Ddo_checklist_ativo_Datalistfixedvalues = cgiGet( "DDO_CHECKLIST_ATIVO_Datalistfixedvalues");
            Ddo_checklist_ativo_Sortasc = cgiGet( "DDO_CHECKLIST_ATIVO_Sortasc");
            Ddo_checklist_ativo_Sortdsc = cgiGet( "DDO_CHECKLIST_ATIVO_Sortdsc");
            Ddo_checklist_ativo_Cleanfilter = cgiGet( "DDO_CHECKLIST_ATIVO_Cleanfilter");
            Ddo_checklist_ativo_Searchbuttontext = cgiGet( "DDO_CHECKLIST_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_checklist_codigo_Activeeventkey = cgiGet( "DDO_CHECKLIST_CODIGO_Activeeventkey");
            Ddo_checklist_codigo_Filteredtext_get = cgiGet( "DDO_CHECKLIST_CODIGO_Filteredtext_get");
            Ddo_checklist_codigo_Filteredtextto_get = cgiGet( "DDO_CHECKLIST_CODIGO_Filteredtextto_get");
            Ddo_check_codigo_Activeeventkey = cgiGet( "DDO_CHECK_CODIGO_Activeeventkey");
            Ddo_check_codigo_Filteredtext_get = cgiGet( "DDO_CHECK_CODIGO_Filteredtext_get");
            Ddo_check_codigo_Filteredtextto_get = cgiGet( "DDO_CHECK_CODIGO_Filteredtextto_get");
            Ddo_checklist_descricao_Activeeventkey = cgiGet( "DDO_CHECKLIST_DESCRICAO_Activeeventkey");
            Ddo_checklist_descricao_Filteredtext_get = cgiGet( "DDO_CHECKLIST_DESCRICAO_Filteredtext_get");
            Ddo_checklist_descricao_Selectedvalue_get = cgiGet( "DDO_CHECKLIST_DESCRICAO_Selectedvalue_get");
            Ddo_checklist_de_Activeeventkey = cgiGet( "DDO_CHECKLIST_DE_Activeeventkey");
            Ddo_checklist_de_Selectedvalue_get = cgiGet( "DDO_CHECKLIST_DE_Selectedvalue_get");
            Ddo_checklist_obrigatorio_Activeeventkey = cgiGet( "DDO_CHECKLIST_OBRIGATORIO_Activeeventkey");
            Ddo_checklist_obrigatorio_Selectedvalue_get = cgiGet( "DDO_CHECKLIST_OBRIGATORIO_Selectedvalue_get");
            Ddo_checklist_ativo_Activeeventkey = cgiGet( "DDO_CHECKLIST_ATIVO_Activeeventkey");
            Ddo_checklist_ativo_Selectedvalue_get = cgiGet( "DDO_CHECKLIST_ATIVO_Selectedvalue_get");
            Ddo_managefilters_Activeeventkey = cgiGet( "DDO_MANAGEFILTERS_Activeeventkey");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCHECKLIST_DE1"), ",", ".") != Convert.ToDecimal( AV17CheckList_De1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCHECKLIST_DE2"), ",", ".") != Convert.ToDecimal( AV20CheckList_De2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV22DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCHECKLIST_DE3"), ",", ".") != Convert.ToDecimal( AV23CheckList_De3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV21DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCHECKLIST_CODIGO"), ",", ".") != Convert.ToDecimal( AV57TFCheckList_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCHECKLIST_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV58TFCheckList_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCHECK_CODIGO"), ",", ".") != Convert.ToDecimal( AV61TFCheck_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCHECK_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV62TFCheck_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCHECKLIST_DESCRICAO"), AV65TFCheckList_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCHECKLIST_DESCRICAO_SEL"), AV66TFCheckList_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCHECKLIST_OBRIGATORIO_SEL"), ",", ".") != Convert.ToDecimal( AV73TFCheckList_Obrigatorio_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCHECKLIST_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV76TFCheckList_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E28O22 */
         E28O22 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E28O22( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         if ( StringUtil.StrCmp(AV7HTTPRequest.Method, "GET") == 0 )
         {
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            edtavManagefiltersexecutionstep_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavManagefiltersexecutionstep_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavManagefiltersexecutionstep_Visible), 5, 0)));
         }
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV17CheckList_De1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17CheckList_De1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17CheckList_De1), 4, 0)));
         AV16DynamicFiltersSelector1 = "CHECKLIST_DE";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20CheckList_De2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20CheckList_De2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20CheckList_De2), 4, 0)));
         AV19DynamicFiltersSelector2 = "CHECKLIST_DE";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23CheckList_De3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23CheckList_De3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23CheckList_De3), 4, 0)));
         AV22DynamicFiltersSelector3 = "CHECKLIST_DE";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersSelector3", AV22DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfchecklist_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfchecklist_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfchecklist_codigo_Visible), 5, 0)));
         edtavTfchecklist_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfchecklist_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfchecklist_codigo_to_Visible), 5, 0)));
         edtavTfcheck_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcheck_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcheck_codigo_Visible), 5, 0)));
         edtavTfcheck_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcheck_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcheck_codigo_to_Visible), 5, 0)));
         edtavTfchecklist_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfchecklist_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfchecklist_descricao_Visible), 5, 0)));
         edtavTfchecklist_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfchecklist_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfchecklist_descricao_sel_Visible), 5, 0)));
         edtavTfchecklist_obrigatorio_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfchecklist_obrigatorio_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfchecklist_obrigatorio_sel_Visible), 5, 0)));
         edtavTfchecklist_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfchecklist_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfchecklist_ativo_sel_Visible), 5, 0)));
         Ddo_checklist_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_CheckList_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_codigo_Internalname, "TitleControlIdToReplace", Ddo_checklist_codigo_Titlecontrolidtoreplace);
         AV59ddo_CheckList_CodigoTitleControlIdToReplace = Ddo_checklist_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ddo_CheckList_CodigoTitleControlIdToReplace", AV59ddo_CheckList_CodigoTitleControlIdToReplace);
         edtavDdo_checklist_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_checklist_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_checklist_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_check_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_Check_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_codigo_Internalname, "TitleControlIdToReplace", Ddo_check_codigo_Titlecontrolidtoreplace);
         AV63ddo_Check_CodigoTitleControlIdToReplace = Ddo_check_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_Check_CodigoTitleControlIdToReplace", AV63ddo_Check_CodigoTitleControlIdToReplace);
         edtavDdo_check_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_check_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_check_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_checklist_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_CheckList_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_descricao_Internalname, "TitleControlIdToReplace", Ddo_checklist_descricao_Titlecontrolidtoreplace);
         AV67ddo_CheckList_DescricaoTitleControlIdToReplace = Ddo_checklist_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_CheckList_DescricaoTitleControlIdToReplace", AV67ddo_CheckList_DescricaoTitleControlIdToReplace);
         edtavDdo_checklist_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_checklist_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_checklist_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_checklist_de_Titlecontrolidtoreplace = subGrid_Internalname+"_CheckList_De";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_de_Internalname, "TitleControlIdToReplace", Ddo_checklist_de_Titlecontrolidtoreplace);
         AV71ddo_CheckList_DeTitleControlIdToReplace = Ddo_checklist_de_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ddo_CheckList_DeTitleControlIdToReplace", AV71ddo_CheckList_DeTitleControlIdToReplace);
         edtavDdo_checklist_detitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_checklist_detitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_checklist_detitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_checklist_obrigatorio_Titlecontrolidtoreplace = subGrid_Internalname+"_CheckList_Obrigatorio";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_obrigatorio_Internalname, "TitleControlIdToReplace", Ddo_checklist_obrigatorio_Titlecontrolidtoreplace);
         AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace = Ddo_checklist_obrigatorio_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace", AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace);
         edtavDdo_checklist_obrigatoriotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_checklist_obrigatoriotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_checklist_obrigatoriotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_checklist_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_CheckList_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_ativo_Internalname, "TitleControlIdToReplace", Ddo_checklist_ativo_Titlecontrolidtoreplace);
         AV77ddo_CheckList_AtivoTitleControlIdToReplace = Ddo_checklist_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77ddo_CheckList_AtivoTitleControlIdToReplace", AV77ddo_CheckList_AtivoTitleControlIdToReplace);
         edtavDdo_checklist_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_checklist_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_checklist_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Check List";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "List de", 0);
         cmbavOrderedby.addItem("2", "List Item", 0);
         cmbavOrderedby.addItem("3", "Check List", 0);
         cmbavOrderedby.addItem("4", "Descri��o", 0);
         cmbavOrderedby.addItem("5", "Obrigat�rio", 0);
         cmbavOrderedby.addItem("6", "Ativo?", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         Ddo_managefilters_Icon = context.convertURL( (String)(context.GetImagePath( "5efb9e2b-46db-43f4-8f74-dd3f5818d30e", "", context.GetTheme( ))));
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "Icon", Ddo_managefilters_Icon);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV78DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV78DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E29O22( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV56CheckList_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV60Check_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV64CheckList_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV68CheckList_DeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV72CheckList_ObrigatorioTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV75CheckList_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         if ( AV50ManageFiltersExecutionStep == 1 )
         {
            AV50ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV50ManageFiltersExecutionStep), 1, 0));
         }
         else if ( AV50ManageFiltersExecutionStep == 2 )
         {
            AV50ManageFiltersExecutionStep = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV50ManageFiltersExecutionStep), 1, 0));
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtCheckList_Codigo_Titleformat = 2;
         edtCheckList_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "List Item", AV59ddo_CheckList_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCheckList_Codigo_Internalname, "Title", edtCheckList_Codigo_Title);
         edtCheck_Codigo_Titleformat = 2;
         edtCheck_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Check List", AV63ddo_Check_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCheck_Codigo_Internalname, "Title", edtCheck_Codigo_Title);
         edtCheckList_Descricao_Titleformat = 2;
         edtCheckList_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV67ddo_CheckList_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCheckList_Descricao_Internalname, "Title", edtCheckList_Descricao_Title);
         cmbCheckList_De_Titleformat = 2;
         cmbCheckList_De.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "List de", AV71ddo_CheckList_DeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheckList_De_Internalname, "Title", cmbCheckList_De.Title.Text);
         cmbCheckList_Obrigatorio_Titleformat = 2;
         cmbCheckList_Obrigatorio.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Obrigat�rio", AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheckList_Obrigatorio_Internalname, "Title", cmbCheckList_Obrigatorio.Title.Text);
         chkCheckList_Ativo_Titleformat = 2;
         chkCheckList_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo?", AV77ddo_CheckList_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkCheckList_Ativo_Internalname, "Title", chkCheckList_Ativo.Title.Text);
         AV80GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV80GridCurrentPage), 10, 0)));
         AV81GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81GridPageCount), 10, 0)));
         AV84WWCheckListDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV85WWCheckListDS_2_Checklist_de1 = AV17CheckList_De1;
         AV86WWCheckListDS_3_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV87WWCheckListDS_4_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV88WWCheckListDS_5_Checklist_de2 = AV20CheckList_De2;
         AV89WWCheckListDS_6_Dynamicfiltersenabled3 = AV21DynamicFiltersEnabled3;
         AV90WWCheckListDS_7_Dynamicfiltersselector3 = AV22DynamicFiltersSelector3;
         AV91WWCheckListDS_8_Checklist_de3 = AV23CheckList_De3;
         AV92WWCheckListDS_9_Tfchecklist_codigo = AV57TFCheckList_Codigo;
         AV93WWCheckListDS_10_Tfchecklist_codigo_to = AV58TFCheckList_Codigo_To;
         AV94WWCheckListDS_11_Tfcheck_codigo = AV61TFCheck_Codigo;
         AV95WWCheckListDS_12_Tfcheck_codigo_to = AV62TFCheck_Codigo_To;
         AV96WWCheckListDS_13_Tfchecklist_descricao = AV65TFCheckList_Descricao;
         AV97WWCheckListDS_14_Tfchecklist_descricao_sel = AV66TFCheckList_Descricao_Sel;
         AV98WWCheckListDS_15_Tfchecklist_de_sels = AV70TFCheckList_De_Sels;
         AV99WWCheckListDS_16_Tfchecklist_obrigatorio_sel = AV73TFCheckList_Obrigatorio_Sel;
         AV100WWCheckListDS_17_Tfchecklist_ativo_sel = AV76TFCheckList_Ativo_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV56CheckList_CodigoTitleFilterData", AV56CheckList_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV60Check_CodigoTitleFilterData", AV60Check_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV64CheckList_DescricaoTitleFilterData", AV64CheckList_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV68CheckList_DeTitleFilterData", AV68CheckList_DeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV72CheckList_ObrigatorioTitleFilterData", AV72CheckList_ObrigatorioTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV75CheckList_AtivoTitleFilterData", AV75CheckList_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV54ManageFiltersData", AV54ManageFiltersData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E12O22( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV79PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV79PageToGo) ;
         }
      }

      protected void E13O22( )
      {
         /* Ddo_checklist_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_checklist_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_checklist_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_codigo_Internalname, "SortedStatus", Ddo_checklist_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_checklist_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_checklist_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_codigo_Internalname, "SortedStatus", Ddo_checklist_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_checklist_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV57TFCheckList_Codigo = (int)(NumberUtil.Val( Ddo_checklist_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFCheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57TFCheckList_Codigo), 6, 0)));
            AV58TFCheckList_Codigo_To = (int)(NumberUtil.Val( Ddo_checklist_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFCheckList_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFCheckList_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14O22( )
      {
         /* Ddo_check_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_check_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_check_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_codigo_Internalname, "SortedStatus", Ddo_check_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_check_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_check_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_codigo_Internalname, "SortedStatus", Ddo_check_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_check_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV61TFCheck_Codigo = (int)(NumberUtil.Val( Ddo_check_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFCheck_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFCheck_Codigo), 6, 0)));
            AV62TFCheck_Codigo_To = (int)(NumberUtil.Val( Ddo_check_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFCheck_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFCheck_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15O22( )
      {
         /* Ddo_checklist_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_checklist_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_checklist_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_descricao_Internalname, "SortedStatus", Ddo_checklist_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_checklist_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_checklist_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_descricao_Internalname, "SortedStatus", Ddo_checklist_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_checklist_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV65TFCheckList_Descricao = Ddo_checklist_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFCheckList_Descricao", AV65TFCheckList_Descricao);
            AV66TFCheckList_Descricao_Sel = Ddo_checklist_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFCheckList_Descricao_Sel", AV66TFCheckList_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16O22( )
      {
         /* Ddo_checklist_de_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_checklist_de_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_checklist_de_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_de_Internalname, "SortedStatus", Ddo_checklist_de_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_checklist_de_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_checklist_de_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_de_Internalname, "SortedStatus", Ddo_checklist_de_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_checklist_de_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV69TFCheckList_De_SelsJson = Ddo_checklist_de_Selectedvalue_get;
            AV70TFCheckList_De_Sels.FromJSonString(StringUtil.StringReplace( AV69TFCheckList_De_SelsJson, "\"", ""));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV70TFCheckList_De_Sels", AV70TFCheckList_De_Sels);
      }

      protected void E17O22( )
      {
         /* Ddo_checklist_obrigatorio_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_checklist_obrigatorio_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_checklist_obrigatorio_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_obrigatorio_Internalname, "SortedStatus", Ddo_checklist_obrigatorio_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_checklist_obrigatorio_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_checklist_obrigatorio_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_obrigatorio_Internalname, "SortedStatus", Ddo_checklist_obrigatorio_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_checklist_obrigatorio_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV73TFCheckList_Obrigatorio_Sel = (short)(NumberUtil.Val( Ddo_checklist_obrigatorio_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFCheckList_Obrigatorio_Sel", StringUtil.Str( (decimal)(AV73TFCheckList_Obrigatorio_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E18O22( )
      {
         /* Ddo_checklist_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_checklist_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_checklist_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_ativo_Internalname, "SortedStatus", Ddo_checklist_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_checklist_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_checklist_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_ativo_Internalname, "SortedStatus", Ddo_checklist_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_checklist_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV76TFCheckList_Ativo_Sel = (short)(NumberUtil.Val( Ddo_checklist_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFCheckList_Ativo_Sel", StringUtil.Str( (decimal)(AV76TFCheckList_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E30O22( )
      {
         /* Grid_Load Routine */
         AV26Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV26Update);
         AV101Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("checklist.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A758CheckList_Codigo) + "," + UrlEncode("" +AV27Check_Codigo);
         AV28Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV28Delete);
         AV102Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("checklist.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A758CheckList_Codigo) + "," + UrlEncode("" +AV27Check_Codigo);
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 71;
         }
         sendrow_712( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_71_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(71, GridRow);
         }
      }

      protected void E19O22( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E23O22( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E20O22( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV24DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersRemoving", AV24DynamicFiltersRemoving);
         AV25DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersIgnoreFirst", AV25DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersRemoving", AV24DynamicFiltersRemoving);
         AV25DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersIgnoreFirst", AV25DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17CheckList_De1, AV19DynamicFiltersSelector2, AV20CheckList_De2, AV22DynamicFiltersSelector3, AV23CheckList_De3, AV18DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV57TFCheckList_Codigo, AV58TFCheckList_Codigo_To, AV61TFCheck_Codigo, AV62TFCheck_Codigo_To, AV65TFCheckList_Descricao, AV66TFCheckList_Descricao_Sel, AV73TFCheckList_Obrigatorio_Sel, AV76TFCheckList_Ativo_Sel, AV50ManageFiltersExecutionStep, AV59ddo_CheckList_CodigoTitleControlIdToReplace, AV63ddo_Check_CodigoTitleControlIdToReplace, AV67ddo_CheckList_DescricaoTitleControlIdToReplace, AV71ddo_CheckList_DeTitleControlIdToReplace, AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace, AV77ddo_CheckList_AtivoTitleControlIdToReplace, AV70TFCheckList_De_Sels, AV103Pgmname, AV10GridState, AV25DynamicFiltersIgnoreFirst, AV24DynamicFiltersRemoving, A758CheckList_Codigo, AV27Check_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavChecklist_de2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20CheckList_De2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavChecklist_de2_Internalname, "Values", cmbavChecklist_de2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavChecklist_de3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23CheckList_De3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavChecklist_de3_Internalname, "Values", cmbavChecklist_de3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavChecklist_de1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17CheckList_De1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavChecklist_de1_Internalname, "Values", cmbavChecklist_de1.ToJavascriptSource());
      }

      protected void E24O22( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25O22( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV21DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersEnabled3", AV21DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E21O22( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV24DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersRemoving", AV24DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersRemoving", AV24DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17CheckList_De1, AV19DynamicFiltersSelector2, AV20CheckList_De2, AV22DynamicFiltersSelector3, AV23CheckList_De3, AV18DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV57TFCheckList_Codigo, AV58TFCheckList_Codigo_To, AV61TFCheck_Codigo, AV62TFCheck_Codigo_To, AV65TFCheckList_Descricao, AV66TFCheckList_Descricao_Sel, AV73TFCheckList_Obrigatorio_Sel, AV76TFCheckList_Ativo_Sel, AV50ManageFiltersExecutionStep, AV59ddo_CheckList_CodigoTitleControlIdToReplace, AV63ddo_Check_CodigoTitleControlIdToReplace, AV67ddo_CheckList_DescricaoTitleControlIdToReplace, AV71ddo_CheckList_DeTitleControlIdToReplace, AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace, AV77ddo_CheckList_AtivoTitleControlIdToReplace, AV70TFCheckList_De_Sels, AV103Pgmname, AV10GridState, AV25DynamicFiltersIgnoreFirst, AV24DynamicFiltersRemoving, A758CheckList_Codigo, AV27Check_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavChecklist_de2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20CheckList_De2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavChecklist_de2_Internalname, "Values", cmbavChecklist_de2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavChecklist_de3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23CheckList_De3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavChecklist_de3_Internalname, "Values", cmbavChecklist_de3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavChecklist_de1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17CheckList_De1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavChecklist_de1_Internalname, "Values", cmbavChecklist_de1.ToJavascriptSource());
      }

      protected void E26O22( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22O22( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV24DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersRemoving", AV24DynamicFiltersRemoving);
         AV21DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersEnabled3", AV21DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersRemoving", AV24DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV17CheckList_De1, AV19DynamicFiltersSelector2, AV20CheckList_De2, AV22DynamicFiltersSelector3, AV23CheckList_De3, AV18DynamicFiltersEnabled2, AV21DynamicFiltersEnabled3, AV57TFCheckList_Codigo, AV58TFCheckList_Codigo_To, AV61TFCheck_Codigo, AV62TFCheck_Codigo_To, AV65TFCheckList_Descricao, AV66TFCheckList_Descricao_Sel, AV73TFCheckList_Obrigatorio_Sel, AV76TFCheckList_Ativo_Sel, AV50ManageFiltersExecutionStep, AV59ddo_CheckList_CodigoTitleControlIdToReplace, AV63ddo_Check_CodigoTitleControlIdToReplace, AV67ddo_CheckList_DescricaoTitleControlIdToReplace, AV71ddo_CheckList_DeTitleControlIdToReplace, AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace, AV77ddo_CheckList_AtivoTitleControlIdToReplace, AV70TFCheckList_De_Sels, AV103Pgmname, AV10GridState, AV25DynamicFiltersIgnoreFirst, AV24DynamicFiltersRemoving, A758CheckList_Codigo, AV27Check_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavChecklist_de2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20CheckList_De2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavChecklist_de2_Internalname, "Values", cmbavChecklist_de2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavChecklist_de3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23CheckList_De3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavChecklist_de3_Internalname, "Values", cmbavChecklist_de3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavChecklist_de1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17CheckList_De1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavChecklist_de1_Internalname, "Values", cmbavChecklist_de1.ToJavascriptSource());
      }

      protected void E27O22( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11O22( )
      {
         /* Ddo_managefilters_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Clean#>") == 0 )
         {
            /* Execute user subroutine: 'CLEANFILTERS' */
            S232 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Save#>") == 0 )
         {
            /* Execute user subroutine: 'SAVEGRIDSTATE' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            context.PopUp(formatLink("wwpbaseobjects.savefilteras.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWCheckListFilters")) + "," + UrlEncode(StringUtil.RTrim(AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3"))), new Object[] {});
            AV50ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV50ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Manage#>") == 0 )
         {
            context.PopUp(formatLink("wwpbaseobjects.managefilters.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWCheckListFilters")), new Object[] {});
            AV50ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV50ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else
         {
            GXt_char2 = AV51ManageFiltersXml;
            new wwpbaseobjects.getfilterbyname(context ).execute(  "WWCheckListFilters",  Ddo_managefilters_Activeeventkey, out  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "ActiveEventKey", Ddo_managefilters_Activeeventkey);
            AV51ManageFiltersXml = GXt_char2;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV51ManageFiltersXml)) )
            {
               GX_msglist.addItem("O filtro selecionado n�o existe mais.");
            }
            else
            {
               /* Execute user subroutine: 'CLEANFILTERS' */
               S232 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               new wwpbaseobjects.savegridstate(context ).execute(  AV103Pgmname+"GridState",  AV51ManageFiltersXml) ;
               AV10GridState.FromXml(AV51ManageFiltersXml, "");
               AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
               S172 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
               S242 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
               S222 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               subgrid_firstpage( ) ;
               context.DoAjaxRefresh();
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV70TFCheckList_De_Sels", AV70TFCheckList_De_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavChecklist_de1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17CheckList_De1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavChecklist_de1_Internalname, "Values", cmbavChecklist_de1.ToJavascriptSource());
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavChecklist_de2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20CheckList_De2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavChecklist_de2_Internalname, "Values", cmbavChecklist_de2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavChecklist_de3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23CheckList_De3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavChecklist_de3_Internalname, "Values", cmbavChecklist_de3.ToJavascriptSource());
      }

      protected void S192( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_checklist_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_codigo_Internalname, "SortedStatus", Ddo_checklist_codigo_Sortedstatus);
         Ddo_check_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_codigo_Internalname, "SortedStatus", Ddo_check_codigo_Sortedstatus);
         Ddo_checklist_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_descricao_Internalname, "SortedStatus", Ddo_checklist_descricao_Sortedstatus);
         Ddo_checklist_de_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_de_Internalname, "SortedStatus", Ddo_checklist_de_Sortedstatus);
         Ddo_checklist_obrigatorio_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_obrigatorio_Internalname, "SortedStatus", Ddo_checklist_obrigatorio_Sortedstatus);
         Ddo_checklist_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_ativo_Internalname, "SortedStatus", Ddo_checklist_ativo_Sortedstatus);
      }

      protected void S172( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_checklist_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_codigo_Internalname, "SortedStatus", Ddo_checklist_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_check_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_codigo_Internalname, "SortedStatus", Ddo_check_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_checklist_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_descricao_Internalname, "SortedStatus", Ddo_checklist_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_checklist_de_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_de_Internalname, "SortedStatus", Ddo_checklist_de_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_checklist_obrigatorio_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_obrigatorio_Internalname, "SortedStatus", Ddo_checklist_obrigatorio_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_checklist_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_ativo_Internalname, "SortedStatus", Ddo_checklist_ativo_Sortedstatus);
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         cmbavChecklist_de1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavChecklist_de1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavChecklist_de1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CHECKLIST_DE") == 0 )
         {
            cmbavChecklist_de1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavChecklist_de1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavChecklist_de1.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         cmbavChecklist_de2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavChecklist_de2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavChecklist_de2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CHECKLIST_DE") == 0 )
         {
            cmbavChecklist_de2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavChecklist_de2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavChecklist_de2.Visible), 5, 0)));
         }
      }

      protected void S142( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         cmbavChecklist_de3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavChecklist_de3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavChecklist_de3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV22DynamicFiltersSelector3, "CHECKLIST_DE") == 0 )
         {
            cmbavChecklist_de3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavChecklist_de3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavChecklist_de3.Visible), 5, 0)));
         }
      }

      protected void S212( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "CHECKLIST_DE";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20CheckList_De2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20CheckList_De2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20CheckList_De2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersEnabled3", AV21DynamicFiltersEnabled3);
         AV22DynamicFiltersSelector3 = "CHECKLIST_DE";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersSelector3", AV22DynamicFiltersSelector3);
         AV23CheckList_De3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23CheckList_De3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23CheckList_De3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S112( )
      {
         /* 'LOADSAVEDFILTERS' Routine */
         AV54ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV55ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV55ManageFiltersDataItem.gxTpr_Title = "Limpar filtros";
         AV55ManageFiltersDataItem.gxTpr_Eventkey = "<#Clean#>";
         AV55ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV55ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "63d2ae92-4e43-4a70-af61-0943e39ea422", "", context.GetTheme( ))));
         AV55ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
         AV54ManageFiltersData.Add(AV55ManageFiltersDataItem, 0);
         AV55ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV55ManageFiltersDataItem.gxTpr_Title = "Salvar filtro como...";
         AV55ManageFiltersDataItem.gxTpr_Eventkey = "<#Save#>";
         AV55ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV55ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "6eee63e8-73c7-4738-beee-f98e3a8d2841", "", context.GetTheme( ))));
         AV54ManageFiltersData.Add(AV55ManageFiltersDataItem, 0);
         AV55ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV55ManageFiltersDataItem.gxTpr_Isdivider = true;
         AV54ManageFiltersData.Add(AV55ManageFiltersDataItem, 0);
         AV52ManageFiltersItems.FromXml(new wwpbaseobjects.loadmanagefiltersstate(context).executeUdp(  "WWCheckListFilters"), "");
         AV104GXV1 = 1;
         while ( AV104GXV1 <= AV52ManageFiltersItems.Count )
         {
            AV53ManageFiltersItem = ((wwpbaseobjects.SdtGridStateCollection_Item)AV52ManageFiltersItems.Item(AV104GXV1));
            AV55ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV55ManageFiltersDataItem.gxTpr_Title = AV53ManageFiltersItem.gxTpr_Title;
            AV55ManageFiltersDataItem.gxTpr_Eventkey = AV53ManageFiltersItem.gxTpr_Title;
            AV55ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV55ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
            AV54ManageFiltersData.Add(AV55ManageFiltersDataItem, 0);
            if ( AV54ManageFiltersData.Count == 13 )
            {
               if (true) break;
            }
            AV104GXV1 = (int)(AV104GXV1+1);
         }
         if ( AV54ManageFiltersData.Count > 3 )
         {
            AV55ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV55ManageFiltersDataItem.gxTpr_Isdivider = true;
            AV54ManageFiltersData.Add(AV55ManageFiltersDataItem, 0);
            AV55ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV55ManageFiltersDataItem.gxTpr_Title = "Gerenciar filtros";
            AV55ManageFiltersDataItem.gxTpr_Eventkey = "<#Manage#>";
            AV55ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV55ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "653f6166-5d82-407a-af84-19e0dde65efd", "", context.GetTheme( ))));
            AV55ManageFiltersDataItem.gxTpr_Jsonclickevent = "";
            AV54ManageFiltersData.Add(AV55ManageFiltersDataItem, 0);
         }
      }

      protected void S232( )
      {
         /* 'CLEANFILTERS' Routine */
         AV57TFCheckList_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFCheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57TFCheckList_Codigo), 6, 0)));
         Ddo_checklist_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_codigo_Internalname, "FilteredText_set", Ddo_checklist_codigo_Filteredtext_set);
         AV58TFCheckList_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFCheckList_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFCheckList_Codigo_To), 6, 0)));
         Ddo_checklist_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_codigo_Internalname, "FilteredTextTo_set", Ddo_checklist_codigo_Filteredtextto_set);
         AV61TFCheck_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFCheck_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFCheck_Codigo), 6, 0)));
         Ddo_check_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_codigo_Internalname, "FilteredText_set", Ddo_check_codigo_Filteredtext_set);
         AV62TFCheck_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFCheck_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFCheck_Codigo_To), 6, 0)));
         Ddo_check_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_codigo_Internalname, "FilteredTextTo_set", Ddo_check_codigo_Filteredtextto_set);
         AV65TFCheckList_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFCheckList_Descricao", AV65TFCheckList_Descricao);
         Ddo_checklist_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_descricao_Internalname, "FilteredText_set", Ddo_checklist_descricao_Filteredtext_set);
         AV66TFCheckList_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFCheckList_Descricao_Sel", AV66TFCheckList_Descricao_Sel);
         Ddo_checklist_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_descricao_Internalname, "SelectedValue_set", Ddo_checklist_descricao_Selectedvalue_set);
         AV70TFCheckList_De_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_checklist_de_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_de_Internalname, "SelectedValue_set", Ddo_checklist_de_Selectedvalue_set);
         AV73TFCheckList_Obrigatorio_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFCheckList_Obrigatorio_Sel", StringUtil.Str( (decimal)(AV73TFCheckList_Obrigatorio_Sel), 1, 0));
         Ddo_checklist_obrigatorio_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_obrigatorio_Internalname, "SelectedValue_set", Ddo_checklist_obrigatorio_Selectedvalue_set);
         AV76TFCheckList_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFCheckList_Ativo_Sel", StringUtil.Str( (decimal)(AV76TFCheckList_Ativo_Sel), 1, 0));
         Ddo_checklist_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_ativo_Internalname, "SelectedValue_set", Ddo_checklist_ativo_Selectedvalue_set);
         AV16DynamicFiltersSelector1 = "CHECKLIST_DE";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV17CheckList_De1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17CheckList_De1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17CheckList_De1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S162( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV49Session.Get(AV103Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV103Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV49Session.Get(AV103Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S242 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S242( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV105GXV2 = 1;
         while ( AV105GXV2 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV105GXV2));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCHECKLIST_CODIGO") == 0 )
            {
               AV57TFCheckList_Codigo = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFCheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57TFCheckList_Codigo), 6, 0)));
               AV58TFCheckList_Codigo_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFCheckList_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58TFCheckList_Codigo_To), 6, 0)));
               if ( ! (0==AV57TFCheckList_Codigo) )
               {
                  Ddo_checklist_codigo_Filteredtext_set = StringUtil.Str( (decimal)(AV57TFCheckList_Codigo), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_codigo_Internalname, "FilteredText_set", Ddo_checklist_codigo_Filteredtext_set);
               }
               if ( ! (0==AV58TFCheckList_Codigo_To) )
               {
                  Ddo_checklist_codigo_Filteredtextto_set = StringUtil.Str( (decimal)(AV58TFCheckList_Codigo_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_codigo_Internalname, "FilteredTextTo_set", Ddo_checklist_codigo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCHECK_CODIGO") == 0 )
            {
               AV61TFCheck_Codigo = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFCheck_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFCheck_Codigo), 6, 0)));
               AV62TFCheck_Codigo_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFCheck_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62TFCheck_Codigo_To), 6, 0)));
               if ( ! (0==AV61TFCheck_Codigo) )
               {
                  Ddo_check_codigo_Filteredtext_set = StringUtil.Str( (decimal)(AV61TFCheck_Codigo), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_codigo_Internalname, "FilteredText_set", Ddo_check_codigo_Filteredtext_set);
               }
               if ( ! (0==AV62TFCheck_Codigo_To) )
               {
                  Ddo_check_codigo_Filteredtextto_set = StringUtil.Str( (decimal)(AV62TFCheck_Codigo_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_check_codigo_Internalname, "FilteredTextTo_set", Ddo_check_codigo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCHECKLIST_DESCRICAO") == 0 )
            {
               AV65TFCheckList_Descricao = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFCheckList_Descricao", AV65TFCheckList_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65TFCheckList_Descricao)) )
               {
                  Ddo_checklist_descricao_Filteredtext_set = AV65TFCheckList_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_descricao_Internalname, "FilteredText_set", Ddo_checklist_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCHECKLIST_DESCRICAO_SEL") == 0 )
            {
               AV66TFCheckList_Descricao_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFCheckList_Descricao_Sel", AV66TFCheckList_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFCheckList_Descricao_Sel)) )
               {
                  Ddo_checklist_descricao_Selectedvalue_set = AV66TFCheckList_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_descricao_Internalname, "SelectedValue_set", Ddo_checklist_descricao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCHECKLIST_DE_SEL") == 0 )
            {
               AV69TFCheckList_De_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV70TFCheckList_De_Sels.FromJSonString(AV69TFCheckList_De_SelsJson);
               if ( ! ( AV70TFCheckList_De_Sels.Count == 0 ) )
               {
                  Ddo_checklist_de_Selectedvalue_set = AV69TFCheckList_De_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_de_Internalname, "SelectedValue_set", Ddo_checklist_de_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCHECKLIST_OBRIGATORIO_SEL") == 0 )
            {
               AV73TFCheckList_Obrigatorio_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFCheckList_Obrigatorio_Sel", StringUtil.Str( (decimal)(AV73TFCheckList_Obrigatorio_Sel), 1, 0));
               if ( ! (0==AV73TFCheckList_Obrigatorio_Sel) )
               {
                  Ddo_checklist_obrigatorio_Selectedvalue_set = StringUtil.Str( (decimal)(AV73TFCheckList_Obrigatorio_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_obrigatorio_Internalname, "SelectedValue_set", Ddo_checklist_obrigatorio_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCHECKLIST_ATIVO_SEL") == 0 )
            {
               AV76TFCheckList_Ativo_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76TFCheckList_Ativo_Sel", StringUtil.Str( (decimal)(AV76TFCheckList_Ativo_Sel), 1, 0));
               if ( ! (0==AV76TFCheckList_Ativo_Sel) )
               {
                  Ddo_checklist_ativo_Selectedvalue_set = StringUtil.Str( (decimal)(AV76TFCheckList_Ativo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_checklist_ativo_Internalname, "SelectedValue_set", Ddo_checklist_ativo_Selectedvalue_set);
               }
            }
            AV105GXV2 = (int)(AV105GXV2+1);
         }
      }

      protected void S222( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CHECKLIST_DE") == 0 )
            {
               AV17CheckList_De1 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17CheckList_De1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17CheckList_De1), 4, 0)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CHECKLIST_DE") == 0 )
               {
                  AV20CheckList_De2 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20CheckList_De2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20CheckList_De2), 4, 0)));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S132 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV21DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersEnabled3", AV21DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV22DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersSelector3", AV22DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV22DynamicFiltersSelector3, "CHECKLIST_DE") == 0 )
                  {
                     AV23CheckList_De3 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23CheckList_De3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23CheckList_De3), 4, 0)));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S142 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV24DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S182( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV49Session.Get(AV103Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV57TFCheckList_Codigo) && (0==AV58TFCheckList_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCHECKLIST_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV57TFCheckList_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV58TFCheckList_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV61TFCheck_Codigo) && (0==AV62TFCheck_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCHECK_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV61TFCheck_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV62TFCheck_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65TFCheckList_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCHECKLIST_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV65TFCheckList_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFCheckList_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCHECKLIST_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV66TFCheckList_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV70TFCheckList_De_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCHECKLIST_DE_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV70TFCheckList_De_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV73TFCheckList_Obrigatorio_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCHECKLIST_OBRIGATORIO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV73TFCheckList_Obrigatorio_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV76TFCheckList_Ativo_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCHECKLIST_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV76TFCheckList_Ativo_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV103Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S202( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV25DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CHECKLIST_DE") == 0 ) && ! (0==AV17CheckList_De1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV17CheckList_De1), 4, 0);
            }
            if ( AV24DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CHECKLIST_DE") == 0 ) && ! (0==AV20CheckList_De2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV20CheckList_De2), 4, 0);
            }
            if ( AV24DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV21DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV22DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV22DynamicFiltersSelector3, "CHECKLIST_DE") == 0 ) && ! (0==AV23CheckList_De3) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV23CheckList_De3), 4, 0);
            }
            if ( AV24DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S152( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV103Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "CheckList";
         AV49Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_O22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_O22( true) ;
         }
         else
         {
            wb_table2_8_O22( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_O22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_65_O22( true) ;
         }
         else
         {
            wb_table3_65_O22( false) ;
         }
         return  ;
      }

      protected void wb_table3_65_O22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_O22e( true) ;
         }
         else
         {
            wb_table1_2_O22e( false) ;
         }
      }

      protected void wb_table3_65_O22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_68_O22( true) ;
         }
         else
         {
            wb_table4_68_O22( false) ;
         }
         return  ;
      }

      protected void wb_table4_68_O22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_65_O22e( true) ;
         }
         else
         {
            wb_table3_65_O22e( false) ;
         }
      }

      protected void wb_table4_68_O22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"71\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtCheckList_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtCheckList_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtCheckList_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtCheck_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtCheck_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtCheck_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtCheckList_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtCheckList_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtCheckList_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbCheckList_De_Titleformat == 0 )
               {
                  context.SendWebValue( cmbCheckList_De.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbCheckList_De.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbCheckList_Obrigatorio_Titleformat == 0 )
               {
                  context.SendWebValue( cmbCheckList_Obrigatorio.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbCheckList_Obrigatorio.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkCheckList_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkCheckList_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkCheckList_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV26Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A758CheckList_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtCheckList_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCheckList_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1839Check_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtCheck_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCheck_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A763CheckList_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtCheckList_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCheckList_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1230CheckList_De), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbCheckList_De.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbCheckList_De_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A1845CheckList_Obrigatorio));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbCheckList_Obrigatorio.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbCheckList_Obrigatorio_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A1151CheckList_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkCheckList_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkCheckList_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 71 )
         {
            wbEnd = 0;
            nRC_GXsfl_71 = (short)(nGXsfl_71_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_68_O22e( true) ;
         }
         else
         {
            wb_table4_68_O22e( false) ;
         }
      }

      protected void wb_table2_8_O22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_11_O22( true) ;
         }
         else
         {
            wb_table5_11_O22( false) ;
         }
         return  ;
      }

      protected void wb_table5_11_O22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_21_O22( true) ;
         }
         else
         {
            wb_table6_21_O22( false) ;
         }
         return  ;
      }

      protected void wb_table6_21_O22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_O22e( true) ;
         }
         else
         {
            wb_table2_8_O22e( false) ;
         }
      }

      protected void wb_table6_21_O22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MANAGEFILTERSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_26_O22( true) ;
         }
         else
         {
            wb_table7_26_O22( false) ;
         }
         return  ;
      }

      protected void wb_table7_26_O22e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWCheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_21_O22e( true) ;
         }
         else
         {
            wb_table6_21_O22e( false) ;
         }
      }

      protected void wb_table7_26_O22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWCheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_71_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_WWCheckList.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWCheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'" + sGXsfl_71_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavChecklist_de1, cmbavChecklist_de1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17CheckList_De1), 4, 0)), 1, cmbavChecklist_de1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavChecklist_de1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,35);\"", "", true, "HLP_WWCheckList.htm");
            cmbavChecklist_de1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17CheckList_De1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavChecklist_de1_Internalname, "Values", (String)(cmbavChecklist_de1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWCheckList.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWCheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWCheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_71_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", "", true, "HLP_WWCheckList.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWCheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'" + sGXsfl_71_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavChecklist_de2, cmbavChecklist_de2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20CheckList_De2), 4, 0)), 1, cmbavChecklist_de2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavChecklist_de2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", "", true, "HLP_WWCheckList.htm");
            cmbavChecklist_de2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20CheckList_De2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavChecklist_de2_Internalname, "Values", (String)(cmbavChecklist_de2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWCheckList.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWCheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWCheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'" + sGXsfl_71_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV22DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", "", true, "HLP_WWCheckList.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWCheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_71_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavChecklist_de3, cmbavChecklist_de3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV23CheckList_De3), 4, 0)), 1, cmbavChecklist_de3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavChecklist_de3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "", true, "HLP_WWCheckList.htm");
            cmbavChecklist_de3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23CheckList_De3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavChecklist_de3_Internalname, "Values", (String)(cmbavChecklist_de3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWCheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_26_O22e( true) ;
         }
         else
         {
            wb_table7_26_O22e( false) ;
         }
      }

      protected void wb_table5_11_O22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblChecklisttitle_Internalname, "Check List", "", "", lblChecklisttitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWCheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWCheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'" + sGXsfl_71_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_WWCheckList.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'" + sGXsfl_71_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWCheckList.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_11_O22e( true) ;
         }
         else
         {
            wb_table5_11_O22e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAO22( ) ;
         WSO22( ) ;
         WEO22( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202051813365990");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwchecklist.js", "?202051813365991");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_712( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_71_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_71_idx;
         edtCheckList_Codigo_Internalname = "CHECKLIST_CODIGO_"+sGXsfl_71_idx;
         edtCheck_Codigo_Internalname = "CHECK_CODIGO_"+sGXsfl_71_idx;
         edtCheckList_Descricao_Internalname = "CHECKLIST_DESCRICAO_"+sGXsfl_71_idx;
         cmbCheckList_De_Internalname = "CHECKLIST_DE_"+sGXsfl_71_idx;
         cmbCheckList_Obrigatorio_Internalname = "CHECKLIST_OBRIGATORIO_"+sGXsfl_71_idx;
         chkCheckList_Ativo_Internalname = "CHECKLIST_ATIVO_"+sGXsfl_71_idx;
      }

      protected void SubsflControlProps_fel_712( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_71_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_71_fel_idx;
         edtCheckList_Codigo_Internalname = "CHECKLIST_CODIGO_"+sGXsfl_71_fel_idx;
         edtCheck_Codigo_Internalname = "CHECK_CODIGO_"+sGXsfl_71_fel_idx;
         edtCheckList_Descricao_Internalname = "CHECKLIST_DESCRICAO_"+sGXsfl_71_fel_idx;
         cmbCheckList_De_Internalname = "CHECKLIST_DE_"+sGXsfl_71_fel_idx;
         cmbCheckList_Obrigatorio_Internalname = "CHECKLIST_OBRIGATORIO_"+sGXsfl_71_fel_idx;
         chkCheckList_Ativo_Internalname = "CHECKLIST_ATIVO_"+sGXsfl_71_fel_idx;
      }

      protected void sendrow_712( )
      {
         SubsflControlProps_712( ) ;
         WBO20( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_71_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_71_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_71_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV26Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV26Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV101Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV26Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV26Update)) ? AV101Update_GXI : context.PathToRelativeUrl( AV26Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV26Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV102Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Delete)) ? AV102Delete_GXI : context.PathToRelativeUrl( AV28Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCheckList_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A758CheckList_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A758CheckList_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtCheckList_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)71,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCheck_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1839Check_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1839Check_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtCheck_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)71,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCheckList_Descricao_Internalname,(String)A763CheckList_Descricao,(String)A763CheckList_Descricao,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtCheckList_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)71,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_71_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CHECKLIST_DE_" + sGXsfl_71_idx;
               cmbCheckList_De.Name = GXCCtl;
               cmbCheckList_De.WebTags = "";
               cmbCheckList_De.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Nenhum)", 0);
               cmbCheckList_De.addItem("1", "Libera valida��o", 0);
               cmbCheckList_De.addItem("2", "Em an�lise", 0);
               if ( cmbCheckList_De.ItemCount > 0 )
               {
                  A1230CheckList_De = (short)(NumberUtil.Val( cmbCheckList_De.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1230CheckList_De), 4, 0))), "."));
                  n1230CheckList_De = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbCheckList_De,(String)cmbCheckList_De_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A1230CheckList_De), 4, 0)),(short)1,(String)cmbCheckList_De_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbCheckList_De.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1230CheckList_De), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheckList_De_Internalname, "Values", (String)(cmbCheckList_De.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_71_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CHECKLIST_OBRIGATORIO_" + sGXsfl_71_idx;
               cmbCheckList_Obrigatorio.Name = GXCCtl;
               cmbCheckList_Obrigatorio.WebTags = "";
               cmbCheckList_Obrigatorio.addItem(StringUtil.BoolToStr( false), "N�o", 0);
               cmbCheckList_Obrigatorio.addItem(StringUtil.BoolToStr( true), "Sim", 0);
               if ( cmbCheckList_Obrigatorio.ItemCount > 0 )
               {
                  A1845CheckList_Obrigatorio = StringUtil.StrToBool( cmbCheckList_Obrigatorio.getValidValue(StringUtil.BoolToStr( A1845CheckList_Obrigatorio)));
                  n1845CheckList_Obrigatorio = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbCheckList_Obrigatorio,(String)cmbCheckList_Obrigatorio_Internalname,StringUtil.BoolToStr( A1845CheckList_Obrigatorio),(short)1,(String)cmbCheckList_Obrigatorio_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"boolean",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbCheckList_Obrigatorio.CurrentValue = StringUtil.BoolToStr( A1845CheckList_Obrigatorio);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheckList_Obrigatorio_Internalname, "Values", (String)(cmbCheckList_Obrigatorio.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkCheckList_Ativo_Internalname,StringUtil.BoolToStr( A1151CheckList_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, "gxhash_CHECKLIST_CODIGO"+"_"+sGXsfl_71_idx, GetSecureSignedToken( sGXsfl_71_idx, context.localUtil.Format( (decimal)(A758CheckList_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CHECK_CODIGO"+"_"+sGXsfl_71_idx, GetSecureSignedToken( sGXsfl_71_idx, context.localUtil.Format( (decimal)(A1839Check_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CHECKLIST_DESCRICAO"+"_"+sGXsfl_71_idx, GetSecureSignedToken( sGXsfl_71_idx, A763CheckList_Descricao));
            GxWebStd.gx_hidden_field( context, "gxhash_CHECKLIST_DE"+"_"+sGXsfl_71_idx, GetSecureSignedToken( sGXsfl_71_idx, context.localUtil.Format( (decimal)(A1230CheckList_De), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CHECKLIST_OBRIGATORIO"+"_"+sGXsfl_71_idx, GetSecureSignedToken( sGXsfl_71_idx, A1845CheckList_Obrigatorio));
            GxWebStd.gx_hidden_field( context, "gxhash_CHECKLIST_ATIVO"+"_"+sGXsfl_71_idx, GetSecureSignedToken( sGXsfl_71_idx, A1151CheckList_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_71_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_71_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_71_idx+1));
            sGXsfl_71_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_71_idx), 4, 0)), 4, "0");
            SubsflControlProps_712( ) ;
         }
         /* End function sendrow_712 */
      }

      protected void init_default_properties( )
      {
         lblChecklisttitle_Internalname = "CHECKLISTTITLE";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         tblTableactions_Internalname = "TABLEACTIONS";
         Ddo_managefilters_Internalname = "DDO_MANAGEFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavChecklist_de1_Internalname = "vCHECKLIST_DE1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavChecklist_de2_Internalname = "vCHECKLIST_DE2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavChecklist_de3_Internalname = "vCHECKLIST_DE3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtCheckList_Codigo_Internalname = "CHECKLIST_CODIGO";
         edtCheck_Codigo_Internalname = "CHECK_CODIGO";
         edtCheckList_Descricao_Internalname = "CHECKLIST_DESCRICAO";
         cmbCheckList_De_Internalname = "CHECKLIST_DE";
         cmbCheckList_Obrigatorio_Internalname = "CHECKLIST_OBRIGATORIO";
         chkCheckList_Ativo_Internalname = "CHECKLIST_ATIVO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavManagefiltersexecutionstep_Internalname = "vMANAGEFILTERSEXECUTIONSTEP";
         edtavTfchecklist_codigo_Internalname = "vTFCHECKLIST_CODIGO";
         edtavTfchecklist_codigo_to_Internalname = "vTFCHECKLIST_CODIGO_TO";
         edtavTfcheck_codigo_Internalname = "vTFCHECK_CODIGO";
         edtavTfcheck_codigo_to_Internalname = "vTFCHECK_CODIGO_TO";
         edtavTfchecklist_descricao_Internalname = "vTFCHECKLIST_DESCRICAO";
         edtavTfchecklist_descricao_sel_Internalname = "vTFCHECKLIST_DESCRICAO_SEL";
         edtavTfchecklist_obrigatorio_sel_Internalname = "vTFCHECKLIST_OBRIGATORIO_SEL";
         edtavTfchecklist_ativo_sel_Internalname = "vTFCHECKLIST_ATIVO_SEL";
         Ddo_checklist_codigo_Internalname = "DDO_CHECKLIST_CODIGO";
         edtavDdo_checklist_codigotitlecontrolidtoreplace_Internalname = "vDDO_CHECKLIST_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_check_codigo_Internalname = "DDO_CHECK_CODIGO";
         edtavDdo_check_codigotitlecontrolidtoreplace_Internalname = "vDDO_CHECK_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_checklist_descricao_Internalname = "DDO_CHECKLIST_DESCRICAO";
         edtavDdo_checklist_descricaotitlecontrolidtoreplace_Internalname = "vDDO_CHECKLIST_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_checklist_de_Internalname = "DDO_CHECKLIST_DE";
         edtavDdo_checklist_detitlecontrolidtoreplace_Internalname = "vDDO_CHECKLIST_DETITLECONTROLIDTOREPLACE";
         Ddo_checklist_obrigatorio_Internalname = "DDO_CHECKLIST_OBRIGATORIO";
         edtavDdo_checklist_obrigatoriotitlecontrolidtoreplace_Internalname = "vDDO_CHECKLIST_OBRIGATORIOTITLECONTROLIDTOREPLACE";
         Ddo_checklist_ativo_Internalname = "DDO_CHECKLIST_ATIVO";
         edtavDdo_checklist_ativotitlecontrolidtoreplace_Internalname = "vDDO_CHECKLIST_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbCheckList_Obrigatorio_Jsonclick = "";
         cmbCheckList_De_Jsonclick = "";
         edtCheckList_Descricao_Jsonclick = "";
         edtCheck_Codigo_Jsonclick = "";
         edtCheckList_Codigo_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         cmbavChecklist_de3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavChecklist_de2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavChecklist_de1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         chkCheckList_Ativo_Titleformat = 0;
         cmbCheckList_Obrigatorio_Titleformat = 0;
         cmbCheckList_De_Titleformat = 0;
         edtCheckList_Descricao_Titleformat = 0;
         edtCheck_Codigo_Titleformat = 0;
         edtCheckList_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavChecklist_de3.Visible = 1;
         cmbavChecklist_de2.Visible = 1;
         cmbavChecklist_de1.Visible = 1;
         chkCheckList_Ativo.Title.Text = "Ativo?";
         cmbCheckList_Obrigatorio.Title.Text = "Obrigat�rio";
         cmbCheckList_De.Title.Text = "List de";
         edtCheckList_Descricao_Title = "Descri��o";
         edtCheck_Codigo_Title = "Check List";
         edtCheckList_Codigo_Title = "List Item";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkCheckList_Ativo.Caption = "";
         edtavDdo_checklist_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_checklist_obrigatoriotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_checklist_detitlecontrolidtoreplace_Visible = 1;
         edtavDdo_checklist_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_check_codigotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_checklist_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfchecklist_ativo_sel_Jsonclick = "";
         edtavTfchecklist_ativo_sel_Visible = 1;
         edtavTfchecklist_obrigatorio_sel_Jsonclick = "";
         edtavTfchecklist_obrigatorio_sel_Visible = 1;
         edtavTfchecklist_descricao_sel_Visible = 1;
         edtavTfchecklist_descricao_Visible = 1;
         edtavTfcheck_codigo_to_Jsonclick = "";
         edtavTfcheck_codigo_to_Visible = 1;
         edtavTfcheck_codigo_Jsonclick = "";
         edtavTfcheck_codigo_Visible = 1;
         edtavTfchecklist_codigo_to_Jsonclick = "";
         edtavTfchecklist_codigo_to_Visible = 1;
         edtavTfchecklist_codigo_Jsonclick = "";
         edtavTfchecklist_codigo_Visible = 1;
         edtavManagefiltersexecutionstep_Jsonclick = "";
         edtavManagefiltersexecutionstep_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_checklist_ativo_Searchbuttontext = "Pesquisar";
         Ddo_checklist_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_checklist_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_checklist_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_checklist_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_checklist_ativo_Datalisttype = "FixedValues";
         Ddo_checklist_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_checklist_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_checklist_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_checklist_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_checklist_ativo_Titlecontrolidtoreplace = "";
         Ddo_checklist_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_checklist_ativo_Cls = "ColumnSettings";
         Ddo_checklist_ativo_Tooltip = "Op��es";
         Ddo_checklist_ativo_Caption = "";
         Ddo_checklist_obrigatorio_Searchbuttontext = "Pesquisar";
         Ddo_checklist_obrigatorio_Cleanfilter = "Limpar pesquisa";
         Ddo_checklist_obrigatorio_Sortdsc = "Ordenar de Z � A";
         Ddo_checklist_obrigatorio_Sortasc = "Ordenar de A � Z";
         Ddo_checklist_obrigatorio_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_checklist_obrigatorio_Datalisttype = "FixedValues";
         Ddo_checklist_obrigatorio_Includedatalist = Convert.ToBoolean( -1);
         Ddo_checklist_obrigatorio_Includefilter = Convert.ToBoolean( 0);
         Ddo_checklist_obrigatorio_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_checklist_obrigatorio_Includesortasc = Convert.ToBoolean( -1);
         Ddo_checklist_obrigatorio_Titlecontrolidtoreplace = "";
         Ddo_checklist_obrigatorio_Dropdownoptionstype = "GridTitleSettings";
         Ddo_checklist_obrigatorio_Cls = "ColumnSettings";
         Ddo_checklist_obrigatorio_Tooltip = "Op��es";
         Ddo_checklist_obrigatorio_Caption = "";
         Ddo_checklist_de_Searchbuttontext = "Filtrar Selecionados";
         Ddo_checklist_de_Cleanfilter = "Limpar pesquisa";
         Ddo_checklist_de_Sortdsc = "Ordenar de Z � A";
         Ddo_checklist_de_Sortasc = "Ordenar de A � Z";
         Ddo_checklist_de_Datalistfixedvalues = "1:Libera valida��o,2:Em an�lise";
         Ddo_checklist_de_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_checklist_de_Datalisttype = "FixedValues";
         Ddo_checklist_de_Includedatalist = Convert.ToBoolean( -1);
         Ddo_checklist_de_Includefilter = Convert.ToBoolean( 0);
         Ddo_checklist_de_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_checklist_de_Includesortasc = Convert.ToBoolean( -1);
         Ddo_checklist_de_Titlecontrolidtoreplace = "";
         Ddo_checklist_de_Dropdownoptionstype = "GridTitleSettings";
         Ddo_checklist_de_Cls = "ColumnSettings";
         Ddo_checklist_de_Tooltip = "Op��es";
         Ddo_checklist_de_Caption = "";
         Ddo_checklist_descricao_Searchbuttontext = "Pesquisar";
         Ddo_checklist_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_checklist_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_checklist_descricao_Loadingdata = "Carregando dados...";
         Ddo_checklist_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_checklist_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_checklist_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_checklist_descricao_Datalistproc = "GetWWCheckListFilterData";
         Ddo_checklist_descricao_Datalisttype = "Dynamic";
         Ddo_checklist_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_checklist_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_checklist_descricao_Filtertype = "Character";
         Ddo_checklist_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_checklist_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_checklist_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_checklist_descricao_Titlecontrolidtoreplace = "";
         Ddo_checklist_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_checklist_descricao_Cls = "ColumnSettings";
         Ddo_checklist_descricao_Tooltip = "Op��es";
         Ddo_checklist_descricao_Caption = "";
         Ddo_check_codigo_Searchbuttontext = "Pesquisar";
         Ddo_check_codigo_Rangefilterto = "At�";
         Ddo_check_codigo_Rangefilterfrom = "Desde";
         Ddo_check_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_check_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_check_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_check_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_check_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_check_codigo_Filtertype = "Numeric";
         Ddo_check_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_check_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_check_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_check_codigo_Titlecontrolidtoreplace = "";
         Ddo_check_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_check_codigo_Cls = "ColumnSettings";
         Ddo_check_codigo_Tooltip = "Op��es";
         Ddo_check_codigo_Caption = "";
         Ddo_checklist_codigo_Searchbuttontext = "Pesquisar";
         Ddo_checklist_codigo_Rangefilterto = "At�";
         Ddo_checklist_codigo_Rangefilterfrom = "Desde";
         Ddo_checklist_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_checklist_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_checklist_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_checklist_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_checklist_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_checklist_codigo_Filtertype = "Numeric";
         Ddo_checklist_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_checklist_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_checklist_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_checklist_codigo_Titlecontrolidtoreplace = "";
         Ddo_checklist_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_checklist_codigo_Cls = "ColumnSettings";
         Ddo_checklist_codigo_Tooltip = "Op��es";
         Ddo_checklist_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Ddo_managefilters_Cls = "ManageFilters";
         Ddo_managefilters_Tooltip = "Gerenciar filtros";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Check List";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A758CheckList_Codigo',fld:'CHECKLIST_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV27Check_Codigo',fld:'vCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV50ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV59ddo_CheckList_CodigoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_Check_CodigoTitleControlIdToReplace',fld:'vDDO_CHECK_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_CheckList_DescricaoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_CheckList_DeTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace',fld:'vDDO_CHECKLIST_OBRIGATORIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_CheckList_AtivoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17CheckList_De1',fld:'vCHECKLIST_DE1',pic:'ZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20CheckList_De2',fld:'vCHECKLIST_DE2',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23CheckList_De3',fld:'vCHECKLIST_DE3',pic:'ZZZ9',nv:0},{av:'AV57TFCheckList_Codigo',fld:'vTFCHECKLIST_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV58TFCheckList_Codigo_To',fld:'vTFCHECKLIST_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV61TFCheck_Codigo',fld:'vTFCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62TFCheck_Codigo_To',fld:'vTFCHECK_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFCheckList_Descricao',fld:'vTFCHECKLIST_DESCRICAO',pic:'',nv:''},{av:'AV66TFCheckList_Descricao_Sel',fld:'vTFCHECKLIST_DESCRICAO_SEL',pic:'',nv:''},{av:'AV70TFCheckList_De_Sels',fld:'vTFCHECKLIST_DE_SELS',pic:'',nv:null},{av:'AV73TFCheckList_Obrigatorio_Sel',fld:'vTFCHECKLIST_OBRIGATORIO_SEL',pic:'9',nv:0},{av:'AV76TFCheckList_Ativo_Sel',fld:'vTFCHECKLIST_ATIVO_SEL',pic:'9',nv:0},{av:'AV103Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV56CheckList_CodigoTitleFilterData',fld:'vCHECKLIST_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV60Check_CodigoTitleFilterData',fld:'vCHECK_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV64CheckList_DescricaoTitleFilterData',fld:'vCHECKLIST_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV68CheckList_DeTitleFilterData',fld:'vCHECKLIST_DETITLEFILTERDATA',pic:'',nv:null},{av:'AV72CheckList_ObrigatorioTitleFilterData',fld:'vCHECKLIST_OBRIGATORIOTITLEFILTERDATA',pic:'',nv:null},{av:'AV75CheckList_AtivoTitleFilterData',fld:'vCHECKLIST_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV50ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'edtCheckList_Codigo_Titleformat',ctrl:'CHECKLIST_CODIGO',prop:'Titleformat'},{av:'edtCheckList_Codigo_Title',ctrl:'CHECKLIST_CODIGO',prop:'Title'},{av:'edtCheck_Codigo_Titleformat',ctrl:'CHECK_CODIGO',prop:'Titleformat'},{av:'edtCheck_Codigo_Title',ctrl:'CHECK_CODIGO',prop:'Title'},{av:'edtCheckList_Descricao_Titleformat',ctrl:'CHECKLIST_DESCRICAO',prop:'Titleformat'},{av:'edtCheckList_Descricao_Title',ctrl:'CHECKLIST_DESCRICAO',prop:'Title'},{av:'cmbCheckList_De'},{av:'cmbCheckList_Obrigatorio'},{av:'chkCheckList_Ativo_Titleformat',ctrl:'CHECKLIST_ATIVO',prop:'Titleformat'},{av:'chkCheckList_Ativo.Title.Text',ctrl:'CHECKLIST_ATIVO',prop:'Title'},{av:'AV80GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV81GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV54ManageFiltersData',fld:'vMANAGEFILTERSDATA',pic:'',nv:null},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E12O22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17CheckList_De1',fld:'vCHECKLIST_DE1',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20CheckList_De2',fld:'vCHECKLIST_DE2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23CheckList_De3',fld:'vCHECKLIST_DE3',pic:'ZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV57TFCheckList_Codigo',fld:'vTFCHECKLIST_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV58TFCheckList_Codigo_To',fld:'vTFCHECKLIST_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV61TFCheck_Codigo',fld:'vTFCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62TFCheck_Codigo_To',fld:'vTFCHECK_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFCheckList_Descricao',fld:'vTFCHECKLIST_DESCRICAO',pic:'',nv:''},{av:'AV66TFCheckList_Descricao_Sel',fld:'vTFCHECKLIST_DESCRICAO_SEL',pic:'',nv:''},{av:'AV73TFCheckList_Obrigatorio_Sel',fld:'vTFCHECKLIST_OBRIGATORIO_SEL',pic:'9',nv:0},{av:'AV76TFCheckList_Ativo_Sel',fld:'vTFCHECKLIST_ATIVO_SEL',pic:'9',nv:0},{av:'AV50ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV59ddo_CheckList_CodigoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_Check_CodigoTitleControlIdToReplace',fld:'vDDO_CHECK_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_CheckList_DescricaoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_CheckList_DeTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace',fld:'vDDO_CHECKLIST_OBRIGATORIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_CheckList_AtivoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70TFCheckList_De_Sels',fld:'vTFCHECKLIST_DE_SELS',pic:'',nv:null},{av:'AV103Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A758CheckList_Codigo',fld:'CHECKLIST_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV27Check_Codigo',fld:'vCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CHECKLIST_CODIGO.ONOPTIONCLICKED","{handler:'E13O22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17CheckList_De1',fld:'vCHECKLIST_DE1',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20CheckList_De2',fld:'vCHECKLIST_DE2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23CheckList_De3',fld:'vCHECKLIST_DE3',pic:'ZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV57TFCheckList_Codigo',fld:'vTFCHECKLIST_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV58TFCheckList_Codigo_To',fld:'vTFCHECKLIST_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV61TFCheck_Codigo',fld:'vTFCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62TFCheck_Codigo_To',fld:'vTFCHECK_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFCheckList_Descricao',fld:'vTFCHECKLIST_DESCRICAO',pic:'',nv:''},{av:'AV66TFCheckList_Descricao_Sel',fld:'vTFCHECKLIST_DESCRICAO_SEL',pic:'',nv:''},{av:'AV73TFCheckList_Obrigatorio_Sel',fld:'vTFCHECKLIST_OBRIGATORIO_SEL',pic:'9',nv:0},{av:'AV76TFCheckList_Ativo_Sel',fld:'vTFCHECKLIST_ATIVO_SEL',pic:'9',nv:0},{av:'AV50ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV59ddo_CheckList_CodigoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_Check_CodigoTitleControlIdToReplace',fld:'vDDO_CHECK_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_CheckList_DescricaoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_CheckList_DeTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace',fld:'vDDO_CHECKLIST_OBRIGATORIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_CheckList_AtivoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70TFCheckList_De_Sels',fld:'vTFCHECKLIST_DE_SELS',pic:'',nv:null},{av:'AV103Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A758CheckList_Codigo',fld:'CHECKLIST_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV27Check_Codigo',fld:'vCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_checklist_codigo_Activeeventkey',ctrl:'DDO_CHECKLIST_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_checklist_codigo_Filteredtext_get',ctrl:'DDO_CHECKLIST_CODIGO',prop:'FilteredText_get'},{av:'Ddo_checklist_codigo_Filteredtextto_get',ctrl:'DDO_CHECKLIST_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_checklist_codigo_Sortedstatus',ctrl:'DDO_CHECKLIST_CODIGO',prop:'SortedStatus'},{av:'AV57TFCheckList_Codigo',fld:'vTFCHECKLIST_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV58TFCheckList_Codigo_To',fld:'vTFCHECKLIST_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_check_codigo_Sortedstatus',ctrl:'DDO_CHECK_CODIGO',prop:'SortedStatus'},{av:'Ddo_checklist_descricao_Sortedstatus',ctrl:'DDO_CHECKLIST_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_checklist_de_Sortedstatus',ctrl:'DDO_CHECKLIST_DE',prop:'SortedStatus'},{av:'Ddo_checklist_obrigatorio_Sortedstatus',ctrl:'DDO_CHECKLIST_OBRIGATORIO',prop:'SortedStatus'},{av:'Ddo_checklist_ativo_Sortedstatus',ctrl:'DDO_CHECKLIST_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CHECK_CODIGO.ONOPTIONCLICKED","{handler:'E14O22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17CheckList_De1',fld:'vCHECKLIST_DE1',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20CheckList_De2',fld:'vCHECKLIST_DE2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23CheckList_De3',fld:'vCHECKLIST_DE3',pic:'ZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV57TFCheckList_Codigo',fld:'vTFCHECKLIST_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV58TFCheckList_Codigo_To',fld:'vTFCHECKLIST_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV61TFCheck_Codigo',fld:'vTFCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62TFCheck_Codigo_To',fld:'vTFCHECK_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFCheckList_Descricao',fld:'vTFCHECKLIST_DESCRICAO',pic:'',nv:''},{av:'AV66TFCheckList_Descricao_Sel',fld:'vTFCHECKLIST_DESCRICAO_SEL',pic:'',nv:''},{av:'AV73TFCheckList_Obrigatorio_Sel',fld:'vTFCHECKLIST_OBRIGATORIO_SEL',pic:'9',nv:0},{av:'AV76TFCheckList_Ativo_Sel',fld:'vTFCHECKLIST_ATIVO_SEL',pic:'9',nv:0},{av:'AV50ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV59ddo_CheckList_CodigoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_Check_CodigoTitleControlIdToReplace',fld:'vDDO_CHECK_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_CheckList_DescricaoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_CheckList_DeTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace',fld:'vDDO_CHECKLIST_OBRIGATORIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_CheckList_AtivoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70TFCheckList_De_Sels',fld:'vTFCHECKLIST_DE_SELS',pic:'',nv:null},{av:'AV103Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A758CheckList_Codigo',fld:'CHECKLIST_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV27Check_Codigo',fld:'vCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_check_codigo_Activeeventkey',ctrl:'DDO_CHECK_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_check_codigo_Filteredtext_get',ctrl:'DDO_CHECK_CODIGO',prop:'FilteredText_get'},{av:'Ddo_check_codigo_Filteredtextto_get',ctrl:'DDO_CHECK_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_check_codigo_Sortedstatus',ctrl:'DDO_CHECK_CODIGO',prop:'SortedStatus'},{av:'AV61TFCheck_Codigo',fld:'vTFCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62TFCheck_Codigo_To',fld:'vTFCHECK_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_checklist_codigo_Sortedstatus',ctrl:'DDO_CHECKLIST_CODIGO',prop:'SortedStatus'},{av:'Ddo_checklist_descricao_Sortedstatus',ctrl:'DDO_CHECKLIST_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_checklist_de_Sortedstatus',ctrl:'DDO_CHECKLIST_DE',prop:'SortedStatus'},{av:'Ddo_checklist_obrigatorio_Sortedstatus',ctrl:'DDO_CHECKLIST_OBRIGATORIO',prop:'SortedStatus'},{av:'Ddo_checklist_ativo_Sortedstatus',ctrl:'DDO_CHECKLIST_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CHECKLIST_DESCRICAO.ONOPTIONCLICKED","{handler:'E15O22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17CheckList_De1',fld:'vCHECKLIST_DE1',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20CheckList_De2',fld:'vCHECKLIST_DE2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23CheckList_De3',fld:'vCHECKLIST_DE3',pic:'ZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV57TFCheckList_Codigo',fld:'vTFCHECKLIST_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV58TFCheckList_Codigo_To',fld:'vTFCHECKLIST_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV61TFCheck_Codigo',fld:'vTFCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62TFCheck_Codigo_To',fld:'vTFCHECK_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFCheckList_Descricao',fld:'vTFCHECKLIST_DESCRICAO',pic:'',nv:''},{av:'AV66TFCheckList_Descricao_Sel',fld:'vTFCHECKLIST_DESCRICAO_SEL',pic:'',nv:''},{av:'AV73TFCheckList_Obrigatorio_Sel',fld:'vTFCHECKLIST_OBRIGATORIO_SEL',pic:'9',nv:0},{av:'AV76TFCheckList_Ativo_Sel',fld:'vTFCHECKLIST_ATIVO_SEL',pic:'9',nv:0},{av:'AV50ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV59ddo_CheckList_CodigoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_Check_CodigoTitleControlIdToReplace',fld:'vDDO_CHECK_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_CheckList_DescricaoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_CheckList_DeTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace',fld:'vDDO_CHECKLIST_OBRIGATORIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_CheckList_AtivoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70TFCheckList_De_Sels',fld:'vTFCHECKLIST_DE_SELS',pic:'',nv:null},{av:'AV103Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A758CheckList_Codigo',fld:'CHECKLIST_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV27Check_Codigo',fld:'vCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_checklist_descricao_Activeeventkey',ctrl:'DDO_CHECKLIST_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_checklist_descricao_Filteredtext_get',ctrl:'DDO_CHECKLIST_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_checklist_descricao_Selectedvalue_get',ctrl:'DDO_CHECKLIST_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_checklist_descricao_Sortedstatus',ctrl:'DDO_CHECKLIST_DESCRICAO',prop:'SortedStatus'},{av:'AV65TFCheckList_Descricao',fld:'vTFCHECKLIST_DESCRICAO',pic:'',nv:''},{av:'AV66TFCheckList_Descricao_Sel',fld:'vTFCHECKLIST_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_checklist_codigo_Sortedstatus',ctrl:'DDO_CHECKLIST_CODIGO',prop:'SortedStatus'},{av:'Ddo_check_codigo_Sortedstatus',ctrl:'DDO_CHECK_CODIGO',prop:'SortedStatus'},{av:'Ddo_checklist_de_Sortedstatus',ctrl:'DDO_CHECKLIST_DE',prop:'SortedStatus'},{av:'Ddo_checklist_obrigatorio_Sortedstatus',ctrl:'DDO_CHECKLIST_OBRIGATORIO',prop:'SortedStatus'},{av:'Ddo_checklist_ativo_Sortedstatus',ctrl:'DDO_CHECKLIST_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CHECKLIST_DE.ONOPTIONCLICKED","{handler:'E16O22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17CheckList_De1',fld:'vCHECKLIST_DE1',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20CheckList_De2',fld:'vCHECKLIST_DE2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23CheckList_De3',fld:'vCHECKLIST_DE3',pic:'ZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV57TFCheckList_Codigo',fld:'vTFCHECKLIST_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV58TFCheckList_Codigo_To',fld:'vTFCHECKLIST_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV61TFCheck_Codigo',fld:'vTFCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62TFCheck_Codigo_To',fld:'vTFCHECK_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFCheckList_Descricao',fld:'vTFCHECKLIST_DESCRICAO',pic:'',nv:''},{av:'AV66TFCheckList_Descricao_Sel',fld:'vTFCHECKLIST_DESCRICAO_SEL',pic:'',nv:''},{av:'AV73TFCheckList_Obrigatorio_Sel',fld:'vTFCHECKLIST_OBRIGATORIO_SEL',pic:'9',nv:0},{av:'AV76TFCheckList_Ativo_Sel',fld:'vTFCHECKLIST_ATIVO_SEL',pic:'9',nv:0},{av:'AV50ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV59ddo_CheckList_CodigoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_Check_CodigoTitleControlIdToReplace',fld:'vDDO_CHECK_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_CheckList_DescricaoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_CheckList_DeTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace',fld:'vDDO_CHECKLIST_OBRIGATORIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_CheckList_AtivoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70TFCheckList_De_Sels',fld:'vTFCHECKLIST_DE_SELS',pic:'',nv:null},{av:'AV103Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A758CheckList_Codigo',fld:'CHECKLIST_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV27Check_Codigo',fld:'vCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_checklist_de_Activeeventkey',ctrl:'DDO_CHECKLIST_DE',prop:'ActiveEventKey'},{av:'Ddo_checklist_de_Selectedvalue_get',ctrl:'DDO_CHECKLIST_DE',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_checklist_de_Sortedstatus',ctrl:'DDO_CHECKLIST_DE',prop:'SortedStatus'},{av:'AV70TFCheckList_De_Sels',fld:'vTFCHECKLIST_DE_SELS',pic:'',nv:null},{av:'Ddo_checklist_codigo_Sortedstatus',ctrl:'DDO_CHECKLIST_CODIGO',prop:'SortedStatus'},{av:'Ddo_check_codigo_Sortedstatus',ctrl:'DDO_CHECK_CODIGO',prop:'SortedStatus'},{av:'Ddo_checklist_descricao_Sortedstatus',ctrl:'DDO_CHECKLIST_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_checklist_obrigatorio_Sortedstatus',ctrl:'DDO_CHECKLIST_OBRIGATORIO',prop:'SortedStatus'},{av:'Ddo_checklist_ativo_Sortedstatus',ctrl:'DDO_CHECKLIST_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CHECKLIST_OBRIGATORIO.ONOPTIONCLICKED","{handler:'E17O22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17CheckList_De1',fld:'vCHECKLIST_DE1',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20CheckList_De2',fld:'vCHECKLIST_DE2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23CheckList_De3',fld:'vCHECKLIST_DE3',pic:'ZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV57TFCheckList_Codigo',fld:'vTFCHECKLIST_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV58TFCheckList_Codigo_To',fld:'vTFCHECKLIST_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV61TFCheck_Codigo',fld:'vTFCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62TFCheck_Codigo_To',fld:'vTFCHECK_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFCheckList_Descricao',fld:'vTFCHECKLIST_DESCRICAO',pic:'',nv:''},{av:'AV66TFCheckList_Descricao_Sel',fld:'vTFCHECKLIST_DESCRICAO_SEL',pic:'',nv:''},{av:'AV73TFCheckList_Obrigatorio_Sel',fld:'vTFCHECKLIST_OBRIGATORIO_SEL',pic:'9',nv:0},{av:'AV76TFCheckList_Ativo_Sel',fld:'vTFCHECKLIST_ATIVO_SEL',pic:'9',nv:0},{av:'AV50ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV59ddo_CheckList_CodigoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_Check_CodigoTitleControlIdToReplace',fld:'vDDO_CHECK_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_CheckList_DescricaoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_CheckList_DeTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace',fld:'vDDO_CHECKLIST_OBRIGATORIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_CheckList_AtivoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70TFCheckList_De_Sels',fld:'vTFCHECKLIST_DE_SELS',pic:'',nv:null},{av:'AV103Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A758CheckList_Codigo',fld:'CHECKLIST_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV27Check_Codigo',fld:'vCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_checklist_obrigatorio_Activeeventkey',ctrl:'DDO_CHECKLIST_OBRIGATORIO',prop:'ActiveEventKey'},{av:'Ddo_checklist_obrigatorio_Selectedvalue_get',ctrl:'DDO_CHECKLIST_OBRIGATORIO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_checklist_obrigatorio_Sortedstatus',ctrl:'DDO_CHECKLIST_OBRIGATORIO',prop:'SortedStatus'},{av:'AV73TFCheckList_Obrigatorio_Sel',fld:'vTFCHECKLIST_OBRIGATORIO_SEL',pic:'9',nv:0},{av:'Ddo_checklist_codigo_Sortedstatus',ctrl:'DDO_CHECKLIST_CODIGO',prop:'SortedStatus'},{av:'Ddo_check_codigo_Sortedstatus',ctrl:'DDO_CHECK_CODIGO',prop:'SortedStatus'},{av:'Ddo_checklist_descricao_Sortedstatus',ctrl:'DDO_CHECKLIST_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_checklist_de_Sortedstatus',ctrl:'DDO_CHECKLIST_DE',prop:'SortedStatus'},{av:'Ddo_checklist_ativo_Sortedstatus',ctrl:'DDO_CHECKLIST_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CHECKLIST_ATIVO.ONOPTIONCLICKED","{handler:'E18O22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17CheckList_De1',fld:'vCHECKLIST_DE1',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20CheckList_De2',fld:'vCHECKLIST_DE2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23CheckList_De3',fld:'vCHECKLIST_DE3',pic:'ZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV57TFCheckList_Codigo',fld:'vTFCHECKLIST_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV58TFCheckList_Codigo_To',fld:'vTFCHECKLIST_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV61TFCheck_Codigo',fld:'vTFCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62TFCheck_Codigo_To',fld:'vTFCHECK_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFCheckList_Descricao',fld:'vTFCHECKLIST_DESCRICAO',pic:'',nv:''},{av:'AV66TFCheckList_Descricao_Sel',fld:'vTFCHECKLIST_DESCRICAO_SEL',pic:'',nv:''},{av:'AV73TFCheckList_Obrigatorio_Sel',fld:'vTFCHECKLIST_OBRIGATORIO_SEL',pic:'9',nv:0},{av:'AV76TFCheckList_Ativo_Sel',fld:'vTFCHECKLIST_ATIVO_SEL',pic:'9',nv:0},{av:'AV50ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV59ddo_CheckList_CodigoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_Check_CodigoTitleControlIdToReplace',fld:'vDDO_CHECK_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_CheckList_DescricaoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_CheckList_DeTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace',fld:'vDDO_CHECKLIST_OBRIGATORIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_CheckList_AtivoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70TFCheckList_De_Sels',fld:'vTFCHECKLIST_DE_SELS',pic:'',nv:null},{av:'AV103Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A758CheckList_Codigo',fld:'CHECKLIST_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV27Check_Codigo',fld:'vCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_checklist_ativo_Activeeventkey',ctrl:'DDO_CHECKLIST_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_checklist_ativo_Selectedvalue_get',ctrl:'DDO_CHECKLIST_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_checklist_ativo_Sortedstatus',ctrl:'DDO_CHECKLIST_ATIVO',prop:'SortedStatus'},{av:'AV76TFCheckList_Ativo_Sel',fld:'vTFCHECKLIST_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_checklist_codigo_Sortedstatus',ctrl:'DDO_CHECKLIST_CODIGO',prop:'SortedStatus'},{av:'Ddo_check_codigo_Sortedstatus',ctrl:'DDO_CHECK_CODIGO',prop:'SortedStatus'},{av:'Ddo_checklist_descricao_Sortedstatus',ctrl:'DDO_CHECKLIST_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_checklist_de_Sortedstatus',ctrl:'DDO_CHECKLIST_DE',prop:'SortedStatus'},{av:'Ddo_checklist_obrigatorio_Sortedstatus',ctrl:'DDO_CHECKLIST_OBRIGATORIO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E30O22',iparms:[{av:'A758CheckList_Codigo',fld:'CHECKLIST_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV27Check_Codigo',fld:'vCHECK_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV26Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV28Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E19O22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17CheckList_De1',fld:'vCHECKLIST_DE1',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20CheckList_De2',fld:'vCHECKLIST_DE2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23CheckList_De3',fld:'vCHECKLIST_DE3',pic:'ZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV57TFCheckList_Codigo',fld:'vTFCHECKLIST_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV58TFCheckList_Codigo_To',fld:'vTFCHECKLIST_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV61TFCheck_Codigo',fld:'vTFCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62TFCheck_Codigo_To',fld:'vTFCHECK_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFCheckList_Descricao',fld:'vTFCHECKLIST_DESCRICAO',pic:'',nv:''},{av:'AV66TFCheckList_Descricao_Sel',fld:'vTFCHECKLIST_DESCRICAO_SEL',pic:'',nv:''},{av:'AV73TFCheckList_Obrigatorio_Sel',fld:'vTFCHECKLIST_OBRIGATORIO_SEL',pic:'9',nv:0},{av:'AV76TFCheckList_Ativo_Sel',fld:'vTFCHECKLIST_ATIVO_SEL',pic:'9',nv:0},{av:'AV50ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV59ddo_CheckList_CodigoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_Check_CodigoTitleControlIdToReplace',fld:'vDDO_CHECK_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_CheckList_DescricaoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_CheckList_DeTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace',fld:'vDDO_CHECKLIST_OBRIGATORIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_CheckList_AtivoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70TFCheckList_De_Sels',fld:'vTFCHECKLIST_DE_SELS',pic:'',nv:null},{av:'AV103Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A758CheckList_Codigo',fld:'CHECKLIST_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV27Check_Codigo',fld:'vCHECK_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E23O22',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E20O22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17CheckList_De1',fld:'vCHECKLIST_DE1',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20CheckList_De2',fld:'vCHECKLIST_DE2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23CheckList_De3',fld:'vCHECKLIST_DE3',pic:'ZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV57TFCheckList_Codigo',fld:'vTFCHECKLIST_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV58TFCheckList_Codigo_To',fld:'vTFCHECKLIST_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV61TFCheck_Codigo',fld:'vTFCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62TFCheck_Codigo_To',fld:'vTFCHECK_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFCheckList_Descricao',fld:'vTFCHECKLIST_DESCRICAO',pic:'',nv:''},{av:'AV66TFCheckList_Descricao_Sel',fld:'vTFCHECKLIST_DESCRICAO_SEL',pic:'',nv:''},{av:'AV73TFCheckList_Obrigatorio_Sel',fld:'vTFCHECKLIST_OBRIGATORIO_SEL',pic:'9',nv:0},{av:'AV76TFCheckList_Ativo_Sel',fld:'vTFCHECKLIST_ATIVO_SEL',pic:'9',nv:0},{av:'AV50ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV59ddo_CheckList_CodigoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_Check_CodigoTitleControlIdToReplace',fld:'vDDO_CHECK_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_CheckList_DescricaoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_CheckList_DeTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace',fld:'vDDO_CHECKLIST_OBRIGATORIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_CheckList_AtivoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70TFCheckList_De_Sels',fld:'vTFCHECKLIST_DE_SELS',pic:'',nv:null},{av:'AV103Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A758CheckList_Codigo',fld:'CHECKLIST_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV27Check_Codigo',fld:'vCHECK_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20CheckList_De2',fld:'vCHECKLIST_DE2',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23CheckList_De3',fld:'vCHECKLIST_DE3',pic:'ZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17CheckList_De1',fld:'vCHECKLIST_DE1',pic:'ZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'cmbavChecklist_de2'},{av:'cmbavChecklist_de3'},{av:'cmbavChecklist_de1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E24O22',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'cmbavChecklist_de1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E25O22',iparms:[],oparms:[{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E21O22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17CheckList_De1',fld:'vCHECKLIST_DE1',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20CheckList_De2',fld:'vCHECKLIST_DE2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23CheckList_De3',fld:'vCHECKLIST_DE3',pic:'ZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV57TFCheckList_Codigo',fld:'vTFCHECKLIST_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV58TFCheckList_Codigo_To',fld:'vTFCHECKLIST_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV61TFCheck_Codigo',fld:'vTFCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62TFCheck_Codigo_To',fld:'vTFCHECK_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFCheckList_Descricao',fld:'vTFCHECKLIST_DESCRICAO',pic:'',nv:''},{av:'AV66TFCheckList_Descricao_Sel',fld:'vTFCHECKLIST_DESCRICAO_SEL',pic:'',nv:''},{av:'AV73TFCheckList_Obrigatorio_Sel',fld:'vTFCHECKLIST_OBRIGATORIO_SEL',pic:'9',nv:0},{av:'AV76TFCheckList_Ativo_Sel',fld:'vTFCHECKLIST_ATIVO_SEL',pic:'9',nv:0},{av:'AV50ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV59ddo_CheckList_CodigoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_Check_CodigoTitleControlIdToReplace',fld:'vDDO_CHECK_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_CheckList_DescricaoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_CheckList_DeTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace',fld:'vDDO_CHECKLIST_OBRIGATORIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_CheckList_AtivoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70TFCheckList_De_Sels',fld:'vTFCHECKLIST_DE_SELS',pic:'',nv:null},{av:'AV103Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A758CheckList_Codigo',fld:'CHECKLIST_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV27Check_Codigo',fld:'vCHECK_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20CheckList_De2',fld:'vCHECKLIST_DE2',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23CheckList_De3',fld:'vCHECKLIST_DE3',pic:'ZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17CheckList_De1',fld:'vCHECKLIST_DE1',pic:'ZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'cmbavChecklist_de2'},{av:'cmbavChecklist_de3'},{av:'cmbavChecklist_de1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E26O22',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'cmbavChecklist_de2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E22O22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17CheckList_De1',fld:'vCHECKLIST_DE1',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20CheckList_De2',fld:'vCHECKLIST_DE2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23CheckList_De3',fld:'vCHECKLIST_DE3',pic:'ZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV57TFCheckList_Codigo',fld:'vTFCHECKLIST_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV58TFCheckList_Codigo_To',fld:'vTFCHECKLIST_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV61TFCheck_Codigo',fld:'vTFCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62TFCheck_Codigo_To',fld:'vTFCHECK_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFCheckList_Descricao',fld:'vTFCHECKLIST_DESCRICAO',pic:'',nv:''},{av:'AV66TFCheckList_Descricao_Sel',fld:'vTFCHECKLIST_DESCRICAO_SEL',pic:'',nv:''},{av:'AV73TFCheckList_Obrigatorio_Sel',fld:'vTFCHECKLIST_OBRIGATORIO_SEL',pic:'9',nv:0},{av:'AV76TFCheckList_Ativo_Sel',fld:'vTFCHECKLIST_ATIVO_SEL',pic:'9',nv:0},{av:'AV50ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV59ddo_CheckList_CodigoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_Check_CodigoTitleControlIdToReplace',fld:'vDDO_CHECK_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_CheckList_DescricaoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_CheckList_DeTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace',fld:'vDDO_CHECKLIST_OBRIGATORIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_CheckList_AtivoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70TFCheckList_De_Sels',fld:'vTFCHECKLIST_DE_SELS',pic:'',nv:null},{av:'AV103Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A758CheckList_Codigo',fld:'CHECKLIST_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV27Check_Codigo',fld:'vCHECK_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20CheckList_De2',fld:'vCHECKLIST_DE2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23CheckList_De3',fld:'vCHECKLIST_DE3',pic:'ZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17CheckList_De1',fld:'vCHECKLIST_DE1',pic:'ZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'cmbavChecklist_de2'},{av:'cmbavChecklist_de3'},{av:'cmbavChecklist_de1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E27O22',iparms:[{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'cmbavChecklist_de3'}]}");
         setEventMetadata("DDO_MANAGEFILTERS.ONOPTIONCLICKED","{handler:'E11O22',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17CheckList_De1',fld:'vCHECKLIST_DE1',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20CheckList_De2',fld:'vCHECKLIST_DE2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23CheckList_De3',fld:'vCHECKLIST_DE3',pic:'ZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV57TFCheckList_Codigo',fld:'vTFCHECKLIST_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV58TFCheckList_Codigo_To',fld:'vTFCHECKLIST_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV61TFCheck_Codigo',fld:'vTFCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV62TFCheck_Codigo_To',fld:'vTFCHECK_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV65TFCheckList_Descricao',fld:'vTFCHECKLIST_DESCRICAO',pic:'',nv:''},{av:'AV66TFCheckList_Descricao_Sel',fld:'vTFCHECKLIST_DESCRICAO_SEL',pic:'',nv:''},{av:'AV73TFCheckList_Obrigatorio_Sel',fld:'vTFCHECKLIST_OBRIGATORIO_SEL',pic:'9',nv:0},{av:'AV76TFCheckList_Ativo_Sel',fld:'vTFCHECKLIST_ATIVO_SEL',pic:'9',nv:0},{av:'AV50ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV59ddo_CheckList_CodigoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV63ddo_Check_CodigoTitleControlIdToReplace',fld:'vDDO_CHECK_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_CheckList_DescricaoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_CheckList_DeTitleControlIdToReplace',fld:'vDDO_CHECKLIST_DETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace',fld:'vDDO_CHECKLIST_OBRIGATORIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV77ddo_CheckList_AtivoTitleControlIdToReplace',fld:'vDDO_CHECKLIST_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV70TFCheckList_De_Sels',fld:'vTFCHECKLIST_DE_SELS',pic:'',nv:null},{av:'AV103Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A758CheckList_Codigo',fld:'CHECKLIST_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV27Check_Codigo',fld:'vCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_managefilters_Activeeventkey',ctrl:'DDO_MANAGEFILTERS',prop:'ActiveEventKey'}],oparms:[{av:'AV50ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV57TFCheckList_Codigo',fld:'vTFCHECKLIST_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_checklist_codigo_Filteredtext_set',ctrl:'DDO_CHECKLIST_CODIGO',prop:'FilteredText_set'},{av:'AV58TFCheckList_Codigo_To',fld:'vTFCHECKLIST_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_checklist_codigo_Filteredtextto_set',ctrl:'DDO_CHECKLIST_CODIGO',prop:'FilteredTextTo_set'},{av:'AV61TFCheck_Codigo',fld:'vTFCHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_check_codigo_Filteredtext_set',ctrl:'DDO_CHECK_CODIGO',prop:'FilteredText_set'},{av:'AV62TFCheck_Codigo_To',fld:'vTFCHECK_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_check_codigo_Filteredtextto_set',ctrl:'DDO_CHECK_CODIGO',prop:'FilteredTextTo_set'},{av:'AV65TFCheckList_Descricao',fld:'vTFCHECKLIST_DESCRICAO',pic:'',nv:''},{av:'Ddo_checklist_descricao_Filteredtext_set',ctrl:'DDO_CHECKLIST_DESCRICAO',prop:'FilteredText_set'},{av:'AV66TFCheckList_Descricao_Sel',fld:'vTFCHECKLIST_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_checklist_descricao_Selectedvalue_set',ctrl:'DDO_CHECKLIST_DESCRICAO',prop:'SelectedValue_set'},{av:'AV70TFCheckList_De_Sels',fld:'vTFCHECKLIST_DE_SELS',pic:'',nv:null},{av:'Ddo_checklist_de_Selectedvalue_set',ctrl:'DDO_CHECKLIST_DE',prop:'SelectedValue_set'},{av:'AV73TFCheckList_Obrigatorio_Sel',fld:'vTFCHECKLIST_OBRIGATORIO_SEL',pic:'9',nv:0},{av:'Ddo_checklist_obrigatorio_Selectedvalue_set',ctrl:'DDO_CHECKLIST_OBRIGATORIO',prop:'SelectedValue_set'},{av:'AV76TFCheckList_Ativo_Sel',fld:'vTFCHECKLIST_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_checklist_ativo_Selectedvalue_set',ctrl:'DDO_CHECKLIST_ATIVO',prop:'SelectedValue_set'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17CheckList_De1',fld:'vCHECKLIST_DE1',pic:'ZZZ9',nv:0},{av:'Ddo_checklist_ativo_Sortedstatus',ctrl:'DDO_CHECKLIST_ATIVO',prop:'SortedStatus'},{av:'Ddo_checklist_obrigatorio_Sortedstatus',ctrl:'DDO_CHECKLIST_OBRIGATORIO',prop:'SortedStatus'},{av:'Ddo_checklist_de_Sortedstatus',ctrl:'DDO_CHECKLIST_DE',prop:'SortedStatus'},{av:'Ddo_checklist_descricao_Sortedstatus',ctrl:'DDO_CHECKLIST_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_check_codigo_Sortedstatus',ctrl:'DDO_CHECK_CODIGO',prop:'SortedStatus'},{av:'Ddo_checklist_codigo_Sortedstatus',ctrl:'DDO_CHECKLIST_CODIGO',prop:'SortedStatus'},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20CheckList_De2',fld:'vCHECKLIST_DE2',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV22DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV23CheckList_De3',fld:'vCHECKLIST_DE3',pic:'ZZZ9',nv:0},{av:'cmbavChecklist_de1'},{av:'cmbavChecklist_de2'},{av:'cmbavChecklist_de3'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_checklist_codigo_Activeeventkey = "";
         Ddo_checklist_codigo_Filteredtext_get = "";
         Ddo_checklist_codigo_Filteredtextto_get = "";
         Ddo_check_codigo_Activeeventkey = "";
         Ddo_check_codigo_Filteredtext_get = "";
         Ddo_check_codigo_Filteredtextto_get = "";
         Ddo_checklist_descricao_Activeeventkey = "";
         Ddo_checklist_descricao_Filteredtext_get = "";
         Ddo_checklist_descricao_Selectedvalue_get = "";
         Ddo_checklist_de_Activeeventkey = "";
         Ddo_checklist_de_Selectedvalue_get = "";
         Ddo_checklist_obrigatorio_Activeeventkey = "";
         Ddo_checklist_obrigatorio_Selectedvalue_get = "";
         Ddo_checklist_ativo_Activeeventkey = "";
         Ddo_checklist_ativo_Selectedvalue_get = "";
         Ddo_managefilters_Activeeventkey = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV16DynamicFiltersSelector1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV22DynamicFiltersSelector3 = "";
         AV65TFCheckList_Descricao = "";
         AV66TFCheckList_Descricao_Sel = "";
         AV59ddo_CheckList_CodigoTitleControlIdToReplace = "";
         AV63ddo_Check_CodigoTitleControlIdToReplace = "";
         AV67ddo_CheckList_DescricaoTitleControlIdToReplace = "";
         AV71ddo_CheckList_DeTitleControlIdToReplace = "";
         AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace = "";
         AV77ddo_CheckList_AtivoTitleControlIdToReplace = "";
         AV70TFCheckList_De_Sels = new GxSimpleCollection();
         AV103Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV54ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV78DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV56CheckList_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV60Check_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV64CheckList_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV68CheckList_DeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV72CheckList_ObrigatorioTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV75CheckList_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_managefilters_Icon = "";
         Ddo_managefilters_Caption = "";
         Ddo_checklist_codigo_Filteredtext_set = "";
         Ddo_checklist_codigo_Filteredtextto_set = "";
         Ddo_checklist_codigo_Sortedstatus = "";
         Ddo_check_codigo_Filteredtext_set = "";
         Ddo_check_codigo_Filteredtextto_set = "";
         Ddo_check_codigo_Sortedstatus = "";
         Ddo_checklist_descricao_Filteredtext_set = "";
         Ddo_checklist_descricao_Selectedvalue_set = "";
         Ddo_checklist_descricao_Sortedstatus = "";
         Ddo_checklist_de_Selectedvalue_set = "";
         Ddo_checklist_de_Sortedstatus = "";
         Ddo_checklist_obrigatorio_Selectedvalue_set = "";
         Ddo_checklist_obrigatorio_Sortedstatus = "";
         Ddo_checklist_ativo_Selectedvalue_set = "";
         Ddo_checklist_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV26Update = "";
         AV101Update_GXI = "";
         AV28Delete = "";
         AV102Delete_GXI = "";
         A763CheckList_Descricao = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         AV98WWCheckListDS_15_Tfchecklist_de_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV96WWCheckListDS_13_Tfchecklist_descricao = "";
         AV84WWCheckListDS_1_Dynamicfiltersselector1 = "";
         AV87WWCheckListDS_4_Dynamicfiltersselector2 = "";
         AV90WWCheckListDS_7_Dynamicfiltersselector3 = "";
         AV97WWCheckListDS_14_Tfchecklist_descricao_sel = "";
         AV96WWCheckListDS_13_Tfchecklist_descricao = "";
         H00O22_A1151CheckList_Ativo = new bool[] {false} ;
         H00O22_A1845CheckList_Obrigatorio = new bool[] {false} ;
         H00O22_n1845CheckList_Obrigatorio = new bool[] {false} ;
         H00O22_A1230CheckList_De = new short[1] ;
         H00O22_n1230CheckList_De = new bool[] {false} ;
         H00O22_A763CheckList_Descricao = new String[] {""} ;
         H00O22_A1839Check_Codigo = new int[1] ;
         H00O22_n1839Check_Codigo = new bool[] {false} ;
         H00O22_A758CheckList_Codigo = new int[1] ;
         H00O23_AGRID_nRecordCount = new long[1] ;
         AV7HTTPRequest = new GxHttpRequest( context);
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV69TFCheckList_De_SelsJson = "";
         GridRow = new GXWebRow();
         AV51ManageFiltersXml = "";
         GXt_char2 = "";
         AV55ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV52ManageFiltersItems = new GxObjectCollection( context, "GridStateCollection.Item", "", "wwpbaseobjects.SdtGridStateCollection_Item", "GeneXus.Programs");
         AV53ManageFiltersItem = new wwpbaseobjects.SdtGridStateCollection_Item(context);
         AV49Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblChecklisttitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwchecklist__default(),
            new Object[][] {
                new Object[] {
               H00O22_A1151CheckList_Ativo, H00O22_A1845CheckList_Obrigatorio, H00O22_n1845CheckList_Obrigatorio, H00O22_A1230CheckList_De, H00O22_n1230CheckList_De, H00O22_A763CheckList_Descricao, H00O22_A1839Check_Codigo, H00O22_n1839Check_Codigo, H00O22_A758CheckList_Codigo
               }
               , new Object[] {
               H00O23_AGRID_nRecordCount
               }
            }
         );
         AV103Pgmname = "WWCheckList";
         /* GeneXus formulas. */
         AV103Pgmname = "WWCheckList";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_71 ;
      private short nGXsfl_71_idx=1 ;
      private short AV13OrderedBy ;
      private short AV17CheckList_De1 ;
      private short AV20CheckList_De2 ;
      private short AV23CheckList_De3 ;
      private short AV73TFCheckList_Obrigatorio_Sel ;
      private short AV76TFCheckList_Ativo_Sel ;
      private short AV50ManageFiltersExecutionStep ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A1230CheckList_De ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_71_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV85WWCheckListDS_2_Checklist_de1 ;
      private short AV88WWCheckListDS_5_Checklist_de2 ;
      private short AV91WWCheckListDS_8_Checklist_de3 ;
      private short AV99WWCheckListDS_16_Tfchecklist_obrigatorio_sel ;
      private short AV100WWCheckListDS_17_Tfchecklist_ativo_sel ;
      private short edtCheckList_Codigo_Titleformat ;
      private short edtCheck_Codigo_Titleformat ;
      private short edtCheckList_Descricao_Titleformat ;
      private short cmbCheckList_De_Titleformat ;
      private short cmbCheckList_Obrigatorio_Titleformat ;
      private short chkCheckList_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV57TFCheckList_Codigo ;
      private int AV58TFCheckList_Codigo_To ;
      private int AV61TFCheck_Codigo ;
      private int AV62TFCheck_Codigo_To ;
      private int A758CheckList_Codigo ;
      private int AV27Check_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_checklist_descricao_Datalistupdateminimumcharacters ;
      private int edtavManagefiltersexecutionstep_Visible ;
      private int edtavTfchecklist_codigo_Visible ;
      private int edtavTfchecklist_codigo_to_Visible ;
      private int edtavTfcheck_codigo_Visible ;
      private int edtavTfcheck_codigo_to_Visible ;
      private int edtavTfchecklist_descricao_Visible ;
      private int edtavTfchecklist_descricao_sel_Visible ;
      private int edtavTfchecklist_obrigatorio_sel_Visible ;
      private int edtavTfchecklist_ativo_sel_Visible ;
      private int edtavDdo_checklist_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_check_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_checklist_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_checklist_detitlecontrolidtoreplace_Visible ;
      private int edtavDdo_checklist_obrigatoriotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_checklist_ativotitlecontrolidtoreplace_Visible ;
      private int A1839Check_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV98WWCheckListDS_15_Tfchecklist_de_sels_Count ;
      private int AV92WWCheckListDS_9_Tfchecklist_codigo ;
      private int AV93WWCheckListDS_10_Tfchecklist_codigo_to ;
      private int AV94WWCheckListDS_11_Tfcheck_codigo ;
      private int AV95WWCheckListDS_12_Tfcheck_codigo_to ;
      private int edtavOrdereddsc_Visible ;
      private int AV79PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int AV104GXV1 ;
      private int AV105GXV2 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV80GridCurrentPage ;
      private long AV81GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_checklist_codigo_Activeeventkey ;
      private String Ddo_checklist_codigo_Filteredtext_get ;
      private String Ddo_checklist_codigo_Filteredtextto_get ;
      private String Ddo_check_codigo_Activeeventkey ;
      private String Ddo_check_codigo_Filteredtext_get ;
      private String Ddo_check_codigo_Filteredtextto_get ;
      private String Ddo_checklist_descricao_Activeeventkey ;
      private String Ddo_checklist_descricao_Filteredtext_get ;
      private String Ddo_checklist_descricao_Selectedvalue_get ;
      private String Ddo_checklist_de_Activeeventkey ;
      private String Ddo_checklist_de_Selectedvalue_get ;
      private String Ddo_checklist_obrigatorio_Activeeventkey ;
      private String Ddo_checklist_obrigatorio_Selectedvalue_get ;
      private String Ddo_checklist_ativo_Activeeventkey ;
      private String Ddo_checklist_ativo_Selectedvalue_get ;
      private String Ddo_managefilters_Activeeventkey ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_71_idx="0001" ;
      private String AV103Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_managefilters_Icon ;
      private String Ddo_managefilters_Caption ;
      private String Ddo_managefilters_Tooltip ;
      private String Ddo_managefilters_Cls ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_checklist_codigo_Caption ;
      private String Ddo_checklist_codigo_Tooltip ;
      private String Ddo_checklist_codigo_Cls ;
      private String Ddo_checklist_codigo_Filteredtext_set ;
      private String Ddo_checklist_codigo_Filteredtextto_set ;
      private String Ddo_checklist_codigo_Dropdownoptionstype ;
      private String Ddo_checklist_codigo_Titlecontrolidtoreplace ;
      private String Ddo_checklist_codigo_Sortedstatus ;
      private String Ddo_checklist_codigo_Filtertype ;
      private String Ddo_checklist_codigo_Sortasc ;
      private String Ddo_checklist_codigo_Sortdsc ;
      private String Ddo_checklist_codigo_Cleanfilter ;
      private String Ddo_checklist_codigo_Rangefilterfrom ;
      private String Ddo_checklist_codigo_Rangefilterto ;
      private String Ddo_checklist_codigo_Searchbuttontext ;
      private String Ddo_check_codigo_Caption ;
      private String Ddo_check_codigo_Tooltip ;
      private String Ddo_check_codigo_Cls ;
      private String Ddo_check_codigo_Filteredtext_set ;
      private String Ddo_check_codigo_Filteredtextto_set ;
      private String Ddo_check_codigo_Dropdownoptionstype ;
      private String Ddo_check_codigo_Titlecontrolidtoreplace ;
      private String Ddo_check_codigo_Sortedstatus ;
      private String Ddo_check_codigo_Filtertype ;
      private String Ddo_check_codigo_Sortasc ;
      private String Ddo_check_codigo_Sortdsc ;
      private String Ddo_check_codigo_Cleanfilter ;
      private String Ddo_check_codigo_Rangefilterfrom ;
      private String Ddo_check_codigo_Rangefilterto ;
      private String Ddo_check_codigo_Searchbuttontext ;
      private String Ddo_checklist_descricao_Caption ;
      private String Ddo_checklist_descricao_Tooltip ;
      private String Ddo_checklist_descricao_Cls ;
      private String Ddo_checklist_descricao_Filteredtext_set ;
      private String Ddo_checklist_descricao_Selectedvalue_set ;
      private String Ddo_checklist_descricao_Dropdownoptionstype ;
      private String Ddo_checklist_descricao_Titlecontrolidtoreplace ;
      private String Ddo_checklist_descricao_Sortedstatus ;
      private String Ddo_checklist_descricao_Filtertype ;
      private String Ddo_checklist_descricao_Datalisttype ;
      private String Ddo_checklist_descricao_Datalistproc ;
      private String Ddo_checklist_descricao_Sortasc ;
      private String Ddo_checklist_descricao_Sortdsc ;
      private String Ddo_checklist_descricao_Loadingdata ;
      private String Ddo_checklist_descricao_Cleanfilter ;
      private String Ddo_checklist_descricao_Noresultsfound ;
      private String Ddo_checklist_descricao_Searchbuttontext ;
      private String Ddo_checklist_de_Caption ;
      private String Ddo_checklist_de_Tooltip ;
      private String Ddo_checklist_de_Cls ;
      private String Ddo_checklist_de_Selectedvalue_set ;
      private String Ddo_checklist_de_Dropdownoptionstype ;
      private String Ddo_checklist_de_Titlecontrolidtoreplace ;
      private String Ddo_checklist_de_Sortedstatus ;
      private String Ddo_checklist_de_Datalisttype ;
      private String Ddo_checklist_de_Datalistfixedvalues ;
      private String Ddo_checklist_de_Sortasc ;
      private String Ddo_checklist_de_Sortdsc ;
      private String Ddo_checklist_de_Cleanfilter ;
      private String Ddo_checklist_de_Searchbuttontext ;
      private String Ddo_checklist_obrigatorio_Caption ;
      private String Ddo_checklist_obrigatorio_Tooltip ;
      private String Ddo_checklist_obrigatorio_Cls ;
      private String Ddo_checklist_obrigatorio_Selectedvalue_set ;
      private String Ddo_checklist_obrigatorio_Dropdownoptionstype ;
      private String Ddo_checklist_obrigatorio_Titlecontrolidtoreplace ;
      private String Ddo_checklist_obrigatorio_Sortedstatus ;
      private String Ddo_checklist_obrigatorio_Datalisttype ;
      private String Ddo_checklist_obrigatorio_Datalistfixedvalues ;
      private String Ddo_checklist_obrigatorio_Sortasc ;
      private String Ddo_checklist_obrigatorio_Sortdsc ;
      private String Ddo_checklist_obrigatorio_Cleanfilter ;
      private String Ddo_checklist_obrigatorio_Searchbuttontext ;
      private String Ddo_checklist_ativo_Caption ;
      private String Ddo_checklist_ativo_Tooltip ;
      private String Ddo_checklist_ativo_Cls ;
      private String Ddo_checklist_ativo_Selectedvalue_set ;
      private String Ddo_checklist_ativo_Dropdownoptionstype ;
      private String Ddo_checklist_ativo_Titlecontrolidtoreplace ;
      private String Ddo_checklist_ativo_Sortedstatus ;
      private String Ddo_checklist_ativo_Datalisttype ;
      private String Ddo_checklist_ativo_Datalistfixedvalues ;
      private String Ddo_checklist_ativo_Sortasc ;
      private String Ddo_checklist_ativo_Sortdsc ;
      private String Ddo_checklist_ativo_Cleanfilter ;
      private String Ddo_checklist_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavManagefiltersexecutionstep_Internalname ;
      private String edtavManagefiltersexecutionstep_Jsonclick ;
      private String edtavTfchecklist_codigo_Internalname ;
      private String edtavTfchecklist_codigo_Jsonclick ;
      private String edtavTfchecklist_codigo_to_Internalname ;
      private String edtavTfchecklist_codigo_to_Jsonclick ;
      private String edtavTfcheck_codigo_Internalname ;
      private String edtavTfcheck_codigo_Jsonclick ;
      private String edtavTfcheck_codigo_to_Internalname ;
      private String edtavTfcheck_codigo_to_Jsonclick ;
      private String edtavTfchecklist_descricao_Internalname ;
      private String edtavTfchecklist_descricao_sel_Internalname ;
      private String edtavTfchecklist_obrigatorio_sel_Internalname ;
      private String edtavTfchecklist_obrigatorio_sel_Jsonclick ;
      private String edtavTfchecklist_ativo_sel_Internalname ;
      private String edtavTfchecklist_ativo_sel_Jsonclick ;
      private String edtavDdo_checklist_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_check_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_checklist_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_checklist_detitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_checklist_obrigatoriotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_checklist_ativotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtCheckList_Codigo_Internalname ;
      private String edtCheck_Codigo_Internalname ;
      private String edtCheckList_Descricao_Internalname ;
      private String cmbCheckList_De_Internalname ;
      private String cmbCheckList_Obrigatorio_Internalname ;
      private String chkCheckList_Ativo_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavChecklist_de1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavChecklist_de2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavChecklist_de3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_checklist_codigo_Internalname ;
      private String Ddo_check_codigo_Internalname ;
      private String Ddo_checklist_descricao_Internalname ;
      private String Ddo_checklist_de_Internalname ;
      private String Ddo_checklist_obrigatorio_Internalname ;
      private String Ddo_checklist_ativo_Internalname ;
      private String Ddo_managefilters_Internalname ;
      private String edtCheckList_Codigo_Title ;
      private String edtCheck_Codigo_Title ;
      private String edtCheckList_Descricao_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String GXt_char2 ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String cmbavChecklist_de1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String cmbavChecklist_de2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String cmbavChecklist_de3_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String lblChecklisttitle_Internalname ;
      private String lblChecklisttitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String sGXsfl_71_fel_idx="0001" ;
      private String ROClassString ;
      private String edtCheckList_Codigo_Jsonclick ;
      private String edtCheck_Codigo_Jsonclick ;
      private String edtCheckList_Descricao_Jsonclick ;
      private String cmbCheckList_De_Jsonclick ;
      private String cmbCheckList_Obrigatorio_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV21DynamicFiltersEnabled3 ;
      private bool AV25DynamicFiltersIgnoreFirst ;
      private bool AV24DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_checklist_codigo_Includesortasc ;
      private bool Ddo_checklist_codigo_Includesortdsc ;
      private bool Ddo_checklist_codigo_Includefilter ;
      private bool Ddo_checklist_codigo_Filterisrange ;
      private bool Ddo_checklist_codigo_Includedatalist ;
      private bool Ddo_check_codigo_Includesortasc ;
      private bool Ddo_check_codigo_Includesortdsc ;
      private bool Ddo_check_codigo_Includefilter ;
      private bool Ddo_check_codigo_Filterisrange ;
      private bool Ddo_check_codigo_Includedatalist ;
      private bool Ddo_checklist_descricao_Includesortasc ;
      private bool Ddo_checklist_descricao_Includesortdsc ;
      private bool Ddo_checklist_descricao_Includefilter ;
      private bool Ddo_checklist_descricao_Filterisrange ;
      private bool Ddo_checklist_descricao_Includedatalist ;
      private bool Ddo_checklist_de_Includesortasc ;
      private bool Ddo_checklist_de_Includesortdsc ;
      private bool Ddo_checklist_de_Includefilter ;
      private bool Ddo_checklist_de_Includedatalist ;
      private bool Ddo_checklist_de_Allowmultipleselection ;
      private bool Ddo_checklist_obrigatorio_Includesortasc ;
      private bool Ddo_checklist_obrigatorio_Includesortdsc ;
      private bool Ddo_checklist_obrigatorio_Includefilter ;
      private bool Ddo_checklist_obrigatorio_Includedatalist ;
      private bool Ddo_checklist_ativo_Includesortasc ;
      private bool Ddo_checklist_ativo_Includesortdsc ;
      private bool Ddo_checklist_ativo_Includefilter ;
      private bool Ddo_checklist_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1839Check_Codigo ;
      private bool n1230CheckList_De ;
      private bool A1845CheckList_Obrigatorio ;
      private bool n1845CheckList_Obrigatorio ;
      private bool A1151CheckList_Ativo ;
      private bool AV86WWCheckListDS_3_Dynamicfiltersenabled2 ;
      private bool AV89WWCheckListDS_6_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV26Update_IsBlob ;
      private bool AV28Delete_IsBlob ;
      private String A763CheckList_Descricao ;
      private String AV69TFCheckList_De_SelsJson ;
      private String AV51ManageFiltersXml ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV22DynamicFiltersSelector3 ;
      private String AV65TFCheckList_Descricao ;
      private String AV66TFCheckList_Descricao_Sel ;
      private String AV59ddo_CheckList_CodigoTitleControlIdToReplace ;
      private String AV63ddo_Check_CodigoTitleControlIdToReplace ;
      private String AV67ddo_CheckList_DescricaoTitleControlIdToReplace ;
      private String AV71ddo_CheckList_DeTitleControlIdToReplace ;
      private String AV74ddo_CheckList_ObrigatorioTitleControlIdToReplace ;
      private String AV77ddo_CheckList_AtivoTitleControlIdToReplace ;
      private String AV101Update_GXI ;
      private String AV102Delete_GXI ;
      private String lV96WWCheckListDS_13_Tfchecklist_descricao ;
      private String AV84WWCheckListDS_1_Dynamicfiltersselector1 ;
      private String AV87WWCheckListDS_4_Dynamicfiltersselector2 ;
      private String AV90WWCheckListDS_7_Dynamicfiltersselector3 ;
      private String AV97WWCheckListDS_14_Tfchecklist_descricao_sel ;
      private String AV96WWCheckListDS_13_Tfchecklist_descricao ;
      private String AV26Update ;
      private String AV28Delete ;
      private IGxSession AV49Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavChecklist_de1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavChecklist_de2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavChecklist_de3 ;
      private GXCombobox cmbCheckList_De ;
      private GXCombobox cmbCheckList_Obrigatorio ;
      private GXCheckbox chkCheckList_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private bool[] H00O22_A1151CheckList_Ativo ;
      private bool[] H00O22_A1845CheckList_Obrigatorio ;
      private bool[] H00O22_n1845CheckList_Obrigatorio ;
      private short[] H00O22_A1230CheckList_De ;
      private bool[] H00O22_n1230CheckList_De ;
      private String[] H00O22_A763CheckList_Descricao ;
      private int[] H00O22_A1839Check_Codigo ;
      private bool[] H00O22_n1839Check_Codigo ;
      private int[] H00O22_A758CheckList_Codigo ;
      private long[] H00O23_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV70TFCheckList_De_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV98WWCheckListDS_15_Tfchecklist_de_sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtGridStateCollection_Item ))]
      private IGxCollection AV52ManageFiltersItems ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV54ManageFiltersData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV56CheckList_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV60Check_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV64CheckList_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV68CheckList_DeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV72CheckList_ObrigatorioTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV75CheckList_AtivoTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtGridStateCollection_Item AV53ManageFiltersItem ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item AV55ManageFiltersDataItem ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV78DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwchecklist__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00O22( IGxContext context ,
                                             short A1230CheckList_De ,
                                             IGxCollection AV98WWCheckListDS_15_Tfchecklist_de_sels ,
                                             String AV84WWCheckListDS_1_Dynamicfiltersselector1 ,
                                             short AV85WWCheckListDS_2_Checklist_de1 ,
                                             bool AV86WWCheckListDS_3_Dynamicfiltersenabled2 ,
                                             String AV87WWCheckListDS_4_Dynamicfiltersselector2 ,
                                             short AV88WWCheckListDS_5_Checklist_de2 ,
                                             bool AV89WWCheckListDS_6_Dynamicfiltersenabled3 ,
                                             String AV90WWCheckListDS_7_Dynamicfiltersselector3 ,
                                             short AV91WWCheckListDS_8_Checklist_de3 ,
                                             int AV92WWCheckListDS_9_Tfchecklist_codigo ,
                                             int AV93WWCheckListDS_10_Tfchecklist_codigo_to ,
                                             int AV94WWCheckListDS_11_Tfcheck_codigo ,
                                             int AV95WWCheckListDS_12_Tfcheck_codigo_to ,
                                             String AV97WWCheckListDS_14_Tfchecklist_descricao_sel ,
                                             String AV96WWCheckListDS_13_Tfchecklist_descricao ,
                                             int AV98WWCheckListDS_15_Tfchecklist_de_sels_Count ,
                                             short AV99WWCheckListDS_16_Tfchecklist_obrigatorio_sel ,
                                             short AV100WWCheckListDS_17_Tfchecklist_ativo_sel ,
                                             int A758CheckList_Codigo ,
                                             int A1839Check_Codigo ,
                                             String A763CheckList_Descricao ,
                                             bool A1845CheckList_Obrigatorio ,
                                             bool A1151CheckList_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [14] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [CheckList_Ativo], [CheckList_Obrigatorio], [CheckList_De], [CheckList_Descricao], [Check_Codigo], [CheckList_Codigo]";
         sFromString = " FROM [CheckList] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV84WWCheckListDS_1_Dynamicfiltersselector1, "CHECKLIST_DE") == 0 ) && ( ! (0==AV85WWCheckListDS_2_Checklist_de1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_De] = @AV85WWCheckListDS_2_Checklist_de1)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_De] = @AV85WWCheckListDS_2_Checklist_de1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( AV86WWCheckListDS_3_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV87WWCheckListDS_4_Dynamicfiltersselector2, "CHECKLIST_DE") == 0 ) && ( ! (0==AV88WWCheckListDS_5_Checklist_de2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_De] = @AV88WWCheckListDS_5_Checklist_de2)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_De] = @AV88WWCheckListDS_5_Checklist_de2)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV89WWCheckListDS_6_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV90WWCheckListDS_7_Dynamicfiltersselector3, "CHECKLIST_DE") == 0 ) && ( ! (0==AV91WWCheckListDS_8_Checklist_de3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_De] = @AV91WWCheckListDS_8_Checklist_de3)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_De] = @AV91WWCheckListDS_8_Checklist_de3)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ! (0==AV92WWCheckListDS_9_Tfchecklist_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_Codigo] >= @AV92WWCheckListDS_9_Tfchecklist_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_Codigo] >= @AV92WWCheckListDS_9_Tfchecklist_codigo)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! (0==AV93WWCheckListDS_10_Tfchecklist_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_Codigo] <= @AV93WWCheckListDS_10_Tfchecklist_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_Codigo] <= @AV93WWCheckListDS_10_Tfchecklist_codigo_to)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! (0==AV94WWCheckListDS_11_Tfcheck_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Check_Codigo] >= @AV94WWCheckListDS_11_Tfcheck_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Check_Codigo] >= @AV94WWCheckListDS_11_Tfcheck_codigo)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (0==AV95WWCheckListDS_12_Tfcheck_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Check_Codigo] <= @AV95WWCheckListDS_12_Tfcheck_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Check_Codigo] <= @AV95WWCheckListDS_12_Tfcheck_codigo_to)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV97WWCheckListDS_14_Tfchecklist_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWCheckListDS_13_Tfchecklist_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_Descricao] like @lV96WWCheckListDS_13_Tfchecklist_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_Descricao] like @lV96WWCheckListDS_13_Tfchecklist_descricao)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWCheckListDS_14_Tfchecklist_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_Descricao] = @AV97WWCheckListDS_14_Tfchecklist_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_Descricao] = @AV97WWCheckListDS_14_Tfchecklist_descricao_sel)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV98WWCheckListDS_15_Tfchecklist_de_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV98WWCheckListDS_15_Tfchecklist_de_sels, "[CheckList_De] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV98WWCheckListDS_15_Tfchecklist_de_sels, "[CheckList_De] IN (", ")") + ")";
            }
         }
         if ( AV99WWCheckListDS_16_Tfchecklist_obrigatorio_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_Obrigatorio] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_Obrigatorio] = 1)";
            }
         }
         if ( AV99WWCheckListDS_16_Tfchecklist_obrigatorio_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_Obrigatorio] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_Obrigatorio] = 0)";
            }
         }
         if ( AV100WWCheckListDS_17_Tfchecklist_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_Ativo] = 1)";
            }
         }
         if ( AV100WWCheckListDS_17_Tfchecklist_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [CheckList_De]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [CheckList_De] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [CheckList_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [CheckList_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Check_Codigo]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Check_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [CheckList_Descricao]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [CheckList_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [CheckList_Obrigatorio]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [CheckList_Obrigatorio] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [CheckList_Ativo]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [CheckList_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [CheckList_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_H00O23( IGxContext context ,
                                             short A1230CheckList_De ,
                                             IGxCollection AV98WWCheckListDS_15_Tfchecklist_de_sels ,
                                             String AV84WWCheckListDS_1_Dynamicfiltersselector1 ,
                                             short AV85WWCheckListDS_2_Checklist_de1 ,
                                             bool AV86WWCheckListDS_3_Dynamicfiltersenabled2 ,
                                             String AV87WWCheckListDS_4_Dynamicfiltersselector2 ,
                                             short AV88WWCheckListDS_5_Checklist_de2 ,
                                             bool AV89WWCheckListDS_6_Dynamicfiltersenabled3 ,
                                             String AV90WWCheckListDS_7_Dynamicfiltersselector3 ,
                                             short AV91WWCheckListDS_8_Checklist_de3 ,
                                             int AV92WWCheckListDS_9_Tfchecklist_codigo ,
                                             int AV93WWCheckListDS_10_Tfchecklist_codigo_to ,
                                             int AV94WWCheckListDS_11_Tfcheck_codigo ,
                                             int AV95WWCheckListDS_12_Tfcheck_codigo_to ,
                                             String AV97WWCheckListDS_14_Tfchecklist_descricao_sel ,
                                             String AV96WWCheckListDS_13_Tfchecklist_descricao ,
                                             int AV98WWCheckListDS_15_Tfchecklist_de_sels_Count ,
                                             short AV99WWCheckListDS_16_Tfchecklist_obrigatorio_sel ,
                                             short AV100WWCheckListDS_17_Tfchecklist_ativo_sel ,
                                             int A758CheckList_Codigo ,
                                             int A1839Check_Codigo ,
                                             String A763CheckList_Descricao ,
                                             bool A1845CheckList_Obrigatorio ,
                                             bool A1151CheckList_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [9] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [CheckList] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV84WWCheckListDS_1_Dynamicfiltersselector1, "CHECKLIST_DE") == 0 ) && ( ! (0==AV85WWCheckListDS_2_Checklist_de1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_De] = @AV85WWCheckListDS_2_Checklist_de1)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_De] = @AV85WWCheckListDS_2_Checklist_de1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( AV86WWCheckListDS_3_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV87WWCheckListDS_4_Dynamicfiltersselector2, "CHECKLIST_DE") == 0 ) && ( ! (0==AV88WWCheckListDS_5_Checklist_de2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_De] = @AV88WWCheckListDS_5_Checklist_de2)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_De] = @AV88WWCheckListDS_5_Checklist_de2)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( AV89WWCheckListDS_6_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV90WWCheckListDS_7_Dynamicfiltersselector3, "CHECKLIST_DE") == 0 ) && ( ! (0==AV91WWCheckListDS_8_Checklist_de3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_De] = @AV91WWCheckListDS_8_Checklist_de3)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_De] = @AV91WWCheckListDS_8_Checklist_de3)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ! (0==AV92WWCheckListDS_9_Tfchecklist_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_Codigo] >= @AV92WWCheckListDS_9_Tfchecklist_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_Codigo] >= @AV92WWCheckListDS_9_Tfchecklist_codigo)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ! (0==AV93WWCheckListDS_10_Tfchecklist_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_Codigo] <= @AV93WWCheckListDS_10_Tfchecklist_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_Codigo] <= @AV93WWCheckListDS_10_Tfchecklist_codigo_to)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( ! (0==AV94WWCheckListDS_11_Tfcheck_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Check_Codigo] >= @AV94WWCheckListDS_11_Tfcheck_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Check_Codigo] >= @AV94WWCheckListDS_11_Tfcheck_codigo)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( ! (0==AV95WWCheckListDS_12_Tfcheck_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Check_Codigo] <= @AV95WWCheckListDS_12_Tfcheck_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Check_Codigo] <= @AV95WWCheckListDS_12_Tfcheck_codigo_to)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV97WWCheckListDS_14_Tfchecklist_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWCheckListDS_13_Tfchecklist_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_Descricao] like @lV96WWCheckListDS_13_Tfchecklist_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_Descricao] like @lV96WWCheckListDS_13_Tfchecklist_descricao)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWCheckListDS_14_Tfchecklist_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_Descricao] = @AV97WWCheckListDS_14_Tfchecklist_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_Descricao] = @AV97WWCheckListDS_14_Tfchecklist_descricao_sel)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV98WWCheckListDS_15_Tfchecklist_de_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV98WWCheckListDS_15_Tfchecklist_de_sels, "[CheckList_De] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV98WWCheckListDS_15_Tfchecklist_de_sels, "[CheckList_De] IN (", ")") + ")";
            }
         }
         if ( AV99WWCheckListDS_16_Tfchecklist_obrigatorio_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_Obrigatorio] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_Obrigatorio] = 1)";
            }
         }
         if ( AV99WWCheckListDS_16_Tfchecklist_obrigatorio_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_Obrigatorio] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_Obrigatorio] = 0)";
            }
         }
         if ( AV100WWCheckListDS_17_Tfchecklist_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_Ativo] = 1)";
            }
         }
         if ( AV100WWCheckListDS_17_Tfchecklist_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CheckList_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([CheckList_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00O22(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (short)dynConstraints[17] , (short)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (bool)dynConstraints[22] , (bool)dynConstraints[23] , (short)dynConstraints[24] , (bool)dynConstraints[25] );
               case 1 :
                     return conditional_H00O23(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (short)dynConstraints[17] , (short)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (bool)dynConstraints[22] , (bool)dynConstraints[23] , (short)dynConstraints[24] , (bool)dynConstraints[25] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00O22 ;
          prmH00O22 = new Object[] {
          new Object[] {"@AV85WWCheckListDS_2_Checklist_de1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV88WWCheckListDS_5_Checklist_de2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV91WWCheckListDS_8_Checklist_de3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV92WWCheckListDS_9_Tfchecklist_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV93WWCheckListDS_10_Tfchecklist_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV94WWCheckListDS_11_Tfcheck_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV95WWCheckListDS_12_Tfcheck_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV96WWCheckListDS_13_Tfchecklist_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV97WWCheckListDS_14_Tfchecklist_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00O23 ;
          prmH00O23 = new Object[] {
          new Object[] {"@AV85WWCheckListDS_2_Checklist_de1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV88WWCheckListDS_5_Checklist_de2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV91WWCheckListDS_8_Checklist_de3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV92WWCheckListDS_9_Tfchecklist_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV93WWCheckListDS_10_Tfchecklist_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV94WWCheckListDS_11_Tfcheck_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV95WWCheckListDS_12_Tfcheck_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV96WWCheckListDS_13_Tfchecklist_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV97WWCheckListDS_14_Tfchecklist_descricao_sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00O22", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00O22,11,0,true,false )
             ,new CursorDef("H00O23", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00O23,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getLongVarchar(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[14]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                return;
       }
    }

 }

}
