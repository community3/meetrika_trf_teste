/*
               File: PRC_ContratoCaixaEntradaConfiguracoes
        Description: Busca Configurações da Caixa de Entrada do Contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:56:18.12
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_contratocaixaentradaconfiguracoes : GXProcedure
   {
      public prc_contratocaixaentradaconfiguracoes( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_contratocaixaentradaconfiguracoes( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contrato_Codigo ,
                           out bool aP1_IsRetorno )
      {
         this.A74Contrato_Codigo = aP0_Contrato_Codigo;
         this.AV10IsRetorno = false ;
         initialize();
         executePrivate();
         aP1_IsRetorno=this.AV10IsRetorno;
      }

      public bool executeUdp( int aP0_Contrato_Codigo )
      {
         this.A74Contrato_Codigo = aP0_Contrato_Codigo;
         this.AV10IsRetorno = false ;
         initialize();
         executePrivate();
         aP1_IsRetorno=this.AV10IsRetorno;
         return AV10IsRetorno ;
      }

      public void executeSubmit( int aP0_Contrato_Codigo ,
                                 out bool aP1_IsRetorno )
      {
         prc_contratocaixaentradaconfiguracoes objprc_contratocaixaentradaconfiguracoes;
         objprc_contratocaixaentradaconfiguracoes = new prc_contratocaixaentradaconfiguracoes();
         objprc_contratocaixaentradaconfiguracoes.A74Contrato_Codigo = aP0_Contrato_Codigo;
         objprc_contratocaixaentradaconfiguracoes.AV10IsRetorno = false ;
         objprc_contratocaixaentradaconfiguracoes.context.SetSubmitInitialConfig(context);
         objprc_contratocaixaentradaconfiguracoes.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_contratocaixaentradaconfiguracoes);
         aP1_IsRetorno=this.AV10IsRetorno;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_contratocaixaentradaconfiguracoes)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV10IsRetorno = false;
         /* Using cursor P00Y12 */
         pr_default.execute(0, new Object[] {A74Contrato_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A2097ContratoCaixaEntrada_GestorTodasEnviadas = P00Y12_A2097ContratoCaixaEntrada_GestorTodasEnviadas[0];
            A2098ContratoCaixaEntrada_Codigo = P00Y12_A2098ContratoCaixaEntrada_Codigo[0];
            AV10IsRetorno = A2097ContratoCaixaEntrada_GestorTodasEnviadas;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00Y12_A74Contrato_Codigo = new int[1] ;
         P00Y12_A2097ContratoCaixaEntrada_GestorTodasEnviadas = new bool[] {false} ;
         P00Y12_A2098ContratoCaixaEntrada_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_contratocaixaentradaconfiguracoes__default(),
            new Object[][] {
                new Object[] {
               P00Y12_A74Contrato_Codigo, P00Y12_A2097ContratoCaixaEntrada_GestorTodasEnviadas, P00Y12_A2098ContratoCaixaEntrada_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A74Contrato_Codigo ;
      private int A2098ContratoCaixaEntrada_Codigo ;
      private String scmdbuf ;
      private bool AV10IsRetorno ;
      private bool A2097ContratoCaixaEntrada_GestorTodasEnviadas ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00Y12_A74Contrato_Codigo ;
      private bool[] P00Y12_A2097ContratoCaixaEntrada_GestorTodasEnviadas ;
      private int[] P00Y12_A2098ContratoCaixaEntrada_Codigo ;
      private bool aP1_IsRetorno ;
   }

   public class prc_contratocaixaentradaconfiguracoes__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00Y12 ;
          prmP00Y12 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00Y12", "SELECT TOP 1 [Contrato_Codigo], [ContratoCaixaEntrada_GestorTodasEnviadas], [ContratoCaixaEntrada_Codigo] FROM [ContratoCaixaEntrada] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [ContratoCaixaEntrada_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00Y12,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
