/*
               File: WP_ConsultaDiferencaPF
        Description: Diferen�a de PF
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:23:37.12
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_consultadiferencapf : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_consultadiferencapf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_consultadiferencapf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavServico_codigo = new GXCombobox();
         cmbavFiltro_ano = new GXCombobox();
         cmbavFiltro_mes = new GXCombobox();
         cmbavMes = new GXCombobox();
         cmbavAno = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSERVICO_CODIGO") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvSERVICO_CODIGOFU2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid1") == 0 )
            {
               nRC_GXsfl_20 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_20_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_20_idx = GetNextPar( );
               edtavQtdedmn_Tooltiptext = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdedmn_Internalname, "Tooltiptext", edtavQtdedmn_Tooltiptext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid1_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid1") == 0 )
            {
               edtavQtdedmn_Tooltiptext = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdedmn_Internalname, "Tooltiptext", edtavQtdedmn_Tooltiptext);
               AV23Filtro_Ano = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Filtro_Ano), 4, 0)));
               A52Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n52Contratada_AreaTrabalhoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
               A584ContagemResultado_ContadorFM = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n584ContagemResultado_ContadorFM = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A584ContagemResultado_ContadorFM", StringUtil.LTrim( StringUtil.Str( (decimal)(A584ContagemResultado_ContadorFM), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV57WWPContext);
               AV24Filtro_ContadorFMCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Filtro_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Filtro_ContadorFMCod), 6, 0)));
               A566ContagemResultado_DataUltCnt = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A566ContagemResultado_DataUltCnt", context.localUtil.Format(A566ContagemResultado_DataUltCnt, "99/99/99"));
               AV26Filtro_Mes = (long)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Filtro_Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26Filtro_Mes), 10, 0)));
               A601ContagemResultado_Servico = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n601ContagemResultado_Servico = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A601ContagemResultado_Servico", StringUtil.LTrim( StringUtil.Str( (decimal)(A601ContagemResultado_Servico), 6, 0)));
               AV58Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58Servico_Codigo), 6, 0)));
               A684ContagemResultado_PFBFSUltima = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A684ContagemResultado_PFBFSUltima", StringUtil.LTrim( StringUtil.Str( A684ContagemResultado_PFBFSUltima, 14, 5)));
               A484ContagemResultado_StatusDmn = GetNextPar( );
               n484ContagemResultado_StatusDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
               AV55tQtdeDmn = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55tQtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55tQtdeDmn), 8, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTQTDEDMN", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV55tQtdeDmn), "ZZZZZZZ9")));
               AV53tPFBFS = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53tPFBFS", StringUtil.LTrim( StringUtil.Str( AV53tPFBFS, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTPFBFS", GetSecureSignedToken( "", context.localUtil.Format( AV53tPFBFS, "ZZ,ZZZ,ZZ9.999")));
               AV54tPFFinal = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54tPFFinal", StringUtil.LTrim( StringUtil.Str( AV54tPFFinal, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTPFFINAL", GetSecureSignedToken( "", context.localUtil.Format( AV54tPFFinal, "ZZ,ZZZ,ZZ9.999")));
               AV50tDiferenca = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50tDiferenca", StringUtil.LTrim( StringUtil.Str( AV50tDiferenca, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTDIFERENCA", GetSecureSignedToken( "", context.localUtil.Format( AV50tDiferenca, "ZZ,ZZZ,ZZ9.999")));
               A1Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
               AV11ContagemResultado_ContadorFMCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_contadorfmcod_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV11ContagemResultado_ContadorFMCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CONTADORFMCOD"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( (decimal)(AV11ContagemResultado_ContadorFMCod), "ZZZZZ9")));
               A58Usuario_PessoaNom = GetNextPar( );
               n58Usuario_PessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
               A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               A473ContagemResultado_DataCnt = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A473ContagemResultado_DataCnt", context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"));
               A511ContagemResultado_HoraCnt = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A511ContagemResultado_HoraCnt", A511ContagemResultado_HoraCnt);
               A458ContagemResultado_PFBFS = NumberUtil.Val( GetNextPar( ), ".");
               n458ContagemResultado_PFBFS = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A458ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( A458ContagemResultado_PFBFS, 14, 5)));
               A682ContagemResultado_PFBFMUltima = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A682ContagemResultado_PFBFMUltima", StringUtil.LTrim( StringUtil.Str( A682ContagemResultado_PFBFMUltima, 14, 5)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid1_refresh( AV23Filtro_Ano, A52Contratada_AreaTrabalhoCod, A584ContagemResultado_ContadorFM, AV57WWPContext, AV24Filtro_ContadorFMCod, A566ContagemResultado_DataUltCnt, AV26Filtro_Mes, A601ContagemResultado_Servico, AV58Servico_Codigo, A684ContagemResultado_PFBFSUltima, A484ContagemResultado_StatusDmn, AV55tQtdeDmn, AV53tPFBFS, AV54tPFFinal, AV50tDiferenca, A1Usuario_Codigo, AV11ContagemResultado_ContadorFMCod, A58Usuario_PessoaNom, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, A458ContagemResultado_PFBFS, A682ContagemResultado_PFBFMUltima) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "WP_ConsultaDiferencaPF";
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV53tPFBFS, "ZZ,ZZZ,ZZ9.999");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV54tPFFinal, "ZZ,ZZZ,ZZ9.999");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV55tQtdeDmn), "ZZZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV50tDiferenca, "ZZ,ZZZ,ZZ9.999");
               GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
               GXUtil.WriteLog("wp_consultadiferencapf:[SendSecurityCheck value for]"+"tPFBFS:"+context.localUtil.Format( AV53tPFBFS, "ZZ,ZZZ,ZZ9.999"));
               GXUtil.WriteLog("wp_consultadiferencapf:[SendSecurityCheck value for]"+"tPFFinal:"+context.localUtil.Format( AV54tPFFinal, "ZZ,ZZZ,ZZ9.999"));
               GXUtil.WriteLog("wp_consultadiferencapf:[SendSecurityCheck value for]"+"tQtdeDmn:"+context.localUtil.Format( (decimal)(AV55tQtdeDmn), "ZZZZZZZ9"));
               GXUtil.WriteLog("wp_consultadiferencapf:[SendSecurityCheck value for]"+"tDiferenca:"+context.localUtil.Format( AV50tDiferenca, "ZZ,ZZZ,ZZ9.999"));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAFU2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTFU2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216233721");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_consultadiferencapf.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_20", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_20), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTADORFM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A584ContagemResultado_ContadorFM), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV57WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV57WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATAULTCNT", context.localUtil.DToC( A566ContagemResultado_DataUltCnt, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_SERVICO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A601ContagemResultado_Servico), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFSULTIMA", StringUtil.LTrim( StringUtil.NToC( A684ContagemResultado_PFBFSUltima, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_STATUSDMN", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
         GxWebStd.gx_hidden_field( context, "USUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "USUARIO_PESSOANOM", StringUtil.RTrim( A58Usuario_PessoaNom));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATACNT", context.localUtil.DToC( A473ContagemResultado_DataCnt, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_HORACNT", StringUtil.RTrim( A511ContagemResultado_HoraCnt));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFS", StringUtil.LTrim( StringUtil.NToC( A458ContagemResultado_PFBFS, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFMULTIMA", StringUtil.LTrim( StringUtil.NToC( A682ContagemResultado_PFBFMUltima, 14, 5, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_FILTROCONSCONTADORFM", AV46SDT_FiltroConsContadorFM);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_FILTROCONSCONTADORFM", AV46SDT_FiltroConsContadorFM);
         }
         GxWebStd.gx_hidden_field( context, "gxhash_vTPFBFS", GetSecureSignedToken( "", context.localUtil.Format( AV53tPFBFS, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTPFFINAL", GetSecureSignedToken( "", context.localUtil.Format( AV54tPFFinal, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTQTDEDMN", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV55tQtdeDmn), "ZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTDIFERENCA", GetSecureSignedToken( "", context.localUtil.Format( AV50tDiferenca, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTPFBFS", GetSecureSignedToken( "", context.localUtil.Format( AV53tPFBFS, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTPFFINAL", GetSecureSignedToken( "", context.localUtil.Format( AV54tPFFinal, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTQTDEDMN", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV55tQtdeDmn), "ZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTDIFERENCA", GetSecureSignedToken( "", context.localUtil.Format( AV50tDiferenca, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "INNEWWINDOW_Target", StringUtil.RTrim( Innewwindow_Target));
         GxWebStd.gx_hidden_field( context, "vQTDEDMN_Tooltiptext", StringUtil.RTrim( edtavQtdedmn_Tooltiptext));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "WP_ConsultaDiferencaPF";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV53tPFBFS, "ZZ,ZZZ,ZZ9.999");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV54tPFFinal, "ZZ,ZZZ,ZZ9.999");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV55tQtdeDmn), "ZZZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV50tDiferenca, "ZZ,ZZZ,ZZ9.999");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("wp_consultadiferencapf:[SendSecurityCheck value for]"+"tPFBFS:"+context.localUtil.Format( AV53tPFBFS, "ZZ,ZZZ,ZZ9.999"));
         GXUtil.WriteLog("wp_consultadiferencapf:[SendSecurityCheck value for]"+"tPFFinal:"+context.localUtil.Format( AV54tPFFinal, "ZZ,ZZZ,ZZ9.999"));
         GXUtil.WriteLog("wp_consultadiferencapf:[SendSecurityCheck value for]"+"tQtdeDmn:"+context.localUtil.Format( (decimal)(AV55tQtdeDmn), "ZZZZZZZ9"));
         GXUtil.WriteLog("wp_consultadiferencapf:[SendSecurityCheck value for]"+"tDiferenca:"+context.localUtil.Format( AV50tDiferenca, "ZZ,ZZZ,ZZ9.999"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEFU2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTFU2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_consultadiferencapf.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_ConsultaDiferencaPF" ;
      }

      public override String GetPgmdesc( )
      {
         return "Diferen�a de PF" ;
      }

      protected void WBFU0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_FU2( true) ;
         }
         else
         {
            wb_table1_2_FU2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_FU2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"INNEWWINDOWContainer"+"\"></div>") ;
         }
         wbLoad = true;
      }

      protected void STARTFU2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Diferen�a de PF", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPFU0( ) ;
      }

      protected void WSFU2( )
      {
         STARTFU2( ) ;
         EVTFU2( ) ;
      }

      protected void EVTFU2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11FU2 */
                              E11FU2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 4), "LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 14), "VQTDEDMN.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 14), "VQTDEDMN.CLICK") == 0 ) )
                           {
                              nGXsfl_20_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_20_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_20_idx), 4, 0)), 4, "0");
                              SubsflControlProps_202( ) ;
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_contadorfmcod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_contadorfmcod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_CONTADORFMCOD");
                                 GX_FocusControl = edtavContagemresultado_contadorfmcod_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV11ContagemResultado_ContadorFMCod = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_contadorfmcod_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV11ContagemResultado_ContadorFMCod), 6, 0)));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CONTADORFMCOD"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( (decimal)(AV11ContagemResultado_ContadorFMCod), "ZZZZZ9")));
                              }
                              else
                              {
                                 AV11ContagemResultado_ContadorFMCod = (int)(context.localUtil.CToN( cgiGet( edtavContagemresultado_contadorfmcod_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_contadorfmcod_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV11ContagemResultado_ContadorFMCod), 6, 0)));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CONTADORFMCOD"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( (decimal)(AV11ContagemResultado_ContadorFMCod), "ZZZZZ9")));
                              }
                              AV12ContagemResultado_ContadorFMNom = StringUtil.Upper( cgiGet( edtavContagemresultado_contadorfmnom_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_contadorfmnom_Internalname, AV12ContagemResultado_ContadorFMNom);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CONTADORFMNOM"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, StringUtil.RTrim( context.localUtil.Format( AV12ContagemResultado_ContadorFMNom, "@!"))));
                              cmbavMes.Name = cmbavMes_Internalname;
                              cmbavMes.CurrentValue = cgiGet( cmbavMes_Internalname);
                              AV32Mes = (long)(NumberUtil.Val( cgiGet( cmbavMes_Internalname), "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavMes_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV32Mes), 10, 0)));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMES"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( (decimal)(AV32Mes), "ZZZZZZZZZ9")));
                              cmbavAno.Name = cmbavAno_Internalname;
                              cmbavAno.CurrentValue = cgiGet( cmbavAno_Internalname);
                              AV8Ano = (short)(NumberUtil.Val( cgiGet( cmbavAno_Internalname), "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavAno_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Ano), 4, 0)));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vANO"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( (decimal)(AV8Ano), "ZZZ9")));
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavPfbfs_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPfbfs_Internalname), ",", ".") > 99999999.99999m ) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPFBFS");
                                 GX_FocusControl = edtavPfbfs_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV41PFBFS = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfbfs_Internalname, StringUtil.LTrim( StringUtil.Str( AV41PFBFS, 14, 5)));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFBFS"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( AV41PFBFS, "ZZ,ZZZ,ZZ9.999")));
                              }
                              else
                              {
                                 AV41PFBFS = context.localUtil.CToN( cgiGet( edtavPfbfs_Internalname), ",", ".");
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfbfs_Internalname, StringUtil.LTrim( StringUtil.Str( AV41PFBFS, 14, 5)));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFBFS"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( AV41PFBFS, "ZZ,ZZZ,ZZ9.999")));
                              }
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavPffinal_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPffinal_Internalname), ",", ".") > 99999999.99999m ) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPFFINAL");
                                 GX_FocusControl = edtavPffinal_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV42PFFinal = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPffinal_Internalname, StringUtil.LTrim( StringUtil.Str( AV42PFFinal, 14, 5)));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFFINAL"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( AV42PFFinal, "ZZ,ZZZ,ZZ9.999")));
                              }
                              else
                              {
                                 AV42PFFinal = context.localUtil.CToN( cgiGet( edtavPffinal_Internalname), ",", ".");
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPffinal_Internalname, StringUtil.LTrim( StringUtil.Str( AV42PFFinal, 14, 5)));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFFINAL"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( AV42PFFinal, "ZZ,ZZZ,ZZ9.999")));
                              }
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavQtdedmn_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavQtdedmn_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vQTDEDMN");
                                 GX_FocusControl = edtavQtdedmn_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV43QtdeDmn = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdedmn_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV43QtdeDmn), 8, 0)));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDEDMN"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( (decimal)(AV43QtdeDmn), "ZZZZZZZ9")));
                              }
                              else
                              {
                                 AV43QtdeDmn = (int)(context.localUtil.CToN( cgiGet( edtavQtdedmn_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdedmn_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV43QtdeDmn), 8, 0)));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDEDMN"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( (decimal)(AV43QtdeDmn), "ZZZZZZZ9")));
                              }
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavDiferenca_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavDiferenca_Internalname), ",", ".") > 99999999.99999m ) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vDIFERENCA");
                                 GX_FocusControl = edtavDiferenca_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV22Diferenca = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDiferenca_Internalname, StringUtil.LTrim( StringUtil.Str( AV22Diferenca, 14, 5)));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vDIFERENCA"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( AV22Diferenca, "ZZ,ZZZ,ZZ9.999")));
                              }
                              else
                              {
                                 AV22Diferenca = context.localUtil.CToN( cgiGet( edtavDiferenca_Internalname), ",", ".");
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDiferenca_Internalname, StringUtil.LTrim( StringUtil.Str( AV22Diferenca, 14, 5)));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vDIFERENCA"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( AV22Diferenca, "ZZ,ZZZ,ZZ9.999")));
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E12FU2 */
                                    E12FU2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13FU2 */
                                    E13FU2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VQTDEDMN.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14FU2 */
                                    E14FU2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEFU2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAFU2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavServico_codigo.Name = "vSERVICO_CODIGO";
            dynavServico_codigo.WebTags = "";
            cmbavFiltro_ano.Name = "vFILTRO_ANO";
            cmbavFiltro_ano.WebTags = "";
            cmbavFiltro_ano.addItem("2014", "2014", 0);
            cmbavFiltro_ano.addItem("2015", "2015", 0);
            if ( cmbavFiltro_ano.ItemCount > 0 )
            {
               AV23Filtro_Ano = (short)(NumberUtil.Val( cmbavFiltro_ano.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV23Filtro_Ano), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Filtro_Ano), 4, 0)));
            }
            cmbavFiltro_mes.Name = "vFILTRO_MES";
            cmbavFiltro_mes.WebTags = "";
            cmbavFiltro_mes.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 10, 0)), "Todos", 0);
            cmbavFiltro_mes.addItem("1", "Janeiro", 0);
            cmbavFiltro_mes.addItem("2", "Fevereiro", 0);
            cmbavFiltro_mes.addItem("3", "Mar�o", 0);
            cmbavFiltro_mes.addItem("4", "Abril", 0);
            cmbavFiltro_mes.addItem("5", "Maio", 0);
            cmbavFiltro_mes.addItem("6", "Junho", 0);
            cmbavFiltro_mes.addItem("7", "Julho", 0);
            cmbavFiltro_mes.addItem("8", "Agosto", 0);
            cmbavFiltro_mes.addItem("9", "Setembro", 0);
            cmbavFiltro_mes.addItem("10", "Outubro", 0);
            cmbavFiltro_mes.addItem("11", "Novembro", 0);
            cmbavFiltro_mes.addItem("12", "Dezembro", 0);
            if ( cmbavFiltro_mes.ItemCount > 0 )
            {
               AV26Filtro_Mes = (long)(NumberUtil.Val( cmbavFiltro_mes.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26Filtro_Mes), 10, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Filtro_Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26Filtro_Mes), 10, 0)));
            }
            GXCCtl = "vMES_" + sGXsfl_20_idx;
            cmbavMes.Name = GXCCtl;
            cmbavMes.WebTags = "";
            cmbavMes.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 10, 0)), "", 0);
            cmbavMes.addItem("1", "Janeiro", 0);
            cmbavMes.addItem("2", "Fevereiro", 0);
            cmbavMes.addItem("3", "Mar�o", 0);
            cmbavMes.addItem("4", "Abril", 0);
            cmbavMes.addItem("5", "Maio", 0);
            cmbavMes.addItem("6", "Junho", 0);
            cmbavMes.addItem("7", "Julho", 0);
            cmbavMes.addItem("8", "Agosto", 0);
            cmbavMes.addItem("9", "Setembro", 0);
            cmbavMes.addItem("10", "Outubro", 0);
            cmbavMes.addItem("11", "Novembro", 0);
            cmbavMes.addItem("12", "Dezembro", 0);
            if ( cmbavMes.ItemCount > 0 )
            {
               AV32Mes = (long)(NumberUtil.Val( cmbavMes.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV32Mes), 10, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavMes_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV32Mes), 10, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMES"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( (decimal)(AV32Mes), "ZZZZZZZZZ9")));
            }
            GXCCtl = "vANO_" + sGXsfl_20_idx;
            cmbavAno.Name = GXCCtl;
            cmbavAno.WebTags = "";
            cmbavAno.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "", 0);
            cmbavAno.addItem("2014", "2014", 0);
            cmbavAno.addItem("2015", "2015", 0);
            if ( cmbavAno.ItemCount > 0 )
            {
               AV8Ano = (short)(NumberUtil.Val( cmbavAno.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV8Ano), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavAno_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Ano), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vANO"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( (decimal)(AV8Ano), "ZZZ9")));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavFiltro_contadorfmcod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvSERVICO_CODIGOFU2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSERVICO_CODIGO_dataFU2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSERVICO_CODIGO_htmlFU2( )
      {
         int gxdynajaxvalue ;
         GXDLVvSERVICO_CODIGO_dataFU2( ) ;
         gxdynajaxindex = 1;
         dynavServico_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavServico_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavServico_codigo.ItemCount > 0 )
         {
            AV58Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV58Servico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58Servico_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvSERVICO_CODIGO_dataFU2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Todos");
         /* Using cursor H00FU2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00FU2_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00FU2_A605Servico_Sigla[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void gxnrGrid1_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_202( ) ;
         while ( nGXsfl_20_idx <= nRC_GXsfl_20 )
         {
            sendrow_202( ) ;
            nGXsfl_20_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_20_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_20_idx+1));
            sGXsfl_20_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_20_idx), 4, 0)), 4, "0");
            SubsflControlProps_202( ) ;
         }
         context.GX_webresponse.AddString(Grid1Container.ToJavascriptSource());
         /* End function gxnrGrid1_newrow */
      }

      protected void gxgrGrid1_refresh( short AV23Filtro_Ano ,
                                        int A52Contratada_AreaTrabalhoCod ,
                                        int A584ContagemResultado_ContadorFM ,
                                        wwpbaseobjects.SdtWWPContext AV57WWPContext ,
                                        int AV24Filtro_ContadorFMCod ,
                                        DateTime A566ContagemResultado_DataUltCnt ,
                                        long AV26Filtro_Mes ,
                                        int A601ContagemResultado_Servico ,
                                        int AV58Servico_Codigo ,
                                        decimal A684ContagemResultado_PFBFSUltima ,
                                        String A484ContagemResultado_StatusDmn ,
                                        int AV55tQtdeDmn ,
                                        decimal AV53tPFBFS ,
                                        decimal AV54tPFFinal ,
                                        decimal AV50tDiferenca ,
                                        int A1Usuario_Codigo ,
                                        int AV11ContagemResultado_ContadorFMCod ,
                                        String A58Usuario_PessoaNom ,
                                        int A456ContagemResultado_Codigo ,
                                        DateTime A473ContagemResultado_DataCnt ,
                                        String A511ContagemResultado_HoraCnt ,
                                        decimal A458ContagemResultado_PFBFS ,
                                        decimal A682ContagemResultado_PFBFMUltima )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID1_nCurrentRecord = 0;
         RFFU2( ) ;
         /* End function gxgrGrid1_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_CONTADORFMCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11ContagemResultado_ContadorFMCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_CONTADORFMCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11ContagemResultado_ContadorFMCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_CONTADORFMNOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV12ContagemResultado_ContadorFMNom, "@!"))));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_CONTADORFMNOM", StringUtil.RTrim( AV12ContagemResultado_ContadorFMNom));
         GxWebStd.gx_hidden_field( context, "gxhash_vMES", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV32Mes), "ZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "vMES", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32Mes), 10, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vANO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8Ano), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "vANO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8Ano), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFBFS", GetSecureSignedToken( "", context.localUtil.Format( AV41PFBFS, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "vPFBFS", StringUtil.LTrim( StringUtil.NToC( AV41PFBFS, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFFINAL", GetSecureSignedToken( "", context.localUtil.Format( AV42PFFinal, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "vPFFINAL", StringUtil.LTrim( StringUtil.NToC( AV42PFFinal, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vQTDEDMN", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV43QtdeDmn), "ZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "vQTDEDMN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43QtdeDmn), 8, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vDIFERENCA", GetSecureSignedToken( "", context.localUtil.Format( AV22Diferenca, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "vDIFERENCA", StringUtil.LTrim( StringUtil.NToC( AV22Diferenca, 14, 5, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavServico_codigo.ItemCount > 0 )
         {
            AV58Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV58Servico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58Servico_Codigo), 6, 0)));
         }
         if ( cmbavFiltro_ano.ItemCount > 0 )
         {
            AV23Filtro_Ano = (short)(NumberUtil.Val( cmbavFiltro_ano.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV23Filtro_Ano), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Filtro_Ano), 4, 0)));
         }
         if ( cmbavFiltro_mes.ItemCount > 0 )
         {
            AV26Filtro_Mes = (long)(NumberUtil.Val( cmbavFiltro_mes.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26Filtro_Mes), 10, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Filtro_Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26Filtro_Mes), 10, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFFU2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavContagemresultado_contadorfmcod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_contadorfmcod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_contadorfmcod_Enabled), 5, 0)));
         edtavContagemresultado_contadorfmnom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_contadorfmnom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_contadorfmnom_Enabled), 5, 0)));
         cmbavMes.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavMes_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavMes.Enabled), 5, 0)));
         cmbavAno.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAno_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavAno.Enabled), 5, 0)));
         edtavPfbfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfs_Enabled), 5, 0)));
         edtavPffinal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPffinal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPffinal_Enabled), 5, 0)));
         edtavQtdedmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdedmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdedmn_Enabled), 5, 0)));
         edtavDiferenca_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDiferenca_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDiferenca_Enabled), 5, 0)));
         edtavTpfbfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTpfbfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTpfbfs_Enabled), 5, 0)));
         edtavTpffinal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTpffinal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTpffinal_Enabled), 5, 0)));
         edtavTqtdedmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTqtdedmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTqtdedmn_Enabled), 5, 0)));
         edtavTdiferenca_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTdiferenca_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTdiferenca_Enabled), 5, 0)));
      }

      protected void RFFU2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            Grid1Container.ClearRows();
         }
         wbStart = 20;
         /* Execute user event: E11FU2 */
         E11FU2 ();
         nGXsfl_20_idx = 1;
         sGXsfl_20_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_20_idx), 4, 0)), 4, "0");
         SubsflControlProps_202( ) ;
         nGXsfl_20_Refreshing = 1;
         Grid1Container.AddObjectProperty("GridName", "Grid1");
         Grid1Container.AddObjectProperty("CmpContext", "");
         Grid1Container.AddObjectProperty("InMasterPage", "false");
         Grid1Container.AddObjectProperty("Class", "WorkWith");
         Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
         Grid1Container.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Titleforecolor), 9, 0, ".", "")));
         Grid1Container.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Sortable), 1, 0, ".", "")));
         Grid1Container.PageSize = subGrid1_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_202( ) ;
            /* Execute user event: E13FU2 */
            E13FU2 ();
            wbEnd = 20;
            WBFU0( ) ;
         }
         nGXsfl_20_Refreshing = 0;
      }

      protected int subGrid1_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPFU0( )
      {
         /* Before Start, stand alone formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavContagemresultado_contadorfmcod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_contadorfmcod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_contadorfmcod_Enabled), 5, 0)));
         edtavContagemresultado_contadorfmnom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_contadorfmnom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_contadorfmnom_Enabled), 5, 0)));
         cmbavMes.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavMes_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavMes.Enabled), 5, 0)));
         cmbavAno.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAno_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavAno.Enabled), 5, 0)));
         edtavPfbfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPfbfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfs_Enabled), 5, 0)));
         edtavPffinal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPffinal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPffinal_Enabled), 5, 0)));
         edtavQtdedmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdedmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtdedmn_Enabled), 5, 0)));
         edtavDiferenca_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDiferenca_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDiferenca_Enabled), 5, 0)));
         edtavTpfbfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTpfbfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTpfbfs_Enabled), 5, 0)));
         edtavTpffinal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTpffinal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTpffinal_Enabled), 5, 0)));
         edtavTqtdedmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTqtdedmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTqtdedmn_Enabled), 5, 0)));
         edtavTdiferenca_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTdiferenca_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTdiferenca_Enabled), 5, 0)));
         GXVvSERVICO_CODIGO_htmlFU2( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12FU2 */
         E12FU2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavFiltro_contadorfmcod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavFiltro_contadorfmcod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vFILTRO_CONTADORFMCOD");
               GX_FocusControl = edtavFiltro_contadorfmcod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24Filtro_ContadorFMCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Filtro_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Filtro_ContadorFMCod), 6, 0)));
            }
            else
            {
               AV24Filtro_ContadorFMCod = (int)(context.localUtil.CToN( cgiGet( edtavFiltro_contadorfmcod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Filtro_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Filtro_ContadorFMCod), 6, 0)));
            }
            dynavServico_codigo.Name = dynavServico_codigo_Internalname;
            dynavServico_codigo.CurrentValue = cgiGet( dynavServico_codigo_Internalname);
            AV58Servico_Codigo = (int)(NumberUtil.Val( cgiGet( dynavServico_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58Servico_Codigo), 6, 0)));
            cmbavFiltro_ano.Name = cmbavFiltro_ano_Internalname;
            cmbavFiltro_ano.CurrentValue = cgiGet( cmbavFiltro_ano_Internalname);
            AV23Filtro_Ano = (short)(NumberUtil.Val( cgiGet( cmbavFiltro_ano_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Filtro_Ano), 4, 0)));
            cmbavFiltro_mes.Name = cmbavFiltro_mes_Internalname;
            cmbavFiltro_mes.CurrentValue = cgiGet( cmbavFiltro_mes_Internalname);
            AV26Filtro_Mes = (long)(NumberUtil.Val( cgiGet( cmbavFiltro_mes_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Filtro_Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26Filtro_Mes), 10, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTpfbfs_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTpfbfs_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTPFBFS");
               GX_FocusControl = edtavTpfbfs_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV53tPFBFS = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53tPFBFS", StringUtil.LTrim( StringUtil.Str( AV53tPFBFS, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTPFBFS", GetSecureSignedToken( "", context.localUtil.Format( AV53tPFBFS, "ZZ,ZZZ,ZZ9.999")));
            }
            else
            {
               AV53tPFBFS = context.localUtil.CToN( cgiGet( edtavTpfbfs_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53tPFBFS", StringUtil.LTrim( StringUtil.Str( AV53tPFBFS, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTPFBFS", GetSecureSignedToken( "", context.localUtil.Format( AV53tPFBFS, "ZZ,ZZZ,ZZ9.999")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTpffinal_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTpffinal_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTPFFINAL");
               GX_FocusControl = edtavTpffinal_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV54tPFFinal = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54tPFFinal", StringUtil.LTrim( StringUtil.Str( AV54tPFFinal, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTPFFINAL", GetSecureSignedToken( "", context.localUtil.Format( AV54tPFFinal, "ZZ,ZZZ,ZZ9.999")));
            }
            else
            {
               AV54tPFFinal = context.localUtil.CToN( cgiGet( edtavTpffinal_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54tPFFinal", StringUtil.LTrim( StringUtil.Str( AV54tPFFinal, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTPFFINAL", GetSecureSignedToken( "", context.localUtil.Format( AV54tPFFinal, "ZZ,ZZZ,ZZ9.999")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTqtdedmn_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTqtdedmn_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTQTDEDMN");
               GX_FocusControl = edtavTqtdedmn_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV55tQtdeDmn = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55tQtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55tQtdeDmn), 8, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTQTDEDMN", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV55tQtdeDmn), "ZZZZZZZ9")));
            }
            else
            {
               AV55tQtdeDmn = (int)(context.localUtil.CToN( cgiGet( edtavTqtdedmn_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55tQtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55tQtdeDmn), 8, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTQTDEDMN", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV55tQtdeDmn), "ZZZZZZZ9")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTdiferenca_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTdiferenca_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTDIFERENCA");
               GX_FocusControl = edtavTdiferenca_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV50tDiferenca = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50tDiferenca", StringUtil.LTrim( StringUtil.Str( AV50tDiferenca, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTDIFERENCA", GetSecureSignedToken( "", context.localUtil.Format( AV50tDiferenca, "ZZ,ZZZ,ZZ9.999")));
            }
            else
            {
               AV50tDiferenca = context.localUtil.CToN( cgiGet( edtavTdiferenca_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50tDiferenca", StringUtil.LTrim( StringUtil.Str( AV50tDiferenca, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTDIFERENCA", GetSecureSignedToken( "", context.localUtil.Format( AV50tDiferenca, "ZZ,ZZZ,ZZ9.999")));
            }
            /* Read saved values. */
            nRC_GXsfl_20 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_20"), ",", "."));
            Innewwindow_Target = cgiGet( "INNEWWINDOW_Target");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "WP_ConsultaDiferencaPF";
            AV53tPFBFS = context.localUtil.CToN( cgiGet( edtavTpfbfs_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53tPFBFS", StringUtil.LTrim( StringUtil.Str( AV53tPFBFS, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTPFBFS", GetSecureSignedToken( "", context.localUtil.Format( AV53tPFBFS, "ZZ,ZZZ,ZZ9.999")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV53tPFBFS, "ZZ,ZZZ,ZZ9.999");
            AV54tPFFinal = context.localUtil.CToN( cgiGet( edtavTpffinal_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54tPFFinal", StringUtil.LTrim( StringUtil.Str( AV54tPFFinal, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTPFFINAL", GetSecureSignedToken( "", context.localUtil.Format( AV54tPFFinal, "ZZ,ZZZ,ZZ9.999")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV54tPFFinal, "ZZ,ZZZ,ZZ9.999");
            AV55tQtdeDmn = (int)(context.localUtil.CToN( cgiGet( edtavTqtdedmn_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55tQtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55tQtdeDmn), 8, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTQTDEDMN", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV55tQtdeDmn), "ZZZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV55tQtdeDmn), "ZZZZZZZ9");
            AV50tDiferenca = context.localUtil.CToN( cgiGet( edtavTdiferenca_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50tDiferenca", StringUtil.LTrim( StringUtil.Str( AV50tDiferenca, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTDIFERENCA", GetSecureSignedToken( "", context.localUtil.Format( AV50tDiferenca, "ZZ,ZZZ,ZZ9.999")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV50tDiferenca, "ZZ,ZZZ,ZZ9.999");
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("wp_consultadiferencapf:[SecurityCheckFailed value for]"+"tPFBFS:"+context.localUtil.Format( AV53tPFBFS, "ZZ,ZZZ,ZZ9.999"));
               GXUtil.WriteLog("wp_consultadiferencapf:[SecurityCheckFailed value for]"+"tPFFinal:"+context.localUtil.Format( AV54tPFFinal, "ZZ,ZZZ,ZZ9.999"));
               GXUtil.WriteLog("wp_consultadiferencapf:[SecurityCheckFailed value for]"+"tQtdeDmn:"+context.localUtil.Format( (decimal)(AV55tQtdeDmn), "ZZZZZZZ9"));
               GXUtil.WriteLog("wp_consultadiferencapf:[SecurityCheckFailed value for]"+"tDiferenca:"+context.localUtil.Format( AV50tDiferenca, "ZZ,ZZZ,ZZ9.999"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E12FU2 */
         E12FU2 ();
         if (returnInSub) return;
      }

      protected void E12FU2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV57WWPContext) ;
         AV23Filtro_Ano = (short)(DateTimeUtil.Year( Gx_date));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Filtro_Ano), 4, 0)));
         AV26Filtro_Mes = DateTimeUtil.Month( Gx_date);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Filtro_Mes", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26Filtro_Mes), 10, 0)));
         if ( DateTimeUtil.Year( Gx_date) > 2015 )
         {
            AV27i = 2016;
            while ( AV27i <= DateTimeUtil.Year( Gx_date) )
            {
               cmbavFiltro_ano.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV27i), 4, 0)), StringUtil.Str( (decimal)(AV27i), 4, 0), 0);
               AV27i = (short)(AV27i+1);
            }
         }
         edtavQtdedmn_Tooltiptext = "Visualizar demandas";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtdedmn_Internalname, "Tooltiptext", edtavQtdedmn_Tooltiptext);
      }

      protected void E11FU2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV8Ano = AV23Filtro_Ano;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavAno_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Ano), 4, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vANO"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( (decimal)(AV8Ano), "ZZZ9")));
         AV55tQtdeDmn = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55tQtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55tQtdeDmn), 8, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTQTDEDMN", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV55tQtdeDmn), "ZZZZZZZ9")));
         AV53tPFBFS = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53tPFBFS", StringUtil.LTrim( StringUtil.Str( AV53tPFBFS, 14, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTPFBFS", GetSecureSignedToken( "", context.localUtil.Format( AV53tPFBFS, "ZZ,ZZZ,ZZ9.999")));
         AV52tPFBFM = 0;
         AV54tPFFinal = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54tPFFinal", StringUtil.LTrim( StringUtil.Str( AV54tPFFinal, 14, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTPFFINAL", GetSecureSignedToken( "", context.localUtil.Format( AV54tPFFinal, "ZZ,ZZZ,ZZ9.999")));
         AV50tDiferenca = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50tDiferenca", StringUtil.LTrim( StringUtil.Str( AV50tDiferenca, 14, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTDIFERENCA", GetSecureSignedToken( "", context.localUtil.Format( AV50tDiferenca, "ZZ,ZZZ,ZZ9.999")));
         cmbavAno.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV8Ano), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAno_Internalname, "Values", cmbavAno.ToJavascriptSource());
      }

      private void E13FU2( )
      {
         /* Load Routine */
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV58Servico_Codigo ,
                                              A601ContagemResultado_Servico ,
                                              AV24Filtro_ContadorFMCod ,
                                              A584ContagemResultado_ContadorFM ,
                                              A566ContagemResultado_DataUltCnt ,
                                              AV23Filtro_Ano ,
                                              AV26Filtro_Mes ,
                                              A684ContagemResultado_PFBFSUltima ,
                                              A484ContagemResultado_StatusDmn ,
                                              AV57WWPContext.gxTpr_Areatrabalho_codigo ,
                                              A52Contratada_AreaTrabalhoCod },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.LONG, TypeConstants.DECIMAL,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00FU5 */
         pr_default.execute(1, new Object[] {AV57WWPContext.gxTpr_Areatrabalho_codigo, AV24Filtro_ContadorFMCod, AV24Filtro_ContadorFMCod, AV23Filtro_Ano, AV26Filtro_Mes, AV26Filtro_Mes, AV58Servico_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKFU3 = false;
            A146Modulo_Codigo = H00FU5_A146Modulo_Codigo[0];
            n146Modulo_Codigo = H00FU5_n146Modulo_Codigo[0];
            A127Sistema_Codigo = H00FU5_A127Sistema_Codigo[0];
            A135Sistema_AreaTrabalhoCod = H00FU5_A135Sistema_AreaTrabalhoCod[0];
            A830AreaTrabalho_ServicoPadrao = H00FU5_A830AreaTrabalho_ServicoPadrao[0];
            n830AreaTrabalho_ServicoPadrao = H00FU5_n830AreaTrabalho_ServicoPadrao[0];
            A490ContagemResultado_ContratadaCod = H00FU5_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00FU5_n490ContagemResultado_ContratadaCod[0];
            A1553ContagemResultado_CntSrvCod = H00FU5_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00FU5_n1553ContagemResultado_CntSrvCod[0];
            A456ContagemResultado_Codigo = H00FU5_A456ContagemResultado_Codigo[0];
            A52Contratada_AreaTrabalhoCod = H00FU5_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00FU5_n52Contratada_AreaTrabalhoCod[0];
            A632Servico_Ativo = H00FU5_A632Servico_Ativo[0];
            n632Servico_Ativo = H00FU5_n632Servico_Ativo[0];
            A484ContagemResultado_StatusDmn = H00FU5_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00FU5_n484ContagemResultado_StatusDmn[0];
            A601ContagemResultado_Servico = H00FU5_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00FU5_n601ContagemResultado_Servico[0];
            A584ContagemResultado_ContadorFM = H00FU5_A584ContagemResultado_ContadorFM[0];
            n584ContagemResultado_ContadorFM = H00FU5_n584ContagemResultado_ContadorFM[0];
            A684ContagemResultado_PFBFSUltima = H00FU5_A684ContagemResultado_PFBFSUltima[0];
            A566ContagemResultado_DataUltCnt = H00FU5_A566ContagemResultado_DataUltCnt[0];
            A127Sistema_Codigo = H00FU5_A127Sistema_Codigo[0];
            A135Sistema_AreaTrabalhoCod = H00FU5_A135Sistema_AreaTrabalhoCod[0];
            A830AreaTrabalho_ServicoPadrao = H00FU5_A830AreaTrabalho_ServicoPadrao[0];
            n830AreaTrabalho_ServicoPadrao = H00FU5_n830AreaTrabalho_ServicoPadrao[0];
            A632Servico_Ativo = H00FU5_A632Servico_Ativo[0];
            n632Servico_Ativo = H00FU5_n632Servico_Ativo[0];
            A52Contratada_AreaTrabalhoCod = H00FU5_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00FU5_n52Contratada_AreaTrabalhoCod[0];
            A601ContagemResultado_Servico = H00FU5_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00FU5_n601ContagemResultado_Servico[0];
            A584ContagemResultado_ContadorFM = H00FU5_A584ContagemResultado_ContadorFM[0];
            n584ContagemResultado_ContadorFM = H00FU5_n584ContagemResultado_ContadorFM[0];
            A684ContagemResultado_PFBFSUltima = H00FU5_A684ContagemResultado_PFBFSUltima[0];
            A566ContagemResultado_DataUltCnt = H00FU5_A566ContagemResultado_DataUltCnt[0];
            AV11ContagemResultado_ContadorFMCod = A584ContagemResultado_ContadorFM;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_contadorfmcod_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV11ContagemResultado_ContadorFMCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CONTADORFMCOD"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( (decimal)(AV11ContagemResultado_ContadorFMCod), "ZZZZZ9")));
            /* Execute user subroutine: 'NOMEDOCONTADORFM' */
            S113 ();
            if ( returnInSub )
            {
               pr_default.close(1);
               returnInSub = true;
               if (true) return;
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV37mQtdeDmn[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV35mPFBFS[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV36mPFFinal[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV31mDiferenca[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            while ( (pr_default.getStatus(1) != 101) && ( H00FU5_A52Contratada_AreaTrabalhoCod[0] == A52Contratada_AreaTrabalhoCod ) && ( H00FU5_A584ContagemResultado_ContadorFM[0] == A584ContagemResultado_ContadorFM ) )
            {
               BRKFU3 = false;
               A146Modulo_Codigo = H00FU5_A146Modulo_Codigo[0];
               n146Modulo_Codigo = H00FU5_n146Modulo_Codigo[0];
               A490ContagemResultado_ContratadaCod = H00FU5_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00FU5_n490ContagemResultado_ContratadaCod[0];
               A1553ContagemResultado_CntSrvCod = H00FU5_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00FU5_n1553ContagemResultado_CntSrvCod[0];
               A456ContagemResultado_Codigo = H00FU5_A456ContagemResultado_Codigo[0];
               A484ContagemResultado_StatusDmn = H00FU5_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = H00FU5_n484ContagemResultado_StatusDmn[0];
               AV27i = 0;
               BRKFU3 = true;
               pr_default.readNext(1);
            }
            /* Execute user subroutine: 'TOTAISDOCONTADOR' */
            S123 ();
            if ( returnInSub )
            {
               pr_default.close(1);
               returnInSub = true;
               if (true) return;
            }
            AV27i = 1;
            while ( AV27i <= 12 )
            {
               if ( AV37mQtdeDmn[AV27i-1] > 0 )
               {
                  AV32Mes = AV27i;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavMes_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV32Mes), 10, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMES"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( (decimal)(AV32Mes), "ZZZZZZZZZ9")));
                  AV43QtdeDmn = AV37mQtdeDmn[AV27i-1];
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQtdedmn_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV43QtdeDmn), 8, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vQTDEDMN"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( (decimal)(AV43QtdeDmn), "ZZZZZZZ9")));
                  AV41PFBFS = AV35mPFBFS[AV27i-1];
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPfbfs_Internalname, StringUtil.LTrim( StringUtil.Str( AV41PFBFS, 14, 5)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFBFS"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( AV41PFBFS, "ZZ,ZZZ,ZZ9.999")));
                  AV42PFFinal = AV36mPFFinal[AV27i-1];
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPffinal_Internalname, StringUtil.LTrim( StringUtil.Str( AV42PFFinal, 14, 5)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPFFINAL"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( AV42PFFinal, "ZZ,ZZZ,ZZ9.999")));
                  AV22Diferenca = AV31mDiferenca[AV27i-1];
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDiferenca_Internalname, StringUtil.LTrim( StringUtil.Str( AV22Diferenca, 14, 5)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vDIFERENCA"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( AV22Diferenca, "ZZ,ZZZ,ZZ9.999")));
                  sendrow_202( ) ;
                  if ( isFullAjaxMode( ) && ( nGXsfl_20_Refreshing == 0 ) )
                  {
                     context.DoAjaxLoad(20, Grid1Row);
                  }
                  AV12ContagemResultado_ContadorFMNom = "";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_contadorfmnom_Internalname, AV12ContagemResultado_ContadorFMNom);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CONTADORFMNOM"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, StringUtil.RTrim( context.localUtil.Format( AV12ContagemResultado_ContadorFMNom, "@!"))));
                  AV55tQtdeDmn = (int)(AV55tQtdeDmn+(AV37mQtdeDmn[AV27i-1]));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55tQtdeDmn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55tQtdeDmn), 8, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTQTDEDMN", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV55tQtdeDmn), "ZZZZZZZ9")));
                  AV53tPFBFS = (decimal)(AV53tPFBFS+(AV35mPFBFS[AV27i-1]));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53tPFBFS", StringUtil.LTrim( StringUtil.Str( AV53tPFBFS, 14, 5)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTPFBFS", GetSecureSignedToken( "", context.localUtil.Format( AV53tPFBFS, "ZZ,ZZZ,ZZ9.999")));
                  AV54tPFFinal = (decimal)(AV54tPFFinal+(AV36mPFFinal[AV27i-1]));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54tPFFinal", StringUtil.LTrim( StringUtil.Str( AV54tPFFinal, 14, 5)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTPFFINAL", GetSecureSignedToken( "", context.localUtil.Format( AV54tPFFinal, "ZZ,ZZZ,ZZ9.999")));
                  AV50tDiferenca = (decimal)(AV50tDiferenca+(AV31mDiferenca[AV27i-1]));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50tDiferenca", StringUtil.LTrim( StringUtil.Str( AV50tDiferenca, 14, 5)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTDIFERENCA", GetSecureSignedToken( "", context.localUtil.Format( AV50tDiferenca, "ZZ,ZZZ,ZZ9.999")));
               }
               AV27i = (short)(AV27i+1);
            }
            if ( ! BRKFU3 )
            {
               BRKFU3 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void E14FU2( )
      {
         /* Qtdedmn_Click Routine */
         AV46SDT_FiltroConsContadorFM.gxTpr_Areatrabalho_codigo = AV57WWPContext.gxTpr_Areatrabalho_codigo;
         AV46SDT_FiltroConsContadorFM.gxTpr_Contagemresultado_contadorfmcod = AV11ContagemResultado_ContadorFMCod;
         AV46SDT_FiltroConsContadorFM.gxTpr_Mes = AV32Mes;
         AV46SDT_FiltroConsContadorFM.gxTpr_Ano = AV23Filtro_Ano;
         AV46SDT_FiltroConsContadorFM.gxTpr_Comerro = false;
         AV46SDT_FiltroConsContadorFM.gxTpr_Abertas = false;
         AV46SDT_FiltroConsContadorFM.gxTpr_Solicitadas = false;
         AV46SDT_FiltroConsContadorFM.gxTpr_Soconfirmadas = true;
         AV46SDT_FiltroConsContadorFM.gxTpr_Servico = AV58Servico_Codigo;
         AV46SDT_FiltroConsContadorFM.gxTpr_Nome = AV12ContagemResultado_ContadorFMNom;
         AV6WebSession.Set("FiltroConsultaContador", AV46SDT_FiltroConsContadorFM.ToXml(false, true, "SDT_FiltroConsContadorFM", "GxEv3Up14_MeetrikaVs3"));
         Innewwindow_Target = formatLink("wwcontagemresultado.aspx") ;
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Innewwindow_Internalname, "Target", Innewwindow_Target);
         this.executeUsercontrolMethod("", false, "INNEWWINDOWContainer", "OpenWindow", "", new Object[] {});
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV46SDT_FiltroConsContadorFM", AV46SDT_FiltroConsContadorFM);
      }

      protected void S113( )
      {
         /* 'NOMEDOCONTADORFM' Routine */
         /* Using cursor H00FU6 */
         pr_default.execute(2, new Object[] {AV11ContagemResultado_ContadorFMCod});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A57Usuario_PessoaCod = H00FU6_A57Usuario_PessoaCod[0];
            A1Usuario_Codigo = H00FU6_A1Usuario_Codigo[0];
            A58Usuario_PessoaNom = H00FU6_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = H00FU6_n58Usuario_PessoaNom[0];
            A58Usuario_PessoaNom = H00FU6_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = H00FU6_n58Usuario_PessoaNom[0];
            AV12ContagemResultado_ContadorFMNom = A58Usuario_PessoaNom;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContagemresultado_contadorfmnom_Internalname, AV12ContagemResultado_ContadorFMNom);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CONTADORFMNOM"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, StringUtil.RTrim( context.localUtil.Format( AV12ContagemResultado_ContadorFMNom, "@!"))));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
      }

      protected void S123( )
      {
         /* 'TOTAISDOCONTADOR' Routine */
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV58Servico_Codigo ,
                                              A601ContagemResultado_Servico ,
                                              A566ContagemResultado_DataUltCnt ,
                                              AV23Filtro_Ano ,
                                              AV26Filtro_Mes ,
                                              A684ContagemResultado_PFBFSUltima ,
                                              A484ContagemResultado_StatusDmn ,
                                              AV57WWPContext.gxTpr_Areatrabalho_codigo ,
                                              AV11ContagemResultado_ContadorFMCod ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              A584ContagemResultado_ContadorFM },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.LONG, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00FU9 */
         pr_default.execute(3, new Object[] {AV57WWPContext.gxTpr_Areatrabalho_codigo, AV11ContagemResultado_ContadorFMCod, AV23Filtro_Ano, AV26Filtro_Mes, AV26Filtro_Mes, AV58Servico_Codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A146Modulo_Codigo = H00FU9_A146Modulo_Codigo[0];
            n146Modulo_Codigo = H00FU9_n146Modulo_Codigo[0];
            A127Sistema_Codigo = H00FU9_A127Sistema_Codigo[0];
            A135Sistema_AreaTrabalhoCod = H00FU9_A135Sistema_AreaTrabalhoCod[0];
            A830AreaTrabalho_ServicoPadrao = H00FU9_A830AreaTrabalho_ServicoPadrao[0];
            n830AreaTrabalho_ServicoPadrao = H00FU9_n830AreaTrabalho_ServicoPadrao[0];
            A490ContagemResultado_ContratadaCod = H00FU9_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00FU9_n490ContagemResultado_ContratadaCod[0];
            A1553ContagemResultado_CntSrvCod = H00FU9_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00FU9_n1553ContagemResultado_CntSrvCod[0];
            A52Contratada_AreaTrabalhoCod = H00FU9_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00FU9_n52Contratada_AreaTrabalhoCod[0];
            A456ContagemResultado_Codigo = H00FU9_A456ContagemResultado_Codigo[0];
            A632Servico_Ativo = H00FU9_A632Servico_Ativo[0];
            n632Servico_Ativo = H00FU9_n632Servico_Ativo[0];
            A484ContagemResultado_StatusDmn = H00FU9_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00FU9_n484ContagemResultado_StatusDmn[0];
            A601ContagemResultado_Servico = H00FU9_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00FU9_n601ContagemResultado_Servico[0];
            A584ContagemResultado_ContadorFM = H00FU9_A584ContagemResultado_ContadorFM[0];
            n584ContagemResultado_ContadorFM = H00FU9_n584ContagemResultado_ContadorFM[0];
            A684ContagemResultado_PFBFSUltima = H00FU9_A684ContagemResultado_PFBFSUltima[0];
            A566ContagemResultado_DataUltCnt = H00FU9_A566ContagemResultado_DataUltCnt[0];
            A682ContagemResultado_PFBFMUltima = H00FU9_A682ContagemResultado_PFBFMUltima[0];
            A127Sistema_Codigo = H00FU9_A127Sistema_Codigo[0];
            A135Sistema_AreaTrabalhoCod = H00FU9_A135Sistema_AreaTrabalhoCod[0];
            A830AreaTrabalho_ServicoPadrao = H00FU9_A830AreaTrabalho_ServicoPadrao[0];
            n830AreaTrabalho_ServicoPadrao = H00FU9_n830AreaTrabalho_ServicoPadrao[0];
            A632Servico_Ativo = H00FU9_A632Servico_Ativo[0];
            n632Servico_Ativo = H00FU9_n632Servico_Ativo[0];
            A52Contratada_AreaTrabalhoCod = H00FU9_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00FU9_n52Contratada_AreaTrabalhoCod[0];
            A601ContagemResultado_Servico = H00FU9_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00FU9_n601ContagemResultado_Servico[0];
            A584ContagemResultado_ContadorFM = H00FU9_A584ContagemResultado_ContadorFM[0];
            n584ContagemResultado_ContadorFM = H00FU9_n584ContagemResultado_ContadorFM[0];
            A684ContagemResultado_PFBFSUltima = H00FU9_A684ContagemResultado_PFBFSUltima[0];
            A566ContagemResultado_DataUltCnt = H00FU9_A566ContagemResultado_DataUltCnt[0];
            A682ContagemResultado_PFBFMUltima = H00FU9_A682ContagemResultado_PFBFMUltima[0];
            /* Using cursor H00FU10 */
            pr_default.execute(4, new Object[] {A456ContagemResultado_Codigo});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A458ContagemResultado_PFBFS = H00FU10_A458ContagemResultado_PFBFS[0];
               n458ContagemResultado_PFBFS = H00FU10_n458ContagemResultado_PFBFS[0];
               A511ContagemResultado_HoraCnt = H00FU10_A511ContagemResultado_HoraCnt[0];
               A473ContagemResultado_DataCnt = H00FU10_A473ContagemResultado_DataCnt[0];
               AV60PFFSPrimeiraCnt = A458ContagemResultado_PFBFS;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(4);
            }
            pr_default.close(4);
            AV27i = (short)(DateTimeUtil.Month( A566ContagemResultado_DataUltCnt));
            AV37mQtdeDmn[AV27i-1] = (int)(AV37mQtdeDmn[AV27i-1]+1);
            AV35mPFBFS[AV27i-1] = (decimal)(AV35mPFBFS[AV27i-1]+AV60PFFSPrimeiraCnt);
            AV62Contagem_PFFinal = ((A684ContagemResultado_PFBFSUltima<A682ContagemResultado_PFBFMUltima) ? A684ContagemResultado_PFBFSUltima : A682ContagemResultado_PFBFMUltima);
            AV36mPFFinal[AV27i-1] = (decimal)(AV36mPFFinal[AV27i-1]+AV62Contagem_PFFinal);
            AV31mDiferenca[AV27i-1] = (decimal)(AV31mDiferenca[AV27i-1]+(AV60PFFSPrimeiraCnt-AV62Contagem_PFFinal));
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void wb_table1_2_FU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable4_Internalname, tblTable4_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:220px")+"\" class='GridHeaderCell'>") ;
            context.WriteHtmlText( "&nbsp;&nbsp; ") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemresultadotitle_Internalname, "Consulta diferen�a de PF", "", "", lblContagemresultadotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_ConsultaDiferencaPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            context.WriteHtmlText( "&nbsp;&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock4_Internalname, "Contador:", "", "", lblTextblock4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaDiferencaPF.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_20_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFiltro_contadorfmcod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24Filtro_ContadorFMCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV24Filtro_ContadorFMCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,10);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFiltro_contadorfmcod_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_WP_ConsultaDiferencaPF.htm");
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock6_Internalname, "Servi�o:", "", "", lblTextblock6_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaDiferencaPF.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'" + sGXsfl_20_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavServico_codigo, dynavServico_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV58Servico_Codigo), 6, 0)), 1, dynavServico_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,12);\"", "", true, "HLP_WP_ConsultaDiferencaPF.htm");
            dynavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV58Servico_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServico_codigo_Internalname, "Values", (String)(dynavServico_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "Ano:", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaDiferencaPF.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'" + sGXsfl_20_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFiltro_ano, cmbavFiltro_ano_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV23Filtro_Ano), 4, 0)), 1, cmbavFiltro_ano_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,14);\"", "", true, "HLP_WP_ConsultaDiferencaPF.htm");
            cmbavFiltro_ano.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV23Filtro_Ano), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFiltro_ano_Internalname, "Values", (String)(cmbavFiltro_ano.ToJavascriptSource()));
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Mes:", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaDiferencaPF.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'" + sGXsfl_20_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFiltro_mes, cmbavFiltro_mes_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV26Filtro_Mes), 10, 0)), 1, cmbavFiltro_mes_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,16);\"", "", true, "HLP_WP_ConsultaDiferencaPF.htm");
            cmbavFiltro_mes.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26Filtro_Mes), 10, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFiltro_mes_Internalname, "Values", (String)(cmbavFiltro_mes.ToJavascriptSource()));
            context.WriteHtmlText( "&nbsp;&nbsp; ") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton1_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(20), 2, 0)+","+"null"+");", "Atualizar", bttButton1_Jsonclick, 5, "Atualizar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EREFRESH."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ConsultaDiferencaPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            /*  Grid Control  */
            Grid1Container.SetWrapped(nGXWrapped);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"DivS\" data-gxgridid=\"20\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid1_Internalname, subGrid1_Internalname, "", "WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid1_Backcolorstyle == 0 )
               {
                  subGrid1_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid1_Class) > 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Title";
                  }
               }
               else
               {
                  subGrid1_Titlebackstyle = 1;
                  if ( subGrid1_Backcolorstyle == 1 )
                  {
                     subGrid1_Titlebackcolor = subGrid1_Allbackcolor;
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+"display:none;"+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Contador FM") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Contador") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Mes") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Ano") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "1� Contagem") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "PF Final") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Qtde Dmn") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Diferen�a") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               Grid1Container.AddObjectProperty("GridName", "Grid1");
            }
            else
            {
               Grid1Container.AddObjectProperty("GridName", "Grid1");
               Grid1Container.AddObjectProperty("Class", "WorkWith");
               Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Titleforecolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Sortable), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("CmpContext", "");
               Grid1Container.AddObjectProperty("InMasterPage", "false");
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11ContagemResultado_ContadorFMCod), 6, 0, ".", "")));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultado_contadorfmcod_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.RTrim( AV12ContagemResultado_ContadorFMNom));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContagemresultado_contadorfmnom_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32Mes), 10, 0, ".", "")));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavMes.Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8Ano), 4, 0, ".", "")));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavAno.Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV41PFBFS, 14, 5, ".", "")));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPfbfs_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV42PFFinal, 14, 5, ".", "")));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPffinal_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43QtdeDmn), 8, 0, ".", "")));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavQtdedmn_Enabled), 5, 0, ".", "")));
               Grid1Column.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavQtdedmn_Tooltiptext));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV22Diferenca, 14, 5, ".", "")));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDiferenca_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Container.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowselection), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Selectioncolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowhovering), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Hoveringcolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowcollapsing), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 20 )
         {
            wbEnd = 0;
            nRC_GXsfl_20 = (short)(nGXsfl_20_idx-1);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid1", Grid1Container);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData", Grid1Container.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData"+"V", Grid1Container.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Grid1ContainerData"+"V"+"\" value='"+Grid1Container.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"right\" colspan=\"3\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            wb_table2_31_FU2( true) ;
         }
         else
         {
            wb_table2_31_FU2( false) ;
         }
         return  ;
      }

      protected void wb_table2_31_FU2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_FU2e( true) ;
         }
         else
         {
            wb_table1_2_FU2e( false) ;
         }
      }

      protected void wb_table2_31_FU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable3_Internalname, tblTable3_Internalname, "", "Table", 0, "", "", 1, 10, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pflfstotal_Internalname, "1� Contagem: ", "", "", lblTextblockcontagemresultado_pflfstotal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaDiferencaPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'" + sGXsfl_20_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTpfbfs_Internalname, StringUtil.LTrim( StringUtil.NToC( AV53tPFBFS, 14, 5, ",", "")), ((edtavTpfbfs_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV53tPFBFS, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV53tPFBFS, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTpfbfs_Jsonclick, 0, "BootstrapAttribute", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, edtavTpfbfs_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_WP_ConsultaDiferencaPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pfbfmtotal2_Internalname, " - PF Final: ", "", "", lblTextblockcontagemresultado_pfbfmtotal2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaDiferencaPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_20_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTpffinal_Internalname, StringUtil.LTrim( StringUtil.NToC( AV54tPFFinal, 14, 5, ",", "")), ((edtavTpffinal_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV54tPFFinal, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV54tPFFinal, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTpffinal_Jsonclick, 0, "Attribute", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, edtavTpffinal_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_WP_ConsultaDiferencaPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pflfmtotal_Internalname, " - Dmn: ", "", "", lblTextblockcontagemresultado_pflfmtotal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaDiferencaPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_20_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTqtdedmn_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55tQtdeDmn), 8, 0, ",", "")), ((edtavTqtdedmn_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV55tQtdeDmn), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV55tQtdeDmn), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTqtdedmn_Jsonclick, 0, "BootstrapAttribute", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, edtavTqtdedmn_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ConsultaDiferencaPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pflfmtotal2_Internalname, " - Diferen�a: ", "", "", lblTextblockcontagemresultado_pflfmtotal2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:15.0pt; font-weight:bold; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ConsultaDiferencaPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_20_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTdiferenca_Internalname, StringUtil.LTrim( StringUtil.NToC( AV50tDiferenca, 14, 5, ",", "")), ((edtavTdiferenca_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV50tDiferenca, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV50tDiferenca, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,48);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTdiferenca_Jsonclick, 0, "Attribute", "font-family:'Trebuchet MS'; font-size:16.0pt; font-weight:bold; font-style:normal;", "", "", 1, edtavTdiferenca_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_WP_ConsultaDiferencaPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_31_FU2e( true) ;
         }
         else
         {
            wb_table2_31_FU2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAFU2( ) ;
         WSFU2( ) ;
         WEFU2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216233841");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_consultadiferencapf.js", "?20206216233841");
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_202( )
      {
         edtavContagemresultado_contadorfmcod_Internalname = "vCONTAGEMRESULTADO_CONTADORFMCOD_"+sGXsfl_20_idx;
         edtavContagemresultado_contadorfmnom_Internalname = "vCONTAGEMRESULTADO_CONTADORFMNOM_"+sGXsfl_20_idx;
         cmbavMes_Internalname = "vMES_"+sGXsfl_20_idx;
         cmbavAno_Internalname = "vANO_"+sGXsfl_20_idx;
         edtavPfbfs_Internalname = "vPFBFS_"+sGXsfl_20_idx;
         edtavPffinal_Internalname = "vPFFINAL_"+sGXsfl_20_idx;
         edtavQtdedmn_Internalname = "vQTDEDMN_"+sGXsfl_20_idx;
         edtavDiferenca_Internalname = "vDIFERENCA_"+sGXsfl_20_idx;
      }

      protected void SubsflControlProps_fel_202( )
      {
         edtavContagemresultado_contadorfmcod_Internalname = "vCONTAGEMRESULTADO_CONTADORFMCOD_"+sGXsfl_20_fel_idx;
         edtavContagemresultado_contadorfmnom_Internalname = "vCONTAGEMRESULTADO_CONTADORFMNOM_"+sGXsfl_20_fel_idx;
         cmbavMes_Internalname = "vMES_"+sGXsfl_20_fel_idx;
         cmbavAno_Internalname = "vANO_"+sGXsfl_20_fel_idx;
         edtavPfbfs_Internalname = "vPFBFS_"+sGXsfl_20_fel_idx;
         edtavPffinal_Internalname = "vPFFINAL_"+sGXsfl_20_fel_idx;
         edtavQtdedmn_Internalname = "vQTDEDMN_"+sGXsfl_20_fel_idx;
         edtavDiferenca_Internalname = "vDIFERENCA_"+sGXsfl_20_fel_idx;
      }

      protected void sendrow_202( )
      {
         SubsflControlProps_202( ) ;
         WBFU0( ) ;
         Grid1Row = GXWebRow.GetNew(context,Grid1Container);
         if ( subGrid1_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid1_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd";
            }
         }
         else if ( subGrid1_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid1_Backstyle = 0;
            subGrid1_Backcolor = subGrid1_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Uniform";
            }
         }
         else if ( subGrid1_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid1_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd";
            }
            subGrid1_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subGrid1_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid1_Backstyle = 1;
            if ( ((int)((nGXsfl_20_idx) % (2))) == 0 )
            {
               subGrid1_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Even";
               }
            }
            else
            {
               subGrid1_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd";
               }
            }
         }
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid1_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_20_idx+"\">") ;
         }
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavContagemresultado_contadorfmcod_Enabled!=0)&&(edtavContagemresultado_contadorfmcod_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 21,'',false,'"+sGXsfl_20_idx+"',20)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContagemresultado_contadorfmcod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11ContagemResultado_ContadorFMCod), 6, 0, ",", "")),((edtavContagemresultado_contadorfmcod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV11ContagemResultado_ContadorFMCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV11ContagemResultado_ContadorFMCod), "ZZZZZ9")),TempTags+((edtavContagemresultado_contadorfmcod_Enabled!=0)&&(edtavContagemresultado_contadorfmcod_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavContagemresultado_contadorfmcod_Enabled!=0)&&(edtavContagemresultado_contadorfmcod_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,21);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContagemresultado_contadorfmcod_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavContagemresultado_contadorfmcod_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)20,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavContagemresultado_contadorfmnom_Enabled!=0)&&(edtavContagemresultado_contadorfmnom_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 22,'',false,'"+sGXsfl_20_idx+"',20)\"" : " ");
         ROClassString = "BootstrapAttribute100";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContagemresultado_contadorfmnom_Internalname,StringUtil.RTrim( AV12ContagemResultado_ContadorFMNom),StringUtil.RTrim( context.localUtil.Format( AV12ContagemResultado_ContadorFMNom, "@!")),TempTags+((edtavContagemresultado_contadorfmnom_Enabled!=0)&&(edtavContagemresultado_contadorfmnom_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavContagemresultado_contadorfmnom_Enabled!=0)&&(edtavContagemresultado_contadorfmnom_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,22);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContagemresultado_contadorfmnom_Jsonclick,(short)0,(String)"BootstrapAttribute100",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavContagemresultado_contadorfmnom_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)20,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         TempTags = " " + ((cmbavMes.Enabled!=0)&&(cmbavMes.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 23,'',false,'"+sGXsfl_20_idx+"',20)\"" : " ");
         if ( ( nGXsfl_20_idx == 1 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "vMES_" + sGXsfl_20_idx;
            cmbavMes.Name = GXCCtl;
            cmbavMes.WebTags = "";
            cmbavMes.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 10, 0)), "", 0);
            cmbavMes.addItem("1", "Janeiro", 0);
            cmbavMes.addItem("2", "Fevereiro", 0);
            cmbavMes.addItem("3", "Mar�o", 0);
            cmbavMes.addItem("4", "Abril", 0);
            cmbavMes.addItem("5", "Maio", 0);
            cmbavMes.addItem("6", "Junho", 0);
            cmbavMes.addItem("7", "Julho", 0);
            cmbavMes.addItem("8", "Agosto", 0);
            cmbavMes.addItem("9", "Setembro", 0);
            cmbavMes.addItem("10", "Outubro", 0);
            cmbavMes.addItem("11", "Novembro", 0);
            cmbavMes.addItem("12", "Dezembro", 0);
            if ( cmbavMes.ItemCount > 0 )
            {
               AV32Mes = (long)(NumberUtil.Val( cmbavMes.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV32Mes), 10, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavMes_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV32Mes), 10, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMES"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( (decimal)(AV32Mes), "ZZZZZZZZZ9")));
            }
         }
         /* ComboBox */
         Grid1Row.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavMes,(String)cmbavMes_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(AV32Mes), 10, 0)),(short)1,(String)cmbavMes_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,cmbavMes.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",TempTags+((cmbavMes.Enabled!=0)&&(cmbavMes.Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((cmbavMes.Enabled!=0)&&(cmbavMes.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,23);\"" : " "),(String)"",(bool)true});
         cmbavMes.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV32Mes), 10, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavMes_Internalname, "Values", (String)(cmbavMes.ToJavascriptSource()));
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         TempTags = " " + ((cmbavAno.Enabled!=0)&&(cmbavAno.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 24,'',false,'"+sGXsfl_20_idx+"',20)\"" : " ");
         if ( ( nGXsfl_20_idx == 1 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "vANO_" + sGXsfl_20_idx;
            cmbavAno.Name = GXCCtl;
            cmbavAno.WebTags = "";
            cmbavAno.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "", 0);
            cmbavAno.addItem("2014", "2014", 0);
            cmbavAno.addItem("2015", "2015", 0);
            if ( cmbavAno.ItemCount > 0 )
            {
               AV8Ano = (short)(NumberUtil.Val( cmbavAno.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV8Ano), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavAno_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Ano), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vANO"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( (decimal)(AV8Ano), "ZZZ9")));
            }
         }
         /* ComboBox */
         Grid1Row.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavAno,(String)cmbavAno_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(AV8Ano), 4, 0)),(short)1,(String)cmbavAno_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,cmbavAno.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",TempTags+((cmbavAno.Enabled!=0)&&(cmbavAno.Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((cmbavAno.Enabled!=0)&&(cmbavAno.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,24);\"" : " "),(String)"",(bool)true});
         cmbavAno.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV8Ano), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAno_Internalname, "Values", (String)(cmbavAno.ToJavascriptSource()));
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavPfbfs_Enabled!=0)&&(edtavPfbfs_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 25,'',false,'"+sGXsfl_20_idx+"',20)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPfbfs_Internalname,StringUtil.LTrim( StringUtil.NToC( AV41PFBFS, 14, 5, ",", "")),((edtavPfbfs_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV41PFBFS, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV41PFBFS, "ZZ,ZZZ,ZZ9.999")),TempTags+((edtavPfbfs_Enabled!=0)&&(edtavPfbfs_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavPfbfs_Enabled!=0)&&(edtavPfbfs_Visible!=0) ? " onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,25);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPfbfs_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavPfbfs_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)20,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavPffinal_Enabled!=0)&&(edtavPffinal_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 26,'',false,'"+sGXsfl_20_idx+"',20)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPffinal_Internalname,StringUtil.LTrim( StringUtil.NToC( AV42PFFinal, 14, 5, ",", "")),((edtavPffinal_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV42PFFinal, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV42PFFinal, "ZZ,ZZZ,ZZ9.999")),TempTags+((edtavPffinal_Enabled!=0)&&(edtavPffinal_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavPffinal_Enabled!=0)&&(edtavPffinal_Visible!=0) ? " onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,26);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPffinal_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavPffinal_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)20,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavQtdedmn_Enabled!=0)&&(edtavQtdedmn_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 27,'',false,'"+sGXsfl_20_idx+"',20)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavQtdedmn_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43QtdeDmn), 8, 0, ",", "")),((edtavQtdedmn_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV43QtdeDmn), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV43QtdeDmn), "ZZZZZZZ9")),TempTags+((edtavQtdedmn_Enabled!=0)&&(edtavQtdedmn_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavQtdedmn_Enabled!=0)&&(edtavQtdedmn_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,27);\"" : " "),"'"+""+"'"+",false,"+"'"+"EVQTDEDMN.CLICK."+sGXsfl_20_idx+"'",(String)"",(String)"",(String)edtavQtdedmn_Tooltiptext,(String)"",(String)edtavQtdedmn_Jsonclick,(short)5,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavQtdedmn_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)20,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavDiferenca_Enabled!=0)&&(edtavDiferenca_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 28,'',false,'"+sGXsfl_20_idx+"',20)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavDiferenca_Internalname,StringUtil.LTrim( StringUtil.NToC( AV22Diferenca, 14, 5, ",", "")),((edtavDiferenca_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV22Diferenca, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV22Diferenca, "ZZ,ZZZ,ZZ9.999")),TempTags+((edtavDiferenca_Enabled!=0)&&(edtavDiferenca_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavDiferenca_Enabled!=0)&&(edtavDiferenca_Visible!=0) ? " onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,28);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavDiferenca_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavDiferenca_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)20,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_CONTADORFMCOD"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( (decimal)(AV11ContagemResultado_ContadorFMCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_CONTADORFMNOM"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, StringUtil.RTrim( context.localUtil.Format( AV12ContagemResultado_ContadorFMNom, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vMES"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( (decimal)(AV32Mes), "ZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vANO"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( (decimal)(AV8Ano), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFBFS"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( AV41PFBFS, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPFFINAL"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( AV42PFFinal, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vQTDEDMN"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( (decimal)(AV43QtdeDmn), "ZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vDIFERENCA"+"_"+sGXsfl_20_idx, GetSecureSignedToken( sGXsfl_20_idx, context.localUtil.Format( AV22Diferenca, "ZZ,ZZZ,ZZ9.999")));
         Grid1Container.AddRow(Grid1Row);
         nGXsfl_20_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_20_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_20_idx+1));
         sGXsfl_20_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_20_idx), 4, 0)), 4, "0");
         SubsflControlProps_202( ) ;
         /* End function sendrow_202 */
      }

      protected void init_default_properties( )
      {
         lblContagemresultadotitle_Internalname = "CONTAGEMRESULTADOTITLE";
         lblTextblock4_Internalname = "TEXTBLOCK4";
         edtavFiltro_contadorfmcod_Internalname = "vFILTRO_CONTADORFMCOD";
         lblTextblock6_Internalname = "TEXTBLOCK6";
         dynavServico_codigo_Internalname = "vSERVICO_CODIGO";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         cmbavFiltro_ano_Internalname = "vFILTRO_ANO";
         lblTextblock1_Internalname = "TEXTBLOCK1";
         cmbavFiltro_mes_Internalname = "vFILTRO_MES";
         bttButton1_Internalname = "BUTTON1";
         edtavContagemresultado_contadorfmcod_Internalname = "vCONTAGEMRESULTADO_CONTADORFMCOD";
         edtavContagemresultado_contadorfmnom_Internalname = "vCONTAGEMRESULTADO_CONTADORFMNOM";
         cmbavMes_Internalname = "vMES";
         cmbavAno_Internalname = "vANO";
         edtavPfbfs_Internalname = "vPFBFS";
         edtavPffinal_Internalname = "vPFFINAL";
         edtavQtdedmn_Internalname = "vQTDEDMN";
         edtavDiferenca_Internalname = "vDIFERENCA";
         lblTextblockcontagemresultado_pflfstotal_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFLFSTOTAL";
         edtavTpfbfs_Internalname = "vTPFBFS";
         lblTextblockcontagemresultado_pfbfmtotal2_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFBFMTOTAL2";
         edtavTpffinal_Internalname = "vTPFFINAL";
         lblTextblockcontagemresultado_pflfmtotal_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFLFMTOTAL";
         edtavTqtdedmn_Internalname = "vTQTDEDMN";
         lblTextblockcontagemresultado_pflfmtotal2_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFLFMTOTAL2";
         edtavTdiferenca_Internalname = "vTDIFERENCA";
         tblTable3_Internalname = "TABLE3";
         tblTable4_Internalname = "TABLE4";
         Innewwindow_Internalname = "INNEWWINDOW";
         Form.Internalname = "FORM";
         subGrid1_Internalname = "GRID1";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavDiferenca_Jsonclick = "";
         edtavDiferenca_Visible = -1;
         edtavQtdedmn_Jsonclick = "";
         edtavQtdedmn_Visible = -1;
         edtavPffinal_Jsonclick = "";
         edtavPffinal_Visible = -1;
         edtavPfbfs_Jsonclick = "";
         edtavPfbfs_Visible = -1;
         cmbavAno_Jsonclick = "";
         cmbavAno.Visible = -1;
         cmbavMes_Jsonclick = "";
         cmbavMes.Visible = -1;
         edtavContagemresultado_contadorfmnom_Jsonclick = "";
         edtavContagemresultado_contadorfmnom_Visible = -1;
         edtavContagemresultado_contadorfmcod_Jsonclick = "";
         edtavContagemresultado_contadorfmcod_Visible = 0;
         edtavTdiferenca_Jsonclick = "";
         edtavTdiferenca_Enabled = 1;
         edtavTqtdedmn_Jsonclick = "";
         edtavTqtdedmn_Enabled = 1;
         edtavTpffinal_Jsonclick = "";
         edtavTpffinal_Enabled = 1;
         edtavTpfbfs_Jsonclick = "";
         edtavTpfbfs_Enabled = 1;
         subGrid1_Allowcollapsing = 0;
         subGrid1_Allowselection = 0;
         edtavDiferenca_Enabled = 1;
         edtavQtdedmn_Enabled = 1;
         edtavPffinal_Enabled = 1;
         edtavPfbfs_Enabled = 1;
         cmbavAno.Enabled = 1;
         cmbavMes.Enabled = 1;
         edtavContagemresultado_contadorfmnom_Enabled = 1;
         edtavContagemresultado_contadorfmcod_Enabled = 1;
         subGrid1_Class = "WorkWith";
         cmbavFiltro_mes_Jsonclick = "";
         cmbavFiltro_ano_Jsonclick = "";
         dynavServico_codigo_Jsonclick = "";
         edtavFiltro_contadorfmcod_Jsonclick = "";
         subGrid1_Sortable = 0;
         subGrid1_Titleforecolor = (int)(0x000000);
         subGrid1_Backcolorstyle = 3;
         Innewwindow_Target = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Diferen�a de PF";
         edtavQtdedmn_Tooltiptext = "";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'edtavQtdedmn_Tooltiptext',ctrl:'vQTDEDMN',prop:'Tooltiptext'},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A584ContagemResultado_ContadorFM',fld:'CONTAGEMRESULTADO_CONTADORFM',pic:'ZZZZZ9',nv:0},{av:'AV57WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV24Filtro_ContadorFMCod',fld:'vFILTRO_CONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'A566ContagemResultado_DataUltCnt',fld:'CONTAGEMRESULTADO_DATAULTCNT',pic:'',nv:''},{av:'AV26Filtro_Mes',fld:'vFILTRO_MES',pic:'ZZZZZZZZZ9',nv:0},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV58Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV55tQtdeDmn',fld:'vTQTDEDMN',pic:'ZZZZZZZ9',hsh:true,nv:0},{av:'AV53tPFBFS',fld:'vTPFBFS',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV54tPFFinal',fld:'vTPFFINAL',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV50tDiferenca',fld:'vTDIFERENCA',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11ContagemResultado_ContadorFMCod',fld:'vCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A473ContagemResultado_DataCnt',fld:'CONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'A511ContagemResultado_HoraCnt',fld:'CONTAGEMRESULTADO_HORACNT',pic:'',nv:''},{av:'A458ContagemResultado_PFBFS',fld:'CONTAGEMRESULTADO_PFBFS',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV23Filtro_Ano',fld:'vFILTRO_ANO',pic:'ZZZ9',nv:0}],oparms:[{av:'AV8Ano',fld:'vANO',pic:'ZZZ9',hsh:true,nv:0},{av:'AV55tQtdeDmn',fld:'vTQTDEDMN',pic:'ZZZZZZZ9',hsh:true,nv:0},{av:'AV53tPFBFS',fld:'vTPFBFS',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV54tPFFinal',fld:'vTPFFINAL',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV50tDiferenca',fld:'vTDIFERENCA',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0}]}");
         setEventMetadata("VQTDEDMN.CLICK","{handler:'E14FU2',iparms:[{av:'AV57WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV46SDT_FiltroConsContadorFM',fld:'vSDT_FILTROCONSCONTADORFM',pic:'',nv:null},{av:'AV11ContagemResultado_ContadorFMCod',fld:'vCONTAGEMRESULTADO_CONTADORFMCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV32Mes',fld:'vMES',pic:'ZZZZZZZZZ9',hsh:true,nv:0},{av:'AV23Filtro_Ano',fld:'vFILTRO_ANO',pic:'ZZZ9',nv:0},{av:'AV58Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12ContagemResultado_ContadorFMNom',fld:'vCONTAGEMRESULTADO_CONTADORFMNOM',pic:'@!',hsh:true,nv:''}],oparms:[{av:'AV46SDT_FiltroConsContadorFM',fld:'vSDT_FILTROCONSCONTADORFM',pic:'',nv:null},{av:'Innewwindow_Target',ctrl:'INNEWWINDOW',prop:'Target'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV57WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         A484ContagemResultado_StatusDmn = "";
         A58Usuario_PessoaNom = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         GXKey = "";
         forbiddenHiddens = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV46SDT_FiltroConsContadorFM = new SdtSDT_FiltroConsContadorFM(context);
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV12ContagemResultado_ContadorFMNom = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00FU2_A155Servico_Codigo = new int[1] ;
         H00FU2_A605Servico_Sigla = new String[] {""} ;
         H00FU2_A631Servico_Vinculado = new int[1] ;
         H00FU2_n631Servico_Vinculado = new bool[] {false} ;
         H00FU2_A632Servico_Ativo = new bool[] {false} ;
         H00FU2_n632Servico_Ativo = new bool[] {false} ;
         Grid1Container = new GXWebGrid( context);
         Gx_date = DateTime.MinValue;
         hsh = "";
         H00FU5_A146Modulo_Codigo = new int[1] ;
         H00FU5_n146Modulo_Codigo = new bool[] {false} ;
         H00FU5_A127Sistema_Codigo = new int[1] ;
         H00FU5_A135Sistema_AreaTrabalhoCod = new int[1] ;
         H00FU5_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         H00FU5_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         H00FU5_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00FU5_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00FU5_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00FU5_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00FU5_A456ContagemResultado_Codigo = new int[1] ;
         H00FU5_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00FU5_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00FU5_A632Servico_Ativo = new bool[] {false} ;
         H00FU5_n632Servico_Ativo = new bool[] {false} ;
         H00FU5_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00FU5_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00FU5_A601ContagemResultado_Servico = new int[1] ;
         H00FU5_n601ContagemResultado_Servico = new bool[] {false} ;
         H00FU5_A584ContagemResultado_ContadorFM = new int[1] ;
         H00FU5_n584ContagemResultado_ContadorFM = new bool[] {false} ;
         H00FU5_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         H00FU5_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         AV37mQtdeDmn = new int [12] ;
         AV35mPFBFS = new decimal [12] ;
         AV36mPFFinal = new decimal [12] ;
         AV31mDiferenca = new decimal [12] ;
         Grid1Row = new GXWebRow();
         AV6WebSession = context.GetSession();
         H00FU6_A57Usuario_PessoaCod = new int[1] ;
         H00FU6_A1Usuario_Codigo = new int[1] ;
         H00FU6_A58Usuario_PessoaNom = new String[] {""} ;
         H00FU6_n58Usuario_PessoaNom = new bool[] {false} ;
         H00FU9_A146Modulo_Codigo = new int[1] ;
         H00FU9_n146Modulo_Codigo = new bool[] {false} ;
         H00FU9_A127Sistema_Codigo = new int[1] ;
         H00FU9_A135Sistema_AreaTrabalhoCod = new int[1] ;
         H00FU9_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         H00FU9_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         H00FU9_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00FU9_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00FU9_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00FU9_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00FU9_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00FU9_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00FU9_A456ContagemResultado_Codigo = new int[1] ;
         H00FU9_A632Servico_Ativo = new bool[] {false} ;
         H00FU9_n632Servico_Ativo = new bool[] {false} ;
         H00FU9_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00FU9_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00FU9_A601ContagemResultado_Servico = new int[1] ;
         H00FU9_n601ContagemResultado_Servico = new bool[] {false} ;
         H00FU9_A584ContagemResultado_ContadorFM = new int[1] ;
         H00FU9_n584ContagemResultado_ContadorFM = new bool[] {false} ;
         H00FU9_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         H00FU9_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         H00FU9_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         H00FU10_A456ContagemResultado_Codigo = new int[1] ;
         H00FU10_A458ContagemResultado_PFBFS = new decimal[1] ;
         H00FU10_n458ContagemResultado_PFBFS = new bool[] {false} ;
         H00FU10_A511ContagemResultado_HoraCnt = new String[] {""} ;
         H00FU10_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         sStyleString = "";
         lblContagemresultadotitle_Jsonclick = "";
         lblTextblock4_Jsonclick = "";
         TempTags = "";
         lblTextblock6_Jsonclick = "";
         lblTextblock3_Jsonclick = "";
         lblTextblock1_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         bttButton1_Jsonclick = "";
         subGrid1_Linesclass = "";
         Grid1Column = new GXWebColumn();
         lblTextblockcontagemresultado_pflfstotal_Jsonclick = "";
         lblTextblockcontagemresultado_pfbfmtotal2_Jsonclick = "";
         lblTextblockcontagemresultado_pflfmtotal_Jsonclick = "";
         lblTextblockcontagemresultado_pflfmtotal2_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_consultadiferencapf__default(),
            new Object[][] {
                new Object[] {
               H00FU2_A155Servico_Codigo, H00FU2_A605Servico_Sigla, H00FU2_A631Servico_Vinculado, H00FU2_n631Servico_Vinculado, H00FU2_A632Servico_Ativo
               }
               , new Object[] {
               H00FU5_A146Modulo_Codigo, H00FU5_n146Modulo_Codigo, H00FU5_A127Sistema_Codigo, H00FU5_A135Sistema_AreaTrabalhoCod, H00FU5_A830AreaTrabalho_ServicoPadrao, H00FU5_n830AreaTrabalho_ServicoPadrao, H00FU5_A490ContagemResultado_ContratadaCod, H00FU5_n490ContagemResultado_ContratadaCod, H00FU5_A1553ContagemResultado_CntSrvCod, H00FU5_n1553ContagemResultado_CntSrvCod,
               H00FU5_A456ContagemResultado_Codigo, H00FU5_A52Contratada_AreaTrabalhoCod, H00FU5_n52Contratada_AreaTrabalhoCod, H00FU5_A632Servico_Ativo, H00FU5_n632Servico_Ativo, H00FU5_A484ContagemResultado_StatusDmn, H00FU5_n484ContagemResultado_StatusDmn, H00FU5_A601ContagemResultado_Servico, H00FU5_n601ContagemResultado_Servico, H00FU5_A584ContagemResultado_ContadorFM,
               H00FU5_n584ContagemResultado_ContadorFM, H00FU5_A684ContagemResultado_PFBFSUltima, H00FU5_A566ContagemResultado_DataUltCnt
               }
               , new Object[] {
               H00FU6_A57Usuario_PessoaCod, H00FU6_A1Usuario_Codigo, H00FU6_A58Usuario_PessoaNom, H00FU6_n58Usuario_PessoaNom
               }
               , new Object[] {
               H00FU9_A146Modulo_Codigo, H00FU9_n146Modulo_Codigo, H00FU9_A127Sistema_Codigo, H00FU9_A135Sistema_AreaTrabalhoCod, H00FU9_A830AreaTrabalho_ServicoPadrao, H00FU9_n830AreaTrabalho_ServicoPadrao, H00FU9_A490ContagemResultado_ContratadaCod, H00FU9_n490ContagemResultado_ContratadaCod, H00FU9_A1553ContagemResultado_CntSrvCod, H00FU9_n1553ContagemResultado_CntSrvCod,
               H00FU9_A52Contratada_AreaTrabalhoCod, H00FU9_n52Contratada_AreaTrabalhoCod, H00FU9_A456ContagemResultado_Codigo, H00FU9_A632Servico_Ativo, H00FU9_n632Servico_Ativo, H00FU9_A484ContagemResultado_StatusDmn, H00FU9_n484ContagemResultado_StatusDmn, H00FU9_A601ContagemResultado_Servico, H00FU9_n601ContagemResultado_Servico, H00FU9_A584ContagemResultado_ContadorFM,
               H00FU9_n584ContagemResultado_ContadorFM, H00FU9_A684ContagemResultado_PFBFSUltima, H00FU9_A566ContagemResultado_DataUltCnt, H00FU9_A682ContagemResultado_PFBFMUltima
               }
               , new Object[] {
               H00FU10_A456ContagemResultado_Codigo, H00FU10_A458ContagemResultado_PFBFS, H00FU10_n458ContagemResultado_PFBFS, H00FU10_A511ContagemResultado_HoraCnt, H00FU10_A473ContagemResultado_DataCnt
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         edtavContagemresultado_contadorfmcod_Enabled = 0;
         edtavContagemresultado_contadorfmnom_Enabled = 0;
         cmbavMes.Enabled = 0;
         cmbavAno.Enabled = 0;
         edtavPfbfs_Enabled = 0;
         edtavPffinal_Enabled = 0;
         edtavQtdedmn_Enabled = 0;
         edtavDiferenca_Enabled = 0;
         edtavTpfbfs_Enabled = 0;
         edtavTpffinal_Enabled = 0;
         edtavTqtdedmn_Enabled = 0;
         edtavTdiferenca_Enabled = 0;
      }

      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_20 ;
      private short nGXsfl_20_idx=1 ;
      private short AV23Filtro_Ano ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short AV8Ano ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_20_Refreshing=0 ;
      private short subGrid1_Backcolorstyle ;
      private short subGrid1_Sortable ;
      private short AV27i ;
      private short GRID1_nEOF ;
      private short subGrid1_Titlebackstyle ;
      private short subGrid1_Allowselection ;
      private short subGrid1_Allowhovering ;
      private short subGrid1_Allowcollapsing ;
      private short subGrid1_Collapsed ;
      private short nGXWrapped ;
      private short subGrid1_Backstyle ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A584ContagemResultado_ContadorFM ;
      private int AV24Filtro_ContadorFMCod ;
      private int A601ContagemResultado_Servico ;
      private int AV58Servico_Codigo ;
      private int AV55tQtdeDmn ;
      private int A1Usuario_Codigo ;
      private int AV11ContagemResultado_ContadorFMCod ;
      private int A456ContagemResultado_Codigo ;
      private int AV43QtdeDmn ;
      private int gxdynajaxindex ;
      private int subGrid1_Islastpage ;
      private int edtavContagemresultado_contadorfmcod_Enabled ;
      private int edtavContagemresultado_contadorfmnom_Enabled ;
      private int edtavPfbfs_Enabled ;
      private int edtavPffinal_Enabled ;
      private int edtavQtdedmn_Enabled ;
      private int edtavDiferenca_Enabled ;
      private int edtavTpfbfs_Enabled ;
      private int edtavTpffinal_Enabled ;
      private int edtavTqtdedmn_Enabled ;
      private int edtavTdiferenca_Enabled ;
      private int subGrid1_Titleforecolor ;
      private int AV57WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A146Modulo_Codigo ;
      private int A127Sistema_Codigo ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A830AreaTrabalho_ServicoPadrao ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int GX_I ;
      private int [] AV37mQtdeDmn ;
      private int A57Usuario_PessoaCod ;
      private int subGrid1_Titlebackcolor ;
      private int subGrid1_Allbackcolor ;
      private int subGrid1_Selectioncolor ;
      private int subGrid1_Hoveringcolor ;
      private int idxLst ;
      private int subGrid1_Backcolor ;
      private int edtavContagemresultado_contadorfmcod_Visible ;
      private int edtavContagemresultado_contadorfmnom_Visible ;
      private int edtavPfbfs_Visible ;
      private int edtavPffinal_Visible ;
      private int edtavQtdedmn_Visible ;
      private int edtavDiferenca_Visible ;
      private long AV26Filtro_Mes ;
      private long AV32Mes ;
      private long GRID1_nCurrentRecord ;
      private long GRID1_nFirstRecordOnPage ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal AV53tPFBFS ;
      private decimal AV54tPFFinal ;
      private decimal AV50tDiferenca ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal AV41PFBFS ;
      private decimal AV42PFFinal ;
      private decimal AV22Diferenca ;
      private decimal AV52tPFBFM ;
      private decimal [] AV35mPFBFS ;
      private decimal [] AV36mPFFinal ;
      private decimal [] AV31mDiferenca ;
      private decimal AV60PFFSPrimeiraCnt ;
      private decimal AV62Contagem_PFFinal ;
      private String edtavQtdedmn_Tooltiptext ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_20_idx="0001" ;
      private String edtavQtdedmn_Internalname ;
      private String A484ContagemResultado_StatusDmn ;
      private String edtavContagemresultado_contadorfmcod_Internalname ;
      private String A58Usuario_PessoaNom ;
      private String A511ContagemResultado_HoraCnt ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Innewwindow_Target ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV12ContagemResultado_ContadorFMNom ;
      private String edtavContagemresultado_contadorfmnom_Internalname ;
      private String cmbavMes_Internalname ;
      private String cmbavAno_Internalname ;
      private String edtavPfbfs_Internalname ;
      private String edtavPffinal_Internalname ;
      private String edtavDiferenca_Internalname ;
      private String GXCCtl ;
      private String edtavFiltro_contadorfmcod_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtavTpfbfs_Internalname ;
      private String edtavTpffinal_Internalname ;
      private String edtavTqtdedmn_Internalname ;
      private String edtavTdiferenca_Internalname ;
      private String dynavServico_codigo_Internalname ;
      private String cmbavFiltro_ano_Internalname ;
      private String cmbavFiltro_mes_Internalname ;
      private String hsh ;
      private String Innewwindow_Internalname ;
      private String sStyleString ;
      private String tblTable4_Internalname ;
      private String lblContagemresultadotitle_Internalname ;
      private String lblContagemresultadotitle_Jsonclick ;
      private String lblTextblock4_Internalname ;
      private String lblTextblock4_Jsonclick ;
      private String TempTags ;
      private String edtavFiltro_contadorfmcod_Jsonclick ;
      private String lblTextblock6_Internalname ;
      private String lblTextblock6_Jsonclick ;
      private String dynavServico_codigo_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String cmbavFiltro_ano_Jsonclick ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String cmbavFiltro_mes_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String bttButton1_Internalname ;
      private String bttButton1_Jsonclick ;
      private String subGrid1_Internalname ;
      private String subGrid1_Class ;
      private String subGrid1_Linesclass ;
      private String tblTable3_Internalname ;
      private String lblTextblockcontagemresultado_pflfstotal_Internalname ;
      private String lblTextblockcontagemresultado_pflfstotal_Jsonclick ;
      private String edtavTpfbfs_Jsonclick ;
      private String lblTextblockcontagemresultado_pfbfmtotal2_Internalname ;
      private String lblTextblockcontagemresultado_pfbfmtotal2_Jsonclick ;
      private String edtavTpffinal_Jsonclick ;
      private String lblTextblockcontagemresultado_pflfmtotal_Internalname ;
      private String lblTextblockcontagemresultado_pflfmtotal_Jsonclick ;
      private String edtavTqtdedmn_Jsonclick ;
      private String lblTextblockcontagemresultado_pflfmtotal2_Internalname ;
      private String lblTextblockcontagemresultado_pflfmtotal2_Jsonclick ;
      private String edtavTdiferenca_Jsonclick ;
      private String sGXsfl_20_fel_idx="0001" ;
      private String ROClassString ;
      private String edtavContagemresultado_contadorfmcod_Jsonclick ;
      private String edtavContagemresultado_contadorfmnom_Jsonclick ;
      private String cmbavMes_Jsonclick ;
      private String cmbavAno_Jsonclick ;
      private String edtavPfbfs_Jsonclick ;
      private String edtavPffinal_Jsonclick ;
      private String edtavQtdedmn_Jsonclick ;
      private String edtavDiferenca_Jsonclick ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private DateTime A473ContagemResultado_DataCnt ;
      private DateTime Gx_date ;
      private bool entryPointCalled ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n584ContagemResultado_ContadorFM ;
      private bool n601ContagemResultado_Servico ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n58Usuario_PessoaNom ;
      private bool n458ContagemResultado_PFBFS ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool BRKFU3 ;
      private bool n146Modulo_Codigo ;
      private bool n830AreaTrabalho_ServicoPadrao ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool A632Servico_Ativo ;
      private bool n632Servico_Ativo ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid Grid1Container ;
      private GXWebRow Grid1Row ;
      private GXWebColumn Grid1Column ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavServico_codigo ;
      private GXCombobox cmbavFiltro_ano ;
      private GXCombobox cmbavFiltro_mes ;
      private GXCombobox cmbavMes ;
      private GXCombobox cmbavAno ;
      private IDataStoreProvider pr_default ;
      private int[] H00FU2_A155Servico_Codigo ;
      private String[] H00FU2_A605Servico_Sigla ;
      private int[] H00FU2_A631Servico_Vinculado ;
      private bool[] H00FU2_n631Servico_Vinculado ;
      private bool[] H00FU2_A632Servico_Ativo ;
      private bool[] H00FU2_n632Servico_Ativo ;
      private int[] H00FU5_A146Modulo_Codigo ;
      private bool[] H00FU5_n146Modulo_Codigo ;
      private int[] H00FU5_A127Sistema_Codigo ;
      private int[] H00FU5_A135Sistema_AreaTrabalhoCod ;
      private int[] H00FU5_A830AreaTrabalho_ServicoPadrao ;
      private bool[] H00FU5_n830AreaTrabalho_ServicoPadrao ;
      private int[] H00FU5_A490ContagemResultado_ContratadaCod ;
      private bool[] H00FU5_n490ContagemResultado_ContratadaCod ;
      private int[] H00FU5_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00FU5_n1553ContagemResultado_CntSrvCod ;
      private int[] H00FU5_A456ContagemResultado_Codigo ;
      private int[] H00FU5_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00FU5_n52Contratada_AreaTrabalhoCod ;
      private bool[] H00FU5_A632Servico_Ativo ;
      private bool[] H00FU5_n632Servico_Ativo ;
      private String[] H00FU5_A484ContagemResultado_StatusDmn ;
      private bool[] H00FU5_n484ContagemResultado_StatusDmn ;
      private int[] H00FU5_A601ContagemResultado_Servico ;
      private bool[] H00FU5_n601ContagemResultado_Servico ;
      private int[] H00FU5_A584ContagemResultado_ContadorFM ;
      private bool[] H00FU5_n584ContagemResultado_ContadorFM ;
      private decimal[] H00FU5_A684ContagemResultado_PFBFSUltima ;
      private DateTime[] H00FU5_A566ContagemResultado_DataUltCnt ;
      private int[] H00FU6_A57Usuario_PessoaCod ;
      private int[] H00FU6_A1Usuario_Codigo ;
      private String[] H00FU6_A58Usuario_PessoaNom ;
      private bool[] H00FU6_n58Usuario_PessoaNom ;
      private int[] H00FU9_A146Modulo_Codigo ;
      private bool[] H00FU9_n146Modulo_Codigo ;
      private int[] H00FU9_A127Sistema_Codigo ;
      private int[] H00FU9_A135Sistema_AreaTrabalhoCod ;
      private int[] H00FU9_A830AreaTrabalho_ServicoPadrao ;
      private bool[] H00FU9_n830AreaTrabalho_ServicoPadrao ;
      private int[] H00FU9_A490ContagemResultado_ContratadaCod ;
      private bool[] H00FU9_n490ContagemResultado_ContratadaCod ;
      private int[] H00FU9_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00FU9_n1553ContagemResultado_CntSrvCod ;
      private int[] H00FU9_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00FU9_n52Contratada_AreaTrabalhoCod ;
      private int[] H00FU9_A456ContagemResultado_Codigo ;
      private bool[] H00FU9_A632Servico_Ativo ;
      private bool[] H00FU9_n632Servico_Ativo ;
      private String[] H00FU9_A484ContagemResultado_StatusDmn ;
      private bool[] H00FU9_n484ContagemResultado_StatusDmn ;
      private int[] H00FU9_A601ContagemResultado_Servico ;
      private bool[] H00FU9_n601ContagemResultado_Servico ;
      private int[] H00FU9_A584ContagemResultado_ContadorFM ;
      private bool[] H00FU9_n584ContagemResultado_ContadorFM ;
      private decimal[] H00FU9_A684ContagemResultado_PFBFSUltima ;
      private DateTime[] H00FU9_A566ContagemResultado_DataUltCnt ;
      private decimal[] H00FU9_A682ContagemResultado_PFBFMUltima ;
      private int[] H00FU10_A456ContagemResultado_Codigo ;
      private decimal[] H00FU10_A458ContagemResultado_PFBFS ;
      private bool[] H00FU10_n458ContagemResultado_PFBFS ;
      private String[] H00FU10_A511ContagemResultado_HoraCnt ;
      private DateTime[] H00FU10_A473ContagemResultado_DataCnt ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IGxSession AV6WebSession ;
      private GXWebForm Form ;
      private SdtSDT_FiltroConsContadorFM AV46SDT_FiltroConsContadorFM ;
      private wwpbaseobjects.SdtWWPContext AV57WWPContext ;
   }

   public class wp_consultadiferencapf__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00FU5( IGxContext context ,
                                             int AV58Servico_Codigo ,
                                             int A601ContagemResultado_Servico ,
                                             int AV24Filtro_ContadorFMCod ,
                                             int A584ContagemResultado_ContadorFM ,
                                             DateTime A566ContagemResultado_DataUltCnt ,
                                             short AV23Filtro_Ano ,
                                             long AV26Filtro_Mes ,
                                             decimal A684ContagemResultado_PFBFSUltima ,
                                             String A484ContagemResultado_StatusDmn ,
                                             int AV57WWPContext_gxTpr_Areatrabalho_codigo ,
                                             int A52Contratada_AreaTrabalhoCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [7] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Modulo_Codigo], T2.[Sistema_Codigo], T3.[Sistema_AreaTrabalhoCod] AS Sistema_AreaTrabalhoCod, T4.[AreaTrabalho_ServicoPadrao] AS AreaTrabalho_ServicoPadrao, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T6.[Contratada_AreaTrabalhoCod], T5.[Servico_Ativo], T1.[ContagemResultado_StatusDmn], T7.[Servico_Codigo] AS ContagemResultado_Servico, COALESCE( T8.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM, COALESCE( T9.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt FROM (((((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Modulo] T2 WITH (NOLOCK) ON T2.[Modulo_Codigo] = T1.[Modulo_Codigo]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T2.[Sistema_Codigo]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T3.[Sistema_AreaTrabalhoCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[AreaTrabalho_ServicoPadrao]) LEFT JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [ContratoServicos] T7 WITH (NOLOCK) ON T7.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN (SELECT MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T8 ON T8.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, [ContagemResultado_Codigo],";
         scmdbuf = scmdbuf + " MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T9 ON T9.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T6.[Contratada_AreaTrabalhoCod] = @AV57WWPC_1Areatrabalho_codigo)";
         scmdbuf = scmdbuf + " and ((@AV24Filtro_ContadorFMCod = convert(int, 0)) or ( COALESCE( T8.[ContagemResultado_ContadorFM], 0) = @AV24Filtro_ContadorFMCod))";
         scmdbuf = scmdbuf + " and (YEAR(COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) = @AV23Filtro_Ano)";
         scmdbuf = scmdbuf + " and ((@AV26Filtro_Mes = convert(int, 0)) or ( MONTH(COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) = @AV26Filtro_Mes))";
         scmdbuf = scmdbuf + " and (COALESCE( T9.[ContagemResultado_PFBFSUltima], 0) > 0)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_StatusDmn] = 'R' or T1.[ContagemResultado_StatusDmn] = 'C' or T1.[ContagemResultado_StatusDmn] = 'H' or T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L')";
         if ( ! (0==AV58Servico_Codigo) )
         {
            sWhereString = sWhereString + " and (T7.[Servico_Codigo] = @AV58Servico_Codigo)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T6.[Contratada_AreaTrabalhoCod], [ContagemResultado_ContadorFM]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H00FU9( IGxContext context ,
                                             int AV58Servico_Codigo ,
                                             int A601ContagemResultado_Servico ,
                                             DateTime A566ContagemResultado_DataUltCnt ,
                                             short AV23Filtro_Ano ,
                                             long AV26Filtro_Mes ,
                                             decimal A684ContagemResultado_PFBFSUltima ,
                                             String A484ContagemResultado_StatusDmn ,
                                             int AV57WWPContext_gxTpr_Areatrabalho_codigo ,
                                             int AV11ContagemResultado_ContadorFMCod ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int A584ContagemResultado_ContadorFM )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [6] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Modulo_Codigo], T2.[Sistema_Codigo], T3.[Sistema_AreaTrabalhoCod] AS Sistema_AreaTrabalhoCod, T4.[AreaTrabalho_ServicoPadrao] AS AreaTrabalho_ServicoPadrao, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T6.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_Codigo], T5.[Servico_Ativo], T1.[ContagemResultado_StatusDmn], T7.[Servico_Codigo] AS ContagemResultado_Servico, COALESCE( T8.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM, COALESCE( T9.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, COALESCE( T9.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima FROM (((((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Modulo] T2 WITH (NOLOCK) ON T2.[Modulo_Codigo] = T1.[Modulo_Codigo]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T2.[Sistema_Codigo]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T3.[Sistema_AreaTrabalhoCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[AreaTrabalho_ServicoPadrao]) LEFT JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [ContratoServicos] T7 WITH (NOLOCK) ON T7.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN (SELECT MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T8 ON T8.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFS])";
         scmdbuf = scmdbuf + " AS ContagemResultado_PFBFSUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T9 ON T9.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T6.[Contratada_AreaTrabalhoCod] = @AV57WWPC_1Areatrabalho_codigo and COALESCE( T8.[ContagemResultado_ContadorFM], 0) = @AV11ContagemResultado_ContadorFMCod)";
         scmdbuf = scmdbuf + " and (YEAR(COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) = @AV23Filtro_Ano)";
         scmdbuf = scmdbuf + " and ((@AV26Filtro_Mes = convert(int, 0)) or ( MONTH(COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 ))) = @AV26Filtro_Mes))";
         scmdbuf = scmdbuf + " and (COALESCE( T9.[ContagemResultado_PFBFSUltima], 0) > 0)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_StatusDmn] = 'R' or T1.[ContagemResultado_StatusDmn] = 'C' or T1.[ContagemResultado_StatusDmn] = 'H' or T1.[ContagemResultado_StatusDmn] = 'P' or T1.[ContagemResultado_StatusDmn] = 'L')";
         if ( ! (0==AV58Servico_Codigo) )
         {
            sWhereString = sWhereString + " and (T7.[Servico_Codigo] = @AV58Servico_Codigo)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T6.[Contratada_AreaTrabalhoCod], [ContagemResultado_ContadorFM]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_H00FU5(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (DateTime)dynConstraints[4] , (short)dynConstraints[5] , (long)dynConstraints[6] , (decimal)dynConstraints[7] , (String)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] );
               case 3 :
                     return conditional_H00FU9(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (long)dynConstraints[4] , (decimal)dynConstraints[5] , (String)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00FU2 ;
          prmH00FU2 = new Object[] {
          } ;
          Object[] prmH00FU6 ;
          prmH00FU6 = new Object[] {
          new Object[] {"@AV11ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00FU10 ;
          prmH00FU10 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00FU5 ;
          prmH00FU5 = new Object[] {
          new Object[] {"@AV57WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV24Filtro_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV24Filtro_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV23Filtro_Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV26Filtro_Mes",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV26Filtro_Mes",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV58Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00FU9 ;
          prmH00FU9 = new Object[] {
          new Object[] {"@AV57WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV23Filtro_Ano",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV26Filtro_Mes",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV26Filtro_Mes",SqlDbType.Decimal,10,0} ,
          new Object[] {"@AV58Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00FU2", "SELECT [Servico_Codigo], [Servico_Sigla], [Servico_Vinculado], [Servico_Ativo] FROM [Servico] WITH (NOLOCK) WHERE (([Servico_Vinculado] = convert(int, 0))) AND ([Servico_Ativo] = 1) ORDER BY [Servico_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FU2,0,0,true,false )
             ,new CursorDef("H00FU5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FU5,100,0,true,false )
             ,new CursorDef("H00FU6", "SELECT TOP 1 T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE T1.[Usuario_Codigo] = @AV11ContagemResultado_ContadorFMCod ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FU6,1,0,false,true )
             ,new CursorDef("H00FU9", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FU9,100,0,true,false )
             ,new CursorDef("H00FU10", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_PFBFS], [ContagemResultado_HoraCnt], [ContagemResultado_DataCnt] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FU10,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((bool[]) buf[13])[0] = rslt.getBool(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((int[]) buf[19])[0] = rslt.getInt(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((decimal[]) buf[21])[0] = rslt.getDecimal(13) ;
                ((DateTime[]) buf[22])[0] = rslt.getGXDate(14) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.getBool(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((int[]) buf[19])[0] = rslt.getInt(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((decimal[]) buf[21])[0] = rslt.getDecimal(13) ;
                ((DateTime[]) buf[22])[0] = rslt.getGXDate(14) ;
                ((decimal[]) buf[23])[0] = rslt.getDecimal(15) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 5) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
