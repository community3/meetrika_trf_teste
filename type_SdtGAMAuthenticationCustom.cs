/*
               File: type_SdtGAMAuthenticationCustom
        Description: GAMAuthenticationCustom
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:42.71
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMAuthenticationCustom : GxUserType, IGxExternalObject
   {
      public SdtGAMAuthenticationCustom( )
      {
         initialize();
      }

      public SdtGAMAuthenticationCustom( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMAuthenticationCustom_externalReference == null )
         {
            GAMAuthenticationCustom_externalReference = new Artech.Security.GAMAuthenticationCustom(context);
         }
         returntostring = "";
         returntostring = (String)(GAMAuthenticationCustom_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Version
      {
         get {
            if ( GAMAuthenticationCustom_externalReference == null )
            {
               GAMAuthenticationCustom_externalReference = new Artech.Security.GAMAuthenticationCustom(context);
            }
            return GAMAuthenticationCustom_externalReference.Version ;
         }

         set {
            if ( GAMAuthenticationCustom_externalReference == null )
            {
               GAMAuthenticationCustom_externalReference = new Artech.Security.GAMAuthenticationCustom(context);
            }
            GAMAuthenticationCustom_externalReference.Version = value;
         }

      }

      public String gxTpr_Privateencryptkey
      {
         get {
            if ( GAMAuthenticationCustom_externalReference == null )
            {
               GAMAuthenticationCustom_externalReference = new Artech.Security.GAMAuthenticationCustom(context);
            }
            return GAMAuthenticationCustom_externalReference.PrivateEncryptKey ;
         }

         set {
            if ( GAMAuthenticationCustom_externalReference == null )
            {
               GAMAuthenticationCustom_externalReference = new Artech.Security.GAMAuthenticationCustom(context);
            }
            GAMAuthenticationCustom_externalReference.PrivateEncryptKey = value;
         }

      }

      public String gxTpr_Filename
      {
         get {
            if ( GAMAuthenticationCustom_externalReference == null )
            {
               GAMAuthenticationCustom_externalReference = new Artech.Security.GAMAuthenticationCustom(context);
            }
            return GAMAuthenticationCustom_externalReference.FileName ;
         }

         set {
            if ( GAMAuthenticationCustom_externalReference == null )
            {
               GAMAuthenticationCustom_externalReference = new Artech.Security.GAMAuthenticationCustom(context);
            }
            GAMAuthenticationCustom_externalReference.FileName = value;
         }

      }

      public String gxTpr_Package
      {
         get {
            if ( GAMAuthenticationCustom_externalReference == null )
            {
               GAMAuthenticationCustom_externalReference = new Artech.Security.GAMAuthenticationCustom(context);
            }
            return GAMAuthenticationCustom_externalReference.Package ;
         }

         set {
            if ( GAMAuthenticationCustom_externalReference == null )
            {
               GAMAuthenticationCustom_externalReference = new Artech.Security.GAMAuthenticationCustom(context);
            }
            GAMAuthenticationCustom_externalReference.Package = value;
         }

      }

      public String gxTpr_Classname
      {
         get {
            if ( GAMAuthenticationCustom_externalReference == null )
            {
               GAMAuthenticationCustom_externalReference = new Artech.Security.GAMAuthenticationCustom(context);
            }
            return GAMAuthenticationCustom_externalReference.ClassName ;
         }

         set {
            if ( GAMAuthenticationCustom_externalReference == null )
            {
               GAMAuthenticationCustom_externalReference = new Artech.Security.GAMAuthenticationCustom(context);
            }
            GAMAuthenticationCustom_externalReference.ClassName = value;
         }

      }

      public String gxTpr_Method
      {
         get {
            if ( GAMAuthenticationCustom_externalReference == null )
            {
               GAMAuthenticationCustom_externalReference = new Artech.Security.GAMAuthenticationCustom(context);
            }
            return GAMAuthenticationCustom_externalReference.Method ;
         }

         set {
            if ( GAMAuthenticationCustom_externalReference == null )
            {
               GAMAuthenticationCustom_externalReference = new Artech.Security.GAMAuthenticationCustom(context);
            }
            GAMAuthenticationCustom_externalReference.Method = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMAuthenticationCustom_externalReference == null )
            {
               GAMAuthenticationCustom_externalReference = new Artech.Security.GAMAuthenticationCustom(context);
            }
            return GAMAuthenticationCustom_externalReference ;
         }

         set {
            GAMAuthenticationCustom_externalReference = (Artech.Security.GAMAuthenticationCustom)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMAuthenticationCustom GAMAuthenticationCustom_externalReference=null ;
   }

}
