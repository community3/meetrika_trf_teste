/*
               File: LoteArquivoAnexo
        Description: Arquivos Anexos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:51:36.64
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class lotearquivoanexo : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_16") == 0 )
         {
            A841LoteArquivoAnexo_LoteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A841LoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_16( A841LoteArquivoAnexo_LoteCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_15") == 0 )
         {
            A645TipoDocumento_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n645TipoDocumento_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_15( A645TipoDocumento_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7LoteArquivoAnexo_LoteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7LoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7LoteArquivoAnexo_LoteCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vLOTEARQUIVOANEXO_LOTECOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7LoteArquivoAnexo_LoteCod), "ZZZZZ9")));
               AV8LoteArquivoAnexo_Data = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8LoteArquivoAnexo_Data", context.localUtil.TToC( AV8LoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vLOTEARQUIVOANEXO_DATA", GetSecureSignedToken( "", context.localUtil.Format( AV8LoteArquivoAnexo_Data, "99/99/99 99:99")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynTipoDocumento_Codigo.Name = "TIPODOCUMENTO_CODIGO";
         dynTipoDocumento_Codigo.WebTags = "";
         dynTipoDocumento_Codigo.removeAllItems();
         /* Using cursor T002N6 */
         pr_default.execute(4);
         while ( (pr_default.getStatus(4) != 101) )
         {
            dynTipoDocumento_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T002N6_A645TipoDocumento_Codigo[0]), 6, 0)), T002N6_A646TipoDocumento_Nome[0], 0);
            pr_default.readNext(4);
         }
         pr_default.close(4);
         if ( dynTipoDocumento_Codigo.ItemCount > 0 )
         {
            A645TipoDocumento_Codigo = (int)(NumberUtil.Val( dynTipoDocumento_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0))), "."));
            n645TipoDocumento_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Arquivos Anexos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = dynTipoDocumento_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public lotearquivoanexo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public lotearquivoanexo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_LoteArquivoAnexo_LoteCod ,
                           DateTime aP2_LoteArquivoAnexo_Data )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7LoteArquivoAnexo_LoteCod = aP1_LoteArquivoAnexo_LoteCod;
         this.AV8LoteArquivoAnexo_Data = aP2_LoteArquivoAnexo_Data;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynTipoDocumento_Codigo = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynTipoDocumento_Codigo.ItemCount > 0 )
         {
            A645TipoDocumento_Codigo = (int)(NumberUtil.Val( dynTipoDocumento_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0))), "."));
            n645TipoDocumento_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2N103( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2N103e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtLoteArquivoAnexo_LoteCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A841LoteArquivoAnexo_LoteCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,53);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLoteArquivoAnexo_LoteCod_Jsonclick, 0, "Attribute", "", "", "", edtLoteArquivoAnexo_LoteCod_Visible, edtLoteArquivoAnexo_LoteCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_LoteArquivoAnexo.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2N103( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2N103( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2N103e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_45_2N103( true) ;
         }
         return  ;
      }

      protected void wb_table3_45_2N103e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2N103e( true) ;
         }
         else
         {
            wb_table1_2_2N103e( false) ;
         }
      }

      protected void wb_table3_45_2N103( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_LoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_LoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_LoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_45_2N103e( true) ;
         }
         else
         {
            wb_table3_45_2N103e( false) ;
         }
      }

      protected void wb_table2_5_2N103( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_2N103( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_2N103e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2N103e( true) ;
         }
         else
         {
            wb_table2_5_2N103e( false) ;
         }
      }

      protected void wb_table4_13_2N103( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktipodocumento_codigo_Internalname, "Tipo Documento", "", "", lblTextblocktipodocumento_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynTipoDocumento_Codigo, dynTipoDocumento_Codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)), 1, dynTipoDocumento_Codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynTipoDocumento_Codigo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_LoteArquivoAnexo.htm");
            dynTipoDocumento_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTipoDocumento_Codigo_Internalname, "Values", (String)(dynTipoDocumento_Codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklotearquivoanexo_descricao_Internalname, "Descri��o", "", "", lblTextblocklotearquivoanexo_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtLoteArquivoAnexo_Descricao_Internalname, A837LoteArquivoAnexo_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", 0, 1, edtLoteArquivoAnexo_Descricao_Enabled, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "DescricaoLonga", "HLP_LoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklotearquivoanexo_data_Internalname, "Data/Hora Upload", "", "", lblTextblocklotearquivoanexo_data_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtLoteArquivoAnexo_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtLoteArquivoAnexo_Data_Internalname, context.localUtil.TToC( A836LoteArquivoAnexo_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A836LoteArquivoAnexo_Data, "99/99/99 99:99"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLoteArquivoAnexo_Data_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, edtLoteArquivoAnexo_Data_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "DataHora", "right", false, "HLP_LoteArquivoAnexo.htm");
            GxWebStd.gx_bitmap( context, edtLoteArquivoAnexo_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtLoteArquivoAnexo_Data_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_LoteArquivoAnexo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td rowspan=\"4\"  class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklotearquivoanexo_arquivo_Internalname, "Arquivo", "", "", lblTextblocklotearquivoanexo_arquivo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td rowspan=\"4\"  class='DataContentCell'>") ;
            ClassString = "BootstrapAttribute";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'',0)\"";
            edtLoteArquivoAnexo_Arquivo_Filename = A839LoteArquivoAnexo_NomeArq;
            edtLoteArquivoAnexo_Arquivo_Filetype = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "Filetype", edtLoteArquivoAnexo_Arquivo_Filetype);
            edtLoteArquivoAnexo_Arquivo_Filetype = A840LoteArquivoAnexo_TipoArq;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "Filetype", edtLoteArquivoAnexo_Arquivo_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A838LoteArquivoAnexo_Arquivo)) )
            {
               gxblobfileaux.Source = A838LoteArquivoAnexo_Arquivo;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtLoteArquivoAnexo_Arquivo_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtLoteArquivoAnexo_Arquivo_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  A838LoteArquivoAnexo_Arquivo = gxblobfileaux.GetAbsoluteName();
                  n838LoteArquivoAnexo_Arquivo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A838LoteArquivoAnexo_Arquivo", A838LoteArquivoAnexo_Arquivo);
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A838LoteArquivoAnexo_Arquivo));
                  edtLoteArquivoAnexo_Arquivo_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "Filetype", edtLoteArquivoAnexo_Arquivo_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A838LoteArquivoAnexo_Arquivo));
            }
            GxWebStd.gx_blob_field( context, edtLoteArquivoAnexo_Arquivo_Internalname, StringUtil.RTrim( A838LoteArquivoAnexo_Arquivo), context.PathToRelativeUrl( A838LoteArquivoAnexo_Arquivo), (String.IsNullOrEmpty(StringUtil.RTrim( edtLoteArquivoAnexo_Arquivo_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtLoteArquivoAnexo_Arquivo_Filetype)) ? A838LoteArquivoAnexo_Arquivo : edtLoteArquivoAnexo_Arquivo_Filetype)) : edtLoteArquivoAnexo_Arquivo_Contenttype), true, edtLoteArquivoAnexo_Arquivo_Linktarget, edtLoteArquivoAnexo_Arquivo_Parameters, edtLoteArquivoAnexo_Arquivo_Display, edtLoteArquivoAnexo_Arquivo_Enabled, 1, "", "", 0, -1, 250, "px", 60, "px", 0, 0, 0, edtLoteArquivoAnexo_Arquivo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,32);\"", "", "", "HLP_LoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklotearquivoanexo_nomearq_Internalname, "Nome do Arquivo", "", "", lblTextblocklotearquivoanexo_nomearq_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLoteArquivoAnexo_NomeArq_Internalname, StringUtil.RTrim( A839LoteArquivoAnexo_NomeArq), StringUtil.RTrim( context.localUtil.Format( A839LoteArquivoAnexo_NomeArq, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLoteArquivoAnexo_NomeArq_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtLoteArquivoAnexo_NomeArq_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "NomeArq", "left", true, "HLP_LoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklotearquivoanexo_tipoarq_Internalname, "Tipo de Arquivo", "", "", lblTextblocklotearquivoanexo_tipoarq_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLoteArquivoAnexo_TipoArq_Internalname, StringUtil.RTrim( A840LoteArquivoAnexo_TipoArq), StringUtil.RTrim( context.localUtil.Format( A840LoteArquivoAnexo_TipoArq, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLoteArquivoAnexo_TipoArq_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtLoteArquivoAnexo_TipoArq_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "TipoArq", "left", true, "HLP_LoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_2N103e( true) ;
         }
         else
         {
            wb_table4_13_2N103e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E112N2 */
         E112N2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               dynTipoDocumento_Codigo.CurrentValue = cgiGet( dynTipoDocumento_Codigo_Internalname);
               A645TipoDocumento_Codigo = (int)(NumberUtil.Val( cgiGet( dynTipoDocumento_Codigo_Internalname), "."));
               n645TipoDocumento_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
               n645TipoDocumento_Codigo = ((0==A645TipoDocumento_Codigo) ? true : false);
               A837LoteArquivoAnexo_Descricao = cgiGet( edtLoteArquivoAnexo_Descricao_Internalname);
               n837LoteArquivoAnexo_Descricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A837LoteArquivoAnexo_Descricao", A837LoteArquivoAnexo_Descricao);
               n837LoteArquivoAnexo_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A837LoteArquivoAnexo_Descricao)) ? true : false);
               A836LoteArquivoAnexo_Data = context.localUtil.CToT( cgiGet( edtLoteArquivoAnexo_Data_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A836LoteArquivoAnexo_Data", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
               A838LoteArquivoAnexo_Arquivo = cgiGet( edtLoteArquivoAnexo_Arquivo_Internalname);
               n838LoteArquivoAnexo_Arquivo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A838LoteArquivoAnexo_Arquivo", A838LoteArquivoAnexo_Arquivo);
               n838LoteArquivoAnexo_Arquivo = (String.IsNullOrEmpty(StringUtil.RTrim( A838LoteArquivoAnexo_Arquivo)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtLoteArquivoAnexo_LoteCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtLoteArquivoAnexo_LoteCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "LOTEARQUIVOANEXO_LOTECOD");
                  AnyError = 1;
                  GX_FocusControl = edtLoteArquivoAnexo_LoteCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A841LoteArquivoAnexo_LoteCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A841LoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0)));
               }
               else
               {
                  A841LoteArquivoAnexo_LoteCod = (int)(context.localUtil.CToN( cgiGet( edtLoteArquivoAnexo_LoteCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A841LoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0)));
               }
               /* Read saved values. */
               Z841LoteArquivoAnexo_LoteCod = (int)(context.localUtil.CToN( cgiGet( "Z841LoteArquivoAnexo_LoteCod"), ",", "."));
               Z836LoteArquivoAnexo_Data = context.localUtil.CToT( cgiGet( "Z836LoteArquivoAnexo_Data"), 0);
               Z1027LoteArquivoAnexo_Verificador = cgiGet( "Z1027LoteArquivoAnexo_Verificador");
               n1027LoteArquivoAnexo_Verificador = (String.IsNullOrEmpty(StringUtil.RTrim( A1027LoteArquivoAnexo_Verificador)) ? true : false);
               Z645TipoDocumento_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z645TipoDocumento_Codigo"), ",", "."));
               n645TipoDocumento_Codigo = ((0==A645TipoDocumento_Codigo) ? true : false);
               A1027LoteArquivoAnexo_Verificador = cgiGet( "Z1027LoteArquivoAnexo_Verificador");
               n1027LoteArquivoAnexo_Verificador = false;
               n1027LoteArquivoAnexo_Verificador = (String.IsNullOrEmpty(StringUtil.RTrim( A1027LoteArquivoAnexo_Verificador)) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N645TipoDocumento_Codigo = (int)(context.localUtil.CToN( cgiGet( "N645TipoDocumento_Codigo"), ",", "."));
               n645TipoDocumento_Codigo = ((0==A645TipoDocumento_Codigo) ? true : false);
               AV7LoteArquivoAnexo_LoteCod = (int)(context.localUtil.CToN( cgiGet( "vLOTEARQUIVOANEXO_LOTECOD"), ",", "."));
               AV8LoteArquivoAnexo_Data = context.localUtil.CToT( cgiGet( "vLOTEARQUIVOANEXO_DATA"), 0);
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               AV12Insert_TipoDocumento_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_TIPODOCUMENTO_CODIGO"), ",", "."));
               A1027LoteArquivoAnexo_Verificador = cgiGet( "LOTEARQUIVOANEXO_VERIFICADOR");
               n1027LoteArquivoAnexo_Verificador = (String.IsNullOrEmpty(StringUtil.RTrim( A1027LoteArquivoAnexo_Verificador)) ? true : false);
               A646TipoDocumento_Nome = cgiGet( "TIPODOCUMENTO_NOME");
               AV14Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               edtLoteArquivoAnexo_Arquivo_Filetype = cgiGet( "LOTEARQUIVOANEXO_ARQUIVO_Filetype");
               edtLoteArquivoAnexo_Arquivo_Filename = cgiGet( "LOTEARQUIVOANEXO_ARQUIVO_Filename");
               edtLoteArquivoAnexo_Arquivo_Filename = cgiGet( "LOTEARQUIVOANEXO_ARQUIVO_Filename");
               edtLoteArquivoAnexo_Arquivo_Filetype = cgiGet( "LOTEARQUIVOANEXO_ARQUIVO_Filetype");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A838LoteArquivoAnexo_Arquivo)) )
               {
                  edtLoteArquivoAnexo_Arquivo_Filename = (String)(CGIGetFileName(edtLoteArquivoAnexo_Arquivo_Internalname));
                  edtLoteArquivoAnexo_Arquivo_Filetype = (String)(CGIGetFileType(edtLoteArquivoAnexo_Arquivo_Internalname));
               }
               A840LoteArquivoAnexo_TipoArq = edtLoteArquivoAnexo_Arquivo_Filetype;
               n840LoteArquivoAnexo_TipoArq = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A840LoteArquivoAnexo_TipoArq", A840LoteArquivoAnexo_TipoArq);
               A839LoteArquivoAnexo_NomeArq = edtLoteArquivoAnexo_Arquivo_Filename;
               n839LoteArquivoAnexo_NomeArq = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A839LoteArquivoAnexo_NomeArq", A839LoteArquivoAnexo_NomeArq);
               if ( String.IsNullOrEmpty(StringUtil.RTrim( A838LoteArquivoAnexo_Arquivo)) )
               {
                  GXCCtlgxBlob = "LOTEARQUIVOANEXO_ARQUIVO" + "_gxBlob";
                  A838LoteArquivoAnexo_Arquivo = cgiGet( GXCCtlgxBlob);
                  n838LoteArquivoAnexo_Arquivo = false;
                  n838LoteArquivoAnexo_Arquivo = (String.IsNullOrEmpty(StringUtil.RTrim( A838LoteArquivoAnexo_Arquivo)) ? true : false);
               }
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "LoteArquivoAnexo";
               A836LoteArquivoAnexo_Data = context.localUtil.CToT( cgiGet( edtLoteArquivoAnexo_Data_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A836LoteArquivoAnexo_Data", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A836LoteArquivoAnexo_Data, "99/99/99 99:99");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A1027LoteArquivoAnexo_Verificador, ""));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A841LoteArquivoAnexo_LoteCod != Z841LoteArquivoAnexo_LoteCod ) || ( A836LoteArquivoAnexo_Data != Z836LoteArquivoAnexo_Data ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("lotearquivoanexo:[SecurityCheckFailed value for]"+"LoteArquivoAnexo_Data:"+context.localUtil.Format( A836LoteArquivoAnexo_Data, "99/99/99 99:99"));
                  GXUtil.WriteLog("lotearquivoanexo:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("lotearquivoanexo:[SecurityCheckFailed value for]"+"LoteArquivoAnexo_Verificador:"+StringUtil.RTrim( context.localUtil.Format( A1027LoteArquivoAnexo_Verificador, "")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A841LoteArquivoAnexo_LoteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A841LoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0)));
                  A836LoteArquivoAnexo_Data = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A836LoteArquivoAnexo_Data", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
                  getEqualNoModal( ) ;
                  if ( ! (DateTime.MinValue==AV8LoteArquivoAnexo_Data) )
                  {
                     A836LoteArquivoAnexo_Data = AV8LoteArquivoAnexo_Data;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A836LoteArquivoAnexo_Data", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
                  }
                  else
                  {
                     if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (DateTime.MinValue==A836LoteArquivoAnexo_Data) && ( Gx_BScreen == 0 ) )
                     {
                        A836LoteArquivoAnexo_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A836LoteArquivoAnexo_Data", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
                     }
                  }
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode103 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     if ( ! (DateTime.MinValue==AV8LoteArquivoAnexo_Data) )
                     {
                        A836LoteArquivoAnexo_Data = AV8LoteArquivoAnexo_Data;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A836LoteArquivoAnexo_Data", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
                     }
                     else
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (DateTime.MinValue==A836LoteArquivoAnexo_Data) && ( Gx_BScreen == 0 ) )
                        {
                           A836LoteArquivoAnexo_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A836LoteArquivoAnexo_Data", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
                        }
                     }
                     Gx_mode = sMode103;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound103 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_2N0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "LOTEARQUIVOANEXO_LOTECOD");
                        AnyError = 1;
                        GX_FocusControl = edtLoteArquivoAnexo_LoteCod_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E112N2 */
                           E112N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E122N2 */
                           E122N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E122N2 */
            E122N2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2N103( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes2N103( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_2N0( )
      {
         BeforeValidate2N103( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2N103( ) ;
            }
            else
            {
               CheckExtendedTable2N103( ) ;
               CloseExtendedTableCursors2N103( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption2N0( )
      {
      }

      protected void E112N2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         AV10TrnContext.FromXml(AV11WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV10TrnContext.gxTpr_Transactionname, AV14Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV15GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            while ( AV15GXV1 <= AV10TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV10TrnContext.gxTpr_Attributes.Item(AV15GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "TipoDocumento_Codigo") == 0 )
               {
                  AV12Insert_TipoDocumento_Codigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_TipoDocumento_Codigo), 6, 0)));
               }
               AV15GXV1 = (int)(AV15GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            }
         }
         edtLoteArquivoAnexo_LoteCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_LoteCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLoteArquivoAnexo_LoteCod_Visible), 5, 0)));
         edtLoteArquivoAnexo_Arquivo_Display = 1;
         edtLoteArquivoAnexo_Arquivo_Linktarget = "_blank";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "Linktarget", edtLoteArquivoAnexo_Arquivo_Linktarget);
      }

      protected void E122N2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV10TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwlotearquivoanexo.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM2N103( short GX_JID )
      {
         if ( ( GX_JID == 14 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1027LoteArquivoAnexo_Verificador = T002N3_A1027LoteArquivoAnexo_Verificador[0];
               Z645TipoDocumento_Codigo = T002N3_A645TipoDocumento_Codigo[0];
            }
            else
            {
               Z1027LoteArquivoAnexo_Verificador = A1027LoteArquivoAnexo_Verificador;
               Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            }
         }
         if ( GX_JID == -14 )
         {
            Z836LoteArquivoAnexo_Data = A836LoteArquivoAnexo_Data;
            Z838LoteArquivoAnexo_Arquivo = A838LoteArquivoAnexo_Arquivo;
            Z837LoteArquivoAnexo_Descricao = A837LoteArquivoAnexo_Descricao;
            Z1027LoteArquivoAnexo_Verificador = A1027LoteArquivoAnexo_Verificador;
            Z840LoteArquivoAnexo_TipoArq = A840LoteArquivoAnexo_TipoArq;
            Z839LoteArquivoAnexo_NomeArq = A839LoteArquivoAnexo_NomeArq;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z841LoteArquivoAnexo_LoteCod = A841LoteArquivoAnexo_LoteCod;
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
         }
      }

      protected void standaloneNotModal( )
      {
         edtLoteArquivoAnexo_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLoteArquivoAnexo_Data_Enabled), 5, 0)));
         edtLoteArquivoAnexo_NomeArq_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_NomeArq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLoteArquivoAnexo_NomeArq_Enabled), 5, 0)));
         edtLoteArquivoAnexo_TipoArq_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_TipoArq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLoteArquivoAnexo_TipoArq_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         AV14Pgmname = "LoteArquivoAnexo";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
         edtLoteArquivoAnexo_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLoteArquivoAnexo_Data_Enabled), 5, 0)));
         edtLoteArquivoAnexo_NomeArq_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_NomeArq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLoteArquivoAnexo_NomeArq_Enabled), 5, 0)));
         edtLoteArquivoAnexo_TipoArq_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_TipoArq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLoteArquivoAnexo_TipoArq_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7LoteArquivoAnexo_LoteCod) )
         {
            A841LoteArquivoAnexo_LoteCod = AV7LoteArquivoAnexo_LoteCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A841LoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0)));
         }
         if ( ! (0==AV7LoteArquivoAnexo_LoteCod) )
         {
            edtLoteArquivoAnexo_LoteCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_LoteCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLoteArquivoAnexo_LoteCod_Enabled), 5, 0)));
         }
         else
         {
            edtLoteArquivoAnexo_LoteCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_LoteCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLoteArquivoAnexo_LoteCod_Enabled), 5, 0)));
         }
         if ( ! (0==AV7LoteArquivoAnexo_LoteCod) )
         {
            edtLoteArquivoAnexo_LoteCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_LoteCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLoteArquivoAnexo_LoteCod_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_TipoDocumento_Codigo) )
         {
            dynTipoDocumento_Codigo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTipoDocumento_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynTipoDocumento_Codigo.Enabled), 5, 0)));
         }
         else
         {
            dynTipoDocumento_Codigo.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTipoDocumento_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynTipoDocumento_Codigo.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_TipoDocumento_Codigo) )
         {
            A645TipoDocumento_Codigo = AV12Insert_TipoDocumento_Codigo;
            n645TipoDocumento_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ! (DateTime.MinValue==AV8LoteArquivoAnexo_Data) )
         {
            A836LoteArquivoAnexo_Data = AV8LoteArquivoAnexo_Data;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A836LoteArquivoAnexo_Data", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (DateTime.MinValue==A836LoteArquivoAnexo_Data) && ( Gx_BScreen == 0 ) )
            {
               A836LoteArquivoAnexo_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A836LoteArquivoAnexo_Data", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
            }
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T002N4 */
            pr_default.execute(2, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
            A646TipoDocumento_Nome = T002N4_A646TipoDocumento_Nome[0];
            pr_default.close(2);
         }
      }

      protected void Load2N103( )
      {
         /* Using cursor T002N7 */
         pr_default.execute(5, new Object[] {A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound103 = 1;
            A837LoteArquivoAnexo_Descricao = T002N7_A837LoteArquivoAnexo_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A837LoteArquivoAnexo_Descricao", A837LoteArquivoAnexo_Descricao);
            n837LoteArquivoAnexo_Descricao = T002N7_n837LoteArquivoAnexo_Descricao[0];
            A646TipoDocumento_Nome = T002N7_A646TipoDocumento_Nome[0];
            A1027LoteArquivoAnexo_Verificador = T002N7_A1027LoteArquivoAnexo_Verificador[0];
            n1027LoteArquivoAnexo_Verificador = T002N7_n1027LoteArquivoAnexo_Verificador[0];
            A840LoteArquivoAnexo_TipoArq = T002N7_A840LoteArquivoAnexo_TipoArq[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A840LoteArquivoAnexo_TipoArq", A840LoteArquivoAnexo_TipoArq);
            n840LoteArquivoAnexo_TipoArq = T002N7_n840LoteArquivoAnexo_TipoArq[0];
            edtLoteArquivoAnexo_Arquivo_Filetype = A840LoteArquivoAnexo_TipoArq;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "Filetype", edtLoteArquivoAnexo_Arquivo_Filetype);
            A839LoteArquivoAnexo_NomeArq = T002N7_A839LoteArquivoAnexo_NomeArq[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A839LoteArquivoAnexo_NomeArq", A839LoteArquivoAnexo_NomeArq);
            n839LoteArquivoAnexo_NomeArq = T002N7_n839LoteArquivoAnexo_NomeArq[0];
            edtLoteArquivoAnexo_Arquivo_Filename = A839LoteArquivoAnexo_NomeArq;
            A645TipoDocumento_Codigo = T002N7_A645TipoDocumento_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
            n645TipoDocumento_Codigo = T002N7_n645TipoDocumento_Codigo[0];
            A838LoteArquivoAnexo_Arquivo = T002N7_A838LoteArquivoAnexo_Arquivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A838LoteArquivoAnexo_Arquivo", A838LoteArquivoAnexo_Arquivo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A838LoteArquivoAnexo_Arquivo));
            n838LoteArquivoAnexo_Arquivo = T002N7_n838LoteArquivoAnexo_Arquivo[0];
            ZM2N103( -14) ;
         }
         pr_default.close(5);
         OnLoadActions2N103( ) ;
      }

      protected void OnLoadActions2N103( )
      {
      }

      protected void CheckExtendedTable2N103( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T002N5 */
         pr_default.execute(3, new Object[] {A841LoteArquivoAnexo_LoteCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Lote Arquivo Anexo_Lote'.", "ForeignKeyNotFound", 1, "LOTEARQUIVOANEXO_LOTECOD");
            AnyError = 1;
            GX_FocusControl = edtLoteArquivoAnexo_LoteCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(3);
         if ( ! ( (DateTime.MinValue==A836LoteArquivoAnexo_Data) || ( A836LoteArquivoAnexo_Data >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data/Hora Upload fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         /* Using cursor T002N4 */
         pr_default.execute(2, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A645TipoDocumento_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Tipo de Documentos'.", "ForeignKeyNotFound", 1, "TIPODOCUMENTO_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynTipoDocumento_Codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A646TipoDocumento_Nome = T002N4_A646TipoDocumento_Nome[0];
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors2N103( )
      {
         pr_default.close(3);
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_16( int A841LoteArquivoAnexo_LoteCod )
      {
         /* Using cursor T002N8 */
         pr_default.execute(6, new Object[] {A841LoteArquivoAnexo_LoteCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Lote Arquivo Anexo_Lote'.", "ForeignKeyNotFound", 1, "LOTEARQUIVOANEXO_LOTECOD");
            AnyError = 1;
            GX_FocusControl = edtLoteArquivoAnexo_LoteCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void gxLoad_15( int A645TipoDocumento_Codigo )
      {
         /* Using cursor T002N9 */
         pr_default.execute(7, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
         if ( (pr_default.getStatus(7) == 101) )
         {
            if ( ! ( (0==A645TipoDocumento_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Tipo de Documentos'.", "ForeignKeyNotFound", 1, "TIPODOCUMENTO_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynTipoDocumento_Codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A646TipoDocumento_Nome = T002N9_A646TipoDocumento_Nome[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A646TipoDocumento_Nome))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void GetKey2N103( )
      {
         /* Using cursor T002N10 */
         pr_default.execute(8, new Object[] {A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data});
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound103 = 1;
         }
         else
         {
            RcdFound103 = 0;
         }
         pr_default.close(8);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T002N3 */
         pr_default.execute(1, new Object[] {A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2N103( 14) ;
            RcdFound103 = 1;
            A836LoteArquivoAnexo_Data = T002N3_A836LoteArquivoAnexo_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A836LoteArquivoAnexo_Data", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
            A837LoteArquivoAnexo_Descricao = T002N3_A837LoteArquivoAnexo_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A837LoteArquivoAnexo_Descricao", A837LoteArquivoAnexo_Descricao);
            n837LoteArquivoAnexo_Descricao = T002N3_n837LoteArquivoAnexo_Descricao[0];
            A1027LoteArquivoAnexo_Verificador = T002N3_A1027LoteArquivoAnexo_Verificador[0];
            n1027LoteArquivoAnexo_Verificador = T002N3_n1027LoteArquivoAnexo_Verificador[0];
            A840LoteArquivoAnexo_TipoArq = T002N3_A840LoteArquivoAnexo_TipoArq[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A840LoteArquivoAnexo_TipoArq", A840LoteArquivoAnexo_TipoArq);
            n840LoteArquivoAnexo_TipoArq = T002N3_n840LoteArquivoAnexo_TipoArq[0];
            edtLoteArquivoAnexo_Arquivo_Filetype = A840LoteArquivoAnexo_TipoArq;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "Filetype", edtLoteArquivoAnexo_Arquivo_Filetype);
            A839LoteArquivoAnexo_NomeArq = T002N3_A839LoteArquivoAnexo_NomeArq[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A839LoteArquivoAnexo_NomeArq", A839LoteArquivoAnexo_NomeArq);
            n839LoteArquivoAnexo_NomeArq = T002N3_n839LoteArquivoAnexo_NomeArq[0];
            edtLoteArquivoAnexo_Arquivo_Filename = A839LoteArquivoAnexo_NomeArq;
            A645TipoDocumento_Codigo = T002N3_A645TipoDocumento_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
            n645TipoDocumento_Codigo = T002N3_n645TipoDocumento_Codigo[0];
            A841LoteArquivoAnexo_LoteCod = T002N3_A841LoteArquivoAnexo_LoteCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A841LoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0)));
            A838LoteArquivoAnexo_Arquivo = T002N3_A838LoteArquivoAnexo_Arquivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A838LoteArquivoAnexo_Arquivo", A838LoteArquivoAnexo_Arquivo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A838LoteArquivoAnexo_Arquivo));
            n838LoteArquivoAnexo_Arquivo = T002N3_n838LoteArquivoAnexo_Arquivo[0];
            Z841LoteArquivoAnexo_LoteCod = A841LoteArquivoAnexo_LoteCod;
            Z836LoteArquivoAnexo_Data = A836LoteArquivoAnexo_Data;
            sMode103 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load2N103( ) ;
            if ( AnyError == 1 )
            {
               RcdFound103 = 0;
               InitializeNonKey2N103( ) ;
            }
            Gx_mode = sMode103;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound103 = 0;
            InitializeNonKey2N103( ) ;
            sMode103 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode103;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2N103( ) ;
         if ( RcdFound103 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound103 = 0;
         /* Using cursor T002N11 */
         pr_default.execute(9, new Object[] {A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T002N11_A841LoteArquivoAnexo_LoteCod[0] < A841LoteArquivoAnexo_LoteCod ) || ( T002N11_A841LoteArquivoAnexo_LoteCod[0] == A841LoteArquivoAnexo_LoteCod ) && ( T002N11_A836LoteArquivoAnexo_Data[0] < A836LoteArquivoAnexo_Data ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T002N11_A841LoteArquivoAnexo_LoteCod[0] > A841LoteArquivoAnexo_LoteCod ) || ( T002N11_A841LoteArquivoAnexo_LoteCod[0] == A841LoteArquivoAnexo_LoteCod ) && ( T002N11_A836LoteArquivoAnexo_Data[0] > A836LoteArquivoAnexo_Data ) ) )
            {
               A841LoteArquivoAnexo_LoteCod = T002N11_A841LoteArquivoAnexo_LoteCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A841LoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0)));
               A836LoteArquivoAnexo_Data = T002N11_A836LoteArquivoAnexo_Data[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A836LoteArquivoAnexo_Data", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
               RcdFound103 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void move_previous( )
      {
         RcdFound103 = 0;
         /* Using cursor T002N12 */
         pr_default.execute(10, new Object[] {A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data});
         if ( (pr_default.getStatus(10) != 101) )
         {
            while ( (pr_default.getStatus(10) != 101) && ( ( T002N12_A841LoteArquivoAnexo_LoteCod[0] > A841LoteArquivoAnexo_LoteCod ) || ( T002N12_A841LoteArquivoAnexo_LoteCod[0] == A841LoteArquivoAnexo_LoteCod ) && ( T002N12_A836LoteArquivoAnexo_Data[0] > A836LoteArquivoAnexo_Data ) ) )
            {
               pr_default.readNext(10);
            }
            if ( (pr_default.getStatus(10) != 101) && ( ( T002N12_A841LoteArquivoAnexo_LoteCod[0] < A841LoteArquivoAnexo_LoteCod ) || ( T002N12_A841LoteArquivoAnexo_LoteCod[0] == A841LoteArquivoAnexo_LoteCod ) && ( T002N12_A836LoteArquivoAnexo_Data[0] < A836LoteArquivoAnexo_Data ) ) )
            {
               A841LoteArquivoAnexo_LoteCod = T002N12_A841LoteArquivoAnexo_LoteCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A841LoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0)));
               A836LoteArquivoAnexo_Data = T002N12_A836LoteArquivoAnexo_Data[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A836LoteArquivoAnexo_Data", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
               RcdFound103 = 1;
            }
         }
         pr_default.close(10);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2N103( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = dynTipoDocumento_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2N103( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound103 == 1 )
            {
               if ( ( A841LoteArquivoAnexo_LoteCod != Z841LoteArquivoAnexo_LoteCod ) || ( A836LoteArquivoAnexo_Data != Z836LoteArquivoAnexo_Data ) )
               {
                  A841LoteArquivoAnexo_LoteCod = Z841LoteArquivoAnexo_LoteCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A841LoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0)));
                  A836LoteArquivoAnexo_Data = Z836LoteArquivoAnexo_Data;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A836LoteArquivoAnexo_Data", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "LOTEARQUIVOANEXO_LOTECOD");
                  AnyError = 1;
                  GX_FocusControl = edtLoteArquivoAnexo_LoteCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = dynTipoDocumento_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update2N103( ) ;
                  GX_FocusControl = dynTipoDocumento_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A841LoteArquivoAnexo_LoteCod != Z841LoteArquivoAnexo_LoteCod ) || ( A836LoteArquivoAnexo_Data != Z836LoteArquivoAnexo_Data ) )
               {
                  /* Insert record */
                  GX_FocusControl = dynTipoDocumento_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2N103( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "LOTEARQUIVOANEXO_LOTECOD");
                     AnyError = 1;
                     GX_FocusControl = edtLoteArquivoAnexo_LoteCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = dynTipoDocumento_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2N103( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( ( A841LoteArquivoAnexo_LoteCod != Z841LoteArquivoAnexo_LoteCod ) || ( A836LoteArquivoAnexo_Data != Z836LoteArquivoAnexo_Data ) )
         {
            A841LoteArquivoAnexo_LoteCod = Z841LoteArquivoAnexo_LoteCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A841LoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0)));
            A836LoteArquivoAnexo_Data = Z836LoteArquivoAnexo_Data;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A836LoteArquivoAnexo_Data", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "LOTEARQUIVOANEXO_LOTECOD");
            AnyError = 1;
            GX_FocusControl = edtLoteArquivoAnexo_LoteCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = dynTipoDocumento_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency2N103( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T002N2 */
            pr_default.execute(0, new Object[] {A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"LoteArquivoAnexo"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1027LoteArquivoAnexo_Verificador, T002N2_A1027LoteArquivoAnexo_Verificador[0]) != 0 ) || ( Z645TipoDocumento_Codigo != T002N2_A645TipoDocumento_Codigo[0] ) )
            {
               if ( StringUtil.StrCmp(Z1027LoteArquivoAnexo_Verificador, T002N2_A1027LoteArquivoAnexo_Verificador[0]) != 0 )
               {
                  GXUtil.WriteLog("lotearquivoanexo:[seudo value changed for attri]"+"LoteArquivoAnexo_Verificador");
                  GXUtil.WriteLogRaw("Old: ",Z1027LoteArquivoAnexo_Verificador);
                  GXUtil.WriteLogRaw("Current: ",T002N2_A1027LoteArquivoAnexo_Verificador[0]);
               }
               if ( Z645TipoDocumento_Codigo != T002N2_A645TipoDocumento_Codigo[0] )
               {
                  GXUtil.WriteLog("lotearquivoanexo:[seudo value changed for attri]"+"TipoDocumento_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z645TipoDocumento_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T002N2_A645TipoDocumento_Codigo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"LoteArquivoAnexo"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2N103( )
      {
         BeforeValidate2N103( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2N103( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2N103( 0) ;
            CheckOptimisticConcurrency2N103( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2N103( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2N103( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002N13 */
                     A840LoteArquivoAnexo_TipoArq = edtLoteArquivoAnexo_Arquivo_Filetype;
                     n840LoteArquivoAnexo_TipoArq = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A840LoteArquivoAnexo_TipoArq", A840LoteArquivoAnexo_TipoArq);
                     A839LoteArquivoAnexo_NomeArq = edtLoteArquivoAnexo_Arquivo_Filename;
                     n839LoteArquivoAnexo_NomeArq = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A839LoteArquivoAnexo_NomeArq", A839LoteArquivoAnexo_NomeArq);
                     pr_default.execute(11, new Object[] {A836LoteArquivoAnexo_Data, n838LoteArquivoAnexo_Arquivo, A838LoteArquivoAnexo_Arquivo, n837LoteArquivoAnexo_Descricao, A837LoteArquivoAnexo_Descricao, n1027LoteArquivoAnexo_Verificador, A1027LoteArquivoAnexo_Verificador, n840LoteArquivoAnexo_TipoArq, A840LoteArquivoAnexo_TipoArq, n839LoteArquivoAnexo_NomeArq, A839LoteArquivoAnexo_NomeArq, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo, A841LoteArquivoAnexo_LoteCod});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("LoteArquivoAnexo") ;
                     if ( (pr_default.getStatus(11) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption2N0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2N103( ) ;
            }
            EndLevel2N103( ) ;
         }
         CloseExtendedTableCursors2N103( ) ;
      }

      protected void Update2N103( )
      {
         BeforeValidate2N103( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2N103( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2N103( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2N103( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2N103( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002N14 */
                     A840LoteArquivoAnexo_TipoArq = edtLoteArquivoAnexo_Arquivo_Filetype;
                     n840LoteArquivoAnexo_TipoArq = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A840LoteArquivoAnexo_TipoArq", A840LoteArquivoAnexo_TipoArq);
                     A839LoteArquivoAnexo_NomeArq = edtLoteArquivoAnexo_Arquivo_Filename;
                     n839LoteArquivoAnexo_NomeArq = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A839LoteArquivoAnexo_NomeArq", A839LoteArquivoAnexo_NomeArq);
                     pr_default.execute(12, new Object[] {n837LoteArquivoAnexo_Descricao, A837LoteArquivoAnexo_Descricao, n1027LoteArquivoAnexo_Verificador, A1027LoteArquivoAnexo_Verificador, n840LoteArquivoAnexo_TipoArq, A840LoteArquivoAnexo_TipoArq, n839LoteArquivoAnexo_NomeArq, A839LoteArquivoAnexo_NomeArq, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo, A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data});
                     pr_default.close(12);
                     dsDefault.SmartCacheProvider.SetUpdated("LoteArquivoAnexo") ;
                     if ( (pr_default.getStatus(12) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"LoteArquivoAnexo"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2N103( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2N103( ) ;
         }
         CloseExtendedTableCursors2N103( ) ;
      }

      protected void DeferredUpdate2N103( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor T002N15 */
            pr_default.execute(13, new Object[] {n838LoteArquivoAnexo_Arquivo, A838LoteArquivoAnexo_Arquivo, A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data});
            pr_default.close(13);
            dsDefault.SmartCacheProvider.SetUpdated("LoteArquivoAnexo") ;
         }
      }

      protected void delete( )
      {
         BeforeValidate2N103( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2N103( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2N103( ) ;
            AfterConfirm2N103( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2N103( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002N16 */
                  pr_default.execute(14, new Object[] {A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data});
                  pr_default.close(14);
                  dsDefault.SmartCacheProvider.SetUpdated("LoteArquivoAnexo") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode103 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel2N103( ) ;
         Gx_mode = sMode103;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls2N103( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T002N17 */
            pr_default.execute(15, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
            A646TipoDocumento_Nome = T002N17_A646TipoDocumento_Nome[0];
            pr_default.close(15);
         }
      }

      protected void EndLevel2N103( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2N103( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(15);
            context.CommitDataStores( "LoteArquivoAnexo");
            if ( AnyError == 0 )
            {
               ConfirmValues2N0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(15);
            context.RollbackDataStores( "LoteArquivoAnexo");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2N103( )
      {
         /* Scan By routine */
         /* Using cursor T002N18 */
         pr_default.execute(16);
         RcdFound103 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound103 = 1;
            A841LoteArquivoAnexo_LoteCod = T002N18_A841LoteArquivoAnexo_LoteCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A841LoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0)));
            A836LoteArquivoAnexo_Data = T002N18_A836LoteArquivoAnexo_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A836LoteArquivoAnexo_Data", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2N103( )
      {
         /* Scan next routine */
         pr_default.readNext(16);
         RcdFound103 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound103 = 1;
            A841LoteArquivoAnexo_LoteCod = T002N18_A841LoteArquivoAnexo_LoteCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A841LoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0)));
            A836LoteArquivoAnexo_Data = T002N18_A836LoteArquivoAnexo_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A836LoteArquivoAnexo_Data", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
         }
      }

      protected void ScanEnd2N103( )
      {
         pr_default.close(16);
      }

      protected void AfterConfirm2N103( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2N103( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2N103( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2N103( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2N103( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2N103( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2N103( )
      {
         dynTipoDocumento_Codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTipoDocumento_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynTipoDocumento_Codigo.Enabled), 5, 0)));
         edtLoteArquivoAnexo_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLoteArquivoAnexo_Descricao_Enabled), 5, 0)));
         edtLoteArquivoAnexo_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLoteArquivoAnexo_Data_Enabled), 5, 0)));
         edtLoteArquivoAnexo_Arquivo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLoteArquivoAnexo_Arquivo_Enabled), 5, 0)));
         edtLoteArquivoAnexo_NomeArq_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_NomeArq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLoteArquivoAnexo_NomeArq_Enabled), 5, 0)));
         edtLoteArquivoAnexo_TipoArq_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_TipoArq_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLoteArquivoAnexo_TipoArq_Enabled), 5, 0)));
         edtLoteArquivoAnexo_LoteCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_LoteCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLoteArquivoAnexo_LoteCod_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues2N0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051812513867");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("lotearquivoanexo.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7LoteArquivoAnexo_LoteCod) + "," + UrlEncode(DateTimeUtil.FormatDateTimeParm(AV8LoteArquivoAnexo_Data))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z841LoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z841LoteArquivoAnexo_LoteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z836LoteArquivoAnexo_Data", context.localUtil.TToC( Z836LoteArquivoAnexo_Data, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z1027LoteArquivoAnexo_Verificador", Z1027LoteArquivoAnexo_Verificador);
         GxWebStd.gx_hidden_field( context, "Z645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z645TipoDocumento_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A645TipoDocumento_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV10TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV10TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vLOTEARQUIVOANEXO_LOTECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7LoteArquivoAnexo_LoteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vLOTEARQUIVOANEXO_DATA", context.localUtil.TToC( AV8LoteArquivoAnexo_Data, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_TIPODOCUMENTO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Insert_TipoDocumento_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "LOTEARQUIVOANEXO_VERIFICADOR", A1027LoteArquivoAnexo_Verificador);
         GxWebStd.gx_hidden_field( context, "TIPODOCUMENTO_NOME", StringUtil.RTrim( A646TipoDocumento_Nome));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV14Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vLOTEARQUIVOANEXO_LOTECOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7LoteArquivoAnexo_LoteCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vLOTEARQUIVOANEXO_DATA", GetSecureSignedToken( "", context.localUtil.Format( AV8LoteArquivoAnexo_Data, "99/99/99 99:99")));
         GXCCtlgxBlob = "LOTEARQUIVOANEXO_ARQUIVO" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, A838LoteArquivoAnexo_Arquivo);
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GxWebStd.gx_hidden_field( context, "LOTEARQUIVOANEXO_ARQUIVO_Filetype", StringUtil.RTrim( edtLoteArquivoAnexo_Arquivo_Filetype));
         GxWebStd.gx_hidden_field( context, "LOTEARQUIVOANEXO_ARQUIVO_Filename", StringUtil.RTrim( edtLoteArquivoAnexo_Arquivo_Filename));
         GxWebStd.gx_hidden_field( context, "LOTEARQUIVOANEXO_ARQUIVO_Filename", StringUtil.RTrim( edtLoteArquivoAnexo_Arquivo_Filename));
         GxWebStd.gx_hidden_field( context, "LOTEARQUIVOANEXO_ARQUIVO_Filetype", StringUtil.RTrim( edtLoteArquivoAnexo_Arquivo_Filetype));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "LoteArquivoAnexo";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A836LoteArquivoAnexo_Data, "99/99/99 99:99");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A1027LoteArquivoAnexo_Verificador, ""));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("lotearquivoanexo:[SendSecurityCheck value for]"+"LoteArquivoAnexo_Data:"+context.localUtil.Format( A836LoteArquivoAnexo_Data, "99/99/99 99:99"));
         GXUtil.WriteLog("lotearquivoanexo:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("lotearquivoanexo:[SendSecurityCheck value for]"+"LoteArquivoAnexo_Verificador:"+StringUtil.RTrim( context.localUtil.Format( A1027LoteArquivoAnexo_Verificador, "")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("lotearquivoanexo.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7LoteArquivoAnexo_LoteCod) + "," + UrlEncode(DateTimeUtil.FormatDateTimeParm(AV8LoteArquivoAnexo_Data)) ;
      }

      public override String GetPgmname( )
      {
         return "LoteArquivoAnexo" ;
      }

      public override String GetPgmdesc( )
      {
         return "Arquivos Anexos" ;
      }

      protected void InitializeNonKey2N103( )
      {
         A645TipoDocumento_Codigo = 0;
         n645TipoDocumento_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
         n645TipoDocumento_Codigo = ((0==A645TipoDocumento_Codigo) ? true : false);
         A838LoteArquivoAnexo_Arquivo = "";
         n838LoteArquivoAnexo_Arquivo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A838LoteArquivoAnexo_Arquivo", A838LoteArquivoAnexo_Arquivo);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A838LoteArquivoAnexo_Arquivo));
         n838LoteArquivoAnexo_Arquivo = (String.IsNullOrEmpty(StringUtil.RTrim( A838LoteArquivoAnexo_Arquivo)) ? true : false);
         A837LoteArquivoAnexo_Descricao = "";
         n837LoteArquivoAnexo_Descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A837LoteArquivoAnexo_Descricao", A837LoteArquivoAnexo_Descricao);
         n837LoteArquivoAnexo_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A837LoteArquivoAnexo_Descricao)) ? true : false);
         A646TipoDocumento_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A646TipoDocumento_Nome", A646TipoDocumento_Nome);
         A1027LoteArquivoAnexo_Verificador = "";
         n1027LoteArquivoAnexo_Verificador = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1027LoteArquivoAnexo_Verificador", A1027LoteArquivoAnexo_Verificador);
         A840LoteArquivoAnexo_TipoArq = "";
         n840LoteArquivoAnexo_TipoArq = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A840LoteArquivoAnexo_TipoArq", A840LoteArquivoAnexo_TipoArq);
         n840LoteArquivoAnexo_TipoArq = (String.IsNullOrEmpty(StringUtil.RTrim( A840LoteArquivoAnexo_TipoArq)) ? true : false);
         A839LoteArquivoAnexo_NomeArq = "";
         n839LoteArquivoAnexo_NomeArq = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A839LoteArquivoAnexo_NomeArq", A839LoteArquivoAnexo_NomeArq);
         n839LoteArquivoAnexo_NomeArq = (String.IsNullOrEmpty(StringUtil.RTrim( A839LoteArquivoAnexo_NomeArq)) ? true : false);
         Z1027LoteArquivoAnexo_Verificador = "";
         Z645TipoDocumento_Codigo = 0;
      }

      protected void InitAll2N103( )
      {
         A841LoteArquivoAnexo_LoteCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A841LoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0)));
         A836LoteArquivoAnexo_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A836LoteArquivoAnexo_Data", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
         InitializeNonKey2N103( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205181251392");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("lotearquivoanexo.js", "?20205181251393");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocktipodocumento_codigo_Internalname = "TEXTBLOCKTIPODOCUMENTO_CODIGO";
         dynTipoDocumento_Codigo_Internalname = "TIPODOCUMENTO_CODIGO";
         lblTextblocklotearquivoanexo_descricao_Internalname = "TEXTBLOCKLOTEARQUIVOANEXO_DESCRICAO";
         edtLoteArquivoAnexo_Descricao_Internalname = "LOTEARQUIVOANEXO_DESCRICAO";
         lblTextblocklotearquivoanexo_data_Internalname = "TEXTBLOCKLOTEARQUIVOANEXO_DATA";
         edtLoteArquivoAnexo_Data_Internalname = "LOTEARQUIVOANEXO_DATA";
         lblTextblocklotearquivoanexo_arquivo_Internalname = "TEXTBLOCKLOTEARQUIVOANEXO_ARQUIVO";
         edtLoteArquivoAnexo_Arquivo_Internalname = "LOTEARQUIVOANEXO_ARQUIVO";
         lblTextblocklotearquivoanexo_nomearq_Internalname = "TEXTBLOCKLOTEARQUIVOANEXO_NOMEARQ";
         edtLoteArquivoAnexo_NomeArq_Internalname = "LOTEARQUIVOANEXO_NOMEARQ";
         lblTextblocklotearquivoanexo_tipoarq_Internalname = "TEXTBLOCKLOTEARQUIVOANEXO_TIPOARQ";
         edtLoteArquivoAnexo_TipoArq_Internalname = "LOTEARQUIVOANEXO_TIPOARQ";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtLoteArquivoAnexo_LoteCod_Internalname = "LOTEARQUIVOANEXO_LOTECOD";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Arquivo Anexo";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Arquivos Anexos";
         edtLoteArquivoAnexo_Arquivo_Filename = "";
         edtLoteArquivoAnexo_TipoArq_Jsonclick = "";
         edtLoteArquivoAnexo_TipoArq_Enabled = 0;
         edtLoteArquivoAnexo_NomeArq_Jsonclick = "";
         edtLoteArquivoAnexo_NomeArq_Enabled = 0;
         edtLoteArquivoAnexo_Arquivo_Jsonclick = "";
         edtLoteArquivoAnexo_Arquivo_Parameters = "";
         edtLoteArquivoAnexo_Arquivo_Contenttype = "";
         edtLoteArquivoAnexo_Arquivo_Filetype = "";
         edtLoteArquivoAnexo_Arquivo_Linktarget = "";
         edtLoteArquivoAnexo_Arquivo_Display = 0;
         edtLoteArquivoAnexo_Arquivo_Enabled = 1;
         edtLoteArquivoAnexo_Data_Jsonclick = "";
         edtLoteArquivoAnexo_Data_Enabled = 0;
         edtLoteArquivoAnexo_Descricao_Enabled = 1;
         dynTipoDocumento_Codigo_Jsonclick = "";
         dynTipoDocumento_Codigo.Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtLoteArquivoAnexo_LoteCod_Jsonclick = "";
         edtLoteArquivoAnexo_LoteCod_Enabled = 1;
         edtLoteArquivoAnexo_LoteCod_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLATIPODOCUMENTO_CODIGO2N1( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLATIPODOCUMENTO_CODIGO_data2N1( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXATIPODOCUMENTO_CODIGO_html2N1( )
      {
         int gxdynajaxvalue ;
         GXDLATIPODOCUMENTO_CODIGO_data2N1( ) ;
         gxdynajaxindex = 1;
         dynTipoDocumento_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynTipoDocumento_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLATIPODOCUMENTO_CODIGO_data2N1( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor T002N19 */
         pr_default.execute(17);
         while ( (pr_default.getStatus(17) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T002N19_A645TipoDocumento_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T002N19_A646TipoDocumento_Nome[0]));
            pr_default.readNext(17);
         }
         pr_default.close(17);
      }

      public void Valid_Lotearquivoanexo_lotecod( int GX_Parm1 )
      {
         A841LoteArquivoAnexo_LoteCod = GX_Parm1;
         /* Using cursor T002N20 */
         pr_default.execute(18, new Object[] {A841LoteArquivoAnexo_LoteCod});
         if ( (pr_default.getStatus(18) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Lote Arquivo Anexo_Lote'.", "ForeignKeyNotFound", 1, "LOTEARQUIVOANEXO_LOTECOD");
            AnyError = 1;
            GX_FocusControl = edtLoteArquivoAnexo_LoteCod_Internalname;
         }
         pr_default.close(18);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Tipodocumento_codigo( GXCombobox dynGX_Parm1 ,
                                              String GX_Parm2 )
      {
         dynTipoDocumento_Codigo = dynGX_Parm1;
         A645TipoDocumento_Codigo = (int)(NumberUtil.Val( dynTipoDocumento_Codigo.CurrentValue, "."));
         n645TipoDocumento_Codigo = false;
         A646TipoDocumento_Nome = GX_Parm2;
         /* Using cursor T002N17 */
         pr_default.execute(15, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
         if ( (pr_default.getStatus(15) == 101) )
         {
            if ( ! ( (0==A645TipoDocumento_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Tipo de Documentos'.", "ForeignKeyNotFound", 1, "TIPODOCUMENTO_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynTipoDocumento_Codigo_Internalname;
            }
         }
         A646TipoDocumento_Nome = T002N17_A646TipoDocumento_Nome[0];
         pr_default.close(15);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A646TipoDocumento_Nome = "";
         }
         isValidOutput.Add(StringUtil.RTrim( A646TipoDocumento_Nome));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7LoteArquivoAnexo_LoteCod',fld:'vLOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV8LoteArquivoAnexo_Data',fld:'vLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',hsh:true,nv:''}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E122N2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV10TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(15);
         pr_default.close(18);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         wcpOAV8LoteArquivoAnexo_Data = (DateTime)(DateTime.MinValue);
         Z836LoteArquivoAnexo_Data = (DateTime)(DateTime.MinValue);
         Z1027LoteArquivoAnexo_Verificador = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         T002N6_A645TipoDocumento_Codigo = new int[1] ;
         T002N6_n645TipoDocumento_Codigo = new bool[] {false} ;
         T002N6_A646TipoDocumento_Nome = new String[] {""} ;
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblocktipodocumento_codigo_Jsonclick = "";
         lblTextblocklotearquivoanexo_descricao_Jsonclick = "";
         A837LoteArquivoAnexo_Descricao = "";
         lblTextblocklotearquivoanexo_data_Jsonclick = "";
         A836LoteArquivoAnexo_Data = (DateTime)(DateTime.MinValue);
         lblTextblocklotearquivoanexo_arquivo_Jsonclick = "";
         A839LoteArquivoAnexo_NomeArq = "";
         A840LoteArquivoAnexo_TipoArq = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         A838LoteArquivoAnexo_Arquivo = "";
         lblTextblocklotearquivoanexo_nomearq_Jsonclick = "";
         lblTextblocklotearquivoanexo_tipoarq_Jsonclick = "";
         A1027LoteArquivoAnexo_Verificador = "";
         A646TipoDocumento_Nome = "";
         AV14Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         GXCCtlgxBlob = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode103 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11WebSession = context.GetSession();
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z838LoteArquivoAnexo_Arquivo = "";
         Z837LoteArquivoAnexo_Descricao = "";
         Z840LoteArquivoAnexo_TipoArq = "";
         Z839LoteArquivoAnexo_NomeArq = "";
         Z646TipoDocumento_Nome = "";
         T002N4_A646TipoDocumento_Nome = new String[] {""} ;
         T002N7_A836LoteArquivoAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         T002N7_A837LoteArquivoAnexo_Descricao = new String[] {""} ;
         T002N7_n837LoteArquivoAnexo_Descricao = new bool[] {false} ;
         T002N7_A646TipoDocumento_Nome = new String[] {""} ;
         T002N7_A1027LoteArquivoAnexo_Verificador = new String[] {""} ;
         T002N7_n1027LoteArquivoAnexo_Verificador = new bool[] {false} ;
         T002N7_A840LoteArquivoAnexo_TipoArq = new String[] {""} ;
         T002N7_n840LoteArquivoAnexo_TipoArq = new bool[] {false} ;
         T002N7_A839LoteArquivoAnexo_NomeArq = new String[] {""} ;
         T002N7_n839LoteArquivoAnexo_NomeArq = new bool[] {false} ;
         T002N7_A645TipoDocumento_Codigo = new int[1] ;
         T002N7_n645TipoDocumento_Codigo = new bool[] {false} ;
         T002N7_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         T002N7_A838LoteArquivoAnexo_Arquivo = new String[] {""} ;
         T002N7_n838LoteArquivoAnexo_Arquivo = new bool[] {false} ;
         T002N5_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         T002N8_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         T002N9_A646TipoDocumento_Nome = new String[] {""} ;
         T002N10_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         T002N10_A836LoteArquivoAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         T002N3_A836LoteArquivoAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         T002N3_A837LoteArquivoAnexo_Descricao = new String[] {""} ;
         T002N3_n837LoteArquivoAnexo_Descricao = new bool[] {false} ;
         T002N3_A1027LoteArquivoAnexo_Verificador = new String[] {""} ;
         T002N3_n1027LoteArquivoAnexo_Verificador = new bool[] {false} ;
         T002N3_A840LoteArquivoAnexo_TipoArq = new String[] {""} ;
         T002N3_n840LoteArquivoAnexo_TipoArq = new bool[] {false} ;
         T002N3_A839LoteArquivoAnexo_NomeArq = new String[] {""} ;
         T002N3_n839LoteArquivoAnexo_NomeArq = new bool[] {false} ;
         T002N3_A645TipoDocumento_Codigo = new int[1] ;
         T002N3_n645TipoDocumento_Codigo = new bool[] {false} ;
         T002N3_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         T002N3_A838LoteArquivoAnexo_Arquivo = new String[] {""} ;
         T002N3_n838LoteArquivoAnexo_Arquivo = new bool[] {false} ;
         T002N11_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         T002N11_A836LoteArquivoAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         T002N12_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         T002N12_A836LoteArquivoAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         T002N2_A836LoteArquivoAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         T002N2_A837LoteArquivoAnexo_Descricao = new String[] {""} ;
         T002N2_n837LoteArquivoAnexo_Descricao = new bool[] {false} ;
         T002N2_A1027LoteArquivoAnexo_Verificador = new String[] {""} ;
         T002N2_n1027LoteArquivoAnexo_Verificador = new bool[] {false} ;
         T002N2_A840LoteArquivoAnexo_TipoArq = new String[] {""} ;
         T002N2_n840LoteArquivoAnexo_TipoArq = new bool[] {false} ;
         T002N2_A839LoteArquivoAnexo_NomeArq = new String[] {""} ;
         T002N2_n839LoteArquivoAnexo_NomeArq = new bool[] {false} ;
         T002N2_A645TipoDocumento_Codigo = new int[1] ;
         T002N2_n645TipoDocumento_Codigo = new bool[] {false} ;
         T002N2_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         T002N2_A838LoteArquivoAnexo_Arquivo = new String[] {""} ;
         T002N2_n838LoteArquivoAnexo_Arquivo = new bool[] {false} ;
         T002N17_A646TipoDocumento_Nome = new String[] {""} ;
         T002N18_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         T002N18_A836LoteArquivoAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T002N19_A645TipoDocumento_Codigo = new int[1] ;
         T002N19_n645TipoDocumento_Codigo = new bool[] {false} ;
         T002N19_A646TipoDocumento_Nome = new String[] {""} ;
         T002N20_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.lotearquivoanexo__default(),
            new Object[][] {
                new Object[] {
               T002N2_A836LoteArquivoAnexo_Data, T002N2_A837LoteArquivoAnexo_Descricao, T002N2_n837LoteArquivoAnexo_Descricao, T002N2_A1027LoteArquivoAnexo_Verificador, T002N2_n1027LoteArquivoAnexo_Verificador, T002N2_A840LoteArquivoAnexo_TipoArq, T002N2_n840LoteArquivoAnexo_TipoArq, T002N2_A839LoteArquivoAnexo_NomeArq, T002N2_n839LoteArquivoAnexo_NomeArq, T002N2_A645TipoDocumento_Codigo,
               T002N2_n645TipoDocumento_Codigo, T002N2_A841LoteArquivoAnexo_LoteCod, T002N2_A838LoteArquivoAnexo_Arquivo, T002N2_n838LoteArquivoAnexo_Arquivo
               }
               , new Object[] {
               T002N3_A836LoteArquivoAnexo_Data, T002N3_A837LoteArquivoAnexo_Descricao, T002N3_n837LoteArquivoAnexo_Descricao, T002N3_A1027LoteArquivoAnexo_Verificador, T002N3_n1027LoteArquivoAnexo_Verificador, T002N3_A840LoteArquivoAnexo_TipoArq, T002N3_n840LoteArquivoAnexo_TipoArq, T002N3_A839LoteArquivoAnexo_NomeArq, T002N3_n839LoteArquivoAnexo_NomeArq, T002N3_A645TipoDocumento_Codigo,
               T002N3_n645TipoDocumento_Codigo, T002N3_A841LoteArquivoAnexo_LoteCod, T002N3_A838LoteArquivoAnexo_Arquivo, T002N3_n838LoteArquivoAnexo_Arquivo
               }
               , new Object[] {
               T002N4_A646TipoDocumento_Nome
               }
               , new Object[] {
               T002N5_A841LoteArquivoAnexo_LoteCod
               }
               , new Object[] {
               T002N6_A645TipoDocumento_Codigo, T002N6_A646TipoDocumento_Nome
               }
               , new Object[] {
               T002N7_A836LoteArquivoAnexo_Data, T002N7_A837LoteArquivoAnexo_Descricao, T002N7_n837LoteArquivoAnexo_Descricao, T002N7_A646TipoDocumento_Nome, T002N7_A1027LoteArquivoAnexo_Verificador, T002N7_n1027LoteArquivoAnexo_Verificador, T002N7_A840LoteArquivoAnexo_TipoArq, T002N7_n840LoteArquivoAnexo_TipoArq, T002N7_A839LoteArquivoAnexo_NomeArq, T002N7_n839LoteArquivoAnexo_NomeArq,
               T002N7_A645TipoDocumento_Codigo, T002N7_n645TipoDocumento_Codigo, T002N7_A841LoteArquivoAnexo_LoteCod, T002N7_A838LoteArquivoAnexo_Arquivo, T002N7_n838LoteArquivoAnexo_Arquivo
               }
               , new Object[] {
               T002N8_A841LoteArquivoAnexo_LoteCod
               }
               , new Object[] {
               T002N9_A646TipoDocumento_Nome
               }
               , new Object[] {
               T002N10_A841LoteArquivoAnexo_LoteCod, T002N10_A836LoteArquivoAnexo_Data
               }
               , new Object[] {
               T002N11_A841LoteArquivoAnexo_LoteCod, T002N11_A836LoteArquivoAnexo_Data
               }
               , new Object[] {
               T002N12_A841LoteArquivoAnexo_LoteCod, T002N12_A836LoteArquivoAnexo_Data
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002N17_A646TipoDocumento_Nome
               }
               , new Object[] {
               T002N18_A841LoteArquivoAnexo_LoteCod, T002N18_A836LoteArquivoAnexo_Data
               }
               , new Object[] {
               T002N19_A645TipoDocumento_Codigo, T002N19_A646TipoDocumento_Nome
               }
               , new Object[] {
               T002N20_A841LoteArquivoAnexo_LoteCod
               }
            }
         );
         Z836LoteArquivoAnexo_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         A836LoteArquivoAnexo_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         AV14Pgmname = "LoteArquivoAnexo";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short edtLoteArquivoAnexo_Arquivo_Display ;
      private short Gx_BScreen ;
      private short RcdFound103 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7LoteArquivoAnexo_LoteCod ;
      private int Z841LoteArquivoAnexo_LoteCod ;
      private int Z645TipoDocumento_Codigo ;
      private int N645TipoDocumento_Codigo ;
      private int A841LoteArquivoAnexo_LoteCod ;
      private int A645TipoDocumento_Codigo ;
      private int AV7LoteArquivoAnexo_LoteCod ;
      private int trnEnded ;
      private int edtLoteArquivoAnexo_LoteCod_Visible ;
      private int edtLoteArquivoAnexo_LoteCod_Enabled ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtLoteArquivoAnexo_Descricao_Enabled ;
      private int edtLoteArquivoAnexo_Data_Enabled ;
      private int edtLoteArquivoAnexo_Arquivo_Enabled ;
      private int edtLoteArquivoAnexo_NomeArq_Enabled ;
      private int edtLoteArquivoAnexo_TipoArq_Enabled ;
      private int AV12Insert_TipoDocumento_Codigo ;
      private int AV15GXV1 ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String dynTipoDocumento_Codigo_Internalname ;
      private String TempTags ;
      private String edtLoteArquivoAnexo_LoteCod_Internalname ;
      private String edtLoteArquivoAnexo_LoteCod_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocktipodocumento_codigo_Internalname ;
      private String lblTextblocktipodocumento_codigo_Jsonclick ;
      private String dynTipoDocumento_Codigo_Jsonclick ;
      private String lblTextblocklotearquivoanexo_descricao_Internalname ;
      private String lblTextblocklotearquivoanexo_descricao_Jsonclick ;
      private String edtLoteArquivoAnexo_Descricao_Internalname ;
      private String lblTextblocklotearquivoanexo_data_Internalname ;
      private String lblTextblocklotearquivoanexo_data_Jsonclick ;
      private String edtLoteArquivoAnexo_Data_Internalname ;
      private String edtLoteArquivoAnexo_Data_Jsonclick ;
      private String lblTextblocklotearquivoanexo_arquivo_Internalname ;
      private String lblTextblocklotearquivoanexo_arquivo_Jsonclick ;
      private String edtLoteArquivoAnexo_Arquivo_Filename ;
      private String A839LoteArquivoAnexo_NomeArq ;
      private String edtLoteArquivoAnexo_Arquivo_Filetype ;
      private String edtLoteArquivoAnexo_Arquivo_Internalname ;
      private String A840LoteArquivoAnexo_TipoArq ;
      private String edtLoteArquivoAnexo_Arquivo_Contenttype ;
      private String edtLoteArquivoAnexo_Arquivo_Linktarget ;
      private String edtLoteArquivoAnexo_Arquivo_Parameters ;
      private String edtLoteArquivoAnexo_Arquivo_Jsonclick ;
      private String lblTextblocklotearquivoanexo_nomearq_Internalname ;
      private String lblTextblocklotearquivoanexo_nomearq_Jsonclick ;
      private String edtLoteArquivoAnexo_NomeArq_Internalname ;
      private String edtLoteArquivoAnexo_NomeArq_Jsonclick ;
      private String lblTextblocklotearquivoanexo_tipoarq_Internalname ;
      private String lblTextblocklotearquivoanexo_tipoarq_Jsonclick ;
      private String edtLoteArquivoAnexo_TipoArq_Internalname ;
      private String edtLoteArquivoAnexo_TipoArq_Jsonclick ;
      private String A646TipoDocumento_Nome ;
      private String AV14Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String GXCCtlgxBlob ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode103 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z840LoteArquivoAnexo_TipoArq ;
      private String Z839LoteArquivoAnexo_NomeArq ;
      private String Z646TipoDocumento_Nome ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private DateTime wcpOAV8LoteArquivoAnexo_Data ;
      private DateTime Z836LoteArquivoAnexo_Data ;
      private DateTime AV8LoteArquivoAnexo_Data ;
      private DateTime A836LoteArquivoAnexo_Data ;
      private bool entryPointCalled ;
      private bool n645TipoDocumento_Codigo ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n838LoteArquivoAnexo_Arquivo ;
      private bool n837LoteArquivoAnexo_Descricao ;
      private bool n1027LoteArquivoAnexo_Verificador ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool n840LoteArquivoAnexo_TipoArq ;
      private bool n839LoteArquivoAnexo_NomeArq ;
      private bool returnInSub ;
      private String A837LoteArquivoAnexo_Descricao ;
      private String Z837LoteArquivoAnexo_Descricao ;
      private String Z1027LoteArquivoAnexo_Verificador ;
      private String A1027LoteArquivoAnexo_Verificador ;
      private String A838LoteArquivoAnexo_Arquivo ;
      private String Z838LoteArquivoAnexo_Arquivo ;
      private IGxSession AV11WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private GxFile gxblobfileaux ;
      private IDataStoreProvider pr_default ;
      private int[] T002N6_A645TipoDocumento_Codigo ;
      private bool[] T002N6_n645TipoDocumento_Codigo ;
      private String[] T002N6_A646TipoDocumento_Nome ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynTipoDocumento_Codigo ;
      private String[] T002N4_A646TipoDocumento_Nome ;
      private DateTime[] T002N7_A836LoteArquivoAnexo_Data ;
      private String[] T002N7_A837LoteArquivoAnexo_Descricao ;
      private bool[] T002N7_n837LoteArquivoAnexo_Descricao ;
      private String[] T002N7_A646TipoDocumento_Nome ;
      private String[] T002N7_A1027LoteArquivoAnexo_Verificador ;
      private bool[] T002N7_n1027LoteArquivoAnexo_Verificador ;
      private String[] T002N7_A840LoteArquivoAnexo_TipoArq ;
      private bool[] T002N7_n840LoteArquivoAnexo_TipoArq ;
      private String[] T002N7_A839LoteArquivoAnexo_NomeArq ;
      private bool[] T002N7_n839LoteArquivoAnexo_NomeArq ;
      private int[] T002N7_A645TipoDocumento_Codigo ;
      private bool[] T002N7_n645TipoDocumento_Codigo ;
      private int[] T002N7_A841LoteArquivoAnexo_LoteCod ;
      private String[] T002N7_A838LoteArquivoAnexo_Arquivo ;
      private bool[] T002N7_n838LoteArquivoAnexo_Arquivo ;
      private int[] T002N5_A841LoteArquivoAnexo_LoteCod ;
      private int[] T002N8_A841LoteArquivoAnexo_LoteCod ;
      private String[] T002N9_A646TipoDocumento_Nome ;
      private int[] T002N10_A841LoteArquivoAnexo_LoteCod ;
      private DateTime[] T002N10_A836LoteArquivoAnexo_Data ;
      private DateTime[] T002N3_A836LoteArquivoAnexo_Data ;
      private String[] T002N3_A837LoteArquivoAnexo_Descricao ;
      private bool[] T002N3_n837LoteArquivoAnexo_Descricao ;
      private String[] T002N3_A1027LoteArquivoAnexo_Verificador ;
      private bool[] T002N3_n1027LoteArquivoAnexo_Verificador ;
      private String[] T002N3_A840LoteArquivoAnexo_TipoArq ;
      private bool[] T002N3_n840LoteArquivoAnexo_TipoArq ;
      private String[] T002N3_A839LoteArquivoAnexo_NomeArq ;
      private bool[] T002N3_n839LoteArquivoAnexo_NomeArq ;
      private int[] T002N3_A645TipoDocumento_Codigo ;
      private bool[] T002N3_n645TipoDocumento_Codigo ;
      private int[] T002N3_A841LoteArquivoAnexo_LoteCod ;
      private String[] T002N3_A838LoteArquivoAnexo_Arquivo ;
      private bool[] T002N3_n838LoteArquivoAnexo_Arquivo ;
      private int[] T002N11_A841LoteArquivoAnexo_LoteCod ;
      private DateTime[] T002N11_A836LoteArquivoAnexo_Data ;
      private int[] T002N12_A841LoteArquivoAnexo_LoteCod ;
      private DateTime[] T002N12_A836LoteArquivoAnexo_Data ;
      private DateTime[] T002N2_A836LoteArquivoAnexo_Data ;
      private String[] T002N2_A837LoteArquivoAnexo_Descricao ;
      private bool[] T002N2_n837LoteArquivoAnexo_Descricao ;
      private String[] T002N2_A1027LoteArquivoAnexo_Verificador ;
      private bool[] T002N2_n1027LoteArquivoAnexo_Verificador ;
      private String[] T002N2_A840LoteArquivoAnexo_TipoArq ;
      private bool[] T002N2_n840LoteArquivoAnexo_TipoArq ;
      private String[] T002N2_A839LoteArquivoAnexo_NomeArq ;
      private bool[] T002N2_n839LoteArquivoAnexo_NomeArq ;
      private int[] T002N2_A645TipoDocumento_Codigo ;
      private bool[] T002N2_n645TipoDocumento_Codigo ;
      private int[] T002N2_A841LoteArquivoAnexo_LoteCod ;
      private String[] T002N2_A838LoteArquivoAnexo_Arquivo ;
      private bool[] T002N2_n838LoteArquivoAnexo_Arquivo ;
      private String[] T002N17_A646TipoDocumento_Nome ;
      private int[] T002N18_A841LoteArquivoAnexo_LoteCod ;
      private DateTime[] T002N18_A836LoteArquivoAnexo_Data ;
      private int[] T002N19_A645TipoDocumento_Codigo ;
      private bool[] T002N19_n645TipoDocumento_Codigo ;
      private String[] T002N19_A646TipoDocumento_Nome ;
      private int[] T002N20_A841LoteArquivoAnexo_LoteCod ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV10TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
   }

   public class lotearquivoanexo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new UpdateCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT002N6 ;
          prmT002N6 = new Object[] {
          } ;
          Object[] prmT002N7 ;
          prmT002N7 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LoteArquivoAnexo_Data",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmT002N5 ;
          prmT002N5 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002N4 ;
          prmT002N4 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002N8 ;
          prmT002N8 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002N9 ;
          prmT002N9 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002N10 ;
          prmT002N10 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LoteArquivoAnexo_Data",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmT002N3 ;
          prmT002N3 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LoteArquivoAnexo_Data",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmT002N11 ;
          prmT002N11 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LoteArquivoAnexo_Data",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmT002N12 ;
          prmT002N12 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LoteArquivoAnexo_Data",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmT002N2 ;
          prmT002N2 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LoteArquivoAnexo_Data",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmT002N13 ;
          prmT002N13 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@LoteArquivoAnexo_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@LoteArquivoAnexo_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@LoteArquivoAnexo_Verificador",SqlDbType.VarChar,40,0} ,
          new Object[] {"@LoteArquivoAnexo_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@LoteArquivoAnexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002N14 ;
          prmT002N14 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@LoteArquivoAnexo_Verificador",SqlDbType.VarChar,40,0} ,
          new Object[] {"@LoteArquivoAnexo_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@LoteArquivoAnexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LoteArquivoAnexo_Data",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmT002N15 ;
          prmT002N15 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LoteArquivoAnexo_Data",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmT002N16 ;
          prmT002N16 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LoteArquivoAnexo_Data",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmT002N18 ;
          prmT002N18 = new Object[] {
          } ;
          Object[] prmT002N19 ;
          prmT002N19 = new Object[] {
          } ;
          Object[] prmT002N20 ;
          prmT002N20 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002N17 ;
          prmT002N17 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T002N2", "SELECT [LoteArquivoAnexo_Data], [LoteArquivoAnexo_Descricao], [LoteArquivoAnexo_Verificador], [LoteArquivoAnexo_TipoArq], [LoteArquivoAnexo_NomeArq], [TipoDocumento_Codigo], [LoteArquivoAnexo_LoteCod] AS LoteArquivoAnexo_LoteCod, [LoteArquivoAnexo_Arquivo] FROM [LoteArquivoAnexo] WITH (UPDLOCK) WHERE [LoteArquivoAnexo_LoteCod] = @LoteArquivoAnexo_LoteCod AND [LoteArquivoAnexo_Data] = @LoteArquivoAnexo_Data ",true, GxErrorMask.GX_NOMASK, false, this,prmT002N2,1,0,true,false )
             ,new CursorDef("T002N3", "SELECT [LoteArquivoAnexo_Data], [LoteArquivoAnexo_Descricao], [LoteArquivoAnexo_Verificador], [LoteArquivoAnexo_TipoArq], [LoteArquivoAnexo_NomeArq], [TipoDocumento_Codigo], [LoteArquivoAnexo_LoteCod] AS LoteArquivoAnexo_LoteCod, [LoteArquivoAnexo_Arquivo] FROM [LoteArquivoAnexo] WITH (NOLOCK) WHERE [LoteArquivoAnexo_LoteCod] = @LoteArquivoAnexo_LoteCod AND [LoteArquivoAnexo_Data] = @LoteArquivoAnexo_Data ",true, GxErrorMask.GX_NOMASK, false, this,prmT002N3,1,0,true,false )
             ,new CursorDef("T002N4", "SELECT [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002N4,1,0,true,false )
             ,new CursorDef("T002N5", "SELECT [Lote_Codigo] AS LoteArquivoAnexo_LoteCod FROM [Lote] WITH (NOLOCK) WHERE [Lote_Codigo] = @LoteArquivoAnexo_LoteCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002N5,1,0,true,false )
             ,new CursorDef("T002N6", "SELECT [TipoDocumento_Codigo], [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) ORDER BY [TipoDocumento_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002N6,0,0,true,false )
             ,new CursorDef("T002N7", "SELECT TM1.[LoteArquivoAnexo_Data], TM1.[LoteArquivoAnexo_Descricao], T2.[TipoDocumento_Nome], TM1.[LoteArquivoAnexo_Verificador], TM1.[LoteArquivoAnexo_TipoArq], TM1.[LoteArquivoAnexo_NomeArq], TM1.[TipoDocumento_Codigo], TM1.[LoteArquivoAnexo_LoteCod] AS LoteArquivoAnexo_LoteCod, TM1.[LoteArquivoAnexo_Arquivo] FROM ([LoteArquivoAnexo] TM1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = TM1.[TipoDocumento_Codigo]) WHERE TM1.[LoteArquivoAnexo_LoteCod] = @LoteArquivoAnexo_LoteCod and TM1.[LoteArquivoAnexo_Data] = @LoteArquivoAnexo_Data ORDER BY TM1.[LoteArquivoAnexo_LoteCod], TM1.[LoteArquivoAnexo_Data]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002N7,100,0,true,false )
             ,new CursorDef("T002N8", "SELECT [Lote_Codigo] AS LoteArquivoAnexo_LoteCod FROM [Lote] WITH (NOLOCK) WHERE [Lote_Codigo] = @LoteArquivoAnexo_LoteCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002N8,1,0,true,false )
             ,new CursorDef("T002N9", "SELECT [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002N9,1,0,true,false )
             ,new CursorDef("T002N10", "SELECT [LoteArquivoAnexo_LoteCod] AS LoteArquivoAnexo_LoteCod, [LoteArquivoAnexo_Data] FROM [LoteArquivoAnexo] WITH (NOLOCK) WHERE [LoteArquivoAnexo_LoteCod] = @LoteArquivoAnexo_LoteCod AND [LoteArquivoAnexo_Data] = @LoteArquivoAnexo_Data  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002N10,1,0,true,false )
             ,new CursorDef("T002N11", "SELECT TOP 1 [LoteArquivoAnexo_LoteCod] AS LoteArquivoAnexo_LoteCod, [LoteArquivoAnexo_Data] FROM [LoteArquivoAnexo] WITH (NOLOCK) WHERE ( [LoteArquivoAnexo_LoteCod] > @LoteArquivoAnexo_LoteCod or [LoteArquivoAnexo_LoteCod] = @LoteArquivoAnexo_LoteCod and [LoteArquivoAnexo_Data] > @LoteArquivoAnexo_Data) ORDER BY [LoteArquivoAnexo_LoteCod], [LoteArquivoAnexo_Data]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002N11,1,0,true,true )
             ,new CursorDef("T002N12", "SELECT TOP 1 [LoteArquivoAnexo_LoteCod] AS LoteArquivoAnexo_LoteCod, [LoteArquivoAnexo_Data] FROM [LoteArquivoAnexo] WITH (NOLOCK) WHERE ( [LoteArquivoAnexo_LoteCod] < @LoteArquivoAnexo_LoteCod or [LoteArquivoAnexo_LoteCod] = @LoteArquivoAnexo_LoteCod and [LoteArquivoAnexo_Data] < @LoteArquivoAnexo_Data) ORDER BY [LoteArquivoAnexo_LoteCod] DESC, [LoteArquivoAnexo_Data] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002N12,1,0,true,true )
             ,new CursorDef("T002N13", "INSERT INTO [LoteArquivoAnexo]([LoteArquivoAnexo_Data], [LoteArquivoAnexo_Arquivo], [LoteArquivoAnexo_Descricao], [LoteArquivoAnexo_Verificador], [LoteArquivoAnexo_TipoArq], [LoteArquivoAnexo_NomeArq], [TipoDocumento_Codigo], [LoteArquivoAnexo_LoteCod]) VALUES(@LoteArquivoAnexo_Data, @LoteArquivoAnexo_Arquivo, @LoteArquivoAnexo_Descricao, @LoteArquivoAnexo_Verificador, @LoteArquivoAnexo_TipoArq, @LoteArquivoAnexo_NomeArq, @TipoDocumento_Codigo, @LoteArquivoAnexo_LoteCod)", GxErrorMask.GX_NOMASK,prmT002N13)
             ,new CursorDef("T002N14", "UPDATE [LoteArquivoAnexo] SET [LoteArquivoAnexo_Descricao]=@LoteArquivoAnexo_Descricao, [LoteArquivoAnexo_Verificador]=@LoteArquivoAnexo_Verificador, [LoteArquivoAnexo_TipoArq]=@LoteArquivoAnexo_TipoArq, [LoteArquivoAnexo_NomeArq]=@LoteArquivoAnexo_NomeArq, [TipoDocumento_Codigo]=@TipoDocumento_Codigo  WHERE [LoteArquivoAnexo_LoteCod] = @LoteArquivoAnexo_LoteCod AND [LoteArquivoAnexo_Data] = @LoteArquivoAnexo_Data", GxErrorMask.GX_NOMASK,prmT002N14)
             ,new CursorDef("T002N15", "UPDATE [LoteArquivoAnexo] SET [LoteArquivoAnexo_Arquivo]=@LoteArquivoAnexo_Arquivo  WHERE [LoteArquivoAnexo_LoteCod] = @LoteArquivoAnexo_LoteCod AND [LoteArquivoAnexo_Data] = @LoteArquivoAnexo_Data", GxErrorMask.GX_NOMASK,prmT002N15)
             ,new CursorDef("T002N16", "DELETE FROM [LoteArquivoAnexo]  WHERE [LoteArquivoAnexo_LoteCod] = @LoteArquivoAnexo_LoteCod AND [LoteArquivoAnexo_Data] = @LoteArquivoAnexo_Data", GxErrorMask.GX_NOMASK,prmT002N16)
             ,new CursorDef("T002N17", "SELECT [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002N17,1,0,true,false )
             ,new CursorDef("T002N18", "SELECT [LoteArquivoAnexo_LoteCod] AS LoteArquivoAnexo_LoteCod, [LoteArquivoAnexo_Data] FROM [LoteArquivoAnexo] WITH (NOLOCK) ORDER BY [LoteArquivoAnexo_LoteCod], [LoteArquivoAnexo_Data]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002N18,100,0,true,false )
             ,new CursorDef("T002N19", "SELECT [TipoDocumento_Codigo], [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) ORDER BY [TipoDocumento_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002N19,0,0,true,false )
             ,new CursorDef("T002N20", "SELECT [Lote_Codigo] AS LoteArquivoAnexo_LoteCod FROM [Lote] WITH (NOLOCK) WHERE [Lote_Codigo] = @LoteArquivoAnexo_LoteCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002N20,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDateTime(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 10) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((String[]) buf[12])[0] = rslt.getBLOBFile(8, rslt.getString(4, 10), rslt.getString(5, 50)) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                return;
             case 1 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDateTime(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 10) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((String[]) buf[12])[0] = rslt.getBLOBFile(8, rslt.getString(4, 10), rslt.getString(5, 50)) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 5 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDateTime(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((String[]) buf[13])[0] = rslt.getBLOBFile(9, rslt.getString(5, 10), rslt.getString(6, 50)) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                return;
             case 11 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[12]);
                }
                stmt.SetParameter(8, (int)parms[13]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[9]);
                }
                stmt.SetParameter(6, (int)parms[10]);
                stmt.SetParameterDatetime(7, (DateTime)parms[11]);
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameterDatetime(3, (DateTime)parms[3]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
