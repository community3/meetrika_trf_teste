/*
               File: WP_ConsultaContratadaGrf
        Description:
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:21:22.12
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_consultacontratadagrf : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_consultacontratadagrf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_consultacontratadagrf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_Codigo ,
                           short aP1_Filtro_Ano ,
                           String aP2_Graficar )
      {
         this.AV30Contratada_Codigo = aP0_Contratada_Codigo;
         this.AV9Filtro_Ano = aP1_Filtro_Ano;
         this.AV29Graficar = aP2_Graficar;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV30Contratada_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30Contratada_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV30Contratada_Codigo), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV9Filtro_Ano = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Filtro_Ano), 4, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFILTRO_ANO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9Filtro_Ano), "ZZZ9")));
                  AV29Graficar = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Graficar", AV29Graficar);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vGRAFICAR", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV29Graficar, ""))));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PACC2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTCC2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216212214");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("GxChart/gxChart.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_consultacontratadagrf.aspx") + "?" + UrlEncode("" +AV30Contratada_Codigo) + "," + UrlEncode("" +AV9Filtro_Ano) + "," + UrlEncode(StringUtil.RTrim(AV29Graficar))+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGXCHARTDATA", GxChartData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGXCHARTDATA", GxChartData);
         }
         GxWebStd.gx_hidden_field( context, "vCONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV30Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vFILTRO_ANO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9Filtro_Ano), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRAFICAR", StringUtil.RTrim( AV29Graficar));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV30Contratada_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vFILTRO_ANO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9Filtro_Ano), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vGRAFICAR", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV29Graficar, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV30Contratada_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vFILTRO_ANO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9Filtro_Ano), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vGRAFICAR", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV29Graficar, ""))));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL1_Title", StringUtil.RTrim( Gxchartcontrol1_Title));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL1_Width", StringUtil.RTrim( Gxchartcontrol1_Width));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL1_Height", StringUtil.RTrim( Gxchartcontrol1_Height));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL1_Charttype", StringUtil.RTrim( Gxchartcontrol1_Charttype));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL1_X_axistitle", StringUtil.RTrim( Gxchartcontrol1_X_axistitle));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL1_Y_axistitle", StringUtil.RTrim( Gxchartcontrol1_Y_axistitle));
         GxWebStd.gx_hidden_field( context, "GXCHARTCONTROL1_Backgroundcolor1", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gxchartcontrol1_Backgroundcolor1), 9, 0, ".", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "</form>") ;
         }
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WECC2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTCC2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_consultacontratadagrf.aspx") + "?" + UrlEncode("" +AV30Contratada_Codigo) + "," + UrlEncode("" +AV9Filtro_Ano) + "," + UrlEncode(StringUtil.RTrim(AV29Graficar)) ;
      }

      public override String GetPgmname( )
      {
         return "WP_ConsultaContratadaGrf" ;
      }

      public override String GetPgmdesc( )
      {
         return "" ;
      }

      protected void WBCC0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_CC2( true) ;
         }
         else
         {
            wb_table1_2_CC2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_CC2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTCC2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPCC0( ) ;
      }

      protected void WSCC2( )
      {
         STARTCC2( ) ;
         EVTCC2( ) ;
      }

      protected void EVTCC2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11CC2 */
                              E11CC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12CC2 */
                              E12CC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WECC2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PACC2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFCC2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFCC2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E12CC2 */
            E12CC2 ();
            WBCC0( ) ;
         }
      }

      protected void STRUPCC0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11CC2 */
         E11CC2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vGXCHARTDATA"), GxChartData);
            /* Read variables values. */
            /* Read saved values. */
            Gxchartcontrol1_Title = cgiGet( "GXCHARTCONTROL1_Title");
            Gxchartcontrol1_Width = cgiGet( "GXCHARTCONTROL1_Width");
            Gxchartcontrol1_Height = cgiGet( "GXCHARTCONTROL1_Height");
            Gxchartcontrol1_Charttype = cgiGet( "GXCHARTCONTROL1_Charttype");
            Gxchartcontrol1_X_axistitle = cgiGet( "GXCHARTCONTROL1_X_axistitle");
            Gxchartcontrol1_Y_axistitle = cgiGet( "GXCHARTCONTROL1_Y_axistitle");
            Gxchartcontrol1_Backgroundcolor1 = (int)(context.localUtil.CToN( cgiGet( "GXCHARTCONTROL1_Backgroundcolor1"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11CC2 */
         E11CC2 ();
         if (returnInSub) return;
      }

      protected void E11CC2( )
      {
         /* Start Routine */
         Gxchartcontrol1_Title = "Consulta anual"+StringUtil.Str( (decimal)(AV9Filtro_Ano), 4, 0);
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Gxchartcontrol1_Internalname, "Title", Gxchartcontrol1_Title);
         GxChartData.gxTpr_Categories.Add("Janeiro", 0);
         GxChartData.gxTpr_Categories.Add("Fevereiro", 0);
         GxChartData.gxTpr_Categories.Add("Mar�o", 0);
         GxChartData.gxTpr_Categories.Add("Abril", 0);
         GxChartData.gxTpr_Categories.Add("Maio", 0);
         GxChartData.gxTpr_Categories.Add("Junho", 0);
         GxChartData.gxTpr_Categories.Add("Julho", 0);
         GxChartData.gxTpr_Categories.Add("Agosto", 0);
         GxChartData.gxTpr_Categories.Add("Setembro", 0);
         GxChartData.gxTpr_Categories.Add("Outubro", 0);
         GxChartData.gxTpr_Categories.Add("Novembro", 0);
         GxChartData.gxTpr_Categories.Add("Dezembro", 0);
         /* Execute user subroutine: 'GRAFICAR' */
         S112 ();
         if (returnInSub) return;
      }

      protected void S112( )
      {
         /* 'GRAFICAR' Routine */
         /* Using cursor H00CC2 */
         pr_default.execute(0, new Object[] {AV9Filtro_Ano});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKCC3 = false;
            A456ContagemResultado_Codigo = H00CC2_A456ContagemResultado_Codigo[0];
            A470ContagemResultado_ContadorFMCod = H00CC2_A470ContagemResultado_ContadorFMCod[0];
            A479ContagemResultado_CrFMPessoaCod = H00CC2_A479ContagemResultado_CrFMPessoaCod[0];
            n479ContagemResultado_CrFMPessoaCod = H00CC2_n479ContagemResultado_CrFMPessoaCod[0];
            A490ContagemResultado_ContratadaCod = H00CC2_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00CC2_n490ContagemResultado_ContratadaCod[0];
            A473ContagemResultado_DataCnt = H00CC2_A473ContagemResultado_DataCnt[0];
            A458ContagemResultado_PFBFS = H00CC2_A458ContagemResultado_PFBFS[0];
            n458ContagemResultado_PFBFS = H00CC2_n458ContagemResultado_PFBFS[0];
            A460ContagemResultado_PFBFM = H00CC2_A460ContagemResultado_PFBFM[0];
            n460ContagemResultado_PFBFM = H00CC2_n460ContagemResultado_PFBFM[0];
            A484ContagemResultado_StatusDmn = H00CC2_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00CC2_n484ContagemResultado_StatusDmn[0];
            A468ContagemResultado_NaoCnfDmnCod = H00CC2_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = H00CC2_n468ContagemResultado_NaoCnfDmnCod[0];
            A469ContagemResultado_NaoCnfCntCod = H00CC2_A469ContagemResultado_NaoCnfCntCod[0];
            n469ContagemResultado_NaoCnfCntCod = H00CC2_n469ContagemResultado_NaoCnfCntCod[0];
            A462ContagemResultado_Divergencia = H00CC2_A462ContagemResultado_Divergencia[0];
            A517ContagemResultado_Ultima = H00CC2_A517ContagemResultado_Ultima[0];
            A474ContagemResultado_ContadorFMNom = H00CC2_A474ContagemResultado_ContadorFMNom[0];
            n474ContagemResultado_ContadorFMNom = H00CC2_n474ContagemResultado_ContadorFMNom[0];
            A490ContagemResultado_ContratadaCod = H00CC2_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00CC2_n490ContagemResultado_ContratadaCod[0];
            A484ContagemResultado_StatusDmn = H00CC2_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00CC2_n484ContagemResultado_StatusDmn[0];
            A468ContagemResultado_NaoCnfDmnCod = H00CC2_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = H00CC2_n468ContagemResultado_NaoCnfDmnCod[0];
            A479ContagemResultado_CrFMPessoaCod = H00CC2_A479ContagemResultado_CrFMPessoaCod[0];
            n479ContagemResultado_CrFMPessoaCod = H00CC2_n479ContagemResultado_CrFMPessoaCod[0];
            A474ContagemResultado_ContadorFMNom = H00CC2_A474ContagemResultado_ContadorFMNom[0];
            n474ContagemResultado_ContadorFMNom = H00CC2_n474ContagemResultado_ContadorFMNom[0];
            GxChartSerie = new SdtGxChart_Serie(context);
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV19mQtdeDmn[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV17mPFBFM[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV18mPFBFS[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV14mCTPF[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV20mRTPF[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV16mPendencias[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            GX_I = 1;
            while ( GX_I <= 12 )
            {
               AV15mDivergencias[GX_I-1] = 0;
               GX_I = (int)(GX_I+1);
            }
            while ( (pr_default.getStatus(0) != 101) && ( H00CC2_A490ContagemResultado_ContratadaCod[0] == A490ContagemResultado_ContratadaCod ) )
            {
               BRKCC3 = false;
               A456ContagemResultado_Codigo = H00CC2_A456ContagemResultado_Codigo[0];
               A470ContagemResultado_ContadorFMCod = H00CC2_A470ContagemResultado_ContadorFMCod[0];
               A473ContagemResultado_DataCnt = H00CC2_A473ContagemResultado_DataCnt[0];
               A458ContagemResultado_PFBFS = H00CC2_A458ContagemResultado_PFBFS[0];
               n458ContagemResultado_PFBFS = H00CC2_n458ContagemResultado_PFBFS[0];
               A460ContagemResultado_PFBFM = H00CC2_A460ContagemResultado_PFBFM[0];
               n460ContagemResultado_PFBFM = H00CC2_n460ContagemResultado_PFBFM[0];
               A484ContagemResultado_StatusDmn = H00CC2_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = H00CC2_n484ContagemResultado_StatusDmn[0];
               A468ContagemResultado_NaoCnfDmnCod = H00CC2_A468ContagemResultado_NaoCnfDmnCod[0];
               n468ContagemResultado_NaoCnfDmnCod = H00CC2_n468ContagemResultado_NaoCnfDmnCod[0];
               A469ContagemResultado_NaoCnfCntCod = H00CC2_A469ContagemResultado_NaoCnfCntCod[0];
               n469ContagemResultado_NaoCnfCntCod = H00CC2_n469ContagemResultado_NaoCnfCntCod[0];
               A462ContagemResultado_Divergencia = H00CC2_A462ContagemResultado_Divergencia[0];
               A517ContagemResultado_Ultima = H00CC2_A517ContagemResultado_Ultima[0];
               A484ContagemResultado_StatusDmn = H00CC2_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = H00CC2_n484ContagemResultado_StatusDmn[0];
               A468ContagemResultado_NaoCnfDmnCod = H00CC2_A468ContagemResultado_NaoCnfDmnCod[0];
               n468ContagemResultado_NaoCnfDmnCod = H00CC2_n468ContagemResultado_NaoCnfDmnCod[0];
               AV13i = (short)(DateTimeUtil.Month( A473ContagemResultado_DataCnt));
               AV19mQtdeDmn[AV13i-1] = (short)(AV19mQtdeDmn[AV13i-1]+1);
               AV18mPFBFS[AV13i-1] = (decimal)(AV18mPFBFS[AV13i-1]+A458ContagemResultado_PFBFS);
               AV17mPFBFM[AV13i-1] = (decimal)(AV17mPFBFM[AV13i-1]+A460ContagemResultado_PFBFM);
               AV14mCTPF[AV13i-1] = (decimal)(AV14mCTPF[AV13i-1]+(A460ContagemResultado_PFBFM*AV8CUPF));
               AV20mRTPF[AV13i-1] = (decimal)(AV20mRTPF[AV13i-1]+(A460ContagemResultado_PFBFM*AV8CUPF));
               if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "A") == 0 )
               {
                  if ( ! (0==A469ContagemResultado_NaoCnfCntCod) || ! (0==A468ContagemResultado_NaoCnfDmnCod) )
                  {
                     AV16mPendencias[AV13i-1] = (short)(AV16mPendencias[AV13i-1]+1);
                  }
                  if ( A462ContagemResultado_Divergencia > AV7Contrato_IndiceDivergencia )
                  {
                     AV15mDivergencias[AV13i-1] = (short)(AV15mDivergencias[AV13i-1]+1);
                  }
               }
               BRKCC3 = true;
               pr_default.readNext(0);
            }
            if ( (0==AV31Filtro_Mes) )
            {
               AV13i = 1;
               while ( AV13i <= 12 )
               {
                  if ( StringUtil.StrCmp(AV29Graficar, "DMN") == 0 )
                  {
                     GxChartSerie.gxTpr_Values.Add((decimal)(AV19mQtdeDmn[AV13i-1]), 0);
                  }
                  else if ( StringUtil.StrCmp(AV29Graficar, "PFB") == 0 )
                  {
                     GxChartSerie.gxTpr_Values.Add(AV17mPFBFM[AV13i-1], 0);
                  }
                  AV13i = (short)(AV13i+1);
               }
            }
            else
            {
               if ( StringUtil.StrCmp(AV29Graficar, "DMN") == 0 )
               {
                  GxChartSerie.gxTpr_Values.Add((decimal)(AV19mQtdeDmn[(int)(AV31Filtro_Mes)-1]), 0);
               }
               else if ( StringUtil.StrCmp(AV29Graficar, "PFB") == 0 )
               {
                  GxChartSerie.gxTpr_Values.Add(AV17mPFBFM[(int)(AV31Filtro_Mes)-1], 0);
               }
            }
            AV13i = (short)(GxChartData.gxTpr_Series.Count+1);
            GxChartSerie.gxTpr_Name = StringUtil.Trim( StringUtil.Str( (decimal)(AV13i), 4, 0))+" - "+A474ContagemResultado_ContadorFMNom;
            GxChartData.gxTpr_Series.Add(GxChartSerie, 0);
            if ( ! BRKCC3 )
            {
               BRKCC3 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void nextLoad( )
      {
      }

      protected void E12CC2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_CC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GXCHARTCONTROL1Container"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_CC2e( true) ;
         }
         else
         {
            wb_table1_2_CC2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV30Contratada_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30Contratada_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV30Contratada_Codigo), "ZZZZZ9")));
         AV9Filtro_Ano = Convert.ToInt16(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Filtro_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Filtro_Ano), 4, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFILTRO_ANO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9Filtro_Ano), "ZZZ9")));
         AV29Graficar = (String)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Graficar", AV29Graficar);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vGRAFICAR", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV29Graficar, ""))));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PACC2( ) ;
         WSCC2( ) ;
         WECC2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216212236");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
            context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
            context.AddJavascriptSource("wp_consultacontratadagrf.js", "?20206216212236");
            context.AddJavascriptSource("GxChart/gxChart.js", "");
         }
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         Gxchartcontrol1_Internalname = "GXCHARTCONTROL1";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Gxchartcontrol1_Backgroundcolor1 = (int)(0xD3D3D3);
         Gxchartcontrol1_Y_axistitle = "Meses";
         Gxchartcontrol1_X_axistitle = "Pontos de Fun��o";
         Gxchartcontrol1_Charttype = "BAR";
         Gxchartcontrol1_Height = "500";
         Gxchartcontrol1_Width = "900";
         Gxchartcontrol1_Title = "Consulta anual CONTRATADA";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV29Graficar = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GxChartData = new SdtGxChart(context);
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00CC2_A511ContagemResultado_HoraCnt = new String[] {""} ;
         H00CC2_A456ContagemResultado_Codigo = new int[1] ;
         H00CC2_A470ContagemResultado_ContadorFMCod = new int[1] ;
         H00CC2_A479ContagemResultado_CrFMPessoaCod = new int[1] ;
         H00CC2_n479ContagemResultado_CrFMPessoaCod = new bool[] {false} ;
         H00CC2_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00CC2_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00CC2_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         H00CC2_A458ContagemResultado_PFBFS = new decimal[1] ;
         H00CC2_n458ContagemResultado_PFBFS = new bool[] {false} ;
         H00CC2_A460ContagemResultado_PFBFM = new decimal[1] ;
         H00CC2_n460ContagemResultado_PFBFM = new bool[] {false} ;
         H00CC2_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00CC2_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00CC2_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         H00CC2_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         H00CC2_A469ContagemResultado_NaoCnfCntCod = new int[1] ;
         H00CC2_n469ContagemResultado_NaoCnfCntCod = new bool[] {false} ;
         H00CC2_A462ContagemResultado_Divergencia = new decimal[1] ;
         H00CC2_A517ContagemResultado_Ultima = new bool[] {false} ;
         H00CC2_A474ContagemResultado_ContadorFMNom = new String[] {""} ;
         H00CC2_n474ContagemResultado_ContadorFMNom = new bool[] {false} ;
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A484ContagemResultado_StatusDmn = "";
         A474ContagemResultado_ContadorFMNom = "";
         GxChartSerie = new SdtGxChart_Serie(context);
         AV19mQtdeDmn = new short [12] ;
         AV17mPFBFM = new decimal [12] ;
         AV18mPFBFS = new decimal [12] ;
         AV14mCTPF = new decimal [12] ;
         AV20mRTPF = new decimal [12] ;
         AV16mPendencias = new short [12] ;
         AV15mDivergencias = new short [12] ;
         AV7Contrato_IndiceDivergencia = (decimal)(10);
         sStyleString = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_consultacontratadagrf__default(),
            new Object[][] {
                new Object[] {
               H00CC2_A511ContagemResultado_HoraCnt, H00CC2_A456ContagemResultado_Codigo, H00CC2_A470ContagemResultado_ContadorFMCod, H00CC2_A479ContagemResultado_CrFMPessoaCod, H00CC2_n479ContagemResultado_CrFMPessoaCod, H00CC2_A490ContagemResultado_ContratadaCod, H00CC2_n490ContagemResultado_ContratadaCod, H00CC2_A473ContagemResultado_DataCnt, H00CC2_A458ContagemResultado_PFBFS, H00CC2_n458ContagemResultado_PFBFS,
               H00CC2_A460ContagemResultado_PFBFM, H00CC2_n460ContagemResultado_PFBFM, H00CC2_A484ContagemResultado_StatusDmn, H00CC2_n484ContagemResultado_StatusDmn, H00CC2_A468ContagemResultado_NaoCnfDmnCod, H00CC2_n468ContagemResultado_NaoCnfDmnCod, H00CC2_A469ContagemResultado_NaoCnfCntCod, H00CC2_n469ContagemResultado_NaoCnfCntCod, H00CC2_A462ContagemResultado_Divergencia, H00CC2_A517ContagemResultado_Ultima,
               H00CC2_A474ContagemResultado_ContadorFMNom, H00CC2_n474ContagemResultado_ContadorFMNom
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV9Filtro_Ano ;
      private short wcpOAV9Filtro_Ano ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short nGXWrapped ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short [] AV19mQtdeDmn ;
      private short [] AV16mPendencias ;
      private short [] AV15mDivergencias ;
      private short AV13i ;
      private int AV30Contratada_Codigo ;
      private int wcpOAV30Contratada_Codigo ;
      private int Gxchartcontrol1_Backgroundcolor1 ;
      private int A456ContagemResultado_Codigo ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int A479ContagemResultado_CrFMPessoaCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A469ContagemResultado_NaoCnfCntCod ;
      private int GX_I ;
      private int idxLst ;
      private long AV31Filtro_Mes ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A462ContagemResultado_Divergencia ;
      private decimal [] AV17mPFBFM ;
      private decimal [] AV18mPFBFS ;
      private decimal [] AV14mCTPF ;
      private decimal [] AV20mRTPF ;
      private decimal AV8CUPF ;
      private decimal AV7Contrato_IndiceDivergencia ;
      private String AV29Graficar ;
      private String wcpOAV29Graficar ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gxchartcontrol1_Title ;
      private String Gxchartcontrol1_Width ;
      private String Gxchartcontrol1_Height ;
      private String Gxchartcontrol1_Charttype ;
      private String Gxchartcontrol1_X_axistitle ;
      private String Gxchartcontrol1_Y_axistitle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Gxchartcontrol1_Internalname ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private String A474ContagemResultado_ContadorFMNom ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool BRKCC3 ;
      private bool n479ContagemResultado_CrFMPessoaCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n469ContagemResultado_NaoCnfCntCod ;
      private bool A517ContagemResultado_Ultima ;
      private bool n474ContagemResultado_ContadorFMNom ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] H00CC2_A511ContagemResultado_HoraCnt ;
      private int[] H00CC2_A456ContagemResultado_Codigo ;
      private int[] H00CC2_A470ContagemResultado_ContadorFMCod ;
      private int[] H00CC2_A479ContagemResultado_CrFMPessoaCod ;
      private bool[] H00CC2_n479ContagemResultado_CrFMPessoaCod ;
      private int[] H00CC2_A490ContagemResultado_ContratadaCod ;
      private bool[] H00CC2_n490ContagemResultado_ContratadaCod ;
      private DateTime[] H00CC2_A473ContagemResultado_DataCnt ;
      private decimal[] H00CC2_A458ContagemResultado_PFBFS ;
      private bool[] H00CC2_n458ContagemResultado_PFBFS ;
      private decimal[] H00CC2_A460ContagemResultado_PFBFM ;
      private bool[] H00CC2_n460ContagemResultado_PFBFM ;
      private String[] H00CC2_A484ContagemResultado_StatusDmn ;
      private bool[] H00CC2_n484ContagemResultado_StatusDmn ;
      private int[] H00CC2_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] H00CC2_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] H00CC2_A469ContagemResultado_NaoCnfCntCod ;
      private bool[] H00CC2_n469ContagemResultado_NaoCnfCntCod ;
      private decimal[] H00CC2_A462ContagemResultado_Divergencia ;
      private bool[] H00CC2_A517ContagemResultado_Ultima ;
      private String[] H00CC2_A474ContagemResultado_ContadorFMNom ;
      private bool[] H00CC2_n474ContagemResultado_ContadorFMNom ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
      private SdtGxChart GxChartData ;
      private SdtGxChart_Serie GxChartSerie ;
   }

   public class wp_consultacontratadagrf__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00CC2 ;
          prmH00CC2 = new Object[] {
          new Object[] {"@AV9Filtro_Ano",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00CC2", "SELECT T1.[ContagemResultado_HoraCnt], T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ContadorFMCod] AS ContagemResultado_ContadorFMCod, T3.[Usuario_PessoaCod] AS ContagemResultado_CrFMPessoaCod, T2.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_PFBFS], T1.[ContagemResultado_PFBFM], T2.[ContagemResultado_StatusDmn], T2.[ContagemResultado_NaoCnfDmnCod], T1.[ContagemResultado_NaoCnfCntCod], T1.[ContagemResultado_Divergencia], T1.[ContagemResultado_Ultima], T4.[Pessoa_Nome] AS ContagemResultado_ContadorFMNom FROM ((([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContagemResultado_ContadorFMCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) WHERE (YEAR(T1.[ContagemResultado_DataCnt]) = @AV9Filtro_Ano) AND (T1.[ContagemResultado_Ultima] = 1) ORDER BY T2.[ContagemResultado_ContratadaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CC2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 5) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(6) ;
                ((decimal[]) buf[8])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((String[]) buf[12])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((int[]) buf[16])[0] = rslt.getInt(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[19])[0] = rslt.getBool(13) ;
                ((String[]) buf[20])[0] = rslt.getString(14, 100) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(14);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
       }
    }

 }

}
