/*
               File: type_SdtSDT_RedmineIssues_issue
        Description: SDT_RedmineIssues
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:30:6.10
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_RedmineIssues.issue" )]
   [XmlType(TypeName =  "SDT_RedmineIssues.issue" , Namespace = "" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_RedmineIssues_issue_project ))]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_RedmineIssues_issue_tracker ))]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_RedmineIssues_issue_status ))]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_RedmineIssues_issue_priority ))]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_RedmineIssues_issue_author ))]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_RedmineIssues_issue_assigned_to ))]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_RedmineIssues_issue_custom_fields ))]
   [Serializable]
   public class SdtSDT_RedmineIssues_issue : GxUserType
   {
      public SdtSDT_RedmineIssues_issue( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_RedmineIssues_issue_Subject = "";
         gxTv_SdtSDT_RedmineIssues_issue_Description = "";
         gxTv_SdtSDT_RedmineIssues_issue_Start_date = "";
         gxTv_SdtSDT_RedmineIssues_issue_Due_date = "";
         gxTv_SdtSDT_RedmineIssues_issue_Estimated_hours = "";
         gxTv_SdtSDT_RedmineIssues_issue_Created_on = "";
         gxTv_SdtSDT_RedmineIssues_issue_Updated_on = "";
      }

      public SdtSDT_RedmineIssues_issue( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_RedmineIssues_issue deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType());
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_RedmineIssues_issue)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_RedmineIssues_issue obj ;
         obj = this;
         obj.gxTpr_Id = deserialized.gxTpr_Id;
         obj.gxTpr_Project = deserialized.gxTpr_Project;
         obj.gxTpr_Tracker = deserialized.gxTpr_Tracker;
         obj.gxTpr_Status = deserialized.gxTpr_Status;
         obj.gxTpr_Priority = deserialized.gxTpr_Priority;
         obj.gxTpr_Author = deserialized.gxTpr_Author;
         obj.gxTpr_Assigned_to = deserialized.gxTpr_Assigned_to;
         obj.gxTpr_Subject = deserialized.gxTpr_Subject;
         obj.gxTpr_Description = deserialized.gxTpr_Description;
         obj.gxTpr_Start_date = deserialized.gxTpr_Start_date;
         obj.gxTpr_Due_date = deserialized.gxTpr_Due_date;
         obj.gxTpr_Done_ratio = deserialized.gxTpr_Done_ratio;
         obj.gxTpr_Estimated_hours = deserialized.gxTpr_Estimated_hours;
         obj.gxTpr_Custom_fields = deserialized.gxTpr_Custom_fields;
         obj.gxTpr_Created_on = deserialized.gxTpr_Created_on;
         obj.gxTpr_Updated_on = deserialized.gxTpr_Updated_on;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "id") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_RedmineIssues_issue_Id = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "project") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  if ( gxTv_SdtSDT_RedmineIssues_issue_Project == null )
                  {
                     gxTv_SdtSDT_RedmineIssues_issue_Project = new SdtSDT_RedmineIssues_issue_project(context);
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtSDT_RedmineIssues_issue_Project.readxml(oReader, "project");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "tracker") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  if ( gxTv_SdtSDT_RedmineIssues_issue_Tracker == null )
                  {
                     gxTv_SdtSDT_RedmineIssues_issue_Tracker = new SdtSDT_RedmineIssues_issue_tracker(context);
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtSDT_RedmineIssues_issue_Tracker.readxml(oReader, "tracker");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "status") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  if ( gxTv_SdtSDT_RedmineIssues_issue_Status == null )
                  {
                     gxTv_SdtSDT_RedmineIssues_issue_Status = new SdtSDT_RedmineIssues_issue_status(context);
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtSDT_RedmineIssues_issue_Status.readxml(oReader, "status");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "priority") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  if ( gxTv_SdtSDT_RedmineIssues_issue_Priority == null )
                  {
                     gxTv_SdtSDT_RedmineIssues_issue_Priority = new SdtSDT_RedmineIssues_issue_priority(context);
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtSDT_RedmineIssues_issue_Priority.readxml(oReader, "priority");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "author") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  if ( gxTv_SdtSDT_RedmineIssues_issue_Author == null )
                  {
                     gxTv_SdtSDT_RedmineIssues_issue_Author = new SdtSDT_RedmineIssues_issue_author(context);
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtSDT_RedmineIssues_issue_Author.readxml(oReader, "author");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "assigned_to") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  if ( gxTv_SdtSDT_RedmineIssues_issue_Assigned_to == null )
                  {
                     gxTv_SdtSDT_RedmineIssues_issue_Assigned_to = new SdtSDT_RedmineIssues_issue_assigned_to(context);
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtSDT_RedmineIssues_issue_Assigned_to.readxml(oReader, "assigned_to");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "subject") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_RedmineIssues_issue_Subject = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "description") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_RedmineIssues_issue_Description = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "start_date") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_RedmineIssues_issue_Start_date = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "due_date") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_RedmineIssues_issue_Due_date = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "done_ratio") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_RedmineIssues_issue_Done_ratio = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "estimated_hours") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_RedmineIssues_issue_Estimated_hours = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "custom_fields") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  if ( gxTv_SdtSDT_RedmineIssues_issue_Custom_fields == null )
                  {
                     gxTv_SdtSDT_RedmineIssues_issue_Custom_fields = new SdtSDT_RedmineIssues_issue_custom_fields(context);
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtSDT_RedmineIssues_issue_Custom_fields.readxml(oReader, "custom_fields");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "created_on") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_RedmineIssues_issue_Created_on = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "updated_on") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_RedmineIssues_issue_Updated_on = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_RedmineIssues.issue";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("id", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_RedmineIssues_issue_Id), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         if ( gxTv_SdtSDT_RedmineIssues_issue_Project != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "";
            }
            else
            {
               sNameSpace1 = "";
            }
            gxTv_SdtSDT_RedmineIssues_issue_Project.writexml(oWriter, "project", sNameSpace1);
         }
         if ( gxTv_SdtSDT_RedmineIssues_issue_Tracker != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "";
            }
            else
            {
               sNameSpace1 = "";
            }
            gxTv_SdtSDT_RedmineIssues_issue_Tracker.writexml(oWriter, "tracker", sNameSpace1);
         }
         if ( gxTv_SdtSDT_RedmineIssues_issue_Status != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "";
            }
            else
            {
               sNameSpace1 = "";
            }
            gxTv_SdtSDT_RedmineIssues_issue_Status.writexml(oWriter, "status", sNameSpace1);
         }
         if ( gxTv_SdtSDT_RedmineIssues_issue_Priority != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "";
            }
            else
            {
               sNameSpace1 = "";
            }
            gxTv_SdtSDT_RedmineIssues_issue_Priority.writexml(oWriter, "priority", sNameSpace1);
         }
         if ( gxTv_SdtSDT_RedmineIssues_issue_Author != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "";
            }
            else
            {
               sNameSpace1 = "";
            }
            gxTv_SdtSDT_RedmineIssues_issue_Author.writexml(oWriter, "author", sNameSpace1);
         }
         if ( gxTv_SdtSDT_RedmineIssues_issue_Assigned_to != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "";
            }
            else
            {
               sNameSpace1 = "";
            }
            gxTv_SdtSDT_RedmineIssues_issue_Assigned_to.writexml(oWriter, "assigned_to", sNameSpace1);
         }
         oWriter.WriteElement("subject", StringUtil.RTrim( gxTv_SdtSDT_RedmineIssues_issue_Subject));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("description", StringUtil.RTrim( gxTv_SdtSDT_RedmineIssues_issue_Description));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("start_date", StringUtil.RTrim( gxTv_SdtSDT_RedmineIssues_issue_Start_date));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("due_date", StringUtil.RTrim( gxTv_SdtSDT_RedmineIssues_issue_Due_date));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("done_ratio", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_RedmineIssues_issue_Done_ratio), 2, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("estimated_hours", StringUtil.RTrim( gxTv_SdtSDT_RedmineIssues_issue_Estimated_hours));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         if ( gxTv_SdtSDT_RedmineIssues_issue_Custom_fields != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "";
            }
            else
            {
               sNameSpace1 = "";
            }
            gxTv_SdtSDT_RedmineIssues_issue_Custom_fields.writexml(oWriter, "custom_fields", sNameSpace1);
         }
         oWriter.WriteElement("created_on", StringUtil.RTrim( gxTv_SdtSDT_RedmineIssues_issue_Created_on));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("updated_on", StringUtil.RTrim( gxTv_SdtSDT_RedmineIssues_issue_Updated_on));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("id", gxTv_SdtSDT_RedmineIssues_issue_Id, false);
         if ( gxTv_SdtSDT_RedmineIssues_issue_Project != null )
         {
            AddObjectProperty("project", gxTv_SdtSDT_RedmineIssues_issue_Project, false);
         }
         if ( gxTv_SdtSDT_RedmineIssues_issue_Tracker != null )
         {
            AddObjectProperty("tracker", gxTv_SdtSDT_RedmineIssues_issue_Tracker, false);
         }
         if ( gxTv_SdtSDT_RedmineIssues_issue_Status != null )
         {
            AddObjectProperty("status", gxTv_SdtSDT_RedmineIssues_issue_Status, false);
         }
         if ( gxTv_SdtSDT_RedmineIssues_issue_Priority != null )
         {
            AddObjectProperty("priority", gxTv_SdtSDT_RedmineIssues_issue_Priority, false);
         }
         if ( gxTv_SdtSDT_RedmineIssues_issue_Author != null )
         {
            AddObjectProperty("author", gxTv_SdtSDT_RedmineIssues_issue_Author, false);
         }
         if ( gxTv_SdtSDT_RedmineIssues_issue_Assigned_to != null )
         {
            AddObjectProperty("assigned_to", gxTv_SdtSDT_RedmineIssues_issue_Assigned_to, false);
         }
         AddObjectProperty("subject", gxTv_SdtSDT_RedmineIssues_issue_Subject, false);
         AddObjectProperty("description", gxTv_SdtSDT_RedmineIssues_issue_Description, false);
         AddObjectProperty("start_date", gxTv_SdtSDT_RedmineIssues_issue_Start_date, false);
         AddObjectProperty("due_date", gxTv_SdtSDT_RedmineIssues_issue_Due_date, false);
         AddObjectProperty("done_ratio", gxTv_SdtSDT_RedmineIssues_issue_Done_ratio, false);
         AddObjectProperty("estimated_hours", gxTv_SdtSDT_RedmineIssues_issue_Estimated_hours, false);
         if ( gxTv_SdtSDT_RedmineIssues_issue_Custom_fields != null )
         {
            AddObjectProperty("custom_fields", gxTv_SdtSDT_RedmineIssues_issue_Custom_fields, false);
         }
         AddObjectProperty("created_on", gxTv_SdtSDT_RedmineIssues_issue_Created_on, false);
         AddObjectProperty("updated_on", gxTv_SdtSDT_RedmineIssues_issue_Updated_on, false);
         return  ;
      }

      [  SoapElement( ElementName = "id" )]
      [  XmlElement( ElementName = "id" , Namespace = ""  )]
      public short gxTpr_Id
      {
         get {
            return gxTv_SdtSDT_RedmineIssues_issue_Id ;
         }

         set {
            gxTv_SdtSDT_RedmineIssues_issue_Id = (short)(value);
         }

      }

      [  SoapElement( ElementName = "project" )]
      [  XmlElement( ElementName = "project"   )]
      public SdtSDT_RedmineIssues_issue_project gxTpr_Project
      {
         get {
            if ( gxTv_SdtSDT_RedmineIssues_issue_Project == null )
            {
               gxTv_SdtSDT_RedmineIssues_issue_Project = new SdtSDT_RedmineIssues_issue_project(context);
            }
            return gxTv_SdtSDT_RedmineIssues_issue_Project ;
         }

         set {
            gxTv_SdtSDT_RedmineIssues_issue_Project = value;
         }

      }

      public void gxTv_SdtSDT_RedmineIssues_issue_Project_SetNull( )
      {
         gxTv_SdtSDT_RedmineIssues_issue_Project = null;
         return  ;
      }

      public bool gxTv_SdtSDT_RedmineIssues_issue_Project_IsNull( )
      {
         if ( gxTv_SdtSDT_RedmineIssues_issue_Project == null )
         {
            return true ;
         }
         return false ;
      }

      [  SoapElement( ElementName = "tracker" )]
      [  XmlElement( ElementName = "tracker"   )]
      public SdtSDT_RedmineIssues_issue_tracker gxTpr_Tracker
      {
         get {
            if ( gxTv_SdtSDT_RedmineIssues_issue_Tracker == null )
            {
               gxTv_SdtSDT_RedmineIssues_issue_Tracker = new SdtSDT_RedmineIssues_issue_tracker(context);
            }
            return gxTv_SdtSDT_RedmineIssues_issue_Tracker ;
         }

         set {
            gxTv_SdtSDT_RedmineIssues_issue_Tracker = value;
         }

      }

      public void gxTv_SdtSDT_RedmineIssues_issue_Tracker_SetNull( )
      {
         gxTv_SdtSDT_RedmineIssues_issue_Tracker = null;
         return  ;
      }

      public bool gxTv_SdtSDT_RedmineIssues_issue_Tracker_IsNull( )
      {
         if ( gxTv_SdtSDT_RedmineIssues_issue_Tracker == null )
         {
            return true ;
         }
         return false ;
      }

      [  SoapElement( ElementName = "status" )]
      [  XmlElement( ElementName = "status"   )]
      public SdtSDT_RedmineIssues_issue_status gxTpr_Status
      {
         get {
            if ( gxTv_SdtSDT_RedmineIssues_issue_Status == null )
            {
               gxTv_SdtSDT_RedmineIssues_issue_Status = new SdtSDT_RedmineIssues_issue_status(context);
            }
            return gxTv_SdtSDT_RedmineIssues_issue_Status ;
         }

         set {
            gxTv_SdtSDT_RedmineIssues_issue_Status = value;
         }

      }

      public void gxTv_SdtSDT_RedmineIssues_issue_Status_SetNull( )
      {
         gxTv_SdtSDT_RedmineIssues_issue_Status = null;
         return  ;
      }

      public bool gxTv_SdtSDT_RedmineIssues_issue_Status_IsNull( )
      {
         if ( gxTv_SdtSDT_RedmineIssues_issue_Status == null )
         {
            return true ;
         }
         return false ;
      }

      [  SoapElement( ElementName = "priority" )]
      [  XmlElement( ElementName = "priority"   )]
      public SdtSDT_RedmineIssues_issue_priority gxTpr_Priority
      {
         get {
            if ( gxTv_SdtSDT_RedmineIssues_issue_Priority == null )
            {
               gxTv_SdtSDT_RedmineIssues_issue_Priority = new SdtSDT_RedmineIssues_issue_priority(context);
            }
            return gxTv_SdtSDT_RedmineIssues_issue_Priority ;
         }

         set {
            gxTv_SdtSDT_RedmineIssues_issue_Priority = value;
         }

      }

      public void gxTv_SdtSDT_RedmineIssues_issue_Priority_SetNull( )
      {
         gxTv_SdtSDT_RedmineIssues_issue_Priority = null;
         return  ;
      }

      public bool gxTv_SdtSDT_RedmineIssues_issue_Priority_IsNull( )
      {
         if ( gxTv_SdtSDT_RedmineIssues_issue_Priority == null )
         {
            return true ;
         }
         return false ;
      }

      [  SoapElement( ElementName = "author" )]
      [  XmlElement( ElementName = "author"   )]
      public SdtSDT_RedmineIssues_issue_author gxTpr_Author
      {
         get {
            if ( gxTv_SdtSDT_RedmineIssues_issue_Author == null )
            {
               gxTv_SdtSDT_RedmineIssues_issue_Author = new SdtSDT_RedmineIssues_issue_author(context);
            }
            return gxTv_SdtSDT_RedmineIssues_issue_Author ;
         }

         set {
            gxTv_SdtSDT_RedmineIssues_issue_Author = value;
         }

      }

      public void gxTv_SdtSDT_RedmineIssues_issue_Author_SetNull( )
      {
         gxTv_SdtSDT_RedmineIssues_issue_Author = null;
         return  ;
      }

      public bool gxTv_SdtSDT_RedmineIssues_issue_Author_IsNull( )
      {
         if ( gxTv_SdtSDT_RedmineIssues_issue_Author == null )
         {
            return true ;
         }
         return false ;
      }

      [  SoapElement( ElementName = "assigned_to" )]
      [  XmlElement( ElementName = "assigned_to"   )]
      public SdtSDT_RedmineIssues_issue_assigned_to gxTpr_Assigned_to
      {
         get {
            if ( gxTv_SdtSDT_RedmineIssues_issue_Assigned_to == null )
            {
               gxTv_SdtSDT_RedmineIssues_issue_Assigned_to = new SdtSDT_RedmineIssues_issue_assigned_to(context);
            }
            return gxTv_SdtSDT_RedmineIssues_issue_Assigned_to ;
         }

         set {
            gxTv_SdtSDT_RedmineIssues_issue_Assigned_to = value;
         }

      }

      public void gxTv_SdtSDT_RedmineIssues_issue_Assigned_to_SetNull( )
      {
         gxTv_SdtSDT_RedmineIssues_issue_Assigned_to = null;
         return  ;
      }

      public bool gxTv_SdtSDT_RedmineIssues_issue_Assigned_to_IsNull( )
      {
         if ( gxTv_SdtSDT_RedmineIssues_issue_Assigned_to == null )
         {
            return true ;
         }
         return false ;
      }

      [  SoapElement( ElementName = "subject" )]
      [  XmlElement( ElementName = "subject" , Namespace = ""  )]
      public String gxTpr_Subject
      {
         get {
            return gxTv_SdtSDT_RedmineIssues_issue_Subject ;
         }

         set {
            gxTv_SdtSDT_RedmineIssues_issue_Subject = (String)(value);
         }

      }

      [  SoapElement( ElementName = "description" )]
      [  XmlElement( ElementName = "description" , Namespace = ""  )]
      public String gxTpr_Description
      {
         get {
            return gxTv_SdtSDT_RedmineIssues_issue_Description ;
         }

         set {
            gxTv_SdtSDT_RedmineIssues_issue_Description = (String)(value);
         }

      }

      [  SoapElement( ElementName = "start_date" )]
      [  XmlElement( ElementName = "start_date" , Namespace = ""  )]
      public String gxTpr_Start_date
      {
         get {
            return gxTv_SdtSDT_RedmineIssues_issue_Start_date ;
         }

         set {
            gxTv_SdtSDT_RedmineIssues_issue_Start_date = (String)(value);
         }

      }

      [  SoapElement( ElementName = "due_date" )]
      [  XmlElement( ElementName = "due_date" , Namespace = ""  )]
      public String gxTpr_Due_date
      {
         get {
            return gxTv_SdtSDT_RedmineIssues_issue_Due_date ;
         }

         set {
            gxTv_SdtSDT_RedmineIssues_issue_Due_date = (String)(value);
         }

      }

      [  SoapElement( ElementName = "done_ratio" )]
      [  XmlElement( ElementName = "done_ratio" , Namespace = ""  )]
      public short gxTpr_Done_ratio
      {
         get {
            return gxTv_SdtSDT_RedmineIssues_issue_Done_ratio ;
         }

         set {
            gxTv_SdtSDT_RedmineIssues_issue_Done_ratio = (short)(value);
         }

      }

      [  SoapElement( ElementName = "estimated_hours" )]
      [  XmlElement( ElementName = "estimated_hours" , Namespace = ""  )]
      public String gxTpr_Estimated_hours
      {
         get {
            return gxTv_SdtSDT_RedmineIssues_issue_Estimated_hours ;
         }

         set {
            gxTv_SdtSDT_RedmineIssues_issue_Estimated_hours = (String)(value);
         }

      }

      [  SoapElement( ElementName = "custom_fields" )]
      [  XmlElement( ElementName = "custom_fields"   )]
      public SdtSDT_RedmineIssues_issue_custom_fields gxTpr_Custom_fields
      {
         get {
            if ( gxTv_SdtSDT_RedmineIssues_issue_Custom_fields == null )
            {
               gxTv_SdtSDT_RedmineIssues_issue_Custom_fields = new SdtSDT_RedmineIssues_issue_custom_fields(context);
            }
            return gxTv_SdtSDT_RedmineIssues_issue_Custom_fields ;
         }

         set {
            gxTv_SdtSDT_RedmineIssues_issue_Custom_fields = value;
         }

      }

      public void gxTv_SdtSDT_RedmineIssues_issue_Custom_fields_SetNull( )
      {
         gxTv_SdtSDT_RedmineIssues_issue_Custom_fields = null;
         return  ;
      }

      public bool gxTv_SdtSDT_RedmineIssues_issue_Custom_fields_IsNull( )
      {
         if ( gxTv_SdtSDT_RedmineIssues_issue_Custom_fields == null )
         {
            return true ;
         }
         return false ;
      }

      [  SoapElement( ElementName = "created_on" )]
      [  XmlElement( ElementName = "created_on" , Namespace = ""  )]
      public String gxTpr_Created_on
      {
         get {
            return gxTv_SdtSDT_RedmineIssues_issue_Created_on ;
         }

         set {
            gxTv_SdtSDT_RedmineIssues_issue_Created_on = (String)(value);
         }

      }

      [  SoapElement( ElementName = "updated_on" )]
      [  XmlElement( ElementName = "updated_on" , Namespace = ""  )]
      public String gxTpr_Updated_on
      {
         get {
            return gxTv_SdtSDT_RedmineIssues_issue_Updated_on ;
         }

         set {
            gxTv_SdtSDT_RedmineIssues_issue_Updated_on = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_RedmineIssues_issue_Subject = "";
         gxTv_SdtSDT_RedmineIssues_issue_Description = "";
         gxTv_SdtSDT_RedmineIssues_issue_Start_date = "";
         gxTv_SdtSDT_RedmineIssues_issue_Due_date = "";
         gxTv_SdtSDT_RedmineIssues_issue_Estimated_hours = "";
         gxTv_SdtSDT_RedmineIssues_issue_Created_on = "";
         gxTv_SdtSDT_RedmineIssues_issue_Updated_on = "";
         sTagName = "";
         return  ;
      }

      protected short gxTv_SdtSDT_RedmineIssues_issue_Id ;
      protected short gxTv_SdtSDT_RedmineIssues_issue_Done_ratio ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtSDT_RedmineIssues_issue_Subject ;
      protected String gxTv_SdtSDT_RedmineIssues_issue_Description ;
      protected String gxTv_SdtSDT_RedmineIssues_issue_Start_date ;
      protected String gxTv_SdtSDT_RedmineIssues_issue_Due_date ;
      protected String gxTv_SdtSDT_RedmineIssues_issue_Estimated_hours ;
      protected String gxTv_SdtSDT_RedmineIssues_issue_Created_on ;
      protected String gxTv_SdtSDT_RedmineIssues_issue_Updated_on ;
      protected String sTagName ;
      protected SdtSDT_RedmineIssues_issue_project gxTv_SdtSDT_RedmineIssues_issue_Project=null ;
      protected SdtSDT_RedmineIssues_issue_tracker gxTv_SdtSDT_RedmineIssues_issue_Tracker=null ;
      protected SdtSDT_RedmineIssues_issue_status gxTv_SdtSDT_RedmineIssues_issue_Status=null ;
      protected SdtSDT_RedmineIssues_issue_priority gxTv_SdtSDT_RedmineIssues_issue_Priority=null ;
      protected SdtSDT_RedmineIssues_issue_author gxTv_SdtSDT_RedmineIssues_issue_Author=null ;
      protected SdtSDT_RedmineIssues_issue_assigned_to gxTv_SdtSDT_RedmineIssues_issue_Assigned_to=null ;
      protected SdtSDT_RedmineIssues_issue_custom_fields gxTv_SdtSDT_RedmineIssues_issue_Custom_fields=null ;
   }

   [DataContract(Name = @"SDT_RedmineIssues.issue", Namespace = "")]
   public class SdtSDT_RedmineIssues_issue_RESTInterface : GxGenericCollectionItem<SdtSDT_RedmineIssues_issue>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_RedmineIssues_issue_RESTInterface( ) : base()
      {
      }

      public SdtSDT_RedmineIssues_issue_RESTInterface( SdtSDT_RedmineIssues_issue psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "id" , Order = 0 )]
      public Nullable<short> gxTpr_Id
      {
         get {
            return sdt.gxTpr_Id ;
         }

         set {
            sdt.gxTpr_Id = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "project" , Order = 1 )]
      public SdtSDT_RedmineIssues_issue_project_RESTInterface gxTpr_Project
      {
         get {
            return new SdtSDT_RedmineIssues_issue_project_RESTInterface(sdt.gxTpr_Project) ;
         }

         set {
            sdt.gxTpr_Project = value.sdt;
         }

      }

      [DataMember( Name = "tracker" , Order = 2 )]
      public SdtSDT_RedmineIssues_issue_tracker_RESTInterface gxTpr_Tracker
      {
         get {
            return new SdtSDT_RedmineIssues_issue_tracker_RESTInterface(sdt.gxTpr_Tracker) ;
         }

         set {
            sdt.gxTpr_Tracker = value.sdt;
         }

      }

      [DataMember( Name = "status" , Order = 3 )]
      public SdtSDT_RedmineIssues_issue_status_RESTInterface gxTpr_Status
      {
         get {
            return new SdtSDT_RedmineIssues_issue_status_RESTInterface(sdt.gxTpr_Status) ;
         }

         set {
            sdt.gxTpr_Status = value.sdt;
         }

      }

      [DataMember( Name = "priority" , Order = 4 )]
      public SdtSDT_RedmineIssues_issue_priority_RESTInterface gxTpr_Priority
      {
         get {
            return new SdtSDT_RedmineIssues_issue_priority_RESTInterface(sdt.gxTpr_Priority) ;
         }

         set {
            sdt.gxTpr_Priority = value.sdt;
         }

      }

      [DataMember( Name = "author" , Order = 5 )]
      public SdtSDT_RedmineIssues_issue_author_RESTInterface gxTpr_Author
      {
         get {
            return new SdtSDT_RedmineIssues_issue_author_RESTInterface(sdt.gxTpr_Author) ;
         }

         set {
            sdt.gxTpr_Author = value.sdt;
         }

      }

      [DataMember( Name = "assigned_to" , Order = 6 )]
      public SdtSDT_RedmineIssues_issue_assigned_to_RESTInterface gxTpr_Assigned_to
      {
         get {
            return new SdtSDT_RedmineIssues_issue_assigned_to_RESTInterface(sdt.gxTpr_Assigned_to) ;
         }

         set {
            sdt.gxTpr_Assigned_to = value.sdt;
         }

      }

      [DataMember( Name = "subject" , Order = 7 )]
      public String gxTpr_Subject
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Subject) ;
         }

         set {
            sdt.gxTpr_Subject = (String)(value);
         }

      }

      [DataMember( Name = "description" , Order = 8 )]
      public String gxTpr_Description
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Description) ;
         }

         set {
            sdt.gxTpr_Description = (String)(value);
         }

      }

      [DataMember( Name = "start_date" , Order = 9 )]
      public String gxTpr_Start_date
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Start_date) ;
         }

         set {
            sdt.gxTpr_Start_date = (String)(value);
         }

      }

      [DataMember( Name = "due_date" , Order = 10 )]
      public String gxTpr_Due_date
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Due_date) ;
         }

         set {
            sdt.gxTpr_Due_date = (String)(value);
         }

      }

      [DataMember( Name = "done_ratio" , Order = 11 )]
      public Nullable<short> gxTpr_Done_ratio
      {
         get {
            return sdt.gxTpr_Done_ratio ;
         }

         set {
            sdt.gxTpr_Done_ratio = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "estimated_hours" , Order = 12 )]
      public String gxTpr_Estimated_hours
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Estimated_hours) ;
         }

         set {
            sdt.gxTpr_Estimated_hours = (String)(value);
         }

      }

      [DataMember( Name = "custom_fields" , Order = 13 )]
      public SdtSDT_RedmineIssues_issue_custom_fields_RESTInterface gxTpr_Custom_fields
      {
         get {
            return new SdtSDT_RedmineIssues_issue_custom_fields_RESTInterface(sdt.gxTpr_Custom_fields) ;
         }

         set {
            sdt.gxTpr_Custom_fields = value.sdt;
         }

      }

      [DataMember( Name = "created_on" , Order = 14 )]
      public String gxTpr_Created_on
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Created_on) ;
         }

         set {
            sdt.gxTpr_Created_on = (String)(value);
         }

      }

      [DataMember( Name = "updated_on" , Order = 15 )]
      public String gxTpr_Updated_on
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Updated_on) ;
         }

         set {
            sdt.gxTpr_Updated_on = (String)(value);
         }

      }

      public SdtSDT_RedmineIssues_issue sdt
      {
         get {
            return (SdtSDT_RedmineIssues_issue)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_RedmineIssues_issue() ;
         }
      }

   }

}
