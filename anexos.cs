/*
               File: Anexos
        Description: Anexos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:17:27.77
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class anexos : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel2"+"_"+"ANEXO_ENTIDADE") == 0 )
         {
            A1495Anexo_Owner = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1495Anexo_Owner = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1495Anexo_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1495Anexo_Owner), 6, 0)));
            A1496Anexo_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1496Anexo_AreaTrabalhoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1496Anexo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX2ASAANEXO_ENTIDADE36133( A1495Anexo_Owner, A1496Anexo_AreaTrabalhoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel5"+"_"+"ANEXO_ENTIDADE") == 0 )
         {
            A1495Anexo_Owner = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1495Anexo_Owner = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1495Anexo_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1495Anexo_Owner), 6, 0)));
            A1496Anexo_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1496Anexo_AreaTrabalhoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1496Anexo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX5ASAANEXO_ENTIDADE36134( A1495Anexo_Owner, A1496Anexo_AreaTrabalhoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_9") == 0 )
         {
            A1106Anexo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1106Anexo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1106Anexo_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_9( A1106Anexo_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_7") == 0 )
         {
            A1106Anexo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1106Anexo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1106Anexo_Codigo), 6, 0)));
            A1498Anexo_DemandaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1498Anexo_DemandaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1498Anexo_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1498Anexo_DemandaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_7( A1106Anexo_Codigo, A1498Anexo_DemandaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_8") == 0 )
         {
            A645TipoDocumento_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n645TipoDocumento_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_8( A645TipoDocumento_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridanexos_de") == 0 )
         {
            nRC_GXsfl_96 = (short)(NumberUtil.Val( GetNextPar( ), "."));
            nGXsfl_96_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
            sGXsfl_96_idx = GetNextPar( );
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxnrGridanexos_de_newrow( ) ;
            return  ;
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynTipoDocumento_Codigo.Name = "TIPODOCUMENTO_CODIGO";
         dynTipoDocumento_Codigo.WebTags = "";
         dynTipoDocumento_Codigo.removeAllItems();
         /* Using cursor T003613 */
         pr_default.execute(7);
         while ( (pr_default.getStatus(7) != 101) )
         {
            dynTipoDocumento_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T003613_A645TipoDocumento_Codigo[0]), 6, 0)), T003613_A646TipoDocumento_Nome[0], 0);
            pr_default.readNext(7);
         }
         pr_default.close(7);
         if ( dynTipoDocumento_Codigo.ItemCount > 0 )
         {
            A645TipoDocumento_Codigo = (int)(NumberUtil.Val( dynTipoDocumento_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0))), "."));
            n645TipoDocumento_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
         }
         GXCCtl = "ANEXODE_TABELA_" + sGXsfl_96_idx;
         cmbAnexoDe_Tabela.Name = GXCCtl;
         cmbAnexoDe_Tabela.WebTags = "";
         cmbAnexoDe_Tabela.addItem("1", "Contagem Resultado", 0);
         cmbAnexoDe_Tabela.addItem("2", "Evid�ncias", 0);
         cmbAnexoDe_Tabela.addItem("3", "Anexos", 0);
         if ( cmbAnexoDe_Tabela.ItemCount > 0 )
         {
            A1110AnexoDe_Tabela = (int)(NumberUtil.Val( cmbAnexoDe_Tabela.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1110AnexoDe_Tabela), 6, 0))), "."));
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Anexos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtAnexo_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public anexos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public anexos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynTipoDocumento_Codigo = new GXCombobox();
         cmbAnexoDe_Tabela = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynTipoDocumento_Codigo.ItemCount > 0 )
         {
            A645TipoDocumento_Codigo = (int)(NumberUtil.Val( dynTipoDocumento_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0))), "."));
            n645TipoDocumento_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_36133( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_36133e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_36133( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_36133( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_36133e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Anexos", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_Anexos.htm");
            wb_table3_28_36133( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_36133e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_36133e( true) ;
         }
         else
         {
            wb_table1_2_36133e( false) ;
         }
      }

      protected void wb_table3_28_36133( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_36133( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_36133e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Anexos.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Anexos.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Anexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_36133e( true) ;
         }
         else
         {
            wb_table3_28_36133e( false) ;
         }
      }

      protected void wb_table4_34_36133( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockanexo_codigo_Internalname, "Codigo", "", "", lblTextblockanexo_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Anexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtAnexo_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1106Anexo_Codigo), 6, 0, ",", "")), ((edtAnexo_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1106Anexo_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1106Anexo_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAnexo_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtAnexo_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Anexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockanexo_arquivo_Internalname, "Arquivo", "", "", lblTextblockanexo_arquivo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Anexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            ClassString = "Image";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            edtAnexo_Arquivo_Filename = A1107Anexo_NomeArq;
            edtAnexo_Arquivo_Filetype = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAnexo_Arquivo_Internalname, "Filetype", edtAnexo_Arquivo_Filetype);
            edtAnexo_Arquivo_Filetype = A1108Anexo_TipoArq;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAnexo_Arquivo_Internalname, "Filetype", edtAnexo_Arquivo_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1101Anexo_Arquivo)) )
            {
               gxblobfileaux.Source = A1101Anexo_Arquivo;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtAnexo_Arquivo_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtAnexo_Arquivo_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  A1101Anexo_Arquivo = gxblobfileaux.GetAbsoluteName();
                  n1101Anexo_Arquivo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1101Anexo_Arquivo", A1101Anexo_Arquivo);
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAnexo_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A1101Anexo_Arquivo));
                  edtAnexo_Arquivo_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAnexo_Arquivo_Internalname, "Filetype", edtAnexo_Arquivo_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAnexo_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A1101Anexo_Arquivo));
            }
            GxWebStd.gx_blob_field( context, edtAnexo_Arquivo_Internalname, StringUtil.RTrim( A1101Anexo_Arquivo), context.PathToRelativeUrl( A1101Anexo_Arquivo), (String.IsNullOrEmpty(StringUtil.RTrim( edtAnexo_Arquivo_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtAnexo_Arquivo_Filetype)) ? A1101Anexo_Arquivo : edtAnexo_Arquivo_Filetype)) : edtAnexo_Arquivo_Contenttype), true, "", edtAnexo_Arquivo_Parameters, 0, edtAnexo_Arquivo_Enabled, 1, "", "", 0, -1, 250, "px", 60, "px", 0, 0, 0, edtAnexo_Arquivo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "", "", "HLP_Anexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockanexo_userid_Internalname, "Usu�rio", "", "", lblTextblockanexo_userid_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Anexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtAnexo_UserId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1102Anexo_UserId), 6, 0, ",", "")), ((edtAnexo_UserId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1102Anexo_UserId), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1102Anexo_UserId), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAnexo_UserId_Jsonclick, 0, "Attribute", "", "", "", 1, edtAnexo_UserId_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Anexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockanexo_data_Internalname, "Upload", "", "", lblTextblockanexo_data_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Anexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtAnexo_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtAnexo_Data_Internalname, context.localUtil.TToC( A1103Anexo_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1103Anexo_Data, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAnexo_Data_Jsonclick, 0, "Attribute", "", "", "", 1, edtAnexo_Data_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Anexos.htm");
            GxWebStd.gx_bitmap( context, edtAnexo_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtAnexo_Data_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Anexos.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktipodocumento_codigo_Internalname, "Nome", "", "", lblTextblocktipodocumento_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Anexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynTipoDocumento_Codigo, dynTipoDocumento_Codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)), 1, dynTipoDocumento_Codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynTipoDocumento_Codigo.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "", true, "HLP_Anexos.htm");
            dynTipoDocumento_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTipoDocumento_Codigo_Internalname, "Values", (String)(dynTipoDocumento_Codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockanexo_descricao_Internalname, "Anexo_Descricao", "", "", lblTextblockanexo_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Anexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtAnexo_Descricao_Internalname, A1123Anexo_Descricao, StringUtil.RTrim( context.localUtil.Format( A1123Anexo_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAnexo_Descricao_Jsonclick, 0, "Attribute", "", "", "", 1, edtAnexo_Descricao_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_Anexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockanexo_link_Internalname, "Link", "", "", lblTextblockanexo_link_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Anexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtAnexo_Link_Internalname, A1450Anexo_Link, A1450Anexo_Link, TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAnexo_Link_Jsonclick, 0, "Attribute", "", "", "", 1, edtAnexo_Link_Enabled, 0, "text", "", 80, "chr", 1, "row", 2097152, 0, 1, 0, 1, 0, -1, true, "Html", "left", false, "HLP_Anexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockanexo_owner_Internalname, "Responsavel", "", "", lblTextblockanexo_owner_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Anexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtAnexo_Owner_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1495Anexo_Owner), 6, 0, ",", "")), ((edtAnexo_Owner_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1495Anexo_Owner), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1495Anexo_Owner), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAnexo_Owner_Jsonclick, 0, "Attribute", "", "", "", 1, edtAnexo_Owner_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Anexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockanexo_demandacod_Internalname, "demanda", "", "", lblTextblockanexo_demandacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Anexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAnexo_DemandaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1498Anexo_DemandaCod), 6, 0, ",", "")), ((edtAnexo_DemandaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1498Anexo_DemandaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1498Anexo_DemandaCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAnexo_DemandaCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtAnexo_DemandaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Anexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockanexo_areatrabalhocod_Internalname, "Respons�vel", "", "", lblTextblockanexo_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Anexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAnexo_AreaTrabalhoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0, ",", "")), ((edtAnexo_AreaTrabalhoCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1496Anexo_AreaTrabalhoCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1496Anexo_AreaTrabalhoCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAnexo_AreaTrabalhoCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtAnexo_AreaTrabalhoCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Anexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockanexo_entidade_Internalname, "Respons�vel", "", "", lblTextblockanexo_entidade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Anexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAnexo_Entidade_Internalname, StringUtil.RTrim( A1497Anexo_Entidade), StringUtil.RTrim( context.localUtil.Format( A1497Anexo_Entidade, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAnexo_Entidade_Jsonclick, 0, "Attribute", "", "", "", 1, edtAnexo_Entidade_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_Anexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            context.WriteHtmlText( "<br/>") ;
            wb_table5_92_36133( true) ;
         }
         return  ;
      }

      protected void wb_table5_92_36133e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "<hr class=\"HRDefault\"/>") ;
            /*  Grid Control  */
            Gridanexos_deContainer.AddObjectProperty("GridName", "Gridanexos_de");
            Gridanexos_deContainer.AddObjectProperty("Class", "Grid");
            Gridanexos_deContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
            Gridanexos_deContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
            Gridanexos_deContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridanexos_de_Backcolorstyle), 1, 0, ".", "")));
            Gridanexos_deContainer.AddObjectProperty("CmpContext", "");
            Gridanexos_deContainer.AddObjectProperty("InMasterPage", "false");
            Gridanexos_deColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridanexos_deColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1109AnexoDe_Id), 6, 0, ".", "")));
            Gridanexos_deColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAnexoDe_Id_Enabled), 5, 0, ".", "")));
            Gridanexos_deContainer.AddColumnProperties(Gridanexos_deColumn);
            Gridanexos_deColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            Gridanexos_deColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1110AnexoDe_Tabela), 6, 0, ".", "")));
            Gridanexos_deColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbAnexoDe_Tabela.Enabled), 5, 0, ".", "")));
            Gridanexos_deContainer.AddColumnProperties(Gridanexos_deColumn);
            Gridanexos_deContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridanexos_de_Allowselection), 1, 0, ".", "")));
            Gridanexos_deContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridanexos_de_Selectioncolor), 9, 0, ".", "")));
            Gridanexos_deContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridanexos_de_Allowhovering), 1, 0, ".", "")));
            Gridanexos_deContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridanexos_de_Hoveringcolor), 9, 0, ".", "")));
            Gridanexos_deContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridanexos_de_Allowcollapsing), 1, 0, ".", "")));
            Gridanexos_deContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridanexos_de_Collapsed), 1, 0, ".", "")));
            nGXsfl_96_idx = 0;
            if ( ( nKeyPressed == 1 ) && ( AnyError == 0 ) )
            {
               /* Enter key processing. */
               nBlankRcdCount134 = 5;
               if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
               {
                  /* Display confirmed (stored) records */
                  nRcdExists_134 = 1;
                  ScanStart36134( ) ;
                  while ( RcdFound134 != 0 )
                  {
                     init_level_properties134( ) ;
                     getByPrimaryKey36134( ) ;
                     AddRow36134( ) ;
                     ScanNext36134( ) ;
                  }
                  ScanEnd36134( ) ;
                  nBlankRcdCount134 = 5;
               }
            }
            else if ( ( nKeyPressed == 3 ) || ( nKeyPressed == 4 ) || ( ( nKeyPressed == 1 ) && ( AnyError != 0 ) ) )
            {
               /* Button check  or addlines. */
               standaloneNotModal36134( ) ;
               standaloneModal36134( ) ;
               sMode134 = Gx_mode;
               while ( nGXsfl_96_idx < nRC_GXsfl_96 )
               {
                  ReadRow36134( ) ;
                  edtAnexoDe_Id_Enabled = (int)(context.localUtil.CToN( cgiGet( "ANEXODE_ID_"+sGXsfl_96_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAnexoDe_Id_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAnexoDe_Id_Enabled), 5, 0)));
                  cmbAnexoDe_Tabela.Enabled = (int)(context.localUtil.CToN( cgiGet( "ANEXODE_TABELA_"+sGXsfl_96_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAnexoDe_Tabela_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbAnexoDe_Tabela.Enabled), 5, 0)));
                  if ( ( nRcdExists_134 == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     standaloneModal36134( ) ;
                  }
                  SendRow36134( ) ;
               }
               Gx_mode = sMode134;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            }
            else
            {
               /* Get or get-alike key processing. */
               nBlankRcdCount134 = 5;
               nRcdExists_134 = 1;
               if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
               {
                  ScanStart36134( ) ;
                  while ( RcdFound134 != 0 )
                  {
                     sGXsfl_96_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_96_idx+1), 4, 0)), 4, "0");
                     SubsflControlProps_96134( ) ;
                     init_level_properties134( ) ;
                     standaloneNotModal36134( ) ;
                     getByPrimaryKey36134( ) ;
                     standaloneModal36134( ) ;
                     AddRow36134( ) ;
                     ScanNext36134( ) ;
                  }
                  ScanEnd36134( ) ;
               }
            }
            /* Initialize fields for 'new' records and send them. */
            sMode134 = Gx_mode;
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            sGXsfl_96_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_96_idx+1), 4, 0)), 4, "0");
            SubsflControlProps_96134( ) ;
            InitAll36134( ) ;
            init_level_properties134( ) ;
            standaloneNotModal36134( ) ;
            standaloneModal36134( ) ;
            nRcdExists_134 = 0;
            nIsMod_134 = 0;
            nRcdDeleted_134 = 0;
            nBlankRcdCount134 = (short)(nBlankRcdUsr134+nBlankRcdCount134);
            fRowAdded = 0;
            while ( nBlankRcdCount134 > 0 )
            {
               AddRow36134( ) ;
               if ( ( nKeyPressed == 4 ) && ( fRowAdded == 0 ) )
               {
                  fRowAdded = 1;
                  GX_FocusControl = edtAnexoDe_Id_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               nBlankRcdCount134 = (short)(nBlankRcdCount134-1);
            }
            Gx_mode = sMode134;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            sStyleString = "";
            context.WriteHtmlText( "<div id=\""+"Gridanexos_deContainer"+"Div\" "+sStyleString+">"+"</div>") ;
            context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridanexos_de", Gridanexos_deContainer);
            if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
            {
               GxWebStd.gx_hidden_field( context, "Gridanexos_deContainerData", Gridanexos_deContainer.ToJavascriptSource());
            }
            if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
            {
               GxWebStd.gx_hidden_field( context, "Gridanexos_deContainerData"+"V", Gridanexos_deContainer.GridValuesHidden());
            }
            else
            {
               context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Gridanexos_deContainerData"+"V"+"\" value='"+Gridanexos_deContainer.GridValuesHidden()+"'/>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_36133e( true) ;
         }
         else
         {
            wb_table4_34_36133e( false) ;
         }
      }

      protected void wb_table5_92_36133( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable3_Internalname, tblTable3_Internalname, "", "Table95", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='SubTitle'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTitlede_Internalname, "De", "", "", lblTitlede_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 0, "HLP_Anexos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_92_36133e( true) ;
         }
         else
         {
            wb_table5_92_36133e( false) ;
         }
      }

      protected void wb_table2_5_36133( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Anexos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Anexos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Anexos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Anexos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Anexos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Anexos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Anexos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Anexos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Anexos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Anexos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Anexos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Anexos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Anexos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Anexos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Anexos.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Anexos.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_36133e( true) ;
         }
         else
         {
            wb_table2_5_36133e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtAnexo_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtAnexo_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "ANEXO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtAnexo_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1106Anexo_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1106Anexo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1106Anexo_Codigo), 6, 0)));
               }
               else
               {
                  A1106Anexo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAnexo_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1106Anexo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1106Anexo_Codigo), 6, 0)));
               }
               A1101Anexo_Arquivo = cgiGet( edtAnexo_Arquivo_Internalname);
               n1101Anexo_Arquivo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1101Anexo_Arquivo", A1101Anexo_Arquivo);
               n1101Anexo_Arquivo = (String.IsNullOrEmpty(StringUtil.RTrim( A1101Anexo_Arquivo)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtAnexo_UserId_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtAnexo_UserId_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "ANEXO_USERID");
                  AnyError = 1;
                  GX_FocusControl = edtAnexo_UserId_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1102Anexo_UserId = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1102Anexo_UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1102Anexo_UserId), 6, 0)));
               }
               else
               {
                  A1102Anexo_UserId = (int)(context.localUtil.CToN( cgiGet( edtAnexo_UserId_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1102Anexo_UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1102Anexo_UserId), 6, 0)));
               }
               if ( context.localUtil.VCDateTime( cgiGet( edtAnexo_Data_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Upload"}), 1, "ANEXO_DATA");
                  AnyError = 1;
                  GX_FocusControl = edtAnexo_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1103Anexo_Data = (DateTime)(DateTime.MinValue);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1103Anexo_Data", context.localUtil.TToC( A1103Anexo_Data, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A1103Anexo_Data = context.localUtil.CToT( cgiGet( edtAnexo_Data_Internalname));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1103Anexo_Data", context.localUtil.TToC( A1103Anexo_Data, 8, 5, 0, 3, "/", ":", " "));
               }
               dynTipoDocumento_Codigo.Name = dynTipoDocumento_Codigo_Internalname;
               dynTipoDocumento_Codigo.CurrentValue = cgiGet( dynTipoDocumento_Codigo_Internalname);
               A645TipoDocumento_Codigo = (int)(NumberUtil.Val( cgiGet( dynTipoDocumento_Codigo_Internalname), "."));
               n645TipoDocumento_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
               n645TipoDocumento_Codigo = ((0==A645TipoDocumento_Codigo) ? true : false);
               A1123Anexo_Descricao = StringUtil.Upper( cgiGet( edtAnexo_Descricao_Internalname));
               n1123Anexo_Descricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1123Anexo_Descricao", A1123Anexo_Descricao);
               n1123Anexo_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A1123Anexo_Descricao)) ? true : false);
               A1450Anexo_Link = cgiGet( edtAnexo_Link_Internalname);
               n1450Anexo_Link = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1450Anexo_Link", A1450Anexo_Link);
               n1450Anexo_Link = (String.IsNullOrEmpty(StringUtil.RTrim( A1450Anexo_Link)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtAnexo_Owner_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtAnexo_Owner_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "ANEXO_OWNER");
                  AnyError = 1;
                  GX_FocusControl = edtAnexo_Owner_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1495Anexo_Owner = 0;
                  n1495Anexo_Owner = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1495Anexo_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1495Anexo_Owner), 6, 0)));
               }
               else
               {
                  A1495Anexo_Owner = (int)(context.localUtil.CToN( cgiGet( edtAnexo_Owner_Internalname), ",", "."));
                  n1495Anexo_Owner = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1495Anexo_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1495Anexo_Owner), 6, 0)));
               }
               n1495Anexo_Owner = ((0==A1495Anexo_Owner) ? true : false);
               A1498Anexo_DemandaCod = (int)(context.localUtil.CToN( cgiGet( edtAnexo_DemandaCod_Internalname), ",", "."));
               n1498Anexo_DemandaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1498Anexo_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1498Anexo_DemandaCod), 6, 0)));
               A1496Anexo_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtAnexo_AreaTrabalhoCod_Internalname), ",", "."));
               n1496Anexo_AreaTrabalhoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1496Anexo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0)));
               A1497Anexo_Entidade = StringUtil.Upper( cgiGet( edtAnexo_Entidade_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1497Anexo_Entidade", A1497Anexo_Entidade);
               /* Read saved values. */
               Z1106Anexo_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1106Anexo_Codigo"), ",", "."));
               Z1102Anexo_UserId = (int)(context.localUtil.CToN( cgiGet( "Z1102Anexo_UserId"), ",", "."));
               Z1103Anexo_Data = context.localUtil.CToT( cgiGet( "Z1103Anexo_Data"), 0);
               Z1123Anexo_Descricao = cgiGet( "Z1123Anexo_Descricao");
               n1123Anexo_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A1123Anexo_Descricao)) ? true : false);
               Z1495Anexo_Owner = (int)(context.localUtil.CToN( cgiGet( "Z1495Anexo_Owner"), ",", "."));
               n1495Anexo_Owner = ((0==A1495Anexo_Owner) ? true : false);
               Z645TipoDocumento_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z645TipoDocumento_Codigo"), ",", "."));
               n645TipoDocumento_Codigo = ((0==A645TipoDocumento_Codigo) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               nRC_GXsfl_96 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_96"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A1108Anexo_TipoArq = cgiGet( "ANEXO_TIPOARQ");
               n1108Anexo_TipoArq = (String.IsNullOrEmpty(StringUtil.RTrim( A1108Anexo_TipoArq)) ? true : false);
               A1107Anexo_NomeArq = cgiGet( "ANEXO_NOMEARQ");
               n1107Anexo_NomeArq = (String.IsNullOrEmpty(StringUtil.RTrim( A1107Anexo_NomeArq)) ? true : false);
               A646TipoDocumento_Nome = cgiGet( "TIPODOCUMENTO_NOME");
               Gx_mode = cgiGet( "vMODE");
               edtAnexo_Arquivo_Filetype = cgiGet( "ANEXO_ARQUIVO_Filetype");
               edtAnexo_Arquivo_Filename = cgiGet( "ANEXO_ARQUIVO_Filename");
               edtAnexo_Arquivo_Filename = cgiGet( "ANEXO_ARQUIVO_Filename");
               edtAnexo_Arquivo_Filetype = cgiGet( "ANEXO_ARQUIVO_Filetype");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1101Anexo_Arquivo)) )
               {
                  edtAnexo_Arquivo_Filename = (String)(CGIGetFileName(edtAnexo_Arquivo_Internalname));
                  edtAnexo_Arquivo_Filetype = (String)(CGIGetFileType(edtAnexo_Arquivo_Internalname));
               }
               A1108Anexo_TipoArq = edtAnexo_Arquivo_Filetype;
               n1108Anexo_TipoArq = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1108Anexo_TipoArq", A1108Anexo_TipoArq);
               A1107Anexo_NomeArq = edtAnexo_Arquivo_Filename;
               n1107Anexo_NomeArq = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1107Anexo_NomeArq", A1107Anexo_NomeArq);
               if ( String.IsNullOrEmpty(StringUtil.RTrim( A1101Anexo_Arquivo)) )
               {
                  GXCCtlgxBlob = "ANEXO_ARQUIVO" + "_gxBlob";
                  A1101Anexo_Arquivo = cgiGet( GXCCtlgxBlob);
                  n1101Anexo_Arquivo = false;
                  n1101Anexo_Arquivo = (String.IsNullOrEmpty(StringUtil.RTrim( A1101Anexo_Arquivo)) ? true : false);
               }
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               /* Check if conditions changed and reset current page numbers */
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A1106Anexo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1106Anexo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1106Anexo_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll36133( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes36133( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_36134( )
      {
         s1497Anexo_Entidade = O1497Anexo_Entidade;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1497Anexo_Entidade", A1497Anexo_Entidade);
         nGXsfl_96_idx = 0;
         while ( nGXsfl_96_idx < nRC_GXsfl_96 )
         {
            ReadRow36134( ) ;
            if ( ( nRcdExists_134 != 0 ) || ( nIsMod_134 != 0 ) )
            {
               GetKey36134( ) ;
               if ( ( nRcdExists_134 == 0 ) && ( nRcdDeleted_134 == 0 ) )
               {
                  if ( RcdFound134 == 0 )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     BeforeValidate36134( ) ;
                     if ( AnyError == 0 )
                     {
                        CheckExtendedTable36134( ) ;
                        CloseExtendedTableCursors36134( ) ;
                        if ( AnyError == 0 )
                        {
                           IsConfirmed = 1;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                        }
                        O1497Anexo_Entidade = A1497Anexo_Entidade;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1497Anexo_Entidade", A1497Anexo_Entidade);
                     }
                  }
                  else
                  {
                     GXCCtl = "ANEXODE_ID_" + sGXsfl_96_idx;
                     GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, GXCCtl);
                     AnyError = 1;
                     GX_FocusControl = edtAnexoDe_Id_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( RcdFound134 != 0 )
                  {
                     if ( nRcdDeleted_134 != 0 )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        getByPrimaryKey36134( ) ;
                        Load36134( ) ;
                        BeforeValidate36134( ) ;
                        if ( AnyError == 0 )
                        {
                           OnDeleteControls36134( ) ;
                           O1497Anexo_Entidade = A1497Anexo_Entidade;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1497Anexo_Entidade", A1497Anexo_Entidade);
                        }
                     }
                     else
                     {
                        if ( nIsMod_134 != 0 )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           BeforeValidate36134( ) ;
                           if ( AnyError == 0 )
                           {
                              CheckExtendedTable36134( ) ;
                              CloseExtendedTableCursors36134( ) ;
                              if ( AnyError == 0 )
                              {
                                 IsConfirmed = 1;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                              }
                              O1497Anexo_Entidade = A1497Anexo_Entidade;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1497Anexo_Entidade", A1497Anexo_Entidade);
                           }
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_134 == 0 )
                     {
                        GXCCtl = "ANEXODE_ID_" + sGXsfl_96_idx;
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, GXCCtl);
                        AnyError = 1;
                        GX_FocusControl = edtAnexoDe_Id_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
            ChangePostValue( edtAnexoDe_Id_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1109AnexoDe_Id), 6, 0, ",", ""))) ;
            ChangePostValue( cmbAnexoDe_Tabela_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1110AnexoDe_Tabela), 6, 0, ".", ""))) ;
            ChangePostValue( "ZT_"+"Z1109AnexoDe_Id_"+sGXsfl_96_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1109AnexoDe_Id), 6, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1110AnexoDe_Tabela_"+sGXsfl_96_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1110AnexoDe_Tabela), 6, 0, ",", ""))) ;
            ChangePostValue( "nRcdDeleted_134_"+sGXsfl_96_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_134), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_134_"+sGXsfl_96_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_134), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_134_"+sGXsfl_96_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_134), 4, 0, ",", ""))) ;
            if ( nIsMod_134 != 0 )
            {
               ChangePostValue( "ANEXODE_ID_"+sGXsfl_96_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAnexoDe_Id_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "ANEXODE_TABELA_"+sGXsfl_96_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbAnexoDe_Tabela.Enabled), 5, 0, ".", ""))) ;
            }
         }
         O1497Anexo_Entidade = s1497Anexo_Entidade;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1497Anexo_Entidade", A1497Anexo_Entidade);
         /* Start of After( level) rules */
         /* Using cursor T00369 */
         pr_default.execute(3, new Object[] {A1106Anexo_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            A1498Anexo_DemandaCod = T00369_A1498Anexo_DemandaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1498Anexo_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1498Anexo_DemandaCod), 6, 0)));
            n1498Anexo_DemandaCod = T00369_n1498Anexo_DemandaCod[0];
         }
         else
         {
            A1498Anexo_DemandaCod = 0;
            n1498Anexo_DemandaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1498Anexo_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1498Anexo_DemandaCod), 6, 0)));
         }
         /* Using cursor T00367 */
         pr_default.execute(2, new Object[] {n1498Anexo_DemandaCod, A1498Anexo_DemandaCod, A1106Anexo_Codigo});
         if ( (pr_default.getStatus(2) != 101) )
         {
            A1496Anexo_AreaTrabalhoCod = T00367_A1496Anexo_AreaTrabalhoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1496Anexo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0)));
            n1496Anexo_AreaTrabalhoCod = T00367_n1496Anexo_AreaTrabalhoCod[0];
         }
         else
         {
            A1496Anexo_AreaTrabalhoCod = 0;
            n1496Anexo_AreaTrabalhoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1496Anexo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0)));
         }
         GXt_char1 = A1497Anexo_Entidade;
         new prc_entidadedousuario(context ).execute(  A1495Anexo_Owner,  A1496Anexo_AreaTrabalhoCod, out  GXt_char1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1495Anexo_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1495Anexo_Owner), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1496Anexo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0)));
         A1497Anexo_Entidade = GXt_char1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1497Anexo_Entidade", A1497Anexo_Entidade);
         /* Using cursor T00367 */
         pr_default.execute(2, new Object[] {n1498Anexo_DemandaCod, A1498Anexo_DemandaCod, A1106Anexo_Codigo});
         if ( (pr_default.getStatus(2) != 101) )
         {
         }
         else
         {
            A1496Anexo_AreaTrabalhoCod = 0;
            n1496Anexo_AreaTrabalhoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1496Anexo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0)));
         }
         pr_default.close(2);
         /* End of After( level) rules */
      }

      protected void ResetCaption360( )
      {
      }

      protected void ZM36133( short GX_JID )
      {
         if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1102Anexo_UserId = T003611_A1102Anexo_UserId[0];
               Z1103Anexo_Data = T003611_A1103Anexo_Data[0];
               Z1123Anexo_Descricao = T003611_A1123Anexo_Descricao[0];
               Z1495Anexo_Owner = T003611_A1495Anexo_Owner[0];
               Z645TipoDocumento_Codigo = T003611_A645TipoDocumento_Codigo[0];
            }
            else
            {
               Z1102Anexo_UserId = A1102Anexo_UserId;
               Z1103Anexo_Data = A1103Anexo_Data;
               Z1123Anexo_Descricao = A1123Anexo_Descricao;
               Z1495Anexo_Owner = A1495Anexo_Owner;
               Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            }
         }
         if ( GX_JID == -6 )
         {
            Z1106Anexo_Codigo = A1106Anexo_Codigo;
            Z1101Anexo_Arquivo = A1101Anexo_Arquivo;
            Z1102Anexo_UserId = A1102Anexo_UserId;
            Z1103Anexo_Data = A1103Anexo_Data;
            Z1123Anexo_Descricao = A1123Anexo_Descricao;
            Z1450Anexo_Link = A1450Anexo_Link;
            Z1495Anexo_Owner = A1495Anexo_Owner;
            Z1108Anexo_TipoArq = A1108Anexo_TipoArq;
            Z1107Anexo_NomeArq = A1107Anexo_NomeArq;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z1498Anexo_DemandaCod = A1498Anexo_DemandaCod;
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (DateTime.MinValue==A1103Anexo_Data) && ( Gx_BScreen == 0 ) )
         {
            A1103Anexo_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1103Anexo_Data", context.localUtil.TToC( A1103Anexo_Data, 8, 5, 0, 3, "/", ":", " "));
         }
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
         }
      }

      protected void Load36133( )
      {
         /* Using cursor T003615 */
         pr_default.execute(8, new Object[] {A1106Anexo_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound133 = 1;
            A1102Anexo_UserId = T003615_A1102Anexo_UserId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1102Anexo_UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1102Anexo_UserId), 6, 0)));
            A1103Anexo_Data = T003615_A1103Anexo_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1103Anexo_Data", context.localUtil.TToC( A1103Anexo_Data, 8, 5, 0, 3, "/", ":", " "));
            A646TipoDocumento_Nome = T003615_A646TipoDocumento_Nome[0];
            A1123Anexo_Descricao = T003615_A1123Anexo_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1123Anexo_Descricao", A1123Anexo_Descricao);
            n1123Anexo_Descricao = T003615_n1123Anexo_Descricao[0];
            A1450Anexo_Link = T003615_A1450Anexo_Link[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1450Anexo_Link", A1450Anexo_Link);
            n1450Anexo_Link = T003615_n1450Anexo_Link[0];
            A1495Anexo_Owner = T003615_A1495Anexo_Owner[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1495Anexo_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1495Anexo_Owner), 6, 0)));
            n1495Anexo_Owner = T003615_n1495Anexo_Owner[0];
            A1108Anexo_TipoArq = T003615_A1108Anexo_TipoArq[0];
            n1108Anexo_TipoArq = T003615_n1108Anexo_TipoArq[0];
            edtAnexo_Arquivo_Filetype = A1108Anexo_TipoArq;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAnexo_Arquivo_Internalname, "Filetype", edtAnexo_Arquivo_Filetype);
            A1107Anexo_NomeArq = T003615_A1107Anexo_NomeArq[0];
            n1107Anexo_NomeArq = T003615_n1107Anexo_NomeArq[0];
            edtAnexo_Arquivo_Filename = A1107Anexo_NomeArq;
            A645TipoDocumento_Codigo = T003615_A645TipoDocumento_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
            n645TipoDocumento_Codigo = T003615_n645TipoDocumento_Codigo[0];
            A1498Anexo_DemandaCod = T003615_A1498Anexo_DemandaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1498Anexo_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1498Anexo_DemandaCod), 6, 0)));
            n1498Anexo_DemandaCod = T003615_n1498Anexo_DemandaCod[0];
            A1101Anexo_Arquivo = T003615_A1101Anexo_Arquivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1101Anexo_Arquivo", A1101Anexo_Arquivo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAnexo_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A1101Anexo_Arquivo));
            n1101Anexo_Arquivo = T003615_n1101Anexo_Arquivo[0];
            ZM36133( -6) ;
         }
         pr_default.close(8);
         OnLoadActions36133( ) ;
      }

      protected void OnLoadActions36133( )
      {
         /* Using cursor T00367 */
         pr_default.execute(2, new Object[] {n1498Anexo_DemandaCod, A1498Anexo_DemandaCod, A1106Anexo_Codigo});
         if ( (pr_default.getStatus(2) != 101) )
         {
            A1496Anexo_AreaTrabalhoCod = T00367_A1496Anexo_AreaTrabalhoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1496Anexo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0)));
            n1496Anexo_AreaTrabalhoCod = T00367_n1496Anexo_AreaTrabalhoCod[0];
         }
         else
         {
            A1496Anexo_AreaTrabalhoCod = 0;
            n1496Anexo_AreaTrabalhoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1496Anexo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0)));
         }
         pr_default.close(2);
         GXt_char1 = A1497Anexo_Entidade;
         new prc_entidadedousuario(context ).execute(  A1495Anexo_Owner,  A1496Anexo_AreaTrabalhoCod, out  GXt_char1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1495Anexo_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1495Anexo_Owner), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1496Anexo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0)));
         A1497Anexo_Entidade = GXt_char1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1497Anexo_Entidade", A1497Anexo_Entidade);
      }

      protected void CheckExtendedTable36133( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T00369 */
         pr_default.execute(3, new Object[] {A1106Anexo_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            A1498Anexo_DemandaCod = T00369_A1498Anexo_DemandaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1498Anexo_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1498Anexo_DemandaCod), 6, 0)));
            n1498Anexo_DemandaCod = T00369_n1498Anexo_DemandaCod[0];
         }
         else
         {
            A1498Anexo_DemandaCod = 0;
            n1498Anexo_DemandaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1498Anexo_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1498Anexo_DemandaCod), 6, 0)));
         }
         pr_default.close(3);
         /* Using cursor T00367 */
         pr_default.execute(2, new Object[] {n1498Anexo_DemandaCod, A1498Anexo_DemandaCod, A1106Anexo_Codigo});
         if ( (pr_default.getStatus(2) != 101) )
         {
            A1496Anexo_AreaTrabalhoCod = T00367_A1496Anexo_AreaTrabalhoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1496Anexo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0)));
            n1496Anexo_AreaTrabalhoCod = T00367_n1496Anexo_AreaTrabalhoCod[0];
         }
         else
         {
            A1496Anexo_AreaTrabalhoCod = 0;
            n1496Anexo_AreaTrabalhoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1496Anexo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0)));
         }
         pr_default.close(2);
         GXt_char1 = A1497Anexo_Entidade;
         new prc_entidadedousuario(context ).execute(  A1495Anexo_Owner,  A1496Anexo_AreaTrabalhoCod, out  GXt_char1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1495Anexo_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1495Anexo_Owner), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1496Anexo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0)));
         A1497Anexo_Entidade = GXt_char1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1497Anexo_Entidade", A1497Anexo_Entidade);
         if ( ! ( (DateTime.MinValue==A1103Anexo_Data) || ( A1103Anexo_Data >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Upload fora do intervalo", "OutOfRange", 1, "ANEXO_DATA");
            AnyError = 1;
            GX_FocusControl = edtAnexo_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T003612 */
         pr_default.execute(6, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( (0==A645TipoDocumento_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Tipo de Documentos'.", "ForeignKeyNotFound", 1, "TIPODOCUMENTO_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynTipoDocumento_Codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A646TipoDocumento_Nome = T003612_A646TipoDocumento_Nome[0];
         pr_default.close(6);
      }

      protected void CloseExtendedTableCursors36133( )
      {
         pr_default.close(3);
         pr_default.close(2);
         pr_default.close(6);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_9( int A1106Anexo_Codigo )
      {
         /* Using cursor T003617 */
         pr_default.execute(9, new Object[] {A1106Anexo_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            A1498Anexo_DemandaCod = T003617_A1498Anexo_DemandaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1498Anexo_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1498Anexo_DemandaCod), 6, 0)));
            n1498Anexo_DemandaCod = T003617_n1498Anexo_DemandaCod[0];
         }
         else
         {
            A1498Anexo_DemandaCod = 0;
            n1498Anexo_DemandaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1498Anexo_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1498Anexo_DemandaCod), 6, 0)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1498Anexo_DemandaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(9) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(9);
      }

      protected void gxLoad_7( int A1106Anexo_Codigo ,
                               int A1498Anexo_DemandaCod )
      {
         /* Using cursor T003621 */
         pr_default.execute(10, new Object[] {n1498Anexo_DemandaCod, A1498Anexo_DemandaCod, A1106Anexo_Codigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            A1496Anexo_AreaTrabalhoCod = T003621_A1496Anexo_AreaTrabalhoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1496Anexo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0)));
            n1496Anexo_AreaTrabalhoCod = T003621_n1496Anexo_AreaTrabalhoCod[0];
         }
         else
         {
            A1496Anexo_AreaTrabalhoCod = 0;
            n1496Anexo_AreaTrabalhoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1496Anexo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(10) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(10);
      }

      protected void gxLoad_8( int A645TipoDocumento_Codigo )
      {
         /* Using cursor T003622 */
         pr_default.execute(11, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
         if ( (pr_default.getStatus(11) == 101) )
         {
            if ( ! ( (0==A645TipoDocumento_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Tipo de Documentos'.", "ForeignKeyNotFound", 1, "TIPODOCUMENTO_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynTipoDocumento_Codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A646TipoDocumento_Nome = T003622_A646TipoDocumento_Nome[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A646TipoDocumento_Nome))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(11) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(11);
      }

      protected void GetKey36133( )
      {
         /* Using cursor T003623 */
         pr_default.execute(12, new Object[] {A1106Anexo_Codigo});
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound133 = 1;
         }
         else
         {
            RcdFound133 = 0;
         }
         pr_default.close(12);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T003611 */
         pr_default.execute(5, new Object[] {A1106Anexo_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            ZM36133( 6) ;
            RcdFound133 = 1;
            A1106Anexo_Codigo = T003611_A1106Anexo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1106Anexo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1106Anexo_Codigo), 6, 0)));
            A1102Anexo_UserId = T003611_A1102Anexo_UserId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1102Anexo_UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1102Anexo_UserId), 6, 0)));
            A1103Anexo_Data = T003611_A1103Anexo_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1103Anexo_Data", context.localUtil.TToC( A1103Anexo_Data, 8, 5, 0, 3, "/", ":", " "));
            A1123Anexo_Descricao = T003611_A1123Anexo_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1123Anexo_Descricao", A1123Anexo_Descricao);
            n1123Anexo_Descricao = T003611_n1123Anexo_Descricao[0];
            A1450Anexo_Link = T003611_A1450Anexo_Link[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1450Anexo_Link", A1450Anexo_Link);
            n1450Anexo_Link = T003611_n1450Anexo_Link[0];
            A1495Anexo_Owner = T003611_A1495Anexo_Owner[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1495Anexo_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1495Anexo_Owner), 6, 0)));
            n1495Anexo_Owner = T003611_n1495Anexo_Owner[0];
            A1108Anexo_TipoArq = T003611_A1108Anexo_TipoArq[0];
            n1108Anexo_TipoArq = T003611_n1108Anexo_TipoArq[0];
            edtAnexo_Arquivo_Filetype = A1108Anexo_TipoArq;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAnexo_Arquivo_Internalname, "Filetype", edtAnexo_Arquivo_Filetype);
            A1107Anexo_NomeArq = T003611_A1107Anexo_NomeArq[0];
            n1107Anexo_NomeArq = T003611_n1107Anexo_NomeArq[0];
            edtAnexo_Arquivo_Filename = A1107Anexo_NomeArq;
            A645TipoDocumento_Codigo = T003611_A645TipoDocumento_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
            n645TipoDocumento_Codigo = T003611_n645TipoDocumento_Codigo[0];
            A1101Anexo_Arquivo = T003611_A1101Anexo_Arquivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1101Anexo_Arquivo", A1101Anexo_Arquivo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAnexo_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A1101Anexo_Arquivo));
            n1101Anexo_Arquivo = T003611_n1101Anexo_Arquivo[0];
            Z1106Anexo_Codigo = A1106Anexo_Codigo;
            sMode133 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load36133( ) ;
            if ( AnyError == 1 )
            {
               RcdFound133 = 0;
               InitializeNonKey36133( ) ;
            }
            Gx_mode = sMode133;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound133 = 0;
            InitializeNonKey36133( ) ;
            sMode133 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode133;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(5);
      }

      protected void getEqualNoModal( )
      {
         GetKey36133( ) ;
         if ( RcdFound133 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound133 = 0;
         /* Using cursor T003624 */
         pr_default.execute(13, new Object[] {A1106Anexo_Codigo});
         if ( (pr_default.getStatus(13) != 101) )
         {
            while ( (pr_default.getStatus(13) != 101) && ( ( T003624_A1106Anexo_Codigo[0] < A1106Anexo_Codigo ) ) )
            {
               pr_default.readNext(13);
            }
            if ( (pr_default.getStatus(13) != 101) && ( ( T003624_A1106Anexo_Codigo[0] > A1106Anexo_Codigo ) ) )
            {
               A1106Anexo_Codigo = T003624_A1106Anexo_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1106Anexo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1106Anexo_Codigo), 6, 0)));
               RcdFound133 = 1;
            }
         }
         pr_default.close(13);
      }

      protected void move_previous( )
      {
         RcdFound133 = 0;
         /* Using cursor T003625 */
         pr_default.execute(14, new Object[] {A1106Anexo_Codigo});
         if ( (pr_default.getStatus(14) != 101) )
         {
            while ( (pr_default.getStatus(14) != 101) && ( ( T003625_A1106Anexo_Codigo[0] > A1106Anexo_Codigo ) ) )
            {
               pr_default.readNext(14);
            }
            if ( (pr_default.getStatus(14) != 101) && ( ( T003625_A1106Anexo_Codigo[0] < A1106Anexo_Codigo ) ) )
            {
               A1106Anexo_Codigo = T003625_A1106Anexo_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1106Anexo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1106Anexo_Codigo), 6, 0)));
               RcdFound133 = 1;
            }
         }
         pr_default.close(14);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey36133( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            A1497Anexo_Entidade = O1497Anexo_Entidade;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1497Anexo_Entidade", A1497Anexo_Entidade);
            GX_FocusControl = edtAnexo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert36133( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound133 == 1 )
            {
               if ( A1106Anexo_Codigo != Z1106Anexo_Codigo )
               {
                  A1106Anexo_Codigo = Z1106Anexo_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1106Anexo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1106Anexo_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "ANEXO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtAnexo_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  A1497Anexo_Entidade = O1497Anexo_Entidade;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1497Anexo_Entidade", A1497Anexo_Entidade);
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtAnexo_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  A1497Anexo_Entidade = O1497Anexo_Entidade;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1497Anexo_Entidade", A1497Anexo_Entidade);
                  Update36133( ) ;
                  GX_FocusControl = edtAnexo_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1106Anexo_Codigo != Z1106Anexo_Codigo )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  A1497Anexo_Entidade = O1497Anexo_Entidade;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1497Anexo_Entidade", A1497Anexo_Entidade);
                  GX_FocusControl = edtAnexo_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert36133( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "ANEXO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtAnexo_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     A1497Anexo_Entidade = O1497Anexo_Entidade;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1497Anexo_Entidade", A1497Anexo_Entidade);
                     GX_FocusControl = edtAnexo_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert36133( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A1106Anexo_Codigo != Z1106Anexo_Codigo )
         {
            A1106Anexo_Codigo = Z1106Anexo_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1106Anexo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1106Anexo_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "ANEXO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtAnexo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            A1497Anexo_Entidade = O1497Anexo_Entidade;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1497Anexo_Entidade", A1497Anexo_Entidade);
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtAnexo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound133 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "ANEXO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtAnexo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtAnexo_Arquivo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart36133( ) ;
         if ( RcdFound133 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtAnexo_Arquivo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd36133( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound133 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtAnexo_Arquivo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound133 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtAnexo_Arquivo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart36133( ) ;
         if ( RcdFound133 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound133 != 0 )
            {
               ScanNext36133( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtAnexo_Arquivo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd36133( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency36133( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T003610 */
            pr_default.execute(4, new Object[] {A1106Anexo_Codigo});
            if ( (pr_default.getStatus(4) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Anexos"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(4) == 101) || ( Z1102Anexo_UserId != T003610_A1102Anexo_UserId[0] ) || ( Z1103Anexo_Data != T003610_A1103Anexo_Data[0] ) || ( StringUtil.StrCmp(Z1123Anexo_Descricao, T003610_A1123Anexo_Descricao[0]) != 0 ) || ( Z1495Anexo_Owner != T003610_A1495Anexo_Owner[0] ) || ( Z645TipoDocumento_Codigo != T003610_A645TipoDocumento_Codigo[0] ) )
            {
               if ( Z1102Anexo_UserId != T003610_A1102Anexo_UserId[0] )
               {
                  GXUtil.WriteLog("anexos:[seudo value changed for attri]"+"Anexo_UserId");
                  GXUtil.WriteLogRaw("Old: ",Z1102Anexo_UserId);
                  GXUtil.WriteLogRaw("Current: ",T003610_A1102Anexo_UserId[0]);
               }
               if ( Z1103Anexo_Data != T003610_A1103Anexo_Data[0] )
               {
                  GXUtil.WriteLog("anexos:[seudo value changed for attri]"+"Anexo_Data");
                  GXUtil.WriteLogRaw("Old: ",Z1103Anexo_Data);
                  GXUtil.WriteLogRaw("Current: ",T003610_A1103Anexo_Data[0]);
               }
               if ( StringUtil.StrCmp(Z1123Anexo_Descricao, T003610_A1123Anexo_Descricao[0]) != 0 )
               {
                  GXUtil.WriteLog("anexos:[seudo value changed for attri]"+"Anexo_Descricao");
                  GXUtil.WriteLogRaw("Old: ",Z1123Anexo_Descricao);
                  GXUtil.WriteLogRaw("Current: ",T003610_A1123Anexo_Descricao[0]);
               }
               if ( Z1495Anexo_Owner != T003610_A1495Anexo_Owner[0] )
               {
                  GXUtil.WriteLog("anexos:[seudo value changed for attri]"+"Anexo_Owner");
                  GXUtil.WriteLogRaw("Old: ",Z1495Anexo_Owner);
                  GXUtil.WriteLogRaw("Current: ",T003610_A1495Anexo_Owner[0]);
               }
               if ( Z645TipoDocumento_Codigo != T003610_A645TipoDocumento_Codigo[0] )
               {
                  GXUtil.WriteLog("anexos:[seudo value changed for attri]"+"TipoDocumento_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z645TipoDocumento_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T003610_A645TipoDocumento_Codigo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Anexos"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert36133( )
      {
         BeforeValidate36133( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable36133( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM36133( 0) ;
            CheckOptimisticConcurrency36133( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm36133( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert36133( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003626 */
                     A1108Anexo_TipoArq = edtAnexo_Arquivo_Filetype;
                     n1108Anexo_TipoArq = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1108Anexo_TipoArq", A1108Anexo_TipoArq);
                     A1107Anexo_NomeArq = edtAnexo_Arquivo_Filename;
                     n1107Anexo_NomeArq = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1107Anexo_NomeArq", A1107Anexo_NomeArq);
                     pr_default.execute(15, new Object[] {n1101Anexo_Arquivo, A1101Anexo_Arquivo, A1102Anexo_UserId, A1103Anexo_Data, n1123Anexo_Descricao, A1123Anexo_Descricao, n1450Anexo_Link, A1450Anexo_Link, n1495Anexo_Owner, A1495Anexo_Owner, n1108Anexo_TipoArq, A1108Anexo_TipoArq, n1107Anexo_NomeArq, A1107Anexo_NomeArq, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
                     A1106Anexo_Codigo = T003626_A1106Anexo_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1106Anexo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1106Anexo_Codigo), 6, 0)));
                     pr_default.close(15);
                     dsDefault.SmartCacheProvider.SetUpdated("Anexos") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel36133( ) ;
                           if ( AnyError == 0 )
                           {
                              /* Save values for previous() function. */
                              GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                              ResetCaption360( ) ;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load36133( ) ;
            }
            EndLevel36133( ) ;
         }
         CloseExtendedTableCursors36133( ) ;
      }

      protected void Update36133( )
      {
         BeforeValidate36133( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable36133( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency36133( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm36133( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate36133( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003627 */
                     A1108Anexo_TipoArq = edtAnexo_Arquivo_Filetype;
                     n1108Anexo_TipoArq = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1108Anexo_TipoArq", A1108Anexo_TipoArq);
                     A1107Anexo_NomeArq = edtAnexo_Arquivo_Filename;
                     n1107Anexo_NomeArq = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1107Anexo_NomeArq", A1107Anexo_NomeArq);
                     pr_default.execute(16, new Object[] {A1102Anexo_UserId, A1103Anexo_Data, n1123Anexo_Descricao, A1123Anexo_Descricao, n1450Anexo_Link, A1450Anexo_Link, n1495Anexo_Owner, A1495Anexo_Owner, n1108Anexo_TipoArq, A1108Anexo_TipoArq, n1107Anexo_NomeArq, A1107Anexo_NomeArq, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo, A1106Anexo_Codigo});
                     pr_default.close(16);
                     dsDefault.SmartCacheProvider.SetUpdated("Anexos") ;
                     if ( (pr_default.getStatus(16) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Anexos"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate36133( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel36133( ) ;
                           if ( AnyError == 0 )
                           {
                              getByPrimaryKey( ) ;
                              GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                              ResetCaption360( ) ;
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel36133( ) ;
         }
         CloseExtendedTableCursors36133( ) ;
      }

      protected void DeferredUpdate36133( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor T003628 */
            pr_default.execute(17, new Object[] {n1101Anexo_Arquivo, A1101Anexo_Arquivo, A1106Anexo_Codigo});
            pr_default.close(17);
            dsDefault.SmartCacheProvider.SetUpdated("Anexos") ;
         }
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate36133( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency36133( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls36133( ) ;
            AfterConfirm36133( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete36133( ) ;
               if ( AnyError == 0 )
               {
                  A1497Anexo_Entidade = O1497Anexo_Entidade;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1497Anexo_Entidade", A1497Anexo_Entidade);
                  ScanStart36134( ) ;
                  while ( RcdFound134 != 0 )
                  {
                     getByPrimaryKey36134( ) ;
                     Delete36134( ) ;
                     ScanNext36134( ) ;
                     O1497Anexo_Entidade = A1497Anexo_Entidade;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1497Anexo_Entidade", A1497Anexo_Entidade);
                  }
                  ScanEnd36134( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003629 */
                     pr_default.execute(18, new Object[] {A1106Anexo_Codigo});
                     pr_default.close(18);
                     dsDefault.SmartCacheProvider.SetUpdated("Anexos") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( delete) rules */
                        /* End of After( delete) rules */
                        if ( AnyError == 0 )
                        {
                           move_next( ) ;
                           if ( RcdFound133 == 0 )
                           {
                              InitAll36133( ) ;
                              Gx_mode = "INS";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           }
                           else
                           {
                              getByPrimaryKey( ) ;
                              Gx_mode = "UPD";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           }
                           GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                           ResetCaption360( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
         sMode133 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel36133( ) ;
         Gx_mode = sMode133;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls36133( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T003631 */
            pr_default.execute(19, new Object[] {A1106Anexo_Codigo});
            if ( (pr_default.getStatus(19) != 101) )
            {
               A1498Anexo_DemandaCod = T003631_A1498Anexo_DemandaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1498Anexo_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1498Anexo_DemandaCod), 6, 0)));
               n1498Anexo_DemandaCod = T003631_n1498Anexo_DemandaCod[0];
            }
            else
            {
               A1498Anexo_DemandaCod = 0;
               n1498Anexo_DemandaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1498Anexo_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1498Anexo_DemandaCod), 6, 0)));
            }
            pr_default.close(19);
            /* Using cursor T003635 */
            pr_default.execute(20, new Object[] {n1498Anexo_DemandaCod, A1498Anexo_DemandaCod, A1106Anexo_Codigo});
            if ( (pr_default.getStatus(20) != 101) )
            {
               A1496Anexo_AreaTrabalhoCod = T003635_A1496Anexo_AreaTrabalhoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1496Anexo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0)));
               n1496Anexo_AreaTrabalhoCod = T003635_n1496Anexo_AreaTrabalhoCod[0];
            }
            else
            {
               A1496Anexo_AreaTrabalhoCod = 0;
               n1496Anexo_AreaTrabalhoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1496Anexo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0)));
            }
            pr_default.close(20);
            /* Using cursor T003636 */
            pr_default.execute(21, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
            A646TipoDocumento_Nome = T003636_A646TipoDocumento_Nome[0];
            pr_default.close(21);
            GXt_char1 = A1497Anexo_Entidade;
            new prc_entidadedousuario(context ).execute(  A1495Anexo_Owner,  A1496Anexo_AreaTrabalhoCod, out  GXt_char1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1495Anexo_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1495Anexo_Owner), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1496Anexo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0)));
            A1497Anexo_Entidade = GXt_char1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1497Anexo_Entidade", A1497Anexo_Entidade);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T003637 */
            pr_default.execute(22, new Object[] {A1106Anexo_Codigo});
            if ( (pr_default.getStatus(22) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Artefatos das OS"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(22);
         }
      }

      protected void ProcessNestedLevel36134( )
      {
         s1497Anexo_Entidade = O1497Anexo_Entidade;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1497Anexo_Entidade", A1497Anexo_Entidade);
         nGXsfl_96_idx = 0;
         while ( nGXsfl_96_idx < nRC_GXsfl_96 )
         {
            ReadRow36134( ) ;
            if ( ( nRcdExists_134 != 0 ) || ( nIsMod_134 != 0 ) )
            {
               standaloneNotModal36134( ) ;
               GetKey36134( ) ;
               if ( ( nRcdExists_134 == 0 ) && ( nRcdDeleted_134 == 0 ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  Insert36134( ) ;
               }
               else
               {
                  if ( RcdFound134 != 0 )
                  {
                     if ( ( nRcdDeleted_134 != 0 ) && ( nRcdExists_134 != 0 ) )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        Delete36134( ) ;
                     }
                     else
                     {
                        if ( ( nIsMod_134 != 0 ) && ( nRcdExists_134 != 0 ) )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           Update36134( ) ;
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_134 == 0 )
                     {
                        GXCCtl = "ANEXODE_ID_" + sGXsfl_96_idx;
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, GXCCtl);
                        AnyError = 1;
                        GX_FocusControl = edtAnexoDe_Id_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
               O1497Anexo_Entidade = A1497Anexo_Entidade;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1497Anexo_Entidade", A1497Anexo_Entidade);
            }
            ChangePostValue( edtAnexoDe_Id_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1109AnexoDe_Id), 6, 0, ",", ""))) ;
            ChangePostValue( cmbAnexoDe_Tabela_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1110AnexoDe_Tabela), 6, 0, ".", ""))) ;
            ChangePostValue( "ZT_"+"Z1109AnexoDe_Id_"+sGXsfl_96_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1109AnexoDe_Id), 6, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1110AnexoDe_Tabela_"+sGXsfl_96_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1110AnexoDe_Tabela), 6, 0, ",", ""))) ;
            ChangePostValue( "nRcdDeleted_134_"+sGXsfl_96_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_134), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_134_"+sGXsfl_96_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_134), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_134_"+sGXsfl_96_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_134), 4, 0, ",", ""))) ;
            if ( nIsMod_134 != 0 )
            {
               ChangePostValue( "ANEXODE_ID_"+sGXsfl_96_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAnexoDe_Id_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "ANEXODE_TABELA_"+sGXsfl_96_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbAnexoDe_Tabela.Enabled), 5, 0, ".", ""))) ;
            }
         }
         /* Start of After( level) rules */
         /* Using cursor T003631 */
         pr_default.execute(19, new Object[] {A1106Anexo_Codigo});
         if ( (pr_default.getStatus(19) != 101) )
         {
            A1498Anexo_DemandaCod = T003631_A1498Anexo_DemandaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1498Anexo_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1498Anexo_DemandaCod), 6, 0)));
            n1498Anexo_DemandaCod = T003631_n1498Anexo_DemandaCod[0];
         }
         else
         {
            A1498Anexo_DemandaCod = 0;
            n1498Anexo_DemandaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1498Anexo_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1498Anexo_DemandaCod), 6, 0)));
         }
         /* Using cursor T003635 */
         pr_default.execute(20, new Object[] {n1498Anexo_DemandaCod, A1498Anexo_DemandaCod, A1106Anexo_Codigo});
         if ( (pr_default.getStatus(20) != 101) )
         {
            A1496Anexo_AreaTrabalhoCod = T003635_A1496Anexo_AreaTrabalhoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1496Anexo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0)));
            n1496Anexo_AreaTrabalhoCod = T003635_n1496Anexo_AreaTrabalhoCod[0];
         }
         else
         {
            A1496Anexo_AreaTrabalhoCod = 0;
            n1496Anexo_AreaTrabalhoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1496Anexo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0)));
         }
         GXt_char1 = A1497Anexo_Entidade;
         new prc_entidadedousuario(context ).execute(  A1495Anexo_Owner,  A1496Anexo_AreaTrabalhoCod, out  GXt_char1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1495Anexo_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1495Anexo_Owner), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1496Anexo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0)));
         A1497Anexo_Entidade = GXt_char1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1497Anexo_Entidade", A1497Anexo_Entidade);
         /* Using cursor T003635 */
         pr_default.execute(20, new Object[] {n1498Anexo_DemandaCod, A1498Anexo_DemandaCod, A1106Anexo_Codigo});
         if ( (pr_default.getStatus(20) != 101) )
         {
         }
         else
         {
            A1496Anexo_AreaTrabalhoCod = 0;
            n1496Anexo_AreaTrabalhoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1496Anexo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0)));
         }
         pr_default.close(20);
         /* End of After( level) rules */
         InitAll36134( ) ;
         if ( AnyError != 0 )
         {
            O1497Anexo_Entidade = s1497Anexo_Entidade;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1497Anexo_Entidade", A1497Anexo_Entidade);
         }
         nRcdExists_134 = 0;
         nIsMod_134 = 0;
         nRcdDeleted_134 = 0;
      }

      protected void ProcessLevel36133( )
      {
         /* Save parent mode. */
         sMode133 = Gx_mode;
         ProcessNestedLevel36134( ) ;
         if ( AnyError != 0 )
         {
            O1497Anexo_Entidade = s1497Anexo_Entidade;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1497Anexo_Entidade", A1497Anexo_Entidade);
         }
         /* Restore parent mode. */
         Gx_mode = sMode133;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         /* ' Update level parameters */
      }

      protected void EndLevel36133( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(4);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete36133( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(5);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(20);
            pr_default.close(21);
            pr_default.close(19);
            context.CommitDataStores( "Anexos");
            if ( AnyError == 0 )
            {
               ConfirmValues360( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(5);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(20);
            pr_default.close(21);
            pr_default.close(19);
            context.RollbackDataStores( "Anexos");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart36133( )
      {
         /* Using cursor T003638 */
         pr_default.execute(23);
         RcdFound133 = 0;
         if ( (pr_default.getStatus(23) != 101) )
         {
            RcdFound133 = 1;
            A1106Anexo_Codigo = T003638_A1106Anexo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1106Anexo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1106Anexo_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext36133( )
      {
         /* Scan next routine */
         pr_default.readNext(23);
         RcdFound133 = 0;
         if ( (pr_default.getStatus(23) != 101) )
         {
            RcdFound133 = 1;
            A1106Anexo_Codigo = T003638_A1106Anexo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1106Anexo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1106Anexo_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd36133( )
      {
         pr_default.close(23);
      }

      protected void AfterConfirm36133( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert36133( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate36133( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete36133( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete36133( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate36133( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes36133( )
      {
         edtAnexo_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAnexo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAnexo_Codigo_Enabled), 5, 0)));
         edtAnexo_Arquivo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAnexo_Arquivo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAnexo_Arquivo_Enabled), 5, 0)));
         edtAnexo_UserId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAnexo_UserId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAnexo_UserId_Enabled), 5, 0)));
         edtAnexo_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAnexo_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAnexo_Data_Enabled), 5, 0)));
         dynTipoDocumento_Codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTipoDocumento_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynTipoDocumento_Codigo.Enabled), 5, 0)));
         edtAnexo_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAnexo_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAnexo_Descricao_Enabled), 5, 0)));
         edtAnexo_Link_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAnexo_Link_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAnexo_Link_Enabled), 5, 0)));
         edtAnexo_Owner_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAnexo_Owner_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAnexo_Owner_Enabled), 5, 0)));
         edtAnexo_DemandaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAnexo_DemandaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAnexo_DemandaCod_Enabled), 5, 0)));
         edtAnexo_AreaTrabalhoCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAnexo_AreaTrabalhoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAnexo_AreaTrabalhoCod_Enabled), 5, 0)));
         edtAnexo_Entidade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAnexo_Entidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAnexo_Entidade_Enabled), 5, 0)));
      }

      protected void ZM36134( short GX_JID )
      {
         if ( ( GX_JID == 10 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
            }
            else
            {
            }
         }
         if ( GX_JID == -10 )
         {
            Z1106Anexo_Codigo = A1106Anexo_Codigo;
            Z1109AnexoDe_Id = A1109AnexoDe_Id;
            Z1110AnexoDe_Tabela = A1110AnexoDe_Tabela;
         }
      }

      protected void standaloneNotModal36134( )
      {
         GXt_char1 = A1497Anexo_Entidade;
         new prc_entidadedousuario(context ).execute(  A1495Anexo_Owner,  A1496Anexo_AreaTrabalhoCod, out  GXt_char1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1495Anexo_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1495Anexo_Owner), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1496Anexo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0)));
         A1497Anexo_Entidade = GXt_char1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1497Anexo_Entidade", A1497Anexo_Entidade);
      }

      protected void standaloneModal36134( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            edtAnexoDe_Id_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAnexoDe_Id_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAnexoDe_Id_Enabled), 5, 0)));
         }
         else
         {
            edtAnexoDe_Id_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAnexoDe_Id_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAnexoDe_Id_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            cmbAnexoDe_Tabela.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAnexoDe_Tabela_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbAnexoDe_Tabela.Enabled), 5, 0)));
         }
         else
         {
            cmbAnexoDe_Tabela.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAnexoDe_Tabela_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbAnexoDe_Tabela.Enabled), 5, 0)));
         }
      }

      protected void Load36134( )
      {
         /* Using cursor T003639 */
         pr_default.execute(24, new Object[] {A1106Anexo_Codigo, A1109AnexoDe_Id, A1110AnexoDe_Tabela});
         if ( (pr_default.getStatus(24) != 101) )
         {
            RcdFound134 = 1;
            ZM36134( -10) ;
         }
         pr_default.close(24);
         OnLoadActions36134( ) ;
      }

      protected void OnLoadActions36134( )
      {
      }

      protected void CheckExtendedTable36134( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal36134( ) ;
         if ( ! ( ( A1110AnexoDe_Tabela == 1 ) || ( A1110AnexoDe_Tabela == 2 ) || ( A1110AnexoDe_Tabela == 3 ) ) )
         {
            GXCCtl = "ANEXODE_TABELA_" + sGXsfl_96_idx;
            GX_msglist.addItem("Campo Tabela fora do intervalo", "OutOfRange", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = cmbAnexoDe_Tabela_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors36134( )
      {
      }

      protected void enableDisable36134( )
      {
      }

      protected void GetKey36134( )
      {
         /* Using cursor T003640 */
         pr_default.execute(25, new Object[] {A1106Anexo_Codigo, A1109AnexoDe_Id, A1110AnexoDe_Tabela});
         if ( (pr_default.getStatus(25) != 101) )
         {
            RcdFound134 = 1;
         }
         else
         {
            RcdFound134 = 0;
         }
         pr_default.close(25);
      }

      protected void getByPrimaryKey36134( )
      {
         /* Using cursor T00363 */
         pr_default.execute(1, new Object[] {A1106Anexo_Codigo, A1109AnexoDe_Id, A1110AnexoDe_Tabela});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM36134( 10) ;
            RcdFound134 = 1;
            InitializeNonKey36134( ) ;
            A1109AnexoDe_Id = T00363_A1109AnexoDe_Id[0];
            A1110AnexoDe_Tabela = T00363_A1110AnexoDe_Tabela[0];
            Z1106Anexo_Codigo = A1106Anexo_Codigo;
            Z1109AnexoDe_Id = A1109AnexoDe_Id;
            Z1110AnexoDe_Tabela = A1110AnexoDe_Tabela;
            sMode134 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal36134( ) ;
            Load36134( ) ;
            Gx_mode = sMode134;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound134 = 0;
            InitializeNonKey36134( ) ;
            sMode134 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal36134( ) ;
            Gx_mode = sMode134;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            DisableAttributes36134( ) ;
         }
         pr_default.close(1);
      }

      protected void CheckOptimisticConcurrency36134( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00362 */
            pr_default.execute(0, new Object[] {A1106Anexo_Codigo, A1109AnexoDe_Id, A1110AnexoDe_Tabela});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"AnexosDe"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"AnexosDe"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert36134( )
      {
         BeforeValidate36134( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable36134( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM36134( 0) ;
            CheckOptimisticConcurrency36134( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm36134( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert36134( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003641 */
                     pr_default.execute(26, new Object[] {A1106Anexo_Codigo, A1109AnexoDe_Id, A1110AnexoDe_Tabela});
                     pr_default.close(26);
                     dsDefault.SmartCacheProvider.SetUpdated("AnexosDe") ;
                     if ( (pr_default.getStatus(26) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load36134( ) ;
            }
            EndLevel36134( ) ;
         }
         CloseExtendedTableCursors36134( ) ;
      }

      protected void Update36134( )
      {
         BeforeValidate36134( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable36134( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency36134( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm36134( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate36134( ) ;
                  if ( AnyError == 0 )
                  {
                     /* No attributes to update on table [AnexosDe] */
                     DeferredUpdate36134( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey36134( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel36134( ) ;
         }
         CloseExtendedTableCursors36134( ) ;
      }

      protected void DeferredUpdate36134( )
      {
      }

      protected void Delete36134( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate36134( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency36134( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls36134( ) ;
            AfterConfirm36134( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete36134( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003642 */
                  pr_default.execute(27, new Object[] {A1106Anexo_Codigo, A1109AnexoDe_Id, A1110AnexoDe_Tabela});
                  pr_default.close(27);
                  dsDefault.SmartCacheProvider.SetUpdated("AnexosDe") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode134 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel36134( ) ;
         Gx_mode = sMode134;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls36134( )
      {
         standaloneModal36134( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel36134( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart36134( )
      {
         /* Scan By routine */
         /* Using cursor T003643 */
         pr_default.execute(28, new Object[] {A1106Anexo_Codigo});
         RcdFound134 = 0;
         if ( (pr_default.getStatus(28) != 101) )
         {
            RcdFound134 = 1;
            A1109AnexoDe_Id = T003643_A1109AnexoDe_Id[0];
            A1110AnexoDe_Tabela = T003643_A1110AnexoDe_Tabela[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext36134( )
      {
         /* Scan next routine */
         pr_default.readNext(28);
         RcdFound134 = 0;
         if ( (pr_default.getStatus(28) != 101) )
         {
            RcdFound134 = 1;
            A1109AnexoDe_Id = T003643_A1109AnexoDe_Id[0];
            A1110AnexoDe_Tabela = T003643_A1110AnexoDe_Tabela[0];
         }
      }

      protected void ScanEnd36134( )
      {
         pr_default.close(28);
      }

      protected void AfterConfirm36134( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert36134( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate36134( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete36134( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete36134( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate36134( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes36134( )
      {
         edtAnexoDe_Id_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAnexoDe_Id_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAnexoDe_Id_Enabled), 5, 0)));
         cmbAnexoDe_Tabela.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAnexoDe_Tabela_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbAnexoDe_Tabela.Enabled), 5, 0)));
      }

      protected void SubsflControlProps_96134( )
      {
         edtAnexoDe_Id_Internalname = "ANEXODE_ID_"+sGXsfl_96_idx;
         cmbAnexoDe_Tabela_Internalname = "ANEXODE_TABELA_"+sGXsfl_96_idx;
      }

      protected void SubsflControlProps_fel_96134( )
      {
         edtAnexoDe_Id_Internalname = "ANEXODE_ID_"+sGXsfl_96_fel_idx;
         cmbAnexoDe_Tabela_Internalname = "ANEXODE_TABELA_"+sGXsfl_96_fel_idx;
      }

      protected void AddRow36134( )
      {
         nGXsfl_96_idx = (short)(nGXsfl_96_idx+1);
         sGXsfl_96_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_96_idx), 4, 0)), 4, "0");
         SubsflControlProps_96134( ) ;
         SendRow36134( ) ;
      }

      protected void SendRow36134( )
      {
         Gridanexos_deRow = GXWebRow.GetNew(context);
         if ( subGridanexos_de_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGridanexos_de_Backstyle = 0;
            if ( StringUtil.StrCmp(subGridanexos_de_Class, "") != 0 )
            {
               subGridanexos_de_Linesclass = subGridanexos_de_Class+"Odd";
            }
         }
         else if ( subGridanexos_de_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGridanexos_de_Backstyle = 0;
            subGridanexos_de_Backcolor = subGridanexos_de_Allbackcolor;
            if ( StringUtil.StrCmp(subGridanexos_de_Class, "") != 0 )
            {
               subGridanexos_de_Linesclass = subGridanexos_de_Class+"Uniform";
            }
         }
         else if ( subGridanexos_de_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGridanexos_de_Backstyle = 1;
            if ( StringUtil.StrCmp(subGridanexos_de_Class, "") != 0 )
            {
               subGridanexos_de_Linesclass = subGridanexos_de_Class+"Odd";
            }
            subGridanexos_de_Backcolor = (int)(0xF0F0F0);
         }
         else if ( subGridanexos_de_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGridanexos_de_Backstyle = 1;
            if ( ((int)((nGXsfl_96_idx) % (2))) == 0 )
            {
               subGridanexos_de_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridanexos_de_Class, "") != 0 )
               {
                  subGridanexos_de_Linesclass = subGridanexos_de_Class+"Even";
               }
            }
            else
            {
               subGridanexos_de_Backcolor = (int)(0xF0F0F0);
               if ( StringUtil.StrCmp(subGridanexos_de_Class, "") != 0 )
               {
                  subGridanexos_de_Linesclass = subGridanexos_de_Class+"Odd";
               }
            }
         }
         /* Subfile cell */
         /* Single line edit */
         TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_134_" + sGXsfl_96_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_96_idx + "',96)\"";
         ROClassString = "Attribute";
         Gridanexos_deRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAnexoDe_Id_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1109AnexoDe_Id), 6, 0, ",", "")),StringUtil.LTrim( context.localUtil.Format( (decimal)(A1109AnexoDe_Id), "ZZZZZ9")),TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,97);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAnexoDe_Id_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtAnexoDe_Id_Enabled,(short)1,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)96,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
         /* Subfile cell */
         TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_134_" + sGXsfl_96_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_96_idx + "',96)\"";
         GXCCtl = "ANEXODE_TABELA_" + sGXsfl_96_idx;
         cmbAnexoDe_Tabela.Name = GXCCtl;
         cmbAnexoDe_Tabela.WebTags = "";
         cmbAnexoDe_Tabela.addItem("1", "Contagem Resultado", 0);
         cmbAnexoDe_Tabela.addItem("2", "Evid�ncias", 0);
         cmbAnexoDe_Tabela.addItem("3", "Anexos", 0);
         if ( cmbAnexoDe_Tabela.ItemCount > 0 )
         {
            A1110AnexoDe_Tabela = (int)(NumberUtil.Val( cmbAnexoDe_Tabela.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1110AnexoDe_Tabela), 6, 0))), "."));
         }
         /* ComboBox */
         Gridanexos_deRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbAnexoDe_Tabela,(String)cmbAnexoDe_Tabela_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A1110AnexoDe_Tabela), 6, 0)),(short)1,(String)cmbAnexoDe_Tabela_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,cmbAnexoDe_Tabela.Enabled,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,98);\"",(String)"",(bool)true});
         cmbAnexoDe_Tabela.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1110AnexoDe_Tabela), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAnexoDe_Tabela_Internalname, "Values", (String)(cmbAnexoDe_Tabela.ToJavascriptSource()));
         context.httpAjaxContext.ajax_sending_grid_row(Gridanexos_deRow);
         GXCCtl = "Z1109AnexoDe_Id_" + sGXsfl_96_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1109AnexoDe_Id), 6, 0, ",", "")));
         GXCCtl = "Z1110AnexoDe_Tabela_" + sGXsfl_96_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1110AnexoDe_Tabela), 6, 0, ",", "")));
         GXCCtl = "nRcdDeleted_134_" + sGXsfl_96_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_134), 4, 0, ",", "")));
         GXCCtl = "nRcdExists_134_" + sGXsfl_96_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_134), 4, 0, ",", "")));
         GXCCtl = "nIsMod_134_" + sGXsfl_96_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_134), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ANEXODE_ID_"+sGXsfl_96_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAnexoDe_Id_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "ANEXODE_TABELA_"+sGXsfl_96_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbAnexoDe_Tabela.Enabled), 5, 0, ".", "")));
         context.httpAjaxContext.ajax_sending_grid_row(null);
         Gridanexos_deContainer.AddRow(Gridanexos_deRow);
      }

      protected void ReadRow36134( )
      {
         nGXsfl_96_idx = (short)(nGXsfl_96_idx+1);
         sGXsfl_96_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_96_idx), 4, 0)), 4, "0");
         SubsflControlProps_96134( ) ;
         edtAnexoDe_Id_Enabled = (int)(context.localUtil.CToN( cgiGet( "ANEXODE_ID_"+sGXsfl_96_idx+"Enabled"), ",", "."));
         cmbAnexoDe_Tabela.Enabled = (int)(context.localUtil.CToN( cgiGet( "ANEXODE_TABELA_"+sGXsfl_96_idx+"Enabled"), ",", "."));
         if ( ( ( context.localUtil.CToN( cgiGet( edtAnexoDe_Id_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtAnexoDe_Id_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
         {
            GXCCtl = "ANEXODE_ID_" + sGXsfl_96_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtAnexoDe_Id_Internalname;
            wbErr = true;
            A1109AnexoDe_Id = 0;
         }
         else
         {
            A1109AnexoDe_Id = (int)(context.localUtil.CToN( cgiGet( edtAnexoDe_Id_Internalname), ",", "."));
         }
         cmbAnexoDe_Tabela.Name = cmbAnexoDe_Tabela_Internalname;
         cmbAnexoDe_Tabela.CurrentValue = cgiGet( cmbAnexoDe_Tabela_Internalname);
         A1110AnexoDe_Tabela = (int)(NumberUtil.Val( cgiGet( cmbAnexoDe_Tabela_Internalname), "."));
         GXCCtl = "Z1109AnexoDe_Id_" + sGXsfl_96_idx;
         Z1109AnexoDe_Id = (int)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "Z1110AnexoDe_Tabela_" + sGXsfl_96_idx;
         Z1110AnexoDe_Tabela = (int)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nRcdDeleted_134_" + sGXsfl_96_idx;
         nRcdDeleted_134 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nRcdExists_134_" + sGXsfl_96_idx;
         nRcdExists_134 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nIsMod_134_" + sGXsfl_96_idx;
         nIsMod_134 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
      }

      protected void assign_properties_default( )
      {
         defcmbAnexoDe_Tabela_Enabled = cmbAnexoDe_Tabela.Enabled;
         defedtAnexoDe_Id_Enabled = edtAnexoDe_Id_Enabled;
      }

      protected void ConfirmValues360( )
      {
         nGXsfl_96_idx = 0;
         sGXsfl_96_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_96_idx), 4, 0)), 4, "0");
         SubsflControlProps_96134( ) ;
         while ( nGXsfl_96_idx < nRC_GXsfl_96 )
         {
            nGXsfl_96_idx = (short)(nGXsfl_96_idx+1);
            sGXsfl_96_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_96_idx), 4, 0)), 4, "0");
            SubsflControlProps_96134( ) ;
            ChangePostValue( "Z1109AnexoDe_Id_"+sGXsfl_96_idx, cgiGet( "ZT_"+"Z1109AnexoDe_Id_"+sGXsfl_96_idx)) ;
            DeletePostValue( "ZT_"+"Z1109AnexoDe_Id_"+sGXsfl_96_idx) ;
            ChangePostValue( "Z1110AnexoDe_Tabela_"+sGXsfl_96_idx, cgiGet( "ZT_"+"Z1110AnexoDe_Tabela_"+sGXsfl_96_idx)) ;
            DeletePostValue( "ZT_"+"Z1110AnexoDe_Tabela_"+sGXsfl_96_idx) ;
         }
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216173070");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("anexos.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1106Anexo_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1106Anexo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1102Anexo_UserId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1102Anexo_UserId), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1103Anexo_Data", context.localUtil.TToC( Z1103Anexo_Data, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z1123Anexo_Descricao", Z1123Anexo_Descricao);
         GxWebStd.gx_hidden_field( context, "Z1495Anexo_Owner", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1495Anexo_Owner), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z645TipoDocumento_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_96", StringUtil.LTrim( StringUtil.NToC( (decimal)(nGXsfl_96_idx), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ANEXO_TIPOARQ", StringUtil.RTrim( A1108Anexo_TipoArq));
         GxWebStd.gx_hidden_field( context, "ANEXO_NOMEARQ", StringUtil.RTrim( A1107Anexo_NomeArq));
         GxWebStd.gx_hidden_field( context, "TIPODOCUMENTO_NOME", StringUtil.RTrim( A646TipoDocumento_Nome));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXCCtlgxBlob = "ANEXO_ARQUIVO" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, A1101Anexo_Arquivo);
         GxWebStd.gx_hidden_field( context, "ANEXO_ARQUIVO_Filetype", StringUtil.RTrim( edtAnexo_Arquivo_Filetype));
         GxWebStd.gx_hidden_field( context, "ANEXO_ARQUIVO_Filename", StringUtil.RTrim( edtAnexo_Arquivo_Filename));
         GxWebStd.gx_hidden_field( context, "ANEXO_ARQUIVO_Filename", StringUtil.RTrim( edtAnexo_Arquivo_Filename));
         GxWebStd.gx_hidden_field( context, "ANEXO_ARQUIVO_Filetype", StringUtil.RTrim( edtAnexo_Arquivo_Filetype));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("anexos.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "Anexos" ;
      }

      public override String GetPgmdesc( )
      {
         return "Anexos" ;
      }

      protected void InitializeNonKey36133( )
      {
         A1496Anexo_AreaTrabalhoCod = 0;
         n1496Anexo_AreaTrabalhoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1496Anexo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0)));
         A1497Anexo_Entidade = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1497Anexo_Entidade", A1497Anexo_Entidade);
         A1101Anexo_Arquivo = "";
         n1101Anexo_Arquivo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1101Anexo_Arquivo", A1101Anexo_Arquivo);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAnexo_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A1101Anexo_Arquivo));
         n1101Anexo_Arquivo = (String.IsNullOrEmpty(StringUtil.RTrim( A1101Anexo_Arquivo)) ? true : false);
         A1102Anexo_UserId = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1102Anexo_UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1102Anexo_UserId), 6, 0)));
         A645TipoDocumento_Codigo = 0;
         n645TipoDocumento_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
         n645TipoDocumento_Codigo = ((0==A645TipoDocumento_Codigo) ? true : false);
         A646TipoDocumento_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A646TipoDocumento_Nome", A646TipoDocumento_Nome);
         A1123Anexo_Descricao = "";
         n1123Anexo_Descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1123Anexo_Descricao", A1123Anexo_Descricao);
         n1123Anexo_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A1123Anexo_Descricao)) ? true : false);
         A1450Anexo_Link = "";
         n1450Anexo_Link = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1450Anexo_Link", A1450Anexo_Link);
         n1450Anexo_Link = (String.IsNullOrEmpty(StringUtil.RTrim( A1450Anexo_Link)) ? true : false);
         A1495Anexo_Owner = 0;
         n1495Anexo_Owner = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1495Anexo_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1495Anexo_Owner), 6, 0)));
         n1495Anexo_Owner = ((0==A1495Anexo_Owner) ? true : false);
         A1498Anexo_DemandaCod = 0;
         n1498Anexo_DemandaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1498Anexo_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1498Anexo_DemandaCod), 6, 0)));
         A1108Anexo_TipoArq = "";
         n1108Anexo_TipoArq = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1108Anexo_TipoArq", A1108Anexo_TipoArq);
         A1107Anexo_NomeArq = "";
         n1107Anexo_NomeArq = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1107Anexo_NomeArq", A1107Anexo_NomeArq);
         A1103Anexo_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1103Anexo_Data", context.localUtil.TToC( A1103Anexo_Data, 8, 5, 0, 3, "/", ":", " "));
         Z1102Anexo_UserId = 0;
         Z1103Anexo_Data = (DateTime)(DateTime.MinValue);
         Z1123Anexo_Descricao = "";
         Z1495Anexo_Owner = 0;
         Z645TipoDocumento_Codigo = 0;
      }

      protected void InitAll36133( )
      {
         A1106Anexo_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1106Anexo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1106Anexo_Codigo), 6, 0)));
         InitializeNonKey36133( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A1103Anexo_Data = i1103Anexo_Data;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1103Anexo_Data", context.localUtil.TToC( A1103Anexo_Data, 8, 5, 0, 3, "/", ":", " "));
      }

      protected void InitializeNonKey36134( )
      {
      }

      protected void InitAll36134( )
      {
         A1109AnexoDe_Id = 0;
         A1110AnexoDe_Tabela = 0;
         InitializeNonKey36134( ) ;
      }

      protected void StandaloneModalInsert36134( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216173082");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("anexos.js", "?20206216173082");
         /* End function include_jscripts */
      }

      protected void init_level_properties134( )
      {
         cmbAnexoDe_Tabela.Enabled = defcmbAnexoDe_Tabela_Enabled;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAnexoDe_Tabela_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbAnexoDe_Tabela.Enabled), 5, 0)));
         edtAnexoDe_Id_Enabled = defedtAnexoDe_Id_Enabled;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAnexoDe_Id_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAnexoDe_Id_Enabled), 5, 0)));
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockanexo_codigo_Internalname = "TEXTBLOCKANEXO_CODIGO";
         edtAnexo_Codigo_Internalname = "ANEXO_CODIGO";
         lblTextblockanexo_arquivo_Internalname = "TEXTBLOCKANEXO_ARQUIVO";
         edtAnexo_Arquivo_Internalname = "ANEXO_ARQUIVO";
         lblTextblockanexo_userid_Internalname = "TEXTBLOCKANEXO_USERID";
         edtAnexo_UserId_Internalname = "ANEXO_USERID";
         lblTextblockanexo_data_Internalname = "TEXTBLOCKANEXO_DATA";
         edtAnexo_Data_Internalname = "ANEXO_DATA";
         lblTextblocktipodocumento_codigo_Internalname = "TEXTBLOCKTIPODOCUMENTO_CODIGO";
         dynTipoDocumento_Codigo_Internalname = "TIPODOCUMENTO_CODIGO";
         lblTextblockanexo_descricao_Internalname = "TEXTBLOCKANEXO_DESCRICAO";
         edtAnexo_Descricao_Internalname = "ANEXO_DESCRICAO";
         lblTextblockanexo_link_Internalname = "TEXTBLOCKANEXO_LINK";
         edtAnexo_Link_Internalname = "ANEXO_LINK";
         lblTextblockanexo_owner_Internalname = "TEXTBLOCKANEXO_OWNER";
         edtAnexo_Owner_Internalname = "ANEXO_OWNER";
         lblTextblockanexo_demandacod_Internalname = "TEXTBLOCKANEXO_DEMANDACOD";
         edtAnexo_DemandaCod_Internalname = "ANEXO_DEMANDACOD";
         lblTextblockanexo_areatrabalhocod_Internalname = "TEXTBLOCKANEXO_AREATRABALHOCOD";
         edtAnexo_AreaTrabalhoCod_Internalname = "ANEXO_AREATRABALHOCOD";
         lblTextblockanexo_entidade_Internalname = "TEXTBLOCKANEXO_ENTIDADE";
         edtAnexo_Entidade_Internalname = "ANEXO_ENTIDADE";
         lblTitlede_Internalname = "TITLEDE";
         tblTable3_Internalname = "TABLE3";
         edtAnexoDe_Id_Internalname = "ANEXODE_ID";
         cmbAnexoDe_Tabela_Internalname = "ANEXODE_TABELA";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
         subGridanexos_de_Internalname = "GRIDANEXOS_DE";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Anexos";
         cmbAnexoDe_Tabela_Jsonclick = "";
         edtAnexoDe_Id_Jsonclick = "";
         subGridanexos_de_Class = "Grid";
         edtAnexo_Arquivo_Filename = "";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         subGridanexos_de_Allowcollapsing = 0;
         subGridanexos_de_Allowselection = 0;
         cmbAnexoDe_Tabela.Enabled = 1;
         edtAnexoDe_Id_Enabled = 1;
         subGridanexos_de_Backcolorstyle = 2;
         edtAnexo_Entidade_Jsonclick = "";
         edtAnexo_Entidade_Enabled = 0;
         edtAnexo_AreaTrabalhoCod_Jsonclick = "";
         edtAnexo_AreaTrabalhoCod_Enabled = 0;
         edtAnexo_DemandaCod_Jsonclick = "";
         edtAnexo_DemandaCod_Enabled = 0;
         edtAnexo_Owner_Jsonclick = "";
         edtAnexo_Owner_Enabled = 1;
         edtAnexo_Link_Jsonclick = "";
         edtAnexo_Link_Enabled = 1;
         edtAnexo_Descricao_Jsonclick = "";
         edtAnexo_Descricao_Enabled = 1;
         dynTipoDocumento_Codigo_Jsonclick = "";
         dynTipoDocumento_Codigo.Enabled = 1;
         edtAnexo_Data_Jsonclick = "";
         edtAnexo_Data_Enabled = 1;
         edtAnexo_UserId_Jsonclick = "";
         edtAnexo_UserId_Enabled = 1;
         edtAnexo_Arquivo_Jsonclick = "";
         edtAnexo_Arquivo_Parameters = "";
         edtAnexo_Arquivo_Contenttype = "";
         edtAnexo_Arquivo_Filetype = "";
         edtAnexo_Arquivo_Enabled = 1;
         edtAnexo_Codigo_Jsonclick = "";
         edtAnexo_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLATIPODOCUMENTO_CODIGO361( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLATIPODOCUMENTO_CODIGO_data361( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXATIPODOCUMENTO_CODIGO_html361( )
      {
         int gxdynajaxvalue ;
         GXDLATIPODOCUMENTO_CODIGO_data361( ) ;
         gxdynajaxindex = 1;
         dynTipoDocumento_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynTipoDocumento_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLATIPODOCUMENTO_CODIGO_data361( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor T003644 */
         pr_default.execute(29);
         while ( (pr_default.getStatus(29) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T003644_A645TipoDocumento_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T003644_A646TipoDocumento_Nome[0]));
            pr_default.readNext(29);
         }
         pr_default.close(29);
      }

      protected void GX2ASAANEXO_ENTIDADE36133( int A1495Anexo_Owner ,
                                                int A1496Anexo_AreaTrabalhoCod )
      {
         GXt_char1 = A1497Anexo_Entidade;
         new prc_entidadedousuario(context ).execute(  A1495Anexo_Owner,  A1496Anexo_AreaTrabalhoCod, out  GXt_char1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1495Anexo_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1495Anexo_Owner), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1496Anexo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0)));
         A1497Anexo_Entidade = GXt_char1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1497Anexo_Entidade", A1497Anexo_Entidade);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1497Anexo_Entidade))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX5ASAANEXO_ENTIDADE36134( int A1495Anexo_Owner ,
                                                int A1496Anexo_AreaTrabalhoCod )
      {
         GXt_char1 = A1497Anexo_Entidade;
         new prc_entidadedousuario(context ).execute(  A1495Anexo_Owner,  A1496Anexo_AreaTrabalhoCod, out  GXt_char1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1495Anexo_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1495Anexo_Owner), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1496Anexo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0)));
         A1497Anexo_Entidade = GXt_char1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1497Anexo_Entidade", A1497Anexo_Entidade);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1497Anexo_Entidade))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void gxnrGridanexos_de_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         Gx_mode = "INS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         SubsflControlProps_96134( ) ;
         while ( nGXsfl_96_idx <= nRC_GXsfl_96 )
         {
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            standaloneNotModal36134( ) ;
            standaloneModal36134( ) ;
            dynTipoDocumento_Codigo.Name = "TIPODOCUMENTO_CODIGO";
            dynTipoDocumento_Codigo.WebTags = "";
            dynTipoDocumento_Codigo.removeAllItems();
            /* Using cursor T003645 */
            pr_default.execute(30);
            while ( (pr_default.getStatus(30) != 101) )
            {
               dynTipoDocumento_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T003645_A645TipoDocumento_Codigo[0]), 6, 0)), T003645_A646TipoDocumento_Nome[0], 0);
               pr_default.readNext(30);
            }
            pr_default.close(30);
            if ( dynTipoDocumento_Codigo.ItemCount > 0 )
            {
               A645TipoDocumento_Codigo = (int)(NumberUtil.Val( dynTipoDocumento_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0))), "."));
               n645TipoDocumento_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A645TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0)));
            }
            GXCCtl = "ANEXODE_TABELA_" + sGXsfl_96_idx;
            cmbAnexoDe_Tabela.Name = GXCCtl;
            cmbAnexoDe_Tabela.WebTags = "";
            cmbAnexoDe_Tabela.addItem("1", "Contagem Resultado", 0);
            cmbAnexoDe_Tabela.addItem("2", "Evid�ncias", 0);
            cmbAnexoDe_Tabela.addItem("3", "Anexos", 0);
            if ( cmbAnexoDe_Tabela.ItemCount > 0 )
            {
               A1110AnexoDe_Tabela = (int)(NumberUtil.Val( cmbAnexoDe_Tabela.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1110AnexoDe_Tabela), 6, 0))), "."));
            }
            dynload_actions( ) ;
            SendRow36134( ) ;
            nGXsfl_96_idx = (short)(nGXsfl_96_idx+1);
            sGXsfl_96_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_96_idx), 4, 0)), 4, "0");
            SubsflControlProps_96134( ) ;
         }
         context.GX_webresponse.AddString(Gridanexos_deContainer.ToJavascriptSource());
         /* End function gxnrGridanexos_de_newrow */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtAnexo_Arquivo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Anexo_codigo( int GX_Parm1 ,
                                      int GX_Parm2 ,
                                      String GX_Parm3 ,
                                      int GX_Parm4 ,
                                      DateTime GX_Parm5 ,
                                      String GX_Parm6 ,
                                      String GX_Parm7 ,
                                      int GX_Parm8 ,
                                      String GX_Parm9 ,
                                      String GX_Parm10 ,
                                      GXCombobox dynGX_Parm11 ,
                                      int GX_Parm12 )
      {
         A1106Anexo_Codigo = GX_Parm1;
         A1498Anexo_DemandaCod = GX_Parm2;
         n1498Anexo_DemandaCod = false;
         A1101Anexo_Arquivo = GX_Parm3;
         n1101Anexo_Arquivo = false;
         A1102Anexo_UserId = GX_Parm4;
         A1103Anexo_Data = GX_Parm5;
         A1123Anexo_Descricao = GX_Parm6;
         n1123Anexo_Descricao = false;
         A1450Anexo_Link = GX_Parm7;
         n1450Anexo_Link = false;
         A1495Anexo_Owner = GX_Parm8;
         n1495Anexo_Owner = false;
         A1108Anexo_TipoArq = GX_Parm9;
         n1108Anexo_TipoArq = false;
         A1107Anexo_NomeArq = GX_Parm10;
         n1107Anexo_NomeArq = false;
         dynTipoDocumento_Codigo = dynGX_Parm11;
         A645TipoDocumento_Codigo = (int)(NumberUtil.Val( dynTipoDocumento_Codigo.CurrentValue, "."));
         n645TipoDocumento_Codigo = false;
         A1496Anexo_AreaTrabalhoCod = GX_Parm12;
         n1496Anexo_AreaTrabalhoCod = false;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         /* Using cursor T003631 */
         pr_default.execute(19, new Object[] {A1106Anexo_Codigo});
         if ( (pr_default.getStatus(19) != 101) )
         {
            A1498Anexo_DemandaCod = T003631_A1498Anexo_DemandaCod[0];
            n1498Anexo_DemandaCod = T003631_n1498Anexo_DemandaCod[0];
         }
         else
         {
            A1498Anexo_DemandaCod = 0;
            n1498Anexo_DemandaCod = false;
         }
         pr_default.close(19);
         /* Using cursor T003635 */
         pr_default.execute(20, new Object[] {n1498Anexo_DemandaCod, A1498Anexo_DemandaCod, A1106Anexo_Codigo});
         if ( (pr_default.getStatus(20) != 101) )
         {
            A1496Anexo_AreaTrabalhoCod = T003635_A1496Anexo_AreaTrabalhoCod[0];
            n1496Anexo_AreaTrabalhoCod = T003635_n1496Anexo_AreaTrabalhoCod[0];
         }
         else
         {
            A1496Anexo_AreaTrabalhoCod = 0;
            n1496Anexo_AreaTrabalhoCod = false;
         }
         pr_default.close(20);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1498Anexo_DemandaCod = 0;
            n1498Anexo_DemandaCod = false;
            A1496Anexo_AreaTrabalhoCod = 0;
            n1496Anexo_AreaTrabalhoCod = false;
            A646TipoDocumento_Nome = "";
         }
         dynTipoDocumento_Codigo.removeAllItems();
         /* Using cursor T003646 */
         pr_default.execute(31);
         while ( (pr_default.getStatus(31) != 101) )
         {
            dynTipoDocumento_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T003646_A645TipoDocumento_Codigo[0]), 6, 0)), T003646_A646TipoDocumento_Nome[0], 0);
            pr_default.readNext(31);
         }
         pr_default.close(31);
         dynTipoDocumento_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0));
         isValidOutput.Add(context.PathToRelativeUrl( A1101Anexo_Arquivo));
         isValidOutput.Add(A1101Anexo_Arquivo);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1102Anexo_UserId), 6, 0, ".", "")));
         isValidOutput.Add(context.localUtil.TToC( A1103Anexo_Data, 10, 8, 0, 3, "/", ":", " "));
         if ( dynTipoDocumento_Codigo.ItemCount > 0 )
         {
            A645TipoDocumento_Codigo = (int)(NumberUtil.Val( dynTipoDocumento_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0))), "."));
            n645TipoDocumento_Codigo = false;
         }
         dynTipoDocumento_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A645TipoDocumento_Codigo), 6, 0));
         isValidOutput.Add(dynTipoDocumento_Codigo);
         isValidOutput.Add(A1123Anexo_Descricao);
         isValidOutput.Add(A1450Anexo_Link);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1495Anexo_Owner), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A1108Anexo_TipoArq));
         isValidOutput.Add(StringUtil.RTrim( A1107Anexo_NomeArq));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1498Anexo_DemandaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1496Anexo_AreaTrabalhoCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A1497Anexo_Entidade));
         isValidOutput.Add(StringUtil.RTrim( A646TipoDocumento_Nome));
         isValidOutput.Add(edtAnexo_Arquivo_Filetype);
         isValidOutput.Add(edtAnexo_Arquivo_Filename);
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1106Anexo_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(context.PathToRelativeUrl( Z1101Anexo_Arquivo));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1102Anexo_UserId), 6, 0, ",", "")));
         isValidOutput.Add(context.localUtil.TToC( Z1103Anexo_Data, 10, 8, 0, 0, "/", ":", " "));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z645TipoDocumento_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(Z1123Anexo_Descricao);
         isValidOutput.Add(Z1450Anexo_Link);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1495Anexo_Owner), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.RTrim( Z1108Anexo_TipoArq));
         isValidOutput.Add(StringUtil.RTrim( Z1107Anexo_NomeArq));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1498Anexo_DemandaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1496Anexo_AreaTrabalhoCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( Z1497Anexo_Entidade));
         isValidOutput.Add(StringUtil.RTrim( Z646TipoDocumento_Nome));
         isValidOutput.Add(Gridanexos_deContainer);
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Tipodocumento_codigo( GXCombobox dynGX_Parm1 ,
                                              String GX_Parm2 )
      {
         dynTipoDocumento_Codigo = dynGX_Parm1;
         A645TipoDocumento_Codigo = (int)(NumberUtil.Val( dynTipoDocumento_Codigo.CurrentValue, "."));
         n645TipoDocumento_Codigo = false;
         A646TipoDocumento_Nome = GX_Parm2;
         /* Using cursor T003636 */
         pr_default.execute(21, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
         if ( (pr_default.getStatus(21) == 101) )
         {
            if ( ! ( (0==A645TipoDocumento_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Tipo de Documentos'.", "ForeignKeyNotFound", 1, "TIPODOCUMENTO_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynTipoDocumento_Codigo_Internalname;
            }
         }
         A646TipoDocumento_Nome = T003636_A646TipoDocumento_Nome[0];
         pr_default.close(21);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A646TipoDocumento_Nome = "";
         }
         isValidOutput.Add(StringUtil.RTrim( A646TipoDocumento_Nome));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Anexo_owner( int GX_Parm1 ,
                                     int GX_Parm2 ,
                                     String GX_Parm3 )
      {
         A1495Anexo_Owner = GX_Parm1;
         n1495Anexo_Owner = false;
         A1496Anexo_AreaTrabalhoCod = GX_Parm2;
         n1496Anexo_AreaTrabalhoCod = false;
         A1497Anexo_Entidade = GX_Parm3;
         GXt_char1 = A1497Anexo_Entidade;
         new prc_entidadedousuario(context ).execute(  A1495Anexo_Owner,  A1496Anexo_AreaTrabalhoCod, out  GXt_char1) ;
         A1497Anexo_Entidade = GXt_char1;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(StringUtil.RTrim( A1497Anexo_Entidade));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(5);
         pr_default.close(21);
         pr_default.close(20);
         pr_default.close(19);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z1103Anexo_Data = (DateTime)(DateTime.MinValue);
         Z1123Anexo_Descricao = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         T003613_A645TipoDocumento_Codigo = new int[1] ;
         T003613_n645TipoDocumento_Codigo = new bool[] {false} ;
         T003613_A646TipoDocumento_Nome = new String[] {""} ;
         GXCCtl = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockanexo_codigo_Jsonclick = "";
         lblTextblockanexo_arquivo_Jsonclick = "";
         A1107Anexo_NomeArq = "";
         A1108Anexo_TipoArq = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         A1101Anexo_Arquivo = "";
         lblTextblockanexo_userid_Jsonclick = "";
         lblTextblockanexo_data_Jsonclick = "";
         A1103Anexo_Data = (DateTime)(DateTime.MinValue);
         lblTextblocktipodocumento_codigo_Jsonclick = "";
         lblTextblockanexo_descricao_Jsonclick = "";
         A1123Anexo_Descricao = "";
         lblTextblockanexo_link_Jsonclick = "";
         A1450Anexo_Link = "";
         lblTextblockanexo_owner_Jsonclick = "";
         lblTextblockanexo_demandacod_Jsonclick = "";
         lblTextblockanexo_areatrabalhocod_Jsonclick = "";
         lblTextblockanexo_entidade_Jsonclick = "";
         A1497Anexo_Entidade = "";
         Gridanexos_deContainer = new GXWebGrid( context);
         Gridanexos_deColumn = new GXWebColumn();
         Gx_mode = "";
         sMode134 = "";
         lblTitlede_Jsonclick = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         A646TipoDocumento_Nome = "";
         GXCCtlgxBlob = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         s1497Anexo_Entidade = "";
         O1497Anexo_Entidade = "";
         T00369_A1498Anexo_DemandaCod = new int[1] ;
         T00369_n1498Anexo_DemandaCod = new bool[] {false} ;
         T00367_A1496Anexo_AreaTrabalhoCod = new int[1] ;
         T00367_n1496Anexo_AreaTrabalhoCod = new bool[] {false} ;
         Z1101Anexo_Arquivo = "";
         Z1450Anexo_Link = "";
         Z1108Anexo_TipoArq = "";
         Z1107Anexo_NomeArq = "";
         Z646TipoDocumento_Nome = "";
         T003615_A1106Anexo_Codigo = new int[1] ;
         T003615_A1102Anexo_UserId = new int[1] ;
         T003615_A1103Anexo_Data = new DateTime[] {DateTime.MinValue} ;
         T003615_A646TipoDocumento_Nome = new String[] {""} ;
         T003615_A1123Anexo_Descricao = new String[] {""} ;
         T003615_n1123Anexo_Descricao = new bool[] {false} ;
         T003615_A1450Anexo_Link = new String[] {""} ;
         T003615_n1450Anexo_Link = new bool[] {false} ;
         T003615_A1495Anexo_Owner = new int[1] ;
         T003615_n1495Anexo_Owner = new bool[] {false} ;
         T003615_A1108Anexo_TipoArq = new String[] {""} ;
         T003615_n1108Anexo_TipoArq = new bool[] {false} ;
         T003615_A1107Anexo_NomeArq = new String[] {""} ;
         T003615_n1107Anexo_NomeArq = new bool[] {false} ;
         T003615_A645TipoDocumento_Codigo = new int[1] ;
         T003615_n645TipoDocumento_Codigo = new bool[] {false} ;
         T003615_A1498Anexo_DemandaCod = new int[1] ;
         T003615_n1498Anexo_DemandaCod = new bool[] {false} ;
         T003615_A1101Anexo_Arquivo = new String[] {""} ;
         T003615_n1101Anexo_Arquivo = new bool[] {false} ;
         T003612_A646TipoDocumento_Nome = new String[] {""} ;
         T003617_A1498Anexo_DemandaCod = new int[1] ;
         T003617_n1498Anexo_DemandaCod = new bool[] {false} ;
         T003621_A1496Anexo_AreaTrabalhoCod = new int[1] ;
         T003621_n1496Anexo_AreaTrabalhoCod = new bool[] {false} ;
         T003622_A646TipoDocumento_Nome = new String[] {""} ;
         T003623_A1106Anexo_Codigo = new int[1] ;
         T003611_A1106Anexo_Codigo = new int[1] ;
         T003611_A1102Anexo_UserId = new int[1] ;
         T003611_A1103Anexo_Data = new DateTime[] {DateTime.MinValue} ;
         T003611_A1123Anexo_Descricao = new String[] {""} ;
         T003611_n1123Anexo_Descricao = new bool[] {false} ;
         T003611_A1450Anexo_Link = new String[] {""} ;
         T003611_n1450Anexo_Link = new bool[] {false} ;
         T003611_A1495Anexo_Owner = new int[1] ;
         T003611_n1495Anexo_Owner = new bool[] {false} ;
         T003611_A1108Anexo_TipoArq = new String[] {""} ;
         T003611_n1108Anexo_TipoArq = new bool[] {false} ;
         T003611_A1107Anexo_NomeArq = new String[] {""} ;
         T003611_n1107Anexo_NomeArq = new bool[] {false} ;
         T003611_A645TipoDocumento_Codigo = new int[1] ;
         T003611_n645TipoDocumento_Codigo = new bool[] {false} ;
         T003611_A1101Anexo_Arquivo = new String[] {""} ;
         T003611_n1101Anexo_Arquivo = new bool[] {false} ;
         sMode133 = "";
         T003624_A1106Anexo_Codigo = new int[1] ;
         T003625_A1106Anexo_Codigo = new int[1] ;
         T003610_A1106Anexo_Codigo = new int[1] ;
         T003610_A1102Anexo_UserId = new int[1] ;
         T003610_A1103Anexo_Data = new DateTime[] {DateTime.MinValue} ;
         T003610_A1123Anexo_Descricao = new String[] {""} ;
         T003610_n1123Anexo_Descricao = new bool[] {false} ;
         T003610_A1450Anexo_Link = new String[] {""} ;
         T003610_n1450Anexo_Link = new bool[] {false} ;
         T003610_A1495Anexo_Owner = new int[1] ;
         T003610_n1495Anexo_Owner = new bool[] {false} ;
         T003610_A1108Anexo_TipoArq = new String[] {""} ;
         T003610_n1108Anexo_TipoArq = new bool[] {false} ;
         T003610_A1107Anexo_NomeArq = new String[] {""} ;
         T003610_n1107Anexo_NomeArq = new bool[] {false} ;
         T003610_A645TipoDocumento_Codigo = new int[1] ;
         T003610_n645TipoDocumento_Codigo = new bool[] {false} ;
         T003610_A1101Anexo_Arquivo = new String[] {""} ;
         T003610_n1101Anexo_Arquivo = new bool[] {false} ;
         T003626_A1106Anexo_Codigo = new int[1] ;
         T003631_A1498Anexo_DemandaCod = new int[1] ;
         T003631_n1498Anexo_DemandaCod = new bool[] {false} ;
         T003635_A1496Anexo_AreaTrabalhoCod = new int[1] ;
         T003635_n1496Anexo_AreaTrabalhoCod = new bool[] {false} ;
         T003636_A646TipoDocumento_Nome = new String[] {""} ;
         T003637_A1769ContagemResultadoArtefato_Codigo = new int[1] ;
         T003638_A1106Anexo_Codigo = new int[1] ;
         T003639_A1106Anexo_Codigo = new int[1] ;
         T003639_A1109AnexoDe_Id = new int[1] ;
         T003639_A1110AnexoDe_Tabela = new int[1] ;
         T003640_A1106Anexo_Codigo = new int[1] ;
         T003640_A1109AnexoDe_Id = new int[1] ;
         T003640_A1110AnexoDe_Tabela = new int[1] ;
         T00363_A1106Anexo_Codigo = new int[1] ;
         T00363_A1109AnexoDe_Id = new int[1] ;
         T00363_A1110AnexoDe_Tabela = new int[1] ;
         T00362_A1106Anexo_Codigo = new int[1] ;
         T00362_A1109AnexoDe_Id = new int[1] ;
         T00362_A1110AnexoDe_Tabela = new int[1] ;
         T003643_A1106Anexo_Codigo = new int[1] ;
         T003643_A1109AnexoDe_Id = new int[1] ;
         T003643_A1110AnexoDe_Tabela = new int[1] ;
         Gridanexos_deRow = new GXWebRow();
         subGridanexos_de_Linesclass = "";
         ROClassString = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         i1103Anexo_Data = (DateTime)(DateTime.MinValue);
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T003644_A645TipoDocumento_Codigo = new int[1] ;
         T003644_n645TipoDocumento_Codigo = new bool[] {false} ;
         T003644_A646TipoDocumento_Nome = new String[] {""} ;
         T003645_A645TipoDocumento_Codigo = new int[1] ;
         T003645_n645TipoDocumento_Codigo = new bool[] {false} ;
         T003645_A646TipoDocumento_Nome = new String[] {""} ;
         Z1497Anexo_Entidade = "";
         T003646_A645TipoDocumento_Codigo = new int[1] ;
         T003646_n645TipoDocumento_Codigo = new bool[] {false} ;
         T003646_A646TipoDocumento_Nome = new String[] {""} ;
         isValidOutput = new GxUnknownObjectCollection();
         GXt_char1 = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.anexos__default(),
            new Object[][] {
                new Object[] {
               T00362_A1106Anexo_Codigo, T00362_A1109AnexoDe_Id, T00362_A1110AnexoDe_Tabela
               }
               , new Object[] {
               T00363_A1106Anexo_Codigo, T00363_A1109AnexoDe_Id, T00363_A1110AnexoDe_Tabela
               }
               , new Object[] {
               T00367_A1496Anexo_AreaTrabalhoCod, T00367_n1496Anexo_AreaTrabalhoCod
               }
               , new Object[] {
               T00369_A1498Anexo_DemandaCod, T00369_n1498Anexo_DemandaCod
               }
               , new Object[] {
               T003610_A1106Anexo_Codigo, T003610_A1102Anexo_UserId, T003610_A1103Anexo_Data, T003610_A1123Anexo_Descricao, T003610_n1123Anexo_Descricao, T003610_A1450Anexo_Link, T003610_n1450Anexo_Link, T003610_A1495Anexo_Owner, T003610_n1495Anexo_Owner, T003610_A1108Anexo_TipoArq,
               T003610_n1108Anexo_TipoArq, T003610_A1107Anexo_NomeArq, T003610_n1107Anexo_NomeArq, T003610_A645TipoDocumento_Codigo, T003610_n645TipoDocumento_Codigo, T003610_A1101Anexo_Arquivo, T003610_n1101Anexo_Arquivo
               }
               , new Object[] {
               T003611_A1106Anexo_Codigo, T003611_A1102Anexo_UserId, T003611_A1103Anexo_Data, T003611_A1123Anexo_Descricao, T003611_n1123Anexo_Descricao, T003611_A1450Anexo_Link, T003611_n1450Anexo_Link, T003611_A1495Anexo_Owner, T003611_n1495Anexo_Owner, T003611_A1108Anexo_TipoArq,
               T003611_n1108Anexo_TipoArq, T003611_A1107Anexo_NomeArq, T003611_n1107Anexo_NomeArq, T003611_A645TipoDocumento_Codigo, T003611_n645TipoDocumento_Codigo, T003611_A1101Anexo_Arquivo, T003611_n1101Anexo_Arquivo
               }
               , new Object[] {
               T003612_A646TipoDocumento_Nome
               }
               , new Object[] {
               T003613_A645TipoDocumento_Codigo, T003613_A646TipoDocumento_Nome
               }
               , new Object[] {
               T003615_A1106Anexo_Codigo, T003615_A1102Anexo_UserId, T003615_A1103Anexo_Data, T003615_A646TipoDocumento_Nome, T003615_A1123Anexo_Descricao, T003615_n1123Anexo_Descricao, T003615_A1450Anexo_Link, T003615_n1450Anexo_Link, T003615_A1495Anexo_Owner, T003615_n1495Anexo_Owner,
               T003615_A1108Anexo_TipoArq, T003615_n1108Anexo_TipoArq, T003615_A1107Anexo_NomeArq, T003615_n1107Anexo_NomeArq, T003615_A645TipoDocumento_Codigo, T003615_n645TipoDocumento_Codigo, T003615_A1498Anexo_DemandaCod, T003615_n1498Anexo_DemandaCod, T003615_A1101Anexo_Arquivo, T003615_n1101Anexo_Arquivo
               }
               , new Object[] {
               T003617_A1498Anexo_DemandaCod, T003617_n1498Anexo_DemandaCod
               }
               , new Object[] {
               T003621_A1496Anexo_AreaTrabalhoCod, T003621_n1496Anexo_AreaTrabalhoCod
               }
               , new Object[] {
               T003622_A646TipoDocumento_Nome
               }
               , new Object[] {
               T003623_A1106Anexo_Codigo
               }
               , new Object[] {
               T003624_A1106Anexo_Codigo
               }
               , new Object[] {
               T003625_A1106Anexo_Codigo
               }
               , new Object[] {
               T003626_A1106Anexo_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003631_A1498Anexo_DemandaCod, T003631_n1498Anexo_DemandaCod
               }
               , new Object[] {
               T003635_A1496Anexo_AreaTrabalhoCod, T003635_n1496Anexo_AreaTrabalhoCod
               }
               , new Object[] {
               T003636_A646TipoDocumento_Nome
               }
               , new Object[] {
               T003637_A1769ContagemResultadoArtefato_Codigo
               }
               , new Object[] {
               T003638_A1106Anexo_Codigo
               }
               , new Object[] {
               T003639_A1106Anexo_Codigo, T003639_A1109AnexoDe_Id, T003639_A1110AnexoDe_Tabela
               }
               , new Object[] {
               T003640_A1106Anexo_Codigo, T003640_A1109AnexoDe_Id, T003640_A1110AnexoDe_Tabela
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003643_A1106Anexo_Codigo, T003643_A1109AnexoDe_Id, T003643_A1110AnexoDe_Tabela
               }
               , new Object[] {
               T003644_A645TipoDocumento_Codigo, T003644_A646TipoDocumento_Nome
               }
               , new Object[] {
               T003645_A645TipoDocumento_Codigo, T003645_A646TipoDocumento_Nome
               }
               , new Object[] {
               T003646_A645TipoDocumento_Codigo, T003646_A646TipoDocumento_Nome
               }
            }
         );
         Z1103Anexo_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         A1103Anexo_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         i1103Anexo_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
      }

      private short nRC_GXsfl_96 ;
      private short nGXsfl_96_idx=1 ;
      private short nRcdDeleted_134 ;
      private short nRcdExists_134 ;
      private short nIsMod_134 ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short subGridanexos_de_Backcolorstyle ;
      private short subGridanexos_de_Allowselection ;
      private short subGridanexos_de_Allowhovering ;
      private short subGridanexos_de_Allowcollapsing ;
      private short subGridanexos_de_Collapsed ;
      private short nBlankRcdCount134 ;
      private short RcdFound134 ;
      private short nBlankRcdUsr134 ;
      private short Gx_BScreen ;
      private short GX_JID ;
      private short RcdFound133 ;
      private short subGridanexos_de_Backstyle ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z1106Anexo_Codigo ;
      private int Z1102Anexo_UserId ;
      private int Z1495Anexo_Owner ;
      private int Z645TipoDocumento_Codigo ;
      private int Z1109AnexoDe_Id ;
      private int Z1110AnexoDe_Tabela ;
      private int A1495Anexo_Owner ;
      private int A1496Anexo_AreaTrabalhoCod ;
      private int A1106Anexo_Codigo ;
      private int A1498Anexo_DemandaCod ;
      private int A645TipoDocumento_Codigo ;
      private int A1110AnexoDe_Tabela ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int edtAnexo_Codigo_Enabled ;
      private int edtAnexo_Arquivo_Enabled ;
      private int A1102Anexo_UserId ;
      private int edtAnexo_UserId_Enabled ;
      private int edtAnexo_Data_Enabled ;
      private int edtAnexo_Descricao_Enabled ;
      private int edtAnexo_Link_Enabled ;
      private int edtAnexo_Owner_Enabled ;
      private int edtAnexo_DemandaCod_Enabled ;
      private int edtAnexo_AreaTrabalhoCod_Enabled ;
      private int edtAnexo_Entidade_Enabled ;
      private int A1109AnexoDe_Id ;
      private int edtAnexoDe_Id_Enabled ;
      private int subGridanexos_de_Selectioncolor ;
      private int subGridanexos_de_Hoveringcolor ;
      private int fRowAdded ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int Z1498Anexo_DemandaCod ;
      private int subGridanexos_de_Backcolor ;
      private int subGridanexos_de_Allbackcolor ;
      private int defcmbAnexoDe_Tabela_Enabled ;
      private int defedtAnexoDe_Id_Enabled ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private int Z1496Anexo_AreaTrabalhoCod ;
      private long GRIDANEXOS_DE_nFirstRecordOnPage ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_96_idx="0001" ;
      private String GXKey ;
      private String GXCCtl ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtAnexo_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockanexo_codigo_Internalname ;
      private String lblTextblockanexo_codigo_Jsonclick ;
      private String edtAnexo_Codigo_Jsonclick ;
      private String lblTextblockanexo_arquivo_Internalname ;
      private String lblTextblockanexo_arquivo_Jsonclick ;
      private String edtAnexo_Arquivo_Filename ;
      private String A1107Anexo_NomeArq ;
      private String edtAnexo_Arquivo_Filetype ;
      private String edtAnexo_Arquivo_Internalname ;
      private String A1108Anexo_TipoArq ;
      private String edtAnexo_Arquivo_Contenttype ;
      private String edtAnexo_Arquivo_Parameters ;
      private String edtAnexo_Arquivo_Jsonclick ;
      private String lblTextblockanexo_userid_Internalname ;
      private String lblTextblockanexo_userid_Jsonclick ;
      private String edtAnexo_UserId_Internalname ;
      private String edtAnexo_UserId_Jsonclick ;
      private String lblTextblockanexo_data_Internalname ;
      private String lblTextblockanexo_data_Jsonclick ;
      private String edtAnexo_Data_Internalname ;
      private String edtAnexo_Data_Jsonclick ;
      private String lblTextblocktipodocumento_codigo_Internalname ;
      private String lblTextblocktipodocumento_codigo_Jsonclick ;
      private String dynTipoDocumento_Codigo_Internalname ;
      private String dynTipoDocumento_Codigo_Jsonclick ;
      private String lblTextblockanexo_descricao_Internalname ;
      private String lblTextblockanexo_descricao_Jsonclick ;
      private String edtAnexo_Descricao_Internalname ;
      private String edtAnexo_Descricao_Jsonclick ;
      private String lblTextblockanexo_link_Internalname ;
      private String lblTextblockanexo_link_Jsonclick ;
      private String edtAnexo_Link_Internalname ;
      private String edtAnexo_Link_Jsonclick ;
      private String lblTextblockanexo_owner_Internalname ;
      private String lblTextblockanexo_owner_Jsonclick ;
      private String edtAnexo_Owner_Internalname ;
      private String edtAnexo_Owner_Jsonclick ;
      private String lblTextblockanexo_demandacod_Internalname ;
      private String lblTextblockanexo_demandacod_Jsonclick ;
      private String edtAnexo_DemandaCod_Internalname ;
      private String edtAnexo_DemandaCod_Jsonclick ;
      private String lblTextblockanexo_areatrabalhocod_Internalname ;
      private String lblTextblockanexo_areatrabalhocod_Jsonclick ;
      private String edtAnexo_AreaTrabalhoCod_Internalname ;
      private String edtAnexo_AreaTrabalhoCod_Jsonclick ;
      private String lblTextblockanexo_entidade_Internalname ;
      private String lblTextblockanexo_entidade_Jsonclick ;
      private String edtAnexo_Entidade_Internalname ;
      private String A1497Anexo_Entidade ;
      private String edtAnexo_Entidade_Jsonclick ;
      private String Gx_mode ;
      private String sMode134 ;
      private String edtAnexoDe_Id_Internalname ;
      private String cmbAnexoDe_Tabela_Internalname ;
      private String tblTable3_Internalname ;
      private String lblTitlede_Internalname ;
      private String lblTitlede_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String A646TipoDocumento_Nome ;
      private String GXCCtlgxBlob ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String s1497Anexo_Entidade ;
      private String O1497Anexo_Entidade ;
      private String Z1108Anexo_TipoArq ;
      private String Z1107Anexo_NomeArq ;
      private String Z646TipoDocumento_Nome ;
      private String sMode133 ;
      private String sGXsfl_96_fel_idx="0001" ;
      private String subGridanexos_de_Class ;
      private String subGridanexos_de_Linesclass ;
      private String ROClassString ;
      private String edtAnexoDe_Id_Jsonclick ;
      private String cmbAnexoDe_Tabela_Jsonclick ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String subGridanexos_de_Internalname ;
      private String gxwrpcisep ;
      private String Z1497Anexo_Entidade ;
      private String GXt_char1 ;
      private DateTime Z1103Anexo_Data ;
      private DateTime A1103Anexo_Data ;
      private DateTime i1103Anexo_Data ;
      private bool entryPointCalled ;
      private bool n1495Anexo_Owner ;
      private bool n1496Anexo_AreaTrabalhoCod ;
      private bool n1498Anexo_DemandaCod ;
      private bool n645TipoDocumento_Codigo ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1101Anexo_Arquivo ;
      private bool n1123Anexo_Descricao ;
      private bool n1450Anexo_Link ;
      private bool n1108Anexo_TipoArq ;
      private bool n1107Anexo_NomeArq ;
      private String A1450Anexo_Link ;
      private String Z1450Anexo_Link ;
      private String Z1123Anexo_Descricao ;
      private String A1123Anexo_Descricao ;
      private String A1101Anexo_Arquivo ;
      private String Z1101Anexo_Arquivo ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private GxFile gxblobfileaux ;
      private GXWebGrid Gridanexos_deContainer ;
      private GXWebRow Gridanexos_deRow ;
      private GXWebColumn Gridanexos_deColumn ;
      private IDataStoreProvider pr_default ;
      private int[] T003613_A645TipoDocumento_Codigo ;
      private bool[] T003613_n645TipoDocumento_Codigo ;
      private String[] T003613_A646TipoDocumento_Nome ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynTipoDocumento_Codigo ;
      private GXCombobox cmbAnexoDe_Tabela ;
      private int[] T00369_A1498Anexo_DemandaCod ;
      private bool[] T00369_n1498Anexo_DemandaCod ;
      private int[] T00367_A1496Anexo_AreaTrabalhoCod ;
      private bool[] T00367_n1496Anexo_AreaTrabalhoCod ;
      private int[] T003615_A1106Anexo_Codigo ;
      private int[] T003615_A1102Anexo_UserId ;
      private DateTime[] T003615_A1103Anexo_Data ;
      private String[] T003615_A646TipoDocumento_Nome ;
      private String[] T003615_A1123Anexo_Descricao ;
      private bool[] T003615_n1123Anexo_Descricao ;
      private String[] T003615_A1450Anexo_Link ;
      private bool[] T003615_n1450Anexo_Link ;
      private int[] T003615_A1495Anexo_Owner ;
      private bool[] T003615_n1495Anexo_Owner ;
      private String[] T003615_A1108Anexo_TipoArq ;
      private bool[] T003615_n1108Anexo_TipoArq ;
      private String[] T003615_A1107Anexo_NomeArq ;
      private bool[] T003615_n1107Anexo_NomeArq ;
      private int[] T003615_A645TipoDocumento_Codigo ;
      private bool[] T003615_n645TipoDocumento_Codigo ;
      private int[] T003615_A1498Anexo_DemandaCod ;
      private bool[] T003615_n1498Anexo_DemandaCod ;
      private String[] T003615_A1101Anexo_Arquivo ;
      private bool[] T003615_n1101Anexo_Arquivo ;
      private String[] T003612_A646TipoDocumento_Nome ;
      private int[] T003617_A1498Anexo_DemandaCod ;
      private bool[] T003617_n1498Anexo_DemandaCod ;
      private int[] T003621_A1496Anexo_AreaTrabalhoCod ;
      private bool[] T003621_n1496Anexo_AreaTrabalhoCod ;
      private String[] T003622_A646TipoDocumento_Nome ;
      private int[] T003623_A1106Anexo_Codigo ;
      private int[] T003611_A1106Anexo_Codigo ;
      private int[] T003611_A1102Anexo_UserId ;
      private DateTime[] T003611_A1103Anexo_Data ;
      private String[] T003611_A1123Anexo_Descricao ;
      private bool[] T003611_n1123Anexo_Descricao ;
      private String[] T003611_A1450Anexo_Link ;
      private bool[] T003611_n1450Anexo_Link ;
      private int[] T003611_A1495Anexo_Owner ;
      private bool[] T003611_n1495Anexo_Owner ;
      private String[] T003611_A1108Anexo_TipoArq ;
      private bool[] T003611_n1108Anexo_TipoArq ;
      private String[] T003611_A1107Anexo_NomeArq ;
      private bool[] T003611_n1107Anexo_NomeArq ;
      private int[] T003611_A645TipoDocumento_Codigo ;
      private bool[] T003611_n645TipoDocumento_Codigo ;
      private String[] T003611_A1101Anexo_Arquivo ;
      private bool[] T003611_n1101Anexo_Arquivo ;
      private int[] T003624_A1106Anexo_Codigo ;
      private int[] T003625_A1106Anexo_Codigo ;
      private int[] T003610_A1106Anexo_Codigo ;
      private int[] T003610_A1102Anexo_UserId ;
      private DateTime[] T003610_A1103Anexo_Data ;
      private String[] T003610_A1123Anexo_Descricao ;
      private bool[] T003610_n1123Anexo_Descricao ;
      private String[] T003610_A1450Anexo_Link ;
      private bool[] T003610_n1450Anexo_Link ;
      private int[] T003610_A1495Anexo_Owner ;
      private bool[] T003610_n1495Anexo_Owner ;
      private String[] T003610_A1108Anexo_TipoArq ;
      private bool[] T003610_n1108Anexo_TipoArq ;
      private String[] T003610_A1107Anexo_NomeArq ;
      private bool[] T003610_n1107Anexo_NomeArq ;
      private int[] T003610_A645TipoDocumento_Codigo ;
      private bool[] T003610_n645TipoDocumento_Codigo ;
      private String[] T003610_A1101Anexo_Arquivo ;
      private bool[] T003610_n1101Anexo_Arquivo ;
      private int[] T003626_A1106Anexo_Codigo ;
      private int[] T003631_A1498Anexo_DemandaCod ;
      private bool[] T003631_n1498Anexo_DemandaCod ;
      private int[] T003635_A1496Anexo_AreaTrabalhoCod ;
      private bool[] T003635_n1496Anexo_AreaTrabalhoCod ;
      private String[] T003636_A646TipoDocumento_Nome ;
      private int[] T003637_A1769ContagemResultadoArtefato_Codigo ;
      private int[] T003638_A1106Anexo_Codigo ;
      private int[] T003639_A1106Anexo_Codigo ;
      private int[] T003639_A1109AnexoDe_Id ;
      private int[] T003639_A1110AnexoDe_Tabela ;
      private int[] T003640_A1106Anexo_Codigo ;
      private int[] T003640_A1109AnexoDe_Id ;
      private int[] T003640_A1110AnexoDe_Tabela ;
      private int[] T00363_A1106Anexo_Codigo ;
      private int[] T00363_A1109AnexoDe_Id ;
      private int[] T00363_A1110AnexoDe_Tabela ;
      private int[] T00362_A1106Anexo_Codigo ;
      private int[] T00362_A1109AnexoDe_Id ;
      private int[] T00362_A1110AnexoDe_Tabela ;
      private int[] T003643_A1106Anexo_Codigo ;
      private int[] T003643_A1109AnexoDe_Id ;
      private int[] T003643_A1110AnexoDe_Tabela ;
      private int[] T003644_A645TipoDocumento_Codigo ;
      private bool[] T003644_n645TipoDocumento_Codigo ;
      private String[] T003644_A646TipoDocumento_Nome ;
      private int[] T003645_A645TipoDocumento_Codigo ;
      private bool[] T003645_n645TipoDocumento_Codigo ;
      private String[] T003645_A646TipoDocumento_Nome ;
      private int[] T003646_A645TipoDocumento_Codigo ;
      private bool[] T003646_n645TipoDocumento_Codigo ;
      private String[] T003646_A646TipoDocumento_Nome ;
      private GXWebForm Form ;
   }

   public class anexos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new UpdateCursor(def[16])
         ,new UpdateCursor(def[17])
         ,new UpdateCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new UpdateCursor(def[26])
         ,new UpdateCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT003613 ;
          prmT003613 = new Object[] {
          } ;
          Object[] prmT003615 ;
          prmT003615 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00369 ;
          prmT00369 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00367 ;
          prmT00367 = new Object[] {
          new Object[] {"@Anexo_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003612 ;
          prmT003612 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003617 ;
          prmT003617 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003621 ;
          prmT003621 = new Object[] {
          new Object[] {"@Anexo_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003622 ;
          prmT003622 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003623 ;
          prmT003623 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003611 ;
          prmT003611 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003624 ;
          prmT003624 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003625 ;
          prmT003625 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003610 ;
          prmT003610 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003626 ;
          prmT003626 = new Object[] {
          new Object[] {"@Anexo_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Anexo_UserId",SqlDbType.Int,6,0} ,
          new Object[] {"@Anexo_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Anexo_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Anexo_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Anexo_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@Anexo_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@Anexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003627 ;
          prmT003627 = new Object[] {
          new Object[] {"@Anexo_UserId",SqlDbType.Int,6,0} ,
          new Object[] {"@Anexo_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Anexo_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Anexo_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Anexo_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@Anexo_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@Anexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003628 ;
          prmT003628 = new Object[] {
          new Object[] {"@Anexo_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003629 ;
          prmT003629 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003637 ;
          prmT003637 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003638 ;
          prmT003638 = new Object[] {
          } ;
          Object[] prmT003639 ;
          prmT003639 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AnexoDe_Id",SqlDbType.Int,6,0} ,
          new Object[] {"@AnexoDe_Tabela",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003640 ;
          prmT003640 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AnexoDe_Id",SqlDbType.Int,6,0} ,
          new Object[] {"@AnexoDe_Tabela",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00363 ;
          prmT00363 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AnexoDe_Id",SqlDbType.Int,6,0} ,
          new Object[] {"@AnexoDe_Tabela",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00362 ;
          prmT00362 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AnexoDe_Id",SqlDbType.Int,6,0} ,
          new Object[] {"@AnexoDe_Tabela",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003641 ;
          prmT003641 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AnexoDe_Id",SqlDbType.Int,6,0} ,
          new Object[] {"@AnexoDe_Tabela",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003642 ;
          prmT003642 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AnexoDe_Id",SqlDbType.Int,6,0} ,
          new Object[] {"@AnexoDe_Tabela",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003643 ;
          prmT003643 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003644 ;
          prmT003644 = new Object[] {
          } ;
          Object[] prmT003645 ;
          prmT003645 = new Object[] {
          } ;
          Object[] prmT003631 ;
          prmT003631 = new Object[] {
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003635 ;
          prmT003635 = new Object[] {
          new Object[] {"@Anexo_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Anexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003646 ;
          prmT003646 = new Object[] {
          } ;
          Object[] prmT003636 ;
          prmT003636 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00362", "SELECT [Anexo_Codigo], [AnexoDe_Id], [AnexoDe_Tabela] FROM [AnexosDe] WITH (UPDLOCK) WHERE [Anexo_Codigo] = @Anexo_Codigo AND [AnexoDe_Id] = @AnexoDe_Id AND [AnexoDe_Tabela] = @AnexoDe_Tabela ",true, GxErrorMask.GX_NOMASK, false, this,prmT00362,1,0,true,false )
             ,new CursorDef("T00363", "SELECT [Anexo_Codigo], [AnexoDe_Id], [AnexoDe_Tabela] FROM [AnexosDe] WITH (NOLOCK) WHERE [Anexo_Codigo] = @Anexo_Codigo AND [AnexoDe_Id] = @AnexoDe_Id AND [AnexoDe_Tabela] = @AnexoDe_Tabela ",true, GxErrorMask.GX_NOMASK, false, this,prmT00363,1,0,true,false )
             ,new CursorDef("T00367", "SELECT COALESCE( T1.[Anexo_AreaTrabalhoCod], 0) AS Anexo_AreaTrabalhoCod FROM (SELECT CASE  WHEN COALESCE( T2.[AnexoDe_Tabela], 0) = 1 THEN COALESCE( T3.[Contratada_AreaTrabalhoCod], 0) END AS Anexo_AreaTrabalhoCod FROM (SELECT MIN([AnexoDe_Tabela]) AS AnexoDe_Tabela, [Anexo_Codigo] FROM [AnexosDe] WITH (UPDLOCK) GROUP BY [Anexo_Codigo] ) T2,  (SELECT T5.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T4.[ContagemResultado_Codigo] FROM ([ContagemResultado] T4 WITH (NOLOCK) LEFT JOIN [Contratada] T5 WITH (NOLOCK) ON T5.[Contratada_Codigo] = T4.[ContagemResultado_ContratadaCod]) WHERE T4.[ContagemResultado_Codigo] = @Anexo_DemandaCod ) T3 WHERE T2.[Anexo_Codigo] = @Anexo_Codigo ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmT00367,1,0,true,false )
             ,new CursorDef("T00369", "SELECT COALESCE( T1.[Anexo_DemandaCod], 0) AS Anexo_DemandaCod FROM (SELECT MIN([AnexoDe_Id]) AS Anexo_DemandaCod, [Anexo_Codigo] FROM [AnexosDe] WITH (UPDLOCK) GROUP BY [Anexo_Codigo] ) T1 WHERE T1.[Anexo_Codigo] = @Anexo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00369,1,0,true,false )
             ,new CursorDef("T003610", "SELECT [Anexo_Codigo], [Anexo_UserId], [Anexo_Data], [Anexo_Descricao], [Anexo_Link], [Anexo_Owner], [Anexo_TipoArq], [Anexo_NomeArq], [TipoDocumento_Codigo], [Anexo_Arquivo] FROM [Anexos] WITH (UPDLOCK) WHERE [Anexo_Codigo] = @Anexo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003610,1,0,true,false )
             ,new CursorDef("T003611", "SELECT [Anexo_Codigo], [Anexo_UserId], [Anexo_Data], [Anexo_Descricao], [Anexo_Link], [Anexo_Owner], [Anexo_TipoArq], [Anexo_NomeArq], [TipoDocumento_Codigo], [Anexo_Arquivo] FROM [Anexos] WITH (NOLOCK) WHERE [Anexo_Codigo] = @Anexo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003611,1,0,true,false )
             ,new CursorDef("T003612", "SELECT [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003612,1,0,true,false )
             ,new CursorDef("T003613", "SELECT [TipoDocumento_Codigo], [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) ORDER BY [TipoDocumento_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT003613,0,0,true,false )
             ,new CursorDef("T003615", "SELECT TM1.[Anexo_Codigo], TM1.[Anexo_UserId], TM1.[Anexo_Data], T3.[TipoDocumento_Nome], TM1.[Anexo_Descricao], TM1.[Anexo_Link], TM1.[Anexo_Owner], TM1.[Anexo_TipoArq], TM1.[Anexo_NomeArq], TM1.[TipoDocumento_Codigo], COALESCE( T2.[Anexo_DemandaCod], 0) AS Anexo_DemandaCod, TM1.[Anexo_Arquivo] FROM (([Anexos] TM1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([AnexoDe_Id]) AS Anexo_DemandaCod, [Anexo_Codigo] FROM [AnexosDe] WITH (NOLOCK) GROUP BY [Anexo_Codigo] ) T2 ON T2.[Anexo_Codigo] = TM1.[Anexo_Codigo]) LEFT JOIN [TipoDocumento] T3 WITH (NOLOCK) ON T3.[TipoDocumento_Codigo] = TM1.[TipoDocumento_Codigo]) WHERE TM1.[Anexo_Codigo] = @Anexo_Codigo ORDER BY TM1.[Anexo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003615,100,0,true,false )
             ,new CursorDef("T003617", "SELECT COALESCE( T1.[Anexo_DemandaCod], 0) AS Anexo_DemandaCod FROM (SELECT MIN([AnexoDe_Id]) AS Anexo_DemandaCod, [Anexo_Codigo] FROM [AnexosDe] WITH (UPDLOCK) GROUP BY [Anexo_Codigo] ) T1 WHERE T1.[Anexo_Codigo] = @Anexo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003617,1,0,true,false )
             ,new CursorDef("T003621", "SELECT COALESCE( T1.[Anexo_AreaTrabalhoCod], 0) AS Anexo_AreaTrabalhoCod FROM (SELECT CASE  WHEN COALESCE( T2.[AnexoDe_Tabela], 0) = 1 THEN COALESCE( T3.[Contratada_AreaTrabalhoCod], 0) END AS Anexo_AreaTrabalhoCod FROM (SELECT MIN([AnexoDe_Tabela]) AS AnexoDe_Tabela, [Anexo_Codigo] FROM [AnexosDe] WITH (UPDLOCK) GROUP BY [Anexo_Codigo] ) T2,  (SELECT T5.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T4.[ContagemResultado_Codigo] FROM ([ContagemResultado] T4 WITH (NOLOCK) LEFT JOIN [Contratada] T5 WITH (NOLOCK) ON T5.[Contratada_Codigo] = T4.[ContagemResultado_ContratadaCod]) WHERE T4.[ContagemResultado_Codigo] = @Anexo_DemandaCod ) T3 WHERE T2.[Anexo_Codigo] = @Anexo_Codigo ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmT003621,1,0,true,false )
             ,new CursorDef("T003622", "SELECT [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003622,1,0,true,false )
             ,new CursorDef("T003623", "SELECT [Anexo_Codigo] FROM [Anexos] WITH (NOLOCK) WHERE [Anexo_Codigo] = @Anexo_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003623,1,0,true,false )
             ,new CursorDef("T003624", "SELECT TOP 1 [Anexo_Codigo] FROM [Anexos] WITH (NOLOCK) WHERE ( [Anexo_Codigo] > @Anexo_Codigo) ORDER BY [Anexo_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003624,1,0,true,true )
             ,new CursorDef("T003625", "SELECT TOP 1 [Anexo_Codigo] FROM [Anexos] WITH (NOLOCK) WHERE ( [Anexo_Codigo] < @Anexo_Codigo) ORDER BY [Anexo_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003625,1,0,true,true )
             ,new CursorDef("T003626", "INSERT INTO [Anexos]([Anexo_Arquivo], [Anexo_UserId], [Anexo_Data], [Anexo_Descricao], [Anexo_Link], [Anexo_Owner], [Anexo_TipoArq], [Anexo_NomeArq], [TipoDocumento_Codigo]) VALUES(@Anexo_Arquivo, @Anexo_UserId, @Anexo_Data, @Anexo_Descricao, @Anexo_Link, @Anexo_Owner, @Anexo_TipoArq, @Anexo_NomeArq, @TipoDocumento_Codigo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT003626)
             ,new CursorDef("T003627", "UPDATE [Anexos] SET [Anexo_UserId]=@Anexo_UserId, [Anexo_Data]=@Anexo_Data, [Anexo_Descricao]=@Anexo_Descricao, [Anexo_Link]=@Anexo_Link, [Anexo_Owner]=@Anexo_Owner, [Anexo_TipoArq]=@Anexo_TipoArq, [Anexo_NomeArq]=@Anexo_NomeArq, [TipoDocumento_Codigo]=@TipoDocumento_Codigo  WHERE [Anexo_Codigo] = @Anexo_Codigo", GxErrorMask.GX_NOMASK,prmT003627)
             ,new CursorDef("T003628", "UPDATE [Anexos] SET [Anexo_Arquivo]=@Anexo_Arquivo  WHERE [Anexo_Codigo] = @Anexo_Codigo", GxErrorMask.GX_NOMASK,prmT003628)
             ,new CursorDef("T003629", "DELETE FROM [Anexos]  WHERE [Anexo_Codigo] = @Anexo_Codigo", GxErrorMask.GX_NOMASK,prmT003629)
             ,new CursorDef("T003631", "SELECT COALESCE( T1.[Anexo_DemandaCod], 0) AS Anexo_DemandaCod FROM (SELECT MIN([AnexoDe_Id]) AS Anexo_DemandaCod, [Anexo_Codigo] FROM [AnexosDe] WITH (UPDLOCK) GROUP BY [Anexo_Codigo] ) T1 WHERE T1.[Anexo_Codigo] = @Anexo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003631,1,0,true,false )
             ,new CursorDef("T003635", "SELECT COALESCE( T1.[Anexo_AreaTrabalhoCod], 0) AS Anexo_AreaTrabalhoCod FROM (SELECT CASE  WHEN COALESCE( T2.[AnexoDe_Tabela], 0) = 1 THEN COALESCE( T3.[Contratada_AreaTrabalhoCod], 0) END AS Anexo_AreaTrabalhoCod FROM (SELECT MIN([AnexoDe_Tabela]) AS AnexoDe_Tabela, [Anexo_Codigo] FROM [AnexosDe] WITH (UPDLOCK) GROUP BY [Anexo_Codigo] ) T2,  (SELECT T5.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T4.[ContagemResultado_Codigo] FROM ([ContagemResultado] T4 WITH (NOLOCK) LEFT JOIN [Contratada] T5 WITH (NOLOCK) ON T5.[Contratada_Codigo] = T4.[ContagemResultado_ContratadaCod]) WHERE T4.[ContagemResultado_Codigo] = @Anexo_DemandaCod ) T3 WHERE T2.[Anexo_Codigo] = @Anexo_Codigo ) T1 ",true, GxErrorMask.GX_NOMASK, false, this,prmT003635,1,0,true,false )
             ,new CursorDef("T003636", "SELECT [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003636,1,0,true,false )
             ,new CursorDef("T003637", "SELECT TOP 1 [ContagemResultadoArtefato_Codigo] FROM [ContagemResultadoArtefato] WITH (NOLOCK) WHERE [ContagemResultadoArtefato_AnxCod] = @Anexo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003637,1,0,true,true )
             ,new CursorDef("T003638", "SELECT [Anexo_Codigo] FROM [Anexos] WITH (NOLOCK) ORDER BY [Anexo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003638,100,0,true,false )
             ,new CursorDef("T003639", "SELECT [Anexo_Codigo], [AnexoDe_Id], [AnexoDe_Tabela] FROM [AnexosDe] WITH (NOLOCK) WHERE [Anexo_Codigo] = @Anexo_Codigo and [AnexoDe_Id] = @AnexoDe_Id and [AnexoDe_Tabela] = @AnexoDe_Tabela ORDER BY [Anexo_Codigo], [AnexoDe_Id], [AnexoDe_Tabela] ",true, GxErrorMask.GX_NOMASK, false, this,prmT003639,11,0,true,false )
             ,new CursorDef("T003640", "SELECT [Anexo_Codigo], [AnexoDe_Id], [AnexoDe_Tabela] FROM [AnexosDe] WITH (NOLOCK) WHERE [Anexo_Codigo] = @Anexo_Codigo AND [AnexoDe_Id] = @AnexoDe_Id AND [AnexoDe_Tabela] = @AnexoDe_Tabela ",true, GxErrorMask.GX_NOMASK, false, this,prmT003640,1,0,true,false )
             ,new CursorDef("T003641", "INSERT INTO [AnexosDe]([Anexo_Codigo], [AnexoDe_Id], [AnexoDe_Tabela]) VALUES(@Anexo_Codigo, @AnexoDe_Id, @AnexoDe_Tabela)", GxErrorMask.GX_NOMASK,prmT003641)
             ,new CursorDef("T003642", "DELETE FROM [AnexosDe]  WHERE [Anexo_Codigo] = @Anexo_Codigo AND [AnexoDe_Id] = @AnexoDe_Id AND [AnexoDe_Tabela] = @AnexoDe_Tabela", GxErrorMask.GX_NOMASK,prmT003642)
             ,new CursorDef("T003643", "SELECT [Anexo_Codigo], [AnexoDe_Id], [AnexoDe_Tabela] FROM [AnexosDe] WITH (NOLOCK) WHERE [Anexo_Codigo] = @Anexo_Codigo ORDER BY [Anexo_Codigo], [AnexoDe_Id], [AnexoDe_Tabela] ",true, GxErrorMask.GX_NOMASK, false, this,prmT003643,11,0,true,false )
             ,new CursorDef("T003644", "SELECT [TipoDocumento_Codigo], [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) ORDER BY [TipoDocumento_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT003644,0,0,true,false )
             ,new CursorDef("T003645", "SELECT [TipoDocumento_Codigo], [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) ORDER BY [TipoDocumento_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT003645,0,0,true,false )
             ,new CursorDef("T003646", "SELECT [TipoDocumento_Codigo], [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) ORDER BY [TipoDocumento_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT003646,0,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getBLOBFile(10, rslt.getString(7, 10), rslt.getString(8, 50)) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getBLOBFile(10, rslt.getString(7, 10), rslt.getString(8, 50)) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 10) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((String[]) buf[12])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((int[]) buf[16])[0] = rslt.getInt(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((String[]) buf[18])[0] = rslt.getBLOBFile(12, rslt.getString(8, 10), rslt.getString(9, 50)) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 21 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 25 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 28 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 29 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 31 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameterDatetime(3, (DateTime)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[15]);
                }
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[13]);
                }
                stmt.SetParameter(9, (int)parms[14]);
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 21 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 24 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 25 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 26 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 27 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 28 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
