/*
               File: ExportExtraWWContagemResultadoWWAceiteFaturamento
        Description: Export Extra WWContagem Resultado WWAceite Faturamento
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/17/2020 1:56:34.0
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aexportextrawwcontagemresultadowwaceitefaturamento : GXProcedure
   {
      public aexportextrawwcontagemresultadowwaceitefaturamento( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public aexportextrawwcontagemresultadowwaceitefaturamento( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_AreaTrabalhoCod ,
                           int aP1_Contratada_Codigo ,
                           short aP2_ContagemResultado_StatusCnt ,
                           String aP3_ContagemResultado_StatusDmn ,
                           String aP4_TFContagemResultado_LoteAceite ,
                           String aP5_TFContagemResultado_LoteAceite_Sel ,
                           String aP6_TFContagemResultado_OsFsOsFm ,
                           String aP7_TFContagemResultado_OsFsOsFm_Sel ,
                           String aP8_TFContagemResultado_Descricao ,
                           String aP9_TFContagemResultado_Descricao_Sel ,
                           DateTime aP10_TFContagemResultado_DataAceite ,
                           DateTime aP11_TFContagemResultado_DataAceite_To ,
                           DateTime aP12_TFContagemResultado_DataUltCnt ,
                           DateTime aP13_TFContagemResultado_DataUltCnt_To ,
                           String aP14_TFContagemResultado_ContratadaOrigemSigla ,
                           String aP15_TFContagemResultado_ContratadaOrigemSigla_Sel ,
                           String aP16_TFContagemrResultado_SistemaSigla ,
                           String aP17_TFContagemrResultado_SistemaSigla_Sel ,
                           String aP18_TFContagemResultado_StatusDmn_SelsJson ,
                           short aP19_TFContagemResultado_Baseline_Sel ,
                           String aP20_TFContagemResultado_ServicoSigla ,
                           String aP21_TFContagemResultado_ServicoSigla_Sel ,
                           decimal aP22_TFContagemResultado_PFFinal ,
                           decimal aP23_TFContagemResultado_PFFinal_To ,
                           decimal aP24_TFContagemResultado_ValorPF ,
                           decimal aP25_TFContagemResultado_ValorPF_To ,
                           short aP26_OrderedBy ,
                           String aP27_GridStateXML ,
                           out String aP28_Filename ,
                           out String aP29_ErrorMessage )
      {
         this.AV18Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV72Contratada_Codigo = aP1_Contratada_Codigo;
         this.AV19ContagemResultado_StatusCnt = aP2_ContagemResultado_StatusCnt;
         this.AV20ContagemResultado_StatusDmn = aP3_ContagemResultado_StatusDmn;
         this.AV81TFContagemResultado_LoteAceite = aP4_TFContagemResultado_LoteAceite;
         this.AV82TFContagemResultado_LoteAceite_Sel = aP5_TFContagemResultado_LoteAceite_Sel;
         this.AV83TFContagemResultado_OsFsOsFm = aP6_TFContagemResultado_OsFsOsFm;
         this.AV84TFContagemResultado_OsFsOsFm_Sel = aP7_TFContagemResultado_OsFsOsFm_Sel;
         this.AV85TFContagemResultado_Descricao = aP8_TFContagemResultado_Descricao;
         this.AV86TFContagemResultado_Descricao_Sel = aP9_TFContagemResultado_Descricao_Sel;
         this.AV87TFContagemResultado_DataAceite = aP10_TFContagemResultado_DataAceite;
         this.AV88TFContagemResultado_DataAceite_To = aP11_TFContagemResultado_DataAceite_To;
         this.AV89TFContagemResultado_DataUltCnt = aP12_TFContagemResultado_DataUltCnt;
         this.AV90TFContagemResultado_DataUltCnt_To = aP13_TFContagemResultado_DataUltCnt_To;
         this.AV91TFContagemResultado_ContratadaOrigemSigla = aP14_TFContagemResultado_ContratadaOrigemSigla;
         this.AV92TFContagemResultado_ContratadaOrigemSigla_Sel = aP15_TFContagemResultado_ContratadaOrigemSigla_Sel;
         this.AV93TFContagemrResultado_SistemaSigla = aP16_TFContagemrResultado_SistemaSigla;
         this.AV94TFContagemrResultado_SistemaSigla_Sel = aP17_TFContagemrResultado_SistemaSigla_Sel;
         this.AV95TFContagemResultado_StatusDmn_SelsJson = aP18_TFContagemResultado_StatusDmn_SelsJson;
         this.AV98TFContagemResultado_Baseline_Sel = aP19_TFContagemResultado_Baseline_Sel;
         this.AV99TFContagemResultado_ServicoSigla = aP20_TFContagemResultado_ServicoSigla;
         this.AV100TFContagemResultado_ServicoSigla_Sel = aP21_TFContagemResultado_ServicoSigla_Sel;
         this.AV109TFContagemResultado_PFFinal = aP22_TFContagemResultado_PFFinal;
         this.AV110TFContagemResultado_PFFinal_To = aP23_TFContagemResultado_PFFinal_To;
         this.AV111TFContagemResultado_ValorPF = aP24_TFContagemResultado_ValorPF;
         this.AV112TFContagemResultado_ValorPF_To = aP25_TFContagemResultado_ValorPF_To;
         this.AV16OrderedBy = aP26_OrderedBy;
         this.AV45GridStateXML = aP27_GridStateXML;
         this.AV11Filename = "" ;
         this.AV12ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP28_Filename=this.AV11Filename;
         aP29_ErrorMessage=this.AV12ErrorMessage;
      }

      public String executeUdp( int aP0_Contratada_AreaTrabalhoCod ,
                                int aP1_Contratada_Codigo ,
                                short aP2_ContagemResultado_StatusCnt ,
                                String aP3_ContagemResultado_StatusDmn ,
                                String aP4_TFContagemResultado_LoteAceite ,
                                String aP5_TFContagemResultado_LoteAceite_Sel ,
                                String aP6_TFContagemResultado_OsFsOsFm ,
                                String aP7_TFContagemResultado_OsFsOsFm_Sel ,
                                String aP8_TFContagemResultado_Descricao ,
                                String aP9_TFContagemResultado_Descricao_Sel ,
                                DateTime aP10_TFContagemResultado_DataAceite ,
                                DateTime aP11_TFContagemResultado_DataAceite_To ,
                                DateTime aP12_TFContagemResultado_DataUltCnt ,
                                DateTime aP13_TFContagemResultado_DataUltCnt_To ,
                                String aP14_TFContagemResultado_ContratadaOrigemSigla ,
                                String aP15_TFContagemResultado_ContratadaOrigemSigla_Sel ,
                                String aP16_TFContagemrResultado_SistemaSigla ,
                                String aP17_TFContagemrResultado_SistemaSigla_Sel ,
                                String aP18_TFContagemResultado_StatusDmn_SelsJson ,
                                short aP19_TFContagemResultado_Baseline_Sel ,
                                String aP20_TFContagemResultado_ServicoSigla ,
                                String aP21_TFContagemResultado_ServicoSigla_Sel ,
                                decimal aP22_TFContagemResultado_PFFinal ,
                                decimal aP23_TFContagemResultado_PFFinal_To ,
                                decimal aP24_TFContagemResultado_ValorPF ,
                                decimal aP25_TFContagemResultado_ValorPF_To ,
                                short aP26_OrderedBy ,
                                String aP27_GridStateXML ,
                                out String aP28_Filename )
      {
         this.AV18Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV72Contratada_Codigo = aP1_Contratada_Codigo;
         this.AV19ContagemResultado_StatusCnt = aP2_ContagemResultado_StatusCnt;
         this.AV20ContagemResultado_StatusDmn = aP3_ContagemResultado_StatusDmn;
         this.AV81TFContagemResultado_LoteAceite = aP4_TFContagemResultado_LoteAceite;
         this.AV82TFContagemResultado_LoteAceite_Sel = aP5_TFContagemResultado_LoteAceite_Sel;
         this.AV83TFContagemResultado_OsFsOsFm = aP6_TFContagemResultado_OsFsOsFm;
         this.AV84TFContagemResultado_OsFsOsFm_Sel = aP7_TFContagemResultado_OsFsOsFm_Sel;
         this.AV85TFContagemResultado_Descricao = aP8_TFContagemResultado_Descricao;
         this.AV86TFContagemResultado_Descricao_Sel = aP9_TFContagemResultado_Descricao_Sel;
         this.AV87TFContagemResultado_DataAceite = aP10_TFContagemResultado_DataAceite;
         this.AV88TFContagemResultado_DataAceite_To = aP11_TFContagemResultado_DataAceite_To;
         this.AV89TFContagemResultado_DataUltCnt = aP12_TFContagemResultado_DataUltCnt;
         this.AV90TFContagemResultado_DataUltCnt_To = aP13_TFContagemResultado_DataUltCnt_To;
         this.AV91TFContagemResultado_ContratadaOrigemSigla = aP14_TFContagemResultado_ContratadaOrigemSigla;
         this.AV92TFContagemResultado_ContratadaOrigemSigla_Sel = aP15_TFContagemResultado_ContratadaOrigemSigla_Sel;
         this.AV93TFContagemrResultado_SistemaSigla = aP16_TFContagemrResultado_SistemaSigla;
         this.AV94TFContagemrResultado_SistemaSigla_Sel = aP17_TFContagemrResultado_SistemaSigla_Sel;
         this.AV95TFContagemResultado_StatusDmn_SelsJson = aP18_TFContagemResultado_StatusDmn_SelsJson;
         this.AV98TFContagemResultado_Baseline_Sel = aP19_TFContagemResultado_Baseline_Sel;
         this.AV99TFContagemResultado_ServicoSigla = aP20_TFContagemResultado_ServicoSigla;
         this.AV100TFContagemResultado_ServicoSigla_Sel = aP21_TFContagemResultado_ServicoSigla_Sel;
         this.AV109TFContagemResultado_PFFinal = aP22_TFContagemResultado_PFFinal;
         this.AV110TFContagemResultado_PFFinal_To = aP23_TFContagemResultado_PFFinal_To;
         this.AV111TFContagemResultado_ValorPF = aP24_TFContagemResultado_ValorPF;
         this.AV112TFContagemResultado_ValorPF_To = aP25_TFContagemResultado_ValorPF_To;
         this.AV16OrderedBy = aP26_OrderedBy;
         this.AV45GridStateXML = aP27_GridStateXML;
         this.AV11Filename = "" ;
         this.AV12ErrorMessage = "" ;
         initialize();
         executePrivate();
         aP28_Filename=this.AV11Filename;
         aP29_ErrorMessage=this.AV12ErrorMessage;
         return AV12ErrorMessage ;
      }

      public void executeSubmit( int aP0_Contratada_AreaTrabalhoCod ,
                                 int aP1_Contratada_Codigo ,
                                 short aP2_ContagemResultado_StatusCnt ,
                                 String aP3_ContagemResultado_StatusDmn ,
                                 String aP4_TFContagemResultado_LoteAceite ,
                                 String aP5_TFContagemResultado_LoteAceite_Sel ,
                                 String aP6_TFContagemResultado_OsFsOsFm ,
                                 String aP7_TFContagemResultado_OsFsOsFm_Sel ,
                                 String aP8_TFContagemResultado_Descricao ,
                                 String aP9_TFContagemResultado_Descricao_Sel ,
                                 DateTime aP10_TFContagemResultado_DataAceite ,
                                 DateTime aP11_TFContagemResultado_DataAceite_To ,
                                 DateTime aP12_TFContagemResultado_DataUltCnt ,
                                 DateTime aP13_TFContagemResultado_DataUltCnt_To ,
                                 String aP14_TFContagemResultado_ContratadaOrigemSigla ,
                                 String aP15_TFContagemResultado_ContratadaOrigemSigla_Sel ,
                                 String aP16_TFContagemrResultado_SistemaSigla ,
                                 String aP17_TFContagemrResultado_SistemaSigla_Sel ,
                                 String aP18_TFContagemResultado_StatusDmn_SelsJson ,
                                 short aP19_TFContagemResultado_Baseline_Sel ,
                                 String aP20_TFContagemResultado_ServicoSigla ,
                                 String aP21_TFContagemResultado_ServicoSigla_Sel ,
                                 decimal aP22_TFContagemResultado_PFFinal ,
                                 decimal aP23_TFContagemResultado_PFFinal_To ,
                                 decimal aP24_TFContagemResultado_ValorPF ,
                                 decimal aP25_TFContagemResultado_ValorPF_To ,
                                 short aP26_OrderedBy ,
                                 String aP27_GridStateXML ,
                                 out String aP28_Filename ,
                                 out String aP29_ErrorMessage )
      {
         aexportextrawwcontagemresultadowwaceitefaturamento objaexportextrawwcontagemresultadowwaceitefaturamento;
         objaexportextrawwcontagemresultadowwaceitefaturamento = new aexportextrawwcontagemresultadowwaceitefaturamento();
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV18Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV72Contratada_Codigo = aP1_Contratada_Codigo;
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV19ContagemResultado_StatusCnt = aP2_ContagemResultado_StatusCnt;
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV20ContagemResultado_StatusDmn = aP3_ContagemResultado_StatusDmn;
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV81TFContagemResultado_LoteAceite = aP4_TFContagemResultado_LoteAceite;
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV82TFContagemResultado_LoteAceite_Sel = aP5_TFContagemResultado_LoteAceite_Sel;
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV83TFContagemResultado_OsFsOsFm = aP6_TFContagemResultado_OsFsOsFm;
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV84TFContagemResultado_OsFsOsFm_Sel = aP7_TFContagemResultado_OsFsOsFm_Sel;
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV85TFContagemResultado_Descricao = aP8_TFContagemResultado_Descricao;
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV86TFContagemResultado_Descricao_Sel = aP9_TFContagemResultado_Descricao_Sel;
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV87TFContagemResultado_DataAceite = aP10_TFContagemResultado_DataAceite;
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV88TFContagemResultado_DataAceite_To = aP11_TFContagemResultado_DataAceite_To;
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV89TFContagemResultado_DataUltCnt = aP12_TFContagemResultado_DataUltCnt;
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV90TFContagemResultado_DataUltCnt_To = aP13_TFContagemResultado_DataUltCnt_To;
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV91TFContagemResultado_ContratadaOrigemSigla = aP14_TFContagemResultado_ContratadaOrigemSigla;
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV92TFContagemResultado_ContratadaOrigemSigla_Sel = aP15_TFContagemResultado_ContratadaOrigemSigla_Sel;
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV93TFContagemrResultado_SistemaSigla = aP16_TFContagemrResultado_SistemaSigla;
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV94TFContagemrResultado_SistemaSigla_Sel = aP17_TFContagemrResultado_SistemaSigla_Sel;
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV95TFContagemResultado_StatusDmn_SelsJson = aP18_TFContagemResultado_StatusDmn_SelsJson;
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV98TFContagemResultado_Baseline_Sel = aP19_TFContagemResultado_Baseline_Sel;
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV99TFContagemResultado_ServicoSigla = aP20_TFContagemResultado_ServicoSigla;
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV100TFContagemResultado_ServicoSigla_Sel = aP21_TFContagemResultado_ServicoSigla_Sel;
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV109TFContagemResultado_PFFinal = aP22_TFContagemResultado_PFFinal;
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV110TFContagemResultado_PFFinal_To = aP23_TFContagemResultado_PFFinal_To;
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV111TFContagemResultado_ValorPF = aP24_TFContagemResultado_ValorPF;
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV112TFContagemResultado_ValorPF_To = aP25_TFContagemResultado_ValorPF_To;
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV16OrderedBy = aP26_OrderedBy;
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV45GridStateXML = aP27_GridStateXML;
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV11Filename = "" ;
         objaexportextrawwcontagemresultadowwaceitefaturamento.AV12ErrorMessage = "" ;
         objaexportextrawwcontagemresultadowwaceitefaturamento.context.SetSubmitInitialConfig(context);
         objaexportextrawwcontagemresultadowwaceitefaturamento.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaexportextrawwcontagemresultadowwaceitefaturamento);
         aP28_Filename=this.AV11Filename;
         aP29_ErrorMessage=this.AV12ErrorMessage;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aexportextrawwcontagemresultadowwaceitefaturamento)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'OPENDOCUMENT' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV13CellRow = 1;
         AV14FirstColumn = 1;
         /* Execute user subroutine: 'WRITEMAINTITLE' */
         S131 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'WRITEFILTERS' */
         S141 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'WRITECOLUMNTITLES' */
         S151 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'WRITEDATA' */
         S161 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         /* Execute user subroutine: 'CLOSEDOCUMENT' */
         S191 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'OPENDOCUMENT' Routine */
         if ( false )
         {
            AV15Random = (int)(NumberUtil.Random( )*10000);
            AV11Filename = "ExportExtraWWContagemResultadoWWAceiteFaturamento-" + StringUtil.Trim( StringUtil.Str( (decimal)(AV15Random), 8, 0)) + ".xlsx";
            AV10ExcelDocument.Open(AV11Filename);
            /* Execute user subroutine: 'CHECKSTATUS' */
            S121 ();
            if (returnInSub) return;
            AV10ExcelDocument.Clear();
         }
         AV15Random = (int)(NumberUtil.Random( )*10000);
         AV11Filename = "PublicTempStorage\\ExportExtraWWContagemResultadoWWAceiteFaturamento-" + StringUtil.Trim( StringUtil.Str( (decimal)(AV15Random), 8, 0)) + ".xlsx";
         AV10ExcelDocument.Open(AV11Filename);
         /* Execute user subroutine: 'CHECKSTATUS' */
         S121 ();
         if (returnInSub) return;
         AV10ExcelDocument.Clear();
      }

      protected void S131( )
      {
         /* 'WRITEMAINTITLE' Routine */
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Resultado das Contagens";
         AV13CellRow = (int)(AV13CellRow+2);
      }

      protected void S141( )
      {
         /* 'WRITEFILTERS' Routine */
         if ( ! ( (0==AV18Contratada_AreaTrabalhoCod) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = "�rea de Trabalho";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV18Contratada_AreaTrabalhoCod;
         }
         if ( ! ( (0==AV72Contratada_Codigo) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = "Contratada";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV72Contratada_Codigo;
         }
         if ( ! ( (0==AV19ContagemResultado_StatusCnt) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = "Status";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            if ( ! (0==AV19ContagemResultado_StatusCnt) )
            {
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatuscontagem.getDescription(context,AV19ContagemResultado_StatusCnt);
            }
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV20ContagemResultado_StatusDmn)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = "Status da Dmn";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20ContagemResultado_StatusDmn)) )
            {
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = gxdomainstatusdemanda.getDescription(context,AV20ContagemResultado_StatusDmn);
            }
         }
         AV46GridState.gxTpr_Dynamicfilters.FromXml(AV45GridStateXML, "");
         if ( AV46GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV47GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV46GridState.gxTpr_Dynamicfilters.Item(1));
            AV22DynamicFiltersSelector1 = AV47GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV22DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
            {
               AV66ContagemResultado_DataUltCnt1 = context.localUtil.CToD( AV47GridStateDynamicFilter.gxTpr_Value, 2);
               AV67ContagemResultado_DataUltCnt_To1 = context.localUtil.CToD( AV47GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV66ContagemResultado_DataUltCnt1) || ! (DateTime.MinValue==AV67ContagemResultado_DataUltCnt_To1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Contagem";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV66ContagemResultado_DataUltCnt1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV67ContagemResultado_DataUltCnt_To1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
               }
            }
            else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
            {
               AV48DynamicFiltersOperator1 = AV47GridStateDynamicFilter.gxTpr_Operator;
               AV25ContagemResultado_OsFsOsFm1 = AV47GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContagemResultado_OsFsOsFm1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  if ( AV48DynamicFiltersOperator1 == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Igual)";
                  }
                  else if ( AV48DynamicFiltersOperator1 == 1 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Come�a com)";
                  }
                  else if ( AV48DynamicFiltersOperator1 == 2 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Cont�m)";
                  }
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV25ContagemResultado_OsFsOsFm1;
               }
            }
            else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 )
            {
               AV114ContagemResultado_DataDmn1 = context.localUtil.CToD( AV47GridStateDynamicFilter.gxTpr_Value, 2);
               AV115ContagemResultado_DataDmn_To1 = context.localUtil.CToD( AV47GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV114ContagemResultado_DataDmn1) || ! (DateTime.MinValue==AV115ContagemResultado_DataDmn_To1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Demanda";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV114ContagemResultado_DataDmn1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV115ContagemResultado_DataDmn_To1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
               }
            }
            else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAACEITE") == 0 )
            {
               AV116ContagemResultado_DataAceite1 = context.localUtil.CToD( AV47GridStateDynamicFilter.gxTpr_Value, 2);
               AV117ContagemResultado_DataAceite_To1 = context.localUtil.CToD( AV47GridStateDynamicFilter.gxTpr_Valueto, 2);
               if ( ! (DateTime.MinValue==AV116ContagemResultado_DataAceite1) || ! (DateTime.MinValue==AV117ContagemResultado_DataAceite_To1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data do Aceite";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV116ContagemResultado_DataAceite1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "to";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                  GXt_dtime1 = DateTimeUtil.ResetTime( AV117ContagemResultado_DataAceite_To1 ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
               }
            }
            else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
            {
               AV26ContagemResultado_SistemaCod1 = (int)(NumberUtil.Val( AV47GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV26ContagemResultado_SistemaCod1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV26ContagemResultado_SistemaCod1;
               }
            }
            else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
            {
               AV27ContagemResultado_ContratadaCod1 = (int)(NumberUtil.Val( AV47GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV27ContagemResultado_ContratadaCod1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Prestadora";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV27ContagemResultado_ContratadaCod1;
               }
            }
            else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 )
            {
               AV118ContagemResultado_ContratadaOrigemCod1 = (int)(NumberUtil.Val( AV47GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV118ContagemResultado_ContratadaOrigemCod1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Origem";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV118ContagemResultado_ContratadaOrigemCod1;
               }
            }
            else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector1, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
            {
               AV28ContagemResultado_NaoCnfDmnCod1 = (int)(NumberUtil.Val( AV47GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV28ContagemResultado_NaoCnfDmnCod1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "N�o Conformidade";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV28ContagemResultado_NaoCnfDmnCod1;
               }
            }
            else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector1, "CONTAGEMRESULTADO_LOTEACEITE") == 0 )
            {
               AV49ContagemResultado_LoteAceite1 = AV47GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ContagemResultado_LoteAceite1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Lote";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV49ContagemResultado_LoteAceite1;
               }
            }
            else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDAFM_ORDER") == 0 )
            {
               AV50ContagemResultado_DemandaFM_ORDER1 = (long)(NumberUtil.Val( AV47GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV50ContagemResultado_DemandaFM_ORDER1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS FM";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV50ContagemResultado_DemandaFM_ORDER1;
               }
            }
            else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector1, "CONTAGEMRESULTADO_BASELINE") == 0 )
            {
               AV57ContagemResultado_Baseline1 = AV47GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57ContagemResultado_Baseline1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Baseline";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
                  if ( StringUtil.StrCmp(StringUtil.Trim( AV57ContagemResultado_Baseline1), "S") == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Sim";
                  }
                  else if ( StringUtil.StrCmp(StringUtil.Trim( AV57ContagemResultado_Baseline1), "N") == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "N�o";
                  }
               }
            }
            else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector1, "CONTAGEMRESULTADO_SERVICO") == 0 )
            {
               AV63ContagemResultado_Servico1 = (int)(NumberUtil.Val( AV47GridStateDynamicFilter.gxTpr_Value, "."));
               if ( ! (0==AV63ContagemResultado_Servico1) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Servi�o";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV63ContagemResultado_Servico1;
               }
            }
            else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
            {
               AV73ContagemResultado_Descricao1 = AV47GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73ContagemResultado_Descricao1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Titulo";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV73ContagemResultado_Descricao1;
               }
            }
            else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
            {
               AV119ContagemResultado_Agrupador1 = AV47GridStateDynamicFilter.gxTpr_Value;
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV119ContagemResultado_Agrupador1)) )
               {
                  AV13CellRow = (int)(AV13CellRow+1);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Agrupador";
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV119ContagemResultado_Agrupador1;
               }
            }
            if ( AV46GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV29DynamicFiltersEnabled2 = true;
               AV47GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV46GridState.gxTpr_Dynamicfilters.Item(2));
               AV30DynamicFiltersSelector2 = AV47GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV30DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
               {
                  AV68ContagemResultado_DataUltCnt2 = context.localUtil.CToD( AV47GridStateDynamicFilter.gxTpr_Value, 2);
                  AV69ContagemResultado_DataUltCnt_To2 = context.localUtil.CToD( AV47GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV68ContagemResultado_DataUltCnt2) || ! (DateTime.MinValue==AV69ContagemResultado_DataUltCnt_To2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Contagem";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV68ContagemResultado_DataUltCnt2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV69ContagemResultado_DataUltCnt_To2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                  }
               }
               else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
               {
                  AV51DynamicFiltersOperator2 = AV47GridStateDynamicFilter.gxTpr_Operator;
                  AV33ContagemResultado_OsFsOsFm2 = AV47GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33ContagemResultado_OsFsOsFm2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     if ( AV51DynamicFiltersOperator2 == 0 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Igual)";
                     }
                     else if ( AV51DynamicFiltersOperator2 == 1 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Come�a com)";
                     }
                     else if ( AV51DynamicFiltersOperator2 == 2 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Cont�m)";
                     }
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV33ContagemResultado_OsFsOsFm2;
                  }
               }
               else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 )
               {
                  AV120ContagemResultado_DataDmn2 = context.localUtil.CToD( AV47GridStateDynamicFilter.gxTpr_Value, 2);
                  AV121ContagemResultado_DataDmn_To2 = context.localUtil.CToD( AV47GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV120ContagemResultado_DataDmn2) || ! (DateTime.MinValue==AV121ContagemResultado_DataDmn_To2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Demanda";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV120ContagemResultado_DataDmn2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV121ContagemResultado_DataDmn_To2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                  }
               }
               else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAACEITE") == 0 )
               {
                  AV122ContagemResultado_DataAceite2 = context.localUtil.CToD( AV47GridStateDynamicFilter.gxTpr_Value, 2);
                  AV123ContagemResultado_DataAceite_To2 = context.localUtil.CToD( AV47GridStateDynamicFilter.gxTpr_Valueto, 2);
                  if ( ! (DateTime.MinValue==AV122ContagemResultado_DataAceite2) || ! (DateTime.MinValue==AV123ContagemResultado_DataAceite_To2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data do Aceite";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV122ContagemResultado_DataAceite2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "to";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                     GXt_dtime1 = DateTimeUtil.ResetTime( AV123ContagemResultado_DataAceite_To2 ) ;
                     AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                  }
               }
               else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
               {
                  AV34ContagemResultado_SistemaCod2 = (int)(NumberUtil.Val( AV47GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV34ContagemResultado_SistemaCod2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV34ContagemResultado_SistemaCod2;
                  }
               }
               else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
               {
                  AV35ContagemResultado_ContratadaCod2 = (int)(NumberUtil.Val( AV47GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV35ContagemResultado_ContratadaCod2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Prestadora";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV35ContagemResultado_ContratadaCod2;
                  }
               }
               else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 )
               {
                  AV124ContagemResultado_ContratadaOrigemCod2 = (int)(NumberUtil.Val( AV47GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV124ContagemResultado_ContratadaOrigemCod2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Origem";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV124ContagemResultado_ContratadaOrigemCod2;
                  }
               }
               else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector2, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
               {
                  AV36ContagemResultado_NaoCnfDmnCod2 = (int)(NumberUtil.Val( AV47GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV36ContagemResultado_NaoCnfDmnCod2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "N�o Conformidade";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV36ContagemResultado_NaoCnfDmnCod2;
                  }
               }
               else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector2, "CONTAGEMRESULTADO_LOTEACEITE") == 0 )
               {
                  AV52ContagemResultado_LoteAceite2 = AV47GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52ContagemResultado_LoteAceite2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Lote";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV52ContagemResultado_LoteAceite2;
                  }
               }
               else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDAFM_ORDER") == 0 )
               {
                  AV53ContagemResultado_DemandaFM_ORDER2 = (long)(NumberUtil.Val( AV47GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV53ContagemResultado_DemandaFM_ORDER2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS FM";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV53ContagemResultado_DemandaFM_ORDER2;
                  }
               }
               else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector2, "CONTAGEMRESULTADO_BASELINE") == 0 )
               {
                  AV58ContagemResultado_Baseline2 = AV47GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58ContagemResultado_Baseline2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Baseline";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
                     if ( StringUtil.StrCmp(StringUtil.Trim( AV58ContagemResultado_Baseline2), "S") == 0 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Sim";
                     }
                     else if ( StringUtil.StrCmp(StringUtil.Trim( AV58ContagemResultado_Baseline2), "N") == 0 )
                     {
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "N�o";
                     }
                  }
               }
               else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector2, "CONTAGEMRESULTADO_SERVICO") == 0 )
               {
                  AV64ContagemResultado_Servico2 = (int)(NumberUtil.Val( AV47GridStateDynamicFilter.gxTpr_Value, "."));
                  if ( ! (0==AV64ContagemResultado_Servico2) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Servi�o";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV64ContagemResultado_Servico2;
                  }
               }
               else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
               {
                  AV74ContagemResultado_Descricao2 = AV47GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74ContagemResultado_Descricao2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Titulo";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV74ContagemResultado_Descricao2;
                  }
               }
               else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
               {
                  AV125ContagemResultado_Agrupador2 = AV47GridStateDynamicFilter.gxTpr_Value;
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV125ContagemResultado_Agrupador2)) )
                  {
                     AV13CellRow = (int)(AV13CellRow+1);
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Agrupador";
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV125ContagemResultado_Agrupador2;
                  }
               }
               if ( AV46GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV37DynamicFiltersEnabled3 = true;
                  AV47GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV46GridState.gxTpr_Dynamicfilters.Item(3));
                  AV38DynamicFiltersSelector3 = AV47GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV38DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAULTCNT") == 0 )
                  {
                     AV70ContagemResultado_DataUltCnt3 = context.localUtil.CToD( AV47GridStateDynamicFilter.gxTpr_Value, 2);
                     AV71ContagemResultado_DataUltCnt_To3 = context.localUtil.CToD( AV47GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV70ContagemResultado_DataUltCnt3) || ! (DateTime.MinValue==AV71ContagemResultado_DataUltCnt_To3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Contagem";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV70ContagemResultado_DataUltCnt3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV71ContagemResultado_DataUltCnt_To3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV38DynamicFiltersSelector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                  {
                     AV54DynamicFiltersOperator3 = AV47GridStateDynamicFilter.gxTpr_Operator;
                     AV41ContagemResultado_OsFsOsFm3 = AV47GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41ContagemResultado_OsFsOsFm3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        if ( AV54DynamicFiltersOperator3 == 0 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Igual)";
                        }
                        else if ( AV54DynamicFiltersOperator3 == 1 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Come�a com)";
                        }
                        else if ( AV54DynamicFiltersOperator3 == 2 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS / OS Refer�ncia (Cont�m)";
                        }
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV41ContagemResultado_OsFsOsFm3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV38DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 )
                  {
                     AV126ContagemResultado_DataDmn3 = context.localUtil.CToD( AV47GridStateDynamicFilter.gxTpr_Value, 2);
                     AV127ContagemResultado_DataDmn_To3 = context.localUtil.CToD( AV47GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV126ContagemResultado_DataDmn3) || ! (DateTime.MinValue==AV127ContagemResultado_DataDmn_To3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data Demanda";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV126ContagemResultado_DataDmn3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "at�";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV127ContagemResultado_DataDmn_To3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV38DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAACEITE") == 0 )
                  {
                     AV128ContagemResultado_DataAceite3 = context.localUtil.CToD( AV47GridStateDynamicFilter.gxTpr_Value, 2);
                     AV129ContagemResultado_DataAceite_To3 = context.localUtil.CToD( AV47GridStateDynamicFilter.gxTpr_Valueto, 2);
                     if ( ! (DateTime.MinValue==AV128ContagemResultado_DataAceite3) || ! (DateTime.MinValue==AV129ContagemResultado_DataAceite_To3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Data do Aceite";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV128ContagemResultado_DataAceite3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "to";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
                        GXt_dtime1 = DateTimeUtil.ResetTime( AV129ContagemResultado_DataAceite_To3 ) ;
                        AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV38DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                  {
                     AV42ContagemResultado_SistemaCod3 = (int)(NumberUtil.Val( AV47GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV42ContagemResultado_SistemaCod3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV42ContagemResultado_SistemaCod3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV38DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                  {
                     AV43ContagemResultado_ContratadaCod3 = (int)(NumberUtil.Val( AV47GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV43ContagemResultado_ContratadaCod3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Prestadora";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV43ContagemResultado_ContratadaCod3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV38DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 )
                  {
                     AV130ContagemResultado_ContratadaOrigemCod3 = (int)(NumberUtil.Val( AV47GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV130ContagemResultado_ContratadaOrigemCod3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Origem";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV130ContagemResultado_ContratadaOrigemCod3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV38DynamicFiltersSelector3, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
                  {
                     AV44ContagemResultado_NaoCnfDmnCod3 = (int)(NumberUtil.Val( AV47GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV44ContagemResultado_NaoCnfDmnCod3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "N�o Conformidade";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV44ContagemResultado_NaoCnfDmnCod3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV38DynamicFiltersSelector3, "CONTAGEMRESULTADO_LOTEACEITE") == 0 )
                  {
                     AV55ContagemResultado_LoteAceite3 = AV47GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55ContagemResultado_LoteAceite3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Lote";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV55ContagemResultado_LoteAceite3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV38DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDAFM_ORDER") == 0 )
                  {
                     AV56ContagemResultado_DemandaFM_ORDER3 = (long)(NumberUtil.Val( AV47GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV56ContagemResultado_DemandaFM_ORDER3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS FM";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV56ContagemResultado_DemandaFM_ORDER3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV38DynamicFiltersSelector3, "CONTAGEMRESULTADO_BASELINE") == 0 )
                  {
                     AV59ContagemResultado_Baseline3 = AV47GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59ContagemResultado_Baseline3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Baseline";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
                        if ( StringUtil.StrCmp(StringUtil.Trim( AV59ContagemResultado_Baseline3), "S") == 0 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Sim";
                        }
                        else if ( StringUtil.StrCmp(StringUtil.Trim( AV59ContagemResultado_Baseline3), "N") == 0 )
                        {
                           AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "N�o";
                        }
                     }
                  }
                  else if ( StringUtil.StrCmp(AV38DynamicFiltersSelector3, "CONTAGEMRESULTADO_SERVICO") == 0 )
                  {
                     AV65ContagemResultado_Servico3 = (int)(NumberUtil.Val( AV47GridStateDynamicFilter.gxTpr_Value, "."));
                     if ( ! (0==AV65ContagemResultado_Servico3) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Servi�o";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = AV65ContagemResultado_Servico3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV38DynamicFiltersSelector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                  {
                     AV75ContagemResultado_Descricao3 = AV47GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75ContagemResultado_Descricao3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Titulo";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV75ContagemResultado_Descricao3;
                     }
                  }
                  else if ( StringUtil.StrCmp(AV38DynamicFiltersSelector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 )
                  {
                     AV131ContagemResultado_Agrupador3 = AV47GridStateDynamicFilter.gxTpr_Value;
                     if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV131ContagemResultado_Agrupador3)) )
                     {
                        AV13CellRow = (int)(AV13CellRow+1);
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Agrupador";
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
                        AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV131ContagemResultado_Agrupador3;
                     }
                  }
               }
            }
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV82TFContagemResultado_LoteAceite_Sel)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Lote";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV82TFContagemResultado_LoteAceite_Sel;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV81TFContagemResultado_LoteAceite)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Lote";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV81TFContagemResultado_LoteAceite;
            }
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV84TFContagemResultado_OsFsOsFm_Sel)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS Ref|OS";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV84TFContagemResultado_OsFsOsFm_Sel;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV83TFContagemResultado_OsFsOsFm)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "OS Ref|OS";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV83TFContagemResultado_OsFsOsFm;
            }
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV86TFContagemResultado_Descricao_Sel)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Titulo";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV86TFContagemResultado_Descricao_Sel;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV85TFContagemResultado_Descricao)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Titulo";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV85TFContagemResultado_Descricao;
            }
         }
         if ( ! ( (DateTime.MinValue==AV87TFContagemResultado_DataAceite) && (DateTime.MinValue==AV88TFContagemResultado_DataAceite_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Aceite";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = AV87TFContagemResultado_DataAceite;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = AV88TFContagemResultado_DataAceite_To;
         }
         if ( ! ( (DateTime.MinValue==AV89TFContagemResultado_DataUltCnt) && (DateTime.MinValue==AV90TFContagemResultado_DataUltCnt_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Contagem";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            GXt_dtime1 = DateTimeUtil.ResetTime( AV89TFContagemResultado_DataUltCnt ) ;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Date = GXt_dtime1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            GXt_dtime1 = DateTimeUtil.ResetTime( AV90TFContagemResultado_DataUltCnt_To ) ;
            AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = GXt_dtime1;
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV92TFContagemResultado_ContratadaOrigemSigla_Sel)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Origem";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV92TFContagemResultado_ContratadaOrigemSigla_Sel;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV91TFContagemResultado_ContratadaOrigemSigla)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Origem";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV91TFContagemResultado_ContratadaOrigemSigla;
            }
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV94TFContagemrResultado_SistemaSigla_Sel)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV94TFContagemrResultado_SistemaSigla_Sel;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV93TFContagemrResultado_SistemaSigla)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Sistema";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV93TFContagemrResultado_SistemaSigla;
            }
         }
         AV96TFContagemResultado_StatusDmn_Sels.FromJSonString(AV95TFContagemResultado_StatusDmn_SelsJson);
         if ( ! ( ( AV96TFContagemResultado_StatusDmn_Sels.Count == 0 ) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Status Dmn";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV113i = 1;
            AV134GXV1 = 1;
            while ( AV134GXV1 <= AV96TFContagemResultado_StatusDmn_Sels.Count )
            {
               AV97TFContagemResultado_StatusDmn_Sel = AV96TFContagemResultado_StatusDmn_Sels.GetString(AV134GXV1);
               if ( AV113i == 1 )
               {
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "";
               }
               else
               {
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text+", ";
               }
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text+gxdomainstatusdemanda.getDescription(context,AV97TFContagemResultado_StatusDmn_Sel);
               AV113i = (long)(AV113i+1);
               AV134GXV1 = (int)(AV134GXV1+1);
            }
         }
         if ( ! ( (0==AV98TFContagemResultado_Baseline_Sel) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "BS";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            if ( AV98TFContagemResultado_Baseline_Sel == 1 )
            {
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Marcado";
            }
            else if ( AV98TFContagemResultado_Baseline_Sel == 2 )
            {
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "Desmarcado";
            }
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV100TFContagemResultado_ServicoSigla_Sel)) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Servi�o";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV100TFContagemResultado_ServicoSigla_Sel;
         }
         else
         {
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV99TFContagemResultado_ServicoSigla)) ) )
            {
               AV13CellRow = (int)(AV13CellRow+1);
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Servi�o";
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
               AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = AV99TFContagemResultado_ServicoSigla;
            }
         }
         if ( ! ( (Convert.ToDecimal(0)==AV109TFContagemResultado_PFFinal) && (Convert.ToDecimal(0)==AV110TFContagemResultado_PFFinal_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "PF Final";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV109TFContagemResultado_PFFinal);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Number = (double)(AV110TFContagemResultado_PFFinal_To);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV111TFContagemResultado_ValorPF) && (Convert.ToDecimal(0)==AV112TFContagemResultado_ValorPF_To) ) )
         {
            AV13CellRow = (int)(AV13CellRow+1);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn, 1, 1).Text = "Valor da Unidade";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Number = (double)(AV111TFContagemResultado_ValorPF);
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 3;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "At�";
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Italic = 1;
            AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Number = (double)(AV112TFContagemResultado_ValorPF_To);
         }
         AV13CellRow = (int)(AV13CellRow+2);
      }

      protected void S151( )
      {
         /* 'WRITECOLUMNTITLES' Routine */
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = "Lote";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = "OS Ref|OS";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = "Titulo";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Text = "Aceite";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+4, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+4, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+4, 1, 1).Text = "Contagem";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+5, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+5, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+5, 1, 1).Text = "Origem";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+6, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+6, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+6, 1, 1).Text = "Sistema";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+7, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+7, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+7, 1, 1).Text = "Status Dmn";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Text = "BS";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Text = "Servi�o";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+10, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+10, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+10, 1, 1).Text = "PF Final";
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Bold = 1;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Color = 11;
         AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Text = "Valor da Unidade";
      }

      protected void S161( )
      {
         /* 'WRITEDATA' Routine */
         AV136ExtraWWContagemResultadoWWAceiteFaturamentoDS_1_Contratada_areatrabalhocod = AV18Contratada_AreaTrabalhoCod;
         AV137ExtraWWContagemResultadoWWAceiteFaturamentoDS_2_Contratada_codigo = AV72Contratada_Codigo;
         AV138ExtraWWContagemResultadoWWAceiteFaturamentoDS_3_Contagemresultado_statuscnt = AV19ContagemResultado_StatusCnt;
         AV139ExtraWWContagemResultadoWWAceiteFaturamentoDS_4_Contagemresultado_statusdmn = AV20ContagemResultado_StatusDmn;
         AV140ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1 = AV22DynamicFiltersSelector1;
         AV141ExtraWWContagemResultadoWWAceiteFaturamentoDS_6_Dynamicfiltersoperator1 = AV48DynamicFiltersOperator1;
         AV142ExtraWWContagemResultadoWWAceiteFaturamentoDS_7_Contagemresultado_dataultcnt1 = AV66ContagemResultado_DataUltCnt1;
         AV143ExtraWWContagemResultadoWWAceiteFaturamentoDS_8_Contagemresultado_dataultcnt_to1 = AV67ContagemResultado_DataUltCnt_To1;
         AV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1 = AV25ContagemResultado_OsFsOsFm1;
         AV145ExtraWWContagemResultadoWWAceiteFaturamentoDS_10_Contagemresultado_datadmn1 = AV114ContagemResultado_DataDmn1;
         AV146ExtraWWContagemResultadoWWAceiteFaturamentoDS_11_Contagemresultado_datadmn_to1 = AV115ContagemResultado_DataDmn_To1;
         AV147ExtraWWContagemResultadoWWAceiteFaturamentoDS_12_Contagemresultado_dataaceite1 = AV116ContagemResultado_DataAceite1;
         AV148ExtraWWContagemResultadoWWAceiteFaturamentoDS_13_Contagemresultado_dataaceite_to1 = AV117ContagemResultado_DataAceite_To1;
         AV149ExtraWWContagemResultadoWWAceiteFaturamentoDS_14_Contagemresultado_sistemacod1 = AV26ContagemResultado_SistemaCod1;
         AV150ExtraWWContagemResultadoWWAceiteFaturamentoDS_15_Contagemresultado_contratadacod1 = AV27ContagemResultado_ContratadaCod1;
         AV151ExtraWWContagemResultadoWWAceiteFaturamentoDS_16_Contagemresultado_contratadaorigemcod1 = AV118ContagemResultado_ContratadaOrigemCod1;
         AV152ExtraWWContagemResultadoWWAceiteFaturamentoDS_17_Contagemresultado_naocnfdmncod1 = AV28ContagemResultado_NaoCnfDmnCod1;
         AV153ExtraWWContagemResultadoWWAceiteFaturamentoDS_18_Contagemresultado_loteaceite1 = AV49ContagemResultado_LoteAceite1;
         AV154ExtraWWContagemResultadoWWAceiteFaturamentoDS_19_Contagemresultado_demandafm_order1 = AV50ContagemResultado_DemandaFM_ORDER1;
         AV155ExtraWWContagemResultadoWWAceiteFaturamentoDS_20_Contagemresultado_baseline1 = AV57ContagemResultado_Baseline1;
         AV156ExtraWWContagemResultadoWWAceiteFaturamentoDS_21_Contagemresultado_servico1 = AV63ContagemResultado_Servico1;
         AV157ExtraWWContagemResultadoWWAceiteFaturamentoDS_22_Contagemresultado_descricao1 = AV73ContagemResultado_Descricao1;
         AV158ExtraWWContagemResultadoWWAceiteFaturamentoDS_23_Contagemresultado_agrupador1 = AV119ContagemResultado_Agrupador1;
         AV159ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 = AV29DynamicFiltersEnabled2;
         AV160ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2 = AV30DynamicFiltersSelector2;
         AV161ExtraWWContagemResultadoWWAceiteFaturamentoDS_26_Dynamicfiltersoperator2 = AV51DynamicFiltersOperator2;
         AV162ExtraWWContagemResultadoWWAceiteFaturamentoDS_27_Contagemresultado_dataultcnt2 = AV68ContagemResultado_DataUltCnt2;
         AV163ExtraWWContagemResultadoWWAceiteFaturamentoDS_28_Contagemresultado_dataultcnt_to2 = AV69ContagemResultado_DataUltCnt_To2;
         AV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2 = AV33ContagemResultado_OsFsOsFm2;
         AV165ExtraWWContagemResultadoWWAceiteFaturamentoDS_30_Contagemresultado_datadmn2 = AV120ContagemResultado_DataDmn2;
         AV166ExtraWWContagemResultadoWWAceiteFaturamentoDS_31_Contagemresultado_datadmn_to2 = AV121ContagemResultado_DataDmn_To2;
         AV167ExtraWWContagemResultadoWWAceiteFaturamentoDS_32_Contagemresultado_dataaceite2 = AV122ContagemResultado_DataAceite2;
         AV168ExtraWWContagemResultadoWWAceiteFaturamentoDS_33_Contagemresultado_dataaceite_to2 = AV123ContagemResultado_DataAceite_To2;
         AV169ExtraWWContagemResultadoWWAceiteFaturamentoDS_34_Contagemresultado_sistemacod2 = AV34ContagemResultado_SistemaCod2;
         AV170ExtraWWContagemResultadoWWAceiteFaturamentoDS_35_Contagemresultado_contratadacod2 = AV35ContagemResultado_ContratadaCod2;
         AV171ExtraWWContagemResultadoWWAceiteFaturamentoDS_36_Contagemresultado_contratadaorigemcod2 = AV124ContagemResultado_ContratadaOrigemCod2;
         AV172ExtraWWContagemResultadoWWAceiteFaturamentoDS_37_Contagemresultado_naocnfdmncod2 = AV36ContagemResultado_NaoCnfDmnCod2;
         AV173ExtraWWContagemResultadoWWAceiteFaturamentoDS_38_Contagemresultado_loteaceite2 = AV52ContagemResultado_LoteAceite2;
         AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_39_Contagemresultado_demandafm_order2 = AV53ContagemResultado_DemandaFM_ORDER2;
         AV175ExtraWWContagemResultadoWWAceiteFaturamentoDS_40_Contagemresultado_baseline2 = AV58ContagemResultado_Baseline2;
         AV176ExtraWWContagemResultadoWWAceiteFaturamentoDS_41_Contagemresultado_servico2 = AV64ContagemResultado_Servico2;
         AV177ExtraWWContagemResultadoWWAceiteFaturamentoDS_42_Contagemresultado_descricao2 = AV74ContagemResultado_Descricao2;
         AV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_43_Contagemresultado_agrupador2 = AV125ContagemResultado_Agrupador2;
         AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 = AV37DynamicFiltersEnabled3;
         AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3 = AV38DynamicFiltersSelector3;
         AV181ExtraWWContagemResultadoWWAceiteFaturamentoDS_46_Dynamicfiltersoperator3 = AV54DynamicFiltersOperator3;
         AV182ExtraWWContagemResultadoWWAceiteFaturamentoDS_47_Contagemresultado_dataultcnt3 = AV70ContagemResultado_DataUltCnt3;
         AV183ExtraWWContagemResultadoWWAceiteFaturamentoDS_48_Contagemresultado_dataultcnt_to3 = AV71ContagemResultado_DataUltCnt_To3;
         AV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3 = AV41ContagemResultado_OsFsOsFm3;
         AV185ExtraWWContagemResultadoWWAceiteFaturamentoDS_50_Contagemresultado_datadmn3 = AV126ContagemResultado_DataDmn3;
         AV186ExtraWWContagemResultadoWWAceiteFaturamentoDS_51_Contagemresultado_datadmn_to3 = AV127ContagemResultado_DataDmn_To3;
         AV187ExtraWWContagemResultadoWWAceiteFaturamentoDS_52_Contagemresultado_dataaceite3 = AV128ContagemResultado_DataAceite3;
         AV188ExtraWWContagemResultadoWWAceiteFaturamentoDS_53_Contagemresultado_dataaceite_to3 = AV129ContagemResultado_DataAceite_To3;
         AV189ExtraWWContagemResultadoWWAceiteFaturamentoDS_54_Contagemresultado_sistemacod3 = AV42ContagemResultado_SistemaCod3;
         AV190ExtraWWContagemResultadoWWAceiteFaturamentoDS_55_Contagemresultado_contratadacod3 = AV43ContagemResultado_ContratadaCod3;
         AV191ExtraWWContagemResultadoWWAceiteFaturamentoDS_56_Contagemresultado_contratadaorigemcod3 = AV130ContagemResultado_ContratadaOrigemCod3;
         AV192ExtraWWContagemResultadoWWAceiteFaturamentoDS_57_Contagemresultado_naocnfdmncod3 = AV44ContagemResultado_NaoCnfDmnCod3;
         AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_58_Contagemresultado_loteaceite3 = AV55ContagemResultado_LoteAceite3;
         AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_59_Contagemresultado_demandafm_order3 = AV56ContagemResultado_DemandaFM_ORDER3;
         AV195ExtraWWContagemResultadoWWAceiteFaturamentoDS_60_Contagemresultado_baseline3 = AV59ContagemResultado_Baseline3;
         AV196ExtraWWContagemResultadoWWAceiteFaturamentoDS_61_Contagemresultado_servico3 = AV65ContagemResultado_Servico3;
         AV197ExtraWWContagemResultadoWWAceiteFaturamentoDS_62_Contagemresultado_descricao3 = AV75ContagemResultado_Descricao3;
         AV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_63_Contagemresultado_agrupador3 = AV131ContagemResultado_Agrupador3;
         AV199ExtraWWContagemResultadoWWAceiteFaturamentoDS_64_Tfcontagemresultado_loteaceite = AV81TFContagemResultado_LoteAceite;
         AV200ExtraWWContagemResultadoWWAceiteFaturamentoDS_65_Tfcontagemresultado_loteaceite_sel = AV82TFContagemResultado_LoteAceite_Sel;
         AV201ExtraWWContagemResultadoWWAceiteFaturamentoDS_66_Tfcontagemresultado_osfsosfm = AV83TFContagemResultado_OsFsOsFm;
         AV202ExtraWWContagemResultadoWWAceiteFaturamentoDS_67_Tfcontagemresultado_osfsosfm_sel = AV84TFContagemResultado_OsFsOsFm_Sel;
         AV203ExtraWWContagemResultadoWWAceiteFaturamentoDS_68_Tfcontagemresultado_descricao = AV85TFContagemResultado_Descricao;
         AV204ExtraWWContagemResultadoWWAceiteFaturamentoDS_69_Tfcontagemresultado_descricao_sel = AV86TFContagemResultado_Descricao_Sel;
         AV205ExtraWWContagemResultadoWWAceiteFaturamentoDS_70_Tfcontagemresultado_dataaceite = AV87TFContagemResultado_DataAceite;
         AV206ExtraWWContagemResultadoWWAceiteFaturamentoDS_71_Tfcontagemresultado_dataaceite_to = AV88TFContagemResultado_DataAceite_To;
         AV207ExtraWWContagemResultadoWWAceiteFaturamentoDS_72_Tfcontagemresultado_dataultcnt = AV89TFContagemResultado_DataUltCnt;
         AV208ExtraWWContagemResultadoWWAceiteFaturamentoDS_73_Tfcontagemresultado_dataultcnt_to = AV90TFContagemResultado_DataUltCnt_To;
         AV209ExtraWWContagemResultadoWWAceiteFaturamentoDS_74_Tfcontagemresultado_contratadaorigemsigla = AV91TFContagemResultado_ContratadaOrigemSigla;
         AV210ExtraWWContagemResultadoWWAceiteFaturamentoDS_75_Tfcontagemresultado_contratadaorigemsigla_sel = AV92TFContagemResultado_ContratadaOrigemSigla_Sel;
         AV211ExtraWWContagemResultadoWWAceiteFaturamentoDS_76_Tfcontagemrresultado_sistemasigla = AV93TFContagemrResultado_SistemaSigla;
         AV212ExtraWWContagemResultadoWWAceiteFaturamentoDS_77_Tfcontagemrresultado_sistemasigla_sel = AV94TFContagemrResultado_SistemaSigla_Sel;
         AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_78_Tfcontagemresultado_statusdmn_sels = AV96TFContagemResultado_StatusDmn_Sels;
         AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_79_Tfcontagemresultado_baseline_sel = AV98TFContagemResultado_Baseline_Sel;
         AV215ExtraWWContagemResultadoWWAceiteFaturamentoDS_80_Tfcontagemresultado_servicosigla = AV99TFContagemResultado_ServicoSigla;
         AV216ExtraWWContagemResultadoWWAceiteFaturamentoDS_81_Tfcontagemresultado_servicosigla_sel = AV100TFContagemResultado_ServicoSigla_Sel;
         AV217ExtraWWContagemResultadoWWAceiteFaturamentoDS_82_Tfcontagemresultado_pffinal = AV109TFContagemResultado_PFFinal;
         AV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_83_Tfcontagemresultado_pffinal_to = AV110TFContagemResultado_PFFinal_To;
         AV219ExtraWWContagemResultadoWWAceiteFaturamentoDS_84_Tfcontagemresultado_valorpf = AV111TFContagemResultado_ValorPF;
         AV220ExtraWWContagemResultadoWWAceiteFaturamentoDS_85_Tfcontagemresultado_valorpf_to = AV112TFContagemResultado_ValorPF_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A484ContagemResultado_StatusDmn ,
                                              AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_78_Tfcontagemresultado_statusdmn_sels ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              AV140ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1 ,
                                              AV141ExtraWWContagemResultadoWWAceiteFaturamentoDS_6_Dynamicfiltersoperator1 ,
                                              AV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1 ,
                                              AV145ExtraWWContagemResultadoWWAceiteFaturamentoDS_10_Contagemresultado_datadmn1 ,
                                              AV146ExtraWWContagemResultadoWWAceiteFaturamentoDS_11_Contagemresultado_datadmn_to1 ,
                                              AV147ExtraWWContagemResultadoWWAceiteFaturamentoDS_12_Contagemresultado_dataaceite1 ,
                                              AV148ExtraWWContagemResultadoWWAceiteFaturamentoDS_13_Contagemresultado_dataaceite_to1 ,
                                              AV149ExtraWWContagemResultadoWWAceiteFaturamentoDS_14_Contagemresultado_sistemacod1 ,
                                              AV150ExtraWWContagemResultadoWWAceiteFaturamentoDS_15_Contagemresultado_contratadacod1 ,
                                              AV136ExtraWWContagemResultadoWWAceiteFaturamentoDS_1_Contratada_areatrabalhocod ,
                                              AV151ExtraWWContagemResultadoWWAceiteFaturamentoDS_16_Contagemresultado_contratadaorigemcod1 ,
                                              AV152ExtraWWContagemResultadoWWAceiteFaturamentoDS_17_Contagemresultado_naocnfdmncod1 ,
                                              AV153ExtraWWContagemResultadoWWAceiteFaturamentoDS_18_Contagemresultado_loteaceite1 ,
                                              AV154ExtraWWContagemResultadoWWAceiteFaturamentoDS_19_Contagemresultado_demandafm_order1 ,
                                              AV155ExtraWWContagemResultadoWWAceiteFaturamentoDS_20_Contagemresultado_baseline1 ,
                                              AV156ExtraWWContagemResultadoWWAceiteFaturamentoDS_21_Contagemresultado_servico1 ,
                                              AV157ExtraWWContagemResultadoWWAceiteFaturamentoDS_22_Contagemresultado_descricao1 ,
                                              AV158ExtraWWContagemResultadoWWAceiteFaturamentoDS_23_Contagemresultado_agrupador1 ,
                                              AV159ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 ,
                                              AV160ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2 ,
                                              AV161ExtraWWContagemResultadoWWAceiteFaturamentoDS_26_Dynamicfiltersoperator2 ,
                                              AV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2 ,
                                              AV165ExtraWWContagemResultadoWWAceiteFaturamentoDS_30_Contagemresultado_datadmn2 ,
                                              AV166ExtraWWContagemResultadoWWAceiteFaturamentoDS_31_Contagemresultado_datadmn_to2 ,
                                              AV167ExtraWWContagemResultadoWWAceiteFaturamentoDS_32_Contagemresultado_dataaceite2 ,
                                              AV168ExtraWWContagemResultadoWWAceiteFaturamentoDS_33_Contagemresultado_dataaceite_to2 ,
                                              AV169ExtraWWContagemResultadoWWAceiteFaturamentoDS_34_Contagemresultado_sistemacod2 ,
                                              AV170ExtraWWContagemResultadoWWAceiteFaturamentoDS_35_Contagemresultado_contratadacod2 ,
                                              AV171ExtraWWContagemResultadoWWAceiteFaturamentoDS_36_Contagemresultado_contratadaorigemcod2 ,
                                              AV172ExtraWWContagemResultadoWWAceiteFaturamentoDS_37_Contagemresultado_naocnfdmncod2 ,
                                              AV173ExtraWWContagemResultadoWWAceiteFaturamentoDS_38_Contagemresultado_loteaceite2 ,
                                              AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_39_Contagemresultado_demandafm_order2 ,
                                              AV175ExtraWWContagemResultadoWWAceiteFaturamentoDS_40_Contagemresultado_baseline2 ,
                                              AV176ExtraWWContagemResultadoWWAceiteFaturamentoDS_41_Contagemresultado_servico2 ,
                                              AV177ExtraWWContagemResultadoWWAceiteFaturamentoDS_42_Contagemresultado_descricao2 ,
                                              AV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_43_Contagemresultado_agrupador2 ,
                                              AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 ,
                                              AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3 ,
                                              AV181ExtraWWContagemResultadoWWAceiteFaturamentoDS_46_Dynamicfiltersoperator3 ,
                                              AV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3 ,
                                              AV185ExtraWWContagemResultadoWWAceiteFaturamentoDS_50_Contagemresultado_datadmn3 ,
                                              AV186ExtraWWContagemResultadoWWAceiteFaturamentoDS_51_Contagemresultado_datadmn_to3 ,
                                              AV187ExtraWWContagemResultadoWWAceiteFaturamentoDS_52_Contagemresultado_dataaceite3 ,
                                              AV188ExtraWWContagemResultadoWWAceiteFaturamentoDS_53_Contagemresultado_dataaceite_to3 ,
                                              AV189ExtraWWContagemResultadoWWAceiteFaturamentoDS_54_Contagemresultado_sistemacod3 ,
                                              AV190ExtraWWContagemResultadoWWAceiteFaturamentoDS_55_Contagemresultado_contratadacod3 ,
                                              AV191ExtraWWContagemResultadoWWAceiteFaturamentoDS_56_Contagemresultado_contratadaorigemcod3 ,
                                              AV192ExtraWWContagemResultadoWWAceiteFaturamentoDS_57_Contagemresultado_naocnfdmncod3 ,
                                              AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_58_Contagemresultado_loteaceite3 ,
                                              AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_59_Contagemresultado_demandafm_order3 ,
                                              AV195ExtraWWContagemResultadoWWAceiteFaturamentoDS_60_Contagemresultado_baseline3 ,
                                              AV196ExtraWWContagemResultadoWWAceiteFaturamentoDS_61_Contagemresultado_servico3 ,
                                              AV197ExtraWWContagemResultadoWWAceiteFaturamentoDS_62_Contagemresultado_descricao3 ,
                                              AV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_63_Contagemresultado_agrupador3 ,
                                              AV200ExtraWWContagemResultadoWWAceiteFaturamentoDS_65_Tfcontagemresultado_loteaceite_sel ,
                                              AV199ExtraWWContagemResultadoWWAceiteFaturamentoDS_64_Tfcontagemresultado_loteaceite ,
                                              AV202ExtraWWContagemResultadoWWAceiteFaturamentoDS_67_Tfcontagemresultado_osfsosfm_sel ,
                                              AV201ExtraWWContagemResultadoWWAceiteFaturamentoDS_66_Tfcontagemresultado_osfsosfm ,
                                              AV204ExtraWWContagemResultadoWWAceiteFaturamentoDS_69_Tfcontagemresultado_descricao_sel ,
                                              AV203ExtraWWContagemResultadoWWAceiteFaturamentoDS_68_Tfcontagemresultado_descricao ,
                                              AV205ExtraWWContagemResultadoWWAceiteFaturamentoDS_70_Tfcontagemresultado_dataaceite ,
                                              AV206ExtraWWContagemResultadoWWAceiteFaturamentoDS_71_Tfcontagemresultado_dataaceite_to ,
                                              AV210ExtraWWContagemResultadoWWAceiteFaturamentoDS_75_Tfcontagemresultado_contratadaorigemsigla_sel ,
                                              AV209ExtraWWContagemResultadoWWAceiteFaturamentoDS_74_Tfcontagemresultado_contratadaorigemsigla ,
                                              AV212ExtraWWContagemResultadoWWAceiteFaturamentoDS_77_Tfcontagemrresultado_sistemasigla_sel ,
                                              AV211ExtraWWContagemResultadoWWAceiteFaturamentoDS_76_Tfcontagemrresultado_sistemasigla ,
                                              AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_78_Tfcontagemresultado_statusdmn_sels.Count ,
                                              AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_79_Tfcontagemresultado_baseline_sel ,
                                              AV216ExtraWWContagemResultadoWWAceiteFaturamentoDS_81_Tfcontagemresultado_servicosigla_sel ,
                                              AV215ExtraWWContagemResultadoWWAceiteFaturamentoDS_80_Tfcontagemresultado_servicosigla ,
                                              AV219ExtraWWContagemResultadoWWAceiteFaturamentoDS_84_Tfcontagemresultado_valorpf ,
                                              AV220ExtraWWContagemResultadoWWAceiteFaturamentoDS_85_Tfcontagemresultado_valorpf_to ,
                                              AV46GridState.gxTpr_Dynamicfilters.Count ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A529ContagemResultado_DataAceite ,
                                              A489ContagemResultado_SistemaCod ,
                                              A805ContagemResultado_ContratadaOrigemCod ,
                                              A468ContagemResultado_NaoCnfDmnCod ,
                                              A528ContagemResultado_LoteAceite ,
                                              A598ContagemResultado_Baseline ,
                                              A601ContagemResultado_Servico ,
                                              A494ContagemResultado_Descricao ,
                                              A1046ContagemResultado_Agrupador ,
                                              A866ContagemResultado_ContratadaOrigemSigla ,
                                              A509ContagemrResultado_SistemaSigla ,
                                              A801ContagemResultado_ServicoSigla ,
                                              A512ContagemResultado_ValorPF ,
                                              A456ContagemResultado_Codigo ,
                                              AV16OrderedBy ,
                                              AV142ExtraWWContagemResultadoWWAceiteFaturamentoDS_7_Contagemresultado_dataultcnt1 ,
                                              A566ContagemResultado_DataUltCnt ,
                                              AV143ExtraWWContagemResultadoWWAceiteFaturamentoDS_8_Contagemresultado_dataultcnt_to1 ,
                                              AV162ExtraWWContagemResultadoWWAceiteFaturamentoDS_27_Contagemresultado_dataultcnt2 ,
                                              AV163ExtraWWContagemResultadoWWAceiteFaturamentoDS_28_Contagemresultado_dataultcnt_to2 ,
                                              AV182ExtraWWContagemResultadoWWAceiteFaturamentoDS_47_Contagemresultado_dataultcnt3 ,
                                              AV183ExtraWWContagemResultadoWWAceiteFaturamentoDS_48_Contagemresultado_dataultcnt_to3 ,
                                              AV207ExtraWWContagemResultadoWWAceiteFaturamentoDS_72_Tfcontagemresultado_dataultcnt ,
                                              AV208ExtraWWContagemResultadoWWAceiteFaturamentoDS_73_Tfcontagemresultado_dataultcnt_to ,
                                              AV217ExtraWWContagemResultadoWWAceiteFaturamentoDS_82_Tfcontagemresultado_pffinal ,
                                              A574ContagemResultado_PFFinal ,
                                              AV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_83_Tfcontagemresultado_pffinal_to ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo ,
                                              A531ContagemResultado_StatusUltCnt ,
                                              A517ContagemResultado_Ultima ,
                                              A1583ContagemResultado_TipoRegistro },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.LONG, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT
                                              }
         });
         lV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1), "%", "");
         lV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1), "%", "");
         lV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1), "%", "");
         lV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1 = StringUtil.Concat( StringUtil.RTrim( AV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1), "%", "");
         lV153ExtraWWContagemResultadoWWAceiteFaturamentoDS_18_Contagemresultado_loteaceite1 = StringUtil.PadR( StringUtil.RTrim( AV153ExtraWWContagemResultadoWWAceiteFaturamentoDS_18_Contagemresultado_loteaceite1), 10, "%");
         lV157ExtraWWContagemResultadoWWAceiteFaturamentoDS_22_Contagemresultado_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV157ExtraWWContagemResultadoWWAceiteFaturamentoDS_22_Contagemresultado_descricao1), "%", "");
         lV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2), "%", "");
         lV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2), "%", "");
         lV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2), "%", "");
         lV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2 = StringUtil.Concat( StringUtil.RTrim( AV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2), "%", "");
         lV173ExtraWWContagemResultadoWWAceiteFaturamentoDS_38_Contagemresultado_loteaceite2 = StringUtil.PadR( StringUtil.RTrim( AV173ExtraWWContagemResultadoWWAceiteFaturamentoDS_38_Contagemresultado_loteaceite2), 10, "%");
         lV177ExtraWWContagemResultadoWWAceiteFaturamentoDS_42_Contagemresultado_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV177ExtraWWContagemResultadoWWAceiteFaturamentoDS_42_Contagemresultado_descricao2), "%", "");
         lV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3), "%", "");
         lV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3), "%", "");
         lV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3), "%", "");
         lV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3 = StringUtil.Concat( StringUtil.RTrim( AV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3), "%", "");
         lV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_58_Contagemresultado_loteaceite3 = StringUtil.PadR( StringUtil.RTrim( AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_58_Contagemresultado_loteaceite3), 10, "%");
         lV197ExtraWWContagemResultadoWWAceiteFaturamentoDS_62_Contagemresultado_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV197ExtraWWContagemResultadoWWAceiteFaturamentoDS_62_Contagemresultado_descricao3), "%", "");
         lV199ExtraWWContagemResultadoWWAceiteFaturamentoDS_64_Tfcontagemresultado_loteaceite = StringUtil.PadR( StringUtil.RTrim( AV199ExtraWWContagemResultadoWWAceiteFaturamentoDS_64_Tfcontagemresultado_loteaceite), 10, "%");
         lV201ExtraWWContagemResultadoWWAceiteFaturamentoDS_66_Tfcontagemresultado_osfsosfm = StringUtil.Concat( StringUtil.RTrim( AV201ExtraWWContagemResultadoWWAceiteFaturamentoDS_66_Tfcontagemresultado_osfsosfm), "%", "");
         lV203ExtraWWContagemResultadoWWAceiteFaturamentoDS_68_Tfcontagemresultado_descricao = StringUtil.Concat( StringUtil.RTrim( AV203ExtraWWContagemResultadoWWAceiteFaturamentoDS_68_Tfcontagemresultado_descricao), "%", "");
         lV209ExtraWWContagemResultadoWWAceiteFaturamentoDS_74_Tfcontagemresultado_contratadaorigemsigla = StringUtil.PadR( StringUtil.RTrim( AV209ExtraWWContagemResultadoWWAceiteFaturamentoDS_74_Tfcontagemresultado_contratadaorigemsigla), 15, "%");
         lV211ExtraWWContagemResultadoWWAceiteFaturamentoDS_76_Tfcontagemrresultado_sistemasigla = StringUtil.PadR( StringUtil.RTrim( AV211ExtraWWContagemResultadoWWAceiteFaturamentoDS_76_Tfcontagemrresultado_sistemasigla), 25, "%");
         lV215ExtraWWContagemResultadoWWAceiteFaturamentoDS_80_Tfcontagemresultado_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV215ExtraWWContagemResultadoWWAceiteFaturamentoDS_80_Tfcontagemresultado_servicosigla), 15, "%");
         /* Using cursor P003V3 */
         pr_default.execute(0, new Object[] {AV140ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, AV142ExtraWWContagemResultadoWWAceiteFaturamentoDS_7_Contagemresultado_dataultcnt1, AV142ExtraWWContagemResultadoWWAceiteFaturamentoDS_7_Contagemresultado_dataultcnt1, AV140ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, AV143ExtraWWContagemResultadoWWAceiteFaturamentoDS_8_Contagemresultado_dataultcnt_to1, AV143ExtraWWContagemResultadoWWAceiteFaturamentoDS_8_Contagemresultado_dataultcnt_to1, AV159ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2, AV160ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, AV162ExtraWWContagemResultadoWWAceiteFaturamentoDS_27_Contagemresultado_dataultcnt2, AV162ExtraWWContagemResultadoWWAceiteFaturamentoDS_27_Contagemresultado_dataultcnt2, AV159ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2, AV160ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, AV163ExtraWWContagemResultadoWWAceiteFaturamentoDS_28_Contagemresultado_dataultcnt_to2, AV163ExtraWWContagemResultadoWWAceiteFaturamentoDS_28_Contagemresultado_dataultcnt_to2, AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3, AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, AV182ExtraWWContagemResultadoWWAceiteFaturamentoDS_47_Contagemresultado_dataultcnt3, AV182ExtraWWContagemResultadoWWAceiteFaturamentoDS_47_Contagemresultado_dataultcnt3, AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3, AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, AV183ExtraWWContagemResultadoWWAceiteFaturamentoDS_48_Contagemresultado_dataultcnt_to3, AV183ExtraWWContagemResultadoWWAceiteFaturamentoDS_48_Contagemresultado_dataultcnt_to3, AV207ExtraWWContagemResultadoWWAceiteFaturamentoDS_72_Tfcontagemresultado_dataultcnt, AV207ExtraWWContagemResultadoWWAceiteFaturamentoDS_72_Tfcontagemresultado_dataultcnt, AV208ExtraWWContagemResultadoWWAceiteFaturamentoDS_73_Tfcontagemresultado_dataultcnt_to, AV208ExtraWWContagemResultadoWWAceiteFaturamentoDS_73_Tfcontagemresultado_dataultcnt_to, AV9WWPContext.gxTpr_Areatrabalho_codigo, AV9WWPContext.gxTpr_Contratada_codigo, AV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1, AV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1, lV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1, lV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1, lV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1, lV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1, AV145ExtraWWContagemResultadoWWAceiteFaturamentoDS_10_Contagemresultado_datadmn1, AV146ExtraWWContagemResultadoWWAceiteFaturamentoDS_11_Contagemresultado_datadmn_to1, AV147ExtraWWContagemResultadoWWAceiteFaturamentoDS_12_Contagemresultado_dataaceite1, AV148ExtraWWContagemResultadoWWAceiteFaturamentoDS_13_Contagemresultado_dataaceite_to1, AV149ExtraWWContagemResultadoWWAceiteFaturamentoDS_14_Contagemresultado_sistemacod1, AV150ExtraWWContagemResultadoWWAceiteFaturamentoDS_15_Contagemresultado_contratadacod1, AV151ExtraWWContagemResultadoWWAceiteFaturamentoDS_16_Contagemresultado_contratadaorigemcod1, AV152ExtraWWContagemResultadoWWAceiteFaturamentoDS_17_Contagemresultado_naocnfdmncod1, lV153ExtraWWContagemResultadoWWAceiteFaturamentoDS_18_Contagemresultado_loteaceite1, AV154ExtraWWContagemResultadoWWAceiteFaturamentoDS_19_Contagemresultado_demandafm_order1, AV156ExtraWWContagemResultadoWWAceiteFaturamentoDS_21_Contagemresultado_servico1, lV157ExtraWWContagemResultadoWWAceiteFaturamentoDS_22_Contagemresultado_descricao1, AV158ExtraWWContagemResultadoWWAceiteFaturamentoDS_23_Contagemresultado_agrupador1, AV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2, AV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2, lV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2, lV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2, lV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2, lV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2, AV165ExtraWWContagemResultadoWWAceiteFaturamentoDS_30_Contagemresultado_datadmn2, AV166ExtraWWContagemResultadoWWAceiteFaturamentoDS_31_Contagemresultado_datadmn_to2, AV167ExtraWWContagemResultadoWWAceiteFaturamentoDS_32_Contagemresultado_dataaceite2, AV168ExtraWWContagemResultadoWWAceiteFaturamentoDS_33_Contagemresultado_dataaceite_to2, AV169ExtraWWContagemResultadoWWAceiteFaturamentoDS_34_Contagemresultado_sistemacod2, AV170ExtraWWContagemResultadoWWAceiteFaturamentoDS_35_Contagemresultado_contratadacod2, AV171ExtraWWContagemResultadoWWAceiteFaturamentoDS_36_Contagemresultado_contratadaorigemcod2, AV172ExtraWWContagemResultadoWWAceiteFaturamentoDS_37_Contagemresultado_naocnfdmncod2, lV173ExtraWWContagemResultadoWWAceiteFaturamentoDS_38_Contagemresultado_loteaceite2, AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_39_Contagemresultado_demandafm_order2, AV176ExtraWWContagemResultadoWWAceiteFaturamentoDS_41_Contagemresultado_servico2, lV177ExtraWWContagemResultadoWWAceiteFaturamentoDS_42_Contagemresultado_descricao2, AV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_43_Contagemresultado_agrupador2, AV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3, AV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3, lV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3, lV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3, lV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3, lV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3, AV185ExtraWWContagemResultadoWWAceiteFaturamentoDS_50_Contagemresultado_datadmn3, AV186ExtraWWContagemResultadoWWAceiteFaturamentoDS_51_Contagemresultado_datadmn_to3, AV187ExtraWWContagemResultadoWWAceiteFaturamentoDS_52_Contagemresultado_dataaceite3, AV188ExtraWWContagemResultadoWWAceiteFaturamentoDS_53_Contagemresultado_dataaceite_to3, AV189ExtraWWContagemResultadoWWAceiteFaturamentoDS_54_Contagemresultado_sistemacod3, AV190ExtraWWContagemResultadoWWAceiteFaturamentoDS_55_Contagemresultado_contratadacod3, AV191ExtraWWContagemResultadoWWAceiteFaturamentoDS_56_Contagemresultado_contratadaorigemcod3, AV192ExtraWWContagemResultadoWWAceiteFaturamentoDS_57_Contagemresultado_naocnfdmncod3, lV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_58_Contagemresultado_loteaceite3, AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_59_Contagemresultado_demandafm_order3, AV196ExtraWWContagemResultadoWWAceiteFaturamentoDS_61_Contagemresultado_servico3, lV197ExtraWWContagemResultadoWWAceiteFaturamentoDS_62_Contagemresultado_descricao3, AV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_63_Contagemresultado_agrupador3, lV199ExtraWWContagemResultadoWWAceiteFaturamentoDS_64_Tfcontagemresultado_loteaceite, AV200ExtraWWContagemResultadoWWAceiteFaturamentoDS_65_Tfcontagemresultado_loteaceite_sel, lV201ExtraWWContagemResultadoWWAceiteFaturamentoDS_66_Tfcontagemresultado_osfsosfm, AV202ExtraWWContagemResultadoWWAceiteFaturamentoDS_67_Tfcontagemresultado_osfsosfm_sel, lV203ExtraWWContagemResultadoWWAceiteFaturamentoDS_68_Tfcontagemresultado_descricao, AV204ExtraWWContagemResultadoWWAceiteFaturamentoDS_69_Tfcontagemresultado_descricao_sel, AV205ExtraWWContagemResultadoWWAceiteFaturamentoDS_70_Tfcontagemresultado_dataaceite, AV206ExtraWWContagemResultadoWWAceiteFaturamentoDS_71_Tfcontagemresultado_dataaceite_to, lV209ExtraWWContagemResultadoWWAceiteFaturamentoDS_74_Tfcontagemresultado_contratadaorigemsigla, AV210ExtraWWContagemResultadoWWAceiteFaturamentoDS_75_Tfcontagemresultado_contratadaorigemsigla_sel, lV211ExtraWWContagemResultadoWWAceiteFaturamentoDS_76_Tfcontagemrresultado_sistemasigla, AV212ExtraWWContagemResultadoWWAceiteFaturamentoDS_77_Tfcontagemrresultado_sistemasigla_sel, lV215ExtraWWContagemResultadoWWAceiteFaturamentoDS_80_Tfcontagemresultado_servicosigla, AV216ExtraWWContagemResultadoWWAceiteFaturamentoDS_81_Tfcontagemresultado_servicosigla_sel, AV219ExtraWWContagemResultadoWWAceiteFaturamentoDS_84_Tfcontagemresultado_valorpf, AV220ExtraWWContagemResultadoWWAceiteFaturamentoDS_85_Tfcontagemresultado_valorpf_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A597ContagemResultado_LoteAceiteCod = P003V3_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P003V3_n597ContagemResultado_LoteAceiteCod[0];
            A1553ContagemResultado_CntSrvCod = P003V3_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P003V3_n1553ContagemResultado_CntSrvCod[0];
            A1583ContagemResultado_TipoRegistro = P003V3_A1583ContagemResultado_TipoRegistro[0];
            A517ContagemResultado_Ultima = P003V3_A517ContagemResultado_Ultima[0];
            A512ContagemResultado_ValorPF = P003V3_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P003V3_n512ContagemResultado_ValorPF[0];
            A801ContagemResultado_ServicoSigla = P003V3_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P003V3_n801ContagemResultado_ServicoSigla[0];
            A509ContagemrResultado_SistemaSigla = P003V3_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P003V3_n509ContagemrResultado_SistemaSigla[0];
            A866ContagemResultado_ContratadaOrigemSigla = P003V3_A866ContagemResultado_ContratadaOrigemSigla[0];
            n866ContagemResultado_ContratadaOrigemSigla = P003V3_n866ContagemResultado_ContratadaOrigemSigla[0];
            A1046ContagemResultado_Agrupador = P003V3_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P003V3_n1046ContagemResultado_Agrupador[0];
            A494ContagemResultado_Descricao = P003V3_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P003V3_n494ContagemResultado_Descricao[0];
            A601ContagemResultado_Servico = P003V3_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P003V3_n601ContagemResultado_Servico[0];
            A598ContagemResultado_Baseline = P003V3_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = P003V3_n598ContagemResultado_Baseline[0];
            A528ContagemResultado_LoteAceite = P003V3_A528ContagemResultado_LoteAceite[0];
            n528ContagemResultado_LoteAceite = P003V3_n528ContagemResultado_LoteAceite[0];
            A468ContagemResultado_NaoCnfDmnCod = P003V3_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = P003V3_n468ContagemResultado_NaoCnfDmnCod[0];
            A805ContagemResultado_ContratadaOrigemCod = P003V3_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = P003V3_n805ContagemResultado_ContratadaOrigemCod[0];
            A489ContagemResultado_SistemaCod = P003V3_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P003V3_n489ContagemResultado_SistemaCod[0];
            A529ContagemResultado_DataAceite = P003V3_A529ContagemResultado_DataAceite[0];
            n529ContagemResultado_DataAceite = P003V3_n529ContagemResultado_DataAceite[0];
            A471ContagemResultado_DataDmn = P003V3_A471ContagemResultado_DataDmn[0];
            A484ContagemResultado_StatusDmn = P003V3_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P003V3_n484ContagemResultado_StatusDmn[0];
            A490ContagemResultado_ContratadaCod = P003V3_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P003V3_n490ContagemResultado_ContratadaCod[0];
            A52Contratada_AreaTrabalhoCod = P003V3_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P003V3_n52Contratada_AreaTrabalhoCod[0];
            A566ContagemResultado_DataUltCnt = P003V3_A566ContagemResultado_DataUltCnt[0];
            A531ContagemResultado_StatusUltCnt = P003V3_A531ContagemResultado_StatusUltCnt[0];
            A493ContagemResultado_DemandaFM = P003V3_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P003V3_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P003V3_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P003V3_n457ContagemResultado_Demanda[0];
            A456ContagemResultado_Codigo = P003V3_A456ContagemResultado_Codigo[0];
            A473ContagemResultado_DataCnt = P003V3_A473ContagemResultado_DataCnt[0];
            A511ContagemResultado_HoraCnt = P003V3_A511ContagemResultado_HoraCnt[0];
            A597ContagemResultado_LoteAceiteCod = P003V3_A597ContagemResultado_LoteAceiteCod[0];
            n597ContagemResultado_LoteAceiteCod = P003V3_n597ContagemResultado_LoteAceiteCod[0];
            A1553ContagemResultado_CntSrvCod = P003V3_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P003V3_n1553ContagemResultado_CntSrvCod[0];
            A1583ContagemResultado_TipoRegistro = P003V3_A1583ContagemResultado_TipoRegistro[0];
            A512ContagemResultado_ValorPF = P003V3_A512ContagemResultado_ValorPF[0];
            n512ContagemResultado_ValorPF = P003V3_n512ContagemResultado_ValorPF[0];
            A1046ContagemResultado_Agrupador = P003V3_A1046ContagemResultado_Agrupador[0];
            n1046ContagemResultado_Agrupador = P003V3_n1046ContagemResultado_Agrupador[0];
            A494ContagemResultado_Descricao = P003V3_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P003V3_n494ContagemResultado_Descricao[0];
            A598ContagemResultado_Baseline = P003V3_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = P003V3_n598ContagemResultado_Baseline[0];
            A468ContagemResultado_NaoCnfDmnCod = P003V3_A468ContagemResultado_NaoCnfDmnCod[0];
            n468ContagemResultado_NaoCnfDmnCod = P003V3_n468ContagemResultado_NaoCnfDmnCod[0];
            A805ContagemResultado_ContratadaOrigemCod = P003V3_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = P003V3_n805ContagemResultado_ContratadaOrigemCod[0];
            A489ContagemResultado_SistemaCod = P003V3_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P003V3_n489ContagemResultado_SistemaCod[0];
            A471ContagemResultado_DataDmn = P003V3_A471ContagemResultado_DataDmn[0];
            A484ContagemResultado_StatusDmn = P003V3_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P003V3_n484ContagemResultado_StatusDmn[0];
            A490ContagemResultado_ContratadaCod = P003V3_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P003V3_n490ContagemResultado_ContratadaCod[0];
            A493ContagemResultado_DemandaFM = P003V3_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P003V3_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P003V3_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P003V3_n457ContagemResultado_Demanda[0];
            A528ContagemResultado_LoteAceite = P003V3_A528ContagemResultado_LoteAceite[0];
            n528ContagemResultado_LoteAceite = P003V3_n528ContagemResultado_LoteAceite[0];
            A529ContagemResultado_DataAceite = P003V3_A529ContagemResultado_DataAceite[0];
            n529ContagemResultado_DataAceite = P003V3_n529ContagemResultado_DataAceite[0];
            A601ContagemResultado_Servico = P003V3_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P003V3_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P003V3_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P003V3_n801ContagemResultado_ServicoSigla[0];
            A866ContagemResultado_ContratadaOrigemSigla = P003V3_A866ContagemResultado_ContratadaOrigemSigla[0];
            n866ContagemResultado_ContratadaOrigemSigla = P003V3_n866ContagemResultado_ContratadaOrigemSigla[0];
            A509ContagemrResultado_SistemaSigla = P003V3_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P003V3_n509ContagemrResultado_SistemaSigla[0];
            A52Contratada_AreaTrabalhoCod = P003V3_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P003V3_n52Contratada_AreaTrabalhoCod[0];
            A566ContagemResultado_DataUltCnt = P003V3_A566ContagemResultado_DataUltCnt[0];
            A531ContagemResultado_StatusUltCnt = P003V3_A531ContagemResultado_StatusUltCnt[0];
            GXt_decimal2 = A574ContagemResultado_PFFinal;
            new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal2) ;
            A574ContagemResultado_PFFinal = GXt_decimal2;
            if ( (Convert.ToDecimal(0)==AV217ExtraWWContagemResultadoWWAceiteFaturamentoDS_82_Tfcontagemresultado_pffinal) || ( ( A574ContagemResultado_PFFinal >= AV217ExtraWWContagemResultadoWWAceiteFaturamentoDS_82_Tfcontagemresultado_pffinal ) ) )
            {
               if ( (Convert.ToDecimal(0)==AV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_83_Tfcontagemresultado_pffinal_to) || ( ( A574ContagemResultado_PFFinal <= AV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_83_Tfcontagemresultado_pffinal_to ) ) )
               {
                  A553ContagemResultado_DemandaFM_ORDER = (long)(NumberUtil.Val( StringUtil.Substring( A493ContagemResultado_DemandaFM, 1, 18), "."));
                  A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
                  AV13CellRow = (int)(AV13CellRow+1);
                  /* Execute user subroutine: 'BEFOREWRITELINE' */
                  S172 ();
                  if ( returnInSub )
                  {
                     pr_default.close(0);
                     returnInSub = true;
                     if (true) return;
                  }
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+0, 1, 1).Text = A528ContagemResultado_LoteAceite;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+1, 1, 1).Text = A501ContagemResultado_OsFsOsFm;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+2, 1, 1).Text = A494ContagemResultado_Descricao;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+3, 1, 1).Date = A529ContagemResultado_DataAceite;
                  GXt_dtime1 = DateTimeUtil.ResetTime( A566ContagemResultado_DataUltCnt ) ;
                  AV10ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+4, 1, 1).Date = GXt_dtime1;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+5, 1, 1).Text = A866ContagemResultado_ContratadaOrigemSigla;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+6, 1, 1).Text = A509ContagemrResultado_SistemaSigla;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+7, 1, 1).Text = gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Text = "";
                  if ( StringUtil.StrCmp(StringUtil.Trim( StringUtil.BoolToStr( A598ContagemResultado_Baseline)), "True") == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Text = "*";
                  }
                  else if ( StringUtil.StrCmp(StringUtil.Trim( StringUtil.BoolToStr( A598ContagemResultado_Baseline)), "False") == 0 )
                  {
                     AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+8, 1, 1).Text = "";
                  }
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+9, 1, 1).Text = A801ContagemResultado_ServicoSigla;
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+10, 1, 1).Number = (double)(A574ContagemResultado_PFFinal);
                  AV10ExcelDocument.get_Cells(AV13CellRow, AV14FirstColumn+11, 1, 1).Number = (double)(A512ContagemResultado_ValorPF);
                  /* Execute user subroutine: 'AFTERWRITELINE' */
                  S182 ();
                  if ( returnInSub )
                  {
                     pr_default.close(0);
                     returnInSub = true;
                     if (true) return;
                  }
               }
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void S191( )
      {
         /* 'CLOSEDOCUMENT' Routine */
         AV10ExcelDocument.Save();
         /* Execute user subroutine: 'CHECKSTATUS' */
         S121 ();
         if (returnInSub) return;
         AV10ExcelDocument.Close();
      }

      protected void S121( )
      {
         /* 'CHECKSTATUS' Routine */
         if ( AV10ExcelDocument.ErrCode != 0 )
         {
            AV11Filename = "";
            AV12ErrorMessage = AV10ExcelDocument.ErrDescription;
            AV10ExcelDocument.Close();
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S172( )
      {
         /* 'BEFOREWRITELINE' Routine */
      }

      protected void S182( )
      {
         /* 'AFTERWRITELINE' Routine */
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10ExcelDocument = new ExcelDocumentI();
         AV46GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV47GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV22DynamicFiltersSelector1 = "";
         AV66ContagemResultado_DataUltCnt1 = DateTime.MinValue;
         AV67ContagemResultado_DataUltCnt_To1 = DateTime.MinValue;
         AV25ContagemResultado_OsFsOsFm1 = "";
         AV114ContagemResultado_DataDmn1 = DateTime.MinValue;
         AV115ContagemResultado_DataDmn_To1 = DateTime.MinValue;
         AV116ContagemResultado_DataAceite1 = DateTime.MinValue;
         AV117ContagemResultado_DataAceite_To1 = DateTime.MinValue;
         AV49ContagemResultado_LoteAceite1 = "";
         AV57ContagemResultado_Baseline1 = "";
         AV73ContagemResultado_Descricao1 = "";
         AV119ContagemResultado_Agrupador1 = "";
         AV30DynamicFiltersSelector2 = "";
         AV68ContagemResultado_DataUltCnt2 = DateTime.MinValue;
         AV69ContagemResultado_DataUltCnt_To2 = DateTime.MinValue;
         AV33ContagemResultado_OsFsOsFm2 = "";
         AV120ContagemResultado_DataDmn2 = DateTime.MinValue;
         AV121ContagemResultado_DataDmn_To2 = DateTime.MinValue;
         AV122ContagemResultado_DataAceite2 = DateTime.MinValue;
         AV123ContagemResultado_DataAceite_To2 = DateTime.MinValue;
         AV52ContagemResultado_LoteAceite2 = "";
         AV58ContagemResultado_Baseline2 = "";
         AV74ContagemResultado_Descricao2 = "";
         AV125ContagemResultado_Agrupador2 = "";
         AV38DynamicFiltersSelector3 = "";
         AV70ContagemResultado_DataUltCnt3 = DateTime.MinValue;
         AV71ContagemResultado_DataUltCnt_To3 = DateTime.MinValue;
         AV41ContagemResultado_OsFsOsFm3 = "";
         AV126ContagemResultado_DataDmn3 = DateTime.MinValue;
         AV127ContagemResultado_DataDmn_To3 = DateTime.MinValue;
         AV128ContagemResultado_DataAceite3 = DateTime.MinValue;
         AV129ContagemResultado_DataAceite_To3 = DateTime.MinValue;
         AV55ContagemResultado_LoteAceite3 = "";
         AV59ContagemResultado_Baseline3 = "";
         AV75ContagemResultado_Descricao3 = "";
         AV131ContagemResultado_Agrupador3 = "";
         AV96TFContagemResultado_StatusDmn_Sels = new GxSimpleCollection();
         AV97TFContagemResultado_StatusDmn_Sel = "";
         AV139ExtraWWContagemResultadoWWAceiteFaturamentoDS_4_Contagemresultado_statusdmn = "";
         AV140ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1 = "";
         AV142ExtraWWContagemResultadoWWAceiteFaturamentoDS_7_Contagemresultado_dataultcnt1 = DateTime.MinValue;
         AV143ExtraWWContagemResultadoWWAceiteFaturamentoDS_8_Contagemresultado_dataultcnt_to1 = DateTime.MinValue;
         AV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1 = "";
         AV145ExtraWWContagemResultadoWWAceiteFaturamentoDS_10_Contagemresultado_datadmn1 = DateTime.MinValue;
         AV146ExtraWWContagemResultadoWWAceiteFaturamentoDS_11_Contagemresultado_datadmn_to1 = DateTime.MinValue;
         AV147ExtraWWContagemResultadoWWAceiteFaturamentoDS_12_Contagemresultado_dataaceite1 = DateTime.MinValue;
         AV148ExtraWWContagemResultadoWWAceiteFaturamentoDS_13_Contagemresultado_dataaceite_to1 = DateTime.MinValue;
         AV153ExtraWWContagemResultadoWWAceiteFaturamentoDS_18_Contagemresultado_loteaceite1 = "";
         AV155ExtraWWContagemResultadoWWAceiteFaturamentoDS_20_Contagemresultado_baseline1 = "";
         AV157ExtraWWContagemResultadoWWAceiteFaturamentoDS_22_Contagemresultado_descricao1 = "";
         AV158ExtraWWContagemResultadoWWAceiteFaturamentoDS_23_Contagemresultado_agrupador1 = "";
         AV160ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2 = "";
         AV162ExtraWWContagemResultadoWWAceiteFaturamentoDS_27_Contagemresultado_dataultcnt2 = DateTime.MinValue;
         AV163ExtraWWContagemResultadoWWAceiteFaturamentoDS_28_Contagemresultado_dataultcnt_to2 = DateTime.MinValue;
         AV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2 = "";
         AV165ExtraWWContagemResultadoWWAceiteFaturamentoDS_30_Contagemresultado_datadmn2 = DateTime.MinValue;
         AV166ExtraWWContagemResultadoWWAceiteFaturamentoDS_31_Contagemresultado_datadmn_to2 = DateTime.MinValue;
         AV167ExtraWWContagemResultadoWWAceiteFaturamentoDS_32_Contagemresultado_dataaceite2 = DateTime.MinValue;
         AV168ExtraWWContagemResultadoWWAceiteFaturamentoDS_33_Contagemresultado_dataaceite_to2 = DateTime.MinValue;
         AV173ExtraWWContagemResultadoWWAceiteFaturamentoDS_38_Contagemresultado_loteaceite2 = "";
         AV175ExtraWWContagemResultadoWWAceiteFaturamentoDS_40_Contagemresultado_baseline2 = "";
         AV177ExtraWWContagemResultadoWWAceiteFaturamentoDS_42_Contagemresultado_descricao2 = "";
         AV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_43_Contagemresultado_agrupador2 = "";
         AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3 = "";
         AV182ExtraWWContagemResultadoWWAceiteFaturamentoDS_47_Contagemresultado_dataultcnt3 = DateTime.MinValue;
         AV183ExtraWWContagemResultadoWWAceiteFaturamentoDS_48_Contagemresultado_dataultcnt_to3 = DateTime.MinValue;
         AV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3 = "";
         AV185ExtraWWContagemResultadoWWAceiteFaturamentoDS_50_Contagemresultado_datadmn3 = DateTime.MinValue;
         AV186ExtraWWContagemResultadoWWAceiteFaturamentoDS_51_Contagemresultado_datadmn_to3 = DateTime.MinValue;
         AV187ExtraWWContagemResultadoWWAceiteFaturamentoDS_52_Contagemresultado_dataaceite3 = DateTime.MinValue;
         AV188ExtraWWContagemResultadoWWAceiteFaturamentoDS_53_Contagemresultado_dataaceite_to3 = DateTime.MinValue;
         AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_58_Contagemresultado_loteaceite3 = "";
         AV195ExtraWWContagemResultadoWWAceiteFaturamentoDS_60_Contagemresultado_baseline3 = "";
         AV197ExtraWWContagemResultadoWWAceiteFaturamentoDS_62_Contagemresultado_descricao3 = "";
         AV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_63_Contagemresultado_agrupador3 = "";
         AV199ExtraWWContagemResultadoWWAceiteFaturamentoDS_64_Tfcontagemresultado_loteaceite = "";
         AV200ExtraWWContagemResultadoWWAceiteFaturamentoDS_65_Tfcontagemresultado_loteaceite_sel = "";
         AV201ExtraWWContagemResultadoWWAceiteFaturamentoDS_66_Tfcontagemresultado_osfsosfm = "";
         AV202ExtraWWContagemResultadoWWAceiteFaturamentoDS_67_Tfcontagemresultado_osfsosfm_sel = "";
         AV203ExtraWWContagemResultadoWWAceiteFaturamentoDS_68_Tfcontagemresultado_descricao = "";
         AV204ExtraWWContagemResultadoWWAceiteFaturamentoDS_69_Tfcontagemresultado_descricao_sel = "";
         AV205ExtraWWContagemResultadoWWAceiteFaturamentoDS_70_Tfcontagemresultado_dataaceite = (DateTime)(DateTime.MinValue);
         AV206ExtraWWContagemResultadoWWAceiteFaturamentoDS_71_Tfcontagemresultado_dataaceite_to = (DateTime)(DateTime.MinValue);
         AV207ExtraWWContagemResultadoWWAceiteFaturamentoDS_72_Tfcontagemresultado_dataultcnt = DateTime.MinValue;
         AV208ExtraWWContagemResultadoWWAceiteFaturamentoDS_73_Tfcontagemresultado_dataultcnt_to = DateTime.MinValue;
         AV209ExtraWWContagemResultadoWWAceiteFaturamentoDS_74_Tfcontagemresultado_contratadaorigemsigla = "";
         AV210ExtraWWContagemResultadoWWAceiteFaturamentoDS_75_Tfcontagemresultado_contratadaorigemsigla_sel = "";
         AV211ExtraWWContagemResultadoWWAceiteFaturamentoDS_76_Tfcontagemrresultado_sistemasigla = "";
         AV212ExtraWWContagemResultadoWWAceiteFaturamentoDS_77_Tfcontagemrresultado_sistemasigla_sel = "";
         AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_78_Tfcontagemresultado_statusdmn_sels = new GxSimpleCollection();
         AV215ExtraWWContagemResultadoWWAceiteFaturamentoDS_80_Tfcontagemresultado_servicosigla = "";
         AV216ExtraWWContagemResultadoWWAceiteFaturamentoDS_81_Tfcontagemresultado_servicosigla_sel = "";
         scmdbuf = "";
         lV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1 = "";
         lV153ExtraWWContagemResultadoWWAceiteFaturamentoDS_18_Contagemresultado_loteaceite1 = "";
         lV157ExtraWWContagemResultadoWWAceiteFaturamentoDS_22_Contagemresultado_descricao1 = "";
         lV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2 = "";
         lV173ExtraWWContagemResultadoWWAceiteFaturamentoDS_38_Contagemresultado_loteaceite2 = "";
         lV177ExtraWWContagemResultadoWWAceiteFaturamentoDS_42_Contagemresultado_descricao2 = "";
         lV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3 = "";
         lV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_58_Contagemresultado_loteaceite3 = "";
         lV197ExtraWWContagemResultadoWWAceiteFaturamentoDS_62_Contagemresultado_descricao3 = "";
         lV199ExtraWWContagemResultadoWWAceiteFaturamentoDS_64_Tfcontagemresultado_loteaceite = "";
         lV201ExtraWWContagemResultadoWWAceiteFaturamentoDS_66_Tfcontagemresultado_osfsosfm = "";
         lV203ExtraWWContagemResultadoWWAceiteFaturamentoDS_68_Tfcontagemresultado_descricao = "";
         lV209ExtraWWContagemResultadoWWAceiteFaturamentoDS_74_Tfcontagemresultado_contratadaorigemsigla = "";
         lV211ExtraWWContagemResultadoWWAceiteFaturamentoDS_76_Tfcontagemrresultado_sistemasigla = "";
         lV215ExtraWWContagemResultadoWWAceiteFaturamentoDS_80_Tfcontagemresultado_servicosigla = "";
         A484ContagemResultado_StatusDmn = "";
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A529ContagemResultado_DataAceite = (DateTime)(DateTime.MinValue);
         A528ContagemResultado_LoteAceite = "";
         A494ContagemResultado_Descricao = "";
         A1046ContagemResultado_Agrupador = "";
         A866ContagemResultado_ContratadaOrigemSigla = "";
         A509ContagemrResultado_SistemaSigla = "";
         A801ContagemResultado_ServicoSigla = "";
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         P003V3_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         P003V3_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         P003V3_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P003V3_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P003V3_A1583ContagemResultado_TipoRegistro = new short[1] ;
         P003V3_A517ContagemResultado_Ultima = new bool[] {false} ;
         P003V3_A512ContagemResultado_ValorPF = new decimal[1] ;
         P003V3_n512ContagemResultado_ValorPF = new bool[] {false} ;
         P003V3_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P003V3_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P003V3_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P003V3_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P003V3_A866ContagemResultado_ContratadaOrigemSigla = new String[] {""} ;
         P003V3_n866ContagemResultado_ContratadaOrigemSigla = new bool[] {false} ;
         P003V3_A1046ContagemResultado_Agrupador = new String[] {""} ;
         P003V3_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         P003V3_A494ContagemResultado_Descricao = new String[] {""} ;
         P003V3_n494ContagemResultado_Descricao = new bool[] {false} ;
         P003V3_A601ContagemResultado_Servico = new int[1] ;
         P003V3_n601ContagemResultado_Servico = new bool[] {false} ;
         P003V3_A598ContagemResultado_Baseline = new bool[] {false} ;
         P003V3_n598ContagemResultado_Baseline = new bool[] {false} ;
         P003V3_A528ContagemResultado_LoteAceite = new String[] {""} ;
         P003V3_n528ContagemResultado_LoteAceite = new bool[] {false} ;
         P003V3_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         P003V3_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         P003V3_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         P003V3_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         P003V3_A489ContagemResultado_SistemaCod = new int[1] ;
         P003V3_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P003V3_A529ContagemResultado_DataAceite = new DateTime[] {DateTime.MinValue} ;
         P003V3_n529ContagemResultado_DataAceite = new bool[] {false} ;
         P003V3_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P003V3_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P003V3_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P003V3_A490ContagemResultado_ContratadaCod = new int[1] ;
         P003V3_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P003V3_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P003V3_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P003V3_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         P003V3_A531ContagemResultado_StatusUltCnt = new short[1] ;
         P003V3_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P003V3_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P003V3_A457ContagemResultado_Demanda = new String[] {""} ;
         P003V3_n457ContagemResultado_Demanda = new bool[] {false} ;
         P003V3_A456ContagemResultado_Codigo = new int[1] ;
         P003V3_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P003V3_A511ContagemResultado_HoraCnt = new String[] {""} ;
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         A501ContagemResultado_OsFsOsFm = "";
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.aexportextrawwcontagemresultadowwaceitefaturamento__default(),
            new Object[][] {
                new Object[] {
               P003V3_A597ContagemResultado_LoteAceiteCod, P003V3_n597ContagemResultado_LoteAceiteCod, P003V3_A1553ContagemResultado_CntSrvCod, P003V3_n1553ContagemResultado_CntSrvCod, P003V3_A1583ContagemResultado_TipoRegistro, P003V3_A517ContagemResultado_Ultima, P003V3_A512ContagemResultado_ValorPF, P003V3_n512ContagemResultado_ValorPF, P003V3_A801ContagemResultado_ServicoSigla, P003V3_n801ContagemResultado_ServicoSigla,
               P003V3_A509ContagemrResultado_SistemaSigla, P003V3_n509ContagemrResultado_SistemaSigla, P003V3_A866ContagemResultado_ContratadaOrigemSigla, P003V3_n866ContagemResultado_ContratadaOrigemSigla, P003V3_A1046ContagemResultado_Agrupador, P003V3_n1046ContagemResultado_Agrupador, P003V3_A494ContagemResultado_Descricao, P003V3_n494ContagemResultado_Descricao, P003V3_A601ContagemResultado_Servico, P003V3_n601ContagemResultado_Servico,
               P003V3_A598ContagemResultado_Baseline, P003V3_n598ContagemResultado_Baseline, P003V3_A528ContagemResultado_LoteAceite, P003V3_n528ContagemResultado_LoteAceite, P003V3_A468ContagemResultado_NaoCnfDmnCod, P003V3_n468ContagemResultado_NaoCnfDmnCod, P003V3_A805ContagemResultado_ContratadaOrigemCod, P003V3_n805ContagemResultado_ContratadaOrigemCod, P003V3_A489ContagemResultado_SistemaCod, P003V3_n489ContagemResultado_SistemaCod,
               P003V3_A529ContagemResultado_DataAceite, P003V3_n529ContagemResultado_DataAceite, P003V3_A471ContagemResultado_DataDmn, P003V3_A484ContagemResultado_StatusDmn, P003V3_n484ContagemResultado_StatusDmn, P003V3_A490ContagemResultado_ContratadaCod, P003V3_n490ContagemResultado_ContratadaCod, P003V3_A52Contratada_AreaTrabalhoCod, P003V3_n52Contratada_AreaTrabalhoCod, P003V3_A566ContagemResultado_DataUltCnt,
               P003V3_A531ContagemResultado_StatusUltCnt, P003V3_A493ContagemResultado_DemandaFM, P003V3_n493ContagemResultado_DemandaFM, P003V3_A457ContagemResultado_Demanda, P003V3_n457ContagemResultado_Demanda, P003V3_A456ContagemResultado_Codigo, P003V3_A473ContagemResultado_DataCnt, P003V3_A511ContagemResultado_HoraCnt
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV19ContagemResultado_StatusCnt ;
      private short AV98TFContagemResultado_Baseline_Sel ;
      private short AV16OrderedBy ;
      private short AV48DynamicFiltersOperator1 ;
      private short AV51DynamicFiltersOperator2 ;
      private short AV54DynamicFiltersOperator3 ;
      private short AV138ExtraWWContagemResultadoWWAceiteFaturamentoDS_3_Contagemresultado_statuscnt ;
      private short AV141ExtraWWContagemResultadoWWAceiteFaturamentoDS_6_Dynamicfiltersoperator1 ;
      private short AV161ExtraWWContagemResultadoWWAceiteFaturamentoDS_26_Dynamicfiltersoperator2 ;
      private short AV181ExtraWWContagemResultadoWWAceiteFaturamentoDS_46_Dynamicfiltersoperator3 ;
      private short AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_79_Tfcontagemresultado_baseline_sel ;
      private short A531ContagemResultado_StatusUltCnt ;
      private short A1583ContagemResultado_TipoRegistro ;
      private int AV18Contratada_AreaTrabalhoCod ;
      private int AV72Contratada_Codigo ;
      private int AV13CellRow ;
      private int AV14FirstColumn ;
      private int AV15Random ;
      private int AV26ContagemResultado_SistemaCod1 ;
      private int AV27ContagemResultado_ContratadaCod1 ;
      private int AV118ContagemResultado_ContratadaOrigemCod1 ;
      private int AV28ContagemResultado_NaoCnfDmnCod1 ;
      private int AV63ContagemResultado_Servico1 ;
      private int AV34ContagemResultado_SistemaCod2 ;
      private int AV35ContagemResultado_ContratadaCod2 ;
      private int AV124ContagemResultado_ContratadaOrigemCod2 ;
      private int AV36ContagemResultado_NaoCnfDmnCod2 ;
      private int AV64ContagemResultado_Servico2 ;
      private int AV42ContagemResultado_SistemaCod3 ;
      private int AV43ContagemResultado_ContratadaCod3 ;
      private int AV130ContagemResultado_ContratadaOrigemCod3 ;
      private int AV44ContagemResultado_NaoCnfDmnCod3 ;
      private int AV65ContagemResultado_Servico3 ;
      private int AV134GXV1 ;
      private int AV136ExtraWWContagemResultadoWWAceiteFaturamentoDS_1_Contratada_areatrabalhocod ;
      private int AV137ExtraWWContagemResultadoWWAceiteFaturamentoDS_2_Contratada_codigo ;
      private int AV149ExtraWWContagemResultadoWWAceiteFaturamentoDS_14_Contagemresultado_sistemacod1 ;
      private int AV150ExtraWWContagemResultadoWWAceiteFaturamentoDS_15_Contagemresultado_contratadacod1 ;
      private int AV151ExtraWWContagemResultadoWWAceiteFaturamentoDS_16_Contagemresultado_contratadaorigemcod1 ;
      private int AV152ExtraWWContagemResultadoWWAceiteFaturamentoDS_17_Contagemresultado_naocnfdmncod1 ;
      private int AV156ExtraWWContagemResultadoWWAceiteFaturamentoDS_21_Contagemresultado_servico1 ;
      private int AV169ExtraWWContagemResultadoWWAceiteFaturamentoDS_34_Contagemresultado_sistemacod2 ;
      private int AV170ExtraWWContagemResultadoWWAceiteFaturamentoDS_35_Contagemresultado_contratadacod2 ;
      private int AV171ExtraWWContagemResultadoWWAceiteFaturamentoDS_36_Contagemresultado_contratadaorigemcod2 ;
      private int AV172ExtraWWContagemResultadoWWAceiteFaturamentoDS_37_Contagemresultado_naocnfdmncod2 ;
      private int AV176ExtraWWContagemResultadoWWAceiteFaturamentoDS_41_Contagemresultado_servico2 ;
      private int AV189ExtraWWContagemResultadoWWAceiteFaturamentoDS_54_Contagemresultado_sistemacod3 ;
      private int AV190ExtraWWContagemResultadoWWAceiteFaturamentoDS_55_Contagemresultado_contratadacod3 ;
      private int AV191ExtraWWContagemResultadoWWAceiteFaturamentoDS_56_Contagemresultado_contratadaorigemcod3 ;
      private int AV192ExtraWWContagemResultadoWWAceiteFaturamentoDS_57_Contagemresultado_naocnfdmncod3 ;
      private int AV196ExtraWWContagemResultadoWWAceiteFaturamentoDS_61_Contagemresultado_servico3 ;
      private int AV9WWPContext_gxTpr_Contratada_codigo ;
      private int AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_78_Tfcontagemresultado_statusdmn_sels_Count ;
      private int AV46GridState_gxTpr_Dynamicfilters_Count ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A489ContagemResultado_SistemaCod ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A601ContagemResultado_Servico ;
      private int A456ContagemResultado_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private long AV50ContagemResultado_DemandaFM_ORDER1 ;
      private long AV53ContagemResultado_DemandaFM_ORDER2 ;
      private long AV56ContagemResultado_DemandaFM_ORDER3 ;
      private long AV113i ;
      private long AV154ExtraWWContagemResultadoWWAceiteFaturamentoDS_19_Contagemresultado_demandafm_order1 ;
      private long AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_39_Contagemresultado_demandafm_order2 ;
      private long AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_59_Contagemresultado_demandafm_order3 ;
      private long A553ContagemResultado_DemandaFM_ORDER ;
      private decimal AV109TFContagemResultado_PFFinal ;
      private decimal AV110TFContagemResultado_PFFinal_To ;
      private decimal AV111TFContagemResultado_ValorPF ;
      private decimal AV112TFContagemResultado_ValorPF_To ;
      private decimal AV217ExtraWWContagemResultadoWWAceiteFaturamentoDS_82_Tfcontagemresultado_pffinal ;
      private decimal AV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_83_Tfcontagemresultado_pffinal_to ;
      private decimal AV219ExtraWWContagemResultadoWWAceiteFaturamentoDS_84_Tfcontagemresultado_valorpf ;
      private decimal AV220ExtraWWContagemResultadoWWAceiteFaturamentoDS_85_Tfcontagemresultado_valorpf_to ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal GXt_decimal2 ;
      private String AV20ContagemResultado_StatusDmn ;
      private String AV81TFContagemResultado_LoteAceite ;
      private String AV82TFContagemResultado_LoteAceite_Sel ;
      private String AV91TFContagemResultado_ContratadaOrigemSigla ;
      private String AV92TFContagemResultado_ContratadaOrigemSigla_Sel ;
      private String AV93TFContagemrResultado_SistemaSigla ;
      private String AV94TFContagemrResultado_SistemaSigla_Sel ;
      private String AV99TFContagemResultado_ServicoSigla ;
      private String AV100TFContagemResultado_ServicoSigla_Sel ;
      private String AV49ContagemResultado_LoteAceite1 ;
      private String AV57ContagemResultado_Baseline1 ;
      private String AV119ContagemResultado_Agrupador1 ;
      private String AV52ContagemResultado_LoteAceite2 ;
      private String AV58ContagemResultado_Baseline2 ;
      private String AV125ContagemResultado_Agrupador2 ;
      private String AV55ContagemResultado_LoteAceite3 ;
      private String AV59ContagemResultado_Baseline3 ;
      private String AV131ContagemResultado_Agrupador3 ;
      private String AV97TFContagemResultado_StatusDmn_Sel ;
      private String AV139ExtraWWContagemResultadoWWAceiteFaturamentoDS_4_Contagemresultado_statusdmn ;
      private String AV153ExtraWWContagemResultadoWWAceiteFaturamentoDS_18_Contagemresultado_loteaceite1 ;
      private String AV155ExtraWWContagemResultadoWWAceiteFaturamentoDS_20_Contagemresultado_baseline1 ;
      private String AV158ExtraWWContagemResultadoWWAceiteFaturamentoDS_23_Contagemresultado_agrupador1 ;
      private String AV173ExtraWWContagemResultadoWWAceiteFaturamentoDS_38_Contagemresultado_loteaceite2 ;
      private String AV175ExtraWWContagemResultadoWWAceiteFaturamentoDS_40_Contagemresultado_baseline2 ;
      private String AV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_43_Contagemresultado_agrupador2 ;
      private String AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_58_Contagemresultado_loteaceite3 ;
      private String AV195ExtraWWContagemResultadoWWAceiteFaturamentoDS_60_Contagemresultado_baseline3 ;
      private String AV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_63_Contagemresultado_agrupador3 ;
      private String AV199ExtraWWContagemResultadoWWAceiteFaturamentoDS_64_Tfcontagemresultado_loteaceite ;
      private String AV200ExtraWWContagemResultadoWWAceiteFaturamentoDS_65_Tfcontagemresultado_loteaceite_sel ;
      private String AV209ExtraWWContagemResultadoWWAceiteFaturamentoDS_74_Tfcontagemresultado_contratadaorigemsigla ;
      private String AV210ExtraWWContagemResultadoWWAceiteFaturamentoDS_75_Tfcontagemresultado_contratadaorigemsigla_sel ;
      private String AV211ExtraWWContagemResultadoWWAceiteFaturamentoDS_76_Tfcontagemrresultado_sistemasigla ;
      private String AV212ExtraWWContagemResultadoWWAceiteFaturamentoDS_77_Tfcontagemrresultado_sistemasigla_sel ;
      private String AV215ExtraWWContagemResultadoWWAceiteFaturamentoDS_80_Tfcontagemresultado_servicosigla ;
      private String AV216ExtraWWContagemResultadoWWAceiteFaturamentoDS_81_Tfcontagemresultado_servicosigla_sel ;
      private String scmdbuf ;
      private String lV153ExtraWWContagemResultadoWWAceiteFaturamentoDS_18_Contagemresultado_loteaceite1 ;
      private String lV173ExtraWWContagemResultadoWWAceiteFaturamentoDS_38_Contagemresultado_loteaceite2 ;
      private String lV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_58_Contagemresultado_loteaceite3 ;
      private String lV199ExtraWWContagemResultadoWWAceiteFaturamentoDS_64_Tfcontagemresultado_loteaceite ;
      private String lV209ExtraWWContagemResultadoWWAceiteFaturamentoDS_74_Tfcontagemresultado_contratadaorigemsigla ;
      private String lV211ExtraWWContagemResultadoWWAceiteFaturamentoDS_76_Tfcontagemrresultado_sistemasigla ;
      private String lV215ExtraWWContagemResultadoWWAceiteFaturamentoDS_80_Tfcontagemresultado_servicosigla ;
      private String A484ContagemResultado_StatusDmn ;
      private String A528ContagemResultado_LoteAceite ;
      private String A1046ContagemResultado_Agrupador ;
      private String A866ContagemResultado_ContratadaOrigemSigla ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String A801ContagemResultado_ServicoSigla ;
      private String A511ContagemResultado_HoraCnt ;
      private DateTime AV87TFContagemResultado_DataAceite ;
      private DateTime AV88TFContagemResultado_DataAceite_To ;
      private DateTime AV205ExtraWWContagemResultadoWWAceiteFaturamentoDS_70_Tfcontagemresultado_dataaceite ;
      private DateTime AV206ExtraWWContagemResultadoWWAceiteFaturamentoDS_71_Tfcontagemresultado_dataaceite_to ;
      private DateTime A529ContagemResultado_DataAceite ;
      private DateTime GXt_dtime1 ;
      private DateTime AV89TFContagemResultado_DataUltCnt ;
      private DateTime AV90TFContagemResultado_DataUltCnt_To ;
      private DateTime AV66ContagemResultado_DataUltCnt1 ;
      private DateTime AV67ContagemResultado_DataUltCnt_To1 ;
      private DateTime AV114ContagemResultado_DataDmn1 ;
      private DateTime AV115ContagemResultado_DataDmn_To1 ;
      private DateTime AV116ContagemResultado_DataAceite1 ;
      private DateTime AV117ContagemResultado_DataAceite_To1 ;
      private DateTime AV68ContagemResultado_DataUltCnt2 ;
      private DateTime AV69ContagemResultado_DataUltCnt_To2 ;
      private DateTime AV120ContagemResultado_DataDmn2 ;
      private DateTime AV121ContagemResultado_DataDmn_To2 ;
      private DateTime AV122ContagemResultado_DataAceite2 ;
      private DateTime AV123ContagemResultado_DataAceite_To2 ;
      private DateTime AV70ContagemResultado_DataUltCnt3 ;
      private DateTime AV71ContagemResultado_DataUltCnt_To3 ;
      private DateTime AV126ContagemResultado_DataDmn3 ;
      private DateTime AV127ContagemResultado_DataDmn_To3 ;
      private DateTime AV128ContagemResultado_DataAceite3 ;
      private DateTime AV129ContagemResultado_DataAceite_To3 ;
      private DateTime AV142ExtraWWContagemResultadoWWAceiteFaturamentoDS_7_Contagemresultado_dataultcnt1 ;
      private DateTime AV143ExtraWWContagemResultadoWWAceiteFaturamentoDS_8_Contagemresultado_dataultcnt_to1 ;
      private DateTime AV145ExtraWWContagemResultadoWWAceiteFaturamentoDS_10_Contagemresultado_datadmn1 ;
      private DateTime AV146ExtraWWContagemResultadoWWAceiteFaturamentoDS_11_Contagemresultado_datadmn_to1 ;
      private DateTime AV147ExtraWWContagemResultadoWWAceiteFaturamentoDS_12_Contagemresultado_dataaceite1 ;
      private DateTime AV148ExtraWWContagemResultadoWWAceiteFaturamentoDS_13_Contagemresultado_dataaceite_to1 ;
      private DateTime AV162ExtraWWContagemResultadoWWAceiteFaturamentoDS_27_Contagemresultado_dataultcnt2 ;
      private DateTime AV163ExtraWWContagemResultadoWWAceiteFaturamentoDS_28_Contagemresultado_dataultcnt_to2 ;
      private DateTime AV165ExtraWWContagemResultadoWWAceiteFaturamentoDS_30_Contagemresultado_datadmn2 ;
      private DateTime AV166ExtraWWContagemResultadoWWAceiteFaturamentoDS_31_Contagemresultado_datadmn_to2 ;
      private DateTime AV167ExtraWWContagemResultadoWWAceiteFaturamentoDS_32_Contagemresultado_dataaceite2 ;
      private DateTime AV168ExtraWWContagemResultadoWWAceiteFaturamentoDS_33_Contagemresultado_dataaceite_to2 ;
      private DateTime AV182ExtraWWContagemResultadoWWAceiteFaturamentoDS_47_Contagemresultado_dataultcnt3 ;
      private DateTime AV183ExtraWWContagemResultadoWWAceiteFaturamentoDS_48_Contagemresultado_dataultcnt_to3 ;
      private DateTime AV185ExtraWWContagemResultadoWWAceiteFaturamentoDS_50_Contagemresultado_datadmn3 ;
      private DateTime AV186ExtraWWContagemResultadoWWAceiteFaturamentoDS_51_Contagemresultado_datadmn_to3 ;
      private DateTime AV187ExtraWWContagemResultadoWWAceiteFaturamentoDS_52_Contagemresultado_dataaceite3 ;
      private DateTime AV188ExtraWWContagemResultadoWWAceiteFaturamentoDS_53_Contagemresultado_dataaceite_to3 ;
      private DateTime AV207ExtraWWContagemResultadoWWAceiteFaturamentoDS_72_Tfcontagemresultado_dataultcnt ;
      private DateTime AV208ExtraWWContagemResultadoWWAceiteFaturamentoDS_73_Tfcontagemresultado_dataultcnt_to ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool returnInSub ;
      private bool AV29DynamicFiltersEnabled2 ;
      private bool AV37DynamicFiltersEnabled3 ;
      private bool AV159ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 ;
      private bool AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 ;
      private bool A598ContagemResultado_Baseline ;
      private bool A517ContagemResultado_Ultima ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n512ContagemResultado_ValorPF ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n866ContagemResultado_ContratadaOrigemSigla ;
      private bool n1046ContagemResultado_Agrupador ;
      private bool n494ContagemResultado_Descricao ;
      private bool n601ContagemResultado_Servico ;
      private bool n598ContagemResultado_Baseline ;
      private bool n528ContagemResultado_LoteAceite ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n529ContagemResultado_DataAceite ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n457ContagemResultado_Demanda ;
      private String AV95TFContagemResultado_StatusDmn_SelsJson ;
      private String AV45GridStateXML ;
      private String AV83TFContagemResultado_OsFsOsFm ;
      private String AV84TFContagemResultado_OsFsOsFm_Sel ;
      private String AV85TFContagemResultado_Descricao ;
      private String AV86TFContagemResultado_Descricao_Sel ;
      private String AV12ErrorMessage ;
      private String AV11Filename ;
      private String AV22DynamicFiltersSelector1 ;
      private String AV25ContagemResultado_OsFsOsFm1 ;
      private String AV73ContagemResultado_Descricao1 ;
      private String AV30DynamicFiltersSelector2 ;
      private String AV33ContagemResultado_OsFsOsFm2 ;
      private String AV74ContagemResultado_Descricao2 ;
      private String AV38DynamicFiltersSelector3 ;
      private String AV41ContagemResultado_OsFsOsFm3 ;
      private String AV75ContagemResultado_Descricao3 ;
      private String AV140ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1 ;
      private String AV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1 ;
      private String AV157ExtraWWContagemResultadoWWAceiteFaturamentoDS_22_Contagemresultado_descricao1 ;
      private String AV160ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2 ;
      private String AV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2 ;
      private String AV177ExtraWWContagemResultadoWWAceiteFaturamentoDS_42_Contagemresultado_descricao2 ;
      private String AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3 ;
      private String AV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3 ;
      private String AV197ExtraWWContagemResultadoWWAceiteFaturamentoDS_62_Contagemresultado_descricao3 ;
      private String AV201ExtraWWContagemResultadoWWAceiteFaturamentoDS_66_Tfcontagemresultado_osfsosfm ;
      private String AV202ExtraWWContagemResultadoWWAceiteFaturamentoDS_67_Tfcontagemresultado_osfsosfm_sel ;
      private String AV203ExtraWWContagemResultadoWWAceiteFaturamentoDS_68_Tfcontagemresultado_descricao ;
      private String AV204ExtraWWContagemResultadoWWAceiteFaturamentoDS_69_Tfcontagemresultado_descricao_sel ;
      private String lV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1 ;
      private String lV157ExtraWWContagemResultadoWWAceiteFaturamentoDS_22_Contagemresultado_descricao1 ;
      private String lV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2 ;
      private String lV177ExtraWWContagemResultadoWWAceiteFaturamentoDS_42_Contagemresultado_descricao2 ;
      private String lV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3 ;
      private String lV197ExtraWWContagemResultadoWWAceiteFaturamentoDS_62_Contagemresultado_descricao3 ;
      private String lV201ExtraWWContagemResultadoWWAceiteFaturamentoDS_66_Tfcontagemresultado_osfsosfm ;
      private String lV203ExtraWWContagemResultadoWWAceiteFaturamentoDS_68_Tfcontagemresultado_descricao ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A494ContagemResultado_Descricao ;
      private String A501ContagemResultado_OsFsOsFm ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P003V3_A597ContagemResultado_LoteAceiteCod ;
      private bool[] P003V3_n597ContagemResultado_LoteAceiteCod ;
      private int[] P003V3_A1553ContagemResultado_CntSrvCod ;
      private bool[] P003V3_n1553ContagemResultado_CntSrvCod ;
      private short[] P003V3_A1583ContagemResultado_TipoRegistro ;
      private bool[] P003V3_A517ContagemResultado_Ultima ;
      private decimal[] P003V3_A512ContagemResultado_ValorPF ;
      private bool[] P003V3_n512ContagemResultado_ValorPF ;
      private String[] P003V3_A801ContagemResultado_ServicoSigla ;
      private bool[] P003V3_n801ContagemResultado_ServicoSigla ;
      private String[] P003V3_A509ContagemrResultado_SistemaSigla ;
      private bool[] P003V3_n509ContagemrResultado_SistemaSigla ;
      private String[] P003V3_A866ContagemResultado_ContratadaOrigemSigla ;
      private bool[] P003V3_n866ContagemResultado_ContratadaOrigemSigla ;
      private String[] P003V3_A1046ContagemResultado_Agrupador ;
      private bool[] P003V3_n1046ContagemResultado_Agrupador ;
      private String[] P003V3_A494ContagemResultado_Descricao ;
      private bool[] P003V3_n494ContagemResultado_Descricao ;
      private int[] P003V3_A601ContagemResultado_Servico ;
      private bool[] P003V3_n601ContagemResultado_Servico ;
      private bool[] P003V3_A598ContagemResultado_Baseline ;
      private bool[] P003V3_n598ContagemResultado_Baseline ;
      private String[] P003V3_A528ContagemResultado_LoteAceite ;
      private bool[] P003V3_n528ContagemResultado_LoteAceite ;
      private int[] P003V3_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] P003V3_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] P003V3_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] P003V3_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] P003V3_A489ContagemResultado_SistemaCod ;
      private bool[] P003V3_n489ContagemResultado_SistemaCod ;
      private DateTime[] P003V3_A529ContagemResultado_DataAceite ;
      private bool[] P003V3_n529ContagemResultado_DataAceite ;
      private DateTime[] P003V3_A471ContagemResultado_DataDmn ;
      private String[] P003V3_A484ContagemResultado_StatusDmn ;
      private bool[] P003V3_n484ContagemResultado_StatusDmn ;
      private int[] P003V3_A490ContagemResultado_ContratadaCod ;
      private bool[] P003V3_n490ContagemResultado_ContratadaCod ;
      private int[] P003V3_A52Contratada_AreaTrabalhoCod ;
      private bool[] P003V3_n52Contratada_AreaTrabalhoCod ;
      private DateTime[] P003V3_A566ContagemResultado_DataUltCnt ;
      private short[] P003V3_A531ContagemResultado_StatusUltCnt ;
      private String[] P003V3_A493ContagemResultado_DemandaFM ;
      private bool[] P003V3_n493ContagemResultado_DemandaFM ;
      private String[] P003V3_A457ContagemResultado_Demanda ;
      private bool[] P003V3_n457ContagemResultado_Demanda ;
      private int[] P003V3_A456ContagemResultado_Codigo ;
      private DateTime[] P003V3_A473ContagemResultado_DataCnt ;
      private String[] P003V3_A511ContagemResultado_HoraCnt ;
      private String aP28_Filename ;
      private String aP29_ErrorMessage ;
      private ExcelDocumentI AV10ExcelDocument ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV96TFContagemResultado_StatusDmn_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_78_Tfcontagemresultado_statusdmn_sels ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV46GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV47GridStateDynamicFilter ;
   }

   public class aexportextrawwcontagemresultadowwaceitefaturamento__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P003V3( IGxContext context ,
                                             String A484ContagemResultado_StatusDmn ,
                                             IGxCollection AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_78_Tfcontagemresultado_statusdmn_sels ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             String AV140ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1 ,
                                             short AV141ExtraWWContagemResultadoWWAceiteFaturamentoDS_6_Dynamicfiltersoperator1 ,
                                             String AV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1 ,
                                             DateTime AV145ExtraWWContagemResultadoWWAceiteFaturamentoDS_10_Contagemresultado_datadmn1 ,
                                             DateTime AV146ExtraWWContagemResultadoWWAceiteFaturamentoDS_11_Contagemresultado_datadmn_to1 ,
                                             DateTime AV147ExtraWWContagemResultadoWWAceiteFaturamentoDS_12_Contagemresultado_dataaceite1 ,
                                             DateTime AV148ExtraWWContagemResultadoWWAceiteFaturamentoDS_13_Contagemresultado_dataaceite_to1 ,
                                             int AV149ExtraWWContagemResultadoWWAceiteFaturamentoDS_14_Contagemresultado_sistemacod1 ,
                                             int AV150ExtraWWContagemResultadoWWAceiteFaturamentoDS_15_Contagemresultado_contratadacod1 ,
                                             int AV136ExtraWWContagemResultadoWWAceiteFaturamentoDS_1_Contratada_areatrabalhocod ,
                                             int AV151ExtraWWContagemResultadoWWAceiteFaturamentoDS_16_Contagemresultado_contratadaorigemcod1 ,
                                             int AV152ExtraWWContagemResultadoWWAceiteFaturamentoDS_17_Contagemresultado_naocnfdmncod1 ,
                                             String AV153ExtraWWContagemResultadoWWAceiteFaturamentoDS_18_Contagemresultado_loteaceite1 ,
                                             long AV154ExtraWWContagemResultadoWWAceiteFaturamentoDS_19_Contagemresultado_demandafm_order1 ,
                                             String AV155ExtraWWContagemResultadoWWAceiteFaturamentoDS_20_Contagemresultado_baseline1 ,
                                             int AV156ExtraWWContagemResultadoWWAceiteFaturamentoDS_21_Contagemresultado_servico1 ,
                                             String AV157ExtraWWContagemResultadoWWAceiteFaturamentoDS_22_Contagemresultado_descricao1 ,
                                             String AV158ExtraWWContagemResultadoWWAceiteFaturamentoDS_23_Contagemresultado_agrupador1 ,
                                             bool AV159ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 ,
                                             String AV160ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2 ,
                                             short AV161ExtraWWContagemResultadoWWAceiteFaturamentoDS_26_Dynamicfiltersoperator2 ,
                                             String AV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2 ,
                                             DateTime AV165ExtraWWContagemResultadoWWAceiteFaturamentoDS_30_Contagemresultado_datadmn2 ,
                                             DateTime AV166ExtraWWContagemResultadoWWAceiteFaturamentoDS_31_Contagemresultado_datadmn_to2 ,
                                             DateTime AV167ExtraWWContagemResultadoWWAceiteFaturamentoDS_32_Contagemresultado_dataaceite2 ,
                                             DateTime AV168ExtraWWContagemResultadoWWAceiteFaturamentoDS_33_Contagemresultado_dataaceite_to2 ,
                                             int AV169ExtraWWContagemResultadoWWAceiteFaturamentoDS_34_Contagemresultado_sistemacod2 ,
                                             int AV170ExtraWWContagemResultadoWWAceiteFaturamentoDS_35_Contagemresultado_contratadacod2 ,
                                             int AV171ExtraWWContagemResultadoWWAceiteFaturamentoDS_36_Contagemresultado_contratadaorigemcod2 ,
                                             int AV172ExtraWWContagemResultadoWWAceiteFaturamentoDS_37_Contagemresultado_naocnfdmncod2 ,
                                             String AV173ExtraWWContagemResultadoWWAceiteFaturamentoDS_38_Contagemresultado_loteaceite2 ,
                                             long AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_39_Contagemresultado_demandafm_order2 ,
                                             String AV175ExtraWWContagemResultadoWWAceiteFaturamentoDS_40_Contagemresultado_baseline2 ,
                                             int AV176ExtraWWContagemResultadoWWAceiteFaturamentoDS_41_Contagemresultado_servico2 ,
                                             String AV177ExtraWWContagemResultadoWWAceiteFaturamentoDS_42_Contagemresultado_descricao2 ,
                                             String AV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_43_Contagemresultado_agrupador2 ,
                                             bool AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 ,
                                             String AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3 ,
                                             short AV181ExtraWWContagemResultadoWWAceiteFaturamentoDS_46_Dynamicfiltersoperator3 ,
                                             String AV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3 ,
                                             DateTime AV185ExtraWWContagemResultadoWWAceiteFaturamentoDS_50_Contagemresultado_datadmn3 ,
                                             DateTime AV186ExtraWWContagemResultadoWWAceiteFaturamentoDS_51_Contagemresultado_datadmn_to3 ,
                                             DateTime AV187ExtraWWContagemResultadoWWAceiteFaturamentoDS_52_Contagemresultado_dataaceite3 ,
                                             DateTime AV188ExtraWWContagemResultadoWWAceiteFaturamentoDS_53_Contagemresultado_dataaceite_to3 ,
                                             int AV189ExtraWWContagemResultadoWWAceiteFaturamentoDS_54_Contagemresultado_sistemacod3 ,
                                             int AV190ExtraWWContagemResultadoWWAceiteFaturamentoDS_55_Contagemresultado_contratadacod3 ,
                                             int AV191ExtraWWContagemResultadoWWAceiteFaturamentoDS_56_Contagemresultado_contratadaorigemcod3 ,
                                             int AV192ExtraWWContagemResultadoWWAceiteFaturamentoDS_57_Contagemresultado_naocnfdmncod3 ,
                                             String AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_58_Contagemresultado_loteaceite3 ,
                                             long AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_59_Contagemresultado_demandafm_order3 ,
                                             String AV195ExtraWWContagemResultadoWWAceiteFaturamentoDS_60_Contagemresultado_baseline3 ,
                                             int AV196ExtraWWContagemResultadoWWAceiteFaturamentoDS_61_Contagemresultado_servico3 ,
                                             String AV197ExtraWWContagemResultadoWWAceiteFaturamentoDS_62_Contagemresultado_descricao3 ,
                                             String AV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_63_Contagemresultado_agrupador3 ,
                                             String AV200ExtraWWContagemResultadoWWAceiteFaturamentoDS_65_Tfcontagemresultado_loteaceite_sel ,
                                             String AV199ExtraWWContagemResultadoWWAceiteFaturamentoDS_64_Tfcontagemresultado_loteaceite ,
                                             String AV202ExtraWWContagemResultadoWWAceiteFaturamentoDS_67_Tfcontagemresultado_osfsosfm_sel ,
                                             String AV201ExtraWWContagemResultadoWWAceiteFaturamentoDS_66_Tfcontagemresultado_osfsosfm ,
                                             String AV204ExtraWWContagemResultadoWWAceiteFaturamentoDS_69_Tfcontagemresultado_descricao_sel ,
                                             String AV203ExtraWWContagemResultadoWWAceiteFaturamentoDS_68_Tfcontagemresultado_descricao ,
                                             DateTime AV205ExtraWWContagemResultadoWWAceiteFaturamentoDS_70_Tfcontagemresultado_dataaceite ,
                                             DateTime AV206ExtraWWContagemResultadoWWAceiteFaturamentoDS_71_Tfcontagemresultado_dataaceite_to ,
                                             String AV210ExtraWWContagemResultadoWWAceiteFaturamentoDS_75_Tfcontagemresultado_contratadaorigemsigla_sel ,
                                             String AV209ExtraWWContagemResultadoWWAceiteFaturamentoDS_74_Tfcontagemresultado_contratadaorigemsigla ,
                                             String AV212ExtraWWContagemResultadoWWAceiteFaturamentoDS_77_Tfcontagemrresultado_sistemasigla_sel ,
                                             String AV211ExtraWWContagemResultadoWWAceiteFaturamentoDS_76_Tfcontagemrresultado_sistemasigla ,
                                             int AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_78_Tfcontagemresultado_statusdmn_sels_Count ,
                                             short AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_79_Tfcontagemresultado_baseline_sel ,
                                             String AV216ExtraWWContagemResultadoWWAceiteFaturamentoDS_81_Tfcontagemresultado_servicosigla_sel ,
                                             String AV215ExtraWWContagemResultadoWWAceiteFaturamentoDS_80_Tfcontagemresultado_servicosigla ,
                                             decimal AV219ExtraWWContagemResultadoWWAceiteFaturamentoDS_84_Tfcontagemresultado_valorpf ,
                                             decimal AV220ExtraWWContagemResultadoWWAceiteFaturamentoDS_85_Tfcontagemresultado_valorpf_to ,
                                             int AV46GridState_gxTpr_Dynamicfilters_Count ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             DateTime A529ContagemResultado_DataAceite ,
                                             int A489ContagemResultado_SistemaCod ,
                                             int A805ContagemResultado_ContratadaOrigemCod ,
                                             int A468ContagemResultado_NaoCnfDmnCod ,
                                             String A528ContagemResultado_LoteAceite ,
                                             bool A598ContagemResultado_Baseline ,
                                             int A601ContagemResultado_Servico ,
                                             String A494ContagemResultado_Descricao ,
                                             String A1046ContagemResultado_Agrupador ,
                                             String A866ContagemResultado_ContratadaOrigemSigla ,
                                             String A509ContagemrResultado_SistemaSigla ,
                                             String A801ContagemResultado_ServicoSigla ,
                                             decimal A512ContagemResultado_ValorPF ,
                                             int A456ContagemResultado_Codigo ,
                                             short AV16OrderedBy ,
                                             DateTime AV142ExtraWWContagemResultadoWWAceiteFaturamentoDS_7_Contagemresultado_dataultcnt1 ,
                                             DateTime A566ContagemResultado_DataUltCnt ,
                                             DateTime AV143ExtraWWContagemResultadoWWAceiteFaturamentoDS_8_Contagemresultado_dataultcnt_to1 ,
                                             DateTime AV162ExtraWWContagemResultadoWWAceiteFaturamentoDS_27_Contagemresultado_dataultcnt2 ,
                                             DateTime AV163ExtraWWContagemResultadoWWAceiteFaturamentoDS_28_Contagemresultado_dataultcnt_to2 ,
                                             DateTime AV182ExtraWWContagemResultadoWWAceiteFaturamentoDS_47_Contagemresultado_dataultcnt3 ,
                                             DateTime AV183ExtraWWContagemResultadoWWAceiteFaturamentoDS_48_Contagemresultado_dataultcnt_to3 ,
                                             DateTime AV207ExtraWWContagemResultadoWWAceiteFaturamentoDS_72_Tfcontagemresultado_dataultcnt ,
                                             DateTime AV208ExtraWWContagemResultadoWWAceiteFaturamentoDS_73_Tfcontagemresultado_dataultcnt_to ,
                                             decimal AV217ExtraWWContagemResultadoWWAceiteFaturamentoDS_82_Tfcontagemresultado_pffinal ,
                                             decimal A574ContagemResultado_PFFinal ,
                                             decimal AV218ExtraWWContagemResultadoWWAceiteFaturamentoDS_83_Tfcontagemresultado_pffinal_to ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo ,
                                             short A531ContagemResultado_StatusUltCnt ,
                                             bool A517ContagemResultado_Ultima ,
                                             short A1583ContagemResultado_TipoRegistro )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [101] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T2.[ContagemResultado_LoteAceiteCod] AS ContagemResultado_LoteAceiteCod, T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[ContagemResultado_TipoRegistro], T1.[ContagemResultado_Ultima], T2.[ContagemResultado_ValorPF], T5.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T7.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T6.[Contratada_Sigla] AS ContagemResultado_ContratadaOr, T2.[ContagemResultado_Agrupador], T2.[ContagemResultado_Descricao], T4.[Servico_Codigo] AS ContagemResultado_Servico, T2.[ContagemResultado_Baseline], T3.[Lote_Numero] AS ContagemResultado_LoteAceite, T2.[ContagemResultado_NaoCnfDmnCod], T2.[ContagemResultado_ContratadaOrigemCod] AS ContagemResultado_ContratadaOr, T2.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T3.[Lote_Data] AS ContagemResultado_DataAceite, T2.[ContagemResultado_DataDmn], T2.[ContagemResultado_StatusDmn], T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T8.[Contratada_AreaTrabalhoCod], COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, COALESCE( T9.[ContagemResultado_StatusUltCnt], 0) AS ContagemResultado_StatusUltCnt, T2.[ContagemResultado_DemandaFM], T2.[ContagemResultado_Demanda], T1.[ContagemResultado_Codigo], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt] FROM (((((((([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Lote] T3 WITH (NOLOCK) ON T3.[Lote_Codigo] = T2.[ContagemResultado_LoteAceiteCod]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo]";
         scmdbuf = scmdbuf + " = T4.[Servico_Codigo]) LEFT JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaOrigemCod]) LEFT JOIN [Sistema] T7 WITH (NOLOCK) ON T7.[Sistema_Codigo] = T2.[ContagemResultado_SistemaCod]) LEFT JOIN [Contratada] T8 WITH (NOLOCK) ON T8.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo], MIN([ContagemResultado_StatusCnt]) AS ContagemResultado_StatusUltCnt FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T9 ON T9.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T2.[ContagemResultado_StatusDmn] = 'O' or T2.[ContagemResultado_StatusDmn] = 'P' or T2.[ContagemResultado_StatusDmn] = 'L')";
         scmdbuf = scmdbuf + " and (Not ( @AV140ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV142ExtraWWContagemResultadoWWAceiteFaturamentoDS_7_Contagemresultado_dataultcnt1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV142ExtraWWContagemResultadoWWAceiteFaturamentoDS_7_Contagemresultado_dataultcnt1))";
         scmdbuf = scmdbuf + " and (Not ( @AV140ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV143ExtraWWContagemResultadoWWAceiteFaturamentoDS_8_Contagemresultado_dataultcnt_to1 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV143ExtraWWContagemResultadoWWAceiteFaturamentoDS_8_Contagemresultado_dataultcnt_to1))";
         scmdbuf = scmdbuf + " and (Not ( @AV159ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 = 1 and @AV160ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV162ExtraWWContagemResultadoWWAceiteFaturamentoDS_27_Contagemresultado_dataultcnt2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV162ExtraWWContagemResultadoWWAceiteFaturamentoDS_27_Contagemresultado_dataultcnt2))";
         scmdbuf = scmdbuf + " and (Not ( @AV159ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 = 1 and @AV160ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV163ExtraWWContagemResultadoWWAceiteFaturamentoDS_28_Contagemresultado_dataultcnt_to2 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV163ExtraWWContagemResultadoWWAceiteFaturamentoDS_28_Contagemresultado_dataultcnt_to2))";
         scmdbuf = scmdbuf + " and (Not ( @AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 = 1 and @AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV182ExtraWWContagemResultadoWWAceiteFaturamentoDS_47_Contagemresultado_dataultcnt3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV182ExtraWWContagemResultadoWWAceiteFaturamentoDS_47_Contagemresultado_dataultcnt3))";
         scmdbuf = scmdbuf + " and (Not ( @AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 = 1 and @AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3 = 'CONTAGEMRESULTADO_DATAULTCNT' and ( Not (@AV183ExtraWWContagemResultadoWWAceiteFaturamentoDS_48_Contagemresultado_dataultcnt_to3 = convert( DATETIME, '17530101', 112 )))) or ( COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV183ExtraWWContagemResultadoWWAceiteFaturamentoDS_48_Contagemresultado_dataultcnt_to3))";
         scmdbuf = scmdbuf + " and ((@AV207ExtraWWContagemResultadoWWAceiteFaturamentoDS_72_Tfcontagemresultado_dataultcnt = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV207ExtraWWContagemResultadoWWAceiteFaturamentoDS_72_Tfcontagemresultado_dataultcnt))";
         scmdbuf = scmdbuf + " and ((@AV208ExtraWWContagemResultadoWWAceiteFaturamentoDS_73_Tfcontagemresultado_dataultcnt_to = convert( DATETIME, '17530101', 112 )) or ( COALESCE( T9.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV208ExtraWWContagemResultadoWWAceiteFaturamentoDS_73_Tfcontagemresultado_dataultcnt_to))";
         scmdbuf = scmdbuf + " and (T8.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         scmdbuf = scmdbuf + " and (COALESCE( T9.[ContagemResultado_StatusUltCnt], 0) = 8)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_Ultima] = 1)";
         scmdbuf = scmdbuf + " and (T2.[ContagemResultado_TipoRegistro] = 1)";
         if ( AV9WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV9WWPCo_2Contratada_codigo)";
         }
         else
         {
            GXv_int3[27] = 1;
         }
         if ( ( StringUtil.StrCmp(AV140ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV141ExtraWWContagemResultadoWWAceiteFaturamentoDS_6_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1 or T2.[ContagemResultado_DemandaFM] = @AV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int3[28] = 1;
            GXv_int3[29] = 1;
         }
         if ( ( StringUtil.StrCmp(AV140ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV141ExtraWWContagemResultadoWWAceiteFaturamentoDS_6_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1 or T2.[ContagemResultado_DemandaFM] like @lV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int3[30] = 1;
            GXv_int3[31] = 1;
         }
         if ( ( StringUtil.StrCmp(AV140ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV141ExtraWWContagemResultadoWWAceiteFaturamentoDS_6_Dynamicfiltersoperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1 or T2.[ContagemResultado_DemandaFM] like '%' + @lV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1)";
         }
         else
         {
            GXv_int3[32] = 1;
            GXv_int3[33] = 1;
         }
         if ( ( StringUtil.StrCmp(AV140ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV145ExtraWWContagemResultadoWWAceiteFaturamentoDS_10_Contagemresultado_datadmn1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV145ExtraWWContagemResultadoWWAceiteFaturamentoDS_10_Contagemresultado_datadmn1)";
         }
         else
         {
            GXv_int3[34] = 1;
         }
         if ( ( StringUtil.StrCmp(AV140ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV146ExtraWWContagemResultadoWWAceiteFaturamentoDS_11_Contagemresultado_datadmn_to1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV146ExtraWWContagemResultadoWWAceiteFaturamentoDS_11_Contagemresultado_datadmn_to1)";
         }
         else
         {
            GXv_int3[35] = 1;
         }
         if ( ( StringUtil.StrCmp(AV140ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAACEITE") == 0 ) && ( ! (DateTime.MinValue==AV147ExtraWWContagemResultadoWWAceiteFaturamentoDS_12_Contagemresultado_dataaceite1) ) )
         {
            sWhereString = sWhereString + " and (T3.[Lote_Data] >= @AV147ExtraWWContagemResultadoWWAceiteFaturamentoDS_12_Contagemresultado_dataaceite1)";
         }
         else
         {
            GXv_int3[36] = 1;
         }
         if ( ( StringUtil.StrCmp(AV140ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DATAACEITE") == 0 ) && ( ! (DateTime.MinValue==AV148ExtraWWContagemResultadoWWAceiteFaturamentoDS_13_Contagemresultado_dataaceite_to1) ) )
         {
            sWhereString = sWhereString + " and (T3.[Lote_Data] < DATEADD( dd,1, @AV148ExtraWWContagemResultadoWWAceiteFaturamentoDS_13_Contagemresultado_dataaceite_to1))";
         }
         else
         {
            GXv_int3[37] = 1;
         }
         if ( ( StringUtil.StrCmp(AV140ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV149ExtraWWContagemResultadoWWAceiteFaturamentoDS_14_Contagemresultado_sistemacod1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV149ExtraWWContagemResultadoWWAceiteFaturamentoDS_14_Contagemresultado_sistemacod1)";
         }
         else
         {
            GXv_int3[38] = 1;
         }
         if ( ( StringUtil.StrCmp(AV140ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV150ExtraWWContagemResultadoWWAceiteFaturamentoDS_15_Contagemresultado_contratadacod1 > 0 ) && ( AV136ExtraWWContagemResultadoWWAceiteFaturamentoDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV150ExtraWWContagemResultadoWWAceiteFaturamentoDS_15_Contagemresultado_contratadacod1)";
         }
         else
         {
            GXv_int3[39] = 1;
         }
         if ( ( StringUtil.StrCmp(AV140ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 ) && ( ( AV151ExtraWWContagemResultadoWWAceiteFaturamentoDS_16_Contagemresultado_contratadaorigemcod1 > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaOrigemCod] = @AV151ExtraWWContagemResultadoWWAceiteFaturamentoDS_16_Contagemresultado_contratadaorigemcod1)";
         }
         else
         {
            GXv_int3[40] = 1;
         }
         if ( ( StringUtil.StrCmp(AV140ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV152ExtraWWContagemResultadoWWAceiteFaturamentoDS_17_Contagemresultado_naocnfdmncod1) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_NaoCnfDmnCod] = @AV152ExtraWWContagemResultadoWWAceiteFaturamentoDS_17_Contagemresultado_naocnfdmncod1)";
         }
         else
         {
            GXv_int3[41] = 1;
         }
         if ( ( StringUtil.StrCmp(AV140ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_LOTEACEITE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV153ExtraWWContagemResultadoWWAceiteFaturamentoDS_18_Contagemresultado_loteaceite1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Lote_Numero] like @lV153ExtraWWContagemResultadoWWAceiteFaturamentoDS_18_Contagemresultado_loteaceite1 + '%')";
         }
         else
         {
            GXv_int3[42] = 1;
         }
         if ( ( StringUtil.StrCmp(AV140ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DEMANDAFM_ORDER") == 0 ) && ( ! (0==AV154ExtraWWContagemResultadoWWAceiteFaturamentoDS_19_Contagemresultado_demandafm_order1) ) )
         {
            sWhereString = sWhereString + " and (CONVERT( DECIMAL(28,14), SUBSTRING(T2.[ContagemResultado_DemandaFM], 1, 18)) = @AV154ExtraWWContagemResultadoWWAceiteFaturamentoDS_19_Contagemresultado_demandafm_order1)";
         }
         else
         {
            GXv_int3[43] = 1;
         }
         if ( ( StringUtil.StrCmp(AV140ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV155ExtraWWContagemResultadoWWAceiteFaturamentoDS_20_Contagemresultado_baseline1, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( ( StringUtil.StrCmp(AV140ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV155ExtraWWContagemResultadoWWAceiteFaturamentoDS_20_Contagemresultado_baseline1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T2.[ContagemResultado_Baseline] = 1 or T2.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( ( StringUtil.StrCmp(AV140ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV156ExtraWWContagemResultadoWWAceiteFaturamentoDS_21_Contagemresultado_servico1) ) )
         {
            sWhereString = sWhereString + " and (T4.[Servico_Codigo] = @AV156ExtraWWContagemResultadoWWAceiteFaturamentoDS_21_Contagemresultado_servico1)";
         }
         else
         {
            GXv_int3[44] = 1;
         }
         if ( ( StringUtil.StrCmp(AV140ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV157ExtraWWContagemResultadoWWAceiteFaturamentoDS_22_Contagemresultado_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV157ExtraWWContagemResultadoWWAceiteFaturamentoDS_22_Contagemresultado_descricao1 + '%')";
         }
         else
         {
            GXv_int3[45] = 1;
         }
         if ( ( StringUtil.StrCmp(AV140ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV158ExtraWWContagemResultadoWWAceiteFaturamentoDS_23_Contagemresultado_agrupador1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV158ExtraWWContagemResultadoWWAceiteFaturamentoDS_23_Contagemresultado_agrupador1)";
         }
         else
         {
            GXv_int3[46] = 1;
         }
         if ( AV159ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV160ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV161ExtraWWContagemResultadoWWAceiteFaturamentoDS_26_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2 or T2.[ContagemResultado_DemandaFM] = @AV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int3[47] = 1;
            GXv_int3[48] = 1;
         }
         if ( AV159ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV160ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV161ExtraWWContagemResultadoWWAceiteFaturamentoDS_26_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2 or T2.[ContagemResultado_DemandaFM] like @lV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int3[49] = 1;
            GXv_int3[50] = 1;
         }
         if ( AV159ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV160ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV161ExtraWWContagemResultadoWWAceiteFaturamentoDS_26_Dynamicfiltersoperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2 or T2.[ContagemResultado_DemandaFM] like '%' + @lV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2)";
         }
         else
         {
            GXv_int3[51] = 1;
            GXv_int3[52] = 1;
         }
         if ( AV159ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV160ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV165ExtraWWContagemResultadoWWAceiteFaturamentoDS_30_Contagemresultado_datadmn2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV165ExtraWWContagemResultadoWWAceiteFaturamentoDS_30_Contagemresultado_datadmn2)";
         }
         else
         {
            GXv_int3[53] = 1;
         }
         if ( AV159ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV160ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV166ExtraWWContagemResultadoWWAceiteFaturamentoDS_31_Contagemresultado_datadmn_to2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV166ExtraWWContagemResultadoWWAceiteFaturamentoDS_31_Contagemresultado_datadmn_to2)";
         }
         else
         {
            GXv_int3[54] = 1;
         }
         if ( AV159ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV160ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAACEITE") == 0 ) && ( ! (DateTime.MinValue==AV167ExtraWWContagemResultadoWWAceiteFaturamentoDS_32_Contagemresultado_dataaceite2) ) )
         {
            sWhereString = sWhereString + " and (T3.[Lote_Data] >= @AV167ExtraWWContagemResultadoWWAceiteFaturamentoDS_32_Contagemresultado_dataaceite2)";
         }
         else
         {
            GXv_int3[55] = 1;
         }
         if ( AV159ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV160ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DATAACEITE") == 0 ) && ( ! (DateTime.MinValue==AV168ExtraWWContagemResultadoWWAceiteFaturamentoDS_33_Contagemresultado_dataaceite_to2) ) )
         {
            sWhereString = sWhereString + " and (T3.[Lote_Data] < DATEADD( dd,1, @AV168ExtraWWContagemResultadoWWAceiteFaturamentoDS_33_Contagemresultado_dataaceite_to2))";
         }
         else
         {
            GXv_int3[56] = 1;
         }
         if ( AV159ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV160ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV169ExtraWWContagemResultadoWWAceiteFaturamentoDS_34_Contagemresultado_sistemacod2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV169ExtraWWContagemResultadoWWAceiteFaturamentoDS_34_Contagemresultado_sistemacod2)";
         }
         else
         {
            GXv_int3[57] = 1;
         }
         if ( AV159ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV160ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV170ExtraWWContagemResultadoWWAceiteFaturamentoDS_35_Contagemresultado_contratadacod2 > 0 ) && ( AV136ExtraWWContagemResultadoWWAceiteFaturamentoDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV170ExtraWWContagemResultadoWWAceiteFaturamentoDS_35_Contagemresultado_contratadacod2)";
         }
         else
         {
            GXv_int3[58] = 1;
         }
         if ( AV159ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV160ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 ) && ( ( AV171ExtraWWContagemResultadoWWAceiteFaturamentoDS_36_Contagemresultado_contratadaorigemcod2 > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaOrigemCod] = @AV171ExtraWWContagemResultadoWWAceiteFaturamentoDS_36_Contagemresultado_contratadaorigemcod2)";
         }
         else
         {
            GXv_int3[59] = 1;
         }
         if ( AV159ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV160ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV172ExtraWWContagemResultadoWWAceiteFaturamentoDS_37_Contagemresultado_naocnfdmncod2) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_NaoCnfDmnCod] = @AV172ExtraWWContagemResultadoWWAceiteFaturamentoDS_37_Contagemresultado_naocnfdmncod2)";
         }
         else
         {
            GXv_int3[60] = 1;
         }
         if ( AV159ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV160ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_LOTEACEITE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV173ExtraWWContagemResultadoWWAceiteFaturamentoDS_38_Contagemresultado_loteaceite2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Lote_Numero] like @lV173ExtraWWContagemResultadoWWAceiteFaturamentoDS_38_Contagemresultado_loteaceite2 + '%')";
         }
         else
         {
            GXv_int3[61] = 1;
         }
         if ( AV159ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV160ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DEMANDAFM_ORDER") == 0 ) && ( ! (0==AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_39_Contagemresultado_demandafm_order2) ) )
         {
            sWhereString = sWhereString + " and (CONVERT( DECIMAL(28,14), SUBSTRING(T2.[ContagemResultado_DemandaFM], 1, 18)) = @AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_39_Contagemresultado_demandafm_order2)";
         }
         else
         {
            GXv_int3[62] = 1;
         }
         if ( AV159ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV160ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV175ExtraWWContagemResultadoWWAceiteFaturamentoDS_40_Contagemresultado_baseline2, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV159ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV160ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV175ExtraWWContagemResultadoWWAceiteFaturamentoDS_40_Contagemresultado_baseline2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T2.[ContagemResultado_Baseline] = 1 or T2.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV159ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV160ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV176ExtraWWContagemResultadoWWAceiteFaturamentoDS_41_Contagemresultado_servico2) ) )
         {
            sWhereString = sWhereString + " and (T4.[Servico_Codigo] = @AV176ExtraWWContagemResultadoWWAceiteFaturamentoDS_41_Contagemresultado_servico2)";
         }
         else
         {
            GXv_int3[63] = 1;
         }
         if ( AV159ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV160ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV177ExtraWWContagemResultadoWWAceiteFaturamentoDS_42_Contagemresultado_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV177ExtraWWContagemResultadoWWAceiteFaturamentoDS_42_Contagemresultado_descricao2 + '%')";
         }
         else
         {
            GXv_int3[64] = 1;
         }
         if ( AV159ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV160ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_43_Contagemresultado_agrupador2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_43_Contagemresultado_agrupador2)";
         }
         else
         {
            GXv_int3[65] = 1;
         }
         if ( AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV181ExtraWWContagemResultadoWWAceiteFaturamentoDS_46_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3 or T2.[ContagemResultado_DemandaFM] = @AV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int3[66] = 1;
            GXv_int3[67] = 1;
         }
         if ( AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV181ExtraWWContagemResultadoWWAceiteFaturamentoDS_46_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3 or T2.[ContagemResultado_DemandaFM] like @lV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int3[68] = 1;
            GXv_int3[69] = 1;
         }
         if ( AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 ) && ( AV181ExtraWWContagemResultadoWWAceiteFaturamentoDS_46_Dynamicfiltersoperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like '%' + @lV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3 or T2.[ContagemResultado_DemandaFM] like '%' + @lV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3)";
         }
         else
         {
            GXv_int3[70] = 1;
            GXv_int3[71] = 1;
         }
         if ( AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV185ExtraWWContagemResultadoWWAceiteFaturamentoDS_50_Contagemresultado_datadmn3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] >= @AV185ExtraWWContagemResultadoWWAceiteFaturamentoDS_50_Contagemresultado_datadmn3)";
         }
         else
         {
            GXv_int3[72] = 1;
         }
         if ( AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV186ExtraWWContagemResultadoWWAceiteFaturamentoDS_51_Contagemresultado_datadmn_to3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_DataDmn] <= @AV186ExtraWWContagemResultadoWWAceiteFaturamentoDS_51_Contagemresultado_datadmn_to3)";
         }
         else
         {
            GXv_int3[73] = 1;
         }
         if ( AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAACEITE") == 0 ) && ( ! (DateTime.MinValue==AV187ExtraWWContagemResultadoWWAceiteFaturamentoDS_52_Contagemresultado_dataaceite3) ) )
         {
            sWhereString = sWhereString + " and (T3.[Lote_Data] >= @AV187ExtraWWContagemResultadoWWAceiteFaturamentoDS_52_Contagemresultado_dataaceite3)";
         }
         else
         {
            GXv_int3[74] = 1;
         }
         if ( AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DATAACEITE") == 0 ) && ( ! (DateTime.MinValue==AV188ExtraWWContagemResultadoWWAceiteFaturamentoDS_53_Contagemresultado_dataaceite_to3) ) )
         {
            sWhereString = sWhereString + " and (T3.[Lote_Data] < DATEADD( dd,1, @AV188ExtraWWContagemResultadoWWAceiteFaturamentoDS_53_Contagemresultado_dataaceite_to3))";
         }
         else
         {
            GXv_int3[75] = 1;
         }
         if ( AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 ) && ( ! (0==AV189ExtraWWContagemResultadoWWAceiteFaturamentoDS_54_Contagemresultado_sistemacod3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_SistemaCod] = @AV189ExtraWWContagemResultadoWWAceiteFaturamentoDS_54_Contagemresultado_sistemacod3)";
         }
         else
         {
            GXv_int3[76] = 1;
         }
         if ( AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 ) && ( ( AV190ExtraWWContagemResultadoWWAceiteFaturamentoDS_55_Contagemresultado_contratadacod3 > 0 ) && ( AV136ExtraWWContagemResultadoWWAceiteFaturamentoDS_1_Contratada_areatrabalhocod > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaCod] = @AV190ExtraWWContagemResultadoWWAceiteFaturamentoDS_55_Contagemresultado_contratadacod3)";
         }
         else
         {
            GXv_int3[77] = 1;
         }
         if ( AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 ) && ( ( AV191ExtraWWContagemResultadoWWAceiteFaturamentoDS_56_Contagemresultado_contratadaorigemcod3 > 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ContratadaOrigemCod] = @AV191ExtraWWContagemResultadoWWAceiteFaturamentoDS_56_Contagemresultado_contratadaorigemcod3)";
         }
         else
         {
            GXv_int3[78] = 1;
         }
         if ( AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 ) && ( ! (0==AV192ExtraWWContagemResultadoWWAceiteFaturamentoDS_57_Contagemresultado_naocnfdmncod3) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_NaoCnfDmnCod] = @AV192ExtraWWContagemResultadoWWAceiteFaturamentoDS_57_Contagemresultado_naocnfdmncod3)";
         }
         else
         {
            GXv_int3[79] = 1;
         }
         if ( AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_LOTEACEITE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_58_Contagemresultado_loteaceite3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Lote_Numero] like @lV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_58_Contagemresultado_loteaceite3 + '%')";
         }
         else
         {
            GXv_int3[80] = 1;
         }
         if ( AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DEMANDAFM_ORDER") == 0 ) && ( ! (0==AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_59_Contagemresultado_demandafm_order3) ) )
         {
            sWhereString = sWhereString + " and (CONVERT( DECIMAL(28,14), SUBSTRING(T2.[ContagemResultado_DemandaFM], 1, 18)) = @AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_59_Contagemresultado_demandafm_order3)";
         }
         else
         {
            GXv_int3[81] = 1;
         }
         if ( AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoWWAceiteFaturamentoDS_60_Contagemresultado_baseline3, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_BASELINE") == 0 ) && ( ( StringUtil.StrCmp(AV195ExtraWWContagemResultadoWWAceiteFaturamentoDS_60_Contagemresultado_baseline3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T2.[ContagemResultado_Baseline] = 1 or T2.[ContagemResultado_Baseline] IS NULL)";
         }
         if ( AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_SERVICO") == 0 ) && ( ! (0==AV196ExtraWWContagemResultadoWWAceiteFaturamentoDS_61_Contagemresultado_servico3) ) )
         {
            sWhereString = sWhereString + " and (T4.[Servico_Codigo] = @AV196ExtraWWContagemResultadoWWAceiteFaturamentoDS_61_Contagemresultado_servico3)";
         }
         else
         {
            GXv_int3[82] = 1;
         }
         if ( AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV197ExtraWWContagemResultadoWWAceiteFaturamentoDS_62_Contagemresultado_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like '%' + @lV197ExtraWWContagemResultadoWWAceiteFaturamentoDS_62_Contagemresultado_descricao3 + '%')";
         }
         else
         {
            GXv_int3[83] = 1;
         }
         if ( AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3, "CONTAGEMRESULTADO_AGRUPADOR") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_63_Contagemresultado_agrupador3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Agrupador] = @AV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_63_Contagemresultado_agrupador3)";
         }
         else
         {
            GXv_int3[84] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV200ExtraWWContagemResultadoWWAceiteFaturamentoDS_65_Tfcontagemresultado_loteaceite_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV199ExtraWWContagemResultadoWWAceiteFaturamentoDS_64_Tfcontagemresultado_loteaceite)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Lote_Numero] like @lV199ExtraWWContagemResultadoWWAceiteFaturamentoDS_64_Tfcontagemresultado_loteaceite)";
         }
         else
         {
            GXv_int3[85] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV200ExtraWWContagemResultadoWWAceiteFaturamentoDS_65_Tfcontagemresultado_loteaceite_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Lote_Numero] = @AV200ExtraWWContagemResultadoWWAceiteFaturamentoDS_65_Tfcontagemresultado_loteaceite_sel)";
         }
         else
         {
            GXv_int3[86] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV202ExtraWWContagemResultadoWWAceiteFaturamentoDS_67_Tfcontagemresultado_osfsosfm_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV201ExtraWWContagemResultadoWWAceiteFaturamentoDS_66_Tfcontagemresultado_osfsosfm)) ) )
         {
            sWhereString = sWhereString + " and (( RTRIM(LTRIM(T2.[ContagemResultado_Demanda])) + CASE  WHEN (T2.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T2.[ContagemResultado_DemandaFM])) END) like @lV201ExtraWWContagemResultadoWWAceiteFaturamentoDS_66_Tfcontagemresultado_osfsosfm)";
         }
         else
         {
            GXv_int3[87] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV202ExtraWWContagemResultadoWWAceiteFaturamentoDS_67_Tfcontagemresultado_osfsosfm_sel)) )
         {
            sWhereString = sWhereString + " and (( RTRIM(LTRIM(T2.[ContagemResultado_Demanda])) + CASE  WHEN (T2.[ContagemResultado_DemandaFM] = '') THEN '' ELSE '|' + RTRIM(LTRIM(T2.[ContagemResultado_DemandaFM])) END) = @AV202ExtraWWContagemResultadoWWAceiteFaturamentoDS_67_Tfcontagemresultado_osfsosfm_sel)";
         }
         else
         {
            GXv_int3[88] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV204ExtraWWContagemResultadoWWAceiteFaturamentoDS_69_Tfcontagemresultado_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV203ExtraWWContagemResultadoWWAceiteFaturamentoDS_68_Tfcontagemresultado_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] like @lV203ExtraWWContagemResultadoWWAceiteFaturamentoDS_68_Tfcontagemresultado_descricao)";
         }
         else
         {
            GXv_int3[89] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV204ExtraWWContagemResultadoWWAceiteFaturamentoDS_69_Tfcontagemresultado_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Descricao] = @AV204ExtraWWContagemResultadoWWAceiteFaturamentoDS_69_Tfcontagemresultado_descricao_sel)";
         }
         else
         {
            GXv_int3[90] = 1;
         }
         if ( ! (DateTime.MinValue==AV205ExtraWWContagemResultadoWWAceiteFaturamentoDS_70_Tfcontagemresultado_dataaceite) )
         {
            sWhereString = sWhereString + " and (T3.[Lote_Data] >= @AV205ExtraWWContagemResultadoWWAceiteFaturamentoDS_70_Tfcontagemresultado_dataaceite)";
         }
         else
         {
            GXv_int3[91] = 1;
         }
         if ( ! (DateTime.MinValue==AV206ExtraWWContagemResultadoWWAceiteFaturamentoDS_71_Tfcontagemresultado_dataaceite_to) )
         {
            sWhereString = sWhereString + " and (T3.[Lote_Data] <= @AV206ExtraWWContagemResultadoWWAceiteFaturamentoDS_71_Tfcontagemresultado_dataaceite_to)";
         }
         else
         {
            GXv_int3[92] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV210ExtraWWContagemResultadoWWAceiteFaturamentoDS_75_Tfcontagemresultado_contratadaorigemsigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV209ExtraWWContagemResultadoWWAceiteFaturamentoDS_74_Tfcontagemresultado_contratadaorigemsigla)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_Sigla] like @lV209ExtraWWContagemResultadoWWAceiteFaturamentoDS_74_Tfcontagemresultado_contratadaorigemsigla)";
         }
         else
         {
            GXv_int3[93] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV210ExtraWWContagemResultadoWWAceiteFaturamentoDS_75_Tfcontagemresultado_contratadaorigemsigla_sel)) )
         {
            sWhereString = sWhereString + " and (T6.[Contratada_Sigla] = @AV210ExtraWWContagemResultadoWWAceiteFaturamentoDS_75_Tfcontagemresultado_contratadaorigemsigla_sel)";
         }
         else
         {
            GXv_int3[94] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV212ExtraWWContagemResultadoWWAceiteFaturamentoDS_77_Tfcontagemrresultado_sistemasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV211ExtraWWContagemResultadoWWAceiteFaturamentoDS_76_Tfcontagemrresultado_sistemasigla)) ) )
         {
            sWhereString = sWhereString + " and (T7.[Sistema_Sigla] like @lV211ExtraWWContagemResultadoWWAceiteFaturamentoDS_76_Tfcontagemrresultado_sistemasigla)";
         }
         else
         {
            GXv_int3[95] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV212ExtraWWContagemResultadoWWAceiteFaturamentoDS_77_Tfcontagemrresultado_sistemasigla_sel)) )
         {
            sWhereString = sWhereString + " and (T7.[Sistema_Sigla] = @AV212ExtraWWContagemResultadoWWAceiteFaturamentoDS_77_Tfcontagemrresultado_sistemasigla_sel)";
         }
         else
         {
            GXv_int3[96] = 1;
         }
         if ( AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_78_Tfcontagemresultado_statusdmn_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV213ExtraWWContagemResultadoWWAceiteFaturamentoDS_78_Tfcontagemresultado_statusdmn_sels, "T2.[ContagemResultado_StatusDmn] IN (", ")") + ")";
         }
         if ( AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_79_Tfcontagemresultado_baseline_sel == 1 )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 1)";
         }
         if ( AV214ExtraWWContagemResultadoWWAceiteFaturamentoDS_79_Tfcontagemresultado_baseline_sel == 2 )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_Baseline] = 0)";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV216ExtraWWContagemResultadoWWAceiteFaturamentoDS_81_Tfcontagemresultado_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV215ExtraWWContagemResultadoWWAceiteFaturamentoDS_80_Tfcontagemresultado_servicosigla)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Servico_Sigla] like @lV215ExtraWWContagemResultadoWWAceiteFaturamentoDS_80_Tfcontagemresultado_servicosigla)";
         }
         else
         {
            GXv_int3[97] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV216ExtraWWContagemResultadoWWAceiteFaturamentoDS_81_Tfcontagemresultado_servicosigla_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Servico_Sigla] = @AV216ExtraWWContagemResultadoWWAceiteFaturamentoDS_81_Tfcontagemresultado_servicosigla_sel)";
         }
         else
         {
            GXv_int3[98] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV219ExtraWWContagemResultadoWWAceiteFaturamentoDS_84_Tfcontagemresultado_valorpf) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ValorPF] >= @AV219ExtraWWContagemResultadoWWAceiteFaturamentoDS_84_Tfcontagemresultado_valorpf)";
         }
         else
         {
            GXv_int3[99] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV220ExtraWWContagemResultadoWWAceiteFaturamentoDS_85_Tfcontagemresultado_valorpf_to) )
         {
            sWhereString = sWhereString + " and (T2.[ContagemResultado_ValorPF] <= @AV220ExtraWWContagemResultadoWWAceiteFaturamentoDS_85_Tfcontagemresultado_valorpf_to)";
         }
         else
         {
            GXv_int3[100] = 1;
         }
         if ( AV46GridState_gxTpr_Dynamicfilters_Count < 1 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Codigo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV16OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + " ORDER BY T9.[ContagemResultado_DataUltCnt]";
         }
         else if ( AV16OrderedBy == 2 )
         {
            scmdbuf = scmdbuf + " ORDER BY T2.[ContagemResultado_Demanda]";
         }
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P003V3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (String)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (long)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (bool)dynConstraints[21] , (String)dynConstraints[22] , (short)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (DateTime)dynConstraints[27] , (DateTime)dynConstraints[28] , (int)dynConstraints[29] , (int)dynConstraints[30] , (int)dynConstraints[31] , (int)dynConstraints[32] , (String)dynConstraints[33] , (long)dynConstraints[34] , (String)dynConstraints[35] , (int)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (bool)dynConstraints[39] , (String)dynConstraints[40] , (short)dynConstraints[41] , (String)dynConstraints[42] , (DateTime)dynConstraints[43] , (DateTime)dynConstraints[44] , (DateTime)dynConstraints[45] , (DateTime)dynConstraints[46] , (int)dynConstraints[47] , (int)dynConstraints[48] , (int)dynConstraints[49] , (int)dynConstraints[50] , (String)dynConstraints[51] , (long)dynConstraints[52] , (String)dynConstraints[53] , (int)dynConstraints[54] , (String)dynConstraints[55] , (String)dynConstraints[56] , (String)dynConstraints[57] , (String)dynConstraints[58] , (String)dynConstraints[59] , (String)dynConstraints[60] , (String)dynConstraints[61] , (String)dynConstraints[62] , (DateTime)dynConstraints[63] , (DateTime)dynConstraints[64] , (String)dynConstraints[65] , (String)dynConstraints[66] , (String)dynConstraints[67] , (String)dynConstraints[68] , (int)dynConstraints[69] , (short)dynConstraints[70] , (String)dynConstraints[71] , (String)dynConstraints[72] , (decimal)dynConstraints[73] , (decimal)dynConstraints[74] , (int)dynConstraints[75] , (int)dynConstraints[76] , (String)dynConstraints[77] , (String)dynConstraints[78] , (DateTime)dynConstraints[79] , (DateTime)dynConstraints[80] , (int)dynConstraints[81] , (int)dynConstraints[82] , (int)dynConstraints[83] , (String)dynConstraints[84] , (bool)dynConstraints[85] , (int)dynConstraints[86] , (String)dynConstraints[87] , (String)dynConstraints[88] , (String)dynConstraints[89] , (String)dynConstraints[90] , (String)dynConstraints[91] , (decimal)dynConstraints[92] , (int)dynConstraints[93] , (short)dynConstraints[94] , (DateTime)dynConstraints[95] , (DateTime)dynConstraints[96] , (DateTime)dynConstraints[97] , (DateTime)dynConstraints[98] , (DateTime)dynConstraints[99] , (DateTime)dynConstraints[100] , (DateTime)dynConstraints[101] , (DateTime)dynConstraints[102] , (DateTime)dynConstraints[103] , (decimal)dynConstraints[104] , (decimal)dynConstraints[105] , (decimal)dynConstraints[106] , (int)dynConstraints[107] , (int)dynConstraints[108] , (short)dynConstraints[109] , (bool)dynConstraints[110] , (short)dynConstraints[111] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP003V3 ;
          prmP003V3 = new Object[] {
          new Object[] {"@AV140ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV142ExtraWWContagemResultadoWWAceiteFaturamentoDS_7_Contagemresultado_dataultcnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV142ExtraWWContagemResultadoWWAceiteFaturamentoDS_7_Contagemresultado_dataultcnt1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV140ExtraWWContagemResultadoWWAceiteFaturamentoDS_5_Dynamicfiltersselector1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV143ExtraWWContagemResultadoWWAceiteFaturamentoDS_8_Contagemresultado_dataultcnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV143ExtraWWContagemResultadoWWAceiteFaturamentoDS_8_Contagemresultado_dataultcnt_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV159ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV160ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV162ExtraWWContagemResultadoWWAceiteFaturamentoDS_27_Contagemresultado_dataultcnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV162ExtraWWContagemResultadoWWAceiteFaturamentoDS_27_Contagemresultado_dataultcnt2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV159ExtraWWContagemResultadoWWAceiteFaturamentoDS_24_Dynamicfiltersenabled2",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV160ExtraWWContagemResultadoWWAceiteFaturamentoDS_25_Dynamicfiltersselector2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV163ExtraWWContagemResultadoWWAceiteFaturamentoDS_28_Contagemresultado_dataultcnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV163ExtraWWContagemResultadoWWAceiteFaturamentoDS_28_Contagemresultado_dataultcnt_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV182ExtraWWContagemResultadoWWAceiteFaturamentoDS_47_Contagemresultado_dataultcnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV182ExtraWWContagemResultadoWWAceiteFaturamentoDS_47_Contagemresultado_dataultcnt3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV179ExtraWWContagemResultadoWWAceiteFaturamentoDS_44_Dynamicfiltersenabled3",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV180ExtraWWContagemResultadoWWAceiteFaturamentoDS_45_Dynamicfiltersselector3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV183ExtraWWContagemResultadoWWAceiteFaturamentoDS_48_Contagemresultado_dataultcnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV183ExtraWWContagemResultadoWWAceiteFaturamentoDS_48_Contagemresultado_dataultcnt_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV207ExtraWWContagemResultadoWWAceiteFaturamentoDS_72_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV207ExtraWWContagemResultadoWWAceiteFaturamentoDS_72_Tfcontagemresultado_dataultcnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV208ExtraWWContagemResultadoWWAceiteFaturamentoDS_73_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV208ExtraWWContagemResultadoWWAceiteFaturamentoDS_73_Tfcontagemresultado_dataultcnt_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9WWPCo_2Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV144ExtraWWContagemResultadoWWAceiteFaturamentoDS_9_Contagemresultado_osfsosfm1",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV145ExtraWWContagemResultadoWWAceiteFaturamentoDS_10_Contagemresultado_datadmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV146ExtraWWContagemResultadoWWAceiteFaturamentoDS_11_Contagemresultado_datadmn_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV147ExtraWWContagemResultadoWWAceiteFaturamentoDS_12_Contagemresultado_dataaceite1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV148ExtraWWContagemResultadoWWAceiteFaturamentoDS_13_Contagemresultado_dataaceite_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV149ExtraWWContagemResultadoWWAceiteFaturamentoDS_14_Contagemresultado_sistemacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV150ExtraWWContagemResultadoWWAceiteFaturamentoDS_15_Contagemresultado_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV151ExtraWWContagemResultadoWWAceiteFaturamentoDS_16_Contagemresultado_contratadaorigemcod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV152ExtraWWContagemResultadoWWAceiteFaturamentoDS_17_Contagemresultado_naocnfdmncod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV153ExtraWWContagemResultadoWWAceiteFaturamentoDS_18_Contagemresultado_loteaceite1",SqlDbType.Char,10,0} ,
          new Object[] {"@AV154ExtraWWContagemResultadoWWAceiteFaturamentoDS_19_Contagemresultado_demandafm_order1",SqlDbType.Decimal,18,0} ,
          new Object[] {"@AV156ExtraWWContagemResultadoWWAceiteFaturamentoDS_21_Contagemresultado_servico1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV157ExtraWWContagemResultadoWWAceiteFaturamentoDS_22_Contagemresultado_descricao1",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV158ExtraWWContagemResultadoWWAceiteFaturamentoDS_23_Contagemresultado_agrupador1",SqlDbType.Char,15,0} ,
          new Object[] {"@AV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV164ExtraWWContagemResultadoWWAceiteFaturamentoDS_29_Contagemresultado_osfsosfm2",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV165ExtraWWContagemResultadoWWAceiteFaturamentoDS_30_Contagemresultado_datadmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV166ExtraWWContagemResultadoWWAceiteFaturamentoDS_31_Contagemresultado_datadmn_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV167ExtraWWContagemResultadoWWAceiteFaturamentoDS_32_Contagemresultado_dataaceite2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV168ExtraWWContagemResultadoWWAceiteFaturamentoDS_33_Contagemresultado_dataaceite_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV169ExtraWWContagemResultadoWWAceiteFaturamentoDS_34_Contagemresultado_sistemacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV170ExtraWWContagemResultadoWWAceiteFaturamentoDS_35_Contagemresultado_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV171ExtraWWContagemResultadoWWAceiteFaturamentoDS_36_Contagemresultado_contratadaorigemcod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV172ExtraWWContagemResultadoWWAceiteFaturamentoDS_37_Contagemresultado_naocnfdmncod2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV173ExtraWWContagemResultadoWWAceiteFaturamentoDS_38_Contagemresultado_loteaceite2",SqlDbType.Char,10,0} ,
          new Object[] {"@AV174ExtraWWContagemResultadoWWAceiteFaturamentoDS_39_Contagemresultado_demandafm_order2",SqlDbType.Decimal,18,0} ,
          new Object[] {"@AV176ExtraWWContagemResultadoWWAceiteFaturamentoDS_41_Contagemresultado_servico2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV177ExtraWWContagemResultadoWWAceiteFaturamentoDS_42_Contagemresultado_descricao2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV178ExtraWWContagemResultadoWWAceiteFaturamentoDS_43_Contagemresultado_agrupador2",SqlDbType.Char,15,0} ,
          new Object[] {"@AV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV184ExtraWWContagemResultadoWWAceiteFaturamentoDS_49_Contagemresultado_osfsosfm3",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV185ExtraWWContagemResultadoWWAceiteFaturamentoDS_50_Contagemresultado_datadmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV186ExtraWWContagemResultadoWWAceiteFaturamentoDS_51_Contagemresultado_datadmn_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV187ExtraWWContagemResultadoWWAceiteFaturamentoDS_52_Contagemresultado_dataaceite3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV188ExtraWWContagemResultadoWWAceiteFaturamentoDS_53_Contagemresultado_dataaceite_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV189ExtraWWContagemResultadoWWAceiteFaturamentoDS_54_Contagemresultado_sistemacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV190ExtraWWContagemResultadoWWAceiteFaturamentoDS_55_Contagemresultado_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV191ExtraWWContagemResultadoWWAceiteFaturamentoDS_56_Contagemresultado_contratadaorigemcod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV192ExtraWWContagemResultadoWWAceiteFaturamentoDS_57_Contagemresultado_naocnfdmncod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV193ExtraWWContagemResultadoWWAceiteFaturamentoDS_58_Contagemresultado_loteaceite3",SqlDbType.Char,10,0} ,
          new Object[] {"@AV194ExtraWWContagemResultadoWWAceiteFaturamentoDS_59_Contagemresultado_demandafm_order3",SqlDbType.Decimal,18,0} ,
          new Object[] {"@AV196ExtraWWContagemResultadoWWAceiteFaturamentoDS_61_Contagemresultado_servico3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV197ExtraWWContagemResultadoWWAceiteFaturamentoDS_62_Contagemresultado_descricao3",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV198ExtraWWContagemResultadoWWAceiteFaturamentoDS_63_Contagemresultado_agrupador3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV199ExtraWWContagemResultadoWWAceiteFaturamentoDS_64_Tfcontagemresultado_loteaceite",SqlDbType.Char,10,0} ,
          new Object[] {"@AV200ExtraWWContagemResultadoWWAceiteFaturamentoDS_65_Tfcontagemresultado_loteaceite_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV201ExtraWWContagemResultadoWWAceiteFaturamentoDS_66_Tfcontagemresultado_osfsosfm",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV202ExtraWWContagemResultadoWWAceiteFaturamentoDS_67_Tfcontagemresultado_osfsosfm_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV203ExtraWWContagemResultadoWWAceiteFaturamentoDS_68_Tfcontagemresultado_descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV204ExtraWWContagemResultadoWWAceiteFaturamentoDS_69_Tfcontagemresultado_descricao_sel",SqlDbType.VarChar,500,0} ,
          new Object[] {"@AV205ExtraWWContagemResultadoWWAceiteFaturamentoDS_70_Tfcontagemresultado_dataaceite",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV206ExtraWWContagemResultadoWWAceiteFaturamentoDS_71_Tfcontagemresultado_dataaceite_to",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV209ExtraWWContagemResultadoWWAceiteFaturamentoDS_74_Tfcontagemresultado_contratadaorigemsigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV210ExtraWWContagemResultadoWWAceiteFaturamentoDS_75_Tfcontagemresultado_contratadaorigemsigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV211ExtraWWContagemResultadoWWAceiteFaturamentoDS_76_Tfcontagemrresultado_sistemasigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV212ExtraWWContagemResultadoWWAceiteFaturamentoDS_77_Tfcontagemrresultado_sistemasigla_sel",SqlDbType.Char,25,0} ,
          new Object[] {"@lV215ExtraWWContagemResultadoWWAceiteFaturamentoDS_80_Tfcontagemresultado_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV216ExtraWWContagemResultadoWWAceiteFaturamentoDS_81_Tfcontagemresultado_servicosigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV219ExtraWWContagemResultadoWWAceiteFaturamentoDS_84_Tfcontagemresultado_valorpf",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV220ExtraWWContagemResultadoWWAceiteFaturamentoDS_85_Tfcontagemresultado_valorpf_to",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P003V3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003V3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((short[]) buf[4])[0] = rslt.getShort(3) ;
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 25) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((bool[]) buf[20])[0] = rslt.getBool(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((String[]) buf[22])[0] = rslt.getString(13, 10) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((int[]) buf[24])[0] = rslt.getInt(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((int[]) buf[26])[0] = rslt.getInt(15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((int[]) buf[28])[0] = rslt.getInt(16) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((DateTime[]) buf[30])[0] = rslt.getGXDateTime(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((DateTime[]) buf[32])[0] = rslt.getGXDate(18) ;
                ((String[]) buf[33])[0] = rslt.getString(19, 1) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(19);
                ((int[]) buf[35])[0] = rslt.getInt(20) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(20);
                ((int[]) buf[37])[0] = rslt.getInt(21) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(21);
                ((DateTime[]) buf[39])[0] = rslt.getGXDate(22) ;
                ((short[]) buf[40])[0] = rslt.getShort(23) ;
                ((String[]) buf[41])[0] = rslt.getVarchar(24) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(24);
                ((String[]) buf[43])[0] = rslt.getVarchar(25) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(25);
                ((int[]) buf[45])[0] = rslt.getInt(26) ;
                ((DateTime[]) buf[46])[0] = rslt.getGXDate(27) ;
                ((String[]) buf[47])[0] = rslt.getString(28, 5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[101]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[102]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[103]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[104]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[105]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[106]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[107]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[108]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[109]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[110]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[111]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[112]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[113]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[114]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[115]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[116]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[117]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[118]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (bool)parms[119]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[120]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[121]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[122]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[123]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[124]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[125]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[126]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[127]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[128]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[129]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[130]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[131]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[132]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[133]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[134]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[135]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[136]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[137]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[138]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[139]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[140]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[141]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[142]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[143]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[144]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[145]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[146]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[147]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[148]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[149]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[150]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[151]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[152]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[153]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[154]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[155]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[156]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[157]);
                }
                if ( (short)parms[57] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[158]);
                }
                if ( (short)parms[58] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[159]);
                }
                if ( (short)parms[59] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[160]);
                }
                if ( (short)parms[60] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[161]);
                }
                if ( (short)parms[61] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[162]);
                }
                if ( (short)parms[62] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[163]);
                }
                if ( (short)parms[63] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[164]);
                }
                if ( (short)parms[64] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[165]);
                }
                if ( (short)parms[65] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[166]);
                }
                if ( (short)parms[66] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[167]);
                }
                if ( (short)parms[67] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[168]);
                }
                if ( (short)parms[68] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[169]);
                }
                if ( (short)parms[69] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[170]);
                }
                if ( (short)parms[70] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[171]);
                }
                if ( (short)parms[71] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[172]);
                }
                if ( (short)parms[72] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[173]);
                }
                if ( (short)parms[73] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[174]);
                }
                if ( (short)parms[74] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[175]);
                }
                if ( (short)parms[75] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[176]);
                }
                if ( (short)parms[76] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[177]);
                }
                if ( (short)parms[77] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[178]);
                }
                if ( (short)parms[78] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[179]);
                }
                if ( (short)parms[79] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[180]);
                }
                if ( (short)parms[80] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[181]);
                }
                if ( (short)parms[81] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (long)parms[182]);
                }
                if ( (short)parms[82] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[183]);
                }
                if ( (short)parms[83] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[184]);
                }
                if ( (short)parms[84] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[185]);
                }
                if ( (short)parms[85] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[186]);
                }
                if ( (short)parms[86] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[187]);
                }
                if ( (short)parms[87] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[188]);
                }
                if ( (short)parms[88] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[189]);
                }
                if ( (short)parms[89] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[190]);
                }
                if ( (short)parms[90] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[191]);
                }
                if ( (short)parms[91] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[192]);
                }
                if ( (short)parms[92] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[193]);
                }
                if ( (short)parms[93] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[194]);
                }
                if ( (short)parms[94] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[195]);
                }
                if ( (short)parms[95] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[196]);
                }
                if ( (short)parms[96] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[197]);
                }
                if ( (short)parms[97] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[198]);
                }
                if ( (short)parms[98] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[199]);
                }
                if ( (short)parms[99] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[200]);
                }
                if ( (short)parms[100] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[201]);
                }
                return;
       }
    }

 }

}
