/*
               File: type_SdtSistema_Tecnologia
        Description: Sistema
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:16:38.96
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Sistema.Tecnologia" )]
   [XmlType(TypeName =  "Sistema.Tecnologia" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtSistema_Tecnologia : GxSilentTrnSdt, IGxSilentTrnGridItem
   {
      public SdtSistema_Tecnologia( )
      {
         /* Constructor for serialization */
         gxTv_SdtSistema_Tecnologia_Tecnologia_nome = "";
         gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia = "";
         gxTv_SdtSistema_Tecnologia_Mode = "";
         gxTv_SdtSistema_Tecnologia_Tecnologia_nome_Z = "";
         gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia_Z = "";
      }

      public SdtSistema_Tecnologia( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Tecnologia");
         metadata.Set("BT", "SistemaTecnologia");
         metadata.Set("PK", "[ \"SistemaTecnologiaLinha\" ]");
         metadata.Set("PKAssigned", "[ \"SistemaTecnologiaLinha\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Sistema_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"Tecnologia_Codigo\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Modified" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistematecnologialinha_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tecnologia_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tecnologia_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tecnologia_tipotecnologia_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tecnologia_tipotecnologia_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSistema_Tecnologia deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSistema_Tecnologia)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSistema_Tecnologia obj ;
         obj = this;
         obj.gxTpr_Sistematecnologialinha = deserialized.gxTpr_Sistematecnologialinha;
         obj.gxTpr_Tecnologia_codigo = deserialized.gxTpr_Tecnologia_codigo;
         obj.gxTpr_Tecnologia_nome = deserialized.gxTpr_Tecnologia_nome;
         obj.gxTpr_Tecnologia_tipotecnologia = deserialized.gxTpr_Tecnologia_tipotecnologia;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Modified = deserialized.gxTpr_Modified;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Sistematecnologialinha_Z = deserialized.gxTpr_Sistematecnologialinha_Z;
         obj.gxTpr_Tecnologia_codigo_Z = deserialized.gxTpr_Tecnologia_codigo_Z;
         obj.gxTpr_Tecnologia_nome_Z = deserialized.gxTpr_Tecnologia_nome_Z;
         obj.gxTpr_Tecnologia_tipotecnologia_Z = deserialized.gxTpr_Tecnologia_tipotecnologia_Z;
         obj.gxTpr_Tecnologia_tipotecnologia_N = deserialized.gxTpr_Tecnologia_tipotecnologia_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "SistemaTecnologiaLinha") )
               {
                  gxTv_SdtSistema_Tecnologia_Sistematecnologialinha = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tecnologia_Codigo") )
               {
                  gxTv_SdtSistema_Tecnologia_Tecnologia_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tecnologia_Nome") )
               {
                  gxTv_SdtSistema_Tecnologia_Tecnologia_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tecnologia_TipoTecnologia") )
               {
                  gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtSistema_Tecnologia_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Modified") )
               {
                  gxTv_SdtSistema_Tecnologia_Modified = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtSistema_Tecnologia_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SistemaTecnologiaLinha_Z") )
               {
                  gxTv_SdtSistema_Tecnologia_Sistematecnologialinha_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tecnologia_Codigo_Z") )
               {
                  gxTv_SdtSistema_Tecnologia_Tecnologia_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tecnologia_Nome_Z") )
               {
                  gxTv_SdtSistema_Tecnologia_Tecnologia_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tecnologia_TipoTecnologia_Z") )
               {
                  gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tecnologia_TipoTecnologia_N") )
               {
                  gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "Sistema.Tecnologia";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("SistemaTecnologiaLinha", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Tecnologia_Sistematecnologialinha), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Tecnologia_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Tecnologia_Tecnologia_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Tecnologia_Nome", StringUtil.RTrim( gxTv_SdtSistema_Tecnologia_Tecnologia_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Tecnologia_TipoTecnologia", StringUtil.RTrim( gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtSistema_Tecnologia_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Modified", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Tecnologia_Modified), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Tecnologia_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("SistemaTecnologiaLinha_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Tecnologia_Sistematecnologialinha_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Tecnologia_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Tecnologia_Tecnologia_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Tecnologia_Nome_Z", StringUtil.RTrim( gxTv_SdtSistema_Tecnologia_Tecnologia_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Tecnologia_TipoTecnologia_Z", StringUtil.RTrim( gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Tecnologia_TipoTecnologia_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("SistemaTecnologiaLinha", gxTv_SdtSistema_Tecnologia_Sistematecnologialinha, false);
         AddObjectProperty("Tecnologia_Codigo", gxTv_SdtSistema_Tecnologia_Tecnologia_codigo, false);
         AddObjectProperty("Tecnologia_Nome", gxTv_SdtSistema_Tecnologia_Tecnologia_nome, false);
         AddObjectProperty("Tecnologia_TipoTecnologia", gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtSistema_Tecnologia_Mode, false);
            AddObjectProperty("Modified", gxTv_SdtSistema_Tecnologia_Modified, false);
            AddObjectProperty("Initialized", gxTv_SdtSistema_Tecnologia_Initialized, false);
            AddObjectProperty("SistemaTecnologiaLinha_Z", gxTv_SdtSistema_Tecnologia_Sistematecnologialinha_Z, false);
            AddObjectProperty("Tecnologia_Codigo_Z", gxTv_SdtSistema_Tecnologia_Tecnologia_codigo_Z, false);
            AddObjectProperty("Tecnologia_Nome_Z", gxTv_SdtSistema_Tecnologia_Tecnologia_nome_Z, false);
            AddObjectProperty("Tecnologia_TipoTecnologia_Z", gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia_Z, false);
            AddObjectProperty("Tecnologia_TipoTecnologia_N", gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "SistemaTecnologiaLinha" )]
      [  XmlElement( ElementName = "SistemaTecnologiaLinha"   )]
      public short gxTpr_Sistematecnologialinha
      {
         get {
            return gxTv_SdtSistema_Tecnologia_Sistematecnologialinha ;
         }

         set {
            gxTv_SdtSistema_Tecnologia_Sistematecnologialinha = (short)(value);
            gxTv_SdtSistema_Tecnologia_Modified = 1;
         }

      }

      [  SoapElement( ElementName = "Tecnologia_Codigo" )]
      [  XmlElement( ElementName = "Tecnologia_Codigo"   )]
      public int gxTpr_Tecnologia_codigo
      {
         get {
            return gxTv_SdtSistema_Tecnologia_Tecnologia_codigo ;
         }

         set {
            gxTv_SdtSistema_Tecnologia_Tecnologia_codigo = (int)(value);
            gxTv_SdtSistema_Tecnologia_Modified = 1;
         }

      }

      [  SoapElement( ElementName = "Tecnologia_Nome" )]
      [  XmlElement( ElementName = "Tecnologia_Nome"   )]
      public String gxTpr_Tecnologia_nome
      {
         get {
            return gxTv_SdtSistema_Tecnologia_Tecnologia_nome ;
         }

         set {
            gxTv_SdtSistema_Tecnologia_Tecnologia_nome = (String)(value);
            gxTv_SdtSistema_Tecnologia_Modified = 1;
         }

      }

      [  SoapElement( ElementName = "Tecnologia_TipoTecnologia" )]
      [  XmlElement( ElementName = "Tecnologia_TipoTecnologia"   )]
      public String gxTpr_Tecnologia_tipotecnologia
      {
         get {
            return gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia ;
         }

         set {
            gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia_N = 0;
            gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia = (String)(value);
            gxTv_SdtSistema_Tecnologia_Modified = 1;
         }

      }

      public void gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia_SetNull( )
      {
         gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia_N = 1;
         gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia = "";
         return  ;
      }

      public bool gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtSistema_Tecnologia_Mode ;
         }

         set {
            gxTv_SdtSistema_Tecnologia_Mode = (String)(value);
         }

      }

      public void gxTv_SdtSistema_Tecnologia_Mode_SetNull( )
      {
         gxTv_SdtSistema_Tecnologia_Mode = "";
         return  ;
      }

      public bool gxTv_SdtSistema_Tecnologia_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Modified" )]
      [  XmlElement( ElementName = "Modified"   )]
      public short gxTpr_Modified
      {
         get {
            return gxTv_SdtSistema_Tecnologia_Modified ;
         }

         set {
            gxTv_SdtSistema_Tecnologia_Modified = (short)(value);
         }

      }

      public void gxTv_SdtSistema_Tecnologia_Modified_SetNull( )
      {
         gxTv_SdtSistema_Tecnologia_Modified = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Tecnologia_Modified_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtSistema_Tecnologia_Initialized ;
         }

         set {
            gxTv_SdtSistema_Tecnologia_Initialized = (short)(value);
            gxTv_SdtSistema_Tecnologia_Modified = 1;
         }

      }

      public void gxTv_SdtSistema_Tecnologia_Initialized_SetNull( )
      {
         gxTv_SdtSistema_Tecnologia_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Tecnologia_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "SistemaTecnologiaLinha_Z" )]
      [  XmlElement( ElementName = "SistemaTecnologiaLinha_Z"   )]
      public short gxTpr_Sistematecnologialinha_Z
      {
         get {
            return gxTv_SdtSistema_Tecnologia_Sistematecnologialinha_Z ;
         }

         set {
            gxTv_SdtSistema_Tecnologia_Sistematecnologialinha_Z = (short)(value);
            gxTv_SdtSistema_Tecnologia_Modified = 1;
         }

      }

      public void gxTv_SdtSistema_Tecnologia_Sistematecnologialinha_Z_SetNull( )
      {
         gxTv_SdtSistema_Tecnologia_Sistematecnologialinha_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Tecnologia_Sistematecnologialinha_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tecnologia_Codigo_Z" )]
      [  XmlElement( ElementName = "Tecnologia_Codigo_Z"   )]
      public int gxTpr_Tecnologia_codigo_Z
      {
         get {
            return gxTv_SdtSistema_Tecnologia_Tecnologia_codigo_Z ;
         }

         set {
            gxTv_SdtSistema_Tecnologia_Tecnologia_codigo_Z = (int)(value);
            gxTv_SdtSistema_Tecnologia_Modified = 1;
         }

      }

      public void gxTv_SdtSistema_Tecnologia_Tecnologia_codigo_Z_SetNull( )
      {
         gxTv_SdtSistema_Tecnologia_Tecnologia_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Tecnologia_Tecnologia_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tecnologia_Nome_Z" )]
      [  XmlElement( ElementName = "Tecnologia_Nome_Z"   )]
      public String gxTpr_Tecnologia_nome_Z
      {
         get {
            return gxTv_SdtSistema_Tecnologia_Tecnologia_nome_Z ;
         }

         set {
            gxTv_SdtSistema_Tecnologia_Tecnologia_nome_Z = (String)(value);
            gxTv_SdtSistema_Tecnologia_Modified = 1;
         }

      }

      public void gxTv_SdtSistema_Tecnologia_Tecnologia_nome_Z_SetNull( )
      {
         gxTv_SdtSistema_Tecnologia_Tecnologia_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtSistema_Tecnologia_Tecnologia_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tecnologia_TipoTecnologia_Z" )]
      [  XmlElement( ElementName = "Tecnologia_TipoTecnologia_Z"   )]
      public String gxTpr_Tecnologia_tipotecnologia_Z
      {
         get {
            return gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia_Z ;
         }

         set {
            gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia_Z = (String)(value);
            gxTv_SdtSistema_Tecnologia_Modified = 1;
         }

      }

      public void gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia_Z_SetNull( )
      {
         gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia_Z = "";
         return  ;
      }

      public bool gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Tecnologia_TipoTecnologia_N" )]
      [  XmlElement( ElementName = "Tecnologia_TipoTecnologia_N"   )]
      public short gxTpr_Tecnologia_tipotecnologia_N
      {
         get {
            return gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia_N ;
         }

         set {
            gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia_N = (short)(value);
            gxTv_SdtSistema_Tecnologia_Modified = 1;
         }

      }

      public void gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia_N_SetNull( )
      {
         gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia_N = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtSistema_Tecnologia_Tecnologia_nome = "";
         gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia = "";
         gxTv_SdtSistema_Tecnologia_Mode = "";
         gxTv_SdtSistema_Tecnologia_Tecnologia_nome_Z = "";
         gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia_Z = "";
         sTagName = "";
         return  ;
      }

      private short gxTv_SdtSistema_Tecnologia_Sistematecnologialinha ;
      private short gxTv_SdtSistema_Tecnologia_Modified ;
      private short gxTv_SdtSistema_Tecnologia_Initialized ;
      private short gxTv_SdtSistema_Tecnologia_Sistematecnologialinha_Z ;
      private short gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtSistema_Tecnologia_Tecnologia_codigo ;
      private int gxTv_SdtSistema_Tecnologia_Tecnologia_codigo_Z ;
      private String gxTv_SdtSistema_Tecnologia_Tecnologia_nome ;
      private String gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia ;
      private String gxTv_SdtSistema_Tecnologia_Mode ;
      private String gxTv_SdtSistema_Tecnologia_Tecnologia_nome_Z ;
      private String gxTv_SdtSistema_Tecnologia_Tecnologia_tipotecnologia_Z ;
      private String sTagName ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"Sistema.Tecnologia", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtSistema_Tecnologia_RESTInterface : GxGenericCollectionItem<SdtSistema_Tecnologia>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSistema_Tecnologia_RESTInterface( ) : base()
      {
      }

      public SdtSistema_Tecnologia_RESTInterface( SdtSistema_Tecnologia psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "SistemaTecnologiaLinha" , Order = 0 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Sistematecnologialinha
      {
         get {
            return sdt.gxTpr_Sistematecnologialinha ;
         }

         set {
            sdt.gxTpr_Sistematecnologialinha = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Tecnologia_Codigo" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Tecnologia_codigo
      {
         get {
            return sdt.gxTpr_Tecnologia_codigo ;
         }

         set {
            sdt.gxTpr_Tecnologia_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Tecnologia_Nome" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Tecnologia_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Tecnologia_nome) ;
         }

         set {
            sdt.gxTpr_Tecnologia_nome = (String)(value);
         }

      }

      [DataMember( Name = "Tecnologia_TipoTecnologia" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Tecnologia_tipotecnologia
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Tecnologia_tipotecnologia) ;
         }

         set {
            sdt.gxTpr_Tecnologia_tipotecnologia = (String)(value);
         }

      }

      public SdtSistema_Tecnologia sdt
      {
         get {
            return (SdtSistema_Tecnologia)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSistema_Tecnologia() ;
         }
      }

   }

}
