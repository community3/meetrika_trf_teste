/*
               File: ExportReportWWContagemResultadoCstm
        Description: Stub for ExportReportWWContagemResultadoCstm
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/17/2020 8:52:9.39
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class exportreportwwcontagemresultadocstm : GXProcedure
   {
      public exportreportwwcontagemresultadocstm( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public exportreportwwcontagemresultadocstm( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_AreaTrabalhoCod ,
                           int aP1_Contratada_Codigo ,
                           String aP2_TFContratada_AreaTrabalhoDes ,
                           String aP3_TFContratada_AreaTrabalhoDes_Sel ,
                           DateTime aP4_TFContagemResultado_DataDmn ,
                           DateTime aP5_TFContagemResultado_DataDmn_To ,
                           DateTime aP6_TFContagemResultado_DataUltCnt ,
                           DateTime aP7_TFContagemResultado_DataUltCnt_To ,
                           String aP8_TFContagemResultado_OsFsOsFm ,
                           String aP9_TFContagemResultado_OsFsOsFm_Sel ,
                           String aP10_TFContagemResultado_Descricao ,
                           String aP11_TFContagemResultado_Descricao_Sel ,
                           String aP12_TFContagemrResultado_SistemaSigla ,
                           String aP13_TFContagemrResultado_SistemaSigla_Sel ,
                           String aP14_TFContagemResultado_ContratadaSigla ,
                           String aP15_TFContagemResultado_ContratadaSigla_Sel ,
                           String aP16_TFContagemResultado_StatusDmn_SelsJson ,
                           String aP17_TFContagemResultado_StatusUltCnt_SelsJson ,
                           short aP18_TFContagemResultado_Baseline_Sel ,
                           String aP19_TFContagemResultado_Servico ,
                           int aP20_TFContagemResultado_Servico_Sel ,
                           String aP21_TFContagemResultado_Servico_SelDsc ,
                           decimal aP22_TFContagemResultado_PFBFSUltima ,
                           decimal aP23_TFContagemResultado_PFBFSUltima_To ,
                           decimal aP24_TFContagemResultado_PFLFSUltima ,
                           decimal aP25_TFContagemResultado_PFLFSUltima_To ,
                           decimal aP26_TFContagemResultado_PFBFMUltima ,
                           decimal aP27_TFContagemResultado_PFBFMUltima_To ,
                           decimal aP28_TFContagemResultado_PFLFMUltima ,
                           decimal aP29_TFContagemResultado_PFLFMUltima_To ,
                           String aP30_TFContagemResultado_EsforcoTotal ,
                           String aP31_TFContagemResultado_EsforcoTotal_Sel ,
                           decimal aP32_TFContagemResultado_PFFinal ,
                           decimal aP33_TFContagemResultado_PFFinal_To ,
                           short aP34_OrderedBy ,
                           bool aP35_OrderedDsc ,
                           String aP36_GridStateXML )
      {
         this.AV2Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV3Contratada_Codigo = aP1_Contratada_Codigo;
         this.AV4TFContratada_AreaTrabalhoDes = aP2_TFContratada_AreaTrabalhoDes;
         this.AV5TFContratada_AreaTrabalhoDes_Sel = aP3_TFContratada_AreaTrabalhoDes_Sel;
         this.AV6TFContagemResultado_DataDmn = aP4_TFContagemResultado_DataDmn;
         this.AV7TFContagemResultado_DataDmn_To = aP5_TFContagemResultado_DataDmn_To;
         this.AV8TFContagemResultado_DataUltCnt = aP6_TFContagemResultado_DataUltCnt;
         this.AV9TFContagemResultado_DataUltCnt_To = aP7_TFContagemResultado_DataUltCnt_To;
         this.AV10TFContagemResultado_OsFsOsFm = aP8_TFContagemResultado_OsFsOsFm;
         this.AV11TFContagemResultado_OsFsOsFm_Sel = aP9_TFContagemResultado_OsFsOsFm_Sel;
         this.AV12TFContagemResultado_Descricao = aP10_TFContagemResultado_Descricao;
         this.AV13TFContagemResultado_Descricao_Sel = aP11_TFContagemResultado_Descricao_Sel;
         this.AV14TFContagemrResultado_SistemaSigla = aP12_TFContagemrResultado_SistemaSigla;
         this.AV15TFContagemrResultado_SistemaSigla_Sel = aP13_TFContagemrResultado_SistemaSigla_Sel;
         this.AV16TFContagemResultado_ContratadaSigla = aP14_TFContagemResultado_ContratadaSigla;
         this.AV17TFContagemResultado_ContratadaSigla_Sel = aP15_TFContagemResultado_ContratadaSigla_Sel;
         this.AV18TFContagemResultado_StatusDmn_SelsJson = aP16_TFContagemResultado_StatusDmn_SelsJson;
         this.AV19TFContagemResultado_StatusUltCnt_SelsJson = aP17_TFContagemResultado_StatusUltCnt_SelsJson;
         this.AV20TFContagemResultado_Baseline_Sel = aP18_TFContagemResultado_Baseline_Sel;
         this.AV21TFContagemResultado_Servico = aP19_TFContagemResultado_Servico;
         this.AV22TFContagemResultado_Servico_Sel = aP20_TFContagemResultado_Servico_Sel;
         this.AV23TFContagemResultado_Servico_SelDsc = aP21_TFContagemResultado_Servico_SelDsc;
         this.AV24TFContagemResultado_PFBFSUltima = aP22_TFContagemResultado_PFBFSUltima;
         this.AV25TFContagemResultado_PFBFSUltima_To = aP23_TFContagemResultado_PFBFSUltima_To;
         this.AV26TFContagemResultado_PFLFSUltima = aP24_TFContagemResultado_PFLFSUltima;
         this.AV27TFContagemResultado_PFLFSUltima_To = aP25_TFContagemResultado_PFLFSUltima_To;
         this.AV28TFContagemResultado_PFBFMUltima = aP26_TFContagemResultado_PFBFMUltima;
         this.AV29TFContagemResultado_PFBFMUltima_To = aP27_TFContagemResultado_PFBFMUltima_To;
         this.AV30TFContagemResultado_PFLFMUltima = aP28_TFContagemResultado_PFLFMUltima;
         this.AV31TFContagemResultado_PFLFMUltima_To = aP29_TFContagemResultado_PFLFMUltima_To;
         this.AV32TFContagemResultado_EsforcoTotal = aP30_TFContagemResultado_EsforcoTotal;
         this.AV33TFContagemResultado_EsforcoTotal_Sel = aP31_TFContagemResultado_EsforcoTotal_Sel;
         this.AV34TFContagemResultado_PFFinal = aP32_TFContagemResultado_PFFinal;
         this.AV35TFContagemResultado_PFFinal_To = aP33_TFContagemResultado_PFFinal_To;
         this.AV36OrderedBy = aP34_OrderedBy;
         this.AV37OrderedDsc = aP35_OrderedDsc;
         this.AV38GridStateXML = aP36_GridStateXML;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Contratada_AreaTrabalhoCod ,
                                 int aP1_Contratada_Codigo ,
                                 String aP2_TFContratada_AreaTrabalhoDes ,
                                 String aP3_TFContratada_AreaTrabalhoDes_Sel ,
                                 DateTime aP4_TFContagemResultado_DataDmn ,
                                 DateTime aP5_TFContagemResultado_DataDmn_To ,
                                 DateTime aP6_TFContagemResultado_DataUltCnt ,
                                 DateTime aP7_TFContagemResultado_DataUltCnt_To ,
                                 String aP8_TFContagemResultado_OsFsOsFm ,
                                 String aP9_TFContagemResultado_OsFsOsFm_Sel ,
                                 String aP10_TFContagemResultado_Descricao ,
                                 String aP11_TFContagemResultado_Descricao_Sel ,
                                 String aP12_TFContagemrResultado_SistemaSigla ,
                                 String aP13_TFContagemrResultado_SistemaSigla_Sel ,
                                 String aP14_TFContagemResultado_ContratadaSigla ,
                                 String aP15_TFContagemResultado_ContratadaSigla_Sel ,
                                 String aP16_TFContagemResultado_StatusDmn_SelsJson ,
                                 String aP17_TFContagemResultado_StatusUltCnt_SelsJson ,
                                 short aP18_TFContagemResultado_Baseline_Sel ,
                                 String aP19_TFContagemResultado_Servico ,
                                 int aP20_TFContagemResultado_Servico_Sel ,
                                 String aP21_TFContagemResultado_Servico_SelDsc ,
                                 decimal aP22_TFContagemResultado_PFBFSUltima ,
                                 decimal aP23_TFContagemResultado_PFBFSUltima_To ,
                                 decimal aP24_TFContagemResultado_PFLFSUltima ,
                                 decimal aP25_TFContagemResultado_PFLFSUltima_To ,
                                 decimal aP26_TFContagemResultado_PFBFMUltima ,
                                 decimal aP27_TFContagemResultado_PFBFMUltima_To ,
                                 decimal aP28_TFContagemResultado_PFLFMUltima ,
                                 decimal aP29_TFContagemResultado_PFLFMUltima_To ,
                                 String aP30_TFContagemResultado_EsforcoTotal ,
                                 String aP31_TFContagemResultado_EsforcoTotal_Sel ,
                                 decimal aP32_TFContagemResultado_PFFinal ,
                                 decimal aP33_TFContagemResultado_PFFinal_To ,
                                 short aP34_OrderedBy ,
                                 bool aP35_OrderedDsc ,
                                 String aP36_GridStateXML )
      {
         exportreportwwcontagemresultadocstm objexportreportwwcontagemresultadocstm;
         objexportreportwwcontagemresultadocstm = new exportreportwwcontagemresultadocstm();
         objexportreportwwcontagemresultadocstm.AV2Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         objexportreportwwcontagemresultadocstm.AV3Contratada_Codigo = aP1_Contratada_Codigo;
         objexportreportwwcontagemresultadocstm.AV4TFContratada_AreaTrabalhoDes = aP2_TFContratada_AreaTrabalhoDes;
         objexportreportwwcontagemresultadocstm.AV5TFContratada_AreaTrabalhoDes_Sel = aP3_TFContratada_AreaTrabalhoDes_Sel;
         objexportreportwwcontagemresultadocstm.AV6TFContagemResultado_DataDmn = aP4_TFContagemResultado_DataDmn;
         objexportreportwwcontagemresultadocstm.AV7TFContagemResultado_DataDmn_To = aP5_TFContagemResultado_DataDmn_To;
         objexportreportwwcontagemresultadocstm.AV8TFContagemResultado_DataUltCnt = aP6_TFContagemResultado_DataUltCnt;
         objexportreportwwcontagemresultadocstm.AV9TFContagemResultado_DataUltCnt_To = aP7_TFContagemResultado_DataUltCnt_To;
         objexportreportwwcontagemresultadocstm.AV10TFContagemResultado_OsFsOsFm = aP8_TFContagemResultado_OsFsOsFm;
         objexportreportwwcontagemresultadocstm.AV11TFContagemResultado_OsFsOsFm_Sel = aP9_TFContagemResultado_OsFsOsFm_Sel;
         objexportreportwwcontagemresultadocstm.AV12TFContagemResultado_Descricao = aP10_TFContagemResultado_Descricao;
         objexportreportwwcontagemresultadocstm.AV13TFContagemResultado_Descricao_Sel = aP11_TFContagemResultado_Descricao_Sel;
         objexportreportwwcontagemresultadocstm.AV14TFContagemrResultado_SistemaSigla = aP12_TFContagemrResultado_SistemaSigla;
         objexportreportwwcontagemresultadocstm.AV15TFContagemrResultado_SistemaSigla_Sel = aP13_TFContagemrResultado_SistemaSigla_Sel;
         objexportreportwwcontagemresultadocstm.AV16TFContagemResultado_ContratadaSigla = aP14_TFContagemResultado_ContratadaSigla;
         objexportreportwwcontagemresultadocstm.AV17TFContagemResultado_ContratadaSigla_Sel = aP15_TFContagemResultado_ContratadaSigla_Sel;
         objexportreportwwcontagemresultadocstm.AV18TFContagemResultado_StatusDmn_SelsJson = aP16_TFContagemResultado_StatusDmn_SelsJson;
         objexportreportwwcontagemresultadocstm.AV19TFContagemResultado_StatusUltCnt_SelsJson = aP17_TFContagemResultado_StatusUltCnt_SelsJson;
         objexportreportwwcontagemresultadocstm.AV20TFContagemResultado_Baseline_Sel = aP18_TFContagemResultado_Baseline_Sel;
         objexportreportwwcontagemresultadocstm.AV21TFContagemResultado_Servico = aP19_TFContagemResultado_Servico;
         objexportreportwwcontagemresultadocstm.AV22TFContagemResultado_Servico_Sel = aP20_TFContagemResultado_Servico_Sel;
         objexportreportwwcontagemresultadocstm.AV23TFContagemResultado_Servico_SelDsc = aP21_TFContagemResultado_Servico_SelDsc;
         objexportreportwwcontagemresultadocstm.AV24TFContagemResultado_PFBFSUltima = aP22_TFContagemResultado_PFBFSUltima;
         objexportreportwwcontagemresultadocstm.AV25TFContagemResultado_PFBFSUltima_To = aP23_TFContagemResultado_PFBFSUltima_To;
         objexportreportwwcontagemresultadocstm.AV26TFContagemResultado_PFLFSUltima = aP24_TFContagemResultado_PFLFSUltima;
         objexportreportwwcontagemresultadocstm.AV27TFContagemResultado_PFLFSUltima_To = aP25_TFContagemResultado_PFLFSUltima_To;
         objexportreportwwcontagemresultadocstm.AV28TFContagemResultado_PFBFMUltima = aP26_TFContagemResultado_PFBFMUltima;
         objexportreportwwcontagemresultadocstm.AV29TFContagemResultado_PFBFMUltima_To = aP27_TFContagemResultado_PFBFMUltima_To;
         objexportreportwwcontagemresultadocstm.AV30TFContagemResultado_PFLFMUltima = aP28_TFContagemResultado_PFLFMUltima;
         objexportreportwwcontagemresultadocstm.AV31TFContagemResultado_PFLFMUltima_To = aP29_TFContagemResultado_PFLFMUltima_To;
         objexportreportwwcontagemresultadocstm.AV32TFContagemResultado_EsforcoTotal = aP30_TFContagemResultado_EsforcoTotal;
         objexportreportwwcontagemresultadocstm.AV33TFContagemResultado_EsforcoTotal_Sel = aP31_TFContagemResultado_EsforcoTotal_Sel;
         objexportreportwwcontagemresultadocstm.AV34TFContagemResultado_PFFinal = aP32_TFContagemResultado_PFFinal;
         objexportreportwwcontagemresultadocstm.AV35TFContagemResultado_PFFinal_To = aP33_TFContagemResultado_PFFinal_To;
         objexportreportwwcontagemresultadocstm.AV36OrderedBy = aP34_OrderedBy;
         objexportreportwwcontagemresultadocstm.AV37OrderedDsc = aP35_OrderedDsc;
         objexportreportwwcontagemresultadocstm.AV38GridStateXML = aP36_GridStateXML;
         objexportreportwwcontagemresultadocstm.context.SetSubmitInitialConfig(context);
         objexportreportwwcontagemresultadocstm.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objexportreportwwcontagemresultadocstm);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((exportreportwwcontagemresultadocstm)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(int)AV2Contratada_AreaTrabalhoCod,(int)AV3Contratada_Codigo,(String)AV4TFContratada_AreaTrabalhoDes,(String)AV5TFContratada_AreaTrabalhoDes_Sel,(DateTime)AV6TFContagemResultado_DataDmn,(DateTime)AV7TFContagemResultado_DataDmn_To,(DateTime)AV8TFContagemResultado_DataUltCnt,(DateTime)AV9TFContagemResultado_DataUltCnt_To,(String)AV10TFContagemResultado_OsFsOsFm,(String)AV11TFContagemResultado_OsFsOsFm_Sel,(String)AV12TFContagemResultado_Descricao,(String)AV13TFContagemResultado_Descricao_Sel,(String)AV14TFContagemrResultado_SistemaSigla,(String)AV15TFContagemrResultado_SistemaSigla_Sel,(String)AV16TFContagemResultado_ContratadaSigla,(String)AV17TFContagemResultado_ContratadaSigla_Sel,(String)AV18TFContagemResultado_StatusDmn_SelsJson,(String)AV19TFContagemResultado_StatusUltCnt_SelsJson,(short)AV20TFContagemResultado_Baseline_Sel,(String)AV21TFContagemResultado_Servico,(int)AV22TFContagemResultado_Servico_Sel,(String)AV23TFContagemResultado_Servico_SelDsc,(decimal)AV24TFContagemResultado_PFBFSUltima,(decimal)AV25TFContagemResultado_PFBFSUltima_To,(decimal)AV26TFContagemResultado_PFLFSUltima,(decimal)AV27TFContagemResultado_PFLFSUltima_To,(decimal)AV28TFContagemResultado_PFBFMUltima,(decimal)AV29TFContagemResultado_PFBFMUltima_To,(decimal)AV30TFContagemResultado_PFLFMUltima,(decimal)AV31TFContagemResultado_PFLFMUltima_To,(String)AV32TFContagemResultado_EsforcoTotal,(String)AV33TFContagemResultado_EsforcoTotal_Sel,(decimal)AV34TFContagemResultado_PFFinal,(decimal)AV35TFContagemResultado_PFFinal_To,(short)AV36OrderedBy,(bool)AV37OrderedDsc,(String)AV38GridStateXML} ;
         ClassLoader.Execute("aexportreportwwcontagemresultadocstm","GeneXus.Programs.aexportreportwwcontagemresultadocstm", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 37 ) )
         {
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV20TFContagemResultado_Baseline_Sel ;
      private short AV36OrderedBy ;
      private int AV2Contratada_AreaTrabalhoCod ;
      private int AV3Contratada_Codigo ;
      private int AV22TFContagemResultado_Servico_Sel ;
      private decimal AV24TFContagemResultado_PFBFSUltima ;
      private decimal AV25TFContagemResultado_PFBFSUltima_To ;
      private decimal AV26TFContagemResultado_PFLFSUltima ;
      private decimal AV27TFContagemResultado_PFLFSUltima_To ;
      private decimal AV28TFContagemResultado_PFBFMUltima ;
      private decimal AV29TFContagemResultado_PFBFMUltima_To ;
      private decimal AV30TFContagemResultado_PFLFMUltima ;
      private decimal AV31TFContagemResultado_PFLFMUltima_To ;
      private decimal AV34TFContagemResultado_PFFinal ;
      private decimal AV35TFContagemResultado_PFFinal_To ;
      private String AV14TFContagemrResultado_SistemaSigla ;
      private String AV15TFContagemrResultado_SistemaSigla_Sel ;
      private String AV16TFContagemResultado_ContratadaSigla ;
      private String AV17TFContagemResultado_ContratadaSigla_Sel ;
      private String AV21TFContagemResultado_Servico ;
      private String AV32TFContagemResultado_EsforcoTotal ;
      private String AV33TFContagemResultado_EsforcoTotal_Sel ;
      private DateTime AV6TFContagemResultado_DataDmn ;
      private DateTime AV7TFContagemResultado_DataDmn_To ;
      private DateTime AV8TFContagemResultado_DataUltCnt ;
      private DateTime AV9TFContagemResultado_DataUltCnt_To ;
      private bool AV37OrderedDsc ;
      private String AV18TFContagemResultado_StatusDmn_SelsJson ;
      private String AV19TFContagemResultado_StatusUltCnt_SelsJson ;
      private String AV38GridStateXML ;
      private String AV4TFContratada_AreaTrabalhoDes ;
      private String AV5TFContratada_AreaTrabalhoDes_Sel ;
      private String AV10TFContagemResultado_OsFsOsFm ;
      private String AV11TFContagemResultado_OsFsOsFm_Sel ;
      private String AV12TFContagemResultado_Descricao ;
      private String AV13TFContagemResultado_Descricao_Sel ;
      private String AV23TFContagemResultado_Servico_SelDsc ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private Object[] args ;
   }

}
