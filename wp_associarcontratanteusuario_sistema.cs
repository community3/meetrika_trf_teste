/*
               File: WP_AssociarContratanteUsuario_Sistema
        Description: Associar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:43:48.22
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_associarcontratanteusuario_sistema : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public wp_associarcontratanteusuario_sistema( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_associarcontratanteusuario_sistema( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratanteUsuario_ContratanteCod ,
                           int aP1_ContratanteUsuario_UsuarioCod )
      {
         this.AV10ContratanteUsuario_ContratanteCod = aP0_ContratanteUsuario_ContratanteCod;
         this.AV11ContratanteUsuario_UsuarioCod = aP1_ContratanteUsuario_UsuarioCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV10ContratanteUsuario_ContratanteCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10ContratanteUsuario_ContratanteCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATANTEUSUARIO_CONTRATANTECOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV10ContratanteUsuario_ContratanteCod), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV11ContratanteUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11ContratanteUsuario_UsuarioCod), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATANTEUSUARIO_USUARIOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11ContratanteUsuario_UsuarioCod), "ZZZZZ9")));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAMX2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               edtavContratanteusuario_usuariopessoanom_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratanteusuario_usuariopessoanom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratanteusuario_usuariopessoanom_Enabled), 5, 0)));
               WSMX2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEMX2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( "Associar") ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299434829");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery-1.4.2.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.core.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.widget.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.mouse.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.selectable.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.corner.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery-1.4.2.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.core.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.widget.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.mouse.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.selectable.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.corner.js", "");
         context.AddJavascriptSource("jQueryUI/jqSelectRender.js", "");
         context.AddJavascriptSource("jQueryUI/jqSelectRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_associarcontratanteusuario_sistema.aspx") + "?" + UrlEncode("" +AV10ContratanteUsuario_ContratanteCod) + "," + UrlEncode("" +AV11ContratanteUsuario_UsuarioCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vJQNAOASSOCIADOS", AV32jqNaoAssociados);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vJQNAOASSOCIADOS", AV32jqNaoAssociados);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vJQASSOCIADOS", AV30jqAssociados);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vJQASSOCIADOS", AV30jqAssociados);
         }
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_CONTRATANTECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATANTEUSUARIO_CONTRATANTECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV10ContratanteUsuario_ContratanteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATANTEUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11ContratanteUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMENUPERFIL", AV35MenuPerfil);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMENUPERFIL", AV35MenuPerfil);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vALL", AV29All);
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATANTEUSUARIO_USUARIOPESSOANOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV36ContratanteUsuario_UsuarioPessoaNom, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATANTEUSUARIO_USUARIOPESSOANOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV36ContratanteUsuario_UsuarioPessoaNom, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATANTEUSUARIO_USUARIOPESSOANOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV36ContratanteUsuario_UsuarioPessoaNom, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATANTEUSUARIO_CONTRATANTECOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV10ContratanteUsuario_ContratanteCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATANTEUSUARIO_USUARIOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11ContratanteUsuario_UsuarioCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATANTEUSUARIO_CONTRATANTECOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV10ContratanteUsuario_ContratanteCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATANTEUSUARIO_USUARIOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11ContratanteUsuario_UsuarioCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Width", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Width), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Height", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Height), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Rounding", StringUtil.RTrim( Jqnaoassociados_Rounding));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Theme", StringUtil.RTrim( Jqnaoassociados_Theme));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Unselectedicon", StringUtil.RTrim( Jqnaoassociados_Unselectedicon));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Selectedcolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Selectedcolor), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Itemwidth", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Itemwidth), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Itemheight", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Itemheight), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Width", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Width), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Height", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Height), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Rounding", StringUtil.RTrim( Jqassociados_Rounding));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Theme", StringUtil.RTrim( Jqassociados_Theme));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Readonly", StringUtil.BoolToStr( Jqassociados_Readonly));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Unselectedicon", StringUtil.RTrim( Jqassociados_Unselectedicon));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Selectedcolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Selectedcolor), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Itemwidth", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Itemwidth), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Itemheight", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Itemheight), 9, 0, ".", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormMX2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "WP_AssociarContratanteUsuario_Sistema" ;
      }

      public override String GetPgmdesc( )
      {
         return "Associar" ;
      }

      protected void WBMX0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            wb_table1_2_MX2( true) ;
         }
         else
         {
            wb_table1_2_MX2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_MX2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTMX2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Associar", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPMX0( ) ;
      }

      protected void WSMX2( )
      {
         STARTMX2( ) ;
         EVTMX2( ) ;
      }

      protected void EVTMX2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11MX2 */
                           E11MX2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12MX2 */
                           E12MX2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                                 /* Execute user event: E13MX2 */
                                 E13MX2 ();
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14MX2 */
                           E14MX2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15MX2 */
                           E15MX2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16MX2 */
                           E16MX2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17MX2 */
                           E17MX2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18MX2 */
                           E18MX2 ();
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEMX2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormMX2( ) ;
            }
         }
      }

      protected void PAMX2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavContratanteusuario_usuariopessoanom_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFMX2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavContratanteusuario_usuariopessoanom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratanteusuario_usuariopessoanom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratanteusuario_usuariopessoanom_Enabled), 5, 0)));
      }

      protected void RFMX2( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E12MX2 */
         E12MX2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E18MX2 */
            E18MX2 ();
            WBMX0( ) ;
         }
      }

      protected void STRUPMX0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavContratanteusuario_usuariopessoanom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratanteusuario_usuariopessoanom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratanteusuario_usuariopessoanom_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11MX2 */
         E11MX2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vJQNAOASSOCIADOS"), AV32jqNaoAssociados);
            ajax_req_read_hidden_sdt(cgiGet( "vJQASSOCIADOS"), AV30jqAssociados);
            /* Read variables values. */
            AV36ContratanteUsuario_UsuarioPessoaNom = StringUtil.Upper( cgiGet( edtavContratanteusuario_usuariopessoanom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ContratanteUsuario_UsuarioPessoaNom", AV36ContratanteUsuario_UsuarioPessoaNom);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATANTEUSUARIO_USUARIOPESSOANOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV36ContratanteUsuario_UsuarioPessoaNom, "@!"))));
            /* Read saved values. */
            Jqnaoassociados_Width = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Width"), ",", "."));
            Jqnaoassociados_Height = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Height"), ",", "."));
            Jqnaoassociados_Rounding = cgiGet( "JQNAOASSOCIADOS_Rounding");
            Jqnaoassociados_Theme = cgiGet( "JQNAOASSOCIADOS_Theme");
            Jqnaoassociados_Unselectedicon = cgiGet( "JQNAOASSOCIADOS_Unselectedicon");
            Jqnaoassociados_Selectedcolor = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Selectedcolor"), ",", "."));
            Jqnaoassociados_Itemwidth = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Itemwidth"), ",", "."));
            Jqnaoassociados_Itemheight = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Itemheight"), ",", "."));
            Jqassociados_Width = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Width"), ",", "."));
            Jqassociados_Height = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Height"), ",", "."));
            Jqassociados_Rounding = cgiGet( "JQASSOCIADOS_Rounding");
            Jqassociados_Theme = cgiGet( "JQASSOCIADOS_Theme");
            Jqassociados_Readonly = StringUtil.StrToBool( cgiGet( "JQASSOCIADOS_Readonly"));
            Jqassociados_Unselectedicon = cgiGet( "JQASSOCIADOS_Unselectedicon");
            Jqassociados_Selectedcolor = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Selectedcolor"), ",", "."));
            Jqassociados_Itemwidth = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Itemwidth"), ",", "."));
            Jqassociados_Itemheight = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Itemheight"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11MX2 */
         E11MX2 ();
         if (returnInSub) return;
      }

      protected void E11MX2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV27WWPContext) ;
         if ( StringUtil.StrCmp(AV15HTTPRequest.Method, "GET") == 0 )
         {
            AV39GXLvl5 = 0;
            /* Using cursor H00MX3 */
            pr_default.execute(0, new Object[] {AV10ContratanteUsuario_ContratanteCod, AV11ContratanteUsuario_UsuarioCod});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A61ContratanteUsuario_UsuarioPessoaCod = H00MX3_A61ContratanteUsuario_UsuarioPessoaCod[0];
               n61ContratanteUsuario_UsuarioPessoaCod = H00MX3_n61ContratanteUsuario_UsuarioPessoaCod[0];
               A60ContratanteUsuario_UsuarioCod = H00MX3_A60ContratanteUsuario_UsuarioCod[0];
               A63ContratanteUsuario_ContratanteCod = H00MX3_A63ContratanteUsuario_ContratanteCod[0];
               A62ContratanteUsuario_UsuarioPessoaNom = H00MX3_A62ContratanteUsuario_UsuarioPessoaNom[0];
               n62ContratanteUsuario_UsuarioPessoaNom = H00MX3_n62ContratanteUsuario_UsuarioPessoaNom[0];
               A1020ContratanteUsuario_AreaTrabalhoCod = H00MX3_A1020ContratanteUsuario_AreaTrabalhoCod[0];
               n1020ContratanteUsuario_AreaTrabalhoCod = H00MX3_n1020ContratanteUsuario_AreaTrabalhoCod[0];
               A61ContratanteUsuario_UsuarioPessoaCod = H00MX3_A61ContratanteUsuario_UsuarioPessoaCod[0];
               n61ContratanteUsuario_UsuarioPessoaCod = H00MX3_n61ContratanteUsuario_UsuarioPessoaCod[0];
               A62ContratanteUsuario_UsuarioPessoaNom = H00MX3_A62ContratanteUsuario_UsuarioPessoaNom[0];
               n62ContratanteUsuario_UsuarioPessoaNom = H00MX3_n62ContratanteUsuario_UsuarioPessoaNom[0];
               A1020ContratanteUsuario_AreaTrabalhoCod = H00MX3_A1020ContratanteUsuario_AreaTrabalhoCod[0];
               n1020ContratanteUsuario_AreaTrabalhoCod = H00MX3_n1020ContratanteUsuario_AreaTrabalhoCod[0];
               AV39GXLvl5 = 1;
               AV28AreaTrabalho = A1020ContratanteUsuario_AreaTrabalhoCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28AreaTrabalho", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28AreaTrabalho), 6, 0)));
               AV36ContratanteUsuario_UsuarioPessoaNom = A62ContratanteUsuario_UsuarioPessoaNom;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ContratanteUsuario_UsuarioPessoaNom", AV36ContratanteUsuario_UsuarioPessoaNom);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATANTEUSUARIO_USUARIOPESSOANOM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV36ContratanteUsuario_UsuarioPessoaNom, "@!"))));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            if ( AV39GXLvl5 == 0 )
            {
               GX_msglist.addItem("Usu�rio da Cpntratante n�o encontrado.");
            }
            /* Using cursor H00MX4 */
            pr_default.execute(1, new Object[] {AV28AreaTrabalho});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A135Sistema_AreaTrabalhoCod = H00MX4_A135Sistema_AreaTrabalhoCod[0];
               A699Sistema_Tipo = H00MX4_A699Sistema_Tipo[0];
               n699Sistema_Tipo = H00MX4_n699Sistema_Tipo[0];
               A127Sistema_Codigo = H00MX4_A127Sistema_Codigo[0];
               A130Sistema_Ativo = H00MX4_A130Sistema_Ativo[0];
               A129Sistema_Sigla = H00MX4_A129Sistema_Sigla[0];
               AV14Exist = false;
               /* Using cursor H00MX5 */
               pr_default.execute(2, new Object[] {AV10ContratanteUsuario_ContratanteCod, AV11ContratanteUsuario_UsuarioCod, A127Sistema_Codigo});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A60ContratanteUsuario_UsuarioCod = H00MX5_A60ContratanteUsuario_UsuarioCod[0];
                  A63ContratanteUsuario_ContratanteCod = H00MX5_A63ContratanteUsuario_ContratanteCod[0];
                  AV14Exist = true;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(2);
               if ( AV14Exist )
               {
                  AV31jqItem = new SdtjqSelectData_Item(context);
                  AV31jqItem.gxTpr_Id = StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0);
                  AV31jqItem.gxTpr_Descr = A129Sistema_Sigla;
                  AV31jqItem.gxTpr_Selected = false;
                  AV30jqAssociados.Add(AV31jqItem, 0);
               }
               else
               {
                  AV31jqItem = new SdtjqSelectData_Item(context);
                  AV31jqItem.gxTpr_Id = StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0);
                  AV31jqItem.gxTpr_Descr = A129Sistema_Sigla;
                  AV31jqItem.gxTpr_Selected = false;
                  AV32jqNaoAssociados.Add(AV31jqItem, 0);
               }
               pr_default.readNext(1);
            }
            pr_default.close(1);
         }
      }

      protected void E12MX2( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV27WWPContext) ;
         imgImageassociateselected_Visible = (AV27WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateselected_Visible), 5, 0)));
         imgImageassociateall_Visible = (AV27WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateall_Visible), 5, 0)));
         imgImagedisassociateselected_Visible = (AV27WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateselected_Visible), 5, 0)));
         imgImagedisassociateall_Visible = (AV27WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateall_Visible), 5, 0)));
         bttBtn_confirm_Visible = (AV27WWPContext.gxTpr_Insert||AV27WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_confirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_confirm_Visible), 5, 0)));
      }

      public void GXEnter( )
      {
         /* Execute user event: E13MX2 */
         E13MX2 ();
         if (returnInSub) return;
      }

      protected void E13MX2( )
      {
         /* Enter Routine */
         AV26Success = true;
         AV42GXV1 = 1;
         while ( AV42GXV1 <= AV30jqAssociados.Count )
         {
            AV31jqItem = ((SdtjqSelectData_Item)AV30jqAssociados.Item(AV42GXV1));
            if ( AV26Success )
            {
               AV25Sistema_Codigo = (int)(NumberUtil.Val( AV31jqItem.gxTpr_Id, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25Sistema_Codigo), 6, 0)));
               AV43GXLvl62 = 0;
               /* Using cursor H00MX6 */
               pr_default.execute(3, new Object[] {AV10ContratanteUsuario_ContratanteCod, AV11ContratanteUsuario_UsuarioCod, AV25Sistema_Codigo});
               while ( (pr_default.getStatus(3) != 101) )
               {
                  A127Sistema_Codigo = H00MX6_A127Sistema_Codigo[0];
                  A60ContratanteUsuario_UsuarioCod = H00MX6_A60ContratanteUsuario_UsuarioCod[0];
                  A63ContratanteUsuario_ContratanteCod = H00MX6_A63ContratanteUsuario_ContratanteCod[0];
                  AV43GXLvl62 = 1;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(3);
               if ( AV43GXLvl62 == 0 )
               {
                  AV12ContratanteUsuarioSistema = new SdtContratanteUsuarioSistema(context);
                  AV12ContratanteUsuarioSistema.gxTpr_Contratanteusuario_contratantecod = AV10ContratanteUsuario_ContratanteCod;
                  AV12ContratanteUsuarioSistema.gxTpr_Contratanteusuario_usuariocod = AV11ContratanteUsuario_UsuarioCod;
                  AV12ContratanteUsuarioSistema.gxTpr_Sistema_codigo = AV25Sistema_Codigo;
                  AV12ContratanteUsuarioSistema.Save();
                  if ( ! AV12ContratanteUsuarioSistema.Success() )
                  {
                     AV26Success = false;
                  }
               }
            }
            AV42GXV1 = (int)(AV42GXV1+1);
         }
         AV44GXV2 = 1;
         while ( AV44GXV2 <= AV32jqNaoAssociados.Count )
         {
            AV31jqItem = ((SdtjqSelectData_Item)AV32jqNaoAssociados.Item(AV44GXV2));
            if ( AV26Success )
            {
               AV25Sistema_Codigo = (int)(NumberUtil.Val( AV31jqItem.gxTpr_Id, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25Sistema_Codigo), 6, 0)));
               /* Using cursor H00MX7 */
               pr_default.execute(4, new Object[] {AV10ContratanteUsuario_ContratanteCod, AV11ContratanteUsuario_UsuarioCod, AV25Sistema_Codigo});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A127Sistema_Codigo = H00MX7_A127Sistema_Codigo[0];
                  A60ContratanteUsuario_UsuarioCod = H00MX7_A60ContratanteUsuario_UsuarioCod[0];
                  A63ContratanteUsuario_ContratanteCod = H00MX7_A63ContratanteUsuario_ContratanteCod[0];
                  AV12ContratanteUsuarioSistema = new SdtContratanteUsuarioSistema(context);
                  AV12ContratanteUsuarioSistema.Load(AV10ContratanteUsuario_ContratanteCod, AV11ContratanteUsuario_UsuarioCod, AV25Sistema_Codigo);
                  if ( AV12ContratanteUsuarioSistema.Success() )
                  {
                     AV12ContratanteUsuarioSistema.Delete();
                  }
                  if ( ! AV12ContratanteUsuarioSistema.Success() )
                  {
                     AV26Success = false;
                  }
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(4);
            }
            AV44GXV2 = (int)(AV44GXV2+1);
         }
         if ( AV26Success )
         {
            context.CommitDataStores( "WP_AssociarContratanteUsuario_Sistema");
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         else
         {
            context.RollbackDataStores( "WP_AssociarContratanteUsuario_Sistema");
            /* Execute user subroutine: 'SHOW ERROR MESSAGES' */
            S112 ();
            if (returnInSub) return;
         }
      }

      protected void E14MX2( )
      {
         /* 'Disassociate Selected' Routine */
         bttBtn_cancel_Caption = "Cancelar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_cancel_Internalname, "Caption", bttBtn_cancel_Caption);
         AV29All = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29All", AV29All);
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S122 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV30jqAssociados", AV30jqAssociados);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV32jqNaoAssociados", AV32jqNaoAssociados);
      }

      protected void E15MX2( )
      {
         /* 'Associate selected' Routine */
         bttBtn_cancel_Caption = "Cancelar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_cancel_Internalname, "Caption", bttBtn_cancel_Caption);
         AV29All = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29All", AV29All);
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S132 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV32jqNaoAssociados", AV32jqNaoAssociados);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV30jqAssociados", AV30jqAssociados);
      }

      protected void E16MX2( )
      {
         /* 'Associate All' Routine */
         bttBtn_cancel_Caption = "Cancelar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_cancel_Internalname, "Caption", bttBtn_cancel_Caption);
         AV29All = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29All", AV29All);
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S132 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV32jqNaoAssociados", AV32jqNaoAssociados);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV30jqAssociados", AV30jqAssociados);
      }

      protected void E17MX2( )
      {
         /* 'Disassociate All' Routine */
         bttBtn_cancel_Caption = "Cancelar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_cancel_Internalname, "Caption", bttBtn_cancel_Caption);
         AV29All = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29All", AV29All);
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S122 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV30jqAssociados", AV30jqAssociados);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV32jqNaoAssociados", AV32jqNaoAssociados);
      }

      protected void S112( )
      {
         /* 'SHOW ERROR MESSAGES' Routine */
         AV47GXV4 = 1;
         AV46GXV3 = AV35MenuPerfil.GetMessages();
         while ( AV47GXV4 <= AV46GXV3.Count )
         {
            AV19Message = ((SdtMessages_Message)AV46GXV3.Item(AV47GXV4));
            if ( AV19Message.gxTpr_Type == 1 )
            {
               GX_msglist.addItem(AV19Message.gxTpr_Description);
            }
            AV47GXV4 = (int)(AV47GXV4+1);
         }
      }

      protected void S132( )
      {
         /* 'ASSOCIATESELECTED' Routine */
         AV16i = 1;
         while ( AV16i <= AV32jqNaoAssociados.Count )
         {
            if ( ((SdtjqSelectData_Item)AV32jqNaoAssociados.Item(AV16i)).gxTpr_Selected || AV29All )
            {
               ((SdtjqSelectData_Item)AV32jqNaoAssociados.Item(AV16i)).gxTpr_Selected = false;
               AV30jqAssociados.Add(((SdtjqSelectData_Item)AV32jqNaoAssociados.Item(AV16i)), 0);
               AV32jqNaoAssociados.RemoveItem(AV16i);
               AV16i = (int)(AV16i-1);
            }
            AV16i = (int)(AV16i+1);
         }
         /* Execute user subroutine: 'TITLES' */
         S142 ();
         if (returnInSub) return;
      }

      protected void S122( )
      {
         /* 'DISASSOCIATESELECTED' Routine */
         AV16i = 1;
         while ( AV16i <= AV30jqAssociados.Count )
         {
            if ( ((SdtjqSelectData_Item)AV30jqAssociados.Item(AV16i)).gxTpr_Selected || AV29All )
            {
               ((SdtjqSelectData_Item)AV30jqAssociados.Item(AV16i)).gxTpr_Selected = false;
               AV32jqNaoAssociados.Add(((SdtjqSelectData_Item)AV30jqAssociados.Item(AV16i)), 0);
               AV30jqAssociados.RemoveItem(AV16i);
               AV16i = (int)(AV16i-1);
            }
            AV16i = (int)(AV16i+1);
         }
         /* Execute user subroutine: 'TITLES' */
         S142 ();
         if (returnInSub) return;
      }

      protected void S142( )
      {
         /* 'TITLES' Routine */
         lblNotassociatedrecordstitle_Caption = "N�o Associados ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV32jqNaoAssociados.Count), 9, 0))+")";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblNotassociatedrecordstitle_Internalname, "Caption", lblNotassociatedrecordstitle_Caption);
         lblAssociatedrecordstitle_Caption = "Associados ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV30jqAssociados.Count), 9, 0))+")";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblAssociatedrecordstitle_Internalname, "Caption", lblAssociatedrecordstitle_Caption);
      }

      protected void nextLoad( )
      {
      }

      protected void E18MX2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_MX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TextBlockTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociationtitle_Internalname, "Sistemas ao usu�rio :: ", "", "", lblAssociationtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_AssociarContratanteUsuario_Sistema.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratanteusuario_usuariopessoanom_Internalname, StringUtil.RTrim( AV36ContratanteUsuario_UsuarioPessoaNom), StringUtil.RTrim( context.localUtil.Format( AV36ContratanteUsuario_UsuarioPessoaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,9);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratanteusuario_usuariopessoanom_Jsonclick, 0, "AttributeTitleWWP", "", "", "", 1, edtavContratanteusuario_usuariopessoanom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_AssociarContratanteUsuario_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_12_MX2( true) ;
         }
         else
         {
            wb_table2_12_MX2( false) ;
         }
         return  ;
      }

      protected void wb_table2_12_MX2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_44_MX2( true) ;
         }
         else
         {
            wb_table3_44_MX2( false) ;
         }
         return  ;
      }

      protected void wb_table3_44_MX2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_MX2e( true) ;
         }
         else
         {
            wb_table1_2_MX2e( false) ;
         }
      }

      protected void wb_table3_44_MX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_confirm_Internalname, "", "Confirmar", bttBtn_confirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_confirm_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociarContratanteUsuario_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", bttBtn_cancel_Caption, bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociarContratanteUsuario_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_44_MX2e( true) ;
         }
         else
         {
            wb_table3_44_MX2e( false) ;
         }
      }

      protected void wb_table2_12_MX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefullcontent_Internalname, tblTablefullcontent_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableAttributesCell'>") ;
            wb_table4_15_MX2( true) ;
         }
         else
         {
            wb_table4_15_MX2( false) ;
         }
         return  ;
      }

      protected void wb_table4_15_MX2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_12_MX2e( true) ;
         }
         else
         {
            wb_table2_12_MX2e( false) ;
         }
      }

      protected void wb_table4_15_MX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableAssociation", 0, "", "", 4, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblNotassociatedrecordstitle_Internalname, lblNotassociatedrecordstitle_Caption, "", "", lblNotassociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarContratanteUsuario_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblCenterrecordstitle_Internalname, " ", "", "", lblCenterrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarContratanteUsuario_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociatedrecordstitle_Internalname, lblAssociatedrecordstitle_Caption, "", "", lblAssociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarContratanteUsuario_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"JQNAOASSOCIADOSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table5_27_MX2( true) ;
         }
         else
         {
            wb_table5_27_MX2( false) ;
         }
         return  ;
      }

      protected void wb_table5_27_MX2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"JQASSOCIADOSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_15_MX2e( true) ;
         }
         else
         {
            wb_table4_15_MX2e( false) ;
         }
      }

      protected void wb_table5_27_MX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtableassociationbuttons_Internalname, tblUnnamedtableassociationbuttons_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateall_Internalname, context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarContratanteUsuario_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateselected_Internalname, context.GetImagePath( "56a5f17b-0bc3-48b5-b303-afa6e0585b6d", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarContratanteUsuario_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateselected_Internalname, context.GetImagePath( "a3800d0c-bf04-4575-bc01-11fe5d7b3525", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarContratanteUsuario_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateall_Internalname, context.GetImagePath( "c619e28f-4b32-4ff9-baaf-b3063fe4f782", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarContratanteUsuario_Sistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_27_MX2e( true) ;
         }
         else
         {
            wb_table5_27_MX2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV10ContratanteUsuario_ContratanteCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ContratanteUsuario_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10ContratanteUsuario_ContratanteCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATANTEUSUARIO_CONTRATANTECOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV10ContratanteUsuario_ContratanteCod), "ZZZZZ9")));
         AV11ContratanteUsuario_UsuarioCod = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ContratanteUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11ContratanteUsuario_UsuarioCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATANTEUSUARIO_USUARIOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11ContratanteUsuario_UsuarioCod), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAMX2( ) ;
         WSMX2( ) ;
         WEMX2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("jQueryUI/css/demos.css", "?1349420");
         AddStyleSheetFile("jQueryUI/css/demos.css", "?1349420");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020529943491");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_associarcontratanteusuario_sistema.js", "?2020529943491");
         context.AddJavascriptSource("Shared/jquery/jquery-1.4.2.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.core.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.widget.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.mouse.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.selectable.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.corner.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery-1.4.2.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.core.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.widget.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.mouse.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.selectable.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.corner.js", "");
         context.AddJavascriptSource("jQueryUI/jqSelectRender.js", "");
         context.AddJavascriptSource("jQueryUI/jqSelectRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblAssociationtitle_Internalname = "ASSOCIATIONTITLE";
         edtavContratanteusuario_usuariopessoanom_Internalname = "vCONTRATANTEUSUARIO_USUARIOPESSOANOM";
         lblNotassociatedrecordstitle_Internalname = "NOTASSOCIATEDRECORDSTITLE";
         lblCenterrecordstitle_Internalname = "CENTERRECORDSTITLE";
         lblAssociatedrecordstitle_Internalname = "ASSOCIATEDRECORDSTITLE";
         Jqnaoassociados_Internalname = "JQNAOASSOCIADOS";
         imgImageassociateall_Internalname = "IMAGEASSOCIATEALL";
         imgImageassociateselected_Internalname = "IMAGEASSOCIATESELECTED";
         imgImagedisassociateselected_Internalname = "IMAGEDISASSOCIATESELECTED";
         imgImagedisassociateall_Internalname = "IMAGEDISASSOCIATEALL";
         tblUnnamedtableassociationbuttons_Internalname = "UNNAMEDTABLEASSOCIATIONBUTTONS";
         Jqassociados_Internalname = "JQASSOCIADOS";
         tblTablecontent_Internalname = "TABLECONTENT";
         tblTablefullcontent_Internalname = "TABLEFULLCONTENT";
         bttBtn_confirm_Internalname = "BTN_CONFIRM";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         imgImagedisassociateall_Visible = 1;
         imgImagedisassociateselected_Visible = 1;
         imgImageassociateselected_Visible = 1;
         imgImageassociateall_Visible = 1;
         bttBtn_confirm_Visible = 1;
         edtavContratanteusuario_usuariopessoanom_Jsonclick = "";
         edtavContratanteusuario_usuariopessoanom_Enabled = 1;
         lblAssociatedrecordstitle_Caption = "Sistemas Associados";
         lblNotassociatedrecordstitle_Caption = "Sistemas N�o Associados";
         bttBtn_cancel_Caption = "Fechar";
         Jqassociados_Itemheight = 17;
         Jqassociados_Itemwidth = 270;
         Jqassociados_Selectedcolor = (int)(0xE0E0E0);
         Jqassociados_Unselectedicon = "ui-icon-minus";
         Jqassociados_Readonly = Convert.ToBoolean( -1);
         Jqassociados_Theme = "base";
         Jqassociados_Rounding = "bevelfold bl";
         Jqassociados_Height = 300;
         Jqassociados_Width = 300;
         Jqnaoassociados_Itemheight = 17;
         Jqnaoassociados_Itemwidth = 270;
         Jqnaoassociados_Selectedcolor = (int)(0xE0E0E0);
         Jqnaoassociados_Unselectedicon = "ui-icon-minus";
         Jqnaoassociados_Theme = "base";
         Jqnaoassociados_Rounding = "bevelfold bl";
         Jqnaoassociados_Height = 300;
         Jqnaoassociados_Width = 300;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[{av:'imgImageassociateselected_Visible',ctrl:'IMAGEASSOCIATESELECTED',prop:'Visible'},{av:'imgImageassociateall_Visible',ctrl:'IMAGEASSOCIATEALL',prop:'Visible'},{av:'imgImagedisassociateselected_Visible',ctrl:'IMAGEDISASSOCIATESELECTED',prop:'Visible'},{av:'imgImagedisassociateall_Visible',ctrl:'IMAGEDISASSOCIATEALL',prop:'Visible'},{ctrl:'BTN_CONFIRM',prop:'Visible'}]}");
         setEventMetadata("ENTER","{handler:'E13MX2',iparms:[{av:'AV30jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV10ContratanteUsuario_ContratanteCod',fld:'vCONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV11ContratanteUsuario_UsuarioCod',fld:'vCONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV35MenuPerfil',fld:'vMENUPERFIL',pic:'',nv:null}],oparms:[{av:'AV25Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DISASSOCIATE SELECTED'","{handler:'E14MX2',iparms:[{av:'AV30jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'AV29All',fld:'vALL',pic:'',nv:false},{av:'AV32jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null}],oparms:[{ctrl:'BTN_CANCEL',prop:'Caption'},{av:'AV29All',fld:'vALL',pic:'',nv:false},{av:'AV30jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'AV32jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'lblNotassociatedrecordstitle_Caption',ctrl:'NOTASSOCIATEDRECORDSTITLE',prop:'Caption'},{av:'lblAssociatedrecordstitle_Caption',ctrl:'ASSOCIATEDRECORDSTITLE',prop:'Caption'}]}");
         setEventMetadata("'ASSOCIATE SELECTED'","{handler:'E15MX2',iparms:[{av:'AV32jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV29All',fld:'vALL',pic:'',nv:false},{av:'AV30jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null}],oparms:[{ctrl:'BTN_CANCEL',prop:'Caption'},{av:'AV29All',fld:'vALL',pic:'',nv:false},{av:'AV32jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV30jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'lblNotassociatedrecordstitle_Caption',ctrl:'NOTASSOCIATEDRECORDSTITLE',prop:'Caption'},{av:'lblAssociatedrecordstitle_Caption',ctrl:'ASSOCIATEDRECORDSTITLE',prop:'Caption'}]}");
         setEventMetadata("'ASSOCIATE ALL'","{handler:'E16MX2',iparms:[{av:'AV32jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV29All',fld:'vALL',pic:'',nv:false},{av:'AV30jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null}],oparms:[{ctrl:'BTN_CANCEL',prop:'Caption'},{av:'AV29All',fld:'vALL',pic:'',nv:false},{av:'AV32jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV30jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'lblNotassociatedrecordstitle_Caption',ctrl:'NOTASSOCIATEDRECORDSTITLE',prop:'Caption'},{av:'lblAssociatedrecordstitle_Caption',ctrl:'ASSOCIATEDRECORDSTITLE',prop:'Caption'}]}");
         setEventMetadata("'DISASSOCIATE ALL'","{handler:'E17MX2',iparms:[{av:'AV30jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'AV29All',fld:'vALL',pic:'',nv:false},{av:'AV32jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null}],oparms:[{ctrl:'BTN_CANCEL',prop:'Caption'},{av:'AV29All',fld:'vALL',pic:'',nv:false},{av:'AV30jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'AV32jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'lblNotassociatedrecordstitle_Caption',ctrl:'NOTASSOCIATEDRECORDSTITLE',prop:'Caption'},{av:'lblAssociatedrecordstitle_Caption',ctrl:'ASSOCIATEDRECORDSTITLE',prop:'Caption'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV32jqNaoAssociados = new GxObjectCollection( context, "jqSelectData.Item", "GxEv3Up14_MeetrikaVs3", "SdtjqSelectData_Item", "GeneXus.Programs");
         AV30jqAssociados = new GxObjectCollection( context, "jqSelectData.Item", "GxEv3Up14_MeetrikaVs3", "SdtjqSelectData_Item", "GeneXus.Programs");
         AV35MenuPerfil = new SdtMenuPerfil(context);
         AV36ContratanteUsuario_UsuarioPessoaNom = "";
         GXKey = "";
         GX_FocusControl = "";
         sPrefix = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV27WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV15HTTPRequest = new GxHttpRequest( context);
         scmdbuf = "";
         H00MX3_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         H00MX3_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00MX3_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H00MX3_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H00MX3_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00MX3_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H00MX3_A1020ContratanteUsuario_AreaTrabalhoCod = new int[1] ;
         H00MX3_n1020ContratanteUsuario_AreaTrabalhoCod = new bool[] {false} ;
         A62ContratanteUsuario_UsuarioPessoaNom = "";
         H00MX4_A135Sistema_AreaTrabalhoCod = new int[1] ;
         H00MX4_A699Sistema_Tipo = new String[] {""} ;
         H00MX4_n699Sistema_Tipo = new bool[] {false} ;
         H00MX4_A127Sistema_Codigo = new int[1] ;
         H00MX4_A130Sistema_Ativo = new bool[] {false} ;
         H00MX4_A129Sistema_Sigla = new String[] {""} ;
         A699Sistema_Tipo = "";
         A129Sistema_Sigla = "";
         H00MX5_A127Sistema_Codigo = new int[1] ;
         H00MX5_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H00MX5_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         AV31jqItem = new SdtjqSelectData_Item(context);
         H00MX6_A127Sistema_Codigo = new int[1] ;
         H00MX6_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H00MX6_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         AV12ContratanteUsuarioSistema = new SdtContratanteUsuarioSistema(context);
         H00MX7_A127Sistema_Codigo = new int[1] ;
         H00MX7_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H00MX7_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         AV46GXV3 = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV19Message = new SdtMessages_Message(context);
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblAssociationtitle_Jsonclick = "";
         TempTags = "";
         bttBtn_confirm_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         lblNotassociatedrecordstitle_Jsonclick = "";
         lblCenterrecordstitle_Jsonclick = "";
         lblAssociatedrecordstitle_Jsonclick = "";
         imgImageassociateall_Jsonclick = "";
         imgImageassociateselected_Jsonclick = "";
         imgImagedisassociateselected_Jsonclick = "";
         imgImagedisassociateall_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_associarcontratanteusuario_sistema__default(),
            new Object[][] {
                new Object[] {
               H00MX3_A61ContratanteUsuario_UsuarioPessoaCod, H00MX3_n61ContratanteUsuario_UsuarioPessoaCod, H00MX3_A60ContratanteUsuario_UsuarioCod, H00MX3_A63ContratanteUsuario_ContratanteCod, H00MX3_A62ContratanteUsuario_UsuarioPessoaNom, H00MX3_n62ContratanteUsuario_UsuarioPessoaNom, H00MX3_A1020ContratanteUsuario_AreaTrabalhoCod, H00MX3_n1020ContratanteUsuario_AreaTrabalhoCod
               }
               , new Object[] {
               H00MX4_A135Sistema_AreaTrabalhoCod, H00MX4_A699Sistema_Tipo, H00MX4_n699Sistema_Tipo, H00MX4_A127Sistema_Codigo, H00MX4_A130Sistema_Ativo, H00MX4_A129Sistema_Sigla
               }
               , new Object[] {
               H00MX5_A127Sistema_Codigo, H00MX5_A60ContratanteUsuario_UsuarioCod, H00MX5_A63ContratanteUsuario_ContratanteCod
               }
               , new Object[] {
               H00MX6_A127Sistema_Codigo, H00MX6_A60ContratanteUsuario_UsuarioCod, H00MX6_A63ContratanteUsuario_ContratanteCod
               }
               , new Object[] {
               H00MX7_A127Sistema_Codigo, H00MX7_A60ContratanteUsuario_UsuarioCod, H00MX7_A63ContratanteUsuario_ContratanteCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavContratanteusuario_usuariopessoanom_Enabled = 0;
      }

      private short nRcdExists_7 ;
      private short nIsMod_7 ;
      private short nRcdExists_6 ;
      private short nIsMod_6 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV39GXLvl5 ;
      private short AV43GXLvl62 ;
      private short nGXWrapped ;
      private int AV10ContratanteUsuario_ContratanteCod ;
      private int AV11ContratanteUsuario_UsuarioCod ;
      private int wcpOAV10ContratanteUsuario_ContratanteCod ;
      private int wcpOAV11ContratanteUsuario_UsuarioCod ;
      private int edtavContratanteusuario_usuariopessoanom_Enabled ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A127Sistema_Codigo ;
      private int Jqnaoassociados_Width ;
      private int Jqnaoassociados_Height ;
      private int Jqnaoassociados_Selectedcolor ;
      private int Jqnaoassociados_Itemwidth ;
      private int Jqnaoassociados_Itemheight ;
      private int Jqassociados_Width ;
      private int Jqassociados_Height ;
      private int Jqassociados_Selectedcolor ;
      private int Jqassociados_Itemwidth ;
      private int Jqassociados_Itemheight ;
      private int A61ContratanteUsuario_UsuarioPessoaCod ;
      private int A1020ContratanteUsuario_AreaTrabalhoCod ;
      private int AV28AreaTrabalho ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int imgImageassociateselected_Visible ;
      private int imgImageassociateall_Visible ;
      private int imgImagedisassociateselected_Visible ;
      private int imgImagedisassociateall_Visible ;
      private int bttBtn_confirm_Visible ;
      private int AV42GXV1 ;
      private int AV25Sistema_Codigo ;
      private int AV44GXV2 ;
      private int AV47GXV4 ;
      private int AV16i ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String edtavContratanteusuario_usuariopessoanom_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV36ContratanteUsuario_UsuarioPessoaNom ;
      private String Jqnaoassociados_Rounding ;
      private String Jqnaoassociados_Theme ;
      private String Jqnaoassociados_Unselectedicon ;
      private String Jqassociados_Rounding ;
      private String Jqassociados_Theme ;
      private String Jqassociados_Unselectedicon ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String A62ContratanteUsuario_UsuarioPessoaNom ;
      private String A699Sistema_Tipo ;
      private String A129Sistema_Sigla ;
      private String imgImageassociateselected_Internalname ;
      private String imgImageassociateall_Internalname ;
      private String imgImagedisassociateselected_Internalname ;
      private String imgImagedisassociateall_Internalname ;
      private String bttBtn_confirm_Internalname ;
      private String bttBtn_cancel_Caption ;
      private String bttBtn_cancel_Internalname ;
      private String lblNotassociatedrecordstitle_Caption ;
      private String lblNotassociatedrecordstitle_Internalname ;
      private String lblAssociatedrecordstitle_Caption ;
      private String lblAssociatedrecordstitle_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblAssociationtitle_Internalname ;
      private String lblAssociationtitle_Jsonclick ;
      private String TempTags ;
      private String edtavContratanteusuario_usuariopessoanom_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String bttBtn_confirm_Jsonclick ;
      private String bttBtn_cancel_Jsonclick ;
      private String tblTablefullcontent_Internalname ;
      private String tblTablecontent_Internalname ;
      private String lblNotassociatedrecordstitle_Jsonclick ;
      private String lblCenterrecordstitle_Internalname ;
      private String lblCenterrecordstitle_Jsonclick ;
      private String lblAssociatedrecordstitle_Jsonclick ;
      private String tblUnnamedtableassociationbuttons_Internalname ;
      private String imgImageassociateall_Jsonclick ;
      private String imgImageassociateselected_Jsonclick ;
      private String imgImagedisassociateselected_Jsonclick ;
      private String imgImagedisassociateall_Jsonclick ;
      private String Jqnaoassociados_Internalname ;
      private String Jqassociados_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool AV29All ;
      private bool Jqassociados_Readonly ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n61ContratanteUsuario_UsuarioPessoaCod ;
      private bool n62ContratanteUsuario_UsuarioPessoaNom ;
      private bool n1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool n699Sistema_Tipo ;
      private bool A130Sistema_Ativo ;
      private bool AV14Exist ;
      private bool AV26Success ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00MX3_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] H00MX3_n61ContratanteUsuario_UsuarioPessoaCod ;
      private int[] H00MX3_A60ContratanteUsuario_UsuarioCod ;
      private int[] H00MX3_A63ContratanteUsuario_ContratanteCod ;
      private String[] H00MX3_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] H00MX3_n62ContratanteUsuario_UsuarioPessoaNom ;
      private int[] H00MX3_A1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool[] H00MX3_n1020ContratanteUsuario_AreaTrabalhoCod ;
      private int[] H00MX4_A135Sistema_AreaTrabalhoCod ;
      private String[] H00MX4_A699Sistema_Tipo ;
      private bool[] H00MX4_n699Sistema_Tipo ;
      private int[] H00MX4_A127Sistema_Codigo ;
      private bool[] H00MX4_A130Sistema_Ativo ;
      private String[] H00MX4_A129Sistema_Sigla ;
      private int[] H00MX5_A127Sistema_Codigo ;
      private int[] H00MX5_A60ContratanteUsuario_UsuarioCod ;
      private int[] H00MX5_A63ContratanteUsuario_ContratanteCod ;
      private int[] H00MX6_A127Sistema_Codigo ;
      private int[] H00MX6_A60ContratanteUsuario_UsuarioCod ;
      private int[] H00MX6_A63ContratanteUsuario_ContratanteCod ;
      private int[] H00MX7_A127Sistema_Codigo ;
      private int[] H00MX7_A60ContratanteUsuario_UsuarioCod ;
      private int[] H00MX7_A63ContratanteUsuario_ContratanteCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV15HTTPRequest ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV46GXV3 ;
      [ObjectCollection(ItemType=typeof( SdtjqSelectData_Item ))]
      private IGxCollection AV32jqNaoAssociados ;
      [ObjectCollection(ItemType=typeof( SdtjqSelectData_Item ))]
      private IGxCollection AV30jqAssociados ;
      private SdtContratanteUsuarioSistema AV12ContratanteUsuarioSistema ;
      private SdtMessages_Message AV19Message ;
      private wwpbaseobjects.SdtWWPContext AV27WWPContext ;
      private SdtjqSelectData_Item AV31jqItem ;
      private SdtMenuPerfil AV35MenuPerfil ;
   }

   public class wp_associarcontratanteusuario_sistema__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00MX3 ;
          prmH00MX3 = new Object[] {
          new Object[] {"@AV10ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MX4 ;
          prmH00MX4 = new Object[] {
          new Object[] {"@AV28AreaTrabalho",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MX5 ;
          prmH00MX5 = new Object[] {
          new Object[] {"@AV10ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MX6 ;
          prmH00MX6 = new Object[] {
          new Object[] {"@AV10ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV25Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MX7 ;
          prmH00MX7 = new Object[] {
          new Object[] {"@AV10ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV25Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00MX3", "SELECT TOP 1 T2.[Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPessoaCod, T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T1.[ContratanteUsuario_ContratanteCod], T3.[Pessoa_Nome] AS ContratanteUsuario_UsuarioPessoaNom, COALESCE( T4.[ContratanteUsuario_AreaTrabalhoCod], 0) AS ContratanteUsuario_AreaTrabalhoCod FROM ((([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) LEFT JOIN (SELECT MIN(T5.[AreaTrabalho_Codigo]) AS ContratanteUsuario_AreaTrabalhoCod, T6.[ContratanteUsuario_ContratanteCod], T6.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod FROM [AreaTrabalho] T5 WITH (NOLOCK),  [ContratanteUsuario] T6 WITH (NOLOCK) WHERE T5.[Contratante_Codigo] = T6.[ContratanteUsuario_ContratanteCod] GROUP BY T6.[ContratanteUsuario_ContratanteCod], T6.[ContratanteUsuario_UsuarioCod] ) T4 ON T4.[ContratanteUsuario_ContratanteCod] = T1.[ContratanteUsuario_ContratanteCod] AND T4.[ContratanteUsuario_UsuarioCod] = T1.[ContratanteUsuario_UsuarioCod]) WHERE T1.[ContratanteUsuario_ContratanteCod] = @AV10ContratanteUsuario_ContratanteCod and T1.[ContratanteUsuario_UsuarioCod] = @AV11ContratanteUsuario_UsuarioCod ORDER BY T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MX3,1,0,false,true )
             ,new CursorDef("H00MX4", "SELECT [Sistema_AreaTrabalhoCod], [Sistema_Tipo], [Sistema_Codigo], [Sistema_Ativo], [Sistema_Sigla] FROM [Sistema] WITH (NOLOCK) WHERE ([Sistema_Ativo] = 1) AND ([Sistema_AreaTrabalhoCod] = @AV28AreaTrabalho) AND ([Sistema_Tipo] = 'A') ORDER BY [Sistema_Sigla], [Sistema_AreaTrabalhoCod], [Sistema_Tipo], [Sistema_Ativo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MX4,100,0,true,false )
             ,new CursorDef("H00MX5", "SELECT [Sistema_Codigo], [ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, [ContratanteUsuario_ContratanteCod] FROM [ContratanteUsuarioSistema] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @AV10ContratanteUsuario_ContratanteCod and [ContratanteUsuario_UsuarioCod] = @AV11ContratanteUsuario_UsuarioCod and [Sistema_Codigo] = @Sistema_Codigo ORDER BY [ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod], [Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MX5,1,0,false,true )
             ,new CursorDef("H00MX6", "SELECT TOP 1 [Sistema_Codigo], [ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, [ContratanteUsuario_ContratanteCod] FROM [ContratanteUsuarioSistema] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @AV10ContratanteUsuario_ContratanteCod and [ContratanteUsuario_UsuarioCod] = @AV11ContratanteUsuario_UsuarioCod and [Sistema_Codigo] = @AV25Sistema_Codigo ORDER BY [ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod], [Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MX6,1,0,false,true )
             ,new CursorDef("H00MX7", "SELECT TOP 1 [Sistema_Codigo], [ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, [ContratanteUsuario_ContratanteCod] FROM [ContratanteUsuarioSistema] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @AV10ContratanteUsuario_ContratanteCod and [ContratanteUsuario_UsuarioCod] = @AV11ContratanteUsuario_UsuarioCod and [Sistema_Codigo] = @AV25Sistema_Codigo ORDER BY [ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod], [Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MX7,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 25) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
       }
    }

 }

}
