/*
               File: WWContratoOcorrenciaNotificacao
        Description:  Contrato Ocorrencia Notificacao
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 13:0:54.73
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwcontratoocorrencianotificacao : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwcontratoocorrencianotificacao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwcontratoocorrencianotificacao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_87 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_87_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_87_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17ContratoOcorrenciaNotificacao_Data1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoOcorrenciaNotificacao_Data1", context.localUtil.Format(AV17ContratoOcorrenciaNotificacao_Data1, "99/99/99"));
               AV18ContratoOcorrenciaNotificacao_Data_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContratoOcorrenciaNotificacao_Data_To1", context.localUtil.Format(AV18ContratoOcorrenciaNotificacao_Data_To1, "99/99/99"));
               AV19ContratoOcorrenciaNotificacao_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratoOcorrenciaNotificacao_Nome1", AV19ContratoOcorrenciaNotificacao_Nome1);
               AV21DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
               AV23ContratoOcorrenciaNotificacao_Data2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContratoOcorrenciaNotificacao_Data2", context.localUtil.Format(AV23ContratoOcorrenciaNotificacao_Data2, "99/99/99"));
               AV24ContratoOcorrenciaNotificacao_Data_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratoOcorrenciaNotificacao_Data_To2", context.localUtil.Format(AV24ContratoOcorrenciaNotificacao_Data_To2, "99/99/99"));
               AV25ContratoOcorrenciaNotificacao_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoOcorrenciaNotificacao_Nome2", AV25ContratoOcorrenciaNotificacao_Nome2);
               AV20DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV46TFContrato_Numero = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContrato_Numero", AV46TFContrato_Numero);
               AV47TFContrato_Numero_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContrato_Numero_Sel", AV47TFContrato_Numero_Sel);
               AV50TFContratoOcorrenciaNotificacao_Data = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContratoOcorrenciaNotificacao_Data", context.localUtil.Format(AV50TFContratoOcorrenciaNotificacao_Data, "99/99/99"));
               AV51TFContratoOcorrenciaNotificacao_Data_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratoOcorrenciaNotificacao_Data_To", context.localUtil.Format(AV51TFContratoOcorrenciaNotificacao_Data_To, "99/99/99"));
               AV56TFContratoOcorrenciaNotificacao_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContratoOcorrenciaNotificacao_Descricao", AV56TFContratoOcorrenciaNotificacao_Descricao);
               AV57TFContratoOcorrenciaNotificacao_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFContratoOcorrenciaNotificacao_Descricao_Sel", AV57TFContratoOcorrenciaNotificacao_Descricao_Sel);
               AV60TFContratoOcorrenciaNotificacao_Prazo = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoOcorrenciaNotificacao_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60TFContratoOcorrenciaNotificacao_Prazo), 4, 0)));
               AV61TFContratoOcorrenciaNotificacao_Prazo_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContratoOcorrenciaNotificacao_Prazo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFContratoOcorrenciaNotificacao_Prazo_To), 4, 0)));
               AV64TFContratoOcorrenciaNotificacao_Cumprido = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoOcorrenciaNotificacao_Cumprido", context.localUtil.Format(AV64TFContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
               AV65TFContratoOcorrenciaNotificacao_Cumprido_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratoOcorrenciaNotificacao_Cumprido_To", context.localUtil.Format(AV65TFContratoOcorrenciaNotificacao_Cumprido_To, "99/99/99"));
               AV70TFContratoOcorrenciaNotificacao_Protocolo = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratoOcorrenciaNotificacao_Protocolo", AV70TFContratoOcorrenciaNotificacao_Protocolo);
               AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel", AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel);
               AV74TFContratoOcorrenciaNotificacao_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContratoOcorrenciaNotificacao_Nome", AV74TFContratoOcorrenciaNotificacao_Nome);
               AV75TFContratoOcorrenciaNotificacao_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContratoOcorrenciaNotificacao_Nome_Sel", AV75TFContratoOcorrenciaNotificacao_Nome_Sel);
               AV48ddo_Contrato_NumeroTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_Contrato_NumeroTitleControlIdToReplace", AV48ddo_Contrato_NumeroTitleControlIdToReplace);
               AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace", AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace);
               AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace", AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace);
               AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace", AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace);
               AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace", AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace);
               AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace", AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace);
               AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace", AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace);
               AV110Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV33DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
               AV32DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
               A297ContratoOcorrenciaNotificacao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A294ContratoOcorrencia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A303ContratoOcorrenciaNotificacao_Responsavel = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoOcorrenciaNotificacao_Data1, AV18ContratoOcorrenciaNotificacao_Data_To1, AV19ContratoOcorrenciaNotificacao_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContratoOcorrenciaNotificacao_Data2, AV24ContratoOcorrenciaNotificacao_Data_To2, AV25ContratoOcorrenciaNotificacao_Nome2, AV20DynamicFiltersEnabled2, AV46TFContrato_Numero, AV47TFContrato_Numero_Sel, AV50TFContratoOcorrenciaNotificacao_Data, AV51TFContratoOcorrenciaNotificacao_Data_To, AV56TFContratoOcorrenciaNotificacao_Descricao, AV57TFContratoOcorrenciaNotificacao_Descricao_Sel, AV60TFContratoOcorrenciaNotificacao_Prazo, AV61TFContratoOcorrenciaNotificacao_Prazo_To, AV64TFContratoOcorrenciaNotificacao_Cumprido, AV65TFContratoOcorrenciaNotificacao_Cumprido_To, AV70TFContratoOcorrenciaNotificacao_Protocolo, AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV74TFContratoOcorrenciaNotificacao_Nome, AV75TFContratoOcorrenciaNotificacao_Nome_Sel, AV48ddo_Contrato_NumeroTitleControlIdToReplace, AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, AV110Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A297ContratoOcorrenciaNotificacao_Codigo, A294ContratoOcorrencia_Codigo, A303ContratoOcorrenciaNotificacao_Responsavel) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA7C2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START7C2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205181305581");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwcontratoocorrencianotificacao.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA1", context.localUtil.Format(AV17ContratoOcorrenciaNotificacao_Data1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1", context.localUtil.Format(AV18ContratoOcorrenciaNotificacao_Data_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOOCORRENCIANOTIFICACAO_NOME1", StringUtil.RTrim( AV19ContratoOcorrenciaNotificacao_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV21DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA2", context.localUtil.Format(AV23ContratoOcorrenciaNotificacao_Data2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2", context.localUtil.Format(AV24ContratoOcorrenciaNotificacao_Data_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOOCORRENCIANOTIFICACAO_NOME2", StringUtil.RTrim( AV25ContratoOcorrenciaNotificacao_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV20DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_NUMERO", StringUtil.RTrim( AV46TFContrato_Numero));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_NUMERO_SEL", StringUtil.RTrim( AV47TFContrato_Numero_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DATA", context.localUtil.Format(AV50TFContratoOcorrenciaNotificacao_Data, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO", context.localUtil.Format(AV51TFContratoOcorrenciaNotificacao_Data_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO", AV56TFContratoOcorrenciaNotificacao_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL", AV57TFContratoOcorrenciaNotificacao_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV60TFContratoOcorrenciaNotificacao_Prazo), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV61TFContratoOcorrenciaNotificacao_Prazo_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO", context.localUtil.Format(AV64TFContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO", context.localUtil.Format(AV65TFContratoOcorrenciaNotificacao_Cumprido_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO", AV70TFContratoOcorrenciaNotificacao_Protocolo);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL", AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_NOME", StringUtil.RTrim( AV74TFContratoOcorrenciaNotificacao_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL", StringUtil.RTrim( AV75TFContratoOcorrenciaNotificacao_Nome_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_87", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_87), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV79GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV80GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV77DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV77DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATO_NUMEROTITLEFILTERDATA", AV45Contrato_NumeroTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATO_NUMEROTITLEFILTERDATA", AV45Contrato_NumeroTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOOCORRENCIANOTIFICACAO_DATATITLEFILTERDATA", AV49ContratoOcorrenciaNotificacao_DataTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOOCORRENCIANOTIFICACAO_DATATITLEFILTERDATA", AV49ContratoOcorrenciaNotificacao_DataTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLEFILTERDATA", AV55ContratoOcorrenciaNotificacao_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLEFILTERDATA", AV55ContratoOcorrenciaNotificacao_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLEFILTERDATA", AV59ContratoOcorrenciaNotificacao_PrazoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLEFILTERDATA", AV59ContratoOcorrenciaNotificacao_PrazoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLEFILTERDATA", AV63ContratoOcorrenciaNotificacao_CumpridoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLEFILTERDATA", AV63ContratoOcorrenciaNotificacao_CumpridoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLEFILTERDATA", AV69ContratoOcorrenciaNotificacao_ProtocoloTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLEFILTERDATA", AV69ContratoOcorrenciaNotificacao_ProtocoloTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOOCORRENCIANOTIFICACAO_NOMETITLEFILTERDATA", AV73ContratoOcorrenciaNotificacao_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOOCORRENCIANOTIFICACAO_NOMETITLEFILTERDATA", AV73ContratoOcorrenciaNotificacao_NomeTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV110Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV33DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV32DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Caption", StringUtil.RTrim( Ddo_contrato_numero_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Tooltip", StringUtil.RTrim( Ddo_contrato_numero_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Cls", StringUtil.RTrim( Ddo_contrato_numero_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filteredtext_set", StringUtil.RTrim( Ddo_contrato_numero_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Selectedvalue_set", StringUtil.RTrim( Ddo_contrato_numero_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contrato_numero_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contrato_numero_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includesortasc", StringUtil.BoolToStr( Ddo_contrato_numero_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includesortdsc", StringUtil.BoolToStr( Ddo_contrato_numero_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortedstatus", StringUtil.RTrim( Ddo_contrato_numero_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includefilter", StringUtil.BoolToStr( Ddo_contrato_numero_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filtertype", StringUtil.RTrim( Ddo_contrato_numero_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filterisrange", StringUtil.BoolToStr( Ddo_contrato_numero_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includedatalist", StringUtil.BoolToStr( Ddo_contrato_numero_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalisttype", StringUtil.RTrim( Ddo_contrato_numero_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalistproc", StringUtil.RTrim( Ddo_contrato_numero_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contrato_numero_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortasc", StringUtil.RTrim( Ddo_contrato_numero_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortdsc", StringUtil.RTrim( Ddo_contrato_numero_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Loadingdata", StringUtil.RTrim( Ddo_contrato_numero_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Cleanfilter", StringUtil.RTrim( Ddo_contrato_numero_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Noresultsfound", StringUtil.RTrim( Ddo_contrato_numero_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Searchbuttontext", StringUtil.RTrim( Ddo_contrato_numero_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Caption", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Tooltip", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Cls", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filteredtext_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Includesortasc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_data_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_data_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Sortedstatus", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Includefilter", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_data_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filtertype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filterisrange", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_data_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Includedatalist", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_data_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Sortasc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Sortdsc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Cleanfilter", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Rangefilterto", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Searchbuttontext", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Caption", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Cls", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoocorrencianotificacao_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Caption", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Tooltip", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Cls", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_prazo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_prazo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Sortedstatus", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Includefilter", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_prazo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filtertype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_prazo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_prazo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Sortasc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Sortdsc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Cleanfilter", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Rangefilterto", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Caption", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Tooltip", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Cls", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_cumprido_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_cumprido_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Sortedstatus", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Includefilter", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_cumprido_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filtertype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_cumprido_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_cumprido_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Sortasc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Sortdsc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Cleanfilter", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Rangefilterto", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Caption", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Tooltip", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Cls", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_protocolo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_protocolo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Sortedstatus", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Includefilter", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_protocolo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Filtertype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_protocolo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_protocolo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Datalisttype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Datalistproc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoocorrencianotificacao_protocolo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Sortasc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Sortdsc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Loadingdata", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Cleanfilter", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Noresultsfound", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Caption", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Tooltip", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Cls", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Filtertype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Datalisttype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Datalistproc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoocorrencianotificacao_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Sortasc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Sortdsc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Loadingdata", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Activeeventkey", StringUtil.RTrim( Ddo_contrato_numero_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filteredtext_get", StringUtil.RTrim( Ddo_contrato_numero_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Selectedvalue_get", StringUtil.RTrim( Ddo_contrato_numero_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Activeeventkey", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filteredtext_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Activeeventkey", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Activeeventkey", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Activeeventkey", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE7C2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT7C2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwcontratoocorrencianotificacao.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWContratoOcorrenciaNotificacao" ;
      }

      public override String GetPgmdesc( )
      {
         return " Contrato Ocorrencia Notificacao" ;
      }

      protected void WB7C0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_7C2( true) ;
         }
         else
         {
            wb_table1_2_7C2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_7C2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_87_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(105, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,105);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_numero_Internalname, StringUtil.RTrim( AV46TFContrato_Numero), StringUtil.RTrim( context.localUtil.Format( AV46TFContrato_Numero, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_numero_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_numero_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_numero_sel_Internalname, StringUtil.RTrim( AV47TFContrato_Numero_Sel), StringUtil.RTrim( context.localUtil.Format( AV47TFContrato_Numero_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_numero_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_numero_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_87_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratoocorrencianotificacao_data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_data_Internalname, context.localUtil.Format(AV50TFContratoOcorrenciaNotificacao_Data, "99/99/99"), context.localUtil.Format( AV50TFContratoOcorrenciaNotificacao_Data, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_data_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_data_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratoocorrencianotificacao_data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratoocorrencianotificacao_data_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_87_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratoocorrencianotificacao_data_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_data_to_Internalname, context.localUtil.Format(AV51TFContratoOcorrenciaNotificacao_Data_To, "99/99/99"), context.localUtil.Format( AV51TFContratoOcorrenciaNotificacao_Data_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_data_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_data_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratoocorrencianotificacao_data_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratoocorrencianotificacao_data_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contratoocorrencianotificacao_dataauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_87_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratoocorrencianotificacao_dataauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratoocorrencianotificacao_dataauxdate_Internalname, context.localUtil.Format(AV52DDO_ContratoOcorrenciaNotificacao_DataAuxDate, "99/99/99"), context.localUtil.Format( AV52DDO_ContratoOcorrenciaNotificacao_DataAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratoocorrencianotificacao_dataauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratoocorrencianotificacao_dataauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_87_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratoocorrencianotificacao_dataauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratoocorrencianotificacao_dataauxdateto_Internalname, context.localUtil.Format(AV53DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo, "99/99/99"), context.localUtil.Format( AV53DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,112);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratoocorrencianotificacao_dataauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratoocorrencianotificacao_dataauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_descricao_Internalname, AV56TFContratoOcorrenciaNotificacao_Descricao, StringUtil.RTrim( context.localUtil.Format( AV56TFContratoOcorrenciaNotificacao_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_descricao_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_descricao_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_descricao_sel_Internalname, AV57TFContratoOcorrenciaNotificacao_Descricao_Sel, StringUtil.RTrim( context.localUtil.Format( AV57TFContratoOcorrenciaNotificacao_Descricao_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_descricao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_descricao_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_prazo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV60TFContratoOcorrenciaNotificacao_Prazo), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV60TFContratoOcorrenciaNotificacao_Prazo), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_prazo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_prazo_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_prazo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV61TFContratoOcorrenciaNotificacao_Prazo_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV61TFContratoOcorrenciaNotificacao_Prazo_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_prazo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_prazo_to_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_87_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratoocorrencianotificacao_cumprido_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_cumprido_Internalname, context.localUtil.Format(AV64TFContratoOcorrenciaNotificacao_Cumprido, "99/99/99"), context.localUtil.Format( AV64TFContratoOcorrenciaNotificacao_Cumprido, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,117);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_cumprido_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_cumprido_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratoocorrencianotificacao_cumprido_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratoocorrencianotificacao_cumprido_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_87_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratoocorrencianotificacao_cumprido_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_cumprido_to_Internalname, context.localUtil.Format(AV65TFContratoOcorrenciaNotificacao_Cumprido_To, "99/99/99"), context.localUtil.Format( AV65TFContratoOcorrenciaNotificacao_Cumprido_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,118);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_cumprido_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_cumprido_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratoocorrencianotificacao_cumprido_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratoocorrencianotificacao_cumprido_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contratoocorrencianotificacao_cumpridoauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'" + sGXsfl_87_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Internalname, context.localUtil.Format(AV66DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate, "99/99/99"), context.localUtil.Format( AV66DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,120);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_87_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Internalname, context.localUtil.Format(AV67DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo, "99/99/99"), context.localUtil.Format( AV67DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,121);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_protocolo_Internalname, AV70TFContratoOcorrenciaNotificacao_Protocolo, StringUtil.RTrim( context.localUtil.Format( AV70TFContratoOcorrenciaNotificacao_Protocolo, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,122);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_protocolo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_protocolo_Visible, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_protocolo_sel_Internalname, AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel, StringUtil.RTrim( context.localUtil.Format( AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,123);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_protocolo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_protocolo_sel_Visible, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_nome_Internalname, StringUtil.RTrim( AV74TFContratoOcorrenciaNotificacao_Nome), StringUtil.RTrim( context.localUtil.Format( AV74TFContratoOcorrenciaNotificacao_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,124);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_nome_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratoocorrencianotificacao_nome_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoOcorrenciaNotificacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_nome_sel_Internalname, StringUtil.RTrim( AV75TFContratoOcorrenciaNotificacao_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV75TFContratoOcorrenciaNotificacao_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,125);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_nome_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratoocorrencianotificacao_nome_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoOcorrenciaNotificacao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATO_NUMEROContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'" + sGXsfl_87_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname, AV48ddo_Contrato_NumeroTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,127);\"", 0, edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoOcorrenciaNotificacao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 129,'',false,'" + sGXsfl_87_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Internalname, AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,129);\"", 0, edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoOcorrenciaNotificacao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'" + sGXsfl_87_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Internalname, AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,131);\"", 0, edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoOcorrenciaNotificacao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'" + sGXsfl_87_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Internalname, AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,133);\"", 0, edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoOcorrenciaNotificacao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'" + sGXsfl_87_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Internalname, AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,135);\"", 0, edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoOcorrenciaNotificacao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 137,'',false,'" + sGXsfl_87_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Internalname, AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,137);\"", 0, edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoOcorrenciaNotificacao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 139,'',false,'" + sGXsfl_87_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Internalname, AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,139);\"", 0, edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoOcorrenciaNotificacao.htm");
         }
         wbLoad = true;
      }

      protected void START7C2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Contrato Ocorrencia Notificacao", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP7C0( ) ;
      }

      protected void WS7C2( )
      {
         START7C2( ) ;
         EVT7C2( ) ;
      }

      protected void EVT7C2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E117C2 */
                              E117C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATO_NUMERO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E127C2 */
                              E127C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E137C2 */
                              E137C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E147C2 */
                              E147C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E157C2 */
                              E157C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E167C2 */
                              E167C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E177C2 */
                              E177C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E187C2 */
                              E187C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E197C2 */
                              E197C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E207C2 */
                              E207C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E217C2 */
                              E217C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E227C2 */
                              E227C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E237C2 */
                              E237C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E247C2 */
                              E247C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E257C2 */
                              E257C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E267C2 */
                              E267C2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_87_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_87_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_87_idx), 4, 0)), 4, "0");
                              SubsflControlProps_872( ) ;
                              AV34Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV34Update)) ? AV108Update_GXI : context.convertURL( context.PathToRelativeUrl( AV34Update))));
                              AV35Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete)) ? AV109Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV35Delete))));
                              A297ContratoOcorrenciaNotificacao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoOcorrenciaNotificacao_Codigo_Internalname), ",", "."));
                              A294ContratoOcorrencia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoOcorrencia_Codigo_Internalname), ",", "."));
                              A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContrato_Codigo_Internalname), ",", "."));
                              A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
                              A298ContratoOcorrenciaNotificacao_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContratoOcorrenciaNotificacao_Data_Internalname), 0));
                              A300ContratoOcorrenciaNotificacao_Descricao = StringUtil.Upper( cgiGet( edtContratoOcorrenciaNotificacao_Descricao_Internalname));
                              A299ContratoOcorrenciaNotificacao_Prazo = (short)(context.localUtil.CToN( cgiGet( edtContratoOcorrenciaNotificacao_Prazo_Internalname), ",", "."));
                              A301ContratoOcorrenciaNotificacao_Cumprido = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContratoOcorrenciaNotificacao_Cumprido_Internalname), 0));
                              n301ContratoOcorrenciaNotificacao_Cumprido = false;
                              A302ContratoOcorrenciaNotificacao_Protocolo = cgiGet( edtContratoOcorrenciaNotificacao_Protocolo_Internalname);
                              n302ContratoOcorrenciaNotificacao_Protocolo = false;
                              A303ContratoOcorrenciaNotificacao_Responsavel = (int)(context.localUtil.CToN( cgiGet( edtContratoOcorrenciaNotificacao_Responsavel_Internalname), ",", "."));
                              A304ContratoOcorrenciaNotificacao_Nome = StringUtil.Upper( cgiGet( edtContratoOcorrenciaNotificacao_Nome_Internalname));
                              n304ContratoOcorrenciaNotificacao_Nome = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E277C2 */
                                    E277C2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E287C2 */
                                    E287C2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E297C2 */
                                    E297C2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratoocorrencianotificacao_data1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA1"), 0) != AV17ContratoOcorrenciaNotificacao_Data1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratoocorrencianotificacao_data_to1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1"), 0) != AV18ContratoOcorrenciaNotificacao_Data_To1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratoocorrencianotificacao_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_NOME1"), AV19ContratoOcorrenciaNotificacao_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratoocorrencianotificacao_data2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA2"), 0) != AV23ContratoOcorrenciaNotificacao_Data2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratoocorrencianotificacao_data_to2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2"), 0) != AV24ContratoOcorrenciaNotificacao_Data_To2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratoocorrencianotificacao_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_NOME2"), AV25ContratoOcorrenciaNotificacao_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontrato_numero Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO"), AV46TFContrato_Numero) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontrato_numero_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO_SEL"), AV47TFContrato_Numero_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoocorrencianotificacao_data Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DATA"), 0) != AV50TFContratoOcorrenciaNotificacao_Data )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoocorrencianotificacao_data_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO"), 0) != AV51TFContratoOcorrenciaNotificacao_Data_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoocorrencianotificacao_descricao Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO"), AV56TFContratoOcorrenciaNotificacao_Descricao) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoocorrencianotificacao_descricao_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL"), AV57TFContratoOcorrenciaNotificacao_Descricao_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoocorrencianotificacao_prazo Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO"), ",", ".") != Convert.ToDecimal( AV60TFContratoOcorrenciaNotificacao_Prazo )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoocorrencianotificacao_prazo_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO"), ",", ".") != Convert.ToDecimal( AV61TFContratoOcorrenciaNotificacao_Prazo_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoocorrencianotificacao_cumprido Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO"), 0) != AV64TFContratoOcorrenciaNotificacao_Cumprido )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoocorrencianotificacao_cumprido_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO"), 0) != AV65TFContratoOcorrenciaNotificacao_Cumprido_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoocorrencianotificacao_protocolo Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO"), AV70TFContratoOcorrenciaNotificacao_Protocolo) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoocorrencianotificacao_protocolo_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL"), AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoocorrencianotificacao_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_NOME"), AV74TFContratoOcorrenciaNotificacao_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoocorrencianotificacao_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL"), AV75TFContratoOcorrenciaNotificacao_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE7C2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA7C2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATOOCORRENCIANOTIFICACAO_DATA", "Data", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATOOCORRENCIANOTIFICACAO_NOME", "Respons�vel", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTRATOOCORRENCIANOTIFICACAO_DATA", "Data", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATOOCORRENCIANOTIFICACAO_NOME", "Respons�vel", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_872( ) ;
         while ( nGXsfl_87_idx <= nRC_GXsfl_87 )
         {
            sendrow_872( ) ;
            nGXsfl_87_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_87_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_87_idx+1));
            sGXsfl_87_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_87_idx), 4, 0)), 4, "0");
            SubsflControlProps_872( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       DateTime AV17ContratoOcorrenciaNotificacao_Data1 ,
                                       DateTime AV18ContratoOcorrenciaNotificacao_Data_To1 ,
                                       String AV19ContratoOcorrenciaNotificacao_Nome1 ,
                                       String AV21DynamicFiltersSelector2 ,
                                       short AV22DynamicFiltersOperator2 ,
                                       DateTime AV23ContratoOcorrenciaNotificacao_Data2 ,
                                       DateTime AV24ContratoOcorrenciaNotificacao_Data_To2 ,
                                       String AV25ContratoOcorrenciaNotificacao_Nome2 ,
                                       bool AV20DynamicFiltersEnabled2 ,
                                       String AV46TFContrato_Numero ,
                                       String AV47TFContrato_Numero_Sel ,
                                       DateTime AV50TFContratoOcorrenciaNotificacao_Data ,
                                       DateTime AV51TFContratoOcorrenciaNotificacao_Data_To ,
                                       String AV56TFContratoOcorrenciaNotificacao_Descricao ,
                                       String AV57TFContratoOcorrenciaNotificacao_Descricao_Sel ,
                                       short AV60TFContratoOcorrenciaNotificacao_Prazo ,
                                       short AV61TFContratoOcorrenciaNotificacao_Prazo_To ,
                                       DateTime AV64TFContratoOcorrenciaNotificacao_Cumprido ,
                                       DateTime AV65TFContratoOcorrenciaNotificacao_Cumprido_To ,
                                       String AV70TFContratoOcorrenciaNotificacao_Protocolo ,
                                       String AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel ,
                                       String AV74TFContratoOcorrenciaNotificacao_Nome ,
                                       String AV75TFContratoOcorrenciaNotificacao_Nome_Sel ,
                                       String AV48ddo_Contrato_NumeroTitleControlIdToReplace ,
                                       String AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace ,
                                       String AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace ,
                                       String AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace ,
                                       String AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace ,
                                       String AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace ,
                                       String AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace ,
                                       String AV110Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV33DynamicFiltersIgnoreFirst ,
                                       bool AV32DynamicFiltersRemoving ,
                                       int A297ContratoOcorrenciaNotificacao_Codigo ,
                                       int A294ContratoOcorrencia_Codigo ,
                                       int A303ContratoOcorrenciaNotificacao_Responsavel )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF7C2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOOCORRENCIANOTIFICACAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_DATA", GetSecureSignedToken( "", A298ContratoOcorrenciaNotificacao_Data));
         GxWebStd.gx_hidden_field( context, "CONTRATOOCORRENCIANOTIFICACAO_DATA", context.localUtil.Format(A298ContratoOcorrenciaNotificacao_Data, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A300ContratoOcorrenciaNotificacao_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, "CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO", A300ContratoOcorrenciaNotificacao_Descricao);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_PRAZO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO", GetSecureSignedToken( "", A301ContratoOcorrenciaNotificacao_Cumprido));
         GxWebStd.gx_hidden_field( context, "CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO", context.localUtil.Format(A301ContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A302ContratoOcorrenciaNotificacao_Protocolo, ""))));
         GxWebStd.gx_hidden_field( context, "CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO", A302ContratoOcorrenciaNotificacao_Protocolo);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF7C2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV110Pgmname = "WWContratoOcorrenciaNotificacao";
         context.Gx_err = 0;
      }

      protected void RF7C2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 87;
         /* Execute user event: E287C2 */
         E287C2 ();
         nGXsfl_87_idx = 1;
         sGXsfl_87_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_87_idx), 4, 0)), 4, "0");
         SubsflControlProps_872( ) ;
         nGXsfl_87_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_872( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV83WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1 ,
                                                 AV85WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1 ,
                                                 AV86WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1 ,
                                                 AV84WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 ,
                                                 AV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 ,
                                                 AV88WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 ,
                                                 AV89WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2 ,
                                                 AV91WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2 ,
                                                 AV92WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2 ,
                                                 AV90WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 ,
                                                 AV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 ,
                                                 AV95WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel ,
                                                 AV94WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero ,
                                                 AV96WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data ,
                                                 AV97WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to ,
                                                 AV99WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel ,
                                                 AV98WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao ,
                                                 AV100WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo ,
                                                 AV101WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to ,
                                                 AV102WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido ,
                                                 AV103WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to ,
                                                 AV105WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel ,
                                                 AV104WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo ,
                                                 AV107WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel ,
                                                 AV106WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome ,
                                                 A298ContratoOcorrenciaNotificacao_Data ,
                                                 A304ContratoOcorrenciaNotificacao_Nome ,
                                                 A77Contrato_Numero ,
                                                 A300ContratoOcorrenciaNotificacao_Descricao ,
                                                 A299ContratoOcorrenciaNotificacao_Prazo ,
                                                 A301ContratoOcorrenciaNotificacao_Cumprido ,
                                                 A302ContratoOcorrenciaNotificacao_Protocolo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1), 100, "%");
            lV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1), 100, "%");
            lV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2), 100, "%");
            lV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2), 100, "%");
            lV94WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV94WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero), 20, "%");
            lV98WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao = StringUtil.Concat( StringUtil.RTrim( AV98WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao), "%", "");
            lV104WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo = StringUtil.Concat( StringUtil.RTrim( AV104WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo), "%", "");
            lV106WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome = StringUtil.PadR( StringUtil.RTrim( AV106WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome), 100, "%");
            /* Using cursor H007C2 */
            pr_default.execute(0, new Object[] {AV85WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1, AV86WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1, lV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1, lV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1, AV91WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2, AV92WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2, lV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2, lV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2, lV94WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero, AV95WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel, AV96WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data, AV97WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to, lV98WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao, AV99WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel, AV100WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo, AV101WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to, AV102WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido, AV103WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to, lV104WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo, AV105WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel, lV106WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome, AV107WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_87_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A304ContratoOcorrenciaNotificacao_Nome = H007C2_A304ContratoOcorrenciaNotificacao_Nome[0];
               n304ContratoOcorrenciaNotificacao_Nome = H007C2_n304ContratoOcorrenciaNotificacao_Nome[0];
               A303ContratoOcorrenciaNotificacao_Responsavel = H007C2_A303ContratoOcorrenciaNotificacao_Responsavel[0];
               A302ContratoOcorrenciaNotificacao_Protocolo = H007C2_A302ContratoOcorrenciaNotificacao_Protocolo[0];
               n302ContratoOcorrenciaNotificacao_Protocolo = H007C2_n302ContratoOcorrenciaNotificacao_Protocolo[0];
               A301ContratoOcorrenciaNotificacao_Cumprido = H007C2_A301ContratoOcorrenciaNotificacao_Cumprido[0];
               n301ContratoOcorrenciaNotificacao_Cumprido = H007C2_n301ContratoOcorrenciaNotificacao_Cumprido[0];
               A299ContratoOcorrenciaNotificacao_Prazo = H007C2_A299ContratoOcorrenciaNotificacao_Prazo[0];
               A300ContratoOcorrenciaNotificacao_Descricao = H007C2_A300ContratoOcorrenciaNotificacao_Descricao[0];
               A298ContratoOcorrenciaNotificacao_Data = H007C2_A298ContratoOcorrenciaNotificacao_Data[0];
               A77Contrato_Numero = H007C2_A77Contrato_Numero[0];
               A74Contrato_Codigo = H007C2_A74Contrato_Codigo[0];
               A294ContratoOcorrencia_Codigo = H007C2_A294ContratoOcorrencia_Codigo[0];
               A297ContratoOcorrenciaNotificacao_Codigo = H007C2_A297ContratoOcorrenciaNotificacao_Codigo[0];
               A304ContratoOcorrenciaNotificacao_Nome = H007C2_A304ContratoOcorrenciaNotificacao_Nome[0];
               n304ContratoOcorrenciaNotificacao_Nome = H007C2_n304ContratoOcorrenciaNotificacao_Nome[0];
               A74Contrato_Codigo = H007C2_A74Contrato_Codigo[0];
               A77Contrato_Numero = H007C2_A77Contrato_Numero[0];
               /* Execute user event: E297C2 */
               E297C2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 87;
            WB7C0( ) ;
         }
         nGXsfl_87_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV83WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV84WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV85WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1 = AV17ContratoOcorrenciaNotificacao_Data1;
         AV86WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1 = AV18ContratoOcorrenciaNotificacao_Data_To1;
         AV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 = AV19ContratoOcorrenciaNotificacao_Nome1;
         AV88WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV89WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV90WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV91WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2 = AV23ContratoOcorrenciaNotificacao_Data2;
         AV92WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2 = AV24ContratoOcorrenciaNotificacao_Data_To2;
         AV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 = AV25ContratoOcorrenciaNotificacao_Nome2;
         AV94WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero = AV46TFContrato_Numero;
         AV95WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel = AV47TFContrato_Numero_Sel;
         AV96WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data = AV50TFContratoOcorrenciaNotificacao_Data;
         AV97WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to = AV51TFContratoOcorrenciaNotificacao_Data_To;
         AV98WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao = AV56TFContratoOcorrenciaNotificacao_Descricao;
         AV99WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel = AV57TFContratoOcorrenciaNotificacao_Descricao_Sel;
         AV100WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo = AV60TFContratoOcorrenciaNotificacao_Prazo;
         AV101WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to = AV61TFContratoOcorrenciaNotificacao_Prazo_To;
         AV102WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido = AV64TFContratoOcorrenciaNotificacao_Cumprido;
         AV103WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to = AV65TFContratoOcorrenciaNotificacao_Cumprido_To;
         AV104WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo = AV70TFContratoOcorrenciaNotificacao_Protocolo;
         AV105WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel = AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel;
         AV106WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome = AV74TFContratoOcorrenciaNotificacao_Nome;
         AV107WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel = AV75TFContratoOcorrenciaNotificacao_Nome_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV83WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1 ,
                                              AV85WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1 ,
                                              AV86WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1 ,
                                              AV84WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 ,
                                              AV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 ,
                                              AV88WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 ,
                                              AV89WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2 ,
                                              AV91WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2 ,
                                              AV92WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2 ,
                                              AV90WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 ,
                                              AV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 ,
                                              AV95WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel ,
                                              AV94WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero ,
                                              AV96WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data ,
                                              AV97WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to ,
                                              AV99WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel ,
                                              AV98WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao ,
                                              AV100WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo ,
                                              AV101WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to ,
                                              AV102WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido ,
                                              AV103WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to ,
                                              AV105WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel ,
                                              AV104WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo ,
                                              AV107WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel ,
                                              AV106WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome ,
                                              A298ContratoOcorrenciaNotificacao_Data ,
                                              A304ContratoOcorrenciaNotificacao_Nome ,
                                              A77Contrato_Numero ,
                                              A300ContratoOcorrenciaNotificacao_Descricao ,
                                              A299ContratoOcorrenciaNotificacao_Prazo ,
                                              A301ContratoOcorrenciaNotificacao_Cumprido ,
                                              A302ContratoOcorrenciaNotificacao_Protocolo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1), 100, "%");
         lV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1), 100, "%");
         lV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2), 100, "%");
         lV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2), 100, "%");
         lV94WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV94WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero), 20, "%");
         lV98WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao = StringUtil.Concat( StringUtil.RTrim( AV98WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao), "%", "");
         lV104WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo = StringUtil.Concat( StringUtil.RTrim( AV104WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo), "%", "");
         lV106WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome = StringUtil.PadR( StringUtil.RTrim( AV106WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome), 100, "%");
         /* Using cursor H007C3 */
         pr_default.execute(1, new Object[] {AV85WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1, AV86WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1, lV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1, lV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1, AV91WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2, AV92WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2, lV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2, lV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2, lV94WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero, AV95WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel, AV96WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data, AV97WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to, lV98WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao, AV99WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel, AV100WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo, AV101WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to, AV102WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido, AV103WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to, lV104WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo, AV105WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel, lV106WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome, AV107WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel});
         GRID_nRecordCount = H007C3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV83WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV84WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV85WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1 = AV17ContratoOcorrenciaNotificacao_Data1;
         AV86WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1 = AV18ContratoOcorrenciaNotificacao_Data_To1;
         AV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 = AV19ContratoOcorrenciaNotificacao_Nome1;
         AV88WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV89WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV90WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV91WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2 = AV23ContratoOcorrenciaNotificacao_Data2;
         AV92WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2 = AV24ContratoOcorrenciaNotificacao_Data_To2;
         AV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 = AV25ContratoOcorrenciaNotificacao_Nome2;
         AV94WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero = AV46TFContrato_Numero;
         AV95WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel = AV47TFContrato_Numero_Sel;
         AV96WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data = AV50TFContratoOcorrenciaNotificacao_Data;
         AV97WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to = AV51TFContratoOcorrenciaNotificacao_Data_To;
         AV98WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao = AV56TFContratoOcorrenciaNotificacao_Descricao;
         AV99WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel = AV57TFContratoOcorrenciaNotificacao_Descricao_Sel;
         AV100WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo = AV60TFContratoOcorrenciaNotificacao_Prazo;
         AV101WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to = AV61TFContratoOcorrenciaNotificacao_Prazo_To;
         AV102WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido = AV64TFContratoOcorrenciaNotificacao_Cumprido;
         AV103WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to = AV65TFContratoOcorrenciaNotificacao_Cumprido_To;
         AV104WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo = AV70TFContratoOcorrenciaNotificacao_Protocolo;
         AV105WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel = AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel;
         AV106WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome = AV74TFContratoOcorrenciaNotificacao_Nome;
         AV107WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel = AV75TFContratoOcorrenciaNotificacao_Nome_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoOcorrenciaNotificacao_Data1, AV18ContratoOcorrenciaNotificacao_Data_To1, AV19ContratoOcorrenciaNotificacao_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContratoOcorrenciaNotificacao_Data2, AV24ContratoOcorrenciaNotificacao_Data_To2, AV25ContratoOcorrenciaNotificacao_Nome2, AV20DynamicFiltersEnabled2, AV46TFContrato_Numero, AV47TFContrato_Numero_Sel, AV50TFContratoOcorrenciaNotificacao_Data, AV51TFContratoOcorrenciaNotificacao_Data_To, AV56TFContratoOcorrenciaNotificacao_Descricao, AV57TFContratoOcorrenciaNotificacao_Descricao_Sel, AV60TFContratoOcorrenciaNotificacao_Prazo, AV61TFContratoOcorrenciaNotificacao_Prazo_To, AV64TFContratoOcorrenciaNotificacao_Cumprido, AV65TFContratoOcorrenciaNotificacao_Cumprido_To, AV70TFContratoOcorrenciaNotificacao_Protocolo, AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV74TFContratoOcorrenciaNotificacao_Nome, AV75TFContratoOcorrenciaNotificacao_Nome_Sel, AV48ddo_Contrato_NumeroTitleControlIdToReplace, AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, AV110Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A297ContratoOcorrenciaNotificacao_Codigo, A294ContratoOcorrencia_Codigo, A303ContratoOcorrenciaNotificacao_Responsavel) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV83WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV84WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV85WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1 = AV17ContratoOcorrenciaNotificacao_Data1;
         AV86WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1 = AV18ContratoOcorrenciaNotificacao_Data_To1;
         AV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 = AV19ContratoOcorrenciaNotificacao_Nome1;
         AV88WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV89WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV90WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV91WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2 = AV23ContratoOcorrenciaNotificacao_Data2;
         AV92WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2 = AV24ContratoOcorrenciaNotificacao_Data_To2;
         AV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 = AV25ContratoOcorrenciaNotificacao_Nome2;
         AV94WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero = AV46TFContrato_Numero;
         AV95WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel = AV47TFContrato_Numero_Sel;
         AV96WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data = AV50TFContratoOcorrenciaNotificacao_Data;
         AV97WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to = AV51TFContratoOcorrenciaNotificacao_Data_To;
         AV98WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao = AV56TFContratoOcorrenciaNotificacao_Descricao;
         AV99WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel = AV57TFContratoOcorrenciaNotificacao_Descricao_Sel;
         AV100WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo = AV60TFContratoOcorrenciaNotificacao_Prazo;
         AV101WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to = AV61TFContratoOcorrenciaNotificacao_Prazo_To;
         AV102WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido = AV64TFContratoOcorrenciaNotificacao_Cumprido;
         AV103WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to = AV65TFContratoOcorrenciaNotificacao_Cumprido_To;
         AV104WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo = AV70TFContratoOcorrenciaNotificacao_Protocolo;
         AV105WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel = AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel;
         AV106WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome = AV74TFContratoOcorrenciaNotificacao_Nome;
         AV107WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel = AV75TFContratoOcorrenciaNotificacao_Nome_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoOcorrenciaNotificacao_Data1, AV18ContratoOcorrenciaNotificacao_Data_To1, AV19ContratoOcorrenciaNotificacao_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContratoOcorrenciaNotificacao_Data2, AV24ContratoOcorrenciaNotificacao_Data_To2, AV25ContratoOcorrenciaNotificacao_Nome2, AV20DynamicFiltersEnabled2, AV46TFContrato_Numero, AV47TFContrato_Numero_Sel, AV50TFContratoOcorrenciaNotificacao_Data, AV51TFContratoOcorrenciaNotificacao_Data_To, AV56TFContratoOcorrenciaNotificacao_Descricao, AV57TFContratoOcorrenciaNotificacao_Descricao_Sel, AV60TFContratoOcorrenciaNotificacao_Prazo, AV61TFContratoOcorrenciaNotificacao_Prazo_To, AV64TFContratoOcorrenciaNotificacao_Cumprido, AV65TFContratoOcorrenciaNotificacao_Cumprido_To, AV70TFContratoOcorrenciaNotificacao_Protocolo, AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV74TFContratoOcorrenciaNotificacao_Nome, AV75TFContratoOcorrenciaNotificacao_Nome_Sel, AV48ddo_Contrato_NumeroTitleControlIdToReplace, AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, AV110Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A297ContratoOcorrenciaNotificacao_Codigo, A294ContratoOcorrencia_Codigo, A303ContratoOcorrenciaNotificacao_Responsavel) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV83WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV84WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV85WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1 = AV17ContratoOcorrenciaNotificacao_Data1;
         AV86WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1 = AV18ContratoOcorrenciaNotificacao_Data_To1;
         AV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 = AV19ContratoOcorrenciaNotificacao_Nome1;
         AV88WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV89WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV90WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV91WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2 = AV23ContratoOcorrenciaNotificacao_Data2;
         AV92WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2 = AV24ContratoOcorrenciaNotificacao_Data_To2;
         AV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 = AV25ContratoOcorrenciaNotificacao_Nome2;
         AV94WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero = AV46TFContrato_Numero;
         AV95WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel = AV47TFContrato_Numero_Sel;
         AV96WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data = AV50TFContratoOcorrenciaNotificacao_Data;
         AV97WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to = AV51TFContratoOcorrenciaNotificacao_Data_To;
         AV98WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao = AV56TFContratoOcorrenciaNotificacao_Descricao;
         AV99WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel = AV57TFContratoOcorrenciaNotificacao_Descricao_Sel;
         AV100WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo = AV60TFContratoOcorrenciaNotificacao_Prazo;
         AV101WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to = AV61TFContratoOcorrenciaNotificacao_Prazo_To;
         AV102WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido = AV64TFContratoOcorrenciaNotificacao_Cumprido;
         AV103WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to = AV65TFContratoOcorrenciaNotificacao_Cumprido_To;
         AV104WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo = AV70TFContratoOcorrenciaNotificacao_Protocolo;
         AV105WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel = AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel;
         AV106WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome = AV74TFContratoOcorrenciaNotificacao_Nome;
         AV107WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel = AV75TFContratoOcorrenciaNotificacao_Nome_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoOcorrenciaNotificacao_Data1, AV18ContratoOcorrenciaNotificacao_Data_To1, AV19ContratoOcorrenciaNotificacao_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContratoOcorrenciaNotificacao_Data2, AV24ContratoOcorrenciaNotificacao_Data_To2, AV25ContratoOcorrenciaNotificacao_Nome2, AV20DynamicFiltersEnabled2, AV46TFContrato_Numero, AV47TFContrato_Numero_Sel, AV50TFContratoOcorrenciaNotificacao_Data, AV51TFContratoOcorrenciaNotificacao_Data_To, AV56TFContratoOcorrenciaNotificacao_Descricao, AV57TFContratoOcorrenciaNotificacao_Descricao_Sel, AV60TFContratoOcorrenciaNotificacao_Prazo, AV61TFContratoOcorrenciaNotificacao_Prazo_To, AV64TFContratoOcorrenciaNotificacao_Cumprido, AV65TFContratoOcorrenciaNotificacao_Cumprido_To, AV70TFContratoOcorrenciaNotificacao_Protocolo, AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV74TFContratoOcorrenciaNotificacao_Nome, AV75TFContratoOcorrenciaNotificacao_Nome_Sel, AV48ddo_Contrato_NumeroTitleControlIdToReplace, AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, AV110Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A297ContratoOcorrenciaNotificacao_Codigo, A294ContratoOcorrencia_Codigo, A303ContratoOcorrenciaNotificacao_Responsavel) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV83WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV84WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV85WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1 = AV17ContratoOcorrenciaNotificacao_Data1;
         AV86WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1 = AV18ContratoOcorrenciaNotificacao_Data_To1;
         AV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 = AV19ContratoOcorrenciaNotificacao_Nome1;
         AV88WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV89WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV90WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV91WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2 = AV23ContratoOcorrenciaNotificacao_Data2;
         AV92WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2 = AV24ContratoOcorrenciaNotificacao_Data_To2;
         AV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 = AV25ContratoOcorrenciaNotificacao_Nome2;
         AV94WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero = AV46TFContrato_Numero;
         AV95WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel = AV47TFContrato_Numero_Sel;
         AV96WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data = AV50TFContratoOcorrenciaNotificacao_Data;
         AV97WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to = AV51TFContratoOcorrenciaNotificacao_Data_To;
         AV98WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao = AV56TFContratoOcorrenciaNotificacao_Descricao;
         AV99WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel = AV57TFContratoOcorrenciaNotificacao_Descricao_Sel;
         AV100WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo = AV60TFContratoOcorrenciaNotificacao_Prazo;
         AV101WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to = AV61TFContratoOcorrenciaNotificacao_Prazo_To;
         AV102WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido = AV64TFContratoOcorrenciaNotificacao_Cumprido;
         AV103WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to = AV65TFContratoOcorrenciaNotificacao_Cumprido_To;
         AV104WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo = AV70TFContratoOcorrenciaNotificacao_Protocolo;
         AV105WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel = AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel;
         AV106WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome = AV74TFContratoOcorrenciaNotificacao_Nome;
         AV107WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel = AV75TFContratoOcorrenciaNotificacao_Nome_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoOcorrenciaNotificacao_Data1, AV18ContratoOcorrenciaNotificacao_Data_To1, AV19ContratoOcorrenciaNotificacao_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContratoOcorrenciaNotificacao_Data2, AV24ContratoOcorrenciaNotificacao_Data_To2, AV25ContratoOcorrenciaNotificacao_Nome2, AV20DynamicFiltersEnabled2, AV46TFContrato_Numero, AV47TFContrato_Numero_Sel, AV50TFContratoOcorrenciaNotificacao_Data, AV51TFContratoOcorrenciaNotificacao_Data_To, AV56TFContratoOcorrenciaNotificacao_Descricao, AV57TFContratoOcorrenciaNotificacao_Descricao_Sel, AV60TFContratoOcorrenciaNotificacao_Prazo, AV61TFContratoOcorrenciaNotificacao_Prazo_To, AV64TFContratoOcorrenciaNotificacao_Cumprido, AV65TFContratoOcorrenciaNotificacao_Cumprido_To, AV70TFContratoOcorrenciaNotificacao_Protocolo, AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV74TFContratoOcorrenciaNotificacao_Nome, AV75TFContratoOcorrenciaNotificacao_Nome_Sel, AV48ddo_Contrato_NumeroTitleControlIdToReplace, AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, AV110Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A297ContratoOcorrenciaNotificacao_Codigo, A294ContratoOcorrencia_Codigo, A303ContratoOcorrenciaNotificacao_Responsavel) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV83WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV84WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV85WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1 = AV17ContratoOcorrenciaNotificacao_Data1;
         AV86WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1 = AV18ContratoOcorrenciaNotificacao_Data_To1;
         AV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 = AV19ContratoOcorrenciaNotificacao_Nome1;
         AV88WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV89WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV90WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV91WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2 = AV23ContratoOcorrenciaNotificacao_Data2;
         AV92WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2 = AV24ContratoOcorrenciaNotificacao_Data_To2;
         AV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 = AV25ContratoOcorrenciaNotificacao_Nome2;
         AV94WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero = AV46TFContrato_Numero;
         AV95WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel = AV47TFContrato_Numero_Sel;
         AV96WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data = AV50TFContratoOcorrenciaNotificacao_Data;
         AV97WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to = AV51TFContratoOcorrenciaNotificacao_Data_To;
         AV98WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao = AV56TFContratoOcorrenciaNotificacao_Descricao;
         AV99WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel = AV57TFContratoOcorrenciaNotificacao_Descricao_Sel;
         AV100WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo = AV60TFContratoOcorrenciaNotificacao_Prazo;
         AV101WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to = AV61TFContratoOcorrenciaNotificacao_Prazo_To;
         AV102WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido = AV64TFContratoOcorrenciaNotificacao_Cumprido;
         AV103WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to = AV65TFContratoOcorrenciaNotificacao_Cumprido_To;
         AV104WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo = AV70TFContratoOcorrenciaNotificacao_Protocolo;
         AV105WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel = AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel;
         AV106WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome = AV74TFContratoOcorrenciaNotificacao_Nome;
         AV107WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel = AV75TFContratoOcorrenciaNotificacao_Nome_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoOcorrenciaNotificacao_Data1, AV18ContratoOcorrenciaNotificacao_Data_To1, AV19ContratoOcorrenciaNotificacao_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContratoOcorrenciaNotificacao_Data2, AV24ContratoOcorrenciaNotificacao_Data_To2, AV25ContratoOcorrenciaNotificacao_Nome2, AV20DynamicFiltersEnabled2, AV46TFContrato_Numero, AV47TFContrato_Numero_Sel, AV50TFContratoOcorrenciaNotificacao_Data, AV51TFContratoOcorrenciaNotificacao_Data_To, AV56TFContratoOcorrenciaNotificacao_Descricao, AV57TFContratoOcorrenciaNotificacao_Descricao_Sel, AV60TFContratoOcorrenciaNotificacao_Prazo, AV61TFContratoOcorrenciaNotificacao_Prazo_To, AV64TFContratoOcorrenciaNotificacao_Cumprido, AV65TFContratoOcorrenciaNotificacao_Cumprido_To, AV70TFContratoOcorrenciaNotificacao_Protocolo, AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV74TFContratoOcorrenciaNotificacao_Nome, AV75TFContratoOcorrenciaNotificacao_Nome_Sel, AV48ddo_Contrato_NumeroTitleControlIdToReplace, AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, AV110Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A297ContratoOcorrenciaNotificacao_Codigo, A294ContratoOcorrencia_Codigo, A303ContratoOcorrenciaNotificacao_Responsavel) ;
         }
         return (int)(0) ;
      }

      protected void STRUP7C0( )
      {
         /* Before Start, stand alone formulas. */
         AV110Pgmname = "WWContratoOcorrenciaNotificacao";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E277C2 */
         E277C2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV77DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATO_NUMEROTITLEFILTERDATA"), AV45Contrato_NumeroTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOOCORRENCIANOTIFICACAO_DATATITLEFILTERDATA"), AV49ContratoOcorrenciaNotificacao_DataTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLEFILTERDATA"), AV55ContratoOcorrenciaNotificacao_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLEFILTERDATA"), AV59ContratoOcorrenciaNotificacao_PrazoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLEFILTERDATA"), AV63ContratoOcorrenciaNotificacao_CumpridoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLEFILTERDATA"), AV69ContratoOcorrenciaNotificacao_ProtocoloTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOOCORRENCIANOTIFICACAO_NOMETITLEFILTERDATA"), AV73ContratoOcorrenciaNotificacao_NomeTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratoocorrencianotificacao_data1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Ocorrencia Notificacao_Data1"}), 1, "vCONTRATOOCORRENCIANOTIFICACAO_DATA1");
               GX_FocusControl = edtavContratoocorrencianotificacao_data1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17ContratoOcorrenciaNotificacao_Data1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoOcorrenciaNotificacao_Data1", context.localUtil.Format(AV17ContratoOcorrenciaNotificacao_Data1, "99/99/99"));
            }
            else
            {
               AV17ContratoOcorrenciaNotificacao_Data1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratoocorrencianotificacao_data1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoOcorrenciaNotificacao_Data1", context.localUtil.Format(AV17ContratoOcorrenciaNotificacao_Data1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratoocorrencianotificacao_data_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Ocorrencia Notificacao_Data_To1"}), 1, "vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1");
               GX_FocusControl = edtavContratoocorrencianotificacao_data_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV18ContratoOcorrenciaNotificacao_Data_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContratoOcorrenciaNotificacao_Data_To1", context.localUtil.Format(AV18ContratoOcorrenciaNotificacao_Data_To1, "99/99/99"));
            }
            else
            {
               AV18ContratoOcorrenciaNotificacao_Data_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratoocorrencianotificacao_data_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContratoOcorrenciaNotificacao_Data_To1", context.localUtil.Format(AV18ContratoOcorrenciaNotificacao_Data_To1, "99/99/99"));
            }
            AV19ContratoOcorrenciaNotificacao_Nome1 = StringUtil.Upper( cgiGet( edtavContratoocorrencianotificacao_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratoOcorrenciaNotificacao_Nome1", AV19ContratoOcorrenciaNotificacao_Nome1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV21DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratoocorrencianotificacao_data2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Ocorrencia Notificacao_Data2"}), 1, "vCONTRATOOCORRENCIANOTIFICACAO_DATA2");
               GX_FocusControl = edtavContratoocorrencianotificacao_data2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV23ContratoOcorrenciaNotificacao_Data2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContratoOcorrenciaNotificacao_Data2", context.localUtil.Format(AV23ContratoOcorrenciaNotificacao_Data2, "99/99/99"));
            }
            else
            {
               AV23ContratoOcorrenciaNotificacao_Data2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratoocorrencianotificacao_data2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContratoOcorrenciaNotificacao_Data2", context.localUtil.Format(AV23ContratoOcorrenciaNotificacao_Data2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratoocorrencianotificacao_data_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Ocorrencia Notificacao_Data_To2"}), 1, "vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2");
               GX_FocusControl = edtavContratoocorrencianotificacao_data_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24ContratoOcorrenciaNotificacao_Data_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratoOcorrenciaNotificacao_Data_To2", context.localUtil.Format(AV24ContratoOcorrenciaNotificacao_Data_To2, "99/99/99"));
            }
            else
            {
               AV24ContratoOcorrenciaNotificacao_Data_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratoocorrencianotificacao_data_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratoOcorrenciaNotificacao_Data_To2", context.localUtil.Format(AV24ContratoOcorrenciaNotificacao_Data_To2, "99/99/99"));
            }
            AV25ContratoOcorrenciaNotificacao_Nome2 = StringUtil.Upper( cgiGet( edtavContratoocorrencianotificacao_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoOcorrenciaNotificacao_Nome2", AV25ContratoOcorrenciaNotificacao_Nome2);
            AV20DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
            AV46TFContrato_Numero = cgiGet( edtavTfcontrato_numero_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContrato_Numero", AV46TFContrato_Numero);
            AV47TFContrato_Numero_Sel = cgiGet( edtavTfcontrato_numero_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContrato_Numero_Sel", AV47TFContrato_Numero_Sel);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratoocorrencianotificacao_data_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Ocorrencia Notificacao_Data"}), 1, "vTFCONTRATOOCORRENCIANOTIFICACAO_DATA");
               GX_FocusControl = edtavTfcontratoocorrencianotificacao_data_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV50TFContratoOcorrenciaNotificacao_Data = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContratoOcorrenciaNotificacao_Data", context.localUtil.Format(AV50TFContratoOcorrenciaNotificacao_Data, "99/99/99"));
            }
            else
            {
               AV50TFContratoOcorrenciaNotificacao_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratoocorrencianotificacao_data_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContratoOcorrenciaNotificacao_Data", context.localUtil.Format(AV50TFContratoOcorrenciaNotificacao_Data, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratoocorrencianotificacao_data_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Ocorrencia Notificacao_Data_To"}), 1, "vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO");
               GX_FocusControl = edtavTfcontratoocorrencianotificacao_data_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV51TFContratoOcorrenciaNotificacao_Data_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratoOcorrenciaNotificacao_Data_To", context.localUtil.Format(AV51TFContratoOcorrenciaNotificacao_Data_To, "99/99/99"));
            }
            else
            {
               AV51TFContratoOcorrenciaNotificacao_Data_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratoocorrencianotificacao_data_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratoOcorrenciaNotificacao_Data_To", context.localUtil.Format(AV51TFContratoOcorrenciaNotificacao_Data_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratoocorrencianotificacao_dataauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Ocorrencia Notificacao_Data Aux Date"}), 1, "vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATAAUXDATE");
               GX_FocusControl = edtavDdo_contratoocorrencianotificacao_dataauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV52DDO_ContratoOcorrenciaNotificacao_DataAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52DDO_ContratoOcorrenciaNotificacao_DataAuxDate", context.localUtil.Format(AV52DDO_ContratoOcorrenciaNotificacao_DataAuxDate, "99/99/99"));
            }
            else
            {
               AV52DDO_ContratoOcorrenciaNotificacao_DataAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratoocorrencianotificacao_dataauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52DDO_ContratoOcorrenciaNotificacao_DataAuxDate", context.localUtil.Format(AV52DDO_ContratoOcorrenciaNotificacao_DataAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratoocorrencianotificacao_dataauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Ocorrencia Notificacao_Data Aux Date To"}), 1, "vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATAAUXDATETO");
               GX_FocusControl = edtavDdo_contratoocorrencianotificacao_dataauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV53DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo", context.localUtil.Format(AV53DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo, "99/99/99"));
            }
            else
            {
               AV53DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratoocorrencianotificacao_dataauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo", context.localUtil.Format(AV53DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo, "99/99/99"));
            }
            AV56TFContratoOcorrenciaNotificacao_Descricao = StringUtil.Upper( cgiGet( edtavTfcontratoocorrencianotificacao_descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContratoOcorrenciaNotificacao_Descricao", AV56TFContratoOcorrenciaNotificacao_Descricao);
            AV57TFContratoOcorrenciaNotificacao_Descricao_Sel = StringUtil.Upper( cgiGet( edtavTfcontratoocorrencianotificacao_descricao_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFContratoOcorrenciaNotificacao_Descricao_Sel", AV57TFContratoOcorrenciaNotificacao_Descricao_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_prazo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_prazo_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO");
               GX_FocusControl = edtavTfcontratoocorrencianotificacao_prazo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV60TFContratoOcorrenciaNotificacao_Prazo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoOcorrenciaNotificacao_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60TFContratoOcorrenciaNotificacao_Prazo), 4, 0)));
            }
            else
            {
               AV60TFContratoOcorrenciaNotificacao_Prazo = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_prazo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoOcorrenciaNotificacao_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60TFContratoOcorrenciaNotificacao_Prazo), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_prazo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_prazo_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO");
               GX_FocusControl = edtavTfcontratoocorrencianotificacao_prazo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV61TFContratoOcorrenciaNotificacao_Prazo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContratoOcorrenciaNotificacao_Prazo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFContratoOcorrenciaNotificacao_Prazo_To), 4, 0)));
            }
            else
            {
               AV61TFContratoOcorrenciaNotificacao_Prazo_To = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_prazo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContratoOcorrenciaNotificacao_Prazo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFContratoOcorrenciaNotificacao_Prazo_To), 4, 0)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratoocorrencianotificacao_cumprido_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Ocorrencia Notificacao_Cumprido"}), 1, "vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO");
               GX_FocusControl = edtavTfcontratoocorrencianotificacao_cumprido_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV64TFContratoOcorrenciaNotificacao_Cumprido = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoOcorrenciaNotificacao_Cumprido", context.localUtil.Format(AV64TFContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
            }
            else
            {
               AV64TFContratoOcorrenciaNotificacao_Cumprido = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratoocorrencianotificacao_cumprido_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoOcorrenciaNotificacao_Cumprido", context.localUtil.Format(AV64TFContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratoocorrencianotificacao_cumprido_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Ocorrencia Notificacao_Cumprido_To"}), 1, "vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO");
               GX_FocusControl = edtavTfcontratoocorrencianotificacao_cumprido_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV65TFContratoOcorrenciaNotificacao_Cumprido_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratoOcorrenciaNotificacao_Cumprido_To", context.localUtil.Format(AV65TFContratoOcorrenciaNotificacao_Cumprido_To, "99/99/99"));
            }
            else
            {
               AV65TFContratoOcorrenciaNotificacao_Cumprido_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratoocorrencianotificacao_cumprido_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratoOcorrenciaNotificacao_Cumprido_To", context.localUtil.Format(AV65TFContratoOcorrenciaNotificacao_Cumprido_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Ocorrencia Notificacao_Cumprido Aux Date"}), 1, "vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOAUXDATE");
               GX_FocusControl = edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV66DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate", context.localUtil.Format(AV66DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate, "99/99/99"));
            }
            else
            {
               AV66DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate", context.localUtil.Format(AV66DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Ocorrencia Notificacao_Cumprido Aux Date To"}), 1, "vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOAUXDATETO");
               GX_FocusControl = edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV67DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo", context.localUtil.Format(AV67DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo, "99/99/99"));
            }
            else
            {
               AV67DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo", context.localUtil.Format(AV67DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo, "99/99/99"));
            }
            AV70TFContratoOcorrenciaNotificacao_Protocolo = cgiGet( edtavTfcontratoocorrencianotificacao_protocolo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratoOcorrenciaNotificacao_Protocolo", AV70TFContratoOcorrenciaNotificacao_Protocolo);
            AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel = cgiGet( edtavTfcontratoocorrencianotificacao_protocolo_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel", AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel);
            AV74TFContratoOcorrenciaNotificacao_Nome = StringUtil.Upper( cgiGet( edtavTfcontratoocorrencianotificacao_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContratoOcorrenciaNotificacao_Nome", AV74TFContratoOcorrenciaNotificacao_Nome);
            AV75TFContratoOcorrenciaNotificacao_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfcontratoocorrencianotificacao_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContratoOcorrenciaNotificacao_Nome_Sel", AV75TFContratoOcorrenciaNotificacao_Nome_Sel);
            AV48ddo_Contrato_NumeroTitleControlIdToReplace = cgiGet( edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_Contrato_NumeroTitleControlIdToReplace", AV48ddo_Contrato_NumeroTitleControlIdToReplace);
            AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace = cgiGet( edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace", AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace);
            AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace", AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace);
            AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace = cgiGet( edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace", AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace);
            AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace = cgiGet( edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace", AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace);
            AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace = cgiGet( edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace", AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace);
            AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace = cgiGet( edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace", AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_87 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_87"), ",", "."));
            AV79GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV80GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contrato_numero_Caption = cgiGet( "DDO_CONTRATO_NUMERO_Caption");
            Ddo_contrato_numero_Tooltip = cgiGet( "DDO_CONTRATO_NUMERO_Tooltip");
            Ddo_contrato_numero_Cls = cgiGet( "DDO_CONTRATO_NUMERO_Cls");
            Ddo_contrato_numero_Filteredtext_set = cgiGet( "DDO_CONTRATO_NUMERO_Filteredtext_set");
            Ddo_contrato_numero_Selectedvalue_set = cgiGet( "DDO_CONTRATO_NUMERO_Selectedvalue_set");
            Ddo_contrato_numero_Dropdownoptionstype = cgiGet( "DDO_CONTRATO_NUMERO_Dropdownoptionstype");
            Ddo_contrato_numero_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATO_NUMERO_Titlecontrolidtoreplace");
            Ddo_contrato_numero_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includesortasc"));
            Ddo_contrato_numero_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includesortdsc"));
            Ddo_contrato_numero_Sortedstatus = cgiGet( "DDO_CONTRATO_NUMERO_Sortedstatus");
            Ddo_contrato_numero_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includefilter"));
            Ddo_contrato_numero_Filtertype = cgiGet( "DDO_CONTRATO_NUMERO_Filtertype");
            Ddo_contrato_numero_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Filterisrange"));
            Ddo_contrato_numero_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includedatalist"));
            Ddo_contrato_numero_Datalisttype = cgiGet( "DDO_CONTRATO_NUMERO_Datalisttype");
            Ddo_contrato_numero_Datalistproc = cgiGet( "DDO_CONTRATO_NUMERO_Datalistproc");
            Ddo_contrato_numero_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATO_NUMERO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contrato_numero_Sortasc = cgiGet( "DDO_CONTRATO_NUMERO_Sortasc");
            Ddo_contrato_numero_Sortdsc = cgiGet( "DDO_CONTRATO_NUMERO_Sortdsc");
            Ddo_contrato_numero_Loadingdata = cgiGet( "DDO_CONTRATO_NUMERO_Loadingdata");
            Ddo_contrato_numero_Cleanfilter = cgiGet( "DDO_CONTRATO_NUMERO_Cleanfilter");
            Ddo_contrato_numero_Noresultsfound = cgiGet( "DDO_CONTRATO_NUMERO_Noresultsfound");
            Ddo_contrato_numero_Searchbuttontext = cgiGet( "DDO_CONTRATO_NUMERO_Searchbuttontext");
            Ddo_contratoocorrencianotificacao_data_Caption = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Caption");
            Ddo_contratoocorrencianotificacao_data_Tooltip = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Tooltip");
            Ddo_contratoocorrencianotificacao_data_Cls = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Cls");
            Ddo_contratoocorrencianotificacao_data_Filteredtext_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filteredtext_set");
            Ddo_contratoocorrencianotificacao_data_Filteredtextto_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filteredtextto_set");
            Ddo_contratoocorrencianotificacao_data_Dropdownoptionstype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Dropdownoptionstype");
            Ddo_contratoocorrencianotificacao_data_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Titlecontrolidtoreplace");
            Ddo_contratoocorrencianotificacao_data_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Includesortasc"));
            Ddo_contratoocorrencianotificacao_data_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Includesortdsc"));
            Ddo_contratoocorrencianotificacao_data_Sortedstatus = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Sortedstatus");
            Ddo_contratoocorrencianotificacao_data_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Includefilter"));
            Ddo_contratoocorrencianotificacao_data_Filtertype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filtertype");
            Ddo_contratoocorrencianotificacao_data_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filterisrange"));
            Ddo_contratoocorrencianotificacao_data_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Includedatalist"));
            Ddo_contratoocorrencianotificacao_data_Sortasc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Sortasc");
            Ddo_contratoocorrencianotificacao_data_Sortdsc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Sortdsc");
            Ddo_contratoocorrencianotificacao_data_Cleanfilter = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Cleanfilter");
            Ddo_contratoocorrencianotificacao_data_Rangefilterfrom = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Rangefilterfrom");
            Ddo_contratoocorrencianotificacao_data_Rangefilterto = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Rangefilterto");
            Ddo_contratoocorrencianotificacao_data_Searchbuttontext = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Searchbuttontext");
            Ddo_contratoocorrencianotificacao_descricao_Caption = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Caption");
            Ddo_contratoocorrencianotificacao_descricao_Tooltip = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Tooltip");
            Ddo_contratoocorrencianotificacao_descricao_Cls = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Cls");
            Ddo_contratoocorrencianotificacao_descricao_Filteredtext_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Filteredtext_set");
            Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Selectedvalue_set");
            Ddo_contratoocorrencianotificacao_descricao_Dropdownoptionstype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Dropdownoptionstype");
            Ddo_contratoocorrencianotificacao_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_contratoocorrencianotificacao_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Includesortasc"));
            Ddo_contratoocorrencianotificacao_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Includesortdsc"));
            Ddo_contratoocorrencianotificacao_descricao_Sortedstatus = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Sortedstatus");
            Ddo_contratoocorrencianotificacao_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Includefilter"));
            Ddo_contratoocorrencianotificacao_descricao_Filtertype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Filtertype");
            Ddo_contratoocorrencianotificacao_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Filterisrange"));
            Ddo_contratoocorrencianotificacao_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Includedatalist"));
            Ddo_contratoocorrencianotificacao_descricao_Datalisttype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Datalisttype");
            Ddo_contratoocorrencianotificacao_descricao_Datalistproc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Datalistproc");
            Ddo_contratoocorrencianotificacao_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoocorrencianotificacao_descricao_Sortasc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Sortasc");
            Ddo_contratoocorrencianotificacao_descricao_Sortdsc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Sortdsc");
            Ddo_contratoocorrencianotificacao_descricao_Loadingdata = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Loadingdata");
            Ddo_contratoocorrencianotificacao_descricao_Cleanfilter = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Cleanfilter");
            Ddo_contratoocorrencianotificacao_descricao_Noresultsfound = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Noresultsfound");
            Ddo_contratoocorrencianotificacao_descricao_Searchbuttontext = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Searchbuttontext");
            Ddo_contratoocorrencianotificacao_prazo_Caption = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Caption");
            Ddo_contratoocorrencianotificacao_prazo_Tooltip = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Tooltip");
            Ddo_contratoocorrencianotificacao_prazo_Cls = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Cls");
            Ddo_contratoocorrencianotificacao_prazo_Filteredtext_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filteredtext_set");
            Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filteredtextto_set");
            Ddo_contratoocorrencianotificacao_prazo_Dropdownoptionstype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Dropdownoptionstype");
            Ddo_contratoocorrencianotificacao_prazo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Titlecontrolidtoreplace");
            Ddo_contratoocorrencianotificacao_prazo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Includesortasc"));
            Ddo_contratoocorrencianotificacao_prazo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Includesortdsc"));
            Ddo_contratoocorrencianotificacao_prazo_Sortedstatus = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Sortedstatus");
            Ddo_contratoocorrencianotificacao_prazo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Includefilter"));
            Ddo_contratoocorrencianotificacao_prazo_Filtertype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filtertype");
            Ddo_contratoocorrencianotificacao_prazo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filterisrange"));
            Ddo_contratoocorrencianotificacao_prazo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Includedatalist"));
            Ddo_contratoocorrencianotificacao_prazo_Sortasc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Sortasc");
            Ddo_contratoocorrencianotificacao_prazo_Sortdsc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Sortdsc");
            Ddo_contratoocorrencianotificacao_prazo_Cleanfilter = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Cleanfilter");
            Ddo_contratoocorrencianotificacao_prazo_Rangefilterfrom = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Rangefilterfrom");
            Ddo_contratoocorrencianotificacao_prazo_Rangefilterto = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Rangefilterto");
            Ddo_contratoocorrencianotificacao_prazo_Searchbuttontext = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Searchbuttontext");
            Ddo_contratoocorrencianotificacao_cumprido_Caption = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Caption");
            Ddo_contratoocorrencianotificacao_cumprido_Tooltip = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Tooltip");
            Ddo_contratoocorrencianotificacao_cumprido_Cls = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Cls");
            Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filteredtext_set");
            Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filteredtextto_set");
            Ddo_contratoocorrencianotificacao_cumprido_Dropdownoptionstype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Dropdownoptionstype");
            Ddo_contratoocorrencianotificacao_cumprido_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Titlecontrolidtoreplace");
            Ddo_contratoocorrencianotificacao_cumprido_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Includesortasc"));
            Ddo_contratoocorrencianotificacao_cumprido_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Includesortdsc"));
            Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Sortedstatus");
            Ddo_contratoocorrencianotificacao_cumprido_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Includefilter"));
            Ddo_contratoocorrencianotificacao_cumprido_Filtertype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filtertype");
            Ddo_contratoocorrencianotificacao_cumprido_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filterisrange"));
            Ddo_contratoocorrencianotificacao_cumprido_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Includedatalist"));
            Ddo_contratoocorrencianotificacao_cumprido_Sortasc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Sortasc");
            Ddo_contratoocorrencianotificacao_cumprido_Sortdsc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Sortdsc");
            Ddo_contratoocorrencianotificacao_cumprido_Cleanfilter = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Cleanfilter");
            Ddo_contratoocorrencianotificacao_cumprido_Rangefilterfrom = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Rangefilterfrom");
            Ddo_contratoocorrencianotificacao_cumprido_Rangefilterto = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Rangefilterto");
            Ddo_contratoocorrencianotificacao_cumprido_Searchbuttontext = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Searchbuttontext");
            Ddo_contratoocorrencianotificacao_protocolo_Caption = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Caption");
            Ddo_contratoocorrencianotificacao_protocolo_Tooltip = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Tooltip");
            Ddo_contratoocorrencianotificacao_protocolo_Cls = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Cls");
            Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Filteredtext_set");
            Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Selectedvalue_set");
            Ddo_contratoocorrencianotificacao_protocolo_Dropdownoptionstype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Dropdownoptionstype");
            Ddo_contratoocorrencianotificacao_protocolo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Titlecontrolidtoreplace");
            Ddo_contratoocorrencianotificacao_protocolo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Includesortasc"));
            Ddo_contratoocorrencianotificacao_protocolo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Includesortdsc"));
            Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Sortedstatus");
            Ddo_contratoocorrencianotificacao_protocolo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Includefilter"));
            Ddo_contratoocorrencianotificacao_protocolo_Filtertype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Filtertype");
            Ddo_contratoocorrencianotificacao_protocolo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Filterisrange"));
            Ddo_contratoocorrencianotificacao_protocolo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Includedatalist"));
            Ddo_contratoocorrencianotificacao_protocolo_Datalisttype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Datalisttype");
            Ddo_contratoocorrencianotificacao_protocolo_Datalistproc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Datalistproc");
            Ddo_contratoocorrencianotificacao_protocolo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoocorrencianotificacao_protocolo_Sortasc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Sortasc");
            Ddo_contratoocorrencianotificacao_protocolo_Sortdsc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Sortdsc");
            Ddo_contratoocorrencianotificacao_protocolo_Loadingdata = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Loadingdata");
            Ddo_contratoocorrencianotificacao_protocolo_Cleanfilter = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Cleanfilter");
            Ddo_contratoocorrencianotificacao_protocolo_Noresultsfound = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Noresultsfound");
            Ddo_contratoocorrencianotificacao_protocolo_Searchbuttontext = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Searchbuttontext");
            Ddo_contratoocorrencianotificacao_nome_Caption = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Caption");
            Ddo_contratoocorrencianotificacao_nome_Tooltip = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Tooltip");
            Ddo_contratoocorrencianotificacao_nome_Cls = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Cls");
            Ddo_contratoocorrencianotificacao_nome_Filteredtext_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Filteredtext_set");
            Ddo_contratoocorrencianotificacao_nome_Selectedvalue_set = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Selectedvalue_set");
            Ddo_contratoocorrencianotificacao_nome_Dropdownoptionstype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Dropdownoptionstype");
            Ddo_contratoocorrencianotificacao_nome_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Titlecontrolidtoreplace");
            Ddo_contratoocorrencianotificacao_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Includesortasc"));
            Ddo_contratoocorrencianotificacao_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Includesortdsc"));
            Ddo_contratoocorrencianotificacao_nome_Sortedstatus = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Sortedstatus");
            Ddo_contratoocorrencianotificacao_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Includefilter"));
            Ddo_contratoocorrencianotificacao_nome_Filtertype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Filtertype");
            Ddo_contratoocorrencianotificacao_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Filterisrange"));
            Ddo_contratoocorrencianotificacao_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Includedatalist"));
            Ddo_contratoocorrencianotificacao_nome_Datalisttype = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Datalisttype");
            Ddo_contratoocorrencianotificacao_nome_Datalistproc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Datalistproc");
            Ddo_contratoocorrencianotificacao_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoocorrencianotificacao_nome_Sortasc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Sortasc");
            Ddo_contratoocorrencianotificacao_nome_Sortdsc = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Sortdsc");
            Ddo_contratoocorrencianotificacao_nome_Loadingdata = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Loadingdata");
            Ddo_contratoocorrencianotificacao_nome_Cleanfilter = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Cleanfilter");
            Ddo_contratoocorrencianotificacao_nome_Noresultsfound = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Noresultsfound");
            Ddo_contratoocorrencianotificacao_nome_Searchbuttontext = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contrato_numero_Activeeventkey = cgiGet( "DDO_CONTRATO_NUMERO_Activeeventkey");
            Ddo_contrato_numero_Filteredtext_get = cgiGet( "DDO_CONTRATO_NUMERO_Filteredtext_get");
            Ddo_contrato_numero_Selectedvalue_get = cgiGet( "DDO_CONTRATO_NUMERO_Selectedvalue_get");
            Ddo_contratoocorrencianotificacao_data_Activeeventkey = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Activeeventkey");
            Ddo_contratoocorrencianotificacao_data_Filteredtext_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filteredtext_get");
            Ddo_contratoocorrencianotificacao_data_Filteredtextto_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filteredtextto_get");
            Ddo_contratoocorrencianotificacao_descricao_Activeeventkey = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Activeeventkey");
            Ddo_contratoocorrencianotificacao_descricao_Filteredtext_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Filteredtext_get");
            Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Selectedvalue_get");
            Ddo_contratoocorrencianotificacao_prazo_Activeeventkey = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Activeeventkey");
            Ddo_contratoocorrencianotificacao_prazo_Filteredtext_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filteredtext_get");
            Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filteredtextto_get");
            Ddo_contratoocorrencianotificacao_cumprido_Activeeventkey = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Activeeventkey");
            Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filteredtext_get");
            Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filteredtextto_get");
            Ddo_contratoocorrencianotificacao_protocolo_Activeeventkey = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Activeeventkey");
            Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Filteredtext_get");
            Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Selectedvalue_get");
            Ddo_contratoocorrencianotificacao_nome_Activeeventkey = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Activeeventkey");
            Ddo_contratoocorrencianotificacao_nome_Filteredtext_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Filteredtext_get");
            Ddo_contratoocorrencianotificacao_nome_Selectedvalue_get = cgiGet( "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA1"), 0) != AV17ContratoOcorrenciaNotificacao_Data1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1"), 0) != AV18ContratoOcorrenciaNotificacao_Data_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_NOME1"), AV19ContratoOcorrenciaNotificacao_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA2"), 0) != AV23ContratoOcorrenciaNotificacao_Data2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2"), 0) != AV24ContratoOcorrenciaNotificacao_Data_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATOOCORRENCIANOTIFICACAO_NOME2"), AV25ContratoOcorrenciaNotificacao_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO"), AV46TFContrato_Numero) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO_SEL"), AV47TFContrato_Numero_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DATA"), 0) != AV50TFContratoOcorrenciaNotificacao_Data )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO"), 0) != AV51TFContratoOcorrenciaNotificacao_Data_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO"), AV56TFContratoOcorrenciaNotificacao_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL"), AV57TFContratoOcorrenciaNotificacao_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO"), ",", ".") != Convert.ToDecimal( AV60TFContratoOcorrenciaNotificacao_Prazo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO"), ",", ".") != Convert.ToDecimal( AV61TFContratoOcorrenciaNotificacao_Prazo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO"), 0) != AV64TFContratoOcorrenciaNotificacao_Cumprido )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO"), 0) != AV65TFContratoOcorrenciaNotificacao_Cumprido_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO"), AV70TFContratoOcorrenciaNotificacao_Protocolo) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL"), AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_NOME"), AV74TFContratoOcorrenciaNotificacao_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL"), AV75TFContratoOcorrenciaNotificacao_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E277C2 */
         E277C2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E277C2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "CONTRATOOCORRENCIANOTIFICACAO_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector2 = "CONTRATOOCORRENCIANOTIFICACAO_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         edtavTfcontrato_numero_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_numero_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_numero_Visible), 5, 0)));
         edtavTfcontrato_numero_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_numero_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_numero_sel_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_data_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_data_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_data_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_data_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_data_to_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_descricao_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_descricao_sel_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_prazo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_prazo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_prazo_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_prazo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_prazo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_prazo_to_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_cumprido_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_cumprido_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_cumprido_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_cumprido_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_cumprido_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_cumprido_to_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_protocolo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_protocolo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_protocolo_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_protocolo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_protocolo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_protocolo_sel_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_nome_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoocorrencianotificacao_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_nome_sel_Visible), 5, 0)));
         Ddo_contrato_numero_Titlecontrolidtoreplace = subGrid_Internalname+"_Contrato_Numero";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "TitleControlIdToReplace", Ddo_contrato_numero_Titlecontrolidtoreplace);
         AV48ddo_Contrato_NumeroTitleControlIdToReplace = Ddo_contrato_numero_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_Contrato_NumeroTitleControlIdToReplace", AV48ddo_Contrato_NumeroTitleControlIdToReplace);
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoocorrencianotificacao_data_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoOcorrenciaNotificacao_Data";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_data_Internalname, "TitleControlIdToReplace", Ddo_contratoocorrencianotificacao_data_Titlecontrolidtoreplace);
         AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace = Ddo_contratoocorrencianotificacao_data_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace", AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace);
         edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoocorrencianotificacao_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoOcorrenciaNotificacao_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_descricao_Internalname, "TitleControlIdToReplace", Ddo_contratoocorrencianotificacao_descricao_Titlecontrolidtoreplace);
         AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace = Ddo_contratoocorrencianotificacao_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace", AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace);
         edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoocorrencianotificacao_prazo_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoOcorrenciaNotificacao_Prazo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_prazo_Internalname, "TitleControlIdToReplace", Ddo_contratoocorrencianotificacao_prazo_Titlecontrolidtoreplace);
         AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace = Ddo_contratoocorrencianotificacao_prazo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace", AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace);
         edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoocorrencianotificacao_cumprido_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoOcorrenciaNotificacao_Cumprido";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_cumprido_Internalname, "TitleControlIdToReplace", Ddo_contratoocorrencianotificacao_cumprido_Titlecontrolidtoreplace);
         AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace = Ddo_contratoocorrencianotificacao_cumprido_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace", AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace);
         edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoocorrencianotificacao_protocolo_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoOcorrenciaNotificacao_Protocolo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_protocolo_Internalname, "TitleControlIdToReplace", Ddo_contratoocorrencianotificacao_protocolo_Titlecontrolidtoreplace);
         AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace = Ddo_contratoocorrencianotificacao_protocolo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace", AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace);
         edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoocorrencianotificacao_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoOcorrenciaNotificacao_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_nome_Internalname, "TitleControlIdToReplace", Ddo_contratoocorrencianotificacao_nome_Titlecontrolidtoreplace);
         AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace = Ddo_contratoocorrencianotificacao_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace", AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace);
         edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Contrato Ocorrencia Notificacao";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Data", 0);
         cmbavOrderedby.addItem("2", "N� Contrato", 0);
         cmbavOrderedby.addItem("3", "Descri��o", 0);
         cmbavOrderedby.addItem("4", "Prazo", 0);
         cmbavOrderedby.addItem("5", "Cumprido", 0);
         cmbavOrderedby.addItem("6", "Protocolo", 0);
         cmbavOrderedby.addItem("7", "Respons�vel", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV77DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV77DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E287C2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV45Contrato_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV49ContratoOcorrenciaNotificacao_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV55ContratoOcorrenciaNotificacao_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV59ContratoOcorrenciaNotificacao_PrazoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV63ContratoOcorrenciaNotificacao_CumpridoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV69ContratoOcorrenciaNotificacao_ProtocoloTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV73ContratoOcorrenciaNotificacao_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContrato_Numero_Titleformat = 2;
         edtContrato_Numero_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "N� Contrato", AV48ddo_Contrato_NumeroTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Numero_Internalname, "Title", edtContrato_Numero_Title);
         edtContratoOcorrenciaNotificacao_Data_Titleformat = 2;
         edtContratoOcorrenciaNotificacao_Data_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data", AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrenciaNotificacao_Data_Internalname, "Title", edtContratoOcorrenciaNotificacao_Data_Title);
         edtContratoOcorrenciaNotificacao_Descricao_Titleformat = 2;
         edtContratoOcorrenciaNotificacao_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrenciaNotificacao_Descricao_Internalname, "Title", edtContratoOcorrenciaNotificacao_Descricao_Title);
         edtContratoOcorrenciaNotificacao_Prazo_Titleformat = 2;
         edtContratoOcorrenciaNotificacao_Prazo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Prazo", AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrenciaNotificacao_Prazo_Internalname, "Title", edtContratoOcorrenciaNotificacao_Prazo_Title);
         edtContratoOcorrenciaNotificacao_Cumprido_Titleformat = 2;
         edtContratoOcorrenciaNotificacao_Cumprido_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Cumprido", AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrenciaNotificacao_Cumprido_Internalname, "Title", edtContratoOcorrenciaNotificacao_Cumprido_Title);
         edtContratoOcorrenciaNotificacao_Protocolo_Titleformat = 2;
         edtContratoOcorrenciaNotificacao_Protocolo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Protocolo", AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrenciaNotificacao_Protocolo_Internalname, "Title", edtContratoOcorrenciaNotificacao_Protocolo_Title);
         edtContratoOcorrenciaNotificacao_Nome_Titleformat = 2;
         edtContratoOcorrenciaNotificacao_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Respons�vel", AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoOcorrenciaNotificacao_Nome_Internalname, "Title", edtContratoOcorrenciaNotificacao_Nome_Title);
         AV79GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV79GridCurrentPage), 10, 0)));
         AV80GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV80GridPageCount), 10, 0)));
         AV83WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV84WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV85WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1 = AV17ContratoOcorrenciaNotificacao_Data1;
         AV86WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1 = AV18ContratoOcorrenciaNotificacao_Data_To1;
         AV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 = AV19ContratoOcorrenciaNotificacao_Nome1;
         AV88WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV89WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV90WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 = AV22DynamicFiltersOperator2;
         AV91WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2 = AV23ContratoOcorrenciaNotificacao_Data2;
         AV92WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2 = AV24ContratoOcorrenciaNotificacao_Data_To2;
         AV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 = AV25ContratoOcorrenciaNotificacao_Nome2;
         AV94WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero = AV46TFContrato_Numero;
         AV95WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel = AV47TFContrato_Numero_Sel;
         AV96WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data = AV50TFContratoOcorrenciaNotificacao_Data;
         AV97WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to = AV51TFContratoOcorrenciaNotificacao_Data_To;
         AV98WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao = AV56TFContratoOcorrenciaNotificacao_Descricao;
         AV99WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel = AV57TFContratoOcorrenciaNotificacao_Descricao_Sel;
         AV100WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo = AV60TFContratoOcorrenciaNotificacao_Prazo;
         AV101WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to = AV61TFContratoOcorrenciaNotificacao_Prazo_To;
         AV102WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido = AV64TFContratoOcorrenciaNotificacao_Cumprido;
         AV103WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to = AV65TFContratoOcorrenciaNotificacao_Cumprido_To;
         AV104WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo = AV70TFContratoOcorrenciaNotificacao_Protocolo;
         AV105WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel = AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel;
         AV106WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome = AV74TFContratoOcorrenciaNotificacao_Nome;
         AV107WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel = AV75TFContratoOcorrenciaNotificacao_Nome_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV45Contrato_NumeroTitleFilterData", AV45Contrato_NumeroTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV49ContratoOcorrenciaNotificacao_DataTitleFilterData", AV49ContratoOcorrenciaNotificacao_DataTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV55ContratoOcorrenciaNotificacao_DescricaoTitleFilterData", AV55ContratoOcorrenciaNotificacao_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV59ContratoOcorrenciaNotificacao_PrazoTitleFilterData", AV59ContratoOcorrenciaNotificacao_PrazoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV63ContratoOcorrenciaNotificacao_CumpridoTitleFilterData", AV63ContratoOcorrenciaNotificacao_CumpridoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV69ContratoOcorrenciaNotificacao_ProtocoloTitleFilterData", AV69ContratoOcorrenciaNotificacao_ProtocoloTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV73ContratoOcorrenciaNotificacao_NomeTitleFilterData", AV73ContratoOcorrenciaNotificacao_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E117C2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV78PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV78PageToGo) ;
         }
      }

      protected void E127C2( )
      {
         /* Ddo_contrato_numero_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_numero_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_numero_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV46TFContrato_Numero = Ddo_contrato_numero_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContrato_Numero", AV46TFContrato_Numero);
            AV47TFContrato_Numero_Sel = Ddo_contrato_numero_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContrato_Numero_Sel", AV47TFContrato_Numero_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E137C2( )
      {
         /* Ddo_contratoocorrencianotificacao_data_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_data_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_data_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_data_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_data_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_data_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_data_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_data_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV50TFContratoOcorrenciaNotificacao_Data = context.localUtil.CToD( Ddo_contratoocorrencianotificacao_data_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContratoOcorrenciaNotificacao_Data", context.localUtil.Format(AV50TFContratoOcorrenciaNotificacao_Data, "99/99/99"));
            AV51TFContratoOcorrenciaNotificacao_Data_To = context.localUtil.CToD( Ddo_contratoocorrencianotificacao_data_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratoOcorrenciaNotificacao_Data_To", context.localUtil.Format(AV51TFContratoOcorrenciaNotificacao_Data_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E147C2( )
      {
         /* Ddo_contratoocorrencianotificacao_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_descricao_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_descricao_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV56TFContratoOcorrenciaNotificacao_Descricao = Ddo_contratoocorrencianotificacao_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContratoOcorrenciaNotificacao_Descricao", AV56TFContratoOcorrenciaNotificacao_Descricao);
            AV57TFContratoOcorrenciaNotificacao_Descricao_Sel = Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFContratoOcorrenciaNotificacao_Descricao_Sel", AV57TFContratoOcorrenciaNotificacao_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E157C2( )
      {
         /* Ddo_contratoocorrencianotificacao_prazo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_prazo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_prazo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_prazo_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_prazo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_prazo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_prazo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_prazo_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_prazo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_prazo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV60TFContratoOcorrenciaNotificacao_Prazo = (short)(NumberUtil.Val( Ddo_contratoocorrencianotificacao_prazo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoOcorrenciaNotificacao_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60TFContratoOcorrenciaNotificacao_Prazo), 4, 0)));
            AV61TFContratoOcorrenciaNotificacao_Prazo_To = (short)(NumberUtil.Val( Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContratoOcorrenciaNotificacao_Prazo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFContratoOcorrenciaNotificacao_Prazo_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E167C2( )
      {
         /* Ddo_contratoocorrencianotificacao_cumprido_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_cumprido_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_cumprido_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_cumprido_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_cumprido_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_cumprido_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV64TFContratoOcorrenciaNotificacao_Cumprido = context.localUtil.CToD( Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoOcorrenciaNotificacao_Cumprido", context.localUtil.Format(AV64TFContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
            AV65TFContratoOcorrenciaNotificacao_Cumprido_To = context.localUtil.CToD( Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratoOcorrenciaNotificacao_Cumprido_To", context.localUtil.Format(AV65TFContratoOcorrenciaNotificacao_Cumprido_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E177C2( )
      {
         /* Ddo_contratoocorrencianotificacao_protocolo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_protocolo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_protocolo_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_protocolo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_protocolo_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_protocolo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV70TFContratoOcorrenciaNotificacao_Protocolo = Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratoOcorrenciaNotificacao_Protocolo", AV70TFContratoOcorrenciaNotificacao_Protocolo);
            AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel = Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel", AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E187C2( )
      {
         /* Ddo_contratoocorrencianotificacao_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_nome_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoocorrencianotificacao_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_nome_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV74TFContratoOcorrenciaNotificacao_Nome = Ddo_contratoocorrencianotificacao_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContratoOcorrenciaNotificacao_Nome", AV74TFContratoOcorrenciaNotificacao_Nome);
            AV75TFContratoOcorrenciaNotificacao_Nome_Sel = Ddo_contratoocorrencianotificacao_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContratoOcorrenciaNotificacao_Nome_Sel", AV75TFContratoOcorrenciaNotificacao_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E297C2( )
      {
         /* Grid_Load Routine */
         AV34Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV34Update);
         AV108Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("contratoocorrencianotificacao.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A297ContratoOcorrenciaNotificacao_Codigo) + "," + UrlEncode("" +A294ContratoOcorrencia_Codigo);
         AV35Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV35Delete);
         AV109Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("contratoocorrencianotificacao.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A297ContratoOcorrenciaNotificacao_Codigo) + "," + UrlEncode("" +A294ContratoOcorrencia_Codigo);
         edtContratoOcorrenciaNotificacao_Data_Link = formatLink("viewcontratoocorrencianotificacao.aspx") + "?" + UrlEncode("" +A297ContratoOcorrenciaNotificacao_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtContratoOcorrenciaNotificacao_Nome_Link = formatLink("viewpessoa.aspx") + "?" + UrlEncode("" +A303ContratoOcorrenciaNotificacao_Responsavel) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 87;
         }
         sendrow_872( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_87_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(87, GridRow);
         }
      }

      protected void E197C2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E247C2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV20DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E207C2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV33DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV33DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33DynamicFiltersIgnoreFirst", AV33DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoOcorrenciaNotificacao_Data1, AV18ContratoOcorrenciaNotificacao_Data_To1, AV19ContratoOcorrenciaNotificacao_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContratoOcorrenciaNotificacao_Data2, AV24ContratoOcorrenciaNotificacao_Data_To2, AV25ContratoOcorrenciaNotificacao_Nome2, AV20DynamicFiltersEnabled2, AV46TFContrato_Numero, AV47TFContrato_Numero_Sel, AV50TFContratoOcorrenciaNotificacao_Data, AV51TFContratoOcorrenciaNotificacao_Data_To, AV56TFContratoOcorrenciaNotificacao_Descricao, AV57TFContratoOcorrenciaNotificacao_Descricao_Sel, AV60TFContratoOcorrenciaNotificacao_Prazo, AV61TFContratoOcorrenciaNotificacao_Prazo_To, AV64TFContratoOcorrenciaNotificacao_Cumprido, AV65TFContratoOcorrenciaNotificacao_Cumprido_To, AV70TFContratoOcorrenciaNotificacao_Protocolo, AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV74TFContratoOcorrenciaNotificacao_Nome, AV75TFContratoOcorrenciaNotificacao_Nome_Sel, AV48ddo_Contrato_NumeroTitleControlIdToReplace, AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, AV110Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A297ContratoOcorrenciaNotificacao_Codigo, A294ContratoOcorrencia_Codigo, A303ContratoOcorrenciaNotificacao_Responsavel) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E257C2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E217C2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV32DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV32DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32DynamicFiltersRemoving", AV32DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoOcorrenciaNotificacao_Data1, AV18ContratoOcorrenciaNotificacao_Data_To1, AV19ContratoOcorrenciaNotificacao_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23ContratoOcorrenciaNotificacao_Data2, AV24ContratoOcorrenciaNotificacao_Data_To2, AV25ContratoOcorrenciaNotificacao_Nome2, AV20DynamicFiltersEnabled2, AV46TFContrato_Numero, AV47TFContrato_Numero_Sel, AV50TFContratoOcorrenciaNotificacao_Data, AV51TFContratoOcorrenciaNotificacao_Data_To, AV56TFContratoOcorrenciaNotificacao_Descricao, AV57TFContratoOcorrenciaNotificacao_Descricao_Sel, AV60TFContratoOcorrenciaNotificacao_Prazo, AV61TFContratoOcorrenciaNotificacao_Prazo_To, AV64TFContratoOcorrenciaNotificacao_Cumprido, AV65TFContratoOcorrenciaNotificacao_Cumprido_To, AV70TFContratoOcorrenciaNotificacao_Protocolo, AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV74TFContratoOcorrenciaNotificacao_Nome, AV75TFContratoOcorrenciaNotificacao_Nome_Sel, AV48ddo_Contrato_NumeroTitleControlIdToReplace, AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, AV110Pgmname, AV10GridState, AV33DynamicFiltersIgnoreFirst, AV32DynamicFiltersRemoving, A297ContratoOcorrenciaNotificacao_Codigo, A294ContratoOcorrencia_Codigo, A303ContratoOcorrenciaNotificacao_Responsavel) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E267C2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E227C2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E237C2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("contratoocorrencianotificacao.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contrato_numero_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
         Ddo_contratoocorrencianotificacao_data_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_data_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_data_Sortedstatus);
         Ddo_contratoocorrencianotificacao_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_descricao_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_descricao_Sortedstatus);
         Ddo_contratoocorrencianotificacao_prazo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_prazo_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_prazo_Sortedstatus);
         Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_cumprido_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus);
         Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_protocolo_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus);
         Ddo_contratoocorrencianotificacao_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_nome_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_nome_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_contrato_numero_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_contratoocorrencianotificacao_data_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_data_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_data_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contratoocorrencianotificacao_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_descricao_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_contratoocorrencianotificacao_prazo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_prazo_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_prazo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_cumprido_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_protocolo_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_contratoocorrencianotificacao_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_nome_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_nome_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible), 5, 0)));
         edtavContratoocorrencianotificacao_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoocorrencianotificacao_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoocorrencianotificacao_nome1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 )
         {
            tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 )
         {
            edtavContratoocorrencianotificacao_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoocorrencianotificacao_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoocorrencianotificacao_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible), 5, 0)));
         edtavContratoocorrencianotificacao_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoocorrencianotificacao_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoocorrencianotificacao_nome2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 )
         {
            tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 )
         {
            edtavContratoocorrencianotificacao_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoocorrencianotificacao_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoocorrencianotificacao_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         AV21DynamicFiltersSelector2 = "CONTRATOOCORRENCIANOTIFICACAO_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         AV23ContratoOcorrenciaNotificacao_Data2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContratoOcorrenciaNotificacao_Data2", context.localUtil.Format(AV23ContratoOcorrenciaNotificacao_Data2, "99/99/99"));
         AV24ContratoOcorrenciaNotificacao_Data_To2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratoOcorrenciaNotificacao_Data_To2", context.localUtil.Format(AV24ContratoOcorrenciaNotificacao_Data_To2, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S212( )
      {
         /* 'CLEANFILTERS' Routine */
         AV46TFContrato_Numero = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContrato_Numero", AV46TFContrato_Numero);
         Ddo_contrato_numero_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "FilteredText_set", Ddo_contrato_numero_Filteredtext_set);
         AV47TFContrato_Numero_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContrato_Numero_Sel", AV47TFContrato_Numero_Sel);
         Ddo_contrato_numero_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SelectedValue_set", Ddo_contrato_numero_Selectedvalue_set);
         AV50TFContratoOcorrenciaNotificacao_Data = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContratoOcorrenciaNotificacao_Data", context.localUtil.Format(AV50TFContratoOcorrenciaNotificacao_Data, "99/99/99"));
         Ddo_contratoocorrencianotificacao_data_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_data_Internalname, "FilteredText_set", Ddo_contratoocorrencianotificacao_data_Filteredtext_set);
         AV51TFContratoOcorrenciaNotificacao_Data_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratoOcorrenciaNotificacao_Data_To", context.localUtil.Format(AV51TFContratoOcorrenciaNotificacao_Data_To, "99/99/99"));
         Ddo_contratoocorrencianotificacao_data_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_data_Internalname, "FilteredTextTo_set", Ddo_contratoocorrencianotificacao_data_Filteredtextto_set);
         AV56TFContratoOcorrenciaNotificacao_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContratoOcorrenciaNotificacao_Descricao", AV56TFContratoOcorrenciaNotificacao_Descricao);
         Ddo_contratoocorrencianotificacao_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_descricao_Internalname, "FilteredText_set", Ddo_contratoocorrencianotificacao_descricao_Filteredtext_set);
         AV57TFContratoOcorrenciaNotificacao_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFContratoOcorrenciaNotificacao_Descricao_Sel", AV57TFContratoOcorrenciaNotificacao_Descricao_Sel);
         Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_descricao_Internalname, "SelectedValue_set", Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_set);
         AV60TFContratoOcorrenciaNotificacao_Prazo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoOcorrenciaNotificacao_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60TFContratoOcorrenciaNotificacao_Prazo), 4, 0)));
         Ddo_contratoocorrencianotificacao_prazo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_prazo_Internalname, "FilteredText_set", Ddo_contratoocorrencianotificacao_prazo_Filteredtext_set);
         AV61TFContratoOcorrenciaNotificacao_Prazo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContratoOcorrenciaNotificacao_Prazo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFContratoOcorrenciaNotificacao_Prazo_To), 4, 0)));
         Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_prazo_Internalname, "FilteredTextTo_set", Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_set);
         AV64TFContratoOcorrenciaNotificacao_Cumprido = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoOcorrenciaNotificacao_Cumprido", context.localUtil.Format(AV64TFContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
         Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_cumprido_Internalname, "FilteredText_set", Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_set);
         AV65TFContratoOcorrenciaNotificacao_Cumprido_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratoOcorrenciaNotificacao_Cumprido_To", context.localUtil.Format(AV65TFContratoOcorrenciaNotificacao_Cumprido_To, "99/99/99"));
         Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_cumprido_Internalname, "FilteredTextTo_set", Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_set);
         AV70TFContratoOcorrenciaNotificacao_Protocolo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratoOcorrenciaNotificacao_Protocolo", AV70TFContratoOcorrenciaNotificacao_Protocolo);
         Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_protocolo_Internalname, "FilteredText_set", Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_set);
         AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel", AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel);
         Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_protocolo_Internalname, "SelectedValue_set", Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_set);
         AV74TFContratoOcorrenciaNotificacao_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContratoOcorrenciaNotificacao_Nome", AV74TFContratoOcorrenciaNotificacao_Nome);
         Ddo_contratoocorrencianotificacao_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_nome_Internalname, "FilteredText_set", Ddo_contratoocorrencianotificacao_nome_Filteredtext_set);
         AV75TFContratoOcorrenciaNotificacao_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContratoOcorrenciaNotificacao_Nome_Sel", AV75TFContratoOcorrenciaNotificacao_Nome_Sel);
         Ddo_contratoocorrencianotificacao_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_nome_Internalname, "SelectedValue_set", Ddo_contratoocorrencianotificacao_nome_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "CONTRATOOCORRENCIANOTIFICACAO_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV17ContratoOcorrenciaNotificacao_Data1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoOcorrenciaNotificacao_Data1", context.localUtil.Format(AV17ContratoOcorrenciaNotificacao_Data1, "99/99/99"));
         AV18ContratoOcorrenciaNotificacao_Data_To1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContratoOcorrenciaNotificacao_Data_To1", context.localUtil.Format(AV18ContratoOcorrenciaNotificacao_Data_To1, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV36Session.Get(AV110Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV110Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV36Session.Get(AV110Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S222( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV111GXV1 = 1;
         while ( AV111GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV111GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO") == 0 )
            {
               AV46TFContrato_Numero = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFContrato_Numero", AV46TFContrato_Numero);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFContrato_Numero)) )
               {
                  Ddo_contrato_numero_Filteredtext_set = AV46TFContrato_Numero;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "FilteredText_set", Ddo_contrato_numero_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO_SEL") == 0 )
            {
               AV47TFContrato_Numero_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFContrato_Numero_Sel", AV47TFContrato_Numero_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFContrato_Numero_Sel)) )
               {
                  Ddo_contrato_numero_Selectedvalue_set = AV47TFContrato_Numero_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SelectedValue_set", Ddo_contrato_numero_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 )
            {
               AV50TFContratoOcorrenciaNotificacao_Data = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFContratoOcorrenciaNotificacao_Data", context.localUtil.Format(AV50TFContratoOcorrenciaNotificacao_Data, "99/99/99"));
               AV51TFContratoOcorrenciaNotificacao_Data_To = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFContratoOcorrenciaNotificacao_Data_To", context.localUtil.Format(AV51TFContratoOcorrenciaNotificacao_Data_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV50TFContratoOcorrenciaNotificacao_Data) )
               {
                  Ddo_contratoocorrencianotificacao_data_Filteredtext_set = context.localUtil.DToC( AV50TFContratoOcorrenciaNotificacao_Data, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_data_Internalname, "FilteredText_set", Ddo_contratoocorrencianotificacao_data_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV51TFContratoOcorrenciaNotificacao_Data_To) )
               {
                  Ddo_contratoocorrencianotificacao_data_Filteredtextto_set = context.localUtil.DToC( AV51TFContratoOcorrenciaNotificacao_Data_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_data_Internalname, "FilteredTextTo_set", Ddo_contratoocorrencianotificacao_data_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO") == 0 )
            {
               AV56TFContratoOcorrenciaNotificacao_Descricao = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContratoOcorrenciaNotificacao_Descricao", AV56TFContratoOcorrenciaNotificacao_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFContratoOcorrenciaNotificacao_Descricao)) )
               {
                  Ddo_contratoocorrencianotificacao_descricao_Filteredtext_set = AV56TFContratoOcorrenciaNotificacao_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_descricao_Internalname, "FilteredText_set", Ddo_contratoocorrencianotificacao_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL") == 0 )
            {
               AV57TFContratoOcorrenciaNotificacao_Descricao_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFContratoOcorrenciaNotificacao_Descricao_Sel", AV57TFContratoOcorrenciaNotificacao_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57TFContratoOcorrenciaNotificacao_Descricao_Sel)) )
               {
                  Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_set = AV57TFContratoOcorrenciaNotificacao_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_descricao_Internalname, "SelectedValue_set", Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 )
            {
               AV60TFContratoOcorrenciaNotificacao_Prazo = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratoOcorrenciaNotificacao_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60TFContratoOcorrenciaNotificacao_Prazo), 4, 0)));
               AV61TFContratoOcorrenciaNotificacao_Prazo_To = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContratoOcorrenciaNotificacao_Prazo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61TFContratoOcorrenciaNotificacao_Prazo_To), 4, 0)));
               if ( ! (0==AV60TFContratoOcorrenciaNotificacao_Prazo) )
               {
                  Ddo_contratoocorrencianotificacao_prazo_Filteredtext_set = StringUtil.Str( (decimal)(AV60TFContratoOcorrenciaNotificacao_Prazo), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_prazo_Internalname, "FilteredText_set", Ddo_contratoocorrencianotificacao_prazo_Filteredtext_set);
               }
               if ( ! (0==AV61TFContratoOcorrenciaNotificacao_Prazo_To) )
               {
                  Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_set = StringUtil.Str( (decimal)(AV61TFContratoOcorrenciaNotificacao_Prazo_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_prazo_Internalname, "FilteredTextTo_set", Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO") == 0 )
            {
               AV64TFContratoOcorrenciaNotificacao_Cumprido = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoOcorrenciaNotificacao_Cumprido", context.localUtil.Format(AV64TFContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
               AV65TFContratoOcorrenciaNotificacao_Cumprido_To = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratoOcorrenciaNotificacao_Cumprido_To", context.localUtil.Format(AV65TFContratoOcorrenciaNotificacao_Cumprido_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV64TFContratoOcorrenciaNotificacao_Cumprido) )
               {
                  Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_set = context.localUtil.DToC( AV64TFContratoOcorrenciaNotificacao_Cumprido, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_cumprido_Internalname, "FilteredText_set", Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV65TFContratoOcorrenciaNotificacao_Cumprido_To) )
               {
                  Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_set = context.localUtil.DToC( AV65TFContratoOcorrenciaNotificacao_Cumprido_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_cumprido_Internalname, "FilteredTextTo_set", Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO") == 0 )
            {
               AV70TFContratoOcorrenciaNotificacao_Protocolo = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratoOcorrenciaNotificacao_Protocolo", AV70TFContratoOcorrenciaNotificacao_Protocolo);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFContratoOcorrenciaNotificacao_Protocolo)) )
               {
                  Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_set = AV70TFContratoOcorrenciaNotificacao_Protocolo;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_protocolo_Internalname, "FilteredText_set", Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL") == 0 )
            {
               AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel", AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel)) )
               {
                  Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_set = AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_protocolo_Internalname, "SelectedValue_set", Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 )
            {
               AV74TFContratoOcorrenciaNotificacao_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContratoOcorrenciaNotificacao_Nome", AV74TFContratoOcorrenciaNotificacao_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74TFContratoOcorrenciaNotificacao_Nome)) )
               {
                  Ddo_contratoocorrencianotificacao_nome_Filteredtext_set = AV74TFContratoOcorrenciaNotificacao_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_nome_Internalname, "FilteredText_set", Ddo_contratoocorrencianotificacao_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL") == 0 )
            {
               AV75TFContratoOcorrenciaNotificacao_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContratoOcorrenciaNotificacao_Nome_Sel", AV75TFContratoOcorrenciaNotificacao_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75TFContratoOcorrenciaNotificacao_Nome_Sel)) )
               {
                  Ddo_contratoocorrencianotificacao_nome_Selectedvalue_set = AV75TFContratoOcorrenciaNotificacao_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoocorrencianotificacao_nome_Internalname, "SelectedValue_set", Ddo_contratoocorrencianotificacao_nome_Selectedvalue_set);
               }
            }
            AV111GXV1 = (int)(AV111GXV1+1);
         }
      }

      protected void S202( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 )
            {
               AV17ContratoOcorrenciaNotificacao_Data1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoOcorrenciaNotificacao_Data1", context.localUtil.Format(AV17ContratoOcorrenciaNotificacao_Data1, "99/99/99"));
               AV18ContratoOcorrenciaNotificacao_Data_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContratoOcorrenciaNotificacao_Data_To1", context.localUtil.Format(AV18ContratoOcorrenciaNotificacao_Data_To1, "99/99/99"));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV19ContratoOcorrenciaNotificacao_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContratoOcorrenciaNotificacao_Nome1", AV19ContratoOcorrenciaNotificacao_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV20DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV21DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 )
               {
                  AV23ContratoOcorrenciaNotificacao_Data2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ContratoOcorrenciaNotificacao_Data2", context.localUtil.Format(AV23ContratoOcorrenciaNotificacao_Data2, "99/99/99"));
                  AV24ContratoOcorrenciaNotificacao_Data_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratoOcorrenciaNotificacao_Data_To2", context.localUtil.Format(AV24ContratoOcorrenciaNotificacao_Data_To2, "99/99/99"));
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV25ContratoOcorrenciaNotificacao_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoOcorrenciaNotificacao_Nome2", AV25ContratoOcorrenciaNotificacao_Nome2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV32DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV36Session.Get(AV110Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFContrato_Numero)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATO_NUMERO";
            AV11GridStateFilterValue.gxTpr_Value = AV46TFContrato_Numero;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFContrato_Numero_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATO_NUMERO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV47TFContrato_Numero_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV50TFContratoOcorrenciaNotificacao_Data) && (DateTime.MinValue==AV51TFContratoOcorrenciaNotificacao_Data_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_DATA";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV50TFContratoOcorrenciaNotificacao_Data, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV51TFContratoOcorrenciaNotificacao_Data_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFContratoOcorrenciaNotificacao_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV56TFContratoOcorrenciaNotificacao_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57TFContratoOcorrenciaNotificacao_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV57TFContratoOcorrenciaNotificacao_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV60TFContratoOcorrenciaNotificacao_Prazo) && (0==AV61TFContratoOcorrenciaNotificacao_Prazo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_PRAZO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV60TFContratoOcorrenciaNotificacao_Prazo), 4, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV61TFContratoOcorrenciaNotificacao_Prazo_To), 4, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV64TFContratoOcorrenciaNotificacao_Cumprido) && (DateTime.MinValue==AV65TFContratoOcorrenciaNotificacao_Cumprido_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV64TFContratoOcorrenciaNotificacao_Cumprido, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV65TFContratoOcorrenciaNotificacao_Cumprido_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFContratoOcorrenciaNotificacao_Protocolo)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO";
            AV11GridStateFilterValue.gxTpr_Value = AV70TFContratoOcorrenciaNotificacao_Protocolo;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74TFContratoOcorrenciaNotificacao_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV74TFContratoOcorrenciaNotificacao_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75TFContratoOcorrenciaNotificacao_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV75TFContratoOcorrenciaNotificacao_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV110Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV33DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ! ( (DateTime.MinValue==AV17ContratoOcorrenciaNotificacao_Data1) && (DateTime.MinValue==AV18ContratoOcorrenciaNotificacao_Data_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV17ContratoOcorrenciaNotificacao_Data1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV18ContratoOcorrenciaNotificacao_Data_To1, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19ContratoOcorrenciaNotificacao_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV19ContratoOcorrenciaNotificacao_Nome1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ! ( (DateTime.MinValue==AV23ContratoOcorrenciaNotificacao_Data2) && (DateTime.MinValue==AV24ContratoOcorrenciaNotificacao_Data_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV23ContratoOcorrenciaNotificacao_Data2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV24ContratoOcorrenciaNotificacao_Data_To2, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ContratoOcorrenciaNotificacao_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25ContratoOcorrenciaNotificacao_Nome2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            if ( AV32DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S132( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV110Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContratoOcorrenciaNotificacao";
         AV36Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_7C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_7C2( true) ;
         }
         else
         {
            wb_table2_8_7C2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_7C2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_81_7C2( true) ;
         }
         else
         {
            wb_table3_81_7C2( false) ;
         }
         return  ;
      }

      protected void wb_table3_81_7C2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_7C2e( true) ;
         }
         else
         {
            wb_table1_2_7C2e( false) ;
         }
      }

      protected void wb_table3_81_7C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_84_7C2( true) ;
         }
         else
         {
            wb_table4_84_7C2( false) ;
         }
         return  ;
      }

      protected void wb_table4_84_7C2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_81_7C2e( true) ;
         }
         else
         {
            wb_table3_81_7C2e( false) ;
         }
      }

      protected void wb_table4_84_7C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"87\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "da Notifica��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo da Ocorr�ncia") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contrato") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(110), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_Numero_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_Numero_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_Numero_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoOcorrenciaNotificacao_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoOcorrenciaNotificacao_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoOcorrenciaNotificacao_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoOcorrenciaNotificacao_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoOcorrenciaNotificacao_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoOcorrenciaNotificacao_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoOcorrenciaNotificacao_Prazo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoOcorrenciaNotificacao_Prazo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoOcorrenciaNotificacao_Prazo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoOcorrenciaNotificacao_Cumprido_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoOcorrenciaNotificacao_Cumprido_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoOcorrenciaNotificacao_Cumprido_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoOcorrenciaNotificacao_Protocolo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoOcorrenciaNotificacao_Protocolo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoOcorrenciaNotificacao_Protocolo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "pessoa respons�vel") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoOcorrenciaNotificacao_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoOcorrenciaNotificacao_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoOcorrenciaNotificacao_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV34Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV35Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A77Contrato_Numero));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_Numero_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_Numero_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A298ContratoOcorrenciaNotificacao_Data, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoOcorrenciaNotificacao_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoOcorrenciaNotificacao_Data_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContratoOcorrenciaNotificacao_Data_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A300ContratoOcorrenciaNotificacao_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoOcorrenciaNotificacao_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoOcorrenciaNotificacao_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoOcorrenciaNotificacao_Prazo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoOcorrenciaNotificacao_Prazo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A301ContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoOcorrenciaNotificacao_Cumprido_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoOcorrenciaNotificacao_Cumprido_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A302ContratoOcorrenciaNotificacao_Protocolo);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoOcorrenciaNotificacao_Protocolo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoOcorrenciaNotificacao_Protocolo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A304ContratoOcorrenciaNotificacao_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoOcorrenciaNotificacao_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoOcorrenciaNotificacao_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContratoOcorrenciaNotificacao_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 87 )
         {
            wbEnd = 0;
            nRC_GXsfl_87 = (short)(nGXsfl_87_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_84_7C2e( true) ;
         }
         else
         {
            wb_table4_84_7C2e( false) ;
         }
      }

      protected void wb_table2_8_7C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoocorrencianotificacaotitle_Internalname, "Notifica��es", "", "", lblContratoocorrencianotificacaotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='Width100'>") ;
            wb_table5_13_7C2( true) ;
         }
         else
         {
            wb_table5_13_7C2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_7C2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_87_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWContratoOcorrenciaNotificacao.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_7C2( true) ;
         }
         else
         {
            wb_table6_23_7C2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_7C2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_7C2e( true) ;
         }
         else
         {
            wb_table2_8_7C2e( false) ;
         }
      }

      protected void wb_table6_23_7C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_7C2( true) ;
         }
         else
         {
            wb_table7_28_7C2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_7C2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_7C2e( true) ;
         }
         else
         {
            wb_table6_23_7C2e( false) ;
         }
      }

      protected void wb_table7_28_7C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_87_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWContratoOcorrenciaNotificacao.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_37_7C2( true) ;
         }
         else
         {
            wb_table8_37_7C2( false) ;
         }
         return  ;
      }

      protected void wb_table8_37_7C2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_87_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "", true, "HLP_WWContratoOcorrenciaNotificacao.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_62_7C2( true) ;
         }
         else
         {
            wb_table9_62_7C2( false) ;
         }
         return  ;
      }

      protected void wb_table9_62_7C2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_7C2e( true) ;
         }
         else
         {
            wb_table7_28_7C2e( false) ;
         }
      }

      protected void wb_table9_62_7C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_87_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", "", true, "HLP_WWContratoOcorrenciaNotificacao.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_67_7C2( true) ;
         }
         else
         {
            wb_table10_67_7C2( false) ;
         }
         return  ;
      }

      protected void wb_table10_67_7C2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencianotificacao_nome2_Internalname, StringUtil.RTrim( AV25ContratoOcorrenciaNotificacao_Nome2), StringUtil.RTrim( context.localUtil.Format( AV25ContratoOcorrenciaNotificacao_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,75);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencianotificacao_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratoocorrencianotificacao_nome2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_62_7C2e( true) ;
         }
         else
         {
            wb_table9_62_7C2e( false) ;
         }
      }

      protected void wb_table10_67_7C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Internalname, tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'" + sGXsfl_87_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratoocorrencianotificacao_data2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencianotificacao_data2_Internalname, context.localUtil.Format(AV23ContratoOcorrenciaNotificacao_Data2, "99/99/99"), context.localUtil.Format( AV23ContratoOcorrenciaNotificacao_Data2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,70);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencianotificacao_data2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavContratoocorrencianotificacao_data2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_87_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratoocorrencianotificacao_data_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencianotificacao_data_to2_Internalname, context.localUtil.Format(AV24ContratoOcorrenciaNotificacao_Data_To2, "99/99/99"), context.localUtil.Format( AV24ContratoOcorrenciaNotificacao_Data_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencianotificacao_data_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavContratoocorrencianotificacao_data_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_67_7C2e( true) ;
         }
         else
         {
            wb_table10_67_7C2e( false) ;
         }
      }

      protected void wb_table8_37_7C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_87_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_WWContratoOcorrenciaNotificacao.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table11_42_7C2( true) ;
         }
         else
         {
            wb_table11_42_7C2( false) ;
         }
         return  ;
      }

      protected void wb_table11_42_7C2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_87_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencianotificacao_nome1_Internalname, StringUtil.RTrim( AV19ContratoOcorrenciaNotificacao_Nome1), StringUtil.RTrim( context.localUtil.Format( AV19ContratoOcorrenciaNotificacao_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencianotificacao_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratoocorrencianotificacao_nome1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_37_7C2e( true) ;
         }
         else
         {
            wb_table8_37_7C2e( false) ;
         }
      }

      protected void wb_table11_42_7C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Internalname, tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'" + sGXsfl_87_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratoocorrencianotificacao_data1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencianotificacao_data1_Internalname, context.localUtil.Format(AV17ContratoOcorrenciaNotificacao_Data1, "99/99/99"), context.localUtil.Format( AV17ContratoOcorrenciaNotificacao_Data1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,45);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencianotificacao_data1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavContratoocorrencianotificacao_data1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_87_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratoocorrencianotificacao_data_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencianotificacao_data_to1_Internalname, context.localUtil.Format(AV18ContratoOcorrenciaNotificacao_Data_To1, "99/99/99"), context.localUtil.Format( AV18ContratoOcorrenciaNotificacao_Data_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencianotificacao_data_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            GxWebStd.gx_bitmap( context, edtavContratoocorrencianotificacao_data_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_42_7C2e( true) ;
         }
         else
         {
            wb_table11_42_7C2e( false) ;
         }
      }

      protected void wb_table5_13_7C2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoOcorrenciaNotificacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_7C2e( true) ;
         }
         else
         {
            wb_table5_13_7C2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA7C2( ) ;
         WS7C2( ) ;
         WE7C2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205181312067");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwcontratoocorrencianotificacao.js", "?20205181312068");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_872( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_87_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_87_idx;
         edtContratoOcorrenciaNotificacao_Codigo_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_CODIGO_"+sGXsfl_87_idx;
         edtContratoOcorrencia_Codigo_Internalname = "CONTRATOOCORRENCIA_CODIGO_"+sGXsfl_87_idx;
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO_"+sGXsfl_87_idx;
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO_"+sGXsfl_87_idx;
         edtContratoOcorrenciaNotificacao_Data_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_DATA_"+sGXsfl_87_idx;
         edtContratoOcorrenciaNotificacao_Descricao_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_"+sGXsfl_87_idx;
         edtContratoOcorrenciaNotificacao_Prazo_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_PRAZO_"+sGXsfl_87_idx;
         edtContratoOcorrenciaNotificacao_Cumprido_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_"+sGXsfl_87_idx;
         edtContratoOcorrenciaNotificacao_Protocolo_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_"+sGXsfl_87_idx;
         edtContratoOcorrenciaNotificacao_Responsavel_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_"+sGXsfl_87_idx;
         edtContratoOcorrenciaNotificacao_Nome_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_NOME_"+sGXsfl_87_idx;
      }

      protected void SubsflControlProps_fel_872( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_87_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_87_fel_idx;
         edtContratoOcorrenciaNotificacao_Codigo_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_CODIGO_"+sGXsfl_87_fel_idx;
         edtContratoOcorrencia_Codigo_Internalname = "CONTRATOOCORRENCIA_CODIGO_"+sGXsfl_87_fel_idx;
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO_"+sGXsfl_87_fel_idx;
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO_"+sGXsfl_87_fel_idx;
         edtContratoOcorrenciaNotificacao_Data_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_DATA_"+sGXsfl_87_fel_idx;
         edtContratoOcorrenciaNotificacao_Descricao_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_"+sGXsfl_87_fel_idx;
         edtContratoOcorrenciaNotificacao_Prazo_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_PRAZO_"+sGXsfl_87_fel_idx;
         edtContratoOcorrenciaNotificacao_Cumprido_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_"+sGXsfl_87_fel_idx;
         edtContratoOcorrenciaNotificacao_Protocolo_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_"+sGXsfl_87_fel_idx;
         edtContratoOcorrenciaNotificacao_Responsavel_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_"+sGXsfl_87_fel_idx;
         edtContratoOcorrenciaNotificacao_Nome_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_NOME_"+sGXsfl_87_fel_idx;
      }

      protected void sendrow_872( )
      {
         SubsflControlProps_872( ) ;
         WB7C0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_87_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_87_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_87_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV34Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV34Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV108Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV34Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV34Update)) ? AV108Update_GXI : context.PathToRelativeUrl( AV34Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV34Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV35Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV109Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete)) ? AV109Delete_GXI : context.PathToRelativeUrl( AV35Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV35Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrenciaNotificacao_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoOcorrenciaNotificacao_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)87,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrencia_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A294ContratoOcorrencia_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoOcorrencia_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)87,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)87,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Numero_Internalname,StringUtil.RTrim( A77Contrato_Numero),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Numero_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)110,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)87,(short)1,(short)-1,(short)-1,(bool)true,(String)"NumeroContrato",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrenciaNotificacao_Data_Internalname,context.localUtil.Format(A298ContratoOcorrenciaNotificacao_Data, "99/99/99"),context.localUtil.Format( A298ContratoOcorrenciaNotificacao_Data, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtContratoOcorrenciaNotificacao_Data_Link,(String)"",(String)"",(String)"",(String)edtContratoOcorrenciaNotificacao_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)87,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrenciaNotificacao_Descricao_Internalname,(String)A300ContratoOcorrenciaNotificacao_Descricao,StringUtil.RTrim( context.localUtil.Format( A300ContratoOcorrenciaNotificacao_Descricao, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoOcorrenciaNotificacao_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)87,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrenciaNotificacao_Prazo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoOcorrenciaNotificacao_Prazo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)87,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrenciaNotificacao_Cumprido_Internalname,context.localUtil.Format(A301ContratoOcorrenciaNotificacao_Cumprido, "99/99/99"),context.localUtil.Format( A301ContratoOcorrenciaNotificacao_Cumprido, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoOcorrenciaNotificacao_Cumprido_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)87,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrenciaNotificacao_Protocolo_Internalname,(String)A302ContratoOcorrenciaNotificacao_Protocolo,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoOcorrenciaNotificacao_Protocolo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)87,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrenciaNotificacao_Responsavel_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoOcorrenciaNotificacao_Responsavel_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)87,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrenciaNotificacao_Nome_Internalname,StringUtil.RTrim( A304ContratoOcorrenciaNotificacao_Nome),StringUtil.RTrim( context.localUtil.Format( A304ContratoOcorrenciaNotificacao_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtContratoOcorrenciaNotificacao_Nome_Link,(String)"",(String)"",(String)"",(String)edtContratoOcorrenciaNotificacao_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)87,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_CODIGO"+"_"+sGXsfl_87_idx, GetSecureSignedToken( sGXsfl_87_idx, context.localUtil.Format( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_DATA"+"_"+sGXsfl_87_idx, GetSecureSignedToken( sGXsfl_87_idx, A298ContratoOcorrenciaNotificacao_Data));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO"+"_"+sGXsfl_87_idx, GetSecureSignedToken( sGXsfl_87_idx, StringUtil.RTrim( context.localUtil.Format( A300ContratoOcorrenciaNotificacao_Descricao, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_PRAZO"+"_"+sGXsfl_87_idx, GetSecureSignedToken( sGXsfl_87_idx, context.localUtil.Format( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO"+"_"+sGXsfl_87_idx, GetSecureSignedToken( sGXsfl_87_idx, A301ContratoOcorrenciaNotificacao_Cumprido));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO"+"_"+sGXsfl_87_idx, GetSecureSignedToken( sGXsfl_87_idx, StringUtil.RTrim( context.localUtil.Format( A302ContratoOcorrenciaNotificacao_Protocolo, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL"+"_"+sGXsfl_87_idx, GetSecureSignedToken( sGXsfl_87_idx, context.localUtil.Format( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_87_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_87_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_87_idx+1));
            sGXsfl_87_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_87_idx), 4, 0)), 4, "0");
            SubsflControlProps_872( ) ;
         }
         /* End function sendrow_872 */
      }

      protected void init_default_properties( )
      {
         lblContratoocorrencianotificacaotitle_Internalname = "CONTRATOOCORRENCIANOTIFICACAOTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavContratoocorrencianotificacao_data1_Internalname = "vCONTRATOOCORRENCIANOTIFICACAO_DATA1";
         lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext1_Internalname = "DYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA_RANGEMIDDLETEXT1";
         edtavContratoocorrencianotificacao_data_to1_Internalname = "vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1";
         tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA1";
         edtavContratoocorrencianotificacao_nome1_Internalname = "vCONTRATOOCORRENCIANOTIFICACAO_NOME1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavContratoocorrencianotificacao_data2_Internalname = "vCONTRATOOCORRENCIANOTIFICACAO_DATA2";
         lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext2_Internalname = "DYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA_RANGEMIDDLETEXT2";
         edtavContratoocorrencianotificacao_data_to2_Internalname = "vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2";
         tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA2";
         edtavContratoocorrencianotificacao_nome2_Internalname = "vCONTRATOOCORRENCIANOTIFICACAO_NOME2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtContratoOcorrenciaNotificacao_Codigo_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_CODIGO";
         edtContratoOcorrencia_Codigo_Internalname = "CONTRATOOCORRENCIA_CODIGO";
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO";
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO";
         edtContratoOcorrenciaNotificacao_Data_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_DATA";
         edtContratoOcorrenciaNotificacao_Descricao_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO";
         edtContratoOcorrenciaNotificacao_Prazo_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_PRAZO";
         edtContratoOcorrenciaNotificacao_Cumprido_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO";
         edtContratoOcorrenciaNotificacao_Protocolo_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO";
         edtContratoOcorrenciaNotificacao_Responsavel_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL";
         edtContratoOcorrenciaNotificacao_Nome_Internalname = "CONTRATOOCORRENCIANOTIFICACAO_NOME";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         edtavTfcontrato_numero_Internalname = "vTFCONTRATO_NUMERO";
         edtavTfcontrato_numero_sel_Internalname = "vTFCONTRATO_NUMERO_SEL";
         edtavTfcontratoocorrencianotificacao_data_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_DATA";
         edtavTfcontratoocorrencianotificacao_data_to_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO";
         edtavDdo_contratoocorrencianotificacao_dataauxdate_Internalname = "vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATAAUXDATE";
         edtavDdo_contratoocorrencianotificacao_dataauxdateto_Internalname = "vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATAAUXDATETO";
         divDdo_contratoocorrencianotificacao_dataauxdates_Internalname = "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATAAUXDATES";
         edtavTfcontratoocorrencianotificacao_descricao_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO";
         edtavTfcontratoocorrencianotificacao_descricao_sel_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL";
         edtavTfcontratoocorrencianotificacao_prazo_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO";
         edtavTfcontratoocorrencianotificacao_prazo_to_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO";
         edtavTfcontratoocorrencianotificacao_cumprido_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO";
         edtavTfcontratoocorrencianotificacao_cumprido_to_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO";
         edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Internalname = "vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOAUXDATE";
         edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Internalname = "vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOAUXDATETO";
         divDdo_contratoocorrencianotificacao_cumpridoauxdates_Internalname = "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOAUXDATES";
         edtavTfcontratoocorrencianotificacao_protocolo_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO";
         edtavTfcontratoocorrencianotificacao_protocolo_sel_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL";
         edtavTfcontratoocorrencianotificacao_nome_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_NOME";
         edtavTfcontratoocorrencianotificacao_nome_sel_Internalname = "vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL";
         Ddo_contrato_numero_Internalname = "DDO_CONTRATO_NUMERO";
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE";
         Ddo_contratoocorrencianotificacao_data_Internalname = "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA";
         edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE";
         Ddo_contratoocorrencianotificacao_descricao_Internalname = "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO";
         edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_contratoocorrencianotificacao_prazo_Internalname = "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO";
         edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE";
         Ddo_contratoocorrencianotificacao_cumprido_Internalname = "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO";
         edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE";
         Ddo_contratoocorrencianotificacao_protocolo_Internalname = "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO";
         edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE";
         Ddo_contratoocorrencianotificacao_nome_Internalname = "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME";
         edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContratoOcorrenciaNotificacao_Nome_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Responsavel_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Protocolo_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Cumprido_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Prazo_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Descricao_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Data_Jsonclick = "";
         edtContrato_Numero_Jsonclick = "";
         edtContrato_Codigo_Jsonclick = "";
         edtContratoOcorrencia_Codigo_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Codigo_Jsonclick = "";
         edtavContratoocorrencianotificacao_data_to1_Jsonclick = "";
         edtavContratoocorrencianotificacao_data1_Jsonclick = "";
         edtavContratoocorrencianotificacao_nome1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavContratoocorrencianotificacao_data_to2_Jsonclick = "";
         edtavContratoocorrencianotificacao_data2_Jsonclick = "";
         edtavContratoocorrencianotificacao_nome2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtContratoOcorrenciaNotificacao_Nome_Link = "";
         edtContratoOcorrenciaNotificacao_Data_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtContratoOcorrenciaNotificacao_Nome_Titleformat = 0;
         edtContratoOcorrenciaNotificacao_Protocolo_Titleformat = 0;
         edtContratoOcorrenciaNotificacao_Cumprido_Titleformat = 0;
         edtContratoOcorrenciaNotificacao_Prazo_Titleformat = 0;
         edtContratoOcorrenciaNotificacao_Descricao_Titleformat = 0;
         edtContratoOcorrenciaNotificacao_Data_Titleformat = 0;
         edtContrato_Numero_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavContratoocorrencianotificacao_nome2_Visible = 1;
         tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavContratoocorrencianotificacao_nome1_Visible = 1;
         tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible = 1;
         edtContratoOcorrenciaNotificacao_Nome_Title = "Respons�vel";
         edtContratoOcorrenciaNotificacao_Protocolo_Title = "Protocolo";
         edtContratoOcorrenciaNotificacao_Cumprido_Title = "Cumprido";
         edtContratoOcorrenciaNotificacao_Prazo_Title = "Prazo";
         edtContratoOcorrenciaNotificacao_Descricao_Title = "Descri��o";
         edtContratoOcorrenciaNotificacao_Data_Title = "Data";
         edtContrato_Numero_Title = "N� Contrato";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratoocorrencianotificacao_nome_sel_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_nome_sel_Visible = 1;
         edtavTfcontratoocorrencianotificacao_nome_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_nome_Visible = 1;
         edtavTfcontratoocorrencianotificacao_protocolo_sel_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_protocolo_sel_Visible = 1;
         edtavTfcontratoocorrencianotificacao_protocolo_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_protocolo_Visible = 1;
         edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Jsonclick = "";
         edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_cumprido_to_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_cumprido_to_Visible = 1;
         edtavTfcontratoocorrencianotificacao_cumprido_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_cumprido_Visible = 1;
         edtavTfcontratoocorrencianotificacao_prazo_to_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_prazo_to_Visible = 1;
         edtavTfcontratoocorrencianotificacao_prazo_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_prazo_Visible = 1;
         edtavTfcontratoocorrencianotificacao_descricao_sel_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_descricao_sel_Visible = 1;
         edtavTfcontratoocorrencianotificacao_descricao_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_descricao_Visible = 1;
         edtavDdo_contratoocorrencianotificacao_dataauxdateto_Jsonclick = "";
         edtavDdo_contratoocorrencianotificacao_dataauxdate_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_data_to_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_data_to_Visible = 1;
         edtavTfcontratoocorrencianotificacao_data_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_data_Visible = 1;
         edtavTfcontrato_numero_sel_Jsonclick = "";
         edtavTfcontrato_numero_sel_Visible = 1;
         edtavTfcontrato_numero_Jsonclick = "";
         edtavTfcontrato_numero_Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_contratoocorrencianotificacao_nome_Searchbuttontext = "Pesquisar";
         Ddo_contratoocorrencianotificacao_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoocorrencianotificacao_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoocorrencianotificacao_nome_Loadingdata = "Carregando dados...";
         Ddo_contratoocorrencianotificacao_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoocorrencianotificacao_nome_Sortasc = "Ordenar de A � Z";
         Ddo_contratoocorrencianotificacao_nome_Datalistupdateminimumcharacters = 0;
         Ddo_contratoocorrencianotificacao_nome_Datalistproc = "GetWWContratoOcorrenciaNotificacaoFilterData";
         Ddo_contratoocorrencianotificacao_nome_Datalisttype = "Dynamic";
         Ddo_contratoocorrencianotificacao_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoocorrencianotificacao_nome_Filtertype = "Character";
         Ddo_contratoocorrencianotificacao_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_nome_Titlecontrolidtoreplace = "";
         Ddo_contratoocorrencianotificacao_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoocorrencianotificacao_nome_Cls = "ColumnSettings";
         Ddo_contratoocorrencianotificacao_nome_Tooltip = "Op��es";
         Ddo_contratoocorrencianotificacao_nome_Caption = "";
         Ddo_contratoocorrencianotificacao_protocolo_Searchbuttontext = "Pesquisar";
         Ddo_contratoocorrencianotificacao_protocolo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoocorrencianotificacao_protocolo_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoocorrencianotificacao_protocolo_Loadingdata = "Carregando dados...";
         Ddo_contratoocorrencianotificacao_protocolo_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoocorrencianotificacao_protocolo_Sortasc = "Ordenar de A � Z";
         Ddo_contratoocorrencianotificacao_protocolo_Datalistupdateminimumcharacters = 0;
         Ddo_contratoocorrencianotificacao_protocolo_Datalistproc = "GetWWContratoOcorrenciaNotificacaoFilterData";
         Ddo_contratoocorrencianotificacao_protocolo_Datalisttype = "Dynamic";
         Ddo_contratoocorrencianotificacao_protocolo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_protocolo_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoocorrencianotificacao_protocolo_Filtertype = "Character";
         Ddo_contratoocorrencianotificacao_protocolo_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_protocolo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_protocolo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_protocolo_Titlecontrolidtoreplace = "";
         Ddo_contratoocorrencianotificacao_protocolo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoocorrencianotificacao_protocolo_Cls = "ColumnSettings";
         Ddo_contratoocorrencianotificacao_protocolo_Tooltip = "Op��es";
         Ddo_contratoocorrencianotificacao_protocolo_Caption = "";
         Ddo_contratoocorrencianotificacao_cumprido_Searchbuttontext = "Pesquisar";
         Ddo_contratoocorrencianotificacao_cumprido_Rangefilterto = "At�";
         Ddo_contratoocorrencianotificacao_cumprido_Rangefilterfrom = "Desde";
         Ddo_contratoocorrencianotificacao_cumprido_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoocorrencianotificacao_cumprido_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoocorrencianotificacao_cumprido_Sortasc = "Ordenar de A � Z";
         Ddo_contratoocorrencianotificacao_cumprido_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoocorrencianotificacao_cumprido_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_cumprido_Filtertype = "Date";
         Ddo_contratoocorrencianotificacao_cumprido_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_cumprido_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_cumprido_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_cumprido_Titlecontrolidtoreplace = "";
         Ddo_contratoocorrencianotificacao_cumprido_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoocorrencianotificacao_cumprido_Cls = "ColumnSettings";
         Ddo_contratoocorrencianotificacao_cumprido_Tooltip = "Op��es";
         Ddo_contratoocorrencianotificacao_cumprido_Caption = "";
         Ddo_contratoocorrencianotificacao_prazo_Searchbuttontext = "Pesquisar";
         Ddo_contratoocorrencianotificacao_prazo_Rangefilterto = "At�";
         Ddo_contratoocorrencianotificacao_prazo_Rangefilterfrom = "Desde";
         Ddo_contratoocorrencianotificacao_prazo_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoocorrencianotificacao_prazo_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoocorrencianotificacao_prazo_Sortasc = "Ordenar de A � Z";
         Ddo_contratoocorrencianotificacao_prazo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoocorrencianotificacao_prazo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_prazo_Filtertype = "Numeric";
         Ddo_contratoocorrencianotificacao_prazo_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_prazo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_prazo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_prazo_Titlecontrolidtoreplace = "";
         Ddo_contratoocorrencianotificacao_prazo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoocorrencianotificacao_prazo_Cls = "ColumnSettings";
         Ddo_contratoocorrencianotificacao_prazo_Tooltip = "Op��es";
         Ddo_contratoocorrencianotificacao_prazo_Caption = "";
         Ddo_contratoocorrencianotificacao_descricao_Searchbuttontext = "Pesquisar";
         Ddo_contratoocorrencianotificacao_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoocorrencianotificacao_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoocorrencianotificacao_descricao_Loadingdata = "Carregando dados...";
         Ddo_contratoocorrencianotificacao_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoocorrencianotificacao_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_contratoocorrencianotificacao_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_contratoocorrencianotificacao_descricao_Datalistproc = "GetWWContratoOcorrenciaNotificacaoFilterData";
         Ddo_contratoocorrencianotificacao_descricao_Datalisttype = "Dynamic";
         Ddo_contratoocorrencianotificacao_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoocorrencianotificacao_descricao_Filtertype = "Character";
         Ddo_contratoocorrencianotificacao_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_descricao_Titlecontrolidtoreplace = "";
         Ddo_contratoocorrencianotificacao_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoocorrencianotificacao_descricao_Cls = "ColumnSettings";
         Ddo_contratoocorrencianotificacao_descricao_Tooltip = "Op��es";
         Ddo_contratoocorrencianotificacao_descricao_Caption = "";
         Ddo_contratoocorrencianotificacao_data_Searchbuttontext = "Pesquisar";
         Ddo_contratoocorrencianotificacao_data_Rangefilterto = "At�";
         Ddo_contratoocorrencianotificacao_data_Rangefilterfrom = "Desde";
         Ddo_contratoocorrencianotificacao_data_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoocorrencianotificacao_data_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoocorrencianotificacao_data_Sortasc = "Ordenar de A � Z";
         Ddo_contratoocorrencianotificacao_data_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoocorrencianotificacao_data_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_data_Filtertype = "Date";
         Ddo_contratoocorrencianotificacao_data_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_data_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_data_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_data_Titlecontrolidtoreplace = "";
         Ddo_contratoocorrencianotificacao_data_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoocorrencianotificacao_data_Cls = "ColumnSettings";
         Ddo_contratoocorrencianotificacao_data_Tooltip = "Op��es";
         Ddo_contratoocorrencianotificacao_data_Caption = "";
         Ddo_contrato_numero_Searchbuttontext = "Pesquisar";
         Ddo_contrato_numero_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contrato_numero_Cleanfilter = "Limpar pesquisa";
         Ddo_contrato_numero_Loadingdata = "Carregando dados...";
         Ddo_contrato_numero_Sortdsc = "Ordenar de Z � A";
         Ddo_contrato_numero_Sortasc = "Ordenar de A � Z";
         Ddo_contrato_numero_Datalistupdateminimumcharacters = 0;
         Ddo_contrato_numero_Datalistproc = "GetWWContratoOcorrenciaNotificacaoFilterData";
         Ddo_contrato_numero_Datalisttype = "Dynamic";
         Ddo_contrato_numero_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contrato_numero_Filtertype = "Character";
         Ddo_contrato_numero_Includefilter = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Titlecontrolidtoreplace = "";
         Ddo_contrato_numero_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contrato_numero_Cls = "ColumnSettings";
         Ddo_contrato_numero_Tooltip = "Op��es";
         Ddo_contrato_numero_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Contrato Ocorrencia Notificacao";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A303ContratoOcorrenciaNotificacao_Responsavel',fld:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV48ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV50TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV51TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV56TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV57TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV61TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV64TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV65TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV70TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV75TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV110Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV45Contrato_NumeroTitleFilterData',fld:'vCONTRATO_NUMEROTITLEFILTERDATA',pic:'',nv:null},{av:'AV49ContratoOcorrenciaNotificacao_DataTitleFilterData',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV55ContratoOcorrenciaNotificacao_DescricaoTitleFilterData',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV59ContratoOcorrenciaNotificacao_PrazoTitleFilterData',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLEFILTERDATA',pic:'',nv:null},{av:'AV63ContratoOcorrenciaNotificacao_CumpridoTitleFilterData',fld:'vCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLEFILTERDATA',pic:'',nv:null},{av:'AV69ContratoOcorrenciaNotificacao_ProtocoloTitleFilterData',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLEFILTERDATA',pic:'',nv:null},{av:'AV73ContratoOcorrenciaNotificacao_NomeTitleFilterData',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'edtContrato_Numero_Titleformat',ctrl:'CONTRATO_NUMERO',prop:'Titleformat'},{av:'edtContrato_Numero_Title',ctrl:'CONTRATO_NUMERO',prop:'Title'},{av:'edtContratoOcorrenciaNotificacao_Data_Titleformat',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'Titleformat'},{av:'edtContratoOcorrenciaNotificacao_Data_Title',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'Title'},{av:'edtContratoOcorrenciaNotificacao_Descricao_Titleformat',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'Titleformat'},{av:'edtContratoOcorrenciaNotificacao_Descricao_Title',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'Title'},{av:'edtContratoOcorrenciaNotificacao_Prazo_Titleformat',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'Titleformat'},{av:'edtContratoOcorrenciaNotificacao_Prazo_Title',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'Title'},{av:'edtContratoOcorrenciaNotificacao_Cumprido_Titleformat',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'Titleformat'},{av:'edtContratoOcorrenciaNotificacao_Cumprido_Title',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'Title'},{av:'edtContratoOcorrenciaNotificacao_Protocolo_Titleformat',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'Titleformat'},{av:'edtContratoOcorrenciaNotificacao_Protocolo_Title',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'Title'},{av:'edtContratoOcorrenciaNotificacao_Nome_Titleformat',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'Titleformat'},{av:'edtContratoOcorrenciaNotificacao_Nome_Title',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'Title'},{av:'AV79GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV80GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E117C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV50TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV51TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV56TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV57TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV61TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV64TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV65TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV70TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV75TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV48ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A303ContratoOcorrenciaNotificacao_Responsavel',fld:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATO_NUMERO.ONOPTIONCLICKED","{handler:'E127C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV50TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV51TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV56TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV57TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV61TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV64TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV65TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV70TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV75TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV48ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A303ContratoOcorrenciaNotificacao_Responsavel',fld:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contrato_numero_Activeeventkey',ctrl:'DDO_CONTRATO_NUMERO',prop:'ActiveEventKey'},{av:'Ddo_contrato_numero_Filteredtext_get',ctrl:'DDO_CONTRATO_NUMERO',prop:'FilteredText_get'},{av:'Ddo_contrato_numero_Selectedvalue_get',ctrl:'DDO_CONTRATO_NUMERO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'Ddo_contratoocorrencianotificacao_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_prazo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_nome_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA.ONOPTIONCLICKED","{handler:'E137C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV50TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV51TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV56TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV57TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV61TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV64TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV65TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV70TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV75TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV48ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A303ContratoOcorrenciaNotificacao_Responsavel',fld:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contratoocorrencianotificacao_data_Activeeventkey',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'ActiveEventKey'},{av:'Ddo_contratoocorrencianotificacao_data_Filteredtext_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'FilteredText_get'},{av:'Ddo_contratoocorrencianotificacao_data_Filteredtextto_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'SortedStatus'},{av:'AV50TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV51TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_prazo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_nome_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO.ONOPTIONCLICKED","{handler:'E147C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV50TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV51TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV56TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV57TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV61TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV64TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV65TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV70TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV75TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV48ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A303ContratoOcorrenciaNotificacao_Responsavel',fld:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contratoocorrencianotificacao_descricao_Activeeventkey',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_contratoocorrencianotificacao_descricao_Filteredtext_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SortedStatus'},{av:'AV56TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV57TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_prazo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_nome_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO.ONOPTIONCLICKED","{handler:'E157C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV50TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV51TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV56TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV57TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV61TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV64TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV65TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV70TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV75TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV48ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A303ContratoOcorrenciaNotificacao_Responsavel',fld:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contratoocorrencianotificacao_prazo_Activeeventkey',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'ActiveEventKey'},{av:'Ddo_contratoocorrencianotificacao_prazo_Filteredtext_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'FilteredText_get'},{av:'Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_prazo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'SortedStatus'},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV61TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_nome_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO.ONOPTIONCLICKED","{handler:'E167C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV50TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV51TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV56TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV57TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV61TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV64TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV65TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV70TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV75TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV48ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A303ContratoOcorrenciaNotificacao_Responsavel',fld:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contratoocorrencianotificacao_cumprido_Activeeventkey',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'ActiveEventKey'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'FilteredText_get'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'SortedStatus'},{av:'AV64TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV65TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_prazo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_nome_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO.ONOPTIONCLICKED","{handler:'E177C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV50TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV51TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV56TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV57TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV61TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV64TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV65TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV70TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV75TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV48ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A303ContratoOcorrenciaNotificacao_Responsavel',fld:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contratoocorrencianotificacao_protocolo_Activeeventkey',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'ActiveEventKey'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'FilteredText_get'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SortedStatus'},{av:'AV70TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_prazo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_nome_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME.ONOPTIONCLICKED","{handler:'E187C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV50TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV51TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV56TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV57TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV61TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV64TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV65TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV70TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV75TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV48ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A303ContratoOcorrenciaNotificacao_Responsavel',fld:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_contratoocorrencianotificacao_nome_Activeeventkey',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'ActiveEventKey'},{av:'Ddo_contratoocorrencianotificacao_nome_Filteredtext_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'FilteredText_get'},{av:'Ddo_contratoocorrencianotificacao_nome_Selectedvalue_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_nome_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SortedStatus'},{av:'AV74TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV75TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_prazo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E297C2',iparms:[{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A303ContratoOcorrenciaNotificacao_Responsavel',fld:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV34Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV35Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtContratoOcorrenciaNotificacao_Data_Link',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'Link'},{av:'edtContratoOcorrenciaNotificacao_Nome_Link',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E197C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV50TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV51TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV56TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV57TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV61TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV64TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV65TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV70TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV75TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV48ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A303ContratoOcorrenciaNotificacao_Responsavel',fld:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E247C2',iparms:[],oparms:[{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E207C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV50TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV51TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV56TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV57TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV61TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV64TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV65TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV70TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV75TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV48ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A303ContratoOcorrenciaNotificacao_Responsavel',fld:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'cmbavDynamicfiltersoperator2'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA2',prop:'Visible'},{av:'edtavContratoocorrencianotificacao_nome2_Visible',ctrl:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA1',prop:'Visible'},{av:'edtavContratoocorrencianotificacao_nome1_Visible',ctrl:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E257C2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA1',prop:'Visible'},{av:'edtavContratoocorrencianotificacao_nome1_Visible',ctrl:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E217C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV50TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV51TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV56TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV57TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV61TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV64TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV65TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV70TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV75TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV48ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A303ContratoOcorrenciaNotificacao_Responsavel',fld:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'cmbavDynamicfiltersoperator2'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA2',prop:'Visible'},{av:'edtavContratoocorrencianotificacao_nome2_Visible',ctrl:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA1',prop:'Visible'},{av:'edtavContratoocorrencianotificacao_nome1_Visible',ctrl:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E267C2',iparms:[{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA2',prop:'Visible'},{av:'edtavContratoocorrencianotificacao_nome2_Visible',ctrl:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E227C2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV50TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV51TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV56TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV57TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV61TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV64TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV65TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV70TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV74TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV75TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV48ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV110Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV33DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV32DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A303ContratoOcorrenciaNotificacao_Responsavel',fld:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV46TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'Ddo_contrato_numero_Filteredtext_set',ctrl:'DDO_CONTRATO_NUMERO',prop:'FilteredText_set'},{av:'AV47TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'Ddo_contrato_numero_Selectedvalue_set',ctrl:'DDO_CONTRATO_NUMERO',prop:'SelectedValue_set'},{av:'AV50TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'Ddo_contratoocorrencianotificacao_data_Filteredtext_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'FilteredText_set'},{av:'AV51TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'Ddo_contratoocorrencianotificacao_data_Filteredtextto_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'FilteredTextTo_set'},{av:'AV56TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'Ddo_contratoocorrencianotificacao_descricao_Filteredtext_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'FilteredText_set'},{av:'AV57TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SelectedValue_set'},{av:'AV60TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'Ddo_contratoocorrencianotificacao_prazo_Filteredtext_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'FilteredText_set'},{av:'AV61TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'FilteredTextTo_set'},{av:'AV64TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'FilteredText_set'},{av:'AV65TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'FilteredTextTo_set'},{av:'AV70TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'FilteredText_set'},{av:'AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SelectedValue_set'},{av:'AV74TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'Ddo_contratoocorrencianotificacao_nome_Filteredtext_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'FilteredText_set'},{av:'AV75TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_contratoocorrencianotificacao_nome_Selectedvalue_set',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA1',prop:'Visible'},{av:'edtavContratoocorrencianotificacao_nome1_Visible',ctrl:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19ContratoOcorrenciaNotificacao_Nome1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV25ContratoOcorrenciaNotificacao_Nome2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',pic:'@!',nv:''},{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA2',prop:'Visible'},{av:'edtavContratoocorrencianotificacao_nome2_Visible',ctrl:'vCONTRATOOCORRENCIANOTIFICACAO_NOME2',prop:'Visible'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E237C2',iparms:[{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contrato_numero_Activeeventkey = "";
         Ddo_contrato_numero_Filteredtext_get = "";
         Ddo_contrato_numero_Selectedvalue_get = "";
         Ddo_contratoocorrencianotificacao_data_Activeeventkey = "";
         Ddo_contratoocorrencianotificacao_data_Filteredtext_get = "";
         Ddo_contratoocorrencianotificacao_data_Filteredtextto_get = "";
         Ddo_contratoocorrencianotificacao_descricao_Activeeventkey = "";
         Ddo_contratoocorrencianotificacao_descricao_Filteredtext_get = "";
         Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_get = "";
         Ddo_contratoocorrencianotificacao_prazo_Activeeventkey = "";
         Ddo_contratoocorrencianotificacao_prazo_Filteredtext_get = "";
         Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_get = "";
         Ddo_contratoocorrencianotificacao_cumprido_Activeeventkey = "";
         Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_get = "";
         Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_get = "";
         Ddo_contratoocorrencianotificacao_protocolo_Activeeventkey = "";
         Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_get = "";
         Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_get = "";
         Ddo_contratoocorrencianotificacao_nome_Activeeventkey = "";
         Ddo_contratoocorrencianotificacao_nome_Filteredtext_get = "";
         Ddo_contratoocorrencianotificacao_nome_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17ContratoOcorrenciaNotificacao_Data1 = DateTime.MinValue;
         AV18ContratoOcorrenciaNotificacao_Data_To1 = DateTime.MinValue;
         AV19ContratoOcorrenciaNotificacao_Nome1 = "";
         AV21DynamicFiltersSelector2 = "";
         AV23ContratoOcorrenciaNotificacao_Data2 = DateTime.MinValue;
         AV24ContratoOcorrenciaNotificacao_Data_To2 = DateTime.MinValue;
         AV25ContratoOcorrenciaNotificacao_Nome2 = "";
         AV46TFContrato_Numero = "";
         AV47TFContrato_Numero_Sel = "";
         AV50TFContratoOcorrenciaNotificacao_Data = DateTime.MinValue;
         AV51TFContratoOcorrenciaNotificacao_Data_To = DateTime.MinValue;
         AV56TFContratoOcorrenciaNotificacao_Descricao = "";
         AV57TFContratoOcorrenciaNotificacao_Descricao_Sel = "";
         AV64TFContratoOcorrenciaNotificacao_Cumprido = DateTime.MinValue;
         AV65TFContratoOcorrenciaNotificacao_Cumprido_To = DateTime.MinValue;
         AV70TFContratoOcorrenciaNotificacao_Protocolo = "";
         AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel = "";
         AV74TFContratoOcorrenciaNotificacao_Nome = "";
         AV75TFContratoOcorrenciaNotificacao_Nome_Sel = "";
         AV48ddo_Contrato_NumeroTitleControlIdToReplace = "";
         AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace = "";
         AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace = "";
         AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace = "";
         AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace = "";
         AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace = "";
         AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace = "";
         AV110Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV77DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV45Contrato_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV49ContratoOcorrenciaNotificacao_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV55ContratoOcorrenciaNotificacao_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV59ContratoOcorrenciaNotificacao_PrazoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV63ContratoOcorrenciaNotificacao_CumpridoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV69ContratoOcorrenciaNotificacao_ProtocoloTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV73ContratoOcorrenciaNotificacao_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contrato_numero_Filteredtext_set = "";
         Ddo_contrato_numero_Selectedvalue_set = "";
         Ddo_contrato_numero_Sortedstatus = "";
         Ddo_contratoocorrencianotificacao_data_Filteredtext_set = "";
         Ddo_contratoocorrencianotificacao_data_Filteredtextto_set = "";
         Ddo_contratoocorrencianotificacao_data_Sortedstatus = "";
         Ddo_contratoocorrencianotificacao_descricao_Filteredtext_set = "";
         Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_set = "";
         Ddo_contratoocorrencianotificacao_descricao_Sortedstatus = "";
         Ddo_contratoocorrencianotificacao_prazo_Filteredtext_set = "";
         Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_set = "";
         Ddo_contratoocorrencianotificacao_prazo_Sortedstatus = "";
         Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_set = "";
         Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_set = "";
         Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus = "";
         Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_set = "";
         Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_set = "";
         Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus = "";
         Ddo_contratoocorrencianotificacao_nome_Filteredtext_set = "";
         Ddo_contratoocorrencianotificacao_nome_Selectedvalue_set = "";
         Ddo_contratoocorrencianotificacao_nome_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV52DDO_ContratoOcorrenciaNotificacao_DataAuxDate = DateTime.MinValue;
         AV53DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo = DateTime.MinValue;
         AV66DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate = DateTime.MinValue;
         AV67DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo = DateTime.MinValue;
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV34Update = "";
         AV108Update_GXI = "";
         AV35Delete = "";
         AV109Delete_GXI = "";
         A77Contrato_Numero = "";
         A298ContratoOcorrenciaNotificacao_Data = DateTime.MinValue;
         A300ContratoOcorrenciaNotificacao_Descricao = "";
         A301ContratoOcorrenciaNotificacao_Cumprido = DateTime.MinValue;
         A302ContratoOcorrenciaNotificacao_Protocolo = "";
         A304ContratoOcorrenciaNotificacao_Nome = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 = "";
         lV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 = "";
         lV94WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero = "";
         lV98WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao = "";
         lV104WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo = "";
         lV106WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome = "";
         AV83WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1 = "";
         AV85WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1 = DateTime.MinValue;
         AV86WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1 = DateTime.MinValue;
         AV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 = "";
         AV89WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2 = "";
         AV91WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2 = DateTime.MinValue;
         AV92WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2 = DateTime.MinValue;
         AV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 = "";
         AV95WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel = "";
         AV94WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero = "";
         AV96WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data = DateTime.MinValue;
         AV97WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to = DateTime.MinValue;
         AV99WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel = "";
         AV98WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao = "";
         AV102WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido = DateTime.MinValue;
         AV103WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to = DateTime.MinValue;
         AV105WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel = "";
         AV104WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo = "";
         AV107WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel = "";
         AV106WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome = "";
         H007C2_A304ContratoOcorrenciaNotificacao_Nome = new String[] {""} ;
         H007C2_n304ContratoOcorrenciaNotificacao_Nome = new bool[] {false} ;
         H007C2_A303ContratoOcorrenciaNotificacao_Responsavel = new int[1] ;
         H007C2_A302ContratoOcorrenciaNotificacao_Protocolo = new String[] {""} ;
         H007C2_n302ContratoOcorrenciaNotificacao_Protocolo = new bool[] {false} ;
         H007C2_A301ContratoOcorrenciaNotificacao_Cumprido = new DateTime[] {DateTime.MinValue} ;
         H007C2_n301ContratoOcorrenciaNotificacao_Cumprido = new bool[] {false} ;
         H007C2_A299ContratoOcorrenciaNotificacao_Prazo = new short[1] ;
         H007C2_A300ContratoOcorrenciaNotificacao_Descricao = new String[] {""} ;
         H007C2_A298ContratoOcorrenciaNotificacao_Data = new DateTime[] {DateTime.MinValue} ;
         H007C2_A77Contrato_Numero = new String[] {""} ;
         H007C2_A74Contrato_Codigo = new int[1] ;
         H007C2_A294ContratoOcorrencia_Codigo = new int[1] ;
         H007C2_A297ContratoOcorrenciaNotificacao_Codigo = new int[1] ;
         H007C3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV36Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblContratoocorrencianotificacaotitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext1_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwcontratoocorrencianotificacao__default(),
            new Object[][] {
                new Object[] {
               H007C2_A304ContratoOcorrenciaNotificacao_Nome, H007C2_n304ContratoOcorrenciaNotificacao_Nome, H007C2_A303ContratoOcorrenciaNotificacao_Responsavel, H007C2_A302ContratoOcorrenciaNotificacao_Protocolo, H007C2_n302ContratoOcorrenciaNotificacao_Protocolo, H007C2_A301ContratoOcorrenciaNotificacao_Cumprido, H007C2_n301ContratoOcorrenciaNotificacao_Cumprido, H007C2_A299ContratoOcorrenciaNotificacao_Prazo, H007C2_A300ContratoOcorrenciaNotificacao_Descricao, H007C2_A298ContratoOcorrenciaNotificacao_Data,
               H007C2_A77Contrato_Numero, H007C2_A74Contrato_Codigo, H007C2_A294ContratoOcorrencia_Codigo, H007C2_A297ContratoOcorrenciaNotificacao_Codigo
               }
               , new Object[] {
               H007C3_AGRID_nRecordCount
               }
            }
         );
         AV110Pgmname = "WWContratoOcorrenciaNotificacao";
         /* GeneXus formulas. */
         AV110Pgmname = "WWContratoOcorrenciaNotificacao";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_87 ;
      private short nGXsfl_87_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV22DynamicFiltersOperator2 ;
      private short AV60TFContratoOcorrenciaNotificacao_Prazo ;
      private short AV61TFContratoOcorrenciaNotificacao_Prazo_To ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A299ContratoOcorrenciaNotificacao_Prazo ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_87_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV84WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 ;
      private short AV90WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 ;
      private short AV100WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo ;
      private short AV101WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to ;
      private short edtContrato_Numero_Titleformat ;
      private short edtContratoOcorrenciaNotificacao_Data_Titleformat ;
      private short edtContratoOcorrenciaNotificacao_Descricao_Titleformat ;
      private short edtContratoOcorrenciaNotificacao_Prazo_Titleformat ;
      private short edtContratoOcorrenciaNotificacao_Cumprido_Titleformat ;
      private short edtContratoOcorrenciaNotificacao_Protocolo_Titleformat ;
      private short edtContratoOcorrenciaNotificacao_Nome_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A297ContratoOcorrenciaNotificacao_Codigo ;
      private int A294ContratoOcorrencia_Codigo ;
      private int A303ContratoOcorrenciaNotificacao_Responsavel ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contrato_numero_Datalistupdateminimumcharacters ;
      private int Ddo_contratoocorrencianotificacao_descricao_Datalistupdateminimumcharacters ;
      private int Ddo_contratoocorrencianotificacao_protocolo_Datalistupdateminimumcharacters ;
      private int Ddo_contratoocorrencianotificacao_nome_Datalistupdateminimumcharacters ;
      private int edtavTfcontrato_numero_Visible ;
      private int edtavTfcontrato_numero_sel_Visible ;
      private int edtavTfcontratoocorrencianotificacao_data_Visible ;
      private int edtavTfcontratoocorrencianotificacao_data_to_Visible ;
      private int edtavTfcontratoocorrencianotificacao_descricao_Visible ;
      private int edtavTfcontratoocorrencianotificacao_descricao_sel_Visible ;
      private int edtavTfcontratoocorrencianotificacao_prazo_Visible ;
      private int edtavTfcontratoocorrencianotificacao_prazo_to_Visible ;
      private int edtavTfcontratoocorrencianotificacao_cumprido_Visible ;
      private int edtavTfcontratoocorrencianotificacao_cumprido_to_Visible ;
      private int edtavTfcontratoocorrencianotificacao_protocolo_Visible ;
      private int edtavTfcontratoocorrencianotificacao_protocolo_sel_Visible ;
      private int edtavTfcontratoocorrencianotificacao_nome_Visible ;
      private int edtavTfcontratoocorrencianotificacao_nome_sel_Visible ;
      private int edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Visible ;
      private int A74Contrato_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV78PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible ;
      private int edtavContratoocorrencianotificacao_nome1_Visible ;
      private int tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible ;
      private int edtavContratoocorrencianotificacao_nome2_Visible ;
      private int AV111GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV79GridCurrentPage ;
      private long AV80GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contrato_numero_Activeeventkey ;
      private String Ddo_contrato_numero_Filteredtext_get ;
      private String Ddo_contrato_numero_Selectedvalue_get ;
      private String Ddo_contratoocorrencianotificacao_data_Activeeventkey ;
      private String Ddo_contratoocorrencianotificacao_data_Filteredtext_get ;
      private String Ddo_contratoocorrencianotificacao_data_Filteredtextto_get ;
      private String Ddo_contratoocorrencianotificacao_descricao_Activeeventkey ;
      private String Ddo_contratoocorrencianotificacao_descricao_Filteredtext_get ;
      private String Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_get ;
      private String Ddo_contratoocorrencianotificacao_prazo_Activeeventkey ;
      private String Ddo_contratoocorrencianotificacao_prazo_Filteredtext_get ;
      private String Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_get ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Activeeventkey ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_get ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_get ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Activeeventkey ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_get ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_get ;
      private String Ddo_contratoocorrencianotificacao_nome_Activeeventkey ;
      private String Ddo_contratoocorrencianotificacao_nome_Filteredtext_get ;
      private String Ddo_contratoocorrencianotificacao_nome_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_87_idx="0001" ;
      private String AV19ContratoOcorrenciaNotificacao_Nome1 ;
      private String AV25ContratoOcorrenciaNotificacao_Nome2 ;
      private String AV46TFContrato_Numero ;
      private String AV47TFContrato_Numero_Sel ;
      private String AV74TFContratoOcorrenciaNotificacao_Nome ;
      private String AV75TFContratoOcorrenciaNotificacao_Nome_Sel ;
      private String AV110Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contrato_numero_Caption ;
      private String Ddo_contrato_numero_Tooltip ;
      private String Ddo_contrato_numero_Cls ;
      private String Ddo_contrato_numero_Filteredtext_set ;
      private String Ddo_contrato_numero_Selectedvalue_set ;
      private String Ddo_contrato_numero_Dropdownoptionstype ;
      private String Ddo_contrato_numero_Titlecontrolidtoreplace ;
      private String Ddo_contrato_numero_Sortedstatus ;
      private String Ddo_contrato_numero_Filtertype ;
      private String Ddo_contrato_numero_Datalisttype ;
      private String Ddo_contrato_numero_Datalistproc ;
      private String Ddo_contrato_numero_Sortasc ;
      private String Ddo_contrato_numero_Sortdsc ;
      private String Ddo_contrato_numero_Loadingdata ;
      private String Ddo_contrato_numero_Cleanfilter ;
      private String Ddo_contrato_numero_Noresultsfound ;
      private String Ddo_contrato_numero_Searchbuttontext ;
      private String Ddo_contratoocorrencianotificacao_data_Caption ;
      private String Ddo_contratoocorrencianotificacao_data_Tooltip ;
      private String Ddo_contratoocorrencianotificacao_data_Cls ;
      private String Ddo_contratoocorrencianotificacao_data_Filteredtext_set ;
      private String Ddo_contratoocorrencianotificacao_data_Filteredtextto_set ;
      private String Ddo_contratoocorrencianotificacao_data_Dropdownoptionstype ;
      private String Ddo_contratoocorrencianotificacao_data_Titlecontrolidtoreplace ;
      private String Ddo_contratoocorrencianotificacao_data_Sortedstatus ;
      private String Ddo_contratoocorrencianotificacao_data_Filtertype ;
      private String Ddo_contratoocorrencianotificacao_data_Sortasc ;
      private String Ddo_contratoocorrencianotificacao_data_Sortdsc ;
      private String Ddo_contratoocorrencianotificacao_data_Cleanfilter ;
      private String Ddo_contratoocorrencianotificacao_data_Rangefilterfrom ;
      private String Ddo_contratoocorrencianotificacao_data_Rangefilterto ;
      private String Ddo_contratoocorrencianotificacao_data_Searchbuttontext ;
      private String Ddo_contratoocorrencianotificacao_descricao_Caption ;
      private String Ddo_contratoocorrencianotificacao_descricao_Tooltip ;
      private String Ddo_contratoocorrencianotificacao_descricao_Cls ;
      private String Ddo_contratoocorrencianotificacao_descricao_Filteredtext_set ;
      private String Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_set ;
      private String Ddo_contratoocorrencianotificacao_descricao_Dropdownoptionstype ;
      private String Ddo_contratoocorrencianotificacao_descricao_Titlecontrolidtoreplace ;
      private String Ddo_contratoocorrencianotificacao_descricao_Sortedstatus ;
      private String Ddo_contratoocorrencianotificacao_descricao_Filtertype ;
      private String Ddo_contratoocorrencianotificacao_descricao_Datalisttype ;
      private String Ddo_contratoocorrencianotificacao_descricao_Datalistproc ;
      private String Ddo_contratoocorrencianotificacao_descricao_Sortasc ;
      private String Ddo_contratoocorrencianotificacao_descricao_Sortdsc ;
      private String Ddo_contratoocorrencianotificacao_descricao_Loadingdata ;
      private String Ddo_contratoocorrencianotificacao_descricao_Cleanfilter ;
      private String Ddo_contratoocorrencianotificacao_descricao_Noresultsfound ;
      private String Ddo_contratoocorrencianotificacao_descricao_Searchbuttontext ;
      private String Ddo_contratoocorrencianotificacao_prazo_Caption ;
      private String Ddo_contratoocorrencianotificacao_prazo_Tooltip ;
      private String Ddo_contratoocorrencianotificacao_prazo_Cls ;
      private String Ddo_contratoocorrencianotificacao_prazo_Filteredtext_set ;
      private String Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_set ;
      private String Ddo_contratoocorrencianotificacao_prazo_Dropdownoptionstype ;
      private String Ddo_contratoocorrencianotificacao_prazo_Titlecontrolidtoreplace ;
      private String Ddo_contratoocorrencianotificacao_prazo_Sortedstatus ;
      private String Ddo_contratoocorrencianotificacao_prazo_Filtertype ;
      private String Ddo_contratoocorrencianotificacao_prazo_Sortasc ;
      private String Ddo_contratoocorrencianotificacao_prazo_Sortdsc ;
      private String Ddo_contratoocorrencianotificacao_prazo_Cleanfilter ;
      private String Ddo_contratoocorrencianotificacao_prazo_Rangefilterfrom ;
      private String Ddo_contratoocorrencianotificacao_prazo_Rangefilterto ;
      private String Ddo_contratoocorrencianotificacao_prazo_Searchbuttontext ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Caption ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Tooltip ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Cls ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_set ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_set ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Dropdownoptionstype ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Titlecontrolidtoreplace ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Filtertype ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Sortasc ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Sortdsc ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Cleanfilter ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Rangefilterfrom ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Rangefilterto ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Searchbuttontext ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Caption ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Tooltip ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Cls ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_set ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_set ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Dropdownoptionstype ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Titlecontrolidtoreplace ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Filtertype ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Datalisttype ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Datalistproc ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Sortasc ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Sortdsc ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Loadingdata ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Cleanfilter ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Noresultsfound ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Searchbuttontext ;
      private String Ddo_contratoocorrencianotificacao_nome_Caption ;
      private String Ddo_contratoocorrencianotificacao_nome_Tooltip ;
      private String Ddo_contratoocorrencianotificacao_nome_Cls ;
      private String Ddo_contratoocorrencianotificacao_nome_Filteredtext_set ;
      private String Ddo_contratoocorrencianotificacao_nome_Selectedvalue_set ;
      private String Ddo_contratoocorrencianotificacao_nome_Dropdownoptionstype ;
      private String Ddo_contratoocorrencianotificacao_nome_Titlecontrolidtoreplace ;
      private String Ddo_contratoocorrencianotificacao_nome_Sortedstatus ;
      private String Ddo_contratoocorrencianotificacao_nome_Filtertype ;
      private String Ddo_contratoocorrencianotificacao_nome_Datalisttype ;
      private String Ddo_contratoocorrencianotificacao_nome_Datalistproc ;
      private String Ddo_contratoocorrencianotificacao_nome_Sortasc ;
      private String Ddo_contratoocorrencianotificacao_nome_Sortdsc ;
      private String Ddo_contratoocorrencianotificacao_nome_Loadingdata ;
      private String Ddo_contratoocorrencianotificacao_nome_Cleanfilter ;
      private String Ddo_contratoocorrencianotificacao_nome_Noresultsfound ;
      private String Ddo_contratoocorrencianotificacao_nome_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String edtavTfcontrato_numero_Internalname ;
      private String edtavTfcontrato_numero_Jsonclick ;
      private String edtavTfcontrato_numero_sel_Internalname ;
      private String edtavTfcontrato_numero_sel_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_data_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_data_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_data_to_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_data_to_Jsonclick ;
      private String divDdo_contratoocorrencianotificacao_dataauxdates_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_dataauxdate_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_dataauxdate_Jsonclick ;
      private String edtavDdo_contratoocorrencianotificacao_dataauxdateto_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_dataauxdateto_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_descricao_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_descricao_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_descricao_sel_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_descricao_sel_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_prazo_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_prazo_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_prazo_to_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_prazo_to_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_cumprido_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_cumprido_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_cumprido_to_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_cumprido_to_Jsonclick ;
      private String divDdo_contratoocorrencianotificacao_cumpridoauxdates_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Jsonclick ;
      private String edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_protocolo_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_protocolo_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_protocolo_sel_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_protocolo_sel_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_nome_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_nome_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_nome_sel_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_nome_sel_Jsonclick ;
      private String edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Codigo_Internalname ;
      private String edtContratoOcorrencia_Codigo_Internalname ;
      private String edtContrato_Codigo_Internalname ;
      private String A77Contrato_Numero ;
      private String edtContrato_Numero_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Data_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Descricao_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Prazo_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Cumprido_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Protocolo_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Responsavel_Internalname ;
      private String A304ContratoOcorrenciaNotificacao_Nome ;
      private String edtContratoOcorrenciaNotificacao_Nome_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 ;
      private String lV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 ;
      private String lV94WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero ;
      private String lV106WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome ;
      private String AV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 ;
      private String AV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 ;
      private String AV95WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel ;
      private String AV94WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero ;
      private String AV107WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel ;
      private String AV106WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavContratoocorrencianotificacao_data1_Internalname ;
      private String edtavContratoocorrencianotificacao_data_to1_Internalname ;
      private String edtavContratoocorrencianotificacao_nome1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavContratoocorrencianotificacao_data2_Internalname ;
      private String edtavContratoocorrencianotificacao_data_to2_Internalname ;
      private String edtavContratoocorrencianotificacao_nome2_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contrato_numero_Internalname ;
      private String Ddo_contratoocorrencianotificacao_data_Internalname ;
      private String Ddo_contratoocorrencianotificacao_descricao_Internalname ;
      private String Ddo_contratoocorrencianotificacao_prazo_Internalname ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Internalname ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Internalname ;
      private String Ddo_contratoocorrencianotificacao_nome_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContrato_Numero_Title ;
      private String edtContratoOcorrenciaNotificacao_Data_Title ;
      private String edtContratoOcorrenciaNotificacao_Descricao_Title ;
      private String edtContratoOcorrenciaNotificacao_Prazo_Title ;
      private String edtContratoOcorrenciaNotificacao_Cumprido_Title ;
      private String edtContratoOcorrenciaNotificacao_Protocolo_Title ;
      private String edtContratoOcorrenciaNotificacao_Nome_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtContratoOcorrenciaNotificacao_Data_Link ;
      private String edtContratoOcorrenciaNotificacao_Nome_Link ;
      private String tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Internalname ;
      private String tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblContratoocorrencianotificacaotitle_Internalname ;
      private String lblContratoocorrencianotificacaotitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavContratoocorrencianotificacao_nome2_Jsonclick ;
      private String edtavContratoocorrencianotificacao_data2_Jsonclick ;
      private String lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext2_Internalname ;
      private String lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext2_Jsonclick ;
      private String edtavContratoocorrencianotificacao_data_to2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavContratoocorrencianotificacao_nome1_Jsonclick ;
      private String edtavContratoocorrencianotificacao_data1_Jsonclick ;
      private String lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext1_Internalname ;
      private String lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext1_Jsonclick ;
      private String edtavContratoocorrencianotificacao_data_to1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_87_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContratoOcorrenciaNotificacao_Codigo_Jsonclick ;
      private String edtContratoOcorrencia_Codigo_Jsonclick ;
      private String edtContrato_Codigo_Jsonclick ;
      private String edtContrato_Numero_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Data_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Descricao_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Prazo_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Cumprido_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Protocolo_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Responsavel_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Nome_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV17ContratoOcorrenciaNotificacao_Data1 ;
      private DateTime AV18ContratoOcorrenciaNotificacao_Data_To1 ;
      private DateTime AV23ContratoOcorrenciaNotificacao_Data2 ;
      private DateTime AV24ContratoOcorrenciaNotificacao_Data_To2 ;
      private DateTime AV50TFContratoOcorrenciaNotificacao_Data ;
      private DateTime AV51TFContratoOcorrenciaNotificacao_Data_To ;
      private DateTime AV64TFContratoOcorrenciaNotificacao_Cumprido ;
      private DateTime AV65TFContratoOcorrenciaNotificacao_Cumprido_To ;
      private DateTime AV52DDO_ContratoOcorrenciaNotificacao_DataAuxDate ;
      private DateTime AV53DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo ;
      private DateTime AV66DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate ;
      private DateTime AV67DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo ;
      private DateTime A298ContratoOcorrenciaNotificacao_Data ;
      private DateTime A301ContratoOcorrenciaNotificacao_Cumprido ;
      private DateTime AV85WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1 ;
      private DateTime AV86WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1 ;
      private DateTime AV91WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2 ;
      private DateTime AV92WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2 ;
      private DateTime AV96WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data ;
      private DateTime AV97WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to ;
      private DateTime AV102WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido ;
      private DateTime AV103WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV20DynamicFiltersEnabled2 ;
      private bool AV33DynamicFiltersIgnoreFirst ;
      private bool AV32DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contrato_numero_Includesortasc ;
      private bool Ddo_contrato_numero_Includesortdsc ;
      private bool Ddo_contrato_numero_Includefilter ;
      private bool Ddo_contrato_numero_Filterisrange ;
      private bool Ddo_contrato_numero_Includedatalist ;
      private bool Ddo_contratoocorrencianotificacao_data_Includesortasc ;
      private bool Ddo_contratoocorrencianotificacao_data_Includesortdsc ;
      private bool Ddo_contratoocorrencianotificacao_data_Includefilter ;
      private bool Ddo_contratoocorrencianotificacao_data_Filterisrange ;
      private bool Ddo_contratoocorrencianotificacao_data_Includedatalist ;
      private bool Ddo_contratoocorrencianotificacao_descricao_Includesortasc ;
      private bool Ddo_contratoocorrencianotificacao_descricao_Includesortdsc ;
      private bool Ddo_contratoocorrencianotificacao_descricao_Includefilter ;
      private bool Ddo_contratoocorrencianotificacao_descricao_Filterisrange ;
      private bool Ddo_contratoocorrencianotificacao_descricao_Includedatalist ;
      private bool Ddo_contratoocorrencianotificacao_prazo_Includesortasc ;
      private bool Ddo_contratoocorrencianotificacao_prazo_Includesortdsc ;
      private bool Ddo_contratoocorrencianotificacao_prazo_Includefilter ;
      private bool Ddo_contratoocorrencianotificacao_prazo_Filterisrange ;
      private bool Ddo_contratoocorrencianotificacao_prazo_Includedatalist ;
      private bool Ddo_contratoocorrencianotificacao_cumprido_Includesortasc ;
      private bool Ddo_contratoocorrencianotificacao_cumprido_Includesortdsc ;
      private bool Ddo_contratoocorrencianotificacao_cumprido_Includefilter ;
      private bool Ddo_contratoocorrencianotificacao_cumprido_Filterisrange ;
      private bool Ddo_contratoocorrencianotificacao_cumprido_Includedatalist ;
      private bool Ddo_contratoocorrencianotificacao_protocolo_Includesortasc ;
      private bool Ddo_contratoocorrencianotificacao_protocolo_Includesortdsc ;
      private bool Ddo_contratoocorrencianotificacao_protocolo_Includefilter ;
      private bool Ddo_contratoocorrencianotificacao_protocolo_Filterisrange ;
      private bool Ddo_contratoocorrencianotificacao_protocolo_Includedatalist ;
      private bool Ddo_contratoocorrencianotificacao_nome_Includesortasc ;
      private bool Ddo_contratoocorrencianotificacao_nome_Includesortdsc ;
      private bool Ddo_contratoocorrencianotificacao_nome_Includefilter ;
      private bool Ddo_contratoocorrencianotificacao_nome_Filterisrange ;
      private bool Ddo_contratoocorrencianotificacao_nome_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool n302ContratoOcorrenciaNotificacao_Protocolo ;
      private bool n304ContratoOcorrenciaNotificacao_Nome ;
      private bool AV88WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV34Update_IsBlob ;
      private bool AV35Delete_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV21DynamicFiltersSelector2 ;
      private String AV56TFContratoOcorrenciaNotificacao_Descricao ;
      private String AV57TFContratoOcorrenciaNotificacao_Descricao_Sel ;
      private String AV70TFContratoOcorrenciaNotificacao_Protocolo ;
      private String AV71TFContratoOcorrenciaNotificacao_Protocolo_Sel ;
      private String AV48ddo_Contrato_NumeroTitleControlIdToReplace ;
      private String AV54ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace ;
      private String AV58ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace ;
      private String AV62ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace ;
      private String AV68ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace ;
      private String AV72ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace ;
      private String AV76ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace ;
      private String AV108Update_GXI ;
      private String AV109Delete_GXI ;
      private String A300ContratoOcorrenciaNotificacao_Descricao ;
      private String A302ContratoOcorrenciaNotificacao_Protocolo ;
      private String lV98WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao ;
      private String lV104WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo ;
      private String AV83WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1 ;
      private String AV89WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2 ;
      private String AV99WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel ;
      private String AV98WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao ;
      private String AV105WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel ;
      private String AV104WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo ;
      private String AV34Update ;
      private String AV35Delete ;
      private IGxSession AV36Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private IDataStoreProvider pr_default ;
      private String[] H007C2_A304ContratoOcorrenciaNotificacao_Nome ;
      private bool[] H007C2_n304ContratoOcorrenciaNotificacao_Nome ;
      private int[] H007C2_A303ContratoOcorrenciaNotificacao_Responsavel ;
      private String[] H007C2_A302ContratoOcorrenciaNotificacao_Protocolo ;
      private bool[] H007C2_n302ContratoOcorrenciaNotificacao_Protocolo ;
      private DateTime[] H007C2_A301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool[] H007C2_n301ContratoOcorrenciaNotificacao_Cumprido ;
      private short[] H007C2_A299ContratoOcorrenciaNotificacao_Prazo ;
      private String[] H007C2_A300ContratoOcorrenciaNotificacao_Descricao ;
      private DateTime[] H007C2_A298ContratoOcorrenciaNotificacao_Data ;
      private String[] H007C2_A77Contrato_Numero ;
      private int[] H007C2_A74Contrato_Codigo ;
      private int[] H007C2_A294ContratoOcorrencia_Codigo ;
      private int[] H007C2_A297ContratoOcorrenciaNotificacao_Codigo ;
      private long[] H007C3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV45Contrato_NumeroTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV49ContratoOcorrenciaNotificacao_DataTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV55ContratoOcorrenciaNotificacao_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV59ContratoOcorrenciaNotificacao_PrazoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV63ContratoOcorrenciaNotificacao_CumpridoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV69ContratoOcorrenciaNotificacao_ProtocoloTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV73ContratoOcorrenciaNotificacao_NomeTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV77DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwcontratoocorrencianotificacao__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H007C2( IGxContext context ,
                                             String AV83WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV85WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1 ,
                                             DateTime AV86WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1 ,
                                             short AV84WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 ,
                                             String AV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 ,
                                             bool AV88WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 ,
                                             String AV89WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2 ,
                                             DateTime AV91WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2 ,
                                             DateTime AV92WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2 ,
                                             short AV90WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 ,
                                             String AV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 ,
                                             String AV95WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel ,
                                             String AV94WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero ,
                                             DateTime AV96WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data ,
                                             DateTime AV97WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to ,
                                             String AV99WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel ,
                                             String AV98WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao ,
                                             short AV100WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo ,
                                             short AV101WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to ,
                                             DateTime AV102WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido ,
                                             DateTime AV103WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to ,
                                             String AV105WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel ,
                                             String AV104WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo ,
                                             String AV107WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel ,
                                             String AV106WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome ,
                                             DateTime A298ContratoOcorrenciaNotificacao_Data ,
                                             String A304ContratoOcorrenciaNotificacao_Nome ,
                                             String A77Contrato_Numero ,
                                             String A300ContratoOcorrenciaNotificacao_Descricao ,
                                             short A299ContratoOcorrenciaNotificacao_Prazo ,
                                             DateTime A301ContratoOcorrenciaNotificacao_Cumprido ,
                                             String A302ContratoOcorrenciaNotificacao_Protocolo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [27] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[Pessoa_Nome] AS ContratoOcorrenciaNotificacao_Nome, T1.[ContratoOcorrenciaNotificacao_Responsavel] AS ContratoOcorrenciaNotificacao_Responsavel, T1.[ContratoOcorrenciaNotificacao_Protocolo], T1.[ContratoOcorrenciaNotificacao_Cumprido], T1.[ContratoOcorrenciaNotificacao_Prazo], T1.[ContratoOcorrenciaNotificacao_Descricao], T1.[ContratoOcorrenciaNotificacao_Data], T4.[Contrato_Numero], T3.[Contrato_Codigo], T1.[ContratoOcorrencia_Codigo], T1.[ContratoOcorrenciaNotificacao_Codigo]";
         sFromString = " FROM ((([ContratoOcorrenciaNotificacao] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[ContratoOcorrenciaNotificacao_Responsavel]) INNER JOIN [ContratoOcorrencia] T3 WITH (NOLOCK) ON T3.[ContratoOcorrencia_Codigo] = T1.[ContratoOcorrencia_Codigo]) INNER JOIN [Contrato] T4 WITH (NOLOCK) ON T4.[Contrato_Codigo] = T3.[Contrato_Codigo])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV83WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV85WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV85WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV85WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV83WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV86WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV86WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV86WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV83WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV84WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV83WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV84WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV88WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV91WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV91WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV91WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV88WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV92WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV92WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV92WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV88WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV90WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV88WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV90WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV95WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Contrato_Numero] like @lV94WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Contrato_Numero] like @lV94WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Contrato_Numero] = @AV95WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Contrato_Numero] = @AV95WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV96WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV96WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV96WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV97WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV97WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV97WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV98WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV98WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV99WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV99WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! (0==AV100WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV100WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV100WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! (0==AV101WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV101WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV101WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV102WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV102WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV102WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV103WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV103WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV103WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV105WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV104WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV104WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo)";
            }
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV105WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV105WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel)";
            }
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV107WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV106WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV106WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome)";
            }
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] = @AV107WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] = @AV107WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel)";
            }
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Data]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Data] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Contrato_Numero]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Contrato_Numero] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Descricao]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Prazo]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Prazo] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Cumprido]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Cumprido] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Protocolo]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Protocolo] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Pessoa_Nome] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H007C3( IGxContext context ,
                                             String AV83WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV85WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1 ,
                                             DateTime AV86WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1 ,
                                             short AV84WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 ,
                                             String AV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1 ,
                                             bool AV88WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 ,
                                             String AV89WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2 ,
                                             DateTime AV91WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2 ,
                                             DateTime AV92WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2 ,
                                             short AV90WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 ,
                                             String AV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2 ,
                                             String AV95WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel ,
                                             String AV94WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero ,
                                             DateTime AV96WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data ,
                                             DateTime AV97WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to ,
                                             String AV99WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel ,
                                             String AV98WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao ,
                                             short AV100WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo ,
                                             short AV101WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to ,
                                             DateTime AV102WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido ,
                                             DateTime AV103WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to ,
                                             String AV105WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel ,
                                             String AV104WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo ,
                                             String AV107WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel ,
                                             String AV106WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome ,
                                             DateTime A298ContratoOcorrenciaNotificacao_Data ,
                                             String A304ContratoOcorrenciaNotificacao_Nome ,
                                             String A77Contrato_Numero ,
                                             String A300ContratoOcorrenciaNotificacao_Descricao ,
                                             short A299ContratoOcorrenciaNotificacao_Prazo ,
                                             DateTime A301ContratoOcorrenciaNotificacao_Cumprido ,
                                             String A302ContratoOcorrenciaNotificacao_Protocolo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [22] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((([ContratoOcorrenciaNotificacao] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T1.[ContratoOcorrenciaNotificacao_Responsavel]) INNER JOIN [ContratoOcorrencia] T2 WITH (NOLOCK) ON T2.[ContratoOcorrencia_Codigo] = T1.[ContratoOcorrencia_Codigo]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV83WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV85WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV85WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV85WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV83WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV86WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV86WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV86WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV83WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV84WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV83WWContratoOcorrenciaNotificacaoDS_1_Dynamicfiltersselector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV84WWContratoOcorrenciaNotificacaoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV88WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV91WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV91WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV91WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV88WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV92WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV92WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV92WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV88WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV90WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV88WWContratoOcorrenciaNotificacaoDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV89WWContratoOcorrenciaNotificacaoDS_7_Dynamicfiltersselector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV90WWContratoOcorrenciaNotificacaoDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV95WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] like @lV94WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] like @lV94WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] = @AV95WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] = @AV95WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV96WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV96WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV96WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV97WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV97WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV97WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV99WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV98WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV98WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV99WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV99WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! (0==AV100WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV100WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV100WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! (0==AV101WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV101WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV101WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV102WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV102WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV102WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV103WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV103WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV103WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV105WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV104WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV104WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo)";
            }
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV105WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV105WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel)";
            }
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV107WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV106WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV106WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome)";
            }
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV107WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV107WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel)";
            }
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H007C2(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (short)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (short)dynConstraints[29] , (DateTime)dynConstraints[30] , (String)dynConstraints[31] , (short)dynConstraints[32] , (bool)dynConstraints[33] );
               case 1 :
                     return conditional_H007C3(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (short)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (short)dynConstraints[29] , (DateTime)dynConstraints[30] , (String)dynConstraints[31] , (short)dynConstraints[32] , (bool)dynConstraints[33] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH007C2 ;
          prmH007C2 = new Object[] {
          new Object[] {"@AV85WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV86WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV91WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV92WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV94WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV95WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV96WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV97WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV98WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV99WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV100WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV101WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV102WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV103WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV104WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV105WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel",SqlDbType.VarChar,20,0} ,
          new Object[] {"@lV106WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV107WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH007C3 ;
          prmH007C3 = new Object[] {
          new Object[] {"@AV85WWContratoOcorrenciaNotificacaoDS_3_Contratoocorrencianotificacao_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV86WWContratoOcorrenciaNotificacaoDS_4_Contratoocorrencianotificacao_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV87WWContratoOcorrenciaNotificacaoDS_5_Contratoocorrencianotificacao_nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV91WWContratoOcorrenciaNotificacaoDS_9_Contratoocorrencianotificacao_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV92WWContratoOcorrenciaNotificacaoDS_10_Contratoocorrencianotificacao_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV93WWContratoOcorrenciaNotificacaoDS_11_Contratoocorrencianotificacao_nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV94WWContratoOcorrenciaNotificacaoDS_12_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV95WWContratoOcorrenciaNotificacaoDS_13_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV96WWContratoOcorrenciaNotificacaoDS_14_Tfcontratoocorrencianotificacao_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV97WWContratoOcorrenciaNotificacaoDS_15_Tfcontratoocorrencianotificacao_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV98WWContratoOcorrenciaNotificacaoDS_16_Tfcontratoocorrencianotificacao_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV99WWContratoOcorrenciaNotificacaoDS_17_Tfcontratoocorrencianotificacao_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV100WWContratoOcorrenciaNotificacaoDS_18_Tfcontratoocorrencianotificacao_prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV101WWContratoOcorrenciaNotificacaoDS_19_Tfcontratoocorrencianotificacao_prazo_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV102WWContratoOcorrenciaNotificacaoDS_20_Tfcontratoocorrencianotificacao_cumprido",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV103WWContratoOcorrenciaNotificacaoDS_21_Tfcontratoocorrencianotificacao_cumprido_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV104WWContratoOcorrenciaNotificacaoDS_22_Tfcontratoocorrencianotificacao_protocolo",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV105WWContratoOcorrenciaNotificacaoDS_23_Tfcontratoocorrencianotificacao_protocolo_sel",SqlDbType.VarChar,20,0} ,
          new Object[] {"@lV106WWContratoOcorrenciaNotificacaoDS_24_Tfcontratoocorrencianotificacao_nome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV107WWContratoOcorrenciaNotificacaoDS_25_Tfcontratoocorrencianotificacao_nome_sel",SqlDbType.Char,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H007C2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH007C2,11,0,true,false )
             ,new CursorDef("H007C3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH007C3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((short[]) buf[7])[0] = rslt.getShort(5) ;
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(7) ;
                ((String[]) buf[10])[0] = rslt.getString(8, 20) ;
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                ((int[]) buf[12])[0] = rslt.getInt(10) ;
                ((int[]) buf[13])[0] = rslt.getInt(11) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[41]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[42]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[43]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[50]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[51]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[52]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[22]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[36]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[37]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                return;
       }
    }

 }

}
