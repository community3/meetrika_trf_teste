/*
               File: PromptFuncaoUsuario
        Description: Selecione Fun��es de Usu�rio
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:13:8.96
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptfuncaousuario : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptfuncaousuario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptfuncaousuario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutFuncaoUsuario_Codigo ,
                           ref String aP1_InOutFuncaoUsuario_Nome )
      {
         this.AV7InOutFuncaoUsuario_Codigo = aP0_InOutFuncaoUsuario_Codigo;
         this.AV8InOutFuncaoUsuario_Nome = aP1_InOutFuncaoUsuario_Nome;
         executePrivate();
         aP0_InOutFuncaoUsuario_Codigo=this.AV7InOutFuncaoUsuario_Codigo;
         aP1_InOutFuncaoUsuario_Nome=this.AV8InOutFuncaoUsuario_Nome;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_37 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_37_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_37_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17FuncaoUsuario_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoUsuario_Nome1", AV17FuncaoUsuario_Nome1);
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV52TFFuncaoUsuario_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFFuncaoUsuario_Nome", AV52TFFuncaoUsuario_Nome);
               AV53TFFuncaoUsuario_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFFuncaoUsuario_Nome_Sel", AV53TFFuncaoUsuario_Nome_Sel);
               AV56TFFuncaoUsuario_PF = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFFuncaoUsuario_PF", StringUtil.LTrim( StringUtil.Str( AV56TFFuncaoUsuario_PF, 14, 5)));
               AV57TFFuncaoUsuario_PF_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFFuncaoUsuario_PF_To", StringUtil.LTrim( StringUtil.Str( AV57TFFuncaoUsuario_PF_To, 14, 5)));
               AV54ddo_FuncaoUsuario_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_FuncaoUsuario_NomeTitleControlIdToReplace", AV54ddo_FuncaoUsuario_NomeTitleControlIdToReplace);
               AV58ddo_FuncaoUsuario_PFTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ddo_FuncaoUsuario_PFTitleControlIdToReplace", AV58ddo_FuncaoUsuario_PFTitleControlIdToReplace);
               AV67Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoUsuario_Nome1, AV14OrderedDsc, AV52TFFuncaoUsuario_Nome, AV53TFFuncaoUsuario_Nome_Sel, AV56TFFuncaoUsuario_PF, AV57TFFuncaoUsuario_PF_To, AV54ddo_FuncaoUsuario_NomeTitleControlIdToReplace, AV58ddo_FuncaoUsuario_PFTitleControlIdToReplace, AV67Pgmname, AV10GridState) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutFuncaoUsuario_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutFuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutFuncaoUsuario_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutFuncaoUsuario_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutFuncaoUsuario_Nome", AV8InOutFuncaoUsuario_Nome);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA3U2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV67Pgmname = "PromptFuncaoUsuario";
               context.Gx_err = 0;
               WS3U2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE3U2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020428231394");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptfuncaousuario.aspx") + "?" + UrlEncode("" +AV7InOutFuncaoUsuario_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV8InOutFuncaoUsuario_Nome))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOUSUARIO_NOME1", AV17FuncaoUsuario_Nome1);
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOUSUARIO_NOME", AV52TFFuncaoUsuario_Nome);
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOUSUARIO_NOME_SEL", AV53TFFuncaoUsuario_Nome_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOUSUARIO_PF", StringUtil.LTrim( StringUtil.NToC( AV56TFFuncaoUsuario_PF, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOUSUARIO_PF_TO", StringUtil.LTrim( StringUtil.NToC( AV57TFFuncaoUsuario_PF_To, 14, 5, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_37", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_37), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV61GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV62GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV59DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV59DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAOUSUARIO_NOMETITLEFILTERDATA", AV51FuncaoUsuario_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAOUSUARIO_NOMETITLEFILTERDATA", AV51FuncaoUsuario_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAOUSUARIO_PFTITLEFILTERDATA", AV55FuncaoUsuario_PFTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAOUSUARIO_PFTITLEFILTERDATA", AV55FuncaoUsuario_PFTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV67Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_hidden_field( context, "vINOUTFUNCAOUSUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutFuncaoUsuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTFUNCAOUSUARIO_NOME", AV8InOutFuncaoUsuario_Nome);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_NOME_Caption", StringUtil.RTrim( Ddo_funcaousuario_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_NOME_Tooltip", StringUtil.RTrim( Ddo_funcaousuario_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_NOME_Cls", StringUtil.RTrim( Ddo_funcaousuario_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_funcaousuario_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaousuario_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaousuario_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaousuario_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_funcaousuario_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaousuario_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_funcaousuario_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_funcaousuario_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_NOME_Filtertype", StringUtil.RTrim( Ddo_funcaousuario_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_funcaousuario_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_funcaousuario_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_NOME_Datalisttype", StringUtil.RTrim( Ddo_funcaousuario_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_NOME_Datalistfixedvalues", StringUtil.RTrim( Ddo_funcaousuario_nome_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_NOME_Datalistproc", StringUtil.RTrim( Ddo_funcaousuario_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaousuario_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_NOME_Sortasc", StringUtil.RTrim( Ddo_funcaousuario_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_NOME_Sortdsc", StringUtil.RTrim( Ddo_funcaousuario_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_NOME_Loadingdata", StringUtil.RTrim( Ddo_funcaousuario_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_funcaousuario_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_NOME_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaousuario_nome_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_NOME_Rangefilterto", StringUtil.RTrim( Ddo_funcaousuario_nome_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_funcaousuario_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_funcaousuario_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_PF_Caption", StringUtil.RTrim( Ddo_funcaousuario_pf_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_PF_Tooltip", StringUtil.RTrim( Ddo_funcaousuario_pf_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_PF_Cls", StringUtil.RTrim( Ddo_funcaousuario_pf_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_PF_Filteredtext_set", StringUtil.RTrim( Ddo_funcaousuario_pf_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_PF_Filteredtextto_set", StringUtil.RTrim( Ddo_funcaousuario_pf_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_PF_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaousuario_pf_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_PF_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaousuario_pf_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_PF_Includesortasc", StringUtil.BoolToStr( Ddo_funcaousuario_pf_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_PF_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaousuario_pf_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_PF_Includefilter", StringUtil.BoolToStr( Ddo_funcaousuario_pf_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_PF_Filtertype", StringUtil.RTrim( Ddo_funcaousuario_pf_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_PF_Filterisrange", StringUtil.BoolToStr( Ddo_funcaousuario_pf_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_PF_Includedatalist", StringUtil.BoolToStr( Ddo_funcaousuario_pf_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_PF_Datalistfixedvalues", StringUtil.RTrim( Ddo_funcaousuario_pf_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_PF_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaousuario_pf_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_PF_Sortasc", StringUtil.RTrim( Ddo_funcaousuario_pf_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_PF_Sortdsc", StringUtil.RTrim( Ddo_funcaousuario_pf_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_PF_Loadingdata", StringUtil.RTrim( Ddo_funcaousuario_pf_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_PF_Cleanfilter", StringUtil.RTrim( Ddo_funcaousuario_pf_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_PF_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaousuario_pf_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_PF_Rangefilterto", StringUtil.RTrim( Ddo_funcaousuario_pf_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_PF_Noresultsfound", StringUtil.RTrim( Ddo_funcaousuario_pf_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_PF_Searchbuttontext", StringUtil.RTrim( Ddo_funcaousuario_pf_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_funcaousuario_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_funcaousuario_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaousuario_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_PF_Activeeventkey", StringUtil.RTrim( Ddo_funcaousuario_pf_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_PF_Filteredtext_get", StringUtil.RTrim( Ddo_funcaousuario_pf_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOUSUARIO_PF_Filteredtextto_get", StringUtil.RTrim( Ddo_funcaousuario_pf_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm3U2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptFuncaoUsuario" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Fun��es de Usu�rio" ;
      }

      protected void WB3U0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_3U2( true) ;
         }
         else
         {
            wb_table1_2_3U2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_3U2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_37_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptFuncaoUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'" + sGXsfl_37_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaousuario_nome_Internalname, AV52TFFuncaoUsuario_Nome, StringUtil.RTrim( context.localUtil.Format( AV52TFFuncaoUsuario_Nome, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaousuario_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaousuario_nome_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 200, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptFuncaoUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_37_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaousuario_nome_sel_Internalname, AV53TFFuncaoUsuario_Nome_Sel, StringUtil.RTrim( context.localUtil.Format( AV53TFFuncaoUsuario_Nome_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaousuario_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaousuario_nome_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 200, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptFuncaoUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_37_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaousuario_pf_Internalname, StringUtil.LTrim( StringUtil.NToC( AV56TFFuncaoUsuario_PF, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV56TFFuncaoUsuario_PF, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaousuario_pf_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaousuario_pf_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFuncaoUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_37_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaousuario_pf_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV57TFFuncaoUsuario_PF_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV57TFFuncaoUsuario_PF_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaousuario_pf_to_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaousuario_pf_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFuncaoUsuario.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAOUSUARIO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_37_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaousuario_nometitlecontrolidtoreplace_Internalname, AV54ddo_FuncaoUsuario_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", 0, edtavDdo_funcaousuario_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFuncaoUsuario.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAOUSUARIO_PFContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_37_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaousuario_pftitlecontrolidtoreplace_Internalname, AV58ddo_FuncaoUsuario_PFTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", 0, edtavDdo_funcaousuario_pftitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFuncaoUsuario.htm");
         }
         wbLoad = true;
      }

      protected void START3U2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Fun��es de Usu�rio", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP3U0( ) ;
      }

      protected void WS3U2( )
      {
         START3U2( ) ;
         EVT3U2( ) ;
      }

      protected void EVT3U2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E113U2 */
                           E113U2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOUSUARIO_NOME.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E123U2 */
                           E123U2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOUSUARIO_PF.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E133U2 */
                           E133U2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E143U2 */
                           E143U2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E153U2 */
                           E153U2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_37_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_37_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_37_idx), 4, 0)), 4, "0");
                           SubsflControlProps_372( ) ;
                           AV28Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV66Select_GXI : context.convertURL( context.PathToRelativeUrl( AV28Select))));
                           A161FuncaoUsuario_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoUsuario_Codigo_Internalname), ",", "."));
                           A162FuncaoUsuario_Nome = cgiGet( edtFuncaoUsuario_Nome_Internalname);
                           A396FuncaoUsuario_PF = context.localUtil.CToN( cgiGet( edtFuncaoUsuario_PF_Internalname), ",", ".");
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E163U2 */
                                 E163U2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E173U2 */
                                 E173U2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E183U2 */
                                 E183U2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Funcaousuario_nome1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOUSUARIO_NOME1"), AV17FuncaoUsuario_Nome1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaousuario_nome Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOUSUARIO_NOME"), AV52TFFuncaoUsuario_Nome) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaousuario_nome_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOUSUARIO_NOME_SEL"), AV53TFFuncaoUsuario_Nome_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaousuario_pf Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOUSUARIO_PF"), ",", ".") != AV56TFFuncaoUsuario_PF )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaousuario_pf_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOUSUARIO_PF_TO"), ",", ".") != AV57TFFuncaoUsuario_PF_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E193U2 */
                                       E193U2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE3U2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm3U2( ) ;
            }
         }
      }

      protected void PA3U2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("FUNCAOUSUARIO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavDynamicfiltersselector1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_372( ) ;
         while ( nGXsfl_37_idx <= nRC_GXsfl_37 )
         {
            sendrow_372( ) ;
            nGXsfl_37_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_37_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_37_idx+1));
            sGXsfl_37_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_37_idx), 4, 0)), 4, "0");
            SubsflControlProps_372( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17FuncaoUsuario_Nome1 ,
                                       bool AV14OrderedDsc ,
                                       String AV52TFFuncaoUsuario_Nome ,
                                       String AV53TFFuncaoUsuario_Nome_Sel ,
                                       decimal AV56TFFuncaoUsuario_PF ,
                                       decimal AV57TFFuncaoUsuario_PF_To ,
                                       String AV54ddo_FuncaoUsuario_NomeTitleControlIdToReplace ,
                                       String AV58ddo_FuncaoUsuario_PFTitleControlIdToReplace ,
                                       String AV67Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF3U2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOUSUARIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A161FuncaoUsuario_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "FUNCAOUSUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A161FuncaoUsuario_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOUSUARIO_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A162FuncaoUsuario_Nome, ""))));
         GxWebStd.gx_hidden_field( context, "FUNCAOUSUARIO_NOME", A162FuncaoUsuario_Nome);
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOUSUARIO_PF", GetSecureSignedToken( "", context.localUtil.Format( A396FuncaoUsuario_PF, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "FUNCAOUSUARIO_PF", StringUtil.LTrim( StringUtil.NToC( A396FuncaoUsuario_PF, 14, 5, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF3U2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV67Pgmname = "PromptFuncaoUsuario";
         context.Gx_err = 0;
      }

      protected void RF3U2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 37;
         /* Execute user event: E173U2 */
         E173U2 ();
         nGXsfl_37_idx = 1;
         sGXsfl_37_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_37_idx), 4, 0)), 4, "0");
         SubsflControlProps_372( ) ;
         nGXsfl_37_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_372( ) ;
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16DynamicFiltersOperator1 ,
                                                 AV17FuncaoUsuario_Nome1 ,
                                                 AV53TFFuncaoUsuario_Nome_Sel ,
                                                 AV52TFFuncaoUsuario_Nome ,
                                                 A162FuncaoUsuario_Nome ,
                                                 AV14OrderedDsc ,
                                                 AV56TFFuncaoUsuario_PF ,
                                                 A396FuncaoUsuario_PF ,
                                                 AV57TFFuncaoUsuario_PF_To },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL
                                                 }
            });
            lV17FuncaoUsuario_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV17FuncaoUsuario_Nome1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoUsuario_Nome1", AV17FuncaoUsuario_Nome1);
            lV17FuncaoUsuario_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV17FuncaoUsuario_Nome1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoUsuario_Nome1", AV17FuncaoUsuario_Nome1);
            lV52TFFuncaoUsuario_Nome = StringUtil.Concat( StringUtil.RTrim( AV52TFFuncaoUsuario_Nome), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFFuncaoUsuario_Nome", AV52TFFuncaoUsuario_Nome);
            /* Using cursor H003U2 */
            pr_default.execute(0, new Object[] {lV17FuncaoUsuario_Nome1, lV17FuncaoUsuario_Nome1, lV52TFFuncaoUsuario_Nome, AV53TFFuncaoUsuario_Nome_Sel});
            nGXsfl_37_idx = 1;
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) ) )
            {
               A162FuncaoUsuario_Nome = H003U2_A162FuncaoUsuario_Nome[0];
               A161FuncaoUsuario_Codigo = H003U2_A161FuncaoUsuario_Codigo[0];
               GXt_int1 = (short)(A396FuncaoUsuario_PF);
               new prc_fupf(context ).execute( ref  A161FuncaoUsuario_Codigo, ref  GXt_int1) ;
               A396FuncaoUsuario_PF = (decimal)(GXt_int1);
               if ( (Convert.ToDecimal(0)==AV56TFFuncaoUsuario_PF) || ( ( A396FuncaoUsuario_PF >= AV56TFFuncaoUsuario_PF ) ) )
               {
                  if ( (Convert.ToDecimal(0)==AV57TFFuncaoUsuario_PF_To) || ( ( A396FuncaoUsuario_PF <= AV57TFFuncaoUsuario_PF_To ) ) )
                  {
                     /* Execute user event: E183U2 */
                     E183U2 ();
                  }
               }
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 37;
            WB3U0( ) ;
         }
         nGXsfl_37_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoUsuario_Nome1, AV14OrderedDsc, AV52TFFuncaoUsuario_Nome, AV53TFFuncaoUsuario_Nome_Sel, AV56TFFuncaoUsuario_PF, AV57TFFuncaoUsuario_PF_To, AV54ddo_FuncaoUsuario_NomeTitleControlIdToReplace, AV58ddo_FuncaoUsuario_PFTitleControlIdToReplace, AV67Pgmname, AV10GridState) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         if ( GRID_nEOF == 0 )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoUsuario_Nome1, AV14OrderedDsc, AV52TFFuncaoUsuario_Nome, AV53TFFuncaoUsuario_Nome_Sel, AV56TFFuncaoUsuario_PF, AV57TFFuncaoUsuario_PF_To, AV54ddo_FuncaoUsuario_NomeTitleControlIdToReplace, AV58ddo_FuncaoUsuario_PFTitleControlIdToReplace, AV67Pgmname, AV10GridState) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoUsuario_Nome1, AV14OrderedDsc, AV52TFFuncaoUsuario_Nome, AV53TFFuncaoUsuario_Nome_Sel, AV56TFFuncaoUsuario_PF, AV57TFFuncaoUsuario_PF_To, AV54ddo_FuncaoUsuario_NomeTitleControlIdToReplace, AV58ddo_FuncaoUsuario_PFTitleControlIdToReplace, AV67Pgmname, AV10GridState) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         subGrid_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoUsuario_Nome1, AV14OrderedDsc, AV52TFFuncaoUsuario_Nome, AV53TFFuncaoUsuario_Nome_Sel, AV56TFFuncaoUsuario_PF, AV57TFFuncaoUsuario_PF_To, AV54ddo_FuncaoUsuario_NomeTitleControlIdToReplace, AV58ddo_FuncaoUsuario_PFTitleControlIdToReplace, AV67Pgmname, AV10GridState) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoUsuario_Nome1, AV14OrderedDsc, AV52TFFuncaoUsuario_Nome, AV53TFFuncaoUsuario_Nome_Sel, AV56TFFuncaoUsuario_PF, AV57TFFuncaoUsuario_PF_To, AV54ddo_FuncaoUsuario_NomeTitleControlIdToReplace, AV58ddo_FuncaoUsuario_PFTitleControlIdToReplace, AV67Pgmname, AV10GridState) ;
         }
         return (int)(0) ;
      }

      protected void STRUP3U0( )
      {
         /* Before Start, stand alone formulas. */
         AV67Pgmname = "PromptFuncaoUsuario";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E163U2 */
         E163U2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV59DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAOUSUARIO_NOMETITLEFILTERDATA"), AV51FuncaoUsuario_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAOUSUARIO_PFTITLEFILTERDATA"), AV55FuncaoUsuario_PFTitleFilterData);
            /* Read variables values. */
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17FuncaoUsuario_Nome1 = cgiGet( edtavFuncaousuario_nome1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoUsuario_Nome1", AV17FuncaoUsuario_Nome1);
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            AV52TFFuncaoUsuario_Nome = cgiGet( edtavTffuncaousuario_nome_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFFuncaoUsuario_Nome", AV52TFFuncaoUsuario_Nome);
            AV53TFFuncaoUsuario_Nome_Sel = cgiGet( edtavTffuncaousuario_nome_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFFuncaoUsuario_Nome_Sel", AV53TFFuncaoUsuario_Nome_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaousuario_pf_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaousuario_pf_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAOUSUARIO_PF");
               GX_FocusControl = edtavTffuncaousuario_pf_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV56TFFuncaoUsuario_PF = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFFuncaoUsuario_PF", StringUtil.LTrim( StringUtil.Str( AV56TFFuncaoUsuario_PF, 14, 5)));
            }
            else
            {
               AV56TFFuncaoUsuario_PF = context.localUtil.CToN( cgiGet( edtavTffuncaousuario_pf_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFFuncaoUsuario_PF", StringUtil.LTrim( StringUtil.Str( AV56TFFuncaoUsuario_PF, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaousuario_pf_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaousuario_pf_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAOUSUARIO_PF_TO");
               GX_FocusControl = edtavTffuncaousuario_pf_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV57TFFuncaoUsuario_PF_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFFuncaoUsuario_PF_To", StringUtil.LTrim( StringUtil.Str( AV57TFFuncaoUsuario_PF_To, 14, 5)));
            }
            else
            {
               AV57TFFuncaoUsuario_PF_To = context.localUtil.CToN( cgiGet( edtavTffuncaousuario_pf_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFFuncaoUsuario_PF_To", StringUtil.LTrim( StringUtil.Str( AV57TFFuncaoUsuario_PF_To, 14, 5)));
            }
            AV54ddo_FuncaoUsuario_NomeTitleControlIdToReplace = cgiGet( edtavDdo_funcaousuario_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_FuncaoUsuario_NomeTitleControlIdToReplace", AV54ddo_FuncaoUsuario_NomeTitleControlIdToReplace);
            AV58ddo_FuncaoUsuario_PFTitleControlIdToReplace = cgiGet( edtavDdo_funcaousuario_pftitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ddo_FuncaoUsuario_PFTitleControlIdToReplace", AV58ddo_FuncaoUsuario_PFTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_37 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_37"), ",", "."));
            AV61GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV62GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_funcaousuario_nome_Caption = cgiGet( "DDO_FUNCAOUSUARIO_NOME_Caption");
            Ddo_funcaousuario_nome_Tooltip = cgiGet( "DDO_FUNCAOUSUARIO_NOME_Tooltip");
            Ddo_funcaousuario_nome_Cls = cgiGet( "DDO_FUNCAOUSUARIO_NOME_Cls");
            Ddo_funcaousuario_nome_Filteredtext_set = cgiGet( "DDO_FUNCAOUSUARIO_NOME_Filteredtext_set");
            Ddo_funcaousuario_nome_Selectedvalue_set = cgiGet( "DDO_FUNCAOUSUARIO_NOME_Selectedvalue_set");
            Ddo_funcaousuario_nome_Dropdownoptionstype = cgiGet( "DDO_FUNCAOUSUARIO_NOME_Dropdownoptionstype");
            Ddo_funcaousuario_nome_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAOUSUARIO_NOME_Titlecontrolidtoreplace");
            Ddo_funcaousuario_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOUSUARIO_NOME_Includesortasc"));
            Ddo_funcaousuario_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOUSUARIO_NOME_Includesortdsc"));
            Ddo_funcaousuario_nome_Sortedstatus = cgiGet( "DDO_FUNCAOUSUARIO_NOME_Sortedstatus");
            Ddo_funcaousuario_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOUSUARIO_NOME_Includefilter"));
            Ddo_funcaousuario_nome_Filtertype = cgiGet( "DDO_FUNCAOUSUARIO_NOME_Filtertype");
            Ddo_funcaousuario_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOUSUARIO_NOME_Filterisrange"));
            Ddo_funcaousuario_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOUSUARIO_NOME_Includedatalist"));
            Ddo_funcaousuario_nome_Datalisttype = cgiGet( "DDO_FUNCAOUSUARIO_NOME_Datalisttype");
            Ddo_funcaousuario_nome_Datalistfixedvalues = cgiGet( "DDO_FUNCAOUSUARIO_NOME_Datalistfixedvalues");
            Ddo_funcaousuario_nome_Datalistproc = cgiGet( "DDO_FUNCAOUSUARIO_NOME_Datalistproc");
            Ddo_funcaousuario_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_FUNCAOUSUARIO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaousuario_nome_Sortasc = cgiGet( "DDO_FUNCAOUSUARIO_NOME_Sortasc");
            Ddo_funcaousuario_nome_Sortdsc = cgiGet( "DDO_FUNCAOUSUARIO_NOME_Sortdsc");
            Ddo_funcaousuario_nome_Loadingdata = cgiGet( "DDO_FUNCAOUSUARIO_NOME_Loadingdata");
            Ddo_funcaousuario_nome_Cleanfilter = cgiGet( "DDO_FUNCAOUSUARIO_NOME_Cleanfilter");
            Ddo_funcaousuario_nome_Rangefilterfrom = cgiGet( "DDO_FUNCAOUSUARIO_NOME_Rangefilterfrom");
            Ddo_funcaousuario_nome_Rangefilterto = cgiGet( "DDO_FUNCAOUSUARIO_NOME_Rangefilterto");
            Ddo_funcaousuario_nome_Noresultsfound = cgiGet( "DDO_FUNCAOUSUARIO_NOME_Noresultsfound");
            Ddo_funcaousuario_nome_Searchbuttontext = cgiGet( "DDO_FUNCAOUSUARIO_NOME_Searchbuttontext");
            Ddo_funcaousuario_pf_Caption = cgiGet( "DDO_FUNCAOUSUARIO_PF_Caption");
            Ddo_funcaousuario_pf_Tooltip = cgiGet( "DDO_FUNCAOUSUARIO_PF_Tooltip");
            Ddo_funcaousuario_pf_Cls = cgiGet( "DDO_FUNCAOUSUARIO_PF_Cls");
            Ddo_funcaousuario_pf_Filteredtext_set = cgiGet( "DDO_FUNCAOUSUARIO_PF_Filteredtext_set");
            Ddo_funcaousuario_pf_Filteredtextto_set = cgiGet( "DDO_FUNCAOUSUARIO_PF_Filteredtextto_set");
            Ddo_funcaousuario_pf_Dropdownoptionstype = cgiGet( "DDO_FUNCAOUSUARIO_PF_Dropdownoptionstype");
            Ddo_funcaousuario_pf_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAOUSUARIO_PF_Titlecontrolidtoreplace");
            Ddo_funcaousuario_pf_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOUSUARIO_PF_Includesortasc"));
            Ddo_funcaousuario_pf_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOUSUARIO_PF_Includesortdsc"));
            Ddo_funcaousuario_pf_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOUSUARIO_PF_Includefilter"));
            Ddo_funcaousuario_pf_Filtertype = cgiGet( "DDO_FUNCAOUSUARIO_PF_Filtertype");
            Ddo_funcaousuario_pf_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOUSUARIO_PF_Filterisrange"));
            Ddo_funcaousuario_pf_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOUSUARIO_PF_Includedatalist"));
            Ddo_funcaousuario_pf_Datalistfixedvalues = cgiGet( "DDO_FUNCAOUSUARIO_PF_Datalistfixedvalues");
            Ddo_funcaousuario_pf_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_FUNCAOUSUARIO_PF_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaousuario_pf_Sortasc = cgiGet( "DDO_FUNCAOUSUARIO_PF_Sortasc");
            Ddo_funcaousuario_pf_Sortdsc = cgiGet( "DDO_FUNCAOUSUARIO_PF_Sortdsc");
            Ddo_funcaousuario_pf_Loadingdata = cgiGet( "DDO_FUNCAOUSUARIO_PF_Loadingdata");
            Ddo_funcaousuario_pf_Cleanfilter = cgiGet( "DDO_FUNCAOUSUARIO_PF_Cleanfilter");
            Ddo_funcaousuario_pf_Rangefilterfrom = cgiGet( "DDO_FUNCAOUSUARIO_PF_Rangefilterfrom");
            Ddo_funcaousuario_pf_Rangefilterto = cgiGet( "DDO_FUNCAOUSUARIO_PF_Rangefilterto");
            Ddo_funcaousuario_pf_Noresultsfound = cgiGet( "DDO_FUNCAOUSUARIO_PF_Noresultsfound");
            Ddo_funcaousuario_pf_Searchbuttontext = cgiGet( "DDO_FUNCAOUSUARIO_PF_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_funcaousuario_nome_Activeeventkey = cgiGet( "DDO_FUNCAOUSUARIO_NOME_Activeeventkey");
            Ddo_funcaousuario_nome_Filteredtext_get = cgiGet( "DDO_FUNCAOUSUARIO_NOME_Filteredtext_get");
            Ddo_funcaousuario_nome_Selectedvalue_get = cgiGet( "DDO_FUNCAOUSUARIO_NOME_Selectedvalue_get");
            Ddo_funcaousuario_pf_Activeeventkey = cgiGet( "DDO_FUNCAOUSUARIO_PF_Activeeventkey");
            Ddo_funcaousuario_pf_Filteredtext_get = cgiGet( "DDO_FUNCAOUSUARIO_PF_Filteredtext_get");
            Ddo_funcaousuario_pf_Filteredtextto_get = cgiGet( "DDO_FUNCAOUSUARIO_PF_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOUSUARIO_NOME1"), AV17FuncaoUsuario_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOUSUARIO_NOME"), AV52TFFuncaoUsuario_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOUSUARIO_NOME_SEL"), AV53TFFuncaoUsuario_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOUSUARIO_PF"), ",", ".") != AV56TFFuncaoUsuario_PF )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOUSUARIO_PF_TO"), ",", ".") != AV57TFFuncaoUsuario_PF_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E163U2 */
         E163U2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E163U2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         AV15DynamicFiltersSelector1 = "FUNCAOUSUARIO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavTffuncaousuario_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaousuario_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaousuario_nome_Visible), 5, 0)));
         edtavTffuncaousuario_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaousuario_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaousuario_nome_sel_Visible), 5, 0)));
         edtavTffuncaousuario_pf_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaousuario_pf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaousuario_pf_Visible), 5, 0)));
         edtavTffuncaousuario_pf_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaousuario_pf_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaousuario_pf_to_Visible), 5, 0)));
         Ddo_funcaousuario_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoUsuario_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaousuario_nome_Internalname, "TitleControlIdToReplace", Ddo_funcaousuario_nome_Titlecontrolidtoreplace);
         AV54ddo_FuncaoUsuario_NomeTitleControlIdToReplace = Ddo_funcaousuario_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_FuncaoUsuario_NomeTitleControlIdToReplace", AV54ddo_FuncaoUsuario_NomeTitleControlIdToReplace);
         edtavDdo_funcaousuario_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaousuario_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaousuario_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaousuario_pf_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoUsuario_PF";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaousuario_pf_Internalname, "TitleControlIdToReplace", Ddo_funcaousuario_pf_Titlecontrolidtoreplace);
         AV58ddo_FuncaoUsuario_PFTitleControlIdToReplace = Ddo_funcaousuario_pf_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ddo_FuncaoUsuario_PFTitleControlIdToReplace", AV58ddo_FuncaoUsuario_PFTitleControlIdToReplace);
         edtavDdo_funcaousuario_pftitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaousuario_pftitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaousuario_pftitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Select Fun��es de Usu�rio";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2 = AV59DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2) ;
         AV59DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2;
      }

      protected void E173U2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV51FuncaoUsuario_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV55FuncaoUsuario_PFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtFuncaoUsuario_Nome_Titleformat = 2;
         edtFuncaoUsuario_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV54ddo_FuncaoUsuario_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoUsuario_Nome_Internalname, "Title", edtFuncaoUsuario_Nome_Title);
         edtFuncaoUsuario_PF_Titleformat = 2;
         edtFuncaoUsuario_PF_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "PF", AV58ddo_FuncaoUsuario_PFTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoUsuario_PF_Internalname, "Title", edtFuncaoUsuario_PF_Title);
         AV61GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV61GridCurrentPage), 10, 0)));
         AV62GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV51FuncaoUsuario_NomeTitleFilterData", AV51FuncaoUsuario_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV55FuncaoUsuario_PFTitleFilterData", AV55FuncaoUsuario_PFTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E113U2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV60PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV60PageToGo) ;
         }
      }

      protected void E123U2( )
      {
         /* Ddo_funcaousuario_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaousuario_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaousuario_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaousuario_nome_Internalname, "SortedStatus", Ddo_funcaousuario_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaousuario_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaousuario_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaousuario_nome_Internalname, "SortedStatus", Ddo_funcaousuario_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaousuario_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV52TFFuncaoUsuario_Nome = Ddo_funcaousuario_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFFuncaoUsuario_Nome", AV52TFFuncaoUsuario_Nome);
            AV53TFFuncaoUsuario_Nome_Sel = Ddo_funcaousuario_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFFuncaoUsuario_Nome_Sel", AV53TFFuncaoUsuario_Nome_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E133U2( )
      {
         /* Ddo_funcaousuario_pf_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaousuario_pf_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV56TFFuncaoUsuario_PF = NumberUtil.Val( Ddo_funcaousuario_pf_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFFuncaoUsuario_PF", StringUtil.LTrim( StringUtil.Str( AV56TFFuncaoUsuario_PF, 14, 5)));
            AV57TFFuncaoUsuario_PF_To = NumberUtil.Val( Ddo_funcaousuario_pf_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFFuncaoUsuario_PF_To", StringUtil.LTrim( StringUtil.Str( AV57TFFuncaoUsuario_PF_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
      }

      private void E183U2( )
      {
         /* Grid_Load Routine */
         AV28Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV28Select);
         AV66Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 37;
         }
         if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
         {
            sendrow_372( ) ;
            GRID_nEOF = 1;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
            {
               GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
            }
         }
         if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
         {
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
         }
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_37_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(37, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E193U2 */
         E193U2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E193U2( )
      {
         /* Enter Routine */
         AV7InOutFuncaoUsuario_Codigo = A161FuncaoUsuario_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutFuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutFuncaoUsuario_Codigo), 6, 0)));
         AV8InOutFuncaoUsuario_Nome = A162FuncaoUsuario_Nome;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutFuncaoUsuario_Nome", AV8InOutFuncaoUsuario_Nome);
         context.setWebReturnParms(new Object[] {(int)AV7InOutFuncaoUsuario_Codigo,(String)AV8InOutFuncaoUsuario_Nome});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E153U2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E143U2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void S142( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         Ddo_funcaousuario_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaousuario_nome_Internalname, "SortedStatus", Ddo_funcaousuario_nome_Sortedstatus);
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavFuncaousuario_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaousuario_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaousuario_nome1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOUSUARIO_NOME") == 0 )
         {
            edtavFuncaousuario_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaousuario_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaousuario_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'CLEANFILTERS' Routine */
         AV52TFFuncaoUsuario_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFFuncaoUsuario_Nome", AV52TFFuncaoUsuario_Nome);
         Ddo_funcaousuario_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaousuario_nome_Internalname, "FilteredText_set", Ddo_funcaousuario_nome_Filteredtext_set);
         AV53TFFuncaoUsuario_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFFuncaoUsuario_Nome_Sel", AV53TFFuncaoUsuario_Nome_Sel);
         Ddo_funcaousuario_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaousuario_nome_Internalname, "SelectedValue_set", Ddo_funcaousuario_nome_Selectedvalue_set);
         AV56TFFuncaoUsuario_PF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFFuncaoUsuario_PF", StringUtil.LTrim( StringUtil.Str( AV56TFFuncaoUsuario_PF, 14, 5)));
         Ddo_funcaousuario_pf_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaousuario_pf_Internalname, "FilteredText_set", Ddo_funcaousuario_pf_Filteredtext_set);
         AV57TFFuncaoUsuario_PF_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFFuncaoUsuario_PF_To", StringUtil.LTrim( StringUtil.Str( AV57TFFuncaoUsuario_PF_To, 14, 5)));
         Ddo_funcaousuario_pf_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaousuario_pf_Internalname, "FilteredTextTo_set", Ddo_funcaousuario_pf_Filteredtextto_set);
         AV15DynamicFiltersSelector1 = "FUNCAOUSUARIO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17FuncaoUsuario_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoUsuario_Nome1", AV17FuncaoUsuario_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S162( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOUSUARIO_NOME") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17FuncaoUsuario_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoUsuario_Nome1", AV17FuncaoUsuario_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void S122( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFFuncaoUsuario_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOUSUARIO_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV52TFFuncaoUsuario_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TFFuncaoUsuario_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOUSUARIO_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV53TFFuncaoUsuario_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV56TFFuncaoUsuario_PF) && (Convert.ToDecimal(0)==AV57TFFuncaoUsuario_PF_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOUSUARIO_PF";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV56TFFuncaoUsuario_PF, 14, 5);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV57TFFuncaoUsuario_PF_To, 14, 5);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV67Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S172( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOUSUARIO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17FuncaoUsuario_Nome1)) )
         {
            AV12GridStateDynamicFilter.gxTpr_Value = AV17FuncaoUsuario_Nome1;
            AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
         {
            AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
         }
      }

      protected void S152( )
      {
         /* 'RESETDYNFILTERS' Routine */
      }

      protected void wb_table1_2_3U2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_3U2( true) ;
         }
         else
         {
            wb_table2_5_3U2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_3U2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_31_3U2( true) ;
         }
         else
         {
            wb_table3_31_3U2( false) ;
         }
         return  ;
      }

      protected void wb_table3_31_3U2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3U2e( true) ;
         }
         else
         {
            wb_table1_2_3U2e( false) ;
         }
      }

      protected void wb_table3_31_3U2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_3U2( true) ;
         }
         else
         {
            wb_table4_34_3U2( false) ;
         }
         return  ;
      }

      protected void wb_table4_34_3U2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_31_3U2e( true) ;
         }
         else
         {
            wb_table3_31_3U2e( false) ;
         }
      }

      protected void wb_table4_34_3U2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"37\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "de Usu�rio") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoUsuario_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoUsuario_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoUsuario_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoUsuario_PF_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoUsuario_PF_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoUsuario_PF_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A161FuncaoUsuario_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A162FuncaoUsuario_Nome);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoUsuario_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoUsuario_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A396FuncaoUsuario_PF, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoUsuario_PF_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoUsuario_PF_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 37 )
         {
            wbEnd = 0;
            nRC_GXsfl_37 = (short)(nGXsfl_37_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_3U2e( true) ;
         }
         else
         {
            wb_table4_34_3U2e( false) ;
         }
      }

      protected void wb_table2_5_3U2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_8_3U2( true) ;
         }
         else
         {
            wb_table5_8_3U2( false) ;
         }
         return  ;
      }

      protected void wb_table5_8_3U2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_3U2e( true) ;
         }
         else
         {
            wb_table2_5_3U2e( false) ;
         }
      }

      protected void wb_table5_8_3U2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptFuncaoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_13_3U2( true) ;
         }
         else
         {
            wb_table6_13_3U2( false) ;
         }
         return  ;
      }

      protected void wb_table6_13_3U2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_8_3U2e( true) ;
         }
         else
         {
            wb_table5_8_3U2e( false) ;
         }
      }

      protected void wb_table6_13_3U2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptFuncaoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'" + sGXsfl_37_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_PromptFuncaoUsuario.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptFuncaoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_22_3U2( true) ;
         }
         else
         {
            wb_table7_22_3U2( false) ;
         }
         return  ;
      }

      protected void wb_table7_22_3U2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_13_3U2e( true) ;
         }
         else
         {
            wb_table6_13_3U2e( false) ;
         }
      }

      protected void wb_table7_22_3U2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'" + sGXsfl_37_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,25);\"", "", true, "HLP_PromptFuncaoUsuario.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'" + sGXsfl_37_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncaousuario_nome1_Internalname, AV17FuncaoUsuario_Nome1, StringUtil.RTrim( context.localUtil.Format( AV17FuncaoUsuario_Nome1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncaousuario_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncaousuario_nome1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 200, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptFuncaoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_22_3U2e( true) ;
         }
         else
         {
            wb_table7_22_3U2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutFuncaoUsuario_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutFuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutFuncaoUsuario_Codigo), 6, 0)));
         AV8InOutFuncaoUsuario_Nome = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutFuncaoUsuario_Nome", AV8InOutFuncaoUsuario_Nome);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA3U2( ) ;
         WS3U2( ) ;
         WE3U2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042823131134");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptfuncaousuario.js", "?202042823131135");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_372( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_37_idx;
         edtFuncaoUsuario_Codigo_Internalname = "FUNCAOUSUARIO_CODIGO_"+sGXsfl_37_idx;
         edtFuncaoUsuario_Nome_Internalname = "FUNCAOUSUARIO_NOME_"+sGXsfl_37_idx;
         edtFuncaoUsuario_PF_Internalname = "FUNCAOUSUARIO_PF_"+sGXsfl_37_idx;
      }

      protected void SubsflControlProps_fel_372( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_37_fel_idx;
         edtFuncaoUsuario_Codigo_Internalname = "FUNCAOUSUARIO_CODIGO_"+sGXsfl_37_fel_idx;
         edtFuncaoUsuario_Nome_Internalname = "FUNCAOUSUARIO_NOME_"+sGXsfl_37_fel_idx;
         edtFuncaoUsuario_PF_Internalname = "FUNCAOUSUARIO_PF_"+sGXsfl_37_fel_idx;
      }

      protected void sendrow_372( )
      {
         SubsflControlProps_372( ) ;
         WB3U0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_37_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_37_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_37_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 38,'',false,'',37)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV28Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV66Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV66Select_GXI : context.PathToRelativeUrl( AV28Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_37_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV28Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoUsuario_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A161FuncaoUsuario_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A161FuncaoUsuario_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoUsuario_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)37,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoUsuario_Nome_Internalname,(String)A162FuncaoUsuario_Nome,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoUsuario_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)200,(short)0,(short)0,(short)37,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoUsuario_PF_Internalname,StringUtil.LTrim( StringUtil.NToC( A396FuncaoUsuario_PF, 14, 5, ",", "")),context.localUtil.Format( A396FuncaoUsuario_PF, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoUsuario_PF_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)37,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOUSUARIO_CODIGO"+"_"+sGXsfl_37_idx, GetSecureSignedToken( sGXsfl_37_idx, context.localUtil.Format( (decimal)(A161FuncaoUsuario_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOUSUARIO_NOME"+"_"+sGXsfl_37_idx, GetSecureSignedToken( sGXsfl_37_idx, StringUtil.RTrim( context.localUtil.Format( A162FuncaoUsuario_Nome, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOUSUARIO_PF"+"_"+sGXsfl_37_idx, GetSecureSignedToken( sGXsfl_37_idx, context.localUtil.Format( A396FuncaoUsuario_PF, "ZZ,ZZZ,ZZ9.999")));
            GridContainer.AddRow(GridRow);
            nGXsfl_37_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_37_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_37_idx+1));
            sGXsfl_37_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_37_idx), 4, 0)), 4, "0");
            SubsflControlProps_372( ) ;
         }
         /* End function sendrow_372 */
      }

      protected void init_default_properties( )
      {
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavFuncaousuario_nome1_Internalname = "vFUNCAOUSUARIO_NOME1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtFuncaoUsuario_Codigo_Internalname = "FUNCAOUSUARIO_CODIGO";
         edtFuncaoUsuario_Nome_Internalname = "FUNCAOUSUARIO_NOME";
         edtFuncaoUsuario_PF_Internalname = "FUNCAOUSUARIO_PF";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         edtavTffuncaousuario_nome_Internalname = "vTFFUNCAOUSUARIO_NOME";
         edtavTffuncaousuario_nome_sel_Internalname = "vTFFUNCAOUSUARIO_NOME_SEL";
         edtavTffuncaousuario_pf_Internalname = "vTFFUNCAOUSUARIO_PF";
         edtavTffuncaousuario_pf_to_Internalname = "vTFFUNCAOUSUARIO_PF_TO";
         Ddo_funcaousuario_nome_Internalname = "DDO_FUNCAOUSUARIO_NOME";
         edtavDdo_funcaousuario_nometitlecontrolidtoreplace_Internalname = "vDDO_FUNCAOUSUARIO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_funcaousuario_pf_Internalname = "DDO_FUNCAOUSUARIO_PF";
         edtavDdo_funcaousuario_pftitlecontrolidtoreplace_Internalname = "vDDO_FUNCAOUSUARIO_PFTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtFuncaoUsuario_PF_Jsonclick = "";
         edtFuncaoUsuario_Nome_Jsonclick = "";
         edtFuncaoUsuario_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavFuncaousuario_nome1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtFuncaoUsuario_PF_Titleformat = 0;
         edtFuncaoUsuario_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavFuncaousuario_nome1_Visible = 1;
         edtFuncaoUsuario_PF_Title = "PF";
         edtFuncaoUsuario_Nome_Title = "Nome";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_funcaousuario_pftitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaousuario_nometitlecontrolidtoreplace_Visible = 1;
         edtavTffuncaousuario_pf_to_Jsonclick = "";
         edtavTffuncaousuario_pf_to_Visible = 1;
         edtavTffuncaousuario_pf_Jsonclick = "";
         edtavTffuncaousuario_pf_Visible = 1;
         edtavTffuncaousuario_nome_sel_Jsonclick = "";
         edtavTffuncaousuario_nome_sel_Visible = 1;
         edtavTffuncaousuario_nome_Jsonclick = "";
         edtavTffuncaousuario_nome_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         Ddo_funcaousuario_pf_Searchbuttontext = "Pesquisar";
         Ddo_funcaousuario_pf_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaousuario_pf_Rangefilterto = "At�";
         Ddo_funcaousuario_pf_Rangefilterfrom = "Desde";
         Ddo_funcaousuario_pf_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaousuario_pf_Loadingdata = "Carregando dados...";
         Ddo_funcaousuario_pf_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaousuario_pf_Sortasc = "Ordenar de A � Z";
         Ddo_funcaousuario_pf_Datalistupdateminimumcharacters = 0;
         Ddo_funcaousuario_pf_Datalistfixedvalues = "";
         Ddo_funcaousuario_pf_Includedatalist = Convert.ToBoolean( 0);
         Ddo_funcaousuario_pf_Filterisrange = Convert.ToBoolean( -1);
         Ddo_funcaousuario_pf_Filtertype = "Numeric";
         Ddo_funcaousuario_pf_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaousuario_pf_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_funcaousuario_pf_Includesortasc = Convert.ToBoolean( 0);
         Ddo_funcaousuario_pf_Titlecontrolidtoreplace = "";
         Ddo_funcaousuario_pf_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaousuario_pf_Cls = "ColumnSettings";
         Ddo_funcaousuario_pf_Tooltip = "Op��es";
         Ddo_funcaousuario_pf_Caption = "";
         Ddo_funcaousuario_nome_Searchbuttontext = "Pesquisar";
         Ddo_funcaousuario_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaousuario_nome_Rangefilterto = "At�";
         Ddo_funcaousuario_nome_Rangefilterfrom = "Desde";
         Ddo_funcaousuario_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaousuario_nome_Loadingdata = "Carregando dados...";
         Ddo_funcaousuario_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaousuario_nome_Sortasc = "Ordenar de A � Z";
         Ddo_funcaousuario_nome_Datalistupdateminimumcharacters = 0;
         Ddo_funcaousuario_nome_Datalistproc = "GetPromptFuncaoUsuarioFilterData";
         Ddo_funcaousuario_nome_Datalistfixedvalues = "";
         Ddo_funcaousuario_nome_Datalisttype = "Dynamic";
         Ddo_funcaousuario_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaousuario_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcaousuario_nome_Filtertype = "Character";
         Ddo_funcaousuario_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaousuario_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaousuario_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaousuario_nome_Titlecontrolidtoreplace = "";
         Ddo_funcaousuario_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaousuario_nome_Cls = "ColumnSettings";
         Ddo_funcaousuario_nome_Tooltip = "Op��es";
         Ddo_funcaousuario_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Fun��es de Usu�rio";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV54ddo_FuncaoUsuario_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOUSUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_FuncaoUsuario_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOUSUARIO_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV52TFFuncaoUsuario_Nome',fld:'vTFFUNCAOUSUARIO_NOME',pic:'',nv:''},{av:'AV53TFFuncaoUsuario_Nome_Sel',fld:'vTFFUNCAOUSUARIO_NOME_SEL',pic:'',nv:''},{av:'AV56TFFuncaoUsuario_PF',fld:'vTFFUNCAOUSUARIO_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFFuncaoUsuario_PF_To',fld:'vTFFUNCAOUSUARIO_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17FuncaoUsuario_Nome1',fld:'vFUNCAOUSUARIO_NOME1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0}],oparms:[{av:'AV51FuncaoUsuario_NomeTitleFilterData',fld:'vFUNCAOUSUARIO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV55FuncaoUsuario_PFTitleFilterData',fld:'vFUNCAOUSUARIO_PFTITLEFILTERDATA',pic:'',nv:null},{av:'edtFuncaoUsuario_Nome_Titleformat',ctrl:'FUNCAOUSUARIO_NOME',prop:'Titleformat'},{av:'edtFuncaoUsuario_Nome_Title',ctrl:'FUNCAOUSUARIO_NOME',prop:'Title'},{av:'edtFuncaoUsuario_PF_Titleformat',ctrl:'FUNCAOUSUARIO_PF',prop:'Titleformat'},{av:'edtFuncaoUsuario_PF_Title',ctrl:'FUNCAOUSUARIO_PF',prop:'Title'},{av:'AV61GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV62GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E113U2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoUsuario_Nome1',fld:'vFUNCAOUSUARIO_NOME1',pic:'',nv:''},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV52TFFuncaoUsuario_Nome',fld:'vTFFUNCAOUSUARIO_NOME',pic:'',nv:''},{av:'AV53TFFuncaoUsuario_Nome_Sel',fld:'vTFFUNCAOUSUARIO_NOME_SEL',pic:'',nv:''},{av:'AV56TFFuncaoUsuario_PF',fld:'vTFFUNCAOUSUARIO_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFFuncaoUsuario_PF_To',fld:'vTFFUNCAOUSUARIO_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54ddo_FuncaoUsuario_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOUSUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_FuncaoUsuario_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOUSUARIO_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_FUNCAOUSUARIO_NOME.ONOPTIONCLICKED","{handler:'E123U2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoUsuario_Nome1',fld:'vFUNCAOUSUARIO_NOME1',pic:'',nv:''},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV52TFFuncaoUsuario_Nome',fld:'vTFFUNCAOUSUARIO_NOME',pic:'',nv:''},{av:'AV53TFFuncaoUsuario_Nome_Sel',fld:'vTFFUNCAOUSUARIO_NOME_SEL',pic:'',nv:''},{av:'AV56TFFuncaoUsuario_PF',fld:'vTFFUNCAOUSUARIO_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFFuncaoUsuario_PF_To',fld:'vTFFUNCAOUSUARIO_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54ddo_FuncaoUsuario_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOUSUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_FuncaoUsuario_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOUSUARIO_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'Ddo_funcaousuario_nome_Activeeventkey',ctrl:'DDO_FUNCAOUSUARIO_NOME',prop:'ActiveEventKey'},{av:'Ddo_funcaousuario_nome_Filteredtext_get',ctrl:'DDO_FUNCAOUSUARIO_NOME',prop:'FilteredText_get'},{av:'Ddo_funcaousuario_nome_Selectedvalue_get',ctrl:'DDO_FUNCAOUSUARIO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaousuario_nome_Sortedstatus',ctrl:'DDO_FUNCAOUSUARIO_NOME',prop:'SortedStatus'},{av:'AV52TFFuncaoUsuario_Nome',fld:'vTFFUNCAOUSUARIO_NOME',pic:'',nv:''},{av:'AV53TFFuncaoUsuario_Nome_Sel',fld:'vTFFUNCAOUSUARIO_NOME_SEL',pic:'',nv:''}]}");
         setEventMetadata("DDO_FUNCAOUSUARIO_PF.ONOPTIONCLICKED","{handler:'E133U2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoUsuario_Nome1',fld:'vFUNCAOUSUARIO_NOME1',pic:'',nv:''},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV52TFFuncaoUsuario_Nome',fld:'vTFFUNCAOUSUARIO_NOME',pic:'',nv:''},{av:'AV53TFFuncaoUsuario_Nome_Sel',fld:'vTFFUNCAOUSUARIO_NOME_SEL',pic:'',nv:''},{av:'AV56TFFuncaoUsuario_PF',fld:'vTFFUNCAOUSUARIO_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFFuncaoUsuario_PF_To',fld:'vTFFUNCAOUSUARIO_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54ddo_FuncaoUsuario_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOUSUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_FuncaoUsuario_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOUSUARIO_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'Ddo_funcaousuario_pf_Activeeventkey',ctrl:'DDO_FUNCAOUSUARIO_PF',prop:'ActiveEventKey'},{av:'Ddo_funcaousuario_pf_Filteredtext_get',ctrl:'DDO_FUNCAOUSUARIO_PF',prop:'FilteredText_get'},{av:'Ddo_funcaousuario_pf_Filteredtextto_get',ctrl:'DDO_FUNCAOUSUARIO_PF',prop:'FilteredTextTo_get'}],oparms:[{av:'AV56TFFuncaoUsuario_PF',fld:'vTFFUNCAOUSUARIO_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFFuncaoUsuario_PF_To',fld:'vTFFUNCAOUSUARIO_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         setEventMetadata("GRID.LOAD","{handler:'E183U2',iparms:[],oparms:[{av:'AV28Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E193U2',iparms:[{av:'A161FuncaoUsuario_Codigo',fld:'FUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A162FuncaoUsuario_Nome',fld:'FUNCAOUSUARIO_NOME',pic:'',hsh:true,nv:''}],oparms:[{av:'AV7InOutFuncaoUsuario_Codigo',fld:'vINOUTFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutFuncaoUsuario_Nome',fld:'vINOUTFUNCAOUSUARIO_NOME',pic:'',nv:''}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E153U2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavFuncaousuario_nome1_Visible',ctrl:'vFUNCAOUSUARIO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E143U2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoUsuario_Nome1',fld:'vFUNCAOUSUARIO_NOME1',pic:'',nv:''},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV52TFFuncaoUsuario_Nome',fld:'vTFFUNCAOUSUARIO_NOME',pic:'',nv:''},{av:'AV53TFFuncaoUsuario_Nome_Sel',fld:'vTFFUNCAOUSUARIO_NOME_SEL',pic:'',nv:''},{av:'AV56TFFuncaoUsuario_PF',fld:'vTFFUNCAOUSUARIO_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV57TFFuncaoUsuario_PF_To',fld:'vTFFUNCAOUSUARIO_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV54ddo_FuncaoUsuario_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOUSUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_FuncaoUsuario_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOUSUARIO_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}],oparms:[{av:'AV52TFFuncaoUsuario_Nome',fld:'vTFFUNCAOUSUARIO_NOME',pic:'',nv:''},{av:'Ddo_funcaousuario_nome_Filteredtext_set',ctrl:'DDO_FUNCAOUSUARIO_NOME',prop:'FilteredText_set'},{av:'AV53TFFuncaoUsuario_Nome_Sel',fld:'vTFFUNCAOUSUARIO_NOME_SEL',pic:'',nv:''},{av:'Ddo_funcaousuario_nome_Selectedvalue_set',ctrl:'DDO_FUNCAOUSUARIO_NOME',prop:'SelectedValue_set'},{av:'AV56TFFuncaoUsuario_PF',fld:'vTFFUNCAOUSUARIO_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_funcaousuario_pf_Filteredtext_set',ctrl:'DDO_FUNCAOUSUARIO_PF',prop:'FilteredText_set'},{av:'AV57TFFuncaoUsuario_PF_To',fld:'vTFFUNCAOUSUARIO_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_funcaousuario_pf_Filteredtextto_set',ctrl:'DDO_FUNCAOUSUARIO_PF',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoUsuario_Nome1',fld:'vFUNCAOUSUARIO_NOME1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavFuncaousuario_nome1_Visible',ctrl:'vFUNCAOUSUARIO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutFuncaoUsuario_Nome = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_funcaousuario_nome_Activeeventkey = "";
         Ddo_funcaousuario_nome_Filteredtext_get = "";
         Ddo_funcaousuario_nome_Selectedvalue_get = "";
         Ddo_funcaousuario_pf_Activeeventkey = "";
         Ddo_funcaousuario_pf_Filteredtext_get = "";
         Ddo_funcaousuario_pf_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17FuncaoUsuario_Nome1 = "";
         AV52TFFuncaoUsuario_Nome = "";
         AV53TFFuncaoUsuario_Nome_Sel = "";
         AV54ddo_FuncaoUsuario_NomeTitleControlIdToReplace = "";
         AV58ddo_FuncaoUsuario_PFTitleControlIdToReplace = "";
         AV67Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV59DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV51FuncaoUsuario_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV55FuncaoUsuario_PFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_funcaousuario_nome_Filteredtext_set = "";
         Ddo_funcaousuario_nome_Selectedvalue_set = "";
         Ddo_funcaousuario_nome_Sortedstatus = "";
         Ddo_funcaousuario_pf_Filteredtext_set = "";
         Ddo_funcaousuario_pf_Filteredtextto_set = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Select = "";
         AV66Select_GXI = "";
         A162FuncaoUsuario_Nome = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV17FuncaoUsuario_Nome1 = "";
         lV52TFFuncaoUsuario_Nome = "";
         H003U2_A162FuncaoUsuario_Nome = new String[] {""} ;
         H003U2_A161FuncaoUsuario_Codigo = new int[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         imgCleanfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptfuncaousuario__default(),
            new Object[][] {
                new Object[] {
               H003U2_A162FuncaoUsuario_Nome, H003U2_A161FuncaoUsuario_Codigo
               }
            }
         );
         AV67Pgmname = "PromptFuncaoUsuario";
         /* GeneXus formulas. */
         AV67Pgmname = "PromptFuncaoUsuario";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_37 ;
      private short nGXsfl_37_idx=1 ;
      private short AV16DynamicFiltersOperator1 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_37_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short GXt_int1 ;
      private short edtFuncaoUsuario_Nome_Titleformat ;
      private short edtFuncaoUsuario_PF_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutFuncaoUsuario_Codigo ;
      private int wcpOAV7InOutFuncaoUsuario_Codigo ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_funcaousuario_nome_Datalistupdateminimumcharacters ;
      private int Ddo_funcaousuario_pf_Datalistupdateminimumcharacters ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTffuncaousuario_nome_Visible ;
      private int edtavTffuncaousuario_nome_sel_Visible ;
      private int edtavTffuncaousuario_pf_Visible ;
      private int edtavTffuncaousuario_pf_to_Visible ;
      private int edtavDdo_funcaousuario_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaousuario_pftitlecontrolidtoreplace_Visible ;
      private int A161FuncaoUsuario_Codigo ;
      private int subGrid_Islastpage ;
      private int AV60PageToGo ;
      private int edtavFuncaousuario_nome1_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV61GridCurrentPage ;
      private long AV62GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV56TFFuncaoUsuario_PF ;
      private decimal AV57TFFuncaoUsuario_PF_To ;
      private decimal A396FuncaoUsuario_PF ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_funcaousuario_nome_Activeeventkey ;
      private String Ddo_funcaousuario_nome_Filteredtext_get ;
      private String Ddo_funcaousuario_nome_Selectedvalue_get ;
      private String Ddo_funcaousuario_pf_Activeeventkey ;
      private String Ddo_funcaousuario_pf_Filteredtext_get ;
      private String Ddo_funcaousuario_pf_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_37_idx="0001" ;
      private String AV67Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_funcaousuario_nome_Caption ;
      private String Ddo_funcaousuario_nome_Tooltip ;
      private String Ddo_funcaousuario_nome_Cls ;
      private String Ddo_funcaousuario_nome_Filteredtext_set ;
      private String Ddo_funcaousuario_nome_Selectedvalue_set ;
      private String Ddo_funcaousuario_nome_Dropdownoptionstype ;
      private String Ddo_funcaousuario_nome_Titlecontrolidtoreplace ;
      private String Ddo_funcaousuario_nome_Sortedstatus ;
      private String Ddo_funcaousuario_nome_Filtertype ;
      private String Ddo_funcaousuario_nome_Datalisttype ;
      private String Ddo_funcaousuario_nome_Datalistfixedvalues ;
      private String Ddo_funcaousuario_nome_Datalistproc ;
      private String Ddo_funcaousuario_nome_Sortasc ;
      private String Ddo_funcaousuario_nome_Sortdsc ;
      private String Ddo_funcaousuario_nome_Loadingdata ;
      private String Ddo_funcaousuario_nome_Cleanfilter ;
      private String Ddo_funcaousuario_nome_Rangefilterfrom ;
      private String Ddo_funcaousuario_nome_Rangefilterto ;
      private String Ddo_funcaousuario_nome_Noresultsfound ;
      private String Ddo_funcaousuario_nome_Searchbuttontext ;
      private String Ddo_funcaousuario_pf_Caption ;
      private String Ddo_funcaousuario_pf_Tooltip ;
      private String Ddo_funcaousuario_pf_Cls ;
      private String Ddo_funcaousuario_pf_Filteredtext_set ;
      private String Ddo_funcaousuario_pf_Filteredtextto_set ;
      private String Ddo_funcaousuario_pf_Dropdownoptionstype ;
      private String Ddo_funcaousuario_pf_Titlecontrolidtoreplace ;
      private String Ddo_funcaousuario_pf_Filtertype ;
      private String Ddo_funcaousuario_pf_Datalistfixedvalues ;
      private String Ddo_funcaousuario_pf_Sortasc ;
      private String Ddo_funcaousuario_pf_Sortdsc ;
      private String Ddo_funcaousuario_pf_Loadingdata ;
      private String Ddo_funcaousuario_pf_Cleanfilter ;
      private String Ddo_funcaousuario_pf_Rangefilterfrom ;
      private String Ddo_funcaousuario_pf_Rangefilterto ;
      private String Ddo_funcaousuario_pf_Noresultsfound ;
      private String Ddo_funcaousuario_pf_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTffuncaousuario_nome_Internalname ;
      private String edtavTffuncaousuario_nome_Jsonclick ;
      private String edtavTffuncaousuario_nome_sel_Internalname ;
      private String edtavTffuncaousuario_nome_sel_Jsonclick ;
      private String edtavTffuncaousuario_pf_Internalname ;
      private String edtavTffuncaousuario_pf_Jsonclick ;
      private String edtavTffuncaousuario_pf_to_Internalname ;
      private String edtavTffuncaousuario_pf_to_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_funcaousuario_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaousuario_pftitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtFuncaoUsuario_Codigo_Internalname ;
      private String edtFuncaoUsuario_Nome_Internalname ;
      private String edtFuncaoUsuario_PF_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String scmdbuf ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavFuncaousuario_nome1_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_funcaousuario_nome_Internalname ;
      private String Ddo_funcaousuario_pf_Internalname ;
      private String edtFuncaoUsuario_Nome_Title ;
      private String edtFuncaoUsuario_PF_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String tblTablefilters_Internalname ;
      private String imgCleanfilters_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavFuncaousuario_nome1_Jsonclick ;
      private String sGXsfl_37_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtFuncaoUsuario_Codigo_Jsonclick ;
      private String edtFuncaoUsuario_Nome_Jsonclick ;
      private String edtFuncaoUsuario_PF_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_funcaousuario_nome_Includesortasc ;
      private bool Ddo_funcaousuario_nome_Includesortdsc ;
      private bool Ddo_funcaousuario_nome_Includefilter ;
      private bool Ddo_funcaousuario_nome_Filterisrange ;
      private bool Ddo_funcaousuario_nome_Includedatalist ;
      private bool Ddo_funcaousuario_pf_Includesortasc ;
      private bool Ddo_funcaousuario_pf_Includesortdsc ;
      private bool Ddo_funcaousuario_pf_Includefilter ;
      private bool Ddo_funcaousuario_pf_Filterisrange ;
      private bool Ddo_funcaousuario_pf_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Select_IsBlob ;
      private String AV8InOutFuncaoUsuario_Nome ;
      private String wcpOAV8InOutFuncaoUsuario_Nome ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV17FuncaoUsuario_Nome1 ;
      private String AV52TFFuncaoUsuario_Nome ;
      private String AV53TFFuncaoUsuario_Nome_Sel ;
      private String AV54ddo_FuncaoUsuario_NomeTitleControlIdToReplace ;
      private String AV58ddo_FuncaoUsuario_PFTitleControlIdToReplace ;
      private String AV66Select_GXI ;
      private String A162FuncaoUsuario_Nome ;
      private String lV17FuncaoUsuario_Nome1 ;
      private String lV52TFFuncaoUsuario_Nome ;
      private String AV28Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutFuncaoUsuario_Codigo ;
      private String aP1_InOutFuncaoUsuario_Nome ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private IDataStoreProvider pr_default ;
      private String[] H003U2_A162FuncaoUsuario_Nome ;
      private int[] H003U2_A161FuncaoUsuario_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV51FuncaoUsuario_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV55FuncaoUsuario_PFTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV59DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2 ;
   }

   public class promptfuncaousuario__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H003U2( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV17FuncaoUsuario_Nome1 ,
                                             String AV53TFFuncaoUsuario_Nome_Sel ,
                                             String AV52TFFuncaoUsuario_Nome ,
                                             String A162FuncaoUsuario_Nome ,
                                             bool AV14OrderedDsc ,
                                             decimal AV56TFFuncaoUsuario_PF ,
                                             decimal A396FuncaoUsuario_PF ,
                                             decimal AV57TFFuncaoUsuario_PF_To )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [4] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [FuncaoUsuario_Nome], [FuncaoUsuario_Codigo] FROM [ModuloFuncoes] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOUSUARIO_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17FuncaoUsuario_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoUsuario_Nome] like @lV17FuncaoUsuario_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoUsuario_Nome] like @lV17FuncaoUsuario_Nome1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOUSUARIO_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17FuncaoUsuario_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoUsuario_Nome] like '%' + @lV17FuncaoUsuario_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoUsuario_Nome] like '%' + @lV17FuncaoUsuario_Nome1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV53TFFuncaoUsuario_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFFuncaoUsuario_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoUsuario_Nome] like @lV52TFFuncaoUsuario_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoUsuario_Nome] like @lV52TFFuncaoUsuario_Nome)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TFFuncaoUsuario_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([FuncaoUsuario_Nome] = @AV53TFFuncaoUsuario_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([FuncaoUsuario_Nome] = @AV53TFFuncaoUsuario_Nome_Sel)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY [FuncaoUsuario_Nome]";
         }
         else if ( AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY [FuncaoUsuario_Nome] DESC";
         }
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H003U2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (decimal)dynConstraints[7] , (decimal)dynConstraints[8] , (decimal)dynConstraints[9] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH003U2 ;
          prmH003U2 = new Object[] {
          new Object[] {"@lV17FuncaoUsuario_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV17FuncaoUsuario_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV52TFFuncaoUsuario_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV53TFFuncaoUsuario_Nome_Sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H003U2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003U2,11,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                return;
       }
    }

 }

}
