/*
               File: PRC_ParametrosSistemaImagem
        Description: Garrega Imagem do Login
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:56:20.38
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_parametrossistemaimagem : GXProcedure
   {
      public prc_parametrossistemaimagem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_parametrossistemaimagem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( short aP0_Acao ,
                           out String aP1_ParametrosSistema_Imagem )
      {
         this.AV8Acao = aP0_Acao;
         this.AV9ParametrosSistema_Imagem = "" ;
         initialize();
         executePrivate();
         aP1_ParametrosSistema_Imagem=this.AV9ParametrosSistema_Imagem;
      }

      public String executeUdp( short aP0_Acao )
      {
         this.AV8Acao = aP0_Acao;
         this.AV9ParametrosSistema_Imagem = "" ;
         initialize();
         executePrivate();
         aP1_ParametrosSistema_Imagem=this.AV9ParametrosSistema_Imagem;
         return AV9ParametrosSistema_Imagem ;
      }

      public void executeSubmit( short aP0_Acao ,
                                 out String aP1_ParametrosSistema_Imagem )
      {
         prc_parametrossistemaimagem objprc_parametrossistemaimagem;
         objprc_parametrossistemaimagem = new prc_parametrossistemaimagem();
         objprc_parametrossistemaimagem.AV8Acao = aP0_Acao;
         objprc_parametrossistemaimagem.AV9ParametrosSistema_Imagem = "" ;
         objprc_parametrossistemaimagem.context.SetSubmitInitialConfig(context);
         objprc_parametrossistemaimagem.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_parametrossistemaimagem);
         aP1_ParametrosSistema_Imagem=this.AV9ParametrosSistema_Imagem;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_parametrossistemaimagem)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         new geralog(context ).execute( ref  AV12Pgmname) ;
         if ( AV8Acao == 1 )
         {
            AV9ParametrosSistema_Imagem = context.convertURL( (String)(context.GetImagePath( "294bb274-035c-4db1-846b-1b6e61288853", "", context.GetTheme( ))));
            AV13Parametrossistema_imagem_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.convertURL( (String)(context.GetImagePath( "294bb274-035c-4db1-846b-1b6e61288853", "", context.GetTheme( )))));
            GXt_char1 = "retorno";
            new geralog(context ).execute( ref  GXt_char1) ;
         }
         else if ( AV8Acao == 2 )
         {
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV12Pgmname = "";
         AV13Parametrossistema_imagem_GXI = "";
         GXt_char1 = "";
         AV12Pgmname = "PRC_ParametrosSistemaImagem";
         /* GeneXus formulas. */
         AV12Pgmname = "PRC_ParametrosSistemaImagem";
         context.Gx_err = 0;
      }

      private short AV8Acao ;
      private String AV12Pgmname ;
      private String GXt_char1 ;
      private String AV13Parametrossistema_imagem_GXI ;
      private String AV9ParametrosSistema_Imagem ;
      private String aP1_ParametrosSistema_Imagem ;
   }

}
