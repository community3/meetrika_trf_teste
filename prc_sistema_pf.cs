/*
               File: PRC_Sistema_PF
        Description: Pontos de Fun��o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:49.93
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_sistema_pf : GXProcedure
   {
      public prc_sistema_pf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_sistema_pf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Sistema_Codigo ,
                           ref decimal aP1_Sistema_PF )
      {
         this.A127Sistema_Codigo = aP0_Sistema_Codigo;
         this.AV8Sistema_PF = aP1_Sistema_PF;
         initialize();
         executePrivate();
         aP0_Sistema_Codigo=this.A127Sistema_Codigo;
         aP1_Sistema_PF=this.AV8Sistema_PF;
      }

      public decimal executeUdp( ref int aP0_Sistema_Codigo )
      {
         this.A127Sistema_Codigo = aP0_Sistema_Codigo;
         this.AV8Sistema_PF = aP1_Sistema_PF;
         initialize();
         executePrivate();
         aP0_Sistema_Codigo=this.A127Sistema_Codigo;
         aP1_Sistema_PF=this.AV8Sistema_PF;
         return AV8Sistema_PF ;
      }

      public void executeSubmit( ref int aP0_Sistema_Codigo ,
                                 ref decimal aP1_Sistema_PF )
      {
         prc_sistema_pf objprc_sistema_pf;
         objprc_sistema_pf = new prc_sistema_pf();
         objprc_sistema_pf.A127Sistema_Codigo = aP0_Sistema_Codigo;
         objprc_sistema_pf.AV8Sistema_PF = aP1_Sistema_PF;
         objprc_sistema_pf.context.SetSubmitInitialConfig(context);
         objprc_sistema_pf.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_sistema_pf);
         aP0_Sistema_Codigo=this.A127Sistema_Codigo;
         aP1_Sistema_PF=this.AV8Sistema_PF;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_sistema_pf)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8Sistema_PF = 0;
         /* Using cursor P00232 */
         pr_default.execute(0, new Object[] {A127Sistema_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A370FuncaoDados_SistemaCod = P00232_A370FuncaoDados_SistemaCod[0];
            A394FuncaoDados_Ativo = P00232_A394FuncaoDados_Ativo[0];
            A755FuncaoDados_Contar = P00232_A755FuncaoDados_Contar[0];
            n755FuncaoDados_Contar = P00232_n755FuncaoDados_Contar[0];
            A368FuncaoDados_Codigo = P00232_A368FuncaoDados_Codigo[0];
            if ( A755FuncaoDados_Contar )
            {
               GXt_int1 = (short)(A377FuncaoDados_PF);
               new prc_fdpf(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_int1) ;
               A377FuncaoDados_PF = (decimal)(GXt_int1);
            }
            else
            {
               A377FuncaoDados_PF = 0;
            }
            AV8Sistema_PF = (decimal)(AV8Sistema_PF+A377FuncaoDados_PF);
            pr_default.readNext(0);
         }
         pr_default.close(0);
         /* Using cursor P00233 */
         pr_default.execute(1, new Object[] {A127Sistema_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A360FuncaoAPF_SistemaCod = P00233_A360FuncaoAPF_SistemaCod[0];
            n360FuncaoAPF_SistemaCod = P00233_n360FuncaoAPF_SistemaCod[0];
            A183FuncaoAPF_Ativo = P00233_A183FuncaoAPF_Ativo[0];
            A165FuncaoAPF_Codigo = P00233_A165FuncaoAPF_Codigo[0];
            GXt_decimal2 = A386FuncaoAPF_PF;
            new prc_fapfpf(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_decimal2) ;
            A386FuncaoAPF_PF = GXt_decimal2;
            AV8Sistema_PF = (decimal)(AV8Sistema_PF+A386FuncaoAPF_PF);
            pr_default.readNext(1);
         }
         pr_default.close(1);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00232_A370FuncaoDados_SistemaCod = new int[1] ;
         P00232_A394FuncaoDados_Ativo = new String[] {""} ;
         P00232_A755FuncaoDados_Contar = new bool[] {false} ;
         P00232_n755FuncaoDados_Contar = new bool[] {false} ;
         P00232_A368FuncaoDados_Codigo = new int[1] ;
         A394FuncaoDados_Ativo = "";
         P00233_A360FuncaoAPF_SistemaCod = new int[1] ;
         P00233_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         P00233_A183FuncaoAPF_Ativo = new String[] {""} ;
         P00233_A165FuncaoAPF_Codigo = new int[1] ;
         A183FuncaoAPF_Ativo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_sistema_pf__default(),
            new Object[][] {
                new Object[] {
               P00232_A370FuncaoDados_SistemaCod, P00232_A394FuncaoDados_Ativo, P00232_A755FuncaoDados_Contar, P00232_n755FuncaoDados_Contar, P00232_A368FuncaoDados_Codigo
               }
               , new Object[] {
               P00233_A360FuncaoAPF_SistemaCod, P00233_n360FuncaoAPF_SistemaCod, P00233_A183FuncaoAPF_Ativo, P00233_A165FuncaoAPF_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short GXt_int1 ;
      private int A127Sistema_Codigo ;
      private int A370FuncaoDados_SistemaCod ;
      private int A368FuncaoDados_Codigo ;
      private int A360FuncaoAPF_SistemaCod ;
      private int A165FuncaoAPF_Codigo ;
      private decimal AV8Sistema_PF ;
      private decimal A377FuncaoDados_PF ;
      private decimal A386FuncaoAPF_PF ;
      private decimal GXt_decimal2 ;
      private String scmdbuf ;
      private String A394FuncaoDados_Ativo ;
      private String A183FuncaoAPF_Ativo ;
      private bool A755FuncaoDados_Contar ;
      private bool n755FuncaoDados_Contar ;
      private bool n360FuncaoAPF_SistemaCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Sistema_Codigo ;
      private decimal aP1_Sistema_PF ;
      private IDataStoreProvider pr_default ;
      private int[] P00232_A370FuncaoDados_SistemaCod ;
      private String[] P00232_A394FuncaoDados_Ativo ;
      private bool[] P00232_A755FuncaoDados_Contar ;
      private bool[] P00232_n755FuncaoDados_Contar ;
      private int[] P00232_A368FuncaoDados_Codigo ;
      private int[] P00233_A360FuncaoAPF_SistemaCod ;
      private bool[] P00233_n360FuncaoAPF_SistemaCod ;
      private String[] P00233_A183FuncaoAPF_Ativo ;
      private int[] P00233_A165FuncaoAPF_Codigo ;
   }

   public class prc_sistema_pf__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00232 ;
          prmP00232 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00233 ;
          prmP00233 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00232", "SELECT [FuncaoDados_SistemaCod], [FuncaoDados_Ativo], [FuncaoDados_Contar], [FuncaoDados_Codigo] FROM [FuncaoDados] WITH (NOLOCK) WHERE ([FuncaoDados_SistemaCod] = @Sistema_Codigo) AND ([FuncaoDados_Ativo] = 'A') ORDER BY [FuncaoDados_SistemaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00232,100,0,true,false )
             ,new CursorDef("P00233", "SELECT [FuncaoAPF_SistemaCod], [FuncaoAPF_Ativo], [FuncaoAPF_Codigo] FROM [FuncoesAPF] WITH (NOLOCK) WHERE ([FuncaoAPF_SistemaCod] = @Sistema_Codigo) AND ([FuncaoAPF_Ativo] = 'A') ORDER BY [FuncaoAPF_SistemaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00233,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
