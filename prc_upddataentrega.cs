/*
               File: PRC_UpdDataEntrega
        Description: Upd Data Entrega
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:18.90
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_upddataentrega : GXProcedure
   {
      public prc_upddataentrega( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_upddataentrega( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo ,
                           DateTime aP1_Data ,
                           out bool aP2_Updated )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV10Data = aP1_Data;
         this.AV8Updated = false ;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP2_Updated=this.AV8Updated;
      }

      public bool executeUdp( ref int aP0_ContagemResultado_Codigo ,
                              DateTime aP1_Data )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV10Data = aP1_Data;
         this.AV8Updated = false ;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP2_Updated=this.AV8Updated;
         return AV8Updated ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo ,
                                 DateTime aP1_Data ,
                                 out bool aP2_Updated )
      {
         prc_upddataentrega objprc_upddataentrega;
         objprc_upddataentrega = new prc_upddataentrega();
         objprc_upddataentrega.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_upddataentrega.AV10Data = aP1_Data;
         objprc_upddataentrega.AV8Updated = false ;
         objprc_upddataentrega.context.SetSubmitInitialConfig(context);
         objprc_upddataentrega.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_upddataentrega);
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         aP2_Updated=this.AV8Updated;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_upddataentrega)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8Updated = true;
         /* Using cursor P006E2 */
         pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P006E2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P006E2_n1553ContagemResultado_CntSrvCod[0];
            A798ContagemResultado_PFBFSImp = P006E2_A798ContagemResultado_PFBFSImp[0];
            n798ContagemResultado_PFBFSImp = P006E2_n798ContagemResultado_PFBFSImp[0];
            A601ContagemResultado_Servico = P006E2_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P006E2_n601ContagemResultado_Servico[0];
            A490ContagemResultado_ContratadaCod = P006E2_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P006E2_n490ContagemResultado_ContratadaCod[0];
            A472ContagemResultado_DataEntrega = P006E2_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P006E2_n472ContagemResultado_DataEntrega[0];
            A601ContagemResultado_Servico = P006E2_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P006E2_n601ContagemResultado_Servico[0];
            /* Using cursor P006E3 */
            pr_default.execute(1, new Object[] {n601ContagemResultado_Servico, A601ContagemResultado_Servico});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A74Contrato_Codigo = P006E3_A74Contrato_Codigo[0];
               A155Servico_Codigo = P006E3_A155Servico_Codigo[0];
               A160ContratoServicos_Codigo = P006E3_A160ContratoServicos_Codigo[0];
               /* Using cursor P006E4 */
               pr_default.execute(2, new Object[] {A74Contrato_Codigo});
               A39Contratada_Codigo = P006E4_A39Contratada_Codigo[0];
               pr_default.close(2);
               if ( A39Contratada_Codigo == A490ContagemResultado_ContratadaCod )
               {
                  /* Using cursor P006E5 */
                  pr_default.execute(3, new Object[] {A160ContratoServicos_Codigo});
                  while ( (pr_default.getStatus(3) != 101) )
                  {
                     A903ContratoServicosPrazo_CntSrvCod = P006E5_A903ContratoServicosPrazo_CntSrvCod[0];
                     A904ContratoServicosPrazo_Tipo = P006E5_A904ContratoServicosPrazo_Tipo[0];
                     A905ContratoServicosPrazo_Dias = P006E5_A905ContratoServicosPrazo_Dias[0];
                     n905ContratoServicosPrazo_Dias = P006E5_n905ContratoServicosPrazo_Dias[0];
                     if ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "F") == 0 )
                     {
                        AV9Dias = A905ContratoServicosPrazo_Dias;
                     }
                     else if ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "V") == 0 )
                     {
                        /* Using cursor P006E6 */
                        pr_default.execute(4, new Object[] {A903ContratoServicosPrazo_CntSrvCod, A160ContratoServicos_Codigo});
                        while ( (pr_default.getStatus(4) != 101) )
                        {
                           A908ContratoServicosPrazoRegra_Fim = P006E6_A908ContratoServicosPrazoRegra_Fim[0];
                           A907ContratoServicosPrazoRegra_Inicio = P006E6_A907ContratoServicosPrazoRegra_Inicio[0];
                           A909ContratoServicosPrazoRegra_Dias = P006E6_A909ContratoServicosPrazoRegra_Dias[0];
                           A906ContratoServicosPrazoRegra_Sequencial = P006E6_A906ContratoServicosPrazoRegra_Sequencial[0];
                           if ( ( A798ContagemResultado_PFBFSImp >= A907ContratoServicosPrazoRegra_Inicio ) && ( A798ContagemResultado_PFBFSImp <= A908ContratoServicosPrazoRegra_Fim ) )
                           {
                              AV9Dias = A909ContratoServicosPrazoRegra_Dias;
                              /* Exit For each command. Update data (if necessary), close cursors & exit. */
                              if (true) break;
                           }
                           pr_default.readNext(4);
                        }
                        pr_default.close(4);
                     }
                     else if ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "S") == 0 )
                     {
                        AV8Updated = false;
                     }
                     pr_default.readNext(3);
                  }
                  pr_default.close(3);
               }
               pr_default.readNext(1);
            }
            pr_default.close(1);
            pr_default.close(2);
            if ( ! (0==AV9Dias) )
            {
               GXt_dtime1 = DateTimeUtil.ResetTime( AV10Data ) ;
               A472ContagemResultado_DataEntrega = DateTimeUtil.ResetTime(DateTimeUtil.TAdd( GXt_dtime1, 86400*(AV9Dias)));
               n472ContagemResultado_DataEntrega = false;
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P006E7 */
            pr_default.execute(5, new Object[] {n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, A456ContagemResultado_Codigo});
            pr_default.close(5);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            if (true) break;
            /* Using cursor P006E8 */
            pr_default.execute(6, new Object[] {n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, A456ContagemResultado_Codigo});
            pr_default.close(6);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P006E2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P006E2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P006E2_A456ContagemResultado_Codigo = new int[1] ;
         P006E2_A798ContagemResultado_PFBFSImp = new decimal[1] ;
         P006E2_n798ContagemResultado_PFBFSImp = new bool[] {false} ;
         P006E2_A601ContagemResultado_Servico = new int[1] ;
         P006E2_n601ContagemResultado_Servico = new bool[] {false} ;
         P006E2_A490ContagemResultado_ContratadaCod = new int[1] ;
         P006E2_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P006E2_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P006E2_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         P006E3_A74Contrato_Codigo = new int[1] ;
         P006E3_A155Servico_Codigo = new int[1] ;
         P006E3_A160ContratoServicos_Codigo = new int[1] ;
         P006E4_A39Contratada_Codigo = new int[1] ;
         P006E5_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         P006E5_A904ContratoServicosPrazo_Tipo = new String[] {""} ;
         P006E5_A905ContratoServicosPrazo_Dias = new short[1] ;
         P006E5_n905ContratoServicosPrazo_Dias = new bool[] {false} ;
         A904ContratoServicosPrazo_Tipo = "";
         P006E6_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         P006E6_A908ContratoServicosPrazoRegra_Fim = new decimal[1] ;
         P006E6_A907ContratoServicosPrazoRegra_Inicio = new decimal[1] ;
         P006E6_A909ContratoServicosPrazoRegra_Dias = new short[1] ;
         P006E6_A906ContratoServicosPrazoRegra_Sequencial = new short[1] ;
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_upddataentrega__default(),
            new Object[][] {
                new Object[] {
               P006E2_A1553ContagemResultado_CntSrvCod, P006E2_n1553ContagemResultado_CntSrvCod, P006E2_A456ContagemResultado_Codigo, P006E2_A798ContagemResultado_PFBFSImp, P006E2_n798ContagemResultado_PFBFSImp, P006E2_A601ContagemResultado_Servico, P006E2_n601ContagemResultado_Servico, P006E2_A490ContagemResultado_ContratadaCod, P006E2_n490ContagemResultado_ContratadaCod, P006E2_A472ContagemResultado_DataEntrega,
               P006E2_n472ContagemResultado_DataEntrega
               }
               , new Object[] {
               P006E3_A74Contrato_Codigo, P006E3_A155Servico_Codigo, P006E3_A160ContratoServicos_Codigo
               }
               , new Object[] {
               P006E4_A39Contratada_Codigo
               }
               , new Object[] {
               P006E5_A903ContratoServicosPrazo_CntSrvCod, P006E5_A904ContratoServicosPrazo_Tipo, P006E5_A905ContratoServicosPrazo_Dias, P006E5_n905ContratoServicosPrazo_Dias
               }
               , new Object[] {
               P006E6_A903ContratoServicosPrazo_CntSrvCod, P006E6_A908ContratoServicosPrazoRegra_Fim, P006E6_A907ContratoServicosPrazoRegra_Inicio, P006E6_A909ContratoServicosPrazoRegra_Dias, P006E6_A906ContratoServicosPrazoRegra_Sequencial
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A905ContratoServicosPrazo_Dias ;
      private short AV9Dias ;
      private short A909ContratoServicosPrazoRegra_Dias ;
      private short A906ContratoServicosPrazoRegra_Sequencial ;
      private int A456ContagemResultado_Codigo ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A601ContagemResultado_Servico ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A74Contrato_Codigo ;
      private int A155Servico_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int A39Contratada_Codigo ;
      private int A903ContratoServicosPrazo_CntSrvCod ;
      private decimal A798ContagemResultado_PFBFSImp ;
      private decimal A908ContratoServicosPrazoRegra_Fim ;
      private decimal A907ContratoServicosPrazoRegra_Inicio ;
      private String scmdbuf ;
      private String A904ContratoServicosPrazo_Tipo ;
      private DateTime GXt_dtime1 ;
      private DateTime AV10Data ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private bool AV8Updated ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n798ContagemResultado_PFBFSImp ;
      private bool n601ContagemResultado_Servico ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n905ContratoServicosPrazo_Dias ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P006E2_A1553ContagemResultado_CntSrvCod ;
      private bool[] P006E2_n1553ContagemResultado_CntSrvCod ;
      private int[] P006E2_A456ContagemResultado_Codigo ;
      private decimal[] P006E2_A798ContagemResultado_PFBFSImp ;
      private bool[] P006E2_n798ContagemResultado_PFBFSImp ;
      private int[] P006E2_A601ContagemResultado_Servico ;
      private bool[] P006E2_n601ContagemResultado_Servico ;
      private int[] P006E2_A490ContagemResultado_ContratadaCod ;
      private bool[] P006E2_n490ContagemResultado_ContratadaCod ;
      private DateTime[] P006E2_A472ContagemResultado_DataEntrega ;
      private bool[] P006E2_n472ContagemResultado_DataEntrega ;
      private int[] P006E3_A74Contrato_Codigo ;
      private int[] P006E3_A155Servico_Codigo ;
      private int[] P006E3_A160ContratoServicos_Codigo ;
      private int[] P006E4_A39Contratada_Codigo ;
      private int[] P006E5_A903ContratoServicosPrazo_CntSrvCod ;
      private String[] P006E5_A904ContratoServicosPrazo_Tipo ;
      private short[] P006E5_A905ContratoServicosPrazo_Dias ;
      private bool[] P006E5_n905ContratoServicosPrazo_Dias ;
      private int[] P006E6_A903ContratoServicosPrazo_CntSrvCod ;
      private decimal[] P006E6_A908ContratoServicosPrazoRegra_Fim ;
      private decimal[] P006E6_A907ContratoServicosPrazoRegra_Inicio ;
      private short[] P006E6_A909ContratoServicosPrazoRegra_Dias ;
      private short[] P006E6_A906ContratoServicosPrazoRegra_Sequencial ;
      private bool aP2_Updated ;
   }

   public class prc_upddataentrega__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new UpdateCursor(def[6])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP006E2 ;
          prmP006E2 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006E3 ;
          prmP006E3 = new Object[] {
          new Object[] {"@ContagemResultado_Servico",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006E4 ;
          prmP006E4 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006E5 ;
          prmP006E5 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006E6 ;
          prmP006E6 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006E7 ;
          prmP006E7 = new Object[] {
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006E8 ;
          prmP006E8 = new Object[] {
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P006E2", "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_PFBFSImp], T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_ContratadaCod], T1.[ContagemResultado_DataEntrega] FROM ([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) WHERE T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006E2,1,0,true,true )
             ,new CursorDef("P006E3", "SELECT [Contrato_Codigo], [Servico_Codigo], [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContagemResultado_Servico ORDER BY [Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006E3,100,0,true,false )
             ,new CursorDef("P006E4", "SELECT [Contratada_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006E4,1,0,true,false )
             ,new CursorDef("P006E5", "SELECT [ContratoServicosPrazo_CntSrvCod], [ContratoServicosPrazo_Tipo], [ContratoServicosPrazo_Dias] FROM [ContratoServicosPrazo] WITH (NOLOCK) WHERE @ContratoServicos_Codigo = @ContratoServicos_Codigo ORDER BY [ContratoServicosPrazo_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006E5,100,0,true,false )
             ,new CursorDef("P006E6", "SELECT [ContratoServicosPrazo_CntSrvCod], [ContratoServicosPrazoRegra_Fim], [ContratoServicosPrazoRegra_Inicio], [ContratoServicosPrazoRegra_Dias], [ContratoServicosPrazoRegra_Sequencial] FROM [ContratoServicosPrazoContratoServicosPrazoRegra] WITH (NOLOCK) WHERE ([ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod) AND (@ContratoServicos_Codigo = @ContratoServicos_Codigo) ORDER BY [ContratoServicosPrazo_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006E6,100,0,false,false )
             ,new CursorDef("P006E7", "UPDATE [ContagemResultado] SET [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP006E7)
             ,new CursorDef("P006E8", "UPDATE [ContagemResultado] SET [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP006E8)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(1, (DateTime)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(1, (DateTime)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
