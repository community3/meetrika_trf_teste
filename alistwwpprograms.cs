/*
               File: ListWWPPrograms
        Description: List Work With Plus Programs
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/7/2020 0:8:8.57
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class alistwwpprograms : GXProcedure
   {
      public alistwwpprograms( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public alistwwpprograms( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( out IGxCollection aP0_ProgramNames )
      {
         this.AV10ProgramNames = new GxObjectCollection( context, "ProgramNames.ProgramName", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtProgramNames_ProgramName", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP0_ProgramNames=this.AV10ProgramNames;
      }

      public IGxCollection executeUdp( )
      {
         this.AV10ProgramNames = new GxObjectCollection( context, "ProgramNames.ProgramName", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtProgramNames_ProgramName", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP0_ProgramNames=this.AV10ProgramNames;
         return AV10ProgramNames ;
      }

      public void executeSubmit( out IGxCollection aP0_ProgramNames )
      {
         alistwwpprograms objalistwwpprograms;
         objalistwwpprograms = new alistwwpprograms();
         objalistwwpprograms.AV10ProgramNames = new GxObjectCollection( context, "ProgramNames.ProgramName", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtProgramNames_ProgramName", "GeneXus.Programs") ;
         objalistwwpprograms.context.SetSubmitInitialConfig(context);
         objalistwwpprograms.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objalistwwpprograms);
         aP0_ProgramNames=this.AV10ProgramNames;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((alistwwpprograms)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV10ProgramNames = new GxObjectCollection( context, "ProgramNames.ProgramName", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtProgramNames_ProgramName", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         AV12name = "WWTipodeConta";
         AV13description = " Tipo de Contas";
         AV14link = "wwtipodeconta.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWSistemaVersao";
         AV13description = " Versionamento";
         AV14link = "wwsistemaversao.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContratoServicosTelas";
         AV13description = " Contrato Servicos Telas";
         AV14link = "wwcontratoservicostelas.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContratoServicosUnidConversao";
         AV13description = "Convers�o USC";
         AV14link = "wwcontratoservicosunidconversao.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWFuncoesAPFAtributos";
         AV13description = " Atributos das Fun��es de APF";
         AV14link = "wwfuncoesapfatributos.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWStatus";
         AV13description = " Status";
         AV14link = "wwstatus.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWUsuario";
         AV13description = "Usu�rio";
         AV14link = "wwusuario.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WP_Grid";
         AV13description = " Resultado das Contagens";
         AV14link = "wp_grid.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContratoDadosCertame";
         AV13description = " Contrato Dados Certame";
         AV14link = "wwcontratodadoscertame.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWGuia";
         AV13description = " Guia";
         AV14link = "wwguia.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWMetodologiaFases";
         AV13description = " Metodologia Fases";
         AV14link = "wwmetodologiafases.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWServicoGrupo";
         AV13description = " Grupo de Servi�o";
         AV14link = "wwservicogrupo.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContratoClausulas";
         AV13description = " Contrato Clausulas";
         AV14link = "wwcontratoclausulas.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContratoUnidades";
         AV13description = " Unidades Contratadas";
         AV14link = "wwcontratounidades.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWIndicador";
         AV13description = " Indicador";
         AV14link = "wwindicador.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWFuncaoDados";
         AV13description = " Grupo L�gico de Dados";
         AV14link = "wwfuncaodados.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWReferenciaTecnica";
         AV13description = " Refer�ncia T�cnica";
         AV14link = "wwreferenciatecnica.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContagemResultadoIncidentes";
         AV13description = " Contagem Resultado Incidentes";
         AV14link = "wwcontagemresultadoincidentes.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWUsuarioNotifica";
         AV13description = " Usuario Notifica";
         AV14link = "wwusuarionotifica.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWFuncaoAPFEvidencia";
         AV13description = " Evidencia da Funcao de Transa��o";
         AV14link = "wwfuncaoapfevidencia.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWCatalogoSutentacao";
         AV13description = " Catalogo Sutentacao";
         AV14link = "wwcatalogosutentacao.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContratanteUsuario";
         AV13description = " Contratante Usuario";
         AV14link = "wwcontratanteusuario.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWCheckList";
         AV13description = " Check List";
         AV14link = "wwchecklist.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWServicoAns";
         AV13description = " Servico Ans";
         AV14link = "wwservicoans.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWTipoProjeto";
         AV13description = " Tipos de Projeto";
         AV14link = "wwtipoprojeto.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWItemNaoMensuravel";
         AV13description = " Item N�o Mensuravel";
         AV14link = "wwitemnaomensuravel.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWBanco";
         AV13description = " Banco";
         AV14link = "wwbanco.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContratoOcorrencia";
         AV13description = " Contrato Ocorr�ncia";
         AV14link = "wwcontratoocorrencia.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContratoServicosVnc";
         AV13description = " Regra de Vinculo entre Servi�os";
         AV14link = "wwcontratoservicosvnc.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWGeral_Tp_UO";
         AV13description = " Tipo Unidade Organizacional";
         AV14link = "wwgeral_tp_uo.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWServicoFluxo";
         AV13description = " Sequ�ncia";
         AV14link = "wwservicofluxo.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWTipoRequisito";
         AV13description = " Tipos de Requisitos";
         AV14link = "wwtiporequisito.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContratoServicosPrazo";
         AV13description = " Prazo";
         AV14link = "wwcontratoservicosprazo.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContratoGarantia";
         AV13description = " Contrato Garantia";
         AV14link = "wwcontratogarantia.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWFuncaoUsuario";
         AV13description = " Fun��es de Usu�rio";
         AV14link = "wwfuncaousuario.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContratoArquivosAnexos";
         AV13description = " Contrato Arquivos Anexos";
         AV14link = "wwcontratoarquivosanexos.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWCaixa";
         AV13description = " Fluxo de Caixa";
         AV14link = "wwcaixa.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContratoObrigacao";
         AV13description = " Contrato Obrigacao";
         AV14link = "wwcontratoobrigacao.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWFeriados";
         AV13description = " Feriados";
         AV14link = "wwferiados.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWBancoAgencia";
         AV13description = " Banco Agencia";
         AV14link = "wwbancoagencia.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWGlosario";
         AV13description = " Glosario";
         AV14link = "wwglosario.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWModulo";
         AV13description = " Modulo";
         AV14link = "wwmodulo.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWPessoaCertificado";
         AV13description = " Certificados";
         AV14link = "wwpessoacertificado.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWGeral_Grupo_Funcao";
         AV13description = " Grupo de Func�es";
         AV14link = "wwgeral_grupo_funcao.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWSaldoContrato";
         AV13description = " Saldo Contrato";
         AV14link = "wwsaldocontrato.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWSolicitacaoMudancaItem";
         AV13description = " Item da Mudan�a";
         AV14link = "wwsolicitacaomudancaitem.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContratoServicosIndicador";
         AV13description = " Indicador";
         AV14link = "wwcontratoservicosindicador.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContagem";
         AV13description = " Contagem ";
         AV14link = "wwcontagem.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWParametrosPlanilhas";
         AV13description = " Par�metros das Planilhas";
         AV14link = "wwparametrosplanilhas.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWSistema";
         AV13description = " Sistema";
         AV14link = "wwsistema.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWArtefatos";
         AV13description = " Artefatos";
         AV14link = "wwartefatos.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContagemResultadoNotificacao";
         AV13description = " Notifica��es da Contagem";
         AV14link = "wwcontagemresultadonotificacao.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContratoTermoAditivo";
         AV13description = " Contrato Termo Aditivo";
         AV14link = "wwcontratotermoaditivo.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWRegrasContagem";
         AV13description = " Regras de Contagem";
         AV14link = "wwregrascontagem.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWParametrosSistema";
         AV13description = " Parametros Sistema";
         AV14link = "wwparametrossistema.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWPadroesArtefatos";
         AV13description = " Padroes Artefatos";
         AV14link = "wwpadroesartefatos.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWServico";
         AV13description = " Servico";
         AV14link = "wwservico.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWLote";
         AV13description = " Lote";
         AV14link = "wwlote.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "ExtraWWLoteAPagar";
         AV13description = " Lotes a Pagar";
         AV14link = "extrawwloteapagar.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "ExtraWWLoteNotasFiscais";
         AV13description = " Notas Fiscais";
         AV14link = "extrawwlotenotasfiscais.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWMenu";
         AV13description = " Menu";
         AV14link = "wwmenu.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWSolicitacaoMudanca";
         AV13description = " Solicita��o de Mudan�a";
         AV14link = "wwsolicitacaomudanca.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWUnidadeMedicao";
         AV13description = " Unidades de Medi��o";
         AV14link = "wwunidademedicao.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWEquipe";
         AV13description = " Equipe";
         AV14link = "wwequipe.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWPessoa";
         AV13description = " Pessoas";
         AV14link = "wwpessoa.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWFatoresImpacto";
         AV13description = " Configura��o de Fatores de Impacto na Contagem";
         AV14link = "wwfatoresimpacto.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContagemItemParecer";
         AV13description = " Contagem Item Parecer";
         AV14link = "wwcontagemitemparecer.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWVariaveisCocomo";
         AV13description = " Variaveis Cocomo";
         AV14link = "wwvariaveiscocomo.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWGeral_Funcao";
         AV13description = " Funcao";
         AV14link = "wwgeral_funcao.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWLoteArquivoAnexo";
         AV13description = " Arquivos Anexos";
         AV14link = "wwlotearquivoanexo.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContratoServicosPrioridade";
         AV13description = " Prioridade";
         AV14link = "wwcontratoservicosprioridade.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWReferenciaINM";
         AV13description = " Referencia INM";
         AV14link = "wwreferenciainm.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContagemResultado";
         AV13description = "Demandas";
         AV14link = "wwcontagemresultado.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "ExtraWWContagemResultadoExtraSelection";
         AV13description = "Demandas Financeiro";
         AV14link = "extrawwcontagemresultadoextraselection.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "ExtraWWContagemResultadoWWAceiteFaturamento";
         AV13description = "Demandas Faturadas";
         AV14link = "extrawwcontagemresultadowwaceitefaturamento.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "ExtraWWContagemResultadoOS";
         AV13description = "Servi�os Vinculados";
         AV14link = "extrawwcontagemresultadoos.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "ExtraWWContagemResultadoGerencial";
         AV13description = "Gerencial";
         AV14link = "extrawwcontagemresultadogerencial.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "ExtraWWContagemResultadoPgtoFnc";
         AV13description = "Pagamento Funcion�rios";
         AV14link = "extrawwcontagemresultadopgtofnc.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "ExtraWWContagemResultadoEmGarantia";
         AV13description = "Demandas em Garant�a";
         AV14link = "extrawwcontagemresultadoemgarantia.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWHistoricoConsumo";
         AV13description = " Historico Consumo";
         AV14link = "wwhistoricoconsumo.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContratoServicos";
         AV13description = "Contrato Servi�os";
         AV14link = "wwcontratoservicos.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContratante";
         AV13description = " Contratante";
         AV14link = "wwcontratante.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWAtributos";
         AV13description = " Atributos";
         AV14link = "wwatributos.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContagemResultadoEvidencia";
         AV13description = " Contagem Resultado Evidencias";
         AV14link = "wwcontagemresultadoevidencia.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWCheck";
         AV13description = " Controle";
         AV14link = "wwcheck.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWNaoConformidade";
         AV13description = " N�o Conformidade";
         AV14link = "wwnaoconformidade.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContratoOcorrenciaNotificacao";
         AV13description = " Contrato Ocorrencia Notificacao";
         AV14link = "wwcontratoocorrencianotificacao.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWEmail";
         AV13description = " Email";
         AV14link = "wwemail.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWMunicipio";
         AV13description = " Municipio";
         AV14link = "wwmunicipio.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContratada";
         AV13description = " Contratadas";
         AV14link = "wwcontratada.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWAudit";
         AV13description = " Audit";
         AV14link = "wwaudit.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContratoCaixaEntrada";
         AV13description = " Configura��es da Caixa de Entrada";
         AV14link = "wwcontratocaixaentrada.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWAgendaAtendimento";
         AV13description = " Agenda Atendimento";
         AV14link = "wwagendaatendimento.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContrato";
         AV13description = " Contrato";
         AV14link = "wwcontrato.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWSolicitacoes";
         AV13description = " Solicitacoes";
         AV14link = "wwsolicitacoes.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWOrganizacao";
         AV13description = " Organiza��o";
         AV14link = "wworganizacao.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWTecnologia";
         AV13description = " Tecnologia";
         AV14link = "wwtecnologia.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWGeral_Cargo";
         AV13description = " Cargos";
         AV14link = "wwgeral_cargo.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWPais";
         AV13description = " Pais";
         AV14link = "wwpais.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContratadaUsuario";
         AV13description = " Contratada Usuario";
         AV14link = "wwcontratadausuario.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWGrupoObjetoControle";
         AV13description = " Grupos de Objetos de Controle";
         AV14link = "wwgrupoobjetocontrole.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWAmbienteTecnologico";
         AV13description = " Ambiente Tecnologico";
         AV14link = "wwambientetecnologico.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWGeral_UnidadeOrganizacional";
         AV13description = " Unidade Organizacional";
         AV14link = "wwgeral_unidadeorganizacional.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContagemResultadoExecucao";
         AV13description = " Contagem Resultado Execucao";
         AV14link = "wwcontagemresultadoexecucao.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWProjeto";
         AV13description = " Projeto";
         AV14link = "wwprojeto.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWAlerta";
         AV13description = " Alertas";
         AV14link = "wwalerta.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWMetodologia";
         AV13description = " Metodologia";
         AV14link = "wwmetodologia.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWPerfil";
         AV13description = "Perfi(s)";
         AV14link = "wwperfil.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWNotaEmpenho";
         AV13description = " Nota Empenho";
         AV14link = "wwnotaempenho.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWServicoPrioridade";
         AV13description = " Prioridades padr�o";
         AV14link = "wwservicoprioridade.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWFuncaoAPF";
         AV13description = " Fun��es de Transa��";
         AV14link = "wwfuncaoapf.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WP_RelatorioComparacaoDemandasModelo";
         AV13description = " Resultado das Contagens";
         AV14link = "wp_relatoriocomparacaodemandasmodelo.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWTipoDocumento";
         AV13description = " Tipo de Documentos";
         AV14link = "wwtipodocumento.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWAreaTrabalho";
         AV13description = " �rea de Trabalho";
         AV14link = "wwareatrabalho.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWLinhaNegocio";
         AV13description = " Linhas de Neg�cio";
         AV14link = "wwlinhanegocio.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContratoServicosCusto";
         AV13description = " Custo do Servi�o";
         AV14link = "wwcontratoservicoscusto.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WP_CaixaDmn";
         AV13description = " Resultado das Contagens";
         AV14link = "wp_caixadmn.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWAutorizacaoConsumo";
         AV13description = " Autoriza��o de Consumo";
         AV14link = "wwautorizacaoconsumo.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWRequisito";
         AV13description = " Requisito";
         AV14link = "wwrequisito.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWGeral_Grupo_Cargo";
         AV13description = " Grupo de Cargos";
         AV14link = "wwgeral_grupo_cargo.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWTabela";
         AV13description = " Tabela";
         AV14link = "wwtabela.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWEstado";
         AV13description = " Estado";
         AV14link = "wwestado.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWContagemResultadoContagens";
         AV13description = " Contagens";
         AV14link = "wwcontagemresultadocontagens.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV12name = "WWUsuarioPerfil";
         AV13description = " Usuario x Perfil";
         AV14link = "wwusuarioperfil.aspx";
         /* Execute user subroutine: 'ADDPROGRAM' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV14link = "gamhome.aspx";
         AV14link = "wp_novoperfil.aspx";
         AV14link = "wp_novousuario.aspx";
         AV14link = "wp_novolicensiado.aspx";
         AV14link = "wp_permissaopapeis.aspx";
         AV14link = "wp_selecaopermissaoperfil.aspx";
         AV14link = "wp_alterarminhasenha.aspx";
         AV14link = "wp_importfromfile.aspx";
         AV14link = "wp_contagemresultadoretorno.aspx";
         AV14link = "wp_consultageral.aspx";
         AV14link = "wp_execucaocontratual.aspx";
         AV14link = "wp_compromissosassumidos.aspx";
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'ADDPROGRAM' Routine */
         AV8IsAuthorized = true;
         if ( AV8IsAuthorized )
         {
            AV11ProgramName = new wwpbaseobjects.SdtProgramNames_ProgramName(context);
            AV11ProgramName.gxTpr_Name = AV12name;
            AV11ProgramName.gxTpr_Description = AV13description;
            AV11ProgramName.gxTpr_Link = AV14link;
            AV10ProgramNames.Add(AV11ProgramName, 0);
         }
         context.wjLoc = "wp_lote.aspx";
         context.wjLocDisableFrm = 1;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV12name = "";
         AV13description = "";
         AV14link = "";
         AV11ProgramName = new wwpbaseobjects.SdtProgramNames_ProgramName(context);
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private bool returnInSub ;
      private bool AV8IsAuthorized ;
      private String AV12name ;
      private String AV13description ;
      private String AV14link ;
      private IGxCollection aP0_ProgramNames ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtProgramNames_ProgramName ))]
      private IGxCollection AV10ProgramNames ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtProgramNames_ProgramName AV11ProgramName ;
   }

}
