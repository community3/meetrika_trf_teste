/*
               File: ContagemResultadoGeneral
        Description: Contagem Resultado General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/17/2020 1:58:58.75
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadogeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contagemresultadogeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contagemresultadogeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         dynContagemResultado_Owner = new GXCombobox();
         cmbContagemResultado_StatusDmn = new GXCombobox();
         lstavPagocolaborador = new GXListbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A456ContagemResultado_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"CONTAGEMRESULTADO_OWNER") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLACONTAGEMRESULTADO_OWNERB52( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAB52( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV52Pgmname = "ContagemResultadoGeneral";
               context.Gx_err = 0;
               edtavContratoservicosunidconversao_sigla_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratoservicosunidconversao_sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosunidconversao_sigla_Enabled), 5, 0)));
               edtavSistema_gestor_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSistema_gestor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_gestor_Enabled), 5, 0)));
               edtavResponsavel_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavResponsavel_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavResponsavel_Enabled), 5, 0)));
               edtavContagemresultado_owneremail_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultado_owneremail_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_owneremail_Enabled), 5, 0)));
               edtavContagemresultado_ownertelefone_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultado_ownertelefone_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_ownertelefone_Enabled), 5, 0)));
               GXACONTAGEMRESULTADO_OWNER_htmlB52( ) ;
               /* Using cursor H00B52 */
               pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
               A454ContagemResultado_ContadorFSCod = H00B52_A454ContagemResultado_ContadorFSCod[0];
               n454ContagemResultado_ContadorFSCod = H00B52_n454ContagemResultado_ContadorFSCod[0];
               A468ContagemResultado_NaoCnfDmnCod = H00B52_A468ContagemResultado_NaoCnfDmnCod[0];
               n468ContagemResultado_NaoCnfDmnCod = H00B52_n468ContagemResultado_NaoCnfDmnCod[0];
               A146Modulo_Codigo = H00B52_A146Modulo_Codigo[0];
               n146Modulo_Codigo = H00B52_n146Modulo_Codigo[0];
               A805ContagemResultado_ContratadaOrigemCod = H00B52_A805ContagemResultado_ContratadaOrigemCod[0];
               n805ContagemResultado_ContratadaOrigemCod = H00B52_n805ContagemResultado_ContratadaOrigemCod[0];
               A1043ContagemResultado_LiqLogCod = H00B52_A1043ContagemResultado_LiqLogCod[0];
               n1043ContagemResultado_LiqLogCod = H00B52_n1043ContagemResultado_LiqLogCod[0];
               A1553ContagemResultado_CntSrvCod = H00B52_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00B52_n1553ContagemResultado_CntSrvCod[0];
               A490ContagemResultado_ContratadaCod = H00B52_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00B52_n490ContagemResultado_ContratadaCod[0];
               A597ContagemResultado_LoteAceiteCod = H00B52_A597ContagemResultado_LoteAceiteCod[0];
               n597ContagemResultado_LoteAceiteCod = H00B52_n597ContagemResultado_LoteAceiteCod[0];
               A602ContagemResultado_OSVinculada = H00B52_A602ContagemResultado_OSVinculada[0];
               n602ContagemResultado_OSVinculada = H00B52_n602ContagemResultado_OSVinculada[0];
               A457ContagemResultado_Demanda = H00B52_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = H00B52_n457ContagemResultado_Demanda[0];
               A489ContagemResultado_SistemaCod = H00B52_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = H00B52_n489ContagemResultado_SistemaCod[0];
               A484ContagemResultado_StatusDmn = H00B52_A484ContagemResultado_StatusDmn[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
               n484ContagemResultado_StatusDmn = H00B52_n484ContagemResultado_StatusDmn[0];
               A912ContagemResultado_HoraEntrega = H00B52_A912ContagemResultado_HoraEntrega[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A912ContagemResultado_HoraEntrega", context.localUtil.TToC( A912ContagemResultado_HoraEntrega, 0, 5, 0, 3, "/", ":", " "));
               n912ContagemResultado_HoraEntrega = H00B52_n912ContagemResultado_HoraEntrega[0];
               A472ContagemResultado_DataEntrega = H00B52_A472ContagemResultado_DataEntrega[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A472ContagemResultado_DataEntrega", context.localUtil.Format(A472ContagemResultado_DataEntrega, "99/99/99"));
               n472ContagemResultado_DataEntrega = H00B52_n472ContagemResultado_DataEntrega[0];
               A1350ContagemResultado_DataCadastro = H00B52_A1350ContagemResultado_DataCadastro[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1350ContagemResultado_DataCadastro", context.localUtil.TToC( A1350ContagemResultado_DataCadastro, 8, 5, 0, 3, "/", ":", " "));
               n1350ContagemResultado_DataCadastro = H00B52_n1350ContagemResultado_DataCadastro[0];
               A508ContagemResultado_Owner = H00B52_A508ContagemResultado_Owner[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A508ContagemResultado_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A508ContagemResultado_Owner), 6, 0)));
               A799ContagemResultado_PFLFSImp = H00B52_A799ContagemResultado_PFLFSImp[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A799ContagemResultado_PFLFSImp", StringUtil.LTrim( StringUtil.Str( A799ContagemResultado_PFLFSImp, 14, 5)));
               n799ContagemResultado_PFLFSImp = H00B52_n799ContagemResultado_PFLFSImp[0];
               A798ContagemResultado_PFBFSImp = H00B52_A798ContagemResultado_PFBFSImp[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A798ContagemResultado_PFBFSImp", StringUtil.LTrim( StringUtil.Str( A798ContagemResultado_PFBFSImp, 14, 5)));
               n798ContagemResultado_PFBFSImp = H00B52_n798ContagemResultado_PFBFSImp[0];
               A1046ContagemResultado_Agrupador = H00B52_A1046ContagemResultado_Agrupador[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1046ContagemResultado_Agrupador", A1046ContagemResultado_Agrupador);
               n1046ContagemResultado_Agrupador = H00B52_n1046ContagemResultado_Agrupador[0];
               A465ContagemResultado_Link = H00B52_A465ContagemResultado_Link[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A465ContagemResultado_Link", A465ContagemResultado_Link);
               n465ContagemResultado_Link = H00B52_n465ContagemResultado_Link[0];
               A1044ContagemResultado_FncUsrCod = H00B52_A1044ContagemResultado_FncUsrCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1044ContagemResultado_FncUsrCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1044ContagemResultado_FncUsrCod), 6, 0)));
               n1044ContagemResultado_FncUsrCod = H00B52_n1044ContagemResultado_FncUsrCod[0];
               A2133ContagemResultado_QuantidadeSolicitada = H00B52_A2133ContagemResultado_QuantidadeSolicitada[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2133ContagemResultado_QuantidadeSolicitada", StringUtil.LTrim( StringUtil.Str( A2133ContagemResultado_QuantidadeSolicitada, 9, 4)));
               n2133ContagemResultado_QuantidadeSolicitada = H00B52_n2133ContagemResultado_QuantidadeSolicitada[0];
               A1452ContagemResultado_SS = H00B52_A1452ContagemResultado_SS[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1452ContagemResultado_SS", StringUtil.LTrim( StringUtil.Str( (decimal)(A1452ContagemResultado_SS), 8, 0)));
               n1452ContagemResultado_SS = H00B52_n1452ContagemResultado_SS[0];
               A471ContagemResultado_DataDmn = H00B52_A471ContagemResultado_DataDmn[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A471ContagemResultado_DataDmn", context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"));
               A1443ContagemResultado_CntSrvPrrCod = H00B52_A1443ContagemResultado_CntSrvPrrCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1443ContagemResultado_CntSrvPrrCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1443ContagemResultado_CntSrvPrrCod), 6, 0)));
               n1443ContagemResultado_CntSrvPrrCod = H00B52_n1443ContagemResultado_CntSrvPrrCod[0];
               A890ContagemResultado_Responsavel = H00B52_A890ContagemResultado_Responsavel[0];
               n890ContagemResultado_Responsavel = H00B52_n890ContagemResultado_Responsavel[0];
               pr_default.close(0);
               /* Using cursor H00B53 */
               pr_default.execute(1, new Object[] {n454ContagemResultado_ContadorFSCod, A454ContagemResultado_ContadorFSCod});
               A480ContagemResultado_CrFSPessoaCod = H00B53_A480ContagemResultado_CrFSPessoaCod[0];
               n480ContagemResultado_CrFSPessoaCod = H00B53_n480ContagemResultado_CrFSPessoaCod[0];
               pr_default.close(1);
               /* Using cursor H00B54 */
               pr_default.execute(2, new Object[] {n480ContagemResultado_CrFSPessoaCod, A480ContagemResultado_CrFSPessoaCod});
               A455ContagemResultado_ContadorFSNom = H00B54_A455ContagemResultado_ContadorFSNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A455ContagemResultado_ContadorFSNom", A455ContagemResultado_ContadorFSNom);
               n455ContagemResultado_ContadorFSNom = H00B54_n455ContagemResultado_ContadorFSNom[0];
               pr_default.close(2);
               /* Using cursor H00B55 */
               pr_default.execute(3, new Object[] {n468ContagemResultado_NaoCnfDmnCod, A468ContagemResultado_NaoCnfDmnCod});
               A477ContagemResultado_NaoCnfDmnNom = H00B55_A477ContagemResultado_NaoCnfDmnNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A477ContagemResultado_NaoCnfDmnNom", A477ContagemResultado_NaoCnfDmnNom);
               n477ContagemResultado_NaoCnfDmnNom = H00B55_n477ContagemResultado_NaoCnfDmnNom[0];
               pr_default.close(3);
               /* Using cursor H00B56 */
               pr_default.execute(4, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
               A143Modulo_Nome = H00B56_A143Modulo_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A143Modulo_Nome", A143Modulo_Nome);
               pr_default.close(4);
               /* Using cursor H00B57 */
               pr_default.execute(5, new Object[] {n805ContagemResultado_ContratadaOrigemCod, A805ContagemResultado_ContratadaOrigemCod});
               A807ContagemResultado_ContratadaOrigemPesCod = H00B57_A807ContagemResultado_ContratadaOrigemPesCod[0];
               n807ContagemResultado_ContratadaOrigemPesCod = H00B57_n807ContagemResultado_ContratadaOrigemPesCod[0];
               pr_default.close(5);
               /* Using cursor H00B58 */
               pr_default.execute(6, new Object[] {n807ContagemResultado_ContratadaOrigemPesCod, A807ContagemResultado_ContratadaOrigemPesCod});
               A808ContagemResultado_ContratadaOrigemPesNom = H00B58_A808ContagemResultado_ContratadaOrigemPesNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A808ContagemResultado_ContratadaOrigemPesNom", A808ContagemResultado_ContratadaOrigemPesNom);
               n808ContagemResultado_ContratadaOrigemPesNom = H00B58_n808ContagemResultado_ContratadaOrigemPesNom[0];
               pr_default.close(6);
               /* Using cursor H00B59 */
               pr_default.execute(7, new Object[] {n1043ContagemResultado_LiqLogCod, A1043ContagemResultado_LiqLogCod});
               A1034ContagemResultadoLiqLog_Data = H00B59_A1034ContagemResultadoLiqLog_Data[0];
               n1034ContagemResultadoLiqLog_Data = H00B59_n1034ContagemResultadoLiqLog_Data[0];
               pr_default.close(7);
               /* Using cursor H00B510 */
               pr_default.execute(8, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
               A601ContagemResultado_Servico = H00B510_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = H00B510_n601ContagemResultado_Servico[0];
               A1620ContagemResultado_CntSrvUndCnt = H00B510_A1620ContagemResultado_CntSrvUndCnt[0];
               n1620ContagemResultado_CntSrvUndCnt = H00B510_n1620ContagemResultado_CntSrvUndCnt[0];
               A2094ContratoServicos_SolicitaGestorSistema = H00B510_A2094ContratoServicos_SolicitaGestorSistema[0];
               n2094ContratoServicos_SolicitaGestorSistema = H00B510_n2094ContratoServicos_SolicitaGestorSistema[0];
               A1613ContagemResultado_CntSrvQtdUntCns = H00B510_A1613ContagemResultado_CntSrvQtdUntCns[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1613ContagemResultado_CntSrvQtdUntCns", StringUtil.LTrim( StringUtil.Str( A1613ContagemResultado_CntSrvQtdUntCns, 9, 4)));
               n1613ContagemResultado_CntSrvQtdUntCns = H00B510_n1613ContagemResultado_CntSrvQtdUntCns[0];
               pr_default.close(8);
               /* Using cursor H00B511 */
               pr_default.execute(9, new Object[] {n601ContagemResultado_Servico, A601ContagemResultado_Servico});
               A801ContagemResultado_ServicoSigla = H00B511_A801ContagemResultado_ServicoSigla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A801ContagemResultado_ServicoSigla", A801ContagemResultado_ServicoSigla);
               n801ContagemResultado_ServicoSigla = H00B511_n801ContagemResultado_ServicoSigla[0];
               pr_default.close(9);
               /* Using cursor H00B512 */
               pr_default.execute(10, new Object[] {n1620ContagemResultado_CntSrvUndCnt, A1620ContagemResultado_CntSrvUndCnt});
               A2209ContagemResultado_CntSrvUndCntNome = H00B512_A2209ContagemResultado_CntSrvUndCntNome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2209ContagemResultado_CntSrvUndCntNome", A2209ContagemResultado_CntSrvUndCntNome);
               n2209ContagemResultado_CntSrvUndCntNome = H00B512_n2209ContagemResultado_CntSrvUndCntNome[0];
               pr_default.close(10);
               /* Using cursor H00B513 */
               pr_default.execute(11, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
               A499ContagemResultado_ContratadaPessoaCod = H00B513_A499ContagemResultado_ContratadaPessoaCod[0];
               n499ContagemResultado_ContratadaPessoaCod = H00B513_n499ContagemResultado_ContratadaPessoaCod[0];
               A52Contratada_AreaTrabalhoCod = H00B513_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = H00B513_n52Contratada_AreaTrabalhoCod[0];
               pr_default.close(11);
               /* Using cursor H00B514 */
               pr_default.execute(12, new Object[] {n499ContagemResultado_ContratadaPessoaCod, A499ContagemResultado_ContratadaPessoaCod});
               A500ContagemResultado_ContratadaPessoaNom = H00B514_A500ContagemResultado_ContratadaPessoaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A500ContagemResultado_ContratadaPessoaNom", A500ContagemResultado_ContratadaPessoaNom);
               n500ContagemResultado_ContratadaPessoaNom = H00B514_n500ContagemResultado_ContratadaPessoaNom[0];
               pr_default.close(12);
               /* Using cursor H00B515 */
               pr_default.execute(13, new Object[] {n597ContagemResultado_LoteAceiteCod, A597ContagemResultado_LoteAceiteCod});
               A1547ContagemResultado_LoteNFe = H00B515_A1547ContagemResultado_LoteNFe[0];
               n1547ContagemResultado_LoteNFe = H00B515_n1547ContagemResultado_LoteNFe[0];
               A525ContagemResultado_AceiteUserCod = H00B515_A525ContagemResultado_AceiteUserCod[0];
               n525ContagemResultado_AceiteUserCod = H00B515_n525ContagemResultado_AceiteUserCod[0];
               A529ContagemResultado_DataAceite = H00B515_A529ContagemResultado_DataAceite[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A529ContagemResultado_DataAceite", context.localUtil.TToC( A529ContagemResultado_DataAceite, 8, 5, 0, 3, "/", ":", " "));
               n529ContagemResultado_DataAceite = H00B515_n529ContagemResultado_DataAceite[0];
               A563Lote_Nome = H00B515_A563Lote_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A563Lote_Nome", A563Lote_Nome);
               n563Lote_Nome = H00B515_n563Lote_Nome[0];
               A528ContagemResultado_LoteAceite = H00B515_A528ContagemResultado_LoteAceite[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A528ContagemResultado_LoteAceite", A528ContagemResultado_LoteAceite);
               n528ContagemResultado_LoteAceite = H00B515_n528ContagemResultado_LoteAceite[0];
               pr_default.close(13);
               /* Using cursor H00B516 */
               pr_default.execute(14, new Object[] {n525ContagemResultado_AceiteUserCod, A525ContagemResultado_AceiteUserCod});
               A526ContagemResultado_AceitePessoaCod = H00B516_A526ContagemResultado_AceitePessoaCod[0];
               n526ContagemResultado_AceitePessoaCod = H00B516_n526ContagemResultado_AceitePessoaCod[0];
               pr_default.close(14);
               /* Using cursor H00B517 */
               pr_default.execute(15, new Object[] {n526ContagemResultado_AceitePessoaCod, A526ContagemResultado_AceitePessoaCod});
               A527ContagemResultado_AceiteUserNom = H00B517_A527ContagemResultado_AceiteUserNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A527ContagemResultado_AceiteUserNom", A527ContagemResultado_AceiteUserNom);
               n527ContagemResultado_AceiteUserNom = H00B517_n527ContagemResultado_AceiteUserNom[0];
               pr_default.close(15);
               /* Using cursor H00B518 */
               pr_default.execute(16, new Object[] {n489ContagemResultado_SistemaCod, A489ContagemResultado_SistemaCod});
               A515ContagemResultado_SistemaCoord = H00B518_A515ContagemResultado_SistemaCoord[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A515ContagemResultado_SistemaCoord", A515ContagemResultado_SistemaCoord);
               n515ContagemResultado_SistemaCoord = H00B518_n515ContagemResultado_SistemaCoord[0];
               A509ContagemrResultado_SistemaSigla = H00B518_A509ContagemrResultado_SistemaSigla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A509ContagemrResultado_SistemaSigla", A509ContagemrResultado_SistemaSigla);
               n509ContagemrResultado_SistemaSigla = H00B518_n509ContagemrResultado_SistemaSigla[0];
               pr_default.close(16);
               GXt_char1 = A2091ContagemResultado_CntSrvPrrNome;
               new prc_buscaprioridadeos(context ).execute(  A1443ContagemResultado_CntSrvPrrCod, out  GXt_char1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1443ContagemResultado_CntSrvPrrCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1443ContagemResultado_CntSrvPrrCod), 6, 0)));
               A2091ContagemResultado_CntSrvPrrNome = GXt_char1;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2091ContagemResultado_CntSrvPrrNome", A2091ContagemResultado_CntSrvPrrNome);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADO_CNTSRVPRRNOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2091ContagemResultado_CntSrvPrrNome, "@!"))));
               /* Using cursor H00B519 */
               pr_default.execute(17, new Object[] {n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel});
               A1312ContagemResultado_ResponsavelPessCod = H00B519_A1312ContagemResultado_ResponsavelPessCod[0];
               n1312ContagemResultado_ResponsavelPessCod = H00B519_n1312ContagemResultado_ResponsavelPessCod[0];
               pr_default.close(17);
               /* Using cursor H00B520 */
               pr_default.execute(18, new Object[] {n1312ContagemResultado_ResponsavelPessCod, A1312ContagemResultado_ResponsavelPessCod});
               A1313ContagemResultado_ResponsavelPessNome = H00B520_A1313ContagemResultado_ResponsavelPessNome[0];
               n1313ContagemResultado_ResponsavelPessNome = H00B520_n1313ContagemResultado_ResponsavelPessNome[0];
               pr_default.close(18);
               GXt_int2 = A1229ContagemResultado_ContratadaDoResponsavel;
               new prc_contratadadousuario(context ).execute( ref  A890ContagemResultado_Responsavel, ref  A52Contratada_AreaTrabalhoCod, out  GXt_int2) ;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A890ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A890ContagemResultado_Responsavel), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
               A1229ContagemResultado_ContratadaDoResponsavel = GXt_int2;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1229ContagemResultado_ContratadaDoResponsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1229ContagemResultado_ContratadaDoResponsavel), 6, 0)));
               WSB52( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contagem Resultado General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202061715928");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemresultadogeneral.aspx") + "?" + UrlEncode("" +A456ContagemResultado_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCALLER", StringUtil.RTrim( AV18Caller));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_DEMANDA", A457ContagemResultado_Demanda);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vISAGENDAOK", AV24isAgendaOK);
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"CONTRATOSERVICOS_SOLICITAGESTORSISTEMA", A2094ContratoServicos_SolicitaGestorSistema);
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A489ContagemResultado_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSUNIDCONVERSAO_SIGLA", StringUtil.RTrim( A2112ContratoServicosUnidConversao_Sigla));
         GxWebStd.gx_hidden_field( context, sPrefix+"USUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTAGEMRESULTADO_OWNER", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37ContagemResultado_Owner), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"USUARIO_PESSOATELEFONE", StringUtil.RTrim( A2095Usuario_PessoaTelefone));
         GxWebStd.gx_hidden_field( context, sPrefix+"USUARIO_EMAIL", A1647Usuario_Email);
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_RESPONSAVEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A890ContagemResultado_Responsavel), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_CONTRATADADORESPONSAVEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1229ContagemResultado_ContratadaDoResponsavel), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_CNTSRVPRRNOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2091ContagemResultado_CntSrvPrrNome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTRATOSERVICOSUNIDCONVERSAO_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV43ContratoServicosUnidConversao_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vSISTEMA_GESTOR", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV38Sistema_Gestor, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vRESPONSAVEL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV19Responsavel, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTAGEMRESULTADO_OWNEREMAIL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV35ContagemResultado_OwnerEmail, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTAGEMRESULTADO_OWNERTELEFONE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV36ContagemResultado_OwnerTelefone, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_CNTSRVPRRNOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2091ContagemResultado_CntSrvPrrNome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTRATOSERVICOSUNIDCONVERSAO_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV43ContratoServicosUnidConversao_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vSISTEMA_GESTOR", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV38Sistema_Gestor, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vRESPONSAVEL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV19Responsavel, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTAGEMRESULTADO_OWNEREMAIL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV35ContagemResultado_OwnerEmail, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTAGEMRESULTADO_OWNERTELEFONE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV36ContagemResultado_OwnerTelefone, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_CNTSRVPRRNOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2091ContagemResultado_CntSrvPrrNome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTRATOSERVICOSUNIDCONVERSAO_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV43ContratoServicosUnidConversao_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vSISTEMA_GESTOR", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV38Sistema_Gestor, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vRESPONSAVEL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV19Responsavel, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTAGEMRESULTADO_OWNEREMAIL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV35ContagemResultado_OwnerEmail, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTAGEMRESULTADO_OWNERTELEFONE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV36ContagemResultado_OwnerTelefone, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_CNTSRVPRRNOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2091ContagemResultado_CntSrvPrrNome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTRATOSERVICOSUNIDCONVERSAO_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV43ContratoServicosUnidConversao_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vSISTEMA_GESTOR", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV38Sistema_Gestor, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vRESPONSAVEL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV19Responsavel, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTAGEMRESULTADO_OWNEREMAIL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV35ContagemResultado_OwnerEmail, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTAGEMRESULTADO_OWNERTELEFONE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV36ContagemResultado_OwnerTelefone, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"INNEWWINDOW1_Target", StringUtil.RTrim( Innewwindow1_Target));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormB52( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contagemresultadogeneral.js", "?2020617159222");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContagemResultadoGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Resultado General" ;
      }

      protected void WBB50( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contagemresultadogeneral.aspx");
               context.AddJavascriptSource("Window/InNewWindowRender.js", "");
            }
            wb_table1_2_B52( true) ;
         }
         else
         {
            wb_table1_2_B52( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_B52e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_CntSrvPrrCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1443ContagemResultado_CntSrvPrrCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1443ContagemResultado_CntSrvPrrCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_CntSrvPrrCod_Jsonclick, 0, "Attribute", "", "", "", edtContagemResultado_CntSrvPrrCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoGeneral.htm");
         }
         wbLoad = true;
      }

      protected void STARTB52( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contagem Resultado General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPB50( ) ;
            }
         }
      }

      protected void WSB52( )
      {
         STARTB52( ) ;
         EVTB52( ) ;
      }

      protected void EVTB52( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPB50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPB50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11B52 */
                                    E11B52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPB50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12B52 */
                                    E12B52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPB50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13B52 */
                                    E13B52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPB50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14B52 */
                                    E14B52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPB50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15B52 */
                                    E15B52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOPAUSARCONTINUAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPB50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16B52 */
                                    E16B52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOEXLOTE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPB50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E17B52 */
                                    E17B52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOAGENDARREUNIAO'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPB50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E18B52 */
                                    E18B52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPB50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E19B52 */
                                    E19B52 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPB50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPB50( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavContratoservicosunidconversao_sigla_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEB52( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormB52( ) ;
            }
         }
      }

      protected void PAB52( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            dynContagemResultado_Owner.Name = "CONTAGEMRESULTADO_OWNER";
            dynContagemResultado_Owner.WebTags = "";
            cmbContagemResultado_StatusDmn.Name = "CONTAGEMRESULTADO_STATUSDMN";
            cmbContagemResultado_StatusDmn.WebTags = "";
            cmbContagemResultado_StatusDmn.addItem("B", "Stand by", 0);
            cmbContagemResultado_StatusDmn.addItem("S", "Solicitada", 0);
            cmbContagemResultado_StatusDmn.addItem("E", "Em An�lise", 0);
            cmbContagemResultado_StatusDmn.addItem("A", "Em execu��o", 0);
            cmbContagemResultado_StatusDmn.addItem("R", "Resolvida", 0);
            cmbContagemResultado_StatusDmn.addItem("C", "Conferida", 0);
            cmbContagemResultado_StatusDmn.addItem("D", "Retornada", 0);
            cmbContagemResultado_StatusDmn.addItem("H", "Homologada", 0);
            cmbContagemResultado_StatusDmn.addItem("O", "Aceite", 0);
            cmbContagemResultado_StatusDmn.addItem("P", "A Pagar", 0);
            cmbContagemResultado_StatusDmn.addItem("L", "Liquidada", 0);
            cmbContagemResultado_StatusDmn.addItem("X", "Cancelada", 0);
            cmbContagemResultado_StatusDmn.addItem("N", "N�o Faturada", 0);
            cmbContagemResultado_StatusDmn.addItem("J", "Planejamento", 0);
            cmbContagemResultado_StatusDmn.addItem("I", "An�lise Planejamento", 0);
            cmbContagemResultado_StatusDmn.addItem("T", "Validacao T�cnica", 0);
            cmbContagemResultado_StatusDmn.addItem("Q", "Validacao Qualidade", 0);
            cmbContagemResultado_StatusDmn.addItem("G", "Em Homologa��o", 0);
            cmbContagemResultado_StatusDmn.addItem("M", "Valida��o Mensura��o", 0);
            cmbContagemResultado_StatusDmn.addItem("U", "Rascunho", 0);
            if ( cmbContagemResultado_StatusDmn.ItemCount > 0 )
            {
               A484ContagemResultado_StatusDmn = cmbContagemResultado_StatusDmn.getValidValue(A484ContagemResultado_StatusDmn);
               n484ContagemResultado_StatusDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
            }
            lstavPagocolaborador.Name = "vPAGOCOLABORADOR";
            lstavPagocolaborador.WebTags = "";
            lstavPagocolaborador.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "N�o", 0);
            if ( lstavPagocolaborador.ItemCount > 0 )
            {
               AV32PagoColaborador = (short)(NumberUtil.Val( lstavPagocolaborador.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV32PagoColaborador), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32PagoColaborador", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32PagoColaborador), 4, 0)));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavContratoservicosunidconversao_sigla_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLACONTAGEMRESULTADO_OWNERB52( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLACONTAGEMRESULTADO_OWNER_dataB52( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXACONTAGEMRESULTADO_OWNER_htmlB52( )
      {
         int gxdynajaxvalue ;
         GXDLACONTAGEMRESULTADO_OWNER_dataB52( ) ;
         gxdynajaxindex = 1;
         dynContagemResultado_Owner.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynContagemResultado_Owner.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynContagemResultado_Owner.ItemCount > 0 )
         {
            A508ContagemResultado_Owner = (int)(NumberUtil.Val( dynContagemResultado_Owner.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A508ContagemResultado_Owner), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A508ContagemResultado_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A508ContagemResultado_Owner), 6, 0)));
         }
      }

      protected void GXDLACONTAGEMRESULTADO_OWNER_dataB52( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H00B521 */
         pr_default.execute(19);
         while ( (pr_default.getStatus(19) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00B521_A1Usuario_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00B521_A58Usuario_PessoaNom[0]));
            pr_default.readNext(19);
         }
         pr_default.close(19);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynContagemResultado_Owner.ItemCount > 0 )
         {
            A508ContagemResultado_Owner = (int)(NumberUtil.Val( dynContagemResultado_Owner.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A508ContagemResultado_Owner), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A508ContagemResultado_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A508ContagemResultado_Owner), 6, 0)));
         }
         if ( cmbContagemResultado_StatusDmn.ItemCount > 0 )
         {
            A484ContagemResultado_StatusDmn = cmbContagemResultado_StatusDmn.getValidValue(A484ContagemResultado_StatusDmn);
            n484ContagemResultado_StatusDmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
         }
         if ( lstavPagocolaborador.ItemCount > 0 )
         {
            AV32PagoColaborador = (short)(NumberUtil.Val( lstavPagocolaborador.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV32PagoColaborador), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32PagoColaborador", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32PagoColaborador), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFB52( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV52Pgmname = "ContagemResultadoGeneral";
         context.Gx_err = 0;
         edtavContratoservicosunidconversao_sigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratoservicosunidconversao_sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosunidconversao_sigla_Enabled), 5, 0)));
         edtavSistema_gestor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSistema_gestor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_gestor_Enabled), 5, 0)));
         edtavResponsavel_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavResponsavel_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavResponsavel_Enabled), 5, 0)));
         edtavContagemresultado_owneremail_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultado_owneremail_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_owneremail_Enabled), 5, 0)));
         edtavContagemresultado_ownertelefone_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultado_ownertelefone_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_ownertelefone_Enabled), 5, 0)));
      }

      protected void RFB52( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E19B52 */
         E19B52 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00B522 */
            pr_default.execute(20, new Object[] {A456ContagemResultado_Codigo});
            while ( (pr_default.getStatus(20) != 101) )
            {
               A470ContagemResultado_ContadorFMCod = H00B522_A470ContagemResultado_ContadorFMCod[0];
               A1000ContagemResultado_CrFMEhContratante = H00B522_A1000ContagemResultado_CrFMEhContratante[0];
               n1000ContagemResultado_CrFMEhContratante = H00B522_n1000ContagemResultado_CrFMEhContratante[0];
               A1000ContagemResultado_CrFMEhContratante = H00B522_A1000ContagemResultado_CrFMEhContratante[0];
               n1000ContagemResultado_CrFMEhContratante = H00B522_n1000ContagemResultado_CrFMEhContratante[0];
               GXACONTAGEMRESULTADO_OWNER_htmlB52( ) ;
               /* Execute user event: E12B52 */
               E12B52 ();
               pr_default.readNext(20);
            }
            pr_default.close(20);
            WBB50( ) ;
         }
      }

      protected void STRUPB50( )
      {
         /* Before Start, stand alone formulas. */
         AV52Pgmname = "ContagemResultadoGeneral";
         context.Gx_err = 0;
         edtavContratoservicosunidconversao_sigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratoservicosunidconversao_sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosunidconversao_sigla_Enabled), 5, 0)));
         edtavSistema_gestor_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSistema_gestor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_gestor_Enabled), 5, 0)));
         edtavResponsavel_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavResponsavel_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavResponsavel_Enabled), 5, 0)));
         edtavContagemresultado_owneremail_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultado_owneremail_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_owneremail_Enabled), 5, 0)));
         edtavContagemresultado_ownertelefone_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContagemresultado_ownertelefone_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_ownertelefone_Enabled), 5, 0)));
         GXACONTAGEMRESULTADO_OWNER_htmlB52( ) ;
         /* Using cursor H00B523 */
         pr_default.execute(21, new Object[] {A456ContagemResultado_Codigo});
         A454ContagemResultado_ContadorFSCod = H00B523_A454ContagemResultado_ContadorFSCod[0];
         n454ContagemResultado_ContadorFSCod = H00B523_n454ContagemResultado_ContadorFSCod[0];
         A468ContagemResultado_NaoCnfDmnCod = H00B523_A468ContagemResultado_NaoCnfDmnCod[0];
         n468ContagemResultado_NaoCnfDmnCod = H00B523_n468ContagemResultado_NaoCnfDmnCod[0];
         A146Modulo_Codigo = H00B523_A146Modulo_Codigo[0];
         n146Modulo_Codigo = H00B523_n146Modulo_Codigo[0];
         A805ContagemResultado_ContratadaOrigemCod = H00B523_A805ContagemResultado_ContratadaOrigemCod[0];
         n805ContagemResultado_ContratadaOrigemCod = H00B523_n805ContagemResultado_ContratadaOrigemCod[0];
         A1043ContagemResultado_LiqLogCod = H00B523_A1043ContagemResultado_LiqLogCod[0];
         n1043ContagemResultado_LiqLogCod = H00B523_n1043ContagemResultado_LiqLogCod[0];
         A1553ContagemResultado_CntSrvCod = H00B523_A1553ContagemResultado_CntSrvCod[0];
         n1553ContagemResultado_CntSrvCod = H00B523_n1553ContagemResultado_CntSrvCod[0];
         A490ContagemResultado_ContratadaCod = H00B523_A490ContagemResultado_ContratadaCod[0];
         n490ContagemResultado_ContratadaCod = H00B523_n490ContagemResultado_ContratadaCod[0];
         A597ContagemResultado_LoteAceiteCod = H00B523_A597ContagemResultado_LoteAceiteCod[0];
         n597ContagemResultado_LoteAceiteCod = H00B523_n597ContagemResultado_LoteAceiteCod[0];
         A602ContagemResultado_OSVinculada = H00B523_A602ContagemResultado_OSVinculada[0];
         n602ContagemResultado_OSVinculada = H00B523_n602ContagemResultado_OSVinculada[0];
         A457ContagemResultado_Demanda = H00B523_A457ContagemResultado_Demanda[0];
         n457ContagemResultado_Demanda = H00B523_n457ContagemResultado_Demanda[0];
         A489ContagemResultado_SistemaCod = H00B523_A489ContagemResultado_SistemaCod[0];
         n489ContagemResultado_SistemaCod = H00B523_n489ContagemResultado_SistemaCod[0];
         A484ContagemResultado_StatusDmn = H00B523_A484ContagemResultado_StatusDmn[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
         n484ContagemResultado_StatusDmn = H00B523_n484ContagemResultado_StatusDmn[0];
         A912ContagemResultado_HoraEntrega = H00B523_A912ContagemResultado_HoraEntrega[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A912ContagemResultado_HoraEntrega", context.localUtil.TToC( A912ContagemResultado_HoraEntrega, 0, 5, 0, 3, "/", ":", " "));
         n912ContagemResultado_HoraEntrega = H00B523_n912ContagemResultado_HoraEntrega[0];
         A472ContagemResultado_DataEntrega = H00B523_A472ContagemResultado_DataEntrega[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A472ContagemResultado_DataEntrega", context.localUtil.Format(A472ContagemResultado_DataEntrega, "99/99/99"));
         n472ContagemResultado_DataEntrega = H00B523_n472ContagemResultado_DataEntrega[0];
         A1350ContagemResultado_DataCadastro = H00B523_A1350ContagemResultado_DataCadastro[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1350ContagemResultado_DataCadastro", context.localUtil.TToC( A1350ContagemResultado_DataCadastro, 8, 5, 0, 3, "/", ":", " "));
         n1350ContagemResultado_DataCadastro = H00B523_n1350ContagemResultado_DataCadastro[0];
         A508ContagemResultado_Owner = H00B523_A508ContagemResultado_Owner[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A508ContagemResultado_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A508ContagemResultado_Owner), 6, 0)));
         A799ContagemResultado_PFLFSImp = H00B523_A799ContagemResultado_PFLFSImp[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A799ContagemResultado_PFLFSImp", StringUtil.LTrim( StringUtil.Str( A799ContagemResultado_PFLFSImp, 14, 5)));
         n799ContagemResultado_PFLFSImp = H00B523_n799ContagemResultado_PFLFSImp[0];
         A798ContagemResultado_PFBFSImp = H00B523_A798ContagemResultado_PFBFSImp[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A798ContagemResultado_PFBFSImp", StringUtil.LTrim( StringUtil.Str( A798ContagemResultado_PFBFSImp, 14, 5)));
         n798ContagemResultado_PFBFSImp = H00B523_n798ContagemResultado_PFBFSImp[0];
         A1046ContagemResultado_Agrupador = H00B523_A1046ContagemResultado_Agrupador[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1046ContagemResultado_Agrupador", A1046ContagemResultado_Agrupador);
         n1046ContagemResultado_Agrupador = H00B523_n1046ContagemResultado_Agrupador[0];
         A465ContagemResultado_Link = H00B523_A465ContagemResultado_Link[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A465ContagemResultado_Link", A465ContagemResultado_Link);
         n465ContagemResultado_Link = H00B523_n465ContagemResultado_Link[0];
         A1044ContagemResultado_FncUsrCod = H00B523_A1044ContagemResultado_FncUsrCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1044ContagemResultado_FncUsrCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1044ContagemResultado_FncUsrCod), 6, 0)));
         n1044ContagemResultado_FncUsrCod = H00B523_n1044ContagemResultado_FncUsrCod[0];
         A2133ContagemResultado_QuantidadeSolicitada = H00B523_A2133ContagemResultado_QuantidadeSolicitada[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2133ContagemResultado_QuantidadeSolicitada", StringUtil.LTrim( StringUtil.Str( A2133ContagemResultado_QuantidadeSolicitada, 9, 4)));
         n2133ContagemResultado_QuantidadeSolicitada = H00B523_n2133ContagemResultado_QuantidadeSolicitada[0];
         A1452ContagemResultado_SS = H00B523_A1452ContagemResultado_SS[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1452ContagemResultado_SS", StringUtil.LTrim( StringUtil.Str( (decimal)(A1452ContagemResultado_SS), 8, 0)));
         n1452ContagemResultado_SS = H00B523_n1452ContagemResultado_SS[0];
         A471ContagemResultado_DataDmn = H00B523_A471ContagemResultado_DataDmn[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A471ContagemResultado_DataDmn", context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"));
         A1443ContagemResultado_CntSrvPrrCod = H00B523_A1443ContagemResultado_CntSrvPrrCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1443ContagemResultado_CntSrvPrrCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1443ContagemResultado_CntSrvPrrCod), 6, 0)));
         n1443ContagemResultado_CntSrvPrrCod = H00B523_n1443ContagemResultado_CntSrvPrrCod[0];
         A890ContagemResultado_Responsavel = H00B523_A890ContagemResultado_Responsavel[0];
         n890ContagemResultado_Responsavel = H00B523_n890ContagemResultado_Responsavel[0];
         pr_default.close(21);
         /* Using cursor H00B524 */
         pr_default.execute(22, new Object[] {n454ContagemResultado_ContadorFSCod, A454ContagemResultado_ContadorFSCod});
         A480ContagemResultado_CrFSPessoaCod = H00B524_A480ContagemResultado_CrFSPessoaCod[0];
         n480ContagemResultado_CrFSPessoaCod = H00B524_n480ContagemResultado_CrFSPessoaCod[0];
         pr_default.close(22);
         /* Using cursor H00B525 */
         pr_default.execute(23, new Object[] {n480ContagemResultado_CrFSPessoaCod, A480ContagemResultado_CrFSPessoaCod});
         A455ContagemResultado_ContadorFSNom = H00B525_A455ContagemResultado_ContadorFSNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A455ContagemResultado_ContadorFSNom", A455ContagemResultado_ContadorFSNom);
         n455ContagemResultado_ContadorFSNom = H00B525_n455ContagemResultado_ContadorFSNom[0];
         pr_default.close(23);
         /* Using cursor H00B526 */
         pr_default.execute(24, new Object[] {n468ContagemResultado_NaoCnfDmnCod, A468ContagemResultado_NaoCnfDmnCod});
         A477ContagemResultado_NaoCnfDmnNom = H00B526_A477ContagemResultado_NaoCnfDmnNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A477ContagemResultado_NaoCnfDmnNom", A477ContagemResultado_NaoCnfDmnNom);
         n477ContagemResultado_NaoCnfDmnNom = H00B526_n477ContagemResultado_NaoCnfDmnNom[0];
         pr_default.close(24);
         /* Using cursor H00B527 */
         pr_default.execute(25, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
         A143Modulo_Nome = H00B527_A143Modulo_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A143Modulo_Nome", A143Modulo_Nome);
         pr_default.close(25);
         /* Using cursor H00B528 */
         pr_default.execute(26, new Object[] {n805ContagemResultado_ContratadaOrigemCod, A805ContagemResultado_ContratadaOrigemCod});
         A807ContagemResultado_ContratadaOrigemPesCod = H00B528_A807ContagemResultado_ContratadaOrigemPesCod[0];
         n807ContagemResultado_ContratadaOrigemPesCod = H00B528_n807ContagemResultado_ContratadaOrigemPesCod[0];
         pr_default.close(26);
         /* Using cursor H00B529 */
         pr_default.execute(27, new Object[] {n807ContagemResultado_ContratadaOrigemPesCod, A807ContagemResultado_ContratadaOrigemPesCod});
         A808ContagemResultado_ContratadaOrigemPesNom = H00B529_A808ContagemResultado_ContratadaOrigemPesNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A808ContagemResultado_ContratadaOrigemPesNom", A808ContagemResultado_ContratadaOrigemPesNom);
         n808ContagemResultado_ContratadaOrigemPesNom = H00B529_n808ContagemResultado_ContratadaOrigemPesNom[0];
         pr_default.close(27);
         /* Using cursor H00B530 */
         pr_default.execute(28, new Object[] {n1043ContagemResultado_LiqLogCod, A1043ContagemResultado_LiqLogCod});
         A1034ContagemResultadoLiqLog_Data = H00B530_A1034ContagemResultadoLiqLog_Data[0];
         n1034ContagemResultadoLiqLog_Data = H00B530_n1034ContagemResultadoLiqLog_Data[0];
         pr_default.close(28);
         /* Using cursor H00B531 */
         pr_default.execute(29, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
         A601ContagemResultado_Servico = H00B531_A601ContagemResultado_Servico[0];
         n601ContagemResultado_Servico = H00B531_n601ContagemResultado_Servico[0];
         A1620ContagemResultado_CntSrvUndCnt = H00B531_A1620ContagemResultado_CntSrvUndCnt[0];
         n1620ContagemResultado_CntSrvUndCnt = H00B531_n1620ContagemResultado_CntSrvUndCnt[0];
         A2094ContratoServicos_SolicitaGestorSistema = H00B531_A2094ContratoServicos_SolicitaGestorSistema[0];
         n2094ContratoServicos_SolicitaGestorSistema = H00B531_n2094ContratoServicos_SolicitaGestorSistema[0];
         A1613ContagemResultado_CntSrvQtdUntCns = H00B531_A1613ContagemResultado_CntSrvQtdUntCns[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1613ContagemResultado_CntSrvQtdUntCns", StringUtil.LTrim( StringUtil.Str( A1613ContagemResultado_CntSrvQtdUntCns, 9, 4)));
         n1613ContagemResultado_CntSrvQtdUntCns = H00B531_n1613ContagemResultado_CntSrvQtdUntCns[0];
         pr_default.close(29);
         /* Using cursor H00B532 */
         pr_default.execute(30, new Object[] {n601ContagemResultado_Servico, A601ContagemResultado_Servico});
         A801ContagemResultado_ServicoSigla = H00B532_A801ContagemResultado_ServicoSigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A801ContagemResultado_ServicoSigla", A801ContagemResultado_ServicoSigla);
         n801ContagemResultado_ServicoSigla = H00B532_n801ContagemResultado_ServicoSigla[0];
         pr_default.close(30);
         /* Using cursor H00B533 */
         pr_default.execute(31, new Object[] {n1620ContagemResultado_CntSrvUndCnt, A1620ContagemResultado_CntSrvUndCnt});
         A2209ContagemResultado_CntSrvUndCntNome = H00B533_A2209ContagemResultado_CntSrvUndCntNome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2209ContagemResultado_CntSrvUndCntNome", A2209ContagemResultado_CntSrvUndCntNome);
         n2209ContagemResultado_CntSrvUndCntNome = H00B533_n2209ContagemResultado_CntSrvUndCntNome[0];
         pr_default.close(31);
         /* Using cursor H00B534 */
         pr_default.execute(32, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
         A499ContagemResultado_ContratadaPessoaCod = H00B534_A499ContagemResultado_ContratadaPessoaCod[0];
         n499ContagemResultado_ContratadaPessoaCod = H00B534_n499ContagemResultado_ContratadaPessoaCod[0];
         A52Contratada_AreaTrabalhoCod = H00B534_A52Contratada_AreaTrabalhoCod[0];
         n52Contratada_AreaTrabalhoCod = H00B534_n52Contratada_AreaTrabalhoCod[0];
         pr_default.close(32);
         /* Using cursor H00B535 */
         pr_default.execute(33, new Object[] {n499ContagemResultado_ContratadaPessoaCod, A499ContagemResultado_ContratadaPessoaCod});
         A500ContagemResultado_ContratadaPessoaNom = H00B535_A500ContagemResultado_ContratadaPessoaNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A500ContagemResultado_ContratadaPessoaNom", A500ContagemResultado_ContratadaPessoaNom);
         n500ContagemResultado_ContratadaPessoaNom = H00B535_n500ContagemResultado_ContratadaPessoaNom[0];
         pr_default.close(33);
         /* Using cursor H00B536 */
         pr_default.execute(34, new Object[] {n597ContagemResultado_LoteAceiteCod, A597ContagemResultado_LoteAceiteCod});
         A1547ContagemResultado_LoteNFe = H00B536_A1547ContagemResultado_LoteNFe[0];
         n1547ContagemResultado_LoteNFe = H00B536_n1547ContagemResultado_LoteNFe[0];
         A525ContagemResultado_AceiteUserCod = H00B536_A525ContagemResultado_AceiteUserCod[0];
         n525ContagemResultado_AceiteUserCod = H00B536_n525ContagemResultado_AceiteUserCod[0];
         A529ContagemResultado_DataAceite = H00B536_A529ContagemResultado_DataAceite[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A529ContagemResultado_DataAceite", context.localUtil.TToC( A529ContagemResultado_DataAceite, 8, 5, 0, 3, "/", ":", " "));
         n529ContagemResultado_DataAceite = H00B536_n529ContagemResultado_DataAceite[0];
         A563Lote_Nome = H00B536_A563Lote_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A563Lote_Nome", A563Lote_Nome);
         n563Lote_Nome = H00B536_n563Lote_Nome[0];
         A528ContagemResultado_LoteAceite = H00B536_A528ContagemResultado_LoteAceite[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A528ContagemResultado_LoteAceite", A528ContagemResultado_LoteAceite);
         n528ContagemResultado_LoteAceite = H00B536_n528ContagemResultado_LoteAceite[0];
         pr_default.close(34);
         /* Using cursor H00B537 */
         pr_default.execute(35, new Object[] {n525ContagemResultado_AceiteUserCod, A525ContagemResultado_AceiteUserCod});
         A526ContagemResultado_AceitePessoaCod = H00B537_A526ContagemResultado_AceitePessoaCod[0];
         n526ContagemResultado_AceitePessoaCod = H00B537_n526ContagemResultado_AceitePessoaCod[0];
         pr_default.close(35);
         /* Using cursor H00B538 */
         pr_default.execute(36, new Object[] {n526ContagemResultado_AceitePessoaCod, A526ContagemResultado_AceitePessoaCod});
         A527ContagemResultado_AceiteUserNom = H00B538_A527ContagemResultado_AceiteUserNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A527ContagemResultado_AceiteUserNom", A527ContagemResultado_AceiteUserNom);
         n527ContagemResultado_AceiteUserNom = H00B538_n527ContagemResultado_AceiteUserNom[0];
         pr_default.close(36);
         /* Using cursor H00B539 */
         pr_default.execute(37, new Object[] {n489ContagemResultado_SistemaCod, A489ContagemResultado_SistemaCod});
         A515ContagemResultado_SistemaCoord = H00B539_A515ContagemResultado_SistemaCoord[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A515ContagemResultado_SistemaCoord", A515ContagemResultado_SistemaCoord);
         n515ContagemResultado_SistemaCoord = H00B539_n515ContagemResultado_SistemaCoord[0];
         A509ContagemrResultado_SistemaSigla = H00B539_A509ContagemrResultado_SistemaSigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A509ContagemrResultado_SistemaSigla", A509ContagemrResultado_SistemaSigla);
         n509ContagemrResultado_SistemaSigla = H00B539_n509ContagemrResultado_SistemaSigla[0];
         pr_default.close(37);
         GXt_char1 = A2091ContagemResultado_CntSrvPrrNome;
         new prc_buscaprioridadeos(context ).execute(  A1443ContagemResultado_CntSrvPrrCod, out  GXt_char1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1443ContagemResultado_CntSrvPrrCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1443ContagemResultado_CntSrvPrrCod), 6, 0)));
         A2091ContagemResultado_CntSrvPrrNome = GXt_char1;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2091ContagemResultado_CntSrvPrrNome", A2091ContagemResultado_CntSrvPrrNome);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADO_CNTSRVPRRNOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2091ContagemResultado_CntSrvPrrNome, "@!"))));
         /* Using cursor H00B540 */
         pr_default.execute(38, new Object[] {n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel});
         A1312ContagemResultado_ResponsavelPessCod = H00B540_A1312ContagemResultado_ResponsavelPessCod[0];
         n1312ContagemResultado_ResponsavelPessCod = H00B540_n1312ContagemResultado_ResponsavelPessCod[0];
         pr_default.close(38);
         /* Using cursor H00B541 */
         pr_default.execute(39, new Object[] {n1312ContagemResultado_ResponsavelPessCod, A1312ContagemResultado_ResponsavelPessCod});
         A1313ContagemResultado_ResponsavelPessNome = H00B541_A1313ContagemResultado_ResponsavelPessNome[0];
         n1313ContagemResultado_ResponsavelPessNome = H00B541_n1313ContagemResultado_ResponsavelPessNome[0];
         pr_default.close(39);
         GXt_int2 = A1229ContagemResultado_ContratadaDoResponsavel;
         new prc_contratadadousuario(context ).execute( ref  A890ContagemResultado_Responsavel, ref  A52Contratada_AreaTrabalhoCod, out  GXt_int2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A890ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A890ContagemResultado_Responsavel), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
         A1229ContagemResultado_ContratadaDoResponsavel = GXt_int2;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1229ContagemResultado_ContratadaDoResponsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1229ContagemResultado_ContratadaDoResponsavel), 6, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11B52 */
         E11B52 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A471ContagemResultado_DataDmn = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContagemResultado_DataDmn_Internalname), 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A471ContagemResultado_DataDmn", context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"));
            A1452ContagemResultado_SS = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_SS_Internalname), ",", "."));
            n1452ContagemResultado_SS = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1452ContagemResultado_SS", StringUtil.LTrim( StringUtil.Str( (decimal)(A1452ContagemResultado_SS), 8, 0)));
            A2091ContagemResultado_CntSrvPrrNome = StringUtil.Upper( cgiGet( edtContagemResultado_CntSrvPrrNome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2091ContagemResultado_CntSrvPrrNome", A2091ContagemResultado_CntSrvPrrNome);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMRESULTADO_CNTSRVPRRNOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2091ContagemResultado_CntSrvPrrNome, "@!"))));
            A500ContagemResultado_ContratadaPessoaNom = StringUtil.Upper( cgiGet( edtContagemResultado_ContratadaPessoaNom_Internalname));
            n500ContagemResultado_ContratadaPessoaNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A500ContagemResultado_ContratadaPessoaNom", A500ContagemResultado_ContratadaPessoaNom);
            A808ContagemResultado_ContratadaOrigemPesNom = StringUtil.Upper( cgiGet( edtContagemResultado_ContratadaOrigemPesNom_Internalname));
            n808ContagemResultado_ContratadaOrigemPesNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A808ContagemResultado_ContratadaOrigemPesNom", A808ContagemResultado_ContratadaOrigemPesNom);
            A455ContagemResultado_ContadorFSNom = StringUtil.Upper( cgiGet( edtContagemResultado_ContadorFSNom_Internalname));
            n455ContagemResultado_ContadorFSNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A455ContagemResultado_ContadorFSNom", A455ContagemResultado_ContadorFSNom);
            A801ContagemResultado_ServicoSigla = StringUtil.Upper( cgiGet( edtContagemResultado_ServicoSigla_Internalname));
            n801ContagemResultado_ServicoSigla = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A801ContagemResultado_ServicoSigla", A801ContagemResultado_ServicoSigla);
            A2209ContagemResultado_CntSrvUndCntNome = StringUtil.Upper( cgiGet( edtContagemResultado_CntSrvUndCntNome_Internalname));
            n2209ContagemResultado_CntSrvUndCntNome = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2209ContagemResultado_CntSrvUndCntNome", A2209ContagemResultado_CntSrvUndCntNome);
            A1613ContagemResultado_CntSrvQtdUntCns = context.localUtil.CToN( cgiGet( edtContagemResultado_CntSrvQtdUntCns_Internalname), ",", ".");
            n1613ContagemResultado_CntSrvQtdUntCns = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1613ContagemResultado_CntSrvQtdUntCns", StringUtil.LTrim( StringUtil.Str( A1613ContagemResultado_CntSrvQtdUntCns, 9, 4)));
            A2133ContagemResultado_QuantidadeSolicitada = context.localUtil.CToN( cgiGet( edtContagemResultado_QuantidadeSolicitada_Internalname), ",", ".");
            n2133ContagemResultado_QuantidadeSolicitada = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2133ContagemResultado_QuantidadeSolicitada", StringUtil.LTrim( StringUtil.Str( A2133ContagemResultado_QuantidadeSolicitada, 9, 4)));
            AV43ContratoServicosUnidConversao_Sigla = StringUtil.Upper( cgiGet( edtavContratoservicosunidconversao_sigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43ContratoServicosUnidConversao_Sigla", AV43ContratoServicosUnidConversao_Sigla);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTRATOSERVICOSUNIDCONVERSAO_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV43ContratoServicosUnidConversao_Sigla, "@!"))));
            A509ContagemrResultado_SistemaSigla = StringUtil.Upper( cgiGet( edtContagemrResultado_SistemaSigla_Internalname));
            n509ContagemrResultado_SistemaSigla = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A509ContagemrResultado_SistemaSigla", A509ContagemrResultado_SistemaSigla);
            A143Modulo_Nome = StringUtil.Upper( cgiGet( edtModulo_Nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A143Modulo_Nome", A143Modulo_Nome);
            AV38Sistema_Gestor = cgiGet( edtavSistema_gestor_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38Sistema_Gestor", AV38Sistema_Gestor);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSISTEMA_GESTOR", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV38Sistema_Gestor, ""))));
            A1044ContagemResultado_FncUsrCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_FncUsrCod_Internalname), ",", "."));
            n1044ContagemResultado_FncUsrCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1044ContagemResultado_FncUsrCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1044ContagemResultado_FncUsrCod), 6, 0)));
            A465ContagemResultado_Link = cgiGet( edtContagemResultado_Link_Internalname);
            n465ContagemResultado_Link = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A465ContagemResultado_Link", A465ContagemResultado_Link);
            A515ContagemResultado_SistemaCoord = StringUtil.Upper( cgiGet( edtContagemResultado_SistemaCoord_Internalname));
            n515ContagemResultado_SistemaCoord = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A515ContagemResultado_SistemaCoord", A515ContagemResultado_SistemaCoord);
            A477ContagemResultado_NaoCnfDmnNom = StringUtil.Upper( cgiGet( edtContagemResultado_NaoCnfDmnNom_Internalname));
            n477ContagemResultado_NaoCnfDmnNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A477ContagemResultado_NaoCnfDmnNom", A477ContagemResultado_NaoCnfDmnNom);
            A1046ContagemResultado_Agrupador = StringUtil.Upper( cgiGet( edtContagemResultado_Agrupador_Internalname));
            n1046ContagemResultado_Agrupador = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1046ContagemResultado_Agrupador", A1046ContagemResultado_Agrupador);
            A798ContagemResultado_PFBFSImp = context.localUtil.CToN( cgiGet( edtContagemResultado_PFBFSImp_Internalname), ",", ".");
            n798ContagemResultado_PFBFSImp = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A798ContagemResultado_PFBFSImp", StringUtil.LTrim( StringUtil.Str( A798ContagemResultado_PFBFSImp, 14, 5)));
            A799ContagemResultado_PFLFSImp = context.localUtil.CToN( cgiGet( edtContagemResultado_PFLFSImp_Internalname), ",", ".");
            n799ContagemResultado_PFLFSImp = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A799ContagemResultado_PFLFSImp", StringUtil.LTrim( StringUtil.Str( A799ContagemResultado_PFLFSImp, 14, 5)));
            AV19Responsavel = StringUtil.Upper( cgiGet( edtavResponsavel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Responsavel", AV19Responsavel);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vRESPONSAVEL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV19Responsavel, "@!"))));
            dynContagemResultado_Owner.CurrentValue = cgiGet( dynContagemResultado_Owner_Internalname);
            A508ContagemResultado_Owner = (int)(NumberUtil.Val( cgiGet( dynContagemResultado_Owner_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A508ContagemResultado_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A508ContagemResultado_Owner), 6, 0)));
            AV35ContagemResultado_OwnerEmail = cgiGet( edtavContagemresultado_owneremail_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ContagemResultado_OwnerEmail", AV35ContagemResultado_OwnerEmail);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTAGEMRESULTADO_OWNEREMAIL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV35ContagemResultado_OwnerEmail, ""))));
            AV36ContagemResultado_OwnerTelefone = cgiGet( edtavContagemresultado_ownertelefone_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36ContagemResultado_OwnerTelefone", AV36ContagemResultado_OwnerTelefone);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTAGEMRESULTADO_OWNERTELEFONE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV36ContagemResultado_OwnerTelefone, ""))));
            A1350ContagemResultado_DataCadastro = context.localUtil.CToT( cgiGet( edtContagemResultado_DataCadastro_Internalname), 0);
            n1350ContagemResultado_DataCadastro = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1350ContagemResultado_DataCadastro", context.localUtil.TToC( A1350ContagemResultado_DataCadastro, 8, 5, 0, 3, "/", ":", " "));
            A472ContagemResultado_DataEntrega = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContagemResultado_DataEntrega_Internalname), 0));
            n472ContagemResultado_DataEntrega = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A472ContagemResultado_DataEntrega", context.localUtil.Format(A472ContagemResultado_DataEntrega, "99/99/99"));
            A912ContagemResultado_HoraEntrega = DateTimeUtil.ResetDate(context.localUtil.CToT( cgiGet( edtContagemResultado_HoraEntrega_Internalname), 0));
            n912ContagemResultado_HoraEntrega = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A912ContagemResultado_HoraEntrega", context.localUtil.TToC( A912ContagemResultado_HoraEntrega, 0, 5, 0, 3, "/", ":", " "));
            cmbContagemResultado_StatusDmn.CurrentValue = cgiGet( cmbContagemResultado_StatusDmn_Internalname);
            A484ContagemResultado_StatusDmn = cgiGet( cmbContagemResultado_StatusDmn_Internalname);
            n484ContagemResultado_StatusDmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
            A528ContagemResultado_LoteAceite = cgiGet( edtContagemResultado_LoteAceite_Internalname);
            n528ContagemResultado_LoteAceite = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A528ContagemResultado_LoteAceite", A528ContagemResultado_LoteAceite);
            A563Lote_Nome = StringUtil.Upper( cgiGet( edtLote_Nome_Internalname));
            n563Lote_Nome = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A563Lote_Nome", A563Lote_Nome);
            A529ContagemResultado_DataAceite = context.localUtil.CToT( cgiGet( edtContagemResultado_DataAceite_Internalname), 0);
            n529ContagemResultado_DataAceite = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A529ContagemResultado_DataAceite", context.localUtil.TToC( A529ContagemResultado_DataAceite, 8, 5, 0, 3, "/", ":", " "));
            A527ContagemResultado_AceiteUserNom = StringUtil.Upper( cgiGet( edtContagemResultado_AceiteUserNom_Internalname));
            n527ContagemResultado_AceiteUserNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A527ContagemResultado_AceiteUserNom", A527ContagemResultado_AceiteUserNom);
            lstavPagocolaborador.CurrentValue = cgiGet( lstavPagocolaborador_Internalname);
            AV32PagoColaborador = (short)(NumberUtil.Val( cgiGet( lstavPagocolaborador_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32PagoColaborador", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32PagoColaborador), 4, 0)));
            A1443ContagemResultado_CntSrvPrrCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_CntSrvPrrCod_Internalname), ",", "."));
            n1443ContagemResultado_CntSrvPrrCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1443ContagemResultado_CntSrvPrrCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1443ContagemResultado_CntSrvPrrCod), 6, 0)));
            /* Read saved values. */
            wcpOA456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA456ContagemResultado_Codigo"), ",", "."));
            A456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"CONTAGEMRESULTADO_CODIGO"), ",", "."));
            A457ContagemResultado_Demanda = cgiGet( sPrefix+"CONTAGEMRESULTADO_DEMANDA");
            Innewwindow1_Target = cgiGet( sPrefix+"INNEWWINDOW1_Target");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXACONTAGEMRESULTADO_OWNER_htmlB52( ) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11B52 */
         E11B52 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11B52( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV12WebSession.Remove("ArquivosEvd");
         AV12WebSession.Remove("ManterDados");
         AV12WebSession.Remove("ContadorFS");
         AV12WebSession.Remove("InserindoContagem");
         AV12WebSession.Remove("FiltroConsultaContador");
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
         bttBtnnovo_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnnovo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnnovo_Visible), 5, 0)));
         bttBtnpausarcontinuar_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnpausarcontinuar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnpausarcontinuar_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E12B52( )
      {
         /* Load Routine */
         edtContagemResultado_CntSrvUndCntNome_Link = formatLink("viewunidademedicao.aspx") + "?" + UrlEncode("" +A1620ContagemResultado_CntSrvUndCnt) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultado_CntSrvUndCntNome_Internalname, "Link", edtContagemResultado_CntSrvUndCntNome_Link);
         edtLote_Nome_Link = formatLink("viewlote.aspx") + "?" + UrlEncode("" +A597ContagemResultado_LoteAceiteCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtLote_Nome_Internalname, "Link", edtLote_Nome_Link);
         edtContagemResultado_CntSrvPrrCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultado_CntSrvPrrCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_CntSrvPrrCod_Visible), 5, 0)));
         if ( ! ( AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            bttBtnnovo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnnovo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnnovo_Enabled), 5, 0)));
         }
         tblTblliquidada_Visible = (((AV6WWPContext.gxTpr_Contratada_codigo==A490ContagemResultado_ContratadaCod)||AV6WWPContext.gxTpr_Userehadministradorgam) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblTblliquidada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblliquidada_Visible), 5, 0)));
         bttBtnexlote_Visible = (((StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "O")==0)&&(A525ContagemResultado_AceiteUserCod==AV6WWPContext.gxTpr_Userid)&&((0==A1547ContagemResultado_LoteNFe)||(0==A1547ContagemResultado_LoteNFe))) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnexlote_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnexlote_Visible), 5, 0)));
         bttBtnpausarcontinuar_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnpausarcontinuar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnpausarcontinuar_Visible), 5, 0)));
         edtContagemResultado_DataCadastro_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultado_DataCadastro_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_DataCadastro_Visible), 5, 0)));
         if ( (0==A597ContagemResultado_LoteAceiteCod) || (0==A597ContagemResultado_LoteAceiteCod) )
         {
            tblGrpaceite_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblGrpaceite_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblGrpaceite_Visible), 5, 0)));
         }
         else
         {
            tblGrpaceite_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblGrpaceite_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblGrpaceite_Visible), 5, 0)));
         }
         if ( StringUtil.StringSearch( A465ContagemResultado_Link, "http://", 1) == 0 )
         {
            edtContagemResultado_Link_Link = "http://"+A465ContagemResultado_Link;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultado_Link_Internalname, "Link", edtContagemResultado_Link_Link);
         }
         else
         {
            edtContagemResultado_Link_Link = A465ContagemResultado_Link;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultado_Link_Internalname, "Link", edtContagemResultado_Link_Link);
         }
         if ( ( A1229ContagemResultado_ContratadaDoResponsavel == AV6WWPContext.gxTpr_Contratada_codigo ) || A1000ContagemResultado_CrFMEhContratante || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            AV19Responsavel = A1313ContagemResultado_ResponsavelPessNome;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Responsavel", AV19Responsavel);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vRESPONSAVEL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV19Responsavel, "@!"))));
         }
         else
         {
            AV21ContagemResultado_Responsavel = A890ContagemResultado_Responsavel;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContagemResultado_Responsavel), 6, 0)));
            AV22ContratadaDoResponsavel = A1229ContagemResultado_ContratadaDoResponsavel;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22ContratadaDoResponsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22ContratadaDoResponsavel), 6, 0)));
            /* Execute user subroutine: 'ENTIDADEDOUSUARIO' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         bttBtnupdate_Enabled = (AV6WWPContext.gxTpr_Update&&(AV6WWPContext.gxTpr_Userehadministradorgam||(A508ContagemResultado_Owner==AV6WWPContext.gxTpr_Userid)||AV23EhGestor&&!((StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "B")==0)||(StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "O")==0)||(StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "P")==0)||(StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "L")==0))) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Enabled), 5, 0)));
         bttBtndelete_Enabled = (AV6WWPContext.gxTpr_Delete&&(AV6WWPContext.gxTpr_Userehadministradorgam||(A508ContagemResultado_Owner==AV6WWPContext.gxTpr_Userid)||AV23EhGestor&&!((StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "B")==0)||(StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "O")==0)||(StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "P")==0)||(StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "L")==0))) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Enabled), 5, 0)));
         imgChcklst_Visible = (((StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "E")==0)&&(A490ContagemResultado_ContratadaCod==AV6WWPContext.gxTpr_Contratada_codigo)&&(A890ContagemResultado_Responsavel==AV6WWPContext.gxTpr_Userid)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgChcklst_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgChcklst_Visible), 5, 0)));
         AV27StatusParaPausar = "SEAD";
         if ( AV6WWPContext.gxTpr_Userehcontratante || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "B") == 0 )
            {
               if ( A602ContagemResultado_OSVinculada > 0 )
               {
                  AV30Codigo = A602ContagemResultado_OSVinculada;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30Codigo), 6, 0)));
               }
               else
               {
                  GXt_int2 = AV30Codigo;
                  new prc_getosvinculada(context ).execute( ref  A456ContagemResultado_Codigo, out  GXt_int2) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
                  AV30Codigo = GXt_int2;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30Codigo), 6, 0)));
               }
               if ( AV30Codigo > 0 )
               {
                  /* Execute user subroutine: 'STATUSDEMANDAVNC' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
                  if ( ! ( ( StringUtil.StrCmp(AV28StatusDemandaVnc, "A") == 0 ) && ( AV26StatusContagemVnc == 7 ) ) )
                  {
                     bttBtnpausarcontinuar_Visible = 1;
                     context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnpausarcontinuar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnpausarcontinuar_Visible), 5, 0)));
                     bttBtnpausarcontinuar_Caption = "Continuar execu��o ��";
                     context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnpausarcontinuar_Internalname, "Caption", bttBtnpausarcontinuar_Caption);
                  }
               }
            }
            else
            {
               if ( StringUtil.StringSearch( AV27StatusParaPausar, A484ContagemResultado_StatusDmn, 1) > 0 )
               {
                  bttBtnpausarcontinuar_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnpausarcontinuar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnpausarcontinuar_Visible), 5, 0)));
                  bttBtnpausarcontinuar_Caption = "Pausar execu��o �II";
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnpausarcontinuar_Internalname, "Caption", bttBtnpausarcontinuar_Caption);
               }
            }
            /* Using cursor H00B542 */
            pr_default.execute(40, new Object[] {n601ContagemResultado_Servico, A601ContagemResultado_Servico});
            while ( (pr_default.getStatus(40) != 101) )
            {
               A74Contrato_Codigo = H00B542_A74Contrato_Codigo[0];
               A155Servico_Codigo = H00B542_A155Servico_Codigo[0];
               /* Using cursor H00B543 */
               pr_default.execute(41, new Object[] {A74Contrato_Codigo});
               A39Contratada_Codigo = H00B543_A39Contratada_Codigo[0];
               pr_default.close(41);
               if ( A39Contratada_Codigo == A490ContagemResultado_ContratadaCod )
               {
                  /* Using cursor H00B544 */
                  pr_default.execute(42, new Object[] {AV6WWPContext.gxTpr_Userid});
                  while ( (pr_default.getStatus(42) != 101) )
                  {
                     A1079ContratoGestor_UsuarioCod = H00B544_A1079ContratoGestor_UsuarioCod[0];
                     AV23EhGestor = true;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23EhGestor", AV23EhGestor);
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     pr_default.readNext(42);
                  }
                  pr_default.close(42);
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
               }
               pr_default.readNext(40);
            }
            pr_default.close(40);
            pr_default.close(41);
         }
         lblTbjava_Caption = "<script language=\"javascript\" type=\"text/javascript\"> document.getElementById(\""+lstavPagocolaborador_Internalname+"\").style.display=\"block\"; </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
         if ( ( AV6WWPContext.gxTpr_Contratada_codigo == A490ContagemResultado_ContratadaCod ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            /* Using cursor H00B545 */
            pr_default.execute(43, new Object[] {A456ContagemResultado_Codigo});
            while ( (pr_default.getStatus(43) != 101) )
            {
               A1372ContagemResultadoLiqLogOS_UserCod = H00B545_A1372ContagemResultadoLiqLogOS_UserCod[0];
               A1374ContagemResultadoLiqLogOS_UserPesCod = H00B545_A1374ContagemResultadoLiqLogOS_UserPesCod[0];
               n1374ContagemResultadoLiqLogOS_UserPesCod = H00B545_n1374ContagemResultadoLiqLogOS_UserPesCod[0];
               A1371ContagemResultadoLiqLogOS_OSCod = H00B545_A1371ContagemResultadoLiqLogOS_OSCod[0];
               A1375ContagemResultadoLiqLogOS_Valor = H00B545_A1375ContagemResultadoLiqLogOS_Valor[0];
               A1373ContagemResultadoLiqLogOS_UserNom = H00B545_A1373ContagemResultadoLiqLogOS_UserNom[0];
               n1373ContagemResultadoLiqLogOS_UserNom = H00B545_n1373ContagemResultadoLiqLogOS_UserNom[0];
               A1374ContagemResultadoLiqLogOS_UserPesCod = H00B545_A1374ContagemResultadoLiqLogOS_UserPesCod[0];
               n1374ContagemResultadoLiqLogOS_UserPesCod = H00B545_n1374ContagemResultadoLiqLogOS_UserPesCod[0];
               A1373ContagemResultadoLiqLogOS_UserNom = H00B545_A1373ContagemResultadoLiqLogOS_UserNom[0];
               n1373ContagemResultadoLiqLogOS_UserNom = H00B545_n1373ContagemResultadoLiqLogOS_UserNom[0];
               AV33i = (short)(lstavPagocolaborador.ItemCount+1);
               AV34txt = context.localUtil.DToC( DateTimeUtil.ResetTime( A1034ContagemResultadoLiqLog_Data), 2, "/") + " " + StringUtil.Trim( A1373ContagemResultadoLiqLogOS_UserNom) + " R$ " + StringUtil.Trim( StringUtil.Str( A1375ContagemResultadoLiqLogOS_Valor, 18, 5));
               lstavPagocolaborador.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV33i), 4, 0)), AV34txt, 0);
               pr_default.readNext(43);
            }
            pr_default.close(43);
            if ( lstavPagocolaborador.ItemCount > 1 )
            {
               lstavPagocolaborador.removeItem(StringUtil.Str( (decimal)(0), 1, 0));
            }
            lstavPagocolaborador.Height = lstavPagocolaborador.ItemCount;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lstavPagocolaborador_Internalname, "Height", StringUtil.LTrim( StringUtil.Str( (decimal)(lstavPagocolaborador.Height), 9, 0)));
         }
         AV39Servico_IsSolicitaGestorSistema = A2094ContratoServicos_SolicitaGestorSistema;
      }

      protected void E13B52( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("contagemresultado.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A456ContagemResultado_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14B52( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("contagemresultado.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A456ContagemResultado_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E15B52( )
      {
         /* 'DoFechar' Routine */
         if ( StringUtil.StrCmp(AV18Caller, "DmnSrv") == 0 )
         {
            AV12WebSession.Remove("Caller");
            context.wjLoc = formatLink("extrawwcontagemresultadoos.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         else
         {
            AV12WebSession.Remove("Caller");
            context.wjLoc = formatLink("wwcontagemresultado.aspx") ;
            context.wjLocDisableFrm = 1;
         }
      }

      protected void E16B52( )
      {
         /* 'DoPausarContinuar' Routine */
         new prc_pausarcontinuaros(context ).execute(  A456ContagemResultado_Codigo,  A484ContagemResultado_StatusDmn,  "",  AV6WWPContext.gxTpr_Userid) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
         context.wjLoc = formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +A456ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.RTrim("General"));
         context.wjLocDisableFrm = 1;
         context.PopUp(formatLink("wp_agendamento.aspx") + "?" + UrlEncode("" +A456ContagemResultado_Codigo), new Object[] {"A456ContagemResultado_Codigo"});
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E17B52( )
      {
         /* 'DoExLote' Routine */
         context.PopUp(formatLink("wp_desfaz.aspx") + "?" + UrlEncode("" +A456ContagemResultado_Codigo), new Object[] {});
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV52Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContagemResultado";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "ContagemResultado_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void E18B52( )
      {
         /* 'DoAgendarReuniao' Routine */
         /* Execute user subroutine: 'VALIDACONTRATOREUNIAO' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV24isAgendaOK )
         {
         }
      }

      protected void E19B52( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         AV18Caller = AV12WebSession.Get("Caller");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Caller", AV18Caller);
         if ( ( StringUtil.StrCmp(AV18Caller, "Mon") == 0 ) || ( StringUtil.StrCmp(AV18Caller, "Dmn") == 0 ) || ( StringUtil.StrCmp(AV18Caller, "DmnFin") == 0 ) || ( StringUtil.StrCmp(AV18Caller, "Cnt") == 0 ) )
         {
            AV12WebSession.Remove("Caller");
            bttBtnfechar_Jsonclick = "self.close()";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnfechar_Internalname, "Jsonclick", bttBtnfechar_Jsonclick);
         }
         AV37ContagemResultado_Owner = A508ContagemResultado_Owner;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37ContagemResultado_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37ContagemResultado_Owner), 6, 0)));
         /* Execute user subroutine: 'BUSCA.DADOS.SOLICITANTE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Using cursor H00B546 */
         pr_default.execute(44, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
         while ( (pr_default.getStatus(44) != 101) )
         {
            A160ContratoServicos_Codigo = H00B546_A160ContratoServicos_Codigo[0];
            A2094ContratoServicos_SolicitaGestorSistema = H00B546_A2094ContratoServicos_SolicitaGestorSistema[0];
            n2094ContratoServicos_SolicitaGestorSistema = H00B546_n2094ContratoServicos_SolicitaGestorSistema[0];
            AV39Servico_IsSolicitaGestorSistema = A2094ContratoServicos_SolicitaGestorSistema;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(44);
         if ( AV39Servico_IsSolicitaGestorSistema )
         {
            lblTextblocksistema_gestor_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblTextblocksistema_gestor_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblocksistema_gestor_Visible), 5, 0)));
            edtavSistema_gestor_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSistema_gestor_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_gestor_Visible), 5, 0)));
            GXt_char1 = AV38Sistema_Gestor;
            new prc_getsistemaresponsavelidentificacao(context ).execute(  A489ContagemResultado_SistemaCod,  AV6WWPContext.gxTpr_Areatrabalho_codigo, out  GXt_char1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A489ContagemResultado_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A489ContagemResultado_SistemaCod), 6, 0)));
            AV38Sistema_Gestor = GXt_char1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38Sistema_Gestor", AV38Sistema_Gestor);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSISTEMA_GESTOR", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV38Sistema_Gestor, ""))));
         }
         else
         {
            lblTextblocksistema_gestor_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblTextblocksistema_gestor_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblocksistema_gestor_Visible), 5, 0)));
            edtavSistema_gestor_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSistema_gestor_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_gestor_Visible), 5, 0)));
         }
         AV45IsContratoServicosUnidConversao = false;
         /* Using cursor H00B547 */
         pr_default.execute(45, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
         while ( (pr_default.getStatus(45) != 101) )
         {
            A2110ContratoServicosUnidConversao_Codigo = H00B547_A2110ContratoServicosUnidConversao_Codigo[0];
            A160ContratoServicos_Codigo = H00B547_A160ContratoServicos_Codigo[0];
            A2112ContratoServicosUnidConversao_Sigla = H00B547_A2112ContratoServicosUnidConversao_Sigla[0];
            n2112ContratoServicosUnidConversao_Sigla = H00B547_n2112ContratoServicosUnidConversao_Sigla[0];
            A2112ContratoServicosUnidConversao_Sigla = H00B547_A2112ContratoServicosUnidConversao_Sigla[0];
            n2112ContratoServicosUnidConversao_Sigla = H00B547_n2112ContratoServicosUnidConversao_Sigla[0];
            AV43ContratoServicosUnidConversao_Sigla = A2112ContratoServicosUnidConversao_Sigla;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43ContratoServicosUnidConversao_Sigla", AV43ContratoServicosUnidConversao_Sigla);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTRATOSERVICOSUNIDCONVERSAO_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV43ContratoServicosUnidConversao_Sigla, "@!"))));
            AV45IsContratoServicosUnidConversao = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(45);
         }
         pr_default.close(45);
         if ( AV45IsContratoServicosUnidConversao )
         {
            edtavContratoservicosunidconversao_sigla_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratoservicosunidconversao_sigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosunidconversao_sigla_Visible), 5, 0)));
            lblTextblockcontratoservicosunidconversao_sigla_Caption = "Unidade de Convers�o";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblTextblockcontratoservicosunidconversao_sigla_Internalname, "Caption", lblTextblockcontratoservicosunidconversao_sigla_Caption);
         }
         else
         {
            edtavContratoservicosunidconversao_sigla_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratoservicosunidconversao_sigla_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosunidconversao_sigla_Visible), 5, 0)));
            lblTextblockcontratoservicosunidconversao_sigla_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblTextblockcontratoservicosunidconversao_sigla_Internalname, "Caption", lblTextblockcontratoservicosunidconversao_sigla_Caption);
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6WWPContext", AV6WWPContext);
      }

      protected void S122( )
      {
         /* 'ENTIDADEDOUSUARIO' Routine */
         if ( (0==AV21ContagemResultado_Responsavel) )
         {
            AV19Responsavel = "Sem atribui��o";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Responsavel", AV19Responsavel);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vRESPONSAVEL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV19Responsavel, "@!"))));
         }
         else
         {
            if ( AV6WWPContext.gxTpr_Userehcontratante )
            {
               /* Using cursor H00B548 */
               pr_default.execute(46, new Object[] {AV21ContagemResultado_Responsavel, AV6WWPContext.gxTpr_Areatrabalho_codigo});
               while ( (pr_default.getStatus(46) != 101) )
               {
                  A66ContratadaUsuario_ContratadaCod = H00B548_A66ContratadaUsuario_ContratadaCod[0];
                  A1228ContratadaUsuario_AreaTrabalhoCod = H00B548_A1228ContratadaUsuario_AreaTrabalhoCod[0];
                  n1228ContratadaUsuario_AreaTrabalhoCod = H00B548_n1228ContratadaUsuario_AreaTrabalhoCod[0];
                  A69ContratadaUsuario_UsuarioCod = H00B548_A69ContratadaUsuario_UsuarioCod[0];
                  A438Contratada_Sigla = H00B548_A438Contratada_Sigla[0];
                  n438Contratada_Sigla = H00B548_n438Contratada_Sigla[0];
                  A1228ContratadaUsuario_AreaTrabalhoCod = H00B548_A1228ContratadaUsuario_AreaTrabalhoCod[0];
                  n1228ContratadaUsuario_AreaTrabalhoCod = H00B548_n1228ContratadaUsuario_AreaTrabalhoCod[0];
                  A438Contratada_Sigla = H00B548_A438Contratada_Sigla[0];
                  n438Contratada_Sigla = H00B548_n438Contratada_Sigla[0];
                  AV19Responsavel = A438Contratada_Sigla;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Responsavel", AV19Responsavel);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vRESPONSAVEL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV19Responsavel, "@!"))));
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(46);
               }
               pr_default.close(46);
            }
            else
            {
               if ( AV22ContratadaDoResponsavel > 0 )
               {
                  /* Using cursor H00B549 */
                  pr_default.execute(47, new Object[] {AV21ContagemResultado_Responsavel, AV6WWPContext.gxTpr_Areatrabalho_codigo});
                  while ( (pr_default.getStatus(47) != 101) )
                  {
                     A66ContratadaUsuario_ContratadaCod = H00B549_A66ContratadaUsuario_ContratadaCod[0];
                     A1018ContratadaUsuario_UsuarioEhContratada = H00B549_A1018ContratadaUsuario_UsuarioEhContratada[0];
                     n1018ContratadaUsuario_UsuarioEhContratada = H00B549_n1018ContratadaUsuario_UsuarioEhContratada[0];
                     A1228ContratadaUsuario_AreaTrabalhoCod = H00B549_A1228ContratadaUsuario_AreaTrabalhoCod[0];
                     n1228ContratadaUsuario_AreaTrabalhoCod = H00B549_n1228ContratadaUsuario_AreaTrabalhoCod[0];
                     A69ContratadaUsuario_UsuarioCod = H00B549_A69ContratadaUsuario_UsuarioCod[0];
                     A438Contratada_Sigla = H00B549_A438Contratada_Sigla[0];
                     n438Contratada_Sigla = H00B549_n438Contratada_Sigla[0];
                     A1228ContratadaUsuario_AreaTrabalhoCod = H00B549_A1228ContratadaUsuario_AreaTrabalhoCod[0];
                     n1228ContratadaUsuario_AreaTrabalhoCod = H00B549_n1228ContratadaUsuario_AreaTrabalhoCod[0];
                     A438Contratada_Sigla = H00B549_A438Contratada_Sigla[0];
                     n438Contratada_Sigla = H00B549_n438Contratada_Sigla[0];
                     A1018ContratadaUsuario_UsuarioEhContratada = H00B549_A1018ContratadaUsuario_UsuarioEhContratada[0];
                     n1018ContratadaUsuario_UsuarioEhContratada = H00B549_n1018ContratadaUsuario_UsuarioEhContratada[0];
                     AV19Responsavel = A438Contratada_Sigla;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Responsavel", AV19Responsavel);
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vRESPONSAVEL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV19Responsavel, "@!"))));
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     pr_default.readNext(47);
                  }
                  pr_default.close(47);
               }
               else
               {
                  AV19Responsavel = AV6WWPContext.gxTpr_Areatrabalho_descricao;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Responsavel", AV19Responsavel);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vRESPONSAVEL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV19Responsavel, "@!"))));
               }
            }
         }
      }

      protected void S142( )
      {
         /* 'VALIDACONTRATOREUNIAO' Routine */
         AV24isAgendaOK = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24isAgendaOK", AV24isAgendaOK);
         if ( (0==A890ContagemResultado_Responsavel) )
         {
            GX_msglist.addItem("Para realizar reuni�o � necess�rio definir um respons�vel pelo contrato!");
            AV24isAgendaOK = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24isAgendaOK", AV24isAgendaOK);
         }
      }

      protected void S132( )
      {
         /* 'STATUSDEMANDAVNC' Routine */
         AV31ContagemResultado.Load(AV30Codigo);
         AV28StatusDemandaVnc = AV31ContagemResultado.gxTpr_Contagemresultado_statusdmn;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28StatusDemandaVnc", AV28StatusDemandaVnc);
         AV26StatusContagemVnc = AV31ContagemResultado.gxTpr_Contagemresultado_statusultcnt;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26StatusContagemVnc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26StatusContagemVnc), 2, 0)));
      }

      protected void S152( )
      {
         /* 'BUSCA.DADOS.SOLICITANTE' Routine */
         AV57GXLvl360 = 0;
         /* Using cursor H00B550 */
         pr_default.execute(48, new Object[] {AV37ContagemResultado_Owner});
         while ( (pr_default.getStatus(48) != 101) )
         {
            A57Usuario_PessoaCod = H00B550_A57Usuario_PessoaCod[0];
            A1Usuario_Codigo = H00B550_A1Usuario_Codigo[0];
            A2095Usuario_PessoaTelefone = H00B550_A2095Usuario_PessoaTelefone[0];
            n2095Usuario_PessoaTelefone = H00B550_n2095Usuario_PessoaTelefone[0];
            A1647Usuario_Email = H00B550_A1647Usuario_Email[0];
            n1647Usuario_Email = H00B550_n1647Usuario_Email[0];
            A2095Usuario_PessoaTelefone = H00B550_A2095Usuario_PessoaTelefone[0];
            n2095Usuario_PessoaTelefone = H00B550_n2095Usuario_PessoaTelefone[0];
            AV57GXLvl360 = 1;
            AV36ContagemResultado_OwnerTelefone = (String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Trim( A2095Usuario_PessoaTelefone))) ? "N�o informado" : A2095Usuario_PessoaTelefone);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36ContagemResultado_OwnerTelefone", AV36ContagemResultado_OwnerTelefone);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTAGEMRESULTADO_OWNERTELEFONE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV36ContagemResultado_OwnerTelefone, ""))));
            AV35ContagemResultado_OwnerEmail = (String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Trim( A1647Usuario_Email))) ? "N�o informado" : A1647Usuario_Email);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ContagemResultado_OwnerEmail", AV35ContagemResultado_OwnerEmail);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTAGEMRESULTADO_OWNEREMAIL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV35ContagemResultado_OwnerEmail, ""))));
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(48);
         if ( AV57GXLvl360 == 0 )
         {
            AV36ContagemResultado_OwnerTelefone = "N�o informado";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36ContagemResultado_OwnerTelefone", AV36ContagemResultado_OwnerTelefone);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTAGEMRESULTADO_OWNERTELEFONE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV36ContagemResultado_OwnerTelefone, ""))));
            AV35ContagemResultado_OwnerEmail = "N�o informado";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35ContagemResultado_OwnerEmail", AV35ContagemResultado_OwnerEmail);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTAGEMRESULTADO_OWNEREMAIL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV35ContagemResultado_OwnerEmail, ""))));
         }
      }

      protected void wb_table1_2_B52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_B52( true) ;
         }
         else
         {
            wb_table2_8_B52( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_B52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_190_B52( true) ;
         }
         else
         {
            wb_table3_190_B52( false) ;
         }
         return  ;
      }

      protected void wb_table3_190_B52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_204_B52( true) ;
         }
         else
         {
            wb_table4_204_B52( false) ;
         }
         return  ;
      }

      protected void wb_table4_204_B52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_B52e( true) ;
         }
         else
         {
            wb_table1_2_B52e( false) ;
         }
      }

      protected void wb_table4_204_B52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUsertable_Internalname, tblUsertable_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "<p></p>") ;
            context.WriteHtmlText( "<p>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"INNEWWINDOW1Container"+"\"></div>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</p>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_204_B52e( true) ;
         }
         else
         {
            wb_table4_204_B52e( false) ;
         }
      }

      protected void wb_table3_190_B52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 193,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, bttBtnupdate_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 195,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, bttBtndelete_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 197,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 199,'" + sPrefix + "',false,'',0)\"";
            ClassString = "ActionButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnnovo_Internalname, "", "Projeto", bttBtnnovo_Jsonclick, 7, "Projeto", "", StyleString, ClassString, bttBtnnovo_Visible, bttBtnnovo_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+"e20b51_client"+"'", TempTags, "", 2, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 201,'" + sPrefix + "',false,'',0)\"";
            ClassString = "ActionButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnpausarcontinuar_Internalname, "", bttBtnpausarcontinuar_Caption, bttBtnpausarcontinuar_Jsonclick, 5, "Pausar execu��o��II", "", StyleString, ClassString, bttBtnpausarcontinuar_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOPAUSARCONTINUAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_190_B52e( true) ;
         }
         else
         {
            wb_table3_190_B52e( false) ;
         }
      }

      protected void wb_table2_8_B52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_datadmn_Internalname, "Data OS FM", "", "", lblTextblockcontagemresultado_datadmn_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContagemResultado_DataDmn_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_DataDmn_Internalname, context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"), context.localUtil.Format( A471ContagemResultado_DataDmn, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_DataDmn_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultado_DataDmn_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_ss_Internalname, "SS", "", "", lblTextblockcontagemresultado_ss_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_SS_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1452ContagemResultado_SS), 8, 0, ",", "")), context.localUtil.Format( (decimal)(A1452ContagemResultado_SS), "ZZZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_SS_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_cntsrvprrnome_Internalname, "Prioridade", "", "", lblTextblockcontagemresultado_cntsrvprrnome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_CntSrvPrrNome_Internalname, StringUtil.RTrim( A2091ContagemResultado_CntSrvPrrNome), StringUtil.RTrim( context.localUtil.Format( A2091ContagemResultado_CntSrvPrrNome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_CntSrvPrrNome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_contratadapessoanom_Internalname, "Prestadora", "", "", lblTextblockcontagemresultado_contratadapessoanom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_ContratadaPessoaNom_Internalname, StringUtil.RTrim( A500ContagemResultado_ContratadaPessoaNom), StringUtil.RTrim( context.localUtil.Format( A500ContagemResultado_ContratadaPessoaNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_ContratadaPessoaNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_contratadaorigempesnom_Internalname, "Origem", "", "", lblTextblockcontagemresultado_contratadaorigempesnom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_ContratadaOrigemPesNom_Internalname, StringUtil.RTrim( A808ContagemResultado_ContratadaOrigemPesNom), StringUtil.RTrim( context.localUtil.Format( A808ContagemResultado_ContratadaOrigemPesNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_ContratadaOrigemPesNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_contadorfsnom_Internalname, "Contador na Origem", "", "", lblTextblockcontagemresultado_contadorfsnom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_ContadorFSNom_Internalname, StringUtil.RTrim( A455ContagemResultado_ContadorFSNom), StringUtil.RTrim( context.localUtil.Format( A455ContagemResultado_ContadorFSNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_ContadorFSNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_servicosigla_Internalname, "Servi�o", "", "", lblTextblockcontagemresultado_servicosigla_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_ServicoSigla_Internalname, StringUtil.RTrim( A801ContagemResultado_ServicoSigla), StringUtil.RTrim( context.localUtil.Format( A801ContagemResultado_ServicoSigla, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_ServicoSigla_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_cntsrvundcntnome_Internalname, "Unidade de Contrata��o", "", "", lblTextblockcontagemresultado_cntsrvundcntnome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_CntSrvUndCntNome_Internalname, StringUtil.RTrim( A2209ContagemResultado_CntSrvUndCntNome), StringUtil.RTrim( context.localUtil.Format( A2209ContagemResultado_CntSrvUndCntNome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtContagemResultado_CntSrvUndCntNome_Link, "", "", "", edtContagemResultado_CntSrvUndCntNome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_cntsrvqtduntcns_Internalname, "Qtd. Unit. Consumo", "", "", lblTextblockcontagemresultado_cntsrvqtduntcns_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_CntSrvQtdUntCns_Internalname, StringUtil.LTrim( StringUtil.NToC( A1613ContagemResultado_CntSrvQtdUntCns, 9, 4, ",", "")), context.localUtil.Format( A1613ContagemResultado_CntSrvQtdUntCns, "ZZZ9.9999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_CntSrvQtdUntCns_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "px", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_quantidadesolicitada_Internalname, "Qtd. Solicitada", "", "", lblTextblockcontagemresultado_quantidadesolicitada_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_QuantidadeSolicitada_Internalname, StringUtil.LTrim( StringUtil.NToC( A2133ContagemResultado_QuantidadeSolicitada, 9, 4, ",", "")), context.localUtil.Format( A2133ContagemResultado_QuantidadeSolicitada, "ZZZ9.9999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_QuantidadeSolicitada_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosunidconversao_sigla_Internalname, lblTextblockcontratoservicosunidconversao_sigla_Caption, "", "", lblTextblockcontratoservicosunidconversao_sigla_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoservicosunidconversao_sigla_Internalname, StringUtil.RTrim( AV43ContratoServicosUnidConversao_Sigla), StringUtil.RTrim( context.localUtil.Format( AV43ContratoServicosUnidConversao_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,56);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoservicosunidconversao_sigla_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", edtavContratoservicosunidconversao_sigla_Visible, edtavContratoservicosunidconversao_sigla_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemrresultado_sistemasigla_Internalname, "Sistema", "", "", lblTextblockcontagemrresultado_sistemasigla_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemrResultado_SistemaSigla_Internalname, StringUtil.RTrim( A509ContagemrResultado_SistemaSigla), StringUtil.RTrim( context.localUtil.Format( A509ContagemrResultado_SistemaSigla, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemrResultado_SistemaSigla_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 25, "chr", 1, "row", 25, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmodulo_nome_Internalname, "M�dulo", "", "", lblTextblockmodulo_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtModulo_Nome_Internalname, StringUtil.RTrim( A143Modulo_Nome), StringUtil.RTrim( context.localUtil.Format( A143Modulo_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtModulo_Nome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_gestor_Internalname, "Gestor Respons�vel pelo Sistema", "", "", lblTextblocksistema_gestor_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblocksistema_gestor_Visible, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_gestor_Internalname, AV38Sistema_Gestor, StringUtil.RTrim( context.localUtil.Format( AV38Sistema_Gestor, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,71);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_gestor_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", edtavSistema_gestor_Visible, edtavSistema_gestor_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_fncusrcod_Internalname, "Fun��o de Usu�rio", "", "", lblTextblockcontagemresultado_fncusrcod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_FncUsrCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1044ContagemResultado_FncUsrCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1044ContagemResultado_FncUsrCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_FncUsrCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_link_Internalname, "Link", "", "", lblTextblockcontagemresultado_link_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_Link_Internalname, A465ContagemResultado_Link, A465ContagemResultado_Link, "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtContagemResultado_Link_Link, "", "", "", edtContagemResultado_Link_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 360, "px", 1, "row", 2097152, 0, 1, 0, 1, 0, -1, true, "Html", "left", false, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_sistemacoord_Internalname, "Coordena��o", "", "", lblTextblockcontagemresultado_sistemacoord_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_84_B52( true) ;
         }
         else
         {
            wb_table5_84_B52( false) ;
         }
         return  ;
      }

      protected void wb_table5_84_B52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_naocnfdmnnom_Internalname, "N�o Conformidade", "", "", lblTextblockcontagemresultado_naocnfdmnnom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_NaoCnfDmnNom_Internalname, StringUtil.RTrim( A477ContagemResultado_NaoCnfDmnNom), StringUtil.RTrim( context.localUtil.Format( A477ContagemResultado_NaoCnfDmnNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_NaoCnfDmnNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_agrupador_Internalname, "Agrupador", "", "", lblTextblockcontagemresultado_agrupador_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_Agrupador_Internalname, StringUtil.RTrim( A1046ContagemResultado_Agrupador), StringUtil.RTrim( context.localUtil.Format( A1046ContagemResultado_Agrupador, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_Agrupador_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, 0, true, "Sigla", "left", true, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pfbfsimp_Internalname, "PF FS importados", "", "", lblTextblockcontagemresultado_pfbfsimp_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table6_105_B52( true) ;
         }
         else
         {
            wb_table6_105_B52( false) ;
         }
         return  ;
      }

      protected void wb_table6_105_B52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockresponsavel_Internalname, "Respons�vel", "", "", lblTextblockresponsavel_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavResponsavel_Internalname, StringUtil.RTrim( AV19Responsavel), StringUtil.RTrim( context.localUtil.Format( AV19Responsavel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,117);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavResponsavel_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, edtavResponsavel_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_owner_Internalname, "Solicitante", "", "", lblTextblockcontagemresultado_owner_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynContagemResultado_Owner, dynContagemResultado_Owner_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A508ContagemResultado_Owner), 6, 0)), 1, dynContagemResultado_Owner_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContagemResultadoGeneral.htm");
            dynContagemResultado_Owner.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A508ContagemResultado_Owner), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynContagemResultado_Owner_Internalname, "Values", (String)(dynContagemResultado_Owner.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_owneremail_Internalname, "E-mail", "", "", lblTextblockcontagemresultado_owneremail_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 130,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_owneremail_Internalname, AV35ContagemResultado_OwnerEmail, StringUtil.RTrim( context.localUtil.Format( AV35ContagemResultado_OwnerEmail, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,130);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_owneremail_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, edtavContagemresultado_owneremail_Enabled, 0, "text", "", 80, "chr", 1, "row", 150, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_ownertelefone_Internalname, "Telefone", "", "", lblTextblockcontagemresultado_ownertelefone_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 134,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_ownertelefone_Internalname, AV36ContagemResultado_OwnerTelefone, StringUtil.RTrim( context.localUtil.Format( AV36ContagemResultado_OwnerTelefone, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,134);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_ownertelefone_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, edtavContagemresultado_ownertelefone_Enabled, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_datacadastro_Internalname, "Data de Cadastro", "", "", lblTextblockcontagemresultado_datacadastro_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContagemResultado_DataCadastro_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_DataCadastro_Internalname, context.localUtil.TToC( A1350ContagemResultado_DataCadastro, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1350ContagemResultado_DataCadastro, "99/99/99 99:99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_DataCadastro_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", edtContagemResultado_DataCadastro_Visible, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultado_DataCadastro_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtContagemResultado_DataCadastro_Visible==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_dataentrega_Internalname, "Prazo", "", "", lblTextblockcontagemresultado_dataentrega_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table7_143_B52( true) ;
         }
         else
         {
            wb_table7_143_B52( false) ;
         }
         return  ;
      }

      protected void wb_table7_143_B52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_statusdmn_Internalname, "Status", "", "", lblTextblockcontagemresultado_statusdmn_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagemResultado_StatusDmn, cmbContagemResultado_StatusDmn_Internalname, StringUtil.RTrim( A484ContagemResultado_StatusDmn), 1, cmbContagemResultado_StatusDmn_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 1, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContagemResultadoGeneral.htm");
            cmbContagemResultado_StatusDmn.CurrentValue = StringUtil.RTrim( A484ContagemResultado_StatusDmn);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagemResultado_StatusDmn_Internalname, "Values", (String)(cmbContagemResultado_StatusDmn.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"6\" >") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup3_Internalname, "Pagamento:", 1, 0, "px", 0, "px", "Group", "", "HLP_ContagemResultadoGeneral.htm");
            wb_table8_156_B52( true) ;
         }
         else
         {
            wb_table8_156_B52( false) ;
         }
         return  ;
      }

      protected void wb_table8_156_B52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_B52e( true) ;
         }
         else
         {
            wb_table2_8_B52e( false) ;
         }
      }

      protected void wb_table8_156_B52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblGrpaceite_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblGrpaceite_Internalname, tblGrpaceite_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_loteaceite_Internalname, "Lote", "", "", lblTextblockcontagemresultado_loteaceite_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table9_161_B52( true) ;
         }
         else
         {
            wb_table9_161_B52( false) ;
         }
         return  ;
      }

      protected void wb_table9_161_B52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_dataaceite_Internalname, "Data do Aceite", "", "", lblTextblockcontagemresultado_dataaceite_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContagemResultado_DataAceite_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_DataAceite_Internalname, context.localUtil.TToC( A529ContagemResultado_DataAceite, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A529ContagemResultado_DataAceite, "99/99/99 99:99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_DataAceite_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultado_DataAceite_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_aceiteusernom_Internalname, "Usu�rio", "", "", lblTextblockcontagemresultado_aceiteusernom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_AceiteUserNom_Internalname, StringUtil.RTrim( A527ContagemResultado_AceiteUserNom), StringUtil.RTrim( context.localUtil.Format( A527ContagemResultado_AceiteUserNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_AceiteUserNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 176,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnexlote_Internalname, "", "Excluir do Lote", bttBtnexlote_Jsonclick, 5, "Exlui esta demanda do Lote", "", StyleString, ClassString, bttBtnexlote_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOEXLOTE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"10\" >") ;
            wb_table10_182_B52( true) ;
         }
         else
         {
            wb_table10_182_B52( false) ;
         }
         return  ;
      }

      protected void wb_table10_182_B52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_156_B52e( true) ;
         }
         else
         {
            wb_table8_156_B52e( false) ;
         }
      }

      protected void wb_table10_182_B52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblliquidada_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblliquidada_Internalname, tblTblliquidada_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpagocolaborador_Internalname, "Liquidada ao prestador:", "", "", lblTextblockpagocolaborador_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 187,'" + sPrefix + "',false,'',0)\"";
            /* ListBox */
            GxWebStd.gx_listbox_ctrl1( context, lstavPagocolaborador, lstavPagocolaborador_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV32PagoColaborador), 4, 0)), lstavPagocolaborador.Height, lstavPagocolaborador_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "row", "", "BootstrapAttributeGray", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,187);\"", "", true, "HLP_ContagemResultadoGeneral.htm");
            lstavPagocolaborador.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV32PagoColaborador), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lstavPagocolaborador_Internalname, "Values", (String)(lstavPagocolaborador.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_182_B52e( true) ;
         }
         else
         {
            wb_table10_182_B52e( false) ;
         }
      }

      protected void wb_table9_161_B52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemresultado_loteaceite_Internalname, tblTablemergedcontagemresultado_loteaceite_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_LoteAceite_Internalname, StringUtil.RTrim( A528ContagemResultado_LoteAceite), StringUtil.RTrim( context.localUtil.Format( A528ContagemResultado_LoteAceite, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_LoteAceite_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLote_Nome_Internalname, StringUtil.RTrim( A563Lote_Nome), StringUtil.RTrim( context.localUtil.Format( A563Lote_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtLote_Nome_Link, "", "", "", edtLote_Nome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_161_B52e( true) ;
         }
         else
         {
            wb_table9_161_B52e( false) ;
         }
      }

      protected void wb_table7_143_B52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemresultado_dataentrega_Internalname, tblTablemergedcontagemresultado_dataentrega_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContagemResultado_DataEntrega_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_DataEntrega_Internalname, context.localUtil.Format(A472ContagemResultado_DataEntrega, "99/99/99"), context.localUtil.Format( A472ContagemResultado_DataEntrega, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_DataEntrega_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultado_DataEntrega_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContagemResultado_HoraEntrega_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_HoraEntrega_Internalname, context.localUtil.TToC( A912ContagemResultado_HoraEntrega, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A912ContagemResultado_HoraEntrega, "99:99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_HoraEntrega_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "Time", "right", false, "HLP_ContagemResultadoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultado_HoraEntrega_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_143_B52e( true) ;
         }
         else
         {
            wb_table7_143_B52e( false) ;
         }
      }

      protected void wb_table6_105_B52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemresultado_pfbfsimp_Internalname, tblTablemergedcontagemresultado_pfbfsimp_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_PFBFSImp_Internalname, StringUtil.LTrim( StringUtil.NToC( A798ContagemResultado_PFBFSImp, 14, 5, ",", "")), context.localUtil.Format( A798ContagemResultado_PFBFSImp, "ZZ,ZZZ,ZZ9.999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_PFBFSImp_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pflfsimp_Internalname, ",", "", "", lblTextblockcontagemresultado_pflfsimp_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_PFLFSImp_Internalname, StringUtil.LTrim( StringUtil.NToC( A799ContagemResultado_PFLFSImp, 14, 5, ",", "")), context.localUtil.Format( A799ContagemResultado_PFLFSImp, "ZZ,ZZZ,ZZ9.999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_PFLFSImp_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_105_B52e( true) ;
         }
         else
         {
            wb_table6_105_B52e( false) ;
         }
      }

      protected void wb_table5_84_B52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemresultado_sistemacoord_Internalname, tblTablemergedcontagemresultado_sistemacoord_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_SistemaCoord_Internalname, A515ContagemResultado_SistemaCoord, StringUtil.RTrim( context.localUtil.Format( A515ContagemResultado_SistemaCoord, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_SistemaCoord_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table11_89_B52( true) ;
         }
         else
         {
            wb_table11_89_B52( false) ;
         }
         return  ;
      }

      protected void wb_table11_89_B52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_84_B52e( true) ;
         }
         else
         {
            wb_table5_84_B52e( false) ;
         }
      }

      protected void wb_table11_89_B52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 15, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgChcklst_Internalname, context.GetImagePath( "d9247895-ebc6-42a8-9963-96a3a06a60c5", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgChcklst_Visible, 1, "", "Check List", 0, 0, 0, "px", 0, "px", 0, 0, 7, imgChcklst_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+"e21b51_client"+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_89_B52e( true) ;
         }
         else
         {
            wb_table11_89_B52e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A456ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAB52( ) ;
         WSB52( ) ;
         WEB52( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA456ContagemResultado_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAB52( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contagemresultadogeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAB52( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A456ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
         }
         wcpOA456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA456ContagemResultado_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A456ContagemResultado_Codigo != wcpOA456ContagemResultado_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA456ContagemResultado_Codigo = cgiGet( sPrefix+"A456ContagemResultado_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA456ContagemResultado_Codigo) > 0 )
         {
            A456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA456ContagemResultado_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
         }
         else
         {
            A456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A456ContagemResultado_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAB52( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSB52( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSB52( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A456ContagemResultado_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA456ContagemResultado_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A456ContagemResultado_Codigo_CTRL", StringUtil.RTrim( sCtrlA456ContagemResultado_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEB52( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020617159645");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contagemresultadogeneral.js", "?2020617159645");
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontagemresultado_datadmn_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_DATADMN";
         edtContagemResultado_DataDmn_Internalname = sPrefix+"CONTAGEMRESULTADO_DATADMN";
         lblTextblockcontagemresultado_ss_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_SS";
         edtContagemResultado_SS_Internalname = sPrefix+"CONTAGEMRESULTADO_SS";
         lblTextblockcontagemresultado_cntsrvprrnome_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_CNTSRVPRRNOME";
         edtContagemResultado_CntSrvPrrNome_Internalname = sPrefix+"CONTAGEMRESULTADO_CNTSRVPRRNOME";
         lblTextblockcontagemresultado_contratadapessoanom_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_CONTRATADAPESSOANOM";
         edtContagemResultado_ContratadaPessoaNom_Internalname = sPrefix+"CONTAGEMRESULTADO_CONTRATADAPESSOANOM";
         lblTextblockcontagemresultado_contratadaorigempesnom_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_CONTRATADAORIGEMPESNOM";
         edtContagemResultado_ContratadaOrigemPesNom_Internalname = sPrefix+"CONTAGEMRESULTADO_CONTRATADAORIGEMPESNOM";
         lblTextblockcontagemresultado_contadorfsnom_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_CONTADORFSNOM";
         edtContagemResultado_ContadorFSNom_Internalname = sPrefix+"CONTAGEMRESULTADO_CONTADORFSNOM";
         lblTextblockcontagemresultado_servicosigla_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_SERVICOSIGLA";
         edtContagemResultado_ServicoSigla_Internalname = sPrefix+"CONTAGEMRESULTADO_SERVICOSIGLA";
         lblTextblockcontagemresultado_cntsrvundcntnome_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_CNTSRVUNDCNTNOME";
         edtContagemResultado_CntSrvUndCntNome_Internalname = sPrefix+"CONTAGEMRESULTADO_CNTSRVUNDCNTNOME";
         lblTextblockcontagemresultado_cntsrvqtduntcns_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_CNTSRVQTDUNTCNS";
         edtContagemResultado_CntSrvQtdUntCns_Internalname = sPrefix+"CONTAGEMRESULTADO_CNTSRVQTDUNTCNS";
         lblTextblockcontagemresultado_quantidadesolicitada_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_QUANTIDADESOLICITADA";
         edtContagemResultado_QuantidadeSolicitada_Internalname = sPrefix+"CONTAGEMRESULTADO_QUANTIDADESOLICITADA";
         lblTextblockcontratoservicosunidconversao_sigla_Internalname = sPrefix+"TEXTBLOCKCONTRATOSERVICOSUNIDCONVERSAO_SIGLA";
         edtavContratoservicosunidconversao_sigla_Internalname = sPrefix+"vCONTRATOSERVICOSUNIDCONVERSAO_SIGLA";
         lblTextblockcontagemrresultado_sistemasigla_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRRESULTADO_SISTEMASIGLA";
         edtContagemrResultado_SistemaSigla_Internalname = sPrefix+"CONTAGEMRRESULTADO_SISTEMASIGLA";
         lblTextblockmodulo_nome_Internalname = sPrefix+"TEXTBLOCKMODULO_NOME";
         edtModulo_Nome_Internalname = sPrefix+"MODULO_NOME";
         lblTextblocksistema_gestor_Internalname = sPrefix+"TEXTBLOCKSISTEMA_GESTOR";
         edtavSistema_gestor_Internalname = sPrefix+"vSISTEMA_GESTOR";
         lblTextblockcontagemresultado_fncusrcod_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_FNCUSRCOD";
         edtContagemResultado_FncUsrCod_Internalname = sPrefix+"CONTAGEMRESULTADO_FNCUSRCOD";
         lblTextblockcontagemresultado_link_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_LINK";
         edtContagemResultado_Link_Internalname = sPrefix+"CONTAGEMRESULTADO_LINK";
         lblTextblockcontagemresultado_sistemacoord_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_SISTEMACOORD";
         edtContagemResultado_SistemaCoord_Internalname = sPrefix+"CONTAGEMRESULTADO_SISTEMACOORD";
         imgChcklst_Internalname = sPrefix+"CHCKLST";
         tblUnnamedtable2_Internalname = sPrefix+"UNNAMEDTABLE2";
         tblTablemergedcontagemresultado_sistemacoord_Internalname = sPrefix+"TABLEMERGEDCONTAGEMRESULTADO_SISTEMACOORD";
         lblTextblockcontagemresultado_naocnfdmnnom_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_NAOCNFDMNNOM";
         edtContagemResultado_NaoCnfDmnNom_Internalname = sPrefix+"CONTAGEMRESULTADO_NAOCNFDMNNOM";
         lblTextblockcontagemresultado_agrupador_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_AGRUPADOR";
         edtContagemResultado_Agrupador_Internalname = sPrefix+"CONTAGEMRESULTADO_AGRUPADOR";
         lblTextblockcontagemresultado_pfbfsimp_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_PFBFSIMP";
         edtContagemResultado_PFBFSImp_Internalname = sPrefix+"CONTAGEMRESULTADO_PFBFSIMP";
         lblTextblockcontagemresultado_pflfsimp_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_PFLFSIMP";
         edtContagemResultado_PFLFSImp_Internalname = sPrefix+"CONTAGEMRESULTADO_PFLFSIMP";
         tblTablemergedcontagemresultado_pfbfsimp_Internalname = sPrefix+"TABLEMERGEDCONTAGEMRESULTADO_PFBFSIMP";
         lblTextblockresponsavel_Internalname = sPrefix+"TEXTBLOCKRESPONSAVEL";
         edtavResponsavel_Internalname = sPrefix+"vRESPONSAVEL";
         lblTextblockcontagemresultado_owner_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_OWNER";
         dynContagemResultado_Owner_Internalname = sPrefix+"CONTAGEMRESULTADO_OWNER";
         lblTextblockcontagemresultado_owneremail_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_OWNEREMAIL";
         edtavContagemresultado_owneremail_Internalname = sPrefix+"vCONTAGEMRESULTADO_OWNEREMAIL";
         lblTextblockcontagemresultado_ownertelefone_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_OWNERTELEFONE";
         edtavContagemresultado_ownertelefone_Internalname = sPrefix+"vCONTAGEMRESULTADO_OWNERTELEFONE";
         lblTextblockcontagemresultado_datacadastro_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_DATACADASTRO";
         edtContagemResultado_DataCadastro_Internalname = sPrefix+"CONTAGEMRESULTADO_DATACADASTRO";
         lblTextblockcontagemresultado_dataentrega_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_DATAENTREGA";
         edtContagemResultado_DataEntrega_Internalname = sPrefix+"CONTAGEMRESULTADO_DATAENTREGA";
         edtContagemResultado_HoraEntrega_Internalname = sPrefix+"CONTAGEMRESULTADO_HORAENTREGA";
         tblTablemergedcontagemresultado_dataentrega_Internalname = sPrefix+"TABLEMERGEDCONTAGEMRESULTADO_DATAENTREGA";
         lblTextblockcontagemresultado_statusdmn_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_STATUSDMN";
         cmbContagemResultado_StatusDmn_Internalname = sPrefix+"CONTAGEMRESULTADO_STATUSDMN";
         lblTextblockcontagemresultado_loteaceite_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_LOTEACEITE";
         edtContagemResultado_LoteAceite_Internalname = sPrefix+"CONTAGEMRESULTADO_LOTEACEITE";
         edtLote_Nome_Internalname = sPrefix+"LOTE_NOME";
         tblTablemergedcontagemresultado_loteaceite_Internalname = sPrefix+"TABLEMERGEDCONTAGEMRESULTADO_LOTEACEITE";
         lblTextblockcontagemresultado_dataaceite_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_DATAACEITE";
         edtContagemResultado_DataAceite_Internalname = sPrefix+"CONTAGEMRESULTADO_DATAACEITE";
         lblTextblockcontagemresultado_aceiteusernom_Internalname = sPrefix+"TEXTBLOCKCONTAGEMRESULTADO_ACEITEUSERNOM";
         edtContagemResultado_AceiteUserNom_Internalname = sPrefix+"CONTAGEMRESULTADO_ACEITEUSERNOM";
         bttBtnexlote_Internalname = sPrefix+"BTNEXLOTE";
         lblTextblockpagocolaborador_Internalname = sPrefix+"TEXTBLOCKPAGOCOLABORADOR";
         lstavPagocolaborador_Internalname = sPrefix+"vPAGOCOLABORADOR";
         tblTblliquidada_Internalname = sPrefix+"TBLLIQUIDADA";
         tblGrpaceite_Internalname = sPrefix+"GRPACEITE";
         grpUnnamedgroup3_Internalname = sPrefix+"UNNAMEDGROUP3";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         bttBtnfechar_Internalname = sPrefix+"BTNFECHAR";
         bttBtnnovo_Internalname = sPrefix+"BTNNOVO";
         bttBtnpausarcontinuar_Internalname = sPrefix+"BTNPAUSARCONTINUAR";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         Innewwindow1_Internalname = sPrefix+"INNEWWINDOW1";
         lblTbjava_Internalname = sPrefix+"TBJAVA";
         tblUsertable_Internalname = sPrefix+"USERTABLE";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtContagemResultado_CntSrvPrrCod_Internalname = sPrefix+"CONTAGEMRESULTADO_CNTSRVPRRCOD";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         imgChcklst_Visible = 1;
         edtContagemResultado_SistemaCoord_Jsonclick = "";
         edtContagemResultado_PFLFSImp_Jsonclick = "";
         edtContagemResultado_PFBFSImp_Jsonclick = "";
         edtContagemResultado_HoraEntrega_Jsonclick = "";
         edtContagemResultado_DataEntrega_Jsonclick = "";
         edtLote_Nome_Jsonclick = "";
         edtContagemResultado_LoteAceite_Jsonclick = "";
         lstavPagocolaborador_Jsonclick = "";
         bttBtnexlote_Visible = 1;
         edtContagemResultado_AceiteUserNom_Jsonclick = "";
         edtContagemResultado_DataAceite_Jsonclick = "";
         cmbContagemResultado_StatusDmn_Jsonclick = "";
         edtContagemResultado_DataCadastro_Jsonclick = "";
         edtavContagemresultado_ownertelefone_Jsonclick = "";
         edtavContagemresultado_ownertelefone_Enabled = 1;
         edtavContagemresultado_owneremail_Jsonclick = "";
         edtavContagemresultado_owneremail_Enabled = 1;
         dynContagemResultado_Owner_Jsonclick = "";
         edtavResponsavel_Jsonclick = "";
         edtavResponsavel_Enabled = 1;
         edtContagemResultado_Agrupador_Jsonclick = "";
         edtContagemResultado_NaoCnfDmnNom_Jsonclick = "";
         edtContagemResultado_Link_Jsonclick = "";
         edtContagemResultado_FncUsrCod_Jsonclick = "";
         edtavSistema_gestor_Jsonclick = "";
         edtavSistema_gestor_Enabled = 1;
         lblTextblocksistema_gestor_Visible = 1;
         edtModulo_Nome_Jsonclick = "";
         edtContagemrResultado_SistemaSigla_Jsonclick = "";
         edtavContratoservicosunidconversao_sigla_Jsonclick = "";
         edtavContratoservicosunidconversao_sigla_Enabled = 1;
         edtContagemResultado_QuantidadeSolicitada_Jsonclick = "";
         edtContagemResultado_CntSrvQtdUntCns_Jsonclick = "";
         edtContagemResultado_CntSrvUndCntNome_Jsonclick = "";
         edtContagemResultado_ServicoSigla_Jsonclick = "";
         edtContagemResultado_ContadorFSNom_Jsonclick = "";
         edtContagemResultado_ContratadaOrigemPesNom_Jsonclick = "";
         edtContagemResultado_ContratadaPessoaNom_Jsonclick = "";
         edtContagemResultado_CntSrvPrrNome_Jsonclick = "";
         edtContagemResultado_SS_Jsonclick = "";
         edtContagemResultado_DataDmn_Jsonclick = "";
         bttBtnpausarcontinuar_Visible = 1;
         bttBtnnovo_Enabled = 1;
         bttBtnnovo_Visible = 1;
         bttBtndelete_Enabled = 1;
         bttBtndelete_Visible = 1;
         bttBtnupdate_Enabled = 1;
         bttBtnupdate_Visible = 1;
         lblTbjava_Visible = 1;
         lblTextblockcontratoservicosunidconversao_sigla_Caption = "Unidade de Convers�o";
         edtavContratoservicosunidconversao_sigla_Visible = 1;
         edtavSistema_gestor_Visible = 1;
         lstavPagocolaborador.Height = 1;
         lblTbjava_Caption = "Script";
         bttBtnpausarcontinuar_Caption = "Pausar execu��o��II";
         edtContagemResultado_Link_Link = "";
         tblGrpaceite_Visible = 1;
         edtContagemResultado_DataCadastro_Visible = 1;
         tblTblliquidada_Visible = 1;
         edtLote_Nome_Link = "";
         edtContagemResultado_CntSrvUndCntNome_Link = "";
         edtContagemResultado_CntSrvPrrCod_Jsonclick = "";
         edtContagemResultado_CntSrvPrrCod_Visible = 1;
         Innewwindow1_Target = "";
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'A508ContagemResultado_Owner',fld:'CONTAGEMRESULTADO_OWNER',pic:'ZZZZZ9',nv:0},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A2094ContratoServicos_SolicitaGestorSistema',fld:'CONTRATOSERVICOS_SOLICITAGESTORSISTEMA',pic:'',nv:false},{av:'A489ContagemResultado_SistemaCod',fld:'CONTAGEMRESULTADO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A2112ContratoServicosUnidConversao_Sigla',fld:'CONTRATOSERVICOSUNIDCONVERSAO_SIGLA',pic:'@!',nv:''},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV37ContagemResultado_Owner',fld:'vCONTAGEMRESULTADO_OWNER',pic:'ZZZZZ9',nv:0},{av:'A2095Usuario_PessoaTelefone',fld:'USUARIO_PESSOATELEFONE',pic:'',nv:''},{av:'A1647Usuario_Email',fld:'USUARIO_EMAIL',pic:'',nv:''}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV18Caller',fld:'vCALLER',pic:'',nv:''},{ctrl:'BTNFECHAR',prop:'Jsonclick'},{av:'AV37ContagemResultado_Owner',fld:'vCONTAGEMRESULTADO_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV38Sistema_Gestor',fld:'vSISTEMA_GESTOR',pic:'',hsh:true,nv:''},{av:'lblTextblocksistema_gestor_Visible',ctrl:'TEXTBLOCKSISTEMA_GESTOR',prop:'Visible'},{av:'edtavSistema_gestor_Visible',ctrl:'vSISTEMA_GESTOR',prop:'Visible'},{av:'AV43ContratoServicosUnidConversao_Sigla',fld:'vCONTRATOSERVICOSUNIDCONVERSAO_SIGLA',pic:'@!',hsh:true,nv:''},{av:'edtavContratoservicosunidconversao_sigla_Visible',ctrl:'vCONTRATOSERVICOSUNIDCONVERSAO_SIGLA',prop:'Visible'},{av:'lblTextblockcontratoservicosunidconversao_sigla_Caption',ctrl:'TEXTBLOCKCONTRATOSERVICOSUNIDCONVERSAO_SIGLA',prop:'Caption'},{av:'AV36ContagemResultado_OwnerTelefone',fld:'vCONTAGEMRESULTADO_OWNERTELEFONE',pic:'',hsh:true,nv:''},{av:'AV35ContagemResultado_OwnerEmail',fld:'vCONTAGEMRESULTADO_OWNEREMAIL',pic:'',hsh:true,nv:''}]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13B52',iparms:[{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14B52',iparms:[{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOFECHAR'","{handler:'E15B52',iparms:[{av:'AV18Caller',fld:'vCALLER',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("'DONOVO'","{handler:'E20B51',iparms:[{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A457ContagemResultado_Demanda',fld:'CONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''}],oparms:[]}");
         setEventMetadata("'DOPAUSARCONTINUAR'","{handler:'E16B52',iparms:[{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOEXLOTE'","{handler:'E17B52',iparms:[{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOCHCKLST'","{handler:'E21B51',iparms:[{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'Innewwindow1_Target',ctrl:'INNEWWINDOW1',prop:'Target'}]}");
         setEventMetadata("'DOAGENDARREUNIAO'","{handler:'E18B52',iparms:[{av:'AV24isAgendaOK',fld:'vISAGENDAOK',pic:'',nv:false},{av:'A890ContagemResultado_Responsavel',fld:'CONTAGEMRESULTADO_RESPONSAVEL',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV24isAgendaOK',fld:'vISAGENDAOK',pic:'',nv:false}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV52Pgmname = "";
         scmdbuf = "";
         H00B52_A454ContagemResultado_ContadorFSCod = new int[1] ;
         H00B52_n454ContagemResultado_ContadorFSCod = new bool[] {false} ;
         H00B52_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         H00B52_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         H00B52_A146Modulo_Codigo = new int[1] ;
         H00B52_n146Modulo_Codigo = new bool[] {false} ;
         H00B52_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         H00B52_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         H00B52_A1043ContagemResultado_LiqLogCod = new int[1] ;
         H00B52_n1043ContagemResultado_LiqLogCod = new bool[] {false} ;
         H00B52_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00B52_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00B52_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00B52_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00B52_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         H00B52_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         H00B52_A602ContagemResultado_OSVinculada = new int[1] ;
         H00B52_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         H00B52_A457ContagemResultado_Demanda = new String[] {""} ;
         H00B52_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00B52_A489ContagemResultado_SistemaCod = new int[1] ;
         H00B52_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00B52_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00B52_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00B52_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         H00B52_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         H00B52_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         H00B52_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         H00B52_A1350ContagemResultado_DataCadastro = new DateTime[] {DateTime.MinValue} ;
         H00B52_n1350ContagemResultado_DataCadastro = new bool[] {false} ;
         H00B52_A508ContagemResultado_Owner = new int[1] ;
         H00B52_A799ContagemResultado_PFLFSImp = new decimal[1] ;
         H00B52_n799ContagemResultado_PFLFSImp = new bool[] {false} ;
         H00B52_A798ContagemResultado_PFBFSImp = new decimal[1] ;
         H00B52_n798ContagemResultado_PFBFSImp = new bool[] {false} ;
         H00B52_A1046ContagemResultado_Agrupador = new String[] {""} ;
         H00B52_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         H00B52_A465ContagemResultado_Link = new String[] {""} ;
         H00B52_n465ContagemResultado_Link = new bool[] {false} ;
         H00B52_A1044ContagemResultado_FncUsrCod = new int[1] ;
         H00B52_n1044ContagemResultado_FncUsrCod = new bool[] {false} ;
         H00B52_A2133ContagemResultado_QuantidadeSolicitada = new decimal[1] ;
         H00B52_n2133ContagemResultado_QuantidadeSolicitada = new bool[] {false} ;
         H00B52_A1452ContagemResultado_SS = new int[1] ;
         H00B52_n1452ContagemResultado_SS = new bool[] {false} ;
         H00B52_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00B52_A1443ContagemResultado_CntSrvPrrCod = new int[1] ;
         H00B52_n1443ContagemResultado_CntSrvPrrCod = new bool[] {false} ;
         H00B52_A890ContagemResultado_Responsavel = new int[1] ;
         H00B52_n890ContagemResultado_Responsavel = new bool[] {false} ;
         A457ContagemResultado_Demanda = "";
         A484ContagemResultado_StatusDmn = "";
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A1350ContagemResultado_DataCadastro = (DateTime)(DateTime.MinValue);
         A1046ContagemResultado_Agrupador = "";
         A465ContagemResultado_Link = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         H00B53_A480ContagemResultado_CrFSPessoaCod = new int[1] ;
         H00B53_n480ContagemResultado_CrFSPessoaCod = new bool[] {false} ;
         H00B54_A455ContagemResultado_ContadorFSNom = new String[] {""} ;
         H00B54_n455ContagemResultado_ContadorFSNom = new bool[] {false} ;
         A455ContagemResultado_ContadorFSNom = "";
         H00B55_A477ContagemResultado_NaoCnfDmnNom = new String[] {""} ;
         H00B55_n477ContagemResultado_NaoCnfDmnNom = new bool[] {false} ;
         A477ContagemResultado_NaoCnfDmnNom = "";
         H00B56_A143Modulo_Nome = new String[] {""} ;
         A143Modulo_Nome = "";
         H00B57_A807ContagemResultado_ContratadaOrigemPesCod = new int[1] ;
         H00B57_n807ContagemResultado_ContratadaOrigemPesCod = new bool[] {false} ;
         H00B58_A808ContagemResultado_ContratadaOrigemPesNom = new String[] {""} ;
         H00B58_n808ContagemResultado_ContratadaOrigemPesNom = new bool[] {false} ;
         A808ContagemResultado_ContratadaOrigemPesNom = "";
         H00B59_A1034ContagemResultadoLiqLog_Data = new DateTime[] {DateTime.MinValue} ;
         H00B59_n1034ContagemResultadoLiqLog_Data = new bool[] {false} ;
         A1034ContagemResultadoLiqLog_Data = (DateTime)(DateTime.MinValue);
         H00B510_A601ContagemResultado_Servico = new int[1] ;
         H00B510_n601ContagemResultado_Servico = new bool[] {false} ;
         H00B510_A1620ContagemResultado_CntSrvUndCnt = new int[1] ;
         H00B510_n1620ContagemResultado_CntSrvUndCnt = new bool[] {false} ;
         H00B510_A2094ContratoServicos_SolicitaGestorSistema = new bool[] {false} ;
         H00B510_n2094ContratoServicos_SolicitaGestorSistema = new bool[] {false} ;
         H00B510_A1613ContagemResultado_CntSrvQtdUntCns = new decimal[1] ;
         H00B510_n1613ContagemResultado_CntSrvQtdUntCns = new bool[] {false} ;
         H00B511_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         H00B511_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         A801ContagemResultado_ServicoSigla = "";
         H00B512_A2209ContagemResultado_CntSrvUndCntNome = new String[] {""} ;
         H00B512_n2209ContagemResultado_CntSrvUndCntNome = new bool[] {false} ;
         A2209ContagemResultado_CntSrvUndCntNome = "";
         H00B513_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         H00B513_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         H00B513_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00B513_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00B514_A500ContagemResultado_ContratadaPessoaNom = new String[] {""} ;
         H00B514_n500ContagemResultado_ContratadaPessoaNom = new bool[] {false} ;
         A500ContagemResultado_ContratadaPessoaNom = "";
         H00B515_A1547ContagemResultado_LoteNFe = new int[1] ;
         H00B515_n1547ContagemResultado_LoteNFe = new bool[] {false} ;
         H00B515_A525ContagemResultado_AceiteUserCod = new int[1] ;
         H00B515_n525ContagemResultado_AceiteUserCod = new bool[] {false} ;
         H00B515_A529ContagemResultado_DataAceite = new DateTime[] {DateTime.MinValue} ;
         H00B515_n529ContagemResultado_DataAceite = new bool[] {false} ;
         H00B515_A563Lote_Nome = new String[] {""} ;
         H00B515_n563Lote_Nome = new bool[] {false} ;
         H00B515_A528ContagemResultado_LoteAceite = new String[] {""} ;
         H00B515_n528ContagemResultado_LoteAceite = new bool[] {false} ;
         A529ContagemResultado_DataAceite = (DateTime)(DateTime.MinValue);
         A563Lote_Nome = "";
         A528ContagemResultado_LoteAceite = "";
         H00B516_A526ContagemResultado_AceitePessoaCod = new int[1] ;
         H00B516_n526ContagemResultado_AceitePessoaCod = new bool[] {false} ;
         H00B517_A527ContagemResultado_AceiteUserNom = new String[] {""} ;
         H00B517_n527ContagemResultado_AceiteUserNom = new bool[] {false} ;
         A527ContagemResultado_AceiteUserNom = "";
         H00B518_A515ContagemResultado_SistemaCoord = new String[] {""} ;
         H00B518_n515ContagemResultado_SistemaCoord = new bool[] {false} ;
         H00B518_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         H00B518_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         A515ContagemResultado_SistemaCoord = "";
         A509ContagemrResultado_SistemaSigla = "";
         A2091ContagemResultado_CntSrvPrrNome = "";
         H00B519_A1312ContagemResultado_ResponsavelPessCod = new int[1] ;
         H00B519_n1312ContagemResultado_ResponsavelPessCod = new bool[] {false} ;
         H00B520_A1313ContagemResultado_ResponsavelPessNome = new String[] {""} ;
         H00B520_n1313ContagemResultado_ResponsavelPessNome = new bool[] {false} ;
         A1313ContagemResultado_ResponsavelPessNome = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV18Caller = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         A2112ContratoServicosUnidConversao_Sigla = "";
         A2095Usuario_PessoaTelefone = "";
         A1647Usuario_Email = "";
         AV43ContratoServicosUnidConversao_Sigla = "";
         AV38Sistema_Gestor = "";
         AV19Responsavel = "";
         AV35ContagemResultado_OwnerEmail = "";
         AV36ContagemResultado_OwnerTelefone = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         H00B521_A57Usuario_PessoaCod = new int[1] ;
         H00B521_A1Usuario_Codigo = new int[1] ;
         H00B521_A58Usuario_PessoaNom = new String[] {""} ;
         H00B521_n58Usuario_PessoaNom = new bool[] {false} ;
         H00B522_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         H00B522_A511ContagemResultado_HoraCnt = new String[] {""} ;
         H00B522_A454ContagemResultado_ContadorFSCod = new int[1] ;
         H00B522_n454ContagemResultado_ContadorFSCod = new bool[] {false} ;
         H00B522_A480ContagemResultado_CrFSPessoaCod = new int[1] ;
         H00B522_n480ContagemResultado_CrFSPessoaCod = new bool[] {false} ;
         H00B522_A1312ContagemResultado_ResponsavelPessCod = new int[1] ;
         H00B522_n1312ContagemResultado_ResponsavelPessCod = new bool[] {false} ;
         H00B522_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         H00B522_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         H00B522_A146Modulo_Codigo = new int[1] ;
         H00B522_n146Modulo_Codigo = new bool[] {false} ;
         H00B522_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         H00B522_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         H00B522_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         H00B522_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         H00B522_A807ContagemResultado_ContratadaOrigemPesCod = new int[1] ;
         H00B522_n807ContagemResultado_ContratadaOrigemPesCod = new bool[] {false} ;
         H00B522_A526ContagemResultado_AceitePessoaCod = new int[1] ;
         H00B522_n526ContagemResultado_AceitePessoaCod = new bool[] {false} ;
         H00B522_A1043ContagemResultado_LiqLogCod = new int[1] ;
         H00B522_n1043ContagemResultado_LiqLogCod = new bool[] {false} ;
         H00B522_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00B522_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00B522_A470ContagemResultado_ContadorFMCod = new int[1] ;
         H00B522_A456ContagemResultado_Codigo = new int[1] ;
         H00B522_A1034ContagemResultadoLiqLog_Data = new DateTime[] {DateTime.MinValue} ;
         H00B522_n1034ContagemResultadoLiqLog_Data = new bool[] {false} ;
         H00B522_A601ContagemResultado_Servico = new int[1] ;
         H00B522_n601ContagemResultado_Servico = new bool[] {false} ;
         H00B522_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00B522_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00B522_A1620ContagemResultado_CntSrvUndCnt = new int[1] ;
         H00B522_n1620ContagemResultado_CntSrvUndCnt = new bool[] {false} ;
         H00B522_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         H00B522_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         H00B522_A1547ContagemResultado_LoteNFe = new int[1] ;
         H00B522_n1547ContagemResultado_LoteNFe = new bool[] {false} ;
         H00B522_A525ContagemResultado_AceiteUserCod = new int[1] ;
         H00B522_n525ContagemResultado_AceiteUserCod = new bool[] {false} ;
         H00B522_A1000ContagemResultado_CrFMEhContratante = new bool[] {false} ;
         H00B522_n1000ContagemResultado_CrFMEhContratante = new bool[] {false} ;
         H00B522_A1313ContagemResultado_ResponsavelPessNome = new String[] {""} ;
         H00B522_n1313ContagemResultado_ResponsavelPessNome = new bool[] {false} ;
         H00B522_A602ContagemResultado_OSVinculada = new int[1] ;
         H00B522_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         H00B522_A2094ContratoServicos_SolicitaGestorSistema = new bool[] {false} ;
         H00B522_n2094ContratoServicos_SolicitaGestorSistema = new bool[] {false} ;
         H00B522_A457ContagemResultado_Demanda = new String[] {""} ;
         H00B522_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00B522_A489ContagemResultado_SistemaCod = new int[1] ;
         H00B522_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00B522_A527ContagemResultado_AceiteUserNom = new String[] {""} ;
         H00B522_n527ContagemResultado_AceiteUserNom = new bool[] {false} ;
         H00B522_A529ContagemResultado_DataAceite = new DateTime[] {DateTime.MinValue} ;
         H00B522_n529ContagemResultado_DataAceite = new bool[] {false} ;
         H00B522_A563Lote_Nome = new String[] {""} ;
         H00B522_n563Lote_Nome = new bool[] {false} ;
         H00B522_A528ContagemResultado_LoteAceite = new String[] {""} ;
         H00B522_n528ContagemResultado_LoteAceite = new bool[] {false} ;
         H00B522_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00B522_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00B522_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         H00B522_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         H00B522_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         H00B522_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         H00B522_A1350ContagemResultado_DataCadastro = new DateTime[] {DateTime.MinValue} ;
         H00B522_n1350ContagemResultado_DataCadastro = new bool[] {false} ;
         H00B522_A508ContagemResultado_Owner = new int[1] ;
         H00B522_A799ContagemResultado_PFLFSImp = new decimal[1] ;
         H00B522_n799ContagemResultado_PFLFSImp = new bool[] {false} ;
         H00B522_A798ContagemResultado_PFBFSImp = new decimal[1] ;
         H00B522_n798ContagemResultado_PFBFSImp = new bool[] {false} ;
         H00B522_A1046ContagemResultado_Agrupador = new String[] {""} ;
         H00B522_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         H00B522_A477ContagemResultado_NaoCnfDmnNom = new String[] {""} ;
         H00B522_n477ContagemResultado_NaoCnfDmnNom = new bool[] {false} ;
         H00B522_A515ContagemResultado_SistemaCoord = new String[] {""} ;
         H00B522_n515ContagemResultado_SistemaCoord = new bool[] {false} ;
         H00B522_A465ContagemResultado_Link = new String[] {""} ;
         H00B522_n465ContagemResultado_Link = new bool[] {false} ;
         H00B522_A1044ContagemResultado_FncUsrCod = new int[1] ;
         H00B522_n1044ContagemResultado_FncUsrCod = new bool[] {false} ;
         H00B522_A143Modulo_Nome = new String[] {""} ;
         H00B522_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         H00B522_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         H00B522_A2133ContagemResultado_QuantidadeSolicitada = new decimal[1] ;
         H00B522_n2133ContagemResultado_QuantidadeSolicitada = new bool[] {false} ;
         H00B522_A1613ContagemResultado_CntSrvQtdUntCns = new decimal[1] ;
         H00B522_n1613ContagemResultado_CntSrvQtdUntCns = new bool[] {false} ;
         H00B522_A2209ContagemResultado_CntSrvUndCntNome = new String[] {""} ;
         H00B522_n2209ContagemResultado_CntSrvUndCntNome = new bool[] {false} ;
         H00B522_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         H00B522_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         H00B522_A455ContagemResultado_ContadorFSNom = new String[] {""} ;
         H00B522_n455ContagemResultado_ContadorFSNom = new bool[] {false} ;
         H00B522_A808ContagemResultado_ContratadaOrigemPesNom = new String[] {""} ;
         H00B522_n808ContagemResultado_ContratadaOrigemPesNom = new bool[] {false} ;
         H00B522_A500ContagemResultado_ContratadaPessoaNom = new String[] {""} ;
         H00B522_n500ContagemResultado_ContratadaPessoaNom = new bool[] {false} ;
         H00B522_A1452ContagemResultado_SS = new int[1] ;
         H00B522_n1452ContagemResultado_SS = new bool[] {false} ;
         H00B522_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00B522_A1443ContagemResultado_CntSrvPrrCod = new int[1] ;
         H00B522_n1443ContagemResultado_CntSrvPrrCod = new bool[] {false} ;
         H00B522_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00B522_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00B522_A890ContagemResultado_Responsavel = new int[1] ;
         H00B522_n890ContagemResultado_Responsavel = new bool[] {false} ;
         H00B523_A454ContagemResultado_ContadorFSCod = new int[1] ;
         H00B523_n454ContagemResultado_ContadorFSCod = new bool[] {false} ;
         H00B523_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         H00B523_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         H00B523_A146Modulo_Codigo = new int[1] ;
         H00B523_n146Modulo_Codigo = new bool[] {false} ;
         H00B523_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         H00B523_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         H00B523_A1043ContagemResultado_LiqLogCod = new int[1] ;
         H00B523_n1043ContagemResultado_LiqLogCod = new bool[] {false} ;
         H00B523_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00B523_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00B523_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00B523_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00B523_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         H00B523_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         H00B523_A602ContagemResultado_OSVinculada = new int[1] ;
         H00B523_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         H00B523_A457ContagemResultado_Demanda = new String[] {""} ;
         H00B523_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00B523_A489ContagemResultado_SistemaCod = new int[1] ;
         H00B523_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00B523_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00B523_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00B523_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         H00B523_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         H00B523_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         H00B523_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         H00B523_A1350ContagemResultado_DataCadastro = new DateTime[] {DateTime.MinValue} ;
         H00B523_n1350ContagemResultado_DataCadastro = new bool[] {false} ;
         H00B523_A508ContagemResultado_Owner = new int[1] ;
         H00B523_A799ContagemResultado_PFLFSImp = new decimal[1] ;
         H00B523_n799ContagemResultado_PFLFSImp = new bool[] {false} ;
         H00B523_A798ContagemResultado_PFBFSImp = new decimal[1] ;
         H00B523_n798ContagemResultado_PFBFSImp = new bool[] {false} ;
         H00B523_A1046ContagemResultado_Agrupador = new String[] {""} ;
         H00B523_n1046ContagemResultado_Agrupador = new bool[] {false} ;
         H00B523_A465ContagemResultado_Link = new String[] {""} ;
         H00B523_n465ContagemResultado_Link = new bool[] {false} ;
         H00B523_A1044ContagemResultado_FncUsrCod = new int[1] ;
         H00B523_n1044ContagemResultado_FncUsrCod = new bool[] {false} ;
         H00B523_A2133ContagemResultado_QuantidadeSolicitada = new decimal[1] ;
         H00B523_n2133ContagemResultado_QuantidadeSolicitada = new bool[] {false} ;
         H00B523_A1452ContagemResultado_SS = new int[1] ;
         H00B523_n1452ContagemResultado_SS = new bool[] {false} ;
         H00B523_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00B523_A1443ContagemResultado_CntSrvPrrCod = new int[1] ;
         H00B523_n1443ContagemResultado_CntSrvPrrCod = new bool[] {false} ;
         H00B523_A890ContagemResultado_Responsavel = new int[1] ;
         H00B523_n890ContagemResultado_Responsavel = new bool[] {false} ;
         H00B524_A480ContagemResultado_CrFSPessoaCod = new int[1] ;
         H00B524_n480ContagemResultado_CrFSPessoaCod = new bool[] {false} ;
         H00B525_A455ContagemResultado_ContadorFSNom = new String[] {""} ;
         H00B525_n455ContagemResultado_ContadorFSNom = new bool[] {false} ;
         H00B526_A477ContagemResultado_NaoCnfDmnNom = new String[] {""} ;
         H00B526_n477ContagemResultado_NaoCnfDmnNom = new bool[] {false} ;
         H00B527_A143Modulo_Nome = new String[] {""} ;
         H00B528_A807ContagemResultado_ContratadaOrigemPesCod = new int[1] ;
         H00B528_n807ContagemResultado_ContratadaOrigemPesCod = new bool[] {false} ;
         H00B529_A808ContagemResultado_ContratadaOrigemPesNom = new String[] {""} ;
         H00B529_n808ContagemResultado_ContratadaOrigemPesNom = new bool[] {false} ;
         H00B530_A1034ContagemResultadoLiqLog_Data = new DateTime[] {DateTime.MinValue} ;
         H00B530_n1034ContagemResultadoLiqLog_Data = new bool[] {false} ;
         H00B531_A601ContagemResultado_Servico = new int[1] ;
         H00B531_n601ContagemResultado_Servico = new bool[] {false} ;
         H00B531_A1620ContagemResultado_CntSrvUndCnt = new int[1] ;
         H00B531_n1620ContagemResultado_CntSrvUndCnt = new bool[] {false} ;
         H00B531_A2094ContratoServicos_SolicitaGestorSistema = new bool[] {false} ;
         H00B531_n2094ContratoServicos_SolicitaGestorSistema = new bool[] {false} ;
         H00B531_A1613ContagemResultado_CntSrvQtdUntCns = new decimal[1] ;
         H00B531_n1613ContagemResultado_CntSrvQtdUntCns = new bool[] {false} ;
         H00B532_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         H00B532_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         H00B533_A2209ContagemResultado_CntSrvUndCntNome = new String[] {""} ;
         H00B533_n2209ContagemResultado_CntSrvUndCntNome = new bool[] {false} ;
         H00B534_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         H00B534_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         H00B534_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00B534_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00B535_A500ContagemResultado_ContratadaPessoaNom = new String[] {""} ;
         H00B535_n500ContagemResultado_ContratadaPessoaNom = new bool[] {false} ;
         H00B536_A1547ContagemResultado_LoteNFe = new int[1] ;
         H00B536_n1547ContagemResultado_LoteNFe = new bool[] {false} ;
         H00B536_A525ContagemResultado_AceiteUserCod = new int[1] ;
         H00B536_n525ContagemResultado_AceiteUserCod = new bool[] {false} ;
         H00B536_A529ContagemResultado_DataAceite = new DateTime[] {DateTime.MinValue} ;
         H00B536_n529ContagemResultado_DataAceite = new bool[] {false} ;
         H00B536_A563Lote_Nome = new String[] {""} ;
         H00B536_n563Lote_Nome = new bool[] {false} ;
         H00B536_A528ContagemResultado_LoteAceite = new String[] {""} ;
         H00B536_n528ContagemResultado_LoteAceite = new bool[] {false} ;
         H00B537_A526ContagemResultado_AceitePessoaCod = new int[1] ;
         H00B537_n526ContagemResultado_AceitePessoaCod = new bool[] {false} ;
         H00B538_A527ContagemResultado_AceiteUserNom = new String[] {""} ;
         H00B538_n527ContagemResultado_AceiteUserNom = new bool[] {false} ;
         H00B539_A515ContagemResultado_SistemaCoord = new String[] {""} ;
         H00B539_n515ContagemResultado_SistemaCoord = new bool[] {false} ;
         H00B539_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         H00B539_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         H00B540_A1312ContagemResultado_ResponsavelPessCod = new int[1] ;
         H00B540_n1312ContagemResultado_ResponsavelPessCod = new bool[] {false} ;
         H00B541_A1313ContagemResultado_ResponsavelPessNome = new String[] {""} ;
         H00B541_n1313ContagemResultado_ResponsavelPessNome = new bool[] {false} ;
         AV12WebSession = context.GetSession();
         AV27StatusParaPausar = "";
         AV28StatusDemandaVnc = "";
         H00B542_A160ContratoServicos_Codigo = new int[1] ;
         H00B542_A74Contrato_Codigo = new int[1] ;
         H00B542_A155Servico_Codigo = new int[1] ;
         H00B543_A39Contratada_Codigo = new int[1] ;
         H00B544_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00B544_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00B545_A1033ContagemResultadoLiqLog_Codigo = new int[1] ;
         H00B545_A1370ContagemResultadoLiqLogOS_Codigo = new int[1] ;
         H00B545_A1372ContagemResultadoLiqLogOS_UserCod = new int[1] ;
         H00B545_A1374ContagemResultadoLiqLogOS_UserPesCod = new int[1] ;
         H00B545_n1374ContagemResultadoLiqLogOS_UserPesCod = new bool[] {false} ;
         H00B545_A1371ContagemResultadoLiqLogOS_OSCod = new int[1] ;
         H00B545_A1375ContagemResultadoLiqLogOS_Valor = new decimal[1] ;
         H00B545_A1373ContagemResultadoLiqLogOS_UserNom = new String[] {""} ;
         H00B545_n1373ContagemResultadoLiqLogOS_UserNom = new bool[] {false} ;
         A1373ContagemResultadoLiqLogOS_UserNom = "";
         AV34txt = "";
         AV39Servico_IsSolicitaGestorSistema = false;
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         bttBtnfechar_Jsonclick = "";
         H00B546_A160ContratoServicos_Codigo = new int[1] ;
         H00B546_A2094ContratoServicos_SolicitaGestorSistema = new bool[] {false} ;
         H00B546_n2094ContratoServicos_SolicitaGestorSistema = new bool[] {false} ;
         GXt_char1 = "";
         H00B547_A2110ContratoServicosUnidConversao_Codigo = new int[1] ;
         H00B547_A160ContratoServicos_Codigo = new int[1] ;
         H00B547_A2112ContratoServicosUnidConversao_Sigla = new String[] {""} ;
         H00B547_n2112ContratoServicosUnidConversao_Sigla = new bool[] {false} ;
         H00B548_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00B548_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         H00B548_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         H00B548_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00B548_A438Contratada_Sigla = new String[] {""} ;
         H00B548_n438Contratada_Sigla = new bool[] {false} ;
         A438Contratada_Sigla = "";
         H00B549_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00B549_A1018ContratadaUsuario_UsuarioEhContratada = new bool[] {false} ;
         H00B549_n1018ContratadaUsuario_UsuarioEhContratada = new bool[] {false} ;
         H00B549_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         H00B549_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         H00B549_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00B549_A438Contratada_Sigla = new String[] {""} ;
         H00B549_n438Contratada_Sigla = new bool[] {false} ;
         AV31ContagemResultado = new SdtContagemResultado(context);
         H00B550_A57Usuario_PessoaCod = new int[1] ;
         H00B550_A1Usuario_Codigo = new int[1] ;
         H00B550_A2095Usuario_PessoaTelefone = new String[] {""} ;
         H00B550_n2095Usuario_PessoaTelefone = new bool[] {false} ;
         H00B550_A1647Usuario_Email = new String[] {""} ;
         H00B550_n1647Usuario_Email = new bool[] {false} ;
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTbjava_Jsonclick = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         bttBtnnovo_Jsonclick = "";
         bttBtnpausarcontinuar_Jsonclick = "";
         lblTextblockcontagemresultado_datadmn_Jsonclick = "";
         lblTextblockcontagemresultado_ss_Jsonclick = "";
         lblTextblockcontagemresultado_cntsrvprrnome_Jsonclick = "";
         lblTextblockcontagemresultado_contratadapessoanom_Jsonclick = "";
         lblTextblockcontagemresultado_contratadaorigempesnom_Jsonclick = "";
         lblTextblockcontagemresultado_contadorfsnom_Jsonclick = "";
         lblTextblockcontagemresultado_servicosigla_Jsonclick = "";
         lblTextblockcontagemresultado_cntsrvundcntnome_Jsonclick = "";
         lblTextblockcontagemresultado_cntsrvqtduntcns_Jsonclick = "";
         lblTextblockcontagemresultado_quantidadesolicitada_Jsonclick = "";
         lblTextblockcontratoservicosunidconversao_sigla_Jsonclick = "";
         lblTextblockcontagemrresultado_sistemasigla_Jsonclick = "";
         lblTextblockmodulo_nome_Jsonclick = "";
         lblTextblocksistema_gestor_Jsonclick = "";
         lblTextblockcontagemresultado_fncusrcod_Jsonclick = "";
         lblTextblockcontagemresultado_link_Jsonclick = "";
         lblTextblockcontagemresultado_sistemacoord_Jsonclick = "";
         lblTextblockcontagemresultado_naocnfdmnnom_Jsonclick = "";
         lblTextblockcontagemresultado_agrupador_Jsonclick = "";
         lblTextblockcontagemresultado_pfbfsimp_Jsonclick = "";
         lblTextblockresponsavel_Jsonclick = "";
         lblTextblockcontagemresultado_owner_Jsonclick = "";
         lblTextblockcontagemresultado_owneremail_Jsonclick = "";
         lblTextblockcontagemresultado_ownertelefone_Jsonclick = "";
         lblTextblockcontagemresultado_datacadastro_Jsonclick = "";
         lblTextblockcontagemresultado_dataentrega_Jsonclick = "";
         lblTextblockcontagemresultado_statusdmn_Jsonclick = "";
         lblTextblockcontagemresultado_loteaceite_Jsonclick = "";
         lblTextblockcontagemresultado_dataaceite_Jsonclick = "";
         lblTextblockcontagemresultado_aceiteusernom_Jsonclick = "";
         bttBtnexlote_Jsonclick = "";
         lblTextblockpagocolaborador_Jsonclick = "";
         lblTextblockcontagemresultado_pflfsimp_Jsonclick = "";
         imgChcklst_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA456ContagemResultado_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadogeneral__default(),
            new Object[][] {
                new Object[] {
               H00B52_A454ContagemResultado_ContadorFSCod, H00B52_n454ContagemResultado_ContadorFSCod, H00B52_A468ContagemResultado_NaoCnfDmnCod, H00B52_n468ContagemResultado_NaoCnfDmnCod, H00B52_A146Modulo_Codigo, H00B52_n146Modulo_Codigo, H00B52_A805ContagemResultado_ContratadaOrigemCod, H00B52_n805ContagemResultado_ContratadaOrigemCod, H00B52_A1043ContagemResultado_LiqLogCod, H00B52_n1043ContagemResultado_LiqLogCod,
               H00B52_A1553ContagemResultado_CntSrvCod, H00B52_n1553ContagemResultado_CntSrvCod, H00B52_A490ContagemResultado_ContratadaCod, H00B52_n490ContagemResultado_ContratadaCod, H00B52_A597ContagemResultado_LoteAceiteCod, H00B52_n597ContagemResultado_LoteAceiteCod, H00B52_A602ContagemResultado_OSVinculada, H00B52_n602ContagemResultado_OSVinculada, H00B52_A457ContagemResultado_Demanda, H00B52_n457ContagemResultado_Demanda,
               H00B52_A489ContagemResultado_SistemaCod, H00B52_n489ContagemResultado_SistemaCod, H00B52_A484ContagemResultado_StatusDmn, H00B52_n484ContagemResultado_StatusDmn, H00B52_A912ContagemResultado_HoraEntrega, H00B52_n912ContagemResultado_HoraEntrega, H00B52_A472ContagemResultado_DataEntrega, H00B52_n472ContagemResultado_DataEntrega, H00B52_A1350ContagemResultado_DataCadastro, H00B52_n1350ContagemResultado_DataCadastro,
               H00B52_A508ContagemResultado_Owner, H00B52_A799ContagemResultado_PFLFSImp, H00B52_n799ContagemResultado_PFLFSImp, H00B52_A798ContagemResultado_PFBFSImp, H00B52_n798ContagemResultado_PFBFSImp, H00B52_A1046ContagemResultado_Agrupador, H00B52_n1046ContagemResultado_Agrupador, H00B52_A465ContagemResultado_Link, H00B52_n465ContagemResultado_Link, H00B52_A1044ContagemResultado_FncUsrCod,
               H00B52_n1044ContagemResultado_FncUsrCod, H00B52_A2133ContagemResultado_QuantidadeSolicitada, H00B52_n2133ContagemResultado_QuantidadeSolicitada, H00B52_A1452ContagemResultado_SS, H00B52_n1452ContagemResultado_SS, H00B52_A471ContagemResultado_DataDmn, H00B52_A1443ContagemResultado_CntSrvPrrCod, H00B52_n1443ContagemResultado_CntSrvPrrCod, H00B52_A890ContagemResultado_Responsavel, H00B52_n890ContagemResultado_Responsavel
               }
               , new Object[] {
               H00B53_A480ContagemResultado_CrFSPessoaCod, H00B53_n480ContagemResultado_CrFSPessoaCod
               }
               , new Object[] {
               H00B54_A455ContagemResultado_ContadorFSNom, H00B54_n455ContagemResultado_ContadorFSNom
               }
               , new Object[] {
               H00B55_A477ContagemResultado_NaoCnfDmnNom, H00B55_n477ContagemResultado_NaoCnfDmnNom
               }
               , new Object[] {
               H00B56_A143Modulo_Nome
               }
               , new Object[] {
               H00B57_A807ContagemResultado_ContratadaOrigemPesCod, H00B57_n807ContagemResultado_ContratadaOrigemPesCod
               }
               , new Object[] {
               H00B58_A808ContagemResultado_ContratadaOrigemPesNom, H00B58_n808ContagemResultado_ContratadaOrigemPesNom
               }
               , new Object[] {
               H00B59_A1034ContagemResultadoLiqLog_Data, H00B59_n1034ContagemResultadoLiqLog_Data
               }
               , new Object[] {
               H00B510_A601ContagemResultado_Servico, H00B510_n601ContagemResultado_Servico, H00B510_A1620ContagemResultado_CntSrvUndCnt, H00B510_n1620ContagemResultado_CntSrvUndCnt, H00B510_A2094ContratoServicos_SolicitaGestorSistema, H00B510_n2094ContratoServicos_SolicitaGestorSistema, H00B510_A1613ContagemResultado_CntSrvQtdUntCns, H00B510_n1613ContagemResultado_CntSrvQtdUntCns
               }
               , new Object[] {
               H00B511_A801ContagemResultado_ServicoSigla, H00B511_n801ContagemResultado_ServicoSigla
               }
               , new Object[] {
               H00B512_A2209ContagemResultado_CntSrvUndCntNome, H00B512_n2209ContagemResultado_CntSrvUndCntNome
               }
               , new Object[] {
               H00B513_A499ContagemResultado_ContratadaPessoaCod, H00B513_n499ContagemResultado_ContratadaPessoaCod, H00B513_A52Contratada_AreaTrabalhoCod, H00B513_n52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               H00B514_A500ContagemResultado_ContratadaPessoaNom, H00B514_n500ContagemResultado_ContratadaPessoaNom
               }
               , new Object[] {
               H00B515_A1547ContagemResultado_LoteNFe, H00B515_n1547ContagemResultado_LoteNFe, H00B515_A525ContagemResultado_AceiteUserCod, H00B515_n525ContagemResultado_AceiteUserCod, H00B515_A529ContagemResultado_DataAceite, H00B515_n529ContagemResultado_DataAceite, H00B515_A563Lote_Nome, H00B515_n563Lote_Nome, H00B515_A528ContagemResultado_LoteAceite, H00B515_n528ContagemResultado_LoteAceite
               }
               , new Object[] {
               H00B516_A526ContagemResultado_AceitePessoaCod, H00B516_n526ContagemResultado_AceitePessoaCod
               }
               , new Object[] {
               H00B517_A527ContagemResultado_AceiteUserNom, H00B517_n527ContagemResultado_AceiteUserNom
               }
               , new Object[] {
               H00B518_A515ContagemResultado_SistemaCoord, H00B518_n515ContagemResultado_SistemaCoord, H00B518_A509ContagemrResultado_SistemaSigla, H00B518_n509ContagemrResultado_SistemaSigla
               }
               , new Object[] {
               H00B519_A1312ContagemResultado_ResponsavelPessCod, H00B519_n1312ContagemResultado_ResponsavelPessCod
               }
               , new Object[] {
               H00B520_A1313ContagemResultado_ResponsavelPessNome, H00B520_n1313ContagemResultado_ResponsavelPessNome
               }
               , new Object[] {
               H00B521_A57Usuario_PessoaCod, H00B521_A1Usuario_Codigo, H00B521_A58Usuario_PessoaNom, H00B521_n58Usuario_PessoaNom
               }
               , new Object[] {
               H00B522_A473ContagemResultado_DataCnt, H00B522_A511ContagemResultado_HoraCnt, H00B522_A454ContagemResultado_ContadorFSCod, H00B522_n454ContagemResultado_ContadorFSCod, H00B522_A480ContagemResultado_CrFSPessoaCod, H00B522_n480ContagemResultado_CrFSPessoaCod, H00B522_A1312ContagemResultado_ResponsavelPessCod, H00B522_n1312ContagemResultado_ResponsavelPessCod, H00B522_A468ContagemResultado_NaoCnfDmnCod, H00B522_n468ContagemResultado_NaoCnfDmnCod,
               H00B522_A146Modulo_Codigo, H00B522_n146Modulo_Codigo, H00B522_A499ContagemResultado_ContratadaPessoaCod, H00B522_n499ContagemResultado_ContratadaPessoaCod, H00B522_A805ContagemResultado_ContratadaOrigemCod, H00B522_n805ContagemResultado_ContratadaOrigemCod, H00B522_A807ContagemResultado_ContratadaOrigemPesCod, H00B522_n807ContagemResultado_ContratadaOrigemPesCod, H00B522_A526ContagemResultado_AceitePessoaCod, H00B522_n526ContagemResultado_AceitePessoaCod,
               H00B522_A1043ContagemResultado_LiqLogCod, H00B522_n1043ContagemResultado_LiqLogCod, H00B522_A1553ContagemResultado_CntSrvCod, H00B522_n1553ContagemResultado_CntSrvCod, H00B522_A470ContagemResultado_ContadorFMCod, H00B522_A456ContagemResultado_Codigo, H00B522_A1034ContagemResultadoLiqLog_Data, H00B522_n1034ContagemResultadoLiqLog_Data, H00B522_A601ContagemResultado_Servico, H00B522_n601ContagemResultado_Servico,
               H00B522_A490ContagemResultado_ContratadaCod, H00B522_n490ContagemResultado_ContratadaCod, H00B522_A1620ContagemResultado_CntSrvUndCnt, H00B522_n1620ContagemResultado_CntSrvUndCnt, H00B522_A597ContagemResultado_LoteAceiteCod, H00B522_n597ContagemResultado_LoteAceiteCod, H00B522_A1547ContagemResultado_LoteNFe, H00B522_n1547ContagemResultado_LoteNFe, H00B522_A525ContagemResultado_AceiteUserCod, H00B522_n525ContagemResultado_AceiteUserCod,
               H00B522_A1000ContagemResultado_CrFMEhContratante, H00B522_n1000ContagemResultado_CrFMEhContratante, H00B522_A1313ContagemResultado_ResponsavelPessNome, H00B522_n1313ContagemResultado_ResponsavelPessNome, H00B522_A602ContagemResultado_OSVinculada, H00B522_n602ContagemResultado_OSVinculada, H00B522_A2094ContratoServicos_SolicitaGestorSistema, H00B522_n2094ContratoServicos_SolicitaGestorSistema, H00B522_A457ContagemResultado_Demanda, H00B522_n457ContagemResultado_Demanda,
               H00B522_A489ContagemResultado_SistemaCod, H00B522_n489ContagemResultado_SistemaCod, H00B522_A527ContagemResultado_AceiteUserNom, H00B522_n527ContagemResultado_AceiteUserNom, H00B522_A529ContagemResultado_DataAceite, H00B522_n529ContagemResultado_DataAceite, H00B522_A563Lote_Nome, H00B522_n563Lote_Nome, H00B522_A528ContagemResultado_LoteAceite, H00B522_n528ContagemResultado_LoteAceite,
               H00B522_A484ContagemResultado_StatusDmn, H00B522_n484ContagemResultado_StatusDmn, H00B522_A912ContagemResultado_HoraEntrega, H00B522_n912ContagemResultado_HoraEntrega, H00B522_A472ContagemResultado_DataEntrega, H00B522_n472ContagemResultado_DataEntrega, H00B522_A1350ContagemResultado_DataCadastro, H00B522_n1350ContagemResultado_DataCadastro, H00B522_A508ContagemResultado_Owner, H00B522_A799ContagemResultado_PFLFSImp,
               H00B522_n799ContagemResultado_PFLFSImp, H00B522_A798ContagemResultado_PFBFSImp, H00B522_n798ContagemResultado_PFBFSImp, H00B522_A1046ContagemResultado_Agrupador, H00B522_n1046ContagemResultado_Agrupador, H00B522_A477ContagemResultado_NaoCnfDmnNom, H00B522_n477ContagemResultado_NaoCnfDmnNom, H00B522_A515ContagemResultado_SistemaCoord, H00B522_n515ContagemResultado_SistemaCoord, H00B522_A465ContagemResultado_Link,
               H00B522_n465ContagemResultado_Link, H00B522_A1044ContagemResultado_FncUsrCod, H00B522_n1044ContagemResultado_FncUsrCod, H00B522_A143Modulo_Nome, H00B522_A509ContagemrResultado_SistemaSigla, H00B522_n509ContagemrResultado_SistemaSigla, H00B522_A2133ContagemResultado_QuantidadeSolicitada, H00B522_n2133ContagemResultado_QuantidadeSolicitada, H00B522_A1613ContagemResultado_CntSrvQtdUntCns, H00B522_n1613ContagemResultado_CntSrvQtdUntCns,
               H00B522_A2209ContagemResultado_CntSrvUndCntNome, H00B522_n2209ContagemResultado_CntSrvUndCntNome, H00B522_A801ContagemResultado_ServicoSigla, H00B522_n801ContagemResultado_ServicoSigla, H00B522_A455ContagemResultado_ContadorFSNom, H00B522_n455ContagemResultado_ContadorFSNom, H00B522_A808ContagemResultado_ContratadaOrigemPesNom, H00B522_n808ContagemResultado_ContratadaOrigemPesNom, H00B522_A500ContagemResultado_ContratadaPessoaNom, H00B522_n500ContagemResultado_ContratadaPessoaNom,
               H00B522_A1452ContagemResultado_SS, H00B522_n1452ContagemResultado_SS, H00B522_A471ContagemResultado_DataDmn, H00B522_A1443ContagemResultado_CntSrvPrrCod, H00B522_n1443ContagemResultado_CntSrvPrrCod, H00B522_A52Contratada_AreaTrabalhoCod, H00B522_n52Contratada_AreaTrabalhoCod, H00B522_A890ContagemResultado_Responsavel, H00B522_n890ContagemResultado_Responsavel
               }
               , new Object[] {
               H00B523_A454ContagemResultado_ContadorFSCod, H00B523_n454ContagemResultado_ContadorFSCod, H00B523_A468ContagemResultado_NaoCnfDmnCod, H00B523_n468ContagemResultado_NaoCnfDmnCod, H00B523_A146Modulo_Codigo, H00B523_n146Modulo_Codigo, H00B523_A805ContagemResultado_ContratadaOrigemCod, H00B523_n805ContagemResultado_ContratadaOrigemCod, H00B523_A1043ContagemResultado_LiqLogCod, H00B523_n1043ContagemResultado_LiqLogCod,
               H00B523_A1553ContagemResultado_CntSrvCod, H00B523_n1553ContagemResultado_CntSrvCod, H00B523_A490ContagemResultado_ContratadaCod, H00B523_n490ContagemResultado_ContratadaCod, H00B523_A597ContagemResultado_LoteAceiteCod, H00B523_n597ContagemResultado_LoteAceiteCod, H00B523_A602ContagemResultado_OSVinculada, H00B523_n602ContagemResultado_OSVinculada, H00B523_A457ContagemResultado_Demanda, H00B523_n457ContagemResultado_Demanda,
               H00B523_A489ContagemResultado_SistemaCod, H00B523_n489ContagemResultado_SistemaCod, H00B523_A484ContagemResultado_StatusDmn, H00B523_n484ContagemResultado_StatusDmn, H00B523_A912ContagemResultado_HoraEntrega, H00B523_n912ContagemResultado_HoraEntrega, H00B523_A472ContagemResultado_DataEntrega, H00B523_n472ContagemResultado_DataEntrega, H00B523_A1350ContagemResultado_DataCadastro, H00B523_n1350ContagemResultado_DataCadastro,
               H00B523_A508ContagemResultado_Owner, H00B523_A799ContagemResultado_PFLFSImp, H00B523_n799ContagemResultado_PFLFSImp, H00B523_A798ContagemResultado_PFBFSImp, H00B523_n798ContagemResultado_PFBFSImp, H00B523_A1046ContagemResultado_Agrupador, H00B523_n1046ContagemResultado_Agrupador, H00B523_A465ContagemResultado_Link, H00B523_n465ContagemResultado_Link, H00B523_A1044ContagemResultado_FncUsrCod,
               H00B523_n1044ContagemResultado_FncUsrCod, H00B523_A2133ContagemResultado_QuantidadeSolicitada, H00B523_n2133ContagemResultado_QuantidadeSolicitada, H00B523_A1452ContagemResultado_SS, H00B523_n1452ContagemResultado_SS, H00B523_A471ContagemResultado_DataDmn, H00B523_A1443ContagemResultado_CntSrvPrrCod, H00B523_n1443ContagemResultado_CntSrvPrrCod, H00B523_A890ContagemResultado_Responsavel, H00B523_n890ContagemResultado_Responsavel
               }
               , new Object[] {
               H00B524_A480ContagemResultado_CrFSPessoaCod, H00B524_n480ContagemResultado_CrFSPessoaCod
               }
               , new Object[] {
               H00B525_A455ContagemResultado_ContadorFSNom, H00B525_n455ContagemResultado_ContadorFSNom
               }
               , new Object[] {
               H00B526_A477ContagemResultado_NaoCnfDmnNom, H00B526_n477ContagemResultado_NaoCnfDmnNom
               }
               , new Object[] {
               H00B527_A143Modulo_Nome
               }
               , new Object[] {
               H00B528_A807ContagemResultado_ContratadaOrigemPesCod, H00B528_n807ContagemResultado_ContratadaOrigemPesCod
               }
               , new Object[] {
               H00B529_A808ContagemResultado_ContratadaOrigemPesNom, H00B529_n808ContagemResultado_ContratadaOrigemPesNom
               }
               , new Object[] {
               H00B530_A1034ContagemResultadoLiqLog_Data, H00B530_n1034ContagemResultadoLiqLog_Data
               }
               , new Object[] {
               H00B531_A601ContagemResultado_Servico, H00B531_n601ContagemResultado_Servico, H00B531_A1620ContagemResultado_CntSrvUndCnt, H00B531_n1620ContagemResultado_CntSrvUndCnt, H00B531_A2094ContratoServicos_SolicitaGestorSistema, H00B531_n2094ContratoServicos_SolicitaGestorSistema, H00B531_A1613ContagemResultado_CntSrvQtdUntCns, H00B531_n1613ContagemResultado_CntSrvQtdUntCns
               }
               , new Object[] {
               H00B532_A801ContagemResultado_ServicoSigla, H00B532_n801ContagemResultado_ServicoSigla
               }
               , new Object[] {
               H00B533_A2209ContagemResultado_CntSrvUndCntNome, H00B533_n2209ContagemResultado_CntSrvUndCntNome
               }
               , new Object[] {
               H00B534_A499ContagemResultado_ContratadaPessoaCod, H00B534_n499ContagemResultado_ContratadaPessoaCod, H00B534_A52Contratada_AreaTrabalhoCod, H00B534_n52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               H00B535_A500ContagemResultado_ContratadaPessoaNom, H00B535_n500ContagemResultado_ContratadaPessoaNom
               }
               , new Object[] {
               H00B536_A1547ContagemResultado_LoteNFe, H00B536_n1547ContagemResultado_LoteNFe, H00B536_A525ContagemResultado_AceiteUserCod, H00B536_n525ContagemResultado_AceiteUserCod, H00B536_A529ContagemResultado_DataAceite, H00B536_n529ContagemResultado_DataAceite, H00B536_A563Lote_Nome, H00B536_n563Lote_Nome, H00B536_A528ContagemResultado_LoteAceite, H00B536_n528ContagemResultado_LoteAceite
               }
               , new Object[] {
               H00B537_A526ContagemResultado_AceitePessoaCod, H00B537_n526ContagemResultado_AceitePessoaCod
               }
               , new Object[] {
               H00B538_A527ContagemResultado_AceiteUserNom, H00B538_n527ContagemResultado_AceiteUserNom
               }
               , new Object[] {
               H00B539_A515ContagemResultado_SistemaCoord, H00B539_n515ContagemResultado_SistemaCoord, H00B539_A509ContagemrResultado_SistemaSigla, H00B539_n509ContagemrResultado_SistemaSigla
               }
               , new Object[] {
               H00B540_A1312ContagemResultado_ResponsavelPessCod, H00B540_n1312ContagemResultado_ResponsavelPessCod
               }
               , new Object[] {
               H00B541_A1313ContagemResultado_ResponsavelPessNome, H00B541_n1313ContagemResultado_ResponsavelPessNome
               }
               , new Object[] {
               H00B542_A160ContratoServicos_Codigo, H00B542_A74Contrato_Codigo, H00B542_A155Servico_Codigo
               }
               , new Object[] {
               H00B543_A39Contratada_Codigo
               }
               , new Object[] {
               H00B544_A1078ContratoGestor_ContratoCod, H00B544_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               H00B545_A1033ContagemResultadoLiqLog_Codigo, H00B545_A1370ContagemResultadoLiqLogOS_Codigo, H00B545_A1372ContagemResultadoLiqLogOS_UserCod, H00B545_A1374ContagemResultadoLiqLogOS_UserPesCod, H00B545_n1374ContagemResultadoLiqLogOS_UserPesCod, H00B545_A1371ContagemResultadoLiqLogOS_OSCod, H00B545_A1375ContagemResultadoLiqLogOS_Valor, H00B545_A1373ContagemResultadoLiqLogOS_UserNom, H00B545_n1373ContagemResultadoLiqLogOS_UserNom
               }
               , new Object[] {
               H00B546_A160ContratoServicos_Codigo, H00B546_A2094ContratoServicos_SolicitaGestorSistema
               }
               , new Object[] {
               H00B547_A2110ContratoServicosUnidConversao_Codigo, H00B547_A160ContratoServicos_Codigo, H00B547_A2112ContratoServicosUnidConversao_Sigla, H00B547_n2112ContratoServicosUnidConversao_Sigla
               }
               , new Object[] {
               H00B548_A66ContratadaUsuario_ContratadaCod, H00B548_A1228ContratadaUsuario_AreaTrabalhoCod, H00B548_n1228ContratadaUsuario_AreaTrabalhoCod, H00B548_A69ContratadaUsuario_UsuarioCod, H00B548_A438Contratada_Sigla, H00B548_n438Contratada_Sigla
               }
               , new Object[] {
               H00B549_A66ContratadaUsuario_ContratadaCod, H00B549_A1018ContratadaUsuario_UsuarioEhContratada, H00B549_n1018ContratadaUsuario_UsuarioEhContratada, H00B549_A1228ContratadaUsuario_AreaTrabalhoCod, H00B549_n1228ContratadaUsuario_AreaTrabalhoCod, H00B549_A69ContratadaUsuario_UsuarioCod, H00B549_A438Contratada_Sigla, H00B549_n438Contratada_Sigla
               }
               , new Object[] {
               H00B550_A57Usuario_PessoaCod, H00B550_A1Usuario_Codigo, H00B550_A2095Usuario_PessoaTelefone, H00B550_n2095Usuario_PessoaTelefone, H00B550_A1647Usuario_Email, H00B550_n1647Usuario_Email
               }
            }
         );
         AV52Pgmname = "ContagemResultadoGeneral";
         /* GeneXus formulas. */
         AV52Pgmname = "ContagemResultadoGeneral";
         context.Gx_err = 0;
         edtavContratoservicosunidconversao_sigla_Enabled = 0;
         edtavSistema_gestor_Enabled = 0;
         edtavResponsavel_Enabled = 0;
         edtavContagemresultado_owneremail_Enabled = 0;
         edtavContagemresultado_ownertelefone_Enabled = 0;
      }

      private short nRcdExists_5 ;
      private short nIsMod_5 ;
      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV32PagoColaborador ;
      private short AV26StatusContagemVnc ;
      private short AV33i ;
      private short AV57GXLvl360 ;
      private short nGXWrapped ;
      private int A456ContagemResultado_Codigo ;
      private int wcpOA456ContagemResultado_Codigo ;
      private int edtavContratoservicosunidconversao_sigla_Enabled ;
      private int edtavSistema_gestor_Enabled ;
      private int edtavResponsavel_Enabled ;
      private int edtavContagemresultado_owneremail_Enabled ;
      private int edtavContagemresultado_ownertelefone_Enabled ;
      private int A454ContagemResultado_ContadorFSCod ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A146Modulo_Codigo ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int A1043ContagemResultado_LiqLogCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A602ContagemResultado_OSVinculada ;
      private int A489ContagemResultado_SistemaCod ;
      private int A508ContagemResultado_Owner ;
      private int A1044ContagemResultado_FncUsrCod ;
      private int A1452ContagemResultado_SS ;
      private int A1443ContagemResultado_CntSrvPrrCod ;
      private int A890ContagemResultado_Responsavel ;
      private int A480ContagemResultado_CrFSPessoaCod ;
      private int A807ContagemResultado_ContratadaOrigemPesCod ;
      private int A601ContagemResultado_Servico ;
      private int A1620ContagemResultado_CntSrvUndCnt ;
      private int A499ContagemResultado_ContratadaPessoaCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A1547ContagemResultado_LoteNFe ;
      private int A525ContagemResultado_AceiteUserCod ;
      private int A526ContagemResultado_AceitePessoaCod ;
      private int A1312ContagemResultado_ResponsavelPessCod ;
      private int A1229ContagemResultado_ContratadaDoResponsavel ;
      private int A160ContratoServicos_Codigo ;
      private int A1Usuario_Codigo ;
      private int AV37ContagemResultado_Owner ;
      private int A39Contratada_Codigo ;
      private int edtContagemResultado_CntSrvPrrCod_Visible ;
      private int gxdynajaxindex ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int lblTbjava_Visible ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int bttBtnnovo_Visible ;
      private int bttBtnpausarcontinuar_Visible ;
      private int bttBtnnovo_Enabled ;
      private int tblTblliquidada_Visible ;
      private int bttBtnexlote_Visible ;
      private int edtContagemResultado_DataCadastro_Visible ;
      private int tblGrpaceite_Visible ;
      private int AV21ContagemResultado_Responsavel ;
      private int AV22ContratadaDoResponsavel ;
      private int bttBtnupdate_Enabled ;
      private int bttBtndelete_Enabled ;
      private int imgChcklst_Visible ;
      private int AV30Codigo ;
      private int GXt_int2 ;
      private int A74Contrato_Codigo ;
      private int A155Servico_Codigo ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A1372ContagemResultadoLiqLogOS_UserCod ;
      private int A1374ContagemResultadoLiqLogOS_UserPesCod ;
      private int A1371ContagemResultadoLiqLogOS_OSCod ;
      private int AV7ContagemResultado_Codigo ;
      private int lblTextblocksistema_gestor_Visible ;
      private int edtavSistema_gestor_Visible ;
      private int A2110ContratoServicosUnidConversao_Codigo ;
      private int edtavContratoservicosunidconversao_sigla_Visible ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A57Usuario_PessoaCod ;
      private int idxLst ;
      private decimal A799ContagemResultado_PFLFSImp ;
      private decimal A798ContagemResultado_PFBFSImp ;
      private decimal A2133ContagemResultado_QuantidadeSolicitada ;
      private decimal A1613ContagemResultado_CntSrvQtdUntCns ;
      private decimal A1375ContagemResultadoLiqLogOS_Valor ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV52Pgmname ;
      private String edtavContratoservicosunidconversao_sigla_Internalname ;
      private String edtavSistema_gestor_Internalname ;
      private String edtavResponsavel_Internalname ;
      private String edtavContagemresultado_owneremail_Internalname ;
      private String edtavContagemresultado_ownertelefone_Internalname ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private String A1046ContagemResultado_Agrupador ;
      private String A455ContagemResultado_ContadorFSNom ;
      private String A477ContagemResultado_NaoCnfDmnNom ;
      private String A143Modulo_Nome ;
      private String A808ContagemResultado_ContratadaOrigemPesNom ;
      private String A801ContagemResultado_ServicoSigla ;
      private String A2209ContagemResultado_CntSrvUndCntNome ;
      private String A500ContagemResultado_ContratadaPessoaNom ;
      private String A563Lote_Nome ;
      private String A528ContagemResultado_LoteAceite ;
      private String A527ContagemResultado_AceiteUserNom ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String A2091ContagemResultado_CntSrvPrrNome ;
      private String A1313ContagemResultado_ResponsavelPessNome ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV18Caller ;
      private String A2112ContratoServicosUnidConversao_Sigla ;
      private String A2095Usuario_PessoaTelefone ;
      private String AV43ContratoServicosUnidConversao_Sigla ;
      private String AV19Responsavel ;
      private String Innewwindow1_Target ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String edtContagemResultado_CntSrvPrrCod_Internalname ;
      private String edtContagemResultado_CntSrvPrrCod_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String gxwrpcisep ;
      private String edtContagemResultado_DataDmn_Internalname ;
      private String edtContagemResultado_SS_Internalname ;
      private String edtContagemResultado_CntSrvPrrNome_Internalname ;
      private String edtContagemResultado_ContratadaPessoaNom_Internalname ;
      private String edtContagemResultado_ContratadaOrigemPesNom_Internalname ;
      private String edtContagemResultado_ContadorFSNom_Internalname ;
      private String edtContagemResultado_ServicoSigla_Internalname ;
      private String edtContagemResultado_CntSrvUndCntNome_Internalname ;
      private String edtContagemResultado_CntSrvQtdUntCns_Internalname ;
      private String edtContagemResultado_QuantidadeSolicitada_Internalname ;
      private String edtContagemrResultado_SistemaSigla_Internalname ;
      private String edtModulo_Nome_Internalname ;
      private String edtContagemResultado_FncUsrCod_Internalname ;
      private String edtContagemResultado_Link_Internalname ;
      private String edtContagemResultado_SistemaCoord_Internalname ;
      private String edtContagemResultado_NaoCnfDmnNom_Internalname ;
      private String edtContagemResultado_Agrupador_Internalname ;
      private String edtContagemResultado_PFBFSImp_Internalname ;
      private String edtContagemResultado_PFLFSImp_Internalname ;
      private String dynContagemResultado_Owner_Internalname ;
      private String edtContagemResultado_DataCadastro_Internalname ;
      private String edtContagemResultado_DataEntrega_Internalname ;
      private String edtContagemResultado_HoraEntrega_Internalname ;
      private String cmbContagemResultado_StatusDmn_Internalname ;
      private String edtContagemResultado_LoteAceite_Internalname ;
      private String edtLote_Nome_Internalname ;
      private String edtContagemResultado_DataAceite_Internalname ;
      private String edtContagemResultado_AceiteUserNom_Internalname ;
      private String lstavPagocolaborador_Internalname ;
      private String lblTbjava_Internalname ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String bttBtnnovo_Internalname ;
      private String bttBtnpausarcontinuar_Internalname ;
      private String edtContagemResultado_CntSrvUndCntNome_Link ;
      private String edtLote_Nome_Link ;
      private String tblTblliquidada_Internalname ;
      private String bttBtnexlote_Internalname ;
      private String tblGrpaceite_Internalname ;
      private String edtContagemResultado_Link_Link ;
      private String imgChcklst_Internalname ;
      private String AV27StatusParaPausar ;
      private String AV28StatusDemandaVnc ;
      private String bttBtnpausarcontinuar_Caption ;
      private String lblTbjava_Caption ;
      private String A1373ContagemResultadoLiqLogOS_UserNom ;
      private String AV34txt ;
      private String bttBtnfechar_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String lblTextblocksistema_gestor_Internalname ;
      private String GXt_char1 ;
      private String lblTextblockcontratoservicosunidconversao_sigla_Caption ;
      private String lblTextblockcontratoservicosunidconversao_sigla_Internalname ;
      private String A438Contratada_Sigla ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblUsertable_Internalname ;
      private String lblTbjava_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String bttBtnnovo_Jsonclick ;
      private String bttBtnpausarcontinuar_Jsonclick ;
      private String tblUnnamedtable1_Internalname ;
      private String lblTextblockcontagemresultado_datadmn_Internalname ;
      private String lblTextblockcontagemresultado_datadmn_Jsonclick ;
      private String edtContagemResultado_DataDmn_Jsonclick ;
      private String lblTextblockcontagemresultado_ss_Internalname ;
      private String lblTextblockcontagemresultado_ss_Jsonclick ;
      private String edtContagemResultado_SS_Jsonclick ;
      private String lblTextblockcontagemresultado_cntsrvprrnome_Internalname ;
      private String lblTextblockcontagemresultado_cntsrvprrnome_Jsonclick ;
      private String edtContagemResultado_CntSrvPrrNome_Jsonclick ;
      private String lblTextblockcontagemresultado_contratadapessoanom_Internalname ;
      private String lblTextblockcontagemresultado_contratadapessoanom_Jsonclick ;
      private String edtContagemResultado_ContratadaPessoaNom_Jsonclick ;
      private String lblTextblockcontagemresultado_contratadaorigempesnom_Internalname ;
      private String lblTextblockcontagemresultado_contratadaorigempesnom_Jsonclick ;
      private String edtContagemResultado_ContratadaOrigemPesNom_Jsonclick ;
      private String lblTextblockcontagemresultado_contadorfsnom_Internalname ;
      private String lblTextblockcontagemresultado_contadorfsnom_Jsonclick ;
      private String edtContagemResultado_ContadorFSNom_Jsonclick ;
      private String lblTextblockcontagemresultado_servicosigla_Internalname ;
      private String lblTextblockcontagemresultado_servicosigla_Jsonclick ;
      private String edtContagemResultado_ServicoSigla_Jsonclick ;
      private String lblTextblockcontagemresultado_cntsrvundcntnome_Internalname ;
      private String lblTextblockcontagemresultado_cntsrvundcntnome_Jsonclick ;
      private String edtContagemResultado_CntSrvUndCntNome_Jsonclick ;
      private String lblTextblockcontagemresultado_cntsrvqtduntcns_Internalname ;
      private String lblTextblockcontagemresultado_cntsrvqtduntcns_Jsonclick ;
      private String edtContagemResultado_CntSrvQtdUntCns_Jsonclick ;
      private String lblTextblockcontagemresultado_quantidadesolicitada_Internalname ;
      private String lblTextblockcontagemresultado_quantidadesolicitada_Jsonclick ;
      private String edtContagemResultado_QuantidadeSolicitada_Jsonclick ;
      private String lblTextblockcontratoservicosunidconversao_sigla_Jsonclick ;
      private String edtavContratoservicosunidconversao_sigla_Jsonclick ;
      private String lblTextblockcontagemrresultado_sistemasigla_Internalname ;
      private String lblTextblockcontagemrresultado_sistemasigla_Jsonclick ;
      private String edtContagemrResultado_SistemaSigla_Jsonclick ;
      private String lblTextblockmodulo_nome_Internalname ;
      private String lblTextblockmodulo_nome_Jsonclick ;
      private String edtModulo_Nome_Jsonclick ;
      private String lblTextblocksistema_gestor_Jsonclick ;
      private String edtavSistema_gestor_Jsonclick ;
      private String lblTextblockcontagemresultado_fncusrcod_Internalname ;
      private String lblTextblockcontagemresultado_fncusrcod_Jsonclick ;
      private String edtContagemResultado_FncUsrCod_Jsonclick ;
      private String lblTextblockcontagemresultado_link_Internalname ;
      private String lblTextblockcontagemresultado_link_Jsonclick ;
      private String edtContagemResultado_Link_Jsonclick ;
      private String lblTextblockcontagemresultado_sistemacoord_Internalname ;
      private String lblTextblockcontagemresultado_sistemacoord_Jsonclick ;
      private String lblTextblockcontagemresultado_naocnfdmnnom_Internalname ;
      private String lblTextblockcontagemresultado_naocnfdmnnom_Jsonclick ;
      private String edtContagemResultado_NaoCnfDmnNom_Jsonclick ;
      private String lblTextblockcontagemresultado_agrupador_Internalname ;
      private String lblTextblockcontagemresultado_agrupador_Jsonclick ;
      private String edtContagemResultado_Agrupador_Jsonclick ;
      private String lblTextblockcontagemresultado_pfbfsimp_Internalname ;
      private String lblTextblockcontagemresultado_pfbfsimp_Jsonclick ;
      private String lblTextblockresponsavel_Internalname ;
      private String lblTextblockresponsavel_Jsonclick ;
      private String edtavResponsavel_Jsonclick ;
      private String lblTextblockcontagemresultado_owner_Internalname ;
      private String lblTextblockcontagemresultado_owner_Jsonclick ;
      private String dynContagemResultado_Owner_Jsonclick ;
      private String lblTextblockcontagemresultado_owneremail_Internalname ;
      private String lblTextblockcontagemresultado_owneremail_Jsonclick ;
      private String edtavContagemresultado_owneremail_Jsonclick ;
      private String lblTextblockcontagemresultado_ownertelefone_Internalname ;
      private String lblTextblockcontagemresultado_ownertelefone_Jsonclick ;
      private String edtavContagemresultado_ownertelefone_Jsonclick ;
      private String lblTextblockcontagemresultado_datacadastro_Internalname ;
      private String lblTextblockcontagemresultado_datacadastro_Jsonclick ;
      private String edtContagemResultado_DataCadastro_Jsonclick ;
      private String lblTextblockcontagemresultado_dataentrega_Internalname ;
      private String lblTextblockcontagemresultado_dataentrega_Jsonclick ;
      private String lblTextblockcontagemresultado_statusdmn_Internalname ;
      private String lblTextblockcontagemresultado_statusdmn_Jsonclick ;
      private String cmbContagemResultado_StatusDmn_Jsonclick ;
      private String grpUnnamedgroup3_Internalname ;
      private String lblTextblockcontagemresultado_loteaceite_Internalname ;
      private String lblTextblockcontagemresultado_loteaceite_Jsonclick ;
      private String lblTextblockcontagemresultado_dataaceite_Internalname ;
      private String lblTextblockcontagemresultado_dataaceite_Jsonclick ;
      private String edtContagemResultado_DataAceite_Jsonclick ;
      private String lblTextblockcontagemresultado_aceiteusernom_Internalname ;
      private String lblTextblockcontagemresultado_aceiteusernom_Jsonclick ;
      private String edtContagemResultado_AceiteUserNom_Jsonclick ;
      private String bttBtnexlote_Jsonclick ;
      private String lblTextblockpagocolaborador_Internalname ;
      private String lblTextblockpagocolaborador_Jsonclick ;
      private String lstavPagocolaborador_Jsonclick ;
      private String tblTablemergedcontagemresultado_loteaceite_Internalname ;
      private String edtContagemResultado_LoteAceite_Jsonclick ;
      private String edtLote_Nome_Jsonclick ;
      private String tblTablemergedcontagemresultado_dataentrega_Internalname ;
      private String edtContagemResultado_DataEntrega_Jsonclick ;
      private String edtContagemResultado_HoraEntrega_Jsonclick ;
      private String tblTablemergedcontagemresultado_pfbfsimp_Internalname ;
      private String edtContagemResultado_PFBFSImp_Jsonclick ;
      private String lblTextblockcontagemresultado_pflfsimp_Internalname ;
      private String lblTextblockcontagemresultado_pflfsimp_Jsonclick ;
      private String edtContagemResultado_PFLFSImp_Jsonclick ;
      private String tblTablemergedcontagemresultado_sistemacoord_Internalname ;
      private String edtContagemResultado_SistemaCoord_Jsonclick ;
      private String tblUnnamedtable2_Internalname ;
      private String imgChcklst_Jsonclick ;
      private String sCtrlA456ContagemResultado_Codigo ;
      private String Innewwindow1_Internalname ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime A1350ContagemResultado_DataCadastro ;
      private DateTime A1034ContagemResultadoLiqLog_Data ;
      private DateTime A529ContagemResultado_DataAceite ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private DateTime A471ContagemResultado_DataDmn ;
      private bool entryPointCalled ;
      private bool n454ContagemResultado_ContadorFSCod ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n146Modulo_Codigo ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private bool n1043ContagemResultado_LiqLogCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n457ContagemResultado_Demanda ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n1350ContagemResultado_DataCadastro ;
      private bool n799ContagemResultado_PFLFSImp ;
      private bool n798ContagemResultado_PFBFSImp ;
      private bool n1046ContagemResultado_Agrupador ;
      private bool n465ContagemResultado_Link ;
      private bool n1044ContagemResultado_FncUsrCod ;
      private bool n2133ContagemResultado_QuantidadeSolicitada ;
      private bool n1452ContagemResultado_SS ;
      private bool n1443ContagemResultado_CntSrvPrrCod ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n480ContagemResultado_CrFSPessoaCod ;
      private bool n455ContagemResultado_ContadorFSNom ;
      private bool n477ContagemResultado_NaoCnfDmnNom ;
      private bool n807ContagemResultado_ContratadaOrigemPesCod ;
      private bool n808ContagemResultado_ContratadaOrigemPesNom ;
      private bool n1034ContagemResultadoLiqLog_Data ;
      private bool n601ContagemResultado_Servico ;
      private bool n1620ContagemResultado_CntSrvUndCnt ;
      private bool A2094ContratoServicos_SolicitaGestorSistema ;
      private bool n2094ContratoServicos_SolicitaGestorSistema ;
      private bool n1613ContagemResultado_CntSrvQtdUntCns ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n2209ContagemResultado_CntSrvUndCntNome ;
      private bool n499ContagemResultado_ContratadaPessoaCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n500ContagemResultado_ContratadaPessoaNom ;
      private bool n1547ContagemResultado_LoteNFe ;
      private bool n525ContagemResultado_AceiteUserCod ;
      private bool n529ContagemResultado_DataAceite ;
      private bool n563Lote_Nome ;
      private bool n528ContagemResultado_LoteAceite ;
      private bool n526ContagemResultado_AceitePessoaCod ;
      private bool n527ContagemResultado_AceiteUserNom ;
      private bool n515ContagemResultado_SistemaCoord ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n1312ContagemResultado_ResponsavelPessCod ;
      private bool n1313ContagemResultado_ResponsavelPessNome ;
      private bool toggleJsOutput ;
      private bool AV24isAgendaOK ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool A1000ContagemResultado_CrFMEhContratante ;
      private bool n1000ContagemResultado_CrFMEhContratante ;
      private bool returnInSub ;
      private bool AV23EhGestor ;
      private bool n1374ContagemResultadoLiqLogOS_UserPesCod ;
      private bool n1373ContagemResultadoLiqLogOS_UserNom ;
      private bool AV39Servico_IsSolicitaGestorSistema ;
      private bool AV45IsContratoServicosUnidConversao ;
      private bool n2112ContratoServicosUnidConversao_Sigla ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool n438Contratada_Sigla ;
      private bool A1018ContratadaUsuario_UsuarioEhContratada ;
      private bool n1018ContratadaUsuario_UsuarioEhContratada ;
      private bool n2095Usuario_PessoaTelefone ;
      private bool n1647Usuario_Email ;
      private String A465ContagemResultado_Link ;
      private String A457ContagemResultado_Demanda ;
      private String A515ContagemResultado_SistemaCoord ;
      private String A1647Usuario_Email ;
      private String AV38Sistema_Gestor ;
      private String AV35ContagemResultado_OwnerEmail ;
      private String AV36ContagemResultado_OwnerTelefone ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynContagemResultado_Owner ;
      private GXCombobox cmbContagemResultado_StatusDmn ;
      private GXListbox lstavPagocolaborador ;
      private IDataStoreProvider pr_default ;
      private int[] H00B52_A454ContagemResultado_ContadorFSCod ;
      private bool[] H00B52_n454ContagemResultado_ContadorFSCod ;
      private int[] H00B52_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] H00B52_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] H00B52_A146Modulo_Codigo ;
      private bool[] H00B52_n146Modulo_Codigo ;
      private int[] H00B52_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] H00B52_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] H00B52_A1043ContagemResultado_LiqLogCod ;
      private bool[] H00B52_n1043ContagemResultado_LiqLogCod ;
      private int[] H00B52_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00B52_n1553ContagemResultado_CntSrvCod ;
      private int[] H00B52_A490ContagemResultado_ContratadaCod ;
      private bool[] H00B52_n490ContagemResultado_ContratadaCod ;
      private int[] H00B52_A597ContagemResultado_LoteAceiteCod ;
      private bool[] H00B52_n597ContagemResultado_LoteAceiteCod ;
      private int[] H00B52_A602ContagemResultado_OSVinculada ;
      private bool[] H00B52_n602ContagemResultado_OSVinculada ;
      private String[] H00B52_A457ContagemResultado_Demanda ;
      private bool[] H00B52_n457ContagemResultado_Demanda ;
      private int[] H00B52_A489ContagemResultado_SistemaCod ;
      private bool[] H00B52_n489ContagemResultado_SistemaCod ;
      private String[] H00B52_A484ContagemResultado_StatusDmn ;
      private bool[] H00B52_n484ContagemResultado_StatusDmn ;
      private DateTime[] H00B52_A912ContagemResultado_HoraEntrega ;
      private bool[] H00B52_n912ContagemResultado_HoraEntrega ;
      private DateTime[] H00B52_A472ContagemResultado_DataEntrega ;
      private bool[] H00B52_n472ContagemResultado_DataEntrega ;
      private DateTime[] H00B52_A1350ContagemResultado_DataCadastro ;
      private bool[] H00B52_n1350ContagemResultado_DataCadastro ;
      private int[] H00B52_A508ContagemResultado_Owner ;
      private decimal[] H00B52_A799ContagemResultado_PFLFSImp ;
      private bool[] H00B52_n799ContagemResultado_PFLFSImp ;
      private decimal[] H00B52_A798ContagemResultado_PFBFSImp ;
      private bool[] H00B52_n798ContagemResultado_PFBFSImp ;
      private String[] H00B52_A1046ContagemResultado_Agrupador ;
      private bool[] H00B52_n1046ContagemResultado_Agrupador ;
      private String[] H00B52_A465ContagemResultado_Link ;
      private bool[] H00B52_n465ContagemResultado_Link ;
      private int[] H00B52_A1044ContagemResultado_FncUsrCod ;
      private bool[] H00B52_n1044ContagemResultado_FncUsrCod ;
      private decimal[] H00B52_A2133ContagemResultado_QuantidadeSolicitada ;
      private bool[] H00B52_n2133ContagemResultado_QuantidadeSolicitada ;
      private int[] H00B52_A1452ContagemResultado_SS ;
      private bool[] H00B52_n1452ContagemResultado_SS ;
      private DateTime[] H00B52_A471ContagemResultado_DataDmn ;
      private int[] H00B52_A1443ContagemResultado_CntSrvPrrCod ;
      private bool[] H00B52_n1443ContagemResultado_CntSrvPrrCod ;
      private int[] H00B52_A890ContagemResultado_Responsavel ;
      private bool[] H00B52_n890ContagemResultado_Responsavel ;
      private int[] H00B53_A480ContagemResultado_CrFSPessoaCod ;
      private bool[] H00B53_n480ContagemResultado_CrFSPessoaCod ;
      private String[] H00B54_A455ContagemResultado_ContadorFSNom ;
      private bool[] H00B54_n455ContagemResultado_ContadorFSNom ;
      private String[] H00B55_A477ContagemResultado_NaoCnfDmnNom ;
      private bool[] H00B55_n477ContagemResultado_NaoCnfDmnNom ;
      private String[] H00B56_A143Modulo_Nome ;
      private int[] H00B57_A807ContagemResultado_ContratadaOrigemPesCod ;
      private bool[] H00B57_n807ContagemResultado_ContratadaOrigemPesCod ;
      private String[] H00B58_A808ContagemResultado_ContratadaOrigemPesNom ;
      private bool[] H00B58_n808ContagemResultado_ContratadaOrigemPesNom ;
      private DateTime[] H00B59_A1034ContagemResultadoLiqLog_Data ;
      private bool[] H00B59_n1034ContagemResultadoLiqLog_Data ;
      private int[] H00B510_A601ContagemResultado_Servico ;
      private bool[] H00B510_n601ContagemResultado_Servico ;
      private int[] H00B510_A1620ContagemResultado_CntSrvUndCnt ;
      private bool[] H00B510_n1620ContagemResultado_CntSrvUndCnt ;
      private bool[] H00B510_A2094ContratoServicos_SolicitaGestorSistema ;
      private bool[] H00B510_n2094ContratoServicos_SolicitaGestorSistema ;
      private decimal[] H00B510_A1613ContagemResultado_CntSrvQtdUntCns ;
      private bool[] H00B510_n1613ContagemResultado_CntSrvQtdUntCns ;
      private String[] H00B511_A801ContagemResultado_ServicoSigla ;
      private bool[] H00B511_n801ContagemResultado_ServicoSigla ;
      private String[] H00B512_A2209ContagemResultado_CntSrvUndCntNome ;
      private bool[] H00B512_n2209ContagemResultado_CntSrvUndCntNome ;
      private int[] H00B513_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] H00B513_n499ContagemResultado_ContratadaPessoaCod ;
      private int[] H00B513_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00B513_n52Contratada_AreaTrabalhoCod ;
      private String[] H00B514_A500ContagemResultado_ContratadaPessoaNom ;
      private bool[] H00B514_n500ContagemResultado_ContratadaPessoaNom ;
      private int[] H00B515_A1547ContagemResultado_LoteNFe ;
      private bool[] H00B515_n1547ContagemResultado_LoteNFe ;
      private int[] H00B515_A525ContagemResultado_AceiteUserCod ;
      private bool[] H00B515_n525ContagemResultado_AceiteUserCod ;
      private DateTime[] H00B515_A529ContagemResultado_DataAceite ;
      private bool[] H00B515_n529ContagemResultado_DataAceite ;
      private String[] H00B515_A563Lote_Nome ;
      private bool[] H00B515_n563Lote_Nome ;
      private String[] H00B515_A528ContagemResultado_LoteAceite ;
      private bool[] H00B515_n528ContagemResultado_LoteAceite ;
      private int[] H00B516_A526ContagemResultado_AceitePessoaCod ;
      private bool[] H00B516_n526ContagemResultado_AceitePessoaCod ;
      private String[] H00B517_A527ContagemResultado_AceiteUserNom ;
      private bool[] H00B517_n527ContagemResultado_AceiteUserNom ;
      private String[] H00B518_A515ContagemResultado_SistemaCoord ;
      private bool[] H00B518_n515ContagemResultado_SistemaCoord ;
      private String[] H00B518_A509ContagemrResultado_SistemaSigla ;
      private bool[] H00B518_n509ContagemrResultado_SistemaSigla ;
      private int[] H00B519_A1312ContagemResultado_ResponsavelPessCod ;
      private bool[] H00B519_n1312ContagemResultado_ResponsavelPessCod ;
      private String[] H00B520_A1313ContagemResultado_ResponsavelPessNome ;
      private bool[] H00B520_n1313ContagemResultado_ResponsavelPessNome ;
      private int[] H00B521_A57Usuario_PessoaCod ;
      private int[] H00B521_A1Usuario_Codigo ;
      private String[] H00B521_A58Usuario_PessoaNom ;
      private bool[] H00B521_n58Usuario_PessoaNom ;
      private DateTime[] H00B522_A473ContagemResultado_DataCnt ;
      private String[] H00B522_A511ContagemResultado_HoraCnt ;
      private int[] H00B522_A454ContagemResultado_ContadorFSCod ;
      private bool[] H00B522_n454ContagemResultado_ContadorFSCod ;
      private int[] H00B522_A480ContagemResultado_CrFSPessoaCod ;
      private bool[] H00B522_n480ContagemResultado_CrFSPessoaCod ;
      private int[] H00B522_A1312ContagemResultado_ResponsavelPessCod ;
      private bool[] H00B522_n1312ContagemResultado_ResponsavelPessCod ;
      private int[] H00B522_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] H00B522_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] H00B522_A146Modulo_Codigo ;
      private bool[] H00B522_n146Modulo_Codigo ;
      private int[] H00B522_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] H00B522_n499ContagemResultado_ContratadaPessoaCod ;
      private int[] H00B522_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] H00B522_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] H00B522_A807ContagemResultado_ContratadaOrigemPesCod ;
      private bool[] H00B522_n807ContagemResultado_ContratadaOrigemPesCod ;
      private int[] H00B522_A526ContagemResultado_AceitePessoaCod ;
      private bool[] H00B522_n526ContagemResultado_AceitePessoaCod ;
      private int[] H00B522_A1043ContagemResultado_LiqLogCod ;
      private bool[] H00B522_n1043ContagemResultado_LiqLogCod ;
      private int[] H00B522_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00B522_n1553ContagemResultado_CntSrvCod ;
      private int[] H00B522_A470ContagemResultado_ContadorFMCod ;
      private int[] H00B522_A456ContagemResultado_Codigo ;
      private DateTime[] H00B522_A1034ContagemResultadoLiqLog_Data ;
      private bool[] H00B522_n1034ContagemResultadoLiqLog_Data ;
      private int[] H00B522_A601ContagemResultado_Servico ;
      private bool[] H00B522_n601ContagemResultado_Servico ;
      private int[] H00B522_A490ContagemResultado_ContratadaCod ;
      private bool[] H00B522_n490ContagemResultado_ContratadaCod ;
      private int[] H00B522_A1620ContagemResultado_CntSrvUndCnt ;
      private bool[] H00B522_n1620ContagemResultado_CntSrvUndCnt ;
      private int[] H00B522_A597ContagemResultado_LoteAceiteCod ;
      private bool[] H00B522_n597ContagemResultado_LoteAceiteCod ;
      private int[] H00B522_A1547ContagemResultado_LoteNFe ;
      private bool[] H00B522_n1547ContagemResultado_LoteNFe ;
      private int[] H00B522_A525ContagemResultado_AceiteUserCod ;
      private bool[] H00B522_n525ContagemResultado_AceiteUserCod ;
      private bool[] H00B522_A1000ContagemResultado_CrFMEhContratante ;
      private bool[] H00B522_n1000ContagemResultado_CrFMEhContratante ;
      private String[] H00B522_A1313ContagemResultado_ResponsavelPessNome ;
      private bool[] H00B522_n1313ContagemResultado_ResponsavelPessNome ;
      private int[] H00B522_A602ContagemResultado_OSVinculada ;
      private bool[] H00B522_n602ContagemResultado_OSVinculada ;
      private bool[] H00B522_A2094ContratoServicos_SolicitaGestorSistema ;
      private bool[] H00B522_n2094ContratoServicos_SolicitaGestorSistema ;
      private String[] H00B522_A457ContagemResultado_Demanda ;
      private bool[] H00B522_n457ContagemResultado_Demanda ;
      private int[] H00B522_A489ContagemResultado_SistemaCod ;
      private bool[] H00B522_n489ContagemResultado_SistemaCod ;
      private String[] H00B522_A527ContagemResultado_AceiteUserNom ;
      private bool[] H00B522_n527ContagemResultado_AceiteUserNom ;
      private DateTime[] H00B522_A529ContagemResultado_DataAceite ;
      private bool[] H00B522_n529ContagemResultado_DataAceite ;
      private String[] H00B522_A563Lote_Nome ;
      private bool[] H00B522_n563Lote_Nome ;
      private String[] H00B522_A528ContagemResultado_LoteAceite ;
      private bool[] H00B522_n528ContagemResultado_LoteAceite ;
      private String[] H00B522_A484ContagemResultado_StatusDmn ;
      private bool[] H00B522_n484ContagemResultado_StatusDmn ;
      private DateTime[] H00B522_A912ContagemResultado_HoraEntrega ;
      private bool[] H00B522_n912ContagemResultado_HoraEntrega ;
      private DateTime[] H00B522_A472ContagemResultado_DataEntrega ;
      private bool[] H00B522_n472ContagemResultado_DataEntrega ;
      private DateTime[] H00B522_A1350ContagemResultado_DataCadastro ;
      private bool[] H00B522_n1350ContagemResultado_DataCadastro ;
      private int[] H00B522_A508ContagemResultado_Owner ;
      private decimal[] H00B522_A799ContagemResultado_PFLFSImp ;
      private bool[] H00B522_n799ContagemResultado_PFLFSImp ;
      private decimal[] H00B522_A798ContagemResultado_PFBFSImp ;
      private bool[] H00B522_n798ContagemResultado_PFBFSImp ;
      private String[] H00B522_A1046ContagemResultado_Agrupador ;
      private bool[] H00B522_n1046ContagemResultado_Agrupador ;
      private String[] H00B522_A477ContagemResultado_NaoCnfDmnNom ;
      private bool[] H00B522_n477ContagemResultado_NaoCnfDmnNom ;
      private String[] H00B522_A515ContagemResultado_SistemaCoord ;
      private bool[] H00B522_n515ContagemResultado_SistemaCoord ;
      private String[] H00B522_A465ContagemResultado_Link ;
      private bool[] H00B522_n465ContagemResultado_Link ;
      private int[] H00B522_A1044ContagemResultado_FncUsrCod ;
      private bool[] H00B522_n1044ContagemResultado_FncUsrCod ;
      private String[] H00B522_A143Modulo_Nome ;
      private String[] H00B522_A509ContagemrResultado_SistemaSigla ;
      private bool[] H00B522_n509ContagemrResultado_SistemaSigla ;
      private decimal[] H00B522_A2133ContagemResultado_QuantidadeSolicitada ;
      private bool[] H00B522_n2133ContagemResultado_QuantidadeSolicitada ;
      private decimal[] H00B522_A1613ContagemResultado_CntSrvQtdUntCns ;
      private bool[] H00B522_n1613ContagemResultado_CntSrvQtdUntCns ;
      private String[] H00B522_A2209ContagemResultado_CntSrvUndCntNome ;
      private bool[] H00B522_n2209ContagemResultado_CntSrvUndCntNome ;
      private String[] H00B522_A801ContagemResultado_ServicoSigla ;
      private bool[] H00B522_n801ContagemResultado_ServicoSigla ;
      private String[] H00B522_A455ContagemResultado_ContadorFSNom ;
      private bool[] H00B522_n455ContagemResultado_ContadorFSNom ;
      private String[] H00B522_A808ContagemResultado_ContratadaOrigemPesNom ;
      private bool[] H00B522_n808ContagemResultado_ContratadaOrigemPesNom ;
      private String[] H00B522_A500ContagemResultado_ContratadaPessoaNom ;
      private bool[] H00B522_n500ContagemResultado_ContratadaPessoaNom ;
      private int[] H00B522_A1452ContagemResultado_SS ;
      private bool[] H00B522_n1452ContagemResultado_SS ;
      private DateTime[] H00B522_A471ContagemResultado_DataDmn ;
      private int[] H00B522_A1443ContagemResultado_CntSrvPrrCod ;
      private bool[] H00B522_n1443ContagemResultado_CntSrvPrrCod ;
      private int[] H00B522_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00B522_n52Contratada_AreaTrabalhoCod ;
      private int[] H00B522_A890ContagemResultado_Responsavel ;
      private bool[] H00B522_n890ContagemResultado_Responsavel ;
      private int[] H00B523_A454ContagemResultado_ContadorFSCod ;
      private bool[] H00B523_n454ContagemResultado_ContadorFSCod ;
      private int[] H00B523_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] H00B523_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] H00B523_A146Modulo_Codigo ;
      private bool[] H00B523_n146Modulo_Codigo ;
      private int[] H00B523_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] H00B523_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] H00B523_A1043ContagemResultado_LiqLogCod ;
      private bool[] H00B523_n1043ContagemResultado_LiqLogCod ;
      private int[] H00B523_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00B523_n1553ContagemResultado_CntSrvCod ;
      private int[] H00B523_A490ContagemResultado_ContratadaCod ;
      private bool[] H00B523_n490ContagemResultado_ContratadaCod ;
      private int[] H00B523_A597ContagemResultado_LoteAceiteCod ;
      private bool[] H00B523_n597ContagemResultado_LoteAceiteCod ;
      private int[] H00B523_A602ContagemResultado_OSVinculada ;
      private bool[] H00B523_n602ContagemResultado_OSVinculada ;
      private String[] H00B523_A457ContagemResultado_Demanda ;
      private bool[] H00B523_n457ContagemResultado_Demanda ;
      private int[] H00B523_A489ContagemResultado_SistemaCod ;
      private bool[] H00B523_n489ContagemResultado_SistemaCod ;
      private String[] H00B523_A484ContagemResultado_StatusDmn ;
      private bool[] H00B523_n484ContagemResultado_StatusDmn ;
      private DateTime[] H00B523_A912ContagemResultado_HoraEntrega ;
      private bool[] H00B523_n912ContagemResultado_HoraEntrega ;
      private DateTime[] H00B523_A472ContagemResultado_DataEntrega ;
      private bool[] H00B523_n472ContagemResultado_DataEntrega ;
      private DateTime[] H00B523_A1350ContagemResultado_DataCadastro ;
      private bool[] H00B523_n1350ContagemResultado_DataCadastro ;
      private int[] H00B523_A508ContagemResultado_Owner ;
      private decimal[] H00B523_A799ContagemResultado_PFLFSImp ;
      private bool[] H00B523_n799ContagemResultado_PFLFSImp ;
      private decimal[] H00B523_A798ContagemResultado_PFBFSImp ;
      private bool[] H00B523_n798ContagemResultado_PFBFSImp ;
      private String[] H00B523_A1046ContagemResultado_Agrupador ;
      private bool[] H00B523_n1046ContagemResultado_Agrupador ;
      private String[] H00B523_A465ContagemResultado_Link ;
      private bool[] H00B523_n465ContagemResultado_Link ;
      private int[] H00B523_A1044ContagemResultado_FncUsrCod ;
      private bool[] H00B523_n1044ContagemResultado_FncUsrCod ;
      private decimal[] H00B523_A2133ContagemResultado_QuantidadeSolicitada ;
      private bool[] H00B523_n2133ContagemResultado_QuantidadeSolicitada ;
      private int[] H00B523_A1452ContagemResultado_SS ;
      private bool[] H00B523_n1452ContagemResultado_SS ;
      private DateTime[] H00B523_A471ContagemResultado_DataDmn ;
      private int[] H00B523_A1443ContagemResultado_CntSrvPrrCod ;
      private bool[] H00B523_n1443ContagemResultado_CntSrvPrrCod ;
      private int[] H00B523_A890ContagemResultado_Responsavel ;
      private bool[] H00B523_n890ContagemResultado_Responsavel ;
      private int[] H00B524_A480ContagemResultado_CrFSPessoaCod ;
      private bool[] H00B524_n480ContagemResultado_CrFSPessoaCod ;
      private String[] H00B525_A455ContagemResultado_ContadorFSNom ;
      private bool[] H00B525_n455ContagemResultado_ContadorFSNom ;
      private String[] H00B526_A477ContagemResultado_NaoCnfDmnNom ;
      private bool[] H00B526_n477ContagemResultado_NaoCnfDmnNom ;
      private String[] H00B527_A143Modulo_Nome ;
      private int[] H00B528_A807ContagemResultado_ContratadaOrigemPesCod ;
      private bool[] H00B528_n807ContagemResultado_ContratadaOrigemPesCod ;
      private String[] H00B529_A808ContagemResultado_ContratadaOrigemPesNom ;
      private bool[] H00B529_n808ContagemResultado_ContratadaOrigemPesNom ;
      private DateTime[] H00B530_A1034ContagemResultadoLiqLog_Data ;
      private bool[] H00B530_n1034ContagemResultadoLiqLog_Data ;
      private int[] H00B531_A601ContagemResultado_Servico ;
      private bool[] H00B531_n601ContagemResultado_Servico ;
      private int[] H00B531_A1620ContagemResultado_CntSrvUndCnt ;
      private bool[] H00B531_n1620ContagemResultado_CntSrvUndCnt ;
      private bool[] H00B531_A2094ContratoServicos_SolicitaGestorSistema ;
      private bool[] H00B531_n2094ContratoServicos_SolicitaGestorSistema ;
      private decimal[] H00B531_A1613ContagemResultado_CntSrvQtdUntCns ;
      private bool[] H00B531_n1613ContagemResultado_CntSrvQtdUntCns ;
      private String[] H00B532_A801ContagemResultado_ServicoSigla ;
      private bool[] H00B532_n801ContagemResultado_ServicoSigla ;
      private String[] H00B533_A2209ContagemResultado_CntSrvUndCntNome ;
      private bool[] H00B533_n2209ContagemResultado_CntSrvUndCntNome ;
      private int[] H00B534_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] H00B534_n499ContagemResultado_ContratadaPessoaCod ;
      private int[] H00B534_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00B534_n52Contratada_AreaTrabalhoCod ;
      private String[] H00B535_A500ContagemResultado_ContratadaPessoaNom ;
      private bool[] H00B535_n500ContagemResultado_ContratadaPessoaNom ;
      private int[] H00B536_A1547ContagemResultado_LoteNFe ;
      private bool[] H00B536_n1547ContagemResultado_LoteNFe ;
      private int[] H00B536_A525ContagemResultado_AceiteUserCod ;
      private bool[] H00B536_n525ContagemResultado_AceiteUserCod ;
      private DateTime[] H00B536_A529ContagemResultado_DataAceite ;
      private bool[] H00B536_n529ContagemResultado_DataAceite ;
      private String[] H00B536_A563Lote_Nome ;
      private bool[] H00B536_n563Lote_Nome ;
      private String[] H00B536_A528ContagemResultado_LoteAceite ;
      private bool[] H00B536_n528ContagemResultado_LoteAceite ;
      private int[] H00B537_A526ContagemResultado_AceitePessoaCod ;
      private bool[] H00B537_n526ContagemResultado_AceitePessoaCod ;
      private String[] H00B538_A527ContagemResultado_AceiteUserNom ;
      private bool[] H00B538_n527ContagemResultado_AceiteUserNom ;
      private String[] H00B539_A515ContagemResultado_SistemaCoord ;
      private bool[] H00B539_n515ContagemResultado_SistemaCoord ;
      private String[] H00B539_A509ContagemrResultado_SistemaSigla ;
      private bool[] H00B539_n509ContagemrResultado_SistemaSigla ;
      private int[] H00B540_A1312ContagemResultado_ResponsavelPessCod ;
      private bool[] H00B540_n1312ContagemResultado_ResponsavelPessCod ;
      private String[] H00B541_A1313ContagemResultado_ResponsavelPessNome ;
      private bool[] H00B541_n1313ContagemResultado_ResponsavelPessNome ;
      private int[] H00B542_A160ContratoServicos_Codigo ;
      private int[] H00B542_A74Contrato_Codigo ;
      private int[] H00B542_A155Servico_Codigo ;
      private int[] H00B543_A39Contratada_Codigo ;
      private int[] H00B544_A1078ContratoGestor_ContratoCod ;
      private int[] H00B544_A1079ContratoGestor_UsuarioCod ;
      private int[] H00B545_A1033ContagemResultadoLiqLog_Codigo ;
      private int[] H00B545_A1370ContagemResultadoLiqLogOS_Codigo ;
      private int[] H00B545_A1372ContagemResultadoLiqLogOS_UserCod ;
      private int[] H00B545_A1374ContagemResultadoLiqLogOS_UserPesCod ;
      private bool[] H00B545_n1374ContagemResultadoLiqLogOS_UserPesCod ;
      private int[] H00B545_A1371ContagemResultadoLiqLogOS_OSCod ;
      private decimal[] H00B545_A1375ContagemResultadoLiqLogOS_Valor ;
      private String[] H00B545_A1373ContagemResultadoLiqLogOS_UserNom ;
      private bool[] H00B545_n1373ContagemResultadoLiqLogOS_UserNom ;
      private int[] H00B546_A160ContratoServicos_Codigo ;
      private bool[] H00B546_A2094ContratoServicos_SolicitaGestorSistema ;
      private bool[] H00B546_n2094ContratoServicos_SolicitaGestorSistema ;
      private int[] H00B547_A2110ContratoServicosUnidConversao_Codigo ;
      private int[] H00B547_A160ContratoServicos_Codigo ;
      private String[] H00B547_A2112ContratoServicosUnidConversao_Sigla ;
      private bool[] H00B547_n2112ContratoServicosUnidConversao_Sigla ;
      private int[] H00B548_A66ContratadaUsuario_ContratadaCod ;
      private int[] H00B548_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H00B548_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private int[] H00B548_A69ContratadaUsuario_UsuarioCod ;
      private String[] H00B548_A438Contratada_Sigla ;
      private bool[] H00B548_n438Contratada_Sigla ;
      private int[] H00B549_A66ContratadaUsuario_ContratadaCod ;
      private bool[] H00B549_A1018ContratadaUsuario_UsuarioEhContratada ;
      private bool[] H00B549_n1018ContratadaUsuario_UsuarioEhContratada ;
      private int[] H00B549_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H00B549_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private int[] H00B549_A69ContratadaUsuario_UsuarioCod ;
      private String[] H00B549_A438Contratada_Sigla ;
      private bool[] H00B549_n438Contratada_Sigla ;
      private int[] H00B550_A57Usuario_PessoaCod ;
      private int[] H00B550_A1Usuario_Codigo ;
      private String[] H00B550_A2095Usuario_PessoaTelefone ;
      private bool[] H00B550_n2095Usuario_PessoaTelefone ;
      private String[] H00B550_A1647Usuario_Email ;
      private bool[] H00B550_n1647Usuario_Email ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV12WebSession ;
      private IGxSession AV10Session ;
      private SdtContagemResultado AV31ContagemResultado ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class contagemresultadogeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
         ,new ForEachCursor(def[32])
         ,new ForEachCursor(def[33])
         ,new ForEachCursor(def[34])
         ,new ForEachCursor(def[35])
         ,new ForEachCursor(def[36])
         ,new ForEachCursor(def[37])
         ,new ForEachCursor(def[38])
         ,new ForEachCursor(def[39])
         ,new ForEachCursor(def[40])
         ,new ForEachCursor(def[41])
         ,new ForEachCursor(def[42])
         ,new ForEachCursor(def[43])
         ,new ForEachCursor(def[44])
         ,new ForEachCursor(def[45])
         ,new ForEachCursor(def[46])
         ,new ForEachCursor(def[47])
         ,new ForEachCursor(def[48])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00B52 ;
          prmH00B52 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B53 ;
          prmH00B53 = new Object[] {
          new Object[] {"@ContagemResultado_ContadorFSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B54 ;
          prmH00B54 = new Object[] {
          new Object[] {"@ContagemResultado_CrFSPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B55 ;
          prmH00B55 = new Object[] {
          new Object[] {"@ContagemResultado_NaoCnfDmnCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B56 ;
          prmH00B56 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B57 ;
          prmH00B57 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaOrigemCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B58 ;
          prmH00B58 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaOrigemPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B59 ;
          prmH00B59 = new Object[] {
          new Object[] {"@ContagemResultado_LiqLogCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B510 ;
          prmH00B510 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B511 ;
          prmH00B511 = new Object[] {
          new Object[] {"@ContagemResultado_Servico",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B512 ;
          prmH00B512 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvUndCnt",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B513 ;
          prmH00B513 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B514 ;
          prmH00B514 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B515 ;
          prmH00B515 = new Object[] {
          new Object[] {"@ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B516 ;
          prmH00B516 = new Object[] {
          new Object[] {"@ContagemResultado_AceiteUserCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B517 ;
          prmH00B517 = new Object[] {
          new Object[] {"@ContagemResultado_AceitePessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B518 ;
          prmH00B518 = new Object[] {
          new Object[] {"@ContagemResultado_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B519 ;
          prmH00B519 = new Object[] {
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B520 ;
          prmH00B520 = new Object[] {
          new Object[] {"@ContagemResultado_ResponsavelPessCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B521 ;
          prmH00B521 = new Object[] {
          } ;
          Object[] prmH00B522 ;
          prmH00B522 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferH00B522 ;
          cmdBufferH00B522=" SELECT T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt], T3.[ContagemResultado_ContadorFSCod] AS ContagemResultado_ContadorFSCo, T4.[Usuario_PessoaCod] AS ContagemResultado_CrFSPessoaCo, T20.[Usuario_PessoaCod] AS ContagemResultado_ResponsavelP, T3.[ContagemResultado_NaoCnfDmnCod] AS ContagemResultado_NaoCnfDmnCod, T3.[Modulo_Codigo], T14.[Contratada_PessoaCod] AS ContagemResultado_ContratadaPe, T3.[ContagemResultado_ContratadaOrigemCod] AS ContagemResultado_ContratadaOr, T8.[Contratada_PessoaCod] AS ContagemResultado_ContratadaOr, T17.[Usuario_PessoaCod] AS ContagemResultado_AceitePessoa, T3.[ContagemResultado_LiqLogCod] AS ContagemResultado_LiqLogCod, T3.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_ContadorFMCod] AS ContagemResultado_ContadorFMCo, T1.[ContagemResultado_Codigo], T10.[ContagemResultadoLiqLog_Data], T11.[Servico_Codigo] AS ContagemResultado_Servico, T3.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCo, T11.[ContratoServicos_UnidadeContratada] AS ContagemResultado_CntSrvUndCnt, T3.[ContagemResultado_LoteAceiteCod] AS ContagemResultado_LoteAceiteCod, T16.[Lote_NFe] AS ContagemResultado_LoteNFe, T16.[Lote_UserCod] AS ContagemResultado_AceiteUserCod, T2.[Usuario_EhContratante] AS ContagemResultado_CrFMEhContratante, T21.[Pessoa_Nome] AS ContagemResultado_ResponsavelP, T3.[ContagemResultado_OSVinculada], T11.[ContratoServicos_SolicitaGestorSistema], T3.[ContagemResultado_Demanda], T3.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T18.[Pessoa_Nome] AS ContagemResultado_AceiteUserNo, T16.[Lote_Data] AS ContagemResultado_DataAceite, T16.[Lote_Nome], T16.[Lote_Numero] AS ContagemResultado_LoteAceite, T3.[ContagemResultado_StatusDmn], T3.[ContagemResultado_HoraEntrega], T3.[ContagemResultado_DataEntrega], "
          + " T3.[ContagemResultado_DataCadastro], T3.[ContagemResultado_Owner], T3.[ContagemResultado_PFLFSImp], T3.[ContagemResultado_PFBFSImp], T3.[ContagemResultado_Agrupador], T6.[NaoConformidade_Nome] AS ContagemResultado_NaoCnfDmnNom, T19.[Sistema_Coordenacao] AS ContagemResultado_SistemaCoord, T3.[ContagemResultado_Link], T3.[ContagemResultado_FncUsrCod], T7.[Modulo_Nome], T19.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T3.[ContagemResultado_QuantidadeSolicitada], T11.[ContratoServicos_QntUntCns] AS ContagemResultado_CntSrvQtdUntCns, T13.[UnidadeMedicao_Nome] AS ContagemResultado_CntSrvUndCnt, T12.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T5.[Pessoa_Nome] AS ContagemResultado_ContadorFSNo, T9.[Pessoa_Nome] AS ContagemResultado_ContratadaOr, T15.[Pessoa_Nome] AS ContagemResultado_ContratadaPe, T3.[ContagemResultado_SS], T3.[ContagemResultado_DataDmn], T3.[ContagemResultado_CntSrvPrrCod], T14.[Contratada_AreaTrabalhoCod], T3.[ContagemResultado_Responsavel] AS ContagemResultado_Responsavel FROM (((((((((((((((((((([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultado_ContadorFMCod]) INNER JOIN [ContagemResultado] T3 WITH (NOLOCK) ON T3.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T3.[ContagemResultado_ContadorFSCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod]) LEFT JOIN [NaoConformidade] T6 WITH (NOLOCK) ON T6.[NaoConformidade_Codigo] = T3.[ContagemResultado_NaoCnfDmnCod]) LEFT JOIN [Modulo] T7 WITH (NOLOCK) ON T7.[Modulo_Codigo] = T3.[Modulo_Codigo]) LEFT JOIN [Contratada] T8 WITH (NOLOCK) ON T8.[Contratada_Codigo] = T3.[ContagemResultado_ContratadaOrigemCod]) LEFT JOIN"
          + " [Pessoa] T9 WITH (NOLOCK) ON T9.[Pessoa_Codigo] = T8.[Contratada_PessoaCod]) LEFT JOIN [ContagemResultadoLiqLog] T10 WITH (NOLOCK) ON T10.[ContagemResultadoLiqLog_Codigo] = T3.[ContagemResultado_LiqLogCod]) LEFT JOIN [ContratoServicos] T11 WITH (NOLOCK) ON T11.[ContratoServicos_Codigo] = T3.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T12 WITH (NOLOCK) ON T12.[Servico_Codigo] = T11.[Servico_Codigo]) LEFT JOIN [UnidadeMedicao] T13 WITH (NOLOCK) ON T13.[UnidadeMedicao_Codigo] = T11.[ContratoServicos_UnidadeContratada]) LEFT JOIN [Contratada] T14 WITH (NOLOCK) ON T14.[Contratada_Codigo] = T3.[ContagemResultado_ContratadaCod]) LEFT JOIN [Pessoa] T15 WITH (NOLOCK) ON T15.[Pessoa_Codigo] = T14.[Contratada_PessoaCod]) LEFT JOIN [Lote] T16 WITH (NOLOCK) ON T16.[Lote_Codigo] = T3.[ContagemResultado_LoteAceiteCod]) LEFT JOIN [Usuario] T17 WITH (NOLOCK) ON T17.[Usuario_Codigo] = T16.[Lote_UserCod]) LEFT JOIN [Pessoa] T18 WITH (NOLOCK) ON T18.[Pessoa_Codigo] = T17.[Usuario_PessoaCod]) LEFT JOIN [Sistema] T19 WITH (NOLOCK) ON T19.[Sistema_Codigo] = T3.[ContagemResultado_SistemaCod]) LEFT JOIN [Usuario] T20 WITH (NOLOCK) ON T20.[Usuario_Codigo] = T3.[ContagemResultado_Responsavel]) LEFT JOIN [Pessoa] T21 WITH (NOLOCK) ON T21.[Pessoa_Codigo] = T20.[Usuario_PessoaCod]) WHERE T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo]" ;
          Object[] prmH00B523 ;
          prmH00B523 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B524 ;
          prmH00B524 = new Object[] {
          new Object[] {"@ContagemResultado_ContadorFSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B525 ;
          prmH00B525 = new Object[] {
          new Object[] {"@ContagemResultado_CrFSPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B526 ;
          prmH00B526 = new Object[] {
          new Object[] {"@ContagemResultado_NaoCnfDmnCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B527 ;
          prmH00B527 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B528 ;
          prmH00B528 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaOrigemCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B529 ;
          prmH00B529 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaOrigemPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B530 ;
          prmH00B530 = new Object[] {
          new Object[] {"@ContagemResultado_LiqLogCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B531 ;
          prmH00B531 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B532 ;
          prmH00B532 = new Object[] {
          new Object[] {"@ContagemResultado_Servico",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B533 ;
          prmH00B533 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvUndCnt",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B534 ;
          prmH00B534 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B535 ;
          prmH00B535 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaPessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B536 ;
          prmH00B536 = new Object[] {
          new Object[] {"@ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B537 ;
          prmH00B537 = new Object[] {
          new Object[] {"@ContagemResultado_AceiteUserCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B538 ;
          prmH00B538 = new Object[] {
          new Object[] {"@ContagemResultado_AceitePessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B539 ;
          prmH00B539 = new Object[] {
          new Object[] {"@ContagemResultado_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B540 ;
          prmH00B540 = new Object[] {
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B541 ;
          prmH00B541 = new Object[] {
          new Object[] {"@ContagemResultado_ResponsavelPessCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B542 ;
          prmH00B542 = new Object[] {
          new Object[] {"@ContagemResultado_Servico",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B543 ;
          prmH00B543 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B544 ;
          prmH00B544 = new Object[] {
          new Object[] {"@AV6WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00B545 ;
          prmH00B545 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B546 ;
          prmH00B546 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B547 ;
          prmH00B547 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B548 ;
          prmH00B548 = new Object[] {
          new Object[] {"@AV21ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B549 ;
          prmH00B549 = new Object[] {
          new Object[] {"@AV21ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B550 ;
          prmH00B550 = new Object[] {
          new Object[] {"@AV37ContagemResultado_Owner",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00B52", "SELECT [ContagemResultado_ContadorFSCod] AS ContagemResultado_ContadorFSCo, [ContagemResultado_NaoCnfDmnCod] AS ContagemResultado_NaoCnfDmnCod, [Modulo_Codigo], [ContagemResultado_ContratadaOrigemCod] AS ContagemResultado_ContratadaOr, [ContagemResultado_LiqLogCod] AS ContagemResultado_LiqLogCod, [ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCo, [ContagemResultado_LoteAceiteCod] AS ContagemResultado_LoteAceiteCod, [ContagemResultado_OSVinculada], [ContagemResultado_Demanda], [ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, [ContagemResultado_StatusDmn], [ContagemResultado_HoraEntrega], [ContagemResultado_DataEntrega], [ContagemResultado_DataCadastro], [ContagemResultado_Owner], [ContagemResultado_PFLFSImp], [ContagemResultado_PFBFSImp], [ContagemResultado_Agrupador], [ContagemResultado_Link], [ContagemResultado_FncUsrCod], [ContagemResultado_QuantidadeSolicitada], [ContagemResultado_SS], [ContagemResultado_DataDmn], [ContagemResultado_CntSrvPrrCod], [ContagemResultado_Responsavel] AS ContagemResultado_Responsavel FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B52,1,0,true,true )
             ,new CursorDef("H00B53", "SELECT [Usuario_PessoaCod] AS ContagemResultado_CrFSPessoaCo FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultado_ContadorFSCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B53,1,0,true,true )
             ,new CursorDef("H00B54", "SELECT [Pessoa_Nome] AS ContagemResultado_ContadorFSNo FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultado_CrFSPessoaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B54,1,0,true,true )
             ,new CursorDef("H00B55", "SELECT [NaoConformidade_Nome] AS ContagemResultado_NaoCnfDmnNom FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @ContagemResultado_NaoCnfDmnCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B55,1,0,true,true )
             ,new CursorDef("H00B56", "SELECT [Modulo_Nome] FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B56,1,0,true,true )
             ,new CursorDef("H00B57", "SELECT [Contratada_PessoaCod] AS ContagemResultado_ContratadaOr FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaOrigemCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B57,1,0,true,true )
             ,new CursorDef("H00B58", "SELECT [Pessoa_Nome] AS ContagemResultado_ContratadaOr FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultado_ContratadaOrigemPesCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B58,1,0,true,true )
             ,new CursorDef("H00B59", "SELECT [ContagemResultadoLiqLog_Data] FROM [ContagemResultadoLiqLog] WITH (NOLOCK) WHERE [ContagemResultadoLiqLog_Codigo] = @ContagemResultado_LiqLogCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B59,1,0,true,true )
             ,new CursorDef("H00B510", "SELECT [Servico_Codigo] AS ContagemResultado_Servico, [ContratoServicos_UnidadeContratada] AS ContagemResultado_CntSrvUndCnt, [ContratoServicos_SolicitaGestorSistema], [ContratoServicos_QntUntCns] AS ContagemResultado_CntSrvQtdUntCns FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContagemResultado_CntSrvCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B510,1,0,true,true )
             ,new CursorDef("H00B511", "SELECT [Servico_Sigla] AS ContagemResultado_ServicoSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContagemResultado_Servico ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B511,1,0,true,true )
             ,new CursorDef("H00B512", "SELECT [UnidadeMedicao_Nome] AS ContagemResultado_CntSrvUndCnt FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @ContagemResultado_CntSrvUndCnt ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B512,1,0,true,true )
             ,new CursorDef("H00B513", "SELECT [Contratada_PessoaCod] AS ContagemResultado_ContratadaPe, [Contratada_AreaTrabalhoCod] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B513,1,0,true,true )
             ,new CursorDef("H00B514", "SELECT [Pessoa_Nome] AS ContagemResultado_ContratadaPe FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultado_ContratadaPessoaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B514,1,0,true,true )
             ,new CursorDef("H00B515", "SELECT [Lote_NFe] AS ContagemResultado_LoteNFe, [Lote_UserCod] AS ContagemResultado_AceiteUserCod, [Lote_Data] AS ContagemResultado_DataAceite, [Lote_Nome], [Lote_Numero] AS ContagemResultado_LoteAceite FROM [Lote] WITH (NOLOCK) WHERE [Lote_Codigo] = @ContagemResultado_LoteAceiteCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B515,1,0,true,true )
             ,new CursorDef("H00B516", "SELECT [Usuario_PessoaCod] AS ContagemResultado_AceitePessoa FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultado_AceiteUserCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B516,1,0,true,true )
             ,new CursorDef("H00B517", "SELECT [Pessoa_Nome] AS ContagemResultado_AceiteUserNo FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultado_AceitePessoaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B517,1,0,true,true )
             ,new CursorDef("H00B518", "SELECT [Sistema_Coordenacao] AS ContagemResultado_SistemaCoord, [Sistema_Sigla] AS ContagemrResultado_SistemaSigla FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @ContagemResultado_SistemaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B518,1,0,true,true )
             ,new CursorDef("H00B519", "SELECT [Usuario_PessoaCod] AS ContagemResultado_ResponsavelP FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultado_Responsavel ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B519,1,0,true,true )
             ,new CursorDef("H00B520", "SELECT [Pessoa_Nome] AS ContagemResultado_ResponsavelP FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultado_ResponsavelPessCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B520,1,0,true,true )
             ,new CursorDef("H00B521", "SELECT T2.[Pessoa_Codigo] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B521,0,0,true,false )
             ,new CursorDef("H00B522", cmdBufferH00B522,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B522,100,0,true,false )
             ,new CursorDef("H00B523", "SELECT [ContagemResultado_ContadorFSCod] AS ContagemResultado_ContadorFSCo, [ContagemResultado_NaoCnfDmnCod] AS ContagemResultado_NaoCnfDmnCod, [Modulo_Codigo], [ContagemResultado_ContratadaOrigemCod] AS ContagemResultado_ContratadaOr, [ContagemResultado_LiqLogCod] AS ContagemResultado_LiqLogCod, [ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCo, [ContagemResultado_LoteAceiteCod] AS ContagemResultado_LoteAceiteCod, [ContagemResultado_OSVinculada], [ContagemResultado_Demanda], [ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, [ContagemResultado_StatusDmn], [ContagemResultado_HoraEntrega], [ContagemResultado_DataEntrega], [ContagemResultado_DataCadastro], [ContagemResultado_Owner], [ContagemResultado_PFLFSImp], [ContagemResultado_PFBFSImp], [ContagemResultado_Agrupador], [ContagemResultado_Link], [ContagemResultado_FncUsrCod], [ContagemResultado_QuantidadeSolicitada], [ContagemResultado_SS], [ContagemResultado_DataDmn], [ContagemResultado_CntSrvPrrCod], [ContagemResultado_Responsavel] AS ContagemResultado_Responsavel FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B523,1,0,true,false )
             ,new CursorDef("H00B524", "SELECT [Usuario_PessoaCod] AS ContagemResultado_CrFSPessoaCo FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultado_ContadorFSCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B524,1,0,true,false )
             ,new CursorDef("H00B525", "SELECT [Pessoa_Nome] AS ContagemResultado_ContadorFSNo FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultado_CrFSPessoaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B525,1,0,true,false )
             ,new CursorDef("H00B526", "SELECT [NaoConformidade_Nome] AS ContagemResultado_NaoCnfDmnNom FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @ContagemResultado_NaoCnfDmnCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B526,1,0,true,false )
             ,new CursorDef("H00B527", "SELECT [Modulo_Nome] FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B527,1,0,true,false )
             ,new CursorDef("H00B528", "SELECT [Contratada_PessoaCod] AS ContagemResultado_ContratadaOr FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaOrigemCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B528,1,0,true,false )
             ,new CursorDef("H00B529", "SELECT [Pessoa_Nome] AS ContagemResultado_ContratadaOr FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultado_ContratadaOrigemPesCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B529,1,0,true,false )
             ,new CursorDef("H00B530", "SELECT [ContagemResultadoLiqLog_Data] FROM [ContagemResultadoLiqLog] WITH (NOLOCK) WHERE [ContagemResultadoLiqLog_Codigo] = @ContagemResultado_LiqLogCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B530,1,0,true,false )
             ,new CursorDef("H00B531", "SELECT [Servico_Codigo] AS ContagemResultado_Servico, [ContratoServicos_UnidadeContratada] AS ContagemResultado_CntSrvUndCnt, [ContratoServicos_SolicitaGestorSistema], [ContratoServicos_QntUntCns] AS ContagemResultado_CntSrvQtdUntCns FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContagemResultado_CntSrvCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B531,1,0,true,false )
             ,new CursorDef("H00B532", "SELECT [Servico_Sigla] AS ContagemResultado_ServicoSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContagemResultado_Servico ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B532,1,0,true,false )
             ,new CursorDef("H00B533", "SELECT [UnidadeMedicao_Nome] AS ContagemResultado_CntSrvUndCnt FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @ContagemResultado_CntSrvUndCnt ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B533,1,0,true,false )
             ,new CursorDef("H00B534", "SELECT [Contratada_PessoaCod] AS ContagemResultado_ContratadaPe, [Contratada_AreaTrabalhoCod] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B534,1,0,true,false )
             ,new CursorDef("H00B535", "SELECT [Pessoa_Nome] AS ContagemResultado_ContratadaPe FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultado_ContratadaPessoaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B535,1,0,true,false )
             ,new CursorDef("H00B536", "SELECT [Lote_NFe] AS ContagemResultado_LoteNFe, [Lote_UserCod] AS ContagemResultado_AceiteUserCod, [Lote_Data] AS ContagemResultado_DataAceite, [Lote_Nome], [Lote_Numero] AS ContagemResultado_LoteAceite FROM [Lote] WITH (NOLOCK) WHERE [Lote_Codigo] = @ContagemResultado_LoteAceiteCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B536,1,0,true,false )
             ,new CursorDef("H00B537", "SELECT [Usuario_PessoaCod] AS ContagemResultado_AceitePessoa FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultado_AceiteUserCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B537,1,0,true,false )
             ,new CursorDef("H00B538", "SELECT [Pessoa_Nome] AS ContagemResultado_AceiteUserNo FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultado_AceitePessoaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B538,1,0,true,false )
             ,new CursorDef("H00B539", "SELECT [Sistema_Coordenacao] AS ContagemResultado_SistemaCoord, [Sistema_Sigla] AS ContagemrResultado_SistemaSigla FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @ContagemResultado_SistemaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B539,1,0,true,false )
             ,new CursorDef("H00B540", "SELECT [Usuario_PessoaCod] AS ContagemResultado_ResponsavelP FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultado_Responsavel ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B540,1,0,true,false )
             ,new CursorDef("H00B541", "SELECT [Pessoa_Nome] AS ContagemResultado_ResponsavelP FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultado_ResponsavelPessCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B541,1,0,true,false )
             ,new CursorDef("H00B542", "SELECT TOP 1 [ContratoServicos_Codigo], [Contrato_Codigo], [Servico_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContagemResultado_Servico ORDER BY [Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B542,1,0,true,true )
             ,new CursorDef("H00B543", "SELECT [Contratada_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B543,1,0,true,false )
             ,new CursorDef("H00B544", "SELECT TOP 1 [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_UsuarioCod] = @AV6WWPContext__Userid ORDER BY [ContratoGestor_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B544,1,0,false,true )
             ,new CursorDef("H00B545", "SELECT T1.[ContagemResultadoLiqLog_Codigo], T1.[ContagemResultadoLiqLogOS_Codigo], T1.[ContagemResultadoLiqLogOS_UserCod] AS ContagemResultadoLiqLogOS_UserCod, T2.[Usuario_PessoaCod] AS ContagemResultadoLiqLogOS_UserPesCod, T1.[ContagemResultadoLiqLogOS_OSCod], T1.[ContagemResultadoLiqLogOS_Valor], T3.[Pessoa_Nome] AS ContagemResultadoLiqLogOS_UserNom FROM (([ContagemResultadoLiqLogOS] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultadoLiqLogOS_UserCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE T1.[ContagemResultadoLiqLogOS_OSCod] = @ContagemResultado_Codigo ORDER BY T1.[ContagemResultadoLiqLogOS_OSCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B545,100,0,false,false )
             ,new CursorDef("H00B546", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicos_SolicitaGestorSistema] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContagemResultado_CntSrvCod ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B546,1,0,false,true )
             ,new CursorDef("H00B547", "SELECT TOP 1 T1.[ContratoServicosUnidConversao_Codigo] AS ContratoServicosUnidConversao_Codigo, T1.[ContratoServicos_Codigo], T2.[UnidadeMedicao_Sigla] AS ContratoServicosUnidConversao_Sigla FROM ([ContratoServicosUnidConversao] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[ContratoServicosUnidConversao_Codigo]) WHERE T1.[ContratoServicos_Codigo] = @ContagemResultado_CntSrvCod ORDER BY T1.[ContratoServicos_Codigo], T1.[ContratoServicosUnidConversao_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B547,1,0,false,true )
             ,new CursorDef("H00B548", "SELECT TOP 1 T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T2.[Contratada_Sigla] FROM ([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) WHERE (T1.[ContratadaUsuario_UsuarioCod] = @AV21ContagemResultado_Responsavel) AND (T2.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo) ORDER BY T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B548,1,0,false,true )
             ,new CursorDef("H00B549", "SELECT TOP 1 T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T3.[Usuario_EhContratada] AS ContratadaUsuario_UsuarioEhContratada, T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T2.[Contratada_Sigla] FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) WHERE (T1.[ContratadaUsuario_UsuarioCod] = @AV21ContagemResultado_Responsavel) AND (T3.[Usuario_EhContratada] = 1) AND (T2.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo) ORDER BY T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B549,1,0,false,true )
             ,new CursorDef("H00B550", "SELECT T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Telefone] AS Usuario_PessoaTelefone, T1.[Usuario_Email] FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE T1.[Usuario_Codigo] = @AV37ContagemResultado_Owner ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B550,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((String[]) buf[18])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((int[]) buf[20])[0] = rslt.getInt(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((String[]) buf[22])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[24])[0] = rslt.getGXDateTime(13) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[26])[0] = rslt.getGXDate(14) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(14);
                ((DateTime[]) buf[28])[0] = rslt.getGXDateTime(15) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(15);
                ((int[]) buf[30])[0] = rslt.getInt(16) ;
                ((decimal[]) buf[31])[0] = rslt.getDecimal(17) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(17);
                ((decimal[]) buf[33])[0] = rslt.getDecimal(18) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(18);
                ((String[]) buf[35])[0] = rslt.getString(19, 15) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(19);
                ((String[]) buf[37])[0] = rslt.getLongVarchar(20) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(20);
                ((int[]) buf[39])[0] = rslt.getInt(21) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(21);
                ((decimal[]) buf[41])[0] = rslt.getDecimal(22) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(22);
                ((int[]) buf[43])[0] = rslt.getInt(23) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(23);
                ((DateTime[]) buf[45])[0] = rslt.getGXDate(24) ;
                ((int[]) buf[46])[0] = rslt.getInt(25) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(25);
                ((int[]) buf[48])[0] = rslt.getInt(26) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(26);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDateTime(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((bool[]) buf[4])[0] = rslt.getBool(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[4])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 16 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 25) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 20 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 5) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((int[]) buf[22])[0] = rslt.getInt(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((int[]) buf[24])[0] = rslt.getInt(14) ;
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((DateTime[]) buf[26])[0] = rslt.getGXDateTime(16) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(16);
                ((int[]) buf[28])[0] = rslt.getInt(17) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(17);
                ((int[]) buf[30])[0] = rslt.getInt(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                ((int[]) buf[32])[0] = rslt.getInt(19) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(19);
                ((int[]) buf[34])[0] = rslt.getInt(20) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(20);
                ((int[]) buf[36])[0] = rslt.getInt(21) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(21);
                ((int[]) buf[38])[0] = rslt.getInt(22) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(22);
                ((bool[]) buf[40])[0] = rslt.getBool(23) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(23);
                ((String[]) buf[42])[0] = rslt.getString(24, 100) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(24);
                ((int[]) buf[44])[0] = rslt.getInt(25) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(25);
                ((bool[]) buf[46])[0] = rslt.getBool(26) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(26);
                ((String[]) buf[48])[0] = rslt.getVarchar(27) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(27);
                ((int[]) buf[50])[0] = rslt.getInt(28) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(28);
                ((String[]) buf[52])[0] = rslt.getString(29, 100) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(29);
                ((DateTime[]) buf[54])[0] = rslt.getGXDateTime(30) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(30);
                ((String[]) buf[56])[0] = rslt.getString(31, 50) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(31);
                ((String[]) buf[58])[0] = rslt.getString(32, 10) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(32);
                ((String[]) buf[60])[0] = rslt.getString(33, 1) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(33);
                ((DateTime[]) buf[62])[0] = rslt.getGXDateTime(34) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(34);
                ((DateTime[]) buf[64])[0] = rslt.getGXDate(35) ;
                ((bool[]) buf[65])[0] = rslt.wasNull(35);
                ((DateTime[]) buf[66])[0] = rslt.getGXDateTime(36) ;
                ((bool[]) buf[67])[0] = rslt.wasNull(36);
                ((int[]) buf[68])[0] = rslt.getInt(37) ;
                ((decimal[]) buf[69])[0] = rslt.getDecimal(38) ;
                ((bool[]) buf[70])[0] = rslt.wasNull(38);
                ((decimal[]) buf[71])[0] = rslt.getDecimal(39) ;
                ((bool[]) buf[72])[0] = rslt.wasNull(39);
                ((String[]) buf[73])[0] = rslt.getString(40, 15) ;
                ((bool[]) buf[74])[0] = rslt.wasNull(40);
                ((String[]) buf[75])[0] = rslt.getString(41, 50) ;
                ((bool[]) buf[76])[0] = rslt.wasNull(41);
                ((String[]) buf[77])[0] = rslt.getVarchar(42) ;
                ((bool[]) buf[78])[0] = rslt.wasNull(42);
                ((String[]) buf[79])[0] = rslt.getLongVarchar(43) ;
                ((bool[]) buf[80])[0] = rslt.wasNull(43);
                ((int[]) buf[81])[0] = rslt.getInt(44) ;
                ((bool[]) buf[82])[0] = rslt.wasNull(44);
                ((String[]) buf[83])[0] = rslt.getString(45, 50) ;
                ((String[]) buf[84])[0] = rslt.getString(46, 25) ;
                ((bool[]) buf[85])[0] = rslt.wasNull(46);
                ((decimal[]) buf[86])[0] = rslt.getDecimal(47) ;
                ((bool[]) buf[87])[0] = rslt.wasNull(47);
                ((decimal[]) buf[88])[0] = rslt.getDecimal(48) ;
                ((bool[]) buf[89])[0] = rslt.wasNull(48);
                ((String[]) buf[90])[0] = rslt.getString(49, 50) ;
                ((bool[]) buf[91])[0] = rslt.wasNull(49);
                ((String[]) buf[92])[0] = rslt.getString(50, 15) ;
                ((bool[]) buf[93])[0] = rslt.wasNull(50);
                ((String[]) buf[94])[0] = rslt.getString(51, 100) ;
                ((bool[]) buf[95])[0] = rslt.wasNull(51);
                ((String[]) buf[96])[0] = rslt.getString(52, 100) ;
                ((bool[]) buf[97])[0] = rslt.wasNull(52);
                ((String[]) buf[98])[0] = rslt.getString(53, 100) ;
                ((bool[]) buf[99])[0] = rslt.wasNull(53);
                ((int[]) buf[100])[0] = rslt.getInt(54) ;
                ((bool[]) buf[101])[0] = rslt.wasNull(54);
                ((DateTime[]) buf[102])[0] = rslt.getGXDate(55) ;
                ((int[]) buf[103])[0] = rslt.getInt(56) ;
                ((bool[]) buf[104])[0] = rslt.wasNull(56);
                ((int[]) buf[105])[0] = rslt.getInt(57) ;
                ((bool[]) buf[106])[0] = rslt.wasNull(57);
                ((int[]) buf[107])[0] = rslt.getInt(58) ;
                ((bool[]) buf[108])[0] = rslt.wasNull(58);
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((String[]) buf[18])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((int[]) buf[20])[0] = rslt.getInt(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((String[]) buf[22])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((DateTime[]) buf[24])[0] = rslt.getGXDateTime(13) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(13);
                ((DateTime[]) buf[26])[0] = rslt.getGXDate(14) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(14);
                ((DateTime[]) buf[28])[0] = rslt.getGXDateTime(15) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(15);
                ((int[]) buf[30])[0] = rslt.getInt(16) ;
                ((decimal[]) buf[31])[0] = rslt.getDecimal(17) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(17);
                ((decimal[]) buf[33])[0] = rslt.getDecimal(18) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(18);
                ((String[]) buf[35])[0] = rslt.getString(19, 15) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(19);
                ((String[]) buf[37])[0] = rslt.getLongVarchar(20) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(20);
                ((int[]) buf[39])[0] = rslt.getInt(21) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(21);
                ((decimal[]) buf[41])[0] = rslt.getDecimal(22) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(22);
                ((int[]) buf[43])[0] = rslt.getInt(23) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(23);
                ((DateTime[]) buf[45])[0] = rslt.getGXDate(24) ;
                ((int[]) buf[46])[0] = rslt.getInt(25) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(25);
                ((int[]) buf[48])[0] = rslt.getInt(26) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(26);
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 23 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 24 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 25 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 26 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 27 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 28 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDateTime(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 29 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((bool[]) buf[4])[0] = rslt.getBool(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 31 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 32 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 33 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 34 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[4])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                return;
             case 35 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 36 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 37 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 25) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 38 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 39 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 40 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 41 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 42 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 43 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(6) ;
                ((String[]) buf[7])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                return;
             case 44 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                return;
             case 45 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 46 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 47 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 48 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 15 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 18 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 23 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 24 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 25 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 26 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 27 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 28 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 29 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 31 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 32 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 33 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 34 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 35 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 36 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 37 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 38 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 39 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 40 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 41 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 42 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 43 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 44 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 45 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 46 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 47 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 48 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
