/*
               File: GetWWUnidadeMedicaoFilterData
        Description: Get WWUnidade Medicao Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:54:22.81
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwunidademedicaofilterdata : GXProcedure
   {
      public getwwunidademedicaofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwunidademedicaofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV17DDOName = aP0_DDOName;
         this.AV15SearchTxt = aP1_SearchTxt;
         this.AV16SearchTxtTo = aP2_SearchTxtTo;
         this.AV21OptionsJson = "" ;
         this.AV24OptionsDescJson = "" ;
         this.AV26OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV21OptionsJson;
         aP4_OptionsDescJson=this.AV24OptionsDescJson;
         aP5_OptionIndexesJson=this.AV26OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV17DDOName = aP0_DDOName;
         this.AV15SearchTxt = aP1_SearchTxt;
         this.AV16SearchTxtTo = aP2_SearchTxtTo;
         this.AV21OptionsJson = "" ;
         this.AV24OptionsDescJson = "" ;
         this.AV26OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV21OptionsJson;
         aP4_OptionsDescJson=this.AV24OptionsDescJson;
         aP5_OptionIndexesJson=this.AV26OptionIndexesJson;
         return AV26OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwunidademedicaofilterdata objgetwwunidademedicaofilterdata;
         objgetwwunidademedicaofilterdata = new getwwunidademedicaofilterdata();
         objgetwwunidademedicaofilterdata.AV17DDOName = aP0_DDOName;
         objgetwwunidademedicaofilterdata.AV15SearchTxt = aP1_SearchTxt;
         objgetwwunidademedicaofilterdata.AV16SearchTxtTo = aP2_SearchTxtTo;
         objgetwwunidademedicaofilterdata.AV21OptionsJson = "" ;
         objgetwwunidademedicaofilterdata.AV24OptionsDescJson = "" ;
         objgetwwunidademedicaofilterdata.AV26OptionIndexesJson = "" ;
         objgetwwunidademedicaofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwunidademedicaofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwunidademedicaofilterdata);
         aP3_OptionsJson=this.AV21OptionsJson;
         aP4_OptionsDescJson=this.AV24OptionsDescJson;
         aP5_OptionIndexesJson=this.AV26OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwunidademedicaofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV20Options = (IGxCollection)(new GxSimpleCollection());
         AV23OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV25OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV17DDOName), "DDO_UNIDADEMEDICAO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADUNIDADEMEDICAO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV17DDOName), "DDO_UNIDADEMEDICAO_SIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADUNIDADEMEDICAO_SIGLAOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV21OptionsJson = AV20Options.ToJSonString(false);
         AV24OptionsDescJson = AV23OptionsDesc.ToJSonString(false);
         AV26OptionIndexesJson = AV25OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV28Session.Get("WWUnidadeMedicaoGridState"), "") == 0 )
         {
            AV30GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWUnidadeMedicaoGridState"), "");
         }
         else
         {
            AV30GridState.FromXml(AV28Session.Get("WWUnidadeMedicaoGridState"), "");
         }
         AV43GXV1 = 1;
         while ( AV43GXV1 <= AV30GridState.gxTpr_Filtervalues.Count )
         {
            AV31GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV30GridState.gxTpr_Filtervalues.Item(AV43GXV1));
            if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFUNIDADEMEDICAO_NOME") == 0 )
            {
               AV10TFUnidadeMedicao_Nome = AV31GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFUNIDADEMEDICAO_NOME_SEL") == 0 )
            {
               AV11TFUnidadeMedicao_Nome_Sel = AV31GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFUNIDADEMEDICAO_SIGLA") == 0 )
            {
               AV12TFUnidadeMedicao_Sigla = AV31GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFUNIDADEMEDICAO_SIGLA_SEL") == 0 )
            {
               AV13TFUnidadeMedicao_Sigla_Sel = AV31GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFUNIDADEMEDICAO_ATIVO_SEL") == 0 )
            {
               AV14TFUnidadeMedicao_Ativo_Sel = (short)(NumberUtil.Val( AV31GridStateFilterValue.gxTpr_Value, "."));
            }
            AV43GXV1 = (int)(AV43GXV1+1);
         }
         if ( AV30GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV32GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV30GridState.gxTpr_Dynamicfilters.Item(1));
            AV33DynamicFiltersSelector1 = AV32GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "UNIDADEMEDICAO_NOME") == 0 )
            {
               AV34UnidadeMedicao_Nome1 = AV32GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV30GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV35DynamicFiltersEnabled2 = true;
               AV32GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV30GridState.gxTpr_Dynamicfilters.Item(2));
               AV36DynamicFiltersSelector2 = AV32GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV36DynamicFiltersSelector2, "UNIDADEMEDICAO_NOME") == 0 )
               {
                  AV37UnidadeMedicao_Nome2 = AV32GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV30GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV38DynamicFiltersEnabled3 = true;
                  AV32GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV30GridState.gxTpr_Dynamicfilters.Item(3));
                  AV39DynamicFiltersSelector3 = AV32GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "UNIDADEMEDICAO_NOME") == 0 )
                  {
                     AV40UnidadeMedicao_Nome3 = AV32GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADUNIDADEMEDICAO_NOMEOPTIONS' Routine */
         AV10TFUnidadeMedicao_Nome = AV15SearchTxt;
         AV11TFUnidadeMedicao_Nome_Sel = "";
         AV45WWUnidadeMedicaoDS_1_Dynamicfiltersselector1 = AV33DynamicFiltersSelector1;
         AV46WWUnidadeMedicaoDS_2_Unidademedicao_nome1 = AV34UnidadeMedicao_Nome1;
         AV47WWUnidadeMedicaoDS_3_Dynamicfiltersenabled2 = AV35DynamicFiltersEnabled2;
         AV48WWUnidadeMedicaoDS_4_Dynamicfiltersselector2 = AV36DynamicFiltersSelector2;
         AV49WWUnidadeMedicaoDS_5_Unidademedicao_nome2 = AV37UnidadeMedicao_Nome2;
         AV50WWUnidadeMedicaoDS_6_Dynamicfiltersenabled3 = AV38DynamicFiltersEnabled3;
         AV51WWUnidadeMedicaoDS_7_Dynamicfiltersselector3 = AV39DynamicFiltersSelector3;
         AV52WWUnidadeMedicaoDS_8_Unidademedicao_nome3 = AV40UnidadeMedicao_Nome3;
         AV53WWUnidadeMedicaoDS_9_Tfunidademedicao_nome = AV10TFUnidadeMedicao_Nome;
         AV54WWUnidadeMedicaoDS_10_Tfunidademedicao_nome_sel = AV11TFUnidadeMedicao_Nome_Sel;
         AV55WWUnidadeMedicaoDS_11_Tfunidademedicao_sigla = AV12TFUnidadeMedicao_Sigla;
         AV56WWUnidadeMedicaoDS_12_Tfunidademedicao_sigla_sel = AV13TFUnidadeMedicao_Sigla_Sel;
         AV57WWUnidadeMedicaoDS_13_Tfunidademedicao_ativo_sel = AV14TFUnidadeMedicao_Ativo_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV45WWUnidadeMedicaoDS_1_Dynamicfiltersselector1 ,
                                              AV46WWUnidadeMedicaoDS_2_Unidademedicao_nome1 ,
                                              AV47WWUnidadeMedicaoDS_3_Dynamicfiltersenabled2 ,
                                              AV48WWUnidadeMedicaoDS_4_Dynamicfiltersselector2 ,
                                              AV49WWUnidadeMedicaoDS_5_Unidademedicao_nome2 ,
                                              AV50WWUnidadeMedicaoDS_6_Dynamicfiltersenabled3 ,
                                              AV51WWUnidadeMedicaoDS_7_Dynamicfiltersselector3 ,
                                              AV52WWUnidadeMedicaoDS_8_Unidademedicao_nome3 ,
                                              AV54WWUnidadeMedicaoDS_10_Tfunidademedicao_nome_sel ,
                                              AV53WWUnidadeMedicaoDS_9_Tfunidademedicao_nome ,
                                              AV56WWUnidadeMedicaoDS_12_Tfunidademedicao_sigla_sel ,
                                              AV55WWUnidadeMedicaoDS_11_Tfunidademedicao_sigla ,
                                              AV57WWUnidadeMedicaoDS_13_Tfunidademedicao_ativo_sel ,
                                              A1198UnidadeMedicao_Nome ,
                                              A1200UnidadeMedicao_Sigla ,
                                              A1199UnidadeMedicao_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV46WWUnidadeMedicaoDS_2_Unidademedicao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV46WWUnidadeMedicaoDS_2_Unidademedicao_nome1), 50, "%");
         lV49WWUnidadeMedicaoDS_5_Unidademedicao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV49WWUnidadeMedicaoDS_5_Unidademedicao_nome2), 50, "%");
         lV52WWUnidadeMedicaoDS_8_Unidademedicao_nome3 = StringUtil.PadR( StringUtil.RTrim( AV52WWUnidadeMedicaoDS_8_Unidademedicao_nome3), 50, "%");
         lV53WWUnidadeMedicaoDS_9_Tfunidademedicao_nome = StringUtil.PadR( StringUtil.RTrim( AV53WWUnidadeMedicaoDS_9_Tfunidademedicao_nome), 50, "%");
         lV55WWUnidadeMedicaoDS_11_Tfunidademedicao_sigla = StringUtil.PadR( StringUtil.RTrim( AV55WWUnidadeMedicaoDS_11_Tfunidademedicao_sigla), 15, "%");
         /* Using cursor P00Q32 */
         pr_default.execute(0, new Object[] {lV46WWUnidadeMedicaoDS_2_Unidademedicao_nome1, lV49WWUnidadeMedicaoDS_5_Unidademedicao_nome2, lV52WWUnidadeMedicaoDS_8_Unidademedicao_nome3, lV53WWUnidadeMedicaoDS_9_Tfunidademedicao_nome, AV54WWUnidadeMedicaoDS_10_Tfunidademedicao_nome_sel, lV55WWUnidadeMedicaoDS_11_Tfunidademedicao_sigla, AV56WWUnidadeMedicaoDS_12_Tfunidademedicao_sigla_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKQ32 = false;
            A1198UnidadeMedicao_Nome = P00Q32_A1198UnidadeMedicao_Nome[0];
            A1199UnidadeMedicao_Ativo = P00Q32_A1199UnidadeMedicao_Ativo[0];
            A1200UnidadeMedicao_Sigla = P00Q32_A1200UnidadeMedicao_Sigla[0];
            A1197UnidadeMedicao_Codigo = P00Q32_A1197UnidadeMedicao_Codigo[0];
            AV27count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00Q32_A1198UnidadeMedicao_Nome[0], A1198UnidadeMedicao_Nome) == 0 ) )
            {
               BRKQ32 = false;
               A1197UnidadeMedicao_Codigo = P00Q32_A1197UnidadeMedicao_Codigo[0];
               AV27count = (long)(AV27count+1);
               BRKQ32 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1198UnidadeMedicao_Nome)) )
            {
               AV19Option = A1198UnidadeMedicao_Nome;
               AV20Options.Add(AV19Option, 0);
               AV25OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV27count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV20Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKQ32 )
            {
               BRKQ32 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADUNIDADEMEDICAO_SIGLAOPTIONS' Routine */
         AV12TFUnidadeMedicao_Sigla = AV15SearchTxt;
         AV13TFUnidadeMedicao_Sigla_Sel = "";
         AV45WWUnidadeMedicaoDS_1_Dynamicfiltersselector1 = AV33DynamicFiltersSelector1;
         AV46WWUnidadeMedicaoDS_2_Unidademedicao_nome1 = AV34UnidadeMedicao_Nome1;
         AV47WWUnidadeMedicaoDS_3_Dynamicfiltersenabled2 = AV35DynamicFiltersEnabled2;
         AV48WWUnidadeMedicaoDS_4_Dynamicfiltersselector2 = AV36DynamicFiltersSelector2;
         AV49WWUnidadeMedicaoDS_5_Unidademedicao_nome2 = AV37UnidadeMedicao_Nome2;
         AV50WWUnidadeMedicaoDS_6_Dynamicfiltersenabled3 = AV38DynamicFiltersEnabled3;
         AV51WWUnidadeMedicaoDS_7_Dynamicfiltersselector3 = AV39DynamicFiltersSelector3;
         AV52WWUnidadeMedicaoDS_8_Unidademedicao_nome3 = AV40UnidadeMedicao_Nome3;
         AV53WWUnidadeMedicaoDS_9_Tfunidademedicao_nome = AV10TFUnidadeMedicao_Nome;
         AV54WWUnidadeMedicaoDS_10_Tfunidademedicao_nome_sel = AV11TFUnidadeMedicao_Nome_Sel;
         AV55WWUnidadeMedicaoDS_11_Tfunidademedicao_sigla = AV12TFUnidadeMedicao_Sigla;
         AV56WWUnidadeMedicaoDS_12_Tfunidademedicao_sigla_sel = AV13TFUnidadeMedicao_Sigla_Sel;
         AV57WWUnidadeMedicaoDS_13_Tfunidademedicao_ativo_sel = AV14TFUnidadeMedicao_Ativo_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV45WWUnidadeMedicaoDS_1_Dynamicfiltersselector1 ,
                                              AV46WWUnidadeMedicaoDS_2_Unidademedicao_nome1 ,
                                              AV47WWUnidadeMedicaoDS_3_Dynamicfiltersenabled2 ,
                                              AV48WWUnidadeMedicaoDS_4_Dynamicfiltersselector2 ,
                                              AV49WWUnidadeMedicaoDS_5_Unidademedicao_nome2 ,
                                              AV50WWUnidadeMedicaoDS_6_Dynamicfiltersenabled3 ,
                                              AV51WWUnidadeMedicaoDS_7_Dynamicfiltersselector3 ,
                                              AV52WWUnidadeMedicaoDS_8_Unidademedicao_nome3 ,
                                              AV54WWUnidadeMedicaoDS_10_Tfunidademedicao_nome_sel ,
                                              AV53WWUnidadeMedicaoDS_9_Tfunidademedicao_nome ,
                                              AV56WWUnidadeMedicaoDS_12_Tfunidademedicao_sigla_sel ,
                                              AV55WWUnidadeMedicaoDS_11_Tfunidademedicao_sigla ,
                                              AV57WWUnidadeMedicaoDS_13_Tfunidademedicao_ativo_sel ,
                                              A1198UnidadeMedicao_Nome ,
                                              A1200UnidadeMedicao_Sigla ,
                                              A1199UnidadeMedicao_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV46WWUnidadeMedicaoDS_2_Unidademedicao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV46WWUnidadeMedicaoDS_2_Unidademedicao_nome1), 50, "%");
         lV49WWUnidadeMedicaoDS_5_Unidademedicao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV49WWUnidadeMedicaoDS_5_Unidademedicao_nome2), 50, "%");
         lV52WWUnidadeMedicaoDS_8_Unidademedicao_nome3 = StringUtil.PadR( StringUtil.RTrim( AV52WWUnidadeMedicaoDS_8_Unidademedicao_nome3), 50, "%");
         lV53WWUnidadeMedicaoDS_9_Tfunidademedicao_nome = StringUtil.PadR( StringUtil.RTrim( AV53WWUnidadeMedicaoDS_9_Tfunidademedicao_nome), 50, "%");
         lV55WWUnidadeMedicaoDS_11_Tfunidademedicao_sigla = StringUtil.PadR( StringUtil.RTrim( AV55WWUnidadeMedicaoDS_11_Tfunidademedicao_sigla), 15, "%");
         /* Using cursor P00Q33 */
         pr_default.execute(1, new Object[] {lV46WWUnidadeMedicaoDS_2_Unidademedicao_nome1, lV49WWUnidadeMedicaoDS_5_Unidademedicao_nome2, lV52WWUnidadeMedicaoDS_8_Unidademedicao_nome3, lV53WWUnidadeMedicaoDS_9_Tfunidademedicao_nome, AV54WWUnidadeMedicaoDS_10_Tfunidademedicao_nome_sel, lV55WWUnidadeMedicaoDS_11_Tfunidademedicao_sigla, AV56WWUnidadeMedicaoDS_12_Tfunidademedicao_sigla_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKQ34 = false;
            A1200UnidadeMedicao_Sigla = P00Q33_A1200UnidadeMedicao_Sigla[0];
            A1199UnidadeMedicao_Ativo = P00Q33_A1199UnidadeMedicao_Ativo[0];
            A1198UnidadeMedicao_Nome = P00Q33_A1198UnidadeMedicao_Nome[0];
            A1197UnidadeMedicao_Codigo = P00Q33_A1197UnidadeMedicao_Codigo[0];
            AV27count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00Q33_A1200UnidadeMedicao_Sigla[0], A1200UnidadeMedicao_Sigla) == 0 ) )
            {
               BRKQ34 = false;
               A1197UnidadeMedicao_Codigo = P00Q33_A1197UnidadeMedicao_Codigo[0];
               AV27count = (long)(AV27count+1);
               BRKQ34 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1200UnidadeMedicao_Sigla)) )
            {
               AV19Option = A1200UnidadeMedicao_Sigla;
               AV20Options.Add(AV19Option, 0);
               AV25OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV27count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV20Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKQ34 )
            {
               BRKQ34 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV20Options = new GxSimpleCollection();
         AV23OptionsDesc = new GxSimpleCollection();
         AV25OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV28Session = context.GetSession();
         AV30GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV31GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFUnidadeMedicao_Nome = "";
         AV11TFUnidadeMedicao_Nome_Sel = "";
         AV12TFUnidadeMedicao_Sigla = "";
         AV13TFUnidadeMedicao_Sigla_Sel = "";
         AV32GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV33DynamicFiltersSelector1 = "";
         AV34UnidadeMedicao_Nome1 = "";
         AV36DynamicFiltersSelector2 = "";
         AV37UnidadeMedicao_Nome2 = "";
         AV39DynamicFiltersSelector3 = "";
         AV40UnidadeMedicao_Nome3 = "";
         AV45WWUnidadeMedicaoDS_1_Dynamicfiltersselector1 = "";
         AV46WWUnidadeMedicaoDS_2_Unidademedicao_nome1 = "";
         AV48WWUnidadeMedicaoDS_4_Dynamicfiltersselector2 = "";
         AV49WWUnidadeMedicaoDS_5_Unidademedicao_nome2 = "";
         AV51WWUnidadeMedicaoDS_7_Dynamicfiltersselector3 = "";
         AV52WWUnidadeMedicaoDS_8_Unidademedicao_nome3 = "";
         AV53WWUnidadeMedicaoDS_9_Tfunidademedicao_nome = "";
         AV54WWUnidadeMedicaoDS_10_Tfunidademedicao_nome_sel = "";
         AV55WWUnidadeMedicaoDS_11_Tfunidademedicao_sigla = "";
         AV56WWUnidadeMedicaoDS_12_Tfunidademedicao_sigla_sel = "";
         scmdbuf = "";
         lV46WWUnidadeMedicaoDS_2_Unidademedicao_nome1 = "";
         lV49WWUnidadeMedicaoDS_5_Unidademedicao_nome2 = "";
         lV52WWUnidadeMedicaoDS_8_Unidademedicao_nome3 = "";
         lV53WWUnidadeMedicaoDS_9_Tfunidademedicao_nome = "";
         lV55WWUnidadeMedicaoDS_11_Tfunidademedicao_sigla = "";
         A1198UnidadeMedicao_Nome = "";
         A1200UnidadeMedicao_Sigla = "";
         P00Q32_A1198UnidadeMedicao_Nome = new String[] {""} ;
         P00Q32_A1199UnidadeMedicao_Ativo = new bool[] {false} ;
         P00Q32_A1200UnidadeMedicao_Sigla = new String[] {""} ;
         P00Q32_A1197UnidadeMedicao_Codigo = new int[1] ;
         AV19Option = "";
         P00Q33_A1200UnidadeMedicao_Sigla = new String[] {""} ;
         P00Q33_A1199UnidadeMedicao_Ativo = new bool[] {false} ;
         P00Q33_A1198UnidadeMedicao_Nome = new String[] {""} ;
         P00Q33_A1197UnidadeMedicao_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwunidademedicaofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00Q32_A1198UnidadeMedicao_Nome, P00Q32_A1199UnidadeMedicao_Ativo, P00Q32_A1200UnidadeMedicao_Sigla, P00Q32_A1197UnidadeMedicao_Codigo
               }
               , new Object[] {
               P00Q33_A1200UnidadeMedicao_Sigla, P00Q33_A1199UnidadeMedicao_Ativo, P00Q33_A1198UnidadeMedicao_Nome, P00Q33_A1197UnidadeMedicao_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV14TFUnidadeMedicao_Ativo_Sel ;
      private short AV57WWUnidadeMedicaoDS_13_Tfunidademedicao_ativo_sel ;
      private int AV43GXV1 ;
      private int A1197UnidadeMedicao_Codigo ;
      private long AV27count ;
      private String AV10TFUnidadeMedicao_Nome ;
      private String AV11TFUnidadeMedicao_Nome_Sel ;
      private String AV12TFUnidadeMedicao_Sigla ;
      private String AV13TFUnidadeMedicao_Sigla_Sel ;
      private String AV34UnidadeMedicao_Nome1 ;
      private String AV37UnidadeMedicao_Nome2 ;
      private String AV40UnidadeMedicao_Nome3 ;
      private String AV46WWUnidadeMedicaoDS_2_Unidademedicao_nome1 ;
      private String AV49WWUnidadeMedicaoDS_5_Unidademedicao_nome2 ;
      private String AV52WWUnidadeMedicaoDS_8_Unidademedicao_nome3 ;
      private String AV53WWUnidadeMedicaoDS_9_Tfunidademedicao_nome ;
      private String AV54WWUnidadeMedicaoDS_10_Tfunidademedicao_nome_sel ;
      private String AV55WWUnidadeMedicaoDS_11_Tfunidademedicao_sigla ;
      private String AV56WWUnidadeMedicaoDS_12_Tfunidademedicao_sigla_sel ;
      private String scmdbuf ;
      private String lV46WWUnidadeMedicaoDS_2_Unidademedicao_nome1 ;
      private String lV49WWUnidadeMedicaoDS_5_Unidademedicao_nome2 ;
      private String lV52WWUnidadeMedicaoDS_8_Unidademedicao_nome3 ;
      private String lV53WWUnidadeMedicaoDS_9_Tfunidademedicao_nome ;
      private String lV55WWUnidadeMedicaoDS_11_Tfunidademedicao_sigla ;
      private String A1198UnidadeMedicao_Nome ;
      private String A1200UnidadeMedicao_Sigla ;
      private bool returnInSub ;
      private bool AV35DynamicFiltersEnabled2 ;
      private bool AV38DynamicFiltersEnabled3 ;
      private bool AV47WWUnidadeMedicaoDS_3_Dynamicfiltersenabled2 ;
      private bool AV50WWUnidadeMedicaoDS_6_Dynamicfiltersenabled3 ;
      private bool A1199UnidadeMedicao_Ativo ;
      private bool BRKQ32 ;
      private bool BRKQ34 ;
      private String AV26OptionIndexesJson ;
      private String AV21OptionsJson ;
      private String AV24OptionsDescJson ;
      private String AV17DDOName ;
      private String AV15SearchTxt ;
      private String AV16SearchTxtTo ;
      private String AV33DynamicFiltersSelector1 ;
      private String AV36DynamicFiltersSelector2 ;
      private String AV39DynamicFiltersSelector3 ;
      private String AV45WWUnidadeMedicaoDS_1_Dynamicfiltersselector1 ;
      private String AV48WWUnidadeMedicaoDS_4_Dynamicfiltersselector2 ;
      private String AV51WWUnidadeMedicaoDS_7_Dynamicfiltersselector3 ;
      private String AV19Option ;
      private IGxSession AV28Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00Q32_A1198UnidadeMedicao_Nome ;
      private bool[] P00Q32_A1199UnidadeMedicao_Ativo ;
      private String[] P00Q32_A1200UnidadeMedicao_Sigla ;
      private int[] P00Q32_A1197UnidadeMedicao_Codigo ;
      private String[] P00Q33_A1200UnidadeMedicao_Sigla ;
      private bool[] P00Q33_A1199UnidadeMedicao_Ativo ;
      private String[] P00Q33_A1198UnidadeMedicao_Nome ;
      private int[] P00Q33_A1197UnidadeMedicao_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV20Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV30GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV31GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV32GridStateDynamicFilter ;
   }

   public class getwwunidademedicaofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00Q32( IGxContext context ,
                                             String AV45WWUnidadeMedicaoDS_1_Dynamicfiltersselector1 ,
                                             String AV46WWUnidadeMedicaoDS_2_Unidademedicao_nome1 ,
                                             bool AV47WWUnidadeMedicaoDS_3_Dynamicfiltersenabled2 ,
                                             String AV48WWUnidadeMedicaoDS_4_Dynamicfiltersselector2 ,
                                             String AV49WWUnidadeMedicaoDS_5_Unidademedicao_nome2 ,
                                             bool AV50WWUnidadeMedicaoDS_6_Dynamicfiltersenabled3 ,
                                             String AV51WWUnidadeMedicaoDS_7_Dynamicfiltersselector3 ,
                                             String AV52WWUnidadeMedicaoDS_8_Unidademedicao_nome3 ,
                                             String AV54WWUnidadeMedicaoDS_10_Tfunidademedicao_nome_sel ,
                                             String AV53WWUnidadeMedicaoDS_9_Tfunidademedicao_nome ,
                                             String AV56WWUnidadeMedicaoDS_12_Tfunidademedicao_sigla_sel ,
                                             String AV55WWUnidadeMedicaoDS_11_Tfunidademedicao_sigla ,
                                             short AV57WWUnidadeMedicaoDS_13_Tfunidademedicao_ativo_sel ,
                                             String A1198UnidadeMedicao_Nome ,
                                             String A1200UnidadeMedicao_Sigla ,
                                             bool A1199UnidadeMedicao_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [7] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [UnidadeMedicao_Nome], [UnidadeMedicao_Ativo], [UnidadeMedicao_Sigla], [UnidadeMedicao_Codigo] FROM [UnidadeMedicao] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV45WWUnidadeMedicaoDS_1_Dynamicfiltersselector1, "UNIDADEMEDICAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46WWUnidadeMedicaoDS_2_Unidademedicao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] like '%' + @lV46WWUnidadeMedicaoDS_2_Unidademedicao_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] like '%' + @lV46WWUnidadeMedicaoDS_2_Unidademedicao_nome1 + '%')";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( AV47WWUnidadeMedicaoDS_3_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV48WWUnidadeMedicaoDS_4_Dynamicfiltersselector2, "UNIDADEMEDICAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWUnidadeMedicaoDS_5_Unidademedicao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] like '%' + @lV49WWUnidadeMedicaoDS_5_Unidademedicao_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] like '%' + @lV49WWUnidadeMedicaoDS_5_Unidademedicao_nome2 + '%')";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV50WWUnidadeMedicaoDS_6_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV51WWUnidadeMedicaoDS_7_Dynamicfiltersselector3, "UNIDADEMEDICAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWUnidadeMedicaoDS_8_Unidademedicao_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] like '%' + @lV52WWUnidadeMedicaoDS_8_Unidademedicao_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] like '%' + @lV52WWUnidadeMedicaoDS_8_Unidademedicao_nome3 + '%')";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV54WWUnidadeMedicaoDS_10_Tfunidademedicao_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWUnidadeMedicaoDS_9_Tfunidademedicao_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] like @lV53WWUnidadeMedicaoDS_9_Tfunidademedicao_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] like @lV53WWUnidadeMedicaoDS_9_Tfunidademedicao_nome)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWUnidadeMedicaoDS_10_Tfunidademedicao_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] = @AV54WWUnidadeMedicaoDS_10_Tfunidademedicao_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] = @AV54WWUnidadeMedicaoDS_10_Tfunidademedicao_nome_sel)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV56WWUnidadeMedicaoDS_12_Tfunidademedicao_sigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWUnidadeMedicaoDS_11_Tfunidademedicao_sigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Sigla] like @lV55WWUnidadeMedicaoDS_11_Tfunidademedicao_sigla)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Sigla] like @lV55WWUnidadeMedicaoDS_11_Tfunidademedicao_sigla)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWUnidadeMedicaoDS_12_Tfunidademedicao_sigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Sigla] = @AV56WWUnidadeMedicaoDS_12_Tfunidademedicao_sigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Sigla] = @AV56WWUnidadeMedicaoDS_12_Tfunidademedicao_sigla_sel)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV57WWUnidadeMedicaoDS_13_Tfunidademedicao_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Ativo] = 1)";
            }
         }
         if ( AV57WWUnidadeMedicaoDS_13_Tfunidademedicao_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [UnidadeMedicao_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00Q33( IGxContext context ,
                                             String AV45WWUnidadeMedicaoDS_1_Dynamicfiltersselector1 ,
                                             String AV46WWUnidadeMedicaoDS_2_Unidademedicao_nome1 ,
                                             bool AV47WWUnidadeMedicaoDS_3_Dynamicfiltersenabled2 ,
                                             String AV48WWUnidadeMedicaoDS_4_Dynamicfiltersselector2 ,
                                             String AV49WWUnidadeMedicaoDS_5_Unidademedicao_nome2 ,
                                             bool AV50WWUnidadeMedicaoDS_6_Dynamicfiltersenabled3 ,
                                             String AV51WWUnidadeMedicaoDS_7_Dynamicfiltersselector3 ,
                                             String AV52WWUnidadeMedicaoDS_8_Unidademedicao_nome3 ,
                                             String AV54WWUnidadeMedicaoDS_10_Tfunidademedicao_nome_sel ,
                                             String AV53WWUnidadeMedicaoDS_9_Tfunidademedicao_nome ,
                                             String AV56WWUnidadeMedicaoDS_12_Tfunidademedicao_sigla_sel ,
                                             String AV55WWUnidadeMedicaoDS_11_Tfunidademedicao_sigla ,
                                             short AV57WWUnidadeMedicaoDS_13_Tfunidademedicao_ativo_sel ,
                                             String A1198UnidadeMedicao_Nome ,
                                             String A1200UnidadeMedicao_Sigla ,
                                             bool A1199UnidadeMedicao_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [7] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [UnidadeMedicao_Sigla], [UnidadeMedicao_Ativo], [UnidadeMedicao_Nome], [UnidadeMedicao_Codigo] FROM [UnidadeMedicao] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV45WWUnidadeMedicaoDS_1_Dynamicfiltersselector1, "UNIDADEMEDICAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46WWUnidadeMedicaoDS_2_Unidademedicao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] like '%' + @lV46WWUnidadeMedicaoDS_2_Unidademedicao_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] like '%' + @lV46WWUnidadeMedicaoDS_2_Unidademedicao_nome1 + '%')";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( AV47WWUnidadeMedicaoDS_3_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV48WWUnidadeMedicaoDS_4_Dynamicfiltersselector2, "UNIDADEMEDICAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWUnidadeMedicaoDS_5_Unidademedicao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] like '%' + @lV49WWUnidadeMedicaoDS_5_Unidademedicao_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] like '%' + @lV49WWUnidadeMedicaoDS_5_Unidademedicao_nome2 + '%')";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV50WWUnidadeMedicaoDS_6_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV51WWUnidadeMedicaoDS_7_Dynamicfiltersselector3, "UNIDADEMEDICAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWUnidadeMedicaoDS_8_Unidademedicao_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] like '%' + @lV52WWUnidadeMedicaoDS_8_Unidademedicao_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] like '%' + @lV52WWUnidadeMedicaoDS_8_Unidademedicao_nome3 + '%')";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV54WWUnidadeMedicaoDS_10_Tfunidademedicao_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWUnidadeMedicaoDS_9_Tfunidademedicao_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] like @lV53WWUnidadeMedicaoDS_9_Tfunidademedicao_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] like @lV53WWUnidadeMedicaoDS_9_Tfunidademedicao_nome)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWUnidadeMedicaoDS_10_Tfunidademedicao_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] = @AV54WWUnidadeMedicaoDS_10_Tfunidademedicao_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] = @AV54WWUnidadeMedicaoDS_10_Tfunidademedicao_nome_sel)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV56WWUnidadeMedicaoDS_12_Tfunidademedicao_sigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWUnidadeMedicaoDS_11_Tfunidademedicao_sigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Sigla] like @lV55WWUnidadeMedicaoDS_11_Tfunidademedicao_sigla)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Sigla] like @lV55WWUnidadeMedicaoDS_11_Tfunidademedicao_sigla)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWUnidadeMedicaoDS_12_Tfunidademedicao_sigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Sigla] = @AV56WWUnidadeMedicaoDS_12_Tfunidademedicao_sigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Sigla] = @AV56WWUnidadeMedicaoDS_12_Tfunidademedicao_sigla_sel)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV57WWUnidadeMedicaoDS_13_Tfunidademedicao_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Ativo] = 1)";
            }
         }
         if ( AV57WWUnidadeMedicaoDS_13_Tfunidademedicao_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [UnidadeMedicao_Sigla]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00Q32(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (short)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] );
               case 1 :
                     return conditional_P00Q33(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (short)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00Q32 ;
          prmP00Q32 = new Object[] {
          new Object[] {"@lV46WWUnidadeMedicaoDS_2_Unidademedicao_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV49WWUnidadeMedicaoDS_5_Unidademedicao_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV52WWUnidadeMedicaoDS_8_Unidademedicao_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53WWUnidadeMedicaoDS_9_Tfunidademedicao_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV54WWUnidadeMedicaoDS_10_Tfunidademedicao_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWUnidadeMedicaoDS_11_Tfunidademedicao_sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV56WWUnidadeMedicaoDS_12_Tfunidademedicao_sigla_sel",SqlDbType.Char,15,0}
          } ;
          Object[] prmP00Q33 ;
          prmP00Q33 = new Object[] {
          new Object[] {"@lV46WWUnidadeMedicaoDS_2_Unidademedicao_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV49WWUnidadeMedicaoDS_5_Unidademedicao_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV52WWUnidadeMedicaoDS_8_Unidademedicao_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53WWUnidadeMedicaoDS_9_Tfunidademedicao_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV54WWUnidadeMedicaoDS_10_Tfunidademedicao_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV55WWUnidadeMedicaoDS_11_Tfunidademedicao_sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV56WWUnidadeMedicaoDS_12_Tfunidademedicao_sigla_sel",SqlDbType.Char,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00Q32", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00Q32,100,0,true,false )
             ,new CursorDef("P00Q33", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00Q33,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwunidademedicaofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwunidademedicaofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwunidademedicaofilterdata") )
          {
             return  ;
          }
          getwwunidademedicaofilterdata worker = new getwwunidademedicaofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
