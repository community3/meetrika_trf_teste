/*
               File: type_SdtPDFTools
        Description: PDFTools
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:57.27
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtPDFTools : GxUserType, IGxExternalObject
   {
      public SdtPDFTools( )
      {
         initialize();
      }

      public SdtPDFTools( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String concatfiles( IGxCollection gxTp_files ,
                                 String gxTp_targetPath )
      {
         String returnconcatfiles ;
         if ( PDFTools_externalReference == null )
         {
            PDFTools_externalReference = new PDFTools_iTextSharpLib.iTextSharpUtil();
         }
         returnconcatfiles = "";
         System.Collections.Generic.List< System.String> externalParm0 ;
         externalParm0 = (System.Collections.Generic.List< System.String>)CollectionUtils.ConvertToExternal( typeof(System.Collections.Generic.List< System.String>), gxTp_files.ExternalInstance);
         returnconcatfiles = (String)(PDFTools_externalReference.ConcatFiles(externalParm0, gxTp_targetPath));
         return returnconcatfiles ;
      }

      public String addsignature( String gxTp_PathSource ,
                                  String gxTp_PathTarget ,
                                  String gxTp_CertPath ,
                                  String gxTp_CertPass ,
                                  bool gxTp_Visible )
      {
         String returnaddsignature ;
         if ( PDFTools_externalReference == null )
         {
            PDFTools_externalReference = new PDFTools_iTextSharpLib.iTextSharpUtil();
         }
         returnaddsignature = "";
         returnaddsignature = (String)(PDFTools_externalReference.AddSignature(gxTp_PathSource, gxTp_PathTarget, gxTp_CertPath, gxTp_CertPass, gxTp_Visible));
         return returnaddsignature ;
      }

      public String addsignature( String gxTp_PathSource ,
                                  String gxTp_PathTarget ,
                                  String gxTp_CertPath ,
                                  String gxTp_CertPass ,
                                  int gxTp_x ,
                                  int gxTp_y ,
                                  int gxTp_LengthX ,
                                  int gxTp_LengthY ,
                                  int gxTp_Page ,
                                  bool gxTp_Visible )
      {
         String returnaddsignature ;
         if ( PDFTools_externalReference == null )
         {
            PDFTools_externalReference = new PDFTools_iTextSharpLib.iTextSharpUtil();
         }
         returnaddsignature = "";
         returnaddsignature = (String)(PDFTools_externalReference.AddSignature(gxTp_PathSource, gxTp_PathTarget, gxTp_CertPath, gxTp_CertPass, gxTp_x, gxTp_y, gxTp_LengthX, gxTp_LengthY, gxTp_Page, gxTp_Visible));
         return returnaddsignature ;
      }

      public String modifypermissions( String gxTp_PathSource ,
                                       String gxTp_PathTarget ,
                                       String gxTp_UserPassword ,
                                       IGxCollection gxTp_Permissons )
      {
         String returnmodifypermissions ;
         if ( PDFTools_externalReference == null )
         {
            PDFTools_externalReference = new PDFTools_iTextSharpLib.iTextSharpUtil();
         }
         returnmodifypermissions = "";
         System.Collections.Generic.List< System.Int32> externalParm0 ;
         externalParm0 = (System.Collections.Generic.List< System.Int32>)CollectionUtils.ConvertToExternal( typeof(System.Collections.Generic.List< System.Int32>), gxTp_Permissons.ExternalInstance);
         returnmodifypermissions = (String)(PDFTools_externalReference.ModifyPermissions(gxTp_PathSource, gxTp_PathTarget, gxTp_UserPassword, externalParm0));
         return returnmodifypermissions ;
      }

      public int pagecount( String gxTp_PathSource )
      {
         int returnpagecount ;
         if ( PDFTools_externalReference == null )
         {
            PDFTools_externalReference = new PDFTools_iTextSharpLib.iTextSharpUtil();
         }
         returnpagecount = 0;
         returnpagecount = (int)(PDFTools_externalReference.NumberOfPages(gxTp_PathSource));
         return returnpagecount ;
      }

      public String tifftopdf( IGxCollection gxTp_files ,
                               String gxTp_targetPath )
      {
         String returntifftopdf ;
         if ( PDFTools_externalReference == null )
         {
            PDFTools_externalReference = new PDFTools_iTextSharpLib.iTextSharpUtil();
         }
         returntifftopdf = "";
         System.Collections.Generic.List< System.String> externalParm0 ;
         externalParm0 = (System.Collections.Generic.List< System.String>)CollectionUtils.ConvertToExternal( typeof(System.Collections.Generic.List< System.String>), gxTp_files.ExternalInstance);
         returntifftopdf = (String)(PDFTools_externalReference.TiffAsPDF(externalParm0, gxTp_targetPath));
         return returntifftopdf ;
      }

      public String setfields( String gxTp_PathSource ,
                               String gxTp_PathTarget ,
                               object gxTp_Fields )
      {
         String returnsetfields ;
         if ( PDFTools_externalReference == null )
         {
            PDFTools_externalReference = new PDFTools_iTextSharpLib.iTextSharpUtil();
         }
         returnsetfields = "";
         returnsetfields = (String)(PDFTools_externalReference.SetFields(gxTp_PathSource, gxTp_PathTarget, (System.Object)(gxTp_Fields)));
         return returnsetfields ;
      }

      public Object ExternalInstance
      {
         get {
            if ( PDFTools_externalReference == null )
            {
               PDFTools_externalReference = new PDFTools_iTextSharpLib.iTextSharpUtil();
            }
            return PDFTools_externalReference ;
         }

         set {
            PDFTools_externalReference = (PDFTools_iTextSharpLib.iTextSharpUtil)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected PDFTools_iTextSharpLib.iTextSharpUtil PDFTools_externalReference=null ;
   }

}
