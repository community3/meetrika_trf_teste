/*
               File: GAMErrorMessages
        Description: GAMErrorMessages
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 21:56:34.88
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gxdomaingamerrormessages
   {
      private static Hashtable domain = new Hashtable();
      static gxdomaingamerrormessages ()
      {
         domain[(long)1] = "A conex�o ao GAM n�o foi especificada, favor contate o administrador do GAM.";
         domain[(long)2] = "Reposit�rio n�o encontrado, favor contate o administrador.";
         domain[(long)3] = "O tipo de autentica��o n�o foi encontrado, favor contate o administrador.";
         domain[(long)4] = "Fun��o de autentica��o n�o encontrada.";
         domain[(long)5] = "Identificador do usu�rio inv�lido.";
         domain[(long)6] = "O Login do usu�rio n�o pode ser nulo.";
         domain[(long)7] = "Usu�rio n�o encontrado.";
         domain[(long)8] = "Usu�rio desconhecido.";
         domain[(long)9] = "O usu�rio est� inativo, contate o administrador.";
         domain[(long)10] = "O usu�rio est� bloqueado, contate o administrador.";
         domain[(long)11] = "Usu�rio ou senha errados.";
         domain[(long)12] = "Erro ao executar o servi�o de autentica��o, contate o administrador.";
         domain[(long)13] = "A sess�o do usu�rio expirou, fa�a login novamente.";
         domain[(long)14] = "A sess�o do usu�rio n�o existe, fa�a login novamente.";
         domain[(long)15] = "Tipo de sess�o de usu�rio inv�lido, fa�a login novamente.";
         domain[(long)16] = "Erro na sess�o do usu�rio, favor contate o administrador.";
         domain[(long)18] = "Usu�rio ou senha errados.";
         domain[(long)17] = "Usu�rio n�o autorizado.";
         domain[(long)19] = "Desafio do consumidor n�o � v�lido, favor contate o administrador do GAM.";
         domain[(long)20] = "Modo de conex�o ao GAM inv�lido, favor contate o administrador.";
         domain[(long)21] = "A sess�o do usu�rio foi finalizada, fa�a login novamente.";
         domain[(long)22] = "O Reposit�rio n�o tem configurado o usu�rio an�nimo, favor contate o administrador.";
         domain[(long)23] = "A senha deve ser modificada.";
         domain[(long)24] = "Senha expirou, favor criar uma nova.";
         domain[(long)25] = "A sess�o do usu�rio foi bloqueada, feche o navegador e fa�a login novamente.";
         domain[(long)26] = "Altera��o da senha n�o � permitida.";
         domain[(long)27] = "O nome do usu�rio � muito curto, deve ter no m�nimo %1 caracteres.";
         domain[(long)28] = "O nome do usu�rio deve conter caracteres especiais.";
         domain[(long)30] = "A conex�o ao GAM n�o foi encontrada, favor contate o administrador do GAM.";
         domain[(long)31] = "Tipo de conex�o ao GAM n�o encontrado, favor contate o administrador do GAM.";
         domain[(long)32] = "Usu�rio da conex�o do reposit�rio inv�lido (usu�rio:%1), favor contate o administrador.";
         domain[(long)33] = "Senha de conex�o do usu�rio %1 inv�lida, favor contate o administrador do GAM.";
         domain[(long)34] = "Usu�rio da conex�o remota inv�lido (usu�rio: %1).";
         domain[(long)35] = "Tipo de autentica��o inv�lido, favor contate o administrador.";
         domain[(long)36] = "Tipo de 'lembrar usu�rio' inv�lido, favor contate o administrador.";
         domain[(long)37] = "Vers�o do servi�o de autentica��o n�o encontrada.";
         domain[(long)38] = "Vers�o do servi�o de autentica��o n�o suportada, contate o administrador.";
         domain[(long)39] = "Aplica��o n�o encontrada, favor contate o administrador.";
         domain[(long)40] = "Erro na resposta do servi�o de autentica��o, favor contate o administrador da aplica��o.";
         domain[(long)41] = "Erro no usu�rio an�nimo, favor contate seu administrador.";
         domain[(long)42] = "Error:";
         domain[(long)43] = "A chave para mudar a senha do usu�rio est� errada, obtenha outra.";
         domain[(long)44] = "A chave para mudar a senha do usu�rio expirou, obtenha outra.";
         domain[(long)45] = "A resposta de recupera��o da senha do usu�rio est� errada.";
         domain[(long)46] = "O namespace n�o pode ser nulo.";
         domain[(long)47] = "A descri��o n�o pode ser nula";
         domain[(long)48] = "Modo de API n�o reconhecido, contate o administrador.";
         domain[(long)49] = "O nome do usu�rio j� existe, favor selecionar outro.";
         domain[(long)50] = "A conta do usu�rio j� se encontra ativada.";
         domain[(long)51] = "A chave de ativa��o do usu�rio est� incorreta, fa�a login novamente ou contate o administrador.";
         domain[(long)52] = "A chave de ativa��o do usu�rio expirou, fa�a login novamente ou contate o administrador.";
         domain[(long)53] = "Nome do namespace inv�lido, favor contate o administrador.";
         domain[(long)54] = "Error OpenId.";
         domain[(long)55] = "Identidade OpenId n�o � igual, tente novamente.";
         domain[(long)56] = "Erro no modo OpenId.";
         domain[(long)57] = "OpenId retornou um erro: %1.";
         domain[(long)59] = "Erro na assinatura do OpenId.";
         domain[(long)60] = "Falta um dado obrigat�rio (%1).";
         domain[(long)62] = "Erro na sintaxe do email, verifique novamente.";
         domain[(long)63] = "Email � obrigat�rio.";
         domain[(long)64] = "Senha do usu�rio obrigat�ria.";
         domain[(long)65] = "O primeiro nome do usu�rio � obrigat�rio.";
         domain[(long)66] = "Sobrenome do usu�rio � obrigat�rio.";
         domain[(long)67] = "Data de nascimento obrigat�ria.";
         domain[(long)68] = "G�nero do Usu�rio � obrigat�rio.";
         domain[(long)69] = "Telefone do usu�rio obrigat�rio.";
         domain[(long)70] = "Endere�o � obrigat�rio.";
         domain[(long)71] = "Cidade � obrigat�ria.";
         domain[(long)72] = "Estado � obrigat�rio, selecione um.";
         domain[(long)73] = "Pa�s � obrigat�rio.";
         domain[(long)74] = "O CEP do usu�rio � obrigat�rio.";
         domain[(long)75] = "Idioma do usu�rio � obrigat�rio.";
         domain[(long)76] = "O Fuso hor�rio � obrigat�rio.";
         domain[(long)77] = "Foto do usu�rio � obrigat�ria.";
         domain[(long)78] = "J� existe um usu�rio registrado com este email, selecione outro.";
         domain[(long)79] = "Nome do usu�rio deve ser digitado.";
         domain[(long)80] = "A aplica��o Pai n�o pode ser uma aplica��o base.";
         domain[(long)81] = "A aplica��o pai n�o existe.";
         domain[(long)82] = "A Aplica��o base n�o � v�lida.";
         domain[(long)83] = "A aplica��o base n�o existe.";
         domain[(long)84] = "O tipo de ativa��o do usu�rios � incorreto, fa�a login novamente ou contate o administrador.";
         domain[(long)85] = "O usu�rio j� possui uma sess�o ativa, favor contate o administrador.";
         domain[(long)86] = "Erro ao tentar gravar um cookie '%1', verifique a configura��o do seu navegador.";
         domain[(long)87] = "O reposit�rio precisa ter habilitado o tipo de autentica��o 'local', favor contate o administrador.";
         domain[(long)88] = "Servidor de autentica��o externa inv�lido, favor contate o administrador da aplica��o.";
         domain[(long)89] = "Token do servi�o de autentica��o externa inv�lido, favor contate o administrador da aplica��o.";
         domain[(long)90] = "Erro no tipo de autentica��o externa, favor contate o administrador da aplica��o.";
         domain[(long)91] = "Erro de acesso ao Facebook (%1), favor contate o administrador da aplica��o.";
         domain[(long)92] = "Erro na resposta dada pelo Facebook, tente novamente.";
         domain[(long)93] = "Erro ao voltar da autentica��o externa (%1).";
         domain[(long)94] = "Erro na resposta enviada pelo Twitter, fa�a login novamente.";
         domain[(long)95] = "Erro no token enviado pelo Twitter, fa�a login novamente.";
         domain[(long)96] = "Erro na verifica��o do Twitter, fa�a login novamente.";
         domain[(long)97] = "Erro de acesso ao Twitter (%1).";
         domain[(long)98] = "GUID do usu�rio n�o obtida, favor contate o administrador.";
         domain[(long)99] = "Tipo de aplica��o j� existe.";
         domain[(long)100] = "O protocolo Oauth n�o est� habilitado, favor contate o administrador.";
         domain[(long)101] = "Erro no refresh do token, fa�a login novamente.";
         domain[(long)102] = "O token foi revogado, fa�a login novamente.";
         domain[(long)103] = "O token expirou, fa�a login novamente.";
         domain[(long)104] = "O usu�rio deve estar autenticado.";
         domain[(long)106] = "Permiss�o de acesso n�o encontrada, fa�a login novamente.";
         domain[(long)107] = "Permiss�o de acesso revogada, fa�a login novamente.";
         domain[(long)108] = "Solicita��o de autoriza��o n�o encontrada, favor contate o administrador.";
         domain[(long)109] = "Solicita��o de autoriza��o revogada, favor contate o administrador.";
         domain[(long)110] = "Solicita��o de autoriza��o negada, favor contate o administrador.";
         domain[(long)111] = "Aplica��o revogada, por favor contate o administrador.";
         domain[(long)112] = "Token n�o encontrado, fa�a login novamente.";
         domain[(long)113] = "Token revogado, fa�a login novamente.";
         domain[(long)114] = "Token n�o � v�lido, fa�a login novamente.";
         domain[(long)115] = "Reposit�rio j� existe, favor contate o administrador do GAM.";
         domain[(long)116] = "C�digo secreto da aplica��o inv�lido, favor contate o administrador";
         domain[(long)117] = "Permiss�o n�o encontrada, contate o administrador.";
         domain[(long)118] = "Nome do usu�rio � obrigat�rio.";
         domain[(long)119] = "IP da sess�o original foi modificado, fa�a login novamente.";
         domain[(long)120] = "Configura��o n�o permitida, porque j� existem emails duplicados.";
         domain[(long)121] = "Erro na sess�o do usu�rio, fa�a login novamente.";
         domain[(long)122] = "Erro no token, fa�a login novamente.";
         domain[(long)123] = "Token da aplica��o n�o encontrado.";
         domain[(long)124] = "O token da aplica��o j� existe.";
         domain[(long)125] = "O identificador externo do usu�rio j� existe, favor ingresse outro.";
         domain[(long)126] = "O c�digo externo do Papel j� existe, favor selecione outro.";
         domain[(long)127] = "N�o � poss�vel mudar a conex�o atual.";
         domain[(long)128] = "N�o � poss�vel apagar a conex�o atual.";
         domain[(long)129] = "Erro do token de acesso ao Facebook (%1), favor contate o administrador da aplica��o.";
         domain[(long)130] = "A conta Facebook n�o esta verificada, favor verifique sua conta em www.facebook.com.";
         domain[(long)131] = "Erro na resposta do Facebook: %1";
         domain[(long)132] = "O usu�rio n�o existe no reposit�rio, favor contate o administrador.";
         domain[(long)133] = "Ambiente da aplica��o n�o encontrado, favor contate o administrador.";
         domain[(long)134] = "Permiss�o de acesso expirou, fa�a login novamente.";
         domain[(long)135] = "A permiss�o do token expirou, fa�a login novamente.";
         domain[(long)136] = "Permiss�o do token n�o encontrada, fa�a login novamente.";
         domain[(long)137] = "O atributo do usu�rio n�o existe.";
         domain[(long)138] = "Nome da aplica��o j� existente, favor selecionar outro.";
         domain[(long)139] = "N�o autorizado: acesso negado.";
         domain[(long)140] = "O nome da aplica��o n�o pode ser nulo.";
         domain[(long)141] = "Senha muito curta, digite outra.";
         domain[(long)142] = "A senha deve conter n�meros (m�nimo %1).";
         domain[(long)143] = "A senha deve conter mai�sculas (m�nimo 1%).";
         domain[(long)144] = "A senha deve conter caracteres especiais(m�nimo %1).";
         domain[(long)145] = "A senha atual e a nova deve ser diferentes, utilize outra.";
         domain[(long)146] = "Senha usada recentemente, crie uma nova.";
         domain[(long)147] = "O email � necess�rio para que o usu�rio possa autenticar-se com ele.";
         domain[(long)148] = "Email obrigat�rio, pois est� configurado para que seja �nico.";
         domain[(long)149] = "O email deve ser �nico, para que o usu�rio possa autenticar-se com ele.";
         domain[(long)150] = "O diret�rio %1 n�o existe, favor contate o administrador.";
         domain[(long)151] = "O token n�o est� relacionado a esta aplica��o, fa�a login novamente.";
         domain[(long)152] = "Nome da permiss�o inv�lido.";
         domain[(long)153] = "Permiss�o n�o encontrada.";
         domain[(long)154] = "O n�mero da permiss�o j� existe, favor eleja um nome diferente.";
         domain[(long)160] = "A senha foi modificada recentemente, dever� esperar %1 minutos para tentar de novo.";
         domain[(long)161] = "H� dados incompletos do usu�rio. Voc� deve complet�-los para continuar.";
         domain[(long)162] = "A sess�o do usu�rio expirou, fa�a login novamente.";
         domain[(long)163] = "A chave para completar os dados do usu�rio n�o existe, fa�a login novamente e se o problema persistir, contate o administrador.";
         domain[(long)164] = "A expira��o da chave para recuperar a senha n�o pode ser nula.";
         domain[(long)166] = "A conta do Twitter n�o est� confirmada, acesse sua conta em www.twitter.com e confirme-a.";
         domain[(long)167] = "N�o foi poss�vel obter o ID do dispositivo, feche a aplica��o e tente novamente.";
         domain[(long)168] = "O token n�o pode ser renovado, fa�a login novamente.";
         domain[(long)169] = "Erro na configura��o da aplica��o, verifique a propriedade Auto-registro do usu�rio an�nimo.";
         domain[(long)170] = "O namespace j� existe e seu uso � restrito.";
         domain[(long)171] = "O arquivo de conex�es ao GAM n�o admite nomes de conex�es repetidas (%1).";
         domain[(long)172] = "N�o � poss�vel apagar o reposit�rio administrador do GAM.";
         domain[(long)173] = "O token n�o � v�lido para finaliza��o (n�o existe ou n�o � um token v�lido).";
         domain[(long)174] = "Aplica��o GUID %1 n�o definida, favor contate o administrador.";
         domain[(long)175] = "O nome da Pol�tica de Seguran�a n�o pode ser nulo.";
         domain[(long)176] = "O nome do Papel n�o pode ser nulo.";
         domain[(long)177] = "O nome do papel j� existe, por favor escolha um nome diferente.";
         domain[(long)178] = "O nome do reposit�rio n�o pode ser nulo.";
         domain[(long)179] = "O nome da conex�o n�o pode ser nulo.";
         domain[(long)180] = "O nome do usu�rio da conex�o n�o pode ser nulo.";
         domain[(long)182] = "A senha do usu�rio da conex�o � necess�ria.";
         domain[(long)183] = "N�o pode deletar o Papel de Reposit�rio padr�o.";
         domain[(long)184] = "N�o pode deletar o Papel an�nimo do Smart Device.";
         domain[(long)185] = "Opera��o inv�lida sobre um Usu�rio de tipo Dispositivo.";
         domain[(long)200] = "N�o encontrado.";
         domain[(long)201] = "Tipo de acesso da permiss�o inv�lido.";
         domain[(long)202] = "Erro de acesso ao Google (%1), favor contate o administrador da aplica��o.";
         domain[(long)203] = "Erro na resposta dada pelo Google, tente novamente.";
         domain[(long)204] = "Erro do token de acesso ao Google (%1), favor contate o administrador da aplica��o.";
         domain[(long)205] = "Erro na resposta do Google: %1";
         domain[(long)206] = "A conta Google n�o esta verificada, favor verifique sua conta.";
         domain[(long)207] = "Diret�rio {0} inv�lido. Est�o faltando arquivos";
         domain[(long)208] = "Faltando Pol�tica de Seguran�a de Reposit�rio padr�o.";
         domain[(long)209] = "Erro no Token de acesso ao Twitter (%1). Por favor, entre em contato com o administrador da aplica��o.";
         domain[(long)211] = "Vers�o do servi�o de autoriza��o n�o suportada. Por favor, entre em contato com o administrador da aplica��o.";
         domain[(long)212] = "Erro ao executar o servi�o de autoriza��o (%1).";
         domain[(long)213] = "GAM_ExternalTokenInvalid";
         domain[(long)220] = "A URL de login local da aplica��o deve ser digitada.";
         domain[(long)221] = "A URL de retorno de chamada da aplica��o eve ser digitada.";
         domain[(long)222] = "URL de retorno da chamada n�o coincidente com a configurada na aplica��o (%1).";
         domain[(long)230] = "A aplica��o n�o permite autentica��o remota, consulte o administrador.";
         domain[(long)231] = "A chave de encripta��o privada da aplica��o est� errada, consulte o administrador.";
         domain[(long)232] = "Falta a solicita��o do scope para os dados do usu�rio, consulte o administrador.";
         domain[(long)233] = "A aplica��o cliente n�o tem permiss�o para obter os dados adicionais do usu�rio, consulte o administrador.";
         domain[(long)234] = "A aplica��o cliente n�o tem permiss�o para obter os pap�is do usu�rio, consulte o administrador.";
         domain[(long)235] = "A aplica��o recebeu um scope inv�lido, consulte o administrador.";
         domain[(long)236] = "A aplica��o cliente n�o foi encontrada (%1), consulte o administrador.";
         domain[(long)237] = "Menu n�o encontrado. Por favor contate o administrador da aplica��o.";
         domain[(long)240] = "Tipo de autentica��o n�o habilitada ou n�o existe (%1).";
         domain[(long)241] = "O nome do tipo de autentica��o n�o pode ser nulo.";
         domain[(long)242] = "N�o existe este tipo de autentica��o definido no reposit�rio.";
         domain[(long)243] = "O identificador do cliente do tipo de autentica��o n�o pode ser nulo.";
         domain[(long)244] = "O c�digo secreto do cliente do tipo de autentica��o n�o pode ser nulo.";
         domain[(long)245] = "A URL local do site n�o pode ser nula.";
         domain[(long)246] = "A URL do servidor remoto n�o pode ser nula.";
         domain[(long)247] = "O nome do servi�o web n�o pode ser nulo.";
         domain[(long)248] = "O nome do servidor n�o pode ser nulo.";
         domain[(long)249] = "GAM_AuthenticationTypeConsumerKeyCannotBeNull";
         domain[(long)250] = "GAM_AuthenticationTypeConsumerSecretCannotBeNull";
         domain[(long)251] = "Digite a URL de retorno da chamada do provedor do servi�o de autentica��o.";
         domain[(long)252] = "O nome do arquivo do servi�o de autentica��o n�o pode ser nulo.";
         domain[(long)253] = "O nome da classe do servi�o de autentica��o n�o pode ser nulo.";
         domain[(long)260] = "O tipo de autentica��o tem usu�rios definidos. N�o pode ser eliminado.";
         domain[(long)261] = "Este tipo de autentica��o � o predeterminado do reposit�rio. N�o pode ser eliminado.";
         domain[(long)270] = "Erro de acesso ao tipo de autentica��o GAMRemote, consulte o administrador.";
         domain[(long)271] = "Erro na resposta dada pelo servi�o de autentica��o remota, consulte o administrador.";
         domain[(long)272] = "Erro ao tentar obter o token do servidor de autentica��o remota, consulte o administrador.";
         domain[(long)273] = "Erro na resposta de %1: %2";
         domain[(long)274] = "A conta %1 n�o foi verificada. Por favor, confirme sua conta em %2 ou consulte o administrador.";
         domain[(long)290] = "Scope (%1) n�o encontrado nas permiss�es da aplica��o. Consulte o administrador.";
         domain[(long)291] = "C�digo de acesso n�o encontrado. Tente autenticar-se novamente ou consulte o administrador.";
         domain[(long)292] = "GAMGrantTypeNotFound";
         domain[(long)300] = "Namespace do usu�rio n�o � v�lido. Por favor, entre em contato com o administrador da aplica��o.";
         domain[(long)301] = "O usu�rio autenticado (GUID) existe com outro namespace, consulte o administrador.";
         domain[(long)302] = "GAM_UserAlreadyExistWithDifferentAuthenticationType";
         domain[(long)303] = "N�o foi encontrado o valor no atributo de 'multi-valores' do usu�rio.";
         domain[(long)304] = "O atributo do usu�rio deve ser 'multi-valores'.";
         domain[(long)305] = "Um ID � obrigat�rio para o atributo de usu�rio ingressado.";
      }

      public static string getDescription( IGxContext context ,
                                           long key )
      {
         return (string)domain[key] ;
      }

      public static GxSimpleCollection getValues( )
      {
         GxSimpleCollection value = new GxSimpleCollection();
         ArrayList aKeys = new ArrayList(domain.Keys);
         aKeys.Sort();
         foreach (long key in aKeys)
         {
            value.Add(key);
         }
         return value;
      }

   }

}
