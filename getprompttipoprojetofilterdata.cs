/*
               File: GetPromptTipoProjetoFilterData
        Description: Get Prompt Tipo Projeto Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:54:19.82
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getprompttipoprojetofilterdata : GXProcedure
   {
      public getprompttipoprojetofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getprompttipoprojetofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getprompttipoprojetofilterdata objgetprompttipoprojetofilterdata;
         objgetprompttipoprojetofilterdata = new getprompttipoprojetofilterdata();
         objgetprompttipoprojetofilterdata.AV18DDOName = aP0_DDOName;
         objgetprompttipoprojetofilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetprompttipoprojetofilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetprompttipoprojetofilterdata.AV22OptionsJson = "" ;
         objgetprompttipoprojetofilterdata.AV25OptionsDescJson = "" ;
         objgetprompttipoprojetofilterdata.AV27OptionIndexesJson = "" ;
         objgetprompttipoprojetofilterdata.context.SetSubmitInitialConfig(context);
         objgetprompttipoprojetofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetprompttipoprojetofilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getprompttipoprojetofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_TIPOPROJETO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADTIPOPROJETO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("PromptTipoProjetoGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptTipoProjetoGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("PromptTipoProjetoGridState"), "");
         }
         AV47GXV1 = 1;
         while ( AV47GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV47GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFTIPOPROJETO_NOME") == 0 )
            {
               AV10TFTipoProjeto_Nome = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFTIPOPROJETO_NOME_SEL") == 0 )
            {
               AV11TFTipoProjeto_Nome_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFTIPOPROJETO_PRAZO") == 0 )
            {
               AV12TFTipoProjeto_Prazo = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, ".");
               AV13TFTipoProjeto_Prazo_To = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFTIPOPROJETO_CNSTEXP") == 0 )
            {
               AV14TFTipoProjeto_CnstExp = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, ".");
               AV15TFTipoProjeto_CnstExp_To = NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Valueto, ".");
            }
            AV47GXV1 = (int)(AV47GXV1+1);
         }
         if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(1));
            AV34DynamicFiltersSelector1 = AV33GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "TIPOPROJETO_NOME") == 0 )
            {
               AV35DynamicFiltersOperator1 = AV33GridStateDynamicFilter.gxTpr_Operator;
               AV36TipoProjeto_Nome1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV37DynamicFiltersEnabled2 = true;
               AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(2));
               AV38DynamicFiltersSelector2 = AV33GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "TIPOPROJETO_NOME") == 0 )
               {
                  AV39DynamicFiltersOperator2 = AV33GridStateDynamicFilter.gxTpr_Operator;
                  AV40TipoProjeto_Nome2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV41DynamicFiltersEnabled3 = true;
                  AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(3));
                  AV42DynamicFiltersSelector3 = AV33GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "TIPOPROJETO_NOME") == 0 )
                  {
                     AV43DynamicFiltersOperator3 = AV33GridStateDynamicFilter.gxTpr_Operator;
                     AV44TipoProjeto_Nome3 = AV33GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADTIPOPROJETO_NOMEOPTIONS' Routine */
         AV10TFTipoProjeto_Nome = AV16SearchTxt;
         AV11TFTipoProjeto_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV34DynamicFiltersSelector1 ,
                                              AV35DynamicFiltersOperator1 ,
                                              AV36TipoProjeto_Nome1 ,
                                              AV37DynamicFiltersEnabled2 ,
                                              AV38DynamicFiltersSelector2 ,
                                              AV39DynamicFiltersOperator2 ,
                                              AV40TipoProjeto_Nome2 ,
                                              AV41DynamicFiltersEnabled3 ,
                                              AV42DynamicFiltersSelector3 ,
                                              AV43DynamicFiltersOperator3 ,
                                              AV44TipoProjeto_Nome3 ,
                                              AV11TFTipoProjeto_Nome_Sel ,
                                              AV10TFTipoProjeto_Nome ,
                                              AV12TFTipoProjeto_Prazo ,
                                              AV13TFTipoProjeto_Prazo_To ,
                                              AV14TFTipoProjeto_CnstExp ,
                                              AV15TFTipoProjeto_CnstExp_To ,
                                              A980TipoProjeto_Nome ,
                                              A981TipoProjeto_Prazo ,
                                              A982TipoProjeto_CnstExp },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL
                                              }
         });
         lV36TipoProjeto_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV36TipoProjeto_Nome1), 50, "%");
         lV36TipoProjeto_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV36TipoProjeto_Nome1), 50, "%");
         lV40TipoProjeto_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV40TipoProjeto_Nome2), 50, "%");
         lV40TipoProjeto_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV40TipoProjeto_Nome2), 50, "%");
         lV44TipoProjeto_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV44TipoProjeto_Nome3), 50, "%");
         lV44TipoProjeto_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV44TipoProjeto_Nome3), 50, "%");
         lV10TFTipoProjeto_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFTipoProjeto_Nome), 50, "%");
         /* Using cursor P00PM2 */
         pr_default.execute(0, new Object[] {lV36TipoProjeto_Nome1, lV36TipoProjeto_Nome1, lV40TipoProjeto_Nome2, lV40TipoProjeto_Nome2, lV44TipoProjeto_Nome3, lV44TipoProjeto_Nome3, lV10TFTipoProjeto_Nome, AV11TFTipoProjeto_Nome_Sel, AV12TFTipoProjeto_Prazo, AV13TFTipoProjeto_Prazo_To, AV14TFTipoProjeto_CnstExp, AV15TFTipoProjeto_CnstExp_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKPM2 = false;
            A980TipoProjeto_Nome = P00PM2_A980TipoProjeto_Nome[0];
            A982TipoProjeto_CnstExp = P00PM2_A982TipoProjeto_CnstExp[0];
            A981TipoProjeto_Prazo = P00PM2_A981TipoProjeto_Prazo[0];
            A979TipoProjeto_Codigo = P00PM2_A979TipoProjeto_Codigo[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00PM2_A980TipoProjeto_Nome[0], A980TipoProjeto_Nome) == 0 ) )
            {
               BRKPM2 = false;
               A979TipoProjeto_Codigo = P00PM2_A979TipoProjeto_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKPM2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A980TipoProjeto_Nome)) )
            {
               AV20Option = A980TipoProjeto_Nome;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKPM2 )
            {
               BRKPM2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFTipoProjeto_Nome = "";
         AV11TFTipoProjeto_Nome_Sel = "";
         AV33GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV34DynamicFiltersSelector1 = "";
         AV36TipoProjeto_Nome1 = "";
         AV38DynamicFiltersSelector2 = "";
         AV40TipoProjeto_Nome2 = "";
         AV42DynamicFiltersSelector3 = "";
         AV44TipoProjeto_Nome3 = "";
         scmdbuf = "";
         lV36TipoProjeto_Nome1 = "";
         lV40TipoProjeto_Nome2 = "";
         lV44TipoProjeto_Nome3 = "";
         lV10TFTipoProjeto_Nome = "";
         A980TipoProjeto_Nome = "";
         P00PM2_A980TipoProjeto_Nome = new String[] {""} ;
         P00PM2_A982TipoProjeto_CnstExp = new decimal[1] ;
         P00PM2_A981TipoProjeto_Prazo = new decimal[1] ;
         P00PM2_A979TipoProjeto_Codigo = new int[1] ;
         AV20Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getprompttipoprojetofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00PM2_A980TipoProjeto_Nome, P00PM2_A982TipoProjeto_CnstExp, P00PM2_A981TipoProjeto_Prazo, P00PM2_A979TipoProjeto_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV35DynamicFiltersOperator1 ;
      private short AV39DynamicFiltersOperator2 ;
      private short AV43DynamicFiltersOperator3 ;
      private int AV47GXV1 ;
      private int A979TipoProjeto_Codigo ;
      private long AV28count ;
      private decimal AV12TFTipoProjeto_Prazo ;
      private decimal AV13TFTipoProjeto_Prazo_To ;
      private decimal AV14TFTipoProjeto_CnstExp ;
      private decimal AV15TFTipoProjeto_CnstExp_To ;
      private decimal A981TipoProjeto_Prazo ;
      private decimal A982TipoProjeto_CnstExp ;
      private String AV10TFTipoProjeto_Nome ;
      private String AV11TFTipoProjeto_Nome_Sel ;
      private String AV36TipoProjeto_Nome1 ;
      private String AV40TipoProjeto_Nome2 ;
      private String AV44TipoProjeto_Nome3 ;
      private String scmdbuf ;
      private String lV36TipoProjeto_Nome1 ;
      private String lV40TipoProjeto_Nome2 ;
      private String lV44TipoProjeto_Nome3 ;
      private String lV10TFTipoProjeto_Nome ;
      private String A980TipoProjeto_Nome ;
      private bool returnInSub ;
      private bool AV37DynamicFiltersEnabled2 ;
      private bool AV41DynamicFiltersEnabled3 ;
      private bool BRKPM2 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV34DynamicFiltersSelector1 ;
      private String AV38DynamicFiltersSelector2 ;
      private String AV42DynamicFiltersSelector3 ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00PM2_A980TipoProjeto_Nome ;
      private decimal[] P00PM2_A982TipoProjeto_CnstExp ;
      private decimal[] P00PM2_A981TipoProjeto_Prazo ;
      private int[] P00PM2_A979TipoProjeto_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV33GridStateDynamicFilter ;
   }

   public class getprompttipoprojetofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00PM2( IGxContext context ,
                                             String AV34DynamicFiltersSelector1 ,
                                             short AV35DynamicFiltersOperator1 ,
                                             String AV36TipoProjeto_Nome1 ,
                                             bool AV37DynamicFiltersEnabled2 ,
                                             String AV38DynamicFiltersSelector2 ,
                                             short AV39DynamicFiltersOperator2 ,
                                             String AV40TipoProjeto_Nome2 ,
                                             bool AV41DynamicFiltersEnabled3 ,
                                             String AV42DynamicFiltersSelector3 ,
                                             short AV43DynamicFiltersOperator3 ,
                                             String AV44TipoProjeto_Nome3 ,
                                             String AV11TFTipoProjeto_Nome_Sel ,
                                             String AV10TFTipoProjeto_Nome ,
                                             decimal AV12TFTipoProjeto_Prazo ,
                                             decimal AV13TFTipoProjeto_Prazo_To ,
                                             decimal AV14TFTipoProjeto_CnstExp ,
                                             decimal AV15TFTipoProjeto_CnstExp_To ,
                                             String A980TipoProjeto_Nome ,
                                             decimal A981TipoProjeto_Prazo ,
                                             decimal A982TipoProjeto_CnstExp )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [12] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [TipoProjeto_Nome], [TipoProjeto_CnstExp], [TipoProjeto_Prazo], [TipoProjeto_Codigo] FROM [TipoProjeto] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "TIPOPROJETO_NOME") == 0 ) && ( AV35DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TipoProjeto_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Nome] like @lV36TipoProjeto_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Nome] like @lV36TipoProjeto_Nome1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "TIPOPROJETO_NOME") == 0 ) && ( AV35DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36TipoProjeto_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Nome] like '%' + @lV36TipoProjeto_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Nome] like '%' + @lV36TipoProjeto_Nome1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV37DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "TIPOPROJETO_NOME") == 0 ) && ( AV39DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TipoProjeto_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Nome] like @lV40TipoProjeto_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Nome] like @lV40TipoProjeto_Nome2)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV37DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "TIPOPROJETO_NOME") == 0 ) && ( AV39DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TipoProjeto_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Nome] like '%' + @lV40TipoProjeto_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Nome] like '%' + @lV40TipoProjeto_Nome2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV41DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "TIPOPROJETO_NOME") == 0 ) && ( AV43DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TipoProjeto_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Nome] like @lV44TipoProjeto_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Nome] like @lV44TipoProjeto_Nome3)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV41DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "TIPOPROJETO_NOME") == 0 ) && ( AV43DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TipoProjeto_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Nome] like '%' + @lV44TipoProjeto_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Nome] like '%' + @lV44TipoProjeto_Nome3)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFTipoProjeto_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFTipoProjeto_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Nome] like @lV10TFTipoProjeto_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Nome] like @lV10TFTipoProjeto_Nome)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFTipoProjeto_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Nome] = @AV11TFTipoProjeto_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Nome] = @AV11TFTipoProjeto_Nome_Sel)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV12TFTipoProjeto_Prazo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Prazo] >= @AV12TFTipoProjeto_Prazo)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Prazo] >= @AV12TFTipoProjeto_Prazo)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV13TFTipoProjeto_Prazo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_Prazo] <= @AV13TFTipoProjeto_Prazo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_Prazo] <= @AV13TFTipoProjeto_Prazo_To)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV14TFTipoProjeto_CnstExp) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_CnstExp] >= @AV14TFTipoProjeto_CnstExp)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_CnstExp] >= @AV14TFTipoProjeto_CnstExp)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV15TFTipoProjeto_CnstExp_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoProjeto_CnstExp] <= @AV15TFTipoProjeto_CnstExp_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoProjeto_CnstExp] <= @AV15TFTipoProjeto_CnstExp_To)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [TipoProjeto_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00PM2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (decimal)dynConstraints[13] , (decimal)dynConstraints[14] , (decimal)dynConstraints[15] , (decimal)dynConstraints[16] , (String)dynConstraints[17] , (decimal)dynConstraints[18] , (decimal)dynConstraints[19] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00PM2 ;
          prmP00PM2 = new Object[] {
          new Object[] {"@lV36TipoProjeto_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV36TipoProjeto_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV40TipoProjeto_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV40TipoProjeto_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV44TipoProjeto_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV44TipoProjeto_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFTipoProjeto_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFTipoProjeto_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV12TFTipoProjeto_Prazo",SqlDbType.Decimal,5,2} ,
          new Object[] {"@AV13TFTipoProjeto_Prazo_To",SqlDbType.Decimal,5,2} ,
          new Object[] {"@AV14TFTipoProjeto_CnstExp",SqlDbType.Decimal,5,2} ,
          new Object[] {"@AV15TFTipoProjeto_CnstExp_To",SqlDbType.Decimal,5,2}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00PM2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00PM2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[23]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getprompttipoprojetofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getprompttipoprojetofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getprompttipoprojetofilterdata") )
          {
             return  ;
          }
          getprompttipoprojetofilterdata worker = new getprompttipoprojetofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
