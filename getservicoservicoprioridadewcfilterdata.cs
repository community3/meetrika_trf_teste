/*
               File: GetServicoServicoPrioridadeWCFilterData
        Description: Get Servico Servico Prioridade WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:53:26.76
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getservicoservicoprioridadewcfilterdata : GXProcedure
   {
      public getservicoservicoprioridadewcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getservicoservicoprioridadewcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
         return AV25OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getservicoservicoprioridadewcfilterdata objgetservicoservicoprioridadewcfilterdata;
         objgetservicoservicoprioridadewcfilterdata = new getservicoservicoprioridadewcfilterdata();
         objgetservicoservicoprioridadewcfilterdata.AV16DDOName = aP0_DDOName;
         objgetservicoservicoprioridadewcfilterdata.AV14SearchTxt = aP1_SearchTxt;
         objgetservicoservicoprioridadewcfilterdata.AV15SearchTxtTo = aP2_SearchTxtTo;
         objgetservicoservicoprioridadewcfilterdata.AV20OptionsJson = "" ;
         objgetservicoservicoprioridadewcfilterdata.AV23OptionsDescJson = "" ;
         objgetservicoservicoprioridadewcfilterdata.AV25OptionIndexesJson = "" ;
         objgetservicoservicoprioridadewcfilterdata.context.SetSubmitInitialConfig(context);
         objgetservicoservicoprioridadewcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetservicoservicoprioridadewcfilterdata);
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getservicoservicoprioridadewcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV19Options = (IGxCollection)(new GxSimpleCollection());
         AV22OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV24OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_SERVICOPRIORIDADE_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADSERVICOPRIORIDADE_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_SERVICOPRIORIDADE_FINALIDADE") == 0 )
         {
            /* Execute user subroutine: 'LOADSERVICOPRIORIDADE_FINALIDADEOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV20OptionsJson = AV19Options.ToJSonString(false);
         AV23OptionsDescJson = AV22OptionsDesc.ToJSonString(false);
         AV25OptionIndexesJson = AV24OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get("ServicoServicoPrioridadeWCGridState"), "") == 0 )
         {
            AV29GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ServicoServicoPrioridadeWCGridState"), "");
         }
         else
         {
            AV29GridState.FromXml(AV27Session.Get("ServicoServicoPrioridadeWCGridState"), "");
         }
         AV35GXV1 = 1;
         while ( AV35GXV1 <= AV29GridState.gxTpr_Filtervalues.Count )
         {
            AV30GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV29GridState.gxTpr_Filtervalues.Item(AV35GXV1));
            if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFSERVICOPRIORIDADE_NOME") == 0 )
            {
               AV10TFServicoPrioridade_Nome = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFSERVICOPRIORIDADE_NOME_SEL") == 0 )
            {
               AV11TFServicoPrioridade_Nome_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFSERVICOPRIORIDADE_FINALIDADE") == 0 )
            {
               AV12TFServicoPrioridade_Finalidade = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFSERVICOPRIORIDADE_FINALIDADE_SEL") == 0 )
            {
               AV13TFServicoPrioridade_Finalidade_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "PARM_&SERVICOPRIORIDADE_SRVCOD") == 0 )
            {
               AV32ServicoPrioridade_SrvCod = (int)(NumberUtil.Val( AV30GridStateFilterValue.gxTpr_Value, "."));
            }
            AV35GXV1 = (int)(AV35GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADSERVICOPRIORIDADE_NOMEOPTIONS' Routine */
         AV10TFServicoPrioridade_Nome = AV14SearchTxt;
         AV11TFServicoPrioridade_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV11TFServicoPrioridade_Nome_Sel ,
                                              AV10TFServicoPrioridade_Nome ,
                                              AV13TFServicoPrioridade_Finalidade_Sel ,
                                              AV12TFServicoPrioridade_Finalidade ,
                                              A1441ServicoPrioridade_Nome ,
                                              A1442ServicoPrioridade_Finalidade ,
                                              AV32ServicoPrioridade_SrvCod ,
                                              A1439ServicoPrioridade_SrvCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV10TFServicoPrioridade_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFServicoPrioridade_Nome), 50, "%");
         lV12TFServicoPrioridade_Finalidade = StringUtil.Concat( StringUtil.RTrim( AV12TFServicoPrioridade_Finalidade), "%", "");
         /* Using cursor P00JP2 */
         pr_default.execute(0, new Object[] {AV32ServicoPrioridade_SrvCod, lV10TFServicoPrioridade_Nome, AV11TFServicoPrioridade_Nome_Sel, lV12TFServicoPrioridade_Finalidade, AV13TFServicoPrioridade_Finalidade_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKJP2 = false;
            A1439ServicoPrioridade_SrvCod = P00JP2_A1439ServicoPrioridade_SrvCod[0];
            A1441ServicoPrioridade_Nome = P00JP2_A1441ServicoPrioridade_Nome[0];
            A1442ServicoPrioridade_Finalidade = P00JP2_A1442ServicoPrioridade_Finalidade[0];
            n1442ServicoPrioridade_Finalidade = P00JP2_n1442ServicoPrioridade_Finalidade[0];
            A1440ServicoPrioridade_Codigo = P00JP2_A1440ServicoPrioridade_Codigo[0];
            AV26count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00JP2_A1439ServicoPrioridade_SrvCod[0] == A1439ServicoPrioridade_SrvCod ) && ( StringUtil.StrCmp(P00JP2_A1441ServicoPrioridade_Nome[0], A1441ServicoPrioridade_Nome) == 0 ) )
            {
               BRKJP2 = false;
               A1440ServicoPrioridade_Codigo = P00JP2_A1440ServicoPrioridade_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKJP2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1441ServicoPrioridade_Nome)) )
            {
               AV18Option = A1441ServicoPrioridade_Nome;
               AV19Options.Add(AV18Option, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJP2 )
            {
               BRKJP2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADSERVICOPRIORIDADE_FINALIDADEOPTIONS' Routine */
         AV12TFServicoPrioridade_Finalidade = AV14SearchTxt;
         AV13TFServicoPrioridade_Finalidade_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV11TFServicoPrioridade_Nome_Sel ,
                                              AV10TFServicoPrioridade_Nome ,
                                              AV13TFServicoPrioridade_Finalidade_Sel ,
                                              AV12TFServicoPrioridade_Finalidade ,
                                              A1441ServicoPrioridade_Nome ,
                                              A1442ServicoPrioridade_Finalidade ,
                                              AV32ServicoPrioridade_SrvCod ,
                                              A1439ServicoPrioridade_SrvCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV10TFServicoPrioridade_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFServicoPrioridade_Nome), 50, "%");
         lV12TFServicoPrioridade_Finalidade = StringUtil.Concat( StringUtil.RTrim( AV12TFServicoPrioridade_Finalidade), "%", "");
         /* Using cursor P00JP3 */
         pr_default.execute(1, new Object[] {AV32ServicoPrioridade_SrvCod, lV10TFServicoPrioridade_Nome, AV11TFServicoPrioridade_Nome_Sel, lV12TFServicoPrioridade_Finalidade, AV13TFServicoPrioridade_Finalidade_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKJP4 = false;
            A1439ServicoPrioridade_SrvCod = P00JP3_A1439ServicoPrioridade_SrvCod[0];
            A1442ServicoPrioridade_Finalidade = P00JP3_A1442ServicoPrioridade_Finalidade[0];
            n1442ServicoPrioridade_Finalidade = P00JP3_n1442ServicoPrioridade_Finalidade[0];
            A1441ServicoPrioridade_Nome = P00JP3_A1441ServicoPrioridade_Nome[0];
            A1440ServicoPrioridade_Codigo = P00JP3_A1440ServicoPrioridade_Codigo[0];
            AV26count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00JP3_A1439ServicoPrioridade_SrvCod[0] == A1439ServicoPrioridade_SrvCod ) && ( StringUtil.StrCmp(P00JP3_A1442ServicoPrioridade_Finalidade[0], A1442ServicoPrioridade_Finalidade) == 0 ) )
            {
               BRKJP4 = false;
               A1440ServicoPrioridade_Codigo = P00JP3_A1440ServicoPrioridade_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKJP4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1442ServicoPrioridade_Finalidade)) )
            {
               AV18Option = A1442ServicoPrioridade_Finalidade;
               AV19Options.Add(AV18Option, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJP4 )
            {
               BRKJP4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV19Options = new GxSimpleCollection();
         AV22OptionsDesc = new GxSimpleCollection();
         AV24OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV27Session = context.GetSession();
         AV29GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV30GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFServicoPrioridade_Nome = "";
         AV11TFServicoPrioridade_Nome_Sel = "";
         AV12TFServicoPrioridade_Finalidade = "";
         AV13TFServicoPrioridade_Finalidade_Sel = "";
         scmdbuf = "";
         lV10TFServicoPrioridade_Nome = "";
         lV12TFServicoPrioridade_Finalidade = "";
         A1441ServicoPrioridade_Nome = "";
         A1442ServicoPrioridade_Finalidade = "";
         P00JP2_A1439ServicoPrioridade_SrvCod = new int[1] ;
         P00JP2_A1441ServicoPrioridade_Nome = new String[] {""} ;
         P00JP2_A1442ServicoPrioridade_Finalidade = new String[] {""} ;
         P00JP2_n1442ServicoPrioridade_Finalidade = new bool[] {false} ;
         P00JP2_A1440ServicoPrioridade_Codigo = new int[1] ;
         AV18Option = "";
         P00JP3_A1439ServicoPrioridade_SrvCod = new int[1] ;
         P00JP3_A1442ServicoPrioridade_Finalidade = new String[] {""} ;
         P00JP3_n1442ServicoPrioridade_Finalidade = new bool[] {false} ;
         P00JP3_A1441ServicoPrioridade_Nome = new String[] {""} ;
         P00JP3_A1440ServicoPrioridade_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getservicoservicoprioridadewcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00JP2_A1439ServicoPrioridade_SrvCod, P00JP2_A1441ServicoPrioridade_Nome, P00JP2_A1442ServicoPrioridade_Finalidade, P00JP2_n1442ServicoPrioridade_Finalidade, P00JP2_A1440ServicoPrioridade_Codigo
               }
               , new Object[] {
               P00JP3_A1439ServicoPrioridade_SrvCod, P00JP3_A1442ServicoPrioridade_Finalidade, P00JP3_n1442ServicoPrioridade_Finalidade, P00JP3_A1441ServicoPrioridade_Nome, P00JP3_A1440ServicoPrioridade_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV35GXV1 ;
      private int AV32ServicoPrioridade_SrvCod ;
      private int A1439ServicoPrioridade_SrvCod ;
      private int A1440ServicoPrioridade_Codigo ;
      private long AV26count ;
      private String AV10TFServicoPrioridade_Nome ;
      private String AV11TFServicoPrioridade_Nome_Sel ;
      private String scmdbuf ;
      private String lV10TFServicoPrioridade_Nome ;
      private String A1441ServicoPrioridade_Nome ;
      private bool returnInSub ;
      private bool BRKJP2 ;
      private bool n1442ServicoPrioridade_Finalidade ;
      private bool BRKJP4 ;
      private String AV25OptionIndexesJson ;
      private String AV20OptionsJson ;
      private String AV23OptionsDescJson ;
      private String A1442ServicoPrioridade_Finalidade ;
      private String AV16DDOName ;
      private String AV14SearchTxt ;
      private String AV15SearchTxtTo ;
      private String AV12TFServicoPrioridade_Finalidade ;
      private String AV13TFServicoPrioridade_Finalidade_Sel ;
      private String lV12TFServicoPrioridade_Finalidade ;
      private String AV18Option ;
      private IGxSession AV27Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00JP2_A1439ServicoPrioridade_SrvCod ;
      private String[] P00JP2_A1441ServicoPrioridade_Nome ;
      private String[] P00JP2_A1442ServicoPrioridade_Finalidade ;
      private bool[] P00JP2_n1442ServicoPrioridade_Finalidade ;
      private int[] P00JP2_A1440ServicoPrioridade_Codigo ;
      private int[] P00JP3_A1439ServicoPrioridade_SrvCod ;
      private String[] P00JP3_A1442ServicoPrioridade_Finalidade ;
      private bool[] P00JP3_n1442ServicoPrioridade_Finalidade ;
      private String[] P00JP3_A1441ServicoPrioridade_Nome ;
      private int[] P00JP3_A1440ServicoPrioridade_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV19Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV29GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV30GridStateFilterValue ;
   }

   public class getservicoservicoprioridadewcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00JP2( IGxContext context ,
                                             String AV11TFServicoPrioridade_Nome_Sel ,
                                             String AV10TFServicoPrioridade_Nome ,
                                             String AV13TFServicoPrioridade_Finalidade_Sel ,
                                             String AV12TFServicoPrioridade_Finalidade ,
                                             String A1441ServicoPrioridade_Nome ,
                                             String A1442ServicoPrioridade_Finalidade ,
                                             int AV32ServicoPrioridade_SrvCod ,
                                             int A1439ServicoPrioridade_SrvCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [5] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [ServicoPrioridade_SrvCod], [ServicoPrioridade_Nome], [ServicoPrioridade_Finalidade], [ServicoPrioridade_Codigo] FROM [ServicoPrioridade] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([ServicoPrioridade_SrvCod] = @AV32ServicoPrioridade_SrvCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFServicoPrioridade_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFServicoPrioridade_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([ServicoPrioridade_Nome] like @lV10TFServicoPrioridade_Nome)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFServicoPrioridade_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([ServicoPrioridade_Nome] = @AV11TFServicoPrioridade_Nome_Sel)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFServicoPrioridade_Finalidade_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFServicoPrioridade_Finalidade)) ) )
         {
            sWhereString = sWhereString + " and ([ServicoPrioridade_Finalidade] like @lV12TFServicoPrioridade_Finalidade)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFServicoPrioridade_Finalidade_Sel)) )
         {
            sWhereString = sWhereString + " and ([ServicoPrioridade_Finalidade] = @AV13TFServicoPrioridade_Finalidade_Sel)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ServicoPrioridade_SrvCod], [ServicoPrioridade_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00JP3( IGxContext context ,
                                             String AV11TFServicoPrioridade_Nome_Sel ,
                                             String AV10TFServicoPrioridade_Nome ,
                                             String AV13TFServicoPrioridade_Finalidade_Sel ,
                                             String AV12TFServicoPrioridade_Finalidade ,
                                             String A1441ServicoPrioridade_Nome ,
                                             String A1442ServicoPrioridade_Finalidade ,
                                             int AV32ServicoPrioridade_SrvCod ,
                                             int A1439ServicoPrioridade_SrvCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [5] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [ServicoPrioridade_SrvCod], [ServicoPrioridade_Finalidade], [ServicoPrioridade_Nome], [ServicoPrioridade_Codigo] FROM [ServicoPrioridade] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([ServicoPrioridade_SrvCod] = @AV32ServicoPrioridade_SrvCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFServicoPrioridade_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFServicoPrioridade_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([ServicoPrioridade_Nome] like @lV10TFServicoPrioridade_Nome)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFServicoPrioridade_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([ServicoPrioridade_Nome] = @AV11TFServicoPrioridade_Nome_Sel)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFServicoPrioridade_Finalidade_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFServicoPrioridade_Finalidade)) ) )
         {
            sWhereString = sWhereString + " and ([ServicoPrioridade_Finalidade] like @lV12TFServicoPrioridade_Finalidade)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFServicoPrioridade_Finalidade_Sel)) )
         {
            sWhereString = sWhereString + " and ([ServicoPrioridade_Finalidade] = @AV13TFServicoPrioridade_Finalidade_Sel)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ServicoPrioridade_SrvCod], [ServicoPrioridade_Finalidade]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00JP2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] );
               case 1 :
                     return conditional_P00JP3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (int)dynConstraints[7] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00JP2 ;
          prmP00JP2 = new Object[] {
          new Object[] {"@AV32ServicoPrioridade_SrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFServicoPrioridade_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFServicoPrioridade_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFServicoPrioridade_Finalidade",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV13TFServicoPrioridade_Finalidade_Sel",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmP00JP3 ;
          prmP00JP3 = new Object[] {
          new Object[] {"@AV32ServicoPrioridade_SrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFServicoPrioridade_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFServicoPrioridade_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFServicoPrioridade_Finalidade",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV13TFServicoPrioridade_Finalidade_Sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00JP2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00JP2,100,0,true,false )
             ,new CursorDef("P00JP3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00JP3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getservicoservicoprioridadewcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getservicoservicoprioridadewcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getservicoservicoprioridadewcfilterdata") )
          {
             return  ;
          }
          getservicoservicoprioridadewcfilterdata worker = new getservicoservicoprioridadewcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
