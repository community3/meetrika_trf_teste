/*
               File: PRC_DemandaTemErros
        Description: Demanda Tem Erros
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:15.60
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_demandatemerros : GXProcedure
   {
      public prc_demandatemerros( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_demandatemerros( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           int aP1_OsVinculada ,
                           ref String aP2_TooltipText ,
                           out bool aP3_ErrosFlag )
      {
         this.AV11ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV12OsVinculada = aP1_OsVinculada;
         this.AV9TooltipText = aP2_TooltipText;
         this.AV8ErrosFlag = false ;
         initialize();
         executePrivate();
         aP2_TooltipText=this.AV9TooltipText;
         aP3_ErrosFlag=this.AV8ErrosFlag;
      }

      public bool executeUdp( int aP0_ContagemResultado_Codigo ,
                              int aP1_OsVinculada ,
                              ref String aP2_TooltipText )
      {
         this.AV11ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV12OsVinculada = aP1_OsVinculada;
         this.AV9TooltipText = aP2_TooltipText;
         this.AV8ErrosFlag = false ;
         initialize();
         executePrivate();
         aP2_TooltipText=this.AV9TooltipText;
         aP3_ErrosFlag=this.AV8ErrosFlag;
         return AV8ErrosFlag ;
      }

      public void executeSubmit( int aP0_ContagemResultado_Codigo ,
                                 int aP1_OsVinculada ,
                                 ref String aP2_TooltipText ,
                                 out bool aP3_ErrosFlag )
      {
         prc_demandatemerros objprc_demandatemerros;
         objprc_demandatemerros = new prc_demandatemerros();
         objprc_demandatemerros.AV11ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_demandatemerros.AV12OsVinculada = aP1_OsVinculada;
         objprc_demandatemerros.AV9TooltipText = aP2_TooltipText;
         objprc_demandatemerros.AV8ErrosFlag = false ;
         objprc_demandatemerros.context.SetSubmitInitialConfig(context);
         objprc_demandatemerros.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_demandatemerros);
         aP2_TooltipText=this.AV9TooltipText;
         aP3_ErrosFlag=this.AV8ErrosFlag;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_demandatemerros)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV9TooltipText = "";
         AV8ErrosFlag = false;
         /* Using cursor P004G2 */
         pr_default.execute(0, new Object[] {AV11ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A581ContagemResultadoErro_Status = P004G2_A581ContagemResultadoErro_Status[0];
            A456ContagemResultado_Codigo = P004G2_A456ContagemResultado_Codigo[0];
            A579ContagemResultadoErro_Tipo = P004G2_A579ContagemResultadoErro_Tipo[0];
            AV8ErrosFlag = true;
            if ( StringUtil.StrCmp(A579ContagemResultadoErro_Tipo, "DE") == 0 )
            {
               AV9TooltipText = AV9TooltipText + "Falta Descri��o - ";
            }
            else if ( StringUtil.StrCmp(A579ContagemResultadoErro_Tipo, "SC") == 0 )
            {
               AV9TooltipText = AV9TooltipText + "Falta Coordena��o - ";
            }
            else if ( StringUtil.StrCmp(A579ContagemResultadoErro_Tipo, "DA") == 0 )
            {
               AV9TooltipText = AV9TooltipText + "Data incorreta - ";
            }
            else if ( StringUtil.StrCmp(A579ContagemResultadoErro_Tipo, "PF") == 0 )
            {
               AV9TooltipText = AV9TooltipText + "Diferen�a em PFs - ";
            }
            else
            {
               AV9TooltipText = "Erro desconhecido";
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV12OsVinculada ,
                                              A1593ContagemResultado_CntSrvTpVnc ,
                                              A484ContagemResultado_StatusDmn ,
                                              AV11ContagemResultado_Codigo ,
                                              A602ContagemResultado_OSVinculada },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor P004G3 */
         pr_default.execute(1, new Object[] {AV11ContagemResultado_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P004G3_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P004G3_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = P004G3_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P004G3_n601ContagemResultado_Servico[0];
            A1593ContagemResultado_CntSrvTpVnc = P004G3_A1593ContagemResultado_CntSrvTpVnc[0];
            n1593ContagemResultado_CntSrvTpVnc = P004G3_n1593ContagemResultado_CntSrvTpVnc[0];
            A484ContagemResultado_StatusDmn = P004G3_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P004G3_n484ContagemResultado_StatusDmn[0];
            A602ContagemResultado_OSVinculada = P004G3_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P004G3_n602ContagemResultado_OSVinculada[0];
            A801ContagemResultado_ServicoSigla = P004G3_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P004G3_n801ContagemResultado_ServicoSigla[0];
            A771ContagemResultado_StatusDmnVnc = P004G3_A771ContagemResultado_StatusDmnVnc[0];
            n771ContagemResultado_StatusDmnVnc = P004G3_n771ContagemResultado_StatusDmnVnc[0];
            A456ContagemResultado_Codigo = P004G3_A456ContagemResultado_Codigo[0];
            A601ContagemResultado_Servico = P004G3_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P004G3_n601ContagemResultado_Servico[0];
            A1593ContagemResultado_CntSrvTpVnc = P004G3_A1593ContagemResultado_CntSrvTpVnc[0];
            n1593ContagemResultado_CntSrvTpVnc = P004G3_n1593ContagemResultado_CntSrvTpVnc[0];
            A801ContagemResultado_ServicoSigla = P004G3_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P004G3_n801ContagemResultado_ServicoSigla[0];
            A771ContagemResultado_StatusDmnVnc = P004G3_A771ContagemResultado_StatusDmnVnc[0];
            n771ContagemResultado_StatusDmnVnc = P004G3_n771ContagemResultado_StatusDmnVnc[0];
            AV8ErrosFlag = true;
            AV9TooltipText = AV9TooltipText + "Servi�o de " + StringUtil.Trim( A801ContagemResultado_ServicoSigla) + " pendente - ";
            if ( StringUtil.StrCmp(A771ContagemResultado_StatusDmnVnc, "B") == 0 )
            {
               /* Using cursor P004G4 */
               pr_default.execute(2, new Object[] {AV11ContagemResultado_Codigo});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A822ContagemResultadoChckLstLog_UsuarioCod = P004G4_A822ContagemResultadoChckLstLog_UsuarioCod[0];
                  A823ContagemResultadoChckLstLog_PessoaCod = P004G4_A823ContagemResultadoChckLstLog_PessoaCod[0];
                  n823ContagemResultadoChckLstLog_PessoaCod = P004G4_n823ContagemResultadoChckLstLog_PessoaCod[0];
                  A811ContagemResultadoChckLstLog_ChckLstCod = P004G4_A811ContagemResultadoChckLstLog_ChckLstCod[0];
                  n811ContagemResultadoChckLstLog_ChckLstCod = P004G4_n811ContagemResultadoChckLstLog_ChckLstCod[0];
                  A1853ContagemResultadoChckLstLog_OSCodigo = P004G4_A1853ContagemResultadoChckLstLog_OSCodigo[0];
                  A817ContagemResultadoChckLstLog_UsuarioNome = P004G4_A817ContagemResultadoChckLstLog_UsuarioNome[0];
                  n817ContagemResultadoChckLstLog_UsuarioNome = P004G4_n817ContagemResultadoChckLstLog_UsuarioNome[0];
                  A812ContagemResultadoChckLstLog_ChckLstDes = P004G4_A812ContagemResultadoChckLstLog_ChckLstDes[0];
                  n812ContagemResultadoChckLstLog_ChckLstDes = P004G4_n812ContagemResultadoChckLstLog_ChckLstDes[0];
                  A820ContagemResultadoChckLstLog_Codigo = P004G4_A820ContagemResultadoChckLstLog_Codigo[0];
                  A823ContagemResultadoChckLstLog_PessoaCod = P004G4_A823ContagemResultadoChckLstLog_PessoaCod[0];
                  n823ContagemResultadoChckLstLog_PessoaCod = P004G4_n823ContagemResultadoChckLstLog_PessoaCod[0];
                  A817ContagemResultadoChckLstLog_UsuarioNome = P004G4_A817ContagemResultadoChckLstLog_UsuarioNome[0];
                  n817ContagemResultadoChckLstLog_UsuarioNome = P004G4_n817ContagemResultadoChckLstLog_UsuarioNome[0];
                  A812ContagemResultadoChckLstLog_ChckLstDes = P004G4_A812ContagemResultadoChckLstLog_ChckLstDes[0];
                  n812ContagemResultadoChckLstLog_ChckLstDes = P004G4_n812ContagemResultadoChckLstLog_ChckLstDes[0];
                  Gx_msg = StringUtil.Substring( A812ContagemResultadoChckLstLog_ChckLstDes, 1, 2) + " pendente (" + StringUtil.Trim( A817ContagemResultadoChckLstLog_UsuarioNome) + ")";
                  pr_default.readNext(2);
               }
               pr_default.close(2);
               AV9TooltipText = AV9TooltipText + Gx_msg;
            }
            pr_default.readNext(1);
         }
         pr_default.close(1);
         /* Using cursor P004G6 */
         pr_default.execute(3, new Object[] {AV11ContagemResultado_Codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P004G6_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P004G6_n1553ContagemResultado_CntSrvCod[0];
            A456ContagemResultado_Codigo = P004G6_A456ContagemResultado_Codigo[0];
            A1596ContagemResultado_CntSrvPrc = P004G6_A1596ContagemResultado_CntSrvPrc[0];
            n1596ContagemResultado_CntSrvPrc = P004G6_n1596ContagemResultado_CntSrvPrc[0];
            A1593ContagemResultado_CntSrvTpVnc = P004G6_A1593ContagemResultado_CntSrvTpVnc[0];
            n1593ContagemResultado_CntSrvTpVnc = P004G6_n1593ContagemResultado_CntSrvTpVnc[0];
            A802ContagemResultado_DeflatorCnt = P004G6_A802ContagemResultado_DeflatorCnt[0];
            A584ContagemResultado_ContadorFM = P004G6_A584ContagemResultado_ContadorFM[0];
            A1596ContagemResultado_CntSrvPrc = P004G6_A1596ContagemResultado_CntSrvPrc[0];
            n1596ContagemResultado_CntSrvPrc = P004G6_n1596ContagemResultado_CntSrvPrc[0];
            A1593ContagemResultado_CntSrvTpVnc = P004G6_A1593ContagemResultado_CntSrvTpVnc[0];
            n1593ContagemResultado_CntSrvTpVnc = P004G6_n1593ContagemResultado_CntSrvTpVnc[0];
            A802ContagemResultado_DeflatorCnt = P004G6_A802ContagemResultado_DeflatorCnt[0];
            A584ContagemResultado_ContadorFM = P004G6_A584ContagemResultado_ContadorFM[0];
            if ( ( A584ContagemResultado_ContadorFM > 0 ) && ( A1596ContagemResultado_CntSrvPrc != A802ContagemResultado_DeflatorCnt ) )
            {
               AV9TooltipText = AV9TooltipText + " % do Deflator diferentes - ";
               AV8ErrosFlag = true;
            }
            if ( ( StringUtil.StrCmp(A1593ContagemResultado_CntSrvTpVnc, "D") == 0 ) || ( StringUtil.StrCmp(A1593ContagemResultado_CntSrvTpVnc, "A") == 0 ) )
            {
               /* Execute user subroutine: 'PENDENCIADEBITOCREDITO' */
               S111 ();
               if ( returnInSub )
               {
                  pr_default.close(3);
                  this.cleanup();
                  if (true) return;
               }
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(3);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'PENDENCIADEBITOCREDITO' Routine */
         /* Using cursor P004G7 */
         pr_default.execute(4, new Object[] {AV12OsVinculada});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P004G7_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P004G7_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = P004G7_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P004G7_n601ContagemResultado_Servico[0];
            A484ContagemResultado_StatusDmn = P004G7_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P004G7_n484ContagemResultado_StatusDmn[0];
            A456ContagemResultado_Codigo = P004G7_A456ContagemResultado_Codigo[0];
            A801ContagemResultado_ServicoSigla = P004G7_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P004G7_n801ContagemResultado_ServicoSigla[0];
            A601ContagemResultado_Servico = P004G7_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P004G7_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P004G7_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P004G7_n801ContagemResultado_ServicoSigla[0];
            AV8ErrosFlag = true;
            AV9TooltipText = AV9TooltipText + StringUtil.Trim( A801ContagemResultado_ServicoSigla) + " da OS vinculada pendente";
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(4);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P004G2_A581ContagemResultadoErro_Status = new String[] {""} ;
         P004G2_A456ContagemResultado_Codigo = new int[1] ;
         P004G2_A579ContagemResultadoErro_Tipo = new String[] {""} ;
         A581ContagemResultadoErro_Status = "";
         A579ContagemResultadoErro_Tipo = "";
         A1593ContagemResultado_CntSrvTpVnc = "";
         A484ContagemResultado_StatusDmn = "";
         P004G3_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P004G3_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P004G3_A601ContagemResultado_Servico = new int[1] ;
         P004G3_n601ContagemResultado_Servico = new bool[] {false} ;
         P004G3_A1593ContagemResultado_CntSrvTpVnc = new String[] {""} ;
         P004G3_n1593ContagemResultado_CntSrvTpVnc = new bool[] {false} ;
         P004G3_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P004G3_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P004G3_A602ContagemResultado_OSVinculada = new int[1] ;
         P004G3_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P004G3_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P004G3_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P004G3_A771ContagemResultado_StatusDmnVnc = new String[] {""} ;
         P004G3_n771ContagemResultado_StatusDmnVnc = new bool[] {false} ;
         P004G3_A456ContagemResultado_Codigo = new int[1] ;
         A801ContagemResultado_ServicoSigla = "";
         A771ContagemResultado_StatusDmnVnc = "";
         P004G4_A822ContagemResultadoChckLstLog_UsuarioCod = new int[1] ;
         P004G4_A823ContagemResultadoChckLstLog_PessoaCod = new int[1] ;
         P004G4_n823ContagemResultadoChckLstLog_PessoaCod = new bool[] {false} ;
         P004G4_A811ContagemResultadoChckLstLog_ChckLstCod = new int[1] ;
         P004G4_n811ContagemResultadoChckLstLog_ChckLstCod = new bool[] {false} ;
         P004G4_A1853ContagemResultadoChckLstLog_OSCodigo = new int[1] ;
         P004G4_A817ContagemResultadoChckLstLog_UsuarioNome = new String[] {""} ;
         P004G4_n817ContagemResultadoChckLstLog_UsuarioNome = new bool[] {false} ;
         P004G4_A812ContagemResultadoChckLstLog_ChckLstDes = new String[] {""} ;
         P004G4_n812ContagemResultadoChckLstLog_ChckLstDes = new bool[] {false} ;
         P004G4_A820ContagemResultadoChckLstLog_Codigo = new int[1] ;
         A817ContagemResultadoChckLstLog_UsuarioNome = "";
         A812ContagemResultadoChckLstLog_ChckLstDes = "";
         Gx_msg = "";
         P004G6_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P004G6_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P004G6_A456ContagemResultado_Codigo = new int[1] ;
         P004G6_A1596ContagemResultado_CntSrvPrc = new decimal[1] ;
         P004G6_n1596ContagemResultado_CntSrvPrc = new bool[] {false} ;
         P004G6_A1593ContagemResultado_CntSrvTpVnc = new String[] {""} ;
         P004G6_n1593ContagemResultado_CntSrvTpVnc = new bool[] {false} ;
         P004G6_A802ContagemResultado_DeflatorCnt = new decimal[1] ;
         P004G6_A584ContagemResultado_ContadorFM = new int[1] ;
         P004G7_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P004G7_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P004G7_A601ContagemResultado_Servico = new int[1] ;
         P004G7_n601ContagemResultado_Servico = new bool[] {false} ;
         P004G7_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P004G7_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P004G7_A456ContagemResultado_Codigo = new int[1] ;
         P004G7_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P004G7_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_demandatemerros__default(),
            new Object[][] {
                new Object[] {
               P004G2_A581ContagemResultadoErro_Status, P004G2_A456ContagemResultado_Codigo, P004G2_A579ContagemResultadoErro_Tipo
               }
               , new Object[] {
               P004G3_A1553ContagemResultado_CntSrvCod, P004G3_n1553ContagemResultado_CntSrvCod, P004G3_A601ContagemResultado_Servico, P004G3_n601ContagemResultado_Servico, P004G3_A1593ContagemResultado_CntSrvTpVnc, P004G3_n1593ContagemResultado_CntSrvTpVnc, P004G3_A484ContagemResultado_StatusDmn, P004G3_n484ContagemResultado_StatusDmn, P004G3_A602ContagemResultado_OSVinculada, P004G3_n602ContagemResultado_OSVinculada,
               P004G3_A801ContagemResultado_ServicoSigla, P004G3_n801ContagemResultado_ServicoSigla, P004G3_A771ContagemResultado_StatusDmnVnc, P004G3_n771ContagemResultado_StatusDmnVnc, P004G3_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P004G4_A822ContagemResultadoChckLstLog_UsuarioCod, P004G4_A823ContagemResultadoChckLstLog_PessoaCod, P004G4_n823ContagemResultadoChckLstLog_PessoaCod, P004G4_A811ContagemResultadoChckLstLog_ChckLstCod, P004G4_n811ContagemResultadoChckLstLog_ChckLstCod, P004G4_A1853ContagemResultadoChckLstLog_OSCodigo, P004G4_A817ContagemResultadoChckLstLog_UsuarioNome, P004G4_n817ContagemResultadoChckLstLog_UsuarioNome, P004G4_A812ContagemResultadoChckLstLog_ChckLstDes, P004G4_n812ContagemResultadoChckLstLog_ChckLstDes,
               P004G4_A820ContagemResultadoChckLstLog_Codigo
               }
               , new Object[] {
               P004G6_A1553ContagemResultado_CntSrvCod, P004G6_n1553ContagemResultado_CntSrvCod, P004G6_A456ContagemResultado_Codigo, P004G6_A1596ContagemResultado_CntSrvPrc, P004G6_n1596ContagemResultado_CntSrvPrc, P004G6_A1593ContagemResultado_CntSrvTpVnc, P004G6_n1593ContagemResultado_CntSrvTpVnc, P004G6_A802ContagemResultado_DeflatorCnt, P004G6_A584ContagemResultado_ContadorFM
               }
               , new Object[] {
               P004G7_A1553ContagemResultado_CntSrvCod, P004G7_n1553ContagemResultado_CntSrvCod, P004G7_A601ContagemResultado_Servico, P004G7_n601ContagemResultado_Servico, P004G7_A484ContagemResultado_StatusDmn, P004G7_n484ContagemResultado_StatusDmn, P004G7_A456ContagemResultado_Codigo, P004G7_A801ContagemResultado_ServicoSigla, P004G7_n801ContagemResultado_ServicoSigla
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV11ContagemResultado_Codigo ;
      private int AV12OsVinculada ;
      private int A456ContagemResultado_Codigo ;
      private int A602ContagemResultado_OSVinculada ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A601ContagemResultado_Servico ;
      private int A822ContagemResultadoChckLstLog_UsuarioCod ;
      private int A823ContagemResultadoChckLstLog_PessoaCod ;
      private int A811ContagemResultadoChckLstLog_ChckLstCod ;
      private int A1853ContagemResultadoChckLstLog_OSCodigo ;
      private int A820ContagemResultadoChckLstLog_Codigo ;
      private int A584ContagemResultado_ContadorFM ;
      private decimal A1596ContagemResultado_CntSrvPrc ;
      private decimal A802ContagemResultado_DeflatorCnt ;
      private String AV9TooltipText ;
      private String scmdbuf ;
      private String A581ContagemResultadoErro_Status ;
      private String A579ContagemResultadoErro_Tipo ;
      private String A1593ContagemResultado_CntSrvTpVnc ;
      private String A484ContagemResultado_StatusDmn ;
      private String A801ContagemResultado_ServicoSigla ;
      private String A771ContagemResultado_StatusDmnVnc ;
      private String A817ContagemResultadoChckLstLog_UsuarioNome ;
      private String Gx_msg ;
      private bool AV8ErrosFlag ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n1593ContagemResultado_CntSrvTpVnc ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n771ContagemResultado_StatusDmnVnc ;
      private bool n823ContagemResultadoChckLstLog_PessoaCod ;
      private bool n811ContagemResultadoChckLstLog_ChckLstCod ;
      private bool n817ContagemResultadoChckLstLog_UsuarioNome ;
      private bool n812ContagemResultadoChckLstLog_ChckLstDes ;
      private bool n1596ContagemResultado_CntSrvPrc ;
      private bool returnInSub ;
      private String A812ContagemResultadoChckLstLog_ChckLstDes ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP2_TooltipText ;
      private IDataStoreProvider pr_default ;
      private String[] P004G2_A581ContagemResultadoErro_Status ;
      private int[] P004G2_A456ContagemResultado_Codigo ;
      private String[] P004G2_A579ContagemResultadoErro_Tipo ;
      private int[] P004G3_A1553ContagemResultado_CntSrvCod ;
      private bool[] P004G3_n1553ContagemResultado_CntSrvCod ;
      private int[] P004G3_A601ContagemResultado_Servico ;
      private bool[] P004G3_n601ContagemResultado_Servico ;
      private String[] P004G3_A1593ContagemResultado_CntSrvTpVnc ;
      private bool[] P004G3_n1593ContagemResultado_CntSrvTpVnc ;
      private String[] P004G3_A484ContagemResultado_StatusDmn ;
      private bool[] P004G3_n484ContagemResultado_StatusDmn ;
      private int[] P004G3_A602ContagemResultado_OSVinculada ;
      private bool[] P004G3_n602ContagemResultado_OSVinculada ;
      private String[] P004G3_A801ContagemResultado_ServicoSigla ;
      private bool[] P004G3_n801ContagemResultado_ServicoSigla ;
      private String[] P004G3_A771ContagemResultado_StatusDmnVnc ;
      private bool[] P004G3_n771ContagemResultado_StatusDmnVnc ;
      private int[] P004G3_A456ContagemResultado_Codigo ;
      private int[] P004G4_A822ContagemResultadoChckLstLog_UsuarioCod ;
      private int[] P004G4_A823ContagemResultadoChckLstLog_PessoaCod ;
      private bool[] P004G4_n823ContagemResultadoChckLstLog_PessoaCod ;
      private int[] P004G4_A811ContagemResultadoChckLstLog_ChckLstCod ;
      private bool[] P004G4_n811ContagemResultadoChckLstLog_ChckLstCod ;
      private int[] P004G4_A1853ContagemResultadoChckLstLog_OSCodigo ;
      private String[] P004G4_A817ContagemResultadoChckLstLog_UsuarioNome ;
      private bool[] P004G4_n817ContagemResultadoChckLstLog_UsuarioNome ;
      private String[] P004G4_A812ContagemResultadoChckLstLog_ChckLstDes ;
      private bool[] P004G4_n812ContagemResultadoChckLstLog_ChckLstDes ;
      private int[] P004G4_A820ContagemResultadoChckLstLog_Codigo ;
      private int[] P004G6_A1553ContagemResultado_CntSrvCod ;
      private bool[] P004G6_n1553ContagemResultado_CntSrvCod ;
      private int[] P004G6_A456ContagemResultado_Codigo ;
      private decimal[] P004G6_A1596ContagemResultado_CntSrvPrc ;
      private bool[] P004G6_n1596ContagemResultado_CntSrvPrc ;
      private String[] P004G6_A1593ContagemResultado_CntSrvTpVnc ;
      private bool[] P004G6_n1593ContagemResultado_CntSrvTpVnc ;
      private decimal[] P004G6_A802ContagemResultado_DeflatorCnt ;
      private int[] P004G6_A584ContagemResultado_ContadorFM ;
      private int[] P004G7_A1553ContagemResultado_CntSrvCod ;
      private bool[] P004G7_n1553ContagemResultado_CntSrvCod ;
      private int[] P004G7_A601ContagemResultado_Servico ;
      private bool[] P004G7_n601ContagemResultado_Servico ;
      private String[] P004G7_A484ContagemResultado_StatusDmn ;
      private bool[] P004G7_n484ContagemResultado_StatusDmn ;
      private int[] P004G7_A456ContagemResultado_Codigo ;
      private String[] P004G7_A801ContagemResultado_ServicoSigla ;
      private bool[] P004G7_n801ContagemResultado_ServicoSigla ;
      private bool aP3_ErrosFlag ;
   }

   public class prc_demandatemerros__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P004G3( IGxContext context ,
                                             int AV12OsVinculada ,
                                             String A1593ContagemResultado_CntSrvTpVnc ,
                                             String A484ContagemResultado_StatusDmn ,
                                             int AV11ContagemResultado_Codigo ,
                                             int A602ContagemResultado_OSVinculada )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [1] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Servico_Codigo] AS ContagemResultado_Servico, T2.[ContratoServicos_TipoVnc] AS ContagemResultado_CntSrvTpVnc, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada, T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T4.[ContagemResultado_StatusDmn] AS ContagemResultado_StatusDmnVnc, T1.[ContagemResultado_Codigo] FROM ((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [ContagemResultado] T4 WITH (NOLOCK) ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_OSVinculada])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultado_OSVinculada] = @AV11ContagemResultado_Codigo)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_StatusDmn] = 'A' or T1.[ContagemResultado_StatusDmn] = 'B' or T1.[ContagemResultado_StatusDmn] = 'D' or T1.[ContagemResultado_StatusDmn] = 'E' or T1.[ContagemResultado_StatusDmn] = 'S' or T1.[ContagemResultado_StatusDmn] = 'I' or T1.[ContagemResultado_StatusDmn] = 'M' or T1.[ContagemResultado_StatusDmn] = 'Q' or T1.[ContagemResultado_StatusDmn] = 'T')";
         if ( (0==AV12OsVinculada) )
         {
            sWhereString = sWhereString + " and (T2.[ContratoServicos_TipoVnc] = 'C' or T2.[ContratoServicos_TipoVnc] = 'A')";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_OSVinculada]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_P004G3(context, (int)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP004G2 ;
          prmP004G2 = new Object[] {
          new Object[] {"@AV11ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004G4 ;
          prmP004G4 = new Object[] {
          new Object[] {"@AV11ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004G6 ;
          prmP004G6 = new Object[] {
          new Object[] {"@AV11ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004G7 ;
          prmP004G7 = new Object[] {
          new Object[] {"@AV12OsVinculada",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004G3 ;
          prmP004G3 = new Object[] {
          new Object[] {"@AV11ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P004G2", "SELECT [ContagemResultadoErro_Status], [ContagemResultado_Codigo], [ContagemResultadoErro_Tipo] FROM [ContagemResultadoErro] WITH (NOLOCK) WHERE ([ContagemResultado_Codigo] = @AV11ContagemResultado_Codigo) AND ([ContagemResultadoErro_Status] = 'P' or [ContagemResultadoErro_Status] = 'E') ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004G2,100,0,false,false )
             ,new CursorDef("P004G3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004G3,100,0,true,false )
             ,new CursorDef("P004G4", "SELECT T1.[ContagemResultadoChckLstLog_UsuarioCod] AS ContagemResultadoChckLstLog_UsuarioCod, T2.[Usuario_PessoaCod] AS ContagemResultadoChckLstLog_PessoaCod, T1.[ContagemResultadoChckLstLog_ChckLstCod] AS ContagemResultadoChckLstLog_ChckLstCod, T1.[ContagemResultadoChckLstLog_OSCodigo], T3.[Pessoa_Nome] AS ContagemResultadoChckLstLog_UsuarioNome, T4.[CheckList_Descricao] AS ContagemResultadoChckLstLog_ChckLstDes, T1.[ContagemResultadoChckLstLog_Codigo] FROM ((([ContagemResultadoChckLstLog] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultadoChckLstLog_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) LEFT JOIN [CheckList] T4 WITH (NOLOCK) ON T4.[CheckList_Codigo] = T1.[ContagemResultadoChckLstLog_ChckLstCod]) WHERE (T1.[ContagemResultadoChckLstLog_OSCodigo] = @AV11ContagemResultado_Codigo) AND (T1.[ContagemResultadoChckLstLog_ChckLstCod] > 0) ORDER BY T1.[ContagemResultadoChckLstLog_OSCodigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004G4,100,0,false,false )
             ,new CursorDef("P004G6", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T2.[Servico_Percentual] AS ContagemResultado_CntSrvPrc, T2.[ContratoServicos_TipoVnc] AS ContagemResultado_CntSrvTpVnc, COALESCE( T3.[ContagemResultado_DeflatorCnt], 0) AS ContagemResultado_DeflatorCnt, COALESCE( T3.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN (SELECT MIN([ContagemResultado_Deflator]) AS ContagemResultado_DeflatorCnt, [ContagemResultado_Codigo], MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T3 ON T3.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @AV11ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004G6,1,0,true,true )
             ,new CursorDef("P004G7", "SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_Codigo], T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) WHERE (T1.[ContagemResultado_Codigo] = @AV12OsVinculada) AND (T1.[ContagemResultado_StatusDmn] = 'A' or T1.[ContagemResultado_StatusDmn] = 'B' or T1.[ContagemResultado_StatusDmn] = 'D' or T1.[ContagemResultado_StatusDmn] = 'E' or T1.[ContagemResultado_StatusDmn] = 'S' or T1.[ContagemResultado_StatusDmn] = 'I' or T1.[ContagemResultado_StatusDmn] = 'M' or T1.[ContagemResultado_StatusDmn] = 'Q' or T1.[ContagemResultado_StatusDmn] = 'T') ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004G7,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(5) ;
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
