/*
               File: SistemaGeneral
        Description: Sistema General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:16:20.85
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class sistemageneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public sistemageneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public sistemageneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Sistema_Codigo )
      {
         this.A127Sistema_Codigo = aP0_Sistema_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         dynSistemaVersao_Codigo = new GXCombobox();
         cmbSistema_Tipo = new GXCombobox();
         cmbSistema_Tecnica = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A127Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A127Sistema_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"SISTEMAVERSAO_CODIGO") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLASISTEMAVERSAO_CODIGO3I2( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA3I2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV17Pgmname = "SistemaGeneral";
               context.Gx_err = 0;
               edtavSistema_pf_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSistema_pf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_pf_Enabled), 5, 0)));
               edtavSistema_pfa_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSistema_pfa_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_pfa_Enabled), 5, 0)));
               GXASISTEMAVERSAO_CODIGO_html3I2( ) ;
               WS3I2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Sistema General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202052118162099");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("sistemageneral.aspx") + "?" + UrlEncode("" +A127Sistema_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA127Sistema_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA127Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMA_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A416Sistema_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMA_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A129Sistema_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMA_DESCRICAO", GetSecureSignedToken( sPrefix, A128Sistema_Descricao));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMAVERSAO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1859SistemaVersao_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMA_COORDENACAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A513Sistema_Coordenacao, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMA_REPOSITORIO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2109Sistema_Repositorio, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vSISTEMA_PF", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV13Sistema_PF, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vSISTEMA_PFA", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV14Sistema_PFA, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMA_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A699Sistema_Tipo, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMA_TECNICA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A700Sistema_Tecnica, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMA_FATORAJUSTE", GetSecureSignedToken( sPrefix, context.localUtil.Format( A686Sistema_FatorAjuste, "ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMA_PRAZO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A688Sistema_Prazo), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMA_CUSTO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A687Sistema_Custo, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMA_ESFORCO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A689Sistema_Esforco), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SISTEMA_IMPDATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1401Sistema_ImpData, "99/99/99 99:99")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "SistemaGeneral";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A699Sistema_Tipo, ""));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A686Sistema_FatorAjuste, "ZZ9.99");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1401Sistema_ImpData, "99/99/99 99:99");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("sistemageneral:[SendSecurityCheck value for]"+"Sistema_Tipo:"+StringUtil.RTrim( context.localUtil.Format( A699Sistema_Tipo, "")));
         GXUtil.WriteLog("sistemageneral:[SendSecurityCheck value for]"+"Sistema_FatorAjuste:"+context.localUtil.Format( A686Sistema_FatorAjuste, "ZZ9.99"));
         GXUtil.WriteLog("sistemageneral:[SendSecurityCheck value for]"+"Sistema_ImpData:"+context.localUtil.Format( A1401Sistema_ImpData, "99/99/99 99:99"));
      }

      protected void RenderHtmlCloseForm3I2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("sistemageneral.js", "?20205211816216");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            if ( ! ( WebComp_Tabelaatributo == null ) )
            {
               WebComp_Tabelaatributo.componentjscripts();
            }
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "SistemaGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Sistema General" ;
      }

      protected void WB3I0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "sistemageneral.aspx");
            }
            wb_table1_2_3I2( true) ;
         }
         else
         {
            wb_table1_2_3I2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_3I2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSistema_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistema_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtSistema_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SistemaGeneral.htm");
         }
         wbLoad = true;
      }

      protected void START3I2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Sistema General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP3I0( ) ;
            }
         }
      }

      protected void WS3I2( )
      {
         START3I2( ) ;
         EVT3I2( ) ;
      }

      protected void EVT3I2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3I0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3I0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E113I2 */
                                    E113I2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3I0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E123I2 */
                                    E123I2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3I0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E133I2 */
                                    E133I2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3I0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E143I2 */
                                    E143I2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3I0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E153I2 */
                                    E153I2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3I0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E163I2 */
                                    E163I2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3I0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E173I2 */
                                    E173I2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3I0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP3I0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavSistema_pf_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     else if ( StringUtil.StrCmp(sEvtType, "W") == 0 )
                     {
                        sEvtType = StringUtil.Left( sEvt, 4);
                        sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                        if ( nCmpId == 116 )
                        {
                           OldTabelaatributo = cgiGet( sPrefix+"W0116");
                           if ( ( StringUtil.Len( OldTabelaatributo) == 0 ) || ( StringUtil.StrCmp(OldTabelaatributo, WebComp_Tabelaatributo_Component) != 0 ) )
                           {
                              WebComp_Tabelaatributo = getWebComponent(GetType(), "GeneXus.Programs", OldTabelaatributo, new Object[] {context} );
                              WebComp_Tabelaatributo.ComponentInit();
                              WebComp_Tabelaatributo.Name = "OldTabelaatributo";
                              WebComp_Tabelaatributo_Component = OldTabelaatributo;
                           }
                           if ( StringUtil.Len( WebComp_Tabelaatributo_Component) != 0 )
                           {
                              WebComp_Tabelaatributo.componentprocess(sPrefix+"W0116", "", sEvt);
                           }
                           WebComp_Tabelaatributo_Component = OldTabelaatributo;
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE3I2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm3I2( ) ;
            }
         }
      }

      protected void PA3I2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            dynSistemaVersao_Codigo.Name = "SISTEMAVERSAO_CODIGO";
            dynSistemaVersao_Codigo.WebTags = "";
            cmbSistema_Tipo.Name = "SISTEMA_TIPO";
            cmbSistema_Tipo.WebTags = "";
            cmbSistema_Tipo.addItem("", "(Nenhum)", 0);
            cmbSistema_Tipo.addItem("D", "Desenvolvimento", 0);
            cmbSistema_Tipo.addItem("M", "Melhoria", 0);
            cmbSistema_Tipo.addItem("A", "Aplica��o", 0);
            if ( cmbSistema_Tipo.ItemCount > 0 )
            {
               A699Sistema_Tipo = cmbSistema_Tipo.getValidValue(A699Sistema_Tipo);
               n699Sistema_Tipo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A699Sistema_Tipo", A699Sistema_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A699Sistema_Tipo, ""))));
            }
            cmbSistema_Tecnica.Name = "SISTEMA_TECNICA";
            cmbSistema_Tecnica.WebTags = "";
            cmbSistema_Tecnica.addItem("", "(Nenhum)", 0);
            cmbSistema_Tecnica.addItem("I", "Indicativa", 0);
            cmbSistema_Tecnica.addItem("E", "Estimada", 0);
            cmbSistema_Tecnica.addItem("D", "Detalhada", 0);
            if ( cmbSistema_Tecnica.ItemCount > 0 )
            {
               A700Sistema_Tecnica = cmbSistema_Tecnica.getValidValue(A700Sistema_Tecnica);
               n700Sistema_Tecnica = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A700Sistema_Tecnica", A700Sistema_Tecnica);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_TECNICA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A700Sistema_Tecnica, ""))));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavSistema_pf_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLASISTEMAVERSAO_CODIGO3I2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLASISTEMAVERSAO_CODIGO_data3I2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXASISTEMAVERSAO_CODIGO_html3I2( )
      {
         int gxdynajaxvalue ;
         GXDLASISTEMAVERSAO_CODIGO_data3I2( ) ;
         gxdynajaxindex = 1;
         dynSistemaVersao_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynSistemaVersao_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynSistemaVersao_Codigo.ItemCount > 0 )
         {
            A1859SistemaVersao_Codigo = (int)(NumberUtil.Val( dynSistemaVersao_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0))), "."));
            n1859SistemaVersao_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMAVERSAO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1859SistemaVersao_Codigo), "ZZZZZ9")));
         }
      }

      protected void GXDLASISTEMAVERSAO_CODIGO_data3I2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor H003I2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H003I2_A1859SistemaVersao_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H003I2_A1860SistemaVersao_Id[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynSistemaVersao_Codigo.ItemCount > 0 )
         {
            A1859SistemaVersao_Codigo = (int)(NumberUtil.Val( dynSistemaVersao_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0))), "."));
            n1859SistemaVersao_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMAVERSAO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1859SistemaVersao_Codigo), "ZZZZZ9")));
         }
         if ( cmbSistema_Tipo.ItemCount > 0 )
         {
            A699Sistema_Tipo = cmbSistema_Tipo.getValidValue(A699Sistema_Tipo);
            n699Sistema_Tipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A699Sistema_Tipo", A699Sistema_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A699Sistema_Tipo, ""))));
         }
         if ( cmbSistema_Tecnica.ItemCount > 0 )
         {
            A700Sistema_Tecnica = cmbSistema_Tecnica.getValidValue(A700Sistema_Tecnica);
            n700Sistema_Tecnica = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A700Sistema_Tecnica", A700Sistema_Tecnica);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_TECNICA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A700Sistema_Tecnica, ""))));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF3I2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV17Pgmname = "SistemaGeneral";
         context.Gx_err = 0;
         edtavSistema_pf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSistema_pf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_pf_Enabled), 5, 0)));
         edtavSistema_pfa_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSistema_pfa_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_pfa_Enabled), 5, 0)));
      }

      protected void RF3I2( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E173I2 */
         E173I2 ();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( 1 != 0 )
            {
               if ( StringUtil.Len( WebComp_Tabelaatributo_Component) != 0 )
               {
                  WebComp_Tabelaatributo.componentstart();
               }
            }
         }
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H003I3 */
            pr_default.execute(1, new Object[] {A127Sistema_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1402Sistema_ImpUserPesCod = H003I3_A1402Sistema_ImpUserPesCod[0];
               n1402Sistema_ImpUserPesCod = H003I3_n1402Sistema_ImpUserPesCod[0];
               A351AmbienteTecnologico_Codigo = H003I3_A351AmbienteTecnologico_Codigo[0];
               n351AmbienteTecnologico_Codigo = H003I3_n351AmbienteTecnologico_Codigo[0];
               A137Metodologia_Codigo = H003I3_A137Metodologia_Codigo[0];
               n137Metodologia_Codigo = H003I3_n137Metodologia_Codigo[0];
               A1399Sistema_ImpUserCod = H003I3_A1399Sistema_ImpUserCod[0];
               n1399Sistema_ImpUserCod = H003I3_n1399Sistema_ImpUserCod[0];
               A1403Sistema_ImpUserPesNom = H003I3_A1403Sistema_ImpUserPesNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1403Sistema_ImpUserPesNom", A1403Sistema_ImpUserPesNom);
               n1403Sistema_ImpUserPesNom = H003I3_n1403Sistema_ImpUserPesNom[0];
               A1401Sistema_ImpData = H003I3_A1401Sistema_ImpData[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1401Sistema_ImpData", context.localUtil.TToC( A1401Sistema_ImpData, 8, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_IMPDATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1401Sistema_ImpData, "99/99/99 99:99")));
               n1401Sistema_ImpData = H003I3_n1401Sistema_ImpData[0];
               A689Sistema_Esforco = H003I3_A689Sistema_Esforco[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A689Sistema_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A689Sistema_Esforco), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_ESFORCO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A689Sistema_Esforco), "ZZZ9")));
               n689Sistema_Esforco = H003I3_n689Sistema_Esforco[0];
               A687Sistema_Custo = H003I3_A687Sistema_Custo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A687Sistema_Custo", StringUtil.LTrim( StringUtil.Str( A687Sistema_Custo, 18, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_CUSTO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A687Sistema_Custo, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
               n687Sistema_Custo = H003I3_n687Sistema_Custo[0];
               A688Sistema_Prazo = H003I3_A688Sistema_Prazo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A688Sistema_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A688Sistema_Prazo), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_PRAZO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A688Sistema_Prazo), "ZZZ9")));
               n688Sistema_Prazo = H003I3_n688Sistema_Prazo[0];
               A686Sistema_FatorAjuste = H003I3_A686Sistema_FatorAjuste[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A686Sistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( A686Sistema_FatorAjuste, 6, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_FATORAJUSTE", GetSecureSignedToken( sPrefix, context.localUtil.Format( A686Sistema_FatorAjuste, "ZZ9.99")));
               n686Sistema_FatorAjuste = H003I3_n686Sistema_FatorAjuste[0];
               A138Metodologia_Descricao = H003I3_A138Metodologia_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A138Metodologia_Descricao", A138Metodologia_Descricao);
               A352AmbienteTecnologico_Descricao = H003I3_A352AmbienteTecnologico_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A352AmbienteTecnologico_Descricao", A352AmbienteTecnologico_Descricao);
               A700Sistema_Tecnica = H003I3_A700Sistema_Tecnica[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A700Sistema_Tecnica", A700Sistema_Tecnica);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_TECNICA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A700Sistema_Tecnica, ""))));
               n700Sistema_Tecnica = H003I3_n700Sistema_Tecnica[0];
               A699Sistema_Tipo = H003I3_A699Sistema_Tipo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A699Sistema_Tipo", A699Sistema_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A699Sistema_Tipo, ""))));
               n699Sistema_Tipo = H003I3_n699Sistema_Tipo[0];
               A2109Sistema_Repositorio = H003I3_A2109Sistema_Repositorio[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2109Sistema_Repositorio", A2109Sistema_Repositorio);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_REPOSITORIO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2109Sistema_Repositorio, ""))));
               n2109Sistema_Repositorio = H003I3_n2109Sistema_Repositorio[0];
               A513Sistema_Coordenacao = H003I3_A513Sistema_Coordenacao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A513Sistema_Coordenacao", A513Sistema_Coordenacao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_COORDENACAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A513Sistema_Coordenacao, "@!"))));
               n513Sistema_Coordenacao = H003I3_n513Sistema_Coordenacao[0];
               A1859SistemaVersao_Codigo = H003I3_A1859SistemaVersao_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMAVERSAO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1859SistemaVersao_Codigo), "ZZZZZ9")));
               n1859SistemaVersao_Codigo = H003I3_n1859SistemaVersao_Codigo[0];
               A128Sistema_Descricao = H003I3_A128Sistema_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A128Sistema_Descricao", A128Sistema_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_DESCRICAO", GetSecureSignedToken( sPrefix, A128Sistema_Descricao));
               n128Sistema_Descricao = H003I3_n128Sistema_Descricao[0];
               A129Sistema_Sigla = H003I3_A129Sistema_Sigla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A129Sistema_Sigla", A129Sistema_Sigla);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A129Sistema_Sigla, "@!"))));
               A416Sistema_Nome = H003I3_A416Sistema_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A416Sistema_Nome", A416Sistema_Nome);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A416Sistema_Nome, "@!"))));
               A352AmbienteTecnologico_Descricao = H003I3_A352AmbienteTecnologico_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A352AmbienteTecnologico_Descricao", A352AmbienteTecnologico_Descricao);
               A138Metodologia_Descricao = H003I3_A138Metodologia_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A138Metodologia_Descricao", A138Metodologia_Descricao);
               A1402Sistema_ImpUserPesCod = H003I3_A1402Sistema_ImpUserPesCod[0];
               n1402Sistema_ImpUserPesCod = H003I3_n1402Sistema_ImpUserPesCod[0];
               A1403Sistema_ImpUserPesNom = H003I3_A1403Sistema_ImpUserPesNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1403Sistema_ImpUserPesNom", A1403Sistema_ImpUserPesNom);
               n1403Sistema_ImpUserPesNom = H003I3_n1403Sistema_ImpUserPesNom[0];
               GXASISTEMAVERSAO_CODIGO_html3I2( ) ;
               /* Execute user event: E123I2 */
               E123I2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            WB3I0( ) ;
         }
      }

      protected void STRUP3I0( )
      {
         /* Before Start, stand alone formulas. */
         AV17Pgmname = "SistemaGeneral";
         context.Gx_err = 0;
         edtavSistema_pf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSistema_pf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_pf_Enabled), 5, 0)));
         edtavSistema_pfa_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSistema_pfa_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_pfa_Enabled), 5, 0)));
         GXASISTEMAVERSAO_CODIGO_html3I2( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E113I2 */
         E113I2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A416Sistema_Nome = StringUtil.Upper( cgiGet( edtSistema_Nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A416Sistema_Nome", A416Sistema_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A416Sistema_Nome, "@!"))));
            A129Sistema_Sigla = StringUtil.Upper( cgiGet( edtSistema_Sigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A129Sistema_Sigla", A129Sistema_Sigla);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A129Sistema_Sigla, "@!"))));
            A128Sistema_Descricao = cgiGet( edtSistema_Descricao_Internalname);
            n128Sistema_Descricao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A128Sistema_Descricao", A128Sistema_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_DESCRICAO", GetSecureSignedToken( sPrefix, A128Sistema_Descricao));
            dynSistemaVersao_Codigo.CurrentValue = cgiGet( dynSistemaVersao_Codigo_Internalname);
            A1859SistemaVersao_Codigo = (int)(NumberUtil.Val( cgiGet( dynSistemaVersao_Codigo_Internalname), "."));
            n1859SistemaVersao_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1859SistemaVersao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMAVERSAO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1859SistemaVersao_Codigo), "ZZZZZ9")));
            A513Sistema_Coordenacao = StringUtil.Upper( cgiGet( edtSistema_Coordenacao_Internalname));
            n513Sistema_Coordenacao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A513Sistema_Coordenacao", A513Sistema_Coordenacao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_COORDENACAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A513Sistema_Coordenacao, "@!"))));
            A2109Sistema_Repositorio = cgiGet( edtSistema_Repositorio_Internalname);
            n2109Sistema_Repositorio = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2109Sistema_Repositorio", A2109Sistema_Repositorio);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_REPOSITORIO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2109Sistema_Repositorio, ""))));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSistema_pf_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSistema_pf_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSISTEMA_PF");
               GX_FocusControl = edtavSistema_pf_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13Sistema_PF = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13Sistema_PF", StringUtil.LTrim( StringUtil.Str( AV13Sistema_PF, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSISTEMA_PF", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV13Sistema_PF, "ZZ,ZZZ,ZZ9.999")));
            }
            else
            {
               AV13Sistema_PF = context.localUtil.CToN( cgiGet( edtavSistema_pf_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13Sistema_PF", StringUtil.LTrim( StringUtil.Str( AV13Sistema_PF, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSISTEMA_PF", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV13Sistema_PF, "ZZ,ZZZ,ZZ9.999")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavSistema_pfa_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavSistema_pfa_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSISTEMA_PFA");
               GX_FocusControl = edtavSistema_pfa_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV14Sistema_PFA = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14Sistema_PFA", StringUtil.LTrim( StringUtil.Str( AV14Sistema_PFA, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSISTEMA_PFA", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV14Sistema_PFA, "ZZ,ZZZ,ZZ9.999")));
            }
            else
            {
               AV14Sistema_PFA = context.localUtil.CToN( cgiGet( edtavSistema_pfa_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14Sistema_PFA", StringUtil.LTrim( StringUtil.Str( AV14Sistema_PFA, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSISTEMA_PFA", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV14Sistema_PFA, "ZZ,ZZZ,ZZ9.999")));
            }
            cmbSistema_Tipo.CurrentValue = cgiGet( cmbSistema_Tipo_Internalname);
            A699Sistema_Tipo = cgiGet( cmbSistema_Tipo_Internalname);
            n699Sistema_Tipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A699Sistema_Tipo", A699Sistema_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A699Sistema_Tipo, ""))));
            cmbSistema_Tecnica.CurrentValue = cgiGet( cmbSistema_Tecnica_Internalname);
            A700Sistema_Tecnica = cgiGet( cmbSistema_Tecnica_Internalname);
            n700Sistema_Tecnica = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A700Sistema_Tecnica", A700Sistema_Tecnica);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_TECNICA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A700Sistema_Tecnica, ""))));
            A352AmbienteTecnologico_Descricao = StringUtil.Upper( cgiGet( edtAmbienteTecnologico_Descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A352AmbienteTecnologico_Descricao", A352AmbienteTecnologico_Descricao);
            A138Metodologia_Descricao = StringUtil.Upper( cgiGet( edtMetodologia_Descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A138Metodologia_Descricao", A138Metodologia_Descricao);
            A686Sistema_FatorAjuste = context.localUtil.CToN( cgiGet( edtSistema_FatorAjuste_Internalname), ",", ".");
            n686Sistema_FatorAjuste = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A686Sistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( A686Sistema_FatorAjuste, 6, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_FATORAJUSTE", GetSecureSignedToken( sPrefix, context.localUtil.Format( A686Sistema_FatorAjuste, "ZZ9.99")));
            A688Sistema_Prazo = (short)(context.localUtil.CToN( cgiGet( edtSistema_Prazo_Internalname), ",", "."));
            n688Sistema_Prazo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A688Sistema_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(A688Sistema_Prazo), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_PRAZO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A688Sistema_Prazo), "ZZZ9")));
            A687Sistema_Custo = context.localUtil.CToN( cgiGet( edtSistema_Custo_Internalname), ",", ".");
            n687Sistema_Custo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A687Sistema_Custo", StringUtil.LTrim( StringUtil.Str( A687Sistema_Custo, 18, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_CUSTO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A687Sistema_Custo, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            A689Sistema_Esforco = (short)(context.localUtil.CToN( cgiGet( edtSistema_Esforco_Internalname), ",", "."));
            n689Sistema_Esforco = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A689Sistema_Esforco", StringUtil.LTrim( StringUtil.Str( (decimal)(A689Sistema_Esforco), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_ESFORCO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A689Sistema_Esforco), "ZZZ9")));
            A1401Sistema_ImpData = context.localUtil.CToT( cgiGet( edtSistema_ImpData_Internalname), 0);
            n1401Sistema_ImpData = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1401Sistema_ImpData", context.localUtil.TToC( A1401Sistema_ImpData, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_IMPDATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1401Sistema_ImpData, "99/99/99 99:99")));
            A1403Sistema_ImpUserPesNom = StringUtil.Upper( cgiGet( edtSistema_ImpUserPesNom_Internalname));
            n1403Sistema_ImpUserPesNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1403Sistema_ImpUserPesNom", A1403Sistema_ImpUserPesNom);
            /* Read saved values. */
            wcpOA127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA127Sistema_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "SistemaGeneral";
            A699Sistema_Tipo = cgiGet( cmbSistema_Tipo_Internalname);
            n699Sistema_Tipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A699Sistema_Tipo", A699Sistema_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A699Sistema_Tipo, ""))));
            forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A699Sistema_Tipo, ""));
            A686Sistema_FatorAjuste = context.localUtil.CToN( cgiGet( edtSistema_FatorAjuste_Internalname), ",", ".");
            n686Sistema_FatorAjuste = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A686Sistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( A686Sistema_FatorAjuste, 6, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_FATORAJUSTE", GetSecureSignedToken( sPrefix, context.localUtil.Format( A686Sistema_FatorAjuste, "ZZ9.99")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A686Sistema_FatorAjuste, "ZZ9.99");
            A1401Sistema_ImpData = context.localUtil.CToT( cgiGet( edtSistema_ImpData_Internalname), 0);
            n1401Sistema_ImpData = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1401Sistema_ImpData", context.localUtil.TToC( A1401Sistema_ImpData, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SISTEMA_IMPDATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1401Sistema_ImpData, "99/99/99 99:99")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1401Sistema_ImpData, "99/99/99 99:99");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("sistemageneral:[SecurityCheckFailed value for]"+"Sistema_Tipo:"+StringUtil.RTrim( context.localUtil.Format( A699Sistema_Tipo, "")));
               GXUtil.WriteLog("sistemageneral:[SecurityCheckFailed value for]"+"Sistema_FatorAjuste:"+context.localUtil.Format( A686Sistema_FatorAjuste, "ZZ9.99"));
               GXUtil.WriteLog("sistemageneral:[SecurityCheckFailed value for]"+"Sistema_ImpData:"+context.localUtil.Format( A1401Sistema_ImpData, "99/99/99 99:99"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            GXASISTEMAVERSAO_CODIGO_html3I2( ) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E113I2 */
         E113I2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E113I2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Object Property */
         if ( StringUtil.StrCmp(StringUtil.Lower( WebComp_Tabelaatributo_Component), StringUtil.Lower( "SistemaTabelaAtributoWC")) != 0 )
         {
            WebComp_Tabelaatributo = getWebComponent(GetType(), "GeneXus.Programs", "sistematabelaatributowc", new Object[] {context} );
            WebComp_Tabelaatributo.ComponentInit();
            WebComp_Tabelaatributo.Name = "SistemaTabelaAtributoWC";
            WebComp_Tabelaatributo_Component = "SistemaTabelaAtributoWC";
         }
         if ( StringUtil.Len( WebComp_Tabelaatributo_Component) != 0 )
         {
            WebComp_Tabelaatributo.setjustcreated();
            WebComp_Tabelaatributo.componentprepare(new Object[] {(String)sPrefix+"W0116",(String)"",(int)A127Sistema_Codigo});
            WebComp_Tabelaatributo.componentbind(new Object[] {(String)""});
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E123I2( )
      {
         /* Load Routine */
         edtAmbienteTecnologico_Descricao_Link = formatLink("viewambientetecnologico.aspx") + "?" + UrlEncode("" +A351AmbienteTecnologico_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtAmbienteTecnologico_Descricao_Internalname, "Link", edtAmbienteTecnologico_Descricao_Link);
         edtMetodologia_Descricao_Link = formatLink("viewmetodologia.aspx") + "?" + UrlEncode("" +A137Metodologia_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtMetodologia_Descricao_Internalname, "Link", edtMetodologia_Descricao_Link);
         edtSistema_ImpUserPesNom_Link = formatLink("viewusuario.aspx") + "?" + UrlEncode("" +A1399Sistema_ImpUserCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSistema_ImpUserPesNom_Internalname, "Link", edtSistema_ImpUserPesNom_Link);
         edtSistema_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSistema_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistema_Codigo_Visible), 5, 0)));
         if ( ! ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            bttBtninsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtninsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtninsert_Enabled), 5, 0)));
         }
         if ( ! ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            bttBtnencerrar_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnencerrar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnencerrar_Enabled), 5, 0)));
         }
         if ( ! ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            bttBtnupdate_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Enabled), 5, 0)));
         }
         if ( ! ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            bttBtndelete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Enabled), 5, 0)));
         }
         if ( ! ( ! (DateTime.MinValue==A1401Sistema_ImpData) ) )
         {
            edtSistema_ImpData_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSistema_ImpData_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistema_ImpData_Visible), 5, 0)));
            cellSistema_impdata_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellSistema_impdata_cell_Internalname, "Class", cellSistema_impdata_cell_Class);
            cellTextblocksistema_impdata_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellTextblocksistema_impdata_cell_Internalname, "Class", cellTextblocksistema_impdata_cell_Class);
         }
         else
         {
            edtSistema_ImpData_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSistema_ImpData_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistema_ImpData_Visible), 5, 0)));
            cellSistema_impdata_cell_Class = "DataContentCellView";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellSistema_impdata_cell_Internalname, "Class", cellSistema_impdata_cell_Class);
            cellTextblocksistema_impdata_cell_Class = "DataDescriptionCell";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellTextblocksistema_impdata_cell_Internalname, "Class", cellTextblocksistema_impdata_cell_Class);
         }
         if ( ! ( ! (DateTime.MinValue==A1401Sistema_ImpData) ) )
         {
            edtSistema_ImpUserPesNom_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSistema_ImpUserPesNom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistema_ImpUserPesNom_Visible), 5, 0)));
            cellSistema_impuserpesnom_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellSistema_impuserpesnom_cell_Internalname, "Class", cellSistema_impuserpesnom_cell_Class);
            cellTextblocksistema_impuserpesnom_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellTextblocksistema_impuserpesnom_cell_Internalname, "Class", cellTextblocksistema_impuserpesnom_cell_Class);
         }
         else
         {
            edtSistema_ImpUserPesNom_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtSistema_ImpUserPesNom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtSistema_ImpUserPesNom_Visible), 5, 0)));
            cellSistema_impuserpesnom_cell_Class = "DataContentCellView";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellSistema_impuserpesnom_cell_Internalname, "Class", cellSistema_impuserpesnom_cell_Class);
            cellTextblocksistema_impuserpesnom_cell_Class = "DataDescriptionCell";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cellTextblocksistema_impuserpesnom_cell_Internalname, "Class", cellTextblocksistema_impuserpesnom_cell_Class);
         }
         bttBtnencerrar_Visible = (AV6WWPContext.gxTpr_Update&&(StringUtil.StrCmp(A699Sistema_Tipo, "M")==0) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnencerrar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnencerrar_Visible), 5, 0)));
      }

      protected void E133I2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("sistema.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void E143I2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("sistema.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A127Sistema_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E153I2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("sistema.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A127Sistema_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E163I2( )
      {
         /* 'DoFechar' Routine */
         AV12WebSession.Remove("Tabela_Codigo");
         AV12WebSession.Remove("FiltroRecebido");
         context.wjLoc = formatLink("wwsistema.aspx") ;
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV17Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Sistema";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "Sistema_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Sistema_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void E173I2( )
      {
         /* Refresh Routine */
         GXt_decimal1 = AV13Sistema_PF;
         new prc_sistema_pf(context ).execute( ref  A127Sistema_Codigo, ref  GXt_decimal1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
         AV13Sistema_PF = GXt_decimal1;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13Sistema_PF", StringUtil.LTrim( StringUtil.Str( AV13Sistema_PF, 14, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSISTEMA_PF", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV13Sistema_PF, "ZZ,ZZZ,ZZ9.999")));
         AV14Sistema_PFA = (decimal)(AV13Sistema_PF*A686Sistema_FatorAjuste);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14Sistema_PFA", StringUtil.LTrim( StringUtil.Str( AV14Sistema_PFA, 14, 5)));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSISTEMA_PFA", GetSecureSignedToken( sPrefix, context.localUtil.Format( AV14Sistema_PFA, "ZZ,ZZZ,ZZ9.999")));
      }

      protected void wb_table1_2_3I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_3I2( true) ;
         }
         else
         {
            wb_table2_8_3I2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_3I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3I2e( true) ;
         }
         else
         {
            wb_table1_2_3I2e( false) ;
         }
      }

      protected void wb_table2_8_3I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "Table", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_11_3I2( true) ;
         }
         else
         {
            wb_table3_11_3I2( false) ;
         }
         return  ;
      }

      protected void wb_table3_11_3I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table4_110_3I2( true) ;
         }
         else
         {
            wb_table4_110_3I2( false) ;
         }
         return  ;
      }

      protected void wb_table4_110_3I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_3I2e( true) ;
         }
         else
         {
            wb_table2_8_3I2e( false) ;
         }
      }

      protected void wb_table4_110_3I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedunnamedtable2_Internalname, tblTablemergedunnamedtable2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_113_3I2( true) ;
         }
         else
         {
            wb_table5_113_3I2( false) ;
         }
         return  ;
      }

      protected void wb_table5_113_3I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table6_118_3I2( true) ;
         }
         else
         {
            wb_table6_118_3I2( false) ;
         }
         return  ;
      }

      protected void wb_table6_118_3I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_110_3I2e( true) ;
         }
         else
         {
            wb_table4_110_3I2e( false) ;
         }
      }

      protected void wb_table6_118_3I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable3_Internalname, tblUnnamedtable3_Internalname, "", "Table", 0, "", "", 5, 5, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtninsert_Internalname, "", "Novo Sistema", bttBtninsert_Jsonclick, 5, "Inserir", "", StyleString, ClassString, 1, bttBtninsert_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnencerrar_Internalname, "", "Encerrar", bttBtnencerrar_Jsonclick, 7, "Atualizar Baseline", "", StyleString, ClassString, bttBtnencerrar_Visible, bttBtnencerrar_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+"e183i1_client"+"'", TempTags, "", 2, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, bttBtnupdate_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 130,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, bttBtndelete_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'" + sPrefix + "',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_118_3I2e( true) ;
         }
         else
         {
            wb_table6_118_3I2e( false) ;
         }
      }

      protected void wb_table5_113_3I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               GxWebStd.gx_hidden_field( context, sPrefix+"W0116"+"", StringUtil.RTrim( WebComp_Tabelaatributo_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+sPrefix+"gxHTMLWrpW0116"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.Len( WebComp_Tabelaatributo_Component) != 0 )
               {
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldTabelaatributo), StringUtil.Lower( WebComp_Tabelaatributo_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspStartCmp(sPrefix+"gxHTMLWrpW0116"+"");
                  }
                  WebComp_Tabelaatributo.componentdraw();
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldTabelaatributo), StringUtil.Lower( WebComp_Tabelaatributo_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspEndCmp();
                  }
               }
               context.WriteHtmlText( "</div>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_113_3I2e( true) ;
         }
         else
         {
            wb_table5_113_3I2e( false) ;
         }
      }

      protected void wb_table3_11_3I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_nome_Internalname, "Nome", "", "", lblTextblocksistema_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSistema_Nome_Internalname, A416Sistema_Nome, StringUtil.RTrim( context.localUtil.Format( A416Sistema_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistema_Nome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 300, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_sigla_Internalname, "Sigla", "", "", lblTextblocksistema_sigla_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSistema_Sigla_Internalname, StringUtil.RTrim( A129Sistema_Sigla), StringUtil.RTrim( context.localUtil.Format( A129Sistema_Sigla, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistema_Sigla_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 25, "chr", 1, "row", 25, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_descricao_Internalname, "Descri��o", "", "", lblTextblocksistema_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"7\"  class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtSistema_Descricao_Internalname, A128Sistema_Descricao, "", "", 0, 1, 0, 0, 80, "chr", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistemaversao_codigo_Internalname, "Vers�o", "", "", lblTextblocksistemaversao_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynSistemaVersao_Codigo, dynSistemaVersao_Codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0)), 1, dynSistemaVersao_Codigo_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_SistemaGeneral.htm");
            dynSistemaVersao_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1859SistemaVersao_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynSistemaVersao_Codigo_Internalname, "Values", (String)(dynSistemaVersao_Codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_coordenacao_Internalname, "Coordena��o", "", "", lblTextblocksistema_coordenacao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSistema_Coordenacao_Internalname, A513Sistema_Coordenacao, StringUtil.RTrim( context.localUtil.Format( A513Sistema_Coordenacao, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistema_Coordenacao_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_repositorio_Internalname, "Reposit�rio", "", "", lblTextblocksistema_repositorio_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtSistema_Repositorio_Internalname, A2109Sistema_Repositorio, "", "", 0, 1, 0, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "250", -1, "", "", -1, true, "", "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_pf_Internalname, "PF.B", "", "", lblTextblocksistema_pf_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_pf_Internalname, StringUtil.LTrim( StringUtil.NToC( AV13Sistema_PF, 14, 5, ",", "")), ((edtavSistema_pf_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV13Sistema_PF, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV13Sistema_PF, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,52);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_pf_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, edtavSistema_pf_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_pfa_Internalname, "PF.A", "", "", lblTextblocksistema_pfa_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_pfa_Internalname, StringUtil.LTrim( StringUtil.NToC( AV14Sistema_PFA, 14, 5, ",", "")), ((edtavSistema_pfa_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV14Sistema_PFA, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV14Sistema_PFA, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,56);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_pfa_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, edtavSistema_pfa_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_tipo_Internalname, "Tipo", "", "", lblTextblocksistema_tipo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbSistema_Tipo, cmbSistema_Tipo_Internalname, StringUtil.RTrim( A699Sistema_Tipo), 1, cmbSistema_Tipo_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_SistemaGeneral.htm");
            cmbSistema_Tipo.CurrentValue = StringUtil.RTrim( A699Sistema_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbSistema_Tipo_Internalname, "Values", (String)(cmbSistema_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_tecnica_Internalname, "T�cnica", "", "", lblTextblocksistema_tecnica_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbSistema_Tecnica, cmbSistema_Tecnica_Internalname, StringUtil.RTrim( A700Sistema_Tecnica), 1, cmbSistema_Tecnica_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_SistemaGeneral.htm");
            cmbSistema_Tecnica.CurrentValue = StringUtil.RTrim( A700Sistema_Tecnica);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbSistema_Tecnica_Internalname, "Values", (String)(cmbSistema_Tecnica.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockambientetecnologico_descricao_Internalname, "Amb. Tec.", "", "", lblTextblockambientetecnologico_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAmbienteTecnologico_Descricao_Internalname, A352AmbienteTecnologico_Descricao, StringUtil.RTrim( context.localUtil.Format( A352AmbienteTecnologico_Descricao, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtAmbienteTecnologico_Descricao_Link, "", "", "", edtAmbienteTecnologico_Descricao_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmetodologia_descricao_Internalname, "Met.", "", "", lblTextblockmetodologia_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtMetodologia_Descricao_Internalname, A138Metodologia_Descricao, StringUtil.RTrim( context.localUtil.Format( A138Metodologia_Descricao, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtMetodologia_Descricao_Link, "", "", "", edtMetodologia_Descricao_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_fatorajuste_Internalname, "Fator de Ajuste", "", "", lblTextblocksistema_fatorajuste_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSistema_FatorAjuste_Internalname, StringUtil.LTrim( StringUtil.NToC( A686Sistema_FatorAjuste, 6, 2, ",", "")), context.localUtil.Format( A686Sistema_FatorAjuste, "ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistema_FatorAjuste_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 60, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Percentual", "right", false, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_prazo_Internalname, "Prazo", "", "", lblTextblocksistema_prazo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSistema_Prazo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A688Sistema_Prazo), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A688Sistema_Prazo), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistema_Prazo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_custo_Internalname, "Custo", "", "", lblTextblocksistema_custo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSistema_Custo_Internalname, StringUtil.LTrim( StringUtil.NToC( A687Sistema_Custo, 18, 5, ",", "")), context.localUtil.Format( A687Sistema_Custo, "ZZZ,ZZZ,ZZZ,ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistema_Custo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 90, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_esforco_Internalname, "Esforco", "", "", lblTextblocksistema_esforco_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSistema_Esforco_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A689Sistema_Esforco), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A689Sistema_Esforco), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistema_Esforco_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblocksistema_impdata_cell_Internalname+"\"  class='"+cellTextblocksistema_impdata_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_impdata_Internalname, "Importado em", "", "", lblTextblocksistema_impdata_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellSistema_impdata_cell_Internalname+"\"  class='"+cellSistema_impdata_cell_Class+"'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtSistema_ImpData_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtSistema_ImpData_Internalname, context.localUtil.TToC( A1401Sistema_ImpData, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1401Sistema_ImpData, "99/99/99 99:99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistema_ImpData_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", edtSistema_ImpData_Visible, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaGeneral.htm");
            GxWebStd.gx_bitmap( context, edtSistema_ImpData_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtSistema_ImpData_Visible==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_SistemaGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblocksistema_impuserpesnom_cell_Internalname+"\"  class='"+cellTextblocksistema_impuserpesnom_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksistema_impuserpesnom_Internalname, "Importado por", "", "", lblTextblocksistema_impuserpesnom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellSistema_impuserpesnom_cell_Internalname+"\"  class='"+cellSistema_impuserpesnom_cell_Class+"'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSistema_ImpUserPesNom_Internalname, StringUtil.RTrim( A1403Sistema_ImpUserPesNom), StringUtil.RTrim( context.localUtil.Format( A1403Sistema_ImpUserPesNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtSistema_ImpUserPesNom_Link, "", "", "", edtSistema_ImpUserPesNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", edtSistema_ImpUserPesNom_Visible, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_SistemaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_11_3I2e( true) ;
         }
         else
         {
            wb_table3_11_3I2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A127Sistema_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA3I2( ) ;
         WS3I2( ) ;
         WE3I2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA127Sistema_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA3I2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "sistemageneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA3I2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A127Sistema_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
         }
         wcpOA127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA127Sistema_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A127Sistema_Codigo != wcpOA127Sistema_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA127Sistema_Codigo = A127Sistema_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA127Sistema_Codigo = cgiGet( sPrefix+"A127Sistema_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA127Sistema_Codigo) > 0 )
         {
            A127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA127Sistema_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
         }
         else
         {
            A127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A127Sistema_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA3I2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS3I2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS3I2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A127Sistema_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA127Sistema_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A127Sistema_Codigo_CTRL", StringUtil.RTrim( sCtrlA127Sistema_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE3I2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
         if ( ! ( WebComp_Tabelaatributo == null ) )
         {
            WebComp_Tabelaatributo.componentjscripts();
         }
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         if ( ! ( WebComp_Tabelaatributo == null ) )
         {
            if ( StringUtil.Len( WebComp_Tabelaatributo_Component) != 0 )
            {
               WebComp_Tabelaatributo.componentthemes();
            }
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202052118162274");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("sistemageneral.js", "?202052118162274");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocksistema_nome_Internalname = sPrefix+"TEXTBLOCKSISTEMA_NOME";
         edtSistema_Nome_Internalname = sPrefix+"SISTEMA_NOME";
         lblTextblocksistema_sigla_Internalname = sPrefix+"TEXTBLOCKSISTEMA_SIGLA";
         edtSistema_Sigla_Internalname = sPrefix+"SISTEMA_SIGLA";
         lblTextblocksistema_descricao_Internalname = sPrefix+"TEXTBLOCKSISTEMA_DESCRICAO";
         edtSistema_Descricao_Internalname = sPrefix+"SISTEMA_DESCRICAO";
         lblTextblocksistemaversao_codigo_Internalname = sPrefix+"TEXTBLOCKSISTEMAVERSAO_CODIGO";
         dynSistemaVersao_Codigo_Internalname = sPrefix+"SISTEMAVERSAO_CODIGO";
         lblTextblocksistema_coordenacao_Internalname = sPrefix+"TEXTBLOCKSISTEMA_COORDENACAO";
         edtSistema_Coordenacao_Internalname = sPrefix+"SISTEMA_COORDENACAO";
         lblTextblocksistema_repositorio_Internalname = sPrefix+"TEXTBLOCKSISTEMA_REPOSITORIO";
         edtSistema_Repositorio_Internalname = sPrefix+"SISTEMA_REPOSITORIO";
         lblTextblocksistema_pf_Internalname = sPrefix+"TEXTBLOCKSISTEMA_PF";
         edtavSistema_pf_Internalname = sPrefix+"vSISTEMA_PF";
         lblTextblocksistema_pfa_Internalname = sPrefix+"TEXTBLOCKSISTEMA_PFA";
         edtavSistema_pfa_Internalname = sPrefix+"vSISTEMA_PFA";
         lblTextblocksistema_tipo_Internalname = sPrefix+"TEXTBLOCKSISTEMA_TIPO";
         cmbSistema_Tipo_Internalname = sPrefix+"SISTEMA_TIPO";
         lblTextblocksistema_tecnica_Internalname = sPrefix+"TEXTBLOCKSISTEMA_TECNICA";
         cmbSistema_Tecnica_Internalname = sPrefix+"SISTEMA_TECNICA";
         lblTextblockambientetecnologico_descricao_Internalname = sPrefix+"TEXTBLOCKAMBIENTETECNOLOGICO_DESCRICAO";
         edtAmbienteTecnologico_Descricao_Internalname = sPrefix+"AMBIENTETECNOLOGICO_DESCRICAO";
         lblTextblockmetodologia_descricao_Internalname = sPrefix+"TEXTBLOCKMETODOLOGIA_DESCRICAO";
         edtMetodologia_Descricao_Internalname = sPrefix+"METODOLOGIA_DESCRICAO";
         lblTextblocksistema_fatorajuste_Internalname = sPrefix+"TEXTBLOCKSISTEMA_FATORAJUSTE";
         edtSistema_FatorAjuste_Internalname = sPrefix+"SISTEMA_FATORAJUSTE";
         lblTextblocksistema_prazo_Internalname = sPrefix+"TEXTBLOCKSISTEMA_PRAZO";
         edtSistema_Prazo_Internalname = sPrefix+"SISTEMA_PRAZO";
         lblTextblocksistema_custo_Internalname = sPrefix+"TEXTBLOCKSISTEMA_CUSTO";
         edtSistema_Custo_Internalname = sPrefix+"SISTEMA_CUSTO";
         lblTextblocksistema_esforco_Internalname = sPrefix+"TEXTBLOCKSISTEMA_ESFORCO";
         edtSistema_Esforco_Internalname = sPrefix+"SISTEMA_ESFORCO";
         lblTextblocksistema_impdata_Internalname = sPrefix+"TEXTBLOCKSISTEMA_IMPDATA";
         cellTextblocksistema_impdata_cell_Internalname = sPrefix+"TEXTBLOCKSISTEMA_IMPDATA_CELL";
         edtSistema_ImpData_Internalname = sPrefix+"SISTEMA_IMPDATA";
         cellSistema_impdata_cell_Internalname = sPrefix+"SISTEMA_IMPDATA_CELL";
         lblTextblocksistema_impuserpesnom_Internalname = sPrefix+"TEXTBLOCKSISTEMA_IMPUSERPESNOM";
         cellTextblocksistema_impuserpesnom_cell_Internalname = sPrefix+"TEXTBLOCKSISTEMA_IMPUSERPESNOM_CELL";
         edtSistema_ImpUserPesNom_Internalname = sPrefix+"SISTEMA_IMPUSERPESNOM";
         cellSistema_impuserpesnom_cell_Internalname = sPrefix+"SISTEMA_IMPUSERPESNOM_CELL";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         tblUnnamedtable2_Internalname = sPrefix+"UNNAMEDTABLE2";
         bttBtninsert_Internalname = sPrefix+"BTNINSERT";
         bttBtnencerrar_Internalname = sPrefix+"BTNENCERRAR";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         bttBtnfechar_Internalname = sPrefix+"BTNFECHAR";
         tblUnnamedtable3_Internalname = sPrefix+"UNNAMEDTABLE3";
         tblTablemergedunnamedtable2_Internalname = sPrefix+"TABLEMERGEDUNNAMEDTABLE2";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtSistema_Codigo_Internalname = sPrefix+"SISTEMA_CODIGO";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtSistema_ImpUserPesNom_Jsonclick = "";
         cellSistema_impuserpesnom_cell_Class = "";
         cellTextblocksistema_impuserpesnom_cell_Class = "";
         edtSistema_ImpData_Jsonclick = "";
         cellSistema_impdata_cell_Class = "";
         cellTextblocksistema_impdata_cell_Class = "";
         edtSistema_Esforco_Jsonclick = "";
         edtSistema_Custo_Jsonclick = "";
         edtSistema_Prazo_Jsonclick = "";
         edtSistema_FatorAjuste_Jsonclick = "";
         edtMetodologia_Descricao_Jsonclick = "";
         edtAmbienteTecnologico_Descricao_Jsonclick = "";
         cmbSistema_Tecnica_Jsonclick = "";
         cmbSistema_Tipo_Jsonclick = "";
         edtavSistema_pfa_Jsonclick = "";
         edtavSistema_pfa_Enabled = 1;
         edtavSistema_pf_Jsonclick = "";
         edtavSistema_pf_Enabled = 1;
         edtSistema_Coordenacao_Jsonclick = "";
         dynSistemaVersao_Codigo_Jsonclick = "";
         edtSistema_Sigla_Jsonclick = "";
         edtSistema_Nome_Jsonclick = "";
         bttBtndelete_Enabled = 1;
         bttBtnupdate_Enabled = 1;
         bttBtnencerrar_Enabled = 1;
         bttBtnencerrar_Visible = 1;
         bttBtninsert_Enabled = 1;
         edtSistema_ImpUserPesNom_Visible = 1;
         edtSistema_ImpData_Visible = 1;
         edtSistema_ImpUserPesNom_Link = "";
         edtMetodologia_Descricao_Link = "";
         edtAmbienteTecnologico_Descricao_Link = "";
         edtSistema_Codigo_Jsonclick = "";
         edtSistema_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A686Sistema_FatorAjuste',fld:'SISTEMA_FATORAJUSTE',pic:'ZZ9.99',hsh:true,nv:0.0}],oparms:[{av:'AV13Sistema_PF',fld:'vSISTEMA_PF',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'AV14Sistema_PFA',fld:'vSISTEMA_PFA',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0}]}");
         setEventMetadata("'DOINSERT'","{handler:'E133I2',iparms:[{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOENCERRAR'","{handler:'E183I1',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E143I2',iparms:[{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E153I2',iparms:[{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOFECHAR'","{handler:'E163I2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV17Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A416Sistema_Nome = "";
         A129Sistema_Sigla = "";
         A128Sistema_Descricao = "";
         A513Sistema_Coordenacao = "";
         A2109Sistema_Repositorio = "";
         A699Sistema_Tipo = "";
         A700Sistema_Tecnica = "";
         A1401Sistema_ImpData = (DateTime)(DateTime.MinValue);
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         OldTabelaatributo = "";
         WebComp_Tabelaatributo_Component = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H003I2_A1859SistemaVersao_Codigo = new int[1] ;
         H003I2_n1859SistemaVersao_Codigo = new bool[] {false} ;
         H003I2_A1860SistemaVersao_Id = new String[] {""} ;
         H003I3_A1402Sistema_ImpUserPesCod = new int[1] ;
         H003I3_n1402Sistema_ImpUserPesCod = new bool[] {false} ;
         H003I3_A127Sistema_Codigo = new int[1] ;
         H003I3_A351AmbienteTecnologico_Codigo = new int[1] ;
         H003I3_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         H003I3_A137Metodologia_Codigo = new int[1] ;
         H003I3_n137Metodologia_Codigo = new bool[] {false} ;
         H003I3_A1399Sistema_ImpUserCod = new int[1] ;
         H003I3_n1399Sistema_ImpUserCod = new bool[] {false} ;
         H003I3_A1403Sistema_ImpUserPesNom = new String[] {""} ;
         H003I3_n1403Sistema_ImpUserPesNom = new bool[] {false} ;
         H003I3_A1401Sistema_ImpData = new DateTime[] {DateTime.MinValue} ;
         H003I3_n1401Sistema_ImpData = new bool[] {false} ;
         H003I3_A689Sistema_Esforco = new short[1] ;
         H003I3_n689Sistema_Esforco = new bool[] {false} ;
         H003I3_A687Sistema_Custo = new decimal[1] ;
         H003I3_n687Sistema_Custo = new bool[] {false} ;
         H003I3_A688Sistema_Prazo = new short[1] ;
         H003I3_n688Sistema_Prazo = new bool[] {false} ;
         H003I3_A686Sistema_FatorAjuste = new decimal[1] ;
         H003I3_n686Sistema_FatorAjuste = new bool[] {false} ;
         H003I3_A138Metodologia_Descricao = new String[] {""} ;
         H003I3_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         H003I3_A700Sistema_Tecnica = new String[] {""} ;
         H003I3_n700Sistema_Tecnica = new bool[] {false} ;
         H003I3_A699Sistema_Tipo = new String[] {""} ;
         H003I3_n699Sistema_Tipo = new bool[] {false} ;
         H003I3_A2109Sistema_Repositorio = new String[] {""} ;
         H003I3_n2109Sistema_Repositorio = new bool[] {false} ;
         H003I3_A513Sistema_Coordenacao = new String[] {""} ;
         H003I3_n513Sistema_Coordenacao = new bool[] {false} ;
         H003I3_A1859SistemaVersao_Codigo = new int[1] ;
         H003I3_n1859SistemaVersao_Codigo = new bool[] {false} ;
         H003I3_A128Sistema_Descricao = new String[] {""} ;
         H003I3_n128Sistema_Descricao = new bool[] {false} ;
         H003I3_A129Sistema_Sigla = new String[] {""} ;
         H003I3_A416Sistema_Nome = new String[] {""} ;
         A1403Sistema_ImpUserPesNom = "";
         A138Metodologia_Descricao = "";
         A352AmbienteTecnologico_Descricao = "";
         hsh = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV12WebSession = context.GetSession();
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtninsert_Jsonclick = "";
         bttBtnencerrar_Jsonclick = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblocksistema_nome_Jsonclick = "";
         lblTextblocksistema_sigla_Jsonclick = "";
         lblTextblocksistema_descricao_Jsonclick = "";
         lblTextblocksistemaversao_codigo_Jsonclick = "";
         lblTextblocksistema_coordenacao_Jsonclick = "";
         lblTextblocksistema_repositorio_Jsonclick = "";
         lblTextblocksistema_pf_Jsonclick = "";
         lblTextblocksistema_pfa_Jsonclick = "";
         lblTextblocksistema_tipo_Jsonclick = "";
         lblTextblocksistema_tecnica_Jsonclick = "";
         lblTextblockambientetecnologico_descricao_Jsonclick = "";
         lblTextblockmetodologia_descricao_Jsonclick = "";
         lblTextblocksistema_fatorajuste_Jsonclick = "";
         lblTextblocksistema_prazo_Jsonclick = "";
         lblTextblocksistema_custo_Jsonclick = "";
         lblTextblocksistema_esforco_Jsonclick = "";
         lblTextblocksistema_impdata_Jsonclick = "";
         lblTextblocksistema_impuserpesnom_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA127Sistema_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.sistemageneral__default(),
            new Object[][] {
                new Object[] {
               H003I2_A1859SistemaVersao_Codigo, H003I2_A1860SistemaVersao_Id
               }
               , new Object[] {
               H003I3_A1402Sistema_ImpUserPesCod, H003I3_n1402Sistema_ImpUserPesCod, H003I3_A127Sistema_Codigo, H003I3_A351AmbienteTecnologico_Codigo, H003I3_n351AmbienteTecnologico_Codigo, H003I3_A137Metodologia_Codigo, H003I3_n137Metodologia_Codigo, H003I3_A1399Sistema_ImpUserCod, H003I3_n1399Sistema_ImpUserCod, H003I3_A1403Sistema_ImpUserPesNom,
               H003I3_n1403Sistema_ImpUserPesNom, H003I3_A1401Sistema_ImpData, H003I3_n1401Sistema_ImpData, H003I3_A689Sistema_Esforco, H003I3_n689Sistema_Esforco, H003I3_A687Sistema_Custo, H003I3_n687Sistema_Custo, H003I3_A688Sistema_Prazo, H003I3_n688Sistema_Prazo, H003I3_A686Sistema_FatorAjuste,
               H003I3_n686Sistema_FatorAjuste, H003I3_A138Metodologia_Descricao, H003I3_A352AmbienteTecnologico_Descricao, H003I3_A700Sistema_Tecnica, H003I3_n700Sistema_Tecnica, H003I3_A699Sistema_Tipo, H003I3_n699Sistema_Tipo, H003I3_A2109Sistema_Repositorio, H003I3_n2109Sistema_Repositorio, H003I3_A513Sistema_Coordenacao,
               H003I3_n513Sistema_Coordenacao, H003I3_A1859SistemaVersao_Codigo, H003I3_n1859SistemaVersao_Codigo, H003I3_A128Sistema_Descricao, H003I3_n128Sistema_Descricao, H003I3_A129Sistema_Sigla, H003I3_A416Sistema_Nome
               }
            }
         );
         WebComp_Tabelaatributo = new GeneXus.Http.GXNullWebComponent();
         AV17Pgmname = "SistemaGeneral";
         /* GeneXus formulas. */
         AV17Pgmname = "SistemaGeneral";
         context.Gx_err = 0;
         edtavSistema_pf_Enabled = 0;
         edtavSistema_pfa_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A688Sistema_Prazo ;
      private short A689Sistema_Esforco ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A127Sistema_Codigo ;
      private int wcpOA127Sistema_Codigo ;
      private int edtavSistema_pf_Enabled ;
      private int edtavSistema_pfa_Enabled ;
      private int A1859SistemaVersao_Codigo ;
      private int edtSistema_Codigo_Visible ;
      private int gxdynajaxindex ;
      private int A1402Sistema_ImpUserPesCod ;
      private int A351AmbienteTecnologico_Codigo ;
      private int A137Metodologia_Codigo ;
      private int A1399Sistema_ImpUserCod ;
      private int bttBtninsert_Enabled ;
      private int bttBtnencerrar_Enabled ;
      private int bttBtnupdate_Enabled ;
      private int bttBtndelete_Enabled ;
      private int edtSistema_ImpData_Visible ;
      private int edtSistema_ImpUserPesNom_Visible ;
      private int bttBtnencerrar_Visible ;
      private int AV7Sistema_Codigo ;
      private int idxLst ;
      private decimal AV13Sistema_PF ;
      private decimal AV14Sistema_PFA ;
      private decimal A686Sistema_FatorAjuste ;
      private decimal A687Sistema_Custo ;
      private decimal GXt_decimal1 ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV17Pgmname ;
      private String edtavSistema_pf_Internalname ;
      private String edtavSistema_pfa_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A129Sistema_Sigla ;
      private String A699Sistema_Tipo ;
      private String A700Sistema_Tecnica ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String edtSistema_Codigo_Internalname ;
      private String edtSistema_Codigo_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String OldTabelaatributo ;
      private String WebComp_Tabelaatributo_Component ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String A1403Sistema_ImpUserPesNom ;
      private String edtSistema_Nome_Internalname ;
      private String edtSistema_Sigla_Internalname ;
      private String edtSistema_Descricao_Internalname ;
      private String dynSistemaVersao_Codigo_Internalname ;
      private String edtSistema_Coordenacao_Internalname ;
      private String edtSistema_Repositorio_Internalname ;
      private String cmbSistema_Tipo_Internalname ;
      private String cmbSistema_Tecnica_Internalname ;
      private String edtAmbienteTecnologico_Descricao_Internalname ;
      private String edtMetodologia_Descricao_Internalname ;
      private String edtSistema_FatorAjuste_Internalname ;
      private String edtSistema_Prazo_Internalname ;
      private String edtSistema_Custo_Internalname ;
      private String edtSistema_Esforco_Internalname ;
      private String edtSistema_ImpData_Internalname ;
      private String edtSistema_ImpUserPesNom_Internalname ;
      private String hsh ;
      private String edtAmbienteTecnologico_Descricao_Link ;
      private String edtMetodologia_Descricao_Link ;
      private String edtSistema_ImpUserPesNom_Link ;
      private String bttBtninsert_Internalname ;
      private String bttBtnencerrar_Internalname ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String cellSistema_impdata_cell_Class ;
      private String cellSistema_impdata_cell_Internalname ;
      private String cellTextblocksistema_impdata_cell_Class ;
      private String cellTextblocksistema_impdata_cell_Internalname ;
      private String cellSistema_impuserpesnom_cell_Class ;
      private String cellSistema_impuserpesnom_cell_Internalname ;
      private String cellTextblocksistema_impuserpesnom_cell_Class ;
      private String cellTextblocksistema_impuserpesnom_cell_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableattributes_Internalname ;
      private String tblTablemergedunnamedtable2_Internalname ;
      private String tblUnnamedtable3_Internalname ;
      private String TempTags ;
      private String bttBtninsert_Jsonclick ;
      private String bttBtnencerrar_Jsonclick ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblUnnamedtable2_Internalname ;
      private String tblUnnamedtable1_Internalname ;
      private String lblTextblocksistema_nome_Internalname ;
      private String lblTextblocksistema_nome_Jsonclick ;
      private String edtSistema_Nome_Jsonclick ;
      private String lblTextblocksistema_sigla_Internalname ;
      private String lblTextblocksistema_sigla_Jsonclick ;
      private String edtSistema_Sigla_Jsonclick ;
      private String lblTextblocksistema_descricao_Internalname ;
      private String lblTextblocksistema_descricao_Jsonclick ;
      private String lblTextblocksistemaversao_codigo_Internalname ;
      private String lblTextblocksistemaversao_codigo_Jsonclick ;
      private String dynSistemaVersao_Codigo_Jsonclick ;
      private String lblTextblocksistema_coordenacao_Internalname ;
      private String lblTextblocksistema_coordenacao_Jsonclick ;
      private String edtSistema_Coordenacao_Jsonclick ;
      private String lblTextblocksistema_repositorio_Internalname ;
      private String lblTextblocksistema_repositorio_Jsonclick ;
      private String lblTextblocksistema_pf_Internalname ;
      private String lblTextblocksistema_pf_Jsonclick ;
      private String edtavSistema_pf_Jsonclick ;
      private String lblTextblocksistema_pfa_Internalname ;
      private String lblTextblocksistema_pfa_Jsonclick ;
      private String edtavSistema_pfa_Jsonclick ;
      private String lblTextblocksistema_tipo_Internalname ;
      private String lblTextblocksistema_tipo_Jsonclick ;
      private String cmbSistema_Tipo_Jsonclick ;
      private String lblTextblocksistema_tecnica_Internalname ;
      private String lblTextblocksistema_tecnica_Jsonclick ;
      private String cmbSistema_Tecnica_Jsonclick ;
      private String lblTextblockambientetecnologico_descricao_Internalname ;
      private String lblTextblockambientetecnologico_descricao_Jsonclick ;
      private String edtAmbienteTecnologico_Descricao_Jsonclick ;
      private String lblTextblockmetodologia_descricao_Internalname ;
      private String lblTextblockmetodologia_descricao_Jsonclick ;
      private String edtMetodologia_Descricao_Jsonclick ;
      private String lblTextblocksistema_fatorajuste_Internalname ;
      private String lblTextblocksistema_fatorajuste_Jsonclick ;
      private String edtSistema_FatorAjuste_Jsonclick ;
      private String lblTextblocksistema_prazo_Internalname ;
      private String lblTextblocksistema_prazo_Jsonclick ;
      private String edtSistema_Prazo_Jsonclick ;
      private String lblTextblocksistema_custo_Internalname ;
      private String lblTextblocksistema_custo_Jsonclick ;
      private String edtSistema_Custo_Jsonclick ;
      private String lblTextblocksistema_esforco_Internalname ;
      private String lblTextblocksistema_esforco_Jsonclick ;
      private String edtSistema_Esforco_Jsonclick ;
      private String lblTextblocksistema_impdata_Internalname ;
      private String lblTextblocksistema_impdata_Jsonclick ;
      private String edtSistema_ImpData_Jsonclick ;
      private String lblTextblocksistema_impuserpesnom_Internalname ;
      private String lblTextblocksistema_impuserpesnom_Jsonclick ;
      private String edtSistema_ImpUserPesNom_Jsonclick ;
      private String sCtrlA127Sistema_Codigo ;
      private DateTime A1401Sistema_ImpData ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n699Sistema_Tipo ;
      private bool n700Sistema_Tecnica ;
      private bool n1859SistemaVersao_Codigo ;
      private bool n1402Sistema_ImpUserPesCod ;
      private bool n351AmbienteTecnologico_Codigo ;
      private bool n137Metodologia_Codigo ;
      private bool n1399Sistema_ImpUserCod ;
      private bool n1403Sistema_ImpUserPesNom ;
      private bool n1401Sistema_ImpData ;
      private bool n689Sistema_Esforco ;
      private bool n687Sistema_Custo ;
      private bool n688Sistema_Prazo ;
      private bool n686Sistema_FatorAjuste ;
      private bool n2109Sistema_Repositorio ;
      private bool n513Sistema_Coordenacao ;
      private bool n128Sistema_Descricao ;
      private bool returnInSub ;
      private String A128Sistema_Descricao ;
      private String A416Sistema_Nome ;
      private String A513Sistema_Coordenacao ;
      private String A2109Sistema_Repositorio ;
      private String A138Metodologia_Descricao ;
      private String A352AmbienteTecnologico_Descricao ;
      private GXWebComponent WebComp_Tabelaatributo ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynSistemaVersao_Codigo ;
      private GXCombobox cmbSistema_Tipo ;
      private GXCombobox cmbSistema_Tecnica ;
      private IDataStoreProvider pr_default ;
      private int[] H003I2_A1859SistemaVersao_Codigo ;
      private bool[] H003I2_n1859SistemaVersao_Codigo ;
      private String[] H003I2_A1860SistemaVersao_Id ;
      private int[] H003I3_A1402Sistema_ImpUserPesCod ;
      private bool[] H003I3_n1402Sistema_ImpUserPesCod ;
      private int[] H003I3_A127Sistema_Codigo ;
      private int[] H003I3_A351AmbienteTecnologico_Codigo ;
      private bool[] H003I3_n351AmbienteTecnologico_Codigo ;
      private int[] H003I3_A137Metodologia_Codigo ;
      private bool[] H003I3_n137Metodologia_Codigo ;
      private int[] H003I3_A1399Sistema_ImpUserCod ;
      private bool[] H003I3_n1399Sistema_ImpUserCod ;
      private String[] H003I3_A1403Sistema_ImpUserPesNom ;
      private bool[] H003I3_n1403Sistema_ImpUserPesNom ;
      private DateTime[] H003I3_A1401Sistema_ImpData ;
      private bool[] H003I3_n1401Sistema_ImpData ;
      private short[] H003I3_A689Sistema_Esforco ;
      private bool[] H003I3_n689Sistema_Esforco ;
      private decimal[] H003I3_A687Sistema_Custo ;
      private bool[] H003I3_n687Sistema_Custo ;
      private short[] H003I3_A688Sistema_Prazo ;
      private bool[] H003I3_n688Sistema_Prazo ;
      private decimal[] H003I3_A686Sistema_FatorAjuste ;
      private bool[] H003I3_n686Sistema_FatorAjuste ;
      private String[] H003I3_A138Metodologia_Descricao ;
      private String[] H003I3_A352AmbienteTecnologico_Descricao ;
      private String[] H003I3_A700Sistema_Tecnica ;
      private bool[] H003I3_n700Sistema_Tecnica ;
      private String[] H003I3_A699Sistema_Tipo ;
      private bool[] H003I3_n699Sistema_Tipo ;
      private String[] H003I3_A2109Sistema_Repositorio ;
      private bool[] H003I3_n2109Sistema_Repositorio ;
      private String[] H003I3_A513Sistema_Coordenacao ;
      private bool[] H003I3_n513Sistema_Coordenacao ;
      private int[] H003I3_A1859SistemaVersao_Codigo ;
      private bool[] H003I3_n1859SistemaVersao_Codigo ;
      private String[] H003I3_A128Sistema_Descricao ;
      private bool[] H003I3_n128Sistema_Descricao ;
      private String[] H003I3_A129Sistema_Sigla ;
      private String[] H003I3_A416Sistema_Nome ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV12WebSession ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class sistemageneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH003I2 ;
          prmH003I2 = new Object[] {
          } ;
          Object[] prmH003I3 ;
          prmH003I3 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H003I2", "SELECT [SistemaVersao_Codigo], [SistemaVersao_Id] FROM [SistemaVersao] WITH (NOLOCK) ORDER BY [SistemaVersao_Id] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003I2,0,0,true,false )
             ,new CursorDef("H003I3", "SELECT T4.[Usuario_PessoaCod] AS Sistema_ImpUserPesCod, T1.[Sistema_Codigo], T1.[AmbienteTecnologico_Codigo], T1.[Metodologia_Codigo], T1.[Sistema_ImpUserCod] AS Sistema_ImpUserCod, T5.[Pessoa_Nome] AS Sistema_ImpUserPesNom, T1.[Sistema_ImpData], T1.[Sistema_Esforco], T1.[Sistema_Custo], T1.[Sistema_Prazo], T1.[Sistema_FatorAjuste], T3.[Metodologia_Descricao], T2.[AmbienteTecnologico_Descricao], T1.[Sistema_Tecnica], T1.[Sistema_Tipo], T1.[Sistema_Repositorio], T1.[Sistema_Coordenacao], T1.[SistemaVersao_Codigo], T1.[Sistema_Descricao], T1.[Sistema_Sigla], T1.[Sistema_Nome] FROM (((([Sistema] T1 WITH (NOLOCK) LEFT JOIN [AmbienteTecnologico] T2 WITH (NOLOCK) ON T2.[AmbienteTecnologico_Codigo] = T1.[AmbienteTecnologico_Codigo]) LEFT JOIN [Metodologia] T3 WITH (NOLOCK) ON T3.[Metodologia_Codigo] = T1.[Metodologia_Codigo]) LEFT JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T1.[Sistema_ImpUserCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod]) WHERE T1.[Sistema_Codigo] = @Sistema_Codigo ORDER BY T1.[Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003I3,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[11])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((short[]) buf[13])[0] = rslt.getShort(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((short[]) buf[17])[0] = rslt.getShort(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((decimal[]) buf[19])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((String[]) buf[21])[0] = rslt.getVarchar(12) ;
                ((String[]) buf[22])[0] = rslt.getVarchar(13) ;
                ((String[]) buf[23])[0] = rslt.getString(14, 1) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((String[]) buf[25])[0] = rslt.getString(15, 1) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((String[]) buf[27])[0] = rslt.getVarchar(16) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((String[]) buf[29])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(17);
                ((int[]) buf[31])[0] = rslt.getInt(18) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(18);
                ((String[]) buf[33])[0] = rslt.getLongVarchar(19) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(19);
                ((String[]) buf[35])[0] = rslt.getString(20, 25) ;
                ((String[]) buf[36])[0] = rslt.getVarchar(21) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
