/*
               File: ContratanteRedmineWC
        Description: Contratante Redmine WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:26:12.14
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratanteredminewc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratanteredminewc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratanteredminewc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Redmine_ContratanteCod )
      {
         this.A1384Redmine_ContratanteCod = aP0_Redmine_ContratanteCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbRedmine_Secure = new GXCombobox();
         cmbRedmine_Versao = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A1384Redmine_ContratanteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1384Redmine_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1384Redmine_ContratanteCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A1384Redmine_ContratanteCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAKF2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV23Pgmname = "ContratanteRedmineWC";
               context.Gx_err = 0;
               WSKF2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contratante Redmine WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202053021261223");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratanteredminewc.aspx") + "?" + UrlEncode("" +A1384Redmine_ContratanteCod)+"\">") ;
               GxWebStd.gx_hidden_field( context, "_EventName", "");
               GxWebStd.gx_hidden_field( context, "_EventGridId", "");
               GxWebStd.gx_hidden_field( context, "_EventRowId", "");
               context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            }
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA1384Redmine_ContratanteCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA1384Redmine_ContratanteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"REDMINE_CONTRATANTECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1384Redmine_ContratanteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REDMINE_USER", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1905Redmine_User, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REDMINE_KEY", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1383Redmine_Key, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REDMINE_SECURE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1904Redmine_Secure), "9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REDMINE_HOST", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1381Redmine_Host, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REDMINE_URL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1382Redmine_Url, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REDMINE_VERSAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1391Redmine_Versao, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REDMINE_CAMPOSISTEMA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1906Redmine_CampoSistema, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_REDMINE_CAMPOSERVICO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1907Redmine_CampoServico, ""))));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormKF2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratanteredminewc.js", "?202053021261228");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "</form>") ;
            }
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratanteRedmineWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contratante Redmine WC" ;
      }

      protected void WBKF0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratanteredminewc.aspx");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_KF2( true) ;
         }
         else
         {
            wb_table1_2_KF2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_KF2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTKF2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contratante Redmine WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPKF0( ) ;
            }
         }
      }

      protected void WSKF2( )
      {
         STARTKF2( ) ;
         EVTKF2( ) ;
      }

      protected void EVTKF2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPKF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPKF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11KF2 */
                                    E11KF2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPKF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12KF2 */
                                    E12KF2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPKF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPKF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEKF2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormKF2( ) ;
            }
         }
      }

      protected void PAKF2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbRedmine_Secure.Name = "REDMINE_SECURE";
            cmbRedmine_Secure.WebTags = "";
            cmbRedmine_Secure.addItem("0", "http://", 0);
            cmbRedmine_Secure.addItem("1", "https://", 0);
            if ( cmbRedmine_Secure.ItemCount > 0 )
            {
               A1904Redmine_Secure = (short)(NumberUtil.Val( cmbRedmine_Secure.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1904Redmine_Secure), 1, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1904Redmine_Secure", StringUtil.Str( (decimal)(A1904Redmine_Secure), 1, 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REDMINE_SECURE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1904Redmine_Secure), "9")));
            }
            cmbRedmine_Versao.Name = "REDMINE_VERSAO";
            cmbRedmine_Versao.WebTags = "";
            cmbRedmine_Versao.addItem("144", "1.4.4", 0);
            cmbRedmine_Versao.addItem("223", "2.2.3", 0);
            if ( cmbRedmine_Versao.ItemCount > 0 )
            {
               A1391Redmine_Versao = cmbRedmine_Versao.getValidValue(A1391Redmine_Versao);
               n1391Redmine_Versao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1391Redmine_Versao", A1391Redmine_Versao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REDMINE_VERSAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1391Redmine_Versao, ""))));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbRedmine_Secure.ItemCount > 0 )
         {
            A1904Redmine_Secure = (short)(NumberUtil.Val( cmbRedmine_Secure.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1904Redmine_Secure), 1, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1904Redmine_Secure", StringUtil.Str( (decimal)(A1904Redmine_Secure), 1, 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REDMINE_SECURE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1904Redmine_Secure), "9")));
         }
         if ( cmbRedmine_Versao.ItemCount > 0 )
         {
            A1391Redmine_Versao = cmbRedmine_Versao.getValidValue(A1391Redmine_Versao);
            n1391Redmine_Versao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1391Redmine_Versao", A1391Redmine_Versao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REDMINE_VERSAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1391Redmine_Versao, ""))));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFKF2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV23Pgmname = "ContratanteRedmineWC";
         context.Gx_err = 0;
      }

      protected void RFKF2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00KF2 */
            pr_default.execute(0, new Object[] {A1384Redmine_ContratanteCod});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1907Redmine_CampoServico = H00KF2_A1907Redmine_CampoServico[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1907Redmine_CampoServico", A1907Redmine_CampoServico);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REDMINE_CAMPOSERVICO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1907Redmine_CampoServico, ""))));
               n1907Redmine_CampoServico = H00KF2_n1907Redmine_CampoServico[0];
               A1906Redmine_CampoSistema = H00KF2_A1906Redmine_CampoSistema[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1906Redmine_CampoSistema", A1906Redmine_CampoSistema);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REDMINE_CAMPOSISTEMA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1906Redmine_CampoSistema, ""))));
               n1906Redmine_CampoSistema = H00KF2_n1906Redmine_CampoSistema[0];
               A1391Redmine_Versao = H00KF2_A1391Redmine_Versao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1391Redmine_Versao", A1391Redmine_Versao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REDMINE_VERSAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1391Redmine_Versao, ""))));
               n1391Redmine_Versao = H00KF2_n1391Redmine_Versao[0];
               A1382Redmine_Url = H00KF2_A1382Redmine_Url[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1382Redmine_Url", A1382Redmine_Url);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REDMINE_URL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1382Redmine_Url, ""))));
               A1381Redmine_Host = H00KF2_A1381Redmine_Host[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1381Redmine_Host", A1381Redmine_Host);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REDMINE_HOST", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1381Redmine_Host, ""))));
               A1904Redmine_Secure = H00KF2_A1904Redmine_Secure[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1904Redmine_Secure", StringUtil.Str( (decimal)(A1904Redmine_Secure), 1, 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REDMINE_SECURE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1904Redmine_Secure), "9")));
               A1383Redmine_Key = H00KF2_A1383Redmine_Key[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1383Redmine_Key", A1383Redmine_Key);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REDMINE_KEY", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1383Redmine_Key, ""))));
               A1905Redmine_User = H00KF2_A1905Redmine_User[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1905Redmine_User", A1905Redmine_User);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REDMINE_USER", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1905Redmine_User, ""))));
               n1905Redmine_User = H00KF2_n1905Redmine_User[0];
               /* Execute user event: E12KF2 */
               E12KF2 ();
               pr_default.readNext(0);
            }
            pr_default.close(0);
            WBKF0( ) ;
         }
      }

      protected void STRUPKF0( )
      {
         /* Before Start, stand alone formulas. */
         AV23Pgmname = "ContratanteRedmineWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11KF2 */
         E11KF2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A1905Redmine_User = cgiGet( edtRedmine_User_Internalname);
            n1905Redmine_User = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1905Redmine_User", A1905Redmine_User);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REDMINE_USER", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1905Redmine_User, ""))));
            A1383Redmine_Key = cgiGet( edtRedmine_Key_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1383Redmine_Key", A1383Redmine_Key);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REDMINE_KEY", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1383Redmine_Key, ""))));
            cmbRedmine_Secure.CurrentValue = cgiGet( cmbRedmine_Secure_Internalname);
            A1904Redmine_Secure = (short)(NumberUtil.Val( cgiGet( cmbRedmine_Secure_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1904Redmine_Secure", StringUtil.Str( (decimal)(A1904Redmine_Secure), 1, 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REDMINE_SECURE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1904Redmine_Secure), "9")));
            A1381Redmine_Host = cgiGet( edtRedmine_Host_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1381Redmine_Host", A1381Redmine_Host);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REDMINE_HOST", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1381Redmine_Host, ""))));
            A1382Redmine_Url = cgiGet( edtRedmine_Url_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1382Redmine_Url", A1382Redmine_Url);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REDMINE_URL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1382Redmine_Url, ""))));
            cmbRedmine_Versao.CurrentValue = cgiGet( cmbRedmine_Versao_Internalname);
            A1391Redmine_Versao = cgiGet( cmbRedmine_Versao_Internalname);
            n1391Redmine_Versao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1391Redmine_Versao", A1391Redmine_Versao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REDMINE_VERSAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1391Redmine_Versao, ""))));
            A1906Redmine_CampoSistema = cgiGet( edtRedmine_CampoSistema_Internalname);
            n1906Redmine_CampoSistema = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1906Redmine_CampoSistema", A1906Redmine_CampoSistema);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REDMINE_CAMPOSISTEMA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1906Redmine_CampoSistema, ""))));
            A1907Redmine_CampoServico = cgiGet( edtRedmine_CampoServico_Internalname);
            n1907Redmine_CampoServico = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1907Redmine_CampoServico", A1907Redmine_CampoServico);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_REDMINE_CAMPOSERVICO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1907Redmine_CampoServico, ""))));
            /* Read saved values. */
            wcpOA1384Redmine_ContratanteCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1384Redmine_ContratanteCod"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11KF2 */
         E11KF2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11KF2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12KF2( )
      {
         /* Load Routine */
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV23Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Contratante";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "Redmine_ContratanteCod";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV15Redmine_ContratanteCod), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_KF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockredmine_user_Internalname, "Usu�rio", "", "", lblTextblockredmine_user_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteRedmineWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            wb_table2_7_KF2( true) ;
         }
         else
         {
            wb_table2_7_KF2( false) ;
         }
         return  ;
      }

      protected void wb_table2_7_KF2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockredmine_secure_Internalname, "Host", "", "", lblTextblockredmine_secure_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteRedmineWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            wb_table3_19_KF2( true) ;
         }
         else
         {
            wb_table3_19_KF2( false) ;
         }
         return  ;
      }

      protected void wb_table3_19_KF2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockredmine_url_Internalname, "Url", "", "", lblTextblockredmine_url_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteRedmineWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtRedmine_Url_Internalname, StringUtil.RTrim( A1382Redmine_Url), StringUtil.RTrim( context.localUtil.Format( A1382Redmine_Url, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRedmine_Url_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 250, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratanteRedmineWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockredmine_versao_Internalname, "Vers�o", "", "", lblTextblockredmine_versao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteRedmineWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbRedmine_Versao, cmbRedmine_Versao_Internalname, StringUtil.RTrim( A1391Redmine_Versao), 1, cmbRedmine_Versao_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratanteRedmineWC.htm");
            cmbRedmine_Versao.CurrentValue = StringUtil.RTrim( A1391Redmine_Versao);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbRedmine_Versao_Internalname, "Values", (String)(cmbRedmine_Versao.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockredmine_camposistema_Internalname, "Campo de Sistema", "", "", lblTextblockredmine_camposistema_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteRedmineWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            wb_table4_39_KF2( true) ;
         }
         else
         {
            wb_table4_39_KF2( false) ;
         }
         return  ;
      }

      protected void wb_table4_39_KF2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_KF2e( true) ;
         }
         else
         {
            wb_table1_2_KF2e( false) ;
         }
      }

      protected void wb_table4_39_KF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedredmine_camposistema_Internalname, tblTablemergedredmine_camposistema_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtRedmine_CampoSistema_Internalname, StringUtil.RTrim( A1906Redmine_CampoSistema), StringUtil.RTrim( context.localUtil.Format( A1906Redmine_CampoSistema, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRedmine_CampoSistema_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratanteRedmineWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockredmine_camposervico_Internalname, "Campo de Servi�o", "", "", lblTextblockredmine_camposervico_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteRedmineWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtRedmine_CampoServico_Internalname, StringUtil.RTrim( A1907Redmine_CampoServico), StringUtil.RTrim( context.localUtil.Format( A1907Redmine_CampoServico, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRedmine_CampoServico_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratanteRedmineWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_39_KF2e( true) ;
         }
         else
         {
            wb_table4_39_KF2e( false) ;
         }
      }

      protected void wb_table3_19_KF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedredmine_secure_Internalname, tblTablemergedredmine_secure_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbRedmine_Secure, cmbRedmine_Secure_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1904Redmine_Secure), 1, 0)), 1, cmbRedmine_Secure_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContratanteRedmineWC.htm");
            cmbRedmine_Secure.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1904Redmine_Secure), 1, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbRedmine_Secure_Internalname, "Values", (String)(cmbRedmine_Secure.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtRedmine_Host_Internalname, StringUtil.RTrim( A1381Redmine_Host), StringUtil.RTrim( context.localUtil.Format( A1381Redmine_Host, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRedmine_Host_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 250, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratanteRedmineWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_19_KF2e( true) ;
         }
         else
         {
            wb_table3_19_KF2e( false) ;
         }
      }

      protected void wb_table2_7_KF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedredmine_user_Internalname, tblTablemergedredmine_user_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtRedmine_User_Internalname, StringUtil.RTrim( A1905Redmine_User), StringUtil.RTrim( context.localUtil.Format( A1905Redmine_User, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRedmine_User_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratanteRedmineWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockredmine_key_Internalname, "Key", "", "", lblTextblockredmine_key_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratanteRedmineWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtRedmine_Key_Internalname, StringUtil.RTrim( A1383Redmine_Key), StringUtil.RTrim( context.localUtil.Format( A1383Redmine_Key, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRedmine_Key_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 250, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratanteRedmineWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_7_KF2e( true) ;
         }
         else
         {
            wb_table2_7_KF2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A1384Redmine_ContratanteCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1384Redmine_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1384Redmine_ContratanteCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAKF2( ) ;
         WSKF2( ) ;
         WEKF2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA1384Redmine_ContratanteCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAKF2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratanteredminewc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAKF2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A1384Redmine_ContratanteCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1384Redmine_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1384Redmine_ContratanteCod), 6, 0)));
         }
         wcpOA1384Redmine_ContratanteCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1384Redmine_ContratanteCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A1384Redmine_ContratanteCod != wcpOA1384Redmine_ContratanteCod ) ) )
         {
            setjustcreated();
         }
         wcpOA1384Redmine_ContratanteCod = A1384Redmine_ContratanteCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA1384Redmine_ContratanteCod = cgiGet( sPrefix+"A1384Redmine_ContratanteCod_CTRL");
         if ( StringUtil.Len( sCtrlA1384Redmine_ContratanteCod) > 0 )
         {
            A1384Redmine_ContratanteCod = (int)(context.localUtil.CToN( cgiGet( sCtrlA1384Redmine_ContratanteCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1384Redmine_ContratanteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1384Redmine_ContratanteCod), 6, 0)));
         }
         else
         {
            A1384Redmine_ContratanteCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A1384Redmine_ContratanteCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAKF2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSKF2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSKF2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A1384Redmine_ContratanteCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1384Redmine_ContratanteCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA1384Redmine_ContratanteCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A1384Redmine_ContratanteCod_CTRL", StringUtil.RTrim( sCtrlA1384Redmine_ContratanteCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEKF2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202053021261297");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("contratanteredminewc.js", "?202053021261298");
         }
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockredmine_user_Internalname = sPrefix+"TEXTBLOCKREDMINE_USER";
         edtRedmine_User_Internalname = sPrefix+"REDMINE_USER";
         lblTextblockredmine_key_Internalname = sPrefix+"TEXTBLOCKREDMINE_KEY";
         edtRedmine_Key_Internalname = sPrefix+"REDMINE_KEY";
         tblTablemergedredmine_user_Internalname = sPrefix+"TABLEMERGEDREDMINE_USER";
         lblTextblockredmine_secure_Internalname = sPrefix+"TEXTBLOCKREDMINE_SECURE";
         cmbRedmine_Secure_Internalname = sPrefix+"REDMINE_SECURE";
         edtRedmine_Host_Internalname = sPrefix+"REDMINE_HOST";
         tblTablemergedredmine_secure_Internalname = sPrefix+"TABLEMERGEDREDMINE_SECURE";
         lblTextblockredmine_url_Internalname = sPrefix+"TEXTBLOCKREDMINE_URL";
         edtRedmine_Url_Internalname = sPrefix+"REDMINE_URL";
         lblTextblockredmine_versao_Internalname = sPrefix+"TEXTBLOCKREDMINE_VERSAO";
         cmbRedmine_Versao_Internalname = sPrefix+"REDMINE_VERSAO";
         lblTextblockredmine_camposistema_Internalname = sPrefix+"TEXTBLOCKREDMINE_CAMPOSISTEMA";
         edtRedmine_CampoSistema_Internalname = sPrefix+"REDMINE_CAMPOSISTEMA";
         lblTextblockredmine_camposervico_Internalname = sPrefix+"TEXTBLOCKREDMINE_CAMPOSERVICO";
         edtRedmine_CampoServico_Internalname = sPrefix+"REDMINE_CAMPOSERVICO";
         tblTablemergedredmine_camposistema_Internalname = sPrefix+"TABLEMERGEDREDMINE_CAMPOSISTEMA";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtRedmine_Key_Jsonclick = "";
         edtRedmine_User_Jsonclick = "";
         edtRedmine_Host_Jsonclick = "";
         cmbRedmine_Secure_Jsonclick = "";
         edtRedmine_CampoServico_Jsonclick = "";
         edtRedmine_CampoSistema_Jsonclick = "";
         cmbRedmine_Versao_Jsonclick = "";
         edtRedmine_Url_Jsonclick = "";
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV23Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A1905Redmine_User = "";
         A1383Redmine_Key = "";
         A1381Redmine_Host = "";
         A1382Redmine_Url = "";
         A1391Redmine_Versao = "";
         A1906Redmine_CampoSistema = "";
         A1907Redmine_CampoServico = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00KF2_A1380Redmine_Codigo = new int[1] ;
         H00KF2_A1384Redmine_ContratanteCod = new int[1] ;
         H00KF2_A1907Redmine_CampoServico = new String[] {""} ;
         H00KF2_n1907Redmine_CampoServico = new bool[] {false} ;
         H00KF2_A1906Redmine_CampoSistema = new String[] {""} ;
         H00KF2_n1906Redmine_CampoSistema = new bool[] {false} ;
         H00KF2_A1391Redmine_Versao = new String[] {""} ;
         H00KF2_n1391Redmine_Versao = new bool[] {false} ;
         H00KF2_A1382Redmine_Url = new String[] {""} ;
         H00KF2_A1381Redmine_Host = new String[] {""} ;
         H00KF2_A1904Redmine_Secure = new short[1] ;
         H00KF2_A1383Redmine_Key = new String[] {""} ;
         H00KF2_A1905Redmine_User = new String[] {""} ;
         H00KF2_n1905Redmine_User = new bool[] {false} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         lblTextblockredmine_user_Jsonclick = "";
         lblTextblockredmine_secure_Jsonclick = "";
         lblTextblockredmine_url_Jsonclick = "";
         lblTextblockredmine_versao_Jsonclick = "";
         lblTextblockredmine_camposistema_Jsonclick = "";
         lblTextblockredmine_camposervico_Jsonclick = "";
         lblTextblockredmine_key_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA1384Redmine_ContratanteCod = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratanteredminewc__default(),
            new Object[][] {
                new Object[] {
               H00KF2_A1380Redmine_Codigo, H00KF2_A1384Redmine_ContratanteCod, H00KF2_A1907Redmine_CampoServico, H00KF2_n1907Redmine_CampoServico, H00KF2_A1906Redmine_CampoSistema, H00KF2_n1906Redmine_CampoSistema, H00KF2_A1391Redmine_Versao, H00KF2_n1391Redmine_Versao, H00KF2_A1382Redmine_Url, H00KF2_A1381Redmine_Host,
               H00KF2_A1904Redmine_Secure, H00KF2_A1383Redmine_Key, H00KF2_A1905Redmine_User, H00KF2_n1905Redmine_User
               }
            }
         );
         AV23Pgmname = "ContratanteRedmineWC";
         /* GeneXus formulas. */
         AV23Pgmname = "ContratanteRedmineWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short nGXWrapped ;
      private short A1904Redmine_Secure ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private int A1384Redmine_ContratanteCod ;
      private int wcpOA1384Redmine_ContratanteCod ;
      private int AV15Redmine_ContratanteCod ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV23Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A1905Redmine_User ;
      private String A1383Redmine_Key ;
      private String A1381Redmine_Host ;
      private String A1382Redmine_Url ;
      private String A1391Redmine_Versao ;
      private String A1906Redmine_CampoSistema ;
      private String A1907Redmine_CampoServico ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String edtRedmine_User_Internalname ;
      private String edtRedmine_Key_Internalname ;
      private String cmbRedmine_Secure_Internalname ;
      private String edtRedmine_Host_Internalname ;
      private String edtRedmine_Url_Internalname ;
      private String cmbRedmine_Versao_Internalname ;
      private String edtRedmine_CampoSistema_Internalname ;
      private String edtRedmine_CampoServico_Internalname ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String lblTextblockredmine_user_Internalname ;
      private String lblTextblockredmine_user_Jsonclick ;
      private String lblTextblockredmine_secure_Internalname ;
      private String lblTextblockredmine_secure_Jsonclick ;
      private String lblTextblockredmine_url_Internalname ;
      private String lblTextblockredmine_url_Jsonclick ;
      private String edtRedmine_Url_Jsonclick ;
      private String lblTextblockredmine_versao_Internalname ;
      private String lblTextblockredmine_versao_Jsonclick ;
      private String cmbRedmine_Versao_Jsonclick ;
      private String lblTextblockredmine_camposistema_Internalname ;
      private String lblTextblockredmine_camposistema_Jsonclick ;
      private String tblTablemergedredmine_camposistema_Internalname ;
      private String edtRedmine_CampoSistema_Jsonclick ;
      private String lblTextblockredmine_camposervico_Internalname ;
      private String lblTextblockredmine_camposervico_Jsonclick ;
      private String edtRedmine_CampoServico_Jsonclick ;
      private String tblTablemergedredmine_secure_Internalname ;
      private String cmbRedmine_Secure_Jsonclick ;
      private String edtRedmine_Host_Jsonclick ;
      private String tblTablemergedredmine_user_Internalname ;
      private String edtRedmine_User_Jsonclick ;
      private String lblTextblockredmine_key_Internalname ;
      private String lblTextblockredmine_key_Jsonclick ;
      private String edtRedmine_Key_Jsonclick ;
      private String sCtrlA1384Redmine_ContratanteCod ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1391Redmine_Versao ;
      private bool n1907Redmine_CampoServico ;
      private bool n1906Redmine_CampoSistema ;
      private bool n1905Redmine_User ;
      private bool returnInSub ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbRedmine_Secure ;
      private GXCombobox cmbRedmine_Versao ;
      private IDataStoreProvider pr_default ;
      private int[] H00KF2_A1380Redmine_Codigo ;
      private int[] H00KF2_A1384Redmine_ContratanteCod ;
      private String[] H00KF2_A1907Redmine_CampoServico ;
      private bool[] H00KF2_n1907Redmine_CampoServico ;
      private String[] H00KF2_A1906Redmine_CampoSistema ;
      private bool[] H00KF2_n1906Redmine_CampoSistema ;
      private String[] H00KF2_A1391Redmine_Versao ;
      private bool[] H00KF2_n1391Redmine_Versao ;
      private String[] H00KF2_A1382Redmine_Url ;
      private String[] H00KF2_A1381Redmine_Host ;
      private short[] H00KF2_A1904Redmine_Secure ;
      private String[] H00KF2_A1383Redmine_Key ;
      private String[] H00KF2_A1905Redmine_User ;
      private bool[] H00KF2_n1905Redmine_User ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class contratanteredminewc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00KF2 ;
          prmH00KF2 = new Object[] {
          new Object[] {"@Redmine_ContratanteCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00KF2", "SELECT [Redmine_Codigo], [Redmine_ContratanteCod], [Redmine_CampoServico], [Redmine_CampoSistema], [Redmine_Versao], [Redmine_Url], [Redmine_Host], [Redmine_Secure], [Redmine_Key], [Redmine_User] FROM [Redmine] WITH (NOLOCK) WHERE [Redmine_ContratanteCod] = @Redmine_ContratanteCod ORDER BY [Redmine_ContratanteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KF2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 20) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 100) ;
                ((String[]) buf[9])[0] = rslt.getString(7, 100) ;
                ((short[]) buf[10])[0] = rslt.getShort(8) ;
                ((String[]) buf[11])[0] = rslt.getString(9, 50) ;
                ((String[]) buf[12])[0] = rslt.getString(10, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(10);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
