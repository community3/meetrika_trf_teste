/*
               File: GetWWContagemFilterData
        Description: Get WWContagem Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:4:38.77
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwcontagemfilterdata : GXProcedure
   {
      public getwwcontagemfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwcontagemfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV77DDOName = aP0_DDOName;
         this.AV75SearchTxt = aP1_SearchTxt;
         this.AV76SearchTxtTo = aP2_SearchTxtTo;
         this.AV81OptionsJson = "" ;
         this.AV84OptionsDescJson = "" ;
         this.AV86OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV81OptionsJson;
         aP4_OptionsDescJson=this.AV84OptionsDescJson;
         aP5_OptionIndexesJson=this.AV86OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV77DDOName = aP0_DDOName;
         this.AV75SearchTxt = aP1_SearchTxt;
         this.AV76SearchTxtTo = aP2_SearchTxtTo;
         this.AV81OptionsJson = "" ;
         this.AV84OptionsDescJson = "" ;
         this.AV86OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV81OptionsJson;
         aP4_OptionsDescJson=this.AV84OptionsDescJson;
         aP5_OptionIndexesJson=this.AV86OptionIndexesJson;
         return AV86OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwcontagemfilterdata objgetwwcontagemfilterdata;
         objgetwwcontagemfilterdata = new getwwcontagemfilterdata();
         objgetwwcontagemfilterdata.AV77DDOName = aP0_DDOName;
         objgetwwcontagemfilterdata.AV75SearchTxt = aP1_SearchTxt;
         objgetwwcontagemfilterdata.AV76SearchTxtTo = aP2_SearchTxtTo;
         objgetwwcontagemfilterdata.AV81OptionsJson = "" ;
         objgetwwcontagemfilterdata.AV84OptionsDescJson = "" ;
         objgetwwcontagemfilterdata.AV86OptionIndexesJson = "" ;
         objgetwwcontagemfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwcontagemfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwcontagemfilterdata);
         aP3_OptionsJson=this.AV81OptionsJson;
         aP4_OptionsDescJson=this.AV84OptionsDescJson;
         aP5_OptionIndexesJson=this.AV86OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwcontagemfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV80Options = (IGxCollection)(new GxSimpleCollection());
         AV83OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV85OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV77DDOName), "DDO_CONTAGEM_AREATRABALHODES") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEM_AREATRABALHODESOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV77DDOName), "DDO_CONTAGEM_NOTAS") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEM_NOTASOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV77DDOName), "DDO_CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEM_USUARIOCONTADORPESSOANOMOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV77DDOName), "DDO_CONTAGEM_DEMANDA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEM_DEMANDAOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV77DDOName), "DDO_CONTAGEM_LINK") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEM_LINKOPTIONS' */
            S161 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV77DDOName), "DDO_CONTAGEM_SISTEMASIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEM_SISTEMASIGLAOPTIONS' */
            S171 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV81OptionsJson = AV80Options.ToJSonString(false);
         AV84OptionsDescJson = AV83OptionsDesc.ToJSonString(false);
         AV86OptionIndexesJson = AV85OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV88Session.Get("WWContagemGridState"), "") == 0 )
         {
            AV90GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWContagemGridState"), "");
         }
         else
         {
            AV90GridState.FromXml(AV88Session.Get("WWContagemGridState"), "");
         }
         AV112GXV1 = 1;
         while ( AV112GXV1 <= AV90GridState.gxTpr_Filtervalues.Count )
         {
            AV91GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV90GridState.gxTpr_Filtervalues.Item(AV112GXV1));
            if ( StringUtil.StrCmp(AV91GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_CODIGO") == 0 )
            {
               AV10TFContagem_Codigo = (int)(NumberUtil.Val( AV91GridStateFilterValue.gxTpr_Value, "."));
               AV11TFContagem_Codigo_To = (int)(NumberUtil.Val( AV91GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV91GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_AREATRABALHOCOD") == 0 )
            {
               AV12TFContagem_AreaTrabalhoCod = (int)(NumberUtil.Val( AV91GridStateFilterValue.gxTpr_Value, "."));
               AV13TFContagem_AreaTrabalhoCod_To = (int)(NumberUtil.Val( AV91GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV91GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_CONTRATADACOD") == 0 )
            {
               AV14TFContagem_ContratadaCod = (int)(NumberUtil.Val( AV91GridStateFilterValue.gxTpr_Value, "."));
               AV15TFContagem_ContratadaCod_To = (int)(NumberUtil.Val( AV91GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV91GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_DATACRIACAO") == 0 )
            {
               AV16TFContagem_DataCriacao = context.localUtil.CToD( AV91GridStateFilterValue.gxTpr_Value, 2);
               AV17TFContagem_DataCriacao_To = context.localUtil.CToD( AV91GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV91GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_AREATRABALHODES") == 0 )
            {
               AV18TFContagem_AreaTrabalhoDes = AV91GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV91GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_AREATRABALHODES_SEL") == 0 )
            {
               AV19TFContagem_AreaTrabalhoDes_Sel = AV91GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV91GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_TECNICA_SEL") == 0 )
            {
               AV20TFContagem_Tecnica_SelsJson = AV91GridStateFilterValue.gxTpr_Value;
               AV21TFContagem_Tecnica_Sels.FromJSonString(AV20TFContagem_Tecnica_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV91GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_TIPO_SEL") == 0 )
            {
               AV22TFContagem_Tipo_SelsJson = AV91GridStateFilterValue.gxTpr_Value;
               AV23TFContagem_Tipo_Sels.FromJSonString(AV22TFContagem_Tipo_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV91GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_NOTAS") == 0 )
            {
               AV24TFContagem_Notas = AV91GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV91GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_NOTAS_SEL") == 0 )
            {
               AV25TFContagem_Notas_Sel = AV91GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV91GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_PFB") == 0 )
            {
               AV26TFContagem_PFB = NumberUtil.Val( AV91GridStateFilterValue.gxTpr_Value, ".");
               AV27TFContagem_PFB_To = NumberUtil.Val( AV91GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV91GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_PFL") == 0 )
            {
               AV28TFContagem_PFL = NumberUtil.Val( AV91GridStateFilterValue.gxTpr_Value, ".");
               AV29TFContagem_PFL_To = NumberUtil.Val( AV91GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV91GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_DIVERGENCIA") == 0 )
            {
               AV30TFContagem_Divergencia = NumberUtil.Val( AV91GridStateFilterValue.gxTpr_Value, ".");
               AV31TFContagem_Divergencia_To = NumberUtil.Val( AV91GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV91GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_USUARIOCONTADORCOD") == 0 )
            {
               AV32TFContagem_UsuarioContadorCod = (int)(NumberUtil.Val( AV91GridStateFilterValue.gxTpr_Value, "."));
               AV33TFContagem_UsuarioContadorCod_To = (int)(NumberUtil.Val( AV91GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV91GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_USUARIOCONTADORPESSOACOD") == 0 )
            {
               AV34TFContagem_UsuarioContadorPessoaCod = (int)(NumberUtil.Val( AV91GridStateFilterValue.gxTpr_Value, "."));
               AV35TFContagem_UsuarioContadorPessoaCod_To = (int)(NumberUtil.Val( AV91GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV91GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_USUARIOCONTADORPESSOANOM") == 0 )
            {
               AV36TFContagem_UsuarioContadorPessoaNom = AV91GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV91GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_USUARIOCONTADORPESSOANOM_SEL") == 0 )
            {
               AV37TFContagem_UsuarioContadorPessoaNom_Sel = AV91GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV91GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_STATUS_SEL") == 0 )
            {
               AV38TFContagem_Status_SelsJson = AV91GridStateFilterValue.gxTpr_Value;
               AV39TFContagem_Status_Sels.FromJSonString(AV38TFContagem_Status_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV91GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_DEMANDA") == 0 )
            {
               AV40TFContagem_Demanda = AV91GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV91GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_DEMANDA_SEL") == 0 )
            {
               AV41TFContagem_Demanda_Sel = AV91GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV91GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_LINK") == 0 )
            {
               AV42TFContagem_Link = AV91GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV91GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_LINK_SEL") == 0 )
            {
               AV43TFContagem_Link_Sel = AV91GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV91GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_FATOR") == 0 )
            {
               AV44TFContagem_Fator = NumberUtil.Val( AV91GridStateFilterValue.gxTpr_Value, ".");
               AV45TFContagem_Fator_To = NumberUtil.Val( AV91GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV91GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_DEFLATOR") == 0 )
            {
               AV46TFContagem_Deflator = NumberUtil.Val( AV91GridStateFilterValue.gxTpr_Value, ".");
               AV47TFContagem_Deflator_To = NumberUtil.Val( AV91GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV91GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_SISTEMACOD") == 0 )
            {
               AV48TFContagem_SistemaCod = (int)(NumberUtil.Val( AV91GridStateFilterValue.gxTpr_Value, "."));
               AV49TFContagem_SistemaCod_To = (int)(NumberUtil.Val( AV91GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV91GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_SISTEMASIGLA") == 0 )
            {
               AV50TFContagem_SistemaSigla = AV91GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV91GridStateFilterValue.gxTpr_Name, "TFCONTAGEM_SISTEMASIGLA_SEL") == 0 )
            {
               AV51TFContagem_SistemaSigla_Sel = AV91GridStateFilterValue.gxTpr_Value;
            }
            AV112GXV1 = (int)(AV112GXV1+1);
         }
         if ( AV90GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV92GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV90GridState.gxTpr_Dynamicfilters.Item(1));
            AV93DynamicFiltersSelector1 = AV92GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV93DynamicFiltersSelector1, "CONTAGEM_CONTRATADACOD") == 0 )
            {
               AV94DynamicFiltersOperator1 = AV92GridStateDynamicFilter.gxTpr_Operator;
               AV95Contagem_ContratadaCod1 = (int)(NumberUtil.Val( AV92GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector1, "CONTAGEM_AREATRABALHODES") == 0 )
            {
               AV94DynamicFiltersOperator1 = AV92GridStateDynamicFilter.gxTpr_Operator;
               AV96Contagem_AreaTrabalhoDes1 = AV92GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV93DynamicFiltersSelector1, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 )
            {
               AV94DynamicFiltersOperator1 = AV92GridStateDynamicFilter.gxTpr_Operator;
               AV97Contagem_UsuarioContadorPessoaNom1 = AV92GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV90GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV98DynamicFiltersEnabled2 = true;
               AV92GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV90GridState.gxTpr_Dynamicfilters.Item(2));
               AV99DynamicFiltersSelector2 = AV92GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV99DynamicFiltersSelector2, "CONTAGEM_CONTRATADACOD") == 0 )
               {
                  AV100DynamicFiltersOperator2 = AV92GridStateDynamicFilter.gxTpr_Operator;
                  AV101Contagem_ContratadaCod2 = (int)(NumberUtil.Val( AV92GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV99DynamicFiltersSelector2, "CONTAGEM_AREATRABALHODES") == 0 )
               {
                  AV100DynamicFiltersOperator2 = AV92GridStateDynamicFilter.gxTpr_Operator;
                  AV102Contagem_AreaTrabalhoDes2 = AV92GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV99DynamicFiltersSelector2, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 )
               {
                  AV100DynamicFiltersOperator2 = AV92GridStateDynamicFilter.gxTpr_Operator;
                  AV103Contagem_UsuarioContadorPessoaNom2 = AV92GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV90GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV104DynamicFiltersEnabled3 = true;
                  AV92GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV90GridState.gxTpr_Dynamicfilters.Item(3));
                  AV105DynamicFiltersSelector3 = AV92GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV105DynamicFiltersSelector3, "CONTAGEM_CONTRATADACOD") == 0 )
                  {
                     AV106DynamicFiltersOperator3 = AV92GridStateDynamicFilter.gxTpr_Operator;
                     AV107Contagem_ContratadaCod3 = (int)(NumberUtil.Val( AV92GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV105DynamicFiltersSelector3, "CONTAGEM_AREATRABALHODES") == 0 )
                  {
                     AV106DynamicFiltersOperator3 = AV92GridStateDynamicFilter.gxTpr_Operator;
                     AV108Contagem_AreaTrabalhoDes3 = AV92GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV105DynamicFiltersSelector3, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 )
                  {
                     AV106DynamicFiltersOperator3 = AV92GridStateDynamicFilter.gxTpr_Operator;
                     AV109Contagem_UsuarioContadorPessoaNom3 = AV92GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTAGEM_AREATRABALHODESOPTIONS' Routine */
         AV18TFContagem_AreaTrabalhoDes = AV75SearchTxt;
         AV19TFContagem_AreaTrabalhoDes_Sel = "";
         AV114WWContagemDS_1_Dynamicfiltersselector1 = AV93DynamicFiltersSelector1;
         AV115WWContagemDS_2_Dynamicfiltersoperator1 = AV94DynamicFiltersOperator1;
         AV116WWContagemDS_3_Contagem_contratadacod1 = AV95Contagem_ContratadaCod1;
         AV117WWContagemDS_4_Contagem_areatrabalhodes1 = AV96Contagem_AreaTrabalhoDes1;
         AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 = AV97Contagem_UsuarioContadorPessoaNom1;
         AV119WWContagemDS_6_Dynamicfiltersenabled2 = AV98DynamicFiltersEnabled2;
         AV120WWContagemDS_7_Dynamicfiltersselector2 = AV99DynamicFiltersSelector2;
         AV121WWContagemDS_8_Dynamicfiltersoperator2 = AV100DynamicFiltersOperator2;
         AV122WWContagemDS_9_Contagem_contratadacod2 = AV101Contagem_ContratadaCod2;
         AV123WWContagemDS_10_Contagem_areatrabalhodes2 = AV102Contagem_AreaTrabalhoDes2;
         AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 = AV103Contagem_UsuarioContadorPessoaNom2;
         AV125WWContagemDS_12_Dynamicfiltersenabled3 = AV104DynamicFiltersEnabled3;
         AV126WWContagemDS_13_Dynamicfiltersselector3 = AV105DynamicFiltersSelector3;
         AV127WWContagemDS_14_Dynamicfiltersoperator3 = AV106DynamicFiltersOperator3;
         AV128WWContagemDS_15_Contagem_contratadacod3 = AV107Contagem_ContratadaCod3;
         AV129WWContagemDS_16_Contagem_areatrabalhodes3 = AV108Contagem_AreaTrabalhoDes3;
         AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 = AV109Contagem_UsuarioContadorPessoaNom3;
         AV131WWContagemDS_18_Tfcontagem_codigo = AV10TFContagem_Codigo;
         AV132WWContagemDS_19_Tfcontagem_codigo_to = AV11TFContagem_Codigo_To;
         AV133WWContagemDS_20_Tfcontagem_areatrabalhocod = AV12TFContagem_AreaTrabalhoCod;
         AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to = AV13TFContagem_AreaTrabalhoCod_To;
         AV135WWContagemDS_22_Tfcontagem_contratadacod = AV14TFContagem_ContratadaCod;
         AV136WWContagemDS_23_Tfcontagem_contratadacod_to = AV15TFContagem_ContratadaCod_To;
         AV137WWContagemDS_24_Tfcontagem_datacriacao = AV16TFContagem_DataCriacao;
         AV138WWContagemDS_25_Tfcontagem_datacriacao_to = AV17TFContagem_DataCriacao_To;
         AV139WWContagemDS_26_Tfcontagem_areatrabalhodes = AV18TFContagem_AreaTrabalhoDes;
         AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel = AV19TFContagem_AreaTrabalhoDes_Sel;
         AV141WWContagemDS_28_Tfcontagem_tecnica_sels = AV21TFContagem_Tecnica_Sels;
         AV142WWContagemDS_29_Tfcontagem_tipo_sels = AV23TFContagem_Tipo_Sels;
         AV143WWContagemDS_30_Tfcontagem_notas = AV24TFContagem_Notas;
         AV144WWContagemDS_31_Tfcontagem_notas_sel = AV25TFContagem_Notas_Sel;
         AV145WWContagemDS_32_Tfcontagem_pfb = AV26TFContagem_PFB;
         AV146WWContagemDS_33_Tfcontagem_pfb_to = AV27TFContagem_PFB_To;
         AV147WWContagemDS_34_Tfcontagem_pfl = AV28TFContagem_PFL;
         AV148WWContagemDS_35_Tfcontagem_pfl_to = AV29TFContagem_PFL_To;
         AV149WWContagemDS_36_Tfcontagem_divergencia = AV30TFContagem_Divergencia;
         AV150WWContagemDS_37_Tfcontagem_divergencia_to = AV31TFContagem_Divergencia_To;
         AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod = AV32TFContagem_UsuarioContadorCod;
         AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to = AV33TFContagem_UsuarioContadorCod_To;
         AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod = AV34TFContagem_UsuarioContadorPessoaCod;
         AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to = AV35TFContagem_UsuarioContadorPessoaCod_To;
         AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom = AV36TFContagem_UsuarioContadorPessoaNom;
         AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel = AV37TFContagem_UsuarioContadorPessoaNom_Sel;
         AV157WWContagemDS_44_Tfcontagem_status_sels = AV39TFContagem_Status_Sels;
         AV158WWContagemDS_45_Tfcontagem_demanda = AV40TFContagem_Demanda;
         AV159WWContagemDS_46_Tfcontagem_demanda_sel = AV41TFContagem_Demanda_Sel;
         AV160WWContagemDS_47_Tfcontagem_link = AV42TFContagem_Link;
         AV161WWContagemDS_48_Tfcontagem_link_sel = AV43TFContagem_Link_Sel;
         AV162WWContagemDS_49_Tfcontagem_fator = AV44TFContagem_Fator;
         AV163WWContagemDS_50_Tfcontagem_fator_to = AV45TFContagem_Fator_To;
         AV164WWContagemDS_51_Tfcontagem_deflator = AV46TFContagem_Deflator;
         AV165WWContagemDS_52_Tfcontagem_deflator_to = AV47TFContagem_Deflator_To;
         AV166WWContagemDS_53_Tfcontagem_sistemacod = AV48TFContagem_SistemaCod;
         AV167WWContagemDS_54_Tfcontagem_sistemacod_to = AV49TFContagem_SistemaCod_To;
         AV168WWContagemDS_55_Tfcontagem_sistemasigla = AV50TFContagem_SistemaSigla;
         AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel = AV51TFContagem_SistemaSigla_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A195Contagem_Tecnica ,
                                              AV141WWContagemDS_28_Tfcontagem_tecnica_sels ,
                                              A196Contagem_Tipo ,
                                              AV142WWContagemDS_29_Tfcontagem_tipo_sels ,
                                              A262Contagem_Status ,
                                              AV157WWContagemDS_44_Tfcontagem_status_sels ,
                                              AV114WWContagemDS_1_Dynamicfiltersselector1 ,
                                              AV115WWContagemDS_2_Dynamicfiltersoperator1 ,
                                              AV116WWContagemDS_3_Contagem_contratadacod1 ,
                                              AV117WWContagemDS_4_Contagem_areatrabalhodes1 ,
                                              AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 ,
                                              AV119WWContagemDS_6_Dynamicfiltersenabled2 ,
                                              AV120WWContagemDS_7_Dynamicfiltersselector2 ,
                                              AV121WWContagemDS_8_Dynamicfiltersoperator2 ,
                                              AV122WWContagemDS_9_Contagem_contratadacod2 ,
                                              AV123WWContagemDS_10_Contagem_areatrabalhodes2 ,
                                              AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 ,
                                              AV125WWContagemDS_12_Dynamicfiltersenabled3 ,
                                              AV126WWContagemDS_13_Dynamicfiltersselector3 ,
                                              AV127WWContagemDS_14_Dynamicfiltersoperator3 ,
                                              AV128WWContagemDS_15_Contagem_contratadacod3 ,
                                              AV129WWContagemDS_16_Contagem_areatrabalhodes3 ,
                                              AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 ,
                                              AV131WWContagemDS_18_Tfcontagem_codigo ,
                                              AV132WWContagemDS_19_Tfcontagem_codigo_to ,
                                              AV133WWContagemDS_20_Tfcontagem_areatrabalhocod ,
                                              AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to ,
                                              AV135WWContagemDS_22_Tfcontagem_contratadacod ,
                                              AV136WWContagemDS_23_Tfcontagem_contratadacod_to ,
                                              AV137WWContagemDS_24_Tfcontagem_datacriacao ,
                                              AV138WWContagemDS_25_Tfcontagem_datacriacao_to ,
                                              AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel ,
                                              AV139WWContagemDS_26_Tfcontagem_areatrabalhodes ,
                                              AV141WWContagemDS_28_Tfcontagem_tecnica_sels.Count ,
                                              AV142WWContagemDS_29_Tfcontagem_tipo_sels.Count ,
                                              AV144WWContagemDS_31_Tfcontagem_notas_sel ,
                                              AV143WWContagemDS_30_Tfcontagem_notas ,
                                              AV145WWContagemDS_32_Tfcontagem_pfb ,
                                              AV146WWContagemDS_33_Tfcontagem_pfb_to ,
                                              AV147WWContagemDS_34_Tfcontagem_pfl ,
                                              AV148WWContagemDS_35_Tfcontagem_pfl_to ,
                                              AV149WWContagemDS_36_Tfcontagem_divergencia ,
                                              AV150WWContagemDS_37_Tfcontagem_divergencia_to ,
                                              AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod ,
                                              AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to ,
                                              AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod ,
                                              AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to ,
                                              AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel ,
                                              AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom ,
                                              AV157WWContagemDS_44_Tfcontagem_status_sels.Count ,
                                              AV159WWContagemDS_46_Tfcontagem_demanda_sel ,
                                              AV158WWContagemDS_45_Tfcontagem_demanda ,
                                              AV161WWContagemDS_48_Tfcontagem_link_sel ,
                                              AV160WWContagemDS_47_Tfcontagem_link ,
                                              AV162WWContagemDS_49_Tfcontagem_fator ,
                                              AV163WWContagemDS_50_Tfcontagem_fator_to ,
                                              AV164WWContagemDS_51_Tfcontagem_deflator ,
                                              AV165WWContagemDS_52_Tfcontagem_deflator_to ,
                                              AV166WWContagemDS_53_Tfcontagem_sistemacod ,
                                              AV167WWContagemDS_54_Tfcontagem_sistemacod_to ,
                                              AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel ,
                                              AV168WWContagemDS_55_Tfcontagem_sistemasigla ,
                                              A1118Contagem_ContratadaCod ,
                                              A194Contagem_AreaTrabalhoDes ,
                                              A215Contagem_UsuarioContadorPessoaNom ,
                                              A192Contagem_Codigo ,
                                              A193Contagem_AreaTrabalhoCod ,
                                              A197Contagem_DataCriacao ,
                                              A1059Contagem_Notas ,
                                              A943Contagem_PFB ,
                                              A944Contagem_PFL ,
                                              A1119Contagem_Divergencia ,
                                              A213Contagem_UsuarioContadorCod ,
                                              A214Contagem_UsuarioContadorPessoaCod ,
                                              A945Contagem_Demanda ,
                                              A946Contagem_Link ,
                                              A947Contagem_Fator ,
                                              A1117Contagem_Deflator ,
                                              A940Contagem_SistemaCod ,
                                              A941Contagem_SistemaSigla },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV117WWContagemDS_4_Contagem_areatrabalhodes1 = StringUtil.Concat( StringUtil.RTrim( AV117WWContagemDS_4_Contagem_areatrabalhodes1), "%", "");
         lV117WWContagemDS_4_Contagem_areatrabalhodes1 = StringUtil.Concat( StringUtil.RTrim( AV117WWContagemDS_4_Contagem_areatrabalhodes1), "%", "");
         lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1), 100, "%");
         lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1), 100, "%");
         lV123WWContagemDS_10_Contagem_areatrabalhodes2 = StringUtil.Concat( StringUtil.RTrim( AV123WWContagemDS_10_Contagem_areatrabalhodes2), "%", "");
         lV123WWContagemDS_10_Contagem_areatrabalhodes2 = StringUtil.Concat( StringUtil.RTrim( AV123WWContagemDS_10_Contagem_areatrabalhodes2), "%", "");
         lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2), 100, "%");
         lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2), 100, "%");
         lV129WWContagemDS_16_Contagem_areatrabalhodes3 = StringUtil.Concat( StringUtil.RTrim( AV129WWContagemDS_16_Contagem_areatrabalhodes3), "%", "");
         lV129WWContagemDS_16_Contagem_areatrabalhodes3 = StringUtil.Concat( StringUtil.RTrim( AV129WWContagemDS_16_Contagem_areatrabalhodes3), "%", "");
         lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3), 100, "%");
         lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3), 100, "%");
         lV139WWContagemDS_26_Tfcontagem_areatrabalhodes = StringUtil.Concat( StringUtil.RTrim( AV139WWContagemDS_26_Tfcontagem_areatrabalhodes), "%", "");
         lV143WWContagemDS_30_Tfcontagem_notas = StringUtil.Concat( StringUtil.RTrim( AV143WWContagemDS_30_Tfcontagem_notas), "%", "");
         lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom = StringUtil.PadR( StringUtil.RTrim( AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom), 100, "%");
         lV158WWContagemDS_45_Tfcontagem_demanda = StringUtil.Concat( StringUtil.RTrim( AV158WWContagemDS_45_Tfcontagem_demanda), "%", "");
         lV160WWContagemDS_47_Tfcontagem_link = StringUtil.Concat( StringUtil.RTrim( AV160WWContagemDS_47_Tfcontagem_link), "%", "");
         lV168WWContagemDS_55_Tfcontagem_sistemasigla = StringUtil.PadR( StringUtil.RTrim( AV168WWContagemDS_55_Tfcontagem_sistemasigla), 25, "%");
         /* Using cursor P00HS2 */
         pr_default.execute(0, new Object[] {AV116WWContagemDS_3_Contagem_contratadacod1, AV116WWContagemDS_3_Contagem_contratadacod1, AV116WWContagemDS_3_Contagem_contratadacod1, lV117WWContagemDS_4_Contagem_areatrabalhodes1, lV117WWContagemDS_4_Contagem_areatrabalhodes1, lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1, lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1, AV122WWContagemDS_9_Contagem_contratadacod2, AV122WWContagemDS_9_Contagem_contratadacod2, AV122WWContagemDS_9_Contagem_contratadacod2, lV123WWContagemDS_10_Contagem_areatrabalhodes2, lV123WWContagemDS_10_Contagem_areatrabalhodes2, lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2, lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2, AV128WWContagemDS_15_Contagem_contratadacod3, AV128WWContagemDS_15_Contagem_contratadacod3, AV128WWContagemDS_15_Contagem_contratadacod3, lV129WWContagemDS_16_Contagem_areatrabalhodes3, lV129WWContagemDS_16_Contagem_areatrabalhodes3, lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3, lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3, AV131WWContagemDS_18_Tfcontagem_codigo, AV132WWContagemDS_19_Tfcontagem_codigo_to, AV133WWContagemDS_20_Tfcontagem_areatrabalhocod, AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to, AV135WWContagemDS_22_Tfcontagem_contratadacod, AV136WWContagemDS_23_Tfcontagem_contratadacod_to, AV137WWContagemDS_24_Tfcontagem_datacriacao, AV138WWContagemDS_25_Tfcontagem_datacriacao_to, lV139WWContagemDS_26_Tfcontagem_areatrabalhodes, AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel, lV143WWContagemDS_30_Tfcontagem_notas, AV144WWContagemDS_31_Tfcontagem_notas_sel, AV145WWContagemDS_32_Tfcontagem_pfb, AV146WWContagemDS_33_Tfcontagem_pfb_to, AV147WWContagemDS_34_Tfcontagem_pfl, AV148WWContagemDS_35_Tfcontagem_pfl_to, AV149WWContagemDS_36_Tfcontagem_divergencia, AV150WWContagemDS_37_Tfcontagem_divergencia_to, AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod, AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to, AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod, AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to, lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom, AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel, lV158WWContagemDS_45_Tfcontagem_demanda, AV159WWContagemDS_46_Tfcontagem_demanda_sel, lV160WWContagemDS_47_Tfcontagem_link, AV161WWContagemDS_48_Tfcontagem_link_sel, AV162WWContagemDS_49_Tfcontagem_fator, AV163WWContagemDS_50_Tfcontagem_fator_to, AV164WWContagemDS_51_Tfcontagem_deflator, AV165WWContagemDS_52_Tfcontagem_deflator_to, AV166WWContagemDS_53_Tfcontagem_sistemacod, AV167WWContagemDS_54_Tfcontagem_sistemacod_to, lV168WWContagemDS_55_Tfcontagem_sistemasigla, AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKHS2 = false;
            A193Contagem_AreaTrabalhoCod = P00HS2_A193Contagem_AreaTrabalhoCod[0];
            A941Contagem_SistemaSigla = P00HS2_A941Contagem_SistemaSigla[0];
            n941Contagem_SistemaSigla = P00HS2_n941Contagem_SistemaSigla[0];
            A940Contagem_SistemaCod = P00HS2_A940Contagem_SistemaCod[0];
            n940Contagem_SistemaCod = P00HS2_n940Contagem_SistemaCod[0];
            A1117Contagem_Deflator = P00HS2_A1117Contagem_Deflator[0];
            n1117Contagem_Deflator = P00HS2_n1117Contagem_Deflator[0];
            A947Contagem_Fator = P00HS2_A947Contagem_Fator[0];
            n947Contagem_Fator = P00HS2_n947Contagem_Fator[0];
            A946Contagem_Link = P00HS2_A946Contagem_Link[0];
            n946Contagem_Link = P00HS2_n946Contagem_Link[0];
            A945Contagem_Demanda = P00HS2_A945Contagem_Demanda[0];
            n945Contagem_Demanda = P00HS2_n945Contagem_Demanda[0];
            A262Contagem_Status = P00HS2_A262Contagem_Status[0];
            n262Contagem_Status = P00HS2_n262Contagem_Status[0];
            A214Contagem_UsuarioContadorPessoaCod = P00HS2_A214Contagem_UsuarioContadorPessoaCod[0];
            n214Contagem_UsuarioContadorPessoaCod = P00HS2_n214Contagem_UsuarioContadorPessoaCod[0];
            A213Contagem_UsuarioContadorCod = P00HS2_A213Contagem_UsuarioContadorCod[0];
            n213Contagem_UsuarioContadorCod = P00HS2_n213Contagem_UsuarioContadorCod[0];
            A1119Contagem_Divergencia = P00HS2_A1119Contagem_Divergencia[0];
            n1119Contagem_Divergencia = P00HS2_n1119Contagem_Divergencia[0];
            A944Contagem_PFL = P00HS2_A944Contagem_PFL[0];
            n944Contagem_PFL = P00HS2_n944Contagem_PFL[0];
            A943Contagem_PFB = P00HS2_A943Contagem_PFB[0];
            n943Contagem_PFB = P00HS2_n943Contagem_PFB[0];
            A1059Contagem_Notas = P00HS2_A1059Contagem_Notas[0];
            n1059Contagem_Notas = P00HS2_n1059Contagem_Notas[0];
            A196Contagem_Tipo = P00HS2_A196Contagem_Tipo[0];
            n196Contagem_Tipo = P00HS2_n196Contagem_Tipo[0];
            A195Contagem_Tecnica = P00HS2_A195Contagem_Tecnica[0];
            n195Contagem_Tecnica = P00HS2_n195Contagem_Tecnica[0];
            A197Contagem_DataCriacao = P00HS2_A197Contagem_DataCriacao[0];
            A192Contagem_Codigo = P00HS2_A192Contagem_Codigo[0];
            A215Contagem_UsuarioContadorPessoaNom = P00HS2_A215Contagem_UsuarioContadorPessoaNom[0];
            n215Contagem_UsuarioContadorPessoaNom = P00HS2_n215Contagem_UsuarioContadorPessoaNom[0];
            A194Contagem_AreaTrabalhoDes = P00HS2_A194Contagem_AreaTrabalhoDes[0];
            n194Contagem_AreaTrabalhoDes = P00HS2_n194Contagem_AreaTrabalhoDes[0];
            A1118Contagem_ContratadaCod = P00HS2_A1118Contagem_ContratadaCod[0];
            n1118Contagem_ContratadaCod = P00HS2_n1118Contagem_ContratadaCod[0];
            A194Contagem_AreaTrabalhoDes = P00HS2_A194Contagem_AreaTrabalhoDes[0];
            n194Contagem_AreaTrabalhoDes = P00HS2_n194Contagem_AreaTrabalhoDes[0];
            A941Contagem_SistemaSigla = P00HS2_A941Contagem_SistemaSigla[0];
            n941Contagem_SistemaSigla = P00HS2_n941Contagem_SistemaSigla[0];
            A214Contagem_UsuarioContadorPessoaCod = P00HS2_A214Contagem_UsuarioContadorPessoaCod[0];
            n214Contagem_UsuarioContadorPessoaCod = P00HS2_n214Contagem_UsuarioContadorPessoaCod[0];
            A215Contagem_UsuarioContadorPessoaNom = P00HS2_A215Contagem_UsuarioContadorPessoaNom[0];
            n215Contagem_UsuarioContadorPessoaNom = P00HS2_n215Contagem_UsuarioContadorPessoaNom[0];
            AV87count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00HS2_A193Contagem_AreaTrabalhoCod[0] == A193Contagem_AreaTrabalhoCod ) )
            {
               BRKHS2 = false;
               A192Contagem_Codigo = P00HS2_A192Contagem_Codigo[0];
               AV87count = (long)(AV87count+1);
               BRKHS2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A194Contagem_AreaTrabalhoDes)) )
            {
               AV79Option = A194Contagem_AreaTrabalhoDes;
               AV78InsertIndex = 1;
               while ( ( AV78InsertIndex <= AV80Options.Count ) && ( StringUtil.StrCmp(((String)AV80Options.Item(AV78InsertIndex)), AV79Option) < 0 ) )
               {
                  AV78InsertIndex = (int)(AV78InsertIndex+1);
               }
               AV80Options.Add(AV79Option, AV78InsertIndex);
               AV85OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV87count), "Z,ZZZ,ZZZ,ZZ9")), AV78InsertIndex);
            }
            if ( AV80Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKHS2 )
            {
               BRKHS2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTAGEM_NOTASOPTIONS' Routine */
         AV24TFContagem_Notas = AV75SearchTxt;
         AV25TFContagem_Notas_Sel = "";
         AV114WWContagemDS_1_Dynamicfiltersselector1 = AV93DynamicFiltersSelector1;
         AV115WWContagemDS_2_Dynamicfiltersoperator1 = AV94DynamicFiltersOperator1;
         AV116WWContagemDS_3_Contagem_contratadacod1 = AV95Contagem_ContratadaCod1;
         AV117WWContagemDS_4_Contagem_areatrabalhodes1 = AV96Contagem_AreaTrabalhoDes1;
         AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 = AV97Contagem_UsuarioContadorPessoaNom1;
         AV119WWContagemDS_6_Dynamicfiltersenabled2 = AV98DynamicFiltersEnabled2;
         AV120WWContagemDS_7_Dynamicfiltersselector2 = AV99DynamicFiltersSelector2;
         AV121WWContagemDS_8_Dynamicfiltersoperator2 = AV100DynamicFiltersOperator2;
         AV122WWContagemDS_9_Contagem_contratadacod2 = AV101Contagem_ContratadaCod2;
         AV123WWContagemDS_10_Contagem_areatrabalhodes2 = AV102Contagem_AreaTrabalhoDes2;
         AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 = AV103Contagem_UsuarioContadorPessoaNom2;
         AV125WWContagemDS_12_Dynamicfiltersenabled3 = AV104DynamicFiltersEnabled3;
         AV126WWContagemDS_13_Dynamicfiltersselector3 = AV105DynamicFiltersSelector3;
         AV127WWContagemDS_14_Dynamicfiltersoperator3 = AV106DynamicFiltersOperator3;
         AV128WWContagemDS_15_Contagem_contratadacod3 = AV107Contagem_ContratadaCod3;
         AV129WWContagemDS_16_Contagem_areatrabalhodes3 = AV108Contagem_AreaTrabalhoDes3;
         AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 = AV109Contagem_UsuarioContadorPessoaNom3;
         AV131WWContagemDS_18_Tfcontagem_codigo = AV10TFContagem_Codigo;
         AV132WWContagemDS_19_Tfcontagem_codigo_to = AV11TFContagem_Codigo_To;
         AV133WWContagemDS_20_Tfcontagem_areatrabalhocod = AV12TFContagem_AreaTrabalhoCod;
         AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to = AV13TFContagem_AreaTrabalhoCod_To;
         AV135WWContagemDS_22_Tfcontagem_contratadacod = AV14TFContagem_ContratadaCod;
         AV136WWContagemDS_23_Tfcontagem_contratadacod_to = AV15TFContagem_ContratadaCod_To;
         AV137WWContagemDS_24_Tfcontagem_datacriacao = AV16TFContagem_DataCriacao;
         AV138WWContagemDS_25_Tfcontagem_datacriacao_to = AV17TFContagem_DataCriacao_To;
         AV139WWContagemDS_26_Tfcontagem_areatrabalhodes = AV18TFContagem_AreaTrabalhoDes;
         AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel = AV19TFContagem_AreaTrabalhoDes_Sel;
         AV141WWContagemDS_28_Tfcontagem_tecnica_sels = AV21TFContagem_Tecnica_Sels;
         AV142WWContagemDS_29_Tfcontagem_tipo_sels = AV23TFContagem_Tipo_Sels;
         AV143WWContagemDS_30_Tfcontagem_notas = AV24TFContagem_Notas;
         AV144WWContagemDS_31_Tfcontagem_notas_sel = AV25TFContagem_Notas_Sel;
         AV145WWContagemDS_32_Tfcontagem_pfb = AV26TFContagem_PFB;
         AV146WWContagemDS_33_Tfcontagem_pfb_to = AV27TFContagem_PFB_To;
         AV147WWContagemDS_34_Tfcontagem_pfl = AV28TFContagem_PFL;
         AV148WWContagemDS_35_Tfcontagem_pfl_to = AV29TFContagem_PFL_To;
         AV149WWContagemDS_36_Tfcontagem_divergencia = AV30TFContagem_Divergencia;
         AV150WWContagemDS_37_Tfcontagem_divergencia_to = AV31TFContagem_Divergencia_To;
         AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod = AV32TFContagem_UsuarioContadorCod;
         AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to = AV33TFContagem_UsuarioContadorCod_To;
         AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod = AV34TFContagem_UsuarioContadorPessoaCod;
         AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to = AV35TFContagem_UsuarioContadorPessoaCod_To;
         AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom = AV36TFContagem_UsuarioContadorPessoaNom;
         AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel = AV37TFContagem_UsuarioContadorPessoaNom_Sel;
         AV157WWContagemDS_44_Tfcontagem_status_sels = AV39TFContagem_Status_Sels;
         AV158WWContagemDS_45_Tfcontagem_demanda = AV40TFContagem_Demanda;
         AV159WWContagemDS_46_Tfcontagem_demanda_sel = AV41TFContagem_Demanda_Sel;
         AV160WWContagemDS_47_Tfcontagem_link = AV42TFContagem_Link;
         AV161WWContagemDS_48_Tfcontagem_link_sel = AV43TFContagem_Link_Sel;
         AV162WWContagemDS_49_Tfcontagem_fator = AV44TFContagem_Fator;
         AV163WWContagemDS_50_Tfcontagem_fator_to = AV45TFContagem_Fator_To;
         AV164WWContagemDS_51_Tfcontagem_deflator = AV46TFContagem_Deflator;
         AV165WWContagemDS_52_Tfcontagem_deflator_to = AV47TFContagem_Deflator_To;
         AV166WWContagemDS_53_Tfcontagem_sistemacod = AV48TFContagem_SistemaCod;
         AV167WWContagemDS_54_Tfcontagem_sistemacod_to = AV49TFContagem_SistemaCod_To;
         AV168WWContagemDS_55_Tfcontagem_sistemasigla = AV50TFContagem_SistemaSigla;
         AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel = AV51TFContagem_SistemaSigla_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A195Contagem_Tecnica ,
                                              AV141WWContagemDS_28_Tfcontagem_tecnica_sels ,
                                              A196Contagem_Tipo ,
                                              AV142WWContagemDS_29_Tfcontagem_tipo_sels ,
                                              A262Contagem_Status ,
                                              AV157WWContagemDS_44_Tfcontagem_status_sels ,
                                              AV114WWContagemDS_1_Dynamicfiltersselector1 ,
                                              AV115WWContagemDS_2_Dynamicfiltersoperator1 ,
                                              AV116WWContagemDS_3_Contagem_contratadacod1 ,
                                              AV117WWContagemDS_4_Contagem_areatrabalhodes1 ,
                                              AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 ,
                                              AV119WWContagemDS_6_Dynamicfiltersenabled2 ,
                                              AV120WWContagemDS_7_Dynamicfiltersselector2 ,
                                              AV121WWContagemDS_8_Dynamicfiltersoperator2 ,
                                              AV122WWContagemDS_9_Contagem_contratadacod2 ,
                                              AV123WWContagemDS_10_Contagem_areatrabalhodes2 ,
                                              AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 ,
                                              AV125WWContagemDS_12_Dynamicfiltersenabled3 ,
                                              AV126WWContagemDS_13_Dynamicfiltersselector3 ,
                                              AV127WWContagemDS_14_Dynamicfiltersoperator3 ,
                                              AV128WWContagemDS_15_Contagem_contratadacod3 ,
                                              AV129WWContagemDS_16_Contagem_areatrabalhodes3 ,
                                              AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 ,
                                              AV131WWContagemDS_18_Tfcontagem_codigo ,
                                              AV132WWContagemDS_19_Tfcontagem_codigo_to ,
                                              AV133WWContagemDS_20_Tfcontagem_areatrabalhocod ,
                                              AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to ,
                                              AV135WWContagemDS_22_Tfcontagem_contratadacod ,
                                              AV136WWContagemDS_23_Tfcontagem_contratadacod_to ,
                                              AV137WWContagemDS_24_Tfcontagem_datacriacao ,
                                              AV138WWContagemDS_25_Tfcontagem_datacriacao_to ,
                                              AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel ,
                                              AV139WWContagemDS_26_Tfcontagem_areatrabalhodes ,
                                              AV141WWContagemDS_28_Tfcontagem_tecnica_sels.Count ,
                                              AV142WWContagemDS_29_Tfcontagem_tipo_sels.Count ,
                                              AV144WWContagemDS_31_Tfcontagem_notas_sel ,
                                              AV143WWContagemDS_30_Tfcontagem_notas ,
                                              AV145WWContagemDS_32_Tfcontagem_pfb ,
                                              AV146WWContagemDS_33_Tfcontagem_pfb_to ,
                                              AV147WWContagemDS_34_Tfcontagem_pfl ,
                                              AV148WWContagemDS_35_Tfcontagem_pfl_to ,
                                              AV149WWContagemDS_36_Tfcontagem_divergencia ,
                                              AV150WWContagemDS_37_Tfcontagem_divergencia_to ,
                                              AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod ,
                                              AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to ,
                                              AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod ,
                                              AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to ,
                                              AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel ,
                                              AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom ,
                                              AV157WWContagemDS_44_Tfcontagem_status_sels.Count ,
                                              AV159WWContagemDS_46_Tfcontagem_demanda_sel ,
                                              AV158WWContagemDS_45_Tfcontagem_demanda ,
                                              AV161WWContagemDS_48_Tfcontagem_link_sel ,
                                              AV160WWContagemDS_47_Tfcontagem_link ,
                                              AV162WWContagemDS_49_Tfcontagem_fator ,
                                              AV163WWContagemDS_50_Tfcontagem_fator_to ,
                                              AV164WWContagemDS_51_Tfcontagem_deflator ,
                                              AV165WWContagemDS_52_Tfcontagem_deflator_to ,
                                              AV166WWContagemDS_53_Tfcontagem_sistemacod ,
                                              AV167WWContagemDS_54_Tfcontagem_sistemacod_to ,
                                              AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel ,
                                              AV168WWContagemDS_55_Tfcontagem_sistemasigla ,
                                              A1118Contagem_ContratadaCod ,
                                              A194Contagem_AreaTrabalhoDes ,
                                              A215Contagem_UsuarioContadorPessoaNom ,
                                              A192Contagem_Codigo ,
                                              A193Contagem_AreaTrabalhoCod ,
                                              A197Contagem_DataCriacao ,
                                              A1059Contagem_Notas ,
                                              A943Contagem_PFB ,
                                              A944Contagem_PFL ,
                                              A1119Contagem_Divergencia ,
                                              A213Contagem_UsuarioContadorCod ,
                                              A214Contagem_UsuarioContadorPessoaCod ,
                                              A945Contagem_Demanda ,
                                              A946Contagem_Link ,
                                              A947Contagem_Fator ,
                                              A1117Contagem_Deflator ,
                                              A940Contagem_SistemaCod ,
                                              A941Contagem_SistemaSigla },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV117WWContagemDS_4_Contagem_areatrabalhodes1 = StringUtil.Concat( StringUtil.RTrim( AV117WWContagemDS_4_Contagem_areatrabalhodes1), "%", "");
         lV117WWContagemDS_4_Contagem_areatrabalhodes1 = StringUtil.Concat( StringUtil.RTrim( AV117WWContagemDS_4_Contagem_areatrabalhodes1), "%", "");
         lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1), 100, "%");
         lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1), 100, "%");
         lV123WWContagemDS_10_Contagem_areatrabalhodes2 = StringUtil.Concat( StringUtil.RTrim( AV123WWContagemDS_10_Contagem_areatrabalhodes2), "%", "");
         lV123WWContagemDS_10_Contagem_areatrabalhodes2 = StringUtil.Concat( StringUtil.RTrim( AV123WWContagemDS_10_Contagem_areatrabalhodes2), "%", "");
         lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2), 100, "%");
         lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2), 100, "%");
         lV129WWContagemDS_16_Contagem_areatrabalhodes3 = StringUtil.Concat( StringUtil.RTrim( AV129WWContagemDS_16_Contagem_areatrabalhodes3), "%", "");
         lV129WWContagemDS_16_Contagem_areatrabalhodes3 = StringUtil.Concat( StringUtil.RTrim( AV129WWContagemDS_16_Contagem_areatrabalhodes3), "%", "");
         lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3), 100, "%");
         lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3), 100, "%");
         lV139WWContagemDS_26_Tfcontagem_areatrabalhodes = StringUtil.Concat( StringUtil.RTrim( AV139WWContagemDS_26_Tfcontagem_areatrabalhodes), "%", "");
         lV143WWContagemDS_30_Tfcontagem_notas = StringUtil.Concat( StringUtil.RTrim( AV143WWContagemDS_30_Tfcontagem_notas), "%", "");
         lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom = StringUtil.PadR( StringUtil.RTrim( AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom), 100, "%");
         lV158WWContagemDS_45_Tfcontagem_demanda = StringUtil.Concat( StringUtil.RTrim( AV158WWContagemDS_45_Tfcontagem_demanda), "%", "");
         lV160WWContagemDS_47_Tfcontagem_link = StringUtil.Concat( StringUtil.RTrim( AV160WWContagemDS_47_Tfcontagem_link), "%", "");
         lV168WWContagemDS_55_Tfcontagem_sistemasigla = StringUtil.PadR( StringUtil.RTrim( AV168WWContagemDS_55_Tfcontagem_sistemasigla), 25, "%");
         /* Using cursor P00HS3 */
         pr_default.execute(1, new Object[] {AV116WWContagemDS_3_Contagem_contratadacod1, AV116WWContagemDS_3_Contagem_contratadacod1, AV116WWContagemDS_3_Contagem_contratadacod1, lV117WWContagemDS_4_Contagem_areatrabalhodes1, lV117WWContagemDS_4_Contagem_areatrabalhodes1, lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1, lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1, AV122WWContagemDS_9_Contagem_contratadacod2, AV122WWContagemDS_9_Contagem_contratadacod2, AV122WWContagemDS_9_Contagem_contratadacod2, lV123WWContagemDS_10_Contagem_areatrabalhodes2, lV123WWContagemDS_10_Contagem_areatrabalhodes2, lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2, lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2, AV128WWContagemDS_15_Contagem_contratadacod3, AV128WWContagemDS_15_Contagem_contratadacod3, AV128WWContagemDS_15_Contagem_contratadacod3, lV129WWContagemDS_16_Contagem_areatrabalhodes3, lV129WWContagemDS_16_Contagem_areatrabalhodes3, lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3, lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3, AV131WWContagemDS_18_Tfcontagem_codigo, AV132WWContagemDS_19_Tfcontagem_codigo_to, AV133WWContagemDS_20_Tfcontagem_areatrabalhocod, AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to, AV135WWContagemDS_22_Tfcontagem_contratadacod, AV136WWContagemDS_23_Tfcontagem_contratadacod_to, AV137WWContagemDS_24_Tfcontagem_datacriacao, AV138WWContagemDS_25_Tfcontagem_datacriacao_to, lV139WWContagemDS_26_Tfcontagem_areatrabalhodes, AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel, lV143WWContagemDS_30_Tfcontagem_notas, AV144WWContagemDS_31_Tfcontagem_notas_sel, AV145WWContagemDS_32_Tfcontagem_pfb, AV146WWContagemDS_33_Tfcontagem_pfb_to, AV147WWContagemDS_34_Tfcontagem_pfl, AV148WWContagemDS_35_Tfcontagem_pfl_to, AV149WWContagemDS_36_Tfcontagem_divergencia, AV150WWContagemDS_37_Tfcontagem_divergencia_to, AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod, AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to, AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod, AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to, lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom, AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel, lV158WWContagemDS_45_Tfcontagem_demanda, AV159WWContagemDS_46_Tfcontagem_demanda_sel, lV160WWContagemDS_47_Tfcontagem_link, AV161WWContagemDS_48_Tfcontagem_link_sel, AV162WWContagemDS_49_Tfcontagem_fator, AV163WWContagemDS_50_Tfcontagem_fator_to, AV164WWContagemDS_51_Tfcontagem_deflator, AV165WWContagemDS_52_Tfcontagem_deflator_to, AV166WWContagemDS_53_Tfcontagem_sistemacod, AV167WWContagemDS_54_Tfcontagem_sistemacod_to, lV168WWContagemDS_55_Tfcontagem_sistemasigla, AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKHS4 = false;
            A1059Contagem_Notas = P00HS3_A1059Contagem_Notas[0];
            n1059Contagem_Notas = P00HS3_n1059Contagem_Notas[0];
            A941Contagem_SistemaSigla = P00HS3_A941Contagem_SistemaSigla[0];
            n941Contagem_SistemaSigla = P00HS3_n941Contagem_SistemaSigla[0];
            A940Contagem_SistemaCod = P00HS3_A940Contagem_SistemaCod[0];
            n940Contagem_SistemaCod = P00HS3_n940Contagem_SistemaCod[0];
            A1117Contagem_Deflator = P00HS3_A1117Contagem_Deflator[0];
            n1117Contagem_Deflator = P00HS3_n1117Contagem_Deflator[0];
            A947Contagem_Fator = P00HS3_A947Contagem_Fator[0];
            n947Contagem_Fator = P00HS3_n947Contagem_Fator[0];
            A946Contagem_Link = P00HS3_A946Contagem_Link[0];
            n946Contagem_Link = P00HS3_n946Contagem_Link[0];
            A945Contagem_Demanda = P00HS3_A945Contagem_Demanda[0];
            n945Contagem_Demanda = P00HS3_n945Contagem_Demanda[0];
            A262Contagem_Status = P00HS3_A262Contagem_Status[0];
            n262Contagem_Status = P00HS3_n262Contagem_Status[0];
            A214Contagem_UsuarioContadorPessoaCod = P00HS3_A214Contagem_UsuarioContadorPessoaCod[0];
            n214Contagem_UsuarioContadorPessoaCod = P00HS3_n214Contagem_UsuarioContadorPessoaCod[0];
            A213Contagem_UsuarioContadorCod = P00HS3_A213Contagem_UsuarioContadorCod[0];
            n213Contagem_UsuarioContadorCod = P00HS3_n213Contagem_UsuarioContadorCod[0];
            A1119Contagem_Divergencia = P00HS3_A1119Contagem_Divergencia[0];
            n1119Contagem_Divergencia = P00HS3_n1119Contagem_Divergencia[0];
            A944Contagem_PFL = P00HS3_A944Contagem_PFL[0];
            n944Contagem_PFL = P00HS3_n944Contagem_PFL[0];
            A943Contagem_PFB = P00HS3_A943Contagem_PFB[0];
            n943Contagem_PFB = P00HS3_n943Contagem_PFB[0];
            A196Contagem_Tipo = P00HS3_A196Contagem_Tipo[0];
            n196Contagem_Tipo = P00HS3_n196Contagem_Tipo[0];
            A195Contagem_Tecnica = P00HS3_A195Contagem_Tecnica[0];
            n195Contagem_Tecnica = P00HS3_n195Contagem_Tecnica[0];
            A197Contagem_DataCriacao = P00HS3_A197Contagem_DataCriacao[0];
            A193Contagem_AreaTrabalhoCod = P00HS3_A193Contagem_AreaTrabalhoCod[0];
            A192Contagem_Codigo = P00HS3_A192Contagem_Codigo[0];
            A215Contagem_UsuarioContadorPessoaNom = P00HS3_A215Contagem_UsuarioContadorPessoaNom[0];
            n215Contagem_UsuarioContadorPessoaNom = P00HS3_n215Contagem_UsuarioContadorPessoaNom[0];
            A194Contagem_AreaTrabalhoDes = P00HS3_A194Contagem_AreaTrabalhoDes[0];
            n194Contagem_AreaTrabalhoDes = P00HS3_n194Contagem_AreaTrabalhoDes[0];
            A1118Contagem_ContratadaCod = P00HS3_A1118Contagem_ContratadaCod[0];
            n1118Contagem_ContratadaCod = P00HS3_n1118Contagem_ContratadaCod[0];
            A941Contagem_SistemaSigla = P00HS3_A941Contagem_SistemaSigla[0];
            n941Contagem_SistemaSigla = P00HS3_n941Contagem_SistemaSigla[0];
            A214Contagem_UsuarioContadorPessoaCod = P00HS3_A214Contagem_UsuarioContadorPessoaCod[0];
            n214Contagem_UsuarioContadorPessoaCod = P00HS3_n214Contagem_UsuarioContadorPessoaCod[0];
            A215Contagem_UsuarioContadorPessoaNom = P00HS3_A215Contagem_UsuarioContadorPessoaNom[0];
            n215Contagem_UsuarioContadorPessoaNom = P00HS3_n215Contagem_UsuarioContadorPessoaNom[0];
            A194Contagem_AreaTrabalhoDes = P00HS3_A194Contagem_AreaTrabalhoDes[0];
            n194Contagem_AreaTrabalhoDes = P00HS3_n194Contagem_AreaTrabalhoDes[0];
            AV87count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00HS3_A1059Contagem_Notas[0], A1059Contagem_Notas) == 0 ) )
            {
               BRKHS4 = false;
               A192Contagem_Codigo = P00HS3_A192Contagem_Codigo[0];
               AV87count = (long)(AV87count+1);
               BRKHS4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1059Contagem_Notas)) )
            {
               AV79Option = A1059Contagem_Notas;
               AV80Options.Add(AV79Option, 0);
               AV85OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV87count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV80Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKHS4 )
            {
               BRKHS4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTAGEM_USUARIOCONTADORPESSOANOMOPTIONS' Routine */
         AV36TFContagem_UsuarioContadorPessoaNom = AV75SearchTxt;
         AV37TFContagem_UsuarioContadorPessoaNom_Sel = "";
         AV114WWContagemDS_1_Dynamicfiltersselector1 = AV93DynamicFiltersSelector1;
         AV115WWContagemDS_2_Dynamicfiltersoperator1 = AV94DynamicFiltersOperator1;
         AV116WWContagemDS_3_Contagem_contratadacod1 = AV95Contagem_ContratadaCod1;
         AV117WWContagemDS_4_Contagem_areatrabalhodes1 = AV96Contagem_AreaTrabalhoDes1;
         AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 = AV97Contagem_UsuarioContadorPessoaNom1;
         AV119WWContagemDS_6_Dynamicfiltersenabled2 = AV98DynamicFiltersEnabled2;
         AV120WWContagemDS_7_Dynamicfiltersselector2 = AV99DynamicFiltersSelector2;
         AV121WWContagemDS_8_Dynamicfiltersoperator2 = AV100DynamicFiltersOperator2;
         AV122WWContagemDS_9_Contagem_contratadacod2 = AV101Contagem_ContratadaCod2;
         AV123WWContagemDS_10_Contagem_areatrabalhodes2 = AV102Contagem_AreaTrabalhoDes2;
         AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 = AV103Contagem_UsuarioContadorPessoaNom2;
         AV125WWContagemDS_12_Dynamicfiltersenabled3 = AV104DynamicFiltersEnabled3;
         AV126WWContagemDS_13_Dynamicfiltersselector3 = AV105DynamicFiltersSelector3;
         AV127WWContagemDS_14_Dynamicfiltersoperator3 = AV106DynamicFiltersOperator3;
         AV128WWContagemDS_15_Contagem_contratadacod3 = AV107Contagem_ContratadaCod3;
         AV129WWContagemDS_16_Contagem_areatrabalhodes3 = AV108Contagem_AreaTrabalhoDes3;
         AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 = AV109Contagem_UsuarioContadorPessoaNom3;
         AV131WWContagemDS_18_Tfcontagem_codigo = AV10TFContagem_Codigo;
         AV132WWContagemDS_19_Tfcontagem_codigo_to = AV11TFContagem_Codigo_To;
         AV133WWContagemDS_20_Tfcontagem_areatrabalhocod = AV12TFContagem_AreaTrabalhoCod;
         AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to = AV13TFContagem_AreaTrabalhoCod_To;
         AV135WWContagemDS_22_Tfcontagem_contratadacod = AV14TFContagem_ContratadaCod;
         AV136WWContagemDS_23_Tfcontagem_contratadacod_to = AV15TFContagem_ContratadaCod_To;
         AV137WWContagemDS_24_Tfcontagem_datacriacao = AV16TFContagem_DataCriacao;
         AV138WWContagemDS_25_Tfcontagem_datacriacao_to = AV17TFContagem_DataCriacao_To;
         AV139WWContagemDS_26_Tfcontagem_areatrabalhodes = AV18TFContagem_AreaTrabalhoDes;
         AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel = AV19TFContagem_AreaTrabalhoDes_Sel;
         AV141WWContagemDS_28_Tfcontagem_tecnica_sels = AV21TFContagem_Tecnica_Sels;
         AV142WWContagemDS_29_Tfcontagem_tipo_sels = AV23TFContagem_Tipo_Sels;
         AV143WWContagemDS_30_Tfcontagem_notas = AV24TFContagem_Notas;
         AV144WWContagemDS_31_Tfcontagem_notas_sel = AV25TFContagem_Notas_Sel;
         AV145WWContagemDS_32_Tfcontagem_pfb = AV26TFContagem_PFB;
         AV146WWContagemDS_33_Tfcontagem_pfb_to = AV27TFContagem_PFB_To;
         AV147WWContagemDS_34_Tfcontagem_pfl = AV28TFContagem_PFL;
         AV148WWContagemDS_35_Tfcontagem_pfl_to = AV29TFContagem_PFL_To;
         AV149WWContagemDS_36_Tfcontagem_divergencia = AV30TFContagem_Divergencia;
         AV150WWContagemDS_37_Tfcontagem_divergencia_to = AV31TFContagem_Divergencia_To;
         AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod = AV32TFContagem_UsuarioContadorCod;
         AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to = AV33TFContagem_UsuarioContadorCod_To;
         AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod = AV34TFContagem_UsuarioContadorPessoaCod;
         AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to = AV35TFContagem_UsuarioContadorPessoaCod_To;
         AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom = AV36TFContagem_UsuarioContadorPessoaNom;
         AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel = AV37TFContagem_UsuarioContadorPessoaNom_Sel;
         AV157WWContagemDS_44_Tfcontagem_status_sels = AV39TFContagem_Status_Sels;
         AV158WWContagemDS_45_Tfcontagem_demanda = AV40TFContagem_Demanda;
         AV159WWContagemDS_46_Tfcontagem_demanda_sel = AV41TFContagem_Demanda_Sel;
         AV160WWContagemDS_47_Tfcontagem_link = AV42TFContagem_Link;
         AV161WWContagemDS_48_Tfcontagem_link_sel = AV43TFContagem_Link_Sel;
         AV162WWContagemDS_49_Tfcontagem_fator = AV44TFContagem_Fator;
         AV163WWContagemDS_50_Tfcontagem_fator_to = AV45TFContagem_Fator_To;
         AV164WWContagemDS_51_Tfcontagem_deflator = AV46TFContagem_Deflator;
         AV165WWContagemDS_52_Tfcontagem_deflator_to = AV47TFContagem_Deflator_To;
         AV166WWContagemDS_53_Tfcontagem_sistemacod = AV48TFContagem_SistemaCod;
         AV167WWContagemDS_54_Tfcontagem_sistemacod_to = AV49TFContagem_SistemaCod_To;
         AV168WWContagemDS_55_Tfcontagem_sistemasigla = AV50TFContagem_SistemaSigla;
         AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel = AV51TFContagem_SistemaSigla_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A195Contagem_Tecnica ,
                                              AV141WWContagemDS_28_Tfcontagem_tecnica_sels ,
                                              A196Contagem_Tipo ,
                                              AV142WWContagemDS_29_Tfcontagem_tipo_sels ,
                                              A262Contagem_Status ,
                                              AV157WWContagemDS_44_Tfcontagem_status_sels ,
                                              AV114WWContagemDS_1_Dynamicfiltersselector1 ,
                                              AV115WWContagemDS_2_Dynamicfiltersoperator1 ,
                                              AV116WWContagemDS_3_Contagem_contratadacod1 ,
                                              AV117WWContagemDS_4_Contagem_areatrabalhodes1 ,
                                              AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 ,
                                              AV119WWContagemDS_6_Dynamicfiltersenabled2 ,
                                              AV120WWContagemDS_7_Dynamicfiltersselector2 ,
                                              AV121WWContagemDS_8_Dynamicfiltersoperator2 ,
                                              AV122WWContagemDS_9_Contagem_contratadacod2 ,
                                              AV123WWContagemDS_10_Contagem_areatrabalhodes2 ,
                                              AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 ,
                                              AV125WWContagemDS_12_Dynamicfiltersenabled3 ,
                                              AV126WWContagemDS_13_Dynamicfiltersselector3 ,
                                              AV127WWContagemDS_14_Dynamicfiltersoperator3 ,
                                              AV128WWContagemDS_15_Contagem_contratadacod3 ,
                                              AV129WWContagemDS_16_Contagem_areatrabalhodes3 ,
                                              AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 ,
                                              AV131WWContagemDS_18_Tfcontagem_codigo ,
                                              AV132WWContagemDS_19_Tfcontagem_codigo_to ,
                                              AV133WWContagemDS_20_Tfcontagem_areatrabalhocod ,
                                              AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to ,
                                              AV135WWContagemDS_22_Tfcontagem_contratadacod ,
                                              AV136WWContagemDS_23_Tfcontagem_contratadacod_to ,
                                              AV137WWContagemDS_24_Tfcontagem_datacriacao ,
                                              AV138WWContagemDS_25_Tfcontagem_datacriacao_to ,
                                              AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel ,
                                              AV139WWContagemDS_26_Tfcontagem_areatrabalhodes ,
                                              AV141WWContagemDS_28_Tfcontagem_tecnica_sels.Count ,
                                              AV142WWContagemDS_29_Tfcontagem_tipo_sels.Count ,
                                              AV144WWContagemDS_31_Tfcontagem_notas_sel ,
                                              AV143WWContagemDS_30_Tfcontagem_notas ,
                                              AV145WWContagemDS_32_Tfcontagem_pfb ,
                                              AV146WWContagemDS_33_Tfcontagem_pfb_to ,
                                              AV147WWContagemDS_34_Tfcontagem_pfl ,
                                              AV148WWContagemDS_35_Tfcontagem_pfl_to ,
                                              AV149WWContagemDS_36_Tfcontagem_divergencia ,
                                              AV150WWContagemDS_37_Tfcontagem_divergencia_to ,
                                              AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod ,
                                              AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to ,
                                              AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod ,
                                              AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to ,
                                              AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel ,
                                              AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom ,
                                              AV157WWContagemDS_44_Tfcontagem_status_sels.Count ,
                                              AV159WWContagemDS_46_Tfcontagem_demanda_sel ,
                                              AV158WWContagemDS_45_Tfcontagem_demanda ,
                                              AV161WWContagemDS_48_Tfcontagem_link_sel ,
                                              AV160WWContagemDS_47_Tfcontagem_link ,
                                              AV162WWContagemDS_49_Tfcontagem_fator ,
                                              AV163WWContagemDS_50_Tfcontagem_fator_to ,
                                              AV164WWContagemDS_51_Tfcontagem_deflator ,
                                              AV165WWContagemDS_52_Tfcontagem_deflator_to ,
                                              AV166WWContagemDS_53_Tfcontagem_sistemacod ,
                                              AV167WWContagemDS_54_Tfcontagem_sistemacod_to ,
                                              AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel ,
                                              AV168WWContagemDS_55_Tfcontagem_sistemasigla ,
                                              A1118Contagem_ContratadaCod ,
                                              A194Contagem_AreaTrabalhoDes ,
                                              A215Contagem_UsuarioContadorPessoaNom ,
                                              A192Contagem_Codigo ,
                                              A193Contagem_AreaTrabalhoCod ,
                                              A197Contagem_DataCriacao ,
                                              A1059Contagem_Notas ,
                                              A943Contagem_PFB ,
                                              A944Contagem_PFL ,
                                              A1119Contagem_Divergencia ,
                                              A213Contagem_UsuarioContadorCod ,
                                              A214Contagem_UsuarioContadorPessoaCod ,
                                              A945Contagem_Demanda ,
                                              A946Contagem_Link ,
                                              A947Contagem_Fator ,
                                              A1117Contagem_Deflator ,
                                              A940Contagem_SistemaCod ,
                                              A941Contagem_SistemaSigla },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV117WWContagemDS_4_Contagem_areatrabalhodes1 = StringUtil.Concat( StringUtil.RTrim( AV117WWContagemDS_4_Contagem_areatrabalhodes1), "%", "");
         lV117WWContagemDS_4_Contagem_areatrabalhodes1 = StringUtil.Concat( StringUtil.RTrim( AV117WWContagemDS_4_Contagem_areatrabalhodes1), "%", "");
         lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1), 100, "%");
         lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1), 100, "%");
         lV123WWContagemDS_10_Contagem_areatrabalhodes2 = StringUtil.Concat( StringUtil.RTrim( AV123WWContagemDS_10_Contagem_areatrabalhodes2), "%", "");
         lV123WWContagemDS_10_Contagem_areatrabalhodes2 = StringUtil.Concat( StringUtil.RTrim( AV123WWContagemDS_10_Contagem_areatrabalhodes2), "%", "");
         lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2), 100, "%");
         lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2), 100, "%");
         lV129WWContagemDS_16_Contagem_areatrabalhodes3 = StringUtil.Concat( StringUtil.RTrim( AV129WWContagemDS_16_Contagem_areatrabalhodes3), "%", "");
         lV129WWContagemDS_16_Contagem_areatrabalhodes3 = StringUtil.Concat( StringUtil.RTrim( AV129WWContagemDS_16_Contagem_areatrabalhodes3), "%", "");
         lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3), 100, "%");
         lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3), 100, "%");
         lV139WWContagemDS_26_Tfcontagem_areatrabalhodes = StringUtil.Concat( StringUtil.RTrim( AV139WWContagemDS_26_Tfcontagem_areatrabalhodes), "%", "");
         lV143WWContagemDS_30_Tfcontagem_notas = StringUtil.Concat( StringUtil.RTrim( AV143WWContagemDS_30_Tfcontagem_notas), "%", "");
         lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom = StringUtil.PadR( StringUtil.RTrim( AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom), 100, "%");
         lV158WWContagemDS_45_Tfcontagem_demanda = StringUtil.Concat( StringUtil.RTrim( AV158WWContagemDS_45_Tfcontagem_demanda), "%", "");
         lV160WWContagemDS_47_Tfcontagem_link = StringUtil.Concat( StringUtil.RTrim( AV160WWContagemDS_47_Tfcontagem_link), "%", "");
         lV168WWContagemDS_55_Tfcontagem_sistemasigla = StringUtil.PadR( StringUtil.RTrim( AV168WWContagemDS_55_Tfcontagem_sistemasigla), 25, "%");
         /* Using cursor P00HS4 */
         pr_default.execute(2, new Object[] {AV116WWContagemDS_3_Contagem_contratadacod1, AV116WWContagemDS_3_Contagem_contratadacod1, AV116WWContagemDS_3_Contagem_contratadacod1, lV117WWContagemDS_4_Contagem_areatrabalhodes1, lV117WWContagemDS_4_Contagem_areatrabalhodes1, lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1, lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1, AV122WWContagemDS_9_Contagem_contratadacod2, AV122WWContagemDS_9_Contagem_contratadacod2, AV122WWContagemDS_9_Contagem_contratadacod2, lV123WWContagemDS_10_Contagem_areatrabalhodes2, lV123WWContagemDS_10_Contagem_areatrabalhodes2, lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2, lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2, AV128WWContagemDS_15_Contagem_contratadacod3, AV128WWContagemDS_15_Contagem_contratadacod3, AV128WWContagemDS_15_Contagem_contratadacod3, lV129WWContagemDS_16_Contagem_areatrabalhodes3, lV129WWContagemDS_16_Contagem_areatrabalhodes3, lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3, lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3, AV131WWContagemDS_18_Tfcontagem_codigo, AV132WWContagemDS_19_Tfcontagem_codigo_to, AV133WWContagemDS_20_Tfcontagem_areatrabalhocod, AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to, AV135WWContagemDS_22_Tfcontagem_contratadacod, AV136WWContagemDS_23_Tfcontagem_contratadacod_to, AV137WWContagemDS_24_Tfcontagem_datacriacao, AV138WWContagemDS_25_Tfcontagem_datacriacao_to, lV139WWContagemDS_26_Tfcontagem_areatrabalhodes, AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel, lV143WWContagemDS_30_Tfcontagem_notas, AV144WWContagemDS_31_Tfcontagem_notas_sel, AV145WWContagemDS_32_Tfcontagem_pfb, AV146WWContagemDS_33_Tfcontagem_pfb_to, AV147WWContagemDS_34_Tfcontagem_pfl, AV148WWContagemDS_35_Tfcontagem_pfl_to, AV149WWContagemDS_36_Tfcontagem_divergencia, AV150WWContagemDS_37_Tfcontagem_divergencia_to, AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod, AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to, AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod, AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to, lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom, AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel, lV158WWContagemDS_45_Tfcontagem_demanda, AV159WWContagemDS_46_Tfcontagem_demanda_sel, lV160WWContagemDS_47_Tfcontagem_link, AV161WWContagemDS_48_Tfcontagem_link_sel, AV162WWContagemDS_49_Tfcontagem_fator, AV163WWContagemDS_50_Tfcontagem_fator_to, AV164WWContagemDS_51_Tfcontagem_deflator, AV165WWContagemDS_52_Tfcontagem_deflator_to, AV166WWContagemDS_53_Tfcontagem_sistemacod, AV167WWContagemDS_54_Tfcontagem_sistemacod_to, lV168WWContagemDS_55_Tfcontagem_sistemasigla, AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKHS6 = false;
            A215Contagem_UsuarioContadorPessoaNom = P00HS4_A215Contagem_UsuarioContadorPessoaNom[0];
            n215Contagem_UsuarioContadorPessoaNom = P00HS4_n215Contagem_UsuarioContadorPessoaNom[0];
            A941Contagem_SistemaSigla = P00HS4_A941Contagem_SistemaSigla[0];
            n941Contagem_SistemaSigla = P00HS4_n941Contagem_SistemaSigla[0];
            A940Contagem_SistemaCod = P00HS4_A940Contagem_SistemaCod[0];
            n940Contagem_SistemaCod = P00HS4_n940Contagem_SistemaCod[0];
            A1117Contagem_Deflator = P00HS4_A1117Contagem_Deflator[0];
            n1117Contagem_Deflator = P00HS4_n1117Contagem_Deflator[0];
            A947Contagem_Fator = P00HS4_A947Contagem_Fator[0];
            n947Contagem_Fator = P00HS4_n947Contagem_Fator[0];
            A946Contagem_Link = P00HS4_A946Contagem_Link[0];
            n946Contagem_Link = P00HS4_n946Contagem_Link[0];
            A945Contagem_Demanda = P00HS4_A945Contagem_Demanda[0];
            n945Contagem_Demanda = P00HS4_n945Contagem_Demanda[0];
            A262Contagem_Status = P00HS4_A262Contagem_Status[0];
            n262Contagem_Status = P00HS4_n262Contagem_Status[0];
            A214Contagem_UsuarioContadorPessoaCod = P00HS4_A214Contagem_UsuarioContadorPessoaCod[0];
            n214Contagem_UsuarioContadorPessoaCod = P00HS4_n214Contagem_UsuarioContadorPessoaCod[0];
            A213Contagem_UsuarioContadorCod = P00HS4_A213Contagem_UsuarioContadorCod[0];
            n213Contagem_UsuarioContadorCod = P00HS4_n213Contagem_UsuarioContadorCod[0];
            A1119Contagem_Divergencia = P00HS4_A1119Contagem_Divergencia[0];
            n1119Contagem_Divergencia = P00HS4_n1119Contagem_Divergencia[0];
            A944Contagem_PFL = P00HS4_A944Contagem_PFL[0];
            n944Contagem_PFL = P00HS4_n944Contagem_PFL[0];
            A943Contagem_PFB = P00HS4_A943Contagem_PFB[0];
            n943Contagem_PFB = P00HS4_n943Contagem_PFB[0];
            A1059Contagem_Notas = P00HS4_A1059Contagem_Notas[0];
            n1059Contagem_Notas = P00HS4_n1059Contagem_Notas[0];
            A196Contagem_Tipo = P00HS4_A196Contagem_Tipo[0];
            n196Contagem_Tipo = P00HS4_n196Contagem_Tipo[0];
            A195Contagem_Tecnica = P00HS4_A195Contagem_Tecnica[0];
            n195Contagem_Tecnica = P00HS4_n195Contagem_Tecnica[0];
            A197Contagem_DataCriacao = P00HS4_A197Contagem_DataCriacao[0];
            A193Contagem_AreaTrabalhoCod = P00HS4_A193Contagem_AreaTrabalhoCod[0];
            A192Contagem_Codigo = P00HS4_A192Contagem_Codigo[0];
            A194Contagem_AreaTrabalhoDes = P00HS4_A194Contagem_AreaTrabalhoDes[0];
            n194Contagem_AreaTrabalhoDes = P00HS4_n194Contagem_AreaTrabalhoDes[0];
            A1118Contagem_ContratadaCod = P00HS4_A1118Contagem_ContratadaCod[0];
            n1118Contagem_ContratadaCod = P00HS4_n1118Contagem_ContratadaCod[0];
            A941Contagem_SistemaSigla = P00HS4_A941Contagem_SistemaSigla[0];
            n941Contagem_SistemaSigla = P00HS4_n941Contagem_SistemaSigla[0];
            A214Contagem_UsuarioContadorPessoaCod = P00HS4_A214Contagem_UsuarioContadorPessoaCod[0];
            n214Contagem_UsuarioContadorPessoaCod = P00HS4_n214Contagem_UsuarioContadorPessoaCod[0];
            A215Contagem_UsuarioContadorPessoaNom = P00HS4_A215Contagem_UsuarioContadorPessoaNom[0];
            n215Contagem_UsuarioContadorPessoaNom = P00HS4_n215Contagem_UsuarioContadorPessoaNom[0];
            A194Contagem_AreaTrabalhoDes = P00HS4_A194Contagem_AreaTrabalhoDes[0];
            n194Contagem_AreaTrabalhoDes = P00HS4_n194Contagem_AreaTrabalhoDes[0];
            AV87count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00HS4_A215Contagem_UsuarioContadorPessoaNom[0], A215Contagem_UsuarioContadorPessoaNom) == 0 ) )
            {
               BRKHS6 = false;
               A214Contagem_UsuarioContadorPessoaCod = P00HS4_A214Contagem_UsuarioContadorPessoaCod[0];
               n214Contagem_UsuarioContadorPessoaCod = P00HS4_n214Contagem_UsuarioContadorPessoaCod[0];
               A213Contagem_UsuarioContadorCod = P00HS4_A213Contagem_UsuarioContadorCod[0];
               n213Contagem_UsuarioContadorCod = P00HS4_n213Contagem_UsuarioContadorCod[0];
               A192Contagem_Codigo = P00HS4_A192Contagem_Codigo[0];
               A214Contagem_UsuarioContadorPessoaCod = P00HS4_A214Contagem_UsuarioContadorPessoaCod[0];
               n214Contagem_UsuarioContadorPessoaCod = P00HS4_n214Contagem_UsuarioContadorPessoaCod[0];
               AV87count = (long)(AV87count+1);
               BRKHS6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A215Contagem_UsuarioContadorPessoaNom)) )
            {
               AV79Option = A215Contagem_UsuarioContadorPessoaNom;
               AV80Options.Add(AV79Option, 0);
               AV85OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV87count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV80Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKHS6 )
            {
               BRKHS6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADCONTAGEM_DEMANDAOPTIONS' Routine */
         AV40TFContagem_Demanda = AV75SearchTxt;
         AV41TFContagem_Demanda_Sel = "";
         AV114WWContagemDS_1_Dynamicfiltersselector1 = AV93DynamicFiltersSelector1;
         AV115WWContagemDS_2_Dynamicfiltersoperator1 = AV94DynamicFiltersOperator1;
         AV116WWContagemDS_3_Contagem_contratadacod1 = AV95Contagem_ContratadaCod1;
         AV117WWContagemDS_4_Contagem_areatrabalhodes1 = AV96Contagem_AreaTrabalhoDes1;
         AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 = AV97Contagem_UsuarioContadorPessoaNom1;
         AV119WWContagemDS_6_Dynamicfiltersenabled2 = AV98DynamicFiltersEnabled2;
         AV120WWContagemDS_7_Dynamicfiltersselector2 = AV99DynamicFiltersSelector2;
         AV121WWContagemDS_8_Dynamicfiltersoperator2 = AV100DynamicFiltersOperator2;
         AV122WWContagemDS_9_Contagem_contratadacod2 = AV101Contagem_ContratadaCod2;
         AV123WWContagemDS_10_Contagem_areatrabalhodes2 = AV102Contagem_AreaTrabalhoDes2;
         AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 = AV103Contagem_UsuarioContadorPessoaNom2;
         AV125WWContagemDS_12_Dynamicfiltersenabled3 = AV104DynamicFiltersEnabled3;
         AV126WWContagemDS_13_Dynamicfiltersselector3 = AV105DynamicFiltersSelector3;
         AV127WWContagemDS_14_Dynamicfiltersoperator3 = AV106DynamicFiltersOperator3;
         AV128WWContagemDS_15_Contagem_contratadacod3 = AV107Contagem_ContratadaCod3;
         AV129WWContagemDS_16_Contagem_areatrabalhodes3 = AV108Contagem_AreaTrabalhoDes3;
         AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 = AV109Contagem_UsuarioContadorPessoaNom3;
         AV131WWContagemDS_18_Tfcontagem_codigo = AV10TFContagem_Codigo;
         AV132WWContagemDS_19_Tfcontagem_codigo_to = AV11TFContagem_Codigo_To;
         AV133WWContagemDS_20_Tfcontagem_areatrabalhocod = AV12TFContagem_AreaTrabalhoCod;
         AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to = AV13TFContagem_AreaTrabalhoCod_To;
         AV135WWContagemDS_22_Tfcontagem_contratadacod = AV14TFContagem_ContratadaCod;
         AV136WWContagemDS_23_Tfcontagem_contratadacod_to = AV15TFContagem_ContratadaCod_To;
         AV137WWContagemDS_24_Tfcontagem_datacriacao = AV16TFContagem_DataCriacao;
         AV138WWContagemDS_25_Tfcontagem_datacriacao_to = AV17TFContagem_DataCriacao_To;
         AV139WWContagemDS_26_Tfcontagem_areatrabalhodes = AV18TFContagem_AreaTrabalhoDes;
         AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel = AV19TFContagem_AreaTrabalhoDes_Sel;
         AV141WWContagemDS_28_Tfcontagem_tecnica_sels = AV21TFContagem_Tecnica_Sels;
         AV142WWContagemDS_29_Tfcontagem_tipo_sels = AV23TFContagem_Tipo_Sels;
         AV143WWContagemDS_30_Tfcontagem_notas = AV24TFContagem_Notas;
         AV144WWContagemDS_31_Tfcontagem_notas_sel = AV25TFContagem_Notas_Sel;
         AV145WWContagemDS_32_Tfcontagem_pfb = AV26TFContagem_PFB;
         AV146WWContagemDS_33_Tfcontagem_pfb_to = AV27TFContagem_PFB_To;
         AV147WWContagemDS_34_Tfcontagem_pfl = AV28TFContagem_PFL;
         AV148WWContagemDS_35_Tfcontagem_pfl_to = AV29TFContagem_PFL_To;
         AV149WWContagemDS_36_Tfcontagem_divergencia = AV30TFContagem_Divergencia;
         AV150WWContagemDS_37_Tfcontagem_divergencia_to = AV31TFContagem_Divergencia_To;
         AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod = AV32TFContagem_UsuarioContadorCod;
         AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to = AV33TFContagem_UsuarioContadorCod_To;
         AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod = AV34TFContagem_UsuarioContadorPessoaCod;
         AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to = AV35TFContagem_UsuarioContadorPessoaCod_To;
         AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom = AV36TFContagem_UsuarioContadorPessoaNom;
         AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel = AV37TFContagem_UsuarioContadorPessoaNom_Sel;
         AV157WWContagemDS_44_Tfcontagem_status_sels = AV39TFContagem_Status_Sels;
         AV158WWContagemDS_45_Tfcontagem_demanda = AV40TFContagem_Demanda;
         AV159WWContagemDS_46_Tfcontagem_demanda_sel = AV41TFContagem_Demanda_Sel;
         AV160WWContagemDS_47_Tfcontagem_link = AV42TFContagem_Link;
         AV161WWContagemDS_48_Tfcontagem_link_sel = AV43TFContagem_Link_Sel;
         AV162WWContagemDS_49_Tfcontagem_fator = AV44TFContagem_Fator;
         AV163WWContagemDS_50_Tfcontagem_fator_to = AV45TFContagem_Fator_To;
         AV164WWContagemDS_51_Tfcontagem_deflator = AV46TFContagem_Deflator;
         AV165WWContagemDS_52_Tfcontagem_deflator_to = AV47TFContagem_Deflator_To;
         AV166WWContagemDS_53_Tfcontagem_sistemacod = AV48TFContagem_SistemaCod;
         AV167WWContagemDS_54_Tfcontagem_sistemacod_to = AV49TFContagem_SistemaCod_To;
         AV168WWContagemDS_55_Tfcontagem_sistemasigla = AV50TFContagem_SistemaSigla;
         AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel = AV51TFContagem_SistemaSigla_Sel;
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              A195Contagem_Tecnica ,
                                              AV141WWContagemDS_28_Tfcontagem_tecnica_sels ,
                                              A196Contagem_Tipo ,
                                              AV142WWContagemDS_29_Tfcontagem_tipo_sels ,
                                              A262Contagem_Status ,
                                              AV157WWContagemDS_44_Tfcontagem_status_sels ,
                                              AV114WWContagemDS_1_Dynamicfiltersselector1 ,
                                              AV115WWContagemDS_2_Dynamicfiltersoperator1 ,
                                              AV116WWContagemDS_3_Contagem_contratadacod1 ,
                                              AV117WWContagemDS_4_Contagem_areatrabalhodes1 ,
                                              AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 ,
                                              AV119WWContagemDS_6_Dynamicfiltersenabled2 ,
                                              AV120WWContagemDS_7_Dynamicfiltersselector2 ,
                                              AV121WWContagemDS_8_Dynamicfiltersoperator2 ,
                                              AV122WWContagemDS_9_Contagem_contratadacod2 ,
                                              AV123WWContagemDS_10_Contagem_areatrabalhodes2 ,
                                              AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 ,
                                              AV125WWContagemDS_12_Dynamicfiltersenabled3 ,
                                              AV126WWContagemDS_13_Dynamicfiltersselector3 ,
                                              AV127WWContagemDS_14_Dynamicfiltersoperator3 ,
                                              AV128WWContagemDS_15_Contagem_contratadacod3 ,
                                              AV129WWContagemDS_16_Contagem_areatrabalhodes3 ,
                                              AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 ,
                                              AV131WWContagemDS_18_Tfcontagem_codigo ,
                                              AV132WWContagemDS_19_Tfcontagem_codigo_to ,
                                              AV133WWContagemDS_20_Tfcontagem_areatrabalhocod ,
                                              AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to ,
                                              AV135WWContagemDS_22_Tfcontagem_contratadacod ,
                                              AV136WWContagemDS_23_Tfcontagem_contratadacod_to ,
                                              AV137WWContagemDS_24_Tfcontagem_datacriacao ,
                                              AV138WWContagemDS_25_Tfcontagem_datacriacao_to ,
                                              AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel ,
                                              AV139WWContagemDS_26_Tfcontagem_areatrabalhodes ,
                                              AV141WWContagemDS_28_Tfcontagem_tecnica_sels.Count ,
                                              AV142WWContagemDS_29_Tfcontagem_tipo_sels.Count ,
                                              AV144WWContagemDS_31_Tfcontagem_notas_sel ,
                                              AV143WWContagemDS_30_Tfcontagem_notas ,
                                              AV145WWContagemDS_32_Tfcontagem_pfb ,
                                              AV146WWContagemDS_33_Tfcontagem_pfb_to ,
                                              AV147WWContagemDS_34_Tfcontagem_pfl ,
                                              AV148WWContagemDS_35_Tfcontagem_pfl_to ,
                                              AV149WWContagemDS_36_Tfcontagem_divergencia ,
                                              AV150WWContagemDS_37_Tfcontagem_divergencia_to ,
                                              AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod ,
                                              AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to ,
                                              AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod ,
                                              AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to ,
                                              AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel ,
                                              AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom ,
                                              AV157WWContagemDS_44_Tfcontagem_status_sels.Count ,
                                              AV159WWContagemDS_46_Tfcontagem_demanda_sel ,
                                              AV158WWContagemDS_45_Tfcontagem_demanda ,
                                              AV161WWContagemDS_48_Tfcontagem_link_sel ,
                                              AV160WWContagemDS_47_Tfcontagem_link ,
                                              AV162WWContagemDS_49_Tfcontagem_fator ,
                                              AV163WWContagemDS_50_Tfcontagem_fator_to ,
                                              AV164WWContagemDS_51_Tfcontagem_deflator ,
                                              AV165WWContagemDS_52_Tfcontagem_deflator_to ,
                                              AV166WWContagemDS_53_Tfcontagem_sistemacod ,
                                              AV167WWContagemDS_54_Tfcontagem_sistemacod_to ,
                                              AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel ,
                                              AV168WWContagemDS_55_Tfcontagem_sistemasigla ,
                                              A1118Contagem_ContratadaCod ,
                                              A194Contagem_AreaTrabalhoDes ,
                                              A215Contagem_UsuarioContadorPessoaNom ,
                                              A192Contagem_Codigo ,
                                              A193Contagem_AreaTrabalhoCod ,
                                              A197Contagem_DataCriacao ,
                                              A1059Contagem_Notas ,
                                              A943Contagem_PFB ,
                                              A944Contagem_PFL ,
                                              A1119Contagem_Divergencia ,
                                              A213Contagem_UsuarioContadorCod ,
                                              A214Contagem_UsuarioContadorPessoaCod ,
                                              A945Contagem_Demanda ,
                                              A946Contagem_Link ,
                                              A947Contagem_Fator ,
                                              A1117Contagem_Deflator ,
                                              A940Contagem_SistemaCod ,
                                              A941Contagem_SistemaSigla },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV117WWContagemDS_4_Contagem_areatrabalhodes1 = StringUtil.Concat( StringUtil.RTrim( AV117WWContagemDS_4_Contagem_areatrabalhodes1), "%", "");
         lV117WWContagemDS_4_Contagem_areatrabalhodes1 = StringUtil.Concat( StringUtil.RTrim( AV117WWContagemDS_4_Contagem_areatrabalhodes1), "%", "");
         lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1), 100, "%");
         lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1), 100, "%");
         lV123WWContagemDS_10_Contagem_areatrabalhodes2 = StringUtil.Concat( StringUtil.RTrim( AV123WWContagemDS_10_Contagem_areatrabalhodes2), "%", "");
         lV123WWContagemDS_10_Contagem_areatrabalhodes2 = StringUtil.Concat( StringUtil.RTrim( AV123WWContagemDS_10_Contagem_areatrabalhodes2), "%", "");
         lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2), 100, "%");
         lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2), 100, "%");
         lV129WWContagemDS_16_Contagem_areatrabalhodes3 = StringUtil.Concat( StringUtil.RTrim( AV129WWContagemDS_16_Contagem_areatrabalhodes3), "%", "");
         lV129WWContagemDS_16_Contagem_areatrabalhodes3 = StringUtil.Concat( StringUtil.RTrim( AV129WWContagemDS_16_Contagem_areatrabalhodes3), "%", "");
         lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3), 100, "%");
         lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3), 100, "%");
         lV139WWContagemDS_26_Tfcontagem_areatrabalhodes = StringUtil.Concat( StringUtil.RTrim( AV139WWContagemDS_26_Tfcontagem_areatrabalhodes), "%", "");
         lV143WWContagemDS_30_Tfcontagem_notas = StringUtil.Concat( StringUtil.RTrim( AV143WWContagemDS_30_Tfcontagem_notas), "%", "");
         lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom = StringUtil.PadR( StringUtil.RTrim( AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom), 100, "%");
         lV158WWContagemDS_45_Tfcontagem_demanda = StringUtil.Concat( StringUtil.RTrim( AV158WWContagemDS_45_Tfcontagem_demanda), "%", "");
         lV160WWContagemDS_47_Tfcontagem_link = StringUtil.Concat( StringUtil.RTrim( AV160WWContagemDS_47_Tfcontagem_link), "%", "");
         lV168WWContagemDS_55_Tfcontagem_sistemasigla = StringUtil.PadR( StringUtil.RTrim( AV168WWContagemDS_55_Tfcontagem_sistemasigla), 25, "%");
         /* Using cursor P00HS5 */
         pr_default.execute(3, new Object[] {AV116WWContagemDS_3_Contagem_contratadacod1, AV116WWContagemDS_3_Contagem_contratadacod1, AV116WWContagemDS_3_Contagem_contratadacod1, lV117WWContagemDS_4_Contagem_areatrabalhodes1, lV117WWContagemDS_4_Contagem_areatrabalhodes1, lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1, lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1, AV122WWContagemDS_9_Contagem_contratadacod2, AV122WWContagemDS_9_Contagem_contratadacod2, AV122WWContagemDS_9_Contagem_contratadacod2, lV123WWContagemDS_10_Contagem_areatrabalhodes2, lV123WWContagemDS_10_Contagem_areatrabalhodes2, lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2, lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2, AV128WWContagemDS_15_Contagem_contratadacod3, AV128WWContagemDS_15_Contagem_contratadacod3, AV128WWContagemDS_15_Contagem_contratadacod3, lV129WWContagemDS_16_Contagem_areatrabalhodes3, lV129WWContagemDS_16_Contagem_areatrabalhodes3, lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3, lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3, AV131WWContagemDS_18_Tfcontagem_codigo, AV132WWContagemDS_19_Tfcontagem_codigo_to, AV133WWContagemDS_20_Tfcontagem_areatrabalhocod, AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to, AV135WWContagemDS_22_Tfcontagem_contratadacod, AV136WWContagemDS_23_Tfcontagem_contratadacod_to, AV137WWContagemDS_24_Tfcontagem_datacriacao, AV138WWContagemDS_25_Tfcontagem_datacriacao_to, lV139WWContagemDS_26_Tfcontagem_areatrabalhodes, AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel, lV143WWContagemDS_30_Tfcontagem_notas, AV144WWContagemDS_31_Tfcontagem_notas_sel, AV145WWContagemDS_32_Tfcontagem_pfb, AV146WWContagemDS_33_Tfcontagem_pfb_to, AV147WWContagemDS_34_Tfcontagem_pfl, AV148WWContagemDS_35_Tfcontagem_pfl_to, AV149WWContagemDS_36_Tfcontagem_divergencia, AV150WWContagemDS_37_Tfcontagem_divergencia_to, AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod, AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to, AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod, AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to, lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom, AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel, lV158WWContagemDS_45_Tfcontagem_demanda, AV159WWContagemDS_46_Tfcontagem_demanda_sel, lV160WWContagemDS_47_Tfcontagem_link, AV161WWContagemDS_48_Tfcontagem_link_sel, AV162WWContagemDS_49_Tfcontagem_fator, AV163WWContagemDS_50_Tfcontagem_fator_to, AV164WWContagemDS_51_Tfcontagem_deflator, AV165WWContagemDS_52_Tfcontagem_deflator_to, AV166WWContagemDS_53_Tfcontagem_sistemacod, AV167WWContagemDS_54_Tfcontagem_sistemacod_to, lV168WWContagemDS_55_Tfcontagem_sistemasigla, AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKHS8 = false;
            A945Contagem_Demanda = P00HS5_A945Contagem_Demanda[0];
            n945Contagem_Demanda = P00HS5_n945Contagem_Demanda[0];
            A941Contagem_SistemaSigla = P00HS5_A941Contagem_SistemaSigla[0];
            n941Contagem_SistemaSigla = P00HS5_n941Contagem_SistemaSigla[0];
            A940Contagem_SistemaCod = P00HS5_A940Contagem_SistemaCod[0];
            n940Contagem_SistemaCod = P00HS5_n940Contagem_SistemaCod[0];
            A1117Contagem_Deflator = P00HS5_A1117Contagem_Deflator[0];
            n1117Contagem_Deflator = P00HS5_n1117Contagem_Deflator[0];
            A947Contagem_Fator = P00HS5_A947Contagem_Fator[0];
            n947Contagem_Fator = P00HS5_n947Contagem_Fator[0];
            A946Contagem_Link = P00HS5_A946Contagem_Link[0];
            n946Contagem_Link = P00HS5_n946Contagem_Link[0];
            A262Contagem_Status = P00HS5_A262Contagem_Status[0];
            n262Contagem_Status = P00HS5_n262Contagem_Status[0];
            A214Contagem_UsuarioContadorPessoaCod = P00HS5_A214Contagem_UsuarioContadorPessoaCod[0];
            n214Contagem_UsuarioContadorPessoaCod = P00HS5_n214Contagem_UsuarioContadorPessoaCod[0];
            A213Contagem_UsuarioContadorCod = P00HS5_A213Contagem_UsuarioContadorCod[0];
            n213Contagem_UsuarioContadorCod = P00HS5_n213Contagem_UsuarioContadorCod[0];
            A1119Contagem_Divergencia = P00HS5_A1119Contagem_Divergencia[0];
            n1119Contagem_Divergencia = P00HS5_n1119Contagem_Divergencia[0];
            A944Contagem_PFL = P00HS5_A944Contagem_PFL[0];
            n944Contagem_PFL = P00HS5_n944Contagem_PFL[0];
            A943Contagem_PFB = P00HS5_A943Contagem_PFB[0];
            n943Contagem_PFB = P00HS5_n943Contagem_PFB[0];
            A1059Contagem_Notas = P00HS5_A1059Contagem_Notas[0];
            n1059Contagem_Notas = P00HS5_n1059Contagem_Notas[0];
            A196Contagem_Tipo = P00HS5_A196Contagem_Tipo[0];
            n196Contagem_Tipo = P00HS5_n196Contagem_Tipo[0];
            A195Contagem_Tecnica = P00HS5_A195Contagem_Tecnica[0];
            n195Contagem_Tecnica = P00HS5_n195Contagem_Tecnica[0];
            A197Contagem_DataCriacao = P00HS5_A197Contagem_DataCriacao[0];
            A193Contagem_AreaTrabalhoCod = P00HS5_A193Contagem_AreaTrabalhoCod[0];
            A192Contagem_Codigo = P00HS5_A192Contagem_Codigo[0];
            A215Contagem_UsuarioContadorPessoaNom = P00HS5_A215Contagem_UsuarioContadorPessoaNom[0];
            n215Contagem_UsuarioContadorPessoaNom = P00HS5_n215Contagem_UsuarioContadorPessoaNom[0];
            A194Contagem_AreaTrabalhoDes = P00HS5_A194Contagem_AreaTrabalhoDes[0];
            n194Contagem_AreaTrabalhoDes = P00HS5_n194Contagem_AreaTrabalhoDes[0];
            A1118Contagem_ContratadaCod = P00HS5_A1118Contagem_ContratadaCod[0];
            n1118Contagem_ContratadaCod = P00HS5_n1118Contagem_ContratadaCod[0];
            A941Contagem_SistemaSigla = P00HS5_A941Contagem_SistemaSigla[0];
            n941Contagem_SistemaSigla = P00HS5_n941Contagem_SistemaSigla[0];
            A214Contagem_UsuarioContadorPessoaCod = P00HS5_A214Contagem_UsuarioContadorPessoaCod[0];
            n214Contagem_UsuarioContadorPessoaCod = P00HS5_n214Contagem_UsuarioContadorPessoaCod[0];
            A215Contagem_UsuarioContadorPessoaNom = P00HS5_A215Contagem_UsuarioContadorPessoaNom[0];
            n215Contagem_UsuarioContadorPessoaNom = P00HS5_n215Contagem_UsuarioContadorPessoaNom[0];
            A194Contagem_AreaTrabalhoDes = P00HS5_A194Contagem_AreaTrabalhoDes[0];
            n194Contagem_AreaTrabalhoDes = P00HS5_n194Contagem_AreaTrabalhoDes[0];
            AV87count = 0;
            while ( (pr_default.getStatus(3) != 101) && ( StringUtil.StrCmp(P00HS5_A945Contagem_Demanda[0], A945Contagem_Demanda) == 0 ) )
            {
               BRKHS8 = false;
               A192Contagem_Codigo = P00HS5_A192Contagem_Codigo[0];
               AV87count = (long)(AV87count+1);
               BRKHS8 = true;
               pr_default.readNext(3);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A945Contagem_Demanda)) )
            {
               AV79Option = A945Contagem_Demanda;
               AV80Options.Add(AV79Option, 0);
               AV85OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV87count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV80Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKHS8 )
            {
               BRKHS8 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      protected void S161( )
      {
         /* 'LOADCONTAGEM_LINKOPTIONS' Routine */
         AV42TFContagem_Link = AV75SearchTxt;
         AV43TFContagem_Link_Sel = "";
         AV114WWContagemDS_1_Dynamicfiltersselector1 = AV93DynamicFiltersSelector1;
         AV115WWContagemDS_2_Dynamicfiltersoperator1 = AV94DynamicFiltersOperator1;
         AV116WWContagemDS_3_Contagem_contratadacod1 = AV95Contagem_ContratadaCod1;
         AV117WWContagemDS_4_Contagem_areatrabalhodes1 = AV96Contagem_AreaTrabalhoDes1;
         AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 = AV97Contagem_UsuarioContadorPessoaNom1;
         AV119WWContagemDS_6_Dynamicfiltersenabled2 = AV98DynamicFiltersEnabled2;
         AV120WWContagemDS_7_Dynamicfiltersselector2 = AV99DynamicFiltersSelector2;
         AV121WWContagemDS_8_Dynamicfiltersoperator2 = AV100DynamicFiltersOperator2;
         AV122WWContagemDS_9_Contagem_contratadacod2 = AV101Contagem_ContratadaCod2;
         AV123WWContagemDS_10_Contagem_areatrabalhodes2 = AV102Contagem_AreaTrabalhoDes2;
         AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 = AV103Contagem_UsuarioContadorPessoaNom2;
         AV125WWContagemDS_12_Dynamicfiltersenabled3 = AV104DynamicFiltersEnabled3;
         AV126WWContagemDS_13_Dynamicfiltersselector3 = AV105DynamicFiltersSelector3;
         AV127WWContagemDS_14_Dynamicfiltersoperator3 = AV106DynamicFiltersOperator3;
         AV128WWContagemDS_15_Contagem_contratadacod3 = AV107Contagem_ContratadaCod3;
         AV129WWContagemDS_16_Contagem_areatrabalhodes3 = AV108Contagem_AreaTrabalhoDes3;
         AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 = AV109Contagem_UsuarioContadorPessoaNom3;
         AV131WWContagemDS_18_Tfcontagem_codigo = AV10TFContagem_Codigo;
         AV132WWContagemDS_19_Tfcontagem_codigo_to = AV11TFContagem_Codigo_To;
         AV133WWContagemDS_20_Tfcontagem_areatrabalhocod = AV12TFContagem_AreaTrabalhoCod;
         AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to = AV13TFContagem_AreaTrabalhoCod_To;
         AV135WWContagemDS_22_Tfcontagem_contratadacod = AV14TFContagem_ContratadaCod;
         AV136WWContagemDS_23_Tfcontagem_contratadacod_to = AV15TFContagem_ContratadaCod_To;
         AV137WWContagemDS_24_Tfcontagem_datacriacao = AV16TFContagem_DataCriacao;
         AV138WWContagemDS_25_Tfcontagem_datacriacao_to = AV17TFContagem_DataCriacao_To;
         AV139WWContagemDS_26_Tfcontagem_areatrabalhodes = AV18TFContagem_AreaTrabalhoDes;
         AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel = AV19TFContagem_AreaTrabalhoDes_Sel;
         AV141WWContagemDS_28_Tfcontagem_tecnica_sels = AV21TFContagem_Tecnica_Sels;
         AV142WWContagemDS_29_Tfcontagem_tipo_sels = AV23TFContagem_Tipo_Sels;
         AV143WWContagemDS_30_Tfcontagem_notas = AV24TFContagem_Notas;
         AV144WWContagemDS_31_Tfcontagem_notas_sel = AV25TFContagem_Notas_Sel;
         AV145WWContagemDS_32_Tfcontagem_pfb = AV26TFContagem_PFB;
         AV146WWContagemDS_33_Tfcontagem_pfb_to = AV27TFContagem_PFB_To;
         AV147WWContagemDS_34_Tfcontagem_pfl = AV28TFContagem_PFL;
         AV148WWContagemDS_35_Tfcontagem_pfl_to = AV29TFContagem_PFL_To;
         AV149WWContagemDS_36_Tfcontagem_divergencia = AV30TFContagem_Divergencia;
         AV150WWContagemDS_37_Tfcontagem_divergencia_to = AV31TFContagem_Divergencia_To;
         AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod = AV32TFContagem_UsuarioContadorCod;
         AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to = AV33TFContagem_UsuarioContadorCod_To;
         AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod = AV34TFContagem_UsuarioContadorPessoaCod;
         AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to = AV35TFContagem_UsuarioContadorPessoaCod_To;
         AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom = AV36TFContagem_UsuarioContadorPessoaNom;
         AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel = AV37TFContagem_UsuarioContadorPessoaNom_Sel;
         AV157WWContagemDS_44_Tfcontagem_status_sels = AV39TFContagem_Status_Sels;
         AV158WWContagemDS_45_Tfcontagem_demanda = AV40TFContagem_Demanda;
         AV159WWContagemDS_46_Tfcontagem_demanda_sel = AV41TFContagem_Demanda_Sel;
         AV160WWContagemDS_47_Tfcontagem_link = AV42TFContagem_Link;
         AV161WWContagemDS_48_Tfcontagem_link_sel = AV43TFContagem_Link_Sel;
         AV162WWContagemDS_49_Tfcontagem_fator = AV44TFContagem_Fator;
         AV163WWContagemDS_50_Tfcontagem_fator_to = AV45TFContagem_Fator_To;
         AV164WWContagemDS_51_Tfcontagem_deflator = AV46TFContagem_Deflator;
         AV165WWContagemDS_52_Tfcontagem_deflator_to = AV47TFContagem_Deflator_To;
         AV166WWContagemDS_53_Tfcontagem_sistemacod = AV48TFContagem_SistemaCod;
         AV167WWContagemDS_54_Tfcontagem_sistemacod_to = AV49TFContagem_SistemaCod_To;
         AV168WWContagemDS_55_Tfcontagem_sistemasigla = AV50TFContagem_SistemaSigla;
         AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel = AV51TFContagem_SistemaSigla_Sel;
         pr_default.dynParam(4, new Object[]{ new Object[]{
                                              A195Contagem_Tecnica ,
                                              AV141WWContagemDS_28_Tfcontagem_tecnica_sels ,
                                              A196Contagem_Tipo ,
                                              AV142WWContagemDS_29_Tfcontagem_tipo_sels ,
                                              A262Contagem_Status ,
                                              AV157WWContagemDS_44_Tfcontagem_status_sels ,
                                              AV114WWContagemDS_1_Dynamicfiltersselector1 ,
                                              AV115WWContagemDS_2_Dynamicfiltersoperator1 ,
                                              AV116WWContagemDS_3_Contagem_contratadacod1 ,
                                              AV117WWContagemDS_4_Contagem_areatrabalhodes1 ,
                                              AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 ,
                                              AV119WWContagemDS_6_Dynamicfiltersenabled2 ,
                                              AV120WWContagemDS_7_Dynamicfiltersselector2 ,
                                              AV121WWContagemDS_8_Dynamicfiltersoperator2 ,
                                              AV122WWContagemDS_9_Contagem_contratadacod2 ,
                                              AV123WWContagemDS_10_Contagem_areatrabalhodes2 ,
                                              AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 ,
                                              AV125WWContagemDS_12_Dynamicfiltersenabled3 ,
                                              AV126WWContagemDS_13_Dynamicfiltersselector3 ,
                                              AV127WWContagemDS_14_Dynamicfiltersoperator3 ,
                                              AV128WWContagemDS_15_Contagem_contratadacod3 ,
                                              AV129WWContagemDS_16_Contagem_areatrabalhodes3 ,
                                              AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 ,
                                              AV131WWContagemDS_18_Tfcontagem_codigo ,
                                              AV132WWContagemDS_19_Tfcontagem_codigo_to ,
                                              AV133WWContagemDS_20_Tfcontagem_areatrabalhocod ,
                                              AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to ,
                                              AV135WWContagemDS_22_Tfcontagem_contratadacod ,
                                              AV136WWContagemDS_23_Tfcontagem_contratadacod_to ,
                                              AV137WWContagemDS_24_Tfcontagem_datacriacao ,
                                              AV138WWContagemDS_25_Tfcontagem_datacriacao_to ,
                                              AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel ,
                                              AV139WWContagemDS_26_Tfcontagem_areatrabalhodes ,
                                              AV141WWContagemDS_28_Tfcontagem_tecnica_sels.Count ,
                                              AV142WWContagemDS_29_Tfcontagem_tipo_sels.Count ,
                                              AV144WWContagemDS_31_Tfcontagem_notas_sel ,
                                              AV143WWContagemDS_30_Tfcontagem_notas ,
                                              AV145WWContagemDS_32_Tfcontagem_pfb ,
                                              AV146WWContagemDS_33_Tfcontagem_pfb_to ,
                                              AV147WWContagemDS_34_Tfcontagem_pfl ,
                                              AV148WWContagemDS_35_Tfcontagem_pfl_to ,
                                              AV149WWContagemDS_36_Tfcontagem_divergencia ,
                                              AV150WWContagemDS_37_Tfcontagem_divergencia_to ,
                                              AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod ,
                                              AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to ,
                                              AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod ,
                                              AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to ,
                                              AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel ,
                                              AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom ,
                                              AV157WWContagemDS_44_Tfcontagem_status_sels.Count ,
                                              AV159WWContagemDS_46_Tfcontagem_demanda_sel ,
                                              AV158WWContagemDS_45_Tfcontagem_demanda ,
                                              AV161WWContagemDS_48_Tfcontagem_link_sel ,
                                              AV160WWContagemDS_47_Tfcontagem_link ,
                                              AV162WWContagemDS_49_Tfcontagem_fator ,
                                              AV163WWContagemDS_50_Tfcontagem_fator_to ,
                                              AV164WWContagemDS_51_Tfcontagem_deflator ,
                                              AV165WWContagemDS_52_Tfcontagem_deflator_to ,
                                              AV166WWContagemDS_53_Tfcontagem_sistemacod ,
                                              AV167WWContagemDS_54_Tfcontagem_sistemacod_to ,
                                              AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel ,
                                              AV168WWContagemDS_55_Tfcontagem_sistemasigla ,
                                              A1118Contagem_ContratadaCod ,
                                              A194Contagem_AreaTrabalhoDes ,
                                              A215Contagem_UsuarioContadorPessoaNom ,
                                              A192Contagem_Codigo ,
                                              A193Contagem_AreaTrabalhoCod ,
                                              A197Contagem_DataCriacao ,
                                              A1059Contagem_Notas ,
                                              A943Contagem_PFB ,
                                              A944Contagem_PFL ,
                                              A1119Contagem_Divergencia ,
                                              A213Contagem_UsuarioContadorCod ,
                                              A214Contagem_UsuarioContadorPessoaCod ,
                                              A945Contagem_Demanda ,
                                              A946Contagem_Link ,
                                              A947Contagem_Fator ,
                                              A1117Contagem_Deflator ,
                                              A940Contagem_SistemaCod ,
                                              A941Contagem_SistemaSigla },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV117WWContagemDS_4_Contagem_areatrabalhodes1 = StringUtil.Concat( StringUtil.RTrim( AV117WWContagemDS_4_Contagem_areatrabalhodes1), "%", "");
         lV117WWContagemDS_4_Contagem_areatrabalhodes1 = StringUtil.Concat( StringUtil.RTrim( AV117WWContagemDS_4_Contagem_areatrabalhodes1), "%", "");
         lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1), 100, "%");
         lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1), 100, "%");
         lV123WWContagemDS_10_Contagem_areatrabalhodes2 = StringUtil.Concat( StringUtil.RTrim( AV123WWContagemDS_10_Contagem_areatrabalhodes2), "%", "");
         lV123WWContagemDS_10_Contagem_areatrabalhodes2 = StringUtil.Concat( StringUtil.RTrim( AV123WWContagemDS_10_Contagem_areatrabalhodes2), "%", "");
         lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2), 100, "%");
         lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2), 100, "%");
         lV129WWContagemDS_16_Contagem_areatrabalhodes3 = StringUtil.Concat( StringUtil.RTrim( AV129WWContagemDS_16_Contagem_areatrabalhodes3), "%", "");
         lV129WWContagemDS_16_Contagem_areatrabalhodes3 = StringUtil.Concat( StringUtil.RTrim( AV129WWContagemDS_16_Contagem_areatrabalhodes3), "%", "");
         lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3), 100, "%");
         lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3), 100, "%");
         lV139WWContagemDS_26_Tfcontagem_areatrabalhodes = StringUtil.Concat( StringUtil.RTrim( AV139WWContagemDS_26_Tfcontagem_areatrabalhodes), "%", "");
         lV143WWContagemDS_30_Tfcontagem_notas = StringUtil.Concat( StringUtil.RTrim( AV143WWContagemDS_30_Tfcontagem_notas), "%", "");
         lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom = StringUtil.PadR( StringUtil.RTrim( AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom), 100, "%");
         lV158WWContagemDS_45_Tfcontagem_demanda = StringUtil.Concat( StringUtil.RTrim( AV158WWContagemDS_45_Tfcontagem_demanda), "%", "");
         lV160WWContagemDS_47_Tfcontagem_link = StringUtil.Concat( StringUtil.RTrim( AV160WWContagemDS_47_Tfcontagem_link), "%", "");
         lV168WWContagemDS_55_Tfcontagem_sistemasigla = StringUtil.PadR( StringUtil.RTrim( AV168WWContagemDS_55_Tfcontagem_sistemasigla), 25, "%");
         /* Using cursor P00HS6 */
         pr_default.execute(4, new Object[] {AV116WWContagemDS_3_Contagem_contratadacod1, AV116WWContagemDS_3_Contagem_contratadacod1, AV116WWContagemDS_3_Contagem_contratadacod1, lV117WWContagemDS_4_Contagem_areatrabalhodes1, lV117WWContagemDS_4_Contagem_areatrabalhodes1, lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1, lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1, AV122WWContagemDS_9_Contagem_contratadacod2, AV122WWContagemDS_9_Contagem_contratadacod2, AV122WWContagemDS_9_Contagem_contratadacod2, lV123WWContagemDS_10_Contagem_areatrabalhodes2, lV123WWContagemDS_10_Contagem_areatrabalhodes2, lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2, lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2, AV128WWContagemDS_15_Contagem_contratadacod3, AV128WWContagemDS_15_Contagem_contratadacod3, AV128WWContagemDS_15_Contagem_contratadacod3, lV129WWContagemDS_16_Contagem_areatrabalhodes3, lV129WWContagemDS_16_Contagem_areatrabalhodes3, lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3, lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3, AV131WWContagemDS_18_Tfcontagem_codigo, AV132WWContagemDS_19_Tfcontagem_codigo_to, AV133WWContagemDS_20_Tfcontagem_areatrabalhocod, AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to, AV135WWContagemDS_22_Tfcontagem_contratadacod, AV136WWContagemDS_23_Tfcontagem_contratadacod_to, AV137WWContagemDS_24_Tfcontagem_datacriacao, AV138WWContagemDS_25_Tfcontagem_datacriacao_to, lV139WWContagemDS_26_Tfcontagem_areatrabalhodes, AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel, lV143WWContagemDS_30_Tfcontagem_notas, AV144WWContagemDS_31_Tfcontagem_notas_sel, AV145WWContagemDS_32_Tfcontagem_pfb, AV146WWContagemDS_33_Tfcontagem_pfb_to, AV147WWContagemDS_34_Tfcontagem_pfl, AV148WWContagemDS_35_Tfcontagem_pfl_to, AV149WWContagemDS_36_Tfcontagem_divergencia, AV150WWContagemDS_37_Tfcontagem_divergencia_to, AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod, AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to, AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod, AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to, lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom, AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel, lV158WWContagemDS_45_Tfcontagem_demanda, AV159WWContagemDS_46_Tfcontagem_demanda_sel, lV160WWContagemDS_47_Tfcontagem_link, AV161WWContagemDS_48_Tfcontagem_link_sel, AV162WWContagemDS_49_Tfcontagem_fator, AV163WWContagemDS_50_Tfcontagem_fator_to, AV164WWContagemDS_51_Tfcontagem_deflator, AV165WWContagemDS_52_Tfcontagem_deflator_to, AV166WWContagemDS_53_Tfcontagem_sistemacod, AV167WWContagemDS_54_Tfcontagem_sistemacod_to, lV168WWContagemDS_55_Tfcontagem_sistemasigla, AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel});
         while ( (pr_default.getStatus(4) != 101) )
         {
            BRKHS10 = false;
            A946Contagem_Link = P00HS6_A946Contagem_Link[0];
            n946Contagem_Link = P00HS6_n946Contagem_Link[0];
            A941Contagem_SistemaSigla = P00HS6_A941Contagem_SistemaSigla[0];
            n941Contagem_SistemaSigla = P00HS6_n941Contagem_SistemaSigla[0];
            A940Contagem_SistemaCod = P00HS6_A940Contagem_SistemaCod[0];
            n940Contagem_SistemaCod = P00HS6_n940Contagem_SistemaCod[0];
            A1117Contagem_Deflator = P00HS6_A1117Contagem_Deflator[0];
            n1117Contagem_Deflator = P00HS6_n1117Contagem_Deflator[0];
            A947Contagem_Fator = P00HS6_A947Contagem_Fator[0];
            n947Contagem_Fator = P00HS6_n947Contagem_Fator[0];
            A945Contagem_Demanda = P00HS6_A945Contagem_Demanda[0];
            n945Contagem_Demanda = P00HS6_n945Contagem_Demanda[0];
            A262Contagem_Status = P00HS6_A262Contagem_Status[0];
            n262Contagem_Status = P00HS6_n262Contagem_Status[0];
            A214Contagem_UsuarioContadorPessoaCod = P00HS6_A214Contagem_UsuarioContadorPessoaCod[0];
            n214Contagem_UsuarioContadorPessoaCod = P00HS6_n214Contagem_UsuarioContadorPessoaCod[0];
            A213Contagem_UsuarioContadorCod = P00HS6_A213Contagem_UsuarioContadorCod[0];
            n213Contagem_UsuarioContadorCod = P00HS6_n213Contagem_UsuarioContadorCod[0];
            A1119Contagem_Divergencia = P00HS6_A1119Contagem_Divergencia[0];
            n1119Contagem_Divergencia = P00HS6_n1119Contagem_Divergencia[0];
            A944Contagem_PFL = P00HS6_A944Contagem_PFL[0];
            n944Contagem_PFL = P00HS6_n944Contagem_PFL[0];
            A943Contagem_PFB = P00HS6_A943Contagem_PFB[0];
            n943Contagem_PFB = P00HS6_n943Contagem_PFB[0];
            A1059Contagem_Notas = P00HS6_A1059Contagem_Notas[0];
            n1059Contagem_Notas = P00HS6_n1059Contagem_Notas[0];
            A196Contagem_Tipo = P00HS6_A196Contagem_Tipo[0];
            n196Contagem_Tipo = P00HS6_n196Contagem_Tipo[0];
            A195Contagem_Tecnica = P00HS6_A195Contagem_Tecnica[0];
            n195Contagem_Tecnica = P00HS6_n195Contagem_Tecnica[0];
            A197Contagem_DataCriacao = P00HS6_A197Contagem_DataCriacao[0];
            A193Contagem_AreaTrabalhoCod = P00HS6_A193Contagem_AreaTrabalhoCod[0];
            A192Contagem_Codigo = P00HS6_A192Contagem_Codigo[0];
            A215Contagem_UsuarioContadorPessoaNom = P00HS6_A215Contagem_UsuarioContadorPessoaNom[0];
            n215Contagem_UsuarioContadorPessoaNom = P00HS6_n215Contagem_UsuarioContadorPessoaNom[0];
            A194Contagem_AreaTrabalhoDes = P00HS6_A194Contagem_AreaTrabalhoDes[0];
            n194Contagem_AreaTrabalhoDes = P00HS6_n194Contagem_AreaTrabalhoDes[0];
            A1118Contagem_ContratadaCod = P00HS6_A1118Contagem_ContratadaCod[0];
            n1118Contagem_ContratadaCod = P00HS6_n1118Contagem_ContratadaCod[0];
            A941Contagem_SistemaSigla = P00HS6_A941Contagem_SistemaSigla[0];
            n941Contagem_SistemaSigla = P00HS6_n941Contagem_SistemaSigla[0];
            A214Contagem_UsuarioContadorPessoaCod = P00HS6_A214Contagem_UsuarioContadorPessoaCod[0];
            n214Contagem_UsuarioContadorPessoaCod = P00HS6_n214Contagem_UsuarioContadorPessoaCod[0];
            A215Contagem_UsuarioContadorPessoaNom = P00HS6_A215Contagem_UsuarioContadorPessoaNom[0];
            n215Contagem_UsuarioContadorPessoaNom = P00HS6_n215Contagem_UsuarioContadorPessoaNom[0];
            A194Contagem_AreaTrabalhoDes = P00HS6_A194Contagem_AreaTrabalhoDes[0];
            n194Contagem_AreaTrabalhoDes = P00HS6_n194Contagem_AreaTrabalhoDes[0];
            AV87count = 0;
            while ( (pr_default.getStatus(4) != 101) && ( StringUtil.StrCmp(P00HS6_A946Contagem_Link[0], A946Contagem_Link) == 0 ) )
            {
               BRKHS10 = false;
               A192Contagem_Codigo = P00HS6_A192Contagem_Codigo[0];
               AV87count = (long)(AV87count+1);
               BRKHS10 = true;
               pr_default.readNext(4);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A946Contagem_Link)) )
            {
               AV79Option = A946Contagem_Link;
               AV80Options.Add(AV79Option, 0);
               AV85OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV87count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV80Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKHS10 )
            {
               BRKHS10 = true;
               pr_default.readNext(4);
            }
         }
         pr_default.close(4);
      }

      protected void S171( )
      {
         /* 'LOADCONTAGEM_SISTEMASIGLAOPTIONS' Routine */
         AV50TFContagem_SistemaSigla = AV75SearchTxt;
         AV51TFContagem_SistemaSigla_Sel = "";
         AV114WWContagemDS_1_Dynamicfiltersselector1 = AV93DynamicFiltersSelector1;
         AV115WWContagemDS_2_Dynamicfiltersoperator1 = AV94DynamicFiltersOperator1;
         AV116WWContagemDS_3_Contagem_contratadacod1 = AV95Contagem_ContratadaCod1;
         AV117WWContagemDS_4_Contagem_areatrabalhodes1 = AV96Contagem_AreaTrabalhoDes1;
         AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 = AV97Contagem_UsuarioContadorPessoaNom1;
         AV119WWContagemDS_6_Dynamicfiltersenabled2 = AV98DynamicFiltersEnabled2;
         AV120WWContagemDS_7_Dynamicfiltersselector2 = AV99DynamicFiltersSelector2;
         AV121WWContagemDS_8_Dynamicfiltersoperator2 = AV100DynamicFiltersOperator2;
         AV122WWContagemDS_9_Contagem_contratadacod2 = AV101Contagem_ContratadaCod2;
         AV123WWContagemDS_10_Contagem_areatrabalhodes2 = AV102Contagem_AreaTrabalhoDes2;
         AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 = AV103Contagem_UsuarioContadorPessoaNom2;
         AV125WWContagemDS_12_Dynamicfiltersenabled3 = AV104DynamicFiltersEnabled3;
         AV126WWContagemDS_13_Dynamicfiltersselector3 = AV105DynamicFiltersSelector3;
         AV127WWContagemDS_14_Dynamicfiltersoperator3 = AV106DynamicFiltersOperator3;
         AV128WWContagemDS_15_Contagem_contratadacod3 = AV107Contagem_ContratadaCod3;
         AV129WWContagemDS_16_Contagem_areatrabalhodes3 = AV108Contagem_AreaTrabalhoDes3;
         AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 = AV109Contagem_UsuarioContadorPessoaNom3;
         AV131WWContagemDS_18_Tfcontagem_codigo = AV10TFContagem_Codigo;
         AV132WWContagemDS_19_Tfcontagem_codigo_to = AV11TFContagem_Codigo_To;
         AV133WWContagemDS_20_Tfcontagem_areatrabalhocod = AV12TFContagem_AreaTrabalhoCod;
         AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to = AV13TFContagem_AreaTrabalhoCod_To;
         AV135WWContagemDS_22_Tfcontagem_contratadacod = AV14TFContagem_ContratadaCod;
         AV136WWContagemDS_23_Tfcontagem_contratadacod_to = AV15TFContagem_ContratadaCod_To;
         AV137WWContagemDS_24_Tfcontagem_datacriacao = AV16TFContagem_DataCriacao;
         AV138WWContagemDS_25_Tfcontagem_datacriacao_to = AV17TFContagem_DataCriacao_To;
         AV139WWContagemDS_26_Tfcontagem_areatrabalhodes = AV18TFContagem_AreaTrabalhoDes;
         AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel = AV19TFContagem_AreaTrabalhoDes_Sel;
         AV141WWContagemDS_28_Tfcontagem_tecnica_sels = AV21TFContagem_Tecnica_Sels;
         AV142WWContagemDS_29_Tfcontagem_tipo_sels = AV23TFContagem_Tipo_Sels;
         AV143WWContagemDS_30_Tfcontagem_notas = AV24TFContagem_Notas;
         AV144WWContagemDS_31_Tfcontagem_notas_sel = AV25TFContagem_Notas_Sel;
         AV145WWContagemDS_32_Tfcontagem_pfb = AV26TFContagem_PFB;
         AV146WWContagemDS_33_Tfcontagem_pfb_to = AV27TFContagem_PFB_To;
         AV147WWContagemDS_34_Tfcontagem_pfl = AV28TFContagem_PFL;
         AV148WWContagemDS_35_Tfcontagem_pfl_to = AV29TFContagem_PFL_To;
         AV149WWContagemDS_36_Tfcontagem_divergencia = AV30TFContagem_Divergencia;
         AV150WWContagemDS_37_Tfcontagem_divergencia_to = AV31TFContagem_Divergencia_To;
         AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod = AV32TFContagem_UsuarioContadorCod;
         AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to = AV33TFContagem_UsuarioContadorCod_To;
         AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod = AV34TFContagem_UsuarioContadorPessoaCod;
         AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to = AV35TFContagem_UsuarioContadorPessoaCod_To;
         AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom = AV36TFContagem_UsuarioContadorPessoaNom;
         AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel = AV37TFContagem_UsuarioContadorPessoaNom_Sel;
         AV157WWContagemDS_44_Tfcontagem_status_sels = AV39TFContagem_Status_Sels;
         AV158WWContagemDS_45_Tfcontagem_demanda = AV40TFContagem_Demanda;
         AV159WWContagemDS_46_Tfcontagem_demanda_sel = AV41TFContagem_Demanda_Sel;
         AV160WWContagemDS_47_Tfcontagem_link = AV42TFContagem_Link;
         AV161WWContagemDS_48_Tfcontagem_link_sel = AV43TFContagem_Link_Sel;
         AV162WWContagemDS_49_Tfcontagem_fator = AV44TFContagem_Fator;
         AV163WWContagemDS_50_Tfcontagem_fator_to = AV45TFContagem_Fator_To;
         AV164WWContagemDS_51_Tfcontagem_deflator = AV46TFContagem_Deflator;
         AV165WWContagemDS_52_Tfcontagem_deflator_to = AV47TFContagem_Deflator_To;
         AV166WWContagemDS_53_Tfcontagem_sistemacod = AV48TFContagem_SistemaCod;
         AV167WWContagemDS_54_Tfcontagem_sistemacod_to = AV49TFContagem_SistemaCod_To;
         AV168WWContagemDS_55_Tfcontagem_sistemasigla = AV50TFContagem_SistemaSigla;
         AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel = AV51TFContagem_SistemaSigla_Sel;
         pr_default.dynParam(5, new Object[]{ new Object[]{
                                              A195Contagem_Tecnica ,
                                              AV141WWContagemDS_28_Tfcontagem_tecnica_sels ,
                                              A196Contagem_Tipo ,
                                              AV142WWContagemDS_29_Tfcontagem_tipo_sels ,
                                              A262Contagem_Status ,
                                              AV157WWContagemDS_44_Tfcontagem_status_sels ,
                                              AV114WWContagemDS_1_Dynamicfiltersselector1 ,
                                              AV115WWContagemDS_2_Dynamicfiltersoperator1 ,
                                              AV116WWContagemDS_3_Contagem_contratadacod1 ,
                                              AV117WWContagemDS_4_Contagem_areatrabalhodes1 ,
                                              AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 ,
                                              AV119WWContagemDS_6_Dynamicfiltersenabled2 ,
                                              AV120WWContagemDS_7_Dynamicfiltersselector2 ,
                                              AV121WWContagemDS_8_Dynamicfiltersoperator2 ,
                                              AV122WWContagemDS_9_Contagem_contratadacod2 ,
                                              AV123WWContagemDS_10_Contagem_areatrabalhodes2 ,
                                              AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 ,
                                              AV125WWContagemDS_12_Dynamicfiltersenabled3 ,
                                              AV126WWContagemDS_13_Dynamicfiltersselector3 ,
                                              AV127WWContagemDS_14_Dynamicfiltersoperator3 ,
                                              AV128WWContagemDS_15_Contagem_contratadacod3 ,
                                              AV129WWContagemDS_16_Contagem_areatrabalhodes3 ,
                                              AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 ,
                                              AV131WWContagemDS_18_Tfcontagem_codigo ,
                                              AV132WWContagemDS_19_Tfcontagem_codigo_to ,
                                              AV133WWContagemDS_20_Tfcontagem_areatrabalhocod ,
                                              AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to ,
                                              AV135WWContagemDS_22_Tfcontagem_contratadacod ,
                                              AV136WWContagemDS_23_Tfcontagem_contratadacod_to ,
                                              AV137WWContagemDS_24_Tfcontagem_datacriacao ,
                                              AV138WWContagemDS_25_Tfcontagem_datacriacao_to ,
                                              AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel ,
                                              AV139WWContagemDS_26_Tfcontagem_areatrabalhodes ,
                                              AV141WWContagemDS_28_Tfcontagem_tecnica_sels.Count ,
                                              AV142WWContagemDS_29_Tfcontagem_tipo_sels.Count ,
                                              AV144WWContagemDS_31_Tfcontagem_notas_sel ,
                                              AV143WWContagemDS_30_Tfcontagem_notas ,
                                              AV145WWContagemDS_32_Tfcontagem_pfb ,
                                              AV146WWContagemDS_33_Tfcontagem_pfb_to ,
                                              AV147WWContagemDS_34_Tfcontagem_pfl ,
                                              AV148WWContagemDS_35_Tfcontagem_pfl_to ,
                                              AV149WWContagemDS_36_Tfcontagem_divergencia ,
                                              AV150WWContagemDS_37_Tfcontagem_divergencia_to ,
                                              AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod ,
                                              AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to ,
                                              AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod ,
                                              AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to ,
                                              AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel ,
                                              AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom ,
                                              AV157WWContagemDS_44_Tfcontagem_status_sels.Count ,
                                              AV159WWContagemDS_46_Tfcontagem_demanda_sel ,
                                              AV158WWContagemDS_45_Tfcontagem_demanda ,
                                              AV161WWContagemDS_48_Tfcontagem_link_sel ,
                                              AV160WWContagemDS_47_Tfcontagem_link ,
                                              AV162WWContagemDS_49_Tfcontagem_fator ,
                                              AV163WWContagemDS_50_Tfcontagem_fator_to ,
                                              AV164WWContagemDS_51_Tfcontagem_deflator ,
                                              AV165WWContagemDS_52_Tfcontagem_deflator_to ,
                                              AV166WWContagemDS_53_Tfcontagem_sistemacod ,
                                              AV167WWContagemDS_54_Tfcontagem_sistemacod_to ,
                                              AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel ,
                                              AV168WWContagemDS_55_Tfcontagem_sistemasigla ,
                                              A1118Contagem_ContratadaCod ,
                                              A194Contagem_AreaTrabalhoDes ,
                                              A215Contagem_UsuarioContadorPessoaNom ,
                                              A192Contagem_Codigo ,
                                              A193Contagem_AreaTrabalhoCod ,
                                              A197Contagem_DataCriacao ,
                                              A1059Contagem_Notas ,
                                              A943Contagem_PFB ,
                                              A944Contagem_PFL ,
                                              A1119Contagem_Divergencia ,
                                              A213Contagem_UsuarioContadorCod ,
                                              A214Contagem_UsuarioContadorPessoaCod ,
                                              A945Contagem_Demanda ,
                                              A946Contagem_Link ,
                                              A947Contagem_Fator ,
                                              A1117Contagem_Deflator ,
                                              A940Contagem_SistemaCod ,
                                              A941Contagem_SistemaSigla },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV117WWContagemDS_4_Contagem_areatrabalhodes1 = StringUtil.Concat( StringUtil.RTrim( AV117WWContagemDS_4_Contagem_areatrabalhodes1), "%", "");
         lV117WWContagemDS_4_Contagem_areatrabalhodes1 = StringUtil.Concat( StringUtil.RTrim( AV117WWContagemDS_4_Contagem_areatrabalhodes1), "%", "");
         lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1), 100, "%");
         lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1), 100, "%");
         lV123WWContagemDS_10_Contagem_areatrabalhodes2 = StringUtil.Concat( StringUtil.RTrim( AV123WWContagemDS_10_Contagem_areatrabalhodes2), "%", "");
         lV123WWContagemDS_10_Contagem_areatrabalhodes2 = StringUtil.Concat( StringUtil.RTrim( AV123WWContagemDS_10_Contagem_areatrabalhodes2), "%", "");
         lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2), 100, "%");
         lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2), 100, "%");
         lV129WWContagemDS_16_Contagem_areatrabalhodes3 = StringUtil.Concat( StringUtil.RTrim( AV129WWContagemDS_16_Contagem_areatrabalhodes3), "%", "");
         lV129WWContagemDS_16_Contagem_areatrabalhodes3 = StringUtil.Concat( StringUtil.RTrim( AV129WWContagemDS_16_Contagem_areatrabalhodes3), "%", "");
         lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3), 100, "%");
         lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3), 100, "%");
         lV139WWContagemDS_26_Tfcontagem_areatrabalhodes = StringUtil.Concat( StringUtil.RTrim( AV139WWContagemDS_26_Tfcontagem_areatrabalhodes), "%", "");
         lV143WWContagemDS_30_Tfcontagem_notas = StringUtil.Concat( StringUtil.RTrim( AV143WWContagemDS_30_Tfcontagem_notas), "%", "");
         lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom = StringUtil.PadR( StringUtil.RTrim( AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom), 100, "%");
         lV158WWContagemDS_45_Tfcontagem_demanda = StringUtil.Concat( StringUtil.RTrim( AV158WWContagemDS_45_Tfcontagem_demanda), "%", "");
         lV160WWContagemDS_47_Tfcontagem_link = StringUtil.Concat( StringUtil.RTrim( AV160WWContagemDS_47_Tfcontagem_link), "%", "");
         lV168WWContagemDS_55_Tfcontagem_sistemasigla = StringUtil.PadR( StringUtil.RTrim( AV168WWContagemDS_55_Tfcontagem_sistemasigla), 25, "%");
         /* Using cursor P00HS7 */
         pr_default.execute(5, new Object[] {AV116WWContagemDS_3_Contagem_contratadacod1, AV116WWContagemDS_3_Contagem_contratadacod1, AV116WWContagemDS_3_Contagem_contratadacod1, lV117WWContagemDS_4_Contagem_areatrabalhodes1, lV117WWContagemDS_4_Contagem_areatrabalhodes1, lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1, lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1, AV122WWContagemDS_9_Contagem_contratadacod2, AV122WWContagemDS_9_Contagem_contratadacod2, AV122WWContagemDS_9_Contagem_contratadacod2, lV123WWContagemDS_10_Contagem_areatrabalhodes2, lV123WWContagemDS_10_Contagem_areatrabalhodes2, lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2, lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2, AV128WWContagemDS_15_Contagem_contratadacod3, AV128WWContagemDS_15_Contagem_contratadacod3, AV128WWContagemDS_15_Contagem_contratadacod3, lV129WWContagemDS_16_Contagem_areatrabalhodes3, lV129WWContagemDS_16_Contagem_areatrabalhodes3, lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3, lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3, AV131WWContagemDS_18_Tfcontagem_codigo, AV132WWContagemDS_19_Tfcontagem_codigo_to, AV133WWContagemDS_20_Tfcontagem_areatrabalhocod, AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to, AV135WWContagemDS_22_Tfcontagem_contratadacod, AV136WWContagemDS_23_Tfcontagem_contratadacod_to, AV137WWContagemDS_24_Tfcontagem_datacriacao, AV138WWContagemDS_25_Tfcontagem_datacriacao_to, lV139WWContagemDS_26_Tfcontagem_areatrabalhodes, AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel, lV143WWContagemDS_30_Tfcontagem_notas, AV144WWContagemDS_31_Tfcontagem_notas_sel, AV145WWContagemDS_32_Tfcontagem_pfb, AV146WWContagemDS_33_Tfcontagem_pfb_to, AV147WWContagemDS_34_Tfcontagem_pfl, AV148WWContagemDS_35_Tfcontagem_pfl_to, AV149WWContagemDS_36_Tfcontagem_divergencia, AV150WWContagemDS_37_Tfcontagem_divergencia_to, AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod, AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to, AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod, AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to, lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom, AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel, lV158WWContagemDS_45_Tfcontagem_demanda, AV159WWContagemDS_46_Tfcontagem_demanda_sel, lV160WWContagemDS_47_Tfcontagem_link, AV161WWContagemDS_48_Tfcontagem_link_sel, AV162WWContagemDS_49_Tfcontagem_fator, AV163WWContagemDS_50_Tfcontagem_fator_to, AV164WWContagemDS_51_Tfcontagem_deflator, AV165WWContagemDS_52_Tfcontagem_deflator_to, AV166WWContagemDS_53_Tfcontagem_sistemacod, AV167WWContagemDS_54_Tfcontagem_sistemacod_to, lV168WWContagemDS_55_Tfcontagem_sistemasigla, AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel});
         while ( (pr_default.getStatus(5) != 101) )
         {
            BRKHS12 = false;
            A941Contagem_SistemaSigla = P00HS7_A941Contagem_SistemaSigla[0];
            n941Contagem_SistemaSigla = P00HS7_n941Contagem_SistemaSigla[0];
            A940Contagem_SistemaCod = P00HS7_A940Contagem_SistemaCod[0];
            n940Contagem_SistemaCod = P00HS7_n940Contagem_SistemaCod[0];
            A1117Contagem_Deflator = P00HS7_A1117Contagem_Deflator[0];
            n1117Contagem_Deflator = P00HS7_n1117Contagem_Deflator[0];
            A947Contagem_Fator = P00HS7_A947Contagem_Fator[0];
            n947Contagem_Fator = P00HS7_n947Contagem_Fator[0];
            A946Contagem_Link = P00HS7_A946Contagem_Link[0];
            n946Contagem_Link = P00HS7_n946Contagem_Link[0];
            A945Contagem_Demanda = P00HS7_A945Contagem_Demanda[0];
            n945Contagem_Demanda = P00HS7_n945Contagem_Demanda[0];
            A262Contagem_Status = P00HS7_A262Contagem_Status[0];
            n262Contagem_Status = P00HS7_n262Contagem_Status[0];
            A214Contagem_UsuarioContadorPessoaCod = P00HS7_A214Contagem_UsuarioContadorPessoaCod[0];
            n214Contagem_UsuarioContadorPessoaCod = P00HS7_n214Contagem_UsuarioContadorPessoaCod[0];
            A213Contagem_UsuarioContadorCod = P00HS7_A213Contagem_UsuarioContadorCod[0];
            n213Contagem_UsuarioContadorCod = P00HS7_n213Contagem_UsuarioContadorCod[0];
            A1119Contagem_Divergencia = P00HS7_A1119Contagem_Divergencia[0];
            n1119Contagem_Divergencia = P00HS7_n1119Contagem_Divergencia[0];
            A944Contagem_PFL = P00HS7_A944Contagem_PFL[0];
            n944Contagem_PFL = P00HS7_n944Contagem_PFL[0];
            A943Contagem_PFB = P00HS7_A943Contagem_PFB[0];
            n943Contagem_PFB = P00HS7_n943Contagem_PFB[0];
            A1059Contagem_Notas = P00HS7_A1059Contagem_Notas[0];
            n1059Contagem_Notas = P00HS7_n1059Contagem_Notas[0];
            A196Contagem_Tipo = P00HS7_A196Contagem_Tipo[0];
            n196Contagem_Tipo = P00HS7_n196Contagem_Tipo[0];
            A195Contagem_Tecnica = P00HS7_A195Contagem_Tecnica[0];
            n195Contagem_Tecnica = P00HS7_n195Contagem_Tecnica[0];
            A197Contagem_DataCriacao = P00HS7_A197Contagem_DataCriacao[0];
            A193Contagem_AreaTrabalhoCod = P00HS7_A193Contagem_AreaTrabalhoCod[0];
            A192Contagem_Codigo = P00HS7_A192Contagem_Codigo[0];
            A215Contagem_UsuarioContadorPessoaNom = P00HS7_A215Contagem_UsuarioContadorPessoaNom[0];
            n215Contagem_UsuarioContadorPessoaNom = P00HS7_n215Contagem_UsuarioContadorPessoaNom[0];
            A194Contagem_AreaTrabalhoDes = P00HS7_A194Contagem_AreaTrabalhoDes[0];
            n194Contagem_AreaTrabalhoDes = P00HS7_n194Contagem_AreaTrabalhoDes[0];
            A1118Contagem_ContratadaCod = P00HS7_A1118Contagem_ContratadaCod[0];
            n1118Contagem_ContratadaCod = P00HS7_n1118Contagem_ContratadaCod[0];
            A941Contagem_SistemaSigla = P00HS7_A941Contagem_SistemaSigla[0];
            n941Contagem_SistemaSigla = P00HS7_n941Contagem_SistemaSigla[0];
            A214Contagem_UsuarioContadorPessoaCod = P00HS7_A214Contagem_UsuarioContadorPessoaCod[0];
            n214Contagem_UsuarioContadorPessoaCod = P00HS7_n214Contagem_UsuarioContadorPessoaCod[0];
            A215Contagem_UsuarioContadorPessoaNom = P00HS7_A215Contagem_UsuarioContadorPessoaNom[0];
            n215Contagem_UsuarioContadorPessoaNom = P00HS7_n215Contagem_UsuarioContadorPessoaNom[0];
            A194Contagem_AreaTrabalhoDes = P00HS7_A194Contagem_AreaTrabalhoDes[0];
            n194Contagem_AreaTrabalhoDes = P00HS7_n194Contagem_AreaTrabalhoDes[0];
            AV87count = 0;
            while ( (pr_default.getStatus(5) != 101) && ( StringUtil.StrCmp(P00HS7_A941Contagem_SistemaSigla[0], A941Contagem_SistemaSigla) == 0 ) )
            {
               BRKHS12 = false;
               A940Contagem_SistemaCod = P00HS7_A940Contagem_SistemaCod[0];
               n940Contagem_SistemaCod = P00HS7_n940Contagem_SistemaCod[0];
               A192Contagem_Codigo = P00HS7_A192Contagem_Codigo[0];
               AV87count = (long)(AV87count+1);
               BRKHS12 = true;
               pr_default.readNext(5);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A941Contagem_SistemaSigla)) )
            {
               AV79Option = A941Contagem_SistemaSigla;
               AV80Options.Add(AV79Option, 0);
               AV85OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV87count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV80Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKHS12 )
            {
               BRKHS12 = true;
               pr_default.readNext(5);
            }
         }
         pr_default.close(5);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV80Options = new GxSimpleCollection();
         AV83OptionsDesc = new GxSimpleCollection();
         AV85OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV88Session = context.GetSession();
         AV90GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV91GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV16TFContagem_DataCriacao = DateTime.MinValue;
         AV17TFContagem_DataCriacao_To = DateTime.MinValue;
         AV18TFContagem_AreaTrabalhoDes = "";
         AV19TFContagem_AreaTrabalhoDes_Sel = "";
         AV20TFContagem_Tecnica_SelsJson = "";
         AV21TFContagem_Tecnica_Sels = new GxSimpleCollection();
         AV22TFContagem_Tipo_SelsJson = "";
         AV23TFContagem_Tipo_Sels = new GxSimpleCollection();
         AV24TFContagem_Notas = "";
         AV25TFContagem_Notas_Sel = "";
         AV36TFContagem_UsuarioContadorPessoaNom = "";
         AV37TFContagem_UsuarioContadorPessoaNom_Sel = "";
         AV38TFContagem_Status_SelsJson = "";
         AV39TFContagem_Status_Sels = new GxSimpleCollection();
         AV40TFContagem_Demanda = "";
         AV41TFContagem_Demanda_Sel = "";
         AV42TFContagem_Link = "";
         AV43TFContagem_Link_Sel = "";
         AV50TFContagem_SistemaSigla = "";
         AV51TFContagem_SistemaSigla_Sel = "";
         AV92GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV93DynamicFiltersSelector1 = "";
         AV96Contagem_AreaTrabalhoDes1 = "";
         AV97Contagem_UsuarioContadorPessoaNom1 = "";
         AV99DynamicFiltersSelector2 = "";
         AV102Contagem_AreaTrabalhoDes2 = "";
         AV103Contagem_UsuarioContadorPessoaNom2 = "";
         AV105DynamicFiltersSelector3 = "";
         AV108Contagem_AreaTrabalhoDes3 = "";
         AV109Contagem_UsuarioContadorPessoaNom3 = "";
         AV114WWContagemDS_1_Dynamicfiltersselector1 = "";
         AV117WWContagemDS_4_Contagem_areatrabalhodes1 = "";
         AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 = "";
         AV120WWContagemDS_7_Dynamicfiltersselector2 = "";
         AV123WWContagemDS_10_Contagem_areatrabalhodes2 = "";
         AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 = "";
         AV126WWContagemDS_13_Dynamicfiltersselector3 = "";
         AV129WWContagemDS_16_Contagem_areatrabalhodes3 = "";
         AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 = "";
         AV137WWContagemDS_24_Tfcontagem_datacriacao = DateTime.MinValue;
         AV138WWContagemDS_25_Tfcontagem_datacriacao_to = DateTime.MinValue;
         AV139WWContagemDS_26_Tfcontagem_areatrabalhodes = "";
         AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel = "";
         AV141WWContagemDS_28_Tfcontagem_tecnica_sels = new GxSimpleCollection();
         AV142WWContagemDS_29_Tfcontagem_tipo_sels = new GxSimpleCollection();
         AV143WWContagemDS_30_Tfcontagem_notas = "";
         AV144WWContagemDS_31_Tfcontagem_notas_sel = "";
         AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom = "";
         AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel = "";
         AV157WWContagemDS_44_Tfcontagem_status_sels = new GxSimpleCollection();
         AV158WWContagemDS_45_Tfcontagem_demanda = "";
         AV159WWContagemDS_46_Tfcontagem_demanda_sel = "";
         AV160WWContagemDS_47_Tfcontagem_link = "";
         AV161WWContagemDS_48_Tfcontagem_link_sel = "";
         AV168WWContagemDS_55_Tfcontagem_sistemasigla = "";
         AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel = "";
         scmdbuf = "";
         lV117WWContagemDS_4_Contagem_areatrabalhodes1 = "";
         lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 = "";
         lV123WWContagemDS_10_Contagem_areatrabalhodes2 = "";
         lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 = "";
         lV129WWContagemDS_16_Contagem_areatrabalhodes3 = "";
         lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 = "";
         lV139WWContagemDS_26_Tfcontagem_areatrabalhodes = "";
         lV143WWContagemDS_30_Tfcontagem_notas = "";
         lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom = "";
         lV158WWContagemDS_45_Tfcontagem_demanda = "";
         lV160WWContagemDS_47_Tfcontagem_link = "";
         lV168WWContagemDS_55_Tfcontagem_sistemasigla = "";
         A195Contagem_Tecnica = "";
         A196Contagem_Tipo = "";
         A262Contagem_Status = "";
         A194Contagem_AreaTrabalhoDes = "";
         A215Contagem_UsuarioContadorPessoaNom = "";
         A197Contagem_DataCriacao = DateTime.MinValue;
         A1059Contagem_Notas = "";
         A945Contagem_Demanda = "";
         A946Contagem_Link = "";
         A941Contagem_SistemaSigla = "";
         P00HS2_A193Contagem_AreaTrabalhoCod = new int[1] ;
         P00HS2_A941Contagem_SistemaSigla = new String[] {""} ;
         P00HS2_n941Contagem_SistemaSigla = new bool[] {false} ;
         P00HS2_A940Contagem_SistemaCod = new int[1] ;
         P00HS2_n940Contagem_SistemaCod = new bool[] {false} ;
         P00HS2_A1117Contagem_Deflator = new decimal[1] ;
         P00HS2_n1117Contagem_Deflator = new bool[] {false} ;
         P00HS2_A947Contagem_Fator = new decimal[1] ;
         P00HS2_n947Contagem_Fator = new bool[] {false} ;
         P00HS2_A946Contagem_Link = new String[] {""} ;
         P00HS2_n946Contagem_Link = new bool[] {false} ;
         P00HS2_A945Contagem_Demanda = new String[] {""} ;
         P00HS2_n945Contagem_Demanda = new bool[] {false} ;
         P00HS2_A262Contagem_Status = new String[] {""} ;
         P00HS2_n262Contagem_Status = new bool[] {false} ;
         P00HS2_A214Contagem_UsuarioContadorPessoaCod = new int[1] ;
         P00HS2_n214Contagem_UsuarioContadorPessoaCod = new bool[] {false} ;
         P00HS2_A213Contagem_UsuarioContadorCod = new int[1] ;
         P00HS2_n213Contagem_UsuarioContadorCod = new bool[] {false} ;
         P00HS2_A1119Contagem_Divergencia = new decimal[1] ;
         P00HS2_n1119Contagem_Divergencia = new bool[] {false} ;
         P00HS2_A944Contagem_PFL = new decimal[1] ;
         P00HS2_n944Contagem_PFL = new bool[] {false} ;
         P00HS2_A943Contagem_PFB = new decimal[1] ;
         P00HS2_n943Contagem_PFB = new bool[] {false} ;
         P00HS2_A1059Contagem_Notas = new String[] {""} ;
         P00HS2_n1059Contagem_Notas = new bool[] {false} ;
         P00HS2_A196Contagem_Tipo = new String[] {""} ;
         P00HS2_n196Contagem_Tipo = new bool[] {false} ;
         P00HS2_A195Contagem_Tecnica = new String[] {""} ;
         P00HS2_n195Contagem_Tecnica = new bool[] {false} ;
         P00HS2_A197Contagem_DataCriacao = new DateTime[] {DateTime.MinValue} ;
         P00HS2_A192Contagem_Codigo = new int[1] ;
         P00HS2_A215Contagem_UsuarioContadorPessoaNom = new String[] {""} ;
         P00HS2_n215Contagem_UsuarioContadorPessoaNom = new bool[] {false} ;
         P00HS2_A194Contagem_AreaTrabalhoDes = new String[] {""} ;
         P00HS2_n194Contagem_AreaTrabalhoDes = new bool[] {false} ;
         P00HS2_A1118Contagem_ContratadaCod = new int[1] ;
         P00HS2_n1118Contagem_ContratadaCod = new bool[] {false} ;
         AV79Option = "";
         P00HS3_A1059Contagem_Notas = new String[] {""} ;
         P00HS3_n1059Contagem_Notas = new bool[] {false} ;
         P00HS3_A941Contagem_SistemaSigla = new String[] {""} ;
         P00HS3_n941Contagem_SistemaSigla = new bool[] {false} ;
         P00HS3_A940Contagem_SistemaCod = new int[1] ;
         P00HS3_n940Contagem_SistemaCod = new bool[] {false} ;
         P00HS3_A1117Contagem_Deflator = new decimal[1] ;
         P00HS3_n1117Contagem_Deflator = new bool[] {false} ;
         P00HS3_A947Contagem_Fator = new decimal[1] ;
         P00HS3_n947Contagem_Fator = new bool[] {false} ;
         P00HS3_A946Contagem_Link = new String[] {""} ;
         P00HS3_n946Contagem_Link = new bool[] {false} ;
         P00HS3_A945Contagem_Demanda = new String[] {""} ;
         P00HS3_n945Contagem_Demanda = new bool[] {false} ;
         P00HS3_A262Contagem_Status = new String[] {""} ;
         P00HS3_n262Contagem_Status = new bool[] {false} ;
         P00HS3_A214Contagem_UsuarioContadorPessoaCod = new int[1] ;
         P00HS3_n214Contagem_UsuarioContadorPessoaCod = new bool[] {false} ;
         P00HS3_A213Contagem_UsuarioContadorCod = new int[1] ;
         P00HS3_n213Contagem_UsuarioContadorCod = new bool[] {false} ;
         P00HS3_A1119Contagem_Divergencia = new decimal[1] ;
         P00HS3_n1119Contagem_Divergencia = new bool[] {false} ;
         P00HS3_A944Contagem_PFL = new decimal[1] ;
         P00HS3_n944Contagem_PFL = new bool[] {false} ;
         P00HS3_A943Contagem_PFB = new decimal[1] ;
         P00HS3_n943Contagem_PFB = new bool[] {false} ;
         P00HS3_A196Contagem_Tipo = new String[] {""} ;
         P00HS3_n196Contagem_Tipo = new bool[] {false} ;
         P00HS3_A195Contagem_Tecnica = new String[] {""} ;
         P00HS3_n195Contagem_Tecnica = new bool[] {false} ;
         P00HS3_A197Contagem_DataCriacao = new DateTime[] {DateTime.MinValue} ;
         P00HS3_A193Contagem_AreaTrabalhoCod = new int[1] ;
         P00HS3_A192Contagem_Codigo = new int[1] ;
         P00HS3_A215Contagem_UsuarioContadorPessoaNom = new String[] {""} ;
         P00HS3_n215Contagem_UsuarioContadorPessoaNom = new bool[] {false} ;
         P00HS3_A194Contagem_AreaTrabalhoDes = new String[] {""} ;
         P00HS3_n194Contagem_AreaTrabalhoDes = new bool[] {false} ;
         P00HS3_A1118Contagem_ContratadaCod = new int[1] ;
         P00HS3_n1118Contagem_ContratadaCod = new bool[] {false} ;
         P00HS4_A215Contagem_UsuarioContadorPessoaNom = new String[] {""} ;
         P00HS4_n215Contagem_UsuarioContadorPessoaNom = new bool[] {false} ;
         P00HS4_A941Contagem_SistemaSigla = new String[] {""} ;
         P00HS4_n941Contagem_SistemaSigla = new bool[] {false} ;
         P00HS4_A940Contagem_SistemaCod = new int[1] ;
         P00HS4_n940Contagem_SistemaCod = new bool[] {false} ;
         P00HS4_A1117Contagem_Deflator = new decimal[1] ;
         P00HS4_n1117Contagem_Deflator = new bool[] {false} ;
         P00HS4_A947Contagem_Fator = new decimal[1] ;
         P00HS4_n947Contagem_Fator = new bool[] {false} ;
         P00HS4_A946Contagem_Link = new String[] {""} ;
         P00HS4_n946Contagem_Link = new bool[] {false} ;
         P00HS4_A945Contagem_Demanda = new String[] {""} ;
         P00HS4_n945Contagem_Demanda = new bool[] {false} ;
         P00HS4_A262Contagem_Status = new String[] {""} ;
         P00HS4_n262Contagem_Status = new bool[] {false} ;
         P00HS4_A214Contagem_UsuarioContadorPessoaCod = new int[1] ;
         P00HS4_n214Contagem_UsuarioContadorPessoaCod = new bool[] {false} ;
         P00HS4_A213Contagem_UsuarioContadorCod = new int[1] ;
         P00HS4_n213Contagem_UsuarioContadorCod = new bool[] {false} ;
         P00HS4_A1119Contagem_Divergencia = new decimal[1] ;
         P00HS4_n1119Contagem_Divergencia = new bool[] {false} ;
         P00HS4_A944Contagem_PFL = new decimal[1] ;
         P00HS4_n944Contagem_PFL = new bool[] {false} ;
         P00HS4_A943Contagem_PFB = new decimal[1] ;
         P00HS4_n943Contagem_PFB = new bool[] {false} ;
         P00HS4_A1059Contagem_Notas = new String[] {""} ;
         P00HS4_n1059Contagem_Notas = new bool[] {false} ;
         P00HS4_A196Contagem_Tipo = new String[] {""} ;
         P00HS4_n196Contagem_Tipo = new bool[] {false} ;
         P00HS4_A195Contagem_Tecnica = new String[] {""} ;
         P00HS4_n195Contagem_Tecnica = new bool[] {false} ;
         P00HS4_A197Contagem_DataCriacao = new DateTime[] {DateTime.MinValue} ;
         P00HS4_A193Contagem_AreaTrabalhoCod = new int[1] ;
         P00HS4_A192Contagem_Codigo = new int[1] ;
         P00HS4_A194Contagem_AreaTrabalhoDes = new String[] {""} ;
         P00HS4_n194Contagem_AreaTrabalhoDes = new bool[] {false} ;
         P00HS4_A1118Contagem_ContratadaCod = new int[1] ;
         P00HS4_n1118Contagem_ContratadaCod = new bool[] {false} ;
         P00HS5_A945Contagem_Demanda = new String[] {""} ;
         P00HS5_n945Contagem_Demanda = new bool[] {false} ;
         P00HS5_A941Contagem_SistemaSigla = new String[] {""} ;
         P00HS5_n941Contagem_SistemaSigla = new bool[] {false} ;
         P00HS5_A940Contagem_SistemaCod = new int[1] ;
         P00HS5_n940Contagem_SistemaCod = new bool[] {false} ;
         P00HS5_A1117Contagem_Deflator = new decimal[1] ;
         P00HS5_n1117Contagem_Deflator = new bool[] {false} ;
         P00HS5_A947Contagem_Fator = new decimal[1] ;
         P00HS5_n947Contagem_Fator = new bool[] {false} ;
         P00HS5_A946Contagem_Link = new String[] {""} ;
         P00HS5_n946Contagem_Link = new bool[] {false} ;
         P00HS5_A262Contagem_Status = new String[] {""} ;
         P00HS5_n262Contagem_Status = new bool[] {false} ;
         P00HS5_A214Contagem_UsuarioContadorPessoaCod = new int[1] ;
         P00HS5_n214Contagem_UsuarioContadorPessoaCod = new bool[] {false} ;
         P00HS5_A213Contagem_UsuarioContadorCod = new int[1] ;
         P00HS5_n213Contagem_UsuarioContadorCod = new bool[] {false} ;
         P00HS5_A1119Contagem_Divergencia = new decimal[1] ;
         P00HS5_n1119Contagem_Divergencia = new bool[] {false} ;
         P00HS5_A944Contagem_PFL = new decimal[1] ;
         P00HS5_n944Contagem_PFL = new bool[] {false} ;
         P00HS5_A943Contagem_PFB = new decimal[1] ;
         P00HS5_n943Contagem_PFB = new bool[] {false} ;
         P00HS5_A1059Contagem_Notas = new String[] {""} ;
         P00HS5_n1059Contagem_Notas = new bool[] {false} ;
         P00HS5_A196Contagem_Tipo = new String[] {""} ;
         P00HS5_n196Contagem_Tipo = new bool[] {false} ;
         P00HS5_A195Contagem_Tecnica = new String[] {""} ;
         P00HS5_n195Contagem_Tecnica = new bool[] {false} ;
         P00HS5_A197Contagem_DataCriacao = new DateTime[] {DateTime.MinValue} ;
         P00HS5_A193Contagem_AreaTrabalhoCod = new int[1] ;
         P00HS5_A192Contagem_Codigo = new int[1] ;
         P00HS5_A215Contagem_UsuarioContadorPessoaNom = new String[] {""} ;
         P00HS5_n215Contagem_UsuarioContadorPessoaNom = new bool[] {false} ;
         P00HS5_A194Contagem_AreaTrabalhoDes = new String[] {""} ;
         P00HS5_n194Contagem_AreaTrabalhoDes = new bool[] {false} ;
         P00HS5_A1118Contagem_ContratadaCod = new int[1] ;
         P00HS5_n1118Contagem_ContratadaCod = new bool[] {false} ;
         P00HS6_A946Contagem_Link = new String[] {""} ;
         P00HS6_n946Contagem_Link = new bool[] {false} ;
         P00HS6_A941Contagem_SistemaSigla = new String[] {""} ;
         P00HS6_n941Contagem_SistemaSigla = new bool[] {false} ;
         P00HS6_A940Contagem_SistemaCod = new int[1] ;
         P00HS6_n940Contagem_SistemaCod = new bool[] {false} ;
         P00HS6_A1117Contagem_Deflator = new decimal[1] ;
         P00HS6_n1117Contagem_Deflator = new bool[] {false} ;
         P00HS6_A947Contagem_Fator = new decimal[1] ;
         P00HS6_n947Contagem_Fator = new bool[] {false} ;
         P00HS6_A945Contagem_Demanda = new String[] {""} ;
         P00HS6_n945Contagem_Demanda = new bool[] {false} ;
         P00HS6_A262Contagem_Status = new String[] {""} ;
         P00HS6_n262Contagem_Status = new bool[] {false} ;
         P00HS6_A214Contagem_UsuarioContadorPessoaCod = new int[1] ;
         P00HS6_n214Contagem_UsuarioContadorPessoaCod = new bool[] {false} ;
         P00HS6_A213Contagem_UsuarioContadorCod = new int[1] ;
         P00HS6_n213Contagem_UsuarioContadorCod = new bool[] {false} ;
         P00HS6_A1119Contagem_Divergencia = new decimal[1] ;
         P00HS6_n1119Contagem_Divergencia = new bool[] {false} ;
         P00HS6_A944Contagem_PFL = new decimal[1] ;
         P00HS6_n944Contagem_PFL = new bool[] {false} ;
         P00HS6_A943Contagem_PFB = new decimal[1] ;
         P00HS6_n943Contagem_PFB = new bool[] {false} ;
         P00HS6_A1059Contagem_Notas = new String[] {""} ;
         P00HS6_n1059Contagem_Notas = new bool[] {false} ;
         P00HS6_A196Contagem_Tipo = new String[] {""} ;
         P00HS6_n196Contagem_Tipo = new bool[] {false} ;
         P00HS6_A195Contagem_Tecnica = new String[] {""} ;
         P00HS6_n195Contagem_Tecnica = new bool[] {false} ;
         P00HS6_A197Contagem_DataCriacao = new DateTime[] {DateTime.MinValue} ;
         P00HS6_A193Contagem_AreaTrabalhoCod = new int[1] ;
         P00HS6_A192Contagem_Codigo = new int[1] ;
         P00HS6_A215Contagem_UsuarioContadorPessoaNom = new String[] {""} ;
         P00HS6_n215Contagem_UsuarioContadorPessoaNom = new bool[] {false} ;
         P00HS6_A194Contagem_AreaTrabalhoDes = new String[] {""} ;
         P00HS6_n194Contagem_AreaTrabalhoDes = new bool[] {false} ;
         P00HS6_A1118Contagem_ContratadaCod = new int[1] ;
         P00HS6_n1118Contagem_ContratadaCod = new bool[] {false} ;
         P00HS7_A941Contagem_SistemaSigla = new String[] {""} ;
         P00HS7_n941Contagem_SistemaSigla = new bool[] {false} ;
         P00HS7_A940Contagem_SistemaCod = new int[1] ;
         P00HS7_n940Contagem_SistemaCod = new bool[] {false} ;
         P00HS7_A1117Contagem_Deflator = new decimal[1] ;
         P00HS7_n1117Contagem_Deflator = new bool[] {false} ;
         P00HS7_A947Contagem_Fator = new decimal[1] ;
         P00HS7_n947Contagem_Fator = new bool[] {false} ;
         P00HS7_A946Contagem_Link = new String[] {""} ;
         P00HS7_n946Contagem_Link = new bool[] {false} ;
         P00HS7_A945Contagem_Demanda = new String[] {""} ;
         P00HS7_n945Contagem_Demanda = new bool[] {false} ;
         P00HS7_A262Contagem_Status = new String[] {""} ;
         P00HS7_n262Contagem_Status = new bool[] {false} ;
         P00HS7_A214Contagem_UsuarioContadorPessoaCod = new int[1] ;
         P00HS7_n214Contagem_UsuarioContadorPessoaCod = new bool[] {false} ;
         P00HS7_A213Contagem_UsuarioContadorCod = new int[1] ;
         P00HS7_n213Contagem_UsuarioContadorCod = new bool[] {false} ;
         P00HS7_A1119Contagem_Divergencia = new decimal[1] ;
         P00HS7_n1119Contagem_Divergencia = new bool[] {false} ;
         P00HS7_A944Contagem_PFL = new decimal[1] ;
         P00HS7_n944Contagem_PFL = new bool[] {false} ;
         P00HS7_A943Contagem_PFB = new decimal[1] ;
         P00HS7_n943Contagem_PFB = new bool[] {false} ;
         P00HS7_A1059Contagem_Notas = new String[] {""} ;
         P00HS7_n1059Contagem_Notas = new bool[] {false} ;
         P00HS7_A196Contagem_Tipo = new String[] {""} ;
         P00HS7_n196Contagem_Tipo = new bool[] {false} ;
         P00HS7_A195Contagem_Tecnica = new String[] {""} ;
         P00HS7_n195Contagem_Tecnica = new bool[] {false} ;
         P00HS7_A197Contagem_DataCriacao = new DateTime[] {DateTime.MinValue} ;
         P00HS7_A193Contagem_AreaTrabalhoCod = new int[1] ;
         P00HS7_A192Contagem_Codigo = new int[1] ;
         P00HS7_A215Contagem_UsuarioContadorPessoaNom = new String[] {""} ;
         P00HS7_n215Contagem_UsuarioContadorPessoaNom = new bool[] {false} ;
         P00HS7_A194Contagem_AreaTrabalhoDes = new String[] {""} ;
         P00HS7_n194Contagem_AreaTrabalhoDes = new bool[] {false} ;
         P00HS7_A1118Contagem_ContratadaCod = new int[1] ;
         P00HS7_n1118Contagem_ContratadaCod = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwcontagemfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00HS2_A193Contagem_AreaTrabalhoCod, P00HS2_A941Contagem_SistemaSigla, P00HS2_n941Contagem_SistemaSigla, P00HS2_A940Contagem_SistemaCod, P00HS2_n940Contagem_SistemaCod, P00HS2_A1117Contagem_Deflator, P00HS2_n1117Contagem_Deflator, P00HS2_A947Contagem_Fator, P00HS2_n947Contagem_Fator, P00HS2_A946Contagem_Link,
               P00HS2_n946Contagem_Link, P00HS2_A945Contagem_Demanda, P00HS2_n945Contagem_Demanda, P00HS2_A262Contagem_Status, P00HS2_n262Contagem_Status, P00HS2_A214Contagem_UsuarioContadorPessoaCod, P00HS2_n214Contagem_UsuarioContadorPessoaCod, P00HS2_A213Contagem_UsuarioContadorCod, P00HS2_n213Contagem_UsuarioContadorCod, P00HS2_A1119Contagem_Divergencia,
               P00HS2_n1119Contagem_Divergencia, P00HS2_A944Contagem_PFL, P00HS2_n944Contagem_PFL, P00HS2_A943Contagem_PFB, P00HS2_n943Contagem_PFB, P00HS2_A1059Contagem_Notas, P00HS2_n1059Contagem_Notas, P00HS2_A196Contagem_Tipo, P00HS2_n196Contagem_Tipo, P00HS2_A195Contagem_Tecnica,
               P00HS2_n195Contagem_Tecnica, P00HS2_A197Contagem_DataCriacao, P00HS2_A192Contagem_Codigo, P00HS2_A215Contagem_UsuarioContadorPessoaNom, P00HS2_n215Contagem_UsuarioContadorPessoaNom, P00HS2_A194Contagem_AreaTrabalhoDes, P00HS2_n194Contagem_AreaTrabalhoDes, P00HS2_A1118Contagem_ContratadaCod, P00HS2_n1118Contagem_ContratadaCod
               }
               , new Object[] {
               P00HS3_A1059Contagem_Notas, P00HS3_n1059Contagem_Notas, P00HS3_A941Contagem_SistemaSigla, P00HS3_n941Contagem_SistemaSigla, P00HS3_A940Contagem_SistemaCod, P00HS3_n940Contagem_SistemaCod, P00HS3_A1117Contagem_Deflator, P00HS3_n1117Contagem_Deflator, P00HS3_A947Contagem_Fator, P00HS3_n947Contagem_Fator,
               P00HS3_A946Contagem_Link, P00HS3_n946Contagem_Link, P00HS3_A945Contagem_Demanda, P00HS3_n945Contagem_Demanda, P00HS3_A262Contagem_Status, P00HS3_n262Contagem_Status, P00HS3_A214Contagem_UsuarioContadorPessoaCod, P00HS3_n214Contagem_UsuarioContadorPessoaCod, P00HS3_A213Contagem_UsuarioContadorCod, P00HS3_n213Contagem_UsuarioContadorCod,
               P00HS3_A1119Contagem_Divergencia, P00HS3_n1119Contagem_Divergencia, P00HS3_A944Contagem_PFL, P00HS3_n944Contagem_PFL, P00HS3_A943Contagem_PFB, P00HS3_n943Contagem_PFB, P00HS3_A196Contagem_Tipo, P00HS3_n196Contagem_Tipo, P00HS3_A195Contagem_Tecnica, P00HS3_n195Contagem_Tecnica,
               P00HS3_A197Contagem_DataCriacao, P00HS3_A193Contagem_AreaTrabalhoCod, P00HS3_A192Contagem_Codigo, P00HS3_A215Contagem_UsuarioContadorPessoaNom, P00HS3_n215Contagem_UsuarioContadorPessoaNom, P00HS3_A194Contagem_AreaTrabalhoDes, P00HS3_n194Contagem_AreaTrabalhoDes, P00HS3_A1118Contagem_ContratadaCod, P00HS3_n1118Contagem_ContratadaCod
               }
               , new Object[] {
               P00HS4_A215Contagem_UsuarioContadorPessoaNom, P00HS4_n215Contagem_UsuarioContadorPessoaNom, P00HS4_A941Contagem_SistemaSigla, P00HS4_n941Contagem_SistemaSigla, P00HS4_A940Contagem_SistemaCod, P00HS4_n940Contagem_SistemaCod, P00HS4_A1117Contagem_Deflator, P00HS4_n1117Contagem_Deflator, P00HS4_A947Contagem_Fator, P00HS4_n947Contagem_Fator,
               P00HS4_A946Contagem_Link, P00HS4_n946Contagem_Link, P00HS4_A945Contagem_Demanda, P00HS4_n945Contagem_Demanda, P00HS4_A262Contagem_Status, P00HS4_n262Contagem_Status, P00HS4_A214Contagem_UsuarioContadorPessoaCod, P00HS4_n214Contagem_UsuarioContadorPessoaCod, P00HS4_A213Contagem_UsuarioContadorCod, P00HS4_n213Contagem_UsuarioContadorCod,
               P00HS4_A1119Contagem_Divergencia, P00HS4_n1119Contagem_Divergencia, P00HS4_A944Contagem_PFL, P00HS4_n944Contagem_PFL, P00HS4_A943Contagem_PFB, P00HS4_n943Contagem_PFB, P00HS4_A1059Contagem_Notas, P00HS4_n1059Contagem_Notas, P00HS4_A196Contagem_Tipo, P00HS4_n196Contagem_Tipo,
               P00HS4_A195Contagem_Tecnica, P00HS4_n195Contagem_Tecnica, P00HS4_A197Contagem_DataCriacao, P00HS4_A193Contagem_AreaTrabalhoCod, P00HS4_A192Contagem_Codigo, P00HS4_A194Contagem_AreaTrabalhoDes, P00HS4_n194Contagem_AreaTrabalhoDes, P00HS4_A1118Contagem_ContratadaCod, P00HS4_n1118Contagem_ContratadaCod
               }
               , new Object[] {
               P00HS5_A945Contagem_Demanda, P00HS5_n945Contagem_Demanda, P00HS5_A941Contagem_SistemaSigla, P00HS5_n941Contagem_SistemaSigla, P00HS5_A940Contagem_SistemaCod, P00HS5_n940Contagem_SistemaCod, P00HS5_A1117Contagem_Deflator, P00HS5_n1117Contagem_Deflator, P00HS5_A947Contagem_Fator, P00HS5_n947Contagem_Fator,
               P00HS5_A946Contagem_Link, P00HS5_n946Contagem_Link, P00HS5_A262Contagem_Status, P00HS5_n262Contagem_Status, P00HS5_A214Contagem_UsuarioContadorPessoaCod, P00HS5_n214Contagem_UsuarioContadorPessoaCod, P00HS5_A213Contagem_UsuarioContadorCod, P00HS5_n213Contagem_UsuarioContadorCod, P00HS5_A1119Contagem_Divergencia, P00HS5_n1119Contagem_Divergencia,
               P00HS5_A944Contagem_PFL, P00HS5_n944Contagem_PFL, P00HS5_A943Contagem_PFB, P00HS5_n943Contagem_PFB, P00HS5_A1059Contagem_Notas, P00HS5_n1059Contagem_Notas, P00HS5_A196Contagem_Tipo, P00HS5_n196Contagem_Tipo, P00HS5_A195Contagem_Tecnica, P00HS5_n195Contagem_Tecnica,
               P00HS5_A197Contagem_DataCriacao, P00HS5_A193Contagem_AreaTrabalhoCod, P00HS5_A192Contagem_Codigo, P00HS5_A215Contagem_UsuarioContadorPessoaNom, P00HS5_n215Contagem_UsuarioContadorPessoaNom, P00HS5_A194Contagem_AreaTrabalhoDes, P00HS5_n194Contagem_AreaTrabalhoDes, P00HS5_A1118Contagem_ContratadaCod, P00HS5_n1118Contagem_ContratadaCod
               }
               , new Object[] {
               P00HS6_A946Contagem_Link, P00HS6_n946Contagem_Link, P00HS6_A941Contagem_SistemaSigla, P00HS6_n941Contagem_SistemaSigla, P00HS6_A940Contagem_SistemaCod, P00HS6_n940Contagem_SistemaCod, P00HS6_A1117Contagem_Deflator, P00HS6_n1117Contagem_Deflator, P00HS6_A947Contagem_Fator, P00HS6_n947Contagem_Fator,
               P00HS6_A945Contagem_Demanda, P00HS6_n945Contagem_Demanda, P00HS6_A262Contagem_Status, P00HS6_n262Contagem_Status, P00HS6_A214Contagem_UsuarioContadorPessoaCod, P00HS6_n214Contagem_UsuarioContadorPessoaCod, P00HS6_A213Contagem_UsuarioContadorCod, P00HS6_n213Contagem_UsuarioContadorCod, P00HS6_A1119Contagem_Divergencia, P00HS6_n1119Contagem_Divergencia,
               P00HS6_A944Contagem_PFL, P00HS6_n944Contagem_PFL, P00HS6_A943Contagem_PFB, P00HS6_n943Contagem_PFB, P00HS6_A1059Contagem_Notas, P00HS6_n1059Contagem_Notas, P00HS6_A196Contagem_Tipo, P00HS6_n196Contagem_Tipo, P00HS6_A195Contagem_Tecnica, P00HS6_n195Contagem_Tecnica,
               P00HS6_A197Contagem_DataCriacao, P00HS6_A193Contagem_AreaTrabalhoCod, P00HS6_A192Contagem_Codigo, P00HS6_A215Contagem_UsuarioContadorPessoaNom, P00HS6_n215Contagem_UsuarioContadorPessoaNom, P00HS6_A194Contagem_AreaTrabalhoDes, P00HS6_n194Contagem_AreaTrabalhoDes, P00HS6_A1118Contagem_ContratadaCod, P00HS6_n1118Contagem_ContratadaCod
               }
               , new Object[] {
               P00HS7_A941Contagem_SistemaSigla, P00HS7_n941Contagem_SistemaSigla, P00HS7_A940Contagem_SistemaCod, P00HS7_n940Contagem_SistemaCod, P00HS7_A1117Contagem_Deflator, P00HS7_n1117Contagem_Deflator, P00HS7_A947Contagem_Fator, P00HS7_n947Contagem_Fator, P00HS7_A946Contagem_Link, P00HS7_n946Contagem_Link,
               P00HS7_A945Contagem_Demanda, P00HS7_n945Contagem_Demanda, P00HS7_A262Contagem_Status, P00HS7_n262Contagem_Status, P00HS7_A214Contagem_UsuarioContadorPessoaCod, P00HS7_n214Contagem_UsuarioContadorPessoaCod, P00HS7_A213Contagem_UsuarioContadorCod, P00HS7_n213Contagem_UsuarioContadorCod, P00HS7_A1119Contagem_Divergencia, P00HS7_n1119Contagem_Divergencia,
               P00HS7_A944Contagem_PFL, P00HS7_n944Contagem_PFL, P00HS7_A943Contagem_PFB, P00HS7_n943Contagem_PFB, P00HS7_A1059Contagem_Notas, P00HS7_n1059Contagem_Notas, P00HS7_A196Contagem_Tipo, P00HS7_n196Contagem_Tipo, P00HS7_A195Contagem_Tecnica, P00HS7_n195Contagem_Tecnica,
               P00HS7_A197Contagem_DataCriacao, P00HS7_A193Contagem_AreaTrabalhoCod, P00HS7_A192Contagem_Codigo, P00HS7_A215Contagem_UsuarioContadorPessoaNom, P00HS7_n215Contagem_UsuarioContadorPessoaNom, P00HS7_A194Contagem_AreaTrabalhoDes, P00HS7_n194Contagem_AreaTrabalhoDes, P00HS7_A1118Contagem_ContratadaCod, P00HS7_n1118Contagem_ContratadaCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV94DynamicFiltersOperator1 ;
      private short AV100DynamicFiltersOperator2 ;
      private short AV106DynamicFiltersOperator3 ;
      private short AV115WWContagemDS_2_Dynamicfiltersoperator1 ;
      private short AV121WWContagemDS_8_Dynamicfiltersoperator2 ;
      private short AV127WWContagemDS_14_Dynamicfiltersoperator3 ;
      private int AV112GXV1 ;
      private int AV10TFContagem_Codigo ;
      private int AV11TFContagem_Codigo_To ;
      private int AV12TFContagem_AreaTrabalhoCod ;
      private int AV13TFContagem_AreaTrabalhoCod_To ;
      private int AV14TFContagem_ContratadaCod ;
      private int AV15TFContagem_ContratadaCod_To ;
      private int AV32TFContagem_UsuarioContadorCod ;
      private int AV33TFContagem_UsuarioContadorCod_To ;
      private int AV34TFContagem_UsuarioContadorPessoaCod ;
      private int AV35TFContagem_UsuarioContadorPessoaCod_To ;
      private int AV48TFContagem_SistemaCod ;
      private int AV49TFContagem_SistemaCod_To ;
      private int AV95Contagem_ContratadaCod1 ;
      private int AV101Contagem_ContratadaCod2 ;
      private int AV107Contagem_ContratadaCod3 ;
      private int AV116WWContagemDS_3_Contagem_contratadacod1 ;
      private int AV122WWContagemDS_9_Contagem_contratadacod2 ;
      private int AV128WWContagemDS_15_Contagem_contratadacod3 ;
      private int AV131WWContagemDS_18_Tfcontagem_codigo ;
      private int AV132WWContagemDS_19_Tfcontagem_codigo_to ;
      private int AV133WWContagemDS_20_Tfcontagem_areatrabalhocod ;
      private int AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to ;
      private int AV135WWContagemDS_22_Tfcontagem_contratadacod ;
      private int AV136WWContagemDS_23_Tfcontagem_contratadacod_to ;
      private int AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod ;
      private int AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to ;
      private int AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod ;
      private int AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to ;
      private int AV166WWContagemDS_53_Tfcontagem_sistemacod ;
      private int AV167WWContagemDS_54_Tfcontagem_sistemacod_to ;
      private int AV141WWContagemDS_28_Tfcontagem_tecnica_sels_Count ;
      private int AV142WWContagemDS_29_Tfcontagem_tipo_sels_Count ;
      private int AV157WWContagemDS_44_Tfcontagem_status_sels_Count ;
      private int A1118Contagem_ContratadaCod ;
      private int A192Contagem_Codigo ;
      private int A193Contagem_AreaTrabalhoCod ;
      private int A213Contagem_UsuarioContadorCod ;
      private int A214Contagem_UsuarioContadorPessoaCod ;
      private int A940Contagem_SistemaCod ;
      private int AV78InsertIndex ;
      private long AV87count ;
      private decimal AV26TFContagem_PFB ;
      private decimal AV27TFContagem_PFB_To ;
      private decimal AV28TFContagem_PFL ;
      private decimal AV29TFContagem_PFL_To ;
      private decimal AV30TFContagem_Divergencia ;
      private decimal AV31TFContagem_Divergencia_To ;
      private decimal AV44TFContagem_Fator ;
      private decimal AV45TFContagem_Fator_To ;
      private decimal AV46TFContagem_Deflator ;
      private decimal AV47TFContagem_Deflator_To ;
      private decimal AV145WWContagemDS_32_Tfcontagem_pfb ;
      private decimal AV146WWContagemDS_33_Tfcontagem_pfb_to ;
      private decimal AV147WWContagemDS_34_Tfcontagem_pfl ;
      private decimal AV148WWContagemDS_35_Tfcontagem_pfl_to ;
      private decimal AV149WWContagemDS_36_Tfcontagem_divergencia ;
      private decimal AV150WWContagemDS_37_Tfcontagem_divergencia_to ;
      private decimal AV162WWContagemDS_49_Tfcontagem_fator ;
      private decimal AV163WWContagemDS_50_Tfcontagem_fator_to ;
      private decimal AV164WWContagemDS_51_Tfcontagem_deflator ;
      private decimal AV165WWContagemDS_52_Tfcontagem_deflator_to ;
      private decimal A943Contagem_PFB ;
      private decimal A944Contagem_PFL ;
      private decimal A1119Contagem_Divergencia ;
      private decimal A947Contagem_Fator ;
      private decimal A1117Contagem_Deflator ;
      private String AV36TFContagem_UsuarioContadorPessoaNom ;
      private String AV37TFContagem_UsuarioContadorPessoaNom_Sel ;
      private String AV50TFContagem_SistemaSigla ;
      private String AV51TFContagem_SistemaSigla_Sel ;
      private String AV97Contagem_UsuarioContadorPessoaNom1 ;
      private String AV103Contagem_UsuarioContadorPessoaNom2 ;
      private String AV109Contagem_UsuarioContadorPessoaNom3 ;
      private String AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 ;
      private String AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 ;
      private String AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 ;
      private String AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom ;
      private String AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel ;
      private String AV168WWContagemDS_55_Tfcontagem_sistemasigla ;
      private String AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel ;
      private String scmdbuf ;
      private String lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 ;
      private String lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 ;
      private String lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 ;
      private String lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom ;
      private String lV168WWContagemDS_55_Tfcontagem_sistemasigla ;
      private String A195Contagem_Tecnica ;
      private String A196Contagem_Tipo ;
      private String A262Contagem_Status ;
      private String A215Contagem_UsuarioContadorPessoaNom ;
      private String A941Contagem_SistemaSigla ;
      private DateTime AV16TFContagem_DataCriacao ;
      private DateTime AV17TFContagem_DataCriacao_To ;
      private DateTime AV137WWContagemDS_24_Tfcontagem_datacriacao ;
      private DateTime AV138WWContagemDS_25_Tfcontagem_datacriacao_to ;
      private DateTime A197Contagem_DataCriacao ;
      private bool returnInSub ;
      private bool AV98DynamicFiltersEnabled2 ;
      private bool AV104DynamicFiltersEnabled3 ;
      private bool AV119WWContagemDS_6_Dynamicfiltersenabled2 ;
      private bool AV125WWContagemDS_12_Dynamicfiltersenabled3 ;
      private bool BRKHS2 ;
      private bool n941Contagem_SistemaSigla ;
      private bool n940Contagem_SistemaCod ;
      private bool n1117Contagem_Deflator ;
      private bool n947Contagem_Fator ;
      private bool n946Contagem_Link ;
      private bool n945Contagem_Demanda ;
      private bool n262Contagem_Status ;
      private bool n214Contagem_UsuarioContadorPessoaCod ;
      private bool n213Contagem_UsuarioContadorCod ;
      private bool n1119Contagem_Divergencia ;
      private bool n944Contagem_PFL ;
      private bool n943Contagem_PFB ;
      private bool n1059Contagem_Notas ;
      private bool n196Contagem_Tipo ;
      private bool n195Contagem_Tecnica ;
      private bool n215Contagem_UsuarioContadorPessoaNom ;
      private bool n194Contagem_AreaTrabalhoDes ;
      private bool n1118Contagem_ContratadaCod ;
      private bool BRKHS4 ;
      private bool BRKHS6 ;
      private bool BRKHS8 ;
      private bool BRKHS10 ;
      private bool BRKHS12 ;
      private String AV86OptionIndexesJson ;
      private String AV81OptionsJson ;
      private String AV84OptionsDescJson ;
      private String AV20TFContagem_Tecnica_SelsJson ;
      private String AV22TFContagem_Tipo_SelsJson ;
      private String AV38TFContagem_Status_SelsJson ;
      private String A1059Contagem_Notas ;
      private String AV77DDOName ;
      private String AV75SearchTxt ;
      private String AV76SearchTxtTo ;
      private String AV18TFContagem_AreaTrabalhoDes ;
      private String AV19TFContagem_AreaTrabalhoDes_Sel ;
      private String AV24TFContagem_Notas ;
      private String AV25TFContagem_Notas_Sel ;
      private String AV40TFContagem_Demanda ;
      private String AV41TFContagem_Demanda_Sel ;
      private String AV42TFContagem_Link ;
      private String AV43TFContagem_Link_Sel ;
      private String AV93DynamicFiltersSelector1 ;
      private String AV96Contagem_AreaTrabalhoDes1 ;
      private String AV99DynamicFiltersSelector2 ;
      private String AV102Contagem_AreaTrabalhoDes2 ;
      private String AV105DynamicFiltersSelector3 ;
      private String AV108Contagem_AreaTrabalhoDes3 ;
      private String AV114WWContagemDS_1_Dynamicfiltersselector1 ;
      private String AV117WWContagemDS_4_Contagem_areatrabalhodes1 ;
      private String AV120WWContagemDS_7_Dynamicfiltersselector2 ;
      private String AV123WWContagemDS_10_Contagem_areatrabalhodes2 ;
      private String AV126WWContagemDS_13_Dynamicfiltersselector3 ;
      private String AV129WWContagemDS_16_Contagem_areatrabalhodes3 ;
      private String AV139WWContagemDS_26_Tfcontagem_areatrabalhodes ;
      private String AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel ;
      private String AV143WWContagemDS_30_Tfcontagem_notas ;
      private String AV144WWContagemDS_31_Tfcontagem_notas_sel ;
      private String AV158WWContagemDS_45_Tfcontagem_demanda ;
      private String AV159WWContagemDS_46_Tfcontagem_demanda_sel ;
      private String AV160WWContagemDS_47_Tfcontagem_link ;
      private String AV161WWContagemDS_48_Tfcontagem_link_sel ;
      private String lV117WWContagemDS_4_Contagem_areatrabalhodes1 ;
      private String lV123WWContagemDS_10_Contagem_areatrabalhodes2 ;
      private String lV129WWContagemDS_16_Contagem_areatrabalhodes3 ;
      private String lV139WWContagemDS_26_Tfcontagem_areatrabalhodes ;
      private String lV143WWContagemDS_30_Tfcontagem_notas ;
      private String lV158WWContagemDS_45_Tfcontagem_demanda ;
      private String lV160WWContagemDS_47_Tfcontagem_link ;
      private String A194Contagem_AreaTrabalhoDes ;
      private String A945Contagem_Demanda ;
      private String A946Contagem_Link ;
      private String AV79Option ;
      private IGxSession AV88Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00HS2_A193Contagem_AreaTrabalhoCod ;
      private String[] P00HS2_A941Contagem_SistemaSigla ;
      private bool[] P00HS2_n941Contagem_SistemaSigla ;
      private int[] P00HS2_A940Contagem_SistemaCod ;
      private bool[] P00HS2_n940Contagem_SistemaCod ;
      private decimal[] P00HS2_A1117Contagem_Deflator ;
      private bool[] P00HS2_n1117Contagem_Deflator ;
      private decimal[] P00HS2_A947Contagem_Fator ;
      private bool[] P00HS2_n947Contagem_Fator ;
      private String[] P00HS2_A946Contagem_Link ;
      private bool[] P00HS2_n946Contagem_Link ;
      private String[] P00HS2_A945Contagem_Demanda ;
      private bool[] P00HS2_n945Contagem_Demanda ;
      private String[] P00HS2_A262Contagem_Status ;
      private bool[] P00HS2_n262Contagem_Status ;
      private int[] P00HS2_A214Contagem_UsuarioContadorPessoaCod ;
      private bool[] P00HS2_n214Contagem_UsuarioContadorPessoaCod ;
      private int[] P00HS2_A213Contagem_UsuarioContadorCod ;
      private bool[] P00HS2_n213Contagem_UsuarioContadorCod ;
      private decimal[] P00HS2_A1119Contagem_Divergencia ;
      private bool[] P00HS2_n1119Contagem_Divergencia ;
      private decimal[] P00HS2_A944Contagem_PFL ;
      private bool[] P00HS2_n944Contagem_PFL ;
      private decimal[] P00HS2_A943Contagem_PFB ;
      private bool[] P00HS2_n943Contagem_PFB ;
      private String[] P00HS2_A1059Contagem_Notas ;
      private bool[] P00HS2_n1059Contagem_Notas ;
      private String[] P00HS2_A196Contagem_Tipo ;
      private bool[] P00HS2_n196Contagem_Tipo ;
      private String[] P00HS2_A195Contagem_Tecnica ;
      private bool[] P00HS2_n195Contagem_Tecnica ;
      private DateTime[] P00HS2_A197Contagem_DataCriacao ;
      private int[] P00HS2_A192Contagem_Codigo ;
      private String[] P00HS2_A215Contagem_UsuarioContadorPessoaNom ;
      private bool[] P00HS2_n215Contagem_UsuarioContadorPessoaNom ;
      private String[] P00HS2_A194Contagem_AreaTrabalhoDes ;
      private bool[] P00HS2_n194Contagem_AreaTrabalhoDes ;
      private int[] P00HS2_A1118Contagem_ContratadaCod ;
      private bool[] P00HS2_n1118Contagem_ContratadaCod ;
      private String[] P00HS3_A1059Contagem_Notas ;
      private bool[] P00HS3_n1059Contagem_Notas ;
      private String[] P00HS3_A941Contagem_SistemaSigla ;
      private bool[] P00HS3_n941Contagem_SistemaSigla ;
      private int[] P00HS3_A940Contagem_SistemaCod ;
      private bool[] P00HS3_n940Contagem_SistemaCod ;
      private decimal[] P00HS3_A1117Contagem_Deflator ;
      private bool[] P00HS3_n1117Contagem_Deflator ;
      private decimal[] P00HS3_A947Contagem_Fator ;
      private bool[] P00HS3_n947Contagem_Fator ;
      private String[] P00HS3_A946Contagem_Link ;
      private bool[] P00HS3_n946Contagem_Link ;
      private String[] P00HS3_A945Contagem_Demanda ;
      private bool[] P00HS3_n945Contagem_Demanda ;
      private String[] P00HS3_A262Contagem_Status ;
      private bool[] P00HS3_n262Contagem_Status ;
      private int[] P00HS3_A214Contagem_UsuarioContadorPessoaCod ;
      private bool[] P00HS3_n214Contagem_UsuarioContadorPessoaCod ;
      private int[] P00HS3_A213Contagem_UsuarioContadorCod ;
      private bool[] P00HS3_n213Contagem_UsuarioContadorCod ;
      private decimal[] P00HS3_A1119Contagem_Divergencia ;
      private bool[] P00HS3_n1119Contagem_Divergencia ;
      private decimal[] P00HS3_A944Contagem_PFL ;
      private bool[] P00HS3_n944Contagem_PFL ;
      private decimal[] P00HS3_A943Contagem_PFB ;
      private bool[] P00HS3_n943Contagem_PFB ;
      private String[] P00HS3_A196Contagem_Tipo ;
      private bool[] P00HS3_n196Contagem_Tipo ;
      private String[] P00HS3_A195Contagem_Tecnica ;
      private bool[] P00HS3_n195Contagem_Tecnica ;
      private DateTime[] P00HS3_A197Contagem_DataCriacao ;
      private int[] P00HS3_A193Contagem_AreaTrabalhoCod ;
      private int[] P00HS3_A192Contagem_Codigo ;
      private String[] P00HS3_A215Contagem_UsuarioContadorPessoaNom ;
      private bool[] P00HS3_n215Contagem_UsuarioContadorPessoaNom ;
      private String[] P00HS3_A194Contagem_AreaTrabalhoDes ;
      private bool[] P00HS3_n194Contagem_AreaTrabalhoDes ;
      private int[] P00HS3_A1118Contagem_ContratadaCod ;
      private bool[] P00HS3_n1118Contagem_ContratadaCod ;
      private String[] P00HS4_A215Contagem_UsuarioContadorPessoaNom ;
      private bool[] P00HS4_n215Contagem_UsuarioContadorPessoaNom ;
      private String[] P00HS4_A941Contagem_SistemaSigla ;
      private bool[] P00HS4_n941Contagem_SistemaSigla ;
      private int[] P00HS4_A940Contagem_SistemaCod ;
      private bool[] P00HS4_n940Contagem_SistemaCod ;
      private decimal[] P00HS4_A1117Contagem_Deflator ;
      private bool[] P00HS4_n1117Contagem_Deflator ;
      private decimal[] P00HS4_A947Contagem_Fator ;
      private bool[] P00HS4_n947Contagem_Fator ;
      private String[] P00HS4_A946Contagem_Link ;
      private bool[] P00HS4_n946Contagem_Link ;
      private String[] P00HS4_A945Contagem_Demanda ;
      private bool[] P00HS4_n945Contagem_Demanda ;
      private String[] P00HS4_A262Contagem_Status ;
      private bool[] P00HS4_n262Contagem_Status ;
      private int[] P00HS4_A214Contagem_UsuarioContadorPessoaCod ;
      private bool[] P00HS4_n214Contagem_UsuarioContadorPessoaCod ;
      private int[] P00HS4_A213Contagem_UsuarioContadorCod ;
      private bool[] P00HS4_n213Contagem_UsuarioContadorCod ;
      private decimal[] P00HS4_A1119Contagem_Divergencia ;
      private bool[] P00HS4_n1119Contagem_Divergencia ;
      private decimal[] P00HS4_A944Contagem_PFL ;
      private bool[] P00HS4_n944Contagem_PFL ;
      private decimal[] P00HS4_A943Contagem_PFB ;
      private bool[] P00HS4_n943Contagem_PFB ;
      private String[] P00HS4_A1059Contagem_Notas ;
      private bool[] P00HS4_n1059Contagem_Notas ;
      private String[] P00HS4_A196Contagem_Tipo ;
      private bool[] P00HS4_n196Contagem_Tipo ;
      private String[] P00HS4_A195Contagem_Tecnica ;
      private bool[] P00HS4_n195Contagem_Tecnica ;
      private DateTime[] P00HS4_A197Contagem_DataCriacao ;
      private int[] P00HS4_A193Contagem_AreaTrabalhoCod ;
      private int[] P00HS4_A192Contagem_Codigo ;
      private String[] P00HS4_A194Contagem_AreaTrabalhoDes ;
      private bool[] P00HS4_n194Contagem_AreaTrabalhoDes ;
      private int[] P00HS4_A1118Contagem_ContratadaCod ;
      private bool[] P00HS4_n1118Contagem_ContratadaCod ;
      private String[] P00HS5_A945Contagem_Demanda ;
      private bool[] P00HS5_n945Contagem_Demanda ;
      private String[] P00HS5_A941Contagem_SistemaSigla ;
      private bool[] P00HS5_n941Contagem_SistemaSigla ;
      private int[] P00HS5_A940Contagem_SistemaCod ;
      private bool[] P00HS5_n940Contagem_SistemaCod ;
      private decimal[] P00HS5_A1117Contagem_Deflator ;
      private bool[] P00HS5_n1117Contagem_Deflator ;
      private decimal[] P00HS5_A947Contagem_Fator ;
      private bool[] P00HS5_n947Contagem_Fator ;
      private String[] P00HS5_A946Contagem_Link ;
      private bool[] P00HS5_n946Contagem_Link ;
      private String[] P00HS5_A262Contagem_Status ;
      private bool[] P00HS5_n262Contagem_Status ;
      private int[] P00HS5_A214Contagem_UsuarioContadorPessoaCod ;
      private bool[] P00HS5_n214Contagem_UsuarioContadorPessoaCod ;
      private int[] P00HS5_A213Contagem_UsuarioContadorCod ;
      private bool[] P00HS5_n213Contagem_UsuarioContadorCod ;
      private decimal[] P00HS5_A1119Contagem_Divergencia ;
      private bool[] P00HS5_n1119Contagem_Divergencia ;
      private decimal[] P00HS5_A944Contagem_PFL ;
      private bool[] P00HS5_n944Contagem_PFL ;
      private decimal[] P00HS5_A943Contagem_PFB ;
      private bool[] P00HS5_n943Contagem_PFB ;
      private String[] P00HS5_A1059Contagem_Notas ;
      private bool[] P00HS5_n1059Contagem_Notas ;
      private String[] P00HS5_A196Contagem_Tipo ;
      private bool[] P00HS5_n196Contagem_Tipo ;
      private String[] P00HS5_A195Contagem_Tecnica ;
      private bool[] P00HS5_n195Contagem_Tecnica ;
      private DateTime[] P00HS5_A197Contagem_DataCriacao ;
      private int[] P00HS5_A193Contagem_AreaTrabalhoCod ;
      private int[] P00HS5_A192Contagem_Codigo ;
      private String[] P00HS5_A215Contagem_UsuarioContadorPessoaNom ;
      private bool[] P00HS5_n215Contagem_UsuarioContadorPessoaNom ;
      private String[] P00HS5_A194Contagem_AreaTrabalhoDes ;
      private bool[] P00HS5_n194Contagem_AreaTrabalhoDes ;
      private int[] P00HS5_A1118Contagem_ContratadaCod ;
      private bool[] P00HS5_n1118Contagem_ContratadaCod ;
      private String[] P00HS6_A946Contagem_Link ;
      private bool[] P00HS6_n946Contagem_Link ;
      private String[] P00HS6_A941Contagem_SistemaSigla ;
      private bool[] P00HS6_n941Contagem_SistemaSigla ;
      private int[] P00HS6_A940Contagem_SistemaCod ;
      private bool[] P00HS6_n940Contagem_SistemaCod ;
      private decimal[] P00HS6_A1117Contagem_Deflator ;
      private bool[] P00HS6_n1117Contagem_Deflator ;
      private decimal[] P00HS6_A947Contagem_Fator ;
      private bool[] P00HS6_n947Contagem_Fator ;
      private String[] P00HS6_A945Contagem_Demanda ;
      private bool[] P00HS6_n945Contagem_Demanda ;
      private String[] P00HS6_A262Contagem_Status ;
      private bool[] P00HS6_n262Contagem_Status ;
      private int[] P00HS6_A214Contagem_UsuarioContadorPessoaCod ;
      private bool[] P00HS6_n214Contagem_UsuarioContadorPessoaCod ;
      private int[] P00HS6_A213Contagem_UsuarioContadorCod ;
      private bool[] P00HS6_n213Contagem_UsuarioContadorCod ;
      private decimal[] P00HS6_A1119Contagem_Divergencia ;
      private bool[] P00HS6_n1119Contagem_Divergencia ;
      private decimal[] P00HS6_A944Contagem_PFL ;
      private bool[] P00HS6_n944Contagem_PFL ;
      private decimal[] P00HS6_A943Contagem_PFB ;
      private bool[] P00HS6_n943Contagem_PFB ;
      private String[] P00HS6_A1059Contagem_Notas ;
      private bool[] P00HS6_n1059Contagem_Notas ;
      private String[] P00HS6_A196Contagem_Tipo ;
      private bool[] P00HS6_n196Contagem_Tipo ;
      private String[] P00HS6_A195Contagem_Tecnica ;
      private bool[] P00HS6_n195Contagem_Tecnica ;
      private DateTime[] P00HS6_A197Contagem_DataCriacao ;
      private int[] P00HS6_A193Contagem_AreaTrabalhoCod ;
      private int[] P00HS6_A192Contagem_Codigo ;
      private String[] P00HS6_A215Contagem_UsuarioContadorPessoaNom ;
      private bool[] P00HS6_n215Contagem_UsuarioContadorPessoaNom ;
      private String[] P00HS6_A194Contagem_AreaTrabalhoDes ;
      private bool[] P00HS6_n194Contagem_AreaTrabalhoDes ;
      private int[] P00HS6_A1118Contagem_ContratadaCod ;
      private bool[] P00HS6_n1118Contagem_ContratadaCod ;
      private String[] P00HS7_A941Contagem_SistemaSigla ;
      private bool[] P00HS7_n941Contagem_SistemaSigla ;
      private int[] P00HS7_A940Contagem_SistemaCod ;
      private bool[] P00HS7_n940Contagem_SistemaCod ;
      private decimal[] P00HS7_A1117Contagem_Deflator ;
      private bool[] P00HS7_n1117Contagem_Deflator ;
      private decimal[] P00HS7_A947Contagem_Fator ;
      private bool[] P00HS7_n947Contagem_Fator ;
      private String[] P00HS7_A946Contagem_Link ;
      private bool[] P00HS7_n946Contagem_Link ;
      private String[] P00HS7_A945Contagem_Demanda ;
      private bool[] P00HS7_n945Contagem_Demanda ;
      private String[] P00HS7_A262Contagem_Status ;
      private bool[] P00HS7_n262Contagem_Status ;
      private int[] P00HS7_A214Contagem_UsuarioContadorPessoaCod ;
      private bool[] P00HS7_n214Contagem_UsuarioContadorPessoaCod ;
      private int[] P00HS7_A213Contagem_UsuarioContadorCod ;
      private bool[] P00HS7_n213Contagem_UsuarioContadorCod ;
      private decimal[] P00HS7_A1119Contagem_Divergencia ;
      private bool[] P00HS7_n1119Contagem_Divergencia ;
      private decimal[] P00HS7_A944Contagem_PFL ;
      private bool[] P00HS7_n944Contagem_PFL ;
      private decimal[] P00HS7_A943Contagem_PFB ;
      private bool[] P00HS7_n943Contagem_PFB ;
      private String[] P00HS7_A1059Contagem_Notas ;
      private bool[] P00HS7_n1059Contagem_Notas ;
      private String[] P00HS7_A196Contagem_Tipo ;
      private bool[] P00HS7_n196Contagem_Tipo ;
      private String[] P00HS7_A195Contagem_Tecnica ;
      private bool[] P00HS7_n195Contagem_Tecnica ;
      private DateTime[] P00HS7_A197Contagem_DataCriacao ;
      private int[] P00HS7_A193Contagem_AreaTrabalhoCod ;
      private int[] P00HS7_A192Contagem_Codigo ;
      private String[] P00HS7_A215Contagem_UsuarioContadorPessoaNom ;
      private bool[] P00HS7_n215Contagem_UsuarioContadorPessoaNom ;
      private String[] P00HS7_A194Contagem_AreaTrabalhoDes ;
      private bool[] P00HS7_n194Contagem_AreaTrabalhoDes ;
      private int[] P00HS7_A1118Contagem_ContratadaCod ;
      private bool[] P00HS7_n1118Contagem_ContratadaCod ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21TFContagem_Tecnica_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23TFContagem_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV39TFContagem_Status_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV141WWContagemDS_28_Tfcontagem_tecnica_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV142WWContagemDS_29_Tfcontagem_tipo_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV157WWContagemDS_44_Tfcontagem_status_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV80Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV83OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV85OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV90GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV91GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV92GridStateDynamicFilter ;
   }

   public class getwwcontagemfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00HS2( IGxContext context ,
                                             String A195Contagem_Tecnica ,
                                             IGxCollection AV141WWContagemDS_28_Tfcontagem_tecnica_sels ,
                                             String A196Contagem_Tipo ,
                                             IGxCollection AV142WWContagemDS_29_Tfcontagem_tipo_sels ,
                                             String A262Contagem_Status ,
                                             IGxCollection AV157WWContagemDS_44_Tfcontagem_status_sels ,
                                             String AV114WWContagemDS_1_Dynamicfiltersselector1 ,
                                             short AV115WWContagemDS_2_Dynamicfiltersoperator1 ,
                                             int AV116WWContagemDS_3_Contagem_contratadacod1 ,
                                             String AV117WWContagemDS_4_Contagem_areatrabalhodes1 ,
                                             String AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 ,
                                             bool AV119WWContagemDS_6_Dynamicfiltersenabled2 ,
                                             String AV120WWContagemDS_7_Dynamicfiltersselector2 ,
                                             short AV121WWContagemDS_8_Dynamicfiltersoperator2 ,
                                             int AV122WWContagemDS_9_Contagem_contratadacod2 ,
                                             String AV123WWContagemDS_10_Contagem_areatrabalhodes2 ,
                                             String AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 ,
                                             bool AV125WWContagemDS_12_Dynamicfiltersenabled3 ,
                                             String AV126WWContagemDS_13_Dynamicfiltersselector3 ,
                                             short AV127WWContagemDS_14_Dynamicfiltersoperator3 ,
                                             int AV128WWContagemDS_15_Contagem_contratadacod3 ,
                                             String AV129WWContagemDS_16_Contagem_areatrabalhodes3 ,
                                             String AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 ,
                                             int AV131WWContagemDS_18_Tfcontagem_codigo ,
                                             int AV132WWContagemDS_19_Tfcontagem_codigo_to ,
                                             int AV133WWContagemDS_20_Tfcontagem_areatrabalhocod ,
                                             int AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to ,
                                             int AV135WWContagemDS_22_Tfcontagem_contratadacod ,
                                             int AV136WWContagemDS_23_Tfcontagem_contratadacod_to ,
                                             DateTime AV137WWContagemDS_24_Tfcontagem_datacriacao ,
                                             DateTime AV138WWContagemDS_25_Tfcontagem_datacriacao_to ,
                                             String AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel ,
                                             String AV139WWContagemDS_26_Tfcontagem_areatrabalhodes ,
                                             int AV141WWContagemDS_28_Tfcontagem_tecnica_sels_Count ,
                                             int AV142WWContagemDS_29_Tfcontagem_tipo_sels_Count ,
                                             String AV144WWContagemDS_31_Tfcontagem_notas_sel ,
                                             String AV143WWContagemDS_30_Tfcontagem_notas ,
                                             decimal AV145WWContagemDS_32_Tfcontagem_pfb ,
                                             decimal AV146WWContagemDS_33_Tfcontagem_pfb_to ,
                                             decimal AV147WWContagemDS_34_Tfcontagem_pfl ,
                                             decimal AV148WWContagemDS_35_Tfcontagem_pfl_to ,
                                             decimal AV149WWContagemDS_36_Tfcontagem_divergencia ,
                                             decimal AV150WWContagemDS_37_Tfcontagem_divergencia_to ,
                                             int AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod ,
                                             int AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to ,
                                             int AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod ,
                                             int AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to ,
                                             String AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel ,
                                             String AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom ,
                                             int AV157WWContagemDS_44_Tfcontagem_status_sels_Count ,
                                             String AV159WWContagemDS_46_Tfcontagem_demanda_sel ,
                                             String AV158WWContagemDS_45_Tfcontagem_demanda ,
                                             String AV161WWContagemDS_48_Tfcontagem_link_sel ,
                                             String AV160WWContagemDS_47_Tfcontagem_link ,
                                             decimal AV162WWContagemDS_49_Tfcontagem_fator ,
                                             decimal AV163WWContagemDS_50_Tfcontagem_fator_to ,
                                             decimal AV164WWContagemDS_51_Tfcontagem_deflator ,
                                             decimal AV165WWContagemDS_52_Tfcontagem_deflator_to ,
                                             int AV166WWContagemDS_53_Tfcontagem_sistemacod ,
                                             int AV167WWContagemDS_54_Tfcontagem_sistemacod_to ,
                                             String AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel ,
                                             String AV168WWContagemDS_55_Tfcontagem_sistemasigla ,
                                             int A1118Contagem_ContratadaCod ,
                                             String A194Contagem_AreaTrabalhoDes ,
                                             String A215Contagem_UsuarioContadorPessoaNom ,
                                             int A192Contagem_Codigo ,
                                             int A193Contagem_AreaTrabalhoCod ,
                                             DateTime A197Contagem_DataCriacao ,
                                             String A1059Contagem_Notas ,
                                             decimal A943Contagem_PFB ,
                                             decimal A944Contagem_PFL ,
                                             decimal A1119Contagem_Divergencia ,
                                             int A213Contagem_UsuarioContadorCod ,
                                             int A214Contagem_UsuarioContadorPessoaCod ,
                                             String A945Contagem_Demanda ,
                                             String A946Contagem_Link ,
                                             decimal A947Contagem_Fator ,
                                             decimal A1117Contagem_Deflator ,
                                             int A940Contagem_SistemaCod ,
                                             String A941Contagem_SistemaSigla )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [57] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contagem_AreaTrabalhoCod] AS Contagem_AreaTrabalhoCod, T3.[Sistema_Sigla] AS Contagem_SistemaSigla, T1.[Contagem_SistemaCod] AS Contagem_SistemaCod, T1.[Contagem_Deflator], T1.[Contagem_Fator], T1.[Contagem_Link], T1.[Contagem_Demanda], T1.[Contagem_Status], T4.[Usuario_PessoaCod] AS Contagem_UsuarioContadorPessoaCod, T1.[Contagem_UsuarioContadorCod] AS Contagem_UsuarioContadorCod, T1.[Contagem_Divergencia], T1.[Contagem_PFL], T1.[Contagem_PFB], T1.[Contagem_Notas], T1.[Contagem_Tipo], T1.[Contagem_Tecnica], T1.[Contagem_DataCriacao], T1.[Contagem_Codigo], T5.[Pessoa_Nome] AS Contagem_UsuarioContadorPessoaNom, T2.[AreaTrabalho_Descricao] AS Contagem_AreaTrabalhoDes, T1.[Contagem_ContratadaCod] FROM (((([Contagem] T1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[Contagem_AreaTrabalhoCod]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T1.[Contagem_SistemaCod]) LEFT JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T1.[Contagem_UsuarioContadorCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 0 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 3 ) ) && ( ! (0==AV116WWContagemDS_3_Contagem_contratadacod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] < @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] < @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 1 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 4 ) ) && ( ! (0==AV116WWContagemDS_3_Contagem_contratadacod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] = @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] = @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 2 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 5 ) ) && ( ! (0==AV116WWContagemDS_3_Contagem_contratadacod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] > @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] > @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 0 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117WWContagemDS_4_Contagem_areatrabalhodes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like @lV117WWContagemDS_4_Contagem_areatrabalhodes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like @lV117WWContagemDS_4_Contagem_areatrabalhodes1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 1 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117WWContagemDS_4_Contagem_areatrabalhodes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like '%' + @lV117WWContagemDS_4_Contagem_areatrabalhodes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like '%' + @lV117WWContagemDS_4_Contagem_areatrabalhodes1)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 0 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 1 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 0 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 3 ) ) && ( ! (0==AV122WWContagemDS_9_Contagem_contratadacod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] < @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] < @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 1 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 4 ) ) && ( ! (0==AV122WWContagemDS_9_Contagem_contratadacod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] = @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] = @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 2 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 5 ) ) && ( ! (0==AV122WWContagemDS_9_Contagem_contratadacod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] > @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] > @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 0 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV123WWContagemDS_10_Contagem_areatrabalhodes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like @lV123WWContagemDS_10_Contagem_areatrabalhodes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like @lV123WWContagemDS_10_Contagem_areatrabalhodes2)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 1 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV123WWContagemDS_10_Contagem_areatrabalhodes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like '%' + @lV123WWContagemDS_10_Contagem_areatrabalhodes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like '%' + @lV123WWContagemDS_10_Contagem_areatrabalhodes2)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 0 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 1 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 0 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 3 ) ) && ( ! (0==AV128WWContagemDS_15_Contagem_contratadacod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] < @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] < @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 1 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 4 ) ) && ( ! (0==AV128WWContagemDS_15_Contagem_contratadacod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] = @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] = @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 2 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 5 ) ) && ( ! (0==AV128WWContagemDS_15_Contagem_contratadacod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] > @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] > @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 0 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV129WWContagemDS_16_Contagem_areatrabalhodes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like @lV129WWContagemDS_16_Contagem_areatrabalhodes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like @lV129WWContagemDS_16_Contagem_areatrabalhodes3)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 1 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV129WWContagemDS_16_Contagem_areatrabalhodes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like '%' + @lV129WWContagemDS_16_Contagem_areatrabalhodes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like '%' + @lV129WWContagemDS_16_Contagem_areatrabalhodes3)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 0 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 1 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like '%' + @lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! (0==AV131WWContagemDS_18_Tfcontagem_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Codigo] >= @AV131WWContagemDS_18_Tfcontagem_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Codigo] >= @AV131WWContagemDS_18_Tfcontagem_codigo)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( ! (0==AV132WWContagemDS_19_Tfcontagem_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Codigo] <= @AV132WWContagemDS_19_Tfcontagem_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Codigo] <= @AV132WWContagemDS_19_Tfcontagem_codigo_to)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( ! (0==AV133WWContagemDS_20_Tfcontagem_areatrabalhocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_AreaTrabalhoCod] >= @AV133WWContagemDS_20_Tfcontagem_areatrabalhocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_AreaTrabalhoCod] >= @AV133WWContagemDS_20_Tfcontagem_areatrabalhocod)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( ! (0==AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_AreaTrabalhoCod] <= @AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_AreaTrabalhoCod] <= @AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to)";
            }
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( ! (0==AV135WWContagemDS_22_Tfcontagem_contratadacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] >= @AV135WWContagemDS_22_Tfcontagem_contratadacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] >= @AV135WWContagemDS_22_Tfcontagem_contratadacod)";
            }
         }
         else
         {
            GXv_int1[25] = 1;
         }
         if ( ! (0==AV136WWContagemDS_23_Tfcontagem_contratadacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] <= @AV136WWContagemDS_23_Tfcontagem_contratadacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] <= @AV136WWContagemDS_23_Tfcontagem_contratadacod_to)";
            }
         }
         else
         {
            GXv_int1[26] = 1;
         }
         if ( ! (DateTime.MinValue==AV137WWContagemDS_24_Tfcontagem_datacriacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_DataCriacao] >= @AV137WWContagemDS_24_Tfcontagem_datacriacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_DataCriacao] >= @AV137WWContagemDS_24_Tfcontagem_datacriacao)";
            }
         }
         else
         {
            GXv_int1[27] = 1;
         }
         if ( ! (DateTime.MinValue==AV138WWContagemDS_25_Tfcontagem_datacriacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_DataCriacao] <= @AV138WWContagemDS_25_Tfcontagem_datacriacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_DataCriacao] <= @AV138WWContagemDS_25_Tfcontagem_datacriacao_to)";
            }
         }
         else
         {
            GXv_int1[28] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV139WWContagemDS_26_Tfcontagem_areatrabalhodes)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like @lV139WWContagemDS_26_Tfcontagem_areatrabalhodes)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like @lV139WWContagemDS_26_Tfcontagem_areatrabalhodes)";
            }
         }
         else
         {
            GXv_int1[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] = @AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] = @AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel)";
            }
         }
         else
         {
            GXv_int1[30] = 1;
         }
         if ( AV141WWContagemDS_28_Tfcontagem_tecnica_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV141WWContagemDS_28_Tfcontagem_tecnica_sels, "T1.[Contagem_Tecnica] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV141WWContagemDS_28_Tfcontagem_tecnica_sels, "T1.[Contagem_Tecnica] IN (", ")") + ")";
            }
         }
         if ( AV142WWContagemDS_29_Tfcontagem_tipo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV142WWContagemDS_29_Tfcontagem_tipo_sels, "T1.[Contagem_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV142WWContagemDS_29_Tfcontagem_tipo_sels, "T1.[Contagem_Tipo] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV144WWContagemDS_31_Tfcontagem_notas_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV143WWContagemDS_30_Tfcontagem_notas)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Notas] like @lV143WWContagemDS_30_Tfcontagem_notas)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Notas] like @lV143WWContagemDS_30_Tfcontagem_notas)";
            }
         }
         else
         {
            GXv_int1[31] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV144WWContagemDS_31_Tfcontagem_notas_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Notas] = @AV144WWContagemDS_31_Tfcontagem_notas_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Notas] = @AV144WWContagemDS_31_Tfcontagem_notas_sel)";
            }
         }
         else
         {
            GXv_int1[32] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV145WWContagemDS_32_Tfcontagem_pfb) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_PFB] >= @AV145WWContagemDS_32_Tfcontagem_pfb)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_PFB] >= @AV145WWContagemDS_32_Tfcontagem_pfb)";
            }
         }
         else
         {
            GXv_int1[33] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV146WWContagemDS_33_Tfcontagem_pfb_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_PFB] <= @AV146WWContagemDS_33_Tfcontagem_pfb_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_PFB] <= @AV146WWContagemDS_33_Tfcontagem_pfb_to)";
            }
         }
         else
         {
            GXv_int1[34] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV147WWContagemDS_34_Tfcontagem_pfl) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_PFL] >= @AV147WWContagemDS_34_Tfcontagem_pfl)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_PFL] >= @AV147WWContagemDS_34_Tfcontagem_pfl)";
            }
         }
         else
         {
            GXv_int1[35] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV148WWContagemDS_35_Tfcontagem_pfl_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_PFL] <= @AV148WWContagemDS_35_Tfcontagem_pfl_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_PFL] <= @AV148WWContagemDS_35_Tfcontagem_pfl_to)";
            }
         }
         else
         {
            GXv_int1[36] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV149WWContagemDS_36_Tfcontagem_divergencia) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Divergencia] >= @AV149WWContagemDS_36_Tfcontagem_divergencia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Divergencia] >= @AV149WWContagemDS_36_Tfcontagem_divergencia)";
            }
         }
         else
         {
            GXv_int1[37] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV150WWContagemDS_37_Tfcontagem_divergencia_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Divergencia] <= @AV150WWContagemDS_37_Tfcontagem_divergencia_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Divergencia] <= @AV150WWContagemDS_37_Tfcontagem_divergencia_to)";
            }
         }
         else
         {
            GXv_int1[38] = 1;
         }
         if ( ! (0==AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_UsuarioContadorCod] >= @AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_UsuarioContadorCod] >= @AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod)";
            }
         }
         else
         {
            GXv_int1[39] = 1;
         }
         if ( ! (0==AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_UsuarioContadorCod] <= @AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_UsuarioContadorCod] <= @AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to)";
            }
         }
         else
         {
            GXv_int1[40] = 1;
         }
         if ( ! (0==AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_PessoaCod] >= @AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_PessoaCod] >= @AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod)";
            }
         }
         else
         {
            GXv_int1[41] = 1;
         }
         if ( ! (0==AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Usuario_PessoaCod] <= @AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Usuario_PessoaCod] <= @AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to)";
            }
         }
         else
         {
            GXv_int1[42] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom)";
            }
         }
         else
         {
            GXv_int1[43] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] = @AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel)";
            }
         }
         else
         {
            GXv_int1[44] = 1;
         }
         if ( AV157WWContagemDS_44_Tfcontagem_status_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV157WWContagemDS_44_Tfcontagem_status_sels, "T1.[Contagem_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV157WWContagemDS_44_Tfcontagem_status_sels, "T1.[Contagem_Status] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV159WWContagemDS_46_Tfcontagem_demanda_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV158WWContagemDS_45_Tfcontagem_demanda)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Demanda] like @lV158WWContagemDS_45_Tfcontagem_demanda)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Demanda] like @lV158WWContagemDS_45_Tfcontagem_demanda)";
            }
         }
         else
         {
            GXv_int1[45] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV159WWContagemDS_46_Tfcontagem_demanda_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Demanda] = @AV159WWContagemDS_46_Tfcontagem_demanda_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Demanda] = @AV159WWContagemDS_46_Tfcontagem_demanda_sel)";
            }
         }
         else
         {
            GXv_int1[46] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV161WWContagemDS_48_Tfcontagem_link_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV160WWContagemDS_47_Tfcontagem_link)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Link] like @lV160WWContagemDS_47_Tfcontagem_link)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Link] like @lV160WWContagemDS_47_Tfcontagem_link)";
            }
         }
         else
         {
            GXv_int1[47] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV161WWContagemDS_48_Tfcontagem_link_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Link] = @AV161WWContagemDS_48_Tfcontagem_link_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Link] = @AV161WWContagemDS_48_Tfcontagem_link_sel)";
            }
         }
         else
         {
            GXv_int1[48] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV162WWContagemDS_49_Tfcontagem_fator) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Fator] >= @AV162WWContagemDS_49_Tfcontagem_fator)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Fator] >= @AV162WWContagemDS_49_Tfcontagem_fator)";
            }
         }
         else
         {
            GXv_int1[49] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV163WWContagemDS_50_Tfcontagem_fator_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Fator] <= @AV163WWContagemDS_50_Tfcontagem_fator_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Fator] <= @AV163WWContagemDS_50_Tfcontagem_fator_to)";
            }
         }
         else
         {
            GXv_int1[50] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV164WWContagemDS_51_Tfcontagem_deflator) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Deflator] >= @AV164WWContagemDS_51_Tfcontagem_deflator)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Deflator] >= @AV164WWContagemDS_51_Tfcontagem_deflator)";
            }
         }
         else
         {
            GXv_int1[51] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV165WWContagemDS_52_Tfcontagem_deflator_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Deflator] <= @AV165WWContagemDS_52_Tfcontagem_deflator_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Deflator] <= @AV165WWContagemDS_52_Tfcontagem_deflator_to)";
            }
         }
         else
         {
            GXv_int1[52] = 1;
         }
         if ( ! (0==AV166WWContagemDS_53_Tfcontagem_sistemacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_SistemaCod] >= @AV166WWContagemDS_53_Tfcontagem_sistemacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_SistemaCod] >= @AV166WWContagemDS_53_Tfcontagem_sistemacod)";
            }
         }
         else
         {
            GXv_int1[53] = 1;
         }
         if ( ! (0==AV167WWContagemDS_54_Tfcontagem_sistemacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_SistemaCod] <= @AV167WWContagemDS_54_Tfcontagem_sistemacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_SistemaCod] <= @AV167WWContagemDS_54_Tfcontagem_sistemacod_to)";
            }
         }
         else
         {
            GXv_int1[54] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV168WWContagemDS_55_Tfcontagem_sistemasigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Sistema_Sigla] like @lV168WWContagemDS_55_Tfcontagem_sistemasigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Sistema_Sigla] like @lV168WWContagemDS_55_Tfcontagem_sistemasigla)";
            }
         }
         else
         {
            GXv_int1[55] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Sistema_Sigla] = @AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Sistema_Sigla] = @AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel)";
            }
         }
         else
         {
            GXv_int1[56] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Contagem_AreaTrabalhoCod]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00HS3( IGxContext context ,
                                             String A195Contagem_Tecnica ,
                                             IGxCollection AV141WWContagemDS_28_Tfcontagem_tecnica_sels ,
                                             String A196Contagem_Tipo ,
                                             IGxCollection AV142WWContagemDS_29_Tfcontagem_tipo_sels ,
                                             String A262Contagem_Status ,
                                             IGxCollection AV157WWContagemDS_44_Tfcontagem_status_sels ,
                                             String AV114WWContagemDS_1_Dynamicfiltersselector1 ,
                                             short AV115WWContagemDS_2_Dynamicfiltersoperator1 ,
                                             int AV116WWContagemDS_3_Contagem_contratadacod1 ,
                                             String AV117WWContagemDS_4_Contagem_areatrabalhodes1 ,
                                             String AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 ,
                                             bool AV119WWContagemDS_6_Dynamicfiltersenabled2 ,
                                             String AV120WWContagemDS_7_Dynamicfiltersselector2 ,
                                             short AV121WWContagemDS_8_Dynamicfiltersoperator2 ,
                                             int AV122WWContagemDS_9_Contagem_contratadacod2 ,
                                             String AV123WWContagemDS_10_Contagem_areatrabalhodes2 ,
                                             String AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 ,
                                             bool AV125WWContagemDS_12_Dynamicfiltersenabled3 ,
                                             String AV126WWContagemDS_13_Dynamicfiltersselector3 ,
                                             short AV127WWContagemDS_14_Dynamicfiltersoperator3 ,
                                             int AV128WWContagemDS_15_Contagem_contratadacod3 ,
                                             String AV129WWContagemDS_16_Contagem_areatrabalhodes3 ,
                                             String AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 ,
                                             int AV131WWContagemDS_18_Tfcontagem_codigo ,
                                             int AV132WWContagemDS_19_Tfcontagem_codigo_to ,
                                             int AV133WWContagemDS_20_Tfcontagem_areatrabalhocod ,
                                             int AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to ,
                                             int AV135WWContagemDS_22_Tfcontagem_contratadacod ,
                                             int AV136WWContagemDS_23_Tfcontagem_contratadacod_to ,
                                             DateTime AV137WWContagemDS_24_Tfcontagem_datacriacao ,
                                             DateTime AV138WWContagemDS_25_Tfcontagem_datacriacao_to ,
                                             String AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel ,
                                             String AV139WWContagemDS_26_Tfcontagem_areatrabalhodes ,
                                             int AV141WWContagemDS_28_Tfcontagem_tecnica_sels_Count ,
                                             int AV142WWContagemDS_29_Tfcontagem_tipo_sels_Count ,
                                             String AV144WWContagemDS_31_Tfcontagem_notas_sel ,
                                             String AV143WWContagemDS_30_Tfcontagem_notas ,
                                             decimal AV145WWContagemDS_32_Tfcontagem_pfb ,
                                             decimal AV146WWContagemDS_33_Tfcontagem_pfb_to ,
                                             decimal AV147WWContagemDS_34_Tfcontagem_pfl ,
                                             decimal AV148WWContagemDS_35_Tfcontagem_pfl_to ,
                                             decimal AV149WWContagemDS_36_Tfcontagem_divergencia ,
                                             decimal AV150WWContagemDS_37_Tfcontagem_divergencia_to ,
                                             int AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod ,
                                             int AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to ,
                                             int AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod ,
                                             int AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to ,
                                             String AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel ,
                                             String AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom ,
                                             int AV157WWContagemDS_44_Tfcontagem_status_sels_Count ,
                                             String AV159WWContagemDS_46_Tfcontagem_demanda_sel ,
                                             String AV158WWContagemDS_45_Tfcontagem_demanda ,
                                             String AV161WWContagemDS_48_Tfcontagem_link_sel ,
                                             String AV160WWContagemDS_47_Tfcontagem_link ,
                                             decimal AV162WWContagemDS_49_Tfcontagem_fator ,
                                             decimal AV163WWContagemDS_50_Tfcontagem_fator_to ,
                                             decimal AV164WWContagemDS_51_Tfcontagem_deflator ,
                                             decimal AV165WWContagemDS_52_Tfcontagem_deflator_to ,
                                             int AV166WWContagemDS_53_Tfcontagem_sistemacod ,
                                             int AV167WWContagemDS_54_Tfcontagem_sistemacod_to ,
                                             String AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel ,
                                             String AV168WWContagemDS_55_Tfcontagem_sistemasigla ,
                                             int A1118Contagem_ContratadaCod ,
                                             String A194Contagem_AreaTrabalhoDes ,
                                             String A215Contagem_UsuarioContadorPessoaNom ,
                                             int A192Contagem_Codigo ,
                                             int A193Contagem_AreaTrabalhoCod ,
                                             DateTime A197Contagem_DataCriacao ,
                                             String A1059Contagem_Notas ,
                                             decimal A943Contagem_PFB ,
                                             decimal A944Contagem_PFL ,
                                             decimal A1119Contagem_Divergencia ,
                                             int A213Contagem_UsuarioContadorCod ,
                                             int A214Contagem_UsuarioContadorPessoaCod ,
                                             String A945Contagem_Demanda ,
                                             String A946Contagem_Link ,
                                             decimal A947Contagem_Fator ,
                                             decimal A1117Contagem_Deflator ,
                                             int A940Contagem_SistemaCod ,
                                             String A941Contagem_SistemaSigla )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [57] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contagem_Notas], T2.[Sistema_Sigla] AS Contagem_SistemaSigla, T1.[Contagem_SistemaCod] AS Contagem_SistemaCod, T1.[Contagem_Deflator], T1.[Contagem_Fator], T1.[Contagem_Link], T1.[Contagem_Demanda], T1.[Contagem_Status], T3.[Usuario_PessoaCod] AS Contagem_UsuarioContadorPessoaCod, T1.[Contagem_UsuarioContadorCod] AS Contagem_UsuarioContadorCod, T1.[Contagem_Divergencia], T1.[Contagem_PFL], T1.[Contagem_PFB], T1.[Contagem_Tipo], T1.[Contagem_Tecnica], T1.[Contagem_DataCriacao], T1.[Contagem_AreaTrabalhoCod] AS Contagem_AreaTrabalhoCod, T1.[Contagem_Codigo], T4.[Pessoa_Nome] AS Contagem_UsuarioContadorPessoaNom, T5.[AreaTrabalho_Descricao] AS Contagem_AreaTrabalhoDes, T1.[Contagem_ContratadaCod] FROM (((([Contagem] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[Contagem_SistemaCod]) LEFT JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[Contagem_UsuarioContadorCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) INNER JOIN [AreaTrabalho] T5 WITH (NOLOCK) ON T5.[AreaTrabalho_Codigo] = T1.[Contagem_AreaTrabalhoCod])";
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 0 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 3 ) ) && ( ! (0==AV116WWContagemDS_3_Contagem_contratadacod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] < @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] < @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 1 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 4 ) ) && ( ! (0==AV116WWContagemDS_3_Contagem_contratadacod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] = @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] = @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 2 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 5 ) ) && ( ! (0==AV116WWContagemDS_3_Contagem_contratadacod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] > @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] > @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 0 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117WWContagemDS_4_Contagem_areatrabalhodes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like @lV117WWContagemDS_4_Contagem_areatrabalhodes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like @lV117WWContagemDS_4_Contagem_areatrabalhodes1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 1 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117WWContagemDS_4_Contagem_areatrabalhodes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like '%' + @lV117WWContagemDS_4_Contagem_areatrabalhodes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like '%' + @lV117WWContagemDS_4_Contagem_areatrabalhodes1)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 0 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 1 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 0 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 3 ) ) && ( ! (0==AV122WWContagemDS_9_Contagem_contratadacod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] < @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] < @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 1 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 4 ) ) && ( ! (0==AV122WWContagemDS_9_Contagem_contratadacod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] = @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] = @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 2 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 5 ) ) && ( ! (0==AV122WWContagemDS_9_Contagem_contratadacod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] > @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] > @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 0 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV123WWContagemDS_10_Contagem_areatrabalhodes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like @lV123WWContagemDS_10_Contagem_areatrabalhodes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like @lV123WWContagemDS_10_Contagem_areatrabalhodes2)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 1 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV123WWContagemDS_10_Contagem_areatrabalhodes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like '%' + @lV123WWContagemDS_10_Contagem_areatrabalhodes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like '%' + @lV123WWContagemDS_10_Contagem_areatrabalhodes2)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 0 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 1 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 0 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 3 ) ) && ( ! (0==AV128WWContagemDS_15_Contagem_contratadacod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] < @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] < @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 1 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 4 ) ) && ( ! (0==AV128WWContagemDS_15_Contagem_contratadacod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] = @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] = @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 2 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 5 ) ) && ( ! (0==AV128WWContagemDS_15_Contagem_contratadacod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] > @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] > @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 0 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV129WWContagemDS_16_Contagem_areatrabalhodes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like @lV129WWContagemDS_16_Contagem_areatrabalhodes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like @lV129WWContagemDS_16_Contagem_areatrabalhodes3)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 1 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV129WWContagemDS_16_Contagem_areatrabalhodes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like '%' + @lV129WWContagemDS_16_Contagem_areatrabalhodes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like '%' + @lV129WWContagemDS_16_Contagem_areatrabalhodes3)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 0 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 1 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ! (0==AV131WWContagemDS_18_Tfcontagem_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Codigo] >= @AV131WWContagemDS_18_Tfcontagem_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Codigo] >= @AV131WWContagemDS_18_Tfcontagem_codigo)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( ! (0==AV132WWContagemDS_19_Tfcontagem_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Codigo] <= @AV132WWContagemDS_19_Tfcontagem_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Codigo] <= @AV132WWContagemDS_19_Tfcontagem_codigo_to)";
            }
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( ! (0==AV133WWContagemDS_20_Tfcontagem_areatrabalhocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_AreaTrabalhoCod] >= @AV133WWContagemDS_20_Tfcontagem_areatrabalhocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_AreaTrabalhoCod] >= @AV133WWContagemDS_20_Tfcontagem_areatrabalhocod)";
            }
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( ! (0==AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_AreaTrabalhoCod] <= @AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_AreaTrabalhoCod] <= @AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to)";
            }
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( ! (0==AV135WWContagemDS_22_Tfcontagem_contratadacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] >= @AV135WWContagemDS_22_Tfcontagem_contratadacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] >= @AV135WWContagemDS_22_Tfcontagem_contratadacod)";
            }
         }
         else
         {
            GXv_int3[25] = 1;
         }
         if ( ! (0==AV136WWContagemDS_23_Tfcontagem_contratadacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] <= @AV136WWContagemDS_23_Tfcontagem_contratadacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] <= @AV136WWContagemDS_23_Tfcontagem_contratadacod_to)";
            }
         }
         else
         {
            GXv_int3[26] = 1;
         }
         if ( ! (DateTime.MinValue==AV137WWContagemDS_24_Tfcontagem_datacriacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_DataCriacao] >= @AV137WWContagemDS_24_Tfcontagem_datacriacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_DataCriacao] >= @AV137WWContagemDS_24_Tfcontagem_datacriacao)";
            }
         }
         else
         {
            GXv_int3[27] = 1;
         }
         if ( ! (DateTime.MinValue==AV138WWContagemDS_25_Tfcontagem_datacriacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_DataCriacao] <= @AV138WWContagemDS_25_Tfcontagem_datacriacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_DataCriacao] <= @AV138WWContagemDS_25_Tfcontagem_datacriacao_to)";
            }
         }
         else
         {
            GXv_int3[28] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV139WWContagemDS_26_Tfcontagem_areatrabalhodes)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like @lV139WWContagemDS_26_Tfcontagem_areatrabalhodes)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like @lV139WWContagemDS_26_Tfcontagem_areatrabalhodes)";
            }
         }
         else
         {
            GXv_int3[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] = @AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] = @AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel)";
            }
         }
         else
         {
            GXv_int3[30] = 1;
         }
         if ( AV141WWContagemDS_28_Tfcontagem_tecnica_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV141WWContagemDS_28_Tfcontagem_tecnica_sels, "T1.[Contagem_Tecnica] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV141WWContagemDS_28_Tfcontagem_tecnica_sels, "T1.[Contagem_Tecnica] IN (", ")") + ")";
            }
         }
         if ( AV142WWContagemDS_29_Tfcontagem_tipo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV142WWContagemDS_29_Tfcontagem_tipo_sels, "T1.[Contagem_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV142WWContagemDS_29_Tfcontagem_tipo_sels, "T1.[Contagem_Tipo] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV144WWContagemDS_31_Tfcontagem_notas_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV143WWContagemDS_30_Tfcontagem_notas)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Notas] like @lV143WWContagemDS_30_Tfcontagem_notas)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Notas] like @lV143WWContagemDS_30_Tfcontagem_notas)";
            }
         }
         else
         {
            GXv_int3[31] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV144WWContagemDS_31_Tfcontagem_notas_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Notas] = @AV144WWContagemDS_31_Tfcontagem_notas_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Notas] = @AV144WWContagemDS_31_Tfcontagem_notas_sel)";
            }
         }
         else
         {
            GXv_int3[32] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV145WWContagemDS_32_Tfcontagem_pfb) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_PFB] >= @AV145WWContagemDS_32_Tfcontagem_pfb)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_PFB] >= @AV145WWContagemDS_32_Tfcontagem_pfb)";
            }
         }
         else
         {
            GXv_int3[33] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV146WWContagemDS_33_Tfcontagem_pfb_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_PFB] <= @AV146WWContagemDS_33_Tfcontagem_pfb_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_PFB] <= @AV146WWContagemDS_33_Tfcontagem_pfb_to)";
            }
         }
         else
         {
            GXv_int3[34] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV147WWContagemDS_34_Tfcontagem_pfl) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_PFL] >= @AV147WWContagemDS_34_Tfcontagem_pfl)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_PFL] >= @AV147WWContagemDS_34_Tfcontagem_pfl)";
            }
         }
         else
         {
            GXv_int3[35] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV148WWContagemDS_35_Tfcontagem_pfl_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_PFL] <= @AV148WWContagemDS_35_Tfcontagem_pfl_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_PFL] <= @AV148WWContagemDS_35_Tfcontagem_pfl_to)";
            }
         }
         else
         {
            GXv_int3[36] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV149WWContagemDS_36_Tfcontagem_divergencia) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Divergencia] >= @AV149WWContagemDS_36_Tfcontagem_divergencia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Divergencia] >= @AV149WWContagemDS_36_Tfcontagem_divergencia)";
            }
         }
         else
         {
            GXv_int3[37] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV150WWContagemDS_37_Tfcontagem_divergencia_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Divergencia] <= @AV150WWContagemDS_37_Tfcontagem_divergencia_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Divergencia] <= @AV150WWContagemDS_37_Tfcontagem_divergencia_to)";
            }
         }
         else
         {
            GXv_int3[38] = 1;
         }
         if ( ! (0==AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_UsuarioContadorCod] >= @AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_UsuarioContadorCod] >= @AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod)";
            }
         }
         else
         {
            GXv_int3[39] = 1;
         }
         if ( ! (0==AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_UsuarioContadorCod] <= @AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_UsuarioContadorCod] <= @AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to)";
            }
         }
         else
         {
            GXv_int3[40] = 1;
         }
         if ( ! (0==AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Usuario_PessoaCod] >= @AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Usuario_PessoaCod] >= @AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod)";
            }
         }
         else
         {
            GXv_int3[41] = 1;
         }
         if ( ! (0==AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Usuario_PessoaCod] <= @AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Usuario_PessoaCod] <= @AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to)";
            }
         }
         else
         {
            GXv_int3[42] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom)";
            }
         }
         else
         {
            GXv_int3[43] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel)";
            }
         }
         else
         {
            GXv_int3[44] = 1;
         }
         if ( AV157WWContagemDS_44_Tfcontagem_status_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV157WWContagemDS_44_Tfcontagem_status_sels, "T1.[Contagem_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV157WWContagemDS_44_Tfcontagem_status_sels, "T1.[Contagem_Status] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV159WWContagemDS_46_Tfcontagem_demanda_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV158WWContagemDS_45_Tfcontagem_demanda)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Demanda] like @lV158WWContagemDS_45_Tfcontagem_demanda)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Demanda] like @lV158WWContagemDS_45_Tfcontagem_demanda)";
            }
         }
         else
         {
            GXv_int3[45] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV159WWContagemDS_46_Tfcontagem_demanda_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Demanda] = @AV159WWContagemDS_46_Tfcontagem_demanda_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Demanda] = @AV159WWContagemDS_46_Tfcontagem_demanda_sel)";
            }
         }
         else
         {
            GXv_int3[46] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV161WWContagemDS_48_Tfcontagem_link_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV160WWContagemDS_47_Tfcontagem_link)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Link] like @lV160WWContagemDS_47_Tfcontagem_link)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Link] like @lV160WWContagemDS_47_Tfcontagem_link)";
            }
         }
         else
         {
            GXv_int3[47] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV161WWContagemDS_48_Tfcontagem_link_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Link] = @AV161WWContagemDS_48_Tfcontagem_link_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Link] = @AV161WWContagemDS_48_Tfcontagem_link_sel)";
            }
         }
         else
         {
            GXv_int3[48] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV162WWContagemDS_49_Tfcontagem_fator) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Fator] >= @AV162WWContagemDS_49_Tfcontagem_fator)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Fator] >= @AV162WWContagemDS_49_Tfcontagem_fator)";
            }
         }
         else
         {
            GXv_int3[49] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV163WWContagemDS_50_Tfcontagem_fator_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Fator] <= @AV163WWContagemDS_50_Tfcontagem_fator_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Fator] <= @AV163WWContagemDS_50_Tfcontagem_fator_to)";
            }
         }
         else
         {
            GXv_int3[50] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV164WWContagemDS_51_Tfcontagem_deflator) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Deflator] >= @AV164WWContagemDS_51_Tfcontagem_deflator)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Deflator] >= @AV164WWContagemDS_51_Tfcontagem_deflator)";
            }
         }
         else
         {
            GXv_int3[51] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV165WWContagemDS_52_Tfcontagem_deflator_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Deflator] <= @AV165WWContagemDS_52_Tfcontagem_deflator_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Deflator] <= @AV165WWContagemDS_52_Tfcontagem_deflator_to)";
            }
         }
         else
         {
            GXv_int3[52] = 1;
         }
         if ( ! (0==AV166WWContagemDS_53_Tfcontagem_sistemacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_SistemaCod] >= @AV166WWContagemDS_53_Tfcontagem_sistemacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_SistemaCod] >= @AV166WWContagemDS_53_Tfcontagem_sistemacod)";
            }
         }
         else
         {
            GXv_int3[53] = 1;
         }
         if ( ! (0==AV167WWContagemDS_54_Tfcontagem_sistemacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_SistemaCod] <= @AV167WWContagemDS_54_Tfcontagem_sistemacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_SistemaCod] <= @AV167WWContagemDS_54_Tfcontagem_sistemacod_to)";
            }
         }
         else
         {
            GXv_int3[54] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV168WWContagemDS_55_Tfcontagem_sistemasigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Sigla] like @lV168WWContagemDS_55_Tfcontagem_sistemasigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Sigla] like @lV168WWContagemDS_55_Tfcontagem_sistemasigla)";
            }
         }
         else
         {
            GXv_int3[55] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Sigla] = @AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Sigla] = @AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel)";
            }
         }
         else
         {
            GXv_int3[56] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Contagem_Notas]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00HS4( IGxContext context ,
                                             String A195Contagem_Tecnica ,
                                             IGxCollection AV141WWContagemDS_28_Tfcontagem_tecnica_sels ,
                                             String A196Contagem_Tipo ,
                                             IGxCollection AV142WWContagemDS_29_Tfcontagem_tipo_sels ,
                                             String A262Contagem_Status ,
                                             IGxCollection AV157WWContagemDS_44_Tfcontagem_status_sels ,
                                             String AV114WWContagemDS_1_Dynamicfiltersselector1 ,
                                             short AV115WWContagemDS_2_Dynamicfiltersoperator1 ,
                                             int AV116WWContagemDS_3_Contagem_contratadacod1 ,
                                             String AV117WWContagemDS_4_Contagem_areatrabalhodes1 ,
                                             String AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 ,
                                             bool AV119WWContagemDS_6_Dynamicfiltersenabled2 ,
                                             String AV120WWContagemDS_7_Dynamicfiltersselector2 ,
                                             short AV121WWContagemDS_8_Dynamicfiltersoperator2 ,
                                             int AV122WWContagemDS_9_Contagem_contratadacod2 ,
                                             String AV123WWContagemDS_10_Contagem_areatrabalhodes2 ,
                                             String AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 ,
                                             bool AV125WWContagemDS_12_Dynamicfiltersenabled3 ,
                                             String AV126WWContagemDS_13_Dynamicfiltersselector3 ,
                                             short AV127WWContagemDS_14_Dynamicfiltersoperator3 ,
                                             int AV128WWContagemDS_15_Contagem_contratadacod3 ,
                                             String AV129WWContagemDS_16_Contagem_areatrabalhodes3 ,
                                             String AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 ,
                                             int AV131WWContagemDS_18_Tfcontagem_codigo ,
                                             int AV132WWContagemDS_19_Tfcontagem_codigo_to ,
                                             int AV133WWContagemDS_20_Tfcontagem_areatrabalhocod ,
                                             int AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to ,
                                             int AV135WWContagemDS_22_Tfcontagem_contratadacod ,
                                             int AV136WWContagemDS_23_Tfcontagem_contratadacod_to ,
                                             DateTime AV137WWContagemDS_24_Tfcontagem_datacriacao ,
                                             DateTime AV138WWContagemDS_25_Tfcontagem_datacriacao_to ,
                                             String AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel ,
                                             String AV139WWContagemDS_26_Tfcontagem_areatrabalhodes ,
                                             int AV141WWContagemDS_28_Tfcontagem_tecnica_sels_Count ,
                                             int AV142WWContagemDS_29_Tfcontagem_tipo_sels_Count ,
                                             String AV144WWContagemDS_31_Tfcontagem_notas_sel ,
                                             String AV143WWContagemDS_30_Tfcontagem_notas ,
                                             decimal AV145WWContagemDS_32_Tfcontagem_pfb ,
                                             decimal AV146WWContagemDS_33_Tfcontagem_pfb_to ,
                                             decimal AV147WWContagemDS_34_Tfcontagem_pfl ,
                                             decimal AV148WWContagemDS_35_Tfcontagem_pfl_to ,
                                             decimal AV149WWContagemDS_36_Tfcontagem_divergencia ,
                                             decimal AV150WWContagemDS_37_Tfcontagem_divergencia_to ,
                                             int AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod ,
                                             int AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to ,
                                             int AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod ,
                                             int AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to ,
                                             String AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel ,
                                             String AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom ,
                                             int AV157WWContagemDS_44_Tfcontagem_status_sels_Count ,
                                             String AV159WWContagemDS_46_Tfcontagem_demanda_sel ,
                                             String AV158WWContagemDS_45_Tfcontagem_demanda ,
                                             String AV161WWContagemDS_48_Tfcontagem_link_sel ,
                                             String AV160WWContagemDS_47_Tfcontagem_link ,
                                             decimal AV162WWContagemDS_49_Tfcontagem_fator ,
                                             decimal AV163WWContagemDS_50_Tfcontagem_fator_to ,
                                             decimal AV164WWContagemDS_51_Tfcontagem_deflator ,
                                             decimal AV165WWContagemDS_52_Tfcontagem_deflator_to ,
                                             int AV166WWContagemDS_53_Tfcontagem_sistemacod ,
                                             int AV167WWContagemDS_54_Tfcontagem_sistemacod_to ,
                                             String AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel ,
                                             String AV168WWContagemDS_55_Tfcontagem_sistemasigla ,
                                             int A1118Contagem_ContratadaCod ,
                                             String A194Contagem_AreaTrabalhoDes ,
                                             String A215Contagem_UsuarioContadorPessoaNom ,
                                             int A192Contagem_Codigo ,
                                             int A193Contagem_AreaTrabalhoCod ,
                                             DateTime A197Contagem_DataCriacao ,
                                             String A1059Contagem_Notas ,
                                             decimal A943Contagem_PFB ,
                                             decimal A944Contagem_PFL ,
                                             decimal A1119Contagem_Divergencia ,
                                             int A213Contagem_UsuarioContadorCod ,
                                             int A214Contagem_UsuarioContadorPessoaCod ,
                                             String A945Contagem_Demanda ,
                                             String A946Contagem_Link ,
                                             decimal A947Contagem_Fator ,
                                             decimal A1117Contagem_Deflator ,
                                             int A940Contagem_SistemaCod ,
                                             String A941Contagem_SistemaSigla )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [57] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T4.[Pessoa_Nome] AS Contagem_UsuarioContadorPessoaNom, T2.[Sistema_Sigla] AS Contagem_SistemaSigla, T1.[Contagem_SistemaCod] AS Contagem_SistemaCod, T1.[Contagem_Deflator], T1.[Contagem_Fator], T1.[Contagem_Link], T1.[Contagem_Demanda], T1.[Contagem_Status], T3.[Usuario_PessoaCod] AS Contagem_UsuarioContadorPessoaCod, T1.[Contagem_UsuarioContadorCod] AS Contagem_UsuarioContadorCod, T1.[Contagem_Divergencia], T1.[Contagem_PFL], T1.[Contagem_PFB], T1.[Contagem_Notas], T1.[Contagem_Tipo], T1.[Contagem_Tecnica], T1.[Contagem_DataCriacao], T1.[Contagem_AreaTrabalhoCod] AS Contagem_AreaTrabalhoCod, T1.[Contagem_Codigo], T5.[AreaTrabalho_Descricao] AS Contagem_AreaTrabalhoDes, T1.[Contagem_ContratadaCod] FROM (((([Contagem] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[Contagem_SistemaCod]) LEFT JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[Contagem_UsuarioContadorCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) INNER JOIN [AreaTrabalho] T5 WITH (NOLOCK) ON T5.[AreaTrabalho_Codigo] = T1.[Contagem_AreaTrabalhoCod])";
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 0 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 3 ) ) && ( ! (0==AV116WWContagemDS_3_Contagem_contratadacod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] < @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] < @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 1 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 4 ) ) && ( ! (0==AV116WWContagemDS_3_Contagem_contratadacod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] = @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] = @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 2 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 5 ) ) && ( ! (0==AV116WWContagemDS_3_Contagem_contratadacod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] > @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] > @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 0 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117WWContagemDS_4_Contagem_areatrabalhodes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like @lV117WWContagemDS_4_Contagem_areatrabalhodes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like @lV117WWContagemDS_4_Contagem_areatrabalhodes1)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 1 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117WWContagemDS_4_Contagem_areatrabalhodes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like '%' + @lV117WWContagemDS_4_Contagem_areatrabalhodes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like '%' + @lV117WWContagemDS_4_Contagem_areatrabalhodes1)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 0 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 1 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 0 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 3 ) ) && ( ! (0==AV122WWContagemDS_9_Contagem_contratadacod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] < @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] < @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 1 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 4 ) ) && ( ! (0==AV122WWContagemDS_9_Contagem_contratadacod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] = @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] = @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 2 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 5 ) ) && ( ! (0==AV122WWContagemDS_9_Contagem_contratadacod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] > @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] > @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 0 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV123WWContagemDS_10_Contagem_areatrabalhodes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like @lV123WWContagemDS_10_Contagem_areatrabalhodes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like @lV123WWContagemDS_10_Contagem_areatrabalhodes2)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 1 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV123WWContagemDS_10_Contagem_areatrabalhodes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like '%' + @lV123WWContagemDS_10_Contagem_areatrabalhodes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like '%' + @lV123WWContagemDS_10_Contagem_areatrabalhodes2)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 0 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 1 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 0 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 3 ) ) && ( ! (0==AV128WWContagemDS_15_Contagem_contratadacod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] < @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] < @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 1 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 4 ) ) && ( ! (0==AV128WWContagemDS_15_Contagem_contratadacod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] = @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] = @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 2 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 5 ) ) && ( ! (0==AV128WWContagemDS_15_Contagem_contratadacod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] > @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] > @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 0 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV129WWContagemDS_16_Contagem_areatrabalhodes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like @lV129WWContagemDS_16_Contagem_areatrabalhodes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like @lV129WWContagemDS_16_Contagem_areatrabalhodes3)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 1 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV129WWContagemDS_16_Contagem_areatrabalhodes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like '%' + @lV129WWContagemDS_16_Contagem_areatrabalhodes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like '%' + @lV129WWContagemDS_16_Contagem_areatrabalhodes3)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 0 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 1 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)";
            }
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( ! (0==AV131WWContagemDS_18_Tfcontagem_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Codigo] >= @AV131WWContagemDS_18_Tfcontagem_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Codigo] >= @AV131WWContagemDS_18_Tfcontagem_codigo)";
            }
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( ! (0==AV132WWContagemDS_19_Tfcontagem_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Codigo] <= @AV132WWContagemDS_19_Tfcontagem_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Codigo] <= @AV132WWContagemDS_19_Tfcontagem_codigo_to)";
            }
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( ! (0==AV133WWContagemDS_20_Tfcontagem_areatrabalhocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_AreaTrabalhoCod] >= @AV133WWContagemDS_20_Tfcontagem_areatrabalhocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_AreaTrabalhoCod] >= @AV133WWContagemDS_20_Tfcontagem_areatrabalhocod)";
            }
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( ! (0==AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_AreaTrabalhoCod] <= @AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_AreaTrabalhoCod] <= @AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to)";
            }
         }
         else
         {
            GXv_int5[24] = 1;
         }
         if ( ! (0==AV135WWContagemDS_22_Tfcontagem_contratadacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] >= @AV135WWContagemDS_22_Tfcontagem_contratadacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] >= @AV135WWContagemDS_22_Tfcontagem_contratadacod)";
            }
         }
         else
         {
            GXv_int5[25] = 1;
         }
         if ( ! (0==AV136WWContagemDS_23_Tfcontagem_contratadacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] <= @AV136WWContagemDS_23_Tfcontagem_contratadacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] <= @AV136WWContagemDS_23_Tfcontagem_contratadacod_to)";
            }
         }
         else
         {
            GXv_int5[26] = 1;
         }
         if ( ! (DateTime.MinValue==AV137WWContagemDS_24_Tfcontagem_datacriacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_DataCriacao] >= @AV137WWContagemDS_24_Tfcontagem_datacriacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_DataCriacao] >= @AV137WWContagemDS_24_Tfcontagem_datacriacao)";
            }
         }
         else
         {
            GXv_int5[27] = 1;
         }
         if ( ! (DateTime.MinValue==AV138WWContagemDS_25_Tfcontagem_datacriacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_DataCriacao] <= @AV138WWContagemDS_25_Tfcontagem_datacriacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_DataCriacao] <= @AV138WWContagemDS_25_Tfcontagem_datacriacao_to)";
            }
         }
         else
         {
            GXv_int5[28] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV139WWContagemDS_26_Tfcontagem_areatrabalhodes)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like @lV139WWContagemDS_26_Tfcontagem_areatrabalhodes)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like @lV139WWContagemDS_26_Tfcontagem_areatrabalhodes)";
            }
         }
         else
         {
            GXv_int5[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] = @AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] = @AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel)";
            }
         }
         else
         {
            GXv_int5[30] = 1;
         }
         if ( AV141WWContagemDS_28_Tfcontagem_tecnica_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV141WWContagemDS_28_Tfcontagem_tecnica_sels, "T1.[Contagem_Tecnica] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV141WWContagemDS_28_Tfcontagem_tecnica_sels, "T1.[Contagem_Tecnica] IN (", ")") + ")";
            }
         }
         if ( AV142WWContagemDS_29_Tfcontagem_tipo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV142WWContagemDS_29_Tfcontagem_tipo_sels, "T1.[Contagem_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV142WWContagemDS_29_Tfcontagem_tipo_sels, "T1.[Contagem_Tipo] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV144WWContagemDS_31_Tfcontagem_notas_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV143WWContagemDS_30_Tfcontagem_notas)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Notas] like @lV143WWContagemDS_30_Tfcontagem_notas)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Notas] like @lV143WWContagemDS_30_Tfcontagem_notas)";
            }
         }
         else
         {
            GXv_int5[31] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV144WWContagemDS_31_Tfcontagem_notas_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Notas] = @AV144WWContagemDS_31_Tfcontagem_notas_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Notas] = @AV144WWContagemDS_31_Tfcontagem_notas_sel)";
            }
         }
         else
         {
            GXv_int5[32] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV145WWContagemDS_32_Tfcontagem_pfb) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_PFB] >= @AV145WWContagemDS_32_Tfcontagem_pfb)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_PFB] >= @AV145WWContagemDS_32_Tfcontagem_pfb)";
            }
         }
         else
         {
            GXv_int5[33] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV146WWContagemDS_33_Tfcontagem_pfb_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_PFB] <= @AV146WWContagemDS_33_Tfcontagem_pfb_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_PFB] <= @AV146WWContagemDS_33_Tfcontagem_pfb_to)";
            }
         }
         else
         {
            GXv_int5[34] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV147WWContagemDS_34_Tfcontagem_pfl) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_PFL] >= @AV147WWContagemDS_34_Tfcontagem_pfl)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_PFL] >= @AV147WWContagemDS_34_Tfcontagem_pfl)";
            }
         }
         else
         {
            GXv_int5[35] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV148WWContagemDS_35_Tfcontagem_pfl_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_PFL] <= @AV148WWContagemDS_35_Tfcontagem_pfl_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_PFL] <= @AV148WWContagemDS_35_Tfcontagem_pfl_to)";
            }
         }
         else
         {
            GXv_int5[36] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV149WWContagemDS_36_Tfcontagem_divergencia) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Divergencia] >= @AV149WWContagemDS_36_Tfcontagem_divergencia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Divergencia] >= @AV149WWContagemDS_36_Tfcontagem_divergencia)";
            }
         }
         else
         {
            GXv_int5[37] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV150WWContagemDS_37_Tfcontagem_divergencia_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Divergencia] <= @AV150WWContagemDS_37_Tfcontagem_divergencia_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Divergencia] <= @AV150WWContagemDS_37_Tfcontagem_divergencia_to)";
            }
         }
         else
         {
            GXv_int5[38] = 1;
         }
         if ( ! (0==AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_UsuarioContadorCod] >= @AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_UsuarioContadorCod] >= @AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod)";
            }
         }
         else
         {
            GXv_int5[39] = 1;
         }
         if ( ! (0==AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_UsuarioContadorCod] <= @AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_UsuarioContadorCod] <= @AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to)";
            }
         }
         else
         {
            GXv_int5[40] = 1;
         }
         if ( ! (0==AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Usuario_PessoaCod] >= @AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Usuario_PessoaCod] >= @AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod)";
            }
         }
         else
         {
            GXv_int5[41] = 1;
         }
         if ( ! (0==AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Usuario_PessoaCod] <= @AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Usuario_PessoaCod] <= @AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to)";
            }
         }
         else
         {
            GXv_int5[42] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom)";
            }
         }
         else
         {
            GXv_int5[43] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel)";
            }
         }
         else
         {
            GXv_int5[44] = 1;
         }
         if ( AV157WWContagemDS_44_Tfcontagem_status_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV157WWContagemDS_44_Tfcontagem_status_sels, "T1.[Contagem_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV157WWContagemDS_44_Tfcontagem_status_sels, "T1.[Contagem_Status] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV159WWContagemDS_46_Tfcontagem_demanda_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV158WWContagemDS_45_Tfcontagem_demanda)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Demanda] like @lV158WWContagemDS_45_Tfcontagem_demanda)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Demanda] like @lV158WWContagemDS_45_Tfcontagem_demanda)";
            }
         }
         else
         {
            GXv_int5[45] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV159WWContagemDS_46_Tfcontagem_demanda_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Demanda] = @AV159WWContagemDS_46_Tfcontagem_demanda_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Demanda] = @AV159WWContagemDS_46_Tfcontagem_demanda_sel)";
            }
         }
         else
         {
            GXv_int5[46] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV161WWContagemDS_48_Tfcontagem_link_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV160WWContagemDS_47_Tfcontagem_link)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Link] like @lV160WWContagemDS_47_Tfcontagem_link)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Link] like @lV160WWContagemDS_47_Tfcontagem_link)";
            }
         }
         else
         {
            GXv_int5[47] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV161WWContagemDS_48_Tfcontagem_link_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Link] = @AV161WWContagemDS_48_Tfcontagem_link_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Link] = @AV161WWContagemDS_48_Tfcontagem_link_sel)";
            }
         }
         else
         {
            GXv_int5[48] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV162WWContagemDS_49_Tfcontagem_fator) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Fator] >= @AV162WWContagemDS_49_Tfcontagem_fator)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Fator] >= @AV162WWContagemDS_49_Tfcontagem_fator)";
            }
         }
         else
         {
            GXv_int5[49] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV163WWContagemDS_50_Tfcontagem_fator_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Fator] <= @AV163WWContagemDS_50_Tfcontagem_fator_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Fator] <= @AV163WWContagemDS_50_Tfcontagem_fator_to)";
            }
         }
         else
         {
            GXv_int5[50] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV164WWContagemDS_51_Tfcontagem_deflator) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Deflator] >= @AV164WWContagemDS_51_Tfcontagem_deflator)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Deflator] >= @AV164WWContagemDS_51_Tfcontagem_deflator)";
            }
         }
         else
         {
            GXv_int5[51] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV165WWContagemDS_52_Tfcontagem_deflator_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Deflator] <= @AV165WWContagemDS_52_Tfcontagem_deflator_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Deflator] <= @AV165WWContagemDS_52_Tfcontagem_deflator_to)";
            }
         }
         else
         {
            GXv_int5[52] = 1;
         }
         if ( ! (0==AV166WWContagemDS_53_Tfcontagem_sistemacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_SistemaCod] >= @AV166WWContagemDS_53_Tfcontagem_sistemacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_SistemaCod] >= @AV166WWContagemDS_53_Tfcontagem_sistemacod)";
            }
         }
         else
         {
            GXv_int5[53] = 1;
         }
         if ( ! (0==AV167WWContagemDS_54_Tfcontagem_sistemacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_SistemaCod] <= @AV167WWContagemDS_54_Tfcontagem_sistemacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_SistemaCod] <= @AV167WWContagemDS_54_Tfcontagem_sistemacod_to)";
            }
         }
         else
         {
            GXv_int5[54] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV168WWContagemDS_55_Tfcontagem_sistemasigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Sigla] like @lV168WWContagemDS_55_Tfcontagem_sistemasigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Sigla] like @lV168WWContagemDS_55_Tfcontagem_sistemasigla)";
            }
         }
         else
         {
            GXv_int5[55] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Sigla] = @AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Sigla] = @AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel)";
            }
         }
         else
         {
            GXv_int5[56] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T4.[Pessoa_Nome]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00HS5( IGxContext context ,
                                             String A195Contagem_Tecnica ,
                                             IGxCollection AV141WWContagemDS_28_Tfcontagem_tecnica_sels ,
                                             String A196Contagem_Tipo ,
                                             IGxCollection AV142WWContagemDS_29_Tfcontagem_tipo_sels ,
                                             String A262Contagem_Status ,
                                             IGxCollection AV157WWContagemDS_44_Tfcontagem_status_sels ,
                                             String AV114WWContagemDS_1_Dynamicfiltersselector1 ,
                                             short AV115WWContagemDS_2_Dynamicfiltersoperator1 ,
                                             int AV116WWContagemDS_3_Contagem_contratadacod1 ,
                                             String AV117WWContagemDS_4_Contagem_areatrabalhodes1 ,
                                             String AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 ,
                                             bool AV119WWContagemDS_6_Dynamicfiltersenabled2 ,
                                             String AV120WWContagemDS_7_Dynamicfiltersselector2 ,
                                             short AV121WWContagemDS_8_Dynamicfiltersoperator2 ,
                                             int AV122WWContagemDS_9_Contagem_contratadacod2 ,
                                             String AV123WWContagemDS_10_Contagem_areatrabalhodes2 ,
                                             String AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 ,
                                             bool AV125WWContagemDS_12_Dynamicfiltersenabled3 ,
                                             String AV126WWContagemDS_13_Dynamicfiltersselector3 ,
                                             short AV127WWContagemDS_14_Dynamicfiltersoperator3 ,
                                             int AV128WWContagemDS_15_Contagem_contratadacod3 ,
                                             String AV129WWContagemDS_16_Contagem_areatrabalhodes3 ,
                                             String AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 ,
                                             int AV131WWContagemDS_18_Tfcontagem_codigo ,
                                             int AV132WWContagemDS_19_Tfcontagem_codigo_to ,
                                             int AV133WWContagemDS_20_Tfcontagem_areatrabalhocod ,
                                             int AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to ,
                                             int AV135WWContagemDS_22_Tfcontagem_contratadacod ,
                                             int AV136WWContagemDS_23_Tfcontagem_contratadacod_to ,
                                             DateTime AV137WWContagemDS_24_Tfcontagem_datacriacao ,
                                             DateTime AV138WWContagemDS_25_Tfcontagem_datacriacao_to ,
                                             String AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel ,
                                             String AV139WWContagemDS_26_Tfcontagem_areatrabalhodes ,
                                             int AV141WWContagemDS_28_Tfcontagem_tecnica_sels_Count ,
                                             int AV142WWContagemDS_29_Tfcontagem_tipo_sels_Count ,
                                             String AV144WWContagemDS_31_Tfcontagem_notas_sel ,
                                             String AV143WWContagemDS_30_Tfcontagem_notas ,
                                             decimal AV145WWContagemDS_32_Tfcontagem_pfb ,
                                             decimal AV146WWContagemDS_33_Tfcontagem_pfb_to ,
                                             decimal AV147WWContagemDS_34_Tfcontagem_pfl ,
                                             decimal AV148WWContagemDS_35_Tfcontagem_pfl_to ,
                                             decimal AV149WWContagemDS_36_Tfcontagem_divergencia ,
                                             decimal AV150WWContagemDS_37_Tfcontagem_divergencia_to ,
                                             int AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod ,
                                             int AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to ,
                                             int AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod ,
                                             int AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to ,
                                             String AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel ,
                                             String AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom ,
                                             int AV157WWContagemDS_44_Tfcontagem_status_sels_Count ,
                                             String AV159WWContagemDS_46_Tfcontagem_demanda_sel ,
                                             String AV158WWContagemDS_45_Tfcontagem_demanda ,
                                             String AV161WWContagemDS_48_Tfcontagem_link_sel ,
                                             String AV160WWContagemDS_47_Tfcontagem_link ,
                                             decimal AV162WWContagemDS_49_Tfcontagem_fator ,
                                             decimal AV163WWContagemDS_50_Tfcontagem_fator_to ,
                                             decimal AV164WWContagemDS_51_Tfcontagem_deflator ,
                                             decimal AV165WWContagemDS_52_Tfcontagem_deflator_to ,
                                             int AV166WWContagemDS_53_Tfcontagem_sistemacod ,
                                             int AV167WWContagemDS_54_Tfcontagem_sistemacod_to ,
                                             String AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel ,
                                             String AV168WWContagemDS_55_Tfcontagem_sistemasigla ,
                                             int A1118Contagem_ContratadaCod ,
                                             String A194Contagem_AreaTrabalhoDes ,
                                             String A215Contagem_UsuarioContadorPessoaNom ,
                                             int A192Contagem_Codigo ,
                                             int A193Contagem_AreaTrabalhoCod ,
                                             DateTime A197Contagem_DataCriacao ,
                                             String A1059Contagem_Notas ,
                                             decimal A943Contagem_PFB ,
                                             decimal A944Contagem_PFL ,
                                             decimal A1119Contagem_Divergencia ,
                                             int A213Contagem_UsuarioContadorCod ,
                                             int A214Contagem_UsuarioContadorPessoaCod ,
                                             String A945Contagem_Demanda ,
                                             String A946Contagem_Link ,
                                             decimal A947Contagem_Fator ,
                                             decimal A1117Contagem_Deflator ,
                                             int A940Contagem_SistemaCod ,
                                             String A941Contagem_SistemaSigla )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [57] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contagem_Demanda], T2.[Sistema_Sigla] AS Contagem_SistemaSigla, T1.[Contagem_SistemaCod] AS Contagem_SistemaCod, T1.[Contagem_Deflator], T1.[Contagem_Fator], T1.[Contagem_Link], T1.[Contagem_Status], T3.[Usuario_PessoaCod] AS Contagem_UsuarioContadorPessoaCod, T1.[Contagem_UsuarioContadorCod] AS Contagem_UsuarioContadorCod, T1.[Contagem_Divergencia], T1.[Contagem_PFL], T1.[Contagem_PFB], T1.[Contagem_Notas], T1.[Contagem_Tipo], T1.[Contagem_Tecnica], T1.[Contagem_DataCriacao], T1.[Contagem_AreaTrabalhoCod] AS Contagem_AreaTrabalhoCod, T1.[Contagem_Codigo], T4.[Pessoa_Nome] AS Contagem_UsuarioContadorPessoaNom, T5.[AreaTrabalho_Descricao] AS Contagem_AreaTrabalhoDes, T1.[Contagem_ContratadaCod] FROM (((([Contagem] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[Contagem_SistemaCod]) LEFT JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[Contagem_UsuarioContadorCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) INNER JOIN [AreaTrabalho] T5 WITH (NOLOCK) ON T5.[AreaTrabalho_Codigo] = T1.[Contagem_AreaTrabalhoCod])";
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 0 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 3 ) ) && ( ! (0==AV116WWContagemDS_3_Contagem_contratadacod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] < @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] < @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
         }
         else
         {
            GXv_int7[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 1 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 4 ) ) && ( ! (0==AV116WWContagemDS_3_Contagem_contratadacod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] = @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] = @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
         }
         else
         {
            GXv_int7[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 2 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 5 ) ) && ( ! (0==AV116WWContagemDS_3_Contagem_contratadacod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] > @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] > @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
         }
         else
         {
            GXv_int7[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 0 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117WWContagemDS_4_Contagem_areatrabalhodes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like @lV117WWContagemDS_4_Contagem_areatrabalhodes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like @lV117WWContagemDS_4_Contagem_areatrabalhodes1)";
            }
         }
         else
         {
            GXv_int7[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 1 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117WWContagemDS_4_Contagem_areatrabalhodes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like '%' + @lV117WWContagemDS_4_Contagem_areatrabalhodes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like '%' + @lV117WWContagemDS_4_Contagem_areatrabalhodes1)";
            }
         }
         else
         {
            GXv_int7[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 0 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)";
            }
         }
         else
         {
            GXv_int7[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 1 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)";
            }
         }
         else
         {
            GXv_int7[6] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 0 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 3 ) ) && ( ! (0==AV122WWContagemDS_9_Contagem_contratadacod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] < @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] < @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
         }
         else
         {
            GXv_int7[7] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 1 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 4 ) ) && ( ! (0==AV122WWContagemDS_9_Contagem_contratadacod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] = @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] = @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
         }
         else
         {
            GXv_int7[8] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 2 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 5 ) ) && ( ! (0==AV122WWContagemDS_9_Contagem_contratadacod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] > @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] > @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
         }
         else
         {
            GXv_int7[9] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 0 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV123WWContagemDS_10_Contagem_areatrabalhodes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like @lV123WWContagemDS_10_Contagem_areatrabalhodes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like @lV123WWContagemDS_10_Contagem_areatrabalhodes2)";
            }
         }
         else
         {
            GXv_int7[10] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 1 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV123WWContagemDS_10_Contagem_areatrabalhodes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like '%' + @lV123WWContagemDS_10_Contagem_areatrabalhodes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like '%' + @lV123WWContagemDS_10_Contagem_areatrabalhodes2)";
            }
         }
         else
         {
            GXv_int7[11] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 0 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)";
            }
         }
         else
         {
            GXv_int7[12] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 1 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)";
            }
         }
         else
         {
            GXv_int7[13] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 0 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 3 ) ) && ( ! (0==AV128WWContagemDS_15_Contagem_contratadacod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] < @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] < @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
         }
         else
         {
            GXv_int7[14] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 1 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 4 ) ) && ( ! (0==AV128WWContagemDS_15_Contagem_contratadacod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] = @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] = @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
         }
         else
         {
            GXv_int7[15] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 2 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 5 ) ) && ( ! (0==AV128WWContagemDS_15_Contagem_contratadacod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] > @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] > @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
         }
         else
         {
            GXv_int7[16] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 0 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV129WWContagemDS_16_Contagem_areatrabalhodes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like @lV129WWContagemDS_16_Contagem_areatrabalhodes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like @lV129WWContagemDS_16_Contagem_areatrabalhodes3)";
            }
         }
         else
         {
            GXv_int7[17] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 1 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV129WWContagemDS_16_Contagem_areatrabalhodes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like '%' + @lV129WWContagemDS_16_Contagem_areatrabalhodes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like '%' + @lV129WWContagemDS_16_Contagem_areatrabalhodes3)";
            }
         }
         else
         {
            GXv_int7[18] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 0 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)";
            }
         }
         else
         {
            GXv_int7[19] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 1 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)";
            }
         }
         else
         {
            GXv_int7[20] = 1;
         }
         if ( ! (0==AV131WWContagemDS_18_Tfcontagem_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Codigo] >= @AV131WWContagemDS_18_Tfcontagem_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Codigo] >= @AV131WWContagemDS_18_Tfcontagem_codigo)";
            }
         }
         else
         {
            GXv_int7[21] = 1;
         }
         if ( ! (0==AV132WWContagemDS_19_Tfcontagem_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Codigo] <= @AV132WWContagemDS_19_Tfcontagem_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Codigo] <= @AV132WWContagemDS_19_Tfcontagem_codigo_to)";
            }
         }
         else
         {
            GXv_int7[22] = 1;
         }
         if ( ! (0==AV133WWContagemDS_20_Tfcontagem_areatrabalhocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_AreaTrabalhoCod] >= @AV133WWContagemDS_20_Tfcontagem_areatrabalhocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_AreaTrabalhoCod] >= @AV133WWContagemDS_20_Tfcontagem_areatrabalhocod)";
            }
         }
         else
         {
            GXv_int7[23] = 1;
         }
         if ( ! (0==AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_AreaTrabalhoCod] <= @AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_AreaTrabalhoCod] <= @AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to)";
            }
         }
         else
         {
            GXv_int7[24] = 1;
         }
         if ( ! (0==AV135WWContagemDS_22_Tfcontagem_contratadacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] >= @AV135WWContagemDS_22_Tfcontagem_contratadacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] >= @AV135WWContagemDS_22_Tfcontagem_contratadacod)";
            }
         }
         else
         {
            GXv_int7[25] = 1;
         }
         if ( ! (0==AV136WWContagemDS_23_Tfcontagem_contratadacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] <= @AV136WWContagemDS_23_Tfcontagem_contratadacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] <= @AV136WWContagemDS_23_Tfcontagem_contratadacod_to)";
            }
         }
         else
         {
            GXv_int7[26] = 1;
         }
         if ( ! (DateTime.MinValue==AV137WWContagemDS_24_Tfcontagem_datacriacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_DataCriacao] >= @AV137WWContagemDS_24_Tfcontagem_datacriacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_DataCriacao] >= @AV137WWContagemDS_24_Tfcontagem_datacriacao)";
            }
         }
         else
         {
            GXv_int7[27] = 1;
         }
         if ( ! (DateTime.MinValue==AV138WWContagemDS_25_Tfcontagem_datacriacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_DataCriacao] <= @AV138WWContagemDS_25_Tfcontagem_datacriacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_DataCriacao] <= @AV138WWContagemDS_25_Tfcontagem_datacriacao_to)";
            }
         }
         else
         {
            GXv_int7[28] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV139WWContagemDS_26_Tfcontagem_areatrabalhodes)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like @lV139WWContagemDS_26_Tfcontagem_areatrabalhodes)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like @lV139WWContagemDS_26_Tfcontagem_areatrabalhodes)";
            }
         }
         else
         {
            GXv_int7[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] = @AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] = @AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel)";
            }
         }
         else
         {
            GXv_int7[30] = 1;
         }
         if ( AV141WWContagemDS_28_Tfcontagem_tecnica_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV141WWContagemDS_28_Tfcontagem_tecnica_sels, "T1.[Contagem_Tecnica] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV141WWContagemDS_28_Tfcontagem_tecnica_sels, "T1.[Contagem_Tecnica] IN (", ")") + ")";
            }
         }
         if ( AV142WWContagemDS_29_Tfcontagem_tipo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV142WWContagemDS_29_Tfcontagem_tipo_sels, "T1.[Contagem_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV142WWContagemDS_29_Tfcontagem_tipo_sels, "T1.[Contagem_Tipo] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV144WWContagemDS_31_Tfcontagem_notas_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV143WWContagemDS_30_Tfcontagem_notas)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Notas] like @lV143WWContagemDS_30_Tfcontagem_notas)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Notas] like @lV143WWContagemDS_30_Tfcontagem_notas)";
            }
         }
         else
         {
            GXv_int7[31] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV144WWContagemDS_31_Tfcontagem_notas_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Notas] = @AV144WWContagemDS_31_Tfcontagem_notas_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Notas] = @AV144WWContagemDS_31_Tfcontagem_notas_sel)";
            }
         }
         else
         {
            GXv_int7[32] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV145WWContagemDS_32_Tfcontagem_pfb) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_PFB] >= @AV145WWContagemDS_32_Tfcontagem_pfb)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_PFB] >= @AV145WWContagemDS_32_Tfcontagem_pfb)";
            }
         }
         else
         {
            GXv_int7[33] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV146WWContagemDS_33_Tfcontagem_pfb_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_PFB] <= @AV146WWContagemDS_33_Tfcontagem_pfb_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_PFB] <= @AV146WWContagemDS_33_Tfcontagem_pfb_to)";
            }
         }
         else
         {
            GXv_int7[34] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV147WWContagemDS_34_Tfcontagem_pfl) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_PFL] >= @AV147WWContagemDS_34_Tfcontagem_pfl)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_PFL] >= @AV147WWContagemDS_34_Tfcontagem_pfl)";
            }
         }
         else
         {
            GXv_int7[35] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV148WWContagemDS_35_Tfcontagem_pfl_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_PFL] <= @AV148WWContagemDS_35_Tfcontagem_pfl_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_PFL] <= @AV148WWContagemDS_35_Tfcontagem_pfl_to)";
            }
         }
         else
         {
            GXv_int7[36] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV149WWContagemDS_36_Tfcontagem_divergencia) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Divergencia] >= @AV149WWContagemDS_36_Tfcontagem_divergencia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Divergencia] >= @AV149WWContagemDS_36_Tfcontagem_divergencia)";
            }
         }
         else
         {
            GXv_int7[37] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV150WWContagemDS_37_Tfcontagem_divergencia_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Divergencia] <= @AV150WWContagemDS_37_Tfcontagem_divergencia_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Divergencia] <= @AV150WWContagemDS_37_Tfcontagem_divergencia_to)";
            }
         }
         else
         {
            GXv_int7[38] = 1;
         }
         if ( ! (0==AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_UsuarioContadorCod] >= @AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_UsuarioContadorCod] >= @AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod)";
            }
         }
         else
         {
            GXv_int7[39] = 1;
         }
         if ( ! (0==AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_UsuarioContadorCod] <= @AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_UsuarioContadorCod] <= @AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to)";
            }
         }
         else
         {
            GXv_int7[40] = 1;
         }
         if ( ! (0==AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Usuario_PessoaCod] >= @AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Usuario_PessoaCod] >= @AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod)";
            }
         }
         else
         {
            GXv_int7[41] = 1;
         }
         if ( ! (0==AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Usuario_PessoaCod] <= @AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Usuario_PessoaCod] <= @AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to)";
            }
         }
         else
         {
            GXv_int7[42] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom)";
            }
         }
         else
         {
            GXv_int7[43] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel)";
            }
         }
         else
         {
            GXv_int7[44] = 1;
         }
         if ( AV157WWContagemDS_44_Tfcontagem_status_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV157WWContagemDS_44_Tfcontagem_status_sels, "T1.[Contagem_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV157WWContagemDS_44_Tfcontagem_status_sels, "T1.[Contagem_Status] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV159WWContagemDS_46_Tfcontagem_demanda_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV158WWContagemDS_45_Tfcontagem_demanda)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Demanda] like @lV158WWContagemDS_45_Tfcontagem_demanda)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Demanda] like @lV158WWContagemDS_45_Tfcontagem_demanda)";
            }
         }
         else
         {
            GXv_int7[45] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV159WWContagemDS_46_Tfcontagem_demanda_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Demanda] = @AV159WWContagemDS_46_Tfcontagem_demanda_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Demanda] = @AV159WWContagemDS_46_Tfcontagem_demanda_sel)";
            }
         }
         else
         {
            GXv_int7[46] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV161WWContagemDS_48_Tfcontagem_link_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV160WWContagemDS_47_Tfcontagem_link)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Link] like @lV160WWContagemDS_47_Tfcontagem_link)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Link] like @lV160WWContagemDS_47_Tfcontagem_link)";
            }
         }
         else
         {
            GXv_int7[47] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV161WWContagemDS_48_Tfcontagem_link_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Link] = @AV161WWContagemDS_48_Tfcontagem_link_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Link] = @AV161WWContagemDS_48_Tfcontagem_link_sel)";
            }
         }
         else
         {
            GXv_int7[48] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV162WWContagemDS_49_Tfcontagem_fator) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Fator] >= @AV162WWContagemDS_49_Tfcontagem_fator)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Fator] >= @AV162WWContagemDS_49_Tfcontagem_fator)";
            }
         }
         else
         {
            GXv_int7[49] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV163WWContagemDS_50_Tfcontagem_fator_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Fator] <= @AV163WWContagemDS_50_Tfcontagem_fator_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Fator] <= @AV163WWContagemDS_50_Tfcontagem_fator_to)";
            }
         }
         else
         {
            GXv_int7[50] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV164WWContagemDS_51_Tfcontagem_deflator) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Deflator] >= @AV164WWContagemDS_51_Tfcontagem_deflator)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Deflator] >= @AV164WWContagemDS_51_Tfcontagem_deflator)";
            }
         }
         else
         {
            GXv_int7[51] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV165WWContagemDS_52_Tfcontagem_deflator_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Deflator] <= @AV165WWContagemDS_52_Tfcontagem_deflator_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Deflator] <= @AV165WWContagemDS_52_Tfcontagem_deflator_to)";
            }
         }
         else
         {
            GXv_int7[52] = 1;
         }
         if ( ! (0==AV166WWContagemDS_53_Tfcontagem_sistemacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_SistemaCod] >= @AV166WWContagemDS_53_Tfcontagem_sistemacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_SistemaCod] >= @AV166WWContagemDS_53_Tfcontagem_sistemacod)";
            }
         }
         else
         {
            GXv_int7[53] = 1;
         }
         if ( ! (0==AV167WWContagemDS_54_Tfcontagem_sistemacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_SistemaCod] <= @AV167WWContagemDS_54_Tfcontagem_sistemacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_SistemaCod] <= @AV167WWContagemDS_54_Tfcontagem_sistemacod_to)";
            }
         }
         else
         {
            GXv_int7[54] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV168WWContagemDS_55_Tfcontagem_sistemasigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Sigla] like @lV168WWContagemDS_55_Tfcontagem_sistemasigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Sigla] like @lV168WWContagemDS_55_Tfcontagem_sistemasigla)";
            }
         }
         else
         {
            GXv_int7[55] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Sigla] = @AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Sigla] = @AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel)";
            }
         }
         else
         {
            GXv_int7[56] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Contagem_Demanda]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      protected Object[] conditional_P00HS6( IGxContext context ,
                                             String A195Contagem_Tecnica ,
                                             IGxCollection AV141WWContagemDS_28_Tfcontagem_tecnica_sels ,
                                             String A196Contagem_Tipo ,
                                             IGxCollection AV142WWContagemDS_29_Tfcontagem_tipo_sels ,
                                             String A262Contagem_Status ,
                                             IGxCollection AV157WWContagemDS_44_Tfcontagem_status_sels ,
                                             String AV114WWContagemDS_1_Dynamicfiltersselector1 ,
                                             short AV115WWContagemDS_2_Dynamicfiltersoperator1 ,
                                             int AV116WWContagemDS_3_Contagem_contratadacod1 ,
                                             String AV117WWContagemDS_4_Contagem_areatrabalhodes1 ,
                                             String AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 ,
                                             bool AV119WWContagemDS_6_Dynamicfiltersenabled2 ,
                                             String AV120WWContagemDS_7_Dynamicfiltersselector2 ,
                                             short AV121WWContagemDS_8_Dynamicfiltersoperator2 ,
                                             int AV122WWContagemDS_9_Contagem_contratadacod2 ,
                                             String AV123WWContagemDS_10_Contagem_areatrabalhodes2 ,
                                             String AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 ,
                                             bool AV125WWContagemDS_12_Dynamicfiltersenabled3 ,
                                             String AV126WWContagemDS_13_Dynamicfiltersselector3 ,
                                             short AV127WWContagemDS_14_Dynamicfiltersoperator3 ,
                                             int AV128WWContagemDS_15_Contagem_contratadacod3 ,
                                             String AV129WWContagemDS_16_Contagem_areatrabalhodes3 ,
                                             String AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 ,
                                             int AV131WWContagemDS_18_Tfcontagem_codigo ,
                                             int AV132WWContagemDS_19_Tfcontagem_codigo_to ,
                                             int AV133WWContagemDS_20_Tfcontagem_areatrabalhocod ,
                                             int AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to ,
                                             int AV135WWContagemDS_22_Tfcontagem_contratadacod ,
                                             int AV136WWContagemDS_23_Tfcontagem_contratadacod_to ,
                                             DateTime AV137WWContagemDS_24_Tfcontagem_datacriacao ,
                                             DateTime AV138WWContagemDS_25_Tfcontagem_datacriacao_to ,
                                             String AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel ,
                                             String AV139WWContagemDS_26_Tfcontagem_areatrabalhodes ,
                                             int AV141WWContagemDS_28_Tfcontagem_tecnica_sels_Count ,
                                             int AV142WWContagemDS_29_Tfcontagem_tipo_sels_Count ,
                                             String AV144WWContagemDS_31_Tfcontagem_notas_sel ,
                                             String AV143WWContagemDS_30_Tfcontagem_notas ,
                                             decimal AV145WWContagemDS_32_Tfcontagem_pfb ,
                                             decimal AV146WWContagemDS_33_Tfcontagem_pfb_to ,
                                             decimal AV147WWContagemDS_34_Tfcontagem_pfl ,
                                             decimal AV148WWContagemDS_35_Tfcontagem_pfl_to ,
                                             decimal AV149WWContagemDS_36_Tfcontagem_divergencia ,
                                             decimal AV150WWContagemDS_37_Tfcontagem_divergencia_to ,
                                             int AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod ,
                                             int AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to ,
                                             int AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod ,
                                             int AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to ,
                                             String AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel ,
                                             String AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom ,
                                             int AV157WWContagemDS_44_Tfcontagem_status_sels_Count ,
                                             String AV159WWContagemDS_46_Tfcontagem_demanda_sel ,
                                             String AV158WWContagemDS_45_Tfcontagem_demanda ,
                                             String AV161WWContagemDS_48_Tfcontagem_link_sel ,
                                             String AV160WWContagemDS_47_Tfcontagem_link ,
                                             decimal AV162WWContagemDS_49_Tfcontagem_fator ,
                                             decimal AV163WWContagemDS_50_Tfcontagem_fator_to ,
                                             decimal AV164WWContagemDS_51_Tfcontagem_deflator ,
                                             decimal AV165WWContagemDS_52_Tfcontagem_deflator_to ,
                                             int AV166WWContagemDS_53_Tfcontagem_sistemacod ,
                                             int AV167WWContagemDS_54_Tfcontagem_sistemacod_to ,
                                             String AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel ,
                                             String AV168WWContagemDS_55_Tfcontagem_sistemasigla ,
                                             int A1118Contagem_ContratadaCod ,
                                             String A194Contagem_AreaTrabalhoDes ,
                                             String A215Contagem_UsuarioContadorPessoaNom ,
                                             int A192Contagem_Codigo ,
                                             int A193Contagem_AreaTrabalhoCod ,
                                             DateTime A197Contagem_DataCriacao ,
                                             String A1059Contagem_Notas ,
                                             decimal A943Contagem_PFB ,
                                             decimal A944Contagem_PFL ,
                                             decimal A1119Contagem_Divergencia ,
                                             int A213Contagem_UsuarioContadorCod ,
                                             int A214Contagem_UsuarioContadorPessoaCod ,
                                             String A945Contagem_Demanda ,
                                             String A946Contagem_Link ,
                                             decimal A947Contagem_Fator ,
                                             decimal A1117Contagem_Deflator ,
                                             int A940Contagem_SistemaCod ,
                                             String A941Contagem_SistemaSigla )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int9 ;
         GXv_int9 = new short [57] ;
         Object[] GXv_Object10 ;
         GXv_Object10 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contagem_Link], T2.[Sistema_Sigla] AS Contagem_SistemaSigla, T1.[Contagem_SistemaCod] AS Contagem_SistemaCod, T1.[Contagem_Deflator], T1.[Contagem_Fator], T1.[Contagem_Demanda], T1.[Contagem_Status], T3.[Usuario_PessoaCod] AS Contagem_UsuarioContadorPessoaCod, T1.[Contagem_UsuarioContadorCod] AS Contagem_UsuarioContadorCod, T1.[Contagem_Divergencia], T1.[Contagem_PFL], T1.[Contagem_PFB], T1.[Contagem_Notas], T1.[Contagem_Tipo], T1.[Contagem_Tecnica], T1.[Contagem_DataCriacao], T1.[Contagem_AreaTrabalhoCod] AS Contagem_AreaTrabalhoCod, T1.[Contagem_Codigo], T4.[Pessoa_Nome] AS Contagem_UsuarioContadorPessoaNom, T5.[AreaTrabalho_Descricao] AS Contagem_AreaTrabalhoDes, T1.[Contagem_ContratadaCod] FROM (((([Contagem] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[Contagem_SistemaCod]) LEFT JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[Contagem_UsuarioContadorCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) INNER JOIN [AreaTrabalho] T5 WITH (NOLOCK) ON T5.[AreaTrabalho_Codigo] = T1.[Contagem_AreaTrabalhoCod])";
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 0 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 3 ) ) && ( ! (0==AV116WWContagemDS_3_Contagem_contratadacod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] < @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] < @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
         }
         else
         {
            GXv_int9[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 1 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 4 ) ) && ( ! (0==AV116WWContagemDS_3_Contagem_contratadacod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] = @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] = @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
         }
         else
         {
            GXv_int9[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 2 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 5 ) ) && ( ! (0==AV116WWContagemDS_3_Contagem_contratadacod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] > @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] > @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
         }
         else
         {
            GXv_int9[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 0 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117WWContagemDS_4_Contagem_areatrabalhodes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like @lV117WWContagemDS_4_Contagem_areatrabalhodes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like @lV117WWContagemDS_4_Contagem_areatrabalhodes1)";
            }
         }
         else
         {
            GXv_int9[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 1 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117WWContagemDS_4_Contagem_areatrabalhodes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like '%' + @lV117WWContagemDS_4_Contagem_areatrabalhodes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like '%' + @lV117WWContagemDS_4_Contagem_areatrabalhodes1)";
            }
         }
         else
         {
            GXv_int9[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 0 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)";
            }
         }
         else
         {
            GXv_int9[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 1 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)";
            }
         }
         else
         {
            GXv_int9[6] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 0 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 3 ) ) && ( ! (0==AV122WWContagemDS_9_Contagem_contratadacod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] < @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] < @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
         }
         else
         {
            GXv_int9[7] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 1 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 4 ) ) && ( ! (0==AV122WWContagemDS_9_Contagem_contratadacod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] = @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] = @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
         }
         else
         {
            GXv_int9[8] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 2 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 5 ) ) && ( ! (0==AV122WWContagemDS_9_Contagem_contratadacod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] > @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] > @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
         }
         else
         {
            GXv_int9[9] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 0 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV123WWContagemDS_10_Contagem_areatrabalhodes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like @lV123WWContagemDS_10_Contagem_areatrabalhodes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like @lV123WWContagemDS_10_Contagem_areatrabalhodes2)";
            }
         }
         else
         {
            GXv_int9[10] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 1 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV123WWContagemDS_10_Contagem_areatrabalhodes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like '%' + @lV123WWContagemDS_10_Contagem_areatrabalhodes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like '%' + @lV123WWContagemDS_10_Contagem_areatrabalhodes2)";
            }
         }
         else
         {
            GXv_int9[11] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 0 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)";
            }
         }
         else
         {
            GXv_int9[12] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 1 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)";
            }
         }
         else
         {
            GXv_int9[13] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 0 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 3 ) ) && ( ! (0==AV128WWContagemDS_15_Contagem_contratadacod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] < @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] < @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
         }
         else
         {
            GXv_int9[14] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 1 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 4 ) ) && ( ! (0==AV128WWContagemDS_15_Contagem_contratadacod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] = @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] = @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
         }
         else
         {
            GXv_int9[15] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 2 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 5 ) ) && ( ! (0==AV128WWContagemDS_15_Contagem_contratadacod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] > @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] > @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
         }
         else
         {
            GXv_int9[16] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 0 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV129WWContagemDS_16_Contagem_areatrabalhodes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like @lV129WWContagemDS_16_Contagem_areatrabalhodes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like @lV129WWContagemDS_16_Contagem_areatrabalhodes3)";
            }
         }
         else
         {
            GXv_int9[17] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 1 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV129WWContagemDS_16_Contagem_areatrabalhodes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like '%' + @lV129WWContagemDS_16_Contagem_areatrabalhodes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like '%' + @lV129WWContagemDS_16_Contagem_areatrabalhodes3)";
            }
         }
         else
         {
            GXv_int9[18] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 0 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)";
            }
         }
         else
         {
            GXv_int9[19] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 1 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)";
            }
         }
         else
         {
            GXv_int9[20] = 1;
         }
         if ( ! (0==AV131WWContagemDS_18_Tfcontagem_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Codigo] >= @AV131WWContagemDS_18_Tfcontagem_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Codigo] >= @AV131WWContagemDS_18_Tfcontagem_codigo)";
            }
         }
         else
         {
            GXv_int9[21] = 1;
         }
         if ( ! (0==AV132WWContagemDS_19_Tfcontagem_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Codigo] <= @AV132WWContagemDS_19_Tfcontagem_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Codigo] <= @AV132WWContagemDS_19_Tfcontagem_codigo_to)";
            }
         }
         else
         {
            GXv_int9[22] = 1;
         }
         if ( ! (0==AV133WWContagemDS_20_Tfcontagem_areatrabalhocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_AreaTrabalhoCod] >= @AV133WWContagemDS_20_Tfcontagem_areatrabalhocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_AreaTrabalhoCod] >= @AV133WWContagemDS_20_Tfcontagem_areatrabalhocod)";
            }
         }
         else
         {
            GXv_int9[23] = 1;
         }
         if ( ! (0==AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_AreaTrabalhoCod] <= @AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_AreaTrabalhoCod] <= @AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to)";
            }
         }
         else
         {
            GXv_int9[24] = 1;
         }
         if ( ! (0==AV135WWContagemDS_22_Tfcontagem_contratadacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] >= @AV135WWContagemDS_22_Tfcontagem_contratadacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] >= @AV135WWContagemDS_22_Tfcontagem_contratadacod)";
            }
         }
         else
         {
            GXv_int9[25] = 1;
         }
         if ( ! (0==AV136WWContagemDS_23_Tfcontagem_contratadacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] <= @AV136WWContagemDS_23_Tfcontagem_contratadacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] <= @AV136WWContagemDS_23_Tfcontagem_contratadacod_to)";
            }
         }
         else
         {
            GXv_int9[26] = 1;
         }
         if ( ! (DateTime.MinValue==AV137WWContagemDS_24_Tfcontagem_datacriacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_DataCriacao] >= @AV137WWContagemDS_24_Tfcontagem_datacriacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_DataCriacao] >= @AV137WWContagemDS_24_Tfcontagem_datacriacao)";
            }
         }
         else
         {
            GXv_int9[27] = 1;
         }
         if ( ! (DateTime.MinValue==AV138WWContagemDS_25_Tfcontagem_datacriacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_DataCriacao] <= @AV138WWContagemDS_25_Tfcontagem_datacriacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_DataCriacao] <= @AV138WWContagemDS_25_Tfcontagem_datacriacao_to)";
            }
         }
         else
         {
            GXv_int9[28] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV139WWContagemDS_26_Tfcontagem_areatrabalhodes)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like @lV139WWContagemDS_26_Tfcontagem_areatrabalhodes)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like @lV139WWContagemDS_26_Tfcontagem_areatrabalhodes)";
            }
         }
         else
         {
            GXv_int9[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] = @AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] = @AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel)";
            }
         }
         else
         {
            GXv_int9[30] = 1;
         }
         if ( AV141WWContagemDS_28_Tfcontagem_tecnica_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV141WWContagemDS_28_Tfcontagem_tecnica_sels, "T1.[Contagem_Tecnica] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV141WWContagemDS_28_Tfcontagem_tecnica_sels, "T1.[Contagem_Tecnica] IN (", ")") + ")";
            }
         }
         if ( AV142WWContagemDS_29_Tfcontagem_tipo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV142WWContagemDS_29_Tfcontagem_tipo_sels, "T1.[Contagem_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV142WWContagemDS_29_Tfcontagem_tipo_sels, "T1.[Contagem_Tipo] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV144WWContagemDS_31_Tfcontagem_notas_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV143WWContagemDS_30_Tfcontagem_notas)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Notas] like @lV143WWContagemDS_30_Tfcontagem_notas)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Notas] like @lV143WWContagemDS_30_Tfcontagem_notas)";
            }
         }
         else
         {
            GXv_int9[31] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV144WWContagemDS_31_Tfcontagem_notas_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Notas] = @AV144WWContagemDS_31_Tfcontagem_notas_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Notas] = @AV144WWContagemDS_31_Tfcontagem_notas_sel)";
            }
         }
         else
         {
            GXv_int9[32] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV145WWContagemDS_32_Tfcontagem_pfb) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_PFB] >= @AV145WWContagemDS_32_Tfcontagem_pfb)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_PFB] >= @AV145WWContagemDS_32_Tfcontagem_pfb)";
            }
         }
         else
         {
            GXv_int9[33] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV146WWContagemDS_33_Tfcontagem_pfb_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_PFB] <= @AV146WWContagemDS_33_Tfcontagem_pfb_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_PFB] <= @AV146WWContagemDS_33_Tfcontagem_pfb_to)";
            }
         }
         else
         {
            GXv_int9[34] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV147WWContagemDS_34_Tfcontagem_pfl) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_PFL] >= @AV147WWContagemDS_34_Tfcontagem_pfl)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_PFL] >= @AV147WWContagemDS_34_Tfcontagem_pfl)";
            }
         }
         else
         {
            GXv_int9[35] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV148WWContagemDS_35_Tfcontagem_pfl_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_PFL] <= @AV148WWContagemDS_35_Tfcontagem_pfl_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_PFL] <= @AV148WWContagemDS_35_Tfcontagem_pfl_to)";
            }
         }
         else
         {
            GXv_int9[36] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV149WWContagemDS_36_Tfcontagem_divergencia) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Divergencia] >= @AV149WWContagemDS_36_Tfcontagem_divergencia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Divergencia] >= @AV149WWContagemDS_36_Tfcontagem_divergencia)";
            }
         }
         else
         {
            GXv_int9[37] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV150WWContagemDS_37_Tfcontagem_divergencia_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Divergencia] <= @AV150WWContagemDS_37_Tfcontagem_divergencia_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Divergencia] <= @AV150WWContagemDS_37_Tfcontagem_divergencia_to)";
            }
         }
         else
         {
            GXv_int9[38] = 1;
         }
         if ( ! (0==AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_UsuarioContadorCod] >= @AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_UsuarioContadorCod] >= @AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod)";
            }
         }
         else
         {
            GXv_int9[39] = 1;
         }
         if ( ! (0==AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_UsuarioContadorCod] <= @AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_UsuarioContadorCod] <= @AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to)";
            }
         }
         else
         {
            GXv_int9[40] = 1;
         }
         if ( ! (0==AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Usuario_PessoaCod] >= @AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Usuario_PessoaCod] >= @AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod)";
            }
         }
         else
         {
            GXv_int9[41] = 1;
         }
         if ( ! (0==AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Usuario_PessoaCod] <= @AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Usuario_PessoaCod] <= @AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to)";
            }
         }
         else
         {
            GXv_int9[42] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom)";
            }
         }
         else
         {
            GXv_int9[43] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel)";
            }
         }
         else
         {
            GXv_int9[44] = 1;
         }
         if ( AV157WWContagemDS_44_Tfcontagem_status_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV157WWContagemDS_44_Tfcontagem_status_sels, "T1.[Contagem_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV157WWContagemDS_44_Tfcontagem_status_sels, "T1.[Contagem_Status] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV159WWContagemDS_46_Tfcontagem_demanda_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV158WWContagemDS_45_Tfcontagem_demanda)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Demanda] like @lV158WWContagemDS_45_Tfcontagem_demanda)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Demanda] like @lV158WWContagemDS_45_Tfcontagem_demanda)";
            }
         }
         else
         {
            GXv_int9[45] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV159WWContagemDS_46_Tfcontagem_demanda_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Demanda] = @AV159WWContagemDS_46_Tfcontagem_demanda_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Demanda] = @AV159WWContagemDS_46_Tfcontagem_demanda_sel)";
            }
         }
         else
         {
            GXv_int9[46] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV161WWContagemDS_48_Tfcontagem_link_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV160WWContagemDS_47_Tfcontagem_link)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Link] like @lV160WWContagemDS_47_Tfcontagem_link)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Link] like @lV160WWContagemDS_47_Tfcontagem_link)";
            }
         }
         else
         {
            GXv_int9[47] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV161WWContagemDS_48_Tfcontagem_link_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Link] = @AV161WWContagemDS_48_Tfcontagem_link_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Link] = @AV161WWContagemDS_48_Tfcontagem_link_sel)";
            }
         }
         else
         {
            GXv_int9[48] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV162WWContagemDS_49_Tfcontagem_fator) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Fator] >= @AV162WWContagemDS_49_Tfcontagem_fator)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Fator] >= @AV162WWContagemDS_49_Tfcontagem_fator)";
            }
         }
         else
         {
            GXv_int9[49] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV163WWContagemDS_50_Tfcontagem_fator_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Fator] <= @AV163WWContagemDS_50_Tfcontagem_fator_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Fator] <= @AV163WWContagemDS_50_Tfcontagem_fator_to)";
            }
         }
         else
         {
            GXv_int9[50] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV164WWContagemDS_51_Tfcontagem_deflator) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Deflator] >= @AV164WWContagemDS_51_Tfcontagem_deflator)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Deflator] >= @AV164WWContagemDS_51_Tfcontagem_deflator)";
            }
         }
         else
         {
            GXv_int9[51] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV165WWContagemDS_52_Tfcontagem_deflator_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Deflator] <= @AV165WWContagemDS_52_Tfcontagem_deflator_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Deflator] <= @AV165WWContagemDS_52_Tfcontagem_deflator_to)";
            }
         }
         else
         {
            GXv_int9[52] = 1;
         }
         if ( ! (0==AV166WWContagemDS_53_Tfcontagem_sistemacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_SistemaCod] >= @AV166WWContagemDS_53_Tfcontagem_sistemacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_SistemaCod] >= @AV166WWContagemDS_53_Tfcontagem_sistemacod)";
            }
         }
         else
         {
            GXv_int9[53] = 1;
         }
         if ( ! (0==AV167WWContagemDS_54_Tfcontagem_sistemacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_SistemaCod] <= @AV167WWContagemDS_54_Tfcontagem_sistemacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_SistemaCod] <= @AV167WWContagemDS_54_Tfcontagem_sistemacod_to)";
            }
         }
         else
         {
            GXv_int9[54] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV168WWContagemDS_55_Tfcontagem_sistemasigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Sigla] like @lV168WWContagemDS_55_Tfcontagem_sistemasigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Sigla] like @lV168WWContagemDS_55_Tfcontagem_sistemasigla)";
            }
         }
         else
         {
            GXv_int9[55] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Sigla] = @AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Sigla] = @AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel)";
            }
         }
         else
         {
            GXv_int9[56] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Contagem_Link]";
         GXv_Object10[0] = scmdbuf;
         GXv_Object10[1] = GXv_int9;
         return GXv_Object10 ;
      }

      protected Object[] conditional_P00HS7( IGxContext context ,
                                             String A195Contagem_Tecnica ,
                                             IGxCollection AV141WWContagemDS_28_Tfcontagem_tecnica_sels ,
                                             String A196Contagem_Tipo ,
                                             IGxCollection AV142WWContagemDS_29_Tfcontagem_tipo_sels ,
                                             String A262Contagem_Status ,
                                             IGxCollection AV157WWContagemDS_44_Tfcontagem_status_sels ,
                                             String AV114WWContagemDS_1_Dynamicfiltersselector1 ,
                                             short AV115WWContagemDS_2_Dynamicfiltersoperator1 ,
                                             int AV116WWContagemDS_3_Contagem_contratadacod1 ,
                                             String AV117WWContagemDS_4_Contagem_areatrabalhodes1 ,
                                             String AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1 ,
                                             bool AV119WWContagemDS_6_Dynamicfiltersenabled2 ,
                                             String AV120WWContagemDS_7_Dynamicfiltersselector2 ,
                                             short AV121WWContagemDS_8_Dynamicfiltersoperator2 ,
                                             int AV122WWContagemDS_9_Contagem_contratadacod2 ,
                                             String AV123WWContagemDS_10_Contagem_areatrabalhodes2 ,
                                             String AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2 ,
                                             bool AV125WWContagemDS_12_Dynamicfiltersenabled3 ,
                                             String AV126WWContagemDS_13_Dynamicfiltersselector3 ,
                                             short AV127WWContagemDS_14_Dynamicfiltersoperator3 ,
                                             int AV128WWContagemDS_15_Contagem_contratadacod3 ,
                                             String AV129WWContagemDS_16_Contagem_areatrabalhodes3 ,
                                             String AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3 ,
                                             int AV131WWContagemDS_18_Tfcontagem_codigo ,
                                             int AV132WWContagemDS_19_Tfcontagem_codigo_to ,
                                             int AV133WWContagemDS_20_Tfcontagem_areatrabalhocod ,
                                             int AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to ,
                                             int AV135WWContagemDS_22_Tfcontagem_contratadacod ,
                                             int AV136WWContagemDS_23_Tfcontagem_contratadacod_to ,
                                             DateTime AV137WWContagemDS_24_Tfcontagem_datacriacao ,
                                             DateTime AV138WWContagemDS_25_Tfcontagem_datacriacao_to ,
                                             String AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel ,
                                             String AV139WWContagemDS_26_Tfcontagem_areatrabalhodes ,
                                             int AV141WWContagemDS_28_Tfcontagem_tecnica_sels_Count ,
                                             int AV142WWContagemDS_29_Tfcontagem_tipo_sels_Count ,
                                             String AV144WWContagemDS_31_Tfcontagem_notas_sel ,
                                             String AV143WWContagemDS_30_Tfcontagem_notas ,
                                             decimal AV145WWContagemDS_32_Tfcontagem_pfb ,
                                             decimal AV146WWContagemDS_33_Tfcontagem_pfb_to ,
                                             decimal AV147WWContagemDS_34_Tfcontagem_pfl ,
                                             decimal AV148WWContagemDS_35_Tfcontagem_pfl_to ,
                                             decimal AV149WWContagemDS_36_Tfcontagem_divergencia ,
                                             decimal AV150WWContagemDS_37_Tfcontagem_divergencia_to ,
                                             int AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod ,
                                             int AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to ,
                                             int AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod ,
                                             int AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to ,
                                             String AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel ,
                                             String AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom ,
                                             int AV157WWContagemDS_44_Tfcontagem_status_sels_Count ,
                                             String AV159WWContagemDS_46_Tfcontagem_demanda_sel ,
                                             String AV158WWContagemDS_45_Tfcontagem_demanda ,
                                             String AV161WWContagemDS_48_Tfcontagem_link_sel ,
                                             String AV160WWContagemDS_47_Tfcontagem_link ,
                                             decimal AV162WWContagemDS_49_Tfcontagem_fator ,
                                             decimal AV163WWContagemDS_50_Tfcontagem_fator_to ,
                                             decimal AV164WWContagemDS_51_Tfcontagem_deflator ,
                                             decimal AV165WWContagemDS_52_Tfcontagem_deflator_to ,
                                             int AV166WWContagemDS_53_Tfcontagem_sistemacod ,
                                             int AV167WWContagemDS_54_Tfcontagem_sistemacod_to ,
                                             String AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel ,
                                             String AV168WWContagemDS_55_Tfcontagem_sistemasigla ,
                                             int A1118Contagem_ContratadaCod ,
                                             String A194Contagem_AreaTrabalhoDes ,
                                             String A215Contagem_UsuarioContadorPessoaNom ,
                                             int A192Contagem_Codigo ,
                                             int A193Contagem_AreaTrabalhoCod ,
                                             DateTime A197Contagem_DataCriacao ,
                                             String A1059Contagem_Notas ,
                                             decimal A943Contagem_PFB ,
                                             decimal A944Contagem_PFL ,
                                             decimal A1119Contagem_Divergencia ,
                                             int A213Contagem_UsuarioContadorCod ,
                                             int A214Contagem_UsuarioContadorPessoaCod ,
                                             String A945Contagem_Demanda ,
                                             String A946Contagem_Link ,
                                             decimal A947Contagem_Fator ,
                                             decimal A1117Contagem_Deflator ,
                                             int A940Contagem_SistemaCod ,
                                             String A941Contagem_SistemaSigla )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int11 ;
         GXv_int11 = new short [57] ;
         Object[] GXv_Object12 ;
         GXv_Object12 = new Object [2] ;
         scmdbuf = "SELECT T2.[Sistema_Sigla] AS Contagem_SistemaSigla, T1.[Contagem_SistemaCod] AS Contagem_SistemaCod, T1.[Contagem_Deflator], T1.[Contagem_Fator], T1.[Contagem_Link], T1.[Contagem_Demanda], T1.[Contagem_Status], T3.[Usuario_PessoaCod] AS Contagem_UsuarioContadorPessoaCod, T1.[Contagem_UsuarioContadorCod] AS Contagem_UsuarioContadorCod, T1.[Contagem_Divergencia], T1.[Contagem_PFL], T1.[Contagem_PFB], T1.[Contagem_Notas], T1.[Contagem_Tipo], T1.[Contagem_Tecnica], T1.[Contagem_DataCriacao], T1.[Contagem_AreaTrabalhoCod] AS Contagem_AreaTrabalhoCod, T1.[Contagem_Codigo], T4.[Pessoa_Nome] AS Contagem_UsuarioContadorPessoaNom, T5.[AreaTrabalho_Descricao] AS Contagem_AreaTrabalhoDes, T1.[Contagem_ContratadaCod] FROM (((([Contagem] T1 WITH (NOLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[Contagem_SistemaCod]) LEFT JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[Contagem_UsuarioContadorCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) INNER JOIN [AreaTrabalho] T5 WITH (NOLOCK) ON T5.[AreaTrabalho_Codigo] = T1.[Contagem_AreaTrabalhoCod])";
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 0 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 3 ) ) && ( ! (0==AV116WWContagemDS_3_Contagem_contratadacod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] < @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] < @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
         }
         else
         {
            GXv_int11[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 1 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 4 ) ) && ( ! (0==AV116WWContagemDS_3_Contagem_contratadacod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] = @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] = @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
         }
         else
         {
            GXv_int11[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 2 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 5 ) ) && ( ! (0==AV116WWContagemDS_3_Contagem_contratadacod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] > @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] > @AV116WWContagemDS_3_Contagem_contratadacod1)";
            }
         }
         else
         {
            GXv_int11[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 0 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117WWContagemDS_4_Contagem_areatrabalhodes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like @lV117WWContagemDS_4_Contagem_areatrabalhodes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like @lV117WWContagemDS_4_Contagem_areatrabalhodes1)";
            }
         }
         else
         {
            GXv_int11[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 1 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117WWContagemDS_4_Contagem_areatrabalhodes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like '%' + @lV117WWContagemDS_4_Contagem_areatrabalhodes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like '%' + @lV117WWContagemDS_4_Contagem_areatrabalhodes1)";
            }
         }
         else
         {
            GXv_int11[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 0 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)";
            }
         }
         else
         {
            GXv_int11[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContagemDS_1_Dynamicfiltersselector1, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 1 ) || ( AV115WWContagemDS_2_Dynamicfiltersoperator1 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1)";
            }
         }
         else
         {
            GXv_int11[6] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 0 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 3 ) ) && ( ! (0==AV122WWContagemDS_9_Contagem_contratadacod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] < @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] < @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
         }
         else
         {
            GXv_int11[7] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 1 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 4 ) ) && ( ! (0==AV122WWContagemDS_9_Contagem_contratadacod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] = @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] = @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
         }
         else
         {
            GXv_int11[8] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 2 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 5 ) ) && ( ! (0==AV122WWContagemDS_9_Contagem_contratadacod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] > @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] > @AV122WWContagemDS_9_Contagem_contratadacod2)";
            }
         }
         else
         {
            GXv_int11[9] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 0 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV123WWContagemDS_10_Contagem_areatrabalhodes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like @lV123WWContagemDS_10_Contagem_areatrabalhodes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like @lV123WWContagemDS_10_Contagem_areatrabalhodes2)";
            }
         }
         else
         {
            GXv_int11[10] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 1 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV123WWContagemDS_10_Contagem_areatrabalhodes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like '%' + @lV123WWContagemDS_10_Contagem_areatrabalhodes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like '%' + @lV123WWContagemDS_10_Contagem_areatrabalhodes2)";
            }
         }
         else
         {
            GXv_int11[11] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 0 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)";
            }
         }
         else
         {
            GXv_int11[12] = 1;
         }
         if ( AV119WWContagemDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV120WWContagemDS_7_Dynamicfiltersselector2, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 1 ) || ( AV121WWContagemDS_8_Dynamicfiltersoperator2 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2)";
            }
         }
         else
         {
            GXv_int11[13] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 0 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 3 ) ) && ( ! (0==AV128WWContagemDS_15_Contagem_contratadacod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] < @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] < @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
         }
         else
         {
            GXv_int11[14] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 1 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 4 ) ) && ( ! (0==AV128WWContagemDS_15_Contagem_contratadacod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] = @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] = @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
         }
         else
         {
            GXv_int11[15] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_CONTRATADACOD") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 2 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 5 ) ) && ( ! (0==AV128WWContagemDS_15_Contagem_contratadacod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] > @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] > @AV128WWContagemDS_15_Contagem_contratadacod3)";
            }
         }
         else
         {
            GXv_int11[16] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 0 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV129WWContagemDS_16_Contagem_areatrabalhodes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like @lV129WWContagemDS_16_Contagem_areatrabalhodes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like @lV129WWContagemDS_16_Contagem_areatrabalhodes3)";
            }
         }
         else
         {
            GXv_int11[17] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_AREATRABALHODES") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 1 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV129WWContagemDS_16_Contagem_areatrabalhodes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like '%' + @lV129WWContagemDS_16_Contagem_areatrabalhodes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like '%' + @lV129WWContagemDS_16_Contagem_areatrabalhodes3)";
            }
         }
         else
         {
            GXv_int11[18] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 0 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 2 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)";
            }
         }
         else
         {
            GXv_int11[19] = 1;
         }
         if ( AV125WWContagemDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV126WWContagemDS_13_Dynamicfiltersselector3, "CONTAGEM_USUARIOCONTADORPESSOANOM") == 0 ) && ( ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 1 ) || ( AV127WWContagemDS_14_Dynamicfiltersoperator3 == 3 ) ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3)";
            }
         }
         else
         {
            GXv_int11[20] = 1;
         }
         if ( ! (0==AV131WWContagemDS_18_Tfcontagem_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Codigo] >= @AV131WWContagemDS_18_Tfcontagem_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Codigo] >= @AV131WWContagemDS_18_Tfcontagem_codigo)";
            }
         }
         else
         {
            GXv_int11[21] = 1;
         }
         if ( ! (0==AV132WWContagemDS_19_Tfcontagem_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Codigo] <= @AV132WWContagemDS_19_Tfcontagem_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Codigo] <= @AV132WWContagemDS_19_Tfcontagem_codigo_to)";
            }
         }
         else
         {
            GXv_int11[22] = 1;
         }
         if ( ! (0==AV133WWContagemDS_20_Tfcontagem_areatrabalhocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_AreaTrabalhoCod] >= @AV133WWContagemDS_20_Tfcontagem_areatrabalhocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_AreaTrabalhoCod] >= @AV133WWContagemDS_20_Tfcontagem_areatrabalhocod)";
            }
         }
         else
         {
            GXv_int11[23] = 1;
         }
         if ( ! (0==AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_AreaTrabalhoCod] <= @AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_AreaTrabalhoCod] <= @AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to)";
            }
         }
         else
         {
            GXv_int11[24] = 1;
         }
         if ( ! (0==AV135WWContagemDS_22_Tfcontagem_contratadacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] >= @AV135WWContagemDS_22_Tfcontagem_contratadacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] >= @AV135WWContagemDS_22_Tfcontagem_contratadacod)";
            }
         }
         else
         {
            GXv_int11[25] = 1;
         }
         if ( ! (0==AV136WWContagemDS_23_Tfcontagem_contratadacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_ContratadaCod] <= @AV136WWContagemDS_23_Tfcontagem_contratadacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_ContratadaCod] <= @AV136WWContagemDS_23_Tfcontagem_contratadacod_to)";
            }
         }
         else
         {
            GXv_int11[26] = 1;
         }
         if ( ! (DateTime.MinValue==AV137WWContagemDS_24_Tfcontagem_datacriacao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_DataCriacao] >= @AV137WWContagemDS_24_Tfcontagem_datacriacao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_DataCriacao] >= @AV137WWContagemDS_24_Tfcontagem_datacriacao)";
            }
         }
         else
         {
            GXv_int11[27] = 1;
         }
         if ( ! (DateTime.MinValue==AV138WWContagemDS_25_Tfcontagem_datacriacao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_DataCriacao] <= @AV138WWContagemDS_25_Tfcontagem_datacriacao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_DataCriacao] <= @AV138WWContagemDS_25_Tfcontagem_datacriacao_to)";
            }
         }
         else
         {
            GXv_int11[28] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV139WWContagemDS_26_Tfcontagem_areatrabalhodes)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] like @lV139WWContagemDS_26_Tfcontagem_areatrabalhodes)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] like @lV139WWContagemDS_26_Tfcontagem_areatrabalhodes)";
            }
         }
         else
         {
            GXv_int11[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[AreaTrabalho_Descricao] = @AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[AreaTrabalho_Descricao] = @AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel)";
            }
         }
         else
         {
            GXv_int11[30] = 1;
         }
         if ( AV141WWContagemDS_28_Tfcontagem_tecnica_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV141WWContagemDS_28_Tfcontagem_tecnica_sels, "T1.[Contagem_Tecnica] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV141WWContagemDS_28_Tfcontagem_tecnica_sels, "T1.[Contagem_Tecnica] IN (", ")") + ")";
            }
         }
         if ( AV142WWContagemDS_29_Tfcontagem_tipo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV142WWContagemDS_29_Tfcontagem_tipo_sels, "T1.[Contagem_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV142WWContagemDS_29_Tfcontagem_tipo_sels, "T1.[Contagem_Tipo] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV144WWContagemDS_31_Tfcontagem_notas_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV143WWContagemDS_30_Tfcontagem_notas)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Notas] like @lV143WWContagemDS_30_Tfcontagem_notas)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Notas] like @lV143WWContagemDS_30_Tfcontagem_notas)";
            }
         }
         else
         {
            GXv_int11[31] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV144WWContagemDS_31_Tfcontagem_notas_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Notas] = @AV144WWContagemDS_31_Tfcontagem_notas_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Notas] = @AV144WWContagemDS_31_Tfcontagem_notas_sel)";
            }
         }
         else
         {
            GXv_int11[32] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV145WWContagemDS_32_Tfcontagem_pfb) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_PFB] >= @AV145WWContagemDS_32_Tfcontagem_pfb)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_PFB] >= @AV145WWContagemDS_32_Tfcontagem_pfb)";
            }
         }
         else
         {
            GXv_int11[33] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV146WWContagemDS_33_Tfcontagem_pfb_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_PFB] <= @AV146WWContagemDS_33_Tfcontagem_pfb_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_PFB] <= @AV146WWContagemDS_33_Tfcontagem_pfb_to)";
            }
         }
         else
         {
            GXv_int11[34] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV147WWContagemDS_34_Tfcontagem_pfl) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_PFL] >= @AV147WWContagemDS_34_Tfcontagem_pfl)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_PFL] >= @AV147WWContagemDS_34_Tfcontagem_pfl)";
            }
         }
         else
         {
            GXv_int11[35] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV148WWContagemDS_35_Tfcontagem_pfl_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_PFL] <= @AV148WWContagemDS_35_Tfcontagem_pfl_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_PFL] <= @AV148WWContagemDS_35_Tfcontagem_pfl_to)";
            }
         }
         else
         {
            GXv_int11[36] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV149WWContagemDS_36_Tfcontagem_divergencia) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Divergencia] >= @AV149WWContagemDS_36_Tfcontagem_divergencia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Divergencia] >= @AV149WWContagemDS_36_Tfcontagem_divergencia)";
            }
         }
         else
         {
            GXv_int11[37] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV150WWContagemDS_37_Tfcontagem_divergencia_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Divergencia] <= @AV150WWContagemDS_37_Tfcontagem_divergencia_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Divergencia] <= @AV150WWContagemDS_37_Tfcontagem_divergencia_to)";
            }
         }
         else
         {
            GXv_int11[38] = 1;
         }
         if ( ! (0==AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_UsuarioContadorCod] >= @AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_UsuarioContadorCod] >= @AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod)";
            }
         }
         else
         {
            GXv_int11[39] = 1;
         }
         if ( ! (0==AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_UsuarioContadorCod] <= @AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_UsuarioContadorCod] <= @AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to)";
            }
         }
         else
         {
            GXv_int11[40] = 1;
         }
         if ( ! (0==AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Usuario_PessoaCod] >= @AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Usuario_PessoaCod] >= @AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod)";
            }
         }
         else
         {
            GXv_int11[41] = 1;
         }
         if ( ! (0==AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Usuario_PessoaCod] <= @AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Usuario_PessoaCod] <= @AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to)";
            }
         }
         else
         {
            GXv_int11[42] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom)";
            }
         }
         else
         {
            GXv_int11[43] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel)";
            }
         }
         else
         {
            GXv_int11[44] = 1;
         }
         if ( AV157WWContagemDS_44_Tfcontagem_status_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV157WWContagemDS_44_Tfcontagem_status_sels, "T1.[Contagem_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV157WWContagemDS_44_Tfcontagem_status_sels, "T1.[Contagem_Status] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV159WWContagemDS_46_Tfcontagem_demanda_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV158WWContagemDS_45_Tfcontagem_demanda)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Demanda] like @lV158WWContagemDS_45_Tfcontagem_demanda)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Demanda] like @lV158WWContagemDS_45_Tfcontagem_demanda)";
            }
         }
         else
         {
            GXv_int11[45] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV159WWContagemDS_46_Tfcontagem_demanda_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Demanda] = @AV159WWContagemDS_46_Tfcontagem_demanda_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Demanda] = @AV159WWContagemDS_46_Tfcontagem_demanda_sel)";
            }
         }
         else
         {
            GXv_int11[46] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV161WWContagemDS_48_Tfcontagem_link_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV160WWContagemDS_47_Tfcontagem_link)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Link] like @lV160WWContagemDS_47_Tfcontagem_link)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Link] like @lV160WWContagemDS_47_Tfcontagem_link)";
            }
         }
         else
         {
            GXv_int11[47] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV161WWContagemDS_48_Tfcontagem_link_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Link] = @AV161WWContagemDS_48_Tfcontagem_link_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Link] = @AV161WWContagemDS_48_Tfcontagem_link_sel)";
            }
         }
         else
         {
            GXv_int11[48] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV162WWContagemDS_49_Tfcontagem_fator) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Fator] >= @AV162WWContagemDS_49_Tfcontagem_fator)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Fator] >= @AV162WWContagemDS_49_Tfcontagem_fator)";
            }
         }
         else
         {
            GXv_int11[49] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV163WWContagemDS_50_Tfcontagem_fator_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Fator] <= @AV163WWContagemDS_50_Tfcontagem_fator_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Fator] <= @AV163WWContagemDS_50_Tfcontagem_fator_to)";
            }
         }
         else
         {
            GXv_int11[50] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV164WWContagemDS_51_Tfcontagem_deflator) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Deflator] >= @AV164WWContagemDS_51_Tfcontagem_deflator)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Deflator] >= @AV164WWContagemDS_51_Tfcontagem_deflator)";
            }
         }
         else
         {
            GXv_int11[51] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV165WWContagemDS_52_Tfcontagem_deflator_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_Deflator] <= @AV165WWContagemDS_52_Tfcontagem_deflator_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_Deflator] <= @AV165WWContagemDS_52_Tfcontagem_deflator_to)";
            }
         }
         else
         {
            GXv_int11[52] = 1;
         }
         if ( ! (0==AV166WWContagemDS_53_Tfcontagem_sistemacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_SistemaCod] >= @AV166WWContagemDS_53_Tfcontagem_sistemacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_SistemaCod] >= @AV166WWContagemDS_53_Tfcontagem_sistemacod)";
            }
         }
         else
         {
            GXv_int11[53] = 1;
         }
         if ( ! (0==AV167WWContagemDS_54_Tfcontagem_sistemacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contagem_SistemaCod] <= @AV167WWContagemDS_54_Tfcontagem_sistemacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contagem_SistemaCod] <= @AV167WWContagemDS_54_Tfcontagem_sistemacod_to)";
            }
         }
         else
         {
            GXv_int11[54] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV168WWContagemDS_55_Tfcontagem_sistemasigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Sigla] like @lV168WWContagemDS_55_Tfcontagem_sistemasigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Sigla] like @lV168WWContagemDS_55_Tfcontagem_sistemasigla)";
            }
         }
         else
         {
            GXv_int11[55] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Sigla] = @AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Sigla] = @AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel)";
            }
         }
         else
         {
            GXv_int11[56] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T2.[Sistema_Sigla]";
         GXv_Object12[0] = scmdbuf;
         GXv_Object12[1] = GXv_int11;
         return GXv_Object12 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00HS2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (int)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (bool)dynConstraints[17] , (String)dynConstraints[18] , (short)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (decimal)dynConstraints[37] , (decimal)dynConstraints[38] , (decimal)dynConstraints[39] , (decimal)dynConstraints[40] , (decimal)dynConstraints[41] , (decimal)dynConstraints[42] , (int)dynConstraints[43] , (int)dynConstraints[44] , (int)dynConstraints[45] , (int)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (int)dynConstraints[49] , (String)dynConstraints[50] , (String)dynConstraints[51] , (String)dynConstraints[52] , (String)dynConstraints[53] , (decimal)dynConstraints[54] , (decimal)dynConstraints[55] , (decimal)dynConstraints[56] , (decimal)dynConstraints[57] , (int)dynConstraints[58] , (int)dynConstraints[59] , (String)dynConstraints[60] , (String)dynConstraints[61] , (int)dynConstraints[62] , (String)dynConstraints[63] , (String)dynConstraints[64] , (int)dynConstraints[65] , (int)dynConstraints[66] , (DateTime)dynConstraints[67] , (String)dynConstraints[68] , (decimal)dynConstraints[69] , (decimal)dynConstraints[70] , (decimal)dynConstraints[71] , (int)dynConstraints[72] , (int)dynConstraints[73] , (String)dynConstraints[74] , (String)dynConstraints[75] , (decimal)dynConstraints[76] , (decimal)dynConstraints[77] , (int)dynConstraints[78] , (String)dynConstraints[79] );
               case 1 :
                     return conditional_P00HS3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (int)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (bool)dynConstraints[17] , (String)dynConstraints[18] , (short)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (decimal)dynConstraints[37] , (decimal)dynConstraints[38] , (decimal)dynConstraints[39] , (decimal)dynConstraints[40] , (decimal)dynConstraints[41] , (decimal)dynConstraints[42] , (int)dynConstraints[43] , (int)dynConstraints[44] , (int)dynConstraints[45] , (int)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (int)dynConstraints[49] , (String)dynConstraints[50] , (String)dynConstraints[51] , (String)dynConstraints[52] , (String)dynConstraints[53] , (decimal)dynConstraints[54] , (decimal)dynConstraints[55] , (decimal)dynConstraints[56] , (decimal)dynConstraints[57] , (int)dynConstraints[58] , (int)dynConstraints[59] , (String)dynConstraints[60] , (String)dynConstraints[61] , (int)dynConstraints[62] , (String)dynConstraints[63] , (String)dynConstraints[64] , (int)dynConstraints[65] , (int)dynConstraints[66] , (DateTime)dynConstraints[67] , (String)dynConstraints[68] , (decimal)dynConstraints[69] , (decimal)dynConstraints[70] , (decimal)dynConstraints[71] , (int)dynConstraints[72] , (int)dynConstraints[73] , (String)dynConstraints[74] , (String)dynConstraints[75] , (decimal)dynConstraints[76] , (decimal)dynConstraints[77] , (int)dynConstraints[78] , (String)dynConstraints[79] );
               case 2 :
                     return conditional_P00HS4(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (int)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (bool)dynConstraints[17] , (String)dynConstraints[18] , (short)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (decimal)dynConstraints[37] , (decimal)dynConstraints[38] , (decimal)dynConstraints[39] , (decimal)dynConstraints[40] , (decimal)dynConstraints[41] , (decimal)dynConstraints[42] , (int)dynConstraints[43] , (int)dynConstraints[44] , (int)dynConstraints[45] , (int)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (int)dynConstraints[49] , (String)dynConstraints[50] , (String)dynConstraints[51] , (String)dynConstraints[52] , (String)dynConstraints[53] , (decimal)dynConstraints[54] , (decimal)dynConstraints[55] , (decimal)dynConstraints[56] , (decimal)dynConstraints[57] , (int)dynConstraints[58] , (int)dynConstraints[59] , (String)dynConstraints[60] , (String)dynConstraints[61] , (int)dynConstraints[62] , (String)dynConstraints[63] , (String)dynConstraints[64] , (int)dynConstraints[65] , (int)dynConstraints[66] , (DateTime)dynConstraints[67] , (String)dynConstraints[68] , (decimal)dynConstraints[69] , (decimal)dynConstraints[70] , (decimal)dynConstraints[71] , (int)dynConstraints[72] , (int)dynConstraints[73] , (String)dynConstraints[74] , (String)dynConstraints[75] , (decimal)dynConstraints[76] , (decimal)dynConstraints[77] , (int)dynConstraints[78] , (String)dynConstraints[79] );
               case 3 :
                     return conditional_P00HS5(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (int)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (bool)dynConstraints[17] , (String)dynConstraints[18] , (short)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (decimal)dynConstraints[37] , (decimal)dynConstraints[38] , (decimal)dynConstraints[39] , (decimal)dynConstraints[40] , (decimal)dynConstraints[41] , (decimal)dynConstraints[42] , (int)dynConstraints[43] , (int)dynConstraints[44] , (int)dynConstraints[45] , (int)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (int)dynConstraints[49] , (String)dynConstraints[50] , (String)dynConstraints[51] , (String)dynConstraints[52] , (String)dynConstraints[53] , (decimal)dynConstraints[54] , (decimal)dynConstraints[55] , (decimal)dynConstraints[56] , (decimal)dynConstraints[57] , (int)dynConstraints[58] , (int)dynConstraints[59] , (String)dynConstraints[60] , (String)dynConstraints[61] , (int)dynConstraints[62] , (String)dynConstraints[63] , (String)dynConstraints[64] , (int)dynConstraints[65] , (int)dynConstraints[66] , (DateTime)dynConstraints[67] , (String)dynConstraints[68] , (decimal)dynConstraints[69] , (decimal)dynConstraints[70] , (decimal)dynConstraints[71] , (int)dynConstraints[72] , (int)dynConstraints[73] , (String)dynConstraints[74] , (String)dynConstraints[75] , (decimal)dynConstraints[76] , (decimal)dynConstraints[77] , (int)dynConstraints[78] , (String)dynConstraints[79] );
               case 4 :
                     return conditional_P00HS6(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (int)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (bool)dynConstraints[17] , (String)dynConstraints[18] , (short)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (decimal)dynConstraints[37] , (decimal)dynConstraints[38] , (decimal)dynConstraints[39] , (decimal)dynConstraints[40] , (decimal)dynConstraints[41] , (decimal)dynConstraints[42] , (int)dynConstraints[43] , (int)dynConstraints[44] , (int)dynConstraints[45] , (int)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (int)dynConstraints[49] , (String)dynConstraints[50] , (String)dynConstraints[51] , (String)dynConstraints[52] , (String)dynConstraints[53] , (decimal)dynConstraints[54] , (decimal)dynConstraints[55] , (decimal)dynConstraints[56] , (decimal)dynConstraints[57] , (int)dynConstraints[58] , (int)dynConstraints[59] , (String)dynConstraints[60] , (String)dynConstraints[61] , (int)dynConstraints[62] , (String)dynConstraints[63] , (String)dynConstraints[64] , (int)dynConstraints[65] , (int)dynConstraints[66] , (DateTime)dynConstraints[67] , (String)dynConstraints[68] , (decimal)dynConstraints[69] , (decimal)dynConstraints[70] , (decimal)dynConstraints[71] , (int)dynConstraints[72] , (int)dynConstraints[73] , (String)dynConstraints[74] , (String)dynConstraints[75] , (decimal)dynConstraints[76] , (decimal)dynConstraints[77] , (int)dynConstraints[78] , (String)dynConstraints[79] );
               case 5 :
                     return conditional_P00HS7(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (int)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (bool)dynConstraints[17] , (String)dynConstraints[18] , (short)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (int)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (decimal)dynConstraints[37] , (decimal)dynConstraints[38] , (decimal)dynConstraints[39] , (decimal)dynConstraints[40] , (decimal)dynConstraints[41] , (decimal)dynConstraints[42] , (int)dynConstraints[43] , (int)dynConstraints[44] , (int)dynConstraints[45] , (int)dynConstraints[46] , (String)dynConstraints[47] , (String)dynConstraints[48] , (int)dynConstraints[49] , (String)dynConstraints[50] , (String)dynConstraints[51] , (String)dynConstraints[52] , (String)dynConstraints[53] , (decimal)dynConstraints[54] , (decimal)dynConstraints[55] , (decimal)dynConstraints[56] , (decimal)dynConstraints[57] , (int)dynConstraints[58] , (int)dynConstraints[59] , (String)dynConstraints[60] , (String)dynConstraints[61] , (int)dynConstraints[62] , (String)dynConstraints[63] , (String)dynConstraints[64] , (int)dynConstraints[65] , (int)dynConstraints[66] , (DateTime)dynConstraints[67] , (String)dynConstraints[68] , (decimal)dynConstraints[69] , (decimal)dynConstraints[70] , (decimal)dynConstraints[71] , (int)dynConstraints[72] , (int)dynConstraints[73] , (String)dynConstraints[74] , (String)dynConstraints[75] , (decimal)dynConstraints[76] , (decimal)dynConstraints[77] , (int)dynConstraints[78] , (String)dynConstraints[79] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00HS2 ;
          prmP00HS2 = new Object[] {
          new Object[] {"@AV116WWContagemDS_3_Contagem_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV116WWContagemDS_3_Contagem_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV116WWContagemDS_3_Contagem_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV117WWContagemDS_4_Contagem_areatrabalhodes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV117WWContagemDS_4_Contagem_areatrabalhodes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV122WWContagemDS_9_Contagem_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV122WWContagemDS_9_Contagem_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV122WWContagemDS_9_Contagem_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV123WWContagemDS_10_Contagem_areatrabalhodes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV123WWContagemDS_10_Contagem_areatrabalhodes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV128WWContagemDS_15_Contagem_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV128WWContagemDS_15_Contagem_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV128WWContagemDS_15_Contagem_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV129WWContagemDS_16_Contagem_areatrabalhodes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV129WWContagemDS_16_Contagem_areatrabalhodes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV131WWContagemDS_18_Tfcontagem_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV132WWContagemDS_19_Tfcontagem_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV133WWContagemDS_20_Tfcontagem_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV135WWContagemDS_22_Tfcontagem_contratadacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV136WWContagemDS_23_Tfcontagem_contratadacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV137WWContagemDS_24_Tfcontagem_datacriacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV138WWContagemDS_25_Tfcontagem_datacriacao_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV139WWContagemDS_26_Tfcontagem_areatrabalhodes",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV143WWContagemDS_30_Tfcontagem_notas",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV144WWContagemDS_31_Tfcontagem_notas_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV145WWContagemDS_32_Tfcontagem_pfb",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV146WWContagemDS_33_Tfcontagem_pfb_to",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV147WWContagemDS_34_Tfcontagem_pfl",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV148WWContagemDS_35_Tfcontagem_pfl_to",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV149WWContagemDS_36_Tfcontagem_divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV150WWContagemDS_37_Tfcontagem_divergencia_to",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV158WWContagemDS_45_Tfcontagem_demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV159WWContagemDS_46_Tfcontagem_demanda_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV160WWContagemDS_47_Tfcontagem_link",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV161WWContagemDS_48_Tfcontagem_link_sel",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV162WWContagemDS_49_Tfcontagem_fator",SqlDbType.Decimal,4,2} ,
          new Object[] {"@AV163WWContagemDS_50_Tfcontagem_fator_to",SqlDbType.Decimal,4,2} ,
          new Object[] {"@AV164WWContagemDS_51_Tfcontagem_deflator",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV165WWContagemDS_52_Tfcontagem_deflator_to",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV166WWContagemDS_53_Tfcontagem_sistemacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV167WWContagemDS_54_Tfcontagem_sistemacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV168WWContagemDS_55_Tfcontagem_sistemasigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel",SqlDbType.Char,25,0}
          } ;
          Object[] prmP00HS3 ;
          prmP00HS3 = new Object[] {
          new Object[] {"@AV116WWContagemDS_3_Contagem_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV116WWContagemDS_3_Contagem_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV116WWContagemDS_3_Contagem_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV117WWContagemDS_4_Contagem_areatrabalhodes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV117WWContagemDS_4_Contagem_areatrabalhodes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV122WWContagemDS_9_Contagem_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV122WWContagemDS_9_Contagem_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV122WWContagemDS_9_Contagem_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV123WWContagemDS_10_Contagem_areatrabalhodes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV123WWContagemDS_10_Contagem_areatrabalhodes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV128WWContagemDS_15_Contagem_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV128WWContagemDS_15_Contagem_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV128WWContagemDS_15_Contagem_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV129WWContagemDS_16_Contagem_areatrabalhodes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV129WWContagemDS_16_Contagem_areatrabalhodes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV131WWContagemDS_18_Tfcontagem_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV132WWContagemDS_19_Tfcontagem_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV133WWContagemDS_20_Tfcontagem_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV135WWContagemDS_22_Tfcontagem_contratadacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV136WWContagemDS_23_Tfcontagem_contratadacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV137WWContagemDS_24_Tfcontagem_datacriacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV138WWContagemDS_25_Tfcontagem_datacriacao_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV139WWContagemDS_26_Tfcontagem_areatrabalhodes",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV143WWContagemDS_30_Tfcontagem_notas",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV144WWContagemDS_31_Tfcontagem_notas_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV145WWContagemDS_32_Tfcontagem_pfb",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV146WWContagemDS_33_Tfcontagem_pfb_to",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV147WWContagemDS_34_Tfcontagem_pfl",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV148WWContagemDS_35_Tfcontagem_pfl_to",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV149WWContagemDS_36_Tfcontagem_divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV150WWContagemDS_37_Tfcontagem_divergencia_to",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV158WWContagemDS_45_Tfcontagem_demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV159WWContagemDS_46_Tfcontagem_demanda_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV160WWContagemDS_47_Tfcontagem_link",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV161WWContagemDS_48_Tfcontagem_link_sel",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV162WWContagemDS_49_Tfcontagem_fator",SqlDbType.Decimal,4,2} ,
          new Object[] {"@AV163WWContagemDS_50_Tfcontagem_fator_to",SqlDbType.Decimal,4,2} ,
          new Object[] {"@AV164WWContagemDS_51_Tfcontagem_deflator",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV165WWContagemDS_52_Tfcontagem_deflator_to",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV166WWContagemDS_53_Tfcontagem_sistemacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV167WWContagemDS_54_Tfcontagem_sistemacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV168WWContagemDS_55_Tfcontagem_sistemasigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel",SqlDbType.Char,25,0}
          } ;
          Object[] prmP00HS4 ;
          prmP00HS4 = new Object[] {
          new Object[] {"@AV116WWContagemDS_3_Contagem_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV116WWContagemDS_3_Contagem_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV116WWContagemDS_3_Contagem_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV117WWContagemDS_4_Contagem_areatrabalhodes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV117WWContagemDS_4_Contagem_areatrabalhodes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV122WWContagemDS_9_Contagem_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV122WWContagemDS_9_Contagem_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV122WWContagemDS_9_Contagem_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV123WWContagemDS_10_Contagem_areatrabalhodes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV123WWContagemDS_10_Contagem_areatrabalhodes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV128WWContagemDS_15_Contagem_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV128WWContagemDS_15_Contagem_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV128WWContagemDS_15_Contagem_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV129WWContagemDS_16_Contagem_areatrabalhodes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV129WWContagemDS_16_Contagem_areatrabalhodes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV131WWContagemDS_18_Tfcontagem_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV132WWContagemDS_19_Tfcontagem_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV133WWContagemDS_20_Tfcontagem_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV135WWContagemDS_22_Tfcontagem_contratadacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV136WWContagemDS_23_Tfcontagem_contratadacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV137WWContagemDS_24_Tfcontagem_datacriacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV138WWContagemDS_25_Tfcontagem_datacriacao_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV139WWContagemDS_26_Tfcontagem_areatrabalhodes",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV143WWContagemDS_30_Tfcontagem_notas",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV144WWContagemDS_31_Tfcontagem_notas_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV145WWContagemDS_32_Tfcontagem_pfb",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV146WWContagemDS_33_Tfcontagem_pfb_to",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV147WWContagemDS_34_Tfcontagem_pfl",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV148WWContagemDS_35_Tfcontagem_pfl_to",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV149WWContagemDS_36_Tfcontagem_divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV150WWContagemDS_37_Tfcontagem_divergencia_to",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV158WWContagemDS_45_Tfcontagem_demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV159WWContagemDS_46_Tfcontagem_demanda_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV160WWContagemDS_47_Tfcontagem_link",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV161WWContagemDS_48_Tfcontagem_link_sel",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV162WWContagemDS_49_Tfcontagem_fator",SqlDbType.Decimal,4,2} ,
          new Object[] {"@AV163WWContagemDS_50_Tfcontagem_fator_to",SqlDbType.Decimal,4,2} ,
          new Object[] {"@AV164WWContagemDS_51_Tfcontagem_deflator",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV165WWContagemDS_52_Tfcontagem_deflator_to",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV166WWContagemDS_53_Tfcontagem_sistemacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV167WWContagemDS_54_Tfcontagem_sistemacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV168WWContagemDS_55_Tfcontagem_sistemasigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel",SqlDbType.Char,25,0}
          } ;
          Object[] prmP00HS5 ;
          prmP00HS5 = new Object[] {
          new Object[] {"@AV116WWContagemDS_3_Contagem_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV116WWContagemDS_3_Contagem_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV116WWContagemDS_3_Contagem_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV117WWContagemDS_4_Contagem_areatrabalhodes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV117WWContagemDS_4_Contagem_areatrabalhodes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV122WWContagemDS_9_Contagem_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV122WWContagemDS_9_Contagem_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV122WWContagemDS_9_Contagem_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV123WWContagemDS_10_Contagem_areatrabalhodes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV123WWContagemDS_10_Contagem_areatrabalhodes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV128WWContagemDS_15_Contagem_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV128WWContagemDS_15_Contagem_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV128WWContagemDS_15_Contagem_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV129WWContagemDS_16_Contagem_areatrabalhodes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV129WWContagemDS_16_Contagem_areatrabalhodes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV131WWContagemDS_18_Tfcontagem_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV132WWContagemDS_19_Tfcontagem_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV133WWContagemDS_20_Tfcontagem_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV135WWContagemDS_22_Tfcontagem_contratadacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV136WWContagemDS_23_Tfcontagem_contratadacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV137WWContagemDS_24_Tfcontagem_datacriacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV138WWContagemDS_25_Tfcontagem_datacriacao_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV139WWContagemDS_26_Tfcontagem_areatrabalhodes",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV143WWContagemDS_30_Tfcontagem_notas",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV144WWContagemDS_31_Tfcontagem_notas_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV145WWContagemDS_32_Tfcontagem_pfb",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV146WWContagemDS_33_Tfcontagem_pfb_to",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV147WWContagemDS_34_Tfcontagem_pfl",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV148WWContagemDS_35_Tfcontagem_pfl_to",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV149WWContagemDS_36_Tfcontagem_divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV150WWContagemDS_37_Tfcontagem_divergencia_to",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV158WWContagemDS_45_Tfcontagem_demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV159WWContagemDS_46_Tfcontagem_demanda_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV160WWContagemDS_47_Tfcontagem_link",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV161WWContagemDS_48_Tfcontagem_link_sel",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV162WWContagemDS_49_Tfcontagem_fator",SqlDbType.Decimal,4,2} ,
          new Object[] {"@AV163WWContagemDS_50_Tfcontagem_fator_to",SqlDbType.Decimal,4,2} ,
          new Object[] {"@AV164WWContagemDS_51_Tfcontagem_deflator",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV165WWContagemDS_52_Tfcontagem_deflator_to",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV166WWContagemDS_53_Tfcontagem_sistemacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV167WWContagemDS_54_Tfcontagem_sistemacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV168WWContagemDS_55_Tfcontagem_sistemasigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel",SqlDbType.Char,25,0}
          } ;
          Object[] prmP00HS6 ;
          prmP00HS6 = new Object[] {
          new Object[] {"@AV116WWContagemDS_3_Contagem_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV116WWContagemDS_3_Contagem_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV116WWContagemDS_3_Contagem_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV117WWContagemDS_4_Contagem_areatrabalhodes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV117WWContagemDS_4_Contagem_areatrabalhodes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV122WWContagemDS_9_Contagem_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV122WWContagemDS_9_Contagem_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV122WWContagemDS_9_Contagem_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV123WWContagemDS_10_Contagem_areatrabalhodes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV123WWContagemDS_10_Contagem_areatrabalhodes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV128WWContagemDS_15_Contagem_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV128WWContagemDS_15_Contagem_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV128WWContagemDS_15_Contagem_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV129WWContagemDS_16_Contagem_areatrabalhodes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV129WWContagemDS_16_Contagem_areatrabalhodes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV131WWContagemDS_18_Tfcontagem_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV132WWContagemDS_19_Tfcontagem_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV133WWContagemDS_20_Tfcontagem_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV135WWContagemDS_22_Tfcontagem_contratadacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV136WWContagemDS_23_Tfcontagem_contratadacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV137WWContagemDS_24_Tfcontagem_datacriacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV138WWContagemDS_25_Tfcontagem_datacriacao_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV139WWContagemDS_26_Tfcontagem_areatrabalhodes",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV143WWContagemDS_30_Tfcontagem_notas",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV144WWContagemDS_31_Tfcontagem_notas_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV145WWContagemDS_32_Tfcontagem_pfb",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV146WWContagemDS_33_Tfcontagem_pfb_to",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV147WWContagemDS_34_Tfcontagem_pfl",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV148WWContagemDS_35_Tfcontagem_pfl_to",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV149WWContagemDS_36_Tfcontagem_divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV150WWContagemDS_37_Tfcontagem_divergencia_to",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV158WWContagemDS_45_Tfcontagem_demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV159WWContagemDS_46_Tfcontagem_demanda_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV160WWContagemDS_47_Tfcontagem_link",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV161WWContagemDS_48_Tfcontagem_link_sel",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV162WWContagemDS_49_Tfcontagem_fator",SqlDbType.Decimal,4,2} ,
          new Object[] {"@AV163WWContagemDS_50_Tfcontagem_fator_to",SqlDbType.Decimal,4,2} ,
          new Object[] {"@AV164WWContagemDS_51_Tfcontagem_deflator",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV165WWContagemDS_52_Tfcontagem_deflator_to",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV166WWContagemDS_53_Tfcontagem_sistemacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV167WWContagemDS_54_Tfcontagem_sistemacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV168WWContagemDS_55_Tfcontagem_sistemasigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel",SqlDbType.Char,25,0}
          } ;
          Object[] prmP00HS7 ;
          prmP00HS7 = new Object[] {
          new Object[] {"@AV116WWContagemDS_3_Contagem_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV116WWContagemDS_3_Contagem_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV116WWContagemDS_3_Contagem_contratadacod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV117WWContagemDS_4_Contagem_areatrabalhodes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV117WWContagemDS_4_Contagem_areatrabalhodes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV118WWContagemDS_5_Contagem_usuariocontadorpessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV122WWContagemDS_9_Contagem_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV122WWContagemDS_9_Contagem_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV122WWContagemDS_9_Contagem_contratadacod2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV123WWContagemDS_10_Contagem_areatrabalhodes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV123WWContagemDS_10_Contagem_areatrabalhodes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV124WWContagemDS_11_Contagem_usuariocontadorpessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV128WWContagemDS_15_Contagem_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV128WWContagemDS_15_Contagem_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV128WWContagemDS_15_Contagem_contratadacod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV129WWContagemDS_16_Contagem_areatrabalhodes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV129WWContagemDS_16_Contagem_areatrabalhodes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV130WWContagemDS_17_Contagem_usuariocontadorpessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV131WWContagemDS_18_Tfcontagem_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV132WWContagemDS_19_Tfcontagem_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV133WWContagemDS_20_Tfcontagem_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV134WWContagemDS_21_Tfcontagem_areatrabalhocod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV135WWContagemDS_22_Tfcontagem_contratadacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV136WWContagemDS_23_Tfcontagem_contratadacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV137WWContagemDS_24_Tfcontagem_datacriacao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV138WWContagemDS_25_Tfcontagem_datacriacao_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV139WWContagemDS_26_Tfcontagem_areatrabalhodes",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV140WWContagemDS_27_Tfcontagem_areatrabalhodes_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV143WWContagemDS_30_Tfcontagem_notas",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV144WWContagemDS_31_Tfcontagem_notas_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV145WWContagemDS_32_Tfcontagem_pfb",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV146WWContagemDS_33_Tfcontagem_pfb_to",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV147WWContagemDS_34_Tfcontagem_pfl",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV148WWContagemDS_35_Tfcontagem_pfl_to",SqlDbType.Decimal,13,5} ,
          new Object[] {"@AV149WWContagemDS_36_Tfcontagem_divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV150WWContagemDS_37_Tfcontagem_divergencia_to",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV151WWContagemDS_38_Tfcontagem_usuariocontadorcod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV152WWContagemDS_39_Tfcontagem_usuariocontadorcod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV153WWContagemDS_40_Tfcontagem_usuariocontadorpessoacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV154WWContagemDS_41_Tfcontagem_usuariocontadorpessoacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV155WWContagemDS_42_Tfcontagem_usuariocontadorpessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV156WWContagemDS_43_Tfcontagem_usuariocontadorpessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV158WWContagemDS_45_Tfcontagem_demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV159WWContagemDS_46_Tfcontagem_demanda_sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@lV160WWContagemDS_47_Tfcontagem_link",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV161WWContagemDS_48_Tfcontagem_link_sel",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV162WWContagemDS_49_Tfcontagem_fator",SqlDbType.Decimal,4,2} ,
          new Object[] {"@AV163WWContagemDS_50_Tfcontagem_fator_to",SqlDbType.Decimal,4,2} ,
          new Object[] {"@AV164WWContagemDS_51_Tfcontagem_deflator",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV165WWContagemDS_52_Tfcontagem_deflator_to",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV166WWContagemDS_53_Tfcontagem_sistemacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV167WWContagemDS_54_Tfcontagem_sistemacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV168WWContagemDS_55_Tfcontagem_sistemasigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV169WWContagemDS_56_Tfcontagem_sistemasigla_sel",SqlDbType.Char,25,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00HS2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00HS2,100,0,true,false )
             ,new CursorDef("P00HS3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00HS3,100,0,true,false )
             ,new CursorDef("P00HS4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00HS4,100,0,true,false )
             ,new CursorDef("P00HS5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00HS5,100,0,true,false )
             ,new CursorDef("P00HS6", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00HS6,100,0,true,false )
             ,new CursorDef("P00HS7", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00HS7,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 25) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((decimal[]) buf[19])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((decimal[]) buf[21])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((decimal[]) buf[23])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((String[]) buf[25])[0] = rslt.getLongVarchar(14) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(14);
                ((String[]) buf[27])[0] = rslt.getString(15, 1) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(15);
                ((String[]) buf[29])[0] = rslt.getString(16, 1) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(16);
                ((DateTime[]) buf[31])[0] = rslt.getGXDate(17) ;
                ((int[]) buf[32])[0] = rslt.getInt(18) ;
                ((String[]) buf[33])[0] = rslt.getString(19, 100) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(19);
                ((String[]) buf[35])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(20);
                ((int[]) buf[37])[0] = rslt.getInt(21) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(21);
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 25) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((String[]) buf[14])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((int[]) buf[18])[0] = rslt.getInt(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((decimal[]) buf[20])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((decimal[]) buf[22])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((decimal[]) buf[24])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(13);
                ((String[]) buf[26])[0] = rslt.getString(14, 1) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(14);
                ((String[]) buf[28])[0] = rslt.getString(15, 1) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[30])[0] = rslt.getGXDate(16) ;
                ((int[]) buf[31])[0] = rslt.getInt(17) ;
                ((int[]) buf[32])[0] = rslt.getInt(18) ;
                ((String[]) buf[33])[0] = rslt.getString(19, 100) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(19);
                ((String[]) buf[35])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(20);
                ((int[]) buf[37])[0] = rslt.getInt(21) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(21);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 25) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((String[]) buf[14])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((int[]) buf[18])[0] = rslt.getInt(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((decimal[]) buf[20])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((decimal[]) buf[22])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((decimal[]) buf[24])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(13);
                ((String[]) buf[26])[0] = rslt.getLongVarchar(14) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(14);
                ((String[]) buf[28])[0] = rslt.getString(15, 1) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(15);
                ((String[]) buf[30])[0] = rslt.getString(16, 1) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(16);
                ((DateTime[]) buf[32])[0] = rslt.getGXDate(17) ;
                ((int[]) buf[33])[0] = rslt.getInt(18) ;
                ((int[]) buf[34])[0] = rslt.getInt(19) ;
                ((String[]) buf[35])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(20);
                ((int[]) buf[37])[0] = rslt.getInt(21) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(21);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 25) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((decimal[]) buf[20])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((decimal[]) buf[22])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((String[]) buf[24])[0] = rslt.getLongVarchar(13) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(13);
                ((String[]) buf[26])[0] = rslt.getString(14, 1) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(14);
                ((String[]) buf[28])[0] = rslt.getString(15, 1) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[30])[0] = rslt.getGXDate(16) ;
                ((int[]) buf[31])[0] = rslt.getInt(17) ;
                ((int[]) buf[32])[0] = rslt.getInt(18) ;
                ((String[]) buf[33])[0] = rslt.getString(19, 100) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(19);
                ((String[]) buf[35])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(20);
                ((int[]) buf[37])[0] = rslt.getInt(21) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(21);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 25) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((decimal[]) buf[20])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((decimal[]) buf[22])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((String[]) buf[24])[0] = rslt.getLongVarchar(13) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(13);
                ((String[]) buf[26])[0] = rslt.getString(14, 1) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(14);
                ((String[]) buf[28])[0] = rslt.getString(15, 1) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[30])[0] = rslt.getGXDate(16) ;
                ((int[]) buf[31])[0] = rslt.getInt(17) ;
                ((int[]) buf[32])[0] = rslt.getInt(18) ;
                ((String[]) buf[33])[0] = rslt.getString(19, 100) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(19);
                ((String[]) buf[35])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(20);
                ((int[]) buf[37])[0] = rslt.getInt(21) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(21);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 25) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((String[]) buf[8])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((decimal[]) buf[20])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((decimal[]) buf[22])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(12);
                ((String[]) buf[24])[0] = rslt.getLongVarchar(13) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(13);
                ((String[]) buf[26])[0] = rslt.getString(14, 1) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(14);
                ((String[]) buf[28])[0] = rslt.getString(15, 1) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[30])[0] = rslt.getGXDate(16) ;
                ((int[]) buf[31])[0] = rslt.getInt(17) ;
                ((int[]) buf[32])[0] = rslt.getInt(18) ;
                ((String[]) buf[33])[0] = rslt.getString(19, 100) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(19);
                ((String[]) buf[35])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(20);
                ((int[]) buf[37])[0] = rslt.getInt(21) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(21);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[59]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[64]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[65]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[66]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[67]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[70]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[71]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[72]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[73]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[74]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[75]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[78]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[79]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[80]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[81]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[82]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[83]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[84]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[85]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[86]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[87]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[88]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[89]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[90]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[91]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[92]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[93]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[94]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[95]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[96]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[97]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[98]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[99]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[100]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[101]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[102]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[103]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[104]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[105]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[106]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[107]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[108]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[109]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[110]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[111]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[112]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[113]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[59]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[64]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[65]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[66]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[67]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[70]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[71]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[72]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[73]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[74]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[75]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[78]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[79]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[80]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[81]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[82]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[83]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[84]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[85]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[86]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[87]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[88]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[89]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[90]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[91]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[92]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[93]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[94]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[95]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[96]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[97]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[98]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[99]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[100]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[101]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[102]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[103]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[104]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[105]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[106]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[107]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[108]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[109]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[110]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[111]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[112]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[113]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[59]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[64]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[65]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[66]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[67]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[70]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[71]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[72]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[73]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[74]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[75]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[78]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[79]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[80]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[81]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[82]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[83]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[84]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[85]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[86]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[87]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[88]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[89]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[90]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[91]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[92]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[93]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[94]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[95]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[96]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[97]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[98]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[99]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[100]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[101]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[102]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[103]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[104]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[105]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[106]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[107]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[108]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[109]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[110]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[111]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[112]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[113]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[59]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[64]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[65]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[66]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[67]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[70]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[71]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[72]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[73]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[74]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[75]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[78]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[79]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[80]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[81]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[82]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[83]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[84]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[85]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[86]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[87]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[88]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[89]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[90]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[91]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[92]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[93]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[94]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[95]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[96]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[97]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[98]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[99]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[100]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[101]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[102]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[103]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[104]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[105]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[106]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[107]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[108]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[109]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[110]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[111]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[112]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[113]);
                }
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[59]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[64]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[65]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[66]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[67]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[70]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[71]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[72]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[73]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[74]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[75]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[78]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[79]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[80]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[81]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[82]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[83]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[84]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[85]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[86]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[87]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[88]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[89]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[90]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[91]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[92]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[93]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[94]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[95]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[96]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[97]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[98]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[99]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[100]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[101]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[102]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[103]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[104]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[105]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[106]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[107]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[108]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[109]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[110]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[111]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[112]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[113]);
                }
                return;
             case 5 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[59]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[64]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[65]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[66]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[67]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[70]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[71]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[72]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[73]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[74]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[75]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[78]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[79]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[80]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[81]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[82]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[83]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[84]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[85]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[86]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[87]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[88]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[89]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[90]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[91]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[92]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[93]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[94]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[95]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[96]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[97]);
                }
                if ( (short)parms[41] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[98]);
                }
                if ( (short)parms[42] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[99]);
                }
                if ( (short)parms[43] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[100]);
                }
                if ( (short)parms[44] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[101]);
                }
                if ( (short)parms[45] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[102]);
                }
                if ( (short)parms[46] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[103]);
                }
                if ( (short)parms[47] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[104]);
                }
                if ( (short)parms[48] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[105]);
                }
                if ( (short)parms[49] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[106]);
                }
                if ( (short)parms[50] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[107]);
                }
                if ( (short)parms[51] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[108]);
                }
                if ( (short)parms[52] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[109]);
                }
                if ( (short)parms[53] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[110]);
                }
                if ( (short)parms[54] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[111]);
                }
                if ( (short)parms[55] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[112]);
                }
                if ( (short)parms[56] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[113]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwcontagemfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwcontagemfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwcontagemfilterdata") )
          {
             return  ;
          }
          getwwcontagemfilterdata worker = new getwwcontagemfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
