/*
               File: ProjetoGeneral
        Description: Projeto General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/4/2020 8:39:58.93
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class projetogeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public projetogeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public projetogeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Projeto_Codigo )
      {
         this.A648Projeto_Codigo = aP0_Projeto_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         chkProjeto_Incremental = new GXCheckbox();
         cmbProjeto_Status = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A648Projeto_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A648Projeto_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PADH2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "ProjetoGeneral";
               context.Gx_err = 0;
               WSDH2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Projeto General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202054839592");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("projetogeneral.aspx") + "?" + UrlEncode("" +A648Projeto_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA648Projeto_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA648Projeto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"PROJETO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A648Projeto_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A650Projeto_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_IDENTIFICADOR", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2144Projeto_Identificador, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A649Projeto_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_TIPOPROJETOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A983Projeto_TipoProjetoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_OBJETIVO", GetSecureSignedToken( sPrefix, A2145Projeto_Objetivo));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_ESCOPO", GetSecureSignedToken( sPrefix, A653Projeto_Escopo));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_DTFIM", GetSecureSignedToken( sPrefix, A2147Projeto_DTFim));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_DTINICIO", GetSecureSignedToken( sPrefix, A2146Projeto_DTInicio));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_PRAZOPREVISTO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2148Projeto_PrazoPrevisto), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_ESFORCOPREVISTO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2149Projeto_EsforcoPrevisto), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_CUSTOPREVISTO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2150Projeto_CustoPrevisto, "ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_FATORESCALA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A985Projeto_FatorEscala, "ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_FATORMULTIPLICADOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A989Projeto_FatorMultiplicador, "ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_CONSTACOCOMO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A990Projeto_ConstACocomo, "ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_INCREMENTAL", GetSecureSignedToken( sPrefix, A1232Projeto_Incremental));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PROJETO_STATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A658Projeto_Status, ""))));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormDH2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("projetogeneral.js", "?202054839597");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ProjetoGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Projeto General" ;
      }

      protected void WBDH0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "projetogeneral.aspx");
            }
            wb_table1_2_DH2( true) ;
         }
         else
         {
            wb_table1_2_DH2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_DH2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTDH2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Projeto General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPDH0( ) ;
            }
         }
      }

      protected void WSDH2( )
      {
         STARTDH2( ) ;
         EVTDH2( ) ;
      }

      protected void EVTDH2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11DH2 */
                                    E11DH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12DH2 */
                                    E12DH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13DH2 */
                                    E13DH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14DH2 */
                                    E14DH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15DH2 */
                                    E15DH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPDH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEDH2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormDH2( ) ;
            }
         }
      }

      protected void PADH2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            chkProjeto_Incremental.Name = "PROJETO_INCREMENTAL";
            chkProjeto_Incremental.WebTags = "";
            chkProjeto_Incremental.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkProjeto_Incremental_Internalname, "TitleCaption", chkProjeto_Incremental.Caption);
            chkProjeto_Incremental.CheckedValue = "false";
            cmbProjeto_Status.Name = "PROJETO_STATUS";
            cmbProjeto_Status.WebTags = "";
            cmbProjeto_Status.addItem("A", "Aberto", 0);
            cmbProjeto_Status.addItem("E", "Em Contagem", 0);
            cmbProjeto_Status.addItem("C", "Contado", 0);
            if ( cmbProjeto_Status.ItemCount > 0 )
            {
               A658Projeto_Status = cmbProjeto_Status.getValidValue(A658Projeto_Status);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A658Projeto_Status", A658Projeto_Status);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_STATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A658Projeto_Status, ""))));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbProjeto_Status.ItemCount > 0 )
         {
            A658Projeto_Status = cmbProjeto_Status.getValidValue(A658Projeto_Status);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A658Projeto_Status", A658Projeto_Status);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_STATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A658Projeto_Status, ""))));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFDH2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "ProjetoGeneral";
         context.Gx_err = 0;
      }

      protected void RFDH2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00DH2 */
            pr_default.execute(0, new Object[] {A648Projeto_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A658Projeto_Status = H00DH2_A658Projeto_Status[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A658Projeto_Status", A658Projeto_Status);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_STATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A658Projeto_Status, ""))));
               A1232Projeto_Incremental = H00DH2_A1232Projeto_Incremental[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1232Projeto_Incremental", A1232Projeto_Incremental);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_INCREMENTAL", GetSecureSignedToken( sPrefix, A1232Projeto_Incremental));
               n1232Projeto_Incremental = H00DH2_n1232Projeto_Incremental[0];
               A990Projeto_ConstACocomo = H00DH2_A990Projeto_ConstACocomo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A990Projeto_ConstACocomo", StringUtil.LTrim( StringUtil.Str( A990Projeto_ConstACocomo, 6, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_CONSTACOCOMO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A990Projeto_ConstACocomo, "ZZ9.99")));
               n990Projeto_ConstACocomo = H00DH2_n990Projeto_ConstACocomo[0];
               A989Projeto_FatorMultiplicador = H00DH2_A989Projeto_FatorMultiplicador[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A989Projeto_FatorMultiplicador", StringUtil.LTrim( StringUtil.Str( A989Projeto_FatorMultiplicador, 6, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_FATORMULTIPLICADOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A989Projeto_FatorMultiplicador, "ZZ9.99")));
               n989Projeto_FatorMultiplicador = H00DH2_n989Projeto_FatorMultiplicador[0];
               A985Projeto_FatorEscala = H00DH2_A985Projeto_FatorEscala[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A985Projeto_FatorEscala", StringUtil.LTrim( StringUtil.Str( A985Projeto_FatorEscala, 6, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_FATORESCALA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A985Projeto_FatorEscala, "ZZ9.99")));
               n985Projeto_FatorEscala = H00DH2_n985Projeto_FatorEscala[0];
               A2150Projeto_CustoPrevisto = H00DH2_A2150Projeto_CustoPrevisto[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2150Projeto_CustoPrevisto", StringUtil.LTrim( StringUtil.Str( A2150Projeto_CustoPrevisto, 12, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_CUSTOPREVISTO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2150Projeto_CustoPrevisto, "ZZZ,ZZZ,ZZ9.99")));
               n2150Projeto_CustoPrevisto = H00DH2_n2150Projeto_CustoPrevisto[0];
               A2149Projeto_EsforcoPrevisto = H00DH2_A2149Projeto_EsforcoPrevisto[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2149Projeto_EsforcoPrevisto", StringUtil.LTrim( StringUtil.Str( (decimal)(A2149Projeto_EsforcoPrevisto), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_ESFORCOPREVISTO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2149Projeto_EsforcoPrevisto), "ZZZ9")));
               n2149Projeto_EsforcoPrevisto = H00DH2_n2149Projeto_EsforcoPrevisto[0];
               A2148Projeto_PrazoPrevisto = H00DH2_A2148Projeto_PrazoPrevisto[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2148Projeto_PrazoPrevisto", StringUtil.LTrim( StringUtil.Str( (decimal)(A2148Projeto_PrazoPrevisto), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_PRAZOPREVISTO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2148Projeto_PrazoPrevisto), "ZZZ9")));
               n2148Projeto_PrazoPrevisto = H00DH2_n2148Projeto_PrazoPrevisto[0];
               A2146Projeto_DTInicio = H00DH2_A2146Projeto_DTInicio[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2146Projeto_DTInicio", context.localUtil.Format(A2146Projeto_DTInicio, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_DTINICIO", GetSecureSignedToken( sPrefix, A2146Projeto_DTInicio));
               n2146Projeto_DTInicio = H00DH2_n2146Projeto_DTInicio[0];
               A2147Projeto_DTFim = H00DH2_A2147Projeto_DTFim[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2147Projeto_DTFim", context.localUtil.Format(A2147Projeto_DTFim, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_DTFIM", GetSecureSignedToken( sPrefix, A2147Projeto_DTFim));
               n2147Projeto_DTFim = H00DH2_n2147Projeto_DTFim[0];
               A653Projeto_Escopo = H00DH2_A653Projeto_Escopo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A653Projeto_Escopo", A653Projeto_Escopo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_ESCOPO", GetSecureSignedToken( sPrefix, A653Projeto_Escopo));
               A2145Projeto_Objetivo = H00DH2_A2145Projeto_Objetivo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2145Projeto_Objetivo", A2145Projeto_Objetivo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_OBJETIVO", GetSecureSignedToken( sPrefix, A2145Projeto_Objetivo));
               n2145Projeto_Objetivo = H00DH2_n2145Projeto_Objetivo[0];
               A983Projeto_TipoProjetoCod = H00DH2_A983Projeto_TipoProjetoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A983Projeto_TipoProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A983Projeto_TipoProjetoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_TIPOPROJETOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A983Projeto_TipoProjetoCod), "ZZZZZ9")));
               n983Projeto_TipoProjetoCod = H00DH2_n983Projeto_TipoProjetoCod[0];
               A649Projeto_Nome = H00DH2_A649Projeto_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A649Projeto_Nome", A649Projeto_Nome);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A649Projeto_Nome, "@!"))));
               A2144Projeto_Identificador = H00DH2_A2144Projeto_Identificador[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2144Projeto_Identificador", A2144Projeto_Identificador);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_IDENTIFICADOR", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2144Projeto_Identificador, ""))));
               n2144Projeto_Identificador = H00DH2_n2144Projeto_Identificador[0];
               A650Projeto_Sigla = H00DH2_A650Projeto_Sigla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A650Projeto_Sigla", A650Projeto_Sigla);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A650Projeto_Sigla, "@!"))));
               /* Execute user event: E12DH2 */
               E12DH2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBDH0( ) ;
         }
      }

      protected void STRUPDH0( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "ProjetoGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11DH2 */
         E11DH2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A650Projeto_Sigla = StringUtil.Upper( cgiGet( edtProjeto_Sigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A650Projeto_Sigla", A650Projeto_Sigla);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A650Projeto_Sigla, "@!"))));
            A2144Projeto_Identificador = cgiGet( edtProjeto_Identificador_Internalname);
            n2144Projeto_Identificador = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2144Projeto_Identificador", A2144Projeto_Identificador);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_IDENTIFICADOR", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2144Projeto_Identificador, ""))));
            A649Projeto_Nome = StringUtil.Upper( cgiGet( edtProjeto_Nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A649Projeto_Nome", A649Projeto_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A649Projeto_Nome, "@!"))));
            A983Projeto_TipoProjetoCod = (int)(context.localUtil.CToN( cgiGet( edtProjeto_TipoProjetoCod_Internalname), ",", "."));
            n983Projeto_TipoProjetoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A983Projeto_TipoProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A983Projeto_TipoProjetoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_TIPOPROJETOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A983Projeto_TipoProjetoCod), "ZZZZZ9")));
            A2145Projeto_Objetivo = cgiGet( edtProjeto_Objetivo_Internalname);
            n2145Projeto_Objetivo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2145Projeto_Objetivo", A2145Projeto_Objetivo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_OBJETIVO", GetSecureSignedToken( sPrefix, A2145Projeto_Objetivo));
            A653Projeto_Escopo = cgiGet( edtProjeto_Escopo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A653Projeto_Escopo", A653Projeto_Escopo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_ESCOPO", GetSecureSignedToken( sPrefix, A653Projeto_Escopo));
            A2147Projeto_DTFim = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtProjeto_DTFim_Internalname), 0));
            n2147Projeto_DTFim = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2147Projeto_DTFim", context.localUtil.Format(A2147Projeto_DTFim, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_DTFIM", GetSecureSignedToken( sPrefix, A2147Projeto_DTFim));
            A2146Projeto_DTInicio = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtProjeto_DTInicio_Internalname), 0));
            n2146Projeto_DTInicio = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2146Projeto_DTInicio", context.localUtil.Format(A2146Projeto_DTInicio, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_DTINICIO", GetSecureSignedToken( sPrefix, A2146Projeto_DTInicio));
            A2148Projeto_PrazoPrevisto = (short)(context.localUtil.CToN( cgiGet( edtProjeto_PrazoPrevisto_Internalname), ",", "."));
            n2148Projeto_PrazoPrevisto = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2148Projeto_PrazoPrevisto", StringUtil.LTrim( StringUtil.Str( (decimal)(A2148Projeto_PrazoPrevisto), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_PRAZOPREVISTO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2148Projeto_PrazoPrevisto), "ZZZ9")));
            A2149Projeto_EsforcoPrevisto = (short)(context.localUtil.CToN( cgiGet( edtProjeto_EsforcoPrevisto_Internalname), ",", "."));
            n2149Projeto_EsforcoPrevisto = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2149Projeto_EsforcoPrevisto", StringUtil.LTrim( StringUtil.Str( (decimal)(A2149Projeto_EsforcoPrevisto), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_ESFORCOPREVISTO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A2149Projeto_EsforcoPrevisto), "ZZZ9")));
            A2150Projeto_CustoPrevisto = context.localUtil.CToN( cgiGet( edtProjeto_CustoPrevisto_Internalname), ",", ".");
            n2150Projeto_CustoPrevisto = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2150Projeto_CustoPrevisto", StringUtil.LTrim( StringUtil.Str( A2150Projeto_CustoPrevisto, 12, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_CUSTOPREVISTO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A2150Projeto_CustoPrevisto, "ZZZ,ZZZ,ZZ9.99")));
            A985Projeto_FatorEscala = context.localUtil.CToN( cgiGet( edtProjeto_FatorEscala_Internalname), ",", ".");
            n985Projeto_FatorEscala = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A985Projeto_FatorEscala", StringUtil.LTrim( StringUtil.Str( A985Projeto_FatorEscala, 6, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_FATORESCALA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A985Projeto_FatorEscala, "ZZ9.99")));
            A989Projeto_FatorMultiplicador = context.localUtil.CToN( cgiGet( edtProjeto_FatorMultiplicador_Internalname), ",", ".");
            n989Projeto_FatorMultiplicador = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A989Projeto_FatorMultiplicador", StringUtil.LTrim( StringUtil.Str( A989Projeto_FatorMultiplicador, 6, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_FATORMULTIPLICADOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A989Projeto_FatorMultiplicador, "ZZ9.99")));
            A990Projeto_ConstACocomo = context.localUtil.CToN( cgiGet( edtProjeto_ConstACocomo_Internalname), ",", ".");
            n990Projeto_ConstACocomo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A990Projeto_ConstACocomo", StringUtil.LTrim( StringUtil.Str( A990Projeto_ConstACocomo, 6, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_CONSTACOCOMO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A990Projeto_ConstACocomo, "ZZ9.99")));
            A1232Projeto_Incremental = StringUtil.StrToBool( cgiGet( chkProjeto_Incremental_Internalname));
            n1232Projeto_Incremental = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1232Projeto_Incremental", A1232Projeto_Incremental);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_INCREMENTAL", GetSecureSignedToken( sPrefix, A1232Projeto_Incremental));
            cmbProjeto_Status.CurrentValue = cgiGet( cmbProjeto_Status_Internalname);
            A658Projeto_Status = cgiGet( cmbProjeto_Status_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A658Projeto_Status", A658Projeto_Status);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PROJETO_STATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A658Projeto_Status, ""))));
            /* Read saved values. */
            wcpOA648Projeto_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA648Projeto_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11DH2 */
         E11DH2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11DH2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12DH2( )
      {
         /* Load Routine */
         if ( ! ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            bttBtnupdate_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Enabled), 5, 0)));
         }
         if ( ! ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            bttBtndelete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Enabled), 5, 0)));
         }
      }

      protected void E13DH2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("projeto.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A648Projeto_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14DH2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("projeto.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A648Projeto_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E15DH2( )
      {
         /* 'DoFechar' Routine */
         context.wjLoc = formatLink("wwprojeto.aspx") ;
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Projeto";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "Projeto_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Projeto_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_DH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_DH2( true) ;
         }
         else
         {
            wb_table2_8_DH2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_DH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_93_DH2( true) ;
         }
         else
         {
            wb_table3_93_DH2( false) ;
         }
         return  ;
      }

      protected void wb_table3_93_DH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_DH2e( true) ;
         }
         else
         {
            wb_table1_2_DH2e( false) ;
         }
      }

      protected void wb_table3_93_DH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, bttBtnupdate_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, bttBtndelete_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_93_DH2e( true) ;
         }
         else
         {
            wb_table3_93_DH2e( false) ;
         }
      }

      protected void wb_table2_8_DH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_sigla_Internalname, "Sigla", "", "", lblTextblockprojeto_sigla_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProjeto_Sigla_Internalname, StringUtil.RTrim( A650Projeto_Sigla), StringUtil.RTrim( context.localUtil.Format( A650Projeto_Sigla, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_Sigla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_identificador_Internalname, "Identificador", "", "", lblTextblockprojeto_identificador_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProjeto_Identificador_Internalname, A2144Projeto_Identificador, StringUtil.RTrim( context.localUtil.Format( A2144Projeto_Identificador, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_Identificador_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_nome_Internalname, "Nome", "", "", lblTextblockprojeto_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProjeto_Nome_Internalname, StringUtil.RTrim( A649Projeto_Nome), StringUtil.RTrim( context.localUtil.Format( A649Projeto_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 100, "%", 1, "row", 50, 0, 0, 0, 1, -1, 0, true, "Nome", "left", true, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_tipoprojetocod_Internalname, "Tipo de Projeto", "", "", lblTextblockprojeto_tipoprojetocod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProjeto_TipoProjetoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A983Projeto_TipoProjetoCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A983Projeto_TipoProjetoCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_TipoProjetoCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_objetivo_Internalname, "Objetivo", "", "", lblTextblockprojeto_objetivo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            ClassString = "ReadonlyAttributeCharLong";
            StyleString = "";
            ClassString = "ReadonlyAttributeCharLong";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtProjeto_Objetivo_Internalname, A2145Projeto_Objetivo, "", "", 0, 1, 0, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_escopo_Internalname, "Escopo", "", "", lblTextblockprojeto_escopo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            ClassString = "ReadonlyAttributeCharLong";
            StyleString = "";
            ClassString = "ReadonlyAttributeCharLong";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtProjeto_Escopo_Internalname, A653Projeto_Escopo, "", "", 0, 1, 0, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_dtfim_Internalname, "Data Prevista de T�rmino", "", "", lblTextblockprojeto_dtfim_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtProjeto_DTFim_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtProjeto_DTFim_Internalname, context.localUtil.Format(A2147Projeto_DTFim, "99/99/99"), context.localUtil.Format( A2147Projeto_DTFim, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_DTFim_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ProjetoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtProjeto_DTFim_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_dtinicio_Internalname, "Data Prevista de In�cio", "", "", lblTextblockprojeto_dtinicio_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtProjeto_DTInicio_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtProjeto_DTInicio_Internalname, context.localUtil.Format(A2146Projeto_DTInicio, "99/99/99"), context.localUtil.Format( A2146Projeto_DTInicio, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_DTInicio_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ProjetoGeneral.htm");
            GxWebStd.gx_bitmap( context, edtProjeto_DTInicio_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_prazoprevisto_Internalname, "Previs�o de Prazo", "", "", lblTextblockprojeto_prazoprevisto_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table4_52_DH2( true) ;
         }
         else
         {
            wb_table4_52_DH2( false) ;
         }
         return  ;
      }

      protected void wb_table4_52_DH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_custoprevisto_Internalname, "Previs�o de Custo", "", "", lblTextblockprojeto_custoprevisto_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProjeto_CustoPrevisto_Internalname, StringUtil.LTrim( StringUtil.NToC( A2150Projeto_CustoPrevisto, 14, 2, ",", "")), context.localUtil.Format( A2150Projeto_CustoPrevisto, "ZZZ,ZZZ,ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_CustoPrevisto_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_fatorescala_Internalname, "Fator de Escala", "", "", lblTextblockprojeto_fatorescala_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_68_DH2( true) ;
         }
         else
         {
            wb_table5_68_DH2( false) ;
         }
         return  ;
      }

      protected void wb_table5_68_DH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_incremental_Internalname, "Incremental", "", "", lblTextblockprojeto_incremental_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkProjeto_Incremental_Internalname, StringUtil.BoolToStr( A1232Projeto_Incremental), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_status_Internalname, "Status", "", "", lblTextblockprojeto_status_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbProjeto_Status, cmbProjeto_Status_Internalname, StringUtil.RTrim( A658Projeto_Status), 1, cmbProjeto_Status_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_ProjetoGeneral.htm");
            cmbProjeto_Status.CurrentValue = StringUtil.RTrim( A658Projeto_Status);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbProjeto_Status_Internalname, "Values", (String)(cmbProjeto_Status.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_DH2e( true) ;
         }
         else
         {
            wb_table2_8_DH2e( false) ;
         }
      }

      protected void wb_table5_68_DH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedprojeto_fatorescala_Internalname, tblTablemergedprojeto_fatorescala_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProjeto_FatorEscala_Internalname, StringUtil.LTrim( StringUtil.NToC( A985Projeto_FatorEscala, 6, 2, ",", "")), context.localUtil.Format( A985Projeto_FatorEscala, "ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_FatorEscala_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 50, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_fatormultiplicador_Internalname, "Fator Multiplicador", "", "", lblTextblockprojeto_fatormultiplicador_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProjeto_FatorMultiplicador_Internalname, StringUtil.LTrim( StringUtil.NToC( A989Projeto_FatorMultiplicador, 6, 2, ",", "")), context.localUtil.Format( A989Projeto_FatorMultiplicador, "ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_FatorMultiplicador_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 50, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_constacocomo_Internalname, "Constante A (Cocomo)", "", "", lblTextblockprojeto_constacocomo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProjeto_ConstACocomo_Internalname, StringUtil.LTrim( StringUtil.NToC( A990Projeto_ConstACocomo, 6, 2, ",", "")), context.localUtil.Format( A990Projeto_ConstACocomo, "ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_ConstACocomo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 50, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_68_DH2e( true) ;
         }
         else
         {
            wb_table5_68_DH2e( false) ;
         }
      }

      protected void wb_table4_52_DH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedprojeto_prazoprevisto_Internalname, tblTablemergedprojeto_prazoprevisto_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProjeto_PrazoPrevisto_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2148Projeto_PrazoPrevisto), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A2148Projeto_PrazoPrevisto), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_PrazoPrevisto_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockprojeto_esforcoprevisto_Internalname, "Previs�o de Esfor�o", "", "", lblTextblockprojeto_esforcoprevisto_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtProjeto_EsforcoPrevisto_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2149Projeto_EsforcoPrevisto), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A2149Projeto_EsforcoPrevisto), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtProjeto_EsforcoPrevisto_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ProjetoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_52_DH2e( true) ;
         }
         else
         {
            wb_table4_52_DH2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A648Projeto_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PADH2( ) ;
         WSDH2( ) ;
         WEDH2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA648Projeto_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PADH2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "projetogeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PADH2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A648Projeto_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
         }
         wcpOA648Projeto_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA648Projeto_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A648Projeto_Codigo != wcpOA648Projeto_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA648Projeto_Codigo = A648Projeto_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA648Projeto_Codigo = cgiGet( sPrefix+"A648Projeto_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA648Projeto_Codigo) > 0 )
         {
            A648Projeto_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA648Projeto_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
         }
         else
         {
            A648Projeto_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A648Projeto_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PADH2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSDH2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSDH2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A648Projeto_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A648Projeto_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA648Projeto_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A648Projeto_Codigo_CTRL", StringUtil.RTrim( sCtrlA648Projeto_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEDH2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202054840013");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("projetogeneral.js", "?202054840013");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockprojeto_sigla_Internalname = sPrefix+"TEXTBLOCKPROJETO_SIGLA";
         edtProjeto_Sigla_Internalname = sPrefix+"PROJETO_SIGLA";
         lblTextblockprojeto_identificador_Internalname = sPrefix+"TEXTBLOCKPROJETO_IDENTIFICADOR";
         edtProjeto_Identificador_Internalname = sPrefix+"PROJETO_IDENTIFICADOR";
         lblTextblockprojeto_nome_Internalname = sPrefix+"TEXTBLOCKPROJETO_NOME";
         edtProjeto_Nome_Internalname = sPrefix+"PROJETO_NOME";
         lblTextblockprojeto_tipoprojetocod_Internalname = sPrefix+"TEXTBLOCKPROJETO_TIPOPROJETOCOD";
         edtProjeto_TipoProjetoCod_Internalname = sPrefix+"PROJETO_TIPOPROJETOCOD";
         lblTextblockprojeto_objetivo_Internalname = sPrefix+"TEXTBLOCKPROJETO_OBJETIVO";
         edtProjeto_Objetivo_Internalname = sPrefix+"PROJETO_OBJETIVO";
         lblTextblockprojeto_escopo_Internalname = sPrefix+"TEXTBLOCKPROJETO_ESCOPO";
         edtProjeto_Escopo_Internalname = sPrefix+"PROJETO_ESCOPO";
         lblTextblockprojeto_dtfim_Internalname = sPrefix+"TEXTBLOCKPROJETO_DTFIM";
         edtProjeto_DTFim_Internalname = sPrefix+"PROJETO_DTFIM";
         lblTextblockprojeto_dtinicio_Internalname = sPrefix+"TEXTBLOCKPROJETO_DTINICIO";
         edtProjeto_DTInicio_Internalname = sPrefix+"PROJETO_DTINICIO";
         lblTextblockprojeto_prazoprevisto_Internalname = sPrefix+"TEXTBLOCKPROJETO_PRAZOPREVISTO";
         edtProjeto_PrazoPrevisto_Internalname = sPrefix+"PROJETO_PRAZOPREVISTO";
         lblTextblockprojeto_esforcoprevisto_Internalname = sPrefix+"TEXTBLOCKPROJETO_ESFORCOPREVISTO";
         edtProjeto_EsforcoPrevisto_Internalname = sPrefix+"PROJETO_ESFORCOPREVISTO";
         tblTablemergedprojeto_prazoprevisto_Internalname = sPrefix+"TABLEMERGEDPROJETO_PRAZOPREVISTO";
         lblTextblockprojeto_custoprevisto_Internalname = sPrefix+"TEXTBLOCKPROJETO_CUSTOPREVISTO";
         edtProjeto_CustoPrevisto_Internalname = sPrefix+"PROJETO_CUSTOPREVISTO";
         lblTextblockprojeto_fatorescala_Internalname = sPrefix+"TEXTBLOCKPROJETO_FATORESCALA";
         edtProjeto_FatorEscala_Internalname = sPrefix+"PROJETO_FATORESCALA";
         lblTextblockprojeto_fatormultiplicador_Internalname = sPrefix+"TEXTBLOCKPROJETO_FATORMULTIPLICADOR";
         edtProjeto_FatorMultiplicador_Internalname = sPrefix+"PROJETO_FATORMULTIPLICADOR";
         lblTextblockprojeto_constacocomo_Internalname = sPrefix+"TEXTBLOCKPROJETO_CONSTACOCOMO";
         edtProjeto_ConstACocomo_Internalname = sPrefix+"PROJETO_CONSTACOCOMO";
         tblTablemergedprojeto_fatorescala_Internalname = sPrefix+"TABLEMERGEDPROJETO_FATORESCALA";
         lblTextblockprojeto_incremental_Internalname = sPrefix+"TEXTBLOCKPROJETO_INCREMENTAL";
         chkProjeto_Incremental_Internalname = sPrefix+"PROJETO_INCREMENTAL";
         lblTextblockprojeto_status_Internalname = sPrefix+"TEXTBLOCKPROJETO_STATUS";
         cmbProjeto_Status_Internalname = sPrefix+"PROJETO_STATUS";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         bttBtnfechar_Internalname = sPrefix+"BTNFECHAR";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtProjeto_EsforcoPrevisto_Jsonclick = "";
         edtProjeto_PrazoPrevisto_Jsonclick = "";
         edtProjeto_ConstACocomo_Jsonclick = "";
         edtProjeto_FatorMultiplicador_Jsonclick = "";
         edtProjeto_FatorEscala_Jsonclick = "";
         cmbProjeto_Status_Jsonclick = "";
         edtProjeto_CustoPrevisto_Jsonclick = "";
         edtProjeto_DTInicio_Jsonclick = "";
         edtProjeto_DTFim_Jsonclick = "";
         edtProjeto_TipoProjetoCod_Jsonclick = "";
         edtProjeto_Nome_Jsonclick = "";
         edtProjeto_Identificador_Jsonclick = "";
         edtProjeto_Sigla_Jsonclick = "";
         bttBtndelete_Enabled = 1;
         bttBtnupdate_Enabled = 1;
         chkProjeto_Incremental.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13DH2',iparms:[{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14DH2',iparms:[{av:'A648Projeto_Codigo',fld:'PROJETO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOFECHAR'","{handler:'E15DH2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A650Projeto_Sigla = "";
         A2144Projeto_Identificador = "";
         A649Projeto_Nome = "";
         A2145Projeto_Objetivo = "";
         A653Projeto_Escopo = "";
         A2147Projeto_DTFim = DateTime.MinValue;
         A2146Projeto_DTInicio = DateTime.MinValue;
         A658Projeto_Status = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00DH2_A648Projeto_Codigo = new int[1] ;
         H00DH2_A658Projeto_Status = new String[] {""} ;
         H00DH2_A1232Projeto_Incremental = new bool[] {false} ;
         H00DH2_n1232Projeto_Incremental = new bool[] {false} ;
         H00DH2_A990Projeto_ConstACocomo = new decimal[1] ;
         H00DH2_n990Projeto_ConstACocomo = new bool[] {false} ;
         H00DH2_A989Projeto_FatorMultiplicador = new decimal[1] ;
         H00DH2_n989Projeto_FatorMultiplicador = new bool[] {false} ;
         H00DH2_A985Projeto_FatorEscala = new decimal[1] ;
         H00DH2_n985Projeto_FatorEscala = new bool[] {false} ;
         H00DH2_A2150Projeto_CustoPrevisto = new decimal[1] ;
         H00DH2_n2150Projeto_CustoPrevisto = new bool[] {false} ;
         H00DH2_A2149Projeto_EsforcoPrevisto = new short[1] ;
         H00DH2_n2149Projeto_EsforcoPrevisto = new bool[] {false} ;
         H00DH2_A2148Projeto_PrazoPrevisto = new short[1] ;
         H00DH2_n2148Projeto_PrazoPrevisto = new bool[] {false} ;
         H00DH2_A2146Projeto_DTInicio = new DateTime[] {DateTime.MinValue} ;
         H00DH2_n2146Projeto_DTInicio = new bool[] {false} ;
         H00DH2_A2147Projeto_DTFim = new DateTime[] {DateTime.MinValue} ;
         H00DH2_n2147Projeto_DTFim = new bool[] {false} ;
         H00DH2_A653Projeto_Escopo = new String[] {""} ;
         H00DH2_A2145Projeto_Objetivo = new String[] {""} ;
         H00DH2_n2145Projeto_Objetivo = new bool[] {false} ;
         H00DH2_A983Projeto_TipoProjetoCod = new int[1] ;
         H00DH2_n983Projeto_TipoProjetoCod = new bool[] {false} ;
         H00DH2_A649Projeto_Nome = new String[] {""} ;
         H00DH2_A2144Projeto_Identificador = new String[] {""} ;
         H00DH2_n2144Projeto_Identificador = new bool[] {false} ;
         H00DH2_A650Projeto_Sigla = new String[] {""} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblockprojeto_sigla_Jsonclick = "";
         lblTextblockprojeto_identificador_Jsonclick = "";
         lblTextblockprojeto_nome_Jsonclick = "";
         lblTextblockprojeto_tipoprojetocod_Jsonclick = "";
         lblTextblockprojeto_objetivo_Jsonclick = "";
         lblTextblockprojeto_escopo_Jsonclick = "";
         lblTextblockprojeto_dtfim_Jsonclick = "";
         lblTextblockprojeto_dtinicio_Jsonclick = "";
         lblTextblockprojeto_prazoprevisto_Jsonclick = "";
         lblTextblockprojeto_custoprevisto_Jsonclick = "";
         lblTextblockprojeto_fatorescala_Jsonclick = "";
         lblTextblockprojeto_incremental_Jsonclick = "";
         lblTextblockprojeto_status_Jsonclick = "";
         lblTextblockprojeto_fatormultiplicador_Jsonclick = "";
         lblTextblockprojeto_constacocomo_Jsonclick = "";
         lblTextblockprojeto_esforcoprevisto_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA648Projeto_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.projetogeneral__default(),
            new Object[][] {
                new Object[] {
               H00DH2_A648Projeto_Codigo, H00DH2_A658Projeto_Status, H00DH2_A1232Projeto_Incremental, H00DH2_n1232Projeto_Incremental, H00DH2_A990Projeto_ConstACocomo, H00DH2_n990Projeto_ConstACocomo, H00DH2_A989Projeto_FatorMultiplicador, H00DH2_n989Projeto_FatorMultiplicador, H00DH2_A985Projeto_FatorEscala, H00DH2_n985Projeto_FatorEscala,
               H00DH2_A2150Projeto_CustoPrevisto, H00DH2_n2150Projeto_CustoPrevisto, H00DH2_A2149Projeto_EsforcoPrevisto, H00DH2_n2149Projeto_EsforcoPrevisto, H00DH2_A2148Projeto_PrazoPrevisto, H00DH2_n2148Projeto_PrazoPrevisto, H00DH2_A2146Projeto_DTInicio, H00DH2_n2146Projeto_DTInicio, H00DH2_A2147Projeto_DTFim, H00DH2_n2147Projeto_DTFim,
               H00DH2_A653Projeto_Escopo, H00DH2_A2145Projeto_Objetivo, H00DH2_n2145Projeto_Objetivo, H00DH2_A983Projeto_TipoProjetoCod, H00DH2_n983Projeto_TipoProjetoCod, H00DH2_A649Projeto_Nome, H00DH2_A2144Projeto_Identificador, H00DH2_n2144Projeto_Identificador, H00DH2_A650Projeto_Sigla
               }
            }
         );
         AV14Pgmname = "ProjetoGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "ProjetoGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A2148Projeto_PrazoPrevisto ;
      private short A2149Projeto_EsforcoPrevisto ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A648Projeto_Codigo ;
      private int wcpOA648Projeto_Codigo ;
      private int A983Projeto_TipoProjetoCod ;
      private int bttBtnupdate_Enabled ;
      private int bttBtndelete_Enabled ;
      private int AV7Projeto_Codigo ;
      private int idxLst ;
      private decimal A2150Projeto_CustoPrevisto ;
      private decimal A985Projeto_FatorEscala ;
      private decimal A989Projeto_FatorMultiplicador ;
      private decimal A990Projeto_ConstACocomo ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A650Projeto_Sigla ;
      private String A649Projeto_Nome ;
      private String A658Projeto_Status ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkProjeto_Incremental_Internalname ;
      private String scmdbuf ;
      private String edtProjeto_Sigla_Internalname ;
      private String edtProjeto_Identificador_Internalname ;
      private String edtProjeto_Nome_Internalname ;
      private String edtProjeto_TipoProjetoCod_Internalname ;
      private String edtProjeto_Objetivo_Internalname ;
      private String edtProjeto_Escopo_Internalname ;
      private String edtProjeto_DTFim_Internalname ;
      private String edtProjeto_DTInicio_Internalname ;
      private String edtProjeto_PrazoPrevisto_Internalname ;
      private String edtProjeto_EsforcoPrevisto_Internalname ;
      private String edtProjeto_CustoPrevisto_Internalname ;
      private String edtProjeto_FatorEscala_Internalname ;
      private String edtProjeto_FatorMultiplicador_Internalname ;
      private String edtProjeto_ConstACocomo_Internalname ;
      private String cmbProjeto_Status_Internalname ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockprojeto_sigla_Internalname ;
      private String lblTextblockprojeto_sigla_Jsonclick ;
      private String edtProjeto_Sigla_Jsonclick ;
      private String lblTextblockprojeto_identificador_Internalname ;
      private String lblTextblockprojeto_identificador_Jsonclick ;
      private String edtProjeto_Identificador_Jsonclick ;
      private String lblTextblockprojeto_nome_Internalname ;
      private String lblTextblockprojeto_nome_Jsonclick ;
      private String edtProjeto_Nome_Jsonclick ;
      private String lblTextblockprojeto_tipoprojetocod_Internalname ;
      private String lblTextblockprojeto_tipoprojetocod_Jsonclick ;
      private String edtProjeto_TipoProjetoCod_Jsonclick ;
      private String lblTextblockprojeto_objetivo_Internalname ;
      private String lblTextblockprojeto_objetivo_Jsonclick ;
      private String lblTextblockprojeto_escopo_Internalname ;
      private String lblTextblockprojeto_escopo_Jsonclick ;
      private String lblTextblockprojeto_dtfim_Internalname ;
      private String lblTextblockprojeto_dtfim_Jsonclick ;
      private String edtProjeto_DTFim_Jsonclick ;
      private String lblTextblockprojeto_dtinicio_Internalname ;
      private String lblTextblockprojeto_dtinicio_Jsonclick ;
      private String edtProjeto_DTInicio_Jsonclick ;
      private String lblTextblockprojeto_prazoprevisto_Internalname ;
      private String lblTextblockprojeto_prazoprevisto_Jsonclick ;
      private String lblTextblockprojeto_custoprevisto_Internalname ;
      private String lblTextblockprojeto_custoprevisto_Jsonclick ;
      private String edtProjeto_CustoPrevisto_Jsonclick ;
      private String lblTextblockprojeto_fatorescala_Internalname ;
      private String lblTextblockprojeto_fatorescala_Jsonclick ;
      private String lblTextblockprojeto_incremental_Internalname ;
      private String lblTextblockprojeto_incremental_Jsonclick ;
      private String lblTextblockprojeto_status_Internalname ;
      private String lblTextblockprojeto_status_Jsonclick ;
      private String cmbProjeto_Status_Jsonclick ;
      private String tblTablemergedprojeto_fatorescala_Internalname ;
      private String edtProjeto_FatorEscala_Jsonclick ;
      private String lblTextblockprojeto_fatormultiplicador_Internalname ;
      private String lblTextblockprojeto_fatormultiplicador_Jsonclick ;
      private String edtProjeto_FatorMultiplicador_Jsonclick ;
      private String lblTextblockprojeto_constacocomo_Internalname ;
      private String lblTextblockprojeto_constacocomo_Jsonclick ;
      private String edtProjeto_ConstACocomo_Jsonclick ;
      private String tblTablemergedprojeto_prazoprevisto_Internalname ;
      private String edtProjeto_PrazoPrevisto_Jsonclick ;
      private String lblTextblockprojeto_esforcoprevisto_Internalname ;
      private String lblTextblockprojeto_esforcoprevisto_Jsonclick ;
      private String edtProjeto_EsforcoPrevisto_Jsonclick ;
      private String sCtrlA648Projeto_Codigo ;
      private DateTime A2147Projeto_DTFim ;
      private DateTime A2146Projeto_DTInicio ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A1232Projeto_Incremental ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1232Projeto_Incremental ;
      private bool n990Projeto_ConstACocomo ;
      private bool n989Projeto_FatorMultiplicador ;
      private bool n985Projeto_FatorEscala ;
      private bool n2150Projeto_CustoPrevisto ;
      private bool n2149Projeto_EsforcoPrevisto ;
      private bool n2148Projeto_PrazoPrevisto ;
      private bool n2146Projeto_DTInicio ;
      private bool n2147Projeto_DTFim ;
      private bool n2145Projeto_Objetivo ;
      private bool n983Projeto_TipoProjetoCod ;
      private bool n2144Projeto_Identificador ;
      private bool returnInSub ;
      private String A2145Projeto_Objetivo ;
      private String A653Projeto_Escopo ;
      private String A2144Projeto_Identificador ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkProjeto_Incremental ;
      private GXCombobox cmbProjeto_Status ;
      private IDataStoreProvider pr_default ;
      private int[] H00DH2_A648Projeto_Codigo ;
      private String[] H00DH2_A658Projeto_Status ;
      private bool[] H00DH2_A1232Projeto_Incremental ;
      private bool[] H00DH2_n1232Projeto_Incremental ;
      private decimal[] H00DH2_A990Projeto_ConstACocomo ;
      private bool[] H00DH2_n990Projeto_ConstACocomo ;
      private decimal[] H00DH2_A989Projeto_FatorMultiplicador ;
      private bool[] H00DH2_n989Projeto_FatorMultiplicador ;
      private decimal[] H00DH2_A985Projeto_FatorEscala ;
      private bool[] H00DH2_n985Projeto_FatorEscala ;
      private decimal[] H00DH2_A2150Projeto_CustoPrevisto ;
      private bool[] H00DH2_n2150Projeto_CustoPrevisto ;
      private short[] H00DH2_A2149Projeto_EsforcoPrevisto ;
      private bool[] H00DH2_n2149Projeto_EsforcoPrevisto ;
      private short[] H00DH2_A2148Projeto_PrazoPrevisto ;
      private bool[] H00DH2_n2148Projeto_PrazoPrevisto ;
      private DateTime[] H00DH2_A2146Projeto_DTInicio ;
      private bool[] H00DH2_n2146Projeto_DTInicio ;
      private DateTime[] H00DH2_A2147Projeto_DTFim ;
      private bool[] H00DH2_n2147Projeto_DTFim ;
      private String[] H00DH2_A653Projeto_Escopo ;
      private String[] H00DH2_A2145Projeto_Objetivo ;
      private bool[] H00DH2_n2145Projeto_Objetivo ;
      private int[] H00DH2_A983Projeto_TipoProjetoCod ;
      private bool[] H00DH2_n983Projeto_TipoProjetoCod ;
      private String[] H00DH2_A649Projeto_Nome ;
      private String[] H00DH2_A2144Projeto_Identificador ;
      private bool[] H00DH2_n2144Projeto_Identificador ;
      private String[] H00DH2_A650Projeto_Sigla ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class projetogeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00DH2 ;
          prmH00DH2 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00DH2", "SELECT [Projeto_Codigo], [Projeto_Status], [Projeto_Incremental], [Projeto_ConstACocomo], [Projeto_FatorMultiplicador], [Projeto_FatorEscala], [Projeto_CustoPrevisto], [Projeto_EsforcoPrevisto], [Projeto_PrazoPrevisto], [Projeto_DTInicio], [Projeto_DTFim], [Projeto_Escopo], [Projeto_Objetivo], [Projeto_TipoProjetoCod], [Projeto_Nome], [Projeto_Identificador], [Projeto_Sigla] FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_Codigo] = @Projeto_Codigo ORDER BY [Projeto_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DH2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((short[]) buf[12])[0] = rslt.getShort(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((short[]) buf[14])[0] = rslt.getShort(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[16])[0] = rslt.getGXDate(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((DateTime[]) buf[18])[0] = rslt.getGXDate(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((String[]) buf[20])[0] = rslt.getLongVarchar(12) ;
                ((String[]) buf[21])[0] = rslt.getLongVarchar(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((int[]) buf[23])[0] = rslt.getInt(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((String[]) buf[25])[0] = rslt.getString(15, 50) ;
                ((String[]) buf[26])[0] = rslt.getVarchar(16) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(16);
                ((String[]) buf[28])[0] = rslt.getString(17, 15) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
