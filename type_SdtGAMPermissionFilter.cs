/*
               File: type_SdtGAMPermissionFilter
        Description: GAMPermissionFilter
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 1:32:7.17
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMPermissionFilter : GxUserType, IGxExternalObject
   {
      public SdtGAMPermissionFilter( )
      {
         initialize();
      }

      public SdtGAMPermissionFilter( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMPermissionFilter_externalReference == null )
         {
            GAMPermissionFilter_externalReference = new Artech.Security.GAMPermissionFilter(context);
         }
         returntostring = "";
         returntostring = (String)(GAMPermissionFilter_externalReference.ToString());
         return returntostring ;
      }

      public long gxTpr_Applicationid
      {
         get {
            if ( GAMPermissionFilter_externalReference == null )
            {
               GAMPermissionFilter_externalReference = new Artech.Security.GAMPermissionFilter(context);
            }
            return GAMPermissionFilter_externalReference.ApplicationId ;
         }

         set {
            if ( GAMPermissionFilter_externalReference == null )
            {
               GAMPermissionFilter_externalReference = new Artech.Security.GAMPermissionFilter(context);
            }
            GAMPermissionFilter_externalReference.ApplicationId = value;
         }

      }

      public String gxTpr_Guid
      {
         get {
            if ( GAMPermissionFilter_externalReference == null )
            {
               GAMPermissionFilter_externalReference = new Artech.Security.GAMPermissionFilter(context);
            }
            return GAMPermissionFilter_externalReference.GUID ;
         }

         set {
            if ( GAMPermissionFilter_externalReference == null )
            {
               GAMPermissionFilter_externalReference = new Artech.Security.GAMPermissionFilter(context);
            }
            GAMPermissionFilter_externalReference.GUID = value;
         }

      }

      public String gxTpr_Name
      {
         get {
            if ( GAMPermissionFilter_externalReference == null )
            {
               GAMPermissionFilter_externalReference = new Artech.Security.GAMPermissionFilter(context);
            }
            return GAMPermissionFilter_externalReference.Name ;
         }

         set {
            if ( GAMPermissionFilter_externalReference == null )
            {
               GAMPermissionFilter_externalReference = new Artech.Security.GAMPermissionFilter(context);
            }
            GAMPermissionFilter_externalReference.Name = value;
         }

      }

      public String gxTpr_Name_expression
      {
         get {
            if ( GAMPermissionFilter_externalReference == null )
            {
               GAMPermissionFilter_externalReference = new Artech.Security.GAMPermissionFilter(context);
            }
            return GAMPermissionFilter_externalReference.Name_Expression ;
         }

         set {
            if ( GAMPermissionFilter_externalReference == null )
            {
               GAMPermissionFilter_externalReference = new Artech.Security.GAMPermissionFilter(context);
            }
            GAMPermissionFilter_externalReference.Name_Expression = value;
         }

      }

      public String gxTpr_Descripcion
      {
         get {
            if ( GAMPermissionFilter_externalReference == null )
            {
               GAMPermissionFilter_externalReference = new Artech.Security.GAMPermissionFilter(context);
            }
            return GAMPermissionFilter_externalReference.Descripcion ;
         }

         set {
            if ( GAMPermissionFilter_externalReference == null )
            {
               GAMPermissionFilter_externalReference = new Artech.Security.GAMPermissionFilter(context);
            }
            GAMPermissionFilter_externalReference.Descripcion = value;
         }

      }

      public String gxTpr_Accesstype
      {
         get {
            if ( GAMPermissionFilter_externalReference == null )
            {
               GAMPermissionFilter_externalReference = new Artech.Security.GAMPermissionFilter(context);
            }
            return GAMPermissionFilter_externalReference.AccessType ;
         }

         set {
            if ( GAMPermissionFilter_externalReference == null )
            {
               GAMPermissionFilter_externalReference = new Artech.Security.GAMPermissionFilter(context);
            }
            GAMPermissionFilter_externalReference.AccessType = value;
         }

      }

      public String gxTpr_Isautomaticpermission
      {
         get {
            if ( GAMPermissionFilter_externalReference == null )
            {
               GAMPermissionFilter_externalReference = new Artech.Security.GAMPermissionFilter(context);
            }
            return GAMPermissionFilter_externalReference.IsAutomaticPermission ;
         }

         set {
            if ( GAMPermissionFilter_externalReference == null )
            {
               GAMPermissionFilter_externalReference = new Artech.Security.GAMPermissionFilter(context);
            }
            GAMPermissionFilter_externalReference.IsAutomaticPermission = value;
         }

      }

      public String gxTpr_Inherited
      {
         get {
            if ( GAMPermissionFilter_externalReference == null )
            {
               GAMPermissionFilter_externalReference = new Artech.Security.GAMPermissionFilter(context);
            }
            return GAMPermissionFilter_externalReference.Inherited ;
         }

         set {
            if ( GAMPermissionFilter_externalReference == null )
            {
               GAMPermissionFilter_externalReference = new Artech.Security.GAMPermissionFilter(context);
            }
            GAMPermissionFilter_externalReference.Inherited = value;
         }

      }

      public String gxTpr_Token
      {
         get {
            if ( GAMPermissionFilter_externalReference == null )
            {
               GAMPermissionFilter_externalReference = new Artech.Security.GAMPermissionFilter(context);
            }
            return GAMPermissionFilter_externalReference.Token ;
         }

         set {
            if ( GAMPermissionFilter_externalReference == null )
            {
               GAMPermissionFilter_externalReference = new Artech.Security.GAMPermissionFilter(context);
            }
            GAMPermissionFilter_externalReference.Token = value;
         }

      }

      public bool gxTpr_Loadproperties
      {
         get {
            if ( GAMPermissionFilter_externalReference == null )
            {
               GAMPermissionFilter_externalReference = new Artech.Security.GAMPermissionFilter(context);
            }
            return GAMPermissionFilter_externalReference.LoadProperties ;
         }

         set {
            if ( GAMPermissionFilter_externalReference == null )
            {
               GAMPermissionFilter_externalReference = new Artech.Security.GAMPermissionFilter(context);
            }
            GAMPermissionFilter_externalReference.LoadProperties = value;
         }

      }

      public int gxTpr_Start
      {
         get {
            if ( GAMPermissionFilter_externalReference == null )
            {
               GAMPermissionFilter_externalReference = new Artech.Security.GAMPermissionFilter(context);
            }
            return GAMPermissionFilter_externalReference.Start ;
         }

         set {
            if ( GAMPermissionFilter_externalReference == null )
            {
               GAMPermissionFilter_externalReference = new Artech.Security.GAMPermissionFilter(context);
            }
            GAMPermissionFilter_externalReference.Start = value;
         }

      }

      public int gxTpr_Limit
      {
         get {
            if ( GAMPermissionFilter_externalReference == null )
            {
               GAMPermissionFilter_externalReference = new Artech.Security.GAMPermissionFilter(context);
            }
            return GAMPermissionFilter_externalReference.Limit ;
         }

         set {
            if ( GAMPermissionFilter_externalReference == null )
            {
               GAMPermissionFilter_externalReference = new Artech.Security.GAMPermissionFilter(context);
            }
            GAMPermissionFilter_externalReference.Limit = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMPermissionFilter_externalReference == null )
            {
               GAMPermissionFilter_externalReference = new Artech.Security.GAMPermissionFilter(context);
            }
            return GAMPermissionFilter_externalReference ;
         }

         set {
            GAMPermissionFilter_externalReference = (Artech.Security.GAMPermissionFilter)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMPermissionFilter GAMPermissionFilter_externalReference=null ;
   }

}
