/*
               File: type_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem
        Description: GxSynchroEventResultSDT
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:56.89
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "GxSynchroEventResultSDT.GxSynchroEventResultSDTItem" )]
   [XmlType(TypeName =  "GxSynchroEventResultSDT.GxSynchroEventResultSDTItem" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem ))]
   [Serializable]
   public class SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem : GxUserType
   {
      public SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem( )
      {
         /* Constructor for serialization */
         gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventtimestamp = (DateTime)(DateTime.MinValue);
         gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventerrors = "";
      }

      public SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem obj ;
         obj = this;
         obj.gxTpr_Eventid = (Guid)(deserialized.gxTpr_Eventid);
         obj.gxTpr_Eventtimestamp = deserialized.gxTpr_Eventtimestamp;
         obj.gxTpr_Eventstatus = deserialized.gxTpr_Eventstatus;
         obj.gxTpr_Eventerrors = deserialized.gxTpr_Eventerrors;
         obj.gxTpr_Mappings = deserialized.gxTpr_Mappings;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "EventId") )
               {
                  gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventid = (Guid)(StringUtil.StrToGuid( oReader.Value));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "EventTimestamp") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventtimestamp = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventtimestamp = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "EventStatus") )
               {
                  gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventstatus = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "EventErrors") )
               {
                  gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventerrors = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mappings") )
               {
                  if ( gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Mappings == null )
                  {
                     gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Mappings = new GxObjectCollection( context, "GxSynchroEventResultSDT.GxSynchroEventResultSDTItem.MappingsItem", "", "SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem", "GeneXus.Programs");
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Mappings.readxmlcollection(oReader, "Mappings", "MappingsItem");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "GxSynchroEventResultSDT.GxSynchroEventResultSDTItem";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("EventId", StringUtil.RTrim( gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventid.ToString()));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( (DateTime.MinValue==gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventtimestamp) )
         {
            oWriter.WriteStartElement("EventTimestamp");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventtimestamp)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventtimestamp)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventtimestamp)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventtimestamp)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventtimestamp)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventtimestamp)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("EventTimestamp", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteElement("EventStatus", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventstatus), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("EventErrors", StringUtil.RTrim( gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventerrors));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Mappings != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_MeetrikaVs3";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_MeetrikaVs3";
            }
            gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Mappings.writexmlcollection(oWriter, "Mappings", sNameSpace1, "MappingsItem", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("EventId", gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventid, false);
         datetime_STZ = gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventtimestamp;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("EventTimestamp", sDateCnv, false);
         AddObjectProperty("EventStatus", gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventstatus, false);
         AddObjectProperty("EventErrors", gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventerrors, false);
         if ( gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Mappings != null )
         {
            AddObjectProperty("Mappings", gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Mappings, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "EventId" )]
      [  XmlElement( ElementName = "EventId"   )]
      public Guid gxTpr_Eventid
      {
         get {
            return gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventid ;
         }

         set {
            gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventid = (Guid)(value);
         }

      }

      [  SoapElement( ElementName = "EventTimestamp" )]
      [  XmlElement( ElementName = "EventTimestamp"  , IsNullable=true )]
      public string gxTpr_Eventtimestamp_Nullable
      {
         get {
            if ( gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventtimestamp == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventtimestamp).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventtimestamp = DateTime.MinValue;
            else
               gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventtimestamp = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Eventtimestamp
      {
         get {
            return gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventtimestamp ;
         }

         set {
            gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventtimestamp = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "EventStatus" )]
      [  XmlElement( ElementName = "EventStatus"   )]
      public short gxTpr_Eventstatus
      {
         get {
            return gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventstatus ;
         }

         set {
            gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventstatus = (short)(value);
         }

      }

      [  SoapElement( ElementName = "EventErrors" )]
      [  XmlElement( ElementName = "EventErrors"   )]
      public String gxTpr_Eventerrors
      {
         get {
            return gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventerrors ;
         }

         set {
            gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventerrors = (String)(value);
         }

      }

      public class gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Mappings_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_80compatibility:SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem {}
      [  SoapElement( ElementName = "Mappings" )]
      [  XmlArray( ElementName = "Mappings"  )]
      [  XmlArrayItemAttribute( Type= typeof( SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem ), ElementName= "MappingsItem"  , IsNullable=false)]
      [  XmlArrayItemAttribute( Type= typeof( gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Mappings_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_80compatibility ), ElementName= "GxSynchroEventResultSDT.GxSynchroEventResultSDTItem.MappingsItem"  , IsNullable=false)]
      public GxObjectCollection gxTpr_Mappings_GxObjectCollection
      {
         get {
            if ( gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Mappings == null )
            {
               gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Mappings = new GxObjectCollection( context, "GxSynchroEventResultSDT.GxSynchroEventResultSDTItem.MappingsItem", "", "SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Mappings ;
         }

         set {
            if ( gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Mappings == null )
            {
               gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Mappings = new GxObjectCollection( context, "GxSynchroEventResultSDT.GxSynchroEventResultSDTItem.MappingsItem", "", "SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem", "GeneXus.Programs");
            }
            gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Mappings = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Mappings
      {
         get {
            if ( gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Mappings == null )
            {
               gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Mappings = new GxObjectCollection( context, "GxSynchroEventResultSDT.GxSynchroEventResultSDTItem.MappingsItem", "", "SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem", "GeneXus.Programs");
            }
            return gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Mappings ;
         }

         set {
            gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Mappings = value;
         }

      }

      public void gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Mappings_SetNull( )
      {
         gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Mappings = null;
         return  ;
      }

      public bool gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Mappings_IsNull( )
      {
         if ( gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Mappings == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventtimestamp = (DateTime)(DateTime.MinValue);
         gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventerrors = "";
         gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventid = (Guid)(System.Guid.Empty);
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         datetime_STZ = (DateTime)(DateTime.MinValue);
         return  ;
      }

      protected short gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventstatus ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected String sTagName ;
      protected String sDateCnv ;
      protected String sNumToPad ;
      protected DateTime gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventtimestamp ;
      protected DateTime datetime_STZ ;
      protected String gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventerrors ;
      protected Guid gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Eventid ;
      [ObjectCollection(ItemType=typeof( SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem ))]
      protected IGxCollection gxTv_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_Mappings=null ;
   }

   [DataContract(Name = @"GxSynchroEventResultSDT.GxSynchroEventResultSDTItem", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_RESTInterface : GxGenericCollectionItem<SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_RESTInterface( ) : base()
      {
      }

      public SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_RESTInterface( SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "EventId" , Order = 0 )]
      public Guid gxTpr_Eventid
      {
         get {
            return sdt.gxTpr_Eventid ;
         }

         set {
            sdt.gxTpr_Eventid = (Guid)((Guid)(value));
         }

      }

      [DataMember( Name = "EventTimestamp" , Order = 1 )]
      public String gxTpr_Eventtimestamp
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Eventtimestamp) ;
         }

         set {
            sdt.gxTpr_Eventtimestamp = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "EventStatus" , Order = 2 )]
      public Nullable<short> gxTpr_Eventstatus
      {
         get {
            return sdt.gxTpr_Eventstatus ;
         }

         set {
            sdt.gxTpr_Eventstatus = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "EventErrors" , Order = 3 )]
      public String gxTpr_Eventerrors
      {
         get {
            return sdt.gxTpr_Eventerrors ;
         }

         set {
            sdt.gxTpr_Eventerrors = (String)(value);
         }

      }

      [DataMember( Name = "Mappings" , Order = 4 )]
      public GxGenericCollection<SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_RESTInterface> gxTpr_Mappings
      {
         get {
            return new GxGenericCollection<SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem_RESTInterface>(sdt.gxTpr_Mappings) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Mappings);
         }

      }

      public SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem sdt
      {
         get {
            return (SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem() ;
         }
      }

   }

}
