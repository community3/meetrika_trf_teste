/*
               File: GetPromptContratoServicosTelasFilterData
        Description: Get Prompt Contrato Servicos Telas Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:4:54.39
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptcontratoservicostelasfilterdata : GXProcedure
   {
      public getpromptcontratoservicostelasfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptcontratoservicostelasfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV24DDOName = aP0_DDOName;
         this.AV22SearchTxt = aP1_SearchTxt;
         this.AV23SearchTxtTo = aP2_SearchTxtTo;
         this.AV28OptionsJson = "" ;
         this.AV31OptionsDescJson = "" ;
         this.AV33OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV24DDOName = aP0_DDOName;
         this.AV22SearchTxt = aP1_SearchTxt;
         this.AV23SearchTxtTo = aP2_SearchTxtTo;
         this.AV28OptionsJson = "" ;
         this.AV31OptionsDescJson = "" ;
         this.AV33OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
         return AV33OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptcontratoservicostelasfilterdata objgetpromptcontratoservicostelasfilterdata;
         objgetpromptcontratoservicostelasfilterdata = new getpromptcontratoservicostelasfilterdata();
         objgetpromptcontratoservicostelasfilterdata.AV24DDOName = aP0_DDOName;
         objgetpromptcontratoservicostelasfilterdata.AV22SearchTxt = aP1_SearchTxt;
         objgetpromptcontratoservicostelasfilterdata.AV23SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptcontratoservicostelasfilterdata.AV28OptionsJson = "" ;
         objgetpromptcontratoservicostelasfilterdata.AV31OptionsDescJson = "" ;
         objgetpromptcontratoservicostelasfilterdata.AV33OptionIndexesJson = "" ;
         objgetpromptcontratoservicostelasfilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptcontratoservicostelasfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptcontratoservicostelasfilterdata);
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptcontratoservicostelasfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV27Options = (IGxCollection)(new GxSimpleCollection());
         AV30OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV32OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_CONTRATADA_PESSOANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATADA_PESSOANOMOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSERVICOSTELAS_SERVICOSIGLAOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_CONTRATOSERVICOSTELAS_TELA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSERVICOSTELAS_TELAOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_CONTRATOSERVICOSTELAS_LINK") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSERVICOSTELAS_LINKOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_CONTRATOSERVICOSTELAS_PARMS") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSERVICOSTELAS_PARMSOPTIONS' */
            S161 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV28OptionsJson = AV27Options.ToJSonString(false);
         AV31OptionsDescJson = AV30OptionsDesc.ToJSonString(false);
         AV33OptionIndexesJson = AV32OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV35Session.Get("PromptContratoServicosTelasGridState"), "") == 0 )
         {
            AV37GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptContratoServicosTelasGridState"), "");
         }
         else
         {
            AV37GridState.FromXml(AV35Session.Get("PromptContratoServicosTelasGridState"), "");
         }
         AV53GXV1 = 1;
         while ( AV53GXV1 <= AV37GridState.gxTpr_Filtervalues.Count )
         {
            AV38GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV37GridState.gxTpr_Filtervalues.Item(AV53GXV1));
            if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM") == 0 )
            {
               AV10TFContratada_PessoaNom = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM_SEL") == 0 )
            {
               AV11TFContratada_PessoaNom_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_SERVICOSIGLA") == 0 )
            {
               AV12TFContratoServicosTelas_ServicoSigla = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_SERVICOSIGLA_SEL") == 0 )
            {
               AV13TFContratoServicosTelas_ServicoSigla_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_TELA") == 0 )
            {
               AV14TFContratoServicosTelas_Tela = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_TELA_SEL") == 0 )
            {
               AV15TFContratoServicosTelas_Tela_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_LINK") == 0 )
            {
               AV16TFContratoServicosTelas_Link = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_LINK_SEL") == 0 )
            {
               AV17TFContratoServicosTelas_Link_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_PARMS") == 0 )
            {
               AV18TFContratoServicosTelas_Parms = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_PARMS_SEL") == 0 )
            {
               AV19TFContratoServicosTelas_Parms_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_STATUS_SEL") == 0 )
            {
               AV20TFContratoServicosTelas_Status_SelsJson = AV38GridStateFilterValue.gxTpr_Value;
               AV21TFContratoServicosTelas_Status_Sels.FromJSonString(AV20TFContratoServicosTelas_Status_SelsJson);
            }
            AV53GXV1 = (int)(AV53GXV1+1);
         }
         if ( AV37GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV39GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV37GridState.gxTpr_Dynamicfilters.Item(1));
            AV40DynamicFiltersSelector1 = AV39GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 )
            {
               AV41DynamicFiltersOperator1 = AV39GridStateDynamicFilter.gxTpr_Operator;
               AV42ContratoServicosTelas_ServicoCod1 = (int)(NumberUtil.Val( AV39GridStateDynamicFilter.gxTpr_Value, "."));
            }
            if ( AV37GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV43DynamicFiltersEnabled2 = true;
               AV39GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV37GridState.gxTpr_Dynamicfilters.Item(2));
               AV44DynamicFiltersSelector2 = AV39GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 )
               {
                  AV45DynamicFiltersOperator2 = AV39GridStateDynamicFilter.gxTpr_Operator;
                  AV46ContratoServicosTelas_ServicoCod2 = (int)(NumberUtil.Val( AV39GridStateDynamicFilter.gxTpr_Value, "."));
               }
               if ( AV37GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV47DynamicFiltersEnabled3 = true;
                  AV39GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV37GridState.gxTpr_Dynamicfilters.Item(3));
                  AV48DynamicFiltersSelector3 = AV39GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV48DynamicFiltersSelector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 )
                  {
                     AV49DynamicFiltersOperator3 = AV39GridStateDynamicFilter.gxTpr_Operator;
                     AV50ContratoServicosTelas_ServicoCod3 = (int)(NumberUtil.Val( AV39GridStateDynamicFilter.gxTpr_Value, "."));
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATADA_PESSOANOMOPTIONS' Routine */
         AV10TFContratada_PessoaNom = AV22SearchTxt;
         AV11TFContratada_PessoaNom_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A932ContratoServicosTelas_Status ,
                                              AV21TFContratoServicosTelas_Status_Sels ,
                                              AV40DynamicFiltersSelector1 ,
                                              AV41DynamicFiltersOperator1 ,
                                              AV42ContratoServicosTelas_ServicoCod1 ,
                                              AV43DynamicFiltersEnabled2 ,
                                              AV44DynamicFiltersSelector2 ,
                                              AV45DynamicFiltersOperator2 ,
                                              AV46ContratoServicosTelas_ServicoCod2 ,
                                              AV47DynamicFiltersEnabled3 ,
                                              AV48DynamicFiltersSelector3 ,
                                              AV49DynamicFiltersOperator3 ,
                                              AV50ContratoServicosTelas_ServicoCod3 ,
                                              AV11TFContratada_PessoaNom_Sel ,
                                              AV10TFContratada_PessoaNom ,
                                              AV13TFContratoServicosTelas_ServicoSigla_Sel ,
                                              AV12TFContratoServicosTelas_ServicoSigla ,
                                              AV15TFContratoServicosTelas_Tela_Sel ,
                                              AV14TFContratoServicosTelas_Tela ,
                                              AV17TFContratoServicosTelas_Link_Sel ,
                                              AV16TFContratoServicosTelas_Link ,
                                              AV19TFContratoServicosTelas_Parms_Sel ,
                                              AV18TFContratoServicosTelas_Parms ,
                                              AV21TFContratoServicosTelas_Status_Sels.Count ,
                                              A925ContratoServicosTelas_ServicoCod ,
                                              A41Contratada_PessoaNom ,
                                              A937ContratoServicosTelas_ServicoSigla ,
                                              A931ContratoServicosTelas_Tela ,
                                              A928ContratoServicosTelas_Link ,
                                              A929ContratoServicosTelas_Parms },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV10TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV10TFContratada_PessoaNom), 100, "%");
         lV12TFContratoServicosTelas_ServicoSigla = StringUtil.PadR( StringUtil.RTrim( AV12TFContratoServicosTelas_ServicoSigla), 15, "%");
         lV14TFContratoServicosTelas_Tela = StringUtil.PadR( StringUtil.RTrim( AV14TFContratoServicosTelas_Tela), 50, "%");
         lV16TFContratoServicosTelas_Link = StringUtil.Concat( StringUtil.RTrim( AV16TFContratoServicosTelas_Link), "%", "");
         lV18TFContratoServicosTelas_Parms = StringUtil.Concat( StringUtil.RTrim( AV18TFContratoServicosTelas_Parms), "%", "");
         /* Using cursor P00PA2 */
         pr_default.execute(0, new Object[] {AV42ContratoServicosTelas_ServicoCod1, AV42ContratoServicosTelas_ServicoCod1, AV42ContratoServicosTelas_ServicoCod1, AV46ContratoServicosTelas_ServicoCod2, AV46ContratoServicosTelas_ServicoCod2, AV46ContratoServicosTelas_ServicoCod2, AV50ContratoServicosTelas_ServicoCod3, AV50ContratoServicosTelas_ServicoCod3, AV50ContratoServicosTelas_ServicoCod3, lV10TFContratada_PessoaNom, AV11TFContratada_PessoaNom_Sel, lV12TFContratoServicosTelas_ServicoSigla, AV13TFContratoServicosTelas_ServicoSigla_Sel, lV14TFContratoServicosTelas_Tela, AV15TFContratoServicosTelas_Tela_Sel, lV16TFContratoServicosTelas_Link, AV17TFContratoServicosTelas_Link_Sel, lV18TFContratoServicosTelas_Parms, AV19TFContratoServicosTelas_Parms_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKPA2 = false;
            A926ContratoServicosTelas_ContratoCod = P00PA2_A926ContratoServicosTelas_ContratoCod[0];
            A74Contrato_Codigo = P00PA2_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00PA2_n74Contrato_Codigo[0];
            A39Contratada_Codigo = P00PA2_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00PA2_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P00PA2_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00PA2_n41Contratada_PessoaNom[0];
            A932ContratoServicosTelas_Status = P00PA2_A932ContratoServicosTelas_Status[0];
            n932ContratoServicosTelas_Status = P00PA2_n932ContratoServicosTelas_Status[0];
            A929ContratoServicosTelas_Parms = P00PA2_A929ContratoServicosTelas_Parms[0];
            n929ContratoServicosTelas_Parms = P00PA2_n929ContratoServicosTelas_Parms[0];
            A928ContratoServicosTelas_Link = P00PA2_A928ContratoServicosTelas_Link[0];
            A931ContratoServicosTelas_Tela = P00PA2_A931ContratoServicosTelas_Tela[0];
            A937ContratoServicosTelas_ServicoSigla = P00PA2_A937ContratoServicosTelas_ServicoSigla[0];
            n937ContratoServicosTelas_ServicoSigla = P00PA2_n937ContratoServicosTelas_ServicoSigla[0];
            A925ContratoServicosTelas_ServicoCod = P00PA2_A925ContratoServicosTelas_ServicoCod[0];
            n925ContratoServicosTelas_ServicoCod = P00PA2_n925ContratoServicosTelas_ServicoCod[0];
            A938ContratoServicosTelas_Sequencial = P00PA2_A938ContratoServicosTelas_Sequencial[0];
            A74Contrato_Codigo = P00PA2_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00PA2_n74Contrato_Codigo[0];
            A925ContratoServicosTelas_ServicoCod = P00PA2_A925ContratoServicosTelas_ServicoCod[0];
            n925ContratoServicosTelas_ServicoCod = P00PA2_n925ContratoServicosTelas_ServicoCod[0];
            A39Contratada_Codigo = P00PA2_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00PA2_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P00PA2_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00PA2_n41Contratada_PessoaNom[0];
            A937ContratoServicosTelas_ServicoSigla = P00PA2_A937ContratoServicosTelas_ServicoSigla[0];
            n937ContratoServicosTelas_ServicoSigla = P00PA2_n937ContratoServicosTelas_ServicoSigla[0];
            AV34count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00PA2_A41Contratada_PessoaNom[0], A41Contratada_PessoaNom) == 0 ) )
            {
               BRKPA2 = false;
               A926ContratoServicosTelas_ContratoCod = P00PA2_A926ContratoServicosTelas_ContratoCod[0];
               A74Contrato_Codigo = P00PA2_A74Contrato_Codigo[0];
               n74Contrato_Codigo = P00PA2_n74Contrato_Codigo[0];
               A39Contratada_Codigo = P00PA2_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = P00PA2_A40Contratada_PessoaCod[0];
               A938ContratoServicosTelas_Sequencial = P00PA2_A938ContratoServicosTelas_Sequencial[0];
               A74Contrato_Codigo = P00PA2_A74Contrato_Codigo[0];
               n74Contrato_Codigo = P00PA2_n74Contrato_Codigo[0];
               A39Contratada_Codigo = P00PA2_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = P00PA2_A40Contratada_PessoaCod[0];
               AV34count = (long)(AV34count+1);
               BRKPA2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A41Contratada_PessoaNom)) )
            {
               AV26Option = A41Contratada_PessoaNom;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKPA2 )
            {
               BRKPA2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATOSERVICOSTELAS_SERVICOSIGLAOPTIONS' Routine */
         AV12TFContratoServicosTelas_ServicoSigla = AV22SearchTxt;
         AV13TFContratoServicosTelas_ServicoSigla_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A932ContratoServicosTelas_Status ,
                                              AV21TFContratoServicosTelas_Status_Sels ,
                                              AV40DynamicFiltersSelector1 ,
                                              AV41DynamicFiltersOperator1 ,
                                              AV42ContratoServicosTelas_ServicoCod1 ,
                                              AV43DynamicFiltersEnabled2 ,
                                              AV44DynamicFiltersSelector2 ,
                                              AV45DynamicFiltersOperator2 ,
                                              AV46ContratoServicosTelas_ServicoCod2 ,
                                              AV47DynamicFiltersEnabled3 ,
                                              AV48DynamicFiltersSelector3 ,
                                              AV49DynamicFiltersOperator3 ,
                                              AV50ContratoServicosTelas_ServicoCod3 ,
                                              AV11TFContratada_PessoaNom_Sel ,
                                              AV10TFContratada_PessoaNom ,
                                              AV13TFContratoServicosTelas_ServicoSigla_Sel ,
                                              AV12TFContratoServicosTelas_ServicoSigla ,
                                              AV15TFContratoServicosTelas_Tela_Sel ,
                                              AV14TFContratoServicosTelas_Tela ,
                                              AV17TFContratoServicosTelas_Link_Sel ,
                                              AV16TFContratoServicosTelas_Link ,
                                              AV19TFContratoServicosTelas_Parms_Sel ,
                                              AV18TFContratoServicosTelas_Parms ,
                                              AV21TFContratoServicosTelas_Status_Sels.Count ,
                                              A925ContratoServicosTelas_ServicoCod ,
                                              A41Contratada_PessoaNom ,
                                              A937ContratoServicosTelas_ServicoSigla ,
                                              A931ContratoServicosTelas_Tela ,
                                              A928ContratoServicosTelas_Link ,
                                              A929ContratoServicosTelas_Parms },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV10TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV10TFContratada_PessoaNom), 100, "%");
         lV12TFContratoServicosTelas_ServicoSigla = StringUtil.PadR( StringUtil.RTrim( AV12TFContratoServicosTelas_ServicoSigla), 15, "%");
         lV14TFContratoServicosTelas_Tela = StringUtil.PadR( StringUtil.RTrim( AV14TFContratoServicosTelas_Tela), 50, "%");
         lV16TFContratoServicosTelas_Link = StringUtil.Concat( StringUtil.RTrim( AV16TFContratoServicosTelas_Link), "%", "");
         lV18TFContratoServicosTelas_Parms = StringUtil.Concat( StringUtil.RTrim( AV18TFContratoServicosTelas_Parms), "%", "");
         /* Using cursor P00PA3 */
         pr_default.execute(1, new Object[] {AV42ContratoServicosTelas_ServicoCod1, AV42ContratoServicosTelas_ServicoCod1, AV42ContratoServicosTelas_ServicoCod1, AV46ContratoServicosTelas_ServicoCod2, AV46ContratoServicosTelas_ServicoCod2, AV46ContratoServicosTelas_ServicoCod2, AV50ContratoServicosTelas_ServicoCod3, AV50ContratoServicosTelas_ServicoCod3, AV50ContratoServicosTelas_ServicoCod3, lV10TFContratada_PessoaNom, AV11TFContratada_PessoaNom_Sel, lV12TFContratoServicosTelas_ServicoSigla, AV13TFContratoServicosTelas_ServicoSigla_Sel, lV14TFContratoServicosTelas_Tela, AV15TFContratoServicosTelas_Tela_Sel, lV16TFContratoServicosTelas_Link, AV17TFContratoServicosTelas_Link_Sel, lV18TFContratoServicosTelas_Parms, AV19TFContratoServicosTelas_Parms_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKPA4 = false;
            A926ContratoServicosTelas_ContratoCod = P00PA3_A926ContratoServicosTelas_ContratoCod[0];
            A74Contrato_Codigo = P00PA3_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00PA3_n74Contrato_Codigo[0];
            A39Contratada_Codigo = P00PA3_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00PA3_A40Contratada_PessoaCod[0];
            A937ContratoServicosTelas_ServicoSigla = P00PA3_A937ContratoServicosTelas_ServicoSigla[0];
            n937ContratoServicosTelas_ServicoSigla = P00PA3_n937ContratoServicosTelas_ServicoSigla[0];
            A932ContratoServicosTelas_Status = P00PA3_A932ContratoServicosTelas_Status[0];
            n932ContratoServicosTelas_Status = P00PA3_n932ContratoServicosTelas_Status[0];
            A929ContratoServicosTelas_Parms = P00PA3_A929ContratoServicosTelas_Parms[0];
            n929ContratoServicosTelas_Parms = P00PA3_n929ContratoServicosTelas_Parms[0];
            A928ContratoServicosTelas_Link = P00PA3_A928ContratoServicosTelas_Link[0];
            A931ContratoServicosTelas_Tela = P00PA3_A931ContratoServicosTelas_Tela[0];
            A41Contratada_PessoaNom = P00PA3_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00PA3_n41Contratada_PessoaNom[0];
            A925ContratoServicosTelas_ServicoCod = P00PA3_A925ContratoServicosTelas_ServicoCod[0];
            n925ContratoServicosTelas_ServicoCod = P00PA3_n925ContratoServicosTelas_ServicoCod[0];
            A938ContratoServicosTelas_Sequencial = P00PA3_A938ContratoServicosTelas_Sequencial[0];
            A74Contrato_Codigo = P00PA3_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00PA3_n74Contrato_Codigo[0];
            A925ContratoServicosTelas_ServicoCod = P00PA3_A925ContratoServicosTelas_ServicoCod[0];
            n925ContratoServicosTelas_ServicoCod = P00PA3_n925ContratoServicosTelas_ServicoCod[0];
            A39Contratada_Codigo = P00PA3_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00PA3_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P00PA3_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00PA3_n41Contratada_PessoaNom[0];
            A937ContratoServicosTelas_ServicoSigla = P00PA3_A937ContratoServicosTelas_ServicoSigla[0];
            n937ContratoServicosTelas_ServicoSigla = P00PA3_n937ContratoServicosTelas_ServicoSigla[0];
            AV34count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00PA3_A937ContratoServicosTelas_ServicoSigla[0], A937ContratoServicosTelas_ServicoSigla) == 0 ) )
            {
               BRKPA4 = false;
               A926ContratoServicosTelas_ContratoCod = P00PA3_A926ContratoServicosTelas_ContratoCod[0];
               A925ContratoServicosTelas_ServicoCod = P00PA3_A925ContratoServicosTelas_ServicoCod[0];
               n925ContratoServicosTelas_ServicoCod = P00PA3_n925ContratoServicosTelas_ServicoCod[0];
               A938ContratoServicosTelas_Sequencial = P00PA3_A938ContratoServicosTelas_Sequencial[0];
               A925ContratoServicosTelas_ServicoCod = P00PA3_A925ContratoServicosTelas_ServicoCod[0];
               n925ContratoServicosTelas_ServicoCod = P00PA3_n925ContratoServicosTelas_ServicoCod[0];
               AV34count = (long)(AV34count+1);
               BRKPA4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A937ContratoServicosTelas_ServicoSigla)) )
            {
               AV26Option = A937ContratoServicosTelas_ServicoSigla;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKPA4 )
            {
               BRKPA4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTRATOSERVICOSTELAS_TELAOPTIONS' Routine */
         AV14TFContratoServicosTelas_Tela = AV22SearchTxt;
         AV15TFContratoServicosTelas_Tela_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A932ContratoServicosTelas_Status ,
                                              AV21TFContratoServicosTelas_Status_Sels ,
                                              AV40DynamicFiltersSelector1 ,
                                              AV41DynamicFiltersOperator1 ,
                                              AV42ContratoServicosTelas_ServicoCod1 ,
                                              AV43DynamicFiltersEnabled2 ,
                                              AV44DynamicFiltersSelector2 ,
                                              AV45DynamicFiltersOperator2 ,
                                              AV46ContratoServicosTelas_ServicoCod2 ,
                                              AV47DynamicFiltersEnabled3 ,
                                              AV48DynamicFiltersSelector3 ,
                                              AV49DynamicFiltersOperator3 ,
                                              AV50ContratoServicosTelas_ServicoCod3 ,
                                              AV11TFContratada_PessoaNom_Sel ,
                                              AV10TFContratada_PessoaNom ,
                                              AV13TFContratoServicosTelas_ServicoSigla_Sel ,
                                              AV12TFContratoServicosTelas_ServicoSigla ,
                                              AV15TFContratoServicosTelas_Tela_Sel ,
                                              AV14TFContratoServicosTelas_Tela ,
                                              AV17TFContratoServicosTelas_Link_Sel ,
                                              AV16TFContratoServicosTelas_Link ,
                                              AV19TFContratoServicosTelas_Parms_Sel ,
                                              AV18TFContratoServicosTelas_Parms ,
                                              AV21TFContratoServicosTelas_Status_Sels.Count ,
                                              A925ContratoServicosTelas_ServicoCod ,
                                              A41Contratada_PessoaNom ,
                                              A937ContratoServicosTelas_ServicoSigla ,
                                              A931ContratoServicosTelas_Tela ,
                                              A928ContratoServicosTelas_Link ,
                                              A929ContratoServicosTelas_Parms },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV10TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV10TFContratada_PessoaNom), 100, "%");
         lV12TFContratoServicosTelas_ServicoSigla = StringUtil.PadR( StringUtil.RTrim( AV12TFContratoServicosTelas_ServicoSigla), 15, "%");
         lV14TFContratoServicosTelas_Tela = StringUtil.PadR( StringUtil.RTrim( AV14TFContratoServicosTelas_Tela), 50, "%");
         lV16TFContratoServicosTelas_Link = StringUtil.Concat( StringUtil.RTrim( AV16TFContratoServicosTelas_Link), "%", "");
         lV18TFContratoServicosTelas_Parms = StringUtil.Concat( StringUtil.RTrim( AV18TFContratoServicosTelas_Parms), "%", "");
         /* Using cursor P00PA4 */
         pr_default.execute(2, new Object[] {AV42ContratoServicosTelas_ServicoCod1, AV42ContratoServicosTelas_ServicoCod1, AV42ContratoServicosTelas_ServicoCod1, AV46ContratoServicosTelas_ServicoCod2, AV46ContratoServicosTelas_ServicoCod2, AV46ContratoServicosTelas_ServicoCod2, AV50ContratoServicosTelas_ServicoCod3, AV50ContratoServicosTelas_ServicoCod3, AV50ContratoServicosTelas_ServicoCod3, lV10TFContratada_PessoaNom, AV11TFContratada_PessoaNom_Sel, lV12TFContratoServicosTelas_ServicoSigla, AV13TFContratoServicosTelas_ServicoSigla_Sel, lV14TFContratoServicosTelas_Tela, AV15TFContratoServicosTelas_Tela_Sel, lV16TFContratoServicosTelas_Link, AV17TFContratoServicosTelas_Link_Sel, lV18TFContratoServicosTelas_Parms, AV19TFContratoServicosTelas_Parms_Sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKPA6 = false;
            A926ContratoServicosTelas_ContratoCod = P00PA4_A926ContratoServicosTelas_ContratoCod[0];
            A74Contrato_Codigo = P00PA4_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00PA4_n74Contrato_Codigo[0];
            A39Contratada_Codigo = P00PA4_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00PA4_A40Contratada_PessoaCod[0];
            A931ContratoServicosTelas_Tela = P00PA4_A931ContratoServicosTelas_Tela[0];
            A932ContratoServicosTelas_Status = P00PA4_A932ContratoServicosTelas_Status[0];
            n932ContratoServicosTelas_Status = P00PA4_n932ContratoServicosTelas_Status[0];
            A929ContratoServicosTelas_Parms = P00PA4_A929ContratoServicosTelas_Parms[0];
            n929ContratoServicosTelas_Parms = P00PA4_n929ContratoServicosTelas_Parms[0];
            A928ContratoServicosTelas_Link = P00PA4_A928ContratoServicosTelas_Link[0];
            A937ContratoServicosTelas_ServicoSigla = P00PA4_A937ContratoServicosTelas_ServicoSigla[0];
            n937ContratoServicosTelas_ServicoSigla = P00PA4_n937ContratoServicosTelas_ServicoSigla[0];
            A41Contratada_PessoaNom = P00PA4_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00PA4_n41Contratada_PessoaNom[0];
            A925ContratoServicosTelas_ServicoCod = P00PA4_A925ContratoServicosTelas_ServicoCod[0];
            n925ContratoServicosTelas_ServicoCod = P00PA4_n925ContratoServicosTelas_ServicoCod[0];
            A938ContratoServicosTelas_Sequencial = P00PA4_A938ContratoServicosTelas_Sequencial[0];
            A74Contrato_Codigo = P00PA4_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00PA4_n74Contrato_Codigo[0];
            A925ContratoServicosTelas_ServicoCod = P00PA4_A925ContratoServicosTelas_ServicoCod[0];
            n925ContratoServicosTelas_ServicoCod = P00PA4_n925ContratoServicosTelas_ServicoCod[0];
            A39Contratada_Codigo = P00PA4_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00PA4_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P00PA4_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00PA4_n41Contratada_PessoaNom[0];
            A937ContratoServicosTelas_ServicoSigla = P00PA4_A937ContratoServicosTelas_ServicoSigla[0];
            n937ContratoServicosTelas_ServicoSigla = P00PA4_n937ContratoServicosTelas_ServicoSigla[0];
            AV34count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00PA4_A931ContratoServicosTelas_Tela[0], A931ContratoServicosTelas_Tela) == 0 ) )
            {
               BRKPA6 = false;
               A926ContratoServicosTelas_ContratoCod = P00PA4_A926ContratoServicosTelas_ContratoCod[0];
               A938ContratoServicosTelas_Sequencial = P00PA4_A938ContratoServicosTelas_Sequencial[0];
               AV34count = (long)(AV34count+1);
               BRKPA6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A931ContratoServicosTelas_Tela)) )
            {
               AV26Option = A931ContratoServicosTelas_Tela;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKPA6 )
            {
               BRKPA6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADCONTRATOSERVICOSTELAS_LINKOPTIONS' Routine */
         AV16TFContratoServicosTelas_Link = AV22SearchTxt;
         AV17TFContratoServicosTelas_Link_Sel = "";
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              A932ContratoServicosTelas_Status ,
                                              AV21TFContratoServicosTelas_Status_Sels ,
                                              AV40DynamicFiltersSelector1 ,
                                              AV41DynamicFiltersOperator1 ,
                                              AV42ContratoServicosTelas_ServicoCod1 ,
                                              AV43DynamicFiltersEnabled2 ,
                                              AV44DynamicFiltersSelector2 ,
                                              AV45DynamicFiltersOperator2 ,
                                              AV46ContratoServicosTelas_ServicoCod2 ,
                                              AV47DynamicFiltersEnabled3 ,
                                              AV48DynamicFiltersSelector3 ,
                                              AV49DynamicFiltersOperator3 ,
                                              AV50ContratoServicosTelas_ServicoCod3 ,
                                              AV11TFContratada_PessoaNom_Sel ,
                                              AV10TFContratada_PessoaNom ,
                                              AV13TFContratoServicosTelas_ServicoSigla_Sel ,
                                              AV12TFContratoServicosTelas_ServicoSigla ,
                                              AV15TFContratoServicosTelas_Tela_Sel ,
                                              AV14TFContratoServicosTelas_Tela ,
                                              AV17TFContratoServicosTelas_Link_Sel ,
                                              AV16TFContratoServicosTelas_Link ,
                                              AV19TFContratoServicosTelas_Parms_Sel ,
                                              AV18TFContratoServicosTelas_Parms ,
                                              AV21TFContratoServicosTelas_Status_Sels.Count ,
                                              A925ContratoServicosTelas_ServicoCod ,
                                              A41Contratada_PessoaNom ,
                                              A937ContratoServicosTelas_ServicoSigla ,
                                              A931ContratoServicosTelas_Tela ,
                                              A928ContratoServicosTelas_Link ,
                                              A929ContratoServicosTelas_Parms },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV10TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV10TFContratada_PessoaNom), 100, "%");
         lV12TFContratoServicosTelas_ServicoSigla = StringUtil.PadR( StringUtil.RTrim( AV12TFContratoServicosTelas_ServicoSigla), 15, "%");
         lV14TFContratoServicosTelas_Tela = StringUtil.PadR( StringUtil.RTrim( AV14TFContratoServicosTelas_Tela), 50, "%");
         lV16TFContratoServicosTelas_Link = StringUtil.Concat( StringUtil.RTrim( AV16TFContratoServicosTelas_Link), "%", "");
         lV18TFContratoServicosTelas_Parms = StringUtil.Concat( StringUtil.RTrim( AV18TFContratoServicosTelas_Parms), "%", "");
         /* Using cursor P00PA5 */
         pr_default.execute(3, new Object[] {AV42ContratoServicosTelas_ServicoCod1, AV42ContratoServicosTelas_ServicoCod1, AV42ContratoServicosTelas_ServicoCod1, AV46ContratoServicosTelas_ServicoCod2, AV46ContratoServicosTelas_ServicoCod2, AV46ContratoServicosTelas_ServicoCod2, AV50ContratoServicosTelas_ServicoCod3, AV50ContratoServicosTelas_ServicoCod3, AV50ContratoServicosTelas_ServicoCod3, lV10TFContratada_PessoaNom, AV11TFContratada_PessoaNom_Sel, lV12TFContratoServicosTelas_ServicoSigla, AV13TFContratoServicosTelas_ServicoSigla_Sel, lV14TFContratoServicosTelas_Tela, AV15TFContratoServicosTelas_Tela_Sel, lV16TFContratoServicosTelas_Link, AV17TFContratoServicosTelas_Link_Sel, lV18TFContratoServicosTelas_Parms, AV19TFContratoServicosTelas_Parms_Sel});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKPA8 = false;
            A926ContratoServicosTelas_ContratoCod = P00PA5_A926ContratoServicosTelas_ContratoCod[0];
            A74Contrato_Codigo = P00PA5_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00PA5_n74Contrato_Codigo[0];
            A39Contratada_Codigo = P00PA5_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00PA5_A40Contratada_PessoaCod[0];
            A928ContratoServicosTelas_Link = P00PA5_A928ContratoServicosTelas_Link[0];
            A932ContratoServicosTelas_Status = P00PA5_A932ContratoServicosTelas_Status[0];
            n932ContratoServicosTelas_Status = P00PA5_n932ContratoServicosTelas_Status[0];
            A929ContratoServicosTelas_Parms = P00PA5_A929ContratoServicosTelas_Parms[0];
            n929ContratoServicosTelas_Parms = P00PA5_n929ContratoServicosTelas_Parms[0];
            A931ContratoServicosTelas_Tela = P00PA5_A931ContratoServicosTelas_Tela[0];
            A937ContratoServicosTelas_ServicoSigla = P00PA5_A937ContratoServicosTelas_ServicoSigla[0];
            n937ContratoServicosTelas_ServicoSigla = P00PA5_n937ContratoServicosTelas_ServicoSigla[0];
            A41Contratada_PessoaNom = P00PA5_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00PA5_n41Contratada_PessoaNom[0];
            A925ContratoServicosTelas_ServicoCod = P00PA5_A925ContratoServicosTelas_ServicoCod[0];
            n925ContratoServicosTelas_ServicoCod = P00PA5_n925ContratoServicosTelas_ServicoCod[0];
            A938ContratoServicosTelas_Sequencial = P00PA5_A938ContratoServicosTelas_Sequencial[0];
            A74Contrato_Codigo = P00PA5_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00PA5_n74Contrato_Codigo[0];
            A925ContratoServicosTelas_ServicoCod = P00PA5_A925ContratoServicosTelas_ServicoCod[0];
            n925ContratoServicosTelas_ServicoCod = P00PA5_n925ContratoServicosTelas_ServicoCod[0];
            A39Contratada_Codigo = P00PA5_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00PA5_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P00PA5_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00PA5_n41Contratada_PessoaNom[0];
            A937ContratoServicosTelas_ServicoSigla = P00PA5_A937ContratoServicosTelas_ServicoSigla[0];
            n937ContratoServicosTelas_ServicoSigla = P00PA5_n937ContratoServicosTelas_ServicoSigla[0];
            AV34count = 0;
            while ( (pr_default.getStatus(3) != 101) && ( StringUtil.StrCmp(P00PA5_A928ContratoServicosTelas_Link[0], A928ContratoServicosTelas_Link) == 0 ) )
            {
               BRKPA8 = false;
               A926ContratoServicosTelas_ContratoCod = P00PA5_A926ContratoServicosTelas_ContratoCod[0];
               A938ContratoServicosTelas_Sequencial = P00PA5_A938ContratoServicosTelas_Sequencial[0];
               AV34count = (long)(AV34count+1);
               BRKPA8 = true;
               pr_default.readNext(3);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A928ContratoServicosTelas_Link)) )
            {
               AV26Option = A928ContratoServicosTelas_Link;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKPA8 )
            {
               BRKPA8 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      protected void S161( )
      {
         /* 'LOADCONTRATOSERVICOSTELAS_PARMSOPTIONS' Routine */
         AV18TFContratoServicosTelas_Parms = AV22SearchTxt;
         AV19TFContratoServicosTelas_Parms_Sel = "";
         pr_default.dynParam(4, new Object[]{ new Object[]{
                                              A932ContratoServicosTelas_Status ,
                                              AV21TFContratoServicosTelas_Status_Sels ,
                                              AV40DynamicFiltersSelector1 ,
                                              AV41DynamicFiltersOperator1 ,
                                              AV42ContratoServicosTelas_ServicoCod1 ,
                                              AV43DynamicFiltersEnabled2 ,
                                              AV44DynamicFiltersSelector2 ,
                                              AV45DynamicFiltersOperator2 ,
                                              AV46ContratoServicosTelas_ServicoCod2 ,
                                              AV47DynamicFiltersEnabled3 ,
                                              AV48DynamicFiltersSelector3 ,
                                              AV49DynamicFiltersOperator3 ,
                                              AV50ContratoServicosTelas_ServicoCod3 ,
                                              AV11TFContratada_PessoaNom_Sel ,
                                              AV10TFContratada_PessoaNom ,
                                              AV13TFContratoServicosTelas_ServicoSigla_Sel ,
                                              AV12TFContratoServicosTelas_ServicoSigla ,
                                              AV15TFContratoServicosTelas_Tela_Sel ,
                                              AV14TFContratoServicosTelas_Tela ,
                                              AV17TFContratoServicosTelas_Link_Sel ,
                                              AV16TFContratoServicosTelas_Link ,
                                              AV19TFContratoServicosTelas_Parms_Sel ,
                                              AV18TFContratoServicosTelas_Parms ,
                                              AV21TFContratoServicosTelas_Status_Sels.Count ,
                                              A925ContratoServicosTelas_ServicoCod ,
                                              A41Contratada_PessoaNom ,
                                              A937ContratoServicosTelas_ServicoSigla ,
                                              A931ContratoServicosTelas_Tela ,
                                              A928ContratoServicosTelas_Link ,
                                              A929ContratoServicosTelas_Parms },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV10TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV10TFContratada_PessoaNom), 100, "%");
         lV12TFContratoServicosTelas_ServicoSigla = StringUtil.PadR( StringUtil.RTrim( AV12TFContratoServicosTelas_ServicoSigla), 15, "%");
         lV14TFContratoServicosTelas_Tela = StringUtil.PadR( StringUtil.RTrim( AV14TFContratoServicosTelas_Tela), 50, "%");
         lV16TFContratoServicosTelas_Link = StringUtil.Concat( StringUtil.RTrim( AV16TFContratoServicosTelas_Link), "%", "");
         lV18TFContratoServicosTelas_Parms = StringUtil.Concat( StringUtil.RTrim( AV18TFContratoServicosTelas_Parms), "%", "");
         /* Using cursor P00PA6 */
         pr_default.execute(4, new Object[] {AV42ContratoServicosTelas_ServicoCod1, AV42ContratoServicosTelas_ServicoCod1, AV42ContratoServicosTelas_ServicoCod1, AV46ContratoServicosTelas_ServicoCod2, AV46ContratoServicosTelas_ServicoCod2, AV46ContratoServicosTelas_ServicoCod2, AV50ContratoServicosTelas_ServicoCod3, AV50ContratoServicosTelas_ServicoCod3, AV50ContratoServicosTelas_ServicoCod3, lV10TFContratada_PessoaNom, AV11TFContratada_PessoaNom_Sel, lV12TFContratoServicosTelas_ServicoSigla, AV13TFContratoServicosTelas_ServicoSigla_Sel, lV14TFContratoServicosTelas_Tela, AV15TFContratoServicosTelas_Tela_Sel, lV16TFContratoServicosTelas_Link, AV17TFContratoServicosTelas_Link_Sel, lV18TFContratoServicosTelas_Parms, AV19TFContratoServicosTelas_Parms_Sel});
         while ( (pr_default.getStatus(4) != 101) )
         {
            BRKPA10 = false;
            A926ContratoServicosTelas_ContratoCod = P00PA6_A926ContratoServicosTelas_ContratoCod[0];
            A74Contrato_Codigo = P00PA6_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00PA6_n74Contrato_Codigo[0];
            A39Contratada_Codigo = P00PA6_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00PA6_A40Contratada_PessoaCod[0];
            A929ContratoServicosTelas_Parms = P00PA6_A929ContratoServicosTelas_Parms[0];
            n929ContratoServicosTelas_Parms = P00PA6_n929ContratoServicosTelas_Parms[0];
            A932ContratoServicosTelas_Status = P00PA6_A932ContratoServicosTelas_Status[0];
            n932ContratoServicosTelas_Status = P00PA6_n932ContratoServicosTelas_Status[0];
            A928ContratoServicosTelas_Link = P00PA6_A928ContratoServicosTelas_Link[0];
            A931ContratoServicosTelas_Tela = P00PA6_A931ContratoServicosTelas_Tela[0];
            A937ContratoServicosTelas_ServicoSigla = P00PA6_A937ContratoServicosTelas_ServicoSigla[0];
            n937ContratoServicosTelas_ServicoSigla = P00PA6_n937ContratoServicosTelas_ServicoSigla[0];
            A41Contratada_PessoaNom = P00PA6_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00PA6_n41Contratada_PessoaNom[0];
            A925ContratoServicosTelas_ServicoCod = P00PA6_A925ContratoServicosTelas_ServicoCod[0];
            n925ContratoServicosTelas_ServicoCod = P00PA6_n925ContratoServicosTelas_ServicoCod[0];
            A938ContratoServicosTelas_Sequencial = P00PA6_A938ContratoServicosTelas_Sequencial[0];
            A74Contrato_Codigo = P00PA6_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00PA6_n74Contrato_Codigo[0];
            A925ContratoServicosTelas_ServicoCod = P00PA6_A925ContratoServicosTelas_ServicoCod[0];
            n925ContratoServicosTelas_ServicoCod = P00PA6_n925ContratoServicosTelas_ServicoCod[0];
            A39Contratada_Codigo = P00PA6_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00PA6_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P00PA6_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00PA6_n41Contratada_PessoaNom[0];
            A937ContratoServicosTelas_ServicoSigla = P00PA6_A937ContratoServicosTelas_ServicoSigla[0];
            n937ContratoServicosTelas_ServicoSigla = P00PA6_n937ContratoServicosTelas_ServicoSigla[0];
            AV34count = 0;
            while ( (pr_default.getStatus(4) != 101) && ( StringUtil.StrCmp(P00PA6_A929ContratoServicosTelas_Parms[0], A929ContratoServicosTelas_Parms) == 0 ) )
            {
               BRKPA10 = false;
               A926ContratoServicosTelas_ContratoCod = P00PA6_A926ContratoServicosTelas_ContratoCod[0];
               A938ContratoServicosTelas_Sequencial = P00PA6_A938ContratoServicosTelas_Sequencial[0];
               AV34count = (long)(AV34count+1);
               BRKPA10 = true;
               pr_default.readNext(4);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A929ContratoServicosTelas_Parms)) )
            {
               AV26Option = A929ContratoServicosTelas_Parms;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKPA10 )
            {
               BRKPA10 = true;
               pr_default.readNext(4);
            }
         }
         pr_default.close(4);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV27Options = new GxSimpleCollection();
         AV30OptionsDesc = new GxSimpleCollection();
         AV32OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV35Session = context.GetSession();
         AV37GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV38GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContratada_PessoaNom = "";
         AV11TFContratada_PessoaNom_Sel = "";
         AV12TFContratoServicosTelas_ServicoSigla = "";
         AV13TFContratoServicosTelas_ServicoSigla_Sel = "";
         AV14TFContratoServicosTelas_Tela = "";
         AV15TFContratoServicosTelas_Tela_Sel = "";
         AV16TFContratoServicosTelas_Link = "";
         AV17TFContratoServicosTelas_Link_Sel = "";
         AV18TFContratoServicosTelas_Parms = "";
         AV19TFContratoServicosTelas_Parms_Sel = "";
         AV20TFContratoServicosTelas_Status_SelsJson = "";
         AV21TFContratoServicosTelas_Status_Sels = new GxSimpleCollection();
         AV39GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV40DynamicFiltersSelector1 = "";
         AV44DynamicFiltersSelector2 = "";
         AV48DynamicFiltersSelector3 = "";
         scmdbuf = "";
         lV10TFContratada_PessoaNom = "";
         lV12TFContratoServicosTelas_ServicoSigla = "";
         lV14TFContratoServicosTelas_Tela = "";
         lV16TFContratoServicosTelas_Link = "";
         lV18TFContratoServicosTelas_Parms = "";
         A932ContratoServicosTelas_Status = "";
         A41Contratada_PessoaNom = "";
         A937ContratoServicosTelas_ServicoSigla = "";
         A931ContratoServicosTelas_Tela = "";
         A928ContratoServicosTelas_Link = "";
         A929ContratoServicosTelas_Parms = "";
         P00PA2_A926ContratoServicosTelas_ContratoCod = new int[1] ;
         P00PA2_A74Contrato_Codigo = new int[1] ;
         P00PA2_n74Contrato_Codigo = new bool[] {false} ;
         P00PA2_A39Contratada_Codigo = new int[1] ;
         P00PA2_A40Contratada_PessoaCod = new int[1] ;
         P00PA2_A41Contratada_PessoaNom = new String[] {""} ;
         P00PA2_n41Contratada_PessoaNom = new bool[] {false} ;
         P00PA2_A932ContratoServicosTelas_Status = new String[] {""} ;
         P00PA2_n932ContratoServicosTelas_Status = new bool[] {false} ;
         P00PA2_A929ContratoServicosTelas_Parms = new String[] {""} ;
         P00PA2_n929ContratoServicosTelas_Parms = new bool[] {false} ;
         P00PA2_A928ContratoServicosTelas_Link = new String[] {""} ;
         P00PA2_A931ContratoServicosTelas_Tela = new String[] {""} ;
         P00PA2_A937ContratoServicosTelas_ServicoSigla = new String[] {""} ;
         P00PA2_n937ContratoServicosTelas_ServicoSigla = new bool[] {false} ;
         P00PA2_A925ContratoServicosTelas_ServicoCod = new int[1] ;
         P00PA2_n925ContratoServicosTelas_ServicoCod = new bool[] {false} ;
         P00PA2_A938ContratoServicosTelas_Sequencial = new short[1] ;
         AV26Option = "";
         P00PA3_A926ContratoServicosTelas_ContratoCod = new int[1] ;
         P00PA3_A74Contrato_Codigo = new int[1] ;
         P00PA3_n74Contrato_Codigo = new bool[] {false} ;
         P00PA3_A39Contratada_Codigo = new int[1] ;
         P00PA3_A40Contratada_PessoaCod = new int[1] ;
         P00PA3_A937ContratoServicosTelas_ServicoSigla = new String[] {""} ;
         P00PA3_n937ContratoServicosTelas_ServicoSigla = new bool[] {false} ;
         P00PA3_A932ContratoServicosTelas_Status = new String[] {""} ;
         P00PA3_n932ContratoServicosTelas_Status = new bool[] {false} ;
         P00PA3_A929ContratoServicosTelas_Parms = new String[] {""} ;
         P00PA3_n929ContratoServicosTelas_Parms = new bool[] {false} ;
         P00PA3_A928ContratoServicosTelas_Link = new String[] {""} ;
         P00PA3_A931ContratoServicosTelas_Tela = new String[] {""} ;
         P00PA3_A41Contratada_PessoaNom = new String[] {""} ;
         P00PA3_n41Contratada_PessoaNom = new bool[] {false} ;
         P00PA3_A925ContratoServicosTelas_ServicoCod = new int[1] ;
         P00PA3_n925ContratoServicosTelas_ServicoCod = new bool[] {false} ;
         P00PA3_A938ContratoServicosTelas_Sequencial = new short[1] ;
         P00PA4_A926ContratoServicosTelas_ContratoCod = new int[1] ;
         P00PA4_A74Contrato_Codigo = new int[1] ;
         P00PA4_n74Contrato_Codigo = new bool[] {false} ;
         P00PA4_A39Contratada_Codigo = new int[1] ;
         P00PA4_A40Contratada_PessoaCod = new int[1] ;
         P00PA4_A931ContratoServicosTelas_Tela = new String[] {""} ;
         P00PA4_A932ContratoServicosTelas_Status = new String[] {""} ;
         P00PA4_n932ContratoServicosTelas_Status = new bool[] {false} ;
         P00PA4_A929ContratoServicosTelas_Parms = new String[] {""} ;
         P00PA4_n929ContratoServicosTelas_Parms = new bool[] {false} ;
         P00PA4_A928ContratoServicosTelas_Link = new String[] {""} ;
         P00PA4_A937ContratoServicosTelas_ServicoSigla = new String[] {""} ;
         P00PA4_n937ContratoServicosTelas_ServicoSigla = new bool[] {false} ;
         P00PA4_A41Contratada_PessoaNom = new String[] {""} ;
         P00PA4_n41Contratada_PessoaNom = new bool[] {false} ;
         P00PA4_A925ContratoServicosTelas_ServicoCod = new int[1] ;
         P00PA4_n925ContratoServicosTelas_ServicoCod = new bool[] {false} ;
         P00PA4_A938ContratoServicosTelas_Sequencial = new short[1] ;
         P00PA5_A926ContratoServicosTelas_ContratoCod = new int[1] ;
         P00PA5_A74Contrato_Codigo = new int[1] ;
         P00PA5_n74Contrato_Codigo = new bool[] {false} ;
         P00PA5_A39Contratada_Codigo = new int[1] ;
         P00PA5_A40Contratada_PessoaCod = new int[1] ;
         P00PA5_A928ContratoServicosTelas_Link = new String[] {""} ;
         P00PA5_A932ContratoServicosTelas_Status = new String[] {""} ;
         P00PA5_n932ContratoServicosTelas_Status = new bool[] {false} ;
         P00PA5_A929ContratoServicosTelas_Parms = new String[] {""} ;
         P00PA5_n929ContratoServicosTelas_Parms = new bool[] {false} ;
         P00PA5_A931ContratoServicosTelas_Tela = new String[] {""} ;
         P00PA5_A937ContratoServicosTelas_ServicoSigla = new String[] {""} ;
         P00PA5_n937ContratoServicosTelas_ServicoSigla = new bool[] {false} ;
         P00PA5_A41Contratada_PessoaNom = new String[] {""} ;
         P00PA5_n41Contratada_PessoaNom = new bool[] {false} ;
         P00PA5_A925ContratoServicosTelas_ServicoCod = new int[1] ;
         P00PA5_n925ContratoServicosTelas_ServicoCod = new bool[] {false} ;
         P00PA5_A938ContratoServicosTelas_Sequencial = new short[1] ;
         P00PA6_A926ContratoServicosTelas_ContratoCod = new int[1] ;
         P00PA6_A74Contrato_Codigo = new int[1] ;
         P00PA6_n74Contrato_Codigo = new bool[] {false} ;
         P00PA6_A39Contratada_Codigo = new int[1] ;
         P00PA6_A40Contratada_PessoaCod = new int[1] ;
         P00PA6_A929ContratoServicosTelas_Parms = new String[] {""} ;
         P00PA6_n929ContratoServicosTelas_Parms = new bool[] {false} ;
         P00PA6_A932ContratoServicosTelas_Status = new String[] {""} ;
         P00PA6_n932ContratoServicosTelas_Status = new bool[] {false} ;
         P00PA6_A928ContratoServicosTelas_Link = new String[] {""} ;
         P00PA6_A931ContratoServicosTelas_Tela = new String[] {""} ;
         P00PA6_A937ContratoServicosTelas_ServicoSigla = new String[] {""} ;
         P00PA6_n937ContratoServicosTelas_ServicoSigla = new bool[] {false} ;
         P00PA6_A41Contratada_PessoaNom = new String[] {""} ;
         P00PA6_n41Contratada_PessoaNom = new bool[] {false} ;
         P00PA6_A925ContratoServicosTelas_ServicoCod = new int[1] ;
         P00PA6_n925ContratoServicosTelas_ServicoCod = new bool[] {false} ;
         P00PA6_A938ContratoServicosTelas_Sequencial = new short[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptcontratoservicostelasfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00PA2_A926ContratoServicosTelas_ContratoCod, P00PA2_A74Contrato_Codigo, P00PA2_n74Contrato_Codigo, P00PA2_A39Contratada_Codigo, P00PA2_A40Contratada_PessoaCod, P00PA2_A41Contratada_PessoaNom, P00PA2_n41Contratada_PessoaNom, P00PA2_A932ContratoServicosTelas_Status, P00PA2_n932ContratoServicosTelas_Status, P00PA2_A929ContratoServicosTelas_Parms,
               P00PA2_n929ContratoServicosTelas_Parms, P00PA2_A928ContratoServicosTelas_Link, P00PA2_A931ContratoServicosTelas_Tela, P00PA2_A937ContratoServicosTelas_ServicoSigla, P00PA2_n937ContratoServicosTelas_ServicoSigla, P00PA2_A925ContratoServicosTelas_ServicoCod, P00PA2_n925ContratoServicosTelas_ServicoCod, P00PA2_A938ContratoServicosTelas_Sequencial
               }
               , new Object[] {
               P00PA3_A926ContratoServicosTelas_ContratoCod, P00PA3_A74Contrato_Codigo, P00PA3_n74Contrato_Codigo, P00PA3_A39Contratada_Codigo, P00PA3_A40Contratada_PessoaCod, P00PA3_A937ContratoServicosTelas_ServicoSigla, P00PA3_n937ContratoServicosTelas_ServicoSigla, P00PA3_A932ContratoServicosTelas_Status, P00PA3_n932ContratoServicosTelas_Status, P00PA3_A929ContratoServicosTelas_Parms,
               P00PA3_n929ContratoServicosTelas_Parms, P00PA3_A928ContratoServicosTelas_Link, P00PA3_A931ContratoServicosTelas_Tela, P00PA3_A41Contratada_PessoaNom, P00PA3_n41Contratada_PessoaNom, P00PA3_A925ContratoServicosTelas_ServicoCod, P00PA3_n925ContratoServicosTelas_ServicoCod, P00PA3_A938ContratoServicosTelas_Sequencial
               }
               , new Object[] {
               P00PA4_A926ContratoServicosTelas_ContratoCod, P00PA4_A74Contrato_Codigo, P00PA4_n74Contrato_Codigo, P00PA4_A39Contratada_Codigo, P00PA4_A40Contratada_PessoaCod, P00PA4_A931ContratoServicosTelas_Tela, P00PA4_A932ContratoServicosTelas_Status, P00PA4_n932ContratoServicosTelas_Status, P00PA4_A929ContratoServicosTelas_Parms, P00PA4_n929ContratoServicosTelas_Parms,
               P00PA4_A928ContratoServicosTelas_Link, P00PA4_A937ContratoServicosTelas_ServicoSigla, P00PA4_n937ContratoServicosTelas_ServicoSigla, P00PA4_A41Contratada_PessoaNom, P00PA4_n41Contratada_PessoaNom, P00PA4_A925ContratoServicosTelas_ServicoCod, P00PA4_n925ContratoServicosTelas_ServicoCod, P00PA4_A938ContratoServicosTelas_Sequencial
               }
               , new Object[] {
               P00PA5_A926ContratoServicosTelas_ContratoCod, P00PA5_A74Contrato_Codigo, P00PA5_n74Contrato_Codigo, P00PA5_A39Contratada_Codigo, P00PA5_A40Contratada_PessoaCod, P00PA5_A928ContratoServicosTelas_Link, P00PA5_A932ContratoServicosTelas_Status, P00PA5_n932ContratoServicosTelas_Status, P00PA5_A929ContratoServicosTelas_Parms, P00PA5_n929ContratoServicosTelas_Parms,
               P00PA5_A931ContratoServicosTelas_Tela, P00PA5_A937ContratoServicosTelas_ServicoSigla, P00PA5_n937ContratoServicosTelas_ServicoSigla, P00PA5_A41Contratada_PessoaNom, P00PA5_n41Contratada_PessoaNom, P00PA5_A925ContratoServicosTelas_ServicoCod, P00PA5_n925ContratoServicosTelas_ServicoCod, P00PA5_A938ContratoServicosTelas_Sequencial
               }
               , new Object[] {
               P00PA6_A926ContratoServicosTelas_ContratoCod, P00PA6_A74Contrato_Codigo, P00PA6_n74Contrato_Codigo, P00PA6_A39Contratada_Codigo, P00PA6_A40Contratada_PessoaCod, P00PA6_A929ContratoServicosTelas_Parms, P00PA6_n929ContratoServicosTelas_Parms, P00PA6_A932ContratoServicosTelas_Status, P00PA6_n932ContratoServicosTelas_Status, P00PA6_A928ContratoServicosTelas_Link,
               P00PA6_A931ContratoServicosTelas_Tela, P00PA6_A937ContratoServicosTelas_ServicoSigla, P00PA6_n937ContratoServicosTelas_ServicoSigla, P00PA6_A41Contratada_PessoaNom, P00PA6_n41Contratada_PessoaNom, P00PA6_A925ContratoServicosTelas_ServicoCod, P00PA6_n925ContratoServicosTelas_ServicoCod, P00PA6_A938ContratoServicosTelas_Sequencial
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV41DynamicFiltersOperator1 ;
      private short AV45DynamicFiltersOperator2 ;
      private short AV49DynamicFiltersOperator3 ;
      private short A938ContratoServicosTelas_Sequencial ;
      private int AV53GXV1 ;
      private int AV42ContratoServicosTelas_ServicoCod1 ;
      private int AV46ContratoServicosTelas_ServicoCod2 ;
      private int AV50ContratoServicosTelas_ServicoCod3 ;
      private int AV21TFContratoServicosTelas_Status_Sels_Count ;
      private int A925ContratoServicosTelas_ServicoCod ;
      private int A926ContratoServicosTelas_ContratoCod ;
      private int A74Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private long AV34count ;
      private String AV10TFContratada_PessoaNom ;
      private String AV11TFContratada_PessoaNom_Sel ;
      private String AV12TFContratoServicosTelas_ServicoSigla ;
      private String AV13TFContratoServicosTelas_ServicoSigla_Sel ;
      private String AV14TFContratoServicosTelas_Tela ;
      private String AV15TFContratoServicosTelas_Tela_Sel ;
      private String scmdbuf ;
      private String lV10TFContratada_PessoaNom ;
      private String lV12TFContratoServicosTelas_ServicoSigla ;
      private String lV14TFContratoServicosTelas_Tela ;
      private String A932ContratoServicosTelas_Status ;
      private String A41Contratada_PessoaNom ;
      private String A937ContratoServicosTelas_ServicoSigla ;
      private String A931ContratoServicosTelas_Tela ;
      private bool returnInSub ;
      private bool AV43DynamicFiltersEnabled2 ;
      private bool AV47DynamicFiltersEnabled3 ;
      private bool BRKPA2 ;
      private bool n74Contrato_Codigo ;
      private bool n41Contratada_PessoaNom ;
      private bool n932ContratoServicosTelas_Status ;
      private bool n929ContratoServicosTelas_Parms ;
      private bool n937ContratoServicosTelas_ServicoSigla ;
      private bool n925ContratoServicosTelas_ServicoCod ;
      private bool BRKPA4 ;
      private bool BRKPA6 ;
      private bool BRKPA8 ;
      private bool BRKPA10 ;
      private String AV33OptionIndexesJson ;
      private String AV28OptionsJson ;
      private String AV31OptionsDescJson ;
      private String AV20TFContratoServicosTelas_Status_SelsJson ;
      private String A929ContratoServicosTelas_Parms ;
      private String AV24DDOName ;
      private String AV22SearchTxt ;
      private String AV23SearchTxtTo ;
      private String AV16TFContratoServicosTelas_Link ;
      private String AV17TFContratoServicosTelas_Link_Sel ;
      private String AV18TFContratoServicosTelas_Parms ;
      private String AV19TFContratoServicosTelas_Parms_Sel ;
      private String AV40DynamicFiltersSelector1 ;
      private String AV44DynamicFiltersSelector2 ;
      private String AV48DynamicFiltersSelector3 ;
      private String lV16TFContratoServicosTelas_Link ;
      private String lV18TFContratoServicosTelas_Parms ;
      private String A928ContratoServicosTelas_Link ;
      private String AV26Option ;
      private IGxSession AV35Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00PA2_A926ContratoServicosTelas_ContratoCod ;
      private int[] P00PA2_A74Contrato_Codigo ;
      private bool[] P00PA2_n74Contrato_Codigo ;
      private int[] P00PA2_A39Contratada_Codigo ;
      private int[] P00PA2_A40Contratada_PessoaCod ;
      private String[] P00PA2_A41Contratada_PessoaNom ;
      private bool[] P00PA2_n41Contratada_PessoaNom ;
      private String[] P00PA2_A932ContratoServicosTelas_Status ;
      private bool[] P00PA2_n932ContratoServicosTelas_Status ;
      private String[] P00PA2_A929ContratoServicosTelas_Parms ;
      private bool[] P00PA2_n929ContratoServicosTelas_Parms ;
      private String[] P00PA2_A928ContratoServicosTelas_Link ;
      private String[] P00PA2_A931ContratoServicosTelas_Tela ;
      private String[] P00PA2_A937ContratoServicosTelas_ServicoSigla ;
      private bool[] P00PA2_n937ContratoServicosTelas_ServicoSigla ;
      private int[] P00PA2_A925ContratoServicosTelas_ServicoCod ;
      private bool[] P00PA2_n925ContratoServicosTelas_ServicoCod ;
      private short[] P00PA2_A938ContratoServicosTelas_Sequencial ;
      private int[] P00PA3_A926ContratoServicosTelas_ContratoCod ;
      private int[] P00PA3_A74Contrato_Codigo ;
      private bool[] P00PA3_n74Contrato_Codigo ;
      private int[] P00PA3_A39Contratada_Codigo ;
      private int[] P00PA3_A40Contratada_PessoaCod ;
      private String[] P00PA3_A937ContratoServicosTelas_ServicoSigla ;
      private bool[] P00PA3_n937ContratoServicosTelas_ServicoSigla ;
      private String[] P00PA3_A932ContratoServicosTelas_Status ;
      private bool[] P00PA3_n932ContratoServicosTelas_Status ;
      private String[] P00PA3_A929ContratoServicosTelas_Parms ;
      private bool[] P00PA3_n929ContratoServicosTelas_Parms ;
      private String[] P00PA3_A928ContratoServicosTelas_Link ;
      private String[] P00PA3_A931ContratoServicosTelas_Tela ;
      private String[] P00PA3_A41Contratada_PessoaNom ;
      private bool[] P00PA3_n41Contratada_PessoaNom ;
      private int[] P00PA3_A925ContratoServicosTelas_ServicoCod ;
      private bool[] P00PA3_n925ContratoServicosTelas_ServicoCod ;
      private short[] P00PA3_A938ContratoServicosTelas_Sequencial ;
      private int[] P00PA4_A926ContratoServicosTelas_ContratoCod ;
      private int[] P00PA4_A74Contrato_Codigo ;
      private bool[] P00PA4_n74Contrato_Codigo ;
      private int[] P00PA4_A39Contratada_Codigo ;
      private int[] P00PA4_A40Contratada_PessoaCod ;
      private String[] P00PA4_A931ContratoServicosTelas_Tela ;
      private String[] P00PA4_A932ContratoServicosTelas_Status ;
      private bool[] P00PA4_n932ContratoServicosTelas_Status ;
      private String[] P00PA4_A929ContratoServicosTelas_Parms ;
      private bool[] P00PA4_n929ContratoServicosTelas_Parms ;
      private String[] P00PA4_A928ContratoServicosTelas_Link ;
      private String[] P00PA4_A937ContratoServicosTelas_ServicoSigla ;
      private bool[] P00PA4_n937ContratoServicosTelas_ServicoSigla ;
      private String[] P00PA4_A41Contratada_PessoaNom ;
      private bool[] P00PA4_n41Contratada_PessoaNom ;
      private int[] P00PA4_A925ContratoServicosTelas_ServicoCod ;
      private bool[] P00PA4_n925ContratoServicosTelas_ServicoCod ;
      private short[] P00PA4_A938ContratoServicosTelas_Sequencial ;
      private int[] P00PA5_A926ContratoServicosTelas_ContratoCod ;
      private int[] P00PA5_A74Contrato_Codigo ;
      private bool[] P00PA5_n74Contrato_Codigo ;
      private int[] P00PA5_A39Contratada_Codigo ;
      private int[] P00PA5_A40Contratada_PessoaCod ;
      private String[] P00PA5_A928ContratoServicosTelas_Link ;
      private String[] P00PA5_A932ContratoServicosTelas_Status ;
      private bool[] P00PA5_n932ContratoServicosTelas_Status ;
      private String[] P00PA5_A929ContratoServicosTelas_Parms ;
      private bool[] P00PA5_n929ContratoServicosTelas_Parms ;
      private String[] P00PA5_A931ContratoServicosTelas_Tela ;
      private String[] P00PA5_A937ContratoServicosTelas_ServicoSigla ;
      private bool[] P00PA5_n937ContratoServicosTelas_ServicoSigla ;
      private String[] P00PA5_A41Contratada_PessoaNom ;
      private bool[] P00PA5_n41Contratada_PessoaNom ;
      private int[] P00PA5_A925ContratoServicosTelas_ServicoCod ;
      private bool[] P00PA5_n925ContratoServicosTelas_ServicoCod ;
      private short[] P00PA5_A938ContratoServicosTelas_Sequencial ;
      private int[] P00PA6_A926ContratoServicosTelas_ContratoCod ;
      private int[] P00PA6_A74Contrato_Codigo ;
      private bool[] P00PA6_n74Contrato_Codigo ;
      private int[] P00PA6_A39Contratada_Codigo ;
      private int[] P00PA6_A40Contratada_PessoaCod ;
      private String[] P00PA6_A929ContratoServicosTelas_Parms ;
      private bool[] P00PA6_n929ContratoServicosTelas_Parms ;
      private String[] P00PA6_A932ContratoServicosTelas_Status ;
      private bool[] P00PA6_n932ContratoServicosTelas_Status ;
      private String[] P00PA6_A928ContratoServicosTelas_Link ;
      private String[] P00PA6_A931ContratoServicosTelas_Tela ;
      private String[] P00PA6_A937ContratoServicosTelas_ServicoSigla ;
      private bool[] P00PA6_n937ContratoServicosTelas_ServicoSigla ;
      private String[] P00PA6_A41Contratada_PessoaNom ;
      private bool[] P00PA6_n41Contratada_PessoaNom ;
      private int[] P00PA6_A925ContratoServicosTelas_ServicoCod ;
      private bool[] P00PA6_n925ContratoServicosTelas_ServicoCod ;
      private short[] P00PA6_A938ContratoServicosTelas_Sequencial ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21TFContratoServicosTelas_Status_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV27Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV37GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV38GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV39GridStateDynamicFilter ;
   }

   public class getpromptcontratoservicostelasfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00PA2( IGxContext context ,
                                             String A932ContratoServicosTelas_Status ,
                                             IGxCollection AV21TFContratoServicosTelas_Status_Sels ,
                                             String AV40DynamicFiltersSelector1 ,
                                             short AV41DynamicFiltersOperator1 ,
                                             int AV42ContratoServicosTelas_ServicoCod1 ,
                                             bool AV43DynamicFiltersEnabled2 ,
                                             String AV44DynamicFiltersSelector2 ,
                                             short AV45DynamicFiltersOperator2 ,
                                             int AV46ContratoServicosTelas_ServicoCod2 ,
                                             bool AV47DynamicFiltersEnabled3 ,
                                             String AV48DynamicFiltersSelector3 ,
                                             short AV49DynamicFiltersOperator3 ,
                                             int AV50ContratoServicosTelas_ServicoCod3 ,
                                             String AV11TFContratada_PessoaNom_Sel ,
                                             String AV10TFContratada_PessoaNom ,
                                             String AV13TFContratoServicosTelas_ServicoSigla_Sel ,
                                             String AV12TFContratoServicosTelas_ServicoSigla ,
                                             String AV15TFContratoServicosTelas_Tela_Sel ,
                                             String AV14TFContratoServicosTelas_Tela ,
                                             String AV17TFContratoServicosTelas_Link_Sel ,
                                             String AV16TFContratoServicosTelas_Link ,
                                             String AV19TFContratoServicosTelas_Parms_Sel ,
                                             String AV18TFContratoServicosTelas_Parms ,
                                             int AV21TFContratoServicosTelas_Status_Sels_Count ,
                                             int A925ContratoServicosTelas_ServicoCod ,
                                             String A41Contratada_PessoaNom ,
                                             String A937ContratoServicosTelas_ServicoSigla ,
                                             String A931ContratoServicosTelas_Tela ,
                                             String A928ContratoServicosTelas_Link ,
                                             String A929ContratoServicosTelas_Parms )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [19] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoServicosTelas_ContratoCod] AS ContratoServicosTelas_ContratoCod, T2.[Contrato_Codigo], T3.[Contratada_Codigo], T4.[Contratada_PessoaCod] AS Contratada_PessoaCod, T5.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[ContratoServicosTelas_Status], T1.[ContratoServicosTelas_Parms], T1.[ContratoServicosTelas_Link], T1.[ContratoServicosTelas_Tela], T6.[Servico_Sigla] AS ContratoServicosTelas_ServicoSigla, T2.[Servico_Codigo] AS ContratoServicosTelas_ServicoCod, T1.[ContratoServicosTelas_Sequencial] FROM ((((([ContratoServicosTelas] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosTelas_ContratoCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T3.[Contratada_Codigo]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratada_PessoaCod]) LEFT JOIN [Servico] T6 WITH (NOLOCK) ON T6.[Servico_Codigo] = T2.[Servico_Codigo])";
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV41DynamicFiltersOperator1 == 0 ) && ( ! (0==AV42ContratoServicosTelas_ServicoCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV42ContratoServicosTelas_ServicoCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV42ContratoServicosTelas_ServicoCod1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV41DynamicFiltersOperator1 == 1 ) && ( ! (0==AV42ContratoServicosTelas_ServicoCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV42ContratoServicosTelas_ServicoCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV42ContratoServicosTelas_ServicoCod1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV41DynamicFiltersOperator1 == 2 ) && ( ! (0==AV42ContratoServicosTelas_ServicoCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV42ContratoServicosTelas_ServicoCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV42ContratoServicosTelas_ServicoCod1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV45DynamicFiltersOperator2 == 0 ) && ( ! (0==AV46ContratoServicosTelas_ServicoCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV46ContratoServicosTelas_ServicoCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV46ContratoServicosTelas_ServicoCod2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV45DynamicFiltersOperator2 == 1 ) && ( ! (0==AV46ContratoServicosTelas_ServicoCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV46ContratoServicosTelas_ServicoCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV46ContratoServicosTelas_ServicoCod2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV45DynamicFiltersOperator2 == 2 ) && ( ! (0==AV46ContratoServicosTelas_ServicoCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV46ContratoServicosTelas_ServicoCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV46ContratoServicosTelas_ServicoCod2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV47DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV48DynamicFiltersSelector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV49DynamicFiltersOperator3 == 0 ) && ( ! (0==AV50ContratoServicosTelas_ServicoCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV50ContratoServicosTelas_ServicoCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV50ContratoServicosTelas_ServicoCod3)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV47DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV48DynamicFiltersSelector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV49DynamicFiltersOperator3 == 1 ) && ( ! (0==AV50ContratoServicosTelas_ServicoCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV50ContratoServicosTelas_ServicoCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV50ContratoServicosTelas_ServicoCod3)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV47DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV48DynamicFiltersSelector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV49DynamicFiltersOperator3 == 2 ) && ( ! (0==AV50ContratoServicosTelas_ServicoCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV50ContratoServicosTelas_ServicoCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV50ContratoServicosTelas_ServicoCod3)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContratada_PessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV10TFContratada_PessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV10TFContratada_PessoaNom)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratada_PessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV11TFContratada_PessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] = @AV11TFContratada_PessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoServicosTelas_ServicoSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratoServicosTelas_ServicoSigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[Servico_Sigla] like @lV12TFContratoServicosTelas_ServicoSigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[Servico_Sigla] like @lV12TFContratoServicosTelas_ServicoSigla)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoServicosTelas_ServicoSigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[Servico_Sigla] = @AV13TFContratoServicosTelas_ServicoSigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[Servico_Sigla] = @AV13TFContratoServicosTelas_ServicoSigla_Sel)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoServicosTelas_Tela_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoServicosTelas_Tela)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] like @lV14TFContratoServicosTelas_Tela)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Tela] like @lV14TFContratoServicosTelas_Tela)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoServicosTelas_Tela_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] = @AV15TFContratoServicosTelas_Tela_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Tela] = @AV15TFContratoServicosTelas_Tela_Sel)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoServicosTelas_Link_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContratoServicosTelas_Link)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] like @lV16TFContratoServicosTelas_Link)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Link] like @lV16TFContratoServicosTelas_Link)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoServicosTelas_Link_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] = @AV17TFContratoServicosTelas_Link_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Link] = @AV17TFContratoServicosTelas_Link_Sel)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratoServicosTelas_Parms_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFContratoServicosTelas_Parms)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] like @lV18TFContratoServicosTelas_Parms)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Parms] like @lV18TFContratoServicosTelas_Parms)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratoServicosTelas_Parms_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] = @AV19TFContratoServicosTelas_Parms_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Parms] = @AV19TFContratoServicosTelas_Parms_Sel)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( AV21TFContratoServicosTelas_Status_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV21TFContratoServicosTelas_Status_Sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV21TFContratoServicosTelas_Status_Sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T5.[Pessoa_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00PA3( IGxContext context ,
                                             String A932ContratoServicosTelas_Status ,
                                             IGxCollection AV21TFContratoServicosTelas_Status_Sels ,
                                             String AV40DynamicFiltersSelector1 ,
                                             short AV41DynamicFiltersOperator1 ,
                                             int AV42ContratoServicosTelas_ServicoCod1 ,
                                             bool AV43DynamicFiltersEnabled2 ,
                                             String AV44DynamicFiltersSelector2 ,
                                             short AV45DynamicFiltersOperator2 ,
                                             int AV46ContratoServicosTelas_ServicoCod2 ,
                                             bool AV47DynamicFiltersEnabled3 ,
                                             String AV48DynamicFiltersSelector3 ,
                                             short AV49DynamicFiltersOperator3 ,
                                             int AV50ContratoServicosTelas_ServicoCod3 ,
                                             String AV11TFContratada_PessoaNom_Sel ,
                                             String AV10TFContratada_PessoaNom ,
                                             String AV13TFContratoServicosTelas_ServicoSigla_Sel ,
                                             String AV12TFContratoServicosTelas_ServicoSigla ,
                                             String AV15TFContratoServicosTelas_Tela_Sel ,
                                             String AV14TFContratoServicosTelas_Tela ,
                                             String AV17TFContratoServicosTelas_Link_Sel ,
                                             String AV16TFContratoServicosTelas_Link ,
                                             String AV19TFContratoServicosTelas_Parms_Sel ,
                                             String AV18TFContratoServicosTelas_Parms ,
                                             int AV21TFContratoServicosTelas_Status_Sels_Count ,
                                             int A925ContratoServicosTelas_ServicoCod ,
                                             String A41Contratada_PessoaNom ,
                                             String A937ContratoServicosTelas_ServicoSigla ,
                                             String A931ContratoServicosTelas_Tela ,
                                             String A928ContratoServicosTelas_Link ,
                                             String A929ContratoServicosTelas_Parms )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [19] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoServicosTelas_ContratoCod] AS ContratoServicosTelas_ContratoCod, T2.[Contrato_Codigo], T3.[Contratada_Codigo], T4.[Contratada_PessoaCod] AS Contratada_PessoaCod, T6.[Servico_Sigla] AS ContratoServicosTelas_ServicoSigla, T1.[ContratoServicosTelas_Status], T1.[ContratoServicosTelas_Parms], T1.[ContratoServicosTelas_Link], T1.[ContratoServicosTelas_Tela], T5.[Pessoa_Nome] AS Contratada_PessoaNom, T2.[Servico_Codigo] AS ContratoServicosTelas_ServicoCod, T1.[ContratoServicosTelas_Sequencial] FROM ((((([ContratoServicosTelas] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosTelas_ContratoCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T3.[Contratada_Codigo]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratada_PessoaCod]) LEFT JOIN [Servico] T6 WITH (NOLOCK) ON T6.[Servico_Codigo] = T2.[Servico_Codigo])";
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV41DynamicFiltersOperator1 == 0 ) && ( ! (0==AV42ContratoServicosTelas_ServicoCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV42ContratoServicosTelas_ServicoCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV42ContratoServicosTelas_ServicoCod1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV41DynamicFiltersOperator1 == 1 ) && ( ! (0==AV42ContratoServicosTelas_ServicoCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV42ContratoServicosTelas_ServicoCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV42ContratoServicosTelas_ServicoCod1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV41DynamicFiltersOperator1 == 2 ) && ( ! (0==AV42ContratoServicosTelas_ServicoCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV42ContratoServicosTelas_ServicoCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV42ContratoServicosTelas_ServicoCod1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV45DynamicFiltersOperator2 == 0 ) && ( ! (0==AV46ContratoServicosTelas_ServicoCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV46ContratoServicosTelas_ServicoCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV46ContratoServicosTelas_ServicoCod2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV45DynamicFiltersOperator2 == 1 ) && ( ! (0==AV46ContratoServicosTelas_ServicoCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV46ContratoServicosTelas_ServicoCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV46ContratoServicosTelas_ServicoCod2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV45DynamicFiltersOperator2 == 2 ) && ( ! (0==AV46ContratoServicosTelas_ServicoCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV46ContratoServicosTelas_ServicoCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV46ContratoServicosTelas_ServicoCod2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV47DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV48DynamicFiltersSelector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV49DynamicFiltersOperator3 == 0 ) && ( ! (0==AV50ContratoServicosTelas_ServicoCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV50ContratoServicosTelas_ServicoCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV50ContratoServicosTelas_ServicoCod3)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV47DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV48DynamicFiltersSelector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV49DynamicFiltersOperator3 == 1 ) && ( ! (0==AV50ContratoServicosTelas_ServicoCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV50ContratoServicosTelas_ServicoCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV50ContratoServicosTelas_ServicoCod3)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV47DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV48DynamicFiltersSelector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV49DynamicFiltersOperator3 == 2 ) && ( ! (0==AV50ContratoServicosTelas_ServicoCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV50ContratoServicosTelas_ServicoCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV50ContratoServicosTelas_ServicoCod3)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContratada_PessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV10TFContratada_PessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV10TFContratada_PessoaNom)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratada_PessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV11TFContratada_PessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] = @AV11TFContratada_PessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoServicosTelas_ServicoSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratoServicosTelas_ServicoSigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[Servico_Sigla] like @lV12TFContratoServicosTelas_ServicoSigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[Servico_Sigla] like @lV12TFContratoServicosTelas_ServicoSigla)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoServicosTelas_ServicoSigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[Servico_Sigla] = @AV13TFContratoServicosTelas_ServicoSigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[Servico_Sigla] = @AV13TFContratoServicosTelas_ServicoSigla_Sel)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoServicosTelas_Tela_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoServicosTelas_Tela)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] like @lV14TFContratoServicosTelas_Tela)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Tela] like @lV14TFContratoServicosTelas_Tela)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoServicosTelas_Tela_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] = @AV15TFContratoServicosTelas_Tela_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Tela] = @AV15TFContratoServicosTelas_Tela_Sel)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoServicosTelas_Link_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContratoServicosTelas_Link)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] like @lV16TFContratoServicosTelas_Link)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Link] like @lV16TFContratoServicosTelas_Link)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoServicosTelas_Link_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] = @AV17TFContratoServicosTelas_Link_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Link] = @AV17TFContratoServicosTelas_Link_Sel)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratoServicosTelas_Parms_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFContratoServicosTelas_Parms)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] like @lV18TFContratoServicosTelas_Parms)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Parms] like @lV18TFContratoServicosTelas_Parms)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratoServicosTelas_Parms_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] = @AV19TFContratoServicosTelas_Parms_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Parms] = @AV19TFContratoServicosTelas_Parms_Sel)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( AV21TFContratoServicosTelas_Status_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV21TFContratoServicosTelas_Status_Sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV21TFContratoServicosTelas_Status_Sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T6.[Servico_Sigla]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00PA4( IGxContext context ,
                                             String A932ContratoServicosTelas_Status ,
                                             IGxCollection AV21TFContratoServicosTelas_Status_Sels ,
                                             String AV40DynamicFiltersSelector1 ,
                                             short AV41DynamicFiltersOperator1 ,
                                             int AV42ContratoServicosTelas_ServicoCod1 ,
                                             bool AV43DynamicFiltersEnabled2 ,
                                             String AV44DynamicFiltersSelector2 ,
                                             short AV45DynamicFiltersOperator2 ,
                                             int AV46ContratoServicosTelas_ServicoCod2 ,
                                             bool AV47DynamicFiltersEnabled3 ,
                                             String AV48DynamicFiltersSelector3 ,
                                             short AV49DynamicFiltersOperator3 ,
                                             int AV50ContratoServicosTelas_ServicoCod3 ,
                                             String AV11TFContratada_PessoaNom_Sel ,
                                             String AV10TFContratada_PessoaNom ,
                                             String AV13TFContratoServicosTelas_ServicoSigla_Sel ,
                                             String AV12TFContratoServicosTelas_ServicoSigla ,
                                             String AV15TFContratoServicosTelas_Tela_Sel ,
                                             String AV14TFContratoServicosTelas_Tela ,
                                             String AV17TFContratoServicosTelas_Link_Sel ,
                                             String AV16TFContratoServicosTelas_Link ,
                                             String AV19TFContratoServicosTelas_Parms_Sel ,
                                             String AV18TFContratoServicosTelas_Parms ,
                                             int AV21TFContratoServicosTelas_Status_Sels_Count ,
                                             int A925ContratoServicosTelas_ServicoCod ,
                                             String A41Contratada_PessoaNom ,
                                             String A937ContratoServicosTelas_ServicoSigla ,
                                             String A931ContratoServicosTelas_Tela ,
                                             String A928ContratoServicosTelas_Link ,
                                             String A929ContratoServicosTelas_Parms )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [19] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoServicosTelas_ContratoCod] AS ContratoServicosTelas_ContratoCod, T2.[Contrato_Codigo], T3.[Contratada_Codigo], T4.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[ContratoServicosTelas_Tela], T1.[ContratoServicosTelas_Status], T1.[ContratoServicosTelas_Parms], T1.[ContratoServicosTelas_Link], T6.[Servico_Sigla] AS ContratoServicosTelas_ServicoSigla, T5.[Pessoa_Nome] AS Contratada_PessoaNom, T2.[Servico_Codigo] AS ContratoServicosTelas_ServicoCod, T1.[ContratoServicosTelas_Sequencial] FROM ((((([ContratoServicosTelas] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosTelas_ContratoCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T3.[Contratada_Codigo]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratada_PessoaCod]) LEFT JOIN [Servico] T6 WITH (NOLOCK) ON T6.[Servico_Codigo] = T2.[Servico_Codigo])";
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV41DynamicFiltersOperator1 == 0 ) && ( ! (0==AV42ContratoServicosTelas_ServicoCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV42ContratoServicosTelas_ServicoCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV42ContratoServicosTelas_ServicoCod1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV41DynamicFiltersOperator1 == 1 ) && ( ! (0==AV42ContratoServicosTelas_ServicoCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV42ContratoServicosTelas_ServicoCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV42ContratoServicosTelas_ServicoCod1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV41DynamicFiltersOperator1 == 2 ) && ( ! (0==AV42ContratoServicosTelas_ServicoCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV42ContratoServicosTelas_ServicoCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV42ContratoServicosTelas_ServicoCod1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV45DynamicFiltersOperator2 == 0 ) && ( ! (0==AV46ContratoServicosTelas_ServicoCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV46ContratoServicosTelas_ServicoCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV46ContratoServicosTelas_ServicoCod2)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV45DynamicFiltersOperator2 == 1 ) && ( ! (0==AV46ContratoServicosTelas_ServicoCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV46ContratoServicosTelas_ServicoCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV46ContratoServicosTelas_ServicoCod2)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV45DynamicFiltersOperator2 == 2 ) && ( ! (0==AV46ContratoServicosTelas_ServicoCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV46ContratoServicosTelas_ServicoCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV46ContratoServicosTelas_ServicoCod2)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV47DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV48DynamicFiltersSelector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV49DynamicFiltersOperator3 == 0 ) && ( ! (0==AV50ContratoServicosTelas_ServicoCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV50ContratoServicosTelas_ServicoCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV50ContratoServicosTelas_ServicoCod3)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV47DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV48DynamicFiltersSelector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV49DynamicFiltersOperator3 == 1 ) && ( ! (0==AV50ContratoServicosTelas_ServicoCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV50ContratoServicosTelas_ServicoCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV50ContratoServicosTelas_ServicoCod3)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV47DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV48DynamicFiltersSelector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV49DynamicFiltersOperator3 == 2 ) && ( ! (0==AV50ContratoServicosTelas_ServicoCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV50ContratoServicosTelas_ServicoCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV50ContratoServicosTelas_ServicoCod3)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContratada_PessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV10TFContratada_PessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV10TFContratada_PessoaNom)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratada_PessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV11TFContratada_PessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] = @AV11TFContratada_PessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoServicosTelas_ServicoSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratoServicosTelas_ServicoSigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[Servico_Sigla] like @lV12TFContratoServicosTelas_ServicoSigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[Servico_Sigla] like @lV12TFContratoServicosTelas_ServicoSigla)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoServicosTelas_ServicoSigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[Servico_Sigla] = @AV13TFContratoServicosTelas_ServicoSigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[Servico_Sigla] = @AV13TFContratoServicosTelas_ServicoSigla_Sel)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoServicosTelas_Tela_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoServicosTelas_Tela)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] like @lV14TFContratoServicosTelas_Tela)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Tela] like @lV14TFContratoServicosTelas_Tela)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoServicosTelas_Tela_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] = @AV15TFContratoServicosTelas_Tela_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Tela] = @AV15TFContratoServicosTelas_Tela_Sel)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoServicosTelas_Link_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContratoServicosTelas_Link)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] like @lV16TFContratoServicosTelas_Link)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Link] like @lV16TFContratoServicosTelas_Link)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoServicosTelas_Link_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] = @AV17TFContratoServicosTelas_Link_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Link] = @AV17TFContratoServicosTelas_Link_Sel)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratoServicosTelas_Parms_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFContratoServicosTelas_Parms)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] like @lV18TFContratoServicosTelas_Parms)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Parms] like @lV18TFContratoServicosTelas_Parms)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratoServicosTelas_Parms_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] = @AV19TFContratoServicosTelas_Parms_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Parms] = @AV19TFContratoServicosTelas_Parms_Sel)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( AV21TFContratoServicosTelas_Status_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV21TFContratoServicosTelas_Status_Sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV21TFContratoServicosTelas_Status_Sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoServicosTelas_Tela]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00PA5( IGxContext context ,
                                             String A932ContratoServicosTelas_Status ,
                                             IGxCollection AV21TFContratoServicosTelas_Status_Sels ,
                                             String AV40DynamicFiltersSelector1 ,
                                             short AV41DynamicFiltersOperator1 ,
                                             int AV42ContratoServicosTelas_ServicoCod1 ,
                                             bool AV43DynamicFiltersEnabled2 ,
                                             String AV44DynamicFiltersSelector2 ,
                                             short AV45DynamicFiltersOperator2 ,
                                             int AV46ContratoServicosTelas_ServicoCod2 ,
                                             bool AV47DynamicFiltersEnabled3 ,
                                             String AV48DynamicFiltersSelector3 ,
                                             short AV49DynamicFiltersOperator3 ,
                                             int AV50ContratoServicosTelas_ServicoCod3 ,
                                             String AV11TFContratada_PessoaNom_Sel ,
                                             String AV10TFContratada_PessoaNom ,
                                             String AV13TFContratoServicosTelas_ServicoSigla_Sel ,
                                             String AV12TFContratoServicosTelas_ServicoSigla ,
                                             String AV15TFContratoServicosTelas_Tela_Sel ,
                                             String AV14TFContratoServicosTelas_Tela ,
                                             String AV17TFContratoServicosTelas_Link_Sel ,
                                             String AV16TFContratoServicosTelas_Link ,
                                             String AV19TFContratoServicosTelas_Parms_Sel ,
                                             String AV18TFContratoServicosTelas_Parms ,
                                             int AV21TFContratoServicosTelas_Status_Sels_Count ,
                                             int A925ContratoServicosTelas_ServicoCod ,
                                             String A41Contratada_PessoaNom ,
                                             String A937ContratoServicosTelas_ServicoSigla ,
                                             String A931ContratoServicosTelas_Tela ,
                                             String A928ContratoServicosTelas_Link ,
                                             String A929ContratoServicosTelas_Parms )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [19] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoServicosTelas_ContratoCod] AS ContratoServicosTelas_ContratoCod, T2.[Contrato_Codigo], T3.[Contratada_Codigo], T4.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[ContratoServicosTelas_Link], T1.[ContratoServicosTelas_Status], T1.[ContratoServicosTelas_Parms], T1.[ContratoServicosTelas_Tela], T6.[Servico_Sigla] AS ContratoServicosTelas_ServicoSigla, T5.[Pessoa_Nome] AS Contratada_PessoaNom, T2.[Servico_Codigo] AS ContratoServicosTelas_ServicoCod, T1.[ContratoServicosTelas_Sequencial] FROM ((((([ContratoServicosTelas] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosTelas_ContratoCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T3.[Contratada_Codigo]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratada_PessoaCod]) LEFT JOIN [Servico] T6 WITH (NOLOCK) ON T6.[Servico_Codigo] = T2.[Servico_Codigo])";
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV41DynamicFiltersOperator1 == 0 ) && ( ! (0==AV42ContratoServicosTelas_ServicoCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV42ContratoServicosTelas_ServicoCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV42ContratoServicosTelas_ServicoCod1)";
            }
         }
         else
         {
            GXv_int7[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV41DynamicFiltersOperator1 == 1 ) && ( ! (0==AV42ContratoServicosTelas_ServicoCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV42ContratoServicosTelas_ServicoCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV42ContratoServicosTelas_ServicoCod1)";
            }
         }
         else
         {
            GXv_int7[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV41DynamicFiltersOperator1 == 2 ) && ( ! (0==AV42ContratoServicosTelas_ServicoCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV42ContratoServicosTelas_ServicoCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV42ContratoServicosTelas_ServicoCod1)";
            }
         }
         else
         {
            GXv_int7[2] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV45DynamicFiltersOperator2 == 0 ) && ( ! (0==AV46ContratoServicosTelas_ServicoCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV46ContratoServicosTelas_ServicoCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV46ContratoServicosTelas_ServicoCod2)";
            }
         }
         else
         {
            GXv_int7[3] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV45DynamicFiltersOperator2 == 1 ) && ( ! (0==AV46ContratoServicosTelas_ServicoCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV46ContratoServicosTelas_ServicoCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV46ContratoServicosTelas_ServicoCod2)";
            }
         }
         else
         {
            GXv_int7[4] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV45DynamicFiltersOperator2 == 2 ) && ( ! (0==AV46ContratoServicosTelas_ServicoCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV46ContratoServicosTelas_ServicoCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV46ContratoServicosTelas_ServicoCod2)";
            }
         }
         else
         {
            GXv_int7[5] = 1;
         }
         if ( AV47DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV48DynamicFiltersSelector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV49DynamicFiltersOperator3 == 0 ) && ( ! (0==AV50ContratoServicosTelas_ServicoCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV50ContratoServicosTelas_ServicoCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV50ContratoServicosTelas_ServicoCod3)";
            }
         }
         else
         {
            GXv_int7[6] = 1;
         }
         if ( AV47DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV48DynamicFiltersSelector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV49DynamicFiltersOperator3 == 1 ) && ( ! (0==AV50ContratoServicosTelas_ServicoCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV50ContratoServicosTelas_ServicoCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV50ContratoServicosTelas_ServicoCod3)";
            }
         }
         else
         {
            GXv_int7[7] = 1;
         }
         if ( AV47DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV48DynamicFiltersSelector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV49DynamicFiltersOperator3 == 2 ) && ( ! (0==AV50ContratoServicosTelas_ServicoCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV50ContratoServicosTelas_ServicoCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV50ContratoServicosTelas_ServicoCod3)";
            }
         }
         else
         {
            GXv_int7[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContratada_PessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV10TFContratada_PessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV10TFContratada_PessoaNom)";
            }
         }
         else
         {
            GXv_int7[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratada_PessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV11TFContratada_PessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] = @AV11TFContratada_PessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int7[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoServicosTelas_ServicoSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratoServicosTelas_ServicoSigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[Servico_Sigla] like @lV12TFContratoServicosTelas_ServicoSigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[Servico_Sigla] like @lV12TFContratoServicosTelas_ServicoSigla)";
            }
         }
         else
         {
            GXv_int7[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoServicosTelas_ServicoSigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[Servico_Sigla] = @AV13TFContratoServicosTelas_ServicoSigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[Servico_Sigla] = @AV13TFContratoServicosTelas_ServicoSigla_Sel)";
            }
         }
         else
         {
            GXv_int7[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoServicosTelas_Tela_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoServicosTelas_Tela)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] like @lV14TFContratoServicosTelas_Tela)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Tela] like @lV14TFContratoServicosTelas_Tela)";
            }
         }
         else
         {
            GXv_int7[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoServicosTelas_Tela_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] = @AV15TFContratoServicosTelas_Tela_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Tela] = @AV15TFContratoServicosTelas_Tela_Sel)";
            }
         }
         else
         {
            GXv_int7[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoServicosTelas_Link_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContratoServicosTelas_Link)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] like @lV16TFContratoServicosTelas_Link)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Link] like @lV16TFContratoServicosTelas_Link)";
            }
         }
         else
         {
            GXv_int7[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoServicosTelas_Link_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] = @AV17TFContratoServicosTelas_Link_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Link] = @AV17TFContratoServicosTelas_Link_Sel)";
            }
         }
         else
         {
            GXv_int7[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratoServicosTelas_Parms_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFContratoServicosTelas_Parms)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] like @lV18TFContratoServicosTelas_Parms)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Parms] like @lV18TFContratoServicosTelas_Parms)";
            }
         }
         else
         {
            GXv_int7[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratoServicosTelas_Parms_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] = @AV19TFContratoServicosTelas_Parms_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Parms] = @AV19TFContratoServicosTelas_Parms_Sel)";
            }
         }
         else
         {
            GXv_int7[18] = 1;
         }
         if ( AV21TFContratoServicosTelas_Status_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV21TFContratoServicosTelas_Status_Sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV21TFContratoServicosTelas_Status_Sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoServicosTelas_Link]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      protected Object[] conditional_P00PA6( IGxContext context ,
                                             String A932ContratoServicosTelas_Status ,
                                             IGxCollection AV21TFContratoServicosTelas_Status_Sels ,
                                             String AV40DynamicFiltersSelector1 ,
                                             short AV41DynamicFiltersOperator1 ,
                                             int AV42ContratoServicosTelas_ServicoCod1 ,
                                             bool AV43DynamicFiltersEnabled2 ,
                                             String AV44DynamicFiltersSelector2 ,
                                             short AV45DynamicFiltersOperator2 ,
                                             int AV46ContratoServicosTelas_ServicoCod2 ,
                                             bool AV47DynamicFiltersEnabled3 ,
                                             String AV48DynamicFiltersSelector3 ,
                                             short AV49DynamicFiltersOperator3 ,
                                             int AV50ContratoServicosTelas_ServicoCod3 ,
                                             String AV11TFContratada_PessoaNom_Sel ,
                                             String AV10TFContratada_PessoaNom ,
                                             String AV13TFContratoServicosTelas_ServicoSigla_Sel ,
                                             String AV12TFContratoServicosTelas_ServicoSigla ,
                                             String AV15TFContratoServicosTelas_Tela_Sel ,
                                             String AV14TFContratoServicosTelas_Tela ,
                                             String AV17TFContratoServicosTelas_Link_Sel ,
                                             String AV16TFContratoServicosTelas_Link ,
                                             String AV19TFContratoServicosTelas_Parms_Sel ,
                                             String AV18TFContratoServicosTelas_Parms ,
                                             int AV21TFContratoServicosTelas_Status_Sels_Count ,
                                             int A925ContratoServicosTelas_ServicoCod ,
                                             String A41Contratada_PessoaNom ,
                                             String A937ContratoServicosTelas_ServicoSigla ,
                                             String A931ContratoServicosTelas_Tela ,
                                             String A928ContratoServicosTelas_Link ,
                                             String A929ContratoServicosTelas_Parms )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int9 ;
         GXv_int9 = new short [19] ;
         Object[] GXv_Object10 ;
         GXv_Object10 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoServicosTelas_ContratoCod] AS ContratoServicosTelas_ContratoCod, T2.[Contrato_Codigo], T3.[Contratada_Codigo], T4.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[ContratoServicosTelas_Parms], T1.[ContratoServicosTelas_Status], T1.[ContratoServicosTelas_Link], T1.[ContratoServicosTelas_Tela], T6.[Servico_Sigla] AS ContratoServicosTelas_ServicoSigla, T5.[Pessoa_Nome] AS Contratada_PessoaNom, T2.[Servico_Codigo] AS ContratoServicosTelas_ServicoCod, T1.[ContratoServicosTelas_Sequencial] FROM ((((([ContratoServicosTelas] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosTelas_ContratoCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T3.[Contratada_Codigo]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratada_PessoaCod]) LEFT JOIN [Servico] T6 WITH (NOLOCK) ON T6.[Servico_Codigo] = T2.[Servico_Codigo])";
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV41DynamicFiltersOperator1 == 0 ) && ( ! (0==AV42ContratoServicosTelas_ServicoCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV42ContratoServicosTelas_ServicoCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV42ContratoServicosTelas_ServicoCod1)";
            }
         }
         else
         {
            GXv_int9[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV41DynamicFiltersOperator1 == 1 ) && ( ! (0==AV42ContratoServicosTelas_ServicoCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV42ContratoServicosTelas_ServicoCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV42ContratoServicosTelas_ServicoCod1)";
            }
         }
         else
         {
            GXv_int9[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV41DynamicFiltersOperator1 == 2 ) && ( ! (0==AV42ContratoServicosTelas_ServicoCod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV42ContratoServicosTelas_ServicoCod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV42ContratoServicosTelas_ServicoCod1)";
            }
         }
         else
         {
            GXv_int9[2] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV45DynamicFiltersOperator2 == 0 ) && ( ! (0==AV46ContratoServicosTelas_ServicoCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV46ContratoServicosTelas_ServicoCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV46ContratoServicosTelas_ServicoCod2)";
            }
         }
         else
         {
            GXv_int9[3] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV45DynamicFiltersOperator2 == 1 ) && ( ! (0==AV46ContratoServicosTelas_ServicoCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV46ContratoServicosTelas_ServicoCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV46ContratoServicosTelas_ServicoCod2)";
            }
         }
         else
         {
            GXv_int9[4] = 1;
         }
         if ( AV43DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV45DynamicFiltersOperator2 == 2 ) && ( ! (0==AV46ContratoServicosTelas_ServicoCod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV46ContratoServicosTelas_ServicoCod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV46ContratoServicosTelas_ServicoCod2)";
            }
         }
         else
         {
            GXv_int9[5] = 1;
         }
         if ( AV47DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV48DynamicFiltersSelector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV49DynamicFiltersOperator3 == 0 ) && ( ! (0==AV50ContratoServicosTelas_ServicoCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV50ContratoServicosTelas_ServicoCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV50ContratoServicosTelas_ServicoCod3)";
            }
         }
         else
         {
            GXv_int9[6] = 1;
         }
         if ( AV47DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV48DynamicFiltersSelector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV49DynamicFiltersOperator3 == 1 ) && ( ! (0==AV50ContratoServicosTelas_ServicoCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV50ContratoServicosTelas_ServicoCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV50ContratoServicosTelas_ServicoCod3)";
            }
         }
         else
         {
            GXv_int9[7] = 1;
         }
         if ( AV47DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV48DynamicFiltersSelector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV49DynamicFiltersOperator3 == 2 ) && ( ! (0==AV50ContratoServicosTelas_ServicoCod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV50ContratoServicosTelas_ServicoCod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV50ContratoServicosTelas_ServicoCod3)";
            }
         }
         else
         {
            GXv_int9[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContratada_PessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV10TFContratada_PessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV10TFContratada_PessoaNom)";
            }
         }
         else
         {
            GXv_int9[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratada_PessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV11TFContratada_PessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] = @AV11TFContratada_PessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int9[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoServicosTelas_ServicoSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratoServicosTelas_ServicoSigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[Servico_Sigla] like @lV12TFContratoServicosTelas_ServicoSigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[Servico_Sigla] like @lV12TFContratoServicosTelas_ServicoSigla)";
            }
         }
         else
         {
            GXv_int9[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratoServicosTelas_ServicoSigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[Servico_Sigla] = @AV13TFContratoServicosTelas_ServicoSigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[Servico_Sigla] = @AV13TFContratoServicosTelas_ServicoSigla_Sel)";
            }
         }
         else
         {
            GXv_int9[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoServicosTelas_Tela_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoServicosTelas_Tela)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] like @lV14TFContratoServicosTelas_Tela)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Tela] like @lV14TFContratoServicosTelas_Tela)";
            }
         }
         else
         {
            GXv_int9[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoServicosTelas_Tela_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] = @AV15TFContratoServicosTelas_Tela_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Tela] = @AV15TFContratoServicosTelas_Tela_Sel)";
            }
         }
         else
         {
            GXv_int9[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoServicosTelas_Link_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContratoServicosTelas_Link)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] like @lV16TFContratoServicosTelas_Link)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Link] like @lV16TFContratoServicosTelas_Link)";
            }
         }
         else
         {
            GXv_int9[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContratoServicosTelas_Link_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] = @AV17TFContratoServicosTelas_Link_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Link] = @AV17TFContratoServicosTelas_Link_Sel)";
            }
         }
         else
         {
            GXv_int9[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratoServicosTelas_Parms_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFContratoServicosTelas_Parms)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] like @lV18TFContratoServicosTelas_Parms)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Parms] like @lV18TFContratoServicosTelas_Parms)";
            }
         }
         else
         {
            GXv_int9[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratoServicosTelas_Parms_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] = @AV19TFContratoServicosTelas_Parms_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Parms] = @AV19TFContratoServicosTelas_Parms_Sel)";
            }
         }
         else
         {
            GXv_int9[18] = 1;
         }
         if ( AV21TFContratoServicosTelas_Status_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV21TFContratoServicosTelas_Status_Sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV21TFContratoServicosTelas_Status_Sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoServicosTelas_Parms]";
         GXv_Object10[0] = scmdbuf;
         GXv_Object10[1] = GXv_int9;
         return GXv_Object10 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00PA2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (int)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] );
               case 1 :
                     return conditional_P00PA3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (int)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] );
               case 2 :
                     return conditional_P00PA4(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (int)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] );
               case 3 :
                     return conditional_P00PA5(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (int)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] );
               case 4 :
                     return conditional_P00PA6(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (int)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00PA2 ;
          prmP00PA2 = new Object[] {
          new Object[] {"@AV42ContratoServicosTelas_ServicoCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV42ContratoServicosTelas_ServicoCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV42ContratoServicosTelas_ServicoCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46ContratoServicosTelas_ServicoCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46ContratoServicosTelas_ServicoCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46ContratoServicosTelas_ServicoCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV50ContratoServicosTelas_ServicoCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV50ContratoServicosTelas_ServicoCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV50ContratoServicosTelas_ServicoCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV11TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV12TFContratoServicosTelas_ServicoSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV13TFContratoServicosTelas_ServicoSigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV14TFContratoServicosTelas_Tela",SqlDbType.Char,50,0} ,
          new Object[] {"@AV15TFContratoServicosTelas_Tela_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV16TFContratoServicosTelas_Link",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV17TFContratoServicosTelas_Link_Sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV18TFContratoServicosTelas_Parms",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV19TFContratoServicosTelas_Parms_Sel",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmP00PA3 ;
          prmP00PA3 = new Object[] {
          new Object[] {"@AV42ContratoServicosTelas_ServicoCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV42ContratoServicosTelas_ServicoCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV42ContratoServicosTelas_ServicoCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46ContratoServicosTelas_ServicoCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46ContratoServicosTelas_ServicoCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46ContratoServicosTelas_ServicoCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV50ContratoServicosTelas_ServicoCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV50ContratoServicosTelas_ServicoCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV50ContratoServicosTelas_ServicoCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV11TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV12TFContratoServicosTelas_ServicoSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV13TFContratoServicosTelas_ServicoSigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV14TFContratoServicosTelas_Tela",SqlDbType.Char,50,0} ,
          new Object[] {"@AV15TFContratoServicosTelas_Tela_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV16TFContratoServicosTelas_Link",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV17TFContratoServicosTelas_Link_Sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV18TFContratoServicosTelas_Parms",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV19TFContratoServicosTelas_Parms_Sel",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmP00PA4 ;
          prmP00PA4 = new Object[] {
          new Object[] {"@AV42ContratoServicosTelas_ServicoCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV42ContratoServicosTelas_ServicoCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV42ContratoServicosTelas_ServicoCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46ContratoServicosTelas_ServicoCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46ContratoServicosTelas_ServicoCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46ContratoServicosTelas_ServicoCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV50ContratoServicosTelas_ServicoCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV50ContratoServicosTelas_ServicoCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV50ContratoServicosTelas_ServicoCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV11TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV12TFContratoServicosTelas_ServicoSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV13TFContratoServicosTelas_ServicoSigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV14TFContratoServicosTelas_Tela",SqlDbType.Char,50,0} ,
          new Object[] {"@AV15TFContratoServicosTelas_Tela_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV16TFContratoServicosTelas_Link",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV17TFContratoServicosTelas_Link_Sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV18TFContratoServicosTelas_Parms",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV19TFContratoServicosTelas_Parms_Sel",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmP00PA5 ;
          prmP00PA5 = new Object[] {
          new Object[] {"@AV42ContratoServicosTelas_ServicoCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV42ContratoServicosTelas_ServicoCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV42ContratoServicosTelas_ServicoCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46ContratoServicosTelas_ServicoCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46ContratoServicosTelas_ServicoCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46ContratoServicosTelas_ServicoCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV50ContratoServicosTelas_ServicoCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV50ContratoServicosTelas_ServicoCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV50ContratoServicosTelas_ServicoCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV11TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV12TFContratoServicosTelas_ServicoSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV13TFContratoServicosTelas_ServicoSigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV14TFContratoServicosTelas_Tela",SqlDbType.Char,50,0} ,
          new Object[] {"@AV15TFContratoServicosTelas_Tela_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV16TFContratoServicosTelas_Link",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV17TFContratoServicosTelas_Link_Sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV18TFContratoServicosTelas_Parms",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV19TFContratoServicosTelas_Parms_Sel",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmP00PA6 ;
          prmP00PA6 = new Object[] {
          new Object[] {"@AV42ContratoServicosTelas_ServicoCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV42ContratoServicosTelas_ServicoCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV42ContratoServicosTelas_ServicoCod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46ContratoServicosTelas_ServicoCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46ContratoServicosTelas_ServicoCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46ContratoServicosTelas_ServicoCod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV50ContratoServicosTelas_ServicoCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV50ContratoServicosTelas_ServicoCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV50ContratoServicosTelas_ServicoCod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV11TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV12TFContratoServicosTelas_ServicoSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV13TFContratoServicosTelas_ServicoSigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV14TFContratoServicosTelas_Tela",SqlDbType.Char,50,0} ,
          new Object[] {"@AV15TFContratoServicosTelas_Tela_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV16TFContratoServicosTelas_Link",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV17TFContratoServicosTelas_Link_Sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV18TFContratoServicosTelas_Parms",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV19TFContratoServicosTelas_Parms_Sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00PA2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00PA2,100,0,true,false )
             ,new CursorDef("P00PA3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00PA3,100,0,true,false )
             ,new CursorDef("P00PA4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00PA4,100,0,true,false )
             ,new CursorDef("P00PA5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00PA5,100,0,true,false )
             ,new CursorDef("P00PA6", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00PA6,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 50) ;
                ((String[]) buf[13])[0] = rslt.getString(10, 15) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((int[]) buf[15])[0] = rslt.getInt(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((short[]) buf[17])[0] = rslt.getShort(12) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 50) ;
                ((String[]) buf[13])[0] = rslt.getString(10, 100) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((int[]) buf[15])[0] = rslt.getInt(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((short[]) buf[17])[0] = rslt.getShort(12) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[11])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getString(10, 100) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((int[]) buf[15])[0] = rslt.getInt(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((short[]) buf[17])[0] = rslt.getShort(12) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 50) ;
                ((String[]) buf[11])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getString(10, 100) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((int[]) buf[15])[0] = rslt.getInt(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((short[]) buf[17])[0] = rslt.getShort(12) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((String[]) buf[10])[0] = rslt.getString(8, 50) ;
                ((String[]) buf[11])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getString(10, 100) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((int[]) buf[15])[0] = rslt.getInt(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((short[]) buf[17])[0] = rslt.getShort(12) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptcontratoservicostelasfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptcontratoservicostelasfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptcontratoservicostelasfilterdata") )
          {
             return  ;
          }
          getpromptcontratoservicostelasfilterdata worker = new getpromptcontratoservicostelasfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
