/*
               File: PRC_ProntoParaLiqFnc
        Description: Pronto Para Liq Fnc
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:43:1.4
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_prontoparaliqfnc : GXProcedure
   {
      public prc_prontoparaliqfnc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_prontoparaliqfnc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( String aP0_StatusDemanda ,
                           String aP1_StatusLiqFnc ,
                           out bool aP2_Pronto )
      {
         this.AV8StatusDemanda = aP0_StatusDemanda;
         this.AV9StatusLiqFnc = aP1_StatusLiqFnc;
         this.AV10Pronto = false ;
         initialize();
         executePrivate();
         aP2_Pronto=this.AV10Pronto;
      }

      public bool executeUdp( String aP0_StatusDemanda ,
                              String aP1_StatusLiqFnc )
      {
         this.AV8StatusDemanda = aP0_StatusDemanda;
         this.AV9StatusLiqFnc = aP1_StatusLiqFnc;
         this.AV10Pronto = false ;
         initialize();
         executePrivate();
         aP2_Pronto=this.AV10Pronto;
         return AV10Pronto ;
      }

      public void executeSubmit( String aP0_StatusDemanda ,
                                 String aP1_StatusLiqFnc ,
                                 out bool aP2_Pronto )
      {
         prc_prontoparaliqfnc objprc_prontoparaliqfnc;
         objprc_prontoparaliqfnc = new prc_prontoparaliqfnc();
         objprc_prontoparaliqfnc.AV8StatusDemanda = aP0_StatusDemanda;
         objprc_prontoparaliqfnc.AV9StatusLiqFnc = aP1_StatusLiqFnc;
         objprc_prontoparaliqfnc.AV10Pronto = false ;
         objprc_prontoparaliqfnc.context.SetSubmitInitialConfig(context);
         objprc_prontoparaliqfnc.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_prontoparaliqfnc);
         aP2_Pronto=this.AV10Pronto;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_prontoparaliqfnc)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV11StatusPosteriores.Add("L", 0);
         if ( StringUtil.StrCmp(AV9StatusLiqFnc, "R") == 0 )
         {
            AV11StatusPosteriores.Add("R", 0);
            AV11StatusPosteriores.Add("C", 0);
            AV11StatusPosteriores.Add("H", 0);
            AV11StatusPosteriores.Add("O", 0);
            AV11StatusPosteriores.Add("P", 0);
            AV10Pronto = (bool)((AV11StatusPosteriores.IndexOf(AV8StatusDemanda)>0));
         }
         else if ( StringUtil.StrCmp(AV9StatusLiqFnc, "C") == 0 )
         {
            AV11StatusPosteriores.Add("C", 0);
            AV11StatusPosteriores.Add("H", 0);
            AV11StatusPosteriores.Add("O", 0);
            AV11StatusPosteriores.Add("P", 0);
            AV10Pronto = (bool)((AV11StatusPosteriores.IndexOf(AV8StatusDemanda)>0));
         }
         else if ( StringUtil.StrCmp(AV9StatusLiqFnc, "H") == 0 )
         {
            AV11StatusPosteriores.Add("H", 0);
            AV11StatusPosteriores.Add("O", 0);
            AV11StatusPosteriores.Add("P", 0);
            AV10Pronto = (bool)((AV11StatusPosteriores.IndexOf(AV8StatusDemanda)>0));
         }
         else if ( StringUtil.StrCmp(AV9StatusLiqFnc, "O") == 0 )
         {
            AV11StatusPosteriores.Add("O", 0);
            AV11StatusPosteriores.Add("P", 0);
            AV10Pronto = (bool)((AV11StatusPosteriores.IndexOf(AV8StatusDemanda)>0));
         }
         else if ( StringUtil.StrCmp(AV9StatusLiqFnc, "P") == 0 )
         {
            AV11StatusPosteriores.Add("P", 0);
            AV10Pronto = (bool)((AV11StatusPosteriores.IndexOf(AV8StatusDemanda)>0));
         }
         else if ( StringUtil.StrCmp(AV9StatusLiqFnc, "L") == 0 )
         {
            AV10Pronto = (bool)((StringUtil.StrCmp(AV8StatusDemanda, "L")==0));
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV11StatusPosteriores = new GxSimpleCollection();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private String AV8StatusDemanda ;
      private String AV9StatusLiqFnc ;
      private bool AV10Pronto ;
      private bool aP2_Pronto ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV11StatusPosteriores ;
   }

}
