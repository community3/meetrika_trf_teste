/*
               File: GetModuloModuloFuncoesWCFilterData
        Description: Get Modulo Modulo Funcoes WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:55:30.71
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getmodulomodulofuncoeswcfilterdata : GXProcedure
   {
      public getmodulomodulofuncoeswcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getmodulomodulofuncoeswcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV14DDOName = aP0_DDOName;
         this.AV12SearchTxt = aP1_SearchTxt;
         this.AV13SearchTxtTo = aP2_SearchTxtTo;
         this.AV18OptionsJson = "" ;
         this.AV21OptionsDescJson = "" ;
         this.AV23OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
         return AV23OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getmodulomodulofuncoeswcfilterdata objgetmodulomodulofuncoeswcfilterdata;
         objgetmodulomodulofuncoeswcfilterdata = new getmodulomodulofuncoeswcfilterdata();
         objgetmodulomodulofuncoeswcfilterdata.AV14DDOName = aP0_DDOName;
         objgetmodulomodulofuncoeswcfilterdata.AV12SearchTxt = aP1_SearchTxt;
         objgetmodulomodulofuncoeswcfilterdata.AV13SearchTxtTo = aP2_SearchTxtTo;
         objgetmodulomodulofuncoeswcfilterdata.AV18OptionsJson = "" ;
         objgetmodulomodulofuncoeswcfilterdata.AV21OptionsDescJson = "" ;
         objgetmodulomodulofuncoeswcfilterdata.AV23OptionIndexesJson = "" ;
         objgetmodulomodulofuncoeswcfilterdata.context.SetSubmitInitialConfig(context);
         objgetmodulomodulofuncoeswcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetmodulomodulofuncoeswcfilterdata);
         aP3_OptionsJson=this.AV18OptionsJson;
         aP4_OptionsDescJson=this.AV21OptionsDescJson;
         aP5_OptionIndexesJson=this.AV23OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getmodulomodulofuncoeswcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV17Options = (IGxCollection)(new GxSimpleCollection());
         AV20OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV22OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV14DDOName), "DDO_FUNCAOUSUARIO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAOUSUARIO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV18OptionsJson = AV17Options.ToJSonString(false);
         AV21OptionsDescJson = AV20OptionsDesc.ToJSonString(false);
         AV23OptionIndexesJson = AV22OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV25Session.Get("ModuloModuloFuncoesWCGridState"), "") == 0 )
         {
            AV27GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ModuloModuloFuncoesWCGridState"), "");
         }
         else
         {
            AV27GridState.FromXml(AV25Session.Get("ModuloModuloFuncoesWCGridState"), "");
         }
         AV35GXV1 = 1;
         while ( AV35GXV1 <= AV27GridState.gxTpr_Filtervalues.Count )
         {
            AV28GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV27GridState.gxTpr_Filtervalues.Item(AV35GXV1));
            if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "FUNCAOUSUARIO_NOME") == 0 )
            {
               AV31FuncaoUsuario_Nome = AV28GridStateFilterValue.gxTpr_Value;
               AV30FuncaoUsuario_NomeOperator = AV28GridStateFilterValue.gxTpr_Operator;
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFFUNCAOUSUARIO_NOME") == 0 )
            {
               AV10TFFuncaoUsuario_Nome = AV28GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "TFFUNCAOUSUARIO_NOME_SEL") == 0 )
            {
               AV11TFFuncaoUsuario_Nome_Sel = AV28GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV28GridStateFilterValue.gxTpr_Name, "PARM_&MODULO_CODIGO") == 0 )
            {
               AV32Modulo_Codigo = (int)(NumberUtil.Val( AV28GridStateFilterValue.gxTpr_Value, "."));
            }
            AV35GXV1 = (int)(AV35GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADFUNCAOUSUARIO_NOMEOPTIONS' Routine */
         AV10TFFuncaoUsuario_Nome = AV12SearchTxt;
         AV11TFFuncaoUsuario_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV30FuncaoUsuario_NomeOperator ,
                                              AV31FuncaoUsuario_Nome ,
                                              AV11TFFuncaoUsuario_Nome_Sel ,
                                              AV10TFFuncaoUsuario_Nome ,
                                              A162FuncaoUsuario_Nome ,
                                              AV32Modulo_Codigo ,
                                              A146Modulo_Codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV31FuncaoUsuario_Nome = StringUtil.Concat( StringUtil.RTrim( AV31FuncaoUsuario_Nome), "%", "");
         lV31FuncaoUsuario_Nome = StringUtil.Concat( StringUtil.RTrim( AV31FuncaoUsuario_Nome), "%", "");
         lV10TFFuncaoUsuario_Nome = StringUtil.Concat( StringUtil.RTrim( AV10TFFuncaoUsuario_Nome), "%", "");
         /* Using cursor P00UR2 */
         pr_default.execute(0, new Object[] {AV32Modulo_Codigo, lV31FuncaoUsuario_Nome, lV31FuncaoUsuario_Nome, lV10TFFuncaoUsuario_Nome, AV11TFFuncaoUsuario_Nome_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKUR2 = false;
            A161FuncaoUsuario_Codigo = P00UR2_A161FuncaoUsuario_Codigo[0];
            A146Modulo_Codigo = P00UR2_A146Modulo_Codigo[0];
            A162FuncaoUsuario_Nome = P00UR2_A162FuncaoUsuario_Nome[0];
            A162FuncaoUsuario_Nome = P00UR2_A162FuncaoUsuario_Nome[0];
            AV24count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00UR2_A146Modulo_Codigo[0] == A146Modulo_Codigo ) && ( P00UR2_A161FuncaoUsuario_Codigo[0] == A161FuncaoUsuario_Codigo ) )
            {
               BRKUR2 = false;
               AV24count = (long)(AV24count+1);
               BRKUR2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A162FuncaoUsuario_Nome)) )
            {
               AV16Option = A162FuncaoUsuario_Nome;
               AV15InsertIndex = 1;
               while ( ( AV15InsertIndex <= AV17Options.Count ) && ( StringUtil.StrCmp(((String)AV17Options.Item(AV15InsertIndex)), AV16Option) < 0 ) )
               {
                  AV15InsertIndex = (int)(AV15InsertIndex+1);
               }
               AV17Options.Add(AV16Option, AV15InsertIndex);
               AV22OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV24count), "Z,ZZZ,ZZZ,ZZ9")), AV15InsertIndex);
            }
            if ( AV17Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUR2 )
            {
               BRKUR2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV17Options = new GxSimpleCollection();
         AV20OptionsDesc = new GxSimpleCollection();
         AV22OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV25Session = context.GetSession();
         AV27GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV28GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV31FuncaoUsuario_Nome = "";
         AV10TFFuncaoUsuario_Nome = "";
         AV11TFFuncaoUsuario_Nome_Sel = "";
         scmdbuf = "";
         lV31FuncaoUsuario_Nome = "";
         lV10TFFuncaoUsuario_Nome = "";
         A162FuncaoUsuario_Nome = "";
         P00UR2_A161FuncaoUsuario_Codigo = new int[1] ;
         P00UR2_A146Modulo_Codigo = new int[1] ;
         P00UR2_A162FuncaoUsuario_Nome = new String[] {""} ;
         AV16Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getmodulomodulofuncoeswcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00UR2_A161FuncaoUsuario_Codigo, P00UR2_A146Modulo_Codigo, P00UR2_A162FuncaoUsuario_Nome
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV30FuncaoUsuario_NomeOperator ;
      private int AV35GXV1 ;
      private int AV32Modulo_Codigo ;
      private int A146Modulo_Codigo ;
      private int A161FuncaoUsuario_Codigo ;
      private int AV15InsertIndex ;
      private long AV24count ;
      private String scmdbuf ;
      private bool returnInSub ;
      private bool BRKUR2 ;
      private String AV23OptionIndexesJson ;
      private String AV18OptionsJson ;
      private String AV21OptionsDescJson ;
      private String AV14DDOName ;
      private String AV12SearchTxt ;
      private String AV13SearchTxtTo ;
      private String AV31FuncaoUsuario_Nome ;
      private String AV10TFFuncaoUsuario_Nome ;
      private String AV11TFFuncaoUsuario_Nome_Sel ;
      private String lV31FuncaoUsuario_Nome ;
      private String lV10TFFuncaoUsuario_Nome ;
      private String A162FuncaoUsuario_Nome ;
      private String AV16Option ;
      private IGxSession AV25Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00UR2_A161FuncaoUsuario_Codigo ;
      private int[] P00UR2_A146Modulo_Codigo ;
      private String[] P00UR2_A162FuncaoUsuario_Nome ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV17Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV20OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV27GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV28GridStateFilterValue ;
   }

   public class getmodulomodulofuncoeswcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00UR2( IGxContext context ,
                                             short AV30FuncaoUsuario_NomeOperator ,
                                             String AV31FuncaoUsuario_Nome ,
                                             String AV11TFFuncaoUsuario_Nome_Sel ,
                                             String AV10TFFuncaoUsuario_Nome ,
                                             String A162FuncaoUsuario_Nome ,
                                             int AV32Modulo_Codigo ,
                                             int A146Modulo_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [5] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[FuncaoUsuario_Codigo], T1.[Modulo_Codigo], T2.[FuncaoUsuario_Nome] FROM ([ModuloFuncoes1] T1 WITH (NOLOCK) INNER JOIN [ModuloFuncoes] T2 WITH (NOLOCK) ON T2.[FuncaoUsuario_Codigo] = T1.[FuncaoUsuario_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Modulo_Codigo] = @AV32Modulo_Codigo)";
         if ( ( AV30FuncaoUsuario_NomeOperator == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31FuncaoUsuario_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoUsuario_Nome] like @lV31FuncaoUsuario_Nome)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( AV30FuncaoUsuario_NomeOperator == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31FuncaoUsuario_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoUsuario_Nome] like '%' + @lV31FuncaoUsuario_Nome)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoUsuario_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFFuncaoUsuario_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoUsuario_Nome] like @lV10TFFuncaoUsuario_Nome)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFFuncaoUsuario_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoUsuario_Nome] = @AV11TFFuncaoUsuario_Nome_Sel)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Modulo_Codigo], T1.[FuncaoUsuario_Codigo]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00UR2(context, (short)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (int)dynConstraints[5] , (int)dynConstraints[6] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00UR2 ;
          prmP00UR2 = new Object[] {
          new Object[] {"@AV32Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV31FuncaoUsuario_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV31FuncaoUsuario_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV10TFFuncaoUsuario_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV11TFFuncaoUsuario_Nome_Sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00UR2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UR2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getmodulomodulofuncoeswcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getmodulomodulofuncoeswcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getmodulomodulofuncoeswcfilterdata") )
          {
             return  ;
          }
          getmodulomodulofuncoeswcfilterdata worker = new getmodulomodulofuncoeswcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
