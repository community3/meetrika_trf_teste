/*
               File: PRC_SaldoContratoExcluir
        Description: Excluir Saldo Contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:35.21
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_saldocontratoexcluir : GXProcedure
   {
      public prc_saldocontratoexcluir( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_saldocontratoexcluir( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_SaldoContrato_Codigo )
      {
         this.AV12SaldoContrato_Codigo = aP0_SaldoContrato_Codigo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_SaldoContrato_Codigo )
      {
         prc_saldocontratoexcluir objprc_saldocontratoexcluir;
         objprc_saldocontratoexcluir = new prc_saldocontratoexcluir();
         objprc_saldocontratoexcluir.AV12SaldoContrato_Codigo = aP0_SaldoContrato_Codigo;
         objprc_saldocontratoexcluir.context.SetSubmitInitialConfig(context);
         objprc_saldocontratoexcluir.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_saldocontratoexcluir);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_saldocontratoexcluir)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00BF2 */
         pr_default.execute(0, new Object[] {AV12SaldoContrato_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1561SaldoContrato_Codigo = P00BF2_A1561SaldoContrato_Codigo[0];
            /* Optimized DELETE. */
            /* Using cursor P00BF3 */
            pr_default.execute(1);
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("HistoricoConsumo") ;
            /* End optimized DELETE. */
            /* Using cursor P00BF4 */
            pr_default.execute(2, new Object[] {A1561SaldoContrato_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("SaldoContrato") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_SaldoContratoExcluir");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00BF2_A1561SaldoContrato_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_saldocontratoexcluir__default(),
            new Object[][] {
                new Object[] {
               P00BF2_A1561SaldoContrato_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV12SaldoContrato_Codigo ;
      private int A1561SaldoContrato_Codigo ;
      private String scmdbuf ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00BF2_A1561SaldoContrato_Codigo ;
   }

   public class prc_saldocontratoexcluir__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00BF2 ;
          prmP00BF2 = new Object[] {
          new Object[] {"@AV12SaldoContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BF3 ;
          prmP00BF3 = new Object[] {
          } ;
          Object[] prmP00BF4 ;
          prmP00BF4 = new Object[] {
          new Object[] {"@SaldoContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00BF2", "SELECT [SaldoContrato_Codigo] FROM [SaldoContrato] WITH (UPDLOCK) WHERE [SaldoContrato_Codigo] = @AV12SaldoContrato_Codigo ORDER BY [SaldoContrato_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BF2,1,0,true,true )
             ,new CursorDef("P00BF3", "DELETE FROM [HistoricoConsumo] ", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00BF3)
             ,new CursorDef("P00BF4", "DELETE FROM [SaldoContrato]  WHERE [SaldoContrato_Codigo] = @SaldoContrato_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00BF4)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
