/*
               File: Artefatos
        Description: Artefatos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:8:57.17
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class artefatos : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Artefatos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Artefatos_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vARTEFATOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Artefatos_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Artefatos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtArtefatos_Descricao_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public artefatos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public artefatos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Artefatos_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Artefatos_Codigo = aP1_Artefatos_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_4B191( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_4B191e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtArtefatos_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1749Artefatos_Codigo), 6, 0, ",", "")), ((edtArtefatos_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1749Artefatos_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1749Artefatos_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtArtefatos_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtArtefatos_Codigo_Visible, edtArtefatos_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Artefatos.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_4B191( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_4B191( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_4B191e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_26_4B191( true) ;
         }
         return  ;
      }

      protected void wb_table3_26_4B191e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_4B191e( true) ;
         }
         else
         {
            wb_table1_2_4B191e( false) ;
         }
      }

      protected void wb_table3_26_4B191( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Artefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Artefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Artefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_26_4B191e( true) ;
         }
         else
         {
            wb_table3_26_4B191e( false) ;
         }
      }

      protected void wb_table2_5_4B191( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_4B191( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_4B191e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_4B191e( true) ;
         }
         else
         {
            wb_table2_5_4B191e( false) ;
         }
      }

      protected void wb_table4_13_4B191( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockartefatos_descricao_Internalname, "Descri��o", "", "", lblTextblockartefatos_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Artefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtArtefatos_Descricao_Internalname, A1751Artefatos_Descricao, StringUtil.RTrim( context.localUtil.Format( A1751Artefatos_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtArtefatos_Descricao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtArtefatos_Descricao_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_Artefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockartefatos_arquivo_Internalname, "Anexo", "", "", lblTextblockartefatos_arquivo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Artefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            ClassString = "BootstrapAttribute";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            edtArtefatos_Arquivo_Filetype = "tmp";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtArtefatos_Arquivo_Internalname, "Filetype", edtArtefatos_Arquivo_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A2129Artefatos_Arquivo)) )
            {
               gxblobfileaux.Source = A2129Artefatos_Arquivo;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtArtefatos_Arquivo_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtArtefatos_Arquivo_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  A2129Artefatos_Arquivo = gxblobfileaux.GetAbsoluteName();
                  n2129Artefatos_Arquivo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2129Artefatos_Arquivo", A2129Artefatos_Arquivo);
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtArtefatos_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A2129Artefatos_Arquivo));
                  edtArtefatos_Arquivo_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtArtefatos_Arquivo_Internalname, "Filetype", edtArtefatos_Arquivo_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtArtefatos_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A2129Artefatos_Arquivo));
            }
            GxWebStd.gx_blob_field( context, edtArtefatos_Arquivo_Internalname, StringUtil.RTrim( A2129Artefatos_Arquivo), context.PathToRelativeUrl( A2129Artefatos_Arquivo), (String.IsNullOrEmpty(StringUtil.RTrim( edtArtefatos_Arquivo_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtArtefatos_Arquivo_Filetype)) ? A2129Artefatos_Arquivo : edtArtefatos_Arquivo_Filetype)) : edtArtefatos_Arquivo_Contenttype), false, "", edtArtefatos_Arquivo_Parameters, 0, edtArtefatos_Arquivo_Enabled, 1, "", "", 0, -1, 250, "px", 60, "px", 0, 0, 0, edtArtefatos_Arquivo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", "", "", "HLP_Artefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_4B191e( true) ;
         }
         else
         {
            wb_table4_13_4B191e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E114B2 */
         E114B2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A1751Artefatos_Descricao = StringUtil.Upper( cgiGet( edtArtefatos_Descricao_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1751Artefatos_Descricao", A1751Artefatos_Descricao);
               A2129Artefatos_Arquivo = cgiGet( edtArtefatos_Arquivo_Internalname);
               n2129Artefatos_Arquivo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2129Artefatos_Arquivo", A2129Artefatos_Arquivo);
               n2129Artefatos_Arquivo = (String.IsNullOrEmpty(StringUtil.RTrim( A2129Artefatos_Arquivo)) ? true : false);
               A1749Artefatos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtArtefatos_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)));
               /* Read saved values. */
               Z1749Artefatos_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1749Artefatos_Codigo"), ",", "."));
               Z1751Artefatos_Descricao = cgiGet( "Z1751Artefatos_Descricao");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV7Artefatos_Codigo = (int)(context.localUtil.CToN( cgiGet( "vARTEFATOS_CODIGO"), ",", "."));
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               edtArtefatos_Arquivo_Filename = cgiGet( "ARTEFATOS_ARQUIVO_Filename");
               edtArtefatos_Arquivo_Filetype = cgiGet( "ARTEFATOS_ARQUIVO_Filetype");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A2129Artefatos_Arquivo)) )
               {
                  edtArtefatos_Arquivo_Filename = (String)(CGIGetFileName(edtArtefatos_Arquivo_Internalname));
                  edtArtefatos_Arquivo_Filetype = (String)(CGIGetFileType(edtArtefatos_Arquivo_Internalname));
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( A2129Artefatos_Arquivo)) )
               {
                  GXCCtlgxBlob = "ARTEFATOS_ARQUIVO" + "_gxBlob";
                  A2129Artefatos_Arquivo = cgiGet( GXCCtlgxBlob);
                  n2129Artefatos_Arquivo = false;
                  n2129Artefatos_Arquivo = (String.IsNullOrEmpty(StringUtil.RTrim( A2129Artefatos_Arquivo)) ? true : false);
               }
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Artefatos";
               A1749Artefatos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtArtefatos_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1749Artefatos_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1749Artefatos_Codigo != Z1749Artefatos_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("artefatos:[SecurityCheckFailed value for]"+"Artefatos_Codigo:"+context.localUtil.Format( (decimal)(A1749Artefatos_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("artefatos:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1749Artefatos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode191 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode191;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound191 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_4B0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "ARTEFATOS_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtArtefatos_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E114B2 */
                           E114B2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E124B2 */
                           E124B2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E124B2 */
            E124B2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll4B191( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes4B191( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_4B0( )
      {
         BeforeValidate4B191( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls4B191( ) ;
            }
            else
            {
               CheckExtendedTable4B191( ) ;
               CloseExtendedTableCursors4B191( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption4B0( )
      {
      }

      protected void E114B2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         edtArtefatos_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtArtefatos_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtArtefatos_Codigo_Visible), 5, 0)));
      }

      protected void E124B2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwartefatos.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM4B191( short GX_JID )
      {
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1751Artefatos_Descricao = T004B3_A1751Artefatos_Descricao[0];
            }
            else
            {
               Z1751Artefatos_Descricao = A1751Artefatos_Descricao;
            }
         }
         if ( GX_JID == -4 )
         {
            Z1749Artefatos_Codigo = A1749Artefatos_Codigo;
            Z1751Artefatos_Descricao = A1751Artefatos_Descricao;
            Z2129Artefatos_Arquivo = A2129Artefatos_Arquivo;
         }
      }

      protected void standaloneNotModal( )
      {
         edtArtefatos_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtArtefatos_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtArtefatos_Codigo_Enabled), 5, 0)));
         edtArtefatos_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtArtefatos_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtArtefatos_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7Artefatos_Codigo) )
         {
            A1749Artefatos_Codigo = AV7Artefatos_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
      }

      protected void Load4B191( )
      {
         /* Using cursor T004B4 */
         pr_default.execute(2, new Object[] {A1749Artefatos_Codigo});
         if ( (pr_default.getStatus(2) != 101) )
         {
            RcdFound191 = 1;
            A1751Artefatos_Descricao = T004B4_A1751Artefatos_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1751Artefatos_Descricao", A1751Artefatos_Descricao);
            A2129Artefatos_Arquivo = T004B4_A2129Artefatos_Arquivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2129Artefatos_Arquivo", A2129Artefatos_Arquivo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtArtefatos_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A2129Artefatos_Arquivo));
            n2129Artefatos_Arquivo = T004B4_n2129Artefatos_Arquivo[0];
            ZM4B191( -4) ;
         }
         pr_default.close(2);
         OnLoadActions4B191( ) ;
      }

      protected void OnLoadActions4B191( )
      {
      }

      protected void CheckExtendedTable4B191( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A1751Artefatos_Descricao)) )
         {
            GX_msglist.addItem("Descri��o � obrigat�rio.", 1, "ARTEFATOS_DESCRICAO");
            AnyError = 1;
            GX_FocusControl = edtArtefatos_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors4B191( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey4B191( )
      {
         /* Using cursor T004B5 */
         pr_default.execute(3, new Object[] {A1749Artefatos_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound191 = 1;
         }
         else
         {
            RcdFound191 = 0;
         }
         pr_default.close(3);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T004B3 */
         pr_default.execute(1, new Object[] {A1749Artefatos_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4B191( 4) ;
            RcdFound191 = 1;
            A1749Artefatos_Codigo = T004B3_A1749Artefatos_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)));
            A1751Artefatos_Descricao = T004B3_A1751Artefatos_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1751Artefatos_Descricao", A1751Artefatos_Descricao);
            A2129Artefatos_Arquivo = T004B3_A2129Artefatos_Arquivo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2129Artefatos_Arquivo", A2129Artefatos_Arquivo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtArtefatos_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A2129Artefatos_Arquivo));
            n2129Artefatos_Arquivo = T004B3_n2129Artefatos_Arquivo[0];
            Z1749Artefatos_Codigo = A1749Artefatos_Codigo;
            sMode191 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load4B191( ) ;
            if ( AnyError == 1 )
            {
               RcdFound191 = 0;
               InitializeNonKey4B191( ) ;
            }
            Gx_mode = sMode191;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound191 = 0;
            InitializeNonKey4B191( ) ;
            sMode191 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode191;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4B191( ) ;
         if ( RcdFound191 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound191 = 0;
         /* Using cursor T004B6 */
         pr_default.execute(4, new Object[] {A1749Artefatos_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            while ( (pr_default.getStatus(4) != 101) && ( ( T004B6_A1749Artefatos_Codigo[0] < A1749Artefatos_Codigo ) ) )
            {
               pr_default.readNext(4);
            }
            if ( (pr_default.getStatus(4) != 101) && ( ( T004B6_A1749Artefatos_Codigo[0] > A1749Artefatos_Codigo ) ) )
            {
               A1749Artefatos_Codigo = T004B6_A1749Artefatos_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)));
               RcdFound191 = 1;
            }
         }
         pr_default.close(4);
      }

      protected void move_previous( )
      {
         RcdFound191 = 0;
         /* Using cursor T004B7 */
         pr_default.execute(5, new Object[] {A1749Artefatos_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            while ( (pr_default.getStatus(5) != 101) && ( ( T004B7_A1749Artefatos_Codigo[0] > A1749Artefatos_Codigo ) ) )
            {
               pr_default.readNext(5);
            }
            if ( (pr_default.getStatus(5) != 101) && ( ( T004B7_A1749Artefatos_Codigo[0] < A1749Artefatos_Codigo ) ) )
            {
               A1749Artefatos_Codigo = T004B7_A1749Artefatos_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)));
               RcdFound191 = 1;
            }
         }
         pr_default.close(5);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey4B191( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtArtefatos_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert4B191( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound191 == 1 )
            {
               if ( A1749Artefatos_Codigo != Z1749Artefatos_Codigo )
               {
                  A1749Artefatos_Codigo = Z1749Artefatos_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "ARTEFATOS_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtArtefatos_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtArtefatos_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update4B191( ) ;
                  GX_FocusControl = edtArtefatos_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1749Artefatos_Codigo != Z1749Artefatos_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtArtefatos_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert4B191( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "ARTEFATOS_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtArtefatos_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtArtefatos_Descricao_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert4B191( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A1749Artefatos_Codigo != Z1749Artefatos_Codigo )
         {
            A1749Artefatos_Codigo = Z1749Artefatos_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "ARTEFATOS_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtArtefatos_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtArtefatos_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency4B191( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T004B2 */
            pr_default.execute(0, new Object[] {A1749Artefatos_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Artefatos"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1751Artefatos_Descricao, T004B2_A1751Artefatos_Descricao[0]) != 0 ) )
            {
               if ( StringUtil.StrCmp(Z1751Artefatos_Descricao, T004B2_A1751Artefatos_Descricao[0]) != 0 )
               {
                  GXUtil.WriteLog("artefatos:[seudo value changed for attri]"+"Artefatos_Descricao");
                  GXUtil.WriteLogRaw("Old: ",Z1751Artefatos_Descricao);
                  GXUtil.WriteLogRaw("Current: ",T004B2_A1751Artefatos_Descricao[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Artefatos"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4B191( )
      {
         BeforeValidate4B191( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4B191( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4B191( 0) ;
            CheckOptimisticConcurrency4B191( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4B191( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4B191( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004B8 */
                     pr_default.execute(6, new Object[] {A1751Artefatos_Descricao, n2129Artefatos_Arquivo, A2129Artefatos_Arquivo});
                     A1749Artefatos_Codigo = T004B8_A1749Artefatos_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)));
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("Artefatos") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption4B0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4B191( ) ;
            }
            EndLevel4B191( ) ;
         }
         CloseExtendedTableCursors4B191( ) ;
      }

      protected void Update4B191( )
      {
         BeforeValidate4B191( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4B191( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4B191( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4B191( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4B191( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004B9 */
                     pr_default.execute(7, new Object[] {A1751Artefatos_Descricao, A1749Artefatos_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("Artefatos") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Artefatos"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate4B191( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4B191( ) ;
         }
         CloseExtendedTableCursors4B191( ) ;
      }

      protected void DeferredUpdate4B191( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor T004B10 */
            pr_default.execute(8, new Object[] {n2129Artefatos_Arquivo, A2129Artefatos_Arquivo, A1749Artefatos_Codigo});
            pr_default.close(8);
            dsDefault.SmartCacheProvider.SetUpdated("Artefatos") ;
         }
      }

      protected void delete( )
      {
         BeforeValidate4B191( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4B191( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4B191( ) ;
            AfterConfirm4B191( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4B191( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T004B11 */
                  pr_default.execute(9, new Object[] {A1749Artefatos_Codigo});
                  pr_default.close(9);
                  dsDefault.SmartCacheProvider.SetUpdated("Artefatos") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode191 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel4B191( ) ;
         Gx_mode = sMode191;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls4B191( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
         if ( AnyError == 0 )
         {
            /* Using cursor T004B12 */
            pr_default.execute(10, new Object[] {A1749Artefatos_Codigo});
            if ( (pr_default.getStatus(10) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Artefatos das OS"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(10);
            /* Using cursor T004B13 */
            pr_default.execute(11, new Object[] {A1749Artefatos_Codigo});
            if ( (pr_default.getStatus(11) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Artefatos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(11);
            /* Using cursor T004B14 */
            pr_default.execute(12, new Object[] {A1749Artefatos_Codigo});
            if ( (pr_default.getStatus(12) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato Servicos Artefatos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(12);
            /* Using cursor T004B15 */
            pr_default.execute(13, new Object[] {A1749Artefatos_Codigo});
            if ( (pr_default.getStatus(13) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Artefatos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(13);
         }
      }

      protected void EndLevel4B191( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4B191( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "Artefatos");
            if ( AnyError == 0 )
            {
               ConfirmValues4B0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "Artefatos");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart4B191( )
      {
         /* Scan By routine */
         /* Using cursor T004B16 */
         pr_default.execute(14);
         RcdFound191 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound191 = 1;
            A1749Artefatos_Codigo = T004B16_A1749Artefatos_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext4B191( )
      {
         /* Scan next routine */
         pr_default.readNext(14);
         RcdFound191 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound191 = 1;
            A1749Artefatos_Codigo = T004B16_A1749Artefatos_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd4B191( )
      {
         pr_default.close(14);
      }

      protected void AfterConfirm4B191( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4B191( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4B191( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4B191( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4B191( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4B191( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4B191( )
      {
         edtArtefatos_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtArtefatos_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtArtefatos_Descricao_Enabled), 5, 0)));
         edtArtefatos_Arquivo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtArtefatos_Arquivo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtArtefatos_Arquivo_Enabled), 5, 0)));
         edtArtefatos_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtArtefatos_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtArtefatos_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues4B0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282385779");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("artefatos.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Artefatos_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1749Artefatos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1751Artefatos_Descricao", Z1751Artefatos_Descricao);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vARTEFATOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Artefatos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vARTEFATOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Artefatos_Codigo), "ZZZZZ9")));
         GXCCtlgxBlob = "ARTEFATOS_ARQUIVO" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, A2129Artefatos_Arquivo);
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GxWebStd.gx_hidden_field( context, "ARTEFATOS_ARQUIVO_Filename", StringUtil.RTrim( edtArtefatos_Arquivo_Filename));
         GxWebStd.gx_hidden_field( context, "ARTEFATOS_ARQUIVO_Filetype", StringUtil.RTrim( edtArtefatos_Arquivo_Filetype));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Artefatos";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1749Artefatos_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("artefatos:[SendSecurityCheck value for]"+"Artefatos_Codigo:"+context.localUtil.Format( (decimal)(A1749Artefatos_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("artefatos:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("artefatos.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Artefatos_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Artefatos" ;
      }

      public override String GetPgmdesc( )
      {
         return "Artefatos" ;
      }

      protected void InitializeNonKey4B191( )
      {
         A1751Artefatos_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1751Artefatos_Descricao", A1751Artefatos_Descricao);
         A2129Artefatos_Arquivo = "";
         n2129Artefatos_Arquivo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2129Artefatos_Arquivo", A2129Artefatos_Arquivo);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtArtefatos_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A2129Artefatos_Arquivo));
         n2129Artefatos_Arquivo = (String.IsNullOrEmpty(StringUtil.RTrim( A2129Artefatos_Arquivo)) ? true : false);
         Z1751Artefatos_Descricao = "";
      }

      protected void InitAll4B191( )
      {
         A1749Artefatos_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1749Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1749Artefatos_Codigo), 6, 0)));
         InitializeNonKey4B191( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282385791");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("artefatos.js", "?20204282385791");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockartefatos_descricao_Internalname = "TEXTBLOCKARTEFATOS_DESCRICAO";
         edtArtefatos_Descricao_Internalname = "ARTEFATOS_DESCRICAO";
         lblTextblockartefatos_arquivo_Internalname = "TEXTBLOCKARTEFATOS_ARQUIVO";
         edtArtefatos_Arquivo_Internalname = "ARTEFATOS_ARQUIVO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtArtefatos_Codigo_Internalname = "ARTEFATOS_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtArtefatos_Arquivo_Filename = "";
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Artefato";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Artefatos";
         edtArtefatos_Arquivo_Jsonclick = "";
         edtArtefatos_Arquivo_Parameters = "";
         edtArtefatos_Arquivo_Contenttype = "";
         edtArtefatos_Arquivo_Filetype = "";
         edtArtefatos_Arquivo_Enabled = 1;
         edtArtefatos_Descricao_Jsonclick = "";
         edtArtefatos_Descricao_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtArtefatos_Codigo_Jsonclick = "";
         edtArtefatos_Codigo_Enabled = 0;
         edtArtefatos_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Artefatos_Codigo',fld:'vARTEFATOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E124B2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z1751Artefatos_Descricao = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockartefatos_descricao_Jsonclick = "";
         A1751Artefatos_Descricao = "";
         lblTextblockartefatos_arquivo_Jsonclick = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         A2129Artefatos_Arquivo = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         GXCCtlgxBlob = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode191 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         Z2129Artefatos_Arquivo = "";
         T004B4_A1749Artefatos_Codigo = new int[1] ;
         T004B4_A1751Artefatos_Descricao = new String[] {""} ;
         T004B4_A2129Artefatos_Arquivo = new String[] {""} ;
         T004B4_n2129Artefatos_Arquivo = new bool[] {false} ;
         T004B5_A1749Artefatos_Codigo = new int[1] ;
         T004B3_A1749Artefatos_Codigo = new int[1] ;
         T004B3_A1751Artefatos_Descricao = new String[] {""} ;
         T004B3_A2129Artefatos_Arquivo = new String[] {""} ;
         T004B3_n2129Artefatos_Arquivo = new bool[] {false} ;
         T004B6_A1749Artefatos_Codigo = new int[1] ;
         T004B7_A1749Artefatos_Codigo = new int[1] ;
         T004B2_A1749Artefatos_Codigo = new int[1] ;
         T004B2_A1751Artefatos_Descricao = new String[] {""} ;
         T004B2_A2129Artefatos_Arquivo = new String[] {""} ;
         T004B2_n2129Artefatos_Arquivo = new bool[] {false} ;
         T004B8_A1749Artefatos_Codigo = new int[1] ;
         T004B12_A1769ContagemResultadoArtefato_Codigo = new int[1] ;
         T004B13_A155Servico_Codigo = new int[1] ;
         T004B13_A1766ServicoArtefato_ArtefatoCod = new int[1] ;
         T004B14_A160ContratoServicos_Codigo = new int[1] ;
         T004B14_A1749Artefatos_Codigo = new int[1] ;
         T004B15_A1750CatalogoSutentacao_Codigo = new int[1] ;
         T004B15_A1749Artefatos_Codigo = new int[1] ;
         T004B16_A1749Artefatos_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.artefatos__default(),
            new Object[][] {
                new Object[] {
               T004B2_A1749Artefatos_Codigo, T004B2_A1751Artefatos_Descricao, T004B2_A2129Artefatos_Arquivo, T004B2_n2129Artefatos_Arquivo
               }
               , new Object[] {
               T004B3_A1749Artefatos_Codigo, T004B3_A1751Artefatos_Descricao, T004B3_A2129Artefatos_Arquivo, T004B3_n2129Artefatos_Arquivo
               }
               , new Object[] {
               T004B4_A1749Artefatos_Codigo, T004B4_A1751Artefatos_Descricao, T004B4_A2129Artefatos_Arquivo, T004B4_n2129Artefatos_Arquivo
               }
               , new Object[] {
               T004B5_A1749Artefatos_Codigo
               }
               , new Object[] {
               T004B6_A1749Artefatos_Codigo
               }
               , new Object[] {
               T004B7_A1749Artefatos_Codigo
               }
               , new Object[] {
               T004B8_A1749Artefatos_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T004B12_A1769ContagemResultadoArtefato_Codigo
               }
               , new Object[] {
               T004B13_A155Servico_Codigo, T004B13_A1766ServicoArtefato_ArtefatoCod
               }
               , new Object[] {
               T004B14_A160ContratoServicos_Codigo, T004B14_A1749Artefatos_Codigo
               }
               , new Object[] {
               T004B15_A1750CatalogoSutentacao_Codigo, T004B15_A1749Artefatos_Codigo
               }
               , new Object[] {
               T004B16_A1749Artefatos_Codigo
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound191 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private int wcpOAV7Artefatos_Codigo ;
      private int Z1749Artefatos_Codigo ;
      private int AV7Artefatos_Codigo ;
      private int trnEnded ;
      private int A1749Artefatos_Codigo ;
      private int edtArtefatos_Codigo_Enabled ;
      private int edtArtefatos_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtArtefatos_Descricao_Enabled ;
      private int edtArtefatos_Arquivo_Enabled ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtArtefatos_Descricao_Internalname ;
      private String edtArtefatos_Codigo_Internalname ;
      private String edtArtefatos_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockartefatos_descricao_Internalname ;
      private String lblTextblockartefatos_descricao_Jsonclick ;
      private String edtArtefatos_Descricao_Jsonclick ;
      private String lblTextblockartefatos_arquivo_Internalname ;
      private String lblTextblockartefatos_arquivo_Jsonclick ;
      private String edtArtefatos_Arquivo_Filetype ;
      private String edtArtefatos_Arquivo_Internalname ;
      private String edtArtefatos_Arquivo_Contenttype ;
      private String edtArtefatos_Arquivo_Parameters ;
      private String edtArtefatos_Arquivo_Jsonclick ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String edtArtefatos_Arquivo_Filename ;
      private String GXCCtlgxBlob ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode191 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n2129Artefatos_Arquivo ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private String Z1751Artefatos_Descricao ;
      private String A1751Artefatos_Descricao ;
      private String A2129Artefatos_Arquivo ;
      private String Z2129Artefatos_Arquivo ;
      private IGxSession AV10WebSession ;
      private GxFile gxblobfileaux ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] T004B4_A1749Artefatos_Codigo ;
      private String[] T004B4_A1751Artefatos_Descricao ;
      private String[] T004B4_A2129Artefatos_Arquivo ;
      private bool[] T004B4_n2129Artefatos_Arquivo ;
      private int[] T004B5_A1749Artefatos_Codigo ;
      private int[] T004B3_A1749Artefatos_Codigo ;
      private String[] T004B3_A1751Artefatos_Descricao ;
      private String[] T004B3_A2129Artefatos_Arquivo ;
      private bool[] T004B3_n2129Artefatos_Arquivo ;
      private int[] T004B6_A1749Artefatos_Codigo ;
      private int[] T004B7_A1749Artefatos_Codigo ;
      private int[] T004B2_A1749Artefatos_Codigo ;
      private String[] T004B2_A1751Artefatos_Descricao ;
      private String[] T004B2_A2129Artefatos_Arquivo ;
      private bool[] T004B2_n2129Artefatos_Arquivo ;
      private int[] T004B8_A1749Artefatos_Codigo ;
      private int[] T004B12_A1769ContagemResultadoArtefato_Codigo ;
      private int[] T004B13_A155Servico_Codigo ;
      private int[] T004B13_A1766ServicoArtefato_ArtefatoCod ;
      private int[] T004B14_A160ContratoServicos_Codigo ;
      private int[] T004B14_A1749Artefatos_Codigo ;
      private int[] T004B15_A1750CatalogoSutentacao_Codigo ;
      private int[] T004B15_A1749Artefatos_Codigo ;
      private int[] T004B16_A1749Artefatos_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
   }

   public class artefatos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT004B4 ;
          prmT004B4 = new Object[] {
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004B5 ;
          prmT004B5 = new Object[] {
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004B3 ;
          prmT004B3 = new Object[] {
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004B6 ;
          prmT004B6 = new Object[] {
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004B7 ;
          prmT004B7 = new Object[] {
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004B2 ;
          prmT004B2 = new Object[] {
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004B8 ;
          prmT004B8 = new Object[] {
          new Object[] {"@Artefatos_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Artefatos_Arquivo",SqlDbType.VarBinary,1024,0}
          } ;
          Object[] prmT004B9 ;
          prmT004B9 = new Object[] {
          new Object[] {"@Artefatos_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004B10 ;
          prmT004B10 = new Object[] {
          new Object[] {"@Artefatos_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004B11 ;
          prmT004B11 = new Object[] {
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004B12 ;
          prmT004B12 = new Object[] {
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004B13 ;
          prmT004B13 = new Object[] {
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004B14 ;
          prmT004B14 = new Object[] {
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004B15 ;
          prmT004B15 = new Object[] {
          new Object[] {"@Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004B16 ;
          prmT004B16 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T004B2", "SELECT [Artefatos_Codigo], [Artefatos_Descricao], [Artefatos_Arquivo] FROM [Artefatos] WITH (UPDLOCK) WHERE [Artefatos_Codigo] = @Artefatos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004B2,1,0,true,false )
             ,new CursorDef("T004B3", "SELECT [Artefatos_Codigo], [Artefatos_Descricao], [Artefatos_Arquivo] FROM [Artefatos] WITH (NOLOCK) WHERE [Artefatos_Codigo] = @Artefatos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004B3,1,0,true,false )
             ,new CursorDef("T004B4", "SELECT TM1.[Artefatos_Codigo], TM1.[Artefatos_Descricao], TM1.[Artefatos_Arquivo] FROM [Artefatos] TM1 WITH (NOLOCK) WHERE TM1.[Artefatos_Codigo] = @Artefatos_Codigo ORDER BY TM1.[Artefatos_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004B4,100,0,true,false )
             ,new CursorDef("T004B5", "SELECT [Artefatos_Codigo] FROM [Artefatos] WITH (NOLOCK) WHERE [Artefatos_Codigo] = @Artefatos_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004B5,1,0,true,false )
             ,new CursorDef("T004B6", "SELECT TOP 1 [Artefatos_Codigo] FROM [Artefatos] WITH (NOLOCK) WHERE ( [Artefatos_Codigo] > @Artefatos_Codigo) ORDER BY [Artefatos_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004B6,1,0,true,true )
             ,new CursorDef("T004B7", "SELECT TOP 1 [Artefatos_Codigo] FROM [Artefatos] WITH (NOLOCK) WHERE ( [Artefatos_Codigo] < @Artefatos_Codigo) ORDER BY [Artefatos_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004B7,1,0,true,true )
             ,new CursorDef("T004B8", "INSERT INTO [Artefatos]([Artefatos_Descricao], [Artefatos_Arquivo]) VALUES(@Artefatos_Descricao, @Artefatos_Arquivo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT004B8)
             ,new CursorDef("T004B9", "UPDATE [Artefatos] SET [Artefatos_Descricao]=@Artefatos_Descricao  WHERE [Artefatos_Codigo] = @Artefatos_Codigo", GxErrorMask.GX_NOMASK,prmT004B9)
             ,new CursorDef("T004B10", "UPDATE [Artefatos] SET [Artefatos_Arquivo]=@Artefatos_Arquivo  WHERE [Artefatos_Codigo] = @Artefatos_Codigo", GxErrorMask.GX_NOMASK,prmT004B10)
             ,new CursorDef("T004B11", "DELETE FROM [Artefatos]  WHERE [Artefatos_Codigo] = @Artefatos_Codigo", GxErrorMask.GX_NOMASK,prmT004B11)
             ,new CursorDef("T004B12", "SELECT TOP 1 [ContagemResultadoArtefato_Codigo] FROM [ContagemResultadoArtefato] WITH (NOLOCK) WHERE [ContagemResultadoArtefato_ArtefatoCod] = @Artefatos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004B12,1,0,true,true )
             ,new CursorDef("T004B13", "SELECT TOP 1 [Servico_Codigo], [ServicoArtefato_ArtefatoCod] FROM [ServicoArtefatos] WITH (NOLOCK) WHERE [ServicoArtefato_ArtefatoCod] = @Artefatos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004B13,1,0,true,true )
             ,new CursorDef("T004B14", "SELECT TOP 1 [ContratoServicos_Codigo], [Artefatos_Codigo] FROM [ContratoServicosArtefatos] WITH (NOLOCK) WHERE [Artefatos_Codigo] = @Artefatos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004B14,1,0,true,true )
             ,new CursorDef("T004B15", "SELECT TOP 1 [CatalogoSutentacao_Codigo], [Artefatos_Codigo] FROM [CatalogoSutentacaoArtefatos] WITH (NOLOCK) WHERE [Artefatos_Codigo] = @Artefatos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004B15,1,0,true,true )
             ,new CursorDef("T004B16", "SELECT [Artefatos_Codigo] FROM [Artefatos] WITH (NOLOCK) ORDER BY [Artefatos_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004B16,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getBLOBFile(3, "tmp", "") ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getBLOBFile(3, "tmp", "") ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getBLOBFile(3, "tmp", "") ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
