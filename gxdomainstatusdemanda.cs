/*
               File: StatusDemanda
        Description: StatusDemanda
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 14:20:27.43
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gxdomainstatusdemanda
   {
      private static Hashtable domain = new Hashtable();
      static gxdomainstatusdemanda ()
      {
         domain["B"] = "Stand by";
         domain["S"] = "Solicitada";
         domain["E"] = "Em An�lise";
         domain["A"] = "Em execu��o";
         domain["R"] = "Resolvida";
         domain["C"] = "Conferida";
         domain["D"] = "Retornada";
         domain["H"] = "Homologada";
         domain["O"] = "Aceite";
         domain["P"] = "A Pagar";
         domain["L"] = "Liquidada";
         domain["X"] = "Cancelada";
         domain["N"] = "N�o Faturada";
         domain["J"] = "Planejamento";
         domain["I"] = "An�lise Planejamento";
         domain["T"] = "Validacao T�cnica";
         domain["Q"] = "Validacao Qualidade";
         domain["G"] = "Em Homologa��o";
         domain["M"] = "Valida��o Mensura��o";
         domain["U"] = "Rascunho";
      }

      public static string getDescription( IGxContext context ,
                                           String key )
      {
         string rtkey ;
         rtkey = StringUtil.Trim( (String)(key));
         return (string)domain[rtkey] ;
      }

      public static GxSimpleCollection getValues( )
      {
         GxSimpleCollection value = new GxSimpleCollection();
         ArrayList aKeys = new ArrayList(domain.Keys);
         aKeys.Sort();
         foreach (String key in aKeys)
         {
            value.Add(key);
         }
         return value;
      }

   }

}
