/*
               File: type_SdtGAMRepositoryConnectionFilter
        Description: GAMRepositoryConnectionFilter
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 1:32:15.65
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMRepositoryConnectionFilter : GxUserType, IGxExternalObject
   {
      public SdtGAMRepositoryConnectionFilter( )
      {
         initialize();
      }

      public SdtGAMRepositoryConnectionFilter( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMRepositoryConnectionFilter_externalReference == null )
         {
            GAMRepositoryConnectionFilter_externalReference = new Artech.Security.GAMRepositoryConnectionFilter(context);
         }
         returntostring = "";
         returntostring = (String)(GAMRepositoryConnectionFilter_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Connectionname
      {
         get {
            if ( GAMRepositoryConnectionFilter_externalReference == null )
            {
               GAMRepositoryConnectionFilter_externalReference = new Artech.Security.GAMRepositoryConnectionFilter(context);
            }
            return GAMRepositoryConnectionFilter_externalReference.ConnectionName ;
         }

         set {
            if ( GAMRepositoryConnectionFilter_externalReference == null )
            {
               GAMRepositoryConnectionFilter_externalReference = new Artech.Security.GAMRepositoryConnectionFilter(context);
            }
            GAMRepositoryConnectionFilter_externalReference.ConnectionName = value;
         }

      }

      public String gxTpr_Connectiontype
      {
         get {
            if ( GAMRepositoryConnectionFilter_externalReference == null )
            {
               GAMRepositoryConnectionFilter_externalReference = new Artech.Security.GAMRepositoryConnectionFilter(context);
            }
            return GAMRepositoryConnectionFilter_externalReference.ConnectionType ;
         }

         set {
            if ( GAMRepositoryConnectionFilter_externalReference == null )
            {
               GAMRepositoryConnectionFilter_externalReference = new Artech.Security.GAMRepositoryConnectionFilter(context);
            }
            GAMRepositoryConnectionFilter_externalReference.ConnectionType = value;
         }

      }

      public String gxTpr_Username
      {
         get {
            if ( GAMRepositoryConnectionFilter_externalReference == null )
            {
               GAMRepositoryConnectionFilter_externalReference = new Artech.Security.GAMRepositoryConnectionFilter(context);
            }
            return GAMRepositoryConnectionFilter_externalReference.UserName ;
         }

         set {
            if ( GAMRepositoryConnectionFilter_externalReference == null )
            {
               GAMRepositoryConnectionFilter_externalReference = new Artech.Security.GAMRepositoryConnectionFilter(context);
            }
            GAMRepositoryConnectionFilter_externalReference.UserName = value;
         }

      }

      public int gxTpr_Start
      {
         get {
            if ( GAMRepositoryConnectionFilter_externalReference == null )
            {
               GAMRepositoryConnectionFilter_externalReference = new Artech.Security.GAMRepositoryConnectionFilter(context);
            }
            return GAMRepositoryConnectionFilter_externalReference.Start ;
         }

         set {
            if ( GAMRepositoryConnectionFilter_externalReference == null )
            {
               GAMRepositoryConnectionFilter_externalReference = new Artech.Security.GAMRepositoryConnectionFilter(context);
            }
            GAMRepositoryConnectionFilter_externalReference.Start = value;
         }

      }

      public int gxTpr_Limit
      {
         get {
            if ( GAMRepositoryConnectionFilter_externalReference == null )
            {
               GAMRepositoryConnectionFilter_externalReference = new Artech.Security.GAMRepositoryConnectionFilter(context);
            }
            return GAMRepositoryConnectionFilter_externalReference.Limit ;
         }

         set {
            if ( GAMRepositoryConnectionFilter_externalReference == null )
            {
               GAMRepositoryConnectionFilter_externalReference = new Artech.Security.GAMRepositoryConnectionFilter(context);
            }
            GAMRepositoryConnectionFilter_externalReference.Limit = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMRepositoryConnectionFilter_externalReference == null )
            {
               GAMRepositoryConnectionFilter_externalReference = new Artech.Security.GAMRepositoryConnectionFilter(context);
            }
            return GAMRepositoryConnectionFilter_externalReference ;
         }

         set {
            GAMRepositoryConnectionFilter_externalReference = (Artech.Security.GAMRepositoryConnectionFilter)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMRepositoryConnectionFilter GAMRepositoryConnectionFilter_externalReference=null ;
   }

}
