/*
               File: PRC_GetProdutividadeDia
        Description: Produtividade Diaria da Unidade no Contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:45.16
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_getprodutividadedia : GXProcedure
   {
      public prc_getprodutividadedia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_getprodutividadedia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratoServicos_Codigo ,
                           out decimal aP1_ProdutividadeDia )
      {
         this.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV8ProdutividadeDia = 0 ;
         initialize();
         executePrivate();
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP1_ProdutividadeDia=this.AV8ProdutividadeDia;
      }

      public decimal executeUdp( ref int aP0_ContratoServicos_Codigo )
      {
         this.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV8ProdutividadeDia = 0 ;
         initialize();
         executePrivate();
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP1_ProdutividadeDia=this.AV8ProdutividadeDia;
         return AV8ProdutividadeDia ;
      }

      public void executeSubmit( ref int aP0_ContratoServicos_Codigo ,
                                 out decimal aP1_ProdutividadeDia )
      {
         prc_getprodutividadedia objprc_getprodutividadedia;
         objprc_getprodutividadedia = new prc_getprodutividadedia();
         objprc_getprodutividadedia.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         objprc_getprodutividadedia.AV8ProdutividadeDia = 0 ;
         objprc_getprodutividadedia.context.SetSubmitInitialConfig(context);
         objprc_getprodutividadedia.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_getprodutividadedia);
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP1_ProdutividadeDia=this.AV8ProdutividadeDia;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_getprodutividadedia)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P007Z2 */
         pr_default.execute(0, new Object[] {A160ContratoServicos_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1212ContratoServicos_UnidadeContratada = P007Z2_A1212ContratoServicos_UnidadeContratada[0];
            A74Contrato_Codigo = P007Z2_A74Contrato_Codigo[0];
            /* Using cursor P007Z3 */
            pr_default.execute(1, new Object[] {A74Contrato_Codigo, A1212ContratoServicos_UnidadeContratada});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1207ContratoUnidades_ContratoCod = P007Z3_A1207ContratoUnidades_ContratoCod[0];
               A1204ContratoUnidades_UndMedCod = P007Z3_A1204ContratoUnidades_UndMedCod[0];
               A1208ContratoUnidades_Produtividade = P007Z3_A1208ContratoUnidades_Produtividade[0];
               n1208ContratoUnidades_Produtividade = P007Z3_n1208ContratoUnidades_Produtividade[0];
               AV8ProdutividadeDia = A1208ContratoUnidades_Produtividade;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P007Z2_A160ContratoServicos_Codigo = new int[1] ;
         P007Z2_A1212ContratoServicos_UnidadeContratada = new int[1] ;
         P007Z2_A74Contrato_Codigo = new int[1] ;
         P007Z3_A1207ContratoUnidades_ContratoCod = new int[1] ;
         P007Z3_A1204ContratoUnidades_UndMedCod = new int[1] ;
         P007Z3_A1208ContratoUnidades_Produtividade = new decimal[1] ;
         P007Z3_n1208ContratoUnidades_Produtividade = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_getprodutividadedia__default(),
            new Object[][] {
                new Object[] {
               P007Z2_A160ContratoServicos_Codigo, P007Z2_A1212ContratoServicos_UnidadeContratada, P007Z2_A74Contrato_Codigo
               }
               , new Object[] {
               P007Z3_A1207ContratoUnidades_ContratoCod, P007Z3_A1204ContratoUnidades_UndMedCod, P007Z3_A1208ContratoUnidades_Produtividade, P007Z3_n1208ContratoUnidades_Produtividade
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A160ContratoServicos_Codigo ;
      private int A1212ContratoServicos_UnidadeContratada ;
      private int A74Contrato_Codigo ;
      private int A1207ContratoUnidades_ContratoCod ;
      private int A1204ContratoUnidades_UndMedCod ;
      private decimal AV8ProdutividadeDia ;
      private decimal A1208ContratoUnidades_Produtividade ;
      private String scmdbuf ;
      private bool n1208ContratoUnidades_Produtividade ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratoServicos_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P007Z2_A160ContratoServicos_Codigo ;
      private int[] P007Z2_A1212ContratoServicos_UnidadeContratada ;
      private int[] P007Z2_A74Contrato_Codigo ;
      private int[] P007Z3_A1207ContratoUnidades_ContratoCod ;
      private int[] P007Z3_A1204ContratoUnidades_UndMedCod ;
      private decimal[] P007Z3_A1208ContratoUnidades_Produtividade ;
      private bool[] P007Z3_n1208ContratoUnidades_Produtividade ;
      private decimal aP1_ProdutividadeDia ;
   }

   public class prc_getprodutividadedia__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP007Z2 ;
          prmP007Z2 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP007Z3 ;
          prmP007Z3 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicos_UnidadeContratada",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P007Z2", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicos_UnidadeContratada], [Contrato_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007Z2,1,0,true,true )
             ,new CursorDef("P007Z3", "SELECT TOP 1 [ContratoUnidades_ContratoCod], [ContratoUnidades_UndMedCod], [ContratoUnidades_Produtividade] FROM [ContratoUnidades] WITH (NOLOCK) WHERE [ContratoUnidades_ContratoCod] = @Contrato_Codigo and [ContratoUnidades_UndMedCod] = @ContratoServicos_UnidadeContratada ORDER BY [ContratoUnidades_ContratoCod], [ContratoUnidades_UndMedCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007Z3,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
