/*
               File: jqSelectSample
        Description: jq Select Sample
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:4:56.1
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class jqselectsample : GXProcedure
   {
      public jqselectsample( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public jqselectsample( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( out IGxCollection aP0_Gxm2rootcol )
      {
         this.Gxm2rootcol = new GxObjectCollection( context, "jqSelectData.Item", "GxEv3Up14_MeetrikaVs3", "SdtjqSelectData_Item", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      public IGxCollection executeUdp( )
      {
         this.Gxm2rootcol = new GxObjectCollection( context, "jqSelectData.Item", "GxEv3Up14_MeetrikaVs3", "SdtjqSelectData_Item", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
         return Gxm2rootcol ;
      }

      public void executeSubmit( out IGxCollection aP0_Gxm2rootcol )
      {
         jqselectsample objjqselectsample;
         objjqselectsample = new jqselectsample();
         objjqselectsample.Gxm2rootcol = new GxObjectCollection( context, "jqSelectData.Item", "GxEv3Up14_MeetrikaVs3", "SdtjqSelectData_Item", "GeneXus.Programs") ;
         objjqselectsample.context.SetSubmitInitialConfig(context);
         objjqselectsample.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objjqselectsample);
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((jqselectsample)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         Gxm1jqselectdata = new SdtjqSelectData_Item(context);
         Gxm2rootcol.Add(Gxm1jqselectdata, 0);
         Gxm1jqselectdata.gxTpr_Id = "1";
         Gxm1jqselectdata.gxTpr_Descr = "Op��o 1";
         Gxm1jqselectdata.gxTpr_Selected = false;
         Gxm1jqselectdata = new SdtjqSelectData_Item(context);
         Gxm2rootcol.Add(Gxm1jqselectdata, 0);
         Gxm1jqselectdata.gxTpr_Id = "2";
         Gxm1jqselectdata.gxTpr_Descr = "Op��o 2";
         Gxm1jqselectdata.gxTpr_Selected = true;
         Gxm1jqselectdata = new SdtjqSelectData_Item(context);
         Gxm2rootcol.Add(Gxm1jqselectdata, 0);
         Gxm1jqselectdata.gxTpr_Id = "3";
         Gxm1jqselectdata.gxTpr_Descr = "Op��o 3";
         Gxm1jqselectdata.gxTpr_Selected = false;
         Gxm1jqselectdata = new SdtjqSelectData_Item(context);
         Gxm2rootcol.Add(Gxm1jqselectdata, 0);
         Gxm1jqselectdata.gxTpr_Id = "4";
         Gxm1jqselectdata.gxTpr_Descr = "Op��o 4";
         Gxm1jqselectdata.gxTpr_Selected = false;
         Gxm1jqselectdata = new SdtjqSelectData_Item(context);
         Gxm2rootcol.Add(Gxm1jqselectdata, 0);
         Gxm1jqselectdata.gxTpr_Id = "5";
         Gxm1jqselectdata.gxTpr_Descr = "Op��o 5";
         Gxm1jqselectdata.gxTpr_Selected = false;
         Gxm1jqselectdata = new SdtjqSelectData_Item(context);
         Gxm2rootcol.Add(Gxm1jqselectdata, 0);
         Gxm1jqselectdata.gxTpr_Id = "6";
         Gxm1jqselectdata.gxTpr_Descr = "Op��o 6";
         Gxm1jqselectdata.gxTpr_Selected = false;
         Gxm1jqselectdata = new SdtjqSelectData_Item(context);
         Gxm2rootcol.Add(Gxm1jqselectdata, 0);
         Gxm1jqselectdata.gxTpr_Id = "7";
         Gxm1jqselectdata.gxTpr_Descr = "Op��o 7";
         Gxm1jqselectdata.gxTpr_Selected = false;
         Gxm1jqselectdata = new SdtjqSelectData_Item(context);
         Gxm2rootcol.Add(Gxm1jqselectdata, 0);
         Gxm1jqselectdata.gxTpr_Id = "8";
         Gxm1jqselectdata.gxTpr_Descr = "Op��o 8";
         Gxm1jqselectdata.gxTpr_Selected = false;
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gxm1jqselectdata = new SdtjqSelectData_Item(context);
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private IGxCollection aP0_Gxm2rootcol ;
      [ObjectCollection(ItemType=typeof( SdtjqSelectData_Item ))]
      private IGxCollection Gxm2rootcol ;
      private SdtjqSelectData_Item Gxm1jqselectdata ;
   }

}
