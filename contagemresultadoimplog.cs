/*
               File: ContagemResultadoImpLog
        Description: Contagem Resultado Importa��o Log
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:31:13.15
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadoimplog : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_3") == 0 )
         {
            A1038ContagemResultadoImpLog_UserCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1038ContagemResultadoImpLog_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1038ContagemResultadoImpLog_UserCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_3( A1038ContagemResultadoImpLog_UserCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_4") == 0 )
         {
            A1039ContagemResultadoImpLog_UserPesCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1039ContagemResultadoImpLog_UserPesCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1039ContagemResultadoImpLog_UserPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1039ContagemResultadoImpLog_UserPesCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_4( A1039ContagemResultadoImpLog_UserPesCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contagem Resultado Importa��o Log", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContagemResultadoImpLog_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contagemresultadoimplog( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemresultadoimplog( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_33126( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_33126e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_33126( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_33126( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_33126e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Contagem Resultado Importa��o Log", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_ContagemResultadoImpLog.htm");
            wb_table3_28_33126( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_33126e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_33126e( true) ;
         }
         else
         {
            wb_table1_2_33126e( false) ;
         }
      }

      protected void wb_table3_28_33126( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_33126( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_33126e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoImpLog.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoImpLog.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoImpLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_33126e( true) ;
         }
         else
         {
            wb_table3_28_33126e( false) ;
         }
      }

      protected void wb_table4_34_33126( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoimplog_codigo_Internalname, "Imp Log_Codigo", "", "", lblTextblockcontagemresultadoimplog_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoImpLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoImpLog_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1041ContagemResultadoImpLog_Codigo), 6, 0, ",", "")), ((edtContagemResultadoImpLog_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1041ContagemResultadoImpLog_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1041ContagemResultadoImpLog_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoImpLog_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoImpLog_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoImpLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoimplog_data_Internalname, "Data", "", "", lblTextblockcontagemresultadoimplog_data_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoImpLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContagemResultadoImpLog_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoImpLog_Data_Internalname, context.localUtil.TToC( A1042ContagemResultadoImpLog_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1042ContagemResultadoImpLog_Data, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoImpLog_Data_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoImpLog_Data_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoImpLog.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultadoImpLog_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContagemResultadoImpLog_Data_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoImpLog.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoimplog_usercod_Internalname, "Usu�rio", "", "", lblTextblockcontagemresultadoimplog_usercod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoImpLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoImpLog_UserCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1038ContagemResultadoImpLog_UserCod), 6, 0, ",", "")), ((edtContagemResultadoImpLog_UserCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1038ContagemResultadoImpLog_UserCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1038ContagemResultadoImpLog_UserCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoImpLog_UserCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoImpLog_UserCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoImpLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoimplog_userpescod_Internalname, "PF", "", "", lblTextblockcontagemresultadoimplog_userpescod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoImpLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoImpLog_UserPesCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1039ContagemResultadoImpLog_UserPesCod), 6, 0, ",", "")), ((edtContagemResultadoImpLog_UserPesCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1039ContagemResultadoImpLog_UserPesCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1039ContagemResultadoImpLog_UserPesCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoImpLog_UserPesCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoImpLog_UserPesCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoImpLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoimplog_usernom_Internalname, "Respons�vel", "", "", lblTextblockcontagemresultadoimplog_usernom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoImpLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoImpLog_UserNom_Internalname, StringUtil.RTrim( A1040ContagemResultadoImpLog_UserNom), StringUtil.RTrim( context.localUtil.Format( A1040ContagemResultadoImpLog_UserNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoImpLog_UserNom_Jsonclick, 0, "BootstrapAttribute100", "", "", "", 1, edtContagemResultadoImpLog_UserNom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContagemResultadoImpLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_33126e( true) ;
         }
         else
         {
            wb_table4_34_33126e( false) ;
         }
      }

      protected void wb_table2_5_33126( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoImpLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoImpLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoImpLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoImpLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoImpLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoImpLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoImpLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoImpLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoImpLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoImpLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoImpLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoImpLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoImpLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoImpLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoImpLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoImpLog.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_33126e( true) ;
         }
         else
         {
            wb_table2_5_33126e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoImpLog_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoImpLog_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOIMPLOG_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoImpLog_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1041ContagemResultadoImpLog_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1041ContagemResultadoImpLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1041ContagemResultadoImpLog_Codigo), 6, 0)));
               }
               else
               {
                  A1041ContagemResultadoImpLog_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoImpLog_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1041ContagemResultadoImpLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1041ContagemResultadoImpLog_Codigo), 6, 0)));
               }
               if ( context.localUtil.VCDateTime( cgiGet( edtContagemResultadoImpLog_Data_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Data"}), 1, "CONTAGEMRESULTADOIMPLOG_DATA");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoImpLog_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1042ContagemResultadoImpLog_Data = (DateTime)(DateTime.MinValue);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1042ContagemResultadoImpLog_Data", context.localUtil.TToC( A1042ContagemResultadoImpLog_Data, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A1042ContagemResultadoImpLog_Data = context.localUtil.CToT( cgiGet( edtContagemResultadoImpLog_Data_Internalname));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1042ContagemResultadoImpLog_Data", context.localUtil.TToC( A1042ContagemResultadoImpLog_Data, 8, 5, 0, 3, "/", ":", " "));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoImpLog_UserCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoImpLog_UserCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOIMPLOG_USERCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoImpLog_UserCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1038ContagemResultadoImpLog_UserCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1038ContagemResultadoImpLog_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1038ContagemResultadoImpLog_UserCod), 6, 0)));
               }
               else
               {
                  A1038ContagemResultadoImpLog_UserCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoImpLog_UserCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1038ContagemResultadoImpLog_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1038ContagemResultadoImpLog_UserCod), 6, 0)));
               }
               A1039ContagemResultadoImpLog_UserPesCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoImpLog_UserPesCod_Internalname), ",", "."));
               n1039ContagemResultadoImpLog_UserPesCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1039ContagemResultadoImpLog_UserPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1039ContagemResultadoImpLog_UserPesCod), 6, 0)));
               A1040ContagemResultadoImpLog_UserNom = StringUtil.Upper( cgiGet( edtContagemResultadoImpLog_UserNom_Internalname));
               n1040ContagemResultadoImpLog_UserNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1040ContagemResultadoImpLog_UserNom", A1040ContagemResultadoImpLog_UserNom);
               /* Read saved values. */
               Z1041ContagemResultadoImpLog_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1041ContagemResultadoImpLog_Codigo"), ",", "."));
               Z1042ContagemResultadoImpLog_Data = context.localUtil.CToT( cgiGet( "Z1042ContagemResultadoImpLog_Data"), 0);
               Z1038ContagemResultadoImpLog_UserCod = (int)(context.localUtil.CToN( cgiGet( "Z1038ContagemResultadoImpLog_UserCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A1041ContagemResultadoImpLog_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1041ContagemResultadoImpLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1041ContagemResultadoImpLog_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll33126( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes33126( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption330( )
      {
      }

      protected void ZM33126( short GX_JID )
      {
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1042ContagemResultadoImpLog_Data = T00333_A1042ContagemResultadoImpLog_Data[0];
               Z1038ContagemResultadoImpLog_UserCod = T00333_A1038ContagemResultadoImpLog_UserCod[0];
            }
            else
            {
               Z1042ContagemResultadoImpLog_Data = A1042ContagemResultadoImpLog_Data;
               Z1038ContagemResultadoImpLog_UserCod = A1038ContagemResultadoImpLog_UserCod;
            }
         }
         if ( GX_JID == -2 )
         {
            Z1041ContagemResultadoImpLog_Codigo = A1041ContagemResultadoImpLog_Codigo;
            Z1042ContagemResultadoImpLog_Data = A1042ContagemResultadoImpLog_Data;
            Z1038ContagemResultadoImpLog_UserCod = A1038ContagemResultadoImpLog_UserCod;
            Z1039ContagemResultadoImpLog_UserPesCod = A1039ContagemResultadoImpLog_UserPesCod;
            Z1040ContagemResultadoImpLog_UserNom = A1040ContagemResultadoImpLog_UserNom;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load33126( )
      {
         /* Using cursor T00336 */
         pr_default.execute(4, new Object[] {A1041ContagemResultadoImpLog_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound126 = 1;
            A1042ContagemResultadoImpLog_Data = T00336_A1042ContagemResultadoImpLog_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1042ContagemResultadoImpLog_Data", context.localUtil.TToC( A1042ContagemResultadoImpLog_Data, 8, 5, 0, 3, "/", ":", " "));
            A1040ContagemResultadoImpLog_UserNom = T00336_A1040ContagemResultadoImpLog_UserNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1040ContagemResultadoImpLog_UserNom", A1040ContagemResultadoImpLog_UserNom);
            n1040ContagemResultadoImpLog_UserNom = T00336_n1040ContagemResultadoImpLog_UserNom[0];
            A1038ContagemResultadoImpLog_UserCod = T00336_A1038ContagemResultadoImpLog_UserCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1038ContagemResultadoImpLog_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1038ContagemResultadoImpLog_UserCod), 6, 0)));
            A1039ContagemResultadoImpLog_UserPesCod = T00336_A1039ContagemResultadoImpLog_UserPesCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1039ContagemResultadoImpLog_UserPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1039ContagemResultadoImpLog_UserPesCod), 6, 0)));
            n1039ContagemResultadoImpLog_UserPesCod = T00336_n1039ContagemResultadoImpLog_UserPesCod[0];
            ZM33126( -2) ;
         }
         pr_default.close(4);
         OnLoadActions33126( ) ;
      }

      protected void OnLoadActions33126( )
      {
      }

      protected void CheckExtendedTable33126( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A1042ContagemResultadoImpLog_Data) || ( A1042ContagemResultadoImpLog_Data >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data fora do intervalo", "OutOfRange", 1, "CONTAGEMRESULTADOIMPLOG_DATA");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoImpLog_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T00334 */
         pr_default.execute(2, new Object[] {A1038ContagemResultadoImpLog_UserCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Imp Log_Usuarios'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOIMPLOG_USERCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoImpLog_UserCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1039ContagemResultadoImpLog_UserPesCod = T00334_A1039ContagemResultadoImpLog_UserPesCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1039ContagemResultadoImpLog_UserPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1039ContagemResultadoImpLog_UserPesCod), 6, 0)));
         n1039ContagemResultadoImpLog_UserPesCod = T00334_n1039ContagemResultadoImpLog_UserPesCod[0];
         pr_default.close(2);
         /* Using cursor T00335 */
         pr_default.execute(3, new Object[] {n1039ContagemResultadoImpLog_UserPesCod, A1039ContagemResultadoImpLog_UserPesCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1040ContagemResultadoImpLog_UserNom = T00335_A1040ContagemResultadoImpLog_UserNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1040ContagemResultadoImpLog_UserNom", A1040ContagemResultadoImpLog_UserNom);
         n1040ContagemResultadoImpLog_UserNom = T00335_n1040ContagemResultadoImpLog_UserNom[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors33126( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_3( int A1038ContagemResultadoImpLog_UserCod )
      {
         /* Using cursor T00337 */
         pr_default.execute(5, new Object[] {A1038ContagemResultadoImpLog_UserCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Imp Log_Usuarios'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOIMPLOG_USERCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoImpLog_UserCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1039ContagemResultadoImpLog_UserPesCod = T00337_A1039ContagemResultadoImpLog_UserPesCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1039ContagemResultadoImpLog_UserPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1039ContagemResultadoImpLog_UserPesCod), 6, 0)));
         n1039ContagemResultadoImpLog_UserPesCod = T00337_n1039ContagemResultadoImpLog_UserPesCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1039ContagemResultadoImpLog_UserPesCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_4( int A1039ContagemResultadoImpLog_UserPesCod )
      {
         /* Using cursor T00338 */
         pr_default.execute(6, new Object[] {n1039ContagemResultadoImpLog_UserPesCod, A1039ContagemResultadoImpLog_UserPesCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1040ContagemResultadoImpLog_UserNom = T00338_A1040ContagemResultadoImpLog_UserNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1040ContagemResultadoImpLog_UserNom", A1040ContagemResultadoImpLog_UserNom);
         n1040ContagemResultadoImpLog_UserNom = T00338_n1040ContagemResultadoImpLog_UserNom[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1040ContagemResultadoImpLog_UserNom))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey33126( )
      {
         /* Using cursor T00339 */
         pr_default.execute(7, new Object[] {A1041ContagemResultadoImpLog_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound126 = 1;
         }
         else
         {
            RcdFound126 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00333 */
         pr_default.execute(1, new Object[] {A1041ContagemResultadoImpLog_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM33126( 2) ;
            RcdFound126 = 1;
            A1041ContagemResultadoImpLog_Codigo = T00333_A1041ContagemResultadoImpLog_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1041ContagemResultadoImpLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1041ContagemResultadoImpLog_Codigo), 6, 0)));
            A1042ContagemResultadoImpLog_Data = T00333_A1042ContagemResultadoImpLog_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1042ContagemResultadoImpLog_Data", context.localUtil.TToC( A1042ContagemResultadoImpLog_Data, 8, 5, 0, 3, "/", ":", " "));
            A1038ContagemResultadoImpLog_UserCod = T00333_A1038ContagemResultadoImpLog_UserCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1038ContagemResultadoImpLog_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1038ContagemResultadoImpLog_UserCod), 6, 0)));
            Z1041ContagemResultadoImpLog_Codigo = A1041ContagemResultadoImpLog_Codigo;
            sMode126 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load33126( ) ;
            if ( AnyError == 1 )
            {
               RcdFound126 = 0;
               InitializeNonKey33126( ) ;
            }
            Gx_mode = sMode126;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound126 = 0;
            InitializeNonKey33126( ) ;
            sMode126 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode126;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey33126( ) ;
         if ( RcdFound126 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound126 = 0;
         /* Using cursor T003310 */
         pr_default.execute(8, new Object[] {A1041ContagemResultadoImpLog_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T003310_A1041ContagemResultadoImpLog_Codigo[0] < A1041ContagemResultadoImpLog_Codigo ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T003310_A1041ContagemResultadoImpLog_Codigo[0] > A1041ContagemResultadoImpLog_Codigo ) ) )
            {
               A1041ContagemResultadoImpLog_Codigo = T003310_A1041ContagemResultadoImpLog_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1041ContagemResultadoImpLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1041ContagemResultadoImpLog_Codigo), 6, 0)));
               RcdFound126 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound126 = 0;
         /* Using cursor T003311 */
         pr_default.execute(9, new Object[] {A1041ContagemResultadoImpLog_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T003311_A1041ContagemResultadoImpLog_Codigo[0] > A1041ContagemResultadoImpLog_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T003311_A1041ContagemResultadoImpLog_Codigo[0] < A1041ContagemResultadoImpLog_Codigo ) ) )
            {
               A1041ContagemResultadoImpLog_Codigo = T003311_A1041ContagemResultadoImpLog_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1041ContagemResultadoImpLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1041ContagemResultadoImpLog_Codigo), 6, 0)));
               RcdFound126 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey33126( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContagemResultadoImpLog_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert33126( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound126 == 1 )
            {
               if ( A1041ContagemResultadoImpLog_Codigo != Z1041ContagemResultadoImpLog_Codigo )
               {
                  A1041ContagemResultadoImpLog_Codigo = Z1041ContagemResultadoImpLog_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1041ContagemResultadoImpLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1041ContagemResultadoImpLog_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTAGEMRESULTADOIMPLOG_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoImpLog_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContagemResultadoImpLog_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update33126( ) ;
                  GX_FocusControl = edtContagemResultadoImpLog_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1041ContagemResultadoImpLog_Codigo != Z1041ContagemResultadoImpLog_Codigo )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtContagemResultadoImpLog_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert33126( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTAGEMRESULTADOIMPLOG_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtContagemResultadoImpLog_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtContagemResultadoImpLog_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert33126( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A1041ContagemResultadoImpLog_Codigo != Z1041ContagemResultadoImpLog_Codigo )
         {
            A1041ContagemResultadoImpLog_Codigo = Z1041ContagemResultadoImpLog_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1041ContagemResultadoImpLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1041ContagemResultadoImpLog_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTAGEMRESULTADOIMPLOG_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoImpLog_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContagemResultadoImpLog_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound126 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "CONTAGEMRESULTADOIMPLOG_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoImpLog_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtContagemResultadoImpLog_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart33126( ) ;
         if ( RcdFound126 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoImpLog_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd33126( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound126 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoImpLog_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound126 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoImpLog_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart33126( ) ;
         if ( RcdFound126 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound126 != 0 )
            {
               ScanNext33126( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoImpLog_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd33126( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency33126( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00332 */
            pr_default.execute(0, new Object[] {A1041ContagemResultadoImpLog_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoImpLog"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1042ContagemResultadoImpLog_Data != T00332_A1042ContagemResultadoImpLog_Data[0] ) || ( Z1038ContagemResultadoImpLog_UserCod != T00332_A1038ContagemResultadoImpLog_UserCod[0] ) )
            {
               if ( Z1042ContagemResultadoImpLog_Data != T00332_A1042ContagemResultadoImpLog_Data[0] )
               {
                  GXUtil.WriteLog("contagemresultadoimplog:[seudo value changed for attri]"+"ContagemResultadoImpLog_Data");
                  GXUtil.WriteLogRaw("Old: ",Z1042ContagemResultadoImpLog_Data);
                  GXUtil.WriteLogRaw("Current: ",T00332_A1042ContagemResultadoImpLog_Data[0]);
               }
               if ( Z1038ContagemResultadoImpLog_UserCod != T00332_A1038ContagemResultadoImpLog_UserCod[0] )
               {
                  GXUtil.WriteLog("contagemresultadoimplog:[seudo value changed for attri]"+"ContagemResultadoImpLog_UserCod");
                  GXUtil.WriteLogRaw("Old: ",Z1038ContagemResultadoImpLog_UserCod);
                  GXUtil.WriteLogRaw("Current: ",T00332_A1038ContagemResultadoImpLog_UserCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultadoImpLog"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert33126( )
      {
         BeforeValidate33126( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable33126( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM33126( 0) ;
            CheckOptimisticConcurrency33126( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm33126( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert33126( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003312 */
                     pr_default.execute(10, new Object[] {A1041ContagemResultadoImpLog_Codigo, A1042ContagemResultadoImpLog_Data, A1038ContagemResultadoImpLog_UserCod});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoImpLog") ;
                     if ( (pr_default.getStatus(10) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption330( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load33126( ) ;
            }
            EndLevel33126( ) ;
         }
         CloseExtendedTableCursors33126( ) ;
      }

      protected void Update33126( )
      {
         BeforeValidate33126( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable33126( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency33126( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm33126( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate33126( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003313 */
                     pr_default.execute(11, new Object[] {A1042ContagemResultadoImpLog_Data, A1038ContagemResultadoImpLog_UserCod, A1041ContagemResultadoImpLog_Codigo});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoImpLog") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoImpLog"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate33126( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption330( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel33126( ) ;
         }
         CloseExtendedTableCursors33126( ) ;
      }

      protected void DeferredUpdate33126( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate33126( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency33126( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls33126( ) ;
            AfterConfirm33126( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete33126( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003314 */
                  pr_default.execute(12, new Object[] {A1041ContagemResultadoImpLog_Codigo});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoImpLog") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound126 == 0 )
                        {
                           InitAll33126( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption330( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode126 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel33126( ) ;
         Gx_mode = sMode126;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls33126( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T003315 */
            pr_default.execute(13, new Object[] {A1038ContagemResultadoImpLog_UserCod});
            A1039ContagemResultadoImpLog_UserPesCod = T003315_A1039ContagemResultadoImpLog_UserPesCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1039ContagemResultadoImpLog_UserPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1039ContagemResultadoImpLog_UserPesCod), 6, 0)));
            n1039ContagemResultadoImpLog_UserPesCod = T003315_n1039ContagemResultadoImpLog_UserPesCod[0];
            pr_default.close(13);
            /* Using cursor T003316 */
            pr_default.execute(14, new Object[] {n1039ContagemResultadoImpLog_UserPesCod, A1039ContagemResultadoImpLog_UserPesCod});
            A1040ContagemResultadoImpLog_UserNom = T003316_A1040ContagemResultadoImpLog_UserNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1040ContagemResultadoImpLog_UserNom", A1040ContagemResultadoImpLog_UserNom);
            n1040ContagemResultadoImpLog_UserNom = T003316_n1040ContagemResultadoImpLog_UserNom[0];
            pr_default.close(14);
         }
      }

      protected void EndLevel33126( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete33126( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(13);
            pr_default.close(14);
            context.CommitDataStores( "ContagemResultadoImpLog");
            if ( AnyError == 0 )
            {
               ConfirmValues330( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(13);
            pr_default.close(14);
            context.RollbackDataStores( "ContagemResultadoImpLog");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart33126( )
      {
         /* Using cursor T003317 */
         pr_default.execute(15);
         RcdFound126 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound126 = 1;
            A1041ContagemResultadoImpLog_Codigo = T003317_A1041ContagemResultadoImpLog_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1041ContagemResultadoImpLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1041ContagemResultadoImpLog_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext33126( )
      {
         /* Scan next routine */
         pr_default.readNext(15);
         RcdFound126 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound126 = 1;
            A1041ContagemResultadoImpLog_Codigo = T003317_A1041ContagemResultadoImpLog_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1041ContagemResultadoImpLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1041ContagemResultadoImpLog_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd33126( )
      {
         pr_default.close(15);
      }

      protected void AfterConfirm33126( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert33126( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate33126( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete33126( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete33126( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate33126( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes33126( )
      {
         edtContagemResultadoImpLog_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoImpLog_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoImpLog_Codigo_Enabled), 5, 0)));
         edtContagemResultadoImpLog_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoImpLog_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoImpLog_Data_Enabled), 5, 0)));
         edtContagemResultadoImpLog_UserCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoImpLog_UserCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoImpLog_UserCod_Enabled), 5, 0)));
         edtContagemResultadoImpLog_UserPesCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoImpLog_UserPesCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoImpLog_UserPesCod_Enabled), 5, 0)));
         edtContagemResultadoImpLog_UserNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoImpLog_UserNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoImpLog_UserNom_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues330( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299311423");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemresultadoimplog.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1041ContagemResultadoImpLog_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1041ContagemResultadoImpLog_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1042ContagemResultadoImpLog_Data", context.localUtil.TToC( Z1042ContagemResultadoImpLog_Data, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z1038ContagemResultadoImpLog_UserCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1038ContagemResultadoImpLog_UserCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contagemresultadoimplog.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "ContagemResultadoImpLog" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Resultado Importa��o Log" ;
      }

      protected void InitializeNonKey33126( )
      {
         A1042ContagemResultadoImpLog_Data = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1042ContagemResultadoImpLog_Data", context.localUtil.TToC( A1042ContagemResultadoImpLog_Data, 8, 5, 0, 3, "/", ":", " "));
         A1038ContagemResultadoImpLog_UserCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1038ContagemResultadoImpLog_UserCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1038ContagemResultadoImpLog_UserCod), 6, 0)));
         A1039ContagemResultadoImpLog_UserPesCod = 0;
         n1039ContagemResultadoImpLog_UserPesCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1039ContagemResultadoImpLog_UserPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1039ContagemResultadoImpLog_UserPesCod), 6, 0)));
         A1040ContagemResultadoImpLog_UserNom = "";
         n1040ContagemResultadoImpLog_UserNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1040ContagemResultadoImpLog_UserNom", A1040ContagemResultadoImpLog_UserNom);
         Z1042ContagemResultadoImpLog_Data = (DateTime)(DateTime.MinValue);
         Z1038ContagemResultadoImpLog_UserCod = 0;
      }

      protected void InitAll33126( )
      {
         A1041ContagemResultadoImpLog_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1041ContagemResultadoImpLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1041ContagemResultadoImpLog_Codigo), 6, 0)));
         InitializeNonKey33126( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299311428");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contagemresultadoimplog.js", "?20205299311428");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockcontagemresultadoimplog_codigo_Internalname = "TEXTBLOCKCONTAGEMRESULTADOIMPLOG_CODIGO";
         edtContagemResultadoImpLog_Codigo_Internalname = "CONTAGEMRESULTADOIMPLOG_CODIGO";
         lblTextblockcontagemresultadoimplog_data_Internalname = "TEXTBLOCKCONTAGEMRESULTADOIMPLOG_DATA";
         edtContagemResultadoImpLog_Data_Internalname = "CONTAGEMRESULTADOIMPLOG_DATA";
         lblTextblockcontagemresultadoimplog_usercod_Internalname = "TEXTBLOCKCONTAGEMRESULTADOIMPLOG_USERCOD";
         edtContagemResultadoImpLog_UserCod_Internalname = "CONTAGEMRESULTADOIMPLOG_USERCOD";
         lblTextblockcontagemresultadoimplog_userpescod_Internalname = "TEXTBLOCKCONTAGEMRESULTADOIMPLOG_USERPESCOD";
         edtContagemResultadoImpLog_UserPesCod_Internalname = "CONTAGEMRESULTADOIMPLOG_USERPESCOD";
         lblTextblockcontagemresultadoimplog_usernom_Internalname = "TEXTBLOCKCONTAGEMRESULTADOIMPLOG_USERNOM";
         edtContagemResultadoImpLog_UserNom_Internalname = "CONTAGEMRESULTADOIMPLOG_USERNOM";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contagem Resultado Importa��o Log";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtContagemResultadoImpLog_UserNom_Jsonclick = "";
         edtContagemResultadoImpLog_UserNom_Enabled = 0;
         edtContagemResultadoImpLog_UserPesCod_Jsonclick = "";
         edtContagemResultadoImpLog_UserPesCod_Enabled = 0;
         edtContagemResultadoImpLog_UserCod_Jsonclick = "";
         edtContagemResultadoImpLog_UserCod_Enabled = 1;
         edtContagemResultadoImpLog_Data_Jsonclick = "";
         edtContagemResultadoImpLog_Data_Enabled = 1;
         edtContagemResultadoImpLog_Codigo_Jsonclick = "";
         edtContagemResultadoImpLog_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtContagemResultadoImpLog_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Contagemresultadoimplog_codigo( int GX_Parm1 ,
                                                        DateTime GX_Parm2 ,
                                                        int GX_Parm3 )
      {
         A1041ContagemResultadoImpLog_Codigo = GX_Parm1;
         A1042ContagemResultadoImpLog_Data = GX_Parm2;
         A1038ContagemResultadoImpLog_UserCod = GX_Parm3;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1039ContagemResultadoImpLog_UserPesCod = 0;
            n1039ContagemResultadoImpLog_UserPesCod = false;
            A1040ContagemResultadoImpLog_UserNom = "";
            n1040ContagemResultadoImpLog_UserNom = false;
         }
         isValidOutput.Add(context.localUtil.TToC( A1042ContagemResultadoImpLog_Data, 10, 8, 0, 3, "/", ":", " "));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1038ContagemResultadoImpLog_UserCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1039ContagemResultadoImpLog_UserPesCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A1040ContagemResultadoImpLog_UserNom));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1041ContagemResultadoImpLog_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(context.localUtil.TToC( Z1042ContagemResultadoImpLog_Data, 10, 8, 0, 0, "/", ":", " "));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1038ContagemResultadoImpLog_UserCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1039ContagemResultadoImpLog_UserPesCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( Z1040ContagemResultadoImpLog_UserNom));
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultadoimplog_usercod( int GX_Parm1 ,
                                                         int GX_Parm2 ,
                                                         String GX_Parm3 )
      {
         A1038ContagemResultadoImpLog_UserCod = GX_Parm1;
         A1039ContagemResultadoImpLog_UserPesCod = GX_Parm2;
         n1039ContagemResultadoImpLog_UserPesCod = false;
         A1040ContagemResultadoImpLog_UserNom = GX_Parm3;
         n1040ContagemResultadoImpLog_UserNom = false;
         /* Using cursor T003315 */
         pr_default.execute(13, new Object[] {A1038ContagemResultadoImpLog_UserCod});
         if ( (pr_default.getStatus(13) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Imp Log_Usuarios'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOIMPLOG_USERCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoImpLog_UserCod_Internalname;
         }
         A1039ContagemResultadoImpLog_UserPesCod = T003315_A1039ContagemResultadoImpLog_UserPesCod[0];
         n1039ContagemResultadoImpLog_UserPesCod = T003315_n1039ContagemResultadoImpLog_UserPesCod[0];
         pr_default.close(13);
         /* Using cursor T003316 */
         pr_default.execute(14, new Object[] {n1039ContagemResultadoImpLog_UserPesCod, A1039ContagemResultadoImpLog_UserPesCod});
         if ( (pr_default.getStatus(14) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1040ContagemResultadoImpLog_UserNom = T003316_A1040ContagemResultadoImpLog_UserNom[0];
         n1040ContagemResultadoImpLog_UserNom = T003316_n1040ContagemResultadoImpLog_UserNom[0];
         pr_default.close(14);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1039ContagemResultadoImpLog_UserPesCod = 0;
            n1039ContagemResultadoImpLog_UserPesCod = false;
            A1040ContagemResultadoImpLog_UserNom = "";
            n1040ContagemResultadoImpLog_UserNom = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1039ContagemResultadoImpLog_UserPesCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A1040ContagemResultadoImpLog_UserNom));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(13);
         pr_default.close(14);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z1042ContagemResultadoImpLog_Data = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockcontagemresultadoimplog_codigo_Jsonclick = "";
         lblTextblockcontagemresultadoimplog_data_Jsonclick = "";
         A1042ContagemResultadoImpLog_Data = (DateTime)(DateTime.MinValue);
         lblTextblockcontagemresultadoimplog_usercod_Jsonclick = "";
         lblTextblockcontagemresultadoimplog_userpescod_Jsonclick = "";
         lblTextblockcontagemresultadoimplog_usernom_Jsonclick = "";
         A1040ContagemResultadoImpLog_UserNom = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z1040ContagemResultadoImpLog_UserNom = "";
         T00336_A1041ContagemResultadoImpLog_Codigo = new int[1] ;
         T00336_A1042ContagemResultadoImpLog_Data = new DateTime[] {DateTime.MinValue} ;
         T00336_A1040ContagemResultadoImpLog_UserNom = new String[] {""} ;
         T00336_n1040ContagemResultadoImpLog_UserNom = new bool[] {false} ;
         T00336_A1038ContagemResultadoImpLog_UserCod = new int[1] ;
         T00336_A1039ContagemResultadoImpLog_UserPesCod = new int[1] ;
         T00336_n1039ContagemResultadoImpLog_UserPesCod = new bool[] {false} ;
         T00334_A1039ContagemResultadoImpLog_UserPesCod = new int[1] ;
         T00334_n1039ContagemResultadoImpLog_UserPesCod = new bool[] {false} ;
         T00335_A1040ContagemResultadoImpLog_UserNom = new String[] {""} ;
         T00335_n1040ContagemResultadoImpLog_UserNom = new bool[] {false} ;
         T00337_A1039ContagemResultadoImpLog_UserPesCod = new int[1] ;
         T00337_n1039ContagemResultadoImpLog_UserPesCod = new bool[] {false} ;
         T00338_A1040ContagemResultadoImpLog_UserNom = new String[] {""} ;
         T00338_n1040ContagemResultadoImpLog_UserNom = new bool[] {false} ;
         T00339_A1041ContagemResultadoImpLog_Codigo = new int[1] ;
         T00333_A1041ContagemResultadoImpLog_Codigo = new int[1] ;
         T00333_A1042ContagemResultadoImpLog_Data = new DateTime[] {DateTime.MinValue} ;
         T00333_A1038ContagemResultadoImpLog_UserCod = new int[1] ;
         sMode126 = "";
         T003310_A1041ContagemResultadoImpLog_Codigo = new int[1] ;
         T003311_A1041ContagemResultadoImpLog_Codigo = new int[1] ;
         T00332_A1041ContagemResultadoImpLog_Codigo = new int[1] ;
         T00332_A1042ContagemResultadoImpLog_Data = new DateTime[] {DateTime.MinValue} ;
         T00332_A1038ContagemResultadoImpLog_UserCod = new int[1] ;
         T003315_A1039ContagemResultadoImpLog_UserPesCod = new int[1] ;
         T003315_n1039ContagemResultadoImpLog_UserPesCod = new bool[] {false} ;
         T003316_A1040ContagemResultadoImpLog_UserNom = new String[] {""} ;
         T003316_n1040ContagemResultadoImpLog_UserNom = new bool[] {false} ;
         T003317_A1041ContagemResultadoImpLog_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadoimplog__default(),
            new Object[][] {
                new Object[] {
               T00332_A1041ContagemResultadoImpLog_Codigo, T00332_A1042ContagemResultadoImpLog_Data, T00332_A1038ContagemResultadoImpLog_UserCod
               }
               , new Object[] {
               T00333_A1041ContagemResultadoImpLog_Codigo, T00333_A1042ContagemResultadoImpLog_Data, T00333_A1038ContagemResultadoImpLog_UserCod
               }
               , new Object[] {
               T00334_A1039ContagemResultadoImpLog_UserPesCod, T00334_n1039ContagemResultadoImpLog_UserPesCod
               }
               , new Object[] {
               T00335_A1040ContagemResultadoImpLog_UserNom, T00335_n1040ContagemResultadoImpLog_UserNom
               }
               , new Object[] {
               T00336_A1041ContagemResultadoImpLog_Codigo, T00336_A1042ContagemResultadoImpLog_Data, T00336_A1040ContagemResultadoImpLog_UserNom, T00336_n1040ContagemResultadoImpLog_UserNom, T00336_A1038ContagemResultadoImpLog_UserCod, T00336_A1039ContagemResultadoImpLog_UserPesCod, T00336_n1039ContagemResultadoImpLog_UserPesCod
               }
               , new Object[] {
               T00337_A1039ContagemResultadoImpLog_UserPesCod, T00337_n1039ContagemResultadoImpLog_UserPesCod
               }
               , new Object[] {
               T00338_A1040ContagemResultadoImpLog_UserNom, T00338_n1040ContagemResultadoImpLog_UserNom
               }
               , new Object[] {
               T00339_A1041ContagemResultadoImpLog_Codigo
               }
               , new Object[] {
               T003310_A1041ContagemResultadoImpLog_Codigo
               }
               , new Object[] {
               T003311_A1041ContagemResultadoImpLog_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003315_A1039ContagemResultadoImpLog_UserPesCod, T003315_n1039ContagemResultadoImpLog_UserPesCod
               }
               , new Object[] {
               T003316_A1040ContagemResultadoImpLog_UserNom, T003316_n1040ContagemResultadoImpLog_UserNom
               }
               , new Object[] {
               T003317_A1041ContagemResultadoImpLog_Codigo
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound126 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z1041ContagemResultadoImpLog_Codigo ;
      private int Z1038ContagemResultadoImpLog_UserCod ;
      private int A1038ContagemResultadoImpLog_UserCod ;
      private int A1039ContagemResultadoImpLog_UserPesCod ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int A1041ContagemResultadoImpLog_Codigo ;
      private int edtContagemResultadoImpLog_Codigo_Enabled ;
      private int edtContagemResultadoImpLog_Data_Enabled ;
      private int edtContagemResultadoImpLog_UserCod_Enabled ;
      private int edtContagemResultadoImpLog_UserPesCod_Enabled ;
      private int edtContagemResultadoImpLog_UserNom_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int Z1039ContagemResultadoImpLog_UserPesCod ;
      private int idxLst ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContagemResultadoImpLog_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockcontagemresultadoimplog_codigo_Internalname ;
      private String lblTextblockcontagemresultadoimplog_codigo_Jsonclick ;
      private String edtContagemResultadoImpLog_Codigo_Jsonclick ;
      private String lblTextblockcontagemresultadoimplog_data_Internalname ;
      private String lblTextblockcontagemresultadoimplog_data_Jsonclick ;
      private String edtContagemResultadoImpLog_Data_Internalname ;
      private String edtContagemResultadoImpLog_Data_Jsonclick ;
      private String lblTextblockcontagemresultadoimplog_usercod_Internalname ;
      private String lblTextblockcontagemresultadoimplog_usercod_Jsonclick ;
      private String edtContagemResultadoImpLog_UserCod_Internalname ;
      private String edtContagemResultadoImpLog_UserCod_Jsonclick ;
      private String lblTextblockcontagemresultadoimplog_userpescod_Internalname ;
      private String lblTextblockcontagemresultadoimplog_userpescod_Jsonclick ;
      private String edtContagemResultadoImpLog_UserPesCod_Internalname ;
      private String edtContagemResultadoImpLog_UserPesCod_Jsonclick ;
      private String lblTextblockcontagemresultadoimplog_usernom_Internalname ;
      private String lblTextblockcontagemresultadoimplog_usernom_Jsonclick ;
      private String edtContagemResultadoImpLog_UserNom_Internalname ;
      private String A1040ContagemResultadoImpLog_UserNom ;
      private String edtContagemResultadoImpLog_UserNom_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z1040ContagemResultadoImpLog_UserNom ;
      private String sMode126 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private DateTime Z1042ContagemResultadoImpLog_Data ;
      private DateTime A1042ContagemResultadoImpLog_Data ;
      private bool entryPointCalled ;
      private bool n1039ContagemResultadoImpLog_UserPesCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1040ContagemResultadoImpLog_UserNom ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] T00336_A1041ContagemResultadoImpLog_Codigo ;
      private DateTime[] T00336_A1042ContagemResultadoImpLog_Data ;
      private String[] T00336_A1040ContagemResultadoImpLog_UserNom ;
      private bool[] T00336_n1040ContagemResultadoImpLog_UserNom ;
      private int[] T00336_A1038ContagemResultadoImpLog_UserCod ;
      private int[] T00336_A1039ContagemResultadoImpLog_UserPesCod ;
      private bool[] T00336_n1039ContagemResultadoImpLog_UserPesCod ;
      private int[] T00334_A1039ContagemResultadoImpLog_UserPesCod ;
      private bool[] T00334_n1039ContagemResultadoImpLog_UserPesCod ;
      private String[] T00335_A1040ContagemResultadoImpLog_UserNom ;
      private bool[] T00335_n1040ContagemResultadoImpLog_UserNom ;
      private int[] T00337_A1039ContagemResultadoImpLog_UserPesCod ;
      private bool[] T00337_n1039ContagemResultadoImpLog_UserPesCod ;
      private String[] T00338_A1040ContagemResultadoImpLog_UserNom ;
      private bool[] T00338_n1040ContagemResultadoImpLog_UserNom ;
      private int[] T00339_A1041ContagemResultadoImpLog_Codigo ;
      private int[] T00333_A1041ContagemResultadoImpLog_Codigo ;
      private DateTime[] T00333_A1042ContagemResultadoImpLog_Data ;
      private int[] T00333_A1038ContagemResultadoImpLog_UserCod ;
      private int[] T003310_A1041ContagemResultadoImpLog_Codigo ;
      private int[] T003311_A1041ContagemResultadoImpLog_Codigo ;
      private int[] T00332_A1041ContagemResultadoImpLog_Codigo ;
      private DateTime[] T00332_A1042ContagemResultadoImpLog_Data ;
      private int[] T00332_A1038ContagemResultadoImpLog_UserCod ;
      private int[] T003315_A1039ContagemResultadoImpLog_UserPesCod ;
      private bool[] T003315_n1039ContagemResultadoImpLog_UserPesCod ;
      private String[] T003316_A1040ContagemResultadoImpLog_UserNom ;
      private bool[] T003316_n1040ContagemResultadoImpLog_UserNom ;
      private int[] T003317_A1041ContagemResultadoImpLog_Codigo ;
      private GXWebForm Form ;
   }

   public class contagemresultadoimplog__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00336 ;
          prmT00336 = new Object[] {
          new Object[] {"@ContagemResultadoImpLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00334 ;
          prmT00334 = new Object[] {
          new Object[] {"@ContagemResultadoImpLog_UserCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00335 ;
          prmT00335 = new Object[] {
          new Object[] {"@ContagemResultadoImpLog_UserPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00337 ;
          prmT00337 = new Object[] {
          new Object[] {"@ContagemResultadoImpLog_UserCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00338 ;
          prmT00338 = new Object[] {
          new Object[] {"@ContagemResultadoImpLog_UserPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00339 ;
          prmT00339 = new Object[] {
          new Object[] {"@ContagemResultadoImpLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00333 ;
          prmT00333 = new Object[] {
          new Object[] {"@ContagemResultadoImpLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003310 ;
          prmT003310 = new Object[] {
          new Object[] {"@ContagemResultadoImpLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003311 ;
          prmT003311 = new Object[] {
          new Object[] {"@ContagemResultadoImpLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00332 ;
          prmT00332 = new Object[] {
          new Object[] {"@ContagemResultadoImpLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003312 ;
          prmT003312 = new Object[] {
          new Object[] {"@ContagemResultadoImpLog_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoImpLog_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoImpLog_UserCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003313 ;
          prmT003313 = new Object[] {
          new Object[] {"@ContagemResultadoImpLog_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoImpLog_UserCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoImpLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003314 ;
          prmT003314 = new Object[] {
          new Object[] {"@ContagemResultadoImpLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003317 ;
          prmT003317 = new Object[] {
          } ;
          Object[] prmT003315 ;
          prmT003315 = new Object[] {
          new Object[] {"@ContagemResultadoImpLog_UserCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003316 ;
          prmT003316 = new Object[] {
          new Object[] {"@ContagemResultadoImpLog_UserPesCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00332", "SELECT [ContagemResultadoImpLog_Codigo], [ContagemResultadoImpLog_Data], [ContagemResultadoImpLog_UserCod] AS ContagemResultadoImpLog_UserCod FROM [ContagemResultadoImpLog] WITH (UPDLOCK) WHERE [ContagemResultadoImpLog_Codigo] = @ContagemResultadoImpLog_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00332,1,0,true,false )
             ,new CursorDef("T00333", "SELECT [ContagemResultadoImpLog_Codigo], [ContagemResultadoImpLog_Data], [ContagemResultadoImpLog_UserCod] AS ContagemResultadoImpLog_UserCod FROM [ContagemResultadoImpLog] WITH (NOLOCK) WHERE [ContagemResultadoImpLog_Codigo] = @ContagemResultadoImpLog_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00333,1,0,true,false )
             ,new CursorDef("T00334", "SELECT [Usuario_PessoaCod] AS ContagemResultadoImpLog_UserPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoImpLog_UserCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00334,1,0,true,false )
             ,new CursorDef("T00335", "SELECT [Pessoa_Nome] AS ContagemResultadoImpLog_UserNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoImpLog_UserPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00335,1,0,true,false )
             ,new CursorDef("T00336", "SELECT TM1.[ContagemResultadoImpLog_Codigo], TM1.[ContagemResultadoImpLog_Data], T3.[Pessoa_Nome] AS ContagemResultadoImpLog_UserNom, TM1.[ContagemResultadoImpLog_UserCod] AS ContagemResultadoImpLog_UserCod, T2.[Usuario_PessoaCod] AS ContagemResultadoImpLog_UserPesCod FROM (([ContagemResultadoImpLog] TM1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = TM1.[ContagemResultadoImpLog_UserCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE TM1.[ContagemResultadoImpLog_Codigo] = @ContagemResultadoImpLog_Codigo ORDER BY TM1.[ContagemResultadoImpLog_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00336,100,0,true,false )
             ,new CursorDef("T00337", "SELECT [Usuario_PessoaCod] AS ContagemResultadoImpLog_UserPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoImpLog_UserCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00337,1,0,true,false )
             ,new CursorDef("T00338", "SELECT [Pessoa_Nome] AS ContagemResultadoImpLog_UserNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoImpLog_UserPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00338,1,0,true,false )
             ,new CursorDef("T00339", "SELECT [ContagemResultadoImpLog_Codigo] FROM [ContagemResultadoImpLog] WITH (NOLOCK) WHERE [ContagemResultadoImpLog_Codigo] = @ContagemResultadoImpLog_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00339,1,0,true,false )
             ,new CursorDef("T003310", "SELECT TOP 1 [ContagemResultadoImpLog_Codigo] FROM [ContagemResultadoImpLog] WITH (NOLOCK) WHERE ( [ContagemResultadoImpLog_Codigo] > @ContagemResultadoImpLog_Codigo) ORDER BY [ContagemResultadoImpLog_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003310,1,0,true,true )
             ,new CursorDef("T003311", "SELECT TOP 1 [ContagemResultadoImpLog_Codigo] FROM [ContagemResultadoImpLog] WITH (NOLOCK) WHERE ( [ContagemResultadoImpLog_Codigo] < @ContagemResultadoImpLog_Codigo) ORDER BY [ContagemResultadoImpLog_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003311,1,0,true,true )
             ,new CursorDef("T003312", "INSERT INTO [ContagemResultadoImpLog]([ContagemResultadoImpLog_Codigo], [ContagemResultadoImpLog_Data], [ContagemResultadoImpLog_UserCod]) VALUES(@ContagemResultadoImpLog_Codigo, @ContagemResultadoImpLog_Data, @ContagemResultadoImpLog_UserCod)", GxErrorMask.GX_NOMASK,prmT003312)
             ,new CursorDef("T003313", "UPDATE [ContagemResultadoImpLog] SET [ContagemResultadoImpLog_Data]=@ContagemResultadoImpLog_Data, [ContagemResultadoImpLog_UserCod]=@ContagemResultadoImpLog_UserCod  WHERE [ContagemResultadoImpLog_Codigo] = @ContagemResultadoImpLog_Codigo", GxErrorMask.GX_NOMASK,prmT003313)
             ,new CursorDef("T003314", "DELETE FROM [ContagemResultadoImpLog]  WHERE [ContagemResultadoImpLog_Codigo] = @ContagemResultadoImpLog_Codigo", GxErrorMask.GX_NOMASK,prmT003314)
             ,new CursorDef("T003315", "SELECT [Usuario_PessoaCod] AS ContagemResultadoImpLog_UserPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoImpLog_UserCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003315,1,0,true,false )
             ,new CursorDef("T003316", "SELECT [Pessoa_Nome] AS ContagemResultadoImpLog_UserNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoImpLog_UserPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003316,1,0,true,false )
             ,new CursorDef("T003317", "SELECT [ContagemResultadoImpLog_Codigo] FROM [ContagemResultadoImpLog] WITH (NOLOCK) ORDER BY [ContagemResultadoImpLog_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003317,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 11 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
