/*
               File: PRC_EnviarEmail
        Description: Enviar Email
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:27:4.75
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Mail;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_enviaremail : GXProcedure
   {
      public prc_enviaremail( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_enviaremail( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AreaTrabalho_Codigo ,
                           IGxCollection aP1_Usuarios ,
                           String aP2_Subject ,
                           String aP3_EmailText ,
                           IGxCollection aP4_Attachments ,
                           ref String aP5_Resultado )
      {
         this.AV20AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV18Usuarios = aP1_Usuarios;
         this.AV19Subject = aP2_Subject;
         this.AV14EmailText = aP3_EmailText;
         this.AV15Attachments = aP4_Attachments;
         this.AV22Resultado = aP5_Resultado;
         initialize();
         executePrivate();
         aP5_Resultado=this.AV22Resultado;
      }

      public String executeUdp( int aP0_AreaTrabalho_Codigo ,
                                IGxCollection aP1_Usuarios ,
                                String aP2_Subject ,
                                String aP3_EmailText ,
                                IGxCollection aP4_Attachments )
      {
         this.AV20AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV18Usuarios = aP1_Usuarios;
         this.AV19Subject = aP2_Subject;
         this.AV14EmailText = aP3_EmailText;
         this.AV15Attachments = aP4_Attachments;
         this.AV22Resultado = aP5_Resultado;
         initialize();
         executePrivate();
         aP5_Resultado=this.AV22Resultado;
         return AV22Resultado ;
      }

      public void executeSubmit( int aP0_AreaTrabalho_Codigo ,
                                 IGxCollection aP1_Usuarios ,
                                 String aP2_Subject ,
                                 String aP3_EmailText ,
                                 IGxCollection aP4_Attachments ,
                                 ref String aP5_Resultado )
      {
         prc_enviaremail objprc_enviaremail;
         objprc_enviaremail = new prc_enviaremail();
         objprc_enviaremail.AV20AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         objprc_enviaremail.AV18Usuarios = aP1_Usuarios;
         objprc_enviaremail.AV19Subject = aP2_Subject;
         objprc_enviaremail.AV14EmailText = aP3_EmailText;
         objprc_enviaremail.AV15Attachments = aP4_Attachments;
         objprc_enviaremail.AV22Resultado = aP5_Resultado;
         objprc_enviaremail.context.SetSubmitInitialConfig(context);
         objprc_enviaremail.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_enviaremail);
         aP5_Resultado=this.AV22Resultado;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_enviaremail)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV25i = 1;
         if ( AV18Usuarios.Count > 0 )
         {
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A1Usuario_Codigo ,
                                                 AV18Usuarios },
                                                 new int[] {
                                                 TypeConstants.INT
                                                 }
            });
            /* Using cursor P00712 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1Usuario_Codigo = P00712_A1Usuario_Codigo[0];
               A1647Usuario_Email = P00712_A1647Usuario_Email[0];
               n1647Usuario_Email = P00712_n1647Usuario_Email[0];
               A341Usuario_UserGamGuid = P00712_A341Usuario_UserGamGuid[0];
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1647Usuario_Email)) )
               {
                  if ( new prc_validaremail(context).executeUdp(  A1647Usuario_Email) )
                  {
                     AV9MailRecipient = new GeneXus.Mail.GXMailRecipient();
                     AV9MailRecipient.Name = StringUtil.Trim( A1647Usuario_Email);
                     AV9MailRecipient.Address = StringUtil.Trim( A1647Usuario_Email);
                     AV24Destinatarios = AV24Destinatarios + AV32Virgola;
                     AV24Destinatarios = AV24Destinatarios + AV9MailRecipient.Address;
                     if ( AV25i == 1 )
                     {
                        AV10Email.To.Add(AV9MailRecipient) ;
                     }
                     else
                     {
                        AV10Email.BCC.Add(AV9MailRecipient) ;
                     }
                     AV25i = (short)(AV25i+1);
                     AV32Virgola = ", ";
                  }
               }
               else
               {
                  AV11GamUser.load( A341Usuario_UserGamGuid);
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11GamUser.gxTpr_Email)) )
                  {
                     if ( new prc_validaremail(context).executeUdp(  AV11GamUser.gxTpr_Email) )
                     {
                        AV9MailRecipient = new GeneXus.Mail.GXMailRecipient();
                        AV9MailRecipient.Name = StringUtil.Trim( AV11GamUser.gxTpr_Email);
                        AV9MailRecipient.Address = StringUtil.Trim( AV11GamUser.gxTpr_Email);
                        AV24Destinatarios = AV24Destinatarios + AV32Virgola;
                        AV24Destinatarios = AV24Destinatarios + AV9MailRecipient.Address;
                        if ( AV25i == 1 )
                        {
                           AV10Email.To.Add(AV9MailRecipient) ;
                        }
                        else
                        {
                           AV10Email.BCC.Add(AV9MailRecipient) ;
                        }
                        AV25i = (short)(AV25i+1);
                        AV32Virgola = ", ";
                     }
                  }
               }
               pr_default.readNext(0);
            }
            pr_default.close(0);
         }
         if ( AV10Email.To.Count > 0 )
         {
            /* Using cursor P00713 */
            pr_default.execute(1);
            while ( (pr_default.getStatus(1) != 101) )
            {
               A331ParametrosSistema_NomeSistema = P00713_A331ParametrosSistema_NomeSistema[0];
               A330ParametrosSistema_Codigo = P00713_A330ParametrosSistema_Codigo[0];
               AV26NomeSistema = StringUtil.Trim( A331ParametrosSistema_NomeSistema);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(1);
            }
            pr_default.close(1);
            /* Using cursor P00714 */
            pr_default.execute(2, new Object[] {AV20AreaTrabalho_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A29Contratante_Codigo = P00714_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P00714_n29Contratante_Codigo[0];
               A5AreaTrabalho_Codigo = P00714_A5AreaTrabalho_Codigo[0];
               A548Contratante_EmailSdaUser = P00714_A548Contratante_EmailSdaUser[0];
               n548Contratante_EmailSdaUser = P00714_n548Contratante_EmailSdaUser[0];
               A547Contratante_EmailSdaHost = P00714_A547Contratante_EmailSdaHost[0];
               n547Contratante_EmailSdaHost = P00714_n547Contratante_EmailSdaHost[0];
               A549Contratante_EmailSdaPass = P00714_A549Contratante_EmailSdaPass[0];
               n549Contratante_EmailSdaPass = P00714_n549Contratante_EmailSdaPass[0];
               A550Contratante_EmailSdaKey = P00714_A550Contratante_EmailSdaKey[0];
               n550Contratante_EmailSdaKey = P00714_n550Contratante_EmailSdaKey[0];
               A552Contratante_EmailSdaPort = P00714_A552Contratante_EmailSdaPort[0];
               n552Contratante_EmailSdaPort = P00714_n552Contratante_EmailSdaPort[0];
               A551Contratante_EmailSdaAut = P00714_A551Contratante_EmailSdaAut[0];
               n551Contratante_EmailSdaAut = P00714_n551Contratante_EmailSdaAut[0];
               A1048Contratante_EmailSdaSec = P00714_A1048Contratante_EmailSdaSec[0];
               n1048Contratante_EmailSdaSec = P00714_n1048Contratante_EmailSdaSec[0];
               A548Contratante_EmailSdaUser = P00714_A548Contratante_EmailSdaUser[0];
               n548Contratante_EmailSdaUser = P00714_n548Contratante_EmailSdaUser[0];
               A547Contratante_EmailSdaHost = P00714_A547Contratante_EmailSdaHost[0];
               n547Contratante_EmailSdaHost = P00714_n547Contratante_EmailSdaHost[0];
               A549Contratante_EmailSdaPass = P00714_A549Contratante_EmailSdaPass[0];
               n549Contratante_EmailSdaPass = P00714_n549Contratante_EmailSdaPass[0];
               A550Contratante_EmailSdaKey = P00714_A550Contratante_EmailSdaKey[0];
               n550Contratante_EmailSdaKey = P00714_n550Contratante_EmailSdaKey[0];
               A552Contratante_EmailSdaPort = P00714_A552Contratante_EmailSdaPort[0];
               n552Contratante_EmailSdaPort = P00714_n552Contratante_EmailSdaPort[0];
               A551Contratante_EmailSdaAut = P00714_A551Contratante_EmailSdaAut[0];
               n551Contratante_EmailSdaAut = P00714_n551Contratante_EmailSdaAut[0];
               A1048Contratante_EmailSdaSec = P00714_A1048Contratante_EmailSdaSec[0];
               n1048Contratante_EmailSdaSec = P00714_n1048Contratante_EmailSdaSec[0];
               AV12SMTPSession.AttachDir = "\\";
               AV12SMTPSession.Sender.Name = StringUtil.Trim( A548Contratante_EmailSdaUser);
               AV12SMTPSession.Sender.Address = StringUtil.Trim( A548Contratante_EmailSdaUser);
               AV12SMTPSession.Host = StringUtil.Trim( A547Contratante_EmailSdaHost);
               AV12SMTPSession.UserName = StringUtil.Trim( A548Contratante_EmailSdaUser);
               if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( A549Contratante_EmailSdaPass)) || P00714_n549Contratante_EmailSdaPass[0] ) )
               {
                  AV12SMTPSession.Password = Crypto.Decrypt64( A549Contratante_EmailSdaPass, A550Contratante_EmailSdaKey);
               }
               AV12SMTPSession.Port = A552Contratante_EmailSdaPort;
               AV12SMTPSession.Authentication = (short)((A551Contratante_EmailSdaAut ? 1 : 0));
               AV12SMTPSession.Secure = A1048Contratante_EmailSdaSec;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(2);
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV12SMTPSession.Host)) || (0==AV12SMTPSession.Port) || String.IsNullOrEmpty(StringUtil.RTrim( AV12SMTPSession.UserName)) )
            {
               AV22Resultado = "Cadastro da Contratante sem dados configurados para envio de e-mails!" + StringUtil.NewLine( );
               new prc_contagemresultadonotificacao(context ).execute(  AV18Usuarios,  AV19Subject,  AV14EmailText,  "EML",  0,  AV22Resultado) ;
            }
            else
            {
               AV12SMTPSession.ErrDisplay = 1;
               if ( StringUtil.StringSearch( AV19Subject, AV26NomeSistema, 1) > 0 )
               {
                  AV10Email.Subject = AV19Subject;
               }
               else
               {
                  AV10Email.Subject = "["+AV26NomeSistema+"] "+AV19Subject;
               }
               AV10Email.Text = AV14EmailText;
               AV10Email.Text = AV10Email.Text+StringUtil.Chr( 13)+"Enviado automaticamente pelo aplicativo "+AV26NomeSistema+StringUtil.Chr( 13);
               AV38GXV1 = 1;
               while ( AV38GXV1 <= AV15Attachments.Count )
               {
                  AV23Attachment = ((String)AV15Attachments.Item(AV38GXV1));
                  AV10Email.Attachments.Add(AV23Attachment) ;
                  AV38GXV1 = (int)(AV38GXV1+1);
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( AV22Resultado)) )
               {
                  AV22Resultado = "Envio de notifica��o";
               }
               AV12SMTPSession.Login();
               if ( (0==AV12SMTPSession.ErrCode) )
               {
                  AV12SMTPSession.Send(AV10Email);
                  if ( (0==AV12SMTPSession.ErrCode) )
                  {
                     if ( StringUtil.StringSearch( AV22Resultado, "sucesso", 1) == 0 )
                     {
                        AV22Resultado = AV22Resultado + " para " + AV24Destinatarios + " com sucesso";
                     }
                     new prc_contagemresultadonotificacao(context ).execute(  AV18Usuarios,  AV19Subject,  AV14EmailText,  "EML",  1,  AV22Resultado) ;
                  }
                  else
                  {
                     AV22Resultado = AV22Resultado + " retornou: " + AV12SMTPSession.ErrDescription;
                     AV13Message.gxTpr_Description = AV12SMTPSession.ErrDescription;
                     AV17Messages.Add(AV13Message, 0);
                     new prc_contagemresultadonotificacao(context ).execute(  AV18Usuarios,  AV19Subject,  AV14EmailText,  "EML",  1,  AV22Resultado) ;
                  }
                  AV12SMTPSession.Logout();
               }
               else
               {
                  AV22Resultado = "Login para " + StringUtil.Lower( AV22Resultado) + StringUtil.Trim( AV12SMTPSession.Sender.Address) + " retornou: " + AV12SMTPSession.ErrDescription;
                  new prc_contagemresultadonotificacao(context ).execute(  AV18Usuarios,  AV19Subject,  AV14EmailText,  "EML",  2,  AV22Resultado) ;
               }
            }
         }
         else
         {
            AV22Resultado = "Usu�rio sem endere�o de e-mail para env�o do teste!";
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00712_A1Usuario_Codigo = new int[1] ;
         P00712_A1647Usuario_Email = new String[] {""} ;
         P00712_n1647Usuario_Email = new bool[] {false} ;
         P00712_A341Usuario_UserGamGuid = new String[] {""} ;
         A1647Usuario_Email = "";
         A341Usuario_UserGamGuid = "";
         AV9MailRecipient = new GeneXus.Mail.GXMailRecipient();
         AV24Destinatarios = "";
         AV32Virgola = "";
         AV10Email = new GeneXus.Mail.GXMailMessage();
         AV11GamUser = new SdtGAMUser(context);
         P00713_A331ParametrosSistema_NomeSistema = new String[] {""} ;
         P00713_A330ParametrosSistema_Codigo = new int[1] ;
         A331ParametrosSistema_NomeSistema = "";
         AV26NomeSistema = "";
         P00714_A29Contratante_Codigo = new int[1] ;
         P00714_n29Contratante_Codigo = new bool[] {false} ;
         P00714_A5AreaTrabalho_Codigo = new int[1] ;
         P00714_A548Contratante_EmailSdaUser = new String[] {""} ;
         P00714_n548Contratante_EmailSdaUser = new bool[] {false} ;
         P00714_A547Contratante_EmailSdaHost = new String[] {""} ;
         P00714_n547Contratante_EmailSdaHost = new bool[] {false} ;
         P00714_A549Contratante_EmailSdaPass = new String[] {""} ;
         P00714_n549Contratante_EmailSdaPass = new bool[] {false} ;
         P00714_A550Contratante_EmailSdaKey = new String[] {""} ;
         P00714_n550Contratante_EmailSdaKey = new bool[] {false} ;
         P00714_A552Contratante_EmailSdaPort = new short[1] ;
         P00714_n552Contratante_EmailSdaPort = new bool[] {false} ;
         P00714_A551Contratante_EmailSdaAut = new bool[] {false} ;
         P00714_n551Contratante_EmailSdaAut = new bool[] {false} ;
         P00714_A1048Contratante_EmailSdaSec = new short[1] ;
         P00714_n1048Contratante_EmailSdaSec = new bool[] {false} ;
         A548Contratante_EmailSdaUser = "";
         A547Contratante_EmailSdaHost = "";
         A549Contratante_EmailSdaPass = "";
         A550Contratante_EmailSdaKey = "";
         AV12SMTPSession = new GeneXus.Mail.GXSMTPSession(context.GetPhysicalPath());
         AV23Attachment = "";
         AV13Message = new SdtMessages_Message(context);
         AV17Messages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_enviaremail__default(),
            new Object[][] {
                new Object[] {
               P00712_A1Usuario_Codigo, P00712_A1647Usuario_Email, P00712_n1647Usuario_Email, P00712_A341Usuario_UserGamGuid
               }
               , new Object[] {
               P00713_A331ParametrosSistema_NomeSistema, P00713_A330ParametrosSistema_Codigo
               }
               , new Object[] {
               P00714_A29Contratante_Codigo, P00714_n29Contratante_Codigo, P00714_A5AreaTrabalho_Codigo, P00714_A548Contratante_EmailSdaUser, P00714_n548Contratante_EmailSdaUser, P00714_A547Contratante_EmailSdaHost, P00714_n547Contratante_EmailSdaHost, P00714_A549Contratante_EmailSdaPass, P00714_n549Contratante_EmailSdaPass, P00714_A550Contratante_EmailSdaKey,
               P00714_n550Contratante_EmailSdaKey, P00714_A552Contratante_EmailSdaPort, P00714_n552Contratante_EmailSdaPort, P00714_A551Contratante_EmailSdaAut, P00714_n551Contratante_EmailSdaAut, P00714_A1048Contratante_EmailSdaSec, P00714_n1048Contratante_EmailSdaSec
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV25i ;
      private short A552Contratante_EmailSdaPort ;
      private short A1048Contratante_EmailSdaSec ;
      private int AV20AreaTrabalho_Codigo ;
      private int A1Usuario_Codigo ;
      private int A330ParametrosSistema_Codigo ;
      private int A29Contratante_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int AV38GXV1 ;
      private String AV19Subject ;
      private String AV14EmailText ;
      private String AV22Resultado ;
      private String scmdbuf ;
      private String A341Usuario_UserGamGuid ;
      private String AV32Virgola ;
      private String A331ParametrosSistema_NomeSistema ;
      private String AV26NomeSistema ;
      private String A550Contratante_EmailSdaKey ;
      private bool n1647Usuario_Email ;
      private bool n29Contratante_Codigo ;
      private bool n548Contratante_EmailSdaUser ;
      private bool n547Contratante_EmailSdaHost ;
      private bool n549Contratante_EmailSdaPass ;
      private bool n550Contratante_EmailSdaKey ;
      private bool n552Contratante_EmailSdaPort ;
      private bool A551Contratante_EmailSdaAut ;
      private bool n551Contratante_EmailSdaAut ;
      private bool n1048Contratante_EmailSdaSec ;
      private String AV23Attachment ;
      private String A1647Usuario_Email ;
      private String AV24Destinatarios ;
      private String A548Contratante_EmailSdaUser ;
      private String A547Contratante_EmailSdaHost ;
      private String A549Contratante_EmailSdaPass ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP5_Resultado ;
      private IDataStoreProvider pr_default ;
      private int[] P00712_A1Usuario_Codigo ;
      private String[] P00712_A1647Usuario_Email ;
      private bool[] P00712_n1647Usuario_Email ;
      private String[] P00712_A341Usuario_UserGamGuid ;
      private String[] P00713_A331ParametrosSistema_NomeSistema ;
      private int[] P00713_A330ParametrosSistema_Codigo ;
      private int[] P00714_A29Contratante_Codigo ;
      private bool[] P00714_n29Contratante_Codigo ;
      private int[] P00714_A5AreaTrabalho_Codigo ;
      private String[] P00714_A548Contratante_EmailSdaUser ;
      private bool[] P00714_n548Contratante_EmailSdaUser ;
      private String[] P00714_A547Contratante_EmailSdaHost ;
      private bool[] P00714_n547Contratante_EmailSdaHost ;
      private String[] P00714_A549Contratante_EmailSdaPass ;
      private bool[] P00714_n549Contratante_EmailSdaPass ;
      private String[] P00714_A550Contratante_EmailSdaKey ;
      private bool[] P00714_n550Contratante_EmailSdaKey ;
      private short[] P00714_A552Contratante_EmailSdaPort ;
      private bool[] P00714_n552Contratante_EmailSdaPort ;
      private bool[] P00714_A551Contratante_EmailSdaAut ;
      private bool[] P00714_n551Contratante_EmailSdaAut ;
      private short[] P00714_A1048Contratante_EmailSdaSec ;
      private bool[] P00714_n1048Contratante_EmailSdaSec ;
      private GeneXus.Mail.GXMailMessage AV10Email ;
      private GeneXus.Mail.GXMailRecipient AV9MailRecipient ;
      private GeneXus.Mail.GXSMTPSession AV12SMTPSession ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV18Usuarios ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV15Attachments ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV17Messages ;
      private SdtGAMUser AV11GamUser ;
      private SdtMessages_Message AV13Message ;
   }

   public class prc_enviaremail__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00712( IGxContext context ,
                                             int A1Usuario_Codigo ,
                                             IGxCollection AV18Usuarios )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object1 ;
         GXv_Object1 = new Object [2] ;
         scmdbuf = "SELECT [Usuario_Codigo], [Usuario_Email], [Usuario_UserGamGuid] FROM [Usuario] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV18Usuarios, "[Usuario_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Usuario_Codigo]";
         GXv_Object1[0] = scmdbuf;
         return GXv_Object1 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00712(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00713 ;
          prmP00713 = new Object[] {
          } ;
          Object[] prmP00714 ;
          prmP00714 = new Object[] {
          new Object[] {"@AV20AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00712 ;
          prmP00712 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("P00712", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00712,100,0,true,false )
             ,new CursorDef("P00713", "SELECT TOP 1 [ParametrosSistema_NomeSistema], [ParametrosSistema_Codigo] FROM [ParametrosSistema] WITH (NOLOCK) ORDER BY [ParametrosSistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00713,1,0,false,true )
             ,new CursorDef("P00714", "SELECT TOP 1 T1.[Contratante_Codigo], T1.[AreaTrabalho_Codigo], T2.[Contratante_EmailSdaUser], T2.[Contratante_EmailSdaHost], T2.[Contratante_EmailSdaPass], T2.[Contratante_EmailSdaKey], T2.[Contratante_EmailSdaPort], T2.[Contratante_EmailSdaAut], T2.[Contratante_EmailSdaSec] FROM ([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) WHERE T1.[AreaTrabalho_Codigo] = @AV20AreaTrabalho_Codigo ORDER BY T1.[AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00714,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 40) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 32) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((short[]) buf[11])[0] = rslt.getShort(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((bool[]) buf[13])[0] = rslt.getBool(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((short[]) buf[15])[0] = rslt.getShort(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
