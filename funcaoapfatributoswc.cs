/*
               File: FuncaoAPFAtributosWC
        Description: Funcao APFAtributos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:48:48.46
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class funcaoapfatributoswc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public funcaoapfatributoswc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public funcaoapfatributoswc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_FuncaoAPF_Codigo ,
                           int aP1_Sistema_Codigo )
      {
         this.AV9FuncaoAPF_Codigo = aP0_FuncaoAPF_Codigo;
         this.A127Sistema_Codigo = aP1_Sistema_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         dynavFuncaodados_codigo = new GXCombobox();
         dynavTabela_codigo = new GXCombobox();
         chkAtributos_Ativo = new GXCheckbox();
         chkavFlag = new GXCheckbox();
         cmbAtributos_TipoDados = new GXCombobox();
         chkavFuncoesapfatributos_regra = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV9FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV9FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9FuncaoAPF_Codigo), 6, 0)));
                  A127Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV9FuncaoAPF_Codigo,(int)A127Sistema_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vFUNCAODADOS_CODIGO") == 0 )
               {
                  A127Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLVvFUNCAODADOS_CODIGO962( A127Sistema_Codigo) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vTABELA_CODIGO") == 0 )
               {
                  AV10FuncaoDados_Codigo = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV10FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10FuncaoDados_Codigo), 4, 0)));
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLVvTABELA_CODIGO962( AV10FuncaoDados_Codigo) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridatributos") == 0 )
               {
                  nRC_GXsfl_43 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_43_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_43_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGridatributos_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridatributos") == 0 )
               {
                  subGridatributos_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13Tabela_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Tabela_Codigo), 6, 0)));
                  A176Atributos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A165FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
                  AV9FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV9FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9FuncaoAPF_Codigo), 6, 0)));
                  A364FuncaoAPFAtributos_AtributosCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A364FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0)));
                  AV5Atributos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5Atributos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Atributos_Codigo), 6, 0)));
                  A389FuncoesAPFAtributos_Regra = (bool)(BooleanUtil.Val(GetNextPar( )));
                  n389FuncoesAPFAtributos_Regra = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A389FuncoesAPFAtributos_Regra", A389FuncoesAPFAtributos_Regra);
                  A383FuncoesAPFAtributos_Code = GetNextPar( );
                  n383FuncoesAPFAtributos_Code = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A383FuncoesAPFAtributos_Code", A383FuncoesAPFAtributos_Code);
                  A384FuncoesAPFAtributos_Nome = GetNextPar( );
                  n384FuncoesAPFAtributos_Nome = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A384FuncoesAPFAtributos_Nome", A384FuncoesAPFAtributos_Nome);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGridatributos_refresh( subGridatributos_Rows, AV13Tabela_Codigo, A176Atributos_Codigo, A165FuncaoAPF_Codigo, AV9FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod, AV5Atributos_Codigo, A389FuncoesAPFAtributos_Regra, A383FuncoesAPFAtributos_Code, A384FuncoesAPFAtributos_Nome, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA962( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               edtavFuncoesapfatributos_code_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncoesapfatributos_code_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncoesapfatributos_code_Enabled), 5, 0)));
               edtavFuncoesapfatributos_nome_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncoesapfatributos_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncoesapfatributos_nome_Enabled), 5, 0)));
               GXVvFUNCAODADOS_CODIGO_html962( A127Sistema_Codigo) ;
               GXVvTABELA_CODIGO_html962( AV10FuncaoDados_Codigo) ;
               WS962( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Funcao APFAtributos") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051812484859");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("funcaoapfatributoswc.aspx") + "?" + UrlEncode("" +AV9FuncaoAPF_Codigo) + "," + UrlEncode("" +A127Sistema_Codigo)+"\">") ;
               GxWebStd.gx_hidden_field( context, "_EventName", "");
               GxWebStd.gx_hidden_field( context, "_EventGridId", "");
               GxWebStd.gx_hidden_field( context, "_EventRowId", "");
               context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            }
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTABELA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Tabela_Codigo), 6, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_43", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_43), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV9FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV9FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA127Sistema_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA127Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vFUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vATRIBUTOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV5Atributos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"FUNCOESAPFATRIBUTOS_REGRA", A389FuncoesAPFAtributos_Regra);
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCOESAPFATRIBUTOS_CODE", A383FuncoesAPFAtributos_Code);
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCOESAPFATRIBUTOS_NOME", StringUtil.RTrim( A384FuncoesAPFAtributos_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"SISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE2_Width", StringUtil.RTrim( Dvpanel_unnamedtable2_Width));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE2_Cls", StringUtil.RTrim( Dvpanel_unnamedtable2_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE2_Title", StringUtil.RTrim( Dvpanel_unnamedtable2_Title));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE2_Collapsible", StringUtil.BoolToStr( Dvpanel_unnamedtable2_Collapsible));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE2_Collapsed", StringUtil.BoolToStr( Dvpanel_unnamedtable2_Collapsed));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE2_Autowidth", StringUtil.BoolToStr( Dvpanel_unnamedtable2_Autowidth));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE2_Autoheight", StringUtil.BoolToStr( Dvpanel_unnamedtable2_Autoheight));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE2_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_unnamedtable2_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE2_Iconposition", StringUtil.RTrim( Dvpanel_unnamedtable2_Iconposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVPANEL_UNNAMEDTABLE2_Autoscroll", StringUtil.BoolToStr( Dvpanel_unnamedtable2_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm962( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("funcaoapfatributoswc.js", "?202051812484875");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "</form>") ;
            }
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "FuncaoAPFAtributosWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Funcao APFAtributos" ;
      }

      protected void WB960( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "funcaoapfatributoswc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
            }
            wb_table1_2_962( true) ;
         }
         else
         {
            wb_table1_2_962( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_962e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START962( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Funcao APFAtributos", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP960( ) ;
            }
         }
      }

      protected void WS962( )
      {
         START962( ) ;
         EVT962( ) ;
      }

      protected void EVT962( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP960( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VFUNCAODADOS_CODIGO.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP960( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11962 */
                                    E11962 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VTABELA_CODIGO.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP960( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12962 */
                                    E12962 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP960( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = chkavFlag_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDATRIBUTOSPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP960( ) ;
                              }
                              sEvt = cgiGet( sPrefix+"GRIDATRIBUTOSPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgridatributos_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgridatributos_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgridatributos_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgridatributos_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 18), "GRIDATRIBUTOS.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 11), "VFLAG.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 32), "VFUNCOESAPFATRIBUTOS_REGRA.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 11), "VFLAG.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 32), "VFUNCOESAPFATRIBUTOS_REGRA.CLICK") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP960( ) ;
                              }
                              nGXsfl_43_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_43_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_43_idx), 4, 0)), 4, "0");
                              SubsflControlProps_432( ) ;
                              A176Atributos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAtributos_Codigo_Internalname), ",", "."));
                              A356Atributos_TabelaCod = (int)(context.localUtil.CToN( cgiGet( edtAtributos_TabelaCod_Internalname), ",", "."));
                              A180Atributos_Ativo = StringUtil.StrToBool( cgiGet( chkAtributos_Ativo_Internalname));
                              AV8Flag = StringUtil.StrToBool( cgiGet( chkavFlag_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, chkavFlag_Internalname, AV8Flag);
                              A177Atributos_Nome = StringUtil.Upper( cgiGet( edtAtributos_Nome_Internalname));
                              cmbAtributos_TipoDados.Name = cmbAtributos_TipoDados_Internalname;
                              cmbAtributos_TipoDados.CurrentValue = cgiGet( cmbAtributos_TipoDados_Internalname);
                              A178Atributos_TipoDados = cgiGet( cmbAtributos_TipoDados_Internalname);
                              n178Atributos_TipoDados = false;
                              AV15FuncoesAPFAtributos_Regra = StringUtil.StrToBool( cgiGet( chkavFuncoesapfatributos_regra_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, chkavFuncoesapfatributos_regra_Internalname, AV15FuncoesAPFAtributos_Regra);
                              AV16FuncoesAPFAtributos_Code = cgiGet( edtavFuncoesapfatributos_code_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFuncoesapfatributos_code_Internalname, AV16FuncoesAPFAtributos_Code);
                              AV17FuncoesAPFAtributos_Nome = StringUtil.Upper( cgiGet( edtavFuncoesapfatributos_nome_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFuncoesapfatributos_nome_Internalname, AV17FuncoesAPFAtributos_Nome);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = chkavFlag_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E13962 */
                                          E13962 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDATRIBUTOS.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = chkavFlag_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E14962 */
                                          E14962 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VFLAG.CLICK") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = chkavFlag_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E15962 */
                                          E15962 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VFUNCOESAPFATRIBUTOS_REGRA.CLICK") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = chkavFlag_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E16962 */
                                          E16962 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = chkavFlag_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E17962 */
                                          E17962 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Tabela_codigo Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTABELA_CODIGO"), ",", ".") != Convert.ToDecimal( AV13Tabela_Codigo )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = chkavFlag_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUP960( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = chkavFlag_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE962( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm962( ) ;
            }
         }
      }

      protected void PA962( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            dynavFuncaodados_codigo.Name = "vFUNCAODADOS_CODIGO";
            dynavFuncaodados_codigo.WebTags = "";
            dynavTabela_codigo.Name = "vTABELA_CODIGO";
            dynavTabela_codigo.WebTags = "";
            GXCCtl = "ATRIBUTOS_ATIVO_" + sGXsfl_43_idx;
            chkAtributos_Ativo.Name = GXCCtl;
            chkAtributos_Ativo.WebTags = "";
            chkAtributos_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkAtributos_Ativo_Internalname, "TitleCaption", chkAtributos_Ativo.Caption);
            chkAtributos_Ativo.CheckedValue = "false";
            GXCCtl = "vFLAG_" + sGXsfl_43_idx;
            chkavFlag.Name = GXCCtl;
            chkavFlag.WebTags = "";
            chkavFlag.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavFlag_Internalname, "TitleCaption", chkavFlag.Caption);
            chkavFlag.CheckedValue = "false";
            GXCCtl = "ATRIBUTOS_TIPODADOS_" + sGXsfl_43_idx;
            cmbAtributos_TipoDados.Name = GXCCtl;
            cmbAtributos_TipoDados.WebTags = "";
            cmbAtributos_TipoDados.addItem("", "Desconhecido", 0);
            cmbAtributos_TipoDados.addItem("N", "Numeric", 0);
            cmbAtributos_TipoDados.addItem("C", "Character", 0);
            cmbAtributos_TipoDados.addItem("VC", "Varchar", 0);
            cmbAtributos_TipoDados.addItem("D", "Date", 0);
            cmbAtributos_TipoDados.addItem("DT", "Date Time", 0);
            cmbAtributos_TipoDados.addItem("Bool", "Boolean", 0);
            cmbAtributos_TipoDados.addItem("Blob", "Blob", 0);
            cmbAtributos_TipoDados.addItem("Outr", "Outros", 0);
            if ( cmbAtributos_TipoDados.ItemCount > 0 )
            {
               A178Atributos_TipoDados = cmbAtributos_TipoDados.getValidValue(A178Atributos_TipoDados);
               n178Atributos_TipoDados = false;
            }
            GXCCtl = "vFUNCOESAPFATRIBUTOS_REGRA_" + sGXsfl_43_idx;
            chkavFuncoesapfatributos_regra.Name = GXCCtl;
            chkavFuncoesapfatributos_regra.WebTags = "";
            chkavFuncoesapfatributos_regra.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavFuncoesapfatributos_regra_Internalname, "TitleCaption", chkavFuncoesapfatributos_regra.Caption);
            chkavFuncoesapfatributos_regra.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavFuncaodados_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         GXVvTABELA_CODIGO_html962( AV10FuncaoDados_Codigo) ;
         /* End function dynload_actions */
      }

      protected void GXDLVvFUNCAODADOS_CODIGO962( int A127Sistema_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvFUNCAODADOS_CODIGO_data962( A127Sistema_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvFUNCAODADOS_CODIGO_html962( int A127Sistema_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvFUNCAODADOS_CODIGO_data962( A127Sistema_Codigo) ;
         gxdynajaxindex = 1;
         dynavFuncaodados_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavFuncaodados_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 4, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavFuncaodados_codigo.ItemCount > 0 )
         {
            AV10FuncaoDados_Codigo = (short)(NumberUtil.Val( dynavFuncaodados_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV10FuncaoDados_Codigo), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV10FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10FuncaoDados_Codigo), 4, 0)));
         }
      }

      protected void GXDLVvFUNCAODADOS_CODIGO_data962( int A127Sistema_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H00962 */
         pr_default.execute(0, new Object[] {A127Sistema_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00962_A368FuncaoDados_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00962_A419FuncaoDados_Nome50[0]);
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvTABELA_CODIGO962( short AV10FuncaoDados_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvTABELA_CODIGO_data962( AV10FuncaoDados_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvTABELA_CODIGO_html962( short AV10FuncaoDados_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvTABELA_CODIGO_data962( AV10FuncaoDados_Codigo) ;
         gxdynajaxindex = 1;
         dynavTabela_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavTabela_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavTabela_codigo.ItemCount > 0 )
         {
            AV13Tabela_Codigo = (int)(NumberUtil.Val( dynavTabela_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13Tabela_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Tabela_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvTABELA_CODIGO_data962( short AV10FuncaoDados_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H00963 */
         pr_default.execute(1, new Object[] {AV10FuncaoDados_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00963_A172Tabela_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00963_A173Tabela_Nome[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void gxnrGridatributos_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_432( ) ;
         while ( nGXsfl_43_idx <= nRC_GXsfl_43 )
         {
            sendrow_432( ) ;
            nGXsfl_43_idx = (short)(((subGridatributos_Islastpage==1)&&(nGXsfl_43_idx+1>subGridatributos_Recordsperpage( )) ? 1 : nGXsfl_43_idx+1));
            sGXsfl_43_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_43_idx), 4, 0)), 4, "0");
            SubsflControlProps_432( ) ;
         }
         context.GX_webresponse.AddString(GridatributosContainer.ToJavascriptSource());
         /* End function gxnrGridatributos_newrow */
      }

      protected void gxgrGridatributos_refresh( int subGridatributos_Rows ,
                                                int AV13Tabela_Codigo ,
                                                int A176Atributos_Codigo ,
                                                int A165FuncaoAPF_Codigo ,
                                                int AV9FuncaoAPF_Codigo ,
                                                int A364FuncaoAPFAtributos_AtributosCod ,
                                                int AV5Atributos_Codigo ,
                                                bool A389FuncoesAPFAtributos_Regra ,
                                                String A383FuncoesAPFAtributos_Code ,
                                                String A384FuncoesAPFAtributos_Nome ,
                                                String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Rows), 6, 0, ".", "")));
         GRIDATRIBUTOS_nCurrentRecord = 0;
         RF962( ) ;
         /* End function gxgrGridatributos_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A176Atributos_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"ATRIBUTOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A176Atributos_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_TABELACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A356Atributos_TabelaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"ATRIBUTOS_TABELACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A356Atributos_TabelaCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_ATIVO", GetSecureSignedToken( sPrefix, A180Atributos_Ativo));
         GxWebStd.gx_hidden_field( context, sPrefix+"ATRIBUTOS_ATIVO", StringUtil.BoolToStr( A180Atributos_Ativo));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A177Atributos_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"ATRIBUTOS_NOME", StringUtil.RTrim( A177Atributos_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_TIPODADOS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A178Atributos_TipoDados, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"ATRIBUTOS_TIPODADOS", StringUtil.RTrim( A178Atributos_TipoDados));
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavFuncaodados_codigo.ItemCount > 0 )
         {
            AV10FuncaoDados_Codigo = (short)(NumberUtil.Val( dynavFuncaodados_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV10FuncaoDados_Codigo), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV10FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10FuncaoDados_Codigo), 4, 0)));
         }
         if ( dynavTabela_codigo.ItemCount > 0 )
         {
            AV13Tabela_Codigo = (int)(NumberUtil.Val( dynavTabela_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13Tabela_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Tabela_Codigo), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF962( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavFuncoesapfatributos_code_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncoesapfatributos_code_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncoesapfatributos_code_Enabled), 5, 0)));
         edtavFuncoesapfatributos_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncoesapfatributos_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncoesapfatributos_nome_Enabled), 5, 0)));
      }

      protected void RF962( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridatributosContainer.ClearRows();
         }
         wbStart = 43;
         /* Execute user event: E17962 */
         E17962 ();
         nGXsfl_43_idx = 1;
         sGXsfl_43_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_43_idx), 4, 0)), 4, "0");
         SubsflControlProps_432( ) ;
         nGXsfl_43_Refreshing = 1;
         GridatributosContainer.AddObjectProperty("GridName", "Gridatributos");
         GridatributosContainer.AddObjectProperty("CmpContext", sPrefix);
         GridatributosContainer.AddObjectProperty("InMasterPage", "false");
         GridatributosContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridatributosContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridatributosContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridatributosContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Backcolorstyle), 1, 0, ".", "")));
         GridatributosContainer.PageSize = subGridatributos_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_432( ) ;
            GXPagingFrom2 = (int)(((subGridatributos_Rows==0) ? 1 : GRIDATRIBUTOS_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGridatributos_Rows==0) ? 10000 : GRIDATRIBUTOS_nFirstRecordOnPage+subGridatributos_Recordsperpage( )+1));
            /* Using cursor H00964 */
            pr_default.execute(2, new Object[] {AV13Tabela_Codigo, GXPagingFrom2, GXPagingTo2});
            nGXsfl_43_idx = 1;
            while ( ( (pr_default.getStatus(2) != 101) ) && ( ( ( subGridatributos_Rows == 0 ) || ( GRIDATRIBUTOS_nCurrentRecord < subGridatributos_Recordsperpage( ) ) ) ) )
            {
               A178Atributos_TipoDados = H00964_A178Atributos_TipoDados[0];
               n178Atributos_TipoDados = H00964_n178Atributos_TipoDados[0];
               A177Atributos_Nome = H00964_A177Atributos_Nome[0];
               A180Atributos_Ativo = H00964_A180Atributos_Ativo[0];
               A356Atributos_TabelaCod = H00964_A356Atributos_TabelaCod[0];
               A176Atributos_Codigo = H00964_A176Atributos_Codigo[0];
               /* Execute user event: E14962 */
               E14962 ();
               pr_default.readNext(2);
            }
            GRIDATRIBUTOS_nEOF = (short)(((pr_default.getStatus(2) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nEOF), 1, 0, ".", "")));
            pr_default.close(2);
            wbEnd = 43;
            WB960( ) ;
         }
         nGXsfl_43_Refreshing = 0;
      }

      protected int subGridatributos_Pagecount( )
      {
         GRIDATRIBUTOS_nRecordCount = subGridatributos_Recordcount( );
         if ( ((int)((GRIDATRIBUTOS_nRecordCount) % (subGridatributos_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRIDATRIBUTOS_nRecordCount/ (decimal)(subGridatributos_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRIDATRIBUTOS_nRecordCount/ (decimal)(subGridatributos_Recordsperpage( ))))+1) ;
      }

      protected int subGridatributos_Recordcount( )
      {
         /* Using cursor H00965 */
         pr_default.execute(3, new Object[] {AV13Tabela_Codigo});
         GRIDATRIBUTOS_nRecordCount = H00965_AGRIDATRIBUTOS_nRecordCount[0];
         pr_default.close(3);
         return (int)(GRIDATRIBUTOS_nRecordCount) ;
      }

      protected int subGridatributos_Recordsperpage( )
      {
         if ( subGridatributos_Rows > 0 )
         {
            return subGridatributos_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGridatributos_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRIDATRIBUTOS_nFirstRecordOnPage/ (decimal)(subGridatributos_Recordsperpage( ))))+1) ;
      }

      protected short subgridatributos_firstpage( )
      {
         GRIDATRIBUTOS_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, AV13Tabela_Codigo, A176Atributos_Codigo, A165FuncaoAPF_Codigo, AV9FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod, AV5Atributos_Codigo, A389FuncoesAPFAtributos_Regra, A383FuncoesAPFAtributos_Code, A384FuncoesAPFAtributos_Nome, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgridatributos_nextpage( )
      {
         GRIDATRIBUTOS_nRecordCount = subGridatributos_Recordcount( );
         if ( ( GRIDATRIBUTOS_nRecordCount >= subGridatributos_Recordsperpage( ) ) && ( GRIDATRIBUTOS_nEOF == 0 ) )
         {
            GRIDATRIBUTOS_nFirstRecordOnPage = (long)(GRIDATRIBUTOS_nFirstRecordOnPage+subGridatributos_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, AV13Tabela_Codigo, A176Atributos_Codigo, A165FuncaoAPF_Codigo, AV9FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod, AV5Atributos_Codigo, A389FuncoesAPFAtributos_Regra, A383FuncoesAPFAtributos_Code, A384FuncoesAPFAtributos_Nome, sPrefix) ;
         }
         return (short)(((GRIDATRIBUTOS_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgridatributos_previouspage( )
      {
         if ( GRIDATRIBUTOS_nFirstRecordOnPage >= subGridatributos_Recordsperpage( ) )
         {
            GRIDATRIBUTOS_nFirstRecordOnPage = (long)(GRIDATRIBUTOS_nFirstRecordOnPage-subGridatributos_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, AV13Tabela_Codigo, A176Atributos_Codigo, A165FuncaoAPF_Codigo, AV9FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod, AV5Atributos_Codigo, A389FuncoesAPFAtributos_Regra, A383FuncoesAPFAtributos_Code, A384FuncoesAPFAtributos_Nome, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgridatributos_lastpage( )
      {
         GRIDATRIBUTOS_nRecordCount = subGridatributos_Recordcount( );
         if ( GRIDATRIBUTOS_nRecordCount > subGridatributos_Recordsperpage( ) )
         {
            if ( ((int)((GRIDATRIBUTOS_nRecordCount) % (subGridatributos_Recordsperpage( )))) == 0 )
            {
               GRIDATRIBUTOS_nFirstRecordOnPage = (long)(GRIDATRIBUTOS_nRecordCount-subGridatributos_Recordsperpage( ));
            }
            else
            {
               GRIDATRIBUTOS_nFirstRecordOnPage = (long)(GRIDATRIBUTOS_nRecordCount-((int)((GRIDATRIBUTOS_nRecordCount) % (subGridatributos_Recordsperpage( )))));
            }
         }
         else
         {
            GRIDATRIBUTOS_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, AV13Tabela_Codigo, A176Atributos_Codigo, A165FuncaoAPF_Codigo, AV9FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod, AV5Atributos_Codigo, A389FuncoesAPFAtributos_Regra, A383FuncoesAPFAtributos_Code, A384FuncoesAPFAtributos_Nome, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgridatributos_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRIDATRIBUTOS_nFirstRecordOnPage = (long)(subGridatributos_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRIDATRIBUTOS_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDATRIBUTOS_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGridatributos_refresh( subGridatributos_Rows, AV13Tabela_Codigo, A176Atributos_Codigo, A165FuncaoAPF_Codigo, AV9FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod, AV5Atributos_Codigo, A389FuncoesAPFAtributos_Regra, A383FuncoesAPFAtributos_Code, A384FuncoesAPFAtributos_Nome, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUP960( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavFuncoesapfatributos_code_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncoesapfatributos_code_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncoesapfatributos_code_Enabled), 5, 0)));
         edtavFuncoesapfatributos_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncoesapfatributos_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncoesapfatributos_nome_Enabled), 5, 0)));
         GXVvFUNCAODADOS_CODIGO_html962( A127Sistema_Codigo) ;
         GXVvTABELA_CODIGO_html962( AV10FuncaoDados_Codigo) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E13962 */
         E13962 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            dynavFuncaodados_codigo.Name = dynavFuncaodados_codigo_Internalname;
            dynavFuncaodados_codigo.CurrentValue = cgiGet( dynavFuncaodados_codigo_Internalname);
            AV10FuncaoDados_Codigo = (short)(NumberUtil.Val( cgiGet( dynavFuncaodados_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV10FuncaoDados_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10FuncaoDados_Codigo), 4, 0)));
            dynavTabela_codigo.Name = dynavTabela_codigo_Internalname;
            dynavTabela_codigo.CurrentValue = cgiGet( dynavTabela_codigo_Internalname);
            AV13Tabela_Codigo = (int)(NumberUtil.Val( cgiGet( dynavTabela_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Tabela_Codigo), 6, 0)));
            /* Read saved values. */
            nRC_GXsfl_43 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_43"), ",", "."));
            wcpOAV9FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV9FuncaoAPF_Codigo"), ",", "."));
            wcpOA127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA127Sistema_Codigo"), ",", "."));
            GRIDATRIBUTOS_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDATRIBUTOS_nFirstRecordOnPage"), ",", "."));
            GRIDATRIBUTOS_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDATRIBUTOS_nEOF"), ",", "."));
            subGridatributos_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDATRIBUTOS_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Rows), 6, 0, ".", "")));
            Dvpanel_unnamedtable2_Width = cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE2_Width");
            Dvpanel_unnamedtable2_Cls = cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE2_Cls");
            Dvpanel_unnamedtable2_Title = cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE2_Title");
            Dvpanel_unnamedtable2_Collapsible = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE2_Collapsible"));
            Dvpanel_unnamedtable2_Collapsed = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE2_Collapsed"));
            Dvpanel_unnamedtable2_Autowidth = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE2_Autowidth"));
            Dvpanel_unnamedtable2_Autoheight = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE2_Autoheight"));
            Dvpanel_unnamedtable2_Showcollapseicon = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE2_Showcollapseicon"));
            Dvpanel_unnamedtable2_Iconposition = cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE2_Iconposition");
            Dvpanel_unnamedtable2_Autoscroll = StringUtil.StrToBool( cgiGet( sPrefix+"DVPANEL_UNNAMEDTABLE2_Autoscroll"));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTABELA_CODIGO"), ",", ".") != Convert.ToDecimal( AV13Tabela_Codigo )) )
            {
               GRIDATRIBUTOS_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E13962 */
         E13962 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E13962( )
      {
         /* Start Routine */
         subGridatributos_Rows = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDATRIBUTOS_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Rows), 6, 0, ".", "")));
         dynavFuncaodados_codigo.Width = 350;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavFuncaodados_codigo_Internalname, "Width", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavFuncaodados_codigo.Width), 9, 0)));
      }

      private void E14962( )
      {
         /* Gridatributos_Load Routine */
         AV5Atributos_Codigo = A176Atributos_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5Atributos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Atributos_Codigo), 6, 0)));
         /* Execute user subroutine: 'SETVARIAVEIS' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 43;
         }
         sendrow_432( ) ;
         GRIDATRIBUTOS_nCurrentRecord = (long)(GRIDATRIBUTOS_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_43_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(43, GridatributosRow);
         }
      }

      protected void E15962( )
      {
         /* Flag_Click Routine */
         AV5Atributos_Codigo = A176Atributos_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5Atributos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Atributos_Codigo), 6, 0)));
         if ( ! AV8Flag )
         {
            AV15FuncoesAPFAtributos_Regra = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, chkavFuncoesapfatributos_regra_Internalname, AV15FuncoesAPFAtributos_Regra);
         }
         /* Execute user subroutine: 'UPDATRIBUTOS' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E16962( )
      {
         /* Funcoesapfatributos_regra_Click Routine */
         AV5Atributos_Codigo = A176Atributos_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV5Atributos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Atributos_Codigo), 6, 0)));
         if ( AV15FuncoesAPFAtributos_Regra && ! AV8Flag )
         {
            AV8Flag = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, chkavFlag_Internalname, AV8Flag);
            /* Execute user subroutine: 'UPDATRIBUTOS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         else
         {
            /* Execute user subroutine: 'UPDATRIBUTOREGRA' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E11962( )
      {
         /* Funcaodados_codigo_Click Routine */
         AV14WebSession.Set("FuncaoDados_Codigo", StringUtil.Str( (decimal)(AV10FuncaoDados_Codigo), 4, 0));
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E12962( )
      {
         /* Tabela_codigo_Click Routine */
         AV14WebSession.Set("APFTabela_Codigo", StringUtil.Str( (decimal)(AV13Tabela_Codigo), 6, 0));
      }

      protected void E17962( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         dynavFuncaodados_codigo.Width = 350;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavFuncaodados_codigo_Internalname, "Width", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavFuncaodados_codigo.Width), 9, 0)));
      }

      protected void S122( )
      {
         /* 'UPDATRIBUTOS' Routine */
         if ( AV8Flag )
         {
            AV11FuncoesAPFAtributos = new SdtFuncoesAPFAtributos(context);
            AV11FuncoesAPFAtributos.gxTpr_Funcaoapf_codigo = AV9FuncaoAPF_Codigo;
            AV11FuncoesAPFAtributos.gxTpr_Funcaoapfatributos_atributoscod = AV5Atributos_Codigo;
            AV11FuncoesAPFAtributos.gxTpr_Funcaoapfatributos_funcaodadoscod = AV10FuncaoDados_Codigo;
            AV11FuncoesAPFAtributos.gxTpr_Funcoesapfatributos_regra = AV15FuncoesAPFAtributos_Regra;
            AV11FuncoesAPFAtributos.Save();
         }
         else
         {
            AV11FuncoesAPFAtributos.Load(AV9FuncaoAPF_Codigo, AV5Atributos_Codigo);
            AV11FuncoesAPFAtributos.Delete();
         }
         if ( AV11FuncoesAPFAtributos.Success() )
         {
            context.CommitDataStores( "FuncaoAPFAtributosWC");
            if ( AV15FuncoesAPFAtributos_Regra )
            {
               context.PopUp(formatLink("atributoregradenegocio.aspx") + "?" + UrlEncode("" +AV9FuncaoAPF_Codigo) + "," + UrlEncode("" +AV5Atributos_Codigo), new Object[] {"AV9FuncaoAPF_Codigo","AV5Atributos_Codigo"});
            }
         }
         else
         {
            context.RollbackDataStores( "FuncaoAPFAtributosWC");
            AV8Flag = (bool)(!AV8Flag);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, chkavFlag_Internalname, AV8Flag);
         }
      }

      protected void S132( )
      {
         /* 'UPDATRIBUTOREGRA' Routine */
         AV11FuncoesAPFAtributos.Load(AV9FuncaoAPF_Codigo, AV5Atributos_Codigo);
         AV11FuncoesAPFAtributos.gxTpr_Funcoesapfatributos_regra = AV15FuncoesAPFAtributos_Regra;
         AV11FuncoesAPFAtributos.gxTpr_Funcoesapfatributos_code = "";
         AV11FuncoesAPFAtributos.gxTpr_Funcoesapfatributos_nome = "";
         AV11FuncoesAPFAtributos.gxTpr_Funcoesapfatributos_descricao = "";
         AV11FuncoesAPFAtributos.Save();
         if ( AV11FuncoesAPFAtributos.Success() )
         {
            context.CommitDataStores( "FuncaoAPFAtributosWC");
            if ( AV15FuncoesAPFAtributos_Regra )
            {
               context.PopUp(formatLink("atributoregradenegocio.aspx") + "?" + UrlEncode("" +AV9FuncaoAPF_Codigo) + "," + UrlEncode("" +AV5Atributos_Codigo), new Object[] {"AV9FuncaoAPF_Codigo","AV5Atributos_Codigo"});
            }
         }
         else
         {
            context.RollbackDataStores( "FuncaoAPFAtributosWC");
            AV15FuncoesAPFAtributos_Regra = (bool)(!AV15FuncoesAPFAtributos_Regra);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, chkavFuncoesapfatributos_regra_Internalname, AV15FuncoesAPFAtributos_Regra);
         }
      }

      protected void S112( )
      {
         /* 'SETVARIAVEIS' Routine */
         AV27GXLvl126 = 0;
         /* Using cursor H00966 */
         pr_default.execute(4, new Object[] {AV9FuncaoAPF_Codigo, AV5Atributos_Codigo});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A364FuncaoAPFAtributos_AtributosCod = H00966_A364FuncaoAPFAtributos_AtributosCod[0];
            A165FuncaoAPF_Codigo = H00966_A165FuncaoAPF_Codigo[0];
            A389FuncoesAPFAtributos_Regra = H00966_A389FuncoesAPFAtributos_Regra[0];
            n389FuncoesAPFAtributos_Regra = H00966_n389FuncoesAPFAtributos_Regra[0];
            A383FuncoesAPFAtributos_Code = H00966_A383FuncoesAPFAtributos_Code[0];
            n383FuncoesAPFAtributos_Code = H00966_n383FuncoesAPFAtributos_Code[0];
            A384FuncoesAPFAtributos_Nome = H00966_A384FuncoesAPFAtributos_Nome[0];
            n384FuncoesAPFAtributos_Nome = H00966_n384FuncoesAPFAtributos_Nome[0];
            AV27GXLvl126 = 1;
            AV8Flag = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, chkavFlag_Internalname, AV8Flag);
            AV15FuncoesAPFAtributos_Regra = A389FuncoesAPFAtributos_Regra;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, chkavFuncoesapfatributos_regra_Internalname, AV15FuncoesAPFAtributos_Regra);
            AV16FuncoesAPFAtributos_Code = A383FuncoesAPFAtributos_Code;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFuncoesapfatributos_code_Internalname, AV16FuncoesAPFAtributos_Code);
            AV17FuncoesAPFAtributos_Nome = A384FuncoesAPFAtributos_Nome;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFuncoesapfatributos_nome_Internalname, AV17FuncoesAPFAtributos_Nome);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(4);
         if ( AV27GXLvl126 == 0 )
         {
            AV8Flag = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, chkavFlag_Internalname, AV8Flag);
            AV15FuncoesAPFAtributos_Regra = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, chkavFuncoesapfatributos_regra_Internalname, AV15FuncoesAPFAtributos_Regra);
            AV16FuncoesAPFAtributos_Code = "";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFuncoesapfatributos_code_Internalname, AV16FuncoesAPFAtributos_Code);
            AV17FuncoesAPFAtributos_Nome = "";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFuncoesapfatributos_nome_Internalname, AV17FuncoesAPFAtributos_Nome);
         }
      }

      protected void wb_table1_2_962( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_962( true) ;
         }
         else
         {
            wb_table2_8_962( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_962e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Table100x100'>") ;
            wb_table3_20_962( true) ;
         }
         else
         {
            wb_table3_20_962( false) ;
         }
         return  ;
      }

      protected void wb_table3_20_962e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_962e( true) ;
         }
         else
         {
            wb_table1_2_962e( false) ;
         }
      }

      protected void wb_table3_20_962( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableSearch", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DVPANEL_UNNAMEDTABLE2Container"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+sPrefix+"DVPANEL_UNNAMEDTABLE2Container"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_25_962( true) ;
         }
         else
         {
            wb_table4_25_962( false) ;
         }
         return  ;
      }

      protected void wb_table4_25_962e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_20_962e( true) ;
         }
         else
         {
            wb_table3_20_962e( false) ;
         }
      }

      protected void wb_table4_25_962( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_28_962( true) ;
         }
         else
         {
            wb_table5_28_962( false) ;
         }
         return  ;
      }

      protected void wb_table5_28_962e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table6_40_962( true) ;
         }
         else
         {
            wb_table6_40_962( false) ;
         }
         return  ;
      }

      protected void wb_table6_40_962e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_25_962e( true) ;
         }
         else
         {
            wb_table4_25_962e( false) ;
         }
      }

      protected void wb_table6_40_962( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable4_Internalname, tblUnnamedtable4_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableContentNoMargin'>") ;
            /*  Grid Control  */
            GridatributosContainer.SetWrapped(nGXWrapped);
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridatributosContainer"+"DivS\" data-gxgridid=\"43\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGridatributos_Internalname, subGridatributos_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGridatributos_Backcolorstyle == 0 )
               {
                  subGridatributos_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGridatributos_Class) > 0 )
                  {
                     subGridatributos_Linesclass = subGridatributos_Class+"Title";
                  }
               }
               else
               {
                  subGridatributos_Titlebackstyle = 1;
                  if ( subGridatributos_Backcolorstyle == 1 )
                  {
                     subGridatributos_Titlebackcolor = subGridatributos_Allbackcolor;
                     if ( StringUtil.Len( subGridatributos_Class) > 0 )
                     {
                        subGridatributos_Linesclass = subGridatributos_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGridatributos_Class) > 0 )
                     {
                        subGridatributos_Linesclass = subGridatributos_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Ativo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Nome") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Tipo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Reg. de Neg.") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridatributos_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Nome") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridatributosContainer.AddObjectProperty("GridName", "Gridatributos");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridatributosContainer = new GXWebGrid( context);
               }
               else
               {
                  GridatributosContainer.Clear();
               }
               GridatributosContainer.SetWrapped(nGXWrapped);
               GridatributosContainer.AddObjectProperty("GridName", "Gridatributos");
               GridatributosContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridatributosContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Backcolorstyle), 1, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("CmpContext", sPrefix);
               GridatributosContainer.AddObjectProperty("InMasterPage", "false");
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A176Atributos_Codigo), 6, 0, ".", "")));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A356Atributos_TabelaCod), 6, 0, ".", "")));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A180Atributos_Ativo));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.BoolToStr( AV8Flag));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.RTrim( A177Atributos_Nome));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.RTrim( A178Atributos_TipoDados));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.BoolToStr( AV15FuncoesAPFAtributos_Regra));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", AV16FuncoesAPFAtributos_Code);
               GridatributosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavFuncoesapfatributos_code_Enabled), 5, 0, ".", "")));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridatributosColumn.AddObjectProperty("Value", StringUtil.RTrim( AV17FuncoesAPFAtributos_Nome));
               GridatributosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavFuncoesapfatributos_nome_Enabled), 5, 0, ".", "")));
               GridatributosContainer.AddColumnProperties(GridatributosColumn);
               GridatributosContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Allowselection), 1, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Selectioncolor), 9, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Allowhovering), 1, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Hoveringcolor), 9, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Allowcollapsing), 1, 0, ".", "")));
               GridatributosContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridatributos_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 43 )
         {
            wbEnd = 0;
            nRC_GXsfl_43 = (short)(nGXsfl_43_idx-1);
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridatributosContainer.AddObjectProperty("GRIDATRIBUTOS_nEOF", GRIDATRIBUTOS_nEOF);
               GridatributosContainer.AddObjectProperty("GRIDATRIBUTOS_nFirstRecordOnPage", GRIDATRIBUTOS_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridatributosContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Gridatributos", GridatributosContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridatributosContainerData", GridatributosContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridatributosContainerData"+"V", GridatributosContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridatributosContainerData"+"V"+"\" value='"+GridatributosContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgConsultaratributos_Internalname, context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Consultar atributos", 0, 0, 0, "px", 0, "px", 0, 0, 7, imgConsultaratributos_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+"e18961_client"+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoAPFAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_40_962e( true) ;
         }
         else
         {
            wb_table6_40_962e( false) ;
         }
      }

      protected void wb_table5_28_962( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable3_Internalname, tblUnnamedtable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaodados_codigo_Internalname, "Fun��o de Dados", "", "", lblTextblockfuncaodados_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoAPFAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'" + sPrefix + "',false,'" + sGXsfl_43_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavFuncaodados_codigo, dynavFuncaodados_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV10FuncaoDados_Codigo), 4, 0)), 1, dynavFuncaodados_codigo_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVFUNCAODADOS_CODIGO.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_FuncaoAPFAtributosWC.htm");
            dynavFuncaodados_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10FuncaoDados_Codigo), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavFuncaodados_codigo_Internalname, "Values", (String)(dynavFuncaodados_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktabela_codigo_Internalname, "Tabela", "", "", lblTextblocktabela_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoAPFAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'" + sPrefix + "',false,'" + sGXsfl_43_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavTabela_codigo, dynavTabela_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13Tabela_Codigo), 6, 0)), 1, dynavTabela_codigo_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVTABELA_CODIGO.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "", true, "HLP_FuncaoAPFAtributosWC.htm");
            dynavTabela_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13Tabela_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavTabela_codigo_Internalname, "Values", (String)(dynavTabela_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_28_962e( true) ;
         }
         else
         {
            wb_table5_28_962e( false) ;
         }
      }

      protected void wb_table2_8_962( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_11_962( true) ;
         }
         else
         {
            wb_table7_11_962( false) ;
         }
         return  ;
      }

      protected void wb_table7_11_962e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table8_15_962( true) ;
         }
         else
         {
            wb_table8_15_962( false) ;
         }
         return  ;
      }

      protected void wb_table8_15_962e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_962e( true) ;
         }
         else
         {
            wb_table2_8_962e( false) ;
         }
      }

      protected void wb_table8_15_962( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_15_962e( true) ;
         }
         else
         {
            wb_table8_15_962e( false) ;
         }
      }

      protected void wb_table7_11_962( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_11_962e( true) ;
         }
         else
         {
            wb_table7_11_962e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV9FuncaoAPF_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV9FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9FuncaoAPF_Codigo), 6, 0)));
         A127Sistema_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA962( ) ;
         WS962( ) ;
         WE962( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV9FuncaoAPF_Codigo = (String)((String)getParm(obj,0));
         sCtrlA127Sistema_Codigo = (String)((String)getParm(obj,1));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA962( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "funcaoapfatributoswc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA962( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV9FuncaoAPF_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV9FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9FuncaoAPF_Codigo), 6, 0)));
            A127Sistema_Codigo = Convert.ToInt32(getParm(obj,3));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
         }
         wcpOAV9FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV9FuncaoAPF_Codigo"), ",", "."));
         wcpOA127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA127Sistema_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV9FuncaoAPF_Codigo != wcpOAV9FuncaoAPF_Codigo ) || ( A127Sistema_Codigo != wcpOA127Sistema_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV9FuncaoAPF_Codigo = AV9FuncaoAPF_Codigo;
         wcpOA127Sistema_Codigo = A127Sistema_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV9FuncaoAPF_Codigo = cgiGet( sPrefix+"AV9FuncaoAPF_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV9FuncaoAPF_Codigo) > 0 )
         {
            AV9FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV9FuncaoAPF_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV9FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9FuncaoAPF_Codigo), 6, 0)));
         }
         else
         {
            AV9FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV9FuncaoAPF_Codigo_PARM"), ",", "."));
         }
         sCtrlA127Sistema_Codigo = cgiGet( sPrefix+"A127Sistema_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA127Sistema_Codigo) > 0 )
         {
            A127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA127Sistema_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
         }
         else
         {
            A127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A127Sistema_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA962( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS962( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS962( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV9FuncaoAPF_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9FuncaoAPF_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV9FuncaoAPF_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV9FuncaoAPF_Codigo_CTRL", StringUtil.RTrim( sCtrlAV9FuncaoAPF_Codigo));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"A127Sistema_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA127Sistema_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A127Sistema_Codigo_CTRL", StringUtil.RTrim( sCtrlA127Sistema_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE962( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202051812485029");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("funcaoapfatributoswc.js", "?202051812485029");
            context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
            context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_432( )
      {
         edtAtributos_Codigo_Internalname = sPrefix+"ATRIBUTOS_CODIGO_"+sGXsfl_43_idx;
         edtAtributos_TabelaCod_Internalname = sPrefix+"ATRIBUTOS_TABELACOD_"+sGXsfl_43_idx;
         chkAtributos_Ativo_Internalname = sPrefix+"ATRIBUTOS_ATIVO_"+sGXsfl_43_idx;
         chkavFlag_Internalname = sPrefix+"vFLAG_"+sGXsfl_43_idx;
         edtAtributos_Nome_Internalname = sPrefix+"ATRIBUTOS_NOME_"+sGXsfl_43_idx;
         cmbAtributos_TipoDados_Internalname = sPrefix+"ATRIBUTOS_TIPODADOS_"+sGXsfl_43_idx;
         chkavFuncoesapfatributos_regra_Internalname = sPrefix+"vFUNCOESAPFATRIBUTOS_REGRA_"+sGXsfl_43_idx;
         edtavFuncoesapfatributos_code_Internalname = sPrefix+"vFUNCOESAPFATRIBUTOS_CODE_"+sGXsfl_43_idx;
         edtavFuncoesapfatributos_nome_Internalname = sPrefix+"vFUNCOESAPFATRIBUTOS_NOME_"+sGXsfl_43_idx;
      }

      protected void SubsflControlProps_fel_432( )
      {
         edtAtributos_Codigo_Internalname = sPrefix+"ATRIBUTOS_CODIGO_"+sGXsfl_43_fel_idx;
         edtAtributos_TabelaCod_Internalname = sPrefix+"ATRIBUTOS_TABELACOD_"+sGXsfl_43_fel_idx;
         chkAtributos_Ativo_Internalname = sPrefix+"ATRIBUTOS_ATIVO_"+sGXsfl_43_fel_idx;
         chkavFlag_Internalname = sPrefix+"vFLAG_"+sGXsfl_43_fel_idx;
         edtAtributos_Nome_Internalname = sPrefix+"ATRIBUTOS_NOME_"+sGXsfl_43_fel_idx;
         cmbAtributos_TipoDados_Internalname = sPrefix+"ATRIBUTOS_TIPODADOS_"+sGXsfl_43_fel_idx;
         chkavFuncoesapfatributos_regra_Internalname = sPrefix+"vFUNCOESAPFATRIBUTOS_REGRA_"+sGXsfl_43_fel_idx;
         edtavFuncoesapfatributos_code_Internalname = sPrefix+"vFUNCOESAPFATRIBUTOS_CODE_"+sGXsfl_43_fel_idx;
         edtavFuncoesapfatributos_nome_Internalname = sPrefix+"vFUNCOESAPFATRIBUTOS_NOME_"+sGXsfl_43_fel_idx;
      }

      protected void sendrow_432( )
      {
         SubsflControlProps_432( ) ;
         WB960( ) ;
         if ( ( subGridatributos_Rows * 1 == 0 ) || ( nGXsfl_43_idx <= subGridatributos_Recordsperpage( ) * 1 ) )
         {
            GridatributosRow = GXWebRow.GetNew(context,GridatributosContainer);
            if ( subGridatributos_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGridatributos_Backstyle = 0;
               if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
               {
                  subGridatributos_Linesclass = subGridatributos_Class+"Odd";
               }
            }
            else if ( subGridatributos_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGridatributos_Backstyle = 0;
               subGridatributos_Backcolor = subGridatributos_Allbackcolor;
               if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
               {
                  subGridatributos_Linesclass = subGridatributos_Class+"Uniform";
               }
            }
            else if ( subGridatributos_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGridatributos_Backstyle = 1;
               if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
               {
                  subGridatributos_Linesclass = subGridatributos_Class+"Odd";
               }
               subGridatributos_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGridatributos_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGridatributos_Backstyle = 1;
               if ( ((int)((nGXsfl_43_idx) % (2))) == 0 )
               {
                  subGridatributos_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
                  {
                     subGridatributos_Linesclass = subGridatributos_Class+"Even";
                  }
               }
               else
               {
                  subGridatributos_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGridatributos_Class, "") != 0 )
                  {
                     subGridatributos_Linesclass = subGridatributos_Class+"Odd";
                  }
               }
            }
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGridatributos_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_43_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridatributosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAtributos_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A176Atributos_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A176Atributos_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAtributos_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)43,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridatributosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAtributos_TabelaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A356Atributos_TabelaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A356Atributos_TabelaCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAtributos_TabelaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)43,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridatributosRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkAtributos_Ativo_Internalname,StringUtil.BoolToStr( A180Atributos_Ativo),(String)"",(String)"",(short)0,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            TempTags = " " + ((chkavFlag.Enabled!=0)&&(chkavFlag.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 47,'"+sPrefix+"',false,'"+sGXsfl_43_idx+"',43)\"" : " ");
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridatributosRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkavFlag_Internalname,StringUtil.BoolToStr( AV8Flag),(String)"",(String)"",(short)-1,(short)1,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",TempTags+((chkavFlag.Enabled!=0)&&(chkavFlag.Visible!=0) ? " onclick=\"gx.fn.checkboxClick(47, this, 'true', 'false');gx.ajax.executeCliEvent('e15962_client',this, event);gx.evt.onchange(this);\" " : " ")+((chkavFlag.Enabled!=0)&&(chkavFlag.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,47);\"" : " ")});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridatributosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAtributos_Nome_Internalname,StringUtil.RTrim( A177Atributos_Nome),StringUtil.RTrim( context.localUtil.Format( A177Atributos_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAtributos_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)43,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_43_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "ATRIBUTOS_TIPODADOS_" + sGXsfl_43_idx;
               cmbAtributos_TipoDados.Name = GXCCtl;
               cmbAtributos_TipoDados.WebTags = "";
               cmbAtributos_TipoDados.addItem("", "Desconhecido", 0);
               cmbAtributos_TipoDados.addItem("N", "Numeric", 0);
               cmbAtributos_TipoDados.addItem("C", "Character", 0);
               cmbAtributos_TipoDados.addItem("VC", "Varchar", 0);
               cmbAtributos_TipoDados.addItem("D", "Date", 0);
               cmbAtributos_TipoDados.addItem("DT", "Date Time", 0);
               cmbAtributos_TipoDados.addItem("Bool", "Boolean", 0);
               cmbAtributos_TipoDados.addItem("Blob", "Blob", 0);
               cmbAtributos_TipoDados.addItem("Outr", "Outros", 0);
               if ( cmbAtributos_TipoDados.ItemCount > 0 )
               {
                  A178Atributos_TipoDados = cmbAtributos_TipoDados.getValidValue(A178Atributos_TipoDados);
                  n178Atributos_TipoDados = false;
               }
            }
            /* ComboBox */
            GridatributosRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbAtributos_TipoDados,(String)cmbAtributos_TipoDados_Internalname,StringUtil.RTrim( A178Atributos_TipoDados),(short)1,(String)cmbAtributos_TipoDados_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbAtributos_TipoDados.CurrentValue = StringUtil.RTrim( A178Atributos_TipoDados);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbAtributos_TipoDados_Internalname, "Values", (String)(cmbAtributos_TipoDados.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            TempTags = " " + ((chkavFuncoesapfatributos_regra.Enabled!=0)&&(chkavFuncoesapfatributos_regra.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 50,'"+sPrefix+"',false,'"+sGXsfl_43_idx+"',43)\"" : " ");
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridatributosRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkavFuncoesapfatributos_regra_Internalname,StringUtil.BoolToStr( AV15FuncoesAPFAtributos_Regra),(String)"",(String)"",(short)-1,(short)1,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",TempTags+((chkavFuncoesapfatributos_regra.Enabled!=0)&&(chkavFuncoesapfatributos_regra.Visible!=0) ? " onclick=\"gx.fn.checkboxClick(50, this, 'true', 'false');gx.ajax.executeCliEvent('e16962_client',this, event);gx.evt.onchange(this);\" " : " ")+((chkavFuncoesapfatributos_regra.Enabled!=0)&&(chkavFuncoesapfatributos_regra.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,50);\"" : " ")});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavFuncoesapfatributos_code_Enabled!=0)&&(edtavFuncoesapfatributos_code_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 51,'"+sPrefix+"',false,'"+sGXsfl_43_idx+"',43)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridatributosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavFuncoesapfatributos_code_Internalname,(String)AV16FuncoesAPFAtributos_Code,(String)"",TempTags+((edtavFuncoesapfatributos_code_Enabled!=0)&&(edtavFuncoesapfatributos_code_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavFuncoesapfatributos_code_Enabled!=0)&&(edtavFuncoesapfatributos_code_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,51);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavFuncoesapfatributos_code_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavFuncoesapfatributos_code_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)43,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridatributosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavFuncoesapfatributos_nome_Enabled!=0)&&(edtavFuncoesapfatributos_nome_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 52,'"+sPrefix+"',false,'"+sGXsfl_43_idx+"',43)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridatributosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavFuncoesapfatributos_nome_Internalname,StringUtil.RTrim( AV17FuncoesAPFAtributos_Nome),StringUtil.RTrim( context.localUtil.Format( AV17FuncoesAPFAtributos_Nome, "@!")),TempTags+((edtavFuncoesapfatributos_nome_Enabled!=0)&&(edtavFuncoesapfatributos_nome_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavFuncoesapfatributos_nome_Enabled!=0)&&(edtavFuncoesapfatributos_nome_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,52);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavFuncoesapfatributos_nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavFuncoesapfatributos_nome_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)43,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_CODIGO"+"_"+sGXsfl_43_idx, GetSecureSignedToken( sPrefix+sGXsfl_43_idx, context.localUtil.Format( (decimal)(A176Atributos_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_TABELACOD"+"_"+sGXsfl_43_idx, GetSecureSignedToken( sPrefix+sGXsfl_43_idx, context.localUtil.Format( (decimal)(A356Atributos_TabelaCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_ATIVO"+"_"+sGXsfl_43_idx, GetSecureSignedToken( sPrefix+sGXsfl_43_idx, A180Atributos_Ativo));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_NOME"+"_"+sGXsfl_43_idx, GetSecureSignedToken( sPrefix+sGXsfl_43_idx, StringUtil.RTrim( context.localUtil.Format( A177Atributos_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_TIPODADOS"+"_"+sGXsfl_43_idx, GetSecureSignedToken( sPrefix+sGXsfl_43_idx, StringUtil.RTrim( context.localUtil.Format( A178Atributos_TipoDados, ""))));
            GridatributosContainer.AddRow(GridatributosRow);
            nGXsfl_43_idx = (short)(((subGridatributos_Islastpage==1)&&(nGXsfl_43_idx+1>subGridatributos_Recordsperpage( )) ? 1 : nGXsfl_43_idx+1));
            sGXsfl_43_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_43_idx), 4, 0)), 4, "0");
            SubsflControlProps_432( ) ;
         }
         /* End function sendrow_432 */
      }

      protected void init_default_properties( )
      {
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTableheader_Internalname = sPrefix+"TABLEHEADER";
         lblTextblockfuncaodados_codigo_Internalname = sPrefix+"TEXTBLOCKFUNCAODADOS_CODIGO";
         dynavFuncaodados_codigo_Internalname = sPrefix+"vFUNCAODADOS_CODIGO";
         lblTextblocktabela_codigo_Internalname = sPrefix+"TEXTBLOCKTABELA_CODIGO";
         dynavTabela_codigo_Internalname = sPrefix+"vTABELA_CODIGO";
         tblUnnamedtable3_Internalname = sPrefix+"UNNAMEDTABLE3";
         edtAtributos_Codigo_Internalname = sPrefix+"ATRIBUTOS_CODIGO";
         edtAtributos_TabelaCod_Internalname = sPrefix+"ATRIBUTOS_TABELACOD";
         chkAtributos_Ativo_Internalname = sPrefix+"ATRIBUTOS_ATIVO";
         chkavFlag_Internalname = sPrefix+"vFLAG";
         edtAtributos_Nome_Internalname = sPrefix+"ATRIBUTOS_NOME";
         cmbAtributos_TipoDados_Internalname = sPrefix+"ATRIBUTOS_TIPODADOS";
         chkavFuncoesapfatributos_regra_Internalname = sPrefix+"vFUNCOESAPFATRIBUTOS_REGRA";
         edtavFuncoesapfatributos_code_Internalname = sPrefix+"vFUNCOESAPFATRIBUTOS_CODE";
         edtavFuncoesapfatributos_nome_Internalname = sPrefix+"vFUNCOESAPFATRIBUTOS_NOME";
         imgConsultaratributos_Internalname = sPrefix+"CONSULTARATRIBUTOS";
         tblUnnamedtable4_Internalname = sPrefix+"UNNAMEDTABLE4";
         tblUnnamedtable2_Internalname = sPrefix+"UNNAMEDTABLE2";
         Dvpanel_unnamedtable2_Internalname = sPrefix+"DVPANEL_UNNAMEDTABLE2";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         Form.Internalname = sPrefix+"FORM";
         subGridatributos_Internalname = sPrefix+"GRIDATRIBUTOS";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtavFuncoesapfatributos_nome_Jsonclick = "";
         edtavFuncoesapfatributos_nome_Visible = -1;
         edtavFuncoesapfatributos_code_Jsonclick = "";
         edtavFuncoesapfatributos_code_Visible = -1;
         chkavFuncoesapfatributos_regra.Visible = -1;
         chkavFuncoesapfatributos_regra.Enabled = 1;
         cmbAtributos_TipoDados_Jsonclick = "";
         edtAtributos_Nome_Jsonclick = "";
         chkavFlag.Visible = -1;
         chkavFlag.Enabled = 1;
         edtAtributos_TabelaCod_Jsonclick = "";
         edtAtributos_Codigo_Jsonclick = "";
         dynavTabela_codigo_Jsonclick = "";
         dynavFuncaodados_codigo_Jsonclick = "";
         subGridatributos_Allowcollapsing = 0;
         subGridatributos_Allowselection = 0;
         edtavFuncoesapfatributos_nome_Enabled = 1;
         edtavFuncoesapfatributos_code_Enabled = 1;
         subGridatributos_Class = "WorkWithBorder WorkWith";
         dynavFuncaodados_codigo.Width = 50;
         subGridatributos_Backcolorstyle = 3;
         chkavFuncoesapfatributos_regra.Caption = "";
         chkavFlag.Caption = "";
         chkAtributos_Ativo.Caption = "";
         Dvpanel_unnamedtable2_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable2_Iconposition = "left";
         Dvpanel_unnamedtable2_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable2_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_unnamedtable2_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable2_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable2_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_unnamedtable2_Title = "Atributos";
         Dvpanel_unnamedtable2_Cls = "GXUI-DVelop-Panel";
         Dvpanel_unnamedtable2_Width = "100%";
         subGridatributos_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public void Validv_Funcaodados_codigo( GXCombobox dynGX_Parm1 ,
                                             GXCombobox dynGX_Parm2 ,
                                             String GX_Parm3 )
      {
         dynavFuncaodados_codigo = dynGX_Parm1;
         AV10FuncaoDados_Codigo = (short)(NumberUtil.Val( dynavFuncaodados_codigo.CurrentValue, "."));
         dynavTabela_codigo = dynGX_Parm2;
         AV13Tabela_Codigo = (int)(NumberUtil.Val( dynavTabela_codigo.CurrentValue, "."));
         sPrefix = GX_Parm3;
         GXVvTABELA_CODIGO_html962( AV10FuncaoDados_Codigo) ;
         dynload_actions( ) ;
         if ( dynavTabela_codigo.ItemCount > 0 )
         {
            AV13Tabela_Codigo = (int)(NumberUtil.Val( dynavTabela_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13Tabela_Codigo), 6, 0))), "."));
         }
         dynavTabela_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13Tabela_Codigo), 6, 0));
         isValidOutput.Add(dynavTabela_codigo);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'AV13Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A364FuncaoAPFAtributos_AtributosCod',fld:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Atributos_Codigo',fld:'vATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A389FuncoesAPFAtributos_Regra',fld:'FUNCOESAPFATRIBUTOS_REGRA',pic:'',nv:false},{av:'A383FuncoesAPFAtributos_Code',fld:'FUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'A384FuncoesAPFAtributos_Nome',fld:'FUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'dynavFuncaodados_codigo'}]}");
         setEventMetadata("GRIDATRIBUTOS.LOAD","{handler:'E14962',iparms:[{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A364FuncaoAPFAtributos_AtributosCod',fld:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Atributos_Codigo',fld:'vATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A389FuncoesAPFAtributos_Regra',fld:'FUNCOESAPFATRIBUTOS_REGRA',pic:'',nv:false},{av:'A383FuncoesAPFAtributos_Code',fld:'FUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'A384FuncoesAPFAtributos_Nome',fld:'FUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''}],oparms:[{av:'AV5Atributos_Codigo',fld:'vATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8Flag',fld:'vFLAG',pic:'',nv:false},{av:'AV15FuncoesAPFAtributos_Regra',fld:'vFUNCOESAPFATRIBUTOS_REGRA',pic:'',nv:false},{av:'AV16FuncoesAPFAtributos_Code',fld:'vFUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'AV17FuncoesAPFAtributos_Nome',fld:'vFUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''}]}");
         setEventMetadata("'DOCONSULTARATRIBUTOS'","{handler:'E18961',iparms:[],oparms:[]}");
         setEventMetadata("VFLAG.CLICK","{handler:'E15962',iparms:[{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV8Flag',fld:'vFLAG',pic:'',nv:false},{av:'AV9FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV5Atributos_Codigo',fld:'vATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV10FuncaoDados_Codigo',fld:'vFUNCAODADOS_CODIGO',pic:'ZZZ9',nv:0},{av:'AV15FuncoesAPFAtributos_Regra',fld:'vFUNCOESAPFATRIBUTOS_REGRA',pic:'',nv:false}],oparms:[{av:'AV5Atributos_Codigo',fld:'vATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15FuncoesAPFAtributos_Regra',fld:'vFUNCOESAPFATRIBUTOS_REGRA',pic:'',nv:false},{av:'AV9FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8Flag',fld:'vFLAG',pic:'',nv:false}]}");
         setEventMetadata("VFUNCOESAPFATRIBUTOS_REGRA.CLICK","{handler:'E16962',iparms:[{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'AV13Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A364FuncaoAPFAtributos_AtributosCod',fld:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Atributos_Codigo',fld:'vATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A389FuncoesAPFAtributos_Regra',fld:'FUNCOESAPFATRIBUTOS_REGRA',pic:'',nv:false},{av:'A383FuncoesAPFAtributos_Code',fld:'FUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'A384FuncoesAPFAtributos_Nome',fld:'FUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'AV15FuncoesAPFAtributos_Regra',fld:'vFUNCOESAPFATRIBUTOS_REGRA',pic:'',nv:false},{av:'AV8Flag',fld:'vFLAG',pic:'',nv:false},{av:'AV10FuncaoDados_Codigo',fld:'vFUNCAODADOS_CODIGO',pic:'ZZZ9',nv:0}],oparms:[{av:'AV5Atributos_Codigo',fld:'vATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8Flag',fld:'vFLAG',pic:'',nv:false},{av:'AV9FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15FuncoesAPFAtributos_Regra',fld:'vFUNCOESAPFATRIBUTOS_REGRA',pic:'',nv:false}]}");
         setEventMetadata("VFUNCAODADOS_CODIGO.CLICK","{handler:'E11962',iparms:[{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'AV13Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A364FuncaoAPFAtributos_AtributosCod',fld:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Atributos_Codigo',fld:'vATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A389FuncoesAPFAtributos_Regra',fld:'FUNCOESAPFATRIBUTOS_REGRA',pic:'',nv:false},{av:'A383FuncoesAPFAtributos_Code',fld:'FUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'A384FuncoesAPFAtributos_Nome',fld:'FUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'sPrefix',nv:''},{av:'AV10FuncaoDados_Codigo',fld:'vFUNCAODADOS_CODIGO',pic:'ZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("VTABELA_CODIGO.CLICK","{handler:'E12962',iparms:[{av:'AV13Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("GRIDATRIBUTOS_FIRSTPAGE","{handler:'subgridatributos_firstpage',iparms:[{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'AV13Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A364FuncaoAPFAtributos_AtributosCod',fld:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Atributos_Codigo',fld:'vATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A389FuncoesAPFAtributos_Regra',fld:'FUNCOESAPFATRIBUTOS_REGRA',pic:'',nv:false},{av:'A383FuncoesAPFAtributos_Code',fld:'FUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'A384FuncoesAPFAtributos_Nome',fld:'FUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'dynavFuncaodados_codigo'}]}");
         setEventMetadata("GRIDATRIBUTOS_PREVPAGE","{handler:'subgridatributos_previouspage',iparms:[{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'AV13Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A364FuncaoAPFAtributos_AtributosCod',fld:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Atributos_Codigo',fld:'vATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A389FuncoesAPFAtributos_Regra',fld:'FUNCOESAPFATRIBUTOS_REGRA',pic:'',nv:false},{av:'A383FuncoesAPFAtributos_Code',fld:'FUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'A384FuncoesAPFAtributos_Nome',fld:'FUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'dynavFuncaodados_codigo'}]}");
         setEventMetadata("GRIDATRIBUTOS_NEXTPAGE","{handler:'subgridatributos_nextpage',iparms:[{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'AV13Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A364FuncaoAPFAtributos_AtributosCod',fld:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Atributos_Codigo',fld:'vATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A389FuncoesAPFAtributos_Regra',fld:'FUNCOESAPFATRIBUTOS_REGRA',pic:'',nv:false},{av:'A383FuncoesAPFAtributos_Code',fld:'FUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'A384FuncoesAPFAtributos_Nome',fld:'FUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'dynavFuncaodados_codigo'}]}");
         setEventMetadata("GRIDATRIBUTOS_LASTPAGE","{handler:'subgridatributos_lastpage',iparms:[{av:'GRIDATRIBUTOS_nFirstRecordOnPage',nv:0},{av:'GRIDATRIBUTOS_nEOF',nv:0},{av:'subGridatributos_Rows',nv:0},{av:'AV13Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A364FuncaoAPFAtributos_AtributosCod',fld:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Atributos_Codigo',fld:'vATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A389FuncoesAPFAtributos_Regra',fld:'FUNCOESAPFATRIBUTOS_REGRA',pic:'',nv:false},{av:'A383FuncoesAPFAtributos_Code',fld:'FUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'A384FuncoesAPFAtributos_Nome',fld:'FUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'dynavFuncaodados_codigo'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         A383FuncoesAPFAtributos_Code = "";
         A384FuncoesAPFAtributos_Nome = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A177Atributos_Nome = "";
         A178Atributos_TipoDados = "";
         AV16FuncoesAPFAtributos_Code = "";
         AV17FuncoesAPFAtributos_Nome = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00962_A368FuncaoDados_Codigo = new int[1] ;
         H00962_A370FuncaoDados_SistemaCod = new int[1] ;
         H00962_A419FuncaoDados_Nome50 = new String[] {""} ;
         H00963_A172Tabela_Codigo = new int[1] ;
         H00963_A173Tabela_Nome = new String[] {""} ;
         H00963_A174Tabela_Ativo = new bool[] {false} ;
         H00963_A368FuncaoDados_Codigo = new int[1] ;
         GridatributosContainer = new GXWebGrid( context);
         H00964_A178Atributos_TipoDados = new String[] {""} ;
         H00964_n178Atributos_TipoDados = new bool[] {false} ;
         H00964_A177Atributos_Nome = new String[] {""} ;
         H00964_A180Atributos_Ativo = new bool[] {false} ;
         H00964_A356Atributos_TabelaCod = new int[1] ;
         H00964_A176Atributos_Codigo = new int[1] ;
         H00965_AGRIDATRIBUTOS_nRecordCount = new long[1] ;
         GridatributosRow = new GXWebRow();
         AV14WebSession = context.GetSession();
         AV11FuncoesAPFAtributos = new SdtFuncoesAPFAtributos(context);
         H00966_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         H00966_A165FuncaoAPF_Codigo = new int[1] ;
         H00966_A389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         H00966_n389FuncoesAPFAtributos_Regra = new bool[] {false} ;
         H00966_A383FuncoesAPFAtributos_Code = new String[] {""} ;
         H00966_n383FuncoesAPFAtributos_Code = new bool[] {false} ;
         H00966_A384FuncoesAPFAtributos_Nome = new String[] {""} ;
         H00966_n384FuncoesAPFAtributos_Nome = new bool[] {false} ;
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         subGridatributos_Linesclass = "";
         GridatributosColumn = new GXWebColumn();
         TempTags = "";
         imgConsultaratributos_Jsonclick = "";
         lblTextblockfuncaodados_codigo_Jsonclick = "";
         lblTextblocktabela_codigo_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV9FuncaoAPF_Codigo = "";
         sCtrlA127Sistema_Codigo = "";
         ROClassString = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.funcaoapfatributoswc__default(),
            new Object[][] {
                new Object[] {
               H00962_A368FuncaoDados_Codigo, H00962_A370FuncaoDados_SistemaCod, H00962_A419FuncaoDados_Nome50
               }
               , new Object[] {
               H00963_A172Tabela_Codigo, H00963_A173Tabela_Nome, H00963_A174Tabela_Ativo, H00963_A368FuncaoDados_Codigo
               }
               , new Object[] {
               H00964_A178Atributos_TipoDados, H00964_n178Atributos_TipoDados, H00964_A177Atributos_Nome, H00964_A180Atributos_Ativo, H00964_A356Atributos_TabelaCod, H00964_A176Atributos_Codigo
               }
               , new Object[] {
               H00965_AGRIDATRIBUTOS_nRecordCount
               }
               , new Object[] {
               H00966_A364FuncaoAPFAtributos_AtributosCod, H00966_A165FuncaoAPF_Codigo, H00966_A389FuncoesAPFAtributos_Regra, H00966_n389FuncoesAPFAtributos_Regra, H00966_A383FuncoesAPFAtributos_Code, H00966_n383FuncoesAPFAtributos_Code, H00966_A384FuncoesAPFAtributos_Nome, H00966_n384FuncoesAPFAtributos_Nome
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavFuncoesapfatributos_code_Enabled = 0;
         edtavFuncoesapfatributos_nome_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short AV10FuncaoDados_Codigo ;
      private short nRC_GXsfl_43 ;
      private short nGXsfl_43_idx=1 ;
      private short initialized ;
      private short nGXWrapped ;
      private short GRIDATRIBUTOS_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_43_Refreshing=0 ;
      private short subGridatributos_Backcolorstyle ;
      private short AV27GXLvl126 ;
      private short subGridatributos_Titlebackstyle ;
      private short subGridatributos_Allowselection ;
      private short subGridatributos_Allowhovering ;
      private short subGridatributos_Allowcollapsing ;
      private short subGridatributos_Collapsed ;
      private short subGridatributos_Backstyle ;
      private short wbTemp ;
      private int AV9FuncaoAPF_Codigo ;
      private int A127Sistema_Codigo ;
      private int wcpOAV9FuncaoAPF_Codigo ;
      private int wcpOA127Sistema_Codigo ;
      private int subGridatributos_Rows ;
      private int AV13Tabela_Codigo ;
      private int A176Atributos_Codigo ;
      private int A165FuncaoAPF_Codigo ;
      private int A364FuncaoAPFAtributos_AtributosCod ;
      private int AV5Atributos_Codigo ;
      private int edtavFuncoesapfatributos_code_Enabled ;
      private int edtavFuncoesapfatributos_nome_Enabled ;
      private int A356Atributos_TabelaCod ;
      private int gxdynajaxindex ;
      private int subGridatributos_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int subGridatributos_Titlebackcolor ;
      private int subGridatributos_Allbackcolor ;
      private int subGridatributos_Selectioncolor ;
      private int subGridatributos_Hoveringcolor ;
      private int idxLst ;
      private int subGridatributos_Backcolor ;
      private int edtavFuncoesapfatributos_code_Visible ;
      private int edtavFuncoesapfatributos_nome_Visible ;
      private long GRIDATRIBUTOS_nFirstRecordOnPage ;
      private long GRIDATRIBUTOS_nCurrentRecord ;
      private long GRIDATRIBUTOS_nRecordCount ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_43_idx="0001" ;
      private String A384FuncoesAPFAtributos_Nome ;
      private String GXKey ;
      private String edtavFuncoesapfatributos_code_Internalname ;
      private String edtavFuncoesapfatributos_nome_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_unnamedtable2_Width ;
      private String Dvpanel_unnamedtable2_Cls ;
      private String Dvpanel_unnamedtable2_Title ;
      private String Dvpanel_unnamedtable2_Iconposition ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkavFlag_Internalname ;
      private String edtAtributos_Codigo_Internalname ;
      private String edtAtributos_TabelaCod_Internalname ;
      private String chkAtributos_Ativo_Internalname ;
      private String A177Atributos_Nome ;
      private String edtAtributos_Nome_Internalname ;
      private String cmbAtributos_TipoDados_Internalname ;
      private String A178Atributos_TipoDados ;
      private String chkavFuncoesapfatributos_regra_Internalname ;
      private String AV17FuncoesAPFAtributos_Nome ;
      private String GXCCtl ;
      private String dynavFuncaodados_codigo_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String dynavTabela_codigo_Internalname ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblUnnamedtable2_Internalname ;
      private String tblUnnamedtable4_Internalname ;
      private String subGridatributos_Internalname ;
      private String subGridatributos_Class ;
      private String subGridatributos_Linesclass ;
      private String TempTags ;
      private String imgConsultaratributos_Internalname ;
      private String imgConsultaratributos_Jsonclick ;
      private String tblUnnamedtable3_Internalname ;
      private String lblTextblockfuncaodados_codigo_Internalname ;
      private String lblTextblockfuncaodados_codigo_Jsonclick ;
      private String dynavFuncaodados_codigo_Jsonclick ;
      private String lblTextblocktabela_codigo_Internalname ;
      private String lblTextblocktabela_codigo_Jsonclick ;
      private String dynavTabela_codigo_Jsonclick ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters_Internalname ;
      private String tblTableactions_Internalname ;
      private String sCtrlAV9FuncaoAPF_Codigo ;
      private String sCtrlA127Sistema_Codigo ;
      private String sGXsfl_43_fel_idx="0001" ;
      private String ROClassString ;
      private String edtAtributos_Codigo_Jsonclick ;
      private String edtAtributos_TabelaCod_Jsonclick ;
      private String edtAtributos_Nome_Jsonclick ;
      private String cmbAtributos_TipoDados_Jsonclick ;
      private String edtavFuncoesapfatributos_code_Jsonclick ;
      private String edtavFuncoesapfatributos_nome_Jsonclick ;
      private String Dvpanel_unnamedtable2_Internalname ;
      private bool entryPointCalled ;
      private bool A389FuncoesAPFAtributos_Regra ;
      private bool n389FuncoesAPFAtributos_Regra ;
      private bool n383FuncoesAPFAtributos_Code ;
      private bool n384FuncoesAPFAtributos_Nome ;
      private bool toggleJsOutput ;
      private bool Dvpanel_unnamedtable2_Collapsible ;
      private bool Dvpanel_unnamedtable2_Collapsed ;
      private bool Dvpanel_unnamedtable2_Autowidth ;
      private bool Dvpanel_unnamedtable2_Autoheight ;
      private bool Dvpanel_unnamedtable2_Showcollapseicon ;
      private bool Dvpanel_unnamedtable2_Autoscroll ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool A180Atributos_Ativo ;
      private bool AV8Flag ;
      private bool n178Atributos_TipoDados ;
      private bool AV15FuncoesAPFAtributos_Regra ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String A383FuncoesAPFAtributos_Code ;
      private String AV16FuncoesAPFAtributos_Code ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private GXWebGrid GridatributosContainer ;
      private GXWebRow GridatributosRow ;
      private GXWebColumn GridatributosColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavFuncaodados_codigo ;
      private GXCombobox dynavTabela_codigo ;
      private GXCheckbox chkAtributos_Ativo ;
      private GXCheckbox chkavFlag ;
      private GXCombobox cmbAtributos_TipoDados ;
      private GXCheckbox chkavFuncoesapfatributos_regra ;
      private IDataStoreProvider pr_default ;
      private int[] H00962_A368FuncaoDados_Codigo ;
      private int[] H00962_A370FuncaoDados_SistemaCod ;
      private String[] H00962_A419FuncaoDados_Nome50 ;
      private int[] H00963_A172Tabela_Codigo ;
      private String[] H00963_A173Tabela_Nome ;
      private bool[] H00963_A174Tabela_Ativo ;
      private int[] H00963_A368FuncaoDados_Codigo ;
      private String[] H00964_A178Atributos_TipoDados ;
      private bool[] H00964_n178Atributos_TipoDados ;
      private String[] H00964_A177Atributos_Nome ;
      private bool[] H00964_A180Atributos_Ativo ;
      private int[] H00964_A356Atributos_TabelaCod ;
      private int[] H00964_A176Atributos_Codigo ;
      private long[] H00965_AGRIDATRIBUTOS_nRecordCount ;
      private int[] H00966_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] H00966_A165FuncaoAPF_Codigo ;
      private bool[] H00966_A389FuncoesAPFAtributos_Regra ;
      private bool[] H00966_n389FuncoesAPFAtributos_Regra ;
      private String[] H00966_A383FuncoesAPFAtributos_Code ;
      private bool[] H00966_n383FuncoesAPFAtributos_Code ;
      private String[] H00966_A384FuncoesAPFAtributos_Nome ;
      private bool[] H00966_n384FuncoesAPFAtributos_Nome ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IGxSession AV14WebSession ;
      private SdtFuncoesAPFAtributos AV11FuncoesAPFAtributos ;
   }

   public class funcaoapfatributoswc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00962 ;
          prmH00962 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00963 ;
          prmH00963 = new Object[] {
          new Object[] {"@AV10FuncaoDados_Codigo",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00964 ;
          prmH00964 = new Object[] {
          new Object[] {"@AV13Tabela_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00965 ;
          prmH00965 = new Object[] {
          new Object[] {"@AV13Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00966 ;
          prmH00966 = new Object[] {
          new Object[] {"@AV9FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV5Atributos_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00962", "SELECT [FuncaoDados_Codigo], [FuncaoDados_SistemaCod], SUBSTRING([FuncaoDados_Nome], 1, 50) AS FuncaoDados_Nome50 FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_SistemaCod] = @Sistema_Codigo ORDER BY [FuncaoDados_Nome50] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00962,0,0,true,false )
             ,new CursorDef("H00963", "SELECT T1.[Tabela_Codigo], T2.[Tabela_Nome], T2.[Tabela_Ativo], T1.[FuncaoDados_Codigo] FROM ([FuncaoDadosTabela] T1 WITH (NOLOCK) INNER JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = T1.[Tabela_Codigo]) WHERE (T2.[Tabela_Ativo] = 1) AND (T1.[FuncaoDados_Codigo] = @AV10FuncaoDados_Codigo) ORDER BY T2.[Tabela_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00963,0,0,true,false )
             ,new CursorDef("H00964", "SELECT * FROM (SELECT  [Atributos_TipoDados], [Atributos_Nome], [Atributos_Ativo], [Atributos_TabelaCod], [Atributos_Codigo], ROW_NUMBER() OVER ( ORDER BY [Atributos_TabelaCod] ) AS GX_ROW_NUMBER FROM [Atributos] WITH (NOLOCK) WHERE ([Atributos_TabelaCod] = @AV13Tabela_Codigo) AND ([Atributos_Ativo] = 1)) AS GX_CTE WHERE GX_ROW_NUMBER BETWEEN @GXPagingFrom2 AND @GXPagingTo2 OR @GXPagingTo2 < @GXPagingFrom2 AND GX_ROW_NUMBER >= @GXPagingFrom2",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00964,11,0,true,false )
             ,new CursorDef("H00965", "SELECT COUNT(*) FROM [Atributos] WITH (NOLOCK) WHERE ([Atributos_TabelaCod] = @AV13Tabela_Codigo) AND ([Atributos_Ativo] = 1) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00965,1,0,true,false )
             ,new CursorDef("H00966", "SELECT [FuncaoAPFAtributos_AtributosCod], [FuncaoAPF_Codigo], [FuncoesAPFAtributos_Regra], [FuncoesAPFAtributos_Code], [FuncoesAPFAtributos_Nome] FROM [FuncoesAPFAtributos] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @AV9FuncaoAPF_Codigo and [FuncaoAPFAtributos_AtributosCod] = @AV5Atributos_Codigo ORDER BY [FuncaoAPF_Codigo], [FuncaoAPFAtributos_AtributosCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00966,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 4) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 3 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
