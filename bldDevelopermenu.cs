using System;
using GeneXus.Builder;
using System.IO;
public class bldDevelopermenu : GxBaseBuilder
{
   string cs_path = "." ;
   public bldDevelopermenu( ) : base()
   {
   }

   public override int BeforeCompile( )
   {
      return 0 ;
   }

   public override int AfterCompile( )
   {
      int ErrCode ;
      ErrCode = 0;
      if ( ! File.Exists(@"bin\client.exe.config") || checkTime(@"bin\client.exe.config",cs_path + @"\client.exe.config") )
      {
         File.Copy( cs_path + @"\client.exe.config", @"bin\client.exe.config", true);
      }
      return ErrCode ;
   }

   static public int Main( string[] args )
   {
      bldDevelopermenu x = new bldDevelopermenu() ;
      x.SetMainSourceFile( "bldDevelopermenu.cs");
      x.LoadVariables( args);
      return x.CompileAll( );
   }

   public override ItemCollection GetSortedBuildList( )
   {
      ItemCollection sc = new ItemCollection() ;
      sc.Add( @"bin\GeneXus.Programs.Common.dll", cs_path + @"\genexus.programs.common.rsp");
      return sc ;
   }

   public override TargetCollection GetRuntimeBuildList( )
   {
      TargetCollection sc = new TargetCollection() ;
      sc.Add( @"tilesdata", "dll");
      sc.Add( @"areatrabalho_sistema", "dll");
      sc.Add( @"usuariosistema", "dll");
      sc.Add( @"alistwwpprograms", "dll");
      sc.Add( @"aprc_sistemasfromexcel", "dll");
      sc.Add( @"aexportreportextrawwcontagemresultadoextraselection", "dll");
      sc.Add( @"aexportreportextrawwcontagemresultadowwaceitefaturamento", "dll");
      sc.Add( @"aexportreportwwcontagemresultadocontagens", "dll");
      sc.Add( @"arel_conferenciams", "dll");
      sc.Add( @"arel_contagemos", "dll");
      sc.Add( @"aprc_defineultimacontagem", "dll");
      sc.Add( @"anotificationsregistrationhandler", "dll");
      sc.Add( @"gxofflineeventreplicator", "dll");
      sc.Add( @"arel_contagemta", "dll");
      sc.Add( @"arel_contagem", "dll");
      sc.Add( @"aexportextrawwcontagemresultadowwaceitefaturamento", "dll");
      sc.Add( @"arel_contagemgerencial2", "dll");
      sc.Add( @"arel_contagemcapa", "dll");
      sc.Add( @"aprc_carregasdtdemandas", "dll");
      sc.Add( @"aprc_inserirsdtdemandas", "dll");
      sc.Add( @"arel_conferenciacast", "dll");
      sc.Add( @"aprc_importarcontagens", "dll");
      sc.Add( @"aprc_vinculademandaitens", "dll");
      sc.Add( @"arel_planodecontagem", "dll");
      sc.Add( @"aprc_importarpffs", "dll");
      sc.Add( @"aws_enviaemail", "dll");
      sc.Add( @"ateste", "dll");
      sc.Add( @"aprc_carregasdtbaseline", "dll");
      sc.Add( @"aprc_inserirsdtbaseline", "dll");
      sc.Add( @"arel_contagemgerencial", "dll");
      sc.Add( @"aprc_downloadfile", "dll");
      sc.Add( @"aprc_processarissuesvs223", "dll");
      sc.Add( @"asdts", "dll");
      sc.Add( @"aws_meetrika", "dll");
      sc.Add( @"aloadeventssampleproc", "dll");
      sc.Add( @"aschedulerrequesthandler", "dll");
      sc.Add( @"aprc_executarpropostadaos", "dll");
      sc.Add( @"aprc_emailreplace_nova_os", "dll");
      sc.Add( @"arel_ordemservico", "dll");
      sc.Add( @"arel_termoderecebimentoprovisorio", "dll");
      sc.Add( @"arel_modelotermodehomologacao", "dll");
      sc.Add( @"arel_termoderecebimentodefinitivo", "dll");
      sc.Add( @"arel_termo_de_recusa", "dll");
      sc.Add( @"aprc_emailreplace_novo_usuario", "dll");
      sc.Add( @"prc_areatrabalho_sistema", "dll");
      sc.Add( @"arel_execucaocontratual", "dll");
      sc.Add( @"arel_compromissosassumidos", "dll");
      sc.Add( @"getwwperfilfilterdata", "dll");
      sc.Add( @"getwwareatrabalhofilterdata", "dll");
      sc.Add( @"getwwcontratantefilterdata", "dll");
      sc.Add( @"getwwpaisfilterdata", "dll");
      sc.Add( @"getwwestadofilterdata", "dll");
      sc.Add( @"getwwmunicipiofilterdata", "dll");
      sc.Add( @"getwwbancofilterdata", "dll");
      sc.Add( @"getwwbancoagenciafilterdata", "dll");
      sc.Add( @"getwwpessoafilterdata", "dll");
      sc.Add( @"getwwcontratadafilterdata", "dll");
      sc.Add( @"getpromptcontratadafilterdata", "dll");
      sc.Add( @"getcontratadacontratadausuariowcfilterdata", "dll");
      sc.Add( @"getwwsistemafilterdata", "dll");
      sc.Add( @"getpromptsistemafilterdata", "dll");
      sc.Add( @"getsistemamodulowcfilterdata", "dll");
      sc.Add( @"getsistemafuncaousuariowcfilterdata", "dll");
      sc.Add( @"getsistemafuncaodadoswcfilterdata", "dll");
      sc.Add( @"getsistemafronteirafilterdata", "dll");
      sc.Add( @"getsistemaevidenciaswcfilterdata", "dll");
      sc.Add( @"getwwmodulofilterdata", "dll");
      sc.Add( @"getwwfuncaousuariofilterdata", "dll");
      sc.Add( @"getfuncaousuariomodulofuncoeswcfilterdata", "dll");
      sc.Add( @"getwwfuncaoapffilterdata", "dll");
      sc.Add( @"getwwtabelafilterdata", "dll");
      sc.Add( @"getwwatributosfilterdata", "dll");
      sc.Add( @"getwwtecnologiafilterdata", "dll");
      sc.Add( @"gettecnologiaambientetecnologicowcfilterdata", "dll");
      sc.Add( @"getwwmetodologiafilterdata", "dll");
      sc.Add( @"getwwcontagemfilterdata", "dll");
      sc.Add( @"getwwguiafilterdata", "dll");
      sc.Add( @"getwwcontagemitemparecerfilterdata", "dll");
      sc.Add( @"getcontrato_unidadesmedicaowcfilterdata", "dll");
      sc.Add( @"getcontratocontratonotaempenhowcfilterdata", "dll");
      sc.Add( @"getcontratocontratosaldocontratowcfilterdata", "dll");
      sc.Add( @"getcontratoautorizacaoconsumowcfilterdata", "dll");
      sc.Add( @"getwwcontratogarantiafilterdata", "dll");
      sc.Add( @"getwwcontratoarquivosanexosfilterdata", "dll");
      sc.Add( @"getwwcontratoclausulasfilterdata", "dll");
      sc.Add( @"getwwservicogrupofilterdata", "dll");
      sc.Add( @"getwwservicofilterdata", "dll");
      sc.Add( @"getpromptservicofilterdata", "dll");
      sc.Add( @"getservicocontratoservicowcfilterdata", "dll");
      sc.Add( @"getservicoservicoprioridadewcfilterdata", "dll");
      sc.Add( @"getservicoservicofluxofilterdata", "dll");
      sc.Add( @"getservicoartefatoswcfilterdata", "dll");
      sc.Add( @"getwwcontratoservicosfilterdata", "dll");
      sc.Add( @"getpromptcontratoservicosfilterdata", "dll");
      sc.Add( @"getcontratoservicos_vncwcfilterdata", "dll");
      sc.Add( @"getcontratoservicos_telaswcfilterdata", "dll");
      sc.Add( @"getcontroservicoindicadoreswcfilterdata", "dll");
      sc.Add( @"getcontratoservicoscustowcfilterdata", "dll");
      sc.Add( @"getcontratoservicosartefatoswcfilterdata", "dll");
      sc.Add( @"getwwcontratoocorrenciafilterdata", "dll");
      sc.Add( @"getpromptcontratoocorrenciafilterdata", "dll");
      sc.Add( @"getcontratoocorrenciacontratoocorrencianotificacaowcfilterdata", "dll");
      sc.Add( @"getwwcontratoocorrencianotificacaofilterdata", "dll");
      sc.Add( @"getpromptcontratoocorrencianotificacaofilterdata", "dll");
      sc.Add( @"getwwcontratodadoscertamefilterdata", "dll");
      sc.Add( @"getpromptcontratodadoscertamefilterdata", "dll");
      sc.Add( @"getwwcontratotermoaditivofilterdata", "dll");
      sc.Add( @"getpromptcontratotermoaditivofilterdata", "dll");
      sc.Add( @"getwwservicoansfilterdata", "dll");
      sc.Add( @"getpromptservicoansfilterdata", "dll");
      sc.Add( @"getwwcontratoobrigacaofilterdata", "dll");
      sc.Add( @"getpromptcontratoobrigacaofilterdata", "dll");
      sc.Add( @"getwwmetodologiafasesfilterdata", "dll");
      sc.Add( @"getpromptmetodologiafasesfilterdata", "dll");
      sc.Add( @"getmetodologiafasespadroesartefatoswcfilterdata", "dll");
      sc.Add( @"getwwpadroesartefatosfilterdata", "dll");
      sc.Add( @"getpromptpadroesartefatosfilterdata", "dll");
      sc.Add( @"getwwcontratanteusuariofilterdata", "dll");
      sc.Add( @"getpromptcontratanteusuariofilterdata", "dll");
      sc.Add( @"getwwcontratadausuariofilterdata", "dll");
      sc.Add( @"getpromptcontratadausuariofilterdata", "dll");
      sc.Add( @"getwwusuarioperfilfilterdata", "dll");
      sc.Add( @"getpromptusuarioperfilfilterdata", "dll");
      sc.Add( @"getwwfuncaodadosfilterdata", "dll");
      sc.Add( @"getpromptfuncaodadosfilterdata", "dll");
      sc.Add( @"getwwfuncoesapfatributosfilterdata", "dll");
      sc.Add( @"getpromptfuncoesapfatributosfilterdata", "dll");
      sc.Add( @"getwwambientetecnologicofilterdata", "dll");
      sc.Add( @"getpromptambientetecnologicofilterdata", "dll");
      sc.Add( @"getambientetecnologicoambientetecnologicotecnologiaswcfilterdata", "dll");
      sc.Add( @"getambientetecnologicosistemasfilterdata", "dll");
      sc.Add( @"getwwfuncaoapfevidenciafilterdata", "dll");
      sc.Add( @"getpromptfuncaoapfevidenciafilterdata", "dll");
      sc.Add( @"getwwnaoconformidadefilterdata", "dll");
      sc.Add( @"getpromptnaoconformidadefilterdata", "dll");
      sc.Add( @"getwwstatusfilterdata", "dll");
      sc.Add( @"getpromptstatusfilterdata", "dll");
      sc.Add( @"getwwsolicitacoesfilterdata", "dll");
      sc.Add( @"getpromptsolicitacoesfilterdata", "dll");
      sc.Add( @"getsolicitacoessolicitacoesitenswcfilterdata", "dll");
      sc.Add( @"getpromptlotefilterdata", "dll");
      sc.Add( @"getwwcontagemresultadoevidenciafilterdata", "dll");
      sc.Add( @"getpromptcontagemresultadoevidenciafilterdata", "dll");
      sc.Add( @"getwwgeral_funcaofilterdata", "dll");
      sc.Add( @"getpromptgeral_funcaofilterdata", "dll");
      sc.Add( @"getwwgeral_grupo_funcaofilterdata", "dll");
      sc.Add( @"getpromptgeral_grupo_funcaofilterdata", "dll");
      sc.Add( @"getgeral_grupo_funcaogeral_funcaowcfilterdata", "dll");
      sc.Add( @"getwwgeral_grupo_cargofilterdata", "dll");
      sc.Add( @"getpromptgeral_grupo_cargofilterdata", "dll");
      sc.Add( @"getgeral_grupo_cargogeral_cargowcfilterdata", "dll");
      sc.Add( @"getwwgeral_cargofilterdata", "dll");
      sc.Add( @"getpromptgeral_cargofilterdata", "dll");
      sc.Add( @"getwwgeral_unidadeorganizacionalfilterdata", "dll");
      sc.Add( @"getpromptgeral_unidadeorganizacionalfilterdata", "dll");
      sc.Add( @"getgeral_unidadeorganizacionalgeral_unidadeorganizacionalwcfilterdata", "dll");
      sc.Add( @"getgeral_uocargoswcfilterdata", "dll");
      sc.Add( @"getwwgeral_tp_uofilterdata", "dll");
      sc.Add( @"getpromptgeral_tp_uofilterdata", "dll");
      sc.Add( @"getgeral_tp_uogeral_unidadeorganizacionalwcfilterdata", "dll");
      sc.Add( @"getwwtipodocumentofilterdata", "dll");
      sc.Add( @"getprompttipodocumentofilterdata", "dll");
      sc.Add( @"getwwitemnaomensuravelfilterdata", "dll");
      sc.Add( @"getpromptitemnaomensuravelfilterdata", "dll");
      sc.Add( @"getwwlotearquivoanexofilterdata", "dll");
      sc.Add( @"getpromptlotearquivoanexofilterdata", "dll");
      sc.Add( @"getwwparametrosplanilhasfilterdata", "dll");
      sc.Add( @"getpromptparametrosplanilhasfilterdata", "dll");
      sc.Add( @"getwwglosariofilterdata", "dll");
      sc.Add( @"getpromptglosariofilterdata", "dll");
      sc.Add( @"getwwregrascontagemfilterdata", "dll");
      sc.Add( @"getpromptregrascontagemfilterdata", "dll");
      sc.Add( @"getwwtipodecontafilterdata", "dll");
      sc.Add( @"getprompttipodecontafilterdata", "dll");
      sc.Add( @"getwwcaixafilterdata", "dll");
      sc.Add( @"getpromptcaixafilterdata", "dll");
      sc.Add( @"getwwcontratoservicostelasfilterdata", "dll");
      sc.Add( @"getpromptcontratoservicostelasfilterdata", "dll");
      sc.Add( @"getwwcontratoservicosvncfilterdata", "dll");
      sc.Add( @"getpromptcontratoservicosvncfilterdata", "dll");
      sc.Add( @"getwwvariaveiscocomofilterdata", "dll");
      sc.Add( @"getpromptvariaveiscocomofilterdata", "dll");
      sc.Add( @"getwwtipoprojetofilterdata", "dll");
      sc.Add( @"getprompttipoprojetofilterdata", "dll");
      sc.Add( @"getwwferiadosfilterdata", "dll");
      sc.Add( @"getpromptferiadosfilterdata", "dll");
      sc.Add( @"getwwagendaatendimentofilterdata", "dll");
      sc.Add( @"getagendaatendimentowcfilterdata", "dll");
      sc.Add( @"getwwunidademedicaofilterdata", "dll");
      sc.Add( @"getpromptunidademedicaofilterdata", "dll");
      sc.Add( @"getwwcontratounidadesfilterdata", "dll");
      sc.Add( @"getpromptcontratounidadesfilterdata", "dll");
      sc.Add( @"getwworganizacaofilterdata", "dll");
      sc.Add( @"getpromptorganizacaofilterdata", "dll");
      sc.Add( @"getorganizacaoareasdetrabalhowcfilterdata", "dll");
      sc.Add( @"getwwcontagemresultadoincidentesfilterdata", "dll");
      sc.Add( @"getpromptcontagemresultadoincidentesfilterdata", "dll");
      sc.Add( @"getwwcontratoservicosindicadorfilterdata", "dll");
      sc.Add( @"getpromptcontratoservicosindicadorfilterdata", "dll");
      sc.Add( @"getwwcontratoservicosprioridadefilterdata", "dll");
      sc.Add( @"getpromptcontratoservicosprioridadefilterdata", "dll");
      sc.Add( @"getwwcontagemresultadoexecucaofilterdata", "dll");
      sc.Add( @"getwwauditfilterdata", "dll");
      sc.Add( @"getwwservicoprioridadefilterdata", "dll");
      sc.Add( @"getpromptservicoprioridadefilterdata", "dll");
      sc.Add( @"getwp_gridfilterdata", "dll");
      sc.Add( @"getwwnotaempenhofilterdata", "dll");
      sc.Add( @"getpromptnotaempenhofilterdata", "dll");
      sc.Add( @"getwwautorizacaoconsumofilterdata", "dll");
      sc.Add( @"getpromptautorizacaoconsumofilterdata", "dll");
      sc.Add( @"getwwcontagemresultadofilterdata", "dll");
      sc.Add( @"getextrawwcontagemresultadoextraselectionfilterdata", "dll");
      sc.Add( @"getextrawwcontagemresultadowwaceitefaturamentofilterdata", "dll");
      sc.Add( @"getextrawwcontagemresultadoosfilterdata", "dll");
      sc.Add( @"getpromptcontagemresultadofilterdata", "dll");
      sc.Add( @"getcontagemresultadochcklstlogwcfilterdata", "dll");
      sc.Add( @"getwclogresponsavelfilterdata", "dll");
      sc.Add( @"getwwcontagemresultadocontagensfilterdata", "dll");
      sc.Add( @"getpromptcontagemresultadocontagensfilterdata", "dll");
      sc.Add( @"getwwservicofluxofilterdata", "dll");
      sc.Add( @"getpromptservicofluxofilterdata", "dll");
      sc.Add( @"getsaldocontratonotaempenhowcfilterdata", "dll");
      sc.Add( @"getwwemailfilterdata", "dll");
      sc.Add( @"getwwartefatosfilterdata", "dll");
      sc.Add( @"getwwcatalogosutentacaofilterdata", "dll");
      sc.Add( @"getcontagemitemcontagemitematributosfsoftwarewcfilterdata", "dll");
      sc.Add( @"getwwreferenciatecnicafilterdata", "dll");
      sc.Add( @"getwwmenufilterdata", "dll");
      sc.Add( @"getwwgrupoobjetocontrolefilterdata", "dll");
      sc.Add( @"getwwcheckfilterdata", "dll");
      sc.Add( @"getchecklistservicoswcfilterdata", "dll");
      sc.Add( @"getwwchecklistfilterdata", "dll");
      sc.Add( @"getchecklistitenswcfilterdata", "dll");
      sc.Add( @"aexportreportwwcontagemresultadocstm", "dll");
      sc.Add( @"getwwsistemaversaofilterdata", "dll");
      sc.Add( @"getsistemaversaowcfilterdata", "dll");
      sc.Add( @"arel_redmineservicos", "dll");
      sc.Add( @"arel_redminesistemas", "dll");
      sc.Add( @"getextrawwcontagemresultadoemgarantiafilterdata", "dll");
      sc.Add( @"getwwcontagemitemfilterdata", "dll");
      sc.Add( @"getpromptfuncaoapffilterdata", "dll");
      sc.Add( @"getfuncaoapffuncoesapfatributoswcfilterdata", "dll");
      sc.Add( @"getfuncaoapffuncaoapfevidenciawcfilterdata", "dll");
      sc.Add( @"getpromptcontratogarantiafilterdata", "dll");
      sc.Add( @"getpromptcontratoarquivosanexosfilterdata", "dll");
      sc.Add( @"getpromptcontratoclausulasfilterdata", "dll");
      sc.Add( @"getpromptservicogrupofilterdata", "dll");
      sc.Add( @"getservicogruposervicowcfilterdata", "dll");
      sc.Add( @"getpromptreferenciatecnicafilterdata", "dll");
      sc.Add( @"getpromptatributosfilterdata", "dll");
      sc.Add( @"getatributosfuncoesapfatributoswcfilterdata", "dll");
      sc.Add( @"getprompttecnologiafilterdata", "dll");
      sc.Add( @"getmodulomodulofuncoeswcfilterdata", "dll");
      sc.Add( @"getmodulotabelawcfilterdata", "dll");
      sc.Add( @"getpromptfuncaousuariofilterdata", "dll");
      sc.Add( @"getfuncaousuariofuncoesusuariofuncoesapfwcfilterdata", "dll");
      sc.Add( @"getwwrequisitofilterdata", "dll");
      sc.Add( @"getwp_caixadmnfilterdata", "dll");
      sc.Add( @"getdemandanotificacoeswcfilterdata", "dll");
      sc.Add( @"getwwcontagemresultadonotificacaofilterdata", "dll");
      sc.Add( @"arel_semplanilha", "dll");
      sc.Add( @"arel_ifpug", "dll");
      sc.Add( @"getwwpessoacertificadofilterdata", "dll");
      sc.Add( @"getwwusuariofilterdata", "dll");
      sc.Add( @"getwc_perfisusuariofilterdata", "dll");
      sc.Add( @"getusuario_servicosfilterdata", "dll");
      sc.Add( @"getusuariocertificadowcfilterdata", "dll");
      sc.Add( @"arel_ifpugsummary", "dll");
      sc.Add( @"aajustardatasentrega", "dll");
      sc.Add( @"getwwlinhanegociofilterdata", "dll");
      sc.Add( @"getwwtiporequisitofilterdata", "dll");
      sc.Add( @"getwwindicadorfilterdata", "dll");
      sc.Add( @"getwwusuarionotificafilterdata", "dll");
      sc.Add( @"getpromptusuarionotificafilterdata", "dll");
      sc.Add( @"aresponsavel_divergencias", "dll");
      sc.Add( @"acriarcicloscrr", "dll");
      sc.Add( @"arel_tav1", "dll");
      sc.Add( @"arel_relatoriogerencialcontagempdf", "dll");
      sc.Add( @"aprc_gerarelatoriogerencialcontagem", "dll");
      sc.Add( @"aws_meetrikaintegracao", "dll");
      sc.Add( @"aprc_descr", "dll");
      sc.Add( @"arel_relatoriocomparacaodemandasxls2", "dll");
      sc.Add( @"aprc_demandasvinculadas", "dll");
      sc.Add( @"arel_relatoriocomparacaodemandasxlsvs2", "dll");
      sc.Add( @"appmasterpage", "dll");
      sc.Add( @"recentlinks", "dll");
      sc.Add( @"promptmasterpage", "dll");
      sc.Add( @"rwdmasterpage", "dll");
      sc.Add( @"rwdrecentlinks", "dll");
      sc.Add( @"rwdpromptmasterpage", "dll");
      sc.Add( @"wwpbaseobjects\managefilters", "dll");
      sc.Add( @"wwpbaseobjects\savefilteras", "dll");
      sc.Add( @"wwpbaseobjects\exportoptions", "dll");
      sc.Add( @"wwpbaseobjects\addressdisplay", "dll");
      sc.Add( @"wwpbaseobjects\columnsselector", "dll");
      sc.Add( @"wwpbaseobjects\home", "dll");
      sc.Add( @"workwithplusbootstrapmasterpage", "dll");
      sc.Add( @"wwpbaseobjects\notauthorized", "dll");
      sc.Add( @"wwpbaseobjects\promptgeolocation", "dll");
      sc.Add( @"wwpbaseobjects\wwptabbedview", "dll");
      sc.Add( @"viewusuario", "dll");
      sc.Add( @"wwusuario", "dll");
      sc.Add( @"promptusuario", "dll");
      sc.Add( @"usuariogeneral", "dll");
      sc.Add( @"wc_perfisusuario", "dll");
      sc.Add( @"wwperfil", "dll");
      sc.Add( @"promptperfil", "dll");
      sc.Add( @"perfilgeneral", "dll");
      sc.Add( @"perfilusuarioperfilwc", "dll");
      sc.Add( @"viewperfil", "dll");
      sc.Add( @"viewareatrabalho", "dll");
      sc.Add( @"wwareatrabalho", "dll");
      sc.Add( @"promptareatrabalho", "dll");
      sc.Add( @"areatrabalhogeneral", "dll");
      sc.Add( @"wp_associationusuariousuarioperfil", "dll");
      sc.Add( @"viewcontratante", "dll");
      sc.Add( @"wwcontratante", "dll");
      sc.Add( @"promptcontratante", "dll");
      sc.Add( @"contratantegeneral", "dll");
      sc.Add( @"viewpais", "dll");
      sc.Add( @"wwpais", "dll");
      sc.Add( @"promptpais", "dll");
      sc.Add( @"paisgeneral", "dll");
      sc.Add( @"paismunicipiowc", "dll");
      sc.Add( @"viewestado", "dll");
      sc.Add( @"wwestado", "dll");
      sc.Add( @"promptestado", "dll");
      sc.Add( @"estadogeneral", "dll");
      sc.Add( @"estadomunicipiowc", "dll");
      sc.Add( @"viewmunicipio", "dll");
      sc.Add( @"wwmunicipio", "dll");
      sc.Add( @"promptmunicipio", "dll");
      sc.Add( @"municipiogeneral", "dll");
      sc.Add( @"viewbanco", "dll");
      sc.Add( @"wwbanco", "dll");
      sc.Add( @"promptbanco", "dll");
      sc.Add( @"bancogeneral", "dll");
      sc.Add( @"bancobancoagenciawc", "dll");
      sc.Add( @"viewbancoagencia", "dll");
      sc.Add( @"wwbancoagencia", "dll");
      sc.Add( @"promptbancoagencia", "dll");
      sc.Add( @"bancoagenciageneral", "dll");
      sc.Add( @"gamlogout", "dll");
      sc.Add( @"gamexamplelogin", "dll");
      sc.Add( @"gamhome", "dll");
      sc.Add( @"gammasterpage", "dll");
      sc.Add( @"gammenu", "dll");
      sc.Add( @"gamexamplewwroles", "dll");
      sc.Add( @"gamexampleentryrole", "dll");
      sc.Add( @"gammasterpagepopup", "dll");
      sc.Add( @"gamexamplewwroleroles", "dll");
      sc.Add( @"gamexampleroleselect", "dll");
      sc.Add( @"gamexamplewwrolepermissions", "dll");
      sc.Add( @"gamexamplewwusers", "dll");
      sc.Add( @"gamexampleentryuser", "dll");
      sc.Add( @"gamexamplewwuserrole", "dll");
      sc.Add( @"gamexamplesetpassword", "dll");
      sc.Add( @"gamexamplewwsecuritypolicy", "dll");
      sc.Add( @"gamexampleentrysecuritypolicy", "dll");
      sc.Add( @"gamexamplewwrepositories", "dll");
      sc.Add( @"gamexampleentryrepository", "dll");
      sc.Add( @"gamrepositoryconfiguration", "dll");
      sc.Add( @"gamexamplewwauthtypes", "dll");
      sc.Add( @"gamexampleentryauthenticationtype", "dll");
      sc.Add( @"gamexampletestexternallogin", "dll");
      sc.Add( @"gamexamplewwconnections", "dll");
      sc.Add( @"gamexampleentryconnection", "dll");
      sc.Add( @"gamexamplechangepassword", "dll");
      sc.Add( @"gamexampleupdateregisteruser", "dll");
      sc.Add( @"gamexamplerecoverpasswordstep1", "dll");
      sc.Add( @"gamexamplechangeyourpassword", "dll");
      sc.Add( @"gamexampleregisteruser", "dll");
      sc.Add( @"gamexamplenotauthorized", "dll");
      sc.Add( @"gamexamplerecoverpasswordstep2", "dll");
      sc.Add( @"gamexamplewwapplications", "dll");
      sc.Add( @"gamexampleentryapplication", "dll");
      sc.Add( @"gamexamplewwapppermissions", "dll");
      sc.Add( @"gamexampleentryapppermission", "dll");
      sc.Add( @"gamexampleapppermissionchildren", "dll");
      sc.Add( @"gamexamplechangerepository", "dll");
      sc.Add( @"gamexamplerolepermissionselect", "dll");
      sc.Add( @"gamexampleapppermissionselect", "dll");
      sc.Add( @"gamremotelogin", "dll");
      sc.Add( @"gamssologin", "dll");
      sc.Add( @"login", "dll");
      sc.Add( @"wp_naoautorizado", "dll");
      sc.Add( @"viewpessoa", "dll");
      sc.Add( @"wwpessoa", "dll");
      sc.Add( @"promptpessoa", "dll");
      sc.Add( @"pessoageneral", "dll");
      sc.Add( @"viewcontratada", "dll");
      sc.Add( @"wwcontratada", "dll");
      sc.Add( @"promptcontratada", "dll");
      sc.Add( @"contratadageneral", "dll");
      sc.Add( @"wp_usuario", "dll");
      sc.Add( @"wp_associationcontratadausuario", "dll");
      sc.Add( @"viewsistema", "dll");
      sc.Add( @"wwsistema", "dll");
      sc.Add( @"promptsistema", "dll");
      sc.Add( @"sistemageneral", "dll");
      sc.Add( @"sistemamodulowc", "dll");
      sc.Add( @"viewmodulo", "dll");
      sc.Add( @"wwmodulo", "dll");
      sc.Add( @"promptmodulo", "dll");
      sc.Add( @"modulogeneral", "dll");
      sc.Add( @"modulomodulofuncoeswc", "dll");
      sc.Add( @"modulotabelawc", "dll");
      sc.Add( @"viewfuncaousuario", "dll");
      sc.Add( @"wwfuncaousuario", "dll");
      sc.Add( @"promptfuncaousuario", "dll");
      sc.Add( @"funcaousuariogeneral", "dll");
      sc.Add( @"funcaousuariomodulofuncoeswc", "dll");
      sc.Add( @"funcaousuariofuncoesusuariofuncoesapfwc", "dll");
      sc.Add( @"wwfuncaoapf", "dll");
      sc.Add( @"promptfuncaoapf", "dll");
      sc.Add( @"viewtabela", "dll");
      sc.Add( @"wwtabela", "dll");
      sc.Add( @"prompttabela", "dll");
      sc.Add( @"tabelageneral", "dll");
      sc.Add( @"tabelaatributoswc", "dll");
      sc.Add( @"viewatributos", "dll");
      sc.Add( @"wwatributos", "dll");
      sc.Add( @"promptatributos", "dll");
      sc.Add( @"atributosgeneral", "dll");
      sc.Add( @"atributosfuncoesapfatributoswc", "dll");
      sc.Add( @"wp_associarfuncoesusuariomodulo", "dll");
      sc.Add( @"wp_associarfuncoesapffuncaousuario", "dll");
      sc.Add( @"viewtecnologia", "dll");
      sc.Add( @"wwtecnologia", "dll");
      sc.Add( @"prompttecnologia", "dll");
      sc.Add( @"tecnologiageneral", "dll");
      sc.Add( @"viewmetodologia", "dll");
      sc.Add( @"wwmetodologia", "dll");
      sc.Add( @"promptmetodologia", "dll");
      sc.Add( @"metodologiageneral", "dll");
      sc.Add( @"metodologiametodologiafaseswc", "dll");
      sc.Add( @"viewcontagem", "dll");
      sc.Add( @"contagemcontagemitemwc", "dll");
      sc.Add( @"viewcontagemitem", "dll");
      sc.Add( @"wwcontagemitem", "dll");
      sc.Add( @"promptcontagemitem", "dll");
      sc.Add( @"contagemitemgeneral", "dll");
      sc.Add( @"contagemitemcontagemitematributosfsoftwarewc", "dll");
      sc.Add( @"contagemitemcontagemitemparecerwc", "dll");
      sc.Add( @"contagemitemcontagemitematributosfmetricawc", "dll");
      sc.Add( @"viewreferenciatecnica", "dll");
      sc.Add( @"wwreferenciatecnica", "dll");
      sc.Add( @"promptreferenciatecnica", "dll");
      sc.Add( @"referenciatecnicageneral", "dll");
      sc.Add( @"viewguia", "dll");
      sc.Add( @"wwguia", "dll");
      sc.Add( @"promptguia", "dll");
      sc.Add( @"guiageneral", "dll");
      sc.Add( @"wc_tabproposito", "dll");
      sc.Add( @"wc_tabescopo", "dll");
      sc.Add( @"wc_tabfronteira", "dll");
      sc.Add( @"wc_tabobservacoes", "dll");
      sc.Add( @"wp_associaratributosfabricasoftware", "dll");
      sc.Add( @"wp_associaratributosfm", "dll");
      sc.Add( @"viewcontagemitemparecer", "dll");
      sc.Add( @"wwcontagemitemparecer", "dll");
      sc.Add( @"promptcontagemitemparecer", "dll");
      sc.Add( @"contagemitemparecergeneral", "dll");
      sc.Add( @"viewmenu", "dll");
      sc.Add( @"wwmenu", "dll");
      sc.Add( @"promptmenu", "dll");
      sc.Add( @"menugeneral", "dll");
      sc.Add( @"wp_associarperfilmenu", "dll");
      sc.Add( @"wwcontrato", "dll");
      sc.Add( @"promptcontrato", "dll");
      sc.Add( @"contratogeneral", "dll");
      sc.Add( @"contratocontratoclausulaswc", "dll");
      sc.Add( @"contratocontratogarantiawc", "dll");
      sc.Add( @"contratocontratoarquivosanexoswc", "dll");
      sc.Add( @"contratocontratodadoscertamewc", "dll");
      sc.Add( @"contratocontratotermoaditivowc", "dll");
      sc.Add( @"contratocontratoobrigacaowc", "dll");
      sc.Add( @"viewcontrato", "dll");
      sc.Add( @"wwcontratogarantia", "dll");
      sc.Add( @"promptcontratogarantia", "dll");
      sc.Add( @"contratogarantiageneral", "dll");
      sc.Add( @"viewcontratogarantia", "dll");
      sc.Add( @"wwcontratoarquivosanexos", "dll");
      sc.Add( @"promptcontratoarquivosanexos", "dll");
      sc.Add( @"contratoarquivosanexosgeneral", "dll");
      sc.Add( @"viewcontratoarquivosanexos", "dll");
      sc.Add( @"wwcontratoclausulas", "dll");
      sc.Add( @"promptcontratoclausulas", "dll");
      sc.Add( @"contratoclausulasgeneral", "dll");
      sc.Add( @"viewcontratoclausulas", "dll");
      sc.Add( @"promptservicogrupo", "dll");
      sc.Add( @"servicogruposervicowc", "dll");
      sc.Add( @"wwservicogrupo", "dll");
      sc.Add( @"servicogrupogeneral", "dll");
      sc.Add( @"viewservicogrupo", "dll");
      sc.Add( @"wwservico", "dll");
      sc.Add( @"promptservico", "dll");
      sc.Add( @"servicogeneral", "dll");
      sc.Add( @"viewservico", "dll");
      sc.Add( @"wwcontratoservicos", "dll");
      sc.Add( @"promptcontratoservicos", "dll");
      sc.Add( @"contratoservicosgeneral", "dll");
      sc.Add( @"viewcontratoservicos", "dll");
      sc.Add( @"wwcontratoocorrencia", "dll");
      sc.Add( @"promptcontratoocorrencia", "dll");
      sc.Add( @"contratoocorrenciageneral", "dll");
      sc.Add( @"contratoocorrenciacontratoocorrencianotificacaowc", "dll");
      sc.Add( @"viewcontratoocorrencia", "dll");
      sc.Add( @"wwcontratoocorrencianotificacao", "dll");
      sc.Add( @"promptcontratoocorrencianotificacao", "dll");
      sc.Add( @"contratoocorrencianotificacaogeneral", "dll");
      sc.Add( @"viewcontratoocorrencianotificacao", "dll");
      sc.Add( @"wwcontratodadoscertame", "dll");
      sc.Add( @"promptcontratodadoscertame", "dll");
      sc.Add( @"contratodadoscertamegeneral", "dll");
      sc.Add( @"viewcontratodadoscertame", "dll");
      sc.Add( @"wwcontratotermoaditivo", "dll");
      sc.Add( @"promptcontratotermoaditivo", "dll");
      sc.Add( @"contratotermoaditivogeneral", "dll");
      sc.Add( @"viewcontratotermoaditivo", "dll");
      sc.Add( @"wwservicoans", "dll");
      sc.Add( @"promptservicoans", "dll");
      sc.Add( @"servicoansgeneral", "dll");
      sc.Add( @"viewservicoans", "dll");
      sc.Add( @"wwcontratoobrigacao", "dll");
      sc.Add( @"promptcontratoobrigacao", "dll");
      sc.Add( @"contratoobrigacaogeneral", "dll");
      sc.Add( @"viewcontratoobrigacao", "dll");
      sc.Add( @"wwmetodologiafases", "dll");
      sc.Add( @"promptmetodologiafases", "dll");
      sc.Add( @"metodologiafasesgeneral", "dll");
      sc.Add( @"metodologiafasespadroesartefatoswc", "dll");
      sc.Add( @"viewmetodologiafases", "dll");
      sc.Add( @"wwpadroesartefatos", "dll");
      sc.Add( @"promptpadroesartefatos", "dll");
      sc.Add( @"padroesartefatosgeneral", "dll");
      sc.Add( @"viewpadroesartefatos", "dll");
      sc.Add( @"contratadacontratowc", "dll");
      sc.Add( @"tabbedview", "dll");
      sc.Add( @"contratanteareatrabalhowc", "dll");
      sc.Add( @"contratantecontratanteusuariowc", "dll");
      sc.Add( @"contratadacontratadausuariowc", "dll");
      sc.Add( @"contratocontratoocorrenciawc", "dll");
      sc.Add( @"viewcontratanteusuario", "dll");
      sc.Add( @"wwcontratanteusuario", "dll");
      sc.Add( @"wp_novousuario", "dll");
      sc.Add( @"promptcontratanteusuario", "dll");
      sc.Add( @"contratanteusuariogeneral", "dll");
      sc.Add( @"wwparametrossistema", "dll");
      sc.Add( @"parametrossistemageneral", "dll");
      sc.Add( @"viewparametrossistema", "dll");
      sc.Add( @"appmastersd", "dll");
      sc.Add( @"wp_novolicensiado", "dll");
      sc.Add( @"wp_novoperfil", "dll");
      sc.Add( @"wp_permissaopapeis", "dll");
      sc.Add( @"wp_selecaopermissaoperfil", "dll");
      sc.Add( @"wp_alterarminhasenha", "dll");
      sc.Add( @"wp_apppermissao", "dll");
      sc.Add( @"wwcontratadausuario", "dll");
      sc.Add( @"promptcontratadausuario", "dll");
      sc.Add( @"contratadausuariogeneral", "dll");
      sc.Add( @"viewcontratadausuario", "dll");
      sc.Add( @"wwusuarioperfil", "dll");
      sc.Add( @"promptusuarioperfil", "dll");
      sc.Add( @"usuarioperfilgeneral", "dll");
      sc.Add( @"viewusuarioperfil", "dll");
      sc.Add( @"wp_associarmenuperfil", "dll");
      sc.Add( @"menumenuperfilwc", "dll");
      sc.Add( @"wp_associationtecnologiaambientetecnologico", "dll");
      sc.Add( @"funcaoapfatributoswc", "dll");
      sc.Add( @"funcaoapfgeneral", "dll");
      sc.Add( @"funcaoapffuncoesapfatributoswc", "dll");
      sc.Add( @"viewfuncaoapf", "dll");
      sc.Add( @"sistemafuncaopapfwc", "dll");
      sc.Add( @"wwfuncaodados", "dll");
      sc.Add( @"promptfuncaodados", "dll");
      sc.Add( @"funcaodadostabelawc", "dll");
      sc.Add( @"funcaodadosgeneral", "dll");
      sc.Add( @"viewfuncaodados", "dll");
      sc.Add( @"sistemafuncaodadoswc", "dll");
      sc.Add( @"sistematabelaatributowc", "dll");
      sc.Add( @"sistemafuncaousuariowc", "dll");
      sc.Add( @"wp_associarfuncaoapffuncoesusuariofuncoesapf", "dll");
      sc.Add( @"funcaoapffuncaoapfwc", "dll");
      sc.Add( @"viewfuncoesapfatributos", "dll");
      sc.Add( @"wwfuncoesapfatributos", "dll");
      sc.Add( @"promptfuncoesapfatributos", "dll");
      sc.Add( @"funcoesapfatributosgeneral", "dll");
      sc.Add( @"wwambientetecnologico", "dll");
      sc.Add( @"wp_associationambientetecnologicotecnologia", "dll");
      sc.Add( @"ambientetecnologicogeneral", "dll");
      sc.Add( @"viewambientetecnologico", "dll");
      sc.Add( @"wp_associationfuncaousuario_modulos", "dll");
      sc.Add( @"atributoregradenegocio", "dll");
      sc.Add( @"promptambientetecnologico", "dll");
      sc.Add( @"ambientetecnologicoambientetecnologicotecnologiaswc", "dll");
      sc.Add( @"wp_copiarcolar", "dll");
      sc.Add( @"wp_copiarcolaratributos", "dll");
      sc.Add( @"wp_consultageral", "dll");
      sc.Add( @"wp_funcaousuariodetalhes", "dll");
      sc.Add( @"funcaousuarioconsultawc", "dll");
      sc.Add( @"wp_funcaodadosdetalhes", "dll");
      sc.Add( @"funcaodadostabelaconsultawc", "dll");
      sc.Add( @"wp_funcaoapfdetalhes", "dll");
      sc.Add( @"funcaoapfatributosconsultawc", "dll");
      sc.Add( @"wp_tabeladetalhes", "dll");
      sc.Add( @"tabelaconsultawc", "dll");
      sc.Add( @"wp_mensagem", "dll");
      sc.Add( @"wc_funcaodadostabelas", "dll");
      sc.Add( @"wp_funcaodadostabelains", "dll");
      sc.Add( @"wc_funcaodadostabelains", "dll");
      sc.Add( @"funcaoapffuncaoapfevidenciawc", "dll");
      sc.Add( @"wwfuncaoapfevidencia", "dll");
      sc.Add( @"promptfuncaoapfevidencia", "dll");
      sc.Add( @"funcaoapfevidenciageneral", "dll");
      sc.Add( @"viewfuncaoapfevidencia", "dll");
      sc.Add( @"tecnologiaambientetecnologicowc", "dll");
      sc.Add( @"wp_funcaoapfatributosins", "dll");
      sc.Add( @"wp_funcaodados_rlrs", "dll");
      sc.Add( @"wc_funcaoapfatributosins", "dll");
      sc.Add( @"wwnaoconformidade", "dll");
      sc.Add( @"promptnaoconformidade", "dll");
      sc.Add( @"naoconformidadegeneral", "dll");
      sc.Add( @"viewnaoconformidade", "dll");
      sc.Add( @"wwstatus", "dll");
      sc.Add( @"promptstatus", "dll");
      sc.Add( @"statusgeneral", "dll");
      sc.Add( @"viewstatus", "dll");
      sc.Add( @"viewsolicitacoes", "dll");
      sc.Add( @"wwsolicitacoes", "dll");
      sc.Add( @"promptsolicitacoes", "dll");
      sc.Add( @"solicitacoesgeneral", "dll");
      sc.Add( @"solicitacoessolicitacoesitenswc", "dll");
      sc.Add( @"solicitacaodeservico", "dll");
      sc.Add( @"wc_contagemresultado_contagens", "dll");
      sc.Add( @"viewcontagemresultadocontagens", "dll");
      sc.Add( @"wwcontagemresultado", "dll");
      sc.Add( @"wp_contagemresultado_contagens", "dll");
      sc.Add( @"promptcontagemresultado", "dll");
      sc.Add( @"contagemresultadogeneral", "dll");
      sc.Add( @"contagemresultadohistoricowc", "dll");
      sc.Add( @"viewcontagemresultado", "dll");
      sc.Add( @"wwcontagemresultadocontagens", "dll");
      sc.Add( @"promptcontagemresultadocontagens", "dll");
      sc.Add( @"contagemresultadocontagensgeneral", "dll");
      sc.Add( @"wp_importarusuarios", "dll");
      sc.Add( @"wc_contadoresfs", "dll");
      sc.Add( @"wp_copiarcolarmodulos", "dll");
      sc.Add( @"wp_newsistema", "dll");
      sc.Add( @"contagemresultadocontagemresultadocontagenswc", "dll");
      sc.Add( @"wp_lote", "dll");
      sc.Add( @"wp_contagemresultadoretorno", "dll");
      sc.Add( @"extrawwcontagemresultadoextraselection", "dll");
      sc.Add( @"extrawwcontagemresultadowwaceitefaturamento", "dll");
      sc.Add( @"wp_importfromfile", "dll");
      sc.Add( @"wp_contagemresultadofinanceiro", "dll");
      sc.Add( @"wp_alteradatacnt", "dll");
      sc.Add( @"viewlote", "dll");
      sc.Add( @"wwlote", "dll");
      sc.Add( @"promptlote", "dll");
      sc.Add( @"lotegeneral", "dll");
      sc.Add( @"wp_lote_nome", "dll");
      sc.Add( @"wp_consultacontadores", "dll");
      sc.Add( @"wp_consultacontadoresgrf", "dll");
      sc.Add( @"viewcontagemresultadoevidencia", "dll");
      sc.Add( @"wwcontagemresultadoevidencia", "dll");
      sc.Add( @"promptcontagemresultadoevidencia", "dll");
      sc.Add( @"contagemresultadoevidenciageneral", "dll");
      sc.Add( @"contagemresultadoevidenciaswc", "dll");
      sc.Add( @"wc_contagemresultadoevidencias", "dll");
      sc.Add( @"wc_viewcontagemresultadoevidencias", "dll");
      sc.Add( @"wp_contagemresultadoevidencia", "dll");
      sc.Add( @"wp_consultacontratadagrf", "dll");
      sc.Add( @"wp_consultacontratadas", "dll");
      sc.Add( @"wp_osvinculada", "dll");
      sc.Add( @"wp_os", "dll");
      sc.Add( @"viewgeral_funcao", "dll");
      sc.Add( @"wwgeral_funcao", "dll");
      sc.Add( @"promptgeral_funcao", "dll");
      sc.Add( @"geral_funcaogeneral", "dll");
      sc.Add( @"viewgeral_grupo_funcao", "dll");
      sc.Add( @"wwgeral_grupo_funcao", "dll");
      sc.Add( @"promptgeral_grupo_funcao", "dll");
      sc.Add( @"geral_grupo_funcaogeneral", "dll");
      sc.Add( @"geral_grupo_funcaogeral_funcaowc", "dll");
      sc.Add( @"viewgeral_grupo_cargo", "dll");
      sc.Add( @"wwgeral_grupo_cargo", "dll");
      sc.Add( @"promptgeral_grupo_cargo", "dll");
      sc.Add( @"geral_grupo_cargogeneral", "dll");
      sc.Add( @"geral_grupo_cargogeral_cargowc", "dll");
      sc.Add( @"viewgeral_cargo", "dll");
      sc.Add( @"wwgeral_cargo", "dll");
      sc.Add( @"promptgeral_cargo", "dll");
      sc.Add( @"geral_cargogeneral", "dll");
      sc.Add( @"viewgeral_unidadeorganizacional", "dll");
      sc.Add( @"wwgeral_unidadeorganizacional", "dll");
      sc.Add( @"promptgeral_unidadeorganizacional", "dll");
      sc.Add( @"geral_unidadeorganizacionalgeneral", "dll");
      sc.Add( @"geral_unidadeorganizacionalgeral_unidadeorganizacionalwc", "dll");
      sc.Add( @"viewgeral_tp_uo", "dll");
      sc.Add( @"wwgeral_tp_uo", "dll");
      sc.Add( @"promptgeral_tp_uo", "dll");
      sc.Add( @"geral_tp_uogeneral", "dll");
      sc.Add( @"geral_tp_uogeral_unidadeorganizacionalwc", "dll");
      sc.Add( @"viewtipodocumento", "dll");
      sc.Add( @"wwtipodocumento", "dll");
      sc.Add( @"prompttipodocumento", "dll");
      sc.Add( @"tipodocumentogeneral", "dll");
      sc.Add( @"viewprojeto", "dll");
      sc.Add( @"wwprojeto", "dll");
      sc.Add( @"promptprojeto", "dll");
      sc.Add( @"projetogeneral", "dll");
      sc.Add( @"sistemafronteira", "dll");
      sc.Add( @"wp_cfgperfil", "dll");
      sc.Add( @"wp_projetomelhoriaitens", "dll");
      sc.Add( @"wp_projetomelhoriaobs", "dll");
      sc.Add( @"wp_lote_nfe", "dll");
      sc.Add( @"wp_lote_liq", "dll");
      sc.Add( @"wp_osservico", "dll");
      sc.Add( @"wp_consultaareasgrf", "dll");
      sc.Add( @"extrawwcontagemresultadoos", "dll");
      sc.Add( @"wp_updossistema", "dll");
      sc.Add( @"wwreferenciainm", "dll");
      sc.Add( @"promptreferenciainm", "dll");
      sc.Add( @"referenciainmgeneral", "dll");
      sc.Add( @"referenciainmitemnaomensuravelwc", "dll");
      sc.Add( @"viewreferenciainm", "dll");
      sc.Add( @"wwitemnaomensuravel", "dll");
      sc.Add( @"itemnaomensuravelgeneral", "dll");
      sc.Add( @"viewitemnaomensuravel", "dll");
      sc.Add( @"promptitemnaomensuravel", "dll");
      sc.Add( @"sistemabaseline", "dll");
      sc.Add( @"wc_sistemacontagens", "dll");
      sc.Add( @"wc_demandasvinculadas", "dll");
      sc.Add( @"wp_checklist", "dll");
      sc.Add( @"wp_estatisticassistemas", "dll");
      sc.Add( @"wp_sistemasxpfgrf", "dll");
      sc.Add( @"wp_eststmgrf", "dll");
      sc.Add( @"wp_estatisticapiegrf", "dll");
      sc.Add( @"wp_aplicardeflator", "dll");
      sc.Add( @"contagemresultadochcklstlogwc", "dll");
      sc.Add( @"usuario_servicos", "dll");
      sc.Add( @"wp_associationusuarioservicos", "dll");
      sc.Add( @"wp_associationservicousuarios", "dll");
      sc.Add( @"lotearquivosanexoswc", "dll");
      sc.Add( @"viewlotearquivoanexo", "dll");
      sc.Add( @"wwlotearquivoanexo", "dll");
      sc.Add( @"promptlotearquivoanexo", "dll");
      sc.Add( @"lotearquivoanexogeneral", "dll");
      sc.Add( @"viewparametrosplanilhas", "dll");
      sc.Add( @"wwparametrosplanilhas", "dll");
      sc.Add( @"promptparametrosplanilhas", "dll");
      sc.Add( @"parametrosplanilhasgeneral", "dll");
      sc.Add( @"wp_selcontagem", "dll");
      sc.Add( @"wp_selarquivo", "dll");
      sc.Add( @"extrawwloteapagar", "dll");
      sc.Add( @"wp_sql", "dll");
      sc.Add( @"viewglosario", "dll");
      sc.Add( @"wwglosario", "dll");
      sc.Add( @"promptglosario", "dll");
      sc.Add( @"glosariogeneral", "dll");
      sc.Add( @"viewregrascontagem", "dll");
      sc.Add( @"wwregrascontagem", "dll");
      sc.Add( @"promptregrascontagem", "dll");
      sc.Add( @"regrascontagemgeneral", "dll");
      sc.Add( @"contagemresultadonotaswc", "dll");
      sc.Add( @"viewtipodeconta", "dll");
      sc.Add( @"wwtipodeconta", "dll");
      sc.Add( @"prompttipodeconta", "dll");
      sc.Add( @"tipodecontageneral", "dll");
      sc.Add( @"viewcaixa", "dll");
      sc.Add( @"wwcaixa", "dll");
      sc.Add( @"promptcaixa", "dll");
      sc.Add( @"caixageneral", "dll");
      sc.Add( @"wp_vincularos", "dll");
      sc.Add( @"wp_consultadiferencapf", "dll");
      sc.Add( @"wp_consultasistemas", "dll");
      sc.Add( @"wp_selecionauo", "dll");
      sc.Add( @"servicocontratoservicowc", "dll");
      sc.Add( @"wclogresponsavel", "dll");
      sc.Add( @"wwcontratoservicosprazo", "dll");
      sc.Add( @"promptcontratoservicosprazo", "dll");
      sc.Add( @"promptcontratoservicosprazocontratoservicosprazoregra", "dll");
      sc.Add( @"contratoservicosprazogeneral", "dll");
      sc.Add( @"viewcontratoservicosprazo", "dll");
      sc.Add( @"contratoservicosprazocontratoservicosprazoregrawc", "dll");
      sc.Add( @"viewcontratoservicostelas", "dll");
      sc.Add( @"wwcontratoservicostelas", "dll");
      sc.Add( @"promptcontratoservicostelas", "dll");
      sc.Add( @"contratoservicostelasgeneral", "dll");
      sc.Add( @"contratoservicos_vncwc", "dll");
      sc.Add( @"contratoservicos_telaswc", "dll");
      sc.Add( @"wwcontratoservicosvnc", "dll");
      sc.Add( @"promptcontratoservicosvnc", "dll");
      sc.Add( @"contratoservicosvncgeneral", "dll");
      sc.Add( @"viewcontratoservicosvnc", "dll");
      sc.Add( @"wp_consultacontagens", "dll");
      sc.Add( @"sistemacontagem", "dll");
      sc.Add( @"viewvariaveiscocomo", "dll");
      sc.Add( @"wwvariaveiscocomo", "dll");
      sc.Add( @"promptvariaveiscocomo", "dll");
      sc.Add( @"variaveiscocomogeneral", "dll");
      sc.Add( @"ambientetecnologicosistemas", "dll");
      sc.Add( @"viewtipoprojeto", "dll");
      sc.Add( @"wwtipoprojeto", "dll");
      sc.Add( @"prompttipoprojeto", "dll");
      sc.Add( @"tipoprojetogeneral", "dll");
      sc.Add( @"wp_fatordeescala", "dll");
      sc.Add( @"wp_mensuracao", "dll");
      sc.Add( @"projetoestimativas", "dll");
      sc.Add( @"wp_projetofatores", "dll");
      sc.Add( @"viewsolicitacaomudanca", "dll");
      sc.Add( @"wwsolicitacaomudanca", "dll");
      sc.Add( @"promptsolicitacaomudanca", "dll");
      sc.Add( @"solicitacaomudancageneral", "dll");
      sc.Add( @"solicitacaomudancasolicitacaomudancaitemwc", "dll");
      sc.Add( @"extrawwlotenotasfiscais", "dll");
      sc.Add( @"wwsolicitacaomudancaitem", "dll");
      sc.Add( @"promptsolicitacaomudancaitem", "dll");
      sc.Add( @"solicitacaomudancaitemgeneral", "dll");
      sc.Add( @"viewsolicitacaomudancaitem", "dll");
      sc.Add( @"wc_regrascontagemanexos", "dll");
      sc.Add( @"wp_assina_docto", "dll");
      sc.Add( @"autenticidade", "dll");
      sc.Add( @"wp_funcoestrn", "dll");
      sc.Add( @"wp_relatoriocontagens", "dll");
      sc.Add( @"wp_graficocontagens", "dll");
      sc.Add( @"wc_totaiscontador", "dll");
      sc.Add( @"wc_percentualstatus", "dll");
      sc.Add( @"wp_glosa", "dll");
      sc.Add( @"wp_associarcontratoservicossistemas", "dll");
      sc.Add( @"wp_associarcontrato_gestor", "dll");
      sc.Add( @"wc_viewanexosdademanda", "dll");
      sc.Add( @"wp_agrupar", "dll");
      sc.Add( @"wp_logresponsavel", "dll");
      sc.Add( @"wp_multiplosstatus", "dll");
      sc.Add( @"wc_multiplosstatus", "dll");
      sc.Add( @"about", "dll");
      sc.Add( @"wwpbaseobjects\wizardstepsarrowwc", "dll");
      sc.Add( @"wwpbaseobjects\wizardstepsbulletwc", "dll");
      sc.Add( @"promptcontagem", "dll");
      sc.Add( @"contagembaselinewc", "dll");
      sc.Add( @"contagemcontagemhistoricowc", "dll");
      sc.Add( @"wwcontagem", "dll");
      sc.Add( @"contagemgeneral", "dll");
      sc.Add( @"geral_uocargoswc", "dll");
      sc.Add( @"viewferiados", "dll");
      sc.Add( @"wwferiados", "dll");
      sc.Add( @"promptferiados", "dll");
      sc.Add( @"feriadosgeneral", "dll");
      sc.Add( @"viewagendaatendimento", "dll");
      sc.Add( @"wwagendaatendimento", "dll");
      sc.Add( @"promptagendaatendimento", "dll");
      sc.Add( @"agendaatendimentowc", "dll");
      sc.Add( @"viewunidademedicao", "dll");
      sc.Add( @"wwunidademedicao", "dll");
      sc.Add( @"unidademedicaogeneral", "dll");
      sc.Add( @"wwcontratounidades", "dll");
      sc.Add( @"promptcontratounidades", "dll");
      sc.Add( @"viewcontratounidades", "dll");
      sc.Add( @"contratounidadesgeneral", "dll");
      sc.Add( @"promptunidademedicao", "dll");
      sc.Add( @"contrato_unidadesmedicaowc", "dll");
      sc.Add( @"contratocontratoservicoswc", "dll");
      sc.Add( @"vieworganizacao", "dll");
      sc.Add( @"wworganizacao", "dll");
      sc.Add( @"promptorganizacao", "dll");
      sc.Add( @"organizacaogeneral", "dll");
      sc.Add( @"organizacaoareasdetrabalhowc", "dll");
      sc.Add( @"wp_monitordmn", "dll");
      sc.Add( @"wp_tramitacao", "dll");
      sc.Add( @"wp_configuracaoplanilha", "dll");
      sc.Add( @"wp_checklistanalise", "dll");
      sc.Add( @"wp_prazoderesposta", "dll");
      sc.Add( @"viewcontagemresultadoincidentes", "dll");
      sc.Add( @"wwcontagemresultadoincidentes", "dll");
      sc.Add( @"promptcontagemresultadoincidentes", "dll");
      sc.Add( @"contagemresultadoincidentesgeneral", "dll");
      sc.Add( @"wp_importabaseline", "dll");
      sc.Add( @"contagemresultadoobservacao", "dll");
      sc.Add( @"wp_renumeraosfm", "dll");
      sc.Add( @"controservicoindicadoreswc", "dll");
      sc.Add( @"wp_fileupload", "dll");
      sc.Add( @"viewcontratoservicosindicador", "dll");
      sc.Add( @"wwcontratoservicosindicador", "dll");
      sc.Add( @"promptcontratoservicosindicador", "dll");
      sc.Add( @"contratoservicosindicadorgeneral", "dll");
      sc.Add( @"wp_resumodmn", "dll");
      sc.Add( @"wwcontratoservicosindicadorfaixas", "dll");
      sc.Add( @"contratoservicosindicadorfaixaswc", "dll");
      sc.Add( @"wp_re", "dll");
      sc.Add( @"wp_sntdmn", "dll");
      sc.Add( @"wp_cancelaros", "dll");
      sc.Add( @"wc_selarquivo", "dll");
      sc.Add( @"wp_novanota", "dll");
      sc.Add( @"viewcontratoservicosprioridade", "dll");
      sc.Add( @"wwcontratoservicosprioridade", "dll");
      sc.Add( @"promptcontratoservicosprioridade", "dll");
      sc.Add( @"contratoservicosprioridadegeneral", "dll");
      sc.Add( @"contratoservicosprioridadeswc", "dll");
      sc.Add( @"extrawwcontagemresultadogerencial", "dll");
      sc.Add( @"extrawwcontagemresultadopgtofnc", "dll");
      sc.Add( @"wp_clonarservico", "dll");
      sc.Add( @"wp_alterarservico", "dll");
      sc.Add( @"wp_resumoafaturar", "dll");
      sc.Add( @"wp_wdsldeos", "dll");
      sc.Add( @"wp_redmine144", "dll");
      sc.Add( @"contratanteredminewc", "dll");
      sc.Add( @"wp_homologaros", "dll");
      sc.Add( @"viewcontagemresultadoexecucao", "dll");
      sc.Add( @"wwcontagemresultadoexecucao", "dll");
      sc.Add( @"promptcontagemresultadoexecucao", "dll");
      sc.Add( @"contagemresultadoexecucaogeneral", "dll");
      sc.Add( @"wp_associationambientetecnologicosistema", "dll");
      sc.Add( @"wp_alteraagrupamento", "dll");
      sc.Add( @"wp_rdmcustomfields", "dll");
      sc.Add( @"wwaudit", "dll");
      sc.Add( @"viewservicoprioridade", "dll");
      sc.Add( @"wwservicoprioridade", "dll");
      sc.Add( @"promptservicoprioridade", "dll");
      sc.Add( @"servicoprioridadegeneral", "dll");
      sc.Add( @"wp_grid", "dll");
      sc.Add( @"wp_renumerass", "dll");
      sc.Add( @"wp_sistemadepara", "dll");
      sc.Add( @"wp_servicosdepara", "dll");
      sc.Add( @"contratoservicoscustowc", "dll");
      sc.Add( @"viewcontratoservicoscusto", "dll");
      sc.Add( @"wwcontratoservicoscusto", "dll");
      sc.Add( @"promptcontratoservicoscusto", "dll");
      sc.Add( @"contratoservicoscustogeneral", "dll");
      sc.Add( @"wp_gestao", "dll");
      sc.Add( @"sistemaevidenciaswc", "dll");
      sc.Add( @"servicoservicofluxo", "dll");
      sc.Add( @"wp_novoprojeto", "dll");
      sc.Add( @"wp_processo", "dll");
      sc.Add( @"viewservicofluxo", "dll");
      sc.Add( @"wwservicofluxo", "dll");
      sc.Add( @"promptservicofluxo", "dll");
      sc.Add( @"servicofluxogeneral", "dll");
      sc.Add( @"schedulerdetailsform", "dll");
      sc.Add( @"wp_desfaz", "dll");
      sc.Add( @"servicoservicoprioridadewc", "dll");
      sc.Add( @"servicoprocessogeneral", "dll");
      sc.Add( @"servicoprocessoservico", "dll");
      sc.Add( @"servicoprocessoservico1wc", "dll");
      sc.Add( @"servicoprocessoservicoprocessowc", "dll");
      sc.Add( @"servicoprocessoservicoanswc", "dll");
      sc.Add( @"servicoprocessosolicitacoeswc", "dll");
      sc.Add( @"servicoprocessousuarioservicoswc", "dll");
      sc.Add( @"servicoprocessocontratoservicossistemaswc", "dll");
      sc.Add( @"servicoprocessoservicoprioridadewc", "dll");
      sc.Add( @"servicoprocessoservicofluxowc", "dll");
      sc.Add( @"servicoprocessoservicofluxo1wc", "dll");
      sc.Add( @"wc_contratohistoricoconsumo", "dll");
      sc.Add( @"wwnotaempenho", "dll");
      sc.Add( @"wwsaldocontrato", "dll");
      sc.Add( @"wwhistoricoconsumo", "dll");
      sc.Add( @"viewnotaempenho", "dll");
      sc.Add( @"notaempenhogeneral", "dll");
      sc.Add( @"promptnotaempenho", "dll");
      sc.Add( @"viewsaldocontrato", "dll");
      sc.Add( @"saldocontratogeneral", "dll");
      sc.Add( @"saldocontratonotaempenhowc", "dll");
      sc.Add( @"promptsaldocontrato", "dll");
      sc.Add( @"viewhistoricoconsumo", "dll");
      sc.Add( @"historicoconsumogeneral", "dll");
      sc.Add( @"prompthistoricoconsumo", "dll");
      sc.Add( @"contratocontratonotaempenhowc", "dll");
      sc.Add( @"contratocontratosaldocontratowc", "dll");
      sc.Add( @"wp_ss", "dll");
      sc.Add( @"wp_contrato", "dll");
      sc.Add( @"wp_helpsistema", "dll");
      sc.Add( @"wp_backlog", "dll");
      sc.Add( @"wp_agendamento", "dll");
      sc.Add( @"masterpagemeetrika", "dll");
      sc.Add( @"recentlinksnew", "dll");
      sc.Add( @"webpanel1", "dll");
      sc.Add( @"wp_saldocontratoreservar", "dll");
      sc.Add( @"wwemail", "dll");
      sc.Add( @"viewemail", "dll");
      sc.Add( @"emailgeneral", "dll");
      sc.Add( @"wp_homologarproposta", "dll");
      sc.Add( @"wp_acompanhamentofinanceiro", "dll");
      sc.Add( @"wp_associarcontratanteusuario_sistema", "dll");
      sc.Add( @"wp_associarcontrato_sistema", "dll");
      sc.Add( @"wp_associarsistema_contratanteusuario", "dll");
      sc.Add( @"wwartefatos", "dll");
      sc.Add( @"wwcatalogosutentacao", "dll");
      sc.Add( @"wp_associarcatalogosutentacaoartefatos", "dll");
      sc.Add( @"wp_associarcontratoservicosartefatos", "dll");
      sc.Add( @"associationservicoservicoartefatos", "dll");
      sc.Add( @"servicoartefatoswc", "dll");
      sc.Add( @"contratoservicosartefatoswc", "dll");
      sc.Add( @"wp_solicitacaoartefatos", "dll");
      sc.Add( @"wc_solicitacaoartefatos", "dll");
      sc.Add( @"wwautorizacaoconsumo", "dll");
      sc.Add( @"contratoautorizacaoconsumowc", "dll");
      sc.Add( @"autorizacaoconsumogeneral", "dll");
      sc.Add( @"wp_linkartefatos", "dll");
      sc.Add( @"wp_execucaocontratual", "dll");
      sc.Add( @"wp_compromissosassumidos", "dll");
      sc.Add( @"wwpbaseobjects\wwpbootstrapresponsivemasterpage", "dll");
      sc.Add( @"wwpbaseobjects\wwppromptmasterpage", "dll");
      sc.Add( @"wwpbaseobjects\workwithpluspromptmasterpage", "dll");
      sc.Add( @"promptautorizacaoconsumo", "dll");
      sc.Add( @"viewautorizacaoconsumo", "dll");
      sc.Add( @"wp_associarcontrato_auxiliar", "dll");
      sc.Add( @"viewgrupoobjetocontrole", "dll");
      sc.Add( @"wwgrupoobjetocontrole", "dll");
      sc.Add( @"promptgrupoobjetocontrole", "dll");
      sc.Add( @"grupoobjetocontrolegeneral", "dll");
      sc.Add( @"wwcheck", "dll");
      sc.Add( @"promptcheck", "dll");
      sc.Add( @"checkgeneral", "dll");
      sc.Add( @"viewcheck", "dll");
      sc.Add( @"checklistservicoswc", "dll");
      sc.Add( @"wp_associationcheckservicocheck", "dll");
      sc.Add( @"wp_associationservicoservicocheck", "dll");
      sc.Add( @"wwchecklist", "dll");
      sc.Add( @"promptchecklist", "dll");
      sc.Add( @"checklistgeneral", "dll");
      sc.Add( @"viewchecklist", "dll");
      sc.Add( @"checklistcontagemresultadochcklstwc", "dll");
      sc.Add( @"checklistcontagemresultadochcklstlogwc", "dll");
      sc.Add( @"checklistitenswc", "dll");
      sc.Add( @"wp_check", "dll");
      sc.Add( @"wp_cancelaracao", "dll");
      sc.Add( @"wp_prazoexecucao", "dll");
      sc.Add( @"sistemaversaowc", "dll");
      sc.Add( @"viewsistemaversao", "dll");
      sc.Add( @"wwsistemaversao", "dll");
      sc.Add( @"promptsistemaversao", "dll");
      sc.Add( @"sistemaversaogeneral", "dll");
      sc.Add( @"wp_alterarprestadora", "dll");
      sc.Add( @"viewalerta", "dll");
      sc.Add( @"wwalerta", "dll");
      sc.Add( @"promptalerta", "dll");
      sc.Add( @"alertageneral", "dll");
      sc.Add( @"alertaalertareadwc", "dll");
      sc.Add( @"wp_alerta", "dll");
      sc.Add( @"wp_alteracontrato", "dll");
      sc.Add( @"estadogeral_unidadeorganizacionalwc", "dll");
      sc.Add( @"municipiocontratantewc", "dll");
      sc.Add( @"municipiocontratadawc", "dll");
      sc.Add( @"municipiopessoawc", "dll");
      sc.Add( @"extrawwcontagemresultadoemgarantia", "dll");
      sc.Add( @"contagemresultadorequisitoswc", "dll");
      sc.Add( @"jqselectexample", "dll");
      sc.Add( @"tabelatabela1wc", "dll");
      sc.Add( @"tabelafuncaodadostabelawc", "dll");
      sc.Add( @"viewrequisito", "dll");
      sc.Add( @"wwrequisito", "dll");
      sc.Add( @"promptrequisito", "dll");
      sc.Add( @"requisitogeneral", "dll");
      sc.Add( @"wp_novorequisito", "dll");
      sc.Add( @"wc_requisitostecnicos", "dll");
      sc.Add( @"wp_requisitos", "dll");
      sc.Add( @"wp_testeassinatura", "dll");
      sc.Add( @"wp_monitoramento", "dll");
      sc.Add( @"wp_caixadmn", "dll");
      sc.Add( @"wp_clonarperfil", "dll");
      sc.Add( @"viewcontagemresultadonotificacao", "dll");
      sc.Add( @"wwcontagemresultadonotificacao", "dll");
      sc.Add( @"promptcontagemresultadonotificacao", "dll");
      sc.Add( @"promptcontagemresultadonotificacaodestinatario", "dll");
      sc.Add( @"promptcontagemresultadonotificacaodemanda", "dll");
      sc.Add( @"contagemresultadonotificacaogeneral", "dll");
      sc.Add( @"contagemresultadonotificacaodestinatariowc", "dll");
      sc.Add( @"contagemresultadonotificacaodemandawc", "dll");
      sc.Add( @"demandanotificacoeswc", "dll");
      sc.Add( @"contagemresultado_qawc", "dll");
      sc.Add( @"wp_qa", "dll");
      sc.Add( @"perfilmenuswc", "dll");
      sc.Add( @"wp_migrarrequisitos", "dll");
      sc.Add( @"wp_corretor", "dll");
      sc.Add( @"wp_descricaocomplementar", "dll");
      sc.Add( @"usuariocertificadowc", "dll");
      sc.Add( @"viewpessoacertificado", "dll");
      sc.Add( @"wwpessoacertificado", "dll");
      sc.Add( @"promptpessoacertificado", "dll");
      sc.Add( @"pessoacertificadogeneral", "dll");
      sc.Add( @"wp_summaryadd", "dll");
      sc.Add( @"usuarioareasdetrabalhowc", "dll");
      sc.Add( @"wp_associarusuariocontratadas", "dll");
      sc.Add( @"webpanel2", "dll");
      sc.Add( @"wwlinhanegocio", "dll");
      sc.Add( @"promptlinhanegocio", "dll");
      sc.Add( @"viewlinhanegocio", "dll");
      sc.Add( @"linhanegociogeneral", "dll");
      sc.Add( @"linhanegocioservicoprocessowc", "dll");
      sc.Add( @"linhanegociotiporequisitowc", "dll");
      sc.Add( @"viewtiporequisito", "dll");
      sc.Add( @"wwtiporequisito", "dll");
      sc.Add( @"prompttiporequisito", "dll");
      sc.Add( @"tiporequisitogeneral", "dll");
      sc.Add( @"viewindicador", "dll");
      sc.Add( @"wwindicador", "dll");
      sc.Add( @"promptindicador", "dll");
      sc.Add( @"indicadorgeneral", "dll");
      sc.Add( @"cntsrvremuneracaowc", "dll");
      sc.Add( @"wwusuarionotifica", "dll");
      sc.Add( @"promptusuarionotifica", "dll");
      sc.Add( @"usuarionotificageneral", "dll");
      sc.Add( @"viewusuarionotifica", "dll");
      sc.Add( @"usuarionotificacarwc", "dll");
      sc.Add( @"wp_relatoriogerencial", "dll");
      sc.Add( @"wp_git_geralistadll", "dll");
      sc.Add( @"webpanel3", "dll");
      sc.Add( @"wp_dashboard", "dll");
      sc.Add( @"webpanel4", "dll");
      sc.Add( @"wwcontratocaixaentrada", "dll");
      sc.Add( @"contratocaixaentradageneral", "dll");
      sc.Add( @"viewcontratocaixaentrada", "dll");
      sc.Add( @"contratocontratocaixaentradawc", "dll");
      sc.Add( @"wp_apresentarcontagem", "dll");
      sc.Add( @"wp_disparoservicovinculado", "dll");
      sc.Add( @"viewcontratoservicosunidconversao", "dll");
      sc.Add( @"wwcontratoservicosunidconversao", "dll");
      sc.Add( @"promptcontratoservicosunidconversao", "dll");
      sc.Add( @"contratoservicosunidconversaogeneral", "dll");
      sc.Add( @"contratoservicoscontratoservicosunidconversaowc", "dll");
      sc.Add( @"wp_dashboardvs2", "dll");
      sc.Add( @"webpanel5", "dll");
      sc.Add( @"wwfatoresimpacto", "dll");
      sc.Add( @"fatoresimpactogeneral", "dll");
      sc.Add( @"viewfatoresimpacto", "dll");
      sc.Add( @"webpanel6", "dll");
      sc.Add( @"viewsistemaexcfuntrn", "dll");
      sc.Add( @"viewsistemaexcfundados", "dll");
      sc.Add( @"wp_osnew", "dll");
      sc.Add( @"wp_relatoriocomparacaodemandasmodelo", "dll");
      sc.Add( @"referenciainmanexoswc", "dll");
      sc.Add( @"wp_sistemahelpvs2", "dll");
      sc.Add( @"wp_referenciainmanexos", "dll");
      sc.Add( @"wp_relatoriocomparacaodemandas", "dll");
      sc.Add( @"wp_projeto", "dll");
      sc.Add( @"wp_loteanexos", "dll");
      sc.Add( @"viewequipe", "dll");
      sc.Add( @"wwequipe", "dll");
      sc.Add( @"promptequipe", "dll");
      sc.Add( @"equipegeneral", "dll");
      sc.Add( @"equipeusuariowc", "dll");
      sc.Add( @"wp_demandasvinculadas", "dll");
      sc.Add( @"wp_importarpffs", "dll");
      sc.Add( @"wp_relatoriocomparacaodemandascopy1", "dll");
      sc.Add( @"wp_demandaalterarqntsolicitada", "dll");
      sc.Add( @"usuario", "dll");
      sc.Add( @"usuarioperfil", "dll");
      sc.Add( @"perfil", "dll");
      sc.Add( @"areatrabalho", "dll");
      sc.Add( @"contratante", "dll");
      sc.Add( @"bancoagencia", "dll");
      sc.Add( @"banco", "dll");
      sc.Add( @"pais", "dll");
      sc.Add( @"estado", "dll");
      sc.Add( @"municipio", "dll");
      sc.Add( @"pessoa", "dll");
      sc.Add( @"pessoa_bc", "dll");
      sc.Add( @"contratada", "dll");
      sc.Add( @"contratanteusuario", "dll");
      sc.Add( @"contratadausuario", "dll");
      sc.Add( @"contrato", "dll");
      sc.Add( @"guia", "dll");
      sc.Add( @"referenciatecnica", "dll");
      sc.Add( @"areatrabalho_guias", "dll");
      sc.Add( @"contratogarantia", "dll");
      sc.Add( @"contratoarquivosanexos", "dll");
      sc.Add( @"sistema", "dll");
      sc.Add( @"tecnologia", "dll");
      sc.Add( @"metodologia", "dll");
      sc.Add( @"modulo", "dll");
      sc.Add( @"metodologiafases", "dll");
      sc.Add( @"padroesartefatos", "dll");
      sc.Add( @"contratoclausulas", "dll");
      sc.Add( @"servico", "dll");
      sc.Add( @"servicogrupo", "dll");
      sc.Add( @"contratoservicos", "dll");
      sc.Add( @"funcaousuario", "dll");
      sc.Add( @"funcaoapf", "dll");
      sc.Add( @"funcoesapfatributos", "dll");
      sc.Add( @"tabela", "dll");
      sc.Add( @"atributos", "dll");
      sc.Add( @"modulofuncoes", "dll");
      sc.Add( @"funcoesusuariofuncoesapf", "dll");
      sc.Add( @"contagem", "dll");
      sc.Add( @"contagemitem", "dll");
      sc.Add( @"contagemitematributosfsoftware", "dll");
      sc.Add( @"contagemitemparecer", "dll");
      sc.Add( @"contagemitematributosfmetrica", "dll");
      sc.Add( @"menu", "dll");
      sc.Add( @"menuperfil", "dll");
      sc.Add( @"contratodadoscertame", "dll");
      sc.Add( @"contratotermoaditivo", "dll");
      sc.Add( @"contratoocorrencia", "dll");
      sc.Add( @"contratoobrigacao", "dll");
      sc.Add( @"servicoans", "dll");
      sc.Add( @"contratoocorrencianotificacao", "dll");
      sc.Add( @"parametrossistema", "dll");
      sc.Add( @"ambientetecnologicotecnologias", "dll");
      sc.Add( @"funcaodados", "dll");
      sc.Add( @"funcaodadostabela", "dll");
      sc.Add( @"ambientetecnologico", "dll");
      sc.Add( @"funcaoapfevidencia", "dll");
      sc.Add( @"status", "dll");
      sc.Add( @"naoconformidade", "dll");
      sc.Add( @"tmp_file", "dll");
      sc.Add( @"solicitacoes", "dll");
      sc.Add( @"solicitacoesitens", "dll");
      sc.Add( @"contagemresultado", "dll");
      sc.Add( @"contagemresultadocontagens", "dll");
      sc.Add( @"selected", "dll");
      sc.Add( @"lote", "dll");
      sc.Add( @"contagemresultadoerro", "dll");
      sc.Add( @"contagemresultadoevidencia", "dll");
      sc.Add( @"geral_tp_uo", "dll");
      sc.Add( @"geral_unidadeorganizacional", "dll");
      sc.Add( @"geral_grupo_cargo", "dll");
      sc.Add( @"geral_cargo", "dll");
      sc.Add( @"geral_grupo_funcao", "dll");
      sc.Add( @"geral_funcao", "dll");
      sc.Add( @"tipodocumento", "dll");
      sc.Add( @"projeto", "dll");
      sc.Add( @"projetomelhoria", "dll");
      sc.Add( @"referenciainm", "dll");
      sc.Add( @"itemnaomensuravel", "dll");
      sc.Add( @"baseline", "dll");
      sc.Add( @"checklist", "dll");
      sc.Add( @"contagemresultadochcklst", "dll");
      sc.Add( @"insertregistros", "dll");
      sc.Add( @"contagemresultadoitem", "dll");
      sc.Add( @"contagemresultadochcklstlog", "dll");
      sc.Add( @"usuarioservicos", "dll");
      sc.Add( @"lotearquivoanexo", "dll");
      sc.Add( @"parametrosplanilhas", "dll");
      sc.Add( @"glosario", "dll");
      sc.Add( @"regrascontagem", "dll");
      sc.Add( @"tipodeconta", "dll");
      sc.Add( @"caixa", "dll");
      sc.Add( @"logresponsavel", "dll");
      sc.Add( @"contratoservicosprazo", "dll");
      sc.Add( @"contratoservicosvnc", "dll");
      sc.Add( @"contratoservicostelas", "dll");
      sc.Add( @"variaveiscocomo", "dll");
      sc.Add( @"tipoprojeto", "dll");
      sc.Add( @"solicitacaomudanca", "dll");
      sc.Add( @"solicitacaomudancaitem", "dll");
      sc.Add( @"regrascontagemanexos", "dll");
      sc.Add( @"contagemresultadoliqlog", "dll");
      sc.Add( @"contagemresultadoimplog", "dll");
      sc.Add( @"contratoservicossistemas", "dll");
      sc.Add( @"contratogestor", "dll");
      sc.Add( @"anexos", "dll");
      sc.Add( @"suporte", "dll");
      sc.Add( @"contagemhistorico", "dll");
      sc.Add( @"feriados", "dll");
      sc.Add( @"agendaatendimento", "dll");
      sc.Add( @"unidademedicao", "dll");
      sc.Add( @"contratounidades", "dll");
      sc.Add( @"organizacao", "dll");
      sc.Add( @"contagemresultadoincidentes", "dll");
      sc.Add( @"funcaoapfpreimp", "dll");
      sc.Add( @"funcaodadospreimp", "dll");
      sc.Add( @"contratoservicosindicador", "dll");
      sc.Add( @"contagemresultadoindicadores", "dll");
      sc.Add( @"contagemresultadonotas", "dll");
      sc.Add( @"contratoservicosprioridade", "dll");
      sc.Add( @"redmine", "dll");
      sc.Add( @"contagemresultadoexecucao", "dll");
      sc.Add( @"contagemresultadonotificacao", "dll");
      sc.Add( @"audit", "dll");
      sc.Add( @"servicoprioridade", "dll");
      sc.Add( @"sistemadepara", "dll");
      sc.Add( @"contratoservicosdepara", "dll");
      sc.Add( @"contratoservicoscusto", "dll");
      sc.Add( @"gestao", "dll");
      sc.Add( @"servicofluxo", "dll");
      sc.Add( @"servicoresponsavel", "dll");
      sc.Add( @"servicoprocesso", "dll");
      sc.Add( @"notaempenho", "dll");
      sc.Add( @"saldocontrato", "dll");
      sc.Add( @"historicoconsumo", "dll");
      sc.Add( @"solicitacaoservico", "dll");
      sc.Add( @"helpsistema", "dll");
      sc.Add( @"tributo", "dll");
      sc.Add( @"email", "dll");
      sc.Add( @"email_instancia", "dll");
      sc.Add( @"email_instancia_destinatarios", "dll");
      sc.Add( @"logerrobc", "dll");
      sc.Add( @"proposta", "dll");
      sc.Add( @"contratosistemas", "dll");
      sc.Add( @"contratanteusuariosistema", "dll");
      sc.Add( @"artefatos", "dll");
      sc.Add( @"catalogosutentacao", "dll");
      sc.Add( @"catalogosutentacaoartefatos", "dll");
      sc.Add( @"contratoservicosartefatos", "dll");
      sc.Add( @"servicoartefatos", "dll");
      sc.Add( @"contagemresultadoartefato", "dll");
      sc.Add( @"autorizacaoconsumo", "dll");
      sc.Add( @"catalogoservicos", "dll");
      sc.Add( @"contratoauxiliar", "dll");
      sc.Add( @"grupoobjetocontrole", "dll");
      sc.Add( @"gpoobjctrlresponsavel", "dll");
      sc.Add( @"check", "dll");
      sc.Add( @"servicocheck", "dll");
      sc.Add( @"sistemaversao", "dll");
      sc.Add( @"alerta", "dll");
      sc.Add( @"alertaread", "dll");
      sc.Add( @"solicitacaoservicoreqnegocio", "dll");
      sc.Add( @"requisito", "dll");
      sc.Add( @"reqnegreqtec", "dll");
      sc.Add( @"contagemresultadoqa", "dll");
      sc.Add( @"contagemresultadorequisito", "dll");
      sc.Add( @"pessoacertificado", "dll");
      sc.Add( @"tiponaoconformidade", "dll");
      sc.Add( @"contagemresultadonaocnf", "dll");
      sc.Add( @"linhanegocio", "dll");
      sc.Add( @"tiporequisito", "dll");
      sc.Add( @"indicador", "dll");
      sc.Add( @"usuarionotifica", "dll");
      sc.Add( @"contratocaixaentrada", "dll");
      sc.Add( @"contratoservicosunidconversao", "dll");
      sc.Add( @"fatoresimpacto", "dll");
      sc.Add( @"equipe", "dll");
      return sc ;
   }

   public override ItemCollection GetResBuildList( )
   {
      ItemCollection sc = new ItemCollection() ;
      sc.Add( @"bin\messages.por.dll", cs_path + @"\messages.por.txt");
      return sc ;
   }

   public override bool ToBuild( String obj )
   {
      if (checkTime(obj, cs_path + @"\bin\GxClasses.dll" ))
         return true;
      if ( obj == @"bin\GeneXus.Programs.Common.dll" )
      {
         if (checkTime(obj, cs_path + @"\type_SdtSDT_WS_ConsultarAutorizacaoUsuario.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_WsFiltros.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_WS_Demandas.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_WS_Demandas_Demanda.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_WSAutenticacao.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_OSVinculada.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Requisitos_SDT_RequisitosItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGeoRegions_GeoRegionsItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGeoMarkers_GeoMarkersItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtjqSelectData_Item.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Parametros.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtDVB_SidebarMenuOptionsData_Item.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Periodos.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_UsuarioSistema.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_ContratoServicoUnidadeMedicao.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Artefatos.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_AreaTrabalho_Sistema.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_ID_Valor.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_SaldoContratoCB.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSchedulerEvents.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSchedulerEvents_Day.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSchedulerEvents_event.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineuser.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineuser_memberships.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineuser_memberships_membership.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineuser_memberships_membership_project.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineuser_memberships_membership_roles.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineuser_memberships_membership_roles_role.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_CustomFields_Item.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Associar.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_AutenticacaoIN.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_AuntenticacaoOUT.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_AuntenticacaoOUT_Error.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_AuntenticacaoOUT_Usuario.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_AuntenticacaoOUT_Usuario_Perfil.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineIssuePut.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineIssuePut_Issue.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineStatus.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineStatus_issue_status.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_assigned_to.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_attachments.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_attachments_attachment.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_attachments_attachment_author.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_author.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_changesets.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_changesets_changeset.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_children.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_children_issue.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_children_issue_children.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_children_issue_children_issue.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_children_issue_children_issue_tracker.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_children_issue_tracker.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_custom_fields.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_custom_fields_custom_field.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_journals.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_journals_journal.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_journals_journal_details.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_journals_journal_details_detail.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_journals_journal_user.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_priority.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_project.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_status.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineissue_tracker.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineIssues.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineIssues_issue.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineIssues_issue_assigned_to.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineIssues_issue_author.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineIssues_issue_custom_fields.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineIssues_issue_custom_fields_custom_field.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineIssues_issue_priority.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineIssues_issue_project.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineIssues_issue_status.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineIssues_issue_tracker.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineTrackers.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineTrackers_tracker.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmine144_issues.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmine144_issues_issue.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmine144_issues_issue_assigned_to.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmine144_issues_issue_author.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmine144_issues_issue_custom_fields.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmine144_issues_issue_custom_fields_custom_field.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmine144_issues_issue_fixed_version.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmine144_issues_issue_priority.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmine144_issues_issue_project.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmine144_issues_issue_status.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmine144_issues_issue_tracker.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineUsers.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineUsers_user.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineUsers_user_custom_fields.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_RedmineUsers_user_custom_fields_custom_field.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineprojects.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineprojects_project.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Redmineprojects_project_parent.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtUploadifyOutput.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGxSynchroInfoSDT.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_AnexosDemanda_SDT_AnexosDemandaItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Baselines_Funcao.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_MonitorDmn.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtWizardSteps_WizardStepsItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtWizardAuxiliarData_WizardAuxiliarDataItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtDVB_SDTDropDownOptionsTitleSettingsIcons.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_VariaveisCocomo_Item.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtTreeNodeCollection_TreeNode.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_ConsultaSistemas_Sistema.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Grafico_Item.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_CheckList_Item.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_CloneAtributo_Atributo.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_ItensConagem_Item.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Codigos.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Demandas_Demanda.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_ContagemResultadoEvidencias_Arquivo.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_FiltroConsContadorFM.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGxChart.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGxChart_Serie.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerParameters_Parameter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerItemExpandData.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerItemDoubleClickData.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerItemDoubleClickData_Element.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerItemDoubleClickData_Filter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerItemCollapseData.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerItemClickData.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerItemClickData_Element.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerItemClickData_Filter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerFilterChangedData.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerDragAndDropData.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerAxes_Axis.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerAxes_Axis_AxisOrder.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerAxes_Axis_ExpandCollapse.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerAxes_Axis_Filter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerAxes_Axis_Format.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerAxes_Axis_Format_ConditionalStyle.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerAxes_Axis_Format_ValueStyle.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerAxes_Axis_Grouping.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtQueryViewerAggregationChangedData.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtTargets_Target.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGxSynchroEventResultSDT_GxSynchroEventResultSDTItem_MappingsItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGxEventFK.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGxKeyValue.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGxSynchroEventSDT_GxSynchroEventSDTItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtTileSDT.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtPurchaseReceiptInformation.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_ManterDadosREC.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtInNewWindowTargets_InNewWindowTargetsItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_Usuarios_Usuario.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDTSolicitacao_SDTSolicitacaoItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtFckEditorMenu_FckEditorMenuItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtTabOptions_TabOptionsItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtContext.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSDT_MenuAcessoRapido_SDT_MenuAcessoRapidoItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSectionsItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMExampleSDTApplicationData.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtGoogleDocsResult.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtGoogleDocsResult_Doc.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtGridStateCollection_Item.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtDVB_SDTDropDownOptionsData_Item.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtWWPTransactionContext.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtWWPTransactionContext_Attribute.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtWWPTabOptions_TabOptionsItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtWWPGridState.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtWWPGridState_DynamicFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtWWPGridState_FilterValue.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtWWPColumnsSelector.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtWWPColumnsSelector_InvisibleColumn.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtWWPColumnsSelector_VisibleColumn.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtTabsMenuData_TabsMenuDataItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtTabsMenuData_TabsMenuDataItem_SectionsItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGxMap.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGxMap_Point.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtColumnsSelectorSDT_ColumnsSelectorSDTItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtWWPContext.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtProgramNames_ProgramName.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtAuditingObject.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtAuditingObject_RecordItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\wwpbaseobjects\type_SdtAuditingObject_RecordItem_AttributeItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtNotificationInfo.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtLinkList_LinkItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtMessages_Message.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GxObjectCollection.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GxSilentTrnGridCollection.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\SoapParm.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GxWebStd.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GxFullTextSearchReindexer.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GxModelInfoProvider.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContagemResultadoNewOS.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtModuloFuncoes.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSolicitacoesItens.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContratoServicosDePara.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtServicoResponsavel.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtEmail_Instancia_Destinatarios.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtEmail_Instancia.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtHelpSistema.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtLogErroBC.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtCatalogoSutentacaoArtefatos.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGpoObjCtrlResponsavel.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtReqNegReqTec.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtFuncoesUsuarioFuncoesAPF.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContagemItemAtributosFMetrica.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContagemItemAtributosFSoftware.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtAreaTrabalho_Guias.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtMenuPerfil.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtAmbienteTecnologicoTecnologias.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtFuncaoDadosTabela.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtRegrasContagemAnexos.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSistemaDePara.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContratoSistemas.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContratanteUsuarioSistema.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtFuncoesAPFAtributos.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtAtributos.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtTabela.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtMenu.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtParametrosSistema.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtReferenciaINM.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtReferenciaINM_Anexos.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtLoteArquivoAnexo.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtFeriados.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContratoServicosArtefatos.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtTmp_File.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtUsuarioPerfil.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContratadaUsuario.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContrato.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSolicitacoes.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtUsuarioServicos.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtProjeto.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtProjeto_Anexos.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContratoServicosSistemas.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContratoGestor.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContratoServicosPrioridade.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtAudit.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtServicoFluxo.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtServicoArtefatos.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContratoAuxiliar.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtServicoCheck.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtRequisito.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtAreaTrabalho.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtPessoa.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContratante.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContratanteUsuario.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContratada.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtModulo.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtUsuario.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtServico.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSistema.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSistema_Tecnologia.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContratoServicos.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContratoServicos_Rmn.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContagemResultadoContagens.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContagemResultadoEvidencia.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContagemResultadoChckLstLog.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContagemResultadoNotas.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtAnexos.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtAnexos_De.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContagemResultadoArtefato.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContagemResultadoNotificacao.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContagemResultadoNotificacao_Demanda.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContagemResultadoNotificacao_Destinatario.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSolicitacaoServicoReqNegocio.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContagemResultadoQA.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContagemResultadoRequisito.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtProposta.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtSolicitacaoServico.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtPerfil.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtContagemResultado.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtWebNotification.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMDescription.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMProperty.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMError.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTypeFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTypeSimple.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMCountry.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMCountryLanguages.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMRepository.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMUpdateRepositoryConfiguration.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMUpdateRepositoryConfigurationApplicationsToImport.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMLoginAdditionalParameters.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMSecurityPolicy.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMUserFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMUser.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMUserAttribute.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMUserAttributeMultiValues.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMRole.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMRoleFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMApplication.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMApplicationEnvironment.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMApplicationDelegateAuthorization.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMApplicationPermissionFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMApplicationPermission.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMApplicationToken.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMApplicationTokenElement.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMPermissionFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMPermission.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMSessionLogFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMSessionLog.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMSessionLogLoginRetry.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMSecurityPolicyFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMRepositoryConnectionFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMRepositoryConnection.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMRepositoryConnectionAddressList.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMApplicationFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMSession.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMSessionLoginRetry.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMSessionRole.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationFacebook.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTypeFacebook.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTypeLocal.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTypeTrusted.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTypeWebService.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationWebService.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationWebServiceServer.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTwitter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTypeTwitter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAM.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMRepositoryCreate.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMRepositoryFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMImportRepositoryConfiguration.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMRepositoryConnectionFileFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMConnectionInfo.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMConnectionInfoProperties.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMConnection.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMConnectionProperties.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationGoogle.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTypeGoogle.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationCustom.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTypeCustom.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationGAMRemote.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTypeGAMRemote.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGxCbb.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtServerSideSign.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtCrcStream.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtPDFTools.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtIgImageEditor.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINCryptoSignAlgorithm.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMExternalAuthentication.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMVersion.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMUserListOrder.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMAllowMultipleConcurrentSessions.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMPermissionAccessTypeDefault.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMPermissionTypeFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMBooleanFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMExternalAuthorizationVersions.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMSessionLogListOrder.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMTracing.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoPessoa.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINUnidadeContratacao.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINUnidadeGuiaRef.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoNaoConformidade.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoStatus.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoDados.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoUnidade.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTecnicaContagem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoContagem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINStatusItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINStatusContagem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoPerfil.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoMenu.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINMessageTypes.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINFckEditorObjectInterface.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINFckEditorModes.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoFuncaoAPF.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoFuncaoDados.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINComplexidade.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINStatusDaFuncao.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINProgressIndicatorType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINDVelopConfirmPanelType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINStatusDemanda.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoReferencia.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINDMStatusSolicitacoes.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoFabrica.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINAPIAuthorizationStatus.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINEventExecution.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINSmartDeviceType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINRecentLinksOptions.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINCameraAPIQuality.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINAudioAPISessionType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINNetworkAPIConnectionType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINEventAction.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINEventStatus.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINApplicationState.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINSynchronizationReceiveResult.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINRegionState.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINIMEMode.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINBeaconProximity.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINCobranca.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINQueryViewerChartType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINQueryViewerOutputType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINQueryViewerXAxisLabels.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINQueryViewerAggregationType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINQueryViewerAxisOrderType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINQueryViewerAxisType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINQueryViewerConditionOperator.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINQueryViewerExpandCollapse.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINCallTargetSize.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINQueryViewerFilterType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINQueryViewerObjectType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINQueryViewerSubtotals.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINQueryViewerShowDataLabelsIn.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoErroContagem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINStatusErroContagem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINLocalExecucao.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINStatusProjeto.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoINM.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipodePrazo.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINgxuiToolbarItemArrowAlign.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINgxuiToolbarItemIconAlign.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINgxuiToolbarItemScale.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINgxuiToolbarItemTypes.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINPdfCertificationLevel.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTela.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINServicoAtende.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTabela.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMConstant.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINWWPDomains.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINNotificacaoMidia.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINObrigaValores.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINObjetoControle.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoDias.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoManutencao.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINOrigem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINEvento.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoHierarquia.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINContagemResultado_TipoRegistro.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoStandBy.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINHistoricoCodigo.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINSaldoContratoOperacao.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINNivelContagem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINAutorizacaoConsumoStatus.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINCheckMomento.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoRequisito.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINStatusRequisito.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINAlertaToGroup.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINReqPrioridade.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINHttpMethod.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINReqSituacao.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINRequisitoStatus.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINNecessidadeStatus.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINPDFpermission.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINBarcodeType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINPage.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINLogFile.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoGestor.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTecnicaMoSCoW.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINIgImageEditorFormat.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINFiltroStatusDemanda.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINFiltroTipoData.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTrnMode.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINFiltroPeriodo.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINFiltroStatusDemandaCaixaEntrada.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINExportType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINEncoding.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMUserActivationMethod.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMUserGender.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMRepositoryRememberUserTypes.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTimezones.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMAuthenticationFunctions.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMAuthenticationTypes.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMBrowser.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINEffect.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMErrorMessages.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMGenerateAuditory.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMGenerateSessionStatistics.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINCallType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMIdentificatorKey.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMMenuOptionType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMPermissionAccessType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINCryptoEncryptAlgorithm.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMRecoveryPasswordTypes.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMRememberUserTypes.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMRepositoryConnectionTypes.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMSessionStatus.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMSessionType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINCryptoHashAlgorithm.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMAPiMode.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMRepositoryUserIdentifications.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMApplicationAuthorizarionRequestType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMApplicationType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMAutExtOAuthVersions.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMAutExtOpenIdVersions.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMAutExtWebServiceVersions.cs" ))
            return true;
      }
      if ( obj == @"bin\messages.por.dll" )
      {
         if (checkTime(obj, cs_path + @"\messages.por.txt" ))
            return true;
      }
      return false ;
   }

}

