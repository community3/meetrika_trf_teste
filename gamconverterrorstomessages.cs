/*
               File: GAMConvertErrorsToMessages
        Description: GAMConvert Errors To Messages
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:48.96
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gamconverterrorstomessages : GXProcedure
   {
      public gamconverterrorstomessages( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public gamconverterrorstomessages( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( IGxCollection aP0_Errors ,
                           out IGxCollection aP1_Messages )
      {
         this.AV10Errors = aP0_Errors;
         this.AV8Messages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP1_Messages=this.AV8Messages;
      }

      public IGxCollection executeUdp( IGxCollection aP0_Errors )
      {
         this.AV10Errors = aP0_Errors;
         this.AV8Messages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP1_Messages=this.AV8Messages;
         return AV8Messages ;
      }

      public void executeSubmit( IGxCollection aP0_Errors ,
                                 out IGxCollection aP1_Messages )
      {
         gamconverterrorstomessages objgamconverterrorstomessages;
         objgamconverterrorstomessages = new gamconverterrorstomessages();
         objgamconverterrorstomessages.AV10Errors = aP0_Errors;
         objgamconverterrorstomessages.AV8Messages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         objgamconverterrorstomessages.context.SetSubmitInitialConfig(context);
         objgamconverterrorstomessages.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgamconverterrorstomessages);
         aP1_Messages=this.AV8Messages;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((gamconverterrorstomessages)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV14GXV1 = 1;
         while ( AV14GXV1 <= AV10Errors.Count )
         {
            AV9Error = ((SdtGAMError)AV10Errors.Item(AV14GXV1));
            AV11Message = new SdtMessages_Message(context);
            AV11Message.gxTpr_Type = 1;
            AV11Message.gxTpr_Description = AV9Error.gxTpr_Message;
            AV11Message.gxTpr_Id = StringUtil.Format( "GAM%2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Error.gxTpr_Code), 12, 0)), "", "", "", "", "", "", "", "");
            AV8Messages.Add(AV11Message, 0);
            AV14GXV1 = (int)(AV14GXV1+1);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV9Error = new SdtGAMError(context);
         AV11Message = new SdtMessages_Message(context);
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV14GXV1 ;
      private IGxCollection aP1_Messages ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV8Messages ;
      [ObjectCollection(ItemType=typeof( SdtGAMError ))]
      private IGxCollection AV10Errors ;
      private SdtMessages_Message AV11Message ;
      private SdtGAMError AV9Error ;
   }

}
